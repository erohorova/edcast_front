'use strict';
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name: ['LXP WEB - Production'],
  /**
   * Your New Relic license key.
   */
  license_key: '4022378fe51dd354cbafd36738dab557c08599e7',
  logging: {
    /**
     * Level at which to log. "trace" is most useful to New Relic when diagnosing
     * issues with the agent, "info" and higher will impose the least overhead on
     * production applications.
     */
    level: 'info'
  },
  browser_monitoring: {
    enable: true
  },
  capture_params: false,
  rules: {
    ignore: ['/*.js', '/.css', '/*.png', '/*.map', '/*.woff', '/*.ico', '/*.gif', '/*.jpg']
  }
};
