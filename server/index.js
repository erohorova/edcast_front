var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('./config/logger')('debug');
var path = require('path');
var glob = require('glob');

var application = app => {
  // If passing in a defined application, reuse it
  if (!app) {
    app = express();
    console.log('Creating Express App');
  }

  if (process.env.NODE_ENV == 'production') {
    // We need to set max age and cache control here
    // also on assets.js
    app.get(/.*dist\-.*(js|css)$/, function(req, res, next) {
      req.url = req.url + '.gz';
      res.set('Content-Encoding', 'gzip');
      switch (req.params[0]) {
        case 'css':
          res.set('Content-type', 'text/css');
          break;
        case 'js':
          res.set('Content-type', 'application/javascript');
          break;
        default:
          console.warn('Unspecified file extension in gzip production file handler.');
      }
      next();
    });
  }

  app.use(
    express.static(path.resolve('.') + '/public', {
      maxAge: '30d' // set for 30 days
    })
  );
  app.use(cookieParser());
  app.use(bodyParser.json());
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  );

  require('./config/view')(app, logger);

  var sink = {
    start: function() {
      glob.sync('./src/**/*.api.server.js').forEach(function(file) {
        console.log(`LOADING SERVER API: ${file}`);
        require(path.resolve(file))(app);
      });
      // Start our server
      require('./config/serverManager')(app, logger);
      require('./config/router')(app, logger);
    },

    getApp: function() {
      return app;
    }
  };

  module.exports = sink;
  return sink;
};

module.exports = application();
