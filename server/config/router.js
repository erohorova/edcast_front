/**
 * @module server/config/router
 */

var express = require('express');
var router = express.Router();
var path = require('path');
var request = require('superagent');
var fs = require('fs');

var maintenance = false;

module.exports = function(app, logger) {
  var envVars = app.configMgr.getClientEnvVars();
  envVars.NODE_ENV = process.env.NODE_ENV;
  envVars.UpshotApplicationOwnerID = process.env.UpshotApplicationOwnerID;
  envVars.UpshotApplicationID = process.env.UpshotApplicationID;

  envVars.JsPublic = process.env.JS_PUBLIC_KEY;
  envVars.TRANSLATION_DOMAIN =
    process.env.TRANSLATION_DOMAIN || 'https://dol6fvsidtwsq.cloudfront.net';

  envVars.FILESTACK_APIKEY = process.env.FILESTACK_APIKEY;
  envVars.FilestackDefaultImagesUrl = process.env.FILESTACK_DEFAULT_IMAGES_URL;
  envVars.FILESTACK_CIPHER = process.env.FILESTACK_CIPHER;
  envVars.FILESTACK_SECRET = process.env.FILESTACK_SECRET;

  envVars.FILESTACK_DEFAULT_EXPIRY = 900;

  envVars.SECURE_AUTHENTICATED_IMAGES = process.env.SECURE_AUTHENTICATED_IMAGES;
  envVars.SECURE_AUTHENTICATED_IMAGES_KEY = process.env.SECURE_AUTHENTICATED_IMAGES_KEY;

  // read only once (version changes only on new build)
  var version = require(path.resolve('.') + '/version.json');

  // cache content of static index.html or maintenance.html page
  // TODO: to minify?
  var indexPagePath = path.resolve(
    __dirname,
    `static/${maintenance ? 'maintenance' : 'index'}.html`
  );
  var indexPageContent = fs.readFileSync(indexPagePath, 'utf8');

  var renderPage = function(req, res) {
    // Everything should be handled by React Router
    return res.send(indexPageContent);
  };

  // TODO: recover New Relic!

  app.use('/log_in', function(req, res) {
    return renderPage(req, res);
  });

  var re = new RegExp('wp-corp.edcastcloud.net', 'g');
  if (process.env.NODE_ENV === 'production') {
    app.use('/', function(req, res, next) {
      if (req.path === '/' && req.hostname && req.hostname.split('.')[0] === 'www') {
        request.get('http://wp-corp.edcastcloud.net/corp/', function(err, resp) {
          var html = resp.text;
          html = html.replace(re, req.hostname);
          return res.send(html);
        });
      } else {
        return next();
      }
    });
  }

  // only static params that doesn't depend on such of process.env (because it's cached by service worker)
  var viewParams =
    process.env.NODE_ENV === 'production'
      ? {
          common: '/dist-' + version.v + '-common.js',
          bootstrap: '/dist-' + version.v + '-bootstrap.js',
          app: '/dist-' + version.v + '-main.js',
          css: '/dist-' + version.v + '-main.css',
          env_vars: JSON.stringify(envVars)
        }
      : {
          common: 'http://localhost:8001/common.js',
          bootstrap: 'http://localhost:8001/bootstrap.js',
          app: 'http://localhost:8001/main.js',
          css: 'http://localhost:8001/main.css',
          env_vars: JSON.stringify(envVars)
        };

  router.get('/assets.js', function(req, res) {
    res.set('Content-Type', 'application/javascript');
    // returning generated assets (going to be cached by service worker)
    return res.render('assets.js', viewParams);
  });
  /* All page routes */
  router.get(['*'], function(req, res) {
    renderPage(req, res);
  });

  app.use('/', router);

  app.use(function(req, res, next) {
    res.redirect('/404');
  });
};
