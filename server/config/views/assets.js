// do not use es6 syntax in this file as it doesn't handled by babel

window.process = process = {
  env: {{{env_vars}}}
}


//  <<< START register service worker

// In production, we register a service worker to serve assets from local cache.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on the "N+1" visit to a page, since previously
// cached resources are updated in the background.
// To learn more about the benefits of this model, read https://goo.gl/KwvDNy.
// This link also includes instructions on opting out of this behavior.
var isLocalhost = Boolean(
  window.location.hostname === 'localhost' ||
  // [::1] is the IPv6 localhost address.
  window.location.hostname === '[::1]' ||
  // 127.0.0.1/8 is considered localhost for IPv4.
  window.location.hostname.match(
    /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
  )
);

function registerServiceWorker() {
  if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
    // The URL constructor is available in all browsers that support SW.
    var publicUrl = new URL('/', window.location);
    if (publicUrl.origin !== window.location.origin) {
      // Our service worker won't work if PUBLIC_URL is on a different origin
      // from what our page is served on. This might happen if a CDN is used to
      // serve assets; see https://github.com/facebookincubator/create-react-app/issues/2374
      return;
    }

    window.addEventListener('load', function () {
      var swUrl = '/service-worker.js';

      if (isLocalhost) {
        // This is running on localhost. Lets check if a service worker still exists or not.
        checkValidServiceWorker(swUrl);

        // Add some additional logging to localhost, pointing developers to the
        // service worker/PWA documentation.
        navigator.serviceWorker.ready.then(function () {
          console.log('This web app is being served cache-first by a service ' +
            'worker. To learn more, visit https://goo.gl/SC7cgQ');
        });
      } else {
        // Is not local host. Just register service worker
        registerValidSW(swUrl);
      }
    });
  }
}


// IE9+ CustomEvent polyfill
(function () {
  if ( typeof window.CustomEvent === "function" ) return false; //If not IE
  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
  }
  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
})();


function triggerRequireReload() {
  var requireReloadEvent = new CustomEvent("require-reload-page");
  document.dispatchEvent(requireReloadEvent);
  window.requireReloadPage = true;
  console.warn('New content is available; need refreshing...');
}

function registerValidSW(swUrl) {
  navigator.serviceWorker
    .register(swUrl)
    .then(function (registration) {
    registration.onupdatefound = function () {
      var installingWorker = registration.installing;
      installingWorker.onstatechange = function () {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the old content will have been purged and
            // the fresh content will have been added to the cache.
            // It's the perfect time to display a "New content is
            // available; please refresh." message in your web app.
            // console.log('New content is available; please refresh.');
            // triggerRequireReload();
            document.getElementById('splashscreen').className += " swisreloading";
            window.location.reload();
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.');
          }
        }
      };
    };
  }).catch(function (error) {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl).then(function (response) {
    // Ensure service worker exists, and that we really are getting a JS file.
    if (response.status === 404 || response.headers.get('content-type').indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(function (registration) {
        registration.unregister().then(function () {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl);
    }
  }).catch(function () {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

// <<< END register service worker








var remoteStyles = [
  'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i',
  '{{css}}',
];

var remoteScripts = [
  '{{common}}',
  '{{bootstrap}}',
  '{{app}}',
];



// Asynchronously load CSS & JS with a Request Animation Frame
window.requestAnimationFrame(function () {
  var elementToInsertElementBefore = document.getElementsByTagName('body')[0];
  for (var i = 0; i < remoteStyles.length; i++) {
    var linkElement = document.createElement('link');
    linkElement.rel = 'stylesheet';
    linkElement.media = 'all';
    linkElement.href = remoteStyles[i];
    elementToInsertElementBefore.parentNode.insertBefore(linkElement,
      elementToInsertElementBefore);
  }
  for (var i = 0; i < remoteScripts.length; i++) {
    var scriptElement = document.createElement('script');
    scriptElement.async = false;
    scriptElement.defer = true;
    scriptElement.type = 'application/javascript';
    scriptElement.src = remoteScripts[i];
    elementToInsertElementBefore.parentNode.insertBefore(scriptElement,
      elementToInsertElementBefore);
  };
});


// if (window.location.hostname.split('.')[0] !== 'www') {
//   // performing registration of SW
//   registerServiceWorker();
// }
if (window.process.env.UpshotApplicationID && window.process.env.UpshotApplicationOwnerID) {
  var params = {
    "UpshotApplicationID": window.process.env.UpshotApplicationID,
    "UpshotApplicationOwnerID": window.process.env.UpshotApplicationOwnerID,
    "UpshotFetchLocation": false,
    "enableDebugLogs": !!(window.localStorage.getItem("enableConsoleLog")),
    "appVersion": "1.0.0",
    'DispatchTimeInterval': 100,
  };
  var s = document.createElement('script');
  s.type = 'text/javascript';
  s.async = true;
  s.src = 'https://cdn.goupshot.com/UpshotWebSDK/v1.4/upshot.min.js';
  var x = document.getElementsByTagName('script')[0];
  x.parentNode.insertBefore(s, x);
  s.onload=function(){
    if (window.upshot){
      window.upshot.init(params);
    }
  };
}