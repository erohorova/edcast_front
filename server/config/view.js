/**.
 * @module server/config/view
 */

/**
 * Initialization method to setup HTML and JSON views.
 * @param express {object} Express framework instance.
 * @param logger {module:kitchensink/app/logger} Default kitchensink logger.
 */
module.exports = function(app, logger) {
  app.set('view engine', 'html');
  app.enable('view cache');
  app.engine('html', require('hbs').__express);
  app.engine('js', require('hbs').__express);

  // View lookup
  app.set('view').prototype.lookup = function(name) {
    if (name.indexOf('json') >= 0) {
      logger.debug('Returned JSON view');
      return __dirname + '/views/page.json';
    } else if (name.indexOf('assets.js') >= 0) {
      logger.debug('Returned JS assets view');
      return __dirname + '/views/assets.js';
    } else {
      // logger.debug('Returned HTML view');
      // return __dirname + '/views/page.html';
      return console.error('not determined html file');
    }
  };

  // Handlebar helpers
  var hbs = require('hbs');
  hbs.registerHelper('json', function(context) {
    return JSON.stringify(context);
  });
  hbs.registerHelper('timestamp', function() {
    return new Date().getTime();
  });
  hbs.registerHelper('if-equal', function(a, b, opts) {
    if (a == b)
      // Or === depending on your needs
      return opts.fn(this);
    else return opts.inverse(this);
  });
};
