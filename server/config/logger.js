/**
 * @module server/config/logger
 */
var logger = require('winston');

/**
 * Logger constructor
 * @param level {string} Logging level: debug, info, warn, and error.
 */
var Logger = function(level) {
  /* Set up console logging */
  logger.remove(logger.transports.Console);
  logger.add(logger.transports.Console, {
    level: level,
    timestamp: true
  });

  return {
    /**
     * Log a debug message.
     * @method
     * @static
     * @param {arguments}
     */
    debug: function() {
      logger.debug.apply(logger, arguments);
    },

    /**
     * Log information.
     * @method
     * @static
     * @param {arguments}
     */
    info: function() {
      logger.info.apply(logger, arguments);
    },

    /**
     * Log a warning message.
     * @method
     * @static
     * @param {arguments}
     */
    warn: function() {
      logger.warn.apply(logger, arguments);
    },

    /**
     * Log an error message.
     * @method
     * @static
     * @param {arguments}
     */
    error: function() {
      logger.error.apply(logger, arguments);
    }
  };
};

module.exports = Logger;
