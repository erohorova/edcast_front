var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var babel = require('gulp-babel');
var eslint = require('gulp-eslint');
var webpack = require('webpack');
var gutil = require('gulp-util');
var WebpackDevServer = require('webpack-dev-server');
var path = require('path');
var fs = require('fs');
var del = require('del');

module.exports = gulp => {
  gulp.task('default', ['watch', 'start']);

  // Lint JS/JSX files
  gulp.task('eslint', () => {
    return gulp
      .src('src/**/*.jsx')
      .pipe(
        eslint({
          parser: 'babel-eslint',
          ecmaFeatures: {
            modules: true,
            jsx: true
          }
        })
      )
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  });

  // We require task: version to be completed first
  gulp.task('build:webpack', ['version'], callback => {
    try {
      var myConfig = Object.create(require(path.resolve('.') + '/webpack.production.config.js'));
    } catch (e) {
      console.error('No webpack.production.config.js available');
      process.exit();
    }

    myConfig.plugins = myConfig.plugins.concat(
      new webpack.DefinePlugin({
        'process.env': {
          // This has effect on the react lib size
          NODE_ENV: JSON.stringify('production')
        }
      }),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin()
    );

    // run webpack
    webpack(myConfig, function(err, stats) {
      if (err) throw new gutil.PluginError('webpack:build', err);
      gutil.log(
        '[webpack:build]',
        stats.toString({
          colors: true
        })
      );
      callback();
    });
  });

  gulp.task('dev:webpack', function(callback) {
    // run webpack
    var webpackConfig = require(path.resolve('.') + '/webpack.config.js');
    var myConfig = Object.create(webpackConfig);

    // Start a webpack-dev-server
    new WebpackDevServer(webpack(myConfig), {
      publicPath: myConfig.output.publicPath,
      hot: true,
      progress: true,
      quiet: false,
      noInfo: false,
      historyApiFallback: true,
      stats: {
        assets: true,
        colors: true,
        version: false,
        hash: true,
        timings: true,
        chunks: false,
        chunkModules: false
      }
    }).listen(8080, 'localhost', function(err) {
      if (err) throw new gutil.PluginError('webpack-dev-server', err);
      gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
    });
  });

  gulp.task('build:move:styles', ['version'], () =>
    gulp
      .src(path.resolve('.') + '/src/app/styles.css')
      .pipe(gulp.dest('public/dist/' + require(path.resolve('.') + '/version.json').v))
  );

  gulp.task('build:css', () =>
    gulp
      .src(__dirname + '/src/components/**/*.scss')
      .pipe(sass({ includePaths: [__dirname + '/src/components/bootstrap'] }))
      .pipe(concat('style.css'))
      .pipe(gulp.dest('public/dist/'))
  );

  gulp.task('monitor', () =>
    nodemon({
      script: 'app.js',
      ext: 'js html'
    })
  );

  // We want to clear out any current packages
  gulp.task('clean:dist', () => {
    console.log('Cleaning dist folder');
    return del(['./public/dist/**/*']);
  });

  // Create a versioning file
  gulp.task('version', ['clean:dist'], cb => {
    var v = { v: 'dev' };
    if (process.env.NODE_ENV == 'production') {
      v.v = Date.now();
    }
    fs.writeFile('./version.json', JSON.stringify(v), cb);
  });

  // Set our build environment
  gulp.task('build:env', () => {
    return (process.env.NODE_ENV = 'production');
  });

  gulp.task('watch', ['version', 'dev:webpack', 'monitor', 'build:move:styles'], () => {
    //gulp.watch('components/**/*.scss', {cwd: 'src'}, ['build:css']);
    //gulp.watch(['components/**/*.js'], {cwd: 'src'}, ['build:js']);
    //gulp.watch('pages/**/*.{js,jsx}', {cwd: 'src'}, ['build:webpack']);
  });

  //gulp.task('build', ['build:css','build:js','build:jsx']);
  gulp.task('build', ['build:env', 'clean:dist', 'version', 'build:webpack', 'build:move:styles']);

  return gulp;
};

module.exports.gulp = module.exports(gulp);
module.exports = gulp;
