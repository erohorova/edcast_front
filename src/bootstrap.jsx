import LDClient from 'ldclient-js';
import { initUserData, errorUserData } from './app/actions/currentUserActions';
import { getOrgInfo } from './app/actions/teamActions';
import * as actionTypes from './app/constants/actionTypes';
import { usersv2 } from 'edc-web-sdk/requests';
import { browserHistory } from 'react-router';
import { getAccessibleCandidates, isDefaultSourceEnabled } from 'edc-web-sdk/requests/search';
import { JWT } from 'edc-web-sdk/requests/csrfToken';
import find from 'lodash/find';

// Define a temporary hash for the user
window.tempHash = new Date().getTime().toString() + document.location.hostname;
/**
 * Our LaunchDarkly anonymous client
 * Useful for toggles on Login pages
 * We create a temporary hash to define an anomymous user
 * and "bootstrap" to localStorage to speed up the "ready" event
 */
let anonClient = {
  key: `anonymous@${document.location.hostname.split('.')[0]}`,
  anonymous: true,
  custom: {
    org: document.location.hostname.split('.')[0]
  }
};
// Make sure ldclient is loaded and ready
window.ldclient = LDClient.initialize(window.process.env['KEY_LAUNCHDARKLY_CLIENT'], anonClient, {
  hash: window.tempHash,
  sendEvents: false,
  fetchGoals: false
});

window.ldclient.waitUntilReady = new Promise(resolve => {
  window.ldclient.on('ready', () => {
    resolve(true);
  });
});

window.redirectToLogIn = () => {
  const auto_sso = window.location.pathname === '/' ? '?auto_sso=true' : '';
  localStorage.removeItem('userLoggedOut');
  browserHistory.push(`/log_in${auto_sso}`);
};

window.redirectToSSO = ssos => {
  let updatedSsos = ssos.filter(item => item.name !== 'email');
  let sso = updatedSsos[0];

  window.location = sso.webAuthenticationUrl;
};

const clearLocalStorage = () => {
  localStorage.removeItem('userLoggedOut');
};

const bootstrap = async () => {
  const dispatches = [];
  const addDispatch = dispatch => dispatches.push(dispatch);
  localStorage.setItem('afterLoginContentURL', '/');
  localStorage.setItem('afterOnboardingURL', '/');
  const toProceedErrorUserData =
    window.location.pathname.indexOf('/@') > -1 &&
    localStorage &&
    !localStorage.getItem('isLoggedIn');
  const userDataFunction = toProceedErrorUserData ? errorUserData : initUserData;
  try {
    await Promise.all([getOrgInfo()(addDispatch), userDataFunction()(addDispatch)]);
  } catch (err) {
    console.error(err);
  }

  let isSmartSearch = !(window.ldclient.variation('smartsearch', 'disabled') === 'disabled');
  isSmartSearch && JWT.token ? getAccessibleCandidates() : null;
  isSmartSearch && JWT.token ? isDefaultSourceEnabled() : null;
  const isLoggedIn = dispatches.some(v => v.type === actionTypes.RECEIVE_INIT_USER_INFO);

  // clears the local storage.
  clearLocalStorage();

  if (
    !isLoggedIn &&
    !/^\/(log_in|forgot_password|create_account|widgets|sign_up|@)/.test(window.location.pathname)
  ) {
    // Force SSO - EP-12361
    let orgInfo = find(dispatches, { type: actionTypes.RECEIVE_ORG_INFO });
    if (orgInfo) {
      let orgInfoData = orgInfo.data;
      let forceSSO = find(orgInfoData.configs, { name: 'force_sso' });
      let forceSSOEnabled = forceSSO && forceSSO.value;
      let ssos = orgInfoData.ssos;
      let userLoggedOut = localStorage.getItem('userLoggedOut') == 'true';
      // Scenario 1: Redirect user to the IdP if only SSO is enabled.
      !userLoggedOut && forceSSOEnabled && ssos.length == 1 && ssos[0].name != 'email'
        ? window.redirectToSSO(ssos)
        : window.redirectToLogIn();
      // Scenario 2: Redirect user to the IdP if SSO & email are enabled.
      !userLoggedOut &&
      forceSSOEnabled &&
      ssos.length == 2 &&
      ssos.map(a => a.name).indexOf('email') > -1
        ? window.redirectToSSO(ssos)
        : window.redirectToLogIn();
    } else {
      window.redirectToLogIn();
    }
  }
  let userInfo = find(dispatches, { type: actionTypes.RECEIVE_INIT_USER_INFO });
  let isOnboardingCompleted =
    (userInfo && userInfo.user) === undefined ? true : userInfo.user.onboardingCompleted;
  localStorage.setItem('isOnboardingCompleted', isOnboardingCompleted);
  let onboardingMicroservice = window.ldclient.variation('onboarding-version', 'v1') !== 'v1';

  // If user onboarding isn't completed, we redirect here
  if (!isOnboardingCompleted && !/^\/(onboard|@).*/.test(location.pathname)) {
    onboardingMicroservice
      ? (window.location.href = '/onboard/v2')
      : browserHistory.push('/onboarding');
  }

  return { dispatches, isLoggedIn, isOnboardingCompleted };
};

// TODO: cache onboarding status in localstorage for specific user?
window.bootstrapOnboarding = async () => {
  let willRedirectToOnboarding = false;
  const { dispatches, isLoggedIn, isOnboardingCompleted } = await bootstrap();
  if (!isLoggedIn)
    return {
      dispatches,
      isLoggedIn
    };
  if (isOnboardingCompleted) {
    return {
      dispatches,
      isLoggedIn,
      willRedirectToOnboarding,
      isOnboardingCompleted
    };
  }

  // Code never reaches here, will leave fur future reference
  const onboardingState = await usersv2.getOnboardingState();
  dispatches.push({
    type: actionTypes.RECEIVE_INIT_ONBOARDING_STATE,
    onboardingState
  });

  return {
    dispatches,
    isLoggedIn,
    willRedirectToOnboarding,
    isOnboardingCompleted
  };
};

// Google Tag Manager
(function(w, d, s, l, i) {
  w[l] = w[l] || [];
  w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
  var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s),
    dl = l != 'dataLayer' ? '&l=' + l : '';
  j.async = true;
  j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
  f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-N899T5');
// End Google Tag Manager
