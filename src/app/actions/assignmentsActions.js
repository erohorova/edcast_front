import * as actionTypes from '../constants/actionTypes';
import * as requests from 'edc-web-sdk/requests/assignments.v2';
import * as learningQueue from 'edc-web-sdk/requests/feed';

export function getLearningQueue(limit, offset, filterType) {
  return dispatch => {
    let payload = {
      include_queueable_details: true,
      limit,
      offset,
      'source[]': 'bookmarks'
    };
    return learningQueue.getLearningQueueItems(payload).then(response => {
      return response.learningQueueItems.map(item => {
        item.queueable.bookmarkedAt = item.updatedAt;
        return item.queueable || [];
      });
    });
  };
}

export function markAssignmentCountToBeUpdated() {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_ASSIGNMENT_COUNT
    });
  };
}

export function markAssignmentCountHasBeenUpdated() {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_ASSIGNMENT_COUNT_DONE
    });
  };
}

export function fetchAssignments(limit, offset, cardType, section, currentUserId) {
  return dispatch => {
    return requests.getAssignmentsV2(limit, offset, cardType).then(data => {
      dispatch({
        type: actionTypes.RECEIVE_ASSIGNEMENTS,
        data,
        section,
        offset,
        currentUserId
      });
    });
  };
}

export function getAssignmentsMetricUsers(query) {
  return dispatch => {
    return requests.getAssignmentsV2UsersByMetrics(query).then(data => {
      dispatch({
        type: actionTypes.GET_ASSIGNED_USERS_BY_METRICS,
        metricData: data
      });
      return data;
    });
  };
}
