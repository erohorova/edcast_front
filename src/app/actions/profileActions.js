import * as actionTypes from '../constants/actionTypes';
import { getUserActivity, getTopicScore } from 'edc-web-sdk/requests/profile.v2';
import { users } from 'edc-web-sdk/requests';
import findIndex from 'lodash/findIndex';
import { fetchUserBadges } from 'edc-web-sdk/requests/badges';
import { getMaps } from 'edc-web-sdk/requests/skillsGraph';
import { getUserContent } from 'edc-web-sdk/requests/cards.v2';

export function _getUserActivity(payload) {
  return dispatch => {
    return getUserActivity(payload).then(response => {
      const userActivities = response.activityStreams;
      dispatch({
        type: actionTypes.RECEIVE_USER_ACTIVITIES,
        userActivities
      });
    });
  };
}

export function reveiveInitProfile(profile) {
  return dispatch => {
    if (profile) {
      dispatch({
        type: actionTypes.RECEIVE_INIT_PROFILE,
        profile
      });
    } else {
      dispatch({
        type: actionTypes.RECEIVE_INIT_PROFILE,
        profile
      });
    }
  };
}

export function reveiveBadges(badges) {
  return dispatch => {
    if (badges) {
      dispatch({
        type: actionTypes.RECEIVE_PROFILE_BADGES,
        badges
      });
    }
  };
}

export function receiveCourses(externalCourses) {
  return dispatch => {
    dispatch({
      type: actionTypes.RECEIVE_PROFILE_COURSES,
      externalCourses
    });
  };
}

export function getIntegration(id) {
  return dispatch => {
    return users.getIntegrationList(id).then(list => {
      dispatch({
        type: actionTypes.RECEIVE_INTEGRATION,
        list: list.integrations || []
      });
    });
  };
}

export function integrationConnectStatus(integration) {
  return dispatch => {
    dispatch({
      type: actionTypes.INTEGRATION_CONNECT_STATUS,
      integration
    });

    dispatch({
      type: actionTypes.OPEN_SNACKBAR,
      message: 'Your courses will be imported shortly',
      autoClose: true
    });
  };
}

export function getUserTopicScore(myScoreTaxonomyTopics, payload) {
  return dispatch => {
    return getTopicScore(payload).then(data => {
      let totalTopicScore = 0;
      let totalTopicAverage = 0;
      let topics = myScoreTaxonomyTopics ? data.taxonomyTopics : data.topics;
      topics.map(function(topic) {
        totalTopicScore += topic.score;
      });

      totalTopicAverage = totalTopicScore / topics.length;

      dispatch({
        type: actionTypes.SET_TOPIC_SCORE,
        totalScore: data.smartbitesScore,
        topics: topics,
        percentile: data.percentile,
        totalTopicAverage: totalTopicAverage
      });
    });
  };
}

export function publishToPublicProfile(widget, dashboardInfo, userID) {
  return dispatch => {
    const index = findIndex(dashboardInfo, ['name', widget]);
    let newDashboardInfo = dashboardInfo;
    newDashboardInfo[index].visible = !dashboardInfo[index].visible;

    return users.postDashboardInfo(newDashboardInfo, userID).then(() => {
      dispatch({
        type: actionTypes.UPDATE_PUBLIC_PROFILE_INFO,
        dashboardInfo: newDashboardInfo
      });
    });
  };
}

export function reorderingLearningAnalytics(dashboardInfo, userID) {
  return dispatch => {
    return users.postDashboardInfo(dashboardInfo, userID).then(() => {
      dispatch({
        type: actionTypes.UPDATE_PUBLIC_PROFILE_INFO,
        dashboardInfo
      });
    });
  };
}

export function getCombinedUserBadges(id, payload, pathwayBadgings = true, clcBadgings = true) {
  return dispatch => {
    let badgeArray = [];
    let badgesAPI = [];

    if (pathwayBadgings) {
      let CardBadge = getUserBadges(id, { ...{ type: 'CardBadge' }, ...payload });
      badgesAPI.push(CardBadge);
    }

    if (clcBadgings) {
      let ClcBadge = getUserBadges(id, { ...{ type: 'ClcBadge' }, ...payload });
      badgesAPI.push(ClcBadge);
    }

    Promise.all(badgesAPI)
      .then(data => {
        data.map((badge, i) => {
          badgeArray.push(badge.userBadges);
        });
        dispatch({
          type: actionTypes.RECEIVE_PROFILE_BADGES,
          userCardBadges: pathwayBadgings ? badgeArray[0] : [],
          userClcBadges: clcBadgings ? badgeArray[badgeArray.length > 1 ? 1 : 0] : []
        });
      })
      .catch(err => {
        console.error('Error in getCombinedUserBadges');
      });
  };
}

function getUserBadges(id, payload) {
  return new Promise(resolve => {
    fetchUserBadges(id, payload)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        console.error(`Error in profileActions.fetchUserBadges.func : ${err}`);
      });
  });
}

export function getActiveTopics(payload) {
  return dispatch => {
    return getMaps(payload)
      .then(response => {
        dispatch({
          type: actionTypes.SET_ACTIVE_TOPICS,
          activeTopics: Object.keys(response.topics)
        });
      })
      .catch(error => {
        console.error(`Error in profileActions.getActiveTopics.getMaps.func : ${error}`);
      });
  };
}
export function _getUserContent(payload) {
  return dispatch => {
    return getUserContent(payload).then(response => {
      const userTopContent =
        response.cards &&
        response.cards.sort((a, b) => parseInt(b.viewsCount) - parseInt(a.viewsCount));
      dispatch({
        type: actionTypes.RECEIVE_USER_CONTENT,
        userTopContent
      });
    });
  };
}
