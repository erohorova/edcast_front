import * as actionTypes from '../constants/actionTypes';
import { cards } from 'edc-web-sdk/requests/index';

export function submitRatings(cardId, payload) {
  return cards.rateCard(cardId, payload);
}

export function updateRelevancyRatingQ(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_RELEVANCY_RATING_QUEUE,
      card
    });
  };
}

export function flushRelevancyRatingQ(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.FLUSH_RELEVANCY_RATING_QUEUE,
      card
    });
  };
}

export function updateRatedQueueAfterRating(ratedQueue) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_RATED_QUEUE_AFTER_RATING,
      ratedQueue
    });
  };
}

export function checkRatedCard(card, cardsArray) {
  if (cardsArray) {
    for (var i = 0; i < cardsArray.length; i++) {
      if (cardsArray[i].id == card.id) {
        return true;
      }
    }
  } else {
    return false;
  }
}
