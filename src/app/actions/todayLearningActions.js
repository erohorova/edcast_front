import * as actionTypes from '../constants/actionTypes';
import { users } from 'edc-web-sdk/requests/index';

export function fetchUserLearningInterest(id) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_USER_INTEREST
    });
    return users.getLearningTopics(id).then(interests => {
      dispatch({
        type: actionTypes.RECEIVE_USER_INTEREST,
        interests: interests
      });
    });
  };
}
