import * as actionTypes from '../constants/actionTypes';
import { setToken } from 'edc-web-sdk/requests/csrfToken';
import { getDetails } from 'edc-web-sdk/requests/orgSettings';
import { getSpecificUserInfo } from './currentUserActions';
import { users, usersv2, topics, channels } from 'edc-web-sdk/requests/index';

export function initOnboardingState() {
  return async dispatch => {
    usersv2
      .getOnboardingState()
      .then(onboardingState => {
        dispatch({
          type: actionTypes.RECEIVE_INIT_ONBOARDING_STATE,
          onboardingState
        });
        if (onboardingState.onboarding_completed) {
          location.assign('/');
        }
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getOnboardingState.func: ${err}`);
      });

    let user = await getSpecificUserInfo(['profile']);
    dispatch({
      type: actionTypes.RECEIVE_USER_PROFILE,
      user
    });
  };
}

export function initOnboardingStateSettings() {
  return async dispatch => {
    usersv2
      .getOnboardingState()
      .then(onboardingState => {
        dispatch({
          type: actionTypes.RECEIVE_INIT_ONBOARDING_STATE,
          onboardingState
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getOnboardingState.func: ${err}`);
      });
  };
}

export function newSuggestedTopics(page = 1) {
  return dispatch => {
    topics
      .getSuggestedTopicsForUserOnboarding(page)
      .then(topicsParam => {
        dispatch({
          type: actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER_ONBOARDING,
          topics: topicsParam,
          section: 'learningTopics'
        });
      })
      .catch(err => {
        console.error(
          `Error in onboardingActions.getSuggestedTopicsForUserOnboarding.func: ${err}`
        );
      });
  };
}

export function suggestedTopics() {
  return dispatch => {
    topics
      .getSuggestedTopicsForUser()
      .then(topicsParam => {
        dispatch({
          type: actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER,
          topics: topicsParam
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getSuggestedTopicsForUser.func: ${err}`);
      });
  };
}

export function suggestedSources() {
  return dispatch => {
    topics
      .getSuggestedSourcesForUser()
      .then(sources => {
        dispatch({
          type: actionTypes.RECEIVE_SOURCE_SUGGESTIONS_USER,
          sources
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getSuggestedSourcesForUser.func: ${err}`);
      });
  };
}

export function postUserInfo(user, step, isLastStep) {
  return dispatch => {
    users
      .onboardingUserDetails(user, step)
      .then(response => {
        dispatch({
          type: actionTypes.RECEIVE_UPDATED_USER_INFO,
          user
        });
        users
          .getUserInfo()
          .then(userData => {
            if (userData && userData.csrfToken) {
              setToken(userData.csrfToken);
              dispatch({
                type: actionTypes.RECEIVE_INIT_USER_INFO,
                user: userData
              });
            }
          })
          .catch(err => {
            console.error(`Error in onboardingActions.getUserInfo.func: ${err}`);
          });
        if (isLastStep) {
          if (localStorage.getItem('afterOnboardingURL')) {
            location.assign(localStorage.getItem('afterOnboardingURL'));
          } else {
            location.assign('/');
          }
        }
      })
      .catch(err => {
        console.error(`Error in onboardingActions.postUserInfo.onboardingUserDetails.func: ${err}`);
      });
  };
}

export function postUserPrefs(user, step) {
  return dispatch => {
    return users
      .onboardingUserDetails(user, step)
      .then(response => {
        dispatch({
          type: actionTypes.RECEIVE_UPDATED_USER_PREFS,
          user
        });
      })
      .catch(err => {
        console.error(
          `Error in onboardingActions.postUserPrefs.onboardingUserDetails.func: ${err}`
        );
      });
  };
}

export function getRecommendations() {
  return dispatch => {
    users
      .getInfluencers(4, 0)
      .then(influencers => {
        dispatch({
          type: actionTypes.RECEIVE_RECOMMENDED_INFLUENCERS,
          influencers
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getInfluencers.func: ${err}`);
      });

    channels
      .getChannels(4, 0)
      .then(channelsData => {
        dispatch({
          type: actionTypes.RECEIVE_RECOMMENDED_CHANNELS,
          channels: channelsData
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getChannels.func: ${err}`);
      });
  };
}

export function postOnboardingStep(user, step, isLastStep) {
  return disptach => {
    users
      .onboardingUserDetails(user, step)
      .then(response => {
        if (isLastStep) {
          if (localStorage.getItem('afterOnboardingURL')) {
            location.assign(localStorage.getItem('afterOnboardingURL'));
          } else {
            location.assign('/');
          }
        }
      })
      .catch(err => {
        console.error(`Error in onboardingActions.onboardingUserDetails.func: ${err}`);
      });
  };
}

export function updateOnboardingStep(step) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_ONBOARDING_STEP,
      step
    });
  };
}

export function getOnBoardingSkillsLevels(payload) {
  return dispatch => {
    return topics
      .getSkillsLevels(payload)
      .then(taxonomy => {
        dispatch({
          type: actionTypes.SET_MULTILEVEL_TAXONOMY,
          taxonomy: taxonomy[0] || []
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.getSkillsLevels.func: ${err}`);
      });
  };
}

export function getOrgInfo() {
  return dispatch => {
    getDetails()
      .then(data => {
        dispatch({
          type: actionTypes.RECEIVE_ORG_INFO,
          data
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.ERROR_INIT_ORG,
          error
        });
      });
  };
}

export function fetchTopicSkills(topicId, rowIndex, page = 1, section) {
  return dispatch => {
    topics
      .fetchTopicSkills(topicId, page)
      .then(data => {
        dispatch({
          type: actionTypes.RECEIVE_TOPIC_SKILLS,
          data,
          rowIndex,
          topicId,
          section
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.ERROR_FETCHING_SKILLS,
          error
        });
      });
  };
}

export function postLearningDomainTopics(learningDomainTopics, userId, profileId) {
  return dispatch => {
    users
      .postLearningDomainTopicsNew(learningDomainTopics, userId, profileId)
      .then(async response => {
        let user = await getSpecificUserInfo(['profile']);
        dispatch({
          type: actionTypes.RECEIVE_USER_PROFILE,
          user
        });
      })
      .catch(err => {
        console.error(`Error in onboardingActions.postLearningDomainTopicsNew.func: ${err}`);
      });
  };
}

export function newSuggestedTopicsForExpertise(page = 1) {
  return dispatch => {
    topics
      .getSuggestedTopicsForUserOnboarding(page)
      .then(expertiseTopics => {
        dispatch({
          type: actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER_ONBOARDING,
          expertiseTopics,
          section: 'expertiseSection'
        });
      })
      .catch(err => {
        console.error(
          `Error in onboardingActions.getSuggestedTopicsForUserOnboarding.func: ${err}`
        );
      });
  };
}

export function postExpertiseDomainTopics(expertiseDomainTopics, userId, profileId) {
  return dispatch => {
    users
      .postExpertiseDomainTopicsNew(expertiseDomainTopics, userId, profileId)
      .then(response => {})
      .catch(err => {
        console.error(`Error in onboardingActions.postExpertiseDomainTopicsNew.func: ${err}`);
      });
  };
}
