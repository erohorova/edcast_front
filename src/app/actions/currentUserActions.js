import * as actionTypes from '../constants/actionTypes';
import { usersv2 } from 'edc-web-sdk/requests';
import { users, groups } from 'edc-web-sdk/requests/index';
import { setToken, JWT } from 'edc-web-sdk/requests/csrfToken';
import { getUserNotificationConfig } from 'edc-web-sdk/requests/orgSettings';
import { requestTopic } from 'edc-web-sdk/requests/users.v2';
import { Permissions } from '../utils/checkPermissions';
import { push } from 'react-router-redux';
import _ from 'lodash';

export function requestUserIsGroupLeader() {
  return async dispatch => {
    let state;
    const groupsData = await groups.getList(undefined, 0, 'admin');
    state = groupsData.length > 0;
    dispatch({
      type: actionTypes.RECEIVE_GROUPLEADER_USER_INFO,
      state
    });
    return Promise.resolve();
  };
}

function isLocalStorageSupported() {
  try {
    localStorage.setItem('testKey', '1');
    localStorage.removeItem('testKey');
    return true;
  } catch (error) {
    return false;
  }
}

export function getSpecificUserInfo(params, currentUser = {}) {
  return async dispatch => {
    let diffKeys = params.filter(param => {
      if (!currentUser.hasOwnProperty(param)) {
        return param;
      }
    });

    if (_.isEmpty(diffKeys)) {
      return;
    }
    /* the key of this paramsMapping object will be the keys for the current reducer
     * and the values are the payload parameters keys */
    let paramsMapping = {
      first_name: 'first_name',
      last_name: 'last_name',
      bio: 'bio',
      userSubscriptions: 'user_subscriptions',
      handle: 'handle',
      followingChannels: 'following_channels',
      followersCount: 'followers_count',
      followingCount: 'following_count',
      defaultTeamId: 'default_team_id',
      roles: 'roles',
      rolesDefaultNames: 'roles_default_names',
      writableChannels: 'writable_channels, channels(id,label,is_private,slug)',
      coverImage: 'coverimages',
      hideFromLeaderboard: 'hide_from_leaderboard',
      onboardingCompletedDate: 'onboarding_completed_date',
      dashboardInfo: 'dashboard_info',
      company: 'company'
    };

    let finalParamsMapping = _.pick(paramsMapping, diffKeys);
    let mappedParam = _.map(finalParamsMapping, val => {
      return val;
    });
    let payload = { fields: _.map(mappedParam).join(',') };
    let data;
    let finalReducerValues;

    if (!_.isEmpty(diffKeys)) {
      try {
        data = await users.getUserInfo(payload);

        let userData = {
          first_name: data.firstName,
          last_name: data.lastName,
          userSubscriptions: (data.userSubscriptions && data.userSubscriptions[0]) || null,
          bio: data.bio,
          hideFromLeaderboard: data.hideFromLeaderboard,
          handle: data.handle && data.handle.substr(1),
          followingChannels: data.followingChannels || [],
          followersCount: data.followersCount,
          followingCount: data.followingCount,
          defaultTeamId: data.defaultTeamId,
          roles: data.roles,
          rolesDefaultNames: data.rolesDefaultNames,
          writableChannels: data.writableChannels,
          onboardingCompletedDate: data.onboardingCompletedDate,
          company: data.company,
          coverImage:
            (data.coverimages && data.coverimages.banner_url) ||
            (data.coverimages && data.coverimages.banner)
        };
        finalReducerValues = _.map(finalParamsMapping, (val, key) => {
          return key;
        });
        userData = _.pick(userData, finalReducerValues);

        dispatch({
          type: actionTypes.SET_CURRENT_USER_INFO,
          userData
        });
        if (data.hasOwnProperty('dashboardInfo')) {
          dispatch({
            type: actionTypes.UPDATE_PUBLIC_PROFILE_INFO,
            dashboardInfo: data.dashboardInfo
          });
        }
        return userData;
      } catch (error) {}
    }
  };
}

function isLocationContentURL() {
  return (
    window.location.pathname.indexOf('/pathways/') >= 0 ||
    window.location.pathname.indexOf('/insights/') >= 0 ||
    window.location.pathname.indexOf('/journey/') >= 0 ||
    window.location.pathname.indexOf('/channel/') >= 0 ||
    window.location.pathname.indexOf('/video_streams/') >= 0 ||
    window.location.pathname.indexOf('/teams/') >= 0
  );
}

export function initUserData() {
  isLocalStorageSupported();
  let isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';
  let url = window.location.pathname;
  url = window.location.search ? url + window.location.search : url;

  if (isLocalStorageSupported() && isLocationContentURL()) {
    localStorage.setItem('afterLoginContentURL', url);
    localStorage.setItem('afterOnboardingURL', url);
  }

  return async dispatch => {
    try {
      let user = await users.getUserInfo();
      if (!user || !user.csrfToken) return;
      setToken(user.csrfToken);
      if (
        (localStorage.getItem('webSessionTimeout') &&
          localStorage.getItem('webSessionTimeout') > Date.now()) ||
        !localStorage.getItem('webSessionTimeout')
      ) {
        JWT.token = user.jwtToken;
        let webSessionTimeout =
          user.organization &&
          user.organization.configs &&
          user.organization['web_session_timeout'];
        webSessionTimeout = webSessionTimeout ? webSessionTimeout * 60 * 1000 + Date.now() : 0;
        localStorage.setItem('webSessionTimeout', webSessionTimeout);
      } else if (localStorage.getItem('webSessionTimeout') === '0') {
        JWT.token = user.jwtToken;
      } else if (localStorage.getItem('webSessionTimeout')) {
        localStorage.removeItem('webSessionTimeout');
      }

      Permissions.enabled = user.permissions;

      try {
        let customFieldsData = await usersv2.getUserCustomFields({
          send_array: true,
          'user_ids[]': user.id
        });
        user.customFields = customFieldsData[0].customFields;
      } catch (e) {
        console.error('Unable to get users custom fields in currentUserActions.initUserData', e);
      }

      dispatch({
        type: actionTypes.RECEIVE_INIT_USER_INFO,
        user
      });
    } catch (error) {
      console.error('init user', error.stack);
      dispatch({
        type: actionTypes.ERROR_INIT_USER,
        error
      });
    }
  };
}

export function errorUserData() {
  return dispatch => {
    dispatch({
      type: actionTypes.ERROR_INIT_USER_PUBLIC
    });
  };
}

export function getWalletBalance() {
  return async dispatch => {
    const walletbalance = await usersv2.getUserBalance();
    dispatch({
      type: actionTypes.RECEIVE_WALLET_BALANCE,
      walletbalance
    });
  };
}

export function requestLogin(email, password) {
  return dispatch => {
    return users
      .requestLogin(email, password)
      .then(user => {
        setToken(user.csrfToken);
        JWT.token = user.jwtToken;
        Permissions.enabled = user.permissions;
        // groups.getList(undefined, 0, 'admin').then((groupsData) => {
        //   user.isGroupLeader = groupsData.length > 0;
        //   dispatch({
        //     type: actionTypes.RECEIVE_USER_AUTHENTICATED,
        //     user
        //   });
        // }).catch((err) => {
        //     console.error(`Error in currentUserActions.getList.func: ${err}`);
        // });
        if (window.location.pathname === '/log_in') {
          dispatch(push('/'));
        }
        return user;
      })
      .catch(error => {
        let errorMsg = JSON.parse(error.response.text).error;
        dispatch({
          type: actionTypes.ERROR_USER_LOGIN,
          error,
          errorMsg
        });
      });
  };
}

export function getFollowingUsers(userList) {
  return dispatch => {
    dispatch({
      type: actionTypes.GET_FOLLOWING_USERS,
      users: userList
    });
  };
}

export function setUserBadges(badges, badgeType) {
  return dispatch => {
    dispatch({
      type: actionTypes.SET_USER_BADGES,
      badges,
      badgeType
    });
  };
}

export function followingChannelAction(channel, follow) {
  return dispatch => {
    dispatch({
      type: actionTypes.FOLLOWING_CHANNEL_STATE,
      channel,
      follow
    });
  };
}

export function followingUserAction(userId, state) {
  return dispatch => {
    dispatch({
      type: actionTypes.FOLLOWING_USER_STATE,
      userId,
      state
    });
  };
}

export function updateFollowingUsersCount(state) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_FOLLOWING_USERS_COUNT,
      state
    });
  };
}

export function updateFollowingList(userId, state) {
  return dispatch => {
    dispatch({
      type: actionTypes.FOLLOWER_USER_STATE,
      userId,
      state
    });
  };
}

export function getFollowerUsers(userList) {
  return dispatch => {
    dispatch({
      type: actionTypes.GET_FOLLOWER_USERS,
      users: userList
    });
  };
}

export function changeEmailInput() {
  return dispatch => {
    dispatch({
      type: actionTypes.CHANGE_EMAIL_INPUT
    });
  };
}

export function userLogOut() {
  return dispatch => {
    dispatch({
      type: actionTypes.USER_LOG_OUT
    });
  };
}

export function changePasswordInput() {
  return dispatch => {
    dispatch({
      type: actionTypes.CHANGE_PASSWORD_INPUT
    });
  };
}

export function updateUserInfo(currentUser, newUser) {
  return dispatch => {
    return users
      .updateUser(currentUser.id, newUser)
      .then(data => {
        let user = _.merge(currentUser, data);
        if (data.avatarimages && data.avatarimages.medium) {
          user.picture = data.avatarimages.medium;
        }
        if (data.coverimages && data.coverimages.medium) {
          user.coverimages = data.coverimages;
        } else {
          user.coverimages.banner = currentUser.coverImage;
        }
        dispatch({
          type: actionTypes.UPDATE_USER_DETAILS,
          user
        });
        dispatch({
          type: actionTypes.UPDATE_USER_EXPERTISE_AND_INTEREST,
          profile: user.profile
        });
        if (newUser.password) {
          return users
            .updateUserPassword(currentUser.id, { user: newUser.password })
            .then(() => {
              return user;
            })
            .catch(error => {
              dispatch({
                type: actionTypes.ERROR_UPDATED_USER_INFO,
                error
              });
              return error;
            });
        } else {
          return user;
        }
      })
      .catch(error => {
        dispatch({
          type: actionTypes.ERROR_UPDATED_USER_INFO,
          error
        });
        return error;
      });
  };
}

export function newRequestLogin(email, password, termsAccepted) {
  return dispatch => {
    return users
      .newRequestLogin(email, password, termsAccepted)
      .then(user => {
        setToken(user.csrfToken);
        JWT.token = user.jwtToken;
        Permissions.enabled = user.permissions;
        // groups.getList(undefined, 0, 'admin').then((groupsData) => {
        //   user.isGroupLeader = groupsData.length > 0;
        //   dispatch({
        //     type: actionTypes.RECEIVE_USER_AUTHENTICATED,
        //     user
        //   });
        // }).catch((err) => {
        //     console.error(`Error in currentUserActions.newRequestLogin.getList.func: ${err}`);
        // });
        return user;
      })
      .catch(error => {
        return error;
      });
  };
}

export function getNotificationConfig() {
  return dispatch => {
    return getUserNotificationConfig().then(response => {
      dispatch({
        type: actionTypes.FETCH_NOTIFICATION_CONFIG,
        notificationConfig: response,
        showNotificationTab: renderNotificationConfig(response.user_config, response.org_config)
      });
      return response;
    });
  };
}

export function renderNotificationConfig(triggerObj, orgConfig) {
  let showTab = false;
  triggerObj.groups.map((group, index) => {
    group.notifications &&
      group.notifications.map((notification, index3) => {
        triggerObj.mediums &&
          triggerObj.mediums.map((medium, index4) => {
            orgConfig.options[notification.id].user_configurable &&
              orgConfig.options[notification.id][medium.id].filter(function(obj) {
                if (obj.label.toLowerCase() !== 'off' && obj.selected) {
                  showTab = true;
                  return showTab;
                }
              });
          });
      });
  });
  return showTab;
}

export function getCustomTopics(payload) {
  return dispatch => {
    requestTopic(payload)
      .then(customTopics => {
        return dispatch({
          type: actionTypes.GET_CUSTOM_TOPICS,
          customTopics
        });
      })
      .catch(err => console.error(err));
  };
}
