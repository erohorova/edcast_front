import { fetchCard } from 'edc-web-sdk/requests/cards';
import { fetchGroups } from 'edc-web-sdk/requests/groups.v2';
import { searchUsers } from 'edc-web-sdk/requests/users.v2';
import * as actionTypes from '../constants/actionTypes';

export function _fetchCard(payload) {
  return dispatch => {
    return fetchCard(payload).then(card => {
      dispatch({
        type: actionTypes.FETCH_CONTENT_TO_SHARE,
        card
      });
    });
  };
}

export function _fetchUsers(payload) {
  return dispatch => {
    return searchUsers(payload).then(users => {
      dispatch({
        type: actionTypes.FETCH_USERS_FOR_SHARE,
        users
      });
    });
  };
}

export function _fetchGroups(payload) {
  return dispatch => {
    return fetchGroups(payload).then(groups => {
      dispatch({
        type: actionTypes.FETCH_MY_TEAM,
        groups
      });
    });
  };
}

export function _addUser(user, card, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch ? actionTypes.ADD_SEARCHED_USER_FOR_SHARE : actionTypes.ADD_USER_FOR_SHARE,
      user,
      card
    });
  };
}

export function _addGroup(group, card, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch ? actionTypes.ADD_SEARCHED_GROUP_FOR_SHARE : actionTypes.ADD_GROUP_FOR_SHARE,
      group,
      card
    });
  };
}

export function _removeUser(user, card, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch
        ? actionTypes.REMOVE_SEARCHED_USER_FROM_SHARE
        : actionTypes.REMOVE_USER_FROM_SHARE,
      user,
      card
    });
  };
}

export function _removeGroup(group, card, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch
        ? actionTypes.REMOVE_SEARCHED_GROUP_FROM_SHARE
        : actionTypes.REMOVE_GROUP_FROM_SHARE,
      group,
      card
    });
  };
}

export function _clearState(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAR_SHARE_CONTENT_STATE,
      card
    });
  };
}
