import * as actionTypes from '../constants/actionTypes';
import { getList } from 'edc-web-sdk/requests/groups.v2';

export function getGroups(limit, offset) {
  return dispatch => {
    return getList(limit, offset).then(groups => {
      dispatch({
        type: actionTypes.RECEIVE_GROUPS,
        groups
      });
      return groups.map(group => group.id + '');
    });
  };
}
