import * as actionTypes from '../constants/actionTypes';
import * as learningQueue from 'edc-web-sdk/requests/feed';
import { getList } from 'edc-web-sdk/requests/groups.v2';

export function getStream(limit, streamFlag, layout, offset) {
  if (streamFlag) {
    if (document.querySelector('#right-rail-activity') != null) {
      let offsetVar =
        document.querySelector('#right-rail-activity .wide-item-list') &&
        document.querySelector('#right-rail-activity .wide-item-list').childNodes.length;
      if (
        document.querySelector('#home .referForPagination').clientHeight + 2500 >
        document.getElementById('right-rail-activity').scrollHeight
      ) {
        return fetchStream(limit, offsetVar, streamFlag);
      } else return dispatch => {};
    }
  } else return fetchStream(limit, offset, streamFlag);
}

export function setActivityListHeight(homepageVersion, layout) {
  let newTeamActivity = window.ldclient.variation('team-activity-v-2', false);
  if (homepageVersion == 'right-sidebar' && !newTeamActivity) {
    let timeDuration = 0;
    if (layout == 'card') timeDuration = 400;
    else timeDuration = 2000;
    setTimeout(function(event) {
      let activityStreamHeight,
        difference = 0;
      if (document.querySelector('.vertical-spacing-large button.viewMore') != null) {
        difference = 55;
      }
      activityStreamHeight =
        document.querySelector('#home .referForPagination').clientHeight - difference;
      if (document.querySelector('#right-rail-activity') != null) {
        activityStreamHeight = activityStreamHeight < 556 ? 556 : activityStreamHeight;
        activityStreamHeight = activityStreamHeight + 'px';
        document.getElementById('right-rail-activity').style.maxHeight = activityStreamHeight;
        document.getElementById('right-rail-activity').style.height = activityStreamHeight;
      }
    }, timeDuration);
  }
}

function fetchStream(limit, offset, streamFlag) {
  return dispatch => {
    let payload = { limit, offset };
    return (
      learningQueue
        .getActivityStream(payload)
        .then(response => {
          dispatch({
            type: actionTypes.GET_ACTIVITY_STREAM,
            response,
            streamFlag
          });
        })
        /*eslint handle-callback-err: "off"*/
        .catch(function(err) {})
    );
  };
}

export function recieveActivityFeed(activityFlag) {
  return {
    type: actionTypes.RECEIVE_NEW_ACTIVITY,
    activityFlag
  };
}

export function activityGetTeams(limit, offset) {
  return dispatch => {
    getList(limit, offset)
      .then(data => {
        dispatch({
          type: actionTypes.GET_ACTIVITY_TEAMS,
          data
        });
      })
      .catch(err => {
        console.error(`Error in teamActions.activityGetTeams.func : ${err}`);
      });
  };
}
