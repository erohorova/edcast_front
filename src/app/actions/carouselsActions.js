import * as actionTypes from '../constants/actionTypes';
import * as sdk from 'edc-web-sdk/requests/carousels';

export function getCarousels(payload) {
  return dispatch => {
    return sdk.getCarousels(payload).then(carousels => {
      dispatch({
        type: actionTypes.RECEIVE_CAROUSELS,
        carousels
      });
      return carousels;
    });
  };
}

export function getCarouselItems(payload) {
  payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
  return dispatch => {
    return sdk.getCarouselItems(payload.structure_id, payload).then(items => {
      dispatch({
        type: actionTypes.RECEIVE_CAROUSEL_ITEMS,
        items
      });
      return items;
    });
  };
}
