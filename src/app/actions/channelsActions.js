import * as actionTypes from '../constants/actionTypes';
import { cards, channels, channelsv2 } from 'edc-web-sdk/requests';
import * as sdk from 'edc-web-sdk/requests/channels';

export function toggleFollow(id, isFollow) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_TOGGLE_CHANNEL_FOLLOW,
      id
    });
    if (isFollow) {
      return channelsv2.follow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW,
          id,
          isFollow
        });
      });
    } else {
      return channelsv2.unfollow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW,
          id,
          isFollow
        });
      });
    }
  };
}

export function getChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS,
        channels: channelsData
      });
      return channelsData.map(channelItem => channelItem.id + '');
    });
  };
}

export function fetchChannelsAction(payload) {
  return dispatch => {
    return channelsv2
      .fetchChannels(payload)
      .then(channelsData => {
        dispatch({
          type: actionTypes.RECEIVE_CHANNELS,
          channels: channelsData
        });
        return channelsData.map(channelItem => channelItem.id + '');
      })
      .catch(() => {
        console.error('Error in fetchChannelsAction');
      });
  };
}

export function getFollowingChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getFollowingChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS,
        channels: channelsData
      });
      return channelsData.map(channelItem => channelItem.id + '');
    });
  };
}

export function getSuggestedChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getSuggestedChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS,
        channels: channelsData
      });
      return channelsData.map(channelItem => channelItem.id + '');
    });
  };
}

export function searchChannels(query) {
  return dispatch => {
    return channelsv2.searchChannels(query).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS,
        channels: channelsData
      });
      return channelsData.map(channelItem => channelItem.id + '');
    });
  };
}

export function getSingleChannel(slug, limit, offset) {
  return dispatch => {
    return sdk.getChannel(slug, limit, offset).then(channelData => {
      let channelsData = [channelData];
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS,
        channels: channelsData
      });
      return channelData;
    });
  };
}

export function getChannelCards(slug, limit, offset) {
  return dispatch => {
    return sdk.getChannel(slug, limit, offset).then(data => {
      let cardIds = data.cards.reduce((acc, curr) => {
        acc.push(curr.id);
        return acc;
      }, []);
      return { cardIds, cards: data.cards };
    });
  };
}

export function updateChannels(channelsData) {
  return {
    type: actionTypes.RECEIVE_CHANNELS,
    channels: channelsData
  };
}

export function createChannel(channel) {
  return dispatch => {
    return sdk
      .createChannel(channel)
      .then(createdChannel => {
        createdChannel.isNewlyCreated = true;
        dispatch({ type: actionTypes.CREATE_CHANNEL, createdChannel });
        return createdChannel;
      })
      .catch(err => {
        return { message: err.toString(), header: 'Create channel error' };
      });
  };
}

export function saveChannelChanges(payload) {
  return dispatch => {
    return sdk
      .editChannel(payload)
      .then(channel => {
        dispatch({ type: actionTypes.UPDATE_CHANNEL, channel });
        return channel;
      })
      .catch(err => {
        return { message: err.toString(), header: 'Create channel error' };
      });
  };
}
