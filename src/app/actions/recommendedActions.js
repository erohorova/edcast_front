import { recommended } from 'edc-web-sdk/requests/index';

export function getRecommendedItems(payload) {
  return new Promise((resolve, reject) => {
    //will use different card action, when server issue done
    return recommended
      .getRecommendedItems(payload)
      .then(result => {
        return resolve(result);
      })
      .catch(err => {
        return reject(err);
      });
  });
}
