import uniqBy from 'lodash/uniqBy';

import * as actionTypes from '../constants/actionTypes';
import { cards, users } from 'edc-web-sdk/requests/index';
import {
  getUserContent,
  getCardsFromCardSubtype as fetchCardsFromSubtype
} from 'edc-web-sdk/requests/cards.v2';
import { fetchVideoStream } from 'edc-web-sdk/requests/liveStream';
import * as learningQueue from 'edc-web-sdk/requests/feed';
import * as ecl from 'edc-web-sdk/requests/ecl';
import { getAssignments } from 'edc-web-sdk/requests/assignments';

export function _getAssignments(
  limit,
  offset,
  state,
  filter_by_language = null,
  last_access_at = null
) {
  const fields =
    'id,state,completed_at,due_at,start_date,started_at,card(id,completed_percentage,card_subtype,all_ratings,author,average_rating,card_metadatum,comments_count,completion_state,filestack,hidden,is_assigned,is_bookmarked,is_clone,is_official,is_paid,is_public,is_upvoted,mark_feature_disabled_for_source,prices,provider,provider_image,share_url,resource,state,votes_count,card_type,slug,badging,skill_level,title,message,ecl_duration_metadata,readable_card_type,is_new,quiz_question_options,quiz,video_stream,published_at,ecl_metadata,is_reported,teams_permitted,users_permitted),assignor,assignable,created_at';

  return dispatch => {
    return getAssignments(limit, offset, state, filter_by_language, last_access_at, fields)
      .then(arr_cards => {
        const dismissedAssignments = arr_cards.assignments;
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: dismissedAssignments.map(card => card.assignable)
        });
        return dismissedAssignments.map(card => card.assignable.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions._getAssignments.func: ${err}`);
      });
  };
}

export function toggleLikeCardAsync(cardId, cardType, isLike) {
  return new Promise((resolve, reject) => {
    if (isLike) {
      cards
        .likeCard(cardId, cardType)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in cardsActions.likeCard.func: ${err}`);
        });
    } else {
      cards
        .dislikeCard(cardId, cardType)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in cardsActions.dislikeCard.func: ${err}`);
        });
    }
  });
}

export function setRateCardAsync(cardId, cardType, rating, dispatch) {
  return new Promise((resolve, reject) => {
    cards
      .rateCard(cardId, { rating: rating })
      .then(result => {
        resolve(result);
      })
      .catch(err => {
        console.error(`Error in cardsActions.rateCard.func: ${err}`);
      });
  });
}

export function toggleBookmarkCardAsync(cardId, cardType, isBookmark) {
  return new Promise((resolve, reject) => {
    if (isBookmark) {
      cards
        .bookmark(cardId)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in cardsActions.bookmark.func: ${err}`);
        });
    } else {
      cards
        .unBookmark(cardId)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in cardsActions.unBookmark.func: ${err}`);
        });
    }
  });
}

export function changeCardState(cardId) {
  return dispatch => {
    dispatch({
      type: actionTypes.CHANGE_CARD_STATE,
      cardId
    });
  };
}

export function toggleBookmarkCard(cardId, isBookmark) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_BOOKMARK_CARD,
      cardId,
      isBookmark
    });
    if (isBookmark) {
      cards
        .bookmark(cardId)
        .then(() => {
          dispatch({
            type: actionTypes.RECEIVE_BOOKMARK_CARD,
            cardId,
            isBookmark
          });
        })
        .catch(err => {
          console.error(`Error in cardsActions.toggleBookmarkCard.bookmark.func: ${err}`);
        });
    } else {
      cards
        .unBookmark(cardId)
        .then(() => {
          dispatch({
            type: actionTypes.RECEIVE_BOOKMARK_CARD,
            cardId,
            isBookmark
          });
        })
        .catch(err => {
          console.error(`Error in cardsActions.toggleBookmarkCard.unBookmark.func: ${err}`);
        });
    }
  };
}

export function dismissCard(cardId, cardType) {
  return dispatch => {
    cards
      .dismiss(cardId, cardType)
      .then(() => {
        dispatch({
          type: actionTypes.RECEIVE_DISMISS_CARD,
          cardId
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'SmartCard Insight dismissed, It will never show again in your feed.',
          autoClose: true
        });
      })
      .catch(err => {
        console.error(`Error in cardsActions.dismiss.func: ${err}`);
      });
  };
}

export function deleteCard(cardId, cardType) {
  return dispatch => {
    return cards
      .deleteCard(cardId, cardType)
      .then(() => {
        dispatch({
          type: actionTypes.RECEIVE_DELETE_CARD,
          cardId
        });
        return cardId;
      })
      .catch(err => {
        console.error(`Error in cardsActions.deleteCard.func: ${err}`);
      });
  };
}

//actions for comments
export function postComment(cardId, message, limit, cardType) {
  return cards
    .postComment(cardId, message, cardType)
    .then(data => {
      if (data && data.embargoErr) {
        return data;
      } else {
        let cardIdParam = data.commentableId + '';
        return cards
          .getComments(cardIdParam, limit, cardType)
          .then(comments => {
            return comments;
          })
          .catch(err => {
            console.error(`Error in cardsActions.getComments.func: ${err}`);
          });
      }
    })
    .catch(err => {
      console.error(`Error in cardsActions.postComment.func: ${err}`);
    });
}

export function loadComments(cardId, limit, cardType, videoStreamId = null) {
  let id = videoStreamId ? videoStreamId : cardId;
  return cards
    .getComments(cardId, limit, cardType)
    .then(comments => {
      return comments;
    })
    .catch(() => {
      return [];
    });
}

export function startAssignment(cardId) {
  return new Promise((resolve, reject) => {
    return cards
      .startAssignment(cardId)
      .then(userContentCompletion => {
        return resolve(userContentCompletion);
      })
      .catch(() => {
        return reject();
      });
  });
}

export function toggleLikeComment(cardId, commentId, isLike) {
  if (isLike) {
    return cards
      .likeComment(commentId)
      .then(() => {
        return { upvoted: true };
      })
      .catch(err => {
        console.error(`Error in cardsActions.likeComment.func: ${err}`);
      });
  } else {
    return cards
      .dislikeComment(commentId)
      .then(() => {
        return { upvoted: false };
      })
      .catch(err => {
        console.error(`Error in cardsActions.dislikeComment.func: ${err}`);
      });
  }
}

export function deleteComment(cardId, commentId) {
  return dispatch => {
    dispatch({
      type: actionTypes.OPEN_SNACKBAR,
      message: 'Deleting comment.',
      autoClose: true
    });
    cards
      .deleteComment(cardId, commentId)
      .then(() => {
        dispatch({
          type: actionTypes.DELETE_CARD_COMMENT,
          cardId,
          commentId
        });
        dispatch({
          type: actionTypes.CLOSE_SNACKBAR
        });
      })
      .catch(err => {
        console.error(`Error in cardsActions.deleteComment.func: ${err}`);
      });
  };
}

export function updateCards(cardsData) {
  return {
    type: actionTypes.RECEIVE_CARDS,
    cards: cardsData
  };
}

export function updateCurrentCard(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.RECEIVE_CARD,
      card
    });
  };
}

export function clearCurrentCard() {
  return dispatch => {
    dispatch({
      type: actionTypes.RECEIVE_CLEAR_CARD
    });
  };
}

export function getVideoStream(slug) {
  return dispatch => {
    return fetchVideoStream(slug)
      .then(videoStream => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: [videoStream]
        });
        return videoStream.id;
      })
      .catch(err => {
        console.error(`Error in cardsActions.fetchVideoStream.func: ${err}`);
      });
  };
}

export function getCard(slug) {
  return dispatch => {
    return cards
      .fetchCard(slug)
      .then(card => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: [card]
        });
        return card.id;
      })
      .catch(err => {
        console.error(`Error in cardsActions.fetchCard.func: ${err}`);
      });
  };
}

export function editCard(cardId, message, resource, channelIds, pollOptions, file_ids, topics) {
  return dispatch => {
    return cards
      .update(cardId, message, resource, channelIds, pollOptions, file_ids, topics)
      .then(
        card => {
          dispatch({
            type: actionTypes.RECEIVE_CARDS,
            cards: [card]
          });
        },
        error => {
          dispatch({
            type: actionTypes.OPEN_SNACKBAR,
            message: error,
            autoClose: true
          });
        }
      )
      .catch(err => {
        console.error(`Error in cardsActions.editCard.update.func: ${err}`);
      });
  };
}

export function getUserCards(
  userId,
  limit,
  offset,
  query,
  filter,
  cardState,
  sort,
  last_access_at = null,
  fields = null
) {
  return dispatch => {
    return cards
      .getUserCards(
        userId,
        limit,
        offset,
        query,
        filter,
        cardState,
        sort,
        null,
        last_access_at,
        fields
      )
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getUserCards.func: ${err}`);
      });
  };
}

export function getCards(
  author_id,
  limit,
  offset,
  cardType,
  only_deleted,
  last_access_at = null,
  fields = null
) {
  let query = { author_id, limit, offset, 'card_type[]': cardType, last_access_at };
  if (only_deleted) {
    query = {
      ...query,
      only_deleted,
      fields:
        'id,filestack,title,message,slug,author,average_rating,card_subtype,card_type,comments_count,prices,provider,provider_image,skill_level,state,quiz_question_options,deleted_at'
    };
  } else {
    query.fields = fields;
  }
  return dispatch => {
    return cards
      .getCards(query)
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getCards.func: ${err}`);
      });
  };
}

export function getSharedCards(limit, offset, sharedType, last_access_at = null, fields = null) {
  return dispatch => {
    return cards
      .getSharedCards({ limit, offset, shared_type: sharedType, last_access_at, fields })
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getSharedCards.func: ${err}`);
      });
  };
}

export function getRecommendedCards() {
  return new Promise((resolve, reject) => {
    //will use different card action, when server issue done
    return cards
      .getCards({ author_id: null, limit: 3, offset: 0, 'card_type[]': [] })
      .then(arr_cards => {
        return resolve(arr_cards);
      })
      .catch(err => {
        return reject(err);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getRecommendedCards.func: ${err}`);
      });
  });
}

export function getManagedCards(limit, offset, last_access_at = null, fields = null) {
  return dispatch => {
    return users
      .getManagedCards(limit, offset)
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getManagedCards.func: ${err}`);
      });
  };
}

export function getPurchasedCards(limit, offset, last_access_at = null, fields = null) {
  return dispatch => {
    return users
      .getPurchasedCards(limit, offset, last_access_at, fields)
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getManagedCards.func: ${err}`);
      });
  };
}

export function getPrivateCards(author_id, limit, offset, last_access_at = null, fields = null) {
  return dispatch => {
    let payload = {
      q: '',
      limit,
      offset,
      only_private: true,
      sort_attr: 'created_at',
      sort_order: 'desc',
      last_access_at,
      fields
    };
    payload['content_type[]'] = [
      'article',
      'poll',
      'video',
      'insight',
      'course',
      'media',
      'video_stream',
      'pack',
      'collection',
      'pathway'
    ];
    return ecl
      .eclSearch(payload)
      .then(arr_cards => {
        let filteredCards =
          arr_cards &&
          arr_cards.cards &&
          arr_cards.cards.filter(obj => !(obj.state === 'draft' && author_id != obj.authorId));
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: filteredCards
        });
        return filteredCards ? filteredCards.map(card => card.id) : [];
      })
      .catch(err => {
        console.error(`Error in cardsActions.getPrivateContent.func: ${err}`);
      });
  };
}

export function getContentCards(
  limit,
  offset,
  source = null,
  state = null,
  last_access_at = null,
  fields = null
) {
  return dispatch => {
    let payload = {
      include_queueable_details: true,
      limit,
      offset,
      last_access_at,
      fields
    };

    if (source) {
      payload['source[]'] = source;
    }

    if (state) {
      payload['state[]'] = state;
    }

    return learningQueue
      .getLearningQueueItems(payload)
      .then(data => {
        let arr_cards = [];
        data.learningQueueItems.map(item => {
          if (item.queueable) {
            arr_cards.push(item.queueable);
          }
        });

        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getLearningQueueItems.func: ${err}`);
      });
  };
}

export function getBookmarkedCards(limit, offset, last_access_at = null, fields = null) {
  return dispatch => {
    let payload = {
      limit,
      offset,
      last_access_at,
      fields
    };
    return learningQueue
      .getUserBookmarkedCards(payload)
      .then(data => {
        let arrCards = [];
        data.bookmarks.cards.map(item => {
          arrCards.push(item);
        });
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arrCards
        });
        return arrCards.map(card => card.id);
      })
      .catch(err => {
        console.error(`Error in cardsActions.getBookmarkedCards.func: ${err}`);
      });
  };
}

export function getCardsWithBaseTypes(payload, payloadAfter) {
  return dispatch => {
    return fetchCardsFromSubtype(payload)
      .then(arr_cards_with_type => {
        return fetchCardsFromSubtype(payloadAfter).then(arr_cards_without_type => {
          let arr_cards = uniqBy(arr_cards_with_type.concat(arr_cards_without_type), 'id');
          dispatch({
            type: actionTypes.RECEIVE_CARDS,
            cards: arr_cards
          });
          let data = {
            cards: arr_cards.map(card => card.id),
            cards_with_type_length: arr_cards_with_type ? arr_cards_with_type.length : 0,
            cards_without_type_length: arr_cards_without_type ? arr_cards_without_type.length : 0
          };
          return data;
        });
      })
      .catch(err => {
        console.error(
          `Error in cardsActions.getCardsWithBaseTypes.fetchCardsFromSubtype.func: ${err}`
        );
      });
  };
}

export function getCardsFromCardSubtype(payload) {
  return dispatch => {
    return fetchCardsFromSubtype(payload)
      .then(arr_cards => {
        dispatch({
          type: actionTypes.RECEIVE_CARDS,
          cards: arr_cards
        });
        return arr_cards.map(card => card.id);
      })
      .catch(err => {
        console.error(
          `Error in cardsActions.getCardsFromCardSubtype.fetchCardsFromSubtype.func: ${err}`
        );
      });
  };
}

export function getUserContentCards(payload) {
  return dispatch => {
    return getUserContent(payload)
      .then(response => {
        return response;
      })
      .catch(err => {
        console.error(`Error in cardsActions.getUserContent.func: ${err}`);
      });
  };
}

export function getBookmarkedCompletedCards(payload) {
  return dispatch => {
    return learningQueue
      .getLearningQueueItems(payload)
      .then(response => {
        let arr_cards = [];
        if (response && response.learningQueueItems) {
          response.learningQueueItems.map(item => {
            if (item.queueable) {
              arr_cards.push(item.queueable);
            }
          });
        }
        return arr_cards;
      })
      .catch(err => {
        console.error(`Error in cardsActions.getBookmarkedCompletedCards.func: ${err}`);
      });
  };
}
