import * as actionTypes from '../constants/actionTypes';
import { getLearningQueueItems } from 'edc-web-sdk/requests/feed';

export function getLearningQueueContent(payload = null) {
  return dispatch => {
    return getLearningQueueItems(payload || { 'state[]': ['active'] })
      .then(data => {
        dispatch({
          type: actionTypes.RECEIVE_LEARNING_QUEUE_ITEMS,
          items: data.learningQueueItems,
          pending: false
        });
      })
      .catch(() => {
        dispatch({
          type: actionTypes.RECEIVE_LEARNING_QUEUE_ITEMS,
          items: [],
          pending: false
        });
      });
  };
}

export function getLearningContent(payload = null) {
  return dispatch => {
    return getLearningQueueItems(payload)
      .then(data => {
        dispatch({
          type: actionTypes.RECEIVE_BOOKMARK_LEARNING_QUEUE,
          bookmarked: data.learningQueueItems,
          pending: false
        });
      })
      .catch(() => {
        dispatch({
          type: actionTypes.RECEIVE_BOOKMARK_LEARNING_QUEUE,
          bookmarked: [],
          pending: false
        });
      });
  };
}

export function removeBookmarkFromLearningQueue(cardId) {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_BOOKMARK_FROM_LEARNING_QUEUE,
      cardId
    });
  };
}
