import * as actionTypes from '../constants/actionTypes';
import { pathways } from 'edc-web-sdk/requests/index';
import { setToken } from 'edc-web-sdk/requests/csrfToken';

// calling api to get the list of user pathways to add to
export function getUserPathways(id, itemType) {
  return dispatch => {
    pathways
      .getPathwaysToAddTo(id, itemType)
      .then(pathwaysData => {
        dispatch({
          type: actionTypes.PATHWAYS_FOUND,
          id,
          pathways: pathwaysData
        });
      })
      .catch(error => {
        dispatch({
          error
        });
      });
  };
}

// calling api from ecl.js in edc-web-sdk
export function addToPathway(pathwayId, cardId, type) {
  return dispatch => {
    pathways
      .addToPathway(pathwayId, cardId, type)
      .then(pathwaysData => {
        dispatch({
          type: actionTypes.ADDED_CARD_TO_PATHWAY,
          cardId
        });
      })
      .catch(error => {
        dispatch({
          type: actionTypes.ERROR_WHILE_ADDING_TO_PATHWAY,
          cardId,
          error
        });
      });
  };
}
