import * as actionTypes from '../constants/actionTypes';
import { ecl } from 'edc-web-sdk/requests/index';
import { setToken } from 'edc-web-sdk/requests/csrfToken';

// function to get search results
// as of now there are 3 type of Use Cases:

// 1. When the user enter the query into search input & hit enter
//		- then the action type will be ECL_RESULTS_FOUND

// 2. When the user click on card type in the left sidebar
//		- then the case type will be 'change_card_type' & action type will be 'ORIGIN_TYPE_CLICKED'

// 3. When the user checks / unchecks an origin in the left sidebar
// 		- then the case type will be 'change_origin_type' & action type will be 'ORIGIN_TYPE_CLICKED'

// after that this call the requests/ecl.js from edc-web-sdk & dispatched the results
// check eclReducer.js & resultsReducer.js for more info.

export function getSearchResults(params, action) {
  let type;
  switch (action) {
    case 'change_card_type':
      type = actionTypes.CARD_TYPE_CLICKED;
      break;
    case 'change_origin_type':
      type = actionTypes.ORIGIN_TYPE_CLICKED;
      break;
    default:
      type = actionTypes.ECL_RESULTS_FOUND;
      break;
  }

  return dispatch => {
    ecl
      .getSearchResults(params)
      .then(results => {
        dispatch({
          type: type,
          results
        });
      })
      .catch(error => {
        dispatch({
          error
        });
      });
  };
}

// action to get all sources list for an org admin
export function getChannelSources(channelId) {
  return new Promise((resolve, reject) => {
    return ecl
      .getChannelSources(channelId)
      .then(data => {
        if (data) {
          resolve(data);
        }
      })
      .catch(error => {
        reject(error);
      });
  });
}
