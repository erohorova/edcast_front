import * as actionTypes from '../constants/actionTypes';
import { users, usersv2 } from 'edc-web-sdk/requests';

export function updateUser(usersParam) {
  return {
    type: actionTypes.RECEIVE_USERS,
    users: usersParam
  };
}

export function updateLeaderboardUser(usersParam) {
  return {
    type: actionTypes.RECEIVE_LEADERBOARD_USERS,
    users: usersParam
  };
}

export function getMentionSuggest(query) {
  return dispatch => {
    return users.getMentionSuggest(query).then(usersParam => {
      dispatch({
        type: actionTypes.RECEIVE_MENTION_USERS,
        users: usersParam
      });
    });
  };
}

export function getIntegrationList(id) {
  return dispatch => {
    return users.getIntegrationList(id).then(list => {
      dispatch({
        type: actionTypes.RECEIVE_INTEGRATION,
        list
      });
      return list.integrations || [];
    });
  };
}

export function toggleFollow(id, isFollow) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_TOGGLE_USER_FOLLOW,
      id
    });
    if (isFollow) {
      return usersv2.follow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_USER_FOLLOW,
          id,
          isFollow
        });
      });
    } else {
      return usersv2.unfollow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_USER_FOLLOW,
          id,
          isFollow
        });
      });
    }
  };
}

export function getUserByHandle(handle) {
  return dispatch => {
    return users.getUserByHandle(handle).then(user => {
      return usersv2.getUserBasicInfo(user.id).then(userParam => {
        dispatch({
          type: actionTypes.RECEIVE_PUBLIC_PROFILE_BASIC_INFO,
          userParam
        });
        return {
          id: userParam.id + '',
          coverImage: userParam.coverimages.banner_url,
          user: userParam
        };
      });
    });
  };
}

export function getUserFromHandle(handle) {
  return users.getPublicProfile(handle).then(user => {
    return user;
  });
}

export function getUserSkills(id) {
  return dispatch => {
    return users.getUserSkills(id).then(data => {
      dispatch({
        type: actionTypes.RECEIVE_SKILLS,
        data
      });
    });
  };
}

export function getPublicProfile(handle) {
  return dispatch => {
    return users.getPublicProfile(handle).then(data => {
      dispatch({
        type: actionTypes.RECEIVE_PUBLIC_PROFILE,
        data
      });
      return data;
    });
  };
}

export function loadingPublicProfileCards(loading) {
  return dispatch => {
    dispatch({
      type: actionTypes.LOADING_PUBLIC_PROFILE_CARDS,
      loading
    });
  };
}

export function removePublicProfile(handle) {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_PUBLIC_PROFILE
    });
  };
}

export function updateUserExpertise(expertiseDomainTopics, userId, profileId) {
  return dispatch => {
    return users
      .postExpertiseDomainTopicsNew(expertiseDomainTopics, userId, profileId)
      .then(response => {
        dispatch({
          type: actionTypes.UPDATE_USER_EXPERTISE_AND_INTEREST,
          profile: response.profile
        });
        return response;
      });
  };
}

export function updateUserInterest(learningDomainTopics, userId, profileId) {
  return dispatch => {
    return users
      .postLearningDomainTopicsNew(learningDomainTopics, userId, profileId)
      .then(response => {
        dispatch({
          type: actionTypes.UPDATE_USER_EXPERTISE_AND_INTEREST,
          profile: response.profile
        });
        return response;
      });
  };
}

export function orgSubscriptionPaid() {
  return {
    type: actionTypes.ORG_SUBSCRIPTION_PAID
  };
}

export function updateReportedCards(cardId) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_REPORTED_CARDS,
      cardId
    });
  };
}

export function updateReportedComments(commentId) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_REPORTED_COMMENTS,
      commentId
    });
  };
}
