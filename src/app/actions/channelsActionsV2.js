import * as actionTypes from '../constants/actionTypes';
import { channels, channelsv2, cards } from 'edc-web-sdk/requests';
import { curateItem as editContentState } from 'edc-web-sdk/requests/curate';
import { close as closeSnackBar } from '../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import _ from 'lodash';

const channelRedesign = window.ldclient.variation('channel-redesigning', false);
const cardV3 = window.ldclient.variation('card-v3', false);

export function toggleFollow(id, isFollow) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_TOGGLE_CHANNEL_FOLLOW_V2,
      id
    });
    if (isFollow) {
      return channelsv2.follow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW_V2,
          id,
          isFollow
        });
      });
    } else {
      return channelsv2.unfollow(id).then(() => {
        dispatch({
          type: actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW_V2,
          id,
          isFollow
        });
      });
    }
  };
}

export function toggleFollowV2(id, isFollow) {
  return dispatch => {
    if (isFollow) {
      return channelsv2.follow(id).then(() => {
        return isFollow;
      });
    } else {
      return channelsv2.unfollow(id).then(() => {
        return isFollow;
      });
    }
  };
}

export function snackBarOpenClose(message, timeout) {
  return dispatch => {
    dispatch({
      type: 'open_snackbar',
      message: message,
      autoClose: true
    });
    window.setTimeout(
      function() {
        dispatch(closeSnackBar());
      }.bind(this),
      timeout
    );
  };
}

export function getChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS_V2,
        channels: channelsData
      });
      return channelsData.map(channel => channel.id + '');
    });
  };
}

export function getFollowingChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getFollowingChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS_V2,
        channels: channelsData
      });
      return channelsData.map(channel => channel.id + '');
    });
  };
}

export function getSuggestedChannels(limit, offset) {
  return dispatch => {
    return channelsv2.getSuggestedChannels(limit, offset).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS_V2,
        channels: channelsData
      });
      return channelsData.map(channel => channel.id + '');
    });
  };
}

export function searchChannels(query) {
  return dispatch => {
    return channelsv2.searchChannels(query).then(channelsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNELS_V2,
        channels: channelsData
      });
      return channelsData.map(channel => channel.id + '');
    });
  };
}

export function getSingleChannel(slug, isUpdatingCurrent) {
  return dispatch => {
    return channels
      .getChannel(slug)
      .then(channel => {
        let channelsData = [channel];
        dispatch({
          type: actionTypes.RECEIVE_CHANNELS_V2,
          channels: channelsData,
          isUpdatingCurrent
        });
        return channelsData;
      })
      .catch(error => {
        return error;
      });
  };
}

export function getSingleChannelv2(slug) {
  return dispatch => {
    return channels
      .getChannel(slug)
      .then(channel => {
        dispatch({
          type: actionTypes.RECEIVE_CHANNELS_V2,
          channels: [channel],
          isUpdatingCurrent: false
        });
        return channel;
      })
      .catch(error => {
        return error;
      });
  };
}

export function getChannelCards(slug, limit, offset) {
  return dispatch => {
    return channels.getChannel(slug, limit, offset).then(data => {
      let cardIds = data.cards.reduce((acc, curr) => {
        acc.push(curr.id);
        return acc;
      }, []);
      return { cardIds, cards: data.cards };
    });
  };
}

export function updateChannels(channelsData) {
  return {
    type: actionTypes.RECEIVE_CHANNELS_V2,
    channels: channelsData
  };
}

export function getTypeCards(
  channelId,
  cardTypes,
  limit,
  offset = 0,
  forAll,
  filterByLanguage,
  last_access_at,
  fields
) {
  let payload = {
    'channel_id[]': channelId,
    limit,
    'card_type[]': cardTypes,
    offset
  };
  if (window.ldclient.variation('reorder-content', false)) {
    payload.sort = 'rank';
  }
  if (filterByLanguage) {
    payload.filter_by_language = true;
  }
  if (last_access_at) {
    payload.last_access_at = last_access_at;
  }
  if (fields) {
    payload.fields = fields;
  }
  return dispatch => {
    return cards.getCards(payload).then(cardsData => {
      dispatch({
        type: actionTypes.RECEIVE_CHANNEL_CARDS,
        channelId,
        cardTypes: forAll ? null : cardTypes,
        cards: cardsData
      });
      if (offset != 0) {
        dispatch({
          type: actionTypes.CHANGE_OFFSET,
          channelId,
          cardTypes: forAll ? null : cardTypes,
          offset
        });
      }
      return cardsData;
    });
  };
}

export function getTypeCardsv2(
  channelId,
  cardTypes,
  limit,
  offset = 0,
  filterByLanguage,
  last_access_at,
  fields
) {
  let payload = {
    'channel_id[]': channelId,
    limit,
    'card_type[]': cardTypes,
    offset
  };
  if (window.ldclient.variation('reorder-content', false)) {
    payload.sort = 'rank';
  }
  if (filterByLanguage) {
    payload.filter_by_language = true;
  }
  if (last_access_at) {
    payload.last_access_at = last_access_at;
  }
  if (fields) {
    payload.fields = fields;
  }
  return cards.getCards(payload).then(cardsData => {
    return cardsData;
  });
}

export function getTypeCardsAfterReordering(channelId, cardTypes, limit, offset = 0) {
  return dispatch => {
    return cards
      .getCards({
        'channel_id[]': channelId,
        limit,
        'card_type[]': cardTypes,
        sort: 'rank',
        offset
      })
      .then(cardsData => {
        dispatch({
          type: actionTypes.RECEIVE_CHANNEL_CARDS_AFTER_REORDERING,
          channelId,
          cardTypes,
          cards: cardsData
        });
        if (offset != 0) {
          dispatch({
            type: actionTypes.CHANGE_OFFSET,
            channelId,
            cardTypes,
            offset
          });
        }
      });
  };
}

export function saveCurators(channelId, curators, currentUsers) {
  let idsToAdd = _.differenceBy(curators, currentUsers, 'id').map(item => item.id);
  let idsToRemove = _.differenceBy(currentUsers, curators, 'id').map(item => item.id);
  let promises = [];
  if (idsToAdd.length) {
    promises.push(
      channels.addCurators({
        id: channelId,
        user_ids: idsToAdd
      })
    );
  }
  if (idsToRemove.length) {
    promises.push(channels.removeCurator(channelId, idsToRemove));
  }
  return dispatch => {
    Promise.all(promises)
      .then(() => {
        dispatch({
          type: actionTypes.UPDATE_CURATORS,
          channelId,
          curators
        });
      })
      .catch(err => {
        console.error(`Error in channelsActionsV2.saveCurators.Promise.all.func : ${err}`);
      });
  };
}

export function updateChannelDetails(channel, labelChanged) {
  return dispatch => {
    return channels.editChannel(channel).then(newChannel => {
      if (labelChanged) {
        window.location.href = '/channel' + `/${newChannel.slug}`;
      }
      dispatch({
        type: actionTypes.UPDATE_CHANNEL_DETAILS,
        newChannel
      });
    });
  };
}

export function removeCurator(channelId, curatorId) {
  return dispatch => {
    return channels.removeCurator(channelId, curatorId).then(() => {
      dispatch({
        type: actionTypes.REMOVE_CURATOR,
        channelId,
        curatorId
      });
    });
  };
}

export function pinUnpinCard(payload, pinnedStatus, cardTypes, card) {
  if (pinnedStatus) {
    return dispatch => {
      return channelsv2.pinCard(payload).then(() => {
        dispatch({
          type: actionTypes.UPDATE_CHANNEL_CARD_PINNED_STATUS,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  } else {
    return dispatch => {
      return channelsv2.unpinCard(payload).then(() => {
        dispatch({
          type: cardTypes
            ? actionTypes.UPDATE_CHANNEL_CARD_PINNED_STATUS
            : actionTypes.UPDATE_CHANNEL_CARD_PINNED_STATUS_IN_ALL,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  }
}

export function pinUnpinCardv2(payload, pinnedStatus, cardTypes, card) {
  if (pinnedStatus) {
    return dispatch => {
      return channelsv2.pinCard(payload).then(() => {
        dispatch({
          type: actionTypes.ADD_TO_PINNED_CARD_CAROUSEL,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  } else {
    return dispatch => {
      return channelsv2.unpinCard(payload).then(() => {
        dispatch({
          type: actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  }
}

export function fetchPinnedCards(channelId, itemType, filterByLanguage, last_access_at, fields) {
  return dispatch => {
    return channelsv2
      .fetchPinnedCards(channelId, itemType, filterByLanguage, last_access_at, fields)
      .then(cardsData => {
        dispatch({
          type: actionTypes.FETCH_PINNED_CARDS,
          channelId,
          itemType,
          cards: cardsData
        });
        return cardsData;
      });
  };
}

export function fetchPinnedCardsv2(channelId, itemType, filterByLanguage) {
  return dispatch => {
    return channelsv2.fetchPinnedCards(channelId, itemType, filterByLanguage).then(cardsData => {
      dispatch({
        type: actionTypes.RECIEVE_PINNED_CARDS,
        channelId,
        itemType,
        cards: cardsData
      });
      return cardsData;
    });
  };
}

export function removeChannelCardv2(channelId, card, pinnedStatus) {
  let payload = {
    'card_ids[]': [card.id]
  };

  return dispatch => {
    return channelsv2.removeCards(channelId, payload).then(() => {
      if (pinnedStatus) {
        dispatch({
          type: actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL,
          card
        });
      }
    });
  };
}

export function removeChannelCard(channelId, card, cardTypes) {
  let payload = {
    'card_ids[]': [card.id]
  };
  return dispatch => {
    return channelsv2.removeCards(channelId, payload).then(() => {
      if (channelRedesign && cardV3) {
        dispatch({
          type: actionTypes.REMOVE_CHANNEL_CARD_FROM_CAROUSELS,
          channelId,
          cardId: card.id,
          cardTypes
        });
      } else {
        dispatch({
          type: actionTypes.REMOVE_CHANNEL_CARD,
          channelId,
          cardId: card.id,
          cardTypes
        });
      }
    });
  };
}

export function getNonCurateCards(isUgc, channel, offset = 0, isInitialize, existingCards) {
  return dispatch => {
    return channelsv2
      .getNonCurateCardsModal(channel.id, {
        limit: 12,
        channel_card_state: 'new',
        offset: offset,
        ugc: isUgc
      })
      .then(response => {
        dispatch({
          type: actionTypes.OPEN_CURATE_CARD_MODAL,
          channel,
          response,
          isInitialize,
          existingCards
        });
      });
  };
}

export function reOpenCurateModal(channel, response, publishedCard) {
  return dispatch => {
    dispatch({
      type: actionTypes.OPEN_CURATE_CARD_MODAL,
      channel,
      response,
      isReopen: true,
      publishedCard
    });
  };
}

export function curateItem(cardId, channelId, channelName) {
  let publishMessage = `Published to ${channelName}.`;
  return dispatch => {
    return editContentState('curate', [cardId], channelId).then(
      () => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: publishMessage,
          autoClose: true
        });
        window.setTimeout(function() {
          dispatch(closeSnackBar());
        }, 1000);
      },
      () => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: tr('Failed to publish. Please try again.'),
          autoClose: true
        });
        window.setTimeout(function() {
          dispatch(closeSnackBar());
        }, 1000);
      }
    );
  };
}
export function skipItem(cardId, channelId) {
  return dispatch => {
    return editContentState('skip', [cardId], channelId)
      .then(() => {
        dispatch({
          type: actionTypes.RECEIVE_CURATE_CHANGE_STATE,
          cardId
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: tr('Rejected card from channel successfully.'),
          autoClose: true
        });
      })
      .catch(() => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: tr('Failed to reject. Please try again.'),
          autoClose: true
        });
      });
  };
}

export function removeCardFromCarousel(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL,
      card
    });
  };
}

export function emptyPinnedCardCarousel(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_ALL_PINNED_CARD
    });
  };
}
