import * as actionTypes from '../constants/actionTypes';
import { feed } from 'edc-web-sdk/requests/index';

export function getPromotedInsights(limit, offset, isInit) {
  return dispatch => {
    dispatch({
      type: actionTypes.REQUEST_USER_FEED
    });
    feed
      .getPromotedFeeds(limit, offset)
      .then(({ items, offsetData }) => {
        dispatch({
          type: actionTypes.RECEIVE_PROMOTED_FEED,
          feed: items,
          offset: offsetData,
          isInit
        });
      })
      .catch(err => {
        console.error(`Error in feedActions.getPromotedFeeds.func: ${err}`);
      });
  };
}

export function setFirstFeedTab(tab) {
  return dispatch => {
    dispatch({
      type: actionTypes.FIRST_FEED_TAB,
      tab
    });
  };
}
