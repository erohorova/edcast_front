import * as actionTypes from '../constants/actionTypes';
import { cards } from 'edc-web-sdk/requests';

export function open(message, autoClose, closeHandler) {
  return {
    type: actionTypes.OPEN_SNACKBAR,
    message,
    autoClose,
    closeHandler
  };
}

export function close() {
  return {
    type: actionTypes.CLOSE_SNACKBAR
  };
}
