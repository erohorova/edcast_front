import * as actionTypes from '../constants/actionTypes';
import { groupsV2 } from 'edc-web-sdk/requests/index';

export function getGraphData(teamId, payload, isTeamAnalyticsV2) {
  return dispatch => {
    return groupsV2.getTeamAnalytics(teamId, payload, isTeamAnalyticsV2).then(graphData => {
      dispatch({
        type: actionTypes.GET_GRAPH_DATA,
        graphData
      });
      return graphData;
    });
  };
}

export function getGroupDetail(slug) {
  return dispatch => {
    return groupsV2.getGroupDetails(slug).then(groupDetail => {
      dispatch({
        type: actionTypes.SET_GROUP_DETAILS,
        group: groupDetail
      });
      return groupDetail;
    });
  };
}

export function topContent(teamId, payload) {
  return dispatch => {
    return groupsV2.getTopContent(teamId, payload).then(content => {
      dispatch({
        type: actionTypes.GET_TOP_CONTENT,
        content
      });
      return content;
    });
  };
}

export function topContributors(teamId, payload) {
  return dispatch => {
    return groupsV2.getTopContributors(teamId, payload).then(contributors => {
      dispatch({
        type: actionTypes.GET_TOP_CONTRIBUTORS,
        contributors
      });
      return contributors;
    });
  };
}

export function getTeamAssignmentMetrics(payload) {
  return dispatch => {
    return groupsV2.getTeamAssessmentMetrics(payload).then(assignmentMetrics => {
      dispatch({
        type: actionTypes.GET_TEAM_ASSIGNMENT_METRICS,
        assignmentMetrics,
        offset: payload.offset
      });
    });
  };
}

export function getTeamAnalyticsUserList(metric, modalState) {
  return {
    type: actionTypes.OPEN_TEAM_ANALYTICS_USER_LIST_MODAL,
    metric,
    modalState
  };
}
