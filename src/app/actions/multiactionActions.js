import { fetchCard } from 'edc-web-sdk/requests/cards';
import { fetchGroups, fetchOrgTeams } from 'edc-web-sdk/requests/groups.v2';
import { searchUsers } from 'edc-web-sdk/requests/users.v2';
import { getCurrentlyAssigned } from 'edc-web-sdk/requests/assignments.v2';
import * as actionTypes from '../constants/actionTypes';

export function _fetchCard(payload) {
  return dispatch => {
    return fetchCard(payload).then(card => {
      dispatch({
        type: actionTypes.FETCH_CONTENT_TO_ACTION,
        card
      });
    });
  };
}

export function _fetchUsers(payload, actionKey) {
  return dispatch => {
    return searchUsers(payload).then(users => {
      dispatch({
        type: actionTypes.FETCH_USERS_FOR_ACTION,
        users,
        actionKey
      });
    });
  };
}

export function _fetchGroups(payload, actionKey) {
  return dispatch => {
    return fetchGroups(payload).then(groups => {
      dispatch({
        type: actionTypes.FETCH_MY_TEAM_FOR_ACTIONS,
        groups,
        actionKey
      });
    });
  };
}

export function _fetchOrgGroups(payload, actionKey) {
  return dispatch => {
    return fetchOrgTeams(payload).then(groups => {
      dispatch({
        type: actionTypes.FETCH_MY_TEAM_FOR_ACTIONS,
        groups,
        actionKey
      });
    });
  };
}

export function _fetchAssigned(payload) {
  return dispatch => {
    return getCurrentlyAssigned(payload).then(assigned => {
      dispatch({
        type: actionTypes.FETCH_ASSIGNED_USERS_AND_GROUPS,
        assigned
      });
    });
  };
}

export function _fetchAllAssigned(payload) {
  return dispatch => {
    return getCurrentlyAssigned(payload).then(assigned => {
      dispatch({
        type: actionTypes.FETCH_ALL_ASSIGNED,
        assigned
      });
    });
  };
}

export function _clearSearchQuery(searchType) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAR_CURRENT_SEARCH,
      searchType
    });
  };
}

export function _fetchSearchedAssigned(results, searchType, query) {
  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_SEARCHED_ASSIGNED,
      results,
      searchType,
      query
    });
  };
}

export function _addUser(user, card, assignments, currentAction, isSearch, groups) {
  return dispatch => {
    dispatch({
      type: isSearch ? actionTypes.ADD_SEARCHED_USER_FOR_ACTION : actionTypes.ADD_USER_FOR_ACTION,
      user,
      card,
      assignments,
      currentAction,
      groups
    });
  };
}

export function _addGroup(group, card, currentAction, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch ? actionTypes.ADD_SEARCHED_GROUP_FOR_ACTION : actionTypes.ADD_GROUP_FOR_ACTION,
      group,
      card,
      currentAction
    });
  };
}

export function _removeUser(user, card, assignments, currentAction, isSearch, groups) {
  return dispatch => {
    dispatch({
      type: isSearch
        ? actionTypes.REMOVE_SEARCHED_USER_FROM_ACTION
        : actionTypes.REMOVE_USER_FROM_ACTION,
      user,
      card,
      assignments,
      currentAction,
      groups
    });
  };
}

export function _removeGroup(group, card, currentAction, isSearch) {
  return dispatch => {
    dispatch({
      type: isSearch
        ? actionTypes.REMOVE_SEARCHED_GROUP_FROM_ACTION
        : actionTypes.REMOVE_GROUP_FROM_ACTION,
      group,
      card,
      currentAction
    });
  };
}

export function _clearState(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAR_ACTION_CONTENT_STATE,
      card
    });
  };
}

export function _clearRemoveUsersList(currentAction) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAR_REMOVE_USERS_LIST,
      currentAction
    });
  };
}

export function _clearRemoveGroupsList(currentAction) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAR_REMOVE_GROUPS_LIST,
      currentAction
    });
  };
}

export function _fillRemoveUsersList(users) {
  return dispatch => {
    dispatch({
      type: actionTypes.FILL_REMOVE_USERS_LIST,
      users
    });
  };
}

export function _fillRemoveGroupsList(groups) {
  return dispatch => {
    dispatch({
      type: actionTypes.FILL_REMOVE_GROUPS_LIST,
      groups
    });
  };
}

export function toggleIndividuals(shouldUnCheck, currentUsers, users, currentAction) {
  return dispatch => {
    dispatch({
      type: actionTypes.FILL_ADD_USERS_LIST,
      shouldUnCheck,
      currentUsers,
      users,
      currentAction
    });
  };
}

export function toggleGroups(shouldUnCheck, groups, currentAction) {
  return dispatch => {
    dispatch({
      type: actionTypes.FILL_ADD_GROUPS_LIST,
      shouldUnCheck,
      groups,
      currentAction
    });
  };
}
