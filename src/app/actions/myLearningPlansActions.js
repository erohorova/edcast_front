import * as actionTypes from '../constants/actionTypes';
import { myLearningPlan } from 'edc-web-sdk/requests/index';

export function fetchQuarterlyPlan(payload) {
  payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
  return dispatch => {
    return myLearningPlan.getQuarterlyPlan(payload).then(data => {
      dispatch({
        type: actionTypes.QUARTERLY_ASSIGNMENTS,
        data
      });
    });
  };
}

export function fetchCompetencyPlan(payload) {
  payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
  return dispatch => {
    return myLearningPlan.getQuarterlyPlan(payload).then(data => {
      dispatch({
        type: actionTypes.COMPETENCY_ASSIGNMENTS,
        data
      });
    });
  };
}
