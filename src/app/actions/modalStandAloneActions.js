import * as actionTypes from '../constants/actionTypes';

export function closeStandaloneOverviewModal(dataCard) {
  return {
    type: actionTypes.CLOSE_STANDALONE_CARD_MODAL,
    dataCard
  };
}

export function openStandaloneOverviewModal(card, cardType, dataCard, cardUpdated) {
  return {
    type: actionTypes.OPEN_STANDALONE_MODAL,
    card,
    cardType,
    dataCard,
    cardUpdated
  };
}
