import * as actionTypes from '../constants/actionTypes';
import {
  users,
  channels,
  courses,
  cards,
  pathways,
  journeys,
  ecl,
  countsv2,
  edcastcloud
} from 'edc-web-sdk/requests';
import isEmpty from 'lodash/isEmpty';

export function getInitDataOld(payload, discoverConfig) {
  return dispatch => {
    if (
      discoverConfig &&
      discoverConfig['discover/courses'] &&
      discoverConfig['discover/courses'].visible
    ) {
      edcastcloud
        .recommendedCourses(20, 0, true)
        .then(coursesData => {
          dispatch({
            type: actionTypes.RECEIVE_DISCOVERY_COURSES,
            courses: coursesData
          });
        })
        .catch(err => {
          console.error(`Error in discoveryActions.recommendedCourses.func: ${err}`);
        });
    }

    if (
      discoverConfig &&
      discoverConfig['discover/videos'] &&
      discoverConfig['discover/videos'].visible
    ) {
      cards
        .getTrendingVideoCards({
          filter_by_language: window.ldclient.variation('multilingual-content', false)
        })
        .then(videos => {
          dispatch({
            type: actionTypes.RECEIVE_DISCOVERY_VIDEOS,
            videos
          });
        })
        .catch(err => {
          console.error(`Error in discoveryActions.getTrendingVideoCards.func: ${err}`);
        });
    }

    if (
      discoverConfig &&
      discoverConfig['discover/pathways'] &&
      discoverConfig['discover/pathways'].visible
    ) {
      pathways
        .getTrendingPathways({
          filter_by_language: window.ldclient.variation('multilingual-content', false)
        })
        .then(pathwaysData => {
          dispatch({
            type: actionTypes.RECEIVE_DISCOVERY_PATHWAYS,
            pathways: pathwaysData
          });
        })
        .catch(err => {
          console.error(`Error in discoveryActions.getTrendingPathways.func:  ${err}`);
        });
    }

    if (
      discoverConfig &&
      discoverConfig['discover/journeys'] &&
      discoverConfig['discover/journeys'].visible
    ) {
      journeys
        .getTrendingJourneys({
          filter_by_language: window.ldclient.variation('multilingual-content', false)
        })
        .then(journeysData => {
          dispatch({
            type: actionTypes.RECEIVE_DISCOVERY_JOURNEYS,
            journeys: journeysData
          });
        })
        .catch(err => {
          console.error(`Error in discoveryActions.getTrendingJourneys.func:  ${err}`);
        });
    }

    if (
      discoverConfig &&
      discoverConfig['discover/featuredProviders'] &&
      discoverConfig['discover/featuredProviders'].visible
    ) {
      ecl
        .getAllSources(payload)
        .then(featuredProviders => {
          dispatch({
            type: actionTypes.RECEIVE_FEATURED_PROVIDERS,
            featuredProviders
          });
        })
        .catch(err => {
          console.error(`Error in discoveryActions.getAllSources.func: ${err}`);
        });
    }
  };
}

export function getInitData(payload, discoverConfig) {
  let promiseArr = [];
  let coursesCall;
  let videosCall;
  let pathwaysCall;
  let journeysCall;
  let featuredProvidersCall;
  if (
    discoverConfig &&
    discoverConfig['discover/courses'] &&
    discoverConfig['discover/courses'].visible
  ) {
    coursesCall = edcastcloud
      .recommendedCourses(20, 0, true)
      .then(coursesData => {
        if (isEmpty(coursesData)) {
          return null;
        } else {
          // return () => {
          return dispatch => {
            dispatch({
              type: actionTypes.RECEIVE_DISCOVERY_COURSES,
              courses: coursesData
            });
          };
          // }
        }
      })
      .catch(error => {
        return null;
      });
    promiseArr.push(coursesCall);
  }

  if (
    discoverConfig &&
    discoverConfig['discover/videos'] &&
    discoverConfig['discover/videos'].visible
  ) {
    videosCall = cards
      .getTrendingVideoCards({
        filter_by_language: window.ldclient.variation('multilingual-content', false)
      })
      .then(videos => {
        if (isEmpty(videos)) {
          return null;
        } else {
          // return () => {
          return dispatch => {
            dispatch({
              type: actionTypes.RECEIVE_DISCOVERY_VIDEOS,
              videos
            });
          };
          // }
        }
      })
      .catch(error => {
        return null;
      });
    promiseArr.push(videosCall);
  }

  if (
    discoverConfig &&
    discoverConfig['discover/pathways'] &&
    discoverConfig['discover/pathways'].visible
  ) {
    pathwaysCall = pathways
      .getTrendingPathways({
        filter_by_language: window.ldclient.variation('multilingual-content', false)
      })
      .then(pathwaysData => {
        if (isEmpty(pathwaysData)) {
          return null;
        } else {
          // return () => {
          return dispatch => {
            dispatch({
              type: actionTypes.RECEIVE_DISCOVERY_PATHWAYS,
              pathways: pathwaysData
            });
          };
          // }
        }
      })
      .catch(error => {
        return null;
      });
    promiseArr.push(pathwaysCall);
  }

  if (
    discoverConfig &&
    discoverConfig['discover/journeys'] &&
    discoverConfig['discover/journeys'].visible
  ) {
    journeysCall = journeys
      .getTrendingJourneys({
        filter_by_language: window.ldclient.variation('multilingual-content', false)
      })
      .then(journeysData => {
        if (isEmpty(journeysData)) {
          return null;
        } else {
          return dispatch => {
            dispatch({
              type: actionTypes.RECEIVE_DISCOVERY_JOURNEYS,
              journeys: journeysData
            });
          };
        }
      })
      .catch(error => {
        return null;
      });
    promiseArr.push(journeysCall);
  }

  if (
    discoverConfig &&
    discoverConfig['discover/featuredProviders'] &&
    discoverConfig['discover/featuredProviders'].visible
  ) {
    featuredProvidersCall = ecl
      .getAllSources(payload)
      .then(featuredProviders => {
        if (isEmpty(featuredProviders)) {
          return null;
        } else {
          // return () => {
          return dispatch => {
            dispatch({
              type: actionTypes.RECEIVE_FEATURED_PROVIDERS,
              featuredProviders
            });
          };
          // }
        }
      })
      .catch(error => {
        return null;
      });
    promiseArr.push(featuredProvidersCall);
  }
  return promiseArr;
}
