import * as actionTypes from '../constants/actionTypes';
import { pathways } from 'edc-web-sdk/requests';

export function getPathways(payload) {
  return dispatch => {
    return pathways.getPathways(payload).then(pathwaysParam => {
      dispatch({
        type: actionTypes.RECEIVE_PATHWAYS,
        pathways: pathwaysParam
      });
      return pathwaysParam;
    });
  };
}

export function searchPathways(payload) {
  return dispatch => {
    return pathways.getPathways(payload).then(pathwaysParam => {
      dispatch({
        type: actionTypes.RECEIVE_PATHWAYS,
        pathways: pathwaysParam
      });
      return pathwaysParam;
    });
  };
}

export function addCardToPathways(arr_pathways) {
  return dispatch => {
    dispatch({
      type: actionTypes.RECEIVE_CARD_INTO_PATHWAYS,
      arr_pathways
    });
  };
}

export function saveTempPathway(card) {
  return {
    type: actionTypes.SAVE_TEMP_PATHWAY,
    card
  };
}

export function deleteTempPathway() {
  return {
    type: actionTypes.DELETE_TEMP_PATHWAY
  };
}

export function saveConsumptionPathway(card) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_PATHWAY,
    card
  };
}

export function saveConsumptionPathwayHistoryURL(url) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_PATHWAY_HISTORY_URL,
    url
  };
}

export function consumptionPathwayHistory(card) {
  return {
    type: actionTypes.CONSUMPTION_PATHWAY_HISTORY,
    card
  };
}

export function removeConsumptionPathway(url) {
  return {
    type: actionTypes.REMOVE_CONSUMPTION_PATHWAY,
    url
  };
}

export function removeConsumptionPathwayHistory() {
  return {
    type: actionTypes.REMOVE_CONSUMPTION_PATHWAY_HISTORY
  };
}

export function saveReorderCardIds(cardIds) {
  return {
    type: actionTypes.SAVE_REORDER_CARD_IDS_PATHWAY,
    cardIds
  };
}

export function removeReorderCardIds() {
  return {
    type: actionTypes.REMOVE_REORDER_CARD_IDS_PATHWAY
  };
}

export function isPreviewMode(bool) {
  return {
    type: actionTypes.IS_PREVIEW_MODE_PATHWAY,
    bool
  };
}
