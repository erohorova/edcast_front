import * as actionTypes from '../constants/actionTypes';
import { ecl } from 'edc-web-sdk/requests/index';

export function getSearchResults(params, initialState, detailedSearch = true) {
  params['segregate_pathways'] = detailedSearch;
  return dispatch => {
    return ecl
      .eclSearch(params)
      .then(items => {
        if (initialState) {
          dispatch({
            type: actionTypes.RECEIVE_FILTERS,
            items
          });
          dispatch({
            type: actionTypes.RESET_STATE
          });
        }

        dispatch({
          type: actionTypes.RECEIVE_SEARCH_ITEMS,
          items
        });

        if (!detailedSearch) {
          dispatch({
            type: actionTypes.UPDATE_MINIMAL_COUNTS,
            items
          });
        }
        return items;
      })
      .catch(error => {
        dispatch({
          type: actionTypes.ERROR_FETCHING_SEARCH_RESULTS
        });
      });
  };
}

export function loadMoreSearchResults(params, detailedSearch = true) {
  params['segregate_pathways'] = detailedSearch;
  return dispatch => {
    return ecl.eclSearch(params, true).then(items => {
      dispatch(LoadMoreSearchItems(items));
      return items;
    });
  };
}

function LoadMoreSearchItems(items) {
  return {
    type: actionTypes.LOAD_MORE_SEARCH_ITEMS,
    items
  };
}

export function modifyTypeFilters(filter, status, filterType) {
  return dispatch => {
    if (filterType == 'content_type') {
      dispatch({
        type: actionTypes.MODIFY_TYPE_FILTERS,
        filter,
        status
      });
    }
    if (filterType == 'plan') {
      dispatch({
        type: actionTypes.MODIFY_PLAN_FILTERS,
        filter,
        status
      });
    }
    if (filterType == 'source_id') {
      dispatch({
        type: actionTypes.MODIFY_SOURCE_FILTERS,
        filter,
        status
      });
    }
    if (filterType == 'provider_name') {
      dispatch({
        type: actionTypes.MODIFY_PROVIDER_FILTERS,
        filter,
        status
      });
    }
    if (filterType == 'subjects') {
      dispatch({
        type: actionTypes.MODIFY_SUBJECTS_FILTERS,
        filter,
        status
      });
    }
    if (filterType == 'languages') {
      dispatch({
        type: actionTypes.MODIFY_LANGUAGE_FILTERS,
        filter,
        status
      });
    }
    if (
      [
        'free_paid',
        'level',
        'rating',
        'currency',
        'amount_range',
        'amount_range_empty',
        'clear_price',
        'posted_by',
        'liked_by',
        'commented_by',
        'bookmarked_by'
      ].includes(filterType)
    ) {
      dispatch({
        type: actionTypes.MODIFY_ACTION_BY_FILTERS,
        filterType,
        filter,
        status
      });
    }
  };
}

export function showLoadingWithReset(shouldResetInline) {
  return dispatch => {
    dispatch({
      type: actionTypes.SHOW_LOADER_WITH_RESET,
      shouldResetInline
    });
  };
}

export function hideFilters() {
  return dispatch => {
    dispatch({
      type: actionTypes.HIDE_FILTERS
    });
  };
}

export function updateCardInReducer(updatedCard) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_CARD_IN_RESULT,
      updatedCard
    });
  };
}
