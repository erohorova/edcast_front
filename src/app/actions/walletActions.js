import * as actionTypes from '../constants/actionTypes';
import { users, groups, wallet } from 'edc-web-sdk/requests/index';
import { setToken, JWT } from 'edc-web-sdk/requests/csrfToken';
import { Permissions } from '../utils/checkPermissions';
import { push } from 'react-router-redux';

export function getWalletRecharges(localCurrency) {
  return async dispatch => {
    const walletRecharges = await wallet.getWalletRecharges();
    walletRecharges.recharges.map(recharge => {
      let localPrices = recharge.prices.filter(price => {
        return price.currency == localCurrency;
      });
      if (localPrices.length != 0) {
        recharge.prices = localPrices;
      }
    });
    dispatch({
      type: actionTypes.RECEIVE_WALLET_RECHARGES,
      walletRecharges
    });
    return Promise.resolve();
  };
}

export function getWalletTranscations(payload) {
  return async dispatch => {
    dispatch({
      type: actionTypes.SHOW_WALLET_LOADING
    });

    const WalletTransactions = await wallet.getAllWalletTransactions(payload);
    dispatch({
      type: actionTypes.RECEIVE_WALLET_TRANSACTIONS,
      walletTransactions: WalletTransactions['transactions'] || [],
      walletTransactionsCount: WalletTransactions['transactionsCount'] || 0,
      reload: payload.reload || false
    });

    dispatch({
      type: actionTypes.HIDE_WALLET_LOADING
    });
  };
}
