import * as actionTypes from '../constants/actionTypes';
import { fetchUserInterests } from 'edc-web-sdk/requests/interests';
import { myLearningPlan } from 'edc-web-sdk/requests/index';
import { searchLearningTopics } from 'edc-web-sdk/requests/users';

export function contentTabChange(activeTab) {
  return dispatch => {
    dispatch({
      type: actionTypes.CONTENT_TAB_CHANGE,
      activeTab
    });
  };
}

export function changeCurrentYearObj(currentYearObj) {
  return dispatch => {
    dispatch({
      type: actionTypes.CHANGE_YEAR_OBJ,
      currentYearObj
    });
  };
}

export function changeCurrentYear(currentYear) {
  return dispatch => {
    dispatch({
      type: actionTypes.CHANGE_YEAR,
      currentYear
    });
  };
}

export function contentTypeTabChange(activeTab) {
  return dispatch => {
    dispatch({
      type: actionTypes.CONTENT_TYPE_TAB_CHANGE,
      activeTab
    });
  };
}

export function getUserInterests(userId) {
  return dispatch => {
    return fetchUserInterests(userId).then(userInterest => {
      dispatch({
        type: actionTypes.GET_USER_INTEREST_FOR_MLP,
        userInterest
      });
    });
  };
}

export function clearAssignments(activeContentTab) {
  return dispatch => {
    dispatch({
      type: actionTypes.CLEAN_ASSIGNMENTS,
      activeContentTab
    });
  };
}

export function fetchQuarterlyPlan(payload, activeContentTab, loadMore, searchQuery = '') {
  return dispatch => {
    payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
    return myLearningPlan.getQuarterlyPlan(payload).then(data => {
      dispatch({
        type: loadMore
          ? actionTypes.LOAD_MORE_ASSIGNMENTS
          : actionTypes.STORE_QUARTERLY_ASSIGNMENTS,
        data,
        activeContentTab,
        searchQuery
      });
      return data;
    });
  };
}

export function fetchCompetencyPlan(payload, activeContentTab, loadMore, searchQuery = '') {
  return dispatch => {
    payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
    return myLearningPlan.getQuarterlyPlan(payload).then(data => {
      dispatch({
        type: loadMore
          ? actionTypes.LOAD_MORE_ASSIGNMENTS
          : actionTypes.STORE_COMPETENCY_ASSIGNMENTS,
        data,
        activeContentTab,
        searchQuery
      });
      return data;
    });
  };
}

export function searchPlan(id, payload, type) {
  return dispatch => {
    return searchLearningTopics(id, payload).then(data => {
      dispatch({
        type: actionTypes.SEARCH_RESULT,
        data
      });
      return data;
    });
  };
}

export function removeDissmissCard(card) {
  return dispatch => {
    dispatch({
      type: actionTypes.REMOVE_DISMISS_CARD,
      card
    });
  };
}

export function getInProgress(payload) {
  return dispatch => {
    payload.filter_by_language = window.ldclient.variation('multilingual-content', false);
    return myLearningPlan.getQuarterlyPlan(payload).then(inProgressData => {
      dispatch({
        type: actionTypes.GET_IN_PROGRESS,
        inProgressData
      });
    });
  };
}
