import * as actionTypes from '../constants/actionTypes';
import { fetchCard, fetchCardForEdit, fetchCardSecuredUrls } from 'edc-web-sdk/requests/cards';
import { fetchJourney } from 'edc-web-sdk/requests/journeys';

export function confirmation(title, message, callback, hideCancelBtn) {
  return {
    type: actionTypes.OPEN_CONFIRMATION_MODAL,
    title,
    message,
    callback,
    hideCancelBtn
  };
}

export function confirmationPrivateCardModal(title, message, isPrivate, callback) {
  return {
    type: actionTypes.OPEN_PRIVATE_CARD_CONFIRMATION_MODAL,
    title,
    message,
    isPrivate,
    callback
  };
}

export function close(packType) {
  return dispatch => {
    if (packType === 'pathway') {
      dispatch({
        type: actionTypes.REMOVE_REORDER_CARD_IDS_PATHWAY
      });
    } else if (packType === 'journey') {
      dispatch({
        type: actionTypes.REMOVE_REORDER_CARD_IDS_JOURNEY
      });
    }
    dispatch({
      type: actionTypes.CLOSE_MODAL
    });
  };
}

export function closeCard() {
  return {
    type: actionTypes.CLOSE_CARD_MODAL
  };
}

export function closeInterestModal() {
  return {
    type: actionTypes.CLOSE_INTEREST_MODAL
  };
}

export function openAssignModal(card, selfAssign, assignedStateChange) {
  return {
    type: actionTypes.OPEN_ASSIGN_MODAL,
    card,
    selfAssign,
    assignedStateChange
  };
}

export function openMultiactionsModal(card, currentAction, selfAssign) {
  return {
    type: actionTypes.OPEN_MULTIACTIONS_MODAL,
    card,
    currentAction,
    selfAssign
  };
}

export function openGroupCreationModal(updateGroupsList, groupId) {
  return {
    type: actionTypes.OPEN_GROUP_CREATION_MODAL,
    updateGroupsList,
    groupId
  };
}

export function openCardStatsModal(card, isViewsModal) {
  return {
    type: actionTypes.OPEN_CARD_STATS_MODAL,
    card,
    isViewsModal
  };
}

export function openCardActionsModal(card, typeOfModal) {
  return {
    type: actionTypes.OPEN_CARD_ACTION_MODAL,
    card,
    typeOfModal
  };
}

export function openIntegrationModal(integration, importClickHandler) {
  return {
    type: actionTypes.OPEN_INTEGRATION_MODAL,
    integration,
    importClickHandler
  };
}

export function openAddToPathway(cardId, card, cardType, isAddingToJourney) {
  return {
    type: actionTypes.OPEN_ADD_TO_PATHWAY_MODAL,
    cardId,
    card,
    cardType,
    isAddingToJourney
  };
}

export function openAddToJourney(cardId) {
  return {
    type: actionTypes.OPEN_ADD_TO_JOURNEY_MODAL,
    cardId
  };
}

export function openInlineCreationModal(enabled) {
  return {
    type: actionTypes.OPEN_INLINE_CREATION_MODAL,
    enabled
  };
}

export function openFollowingUserListModal(typeOfModal) {
  return {
    type: actionTypes.OPEN_FOLLOW_USER_LIST_MODAL,
    typeOfModal
  };
}

export function openStatusModal(statusMessage, handleCloseModal, statusBlock) {
  return {
    type: actionTypes.OPEN_STATUS_MODAL,
    statusMessage,
    handleCloseModal,
    statusBlock
  };
}

export function openPathwayOverviewModal(
  card,
  logoObj,
  defaultImage,
  cardUpdated,
  openChannelModal,
  checkedCardId,
  channelSetting,
  deleteSharedCard,
  groupSetting,
  isStandaloneModal,
  dueAt,
  startDate
) {
  return {
    type: actionTypes.OPEN_PATHWAY_PREVIEW_MODAL,
    card,
    logoObj,
    defaultImage,
    cardUpdated,
    openChannelModal,
    checkedCardId,
    channelSetting,
    deleteSharedCard,
    groupSetting,
    isStandaloneModal,
    dueAt,
    startDate
  };
}

export function openJourneyOverviewModal(
  card,
  logoObj,
  defaultImage,
  cardUpdated,
  openChannelModal,
  checkedCardId,
  channelSetting,
  deleteSharedCard,
  groupSetting,
  currentSection,
  inStandalone,
  isStandaloneModal,
  dueAt,
  startDate
) {
  if (card && card.id) {
    return dispatch => {
      fetchCard(card.id)
        .then(cardRes => {
          dispatch({
            type: actionTypes.OPEN_JOURNEY_PREVIEW_MODAL,
            card: cardRes,
            logoObj,
            defaultImage,
            cardUpdated,
            openChannelModal,
            checkedCardId,
            channelSetting,
            deleteSharedCard,
            groupSetting,
            currentSection,
            inStandalone,
            isStandaloneModal,
            dueAt,
            startDate
          });
        })
        .catch(err => {
          console.error(`Error in modalActions.openJourneyOverviewModal.fetchCard.func: ${err}`);
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_JOURNEY_PREVIEW_MODAL,
      card,
      logoObj,
      defaultImage,
      cardUpdated,
      openChannelModal,
      checkedCardId,
      channelSetting,
      deleteSharedCard,
      groupSetting,
      currentSection,
      inStandalone,
      isStandaloneModal,
      dueAt,
      startDate
    };
  }
}

export function openJourneyOverviewModalV2(
  card,
  logoObj,
  defaultImage,
  cardUpdated,
  openChannelModal,
  checkedCardId,
  channelSetting,
  deleteSharedCard,
  groupSetting,
  currentSection,
  inStandalone,
  isStandaloneModal,
  dueAt,
  startDate
) {
  if (card && card.id) {
    return dispatch => {
      let payload = { is_standalone_page: false };
      fetchJourney(card.id, payload)
        .then(cardRes => {
          dispatch({
            type: actionTypes.OPEN_JOURNEY_PREVIEW_MODAL,
            card: cardRes,
            logoObj,
            defaultImage,
            cardUpdated,
            openChannelModal,
            checkedCardId,
            channelSetting,
            deleteSharedCard,
            groupSetting,
            currentSection,
            inStandalone,
            isStandaloneModal,
            dueAt,
            startDate
          });
        })
        .catch(err => {
          console.error(`Error in modalActions.openJourneyOverviewModalV2.fetchCard.func: ${err}`);
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_JOURNEY_PREVIEW_MODAL,
      card,
      logoObj,
      defaultImage,
      cardUpdated,
      openChannelModal,
      checkedCardId,
      channelSetting,
      deleteSharedCard,
      groupSetting,
      currentSection,
      inStandalone,
      isStandaloneModal,
      dueAt,
      startDate
    };
  }
}

export function openSmartBiteOverviewModal(
  card,
  logoObj,
  defaultImage,
  cardUpdated,
  openChannelModal,
  channelSetting,
  deleteSharedCard,
  groupSetting,
  dueAt,
  startDate
) {
  if (card && card.id) {
    return dispatch => {
      fetchCardSecuredUrls(card.id)
        .then(resp => {
          dispatch({
            type: actionTypes.OPEN_SMARTBITE_PREVIEW_MODAL,
            card: { ...card, ...resp },
            logoObj,
            defaultImage,
            cardUpdated,
            openChannelModal,
            channelSetting,
            dueAt,
            deleteSharedCard,
            groupSetting,
            startDate
          });
        })
        .catch(err => {
          console.error(
            `Error in modalActions.openSmartBiteOverviewModal.fetchCardSecuredUrls.func: ${err}`
          );
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_SMARTBITE_PREVIEW_MODAL,
      card,
      logoObj,
      defaultImage,
      cardUpdated,
      openChannelModal,
      channelSetting,
      dueAt,
      deleteSharedCard,
      groupSetting,
      startDate
    };
  }
}

export function openSmartBiteCreationModal(card, cardUpdated) {
  if (card && card.id) {
    return dispatch => {
      fetchCardForEdit(card.id)
        .then(cardRes => {
          let mergeCardRes = { ...card, ...cardRes };
          dispatch({
            type: actionTypes.OPEN_SMARTBITE_CREATION_MODAL,
            card: mergeCardRes,
            cardUpdated
          });
        })
        .catch(err => {
          console.error(`Error in modalActions.openSmartBiteCreationModal.fetchCard.func: ${err}`);
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_SMARTBITE_CREATION_MODAL,
      card,
      cardUpdated
    };
  }
}

export function openPathwayCreationModal(card, cardUpdated) {
  return {
    type: actionTypes.OPEN_PATHWAY_CREATION_MODAL,
    card,
    cardUpdated
  };
}

export function openJourneyCreationModal(card, cardUpdated) {
  if (card && card.id) {
    return dispatch => {
      fetchCard(card.id)
        .then(cardRes => {
          dispatch({
            type: actionTypes.OPEN_JOURNEY_CREATION_MODAL,
            card: cardRes,
            cardUpdated
          });
        })
        .catch(err => {
          console.error(`Error in modalActions.openJourneyCreationModal.fetchCard.func: ${err}`);
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_JOURNEY_CREATION_MODAL,
      card,
      cardUpdated
    };
  }
}

export function openReasonModal(card) {
  return {
    type: actionTypes.OPEN_REASON_MODAL,
    card
  };
}

export function openMDPModal(card) {
  return {
    type: actionTypes.OPEN_MDP_MODAL,
    card
  };
}

export function openCommentReasonModal(comment) {
  return {
    type: actionTypes.OPEN_COMMENT_REASON_MODAL,
    comment
  };
}

export function openInviteUserModal(groupInfo) {
  return {
    type: actionTypes.OPEN_INVITE_USER_MODAL,
    groupInfo: groupInfo
  };
}

export function openInviteV2UserModal() {
  return {
    type: actionTypes.OPEN_INVITE_V2_MODAL
  };
}

export function openUpdateInterestsModal(callback) {
  return {
    type: actionTypes.OPEN_UPDATE_INTERESTS_MODAL,
    callback
  };
}

export function openUpdateInterestsOnboardV2Modal(callback) {
  return {
    type: actionTypes.OPEN_UPDATE_INTERESTS_ONBOARD_V2_MODAL,
    callback
  };
}

export function openUpdateMultilevelInterestsModal() {
  return {
    type: actionTypes.OPEN_UPDATE_MULTILEVEL_INTERESTS_MODAL
  };
}

export function openUpdateMultilevelExpertiseModal() {
  return {
    type: actionTypes.OPEN_UPDATE_MULTILEVEL_EXPERTISE_MODAL
  };
}

export function openUpdateExpertiseModal(callback) {
  return {
    type: actionTypes.OPEN_UPDATE_EXPERTISE_MODAL,
    callback
  };
}

export function openInsightEditModal(card, cardUpdated) {
  return {
    type: actionTypes.OPEN_INSIGHT_EDIT_MODAL,
    card,
    cardUpdated
  };
}

export function openUploadImageModal(imageType, sizes, updatedImage) {
  return {
    type: actionTypes.OPEN_UPLOAD_IMAGE_MODAL,
    imageType,
    sizes,
    updatedImage
  };
}

export function openChannelCardsModal(
  modalTitle,
  count,
  cardsData,
  channel,
  editMode,
  cardModalDetails = {}
) {
  return {
    type: actionTypes.OPEN_CHANNEL_CARDS_MODAL,
    modalTitle,
    count,
    cards: cardsData,
    channel,
    editMode,
    cardModalDetails
  };
}

export function openTeamCardsModal(
  deleteSharedCard = null,
  groupCards = [],
  cardModalDetails = {},
  group = {},
  isViewAll = null
) {
  return {
    type: actionTypes.OPEN_TEAM_CARD_MODAL,
    deleteSharedCard: deleteSharedCard,
    cards: groupCards,
    cardModalDetails,
    group,
    isViewAll
  };
}

export function openGroupMemberModal(userType, isInitilized) {
  return {
    type: actionTypes.OPEN_GROUP_MEMBER_MODAL,
    userType,
    isInitilized
  };
}

export function openAddCuratorModal(channelId, curators, currentUserId) {
  return {
    type: actionTypes.OPEN_ADD_CURATORS_MODAL,
    channelId,
    curators,
    currentUserId
  };
}

export function openGroupInviteModal(userType) {
  return {
    type: actionTypes.OPEN_GROUP_INVITE_MODAL,
    userType
  };
}

export function openUpdateChannelDescriptionModal(id, label, description) {
  return {
    type: actionTypes.OPEN_UPDATE_CHANNEL_DETAILS_MODAL,
    id,
    label,
    description
  };
}

export function openUpdateGroupDescriptionModal() {
  return {
    type: actionTypes.OPEN_UPDATE_GROUP_DETAILS_MODAL
  };
}

export function openCardRemovalConfirmation(card, channel, cardTypes, isReject, rejectCallback) {
  return {
    type: actionTypes.OPEN_CHANNEL_CARD_REMOVE_MODAL,
    card,
    channel,
    cardTypes,
    isReject,
    rejectCallback
  };
}

export function updateDataBefore(modalTitle, count, cardsData, channel, editMode, offset) {
  return {
    type: actionTypes.UPDATE_DATA_IN_MODAL,
    modalTitle,
    count,
    cards: cardsData,
    channel,
    editMode
  };
}

export function openCongratulationModal(userBadge) {
  return {
    type: actionTypes.OPEN_PATHWAY_CONGRATULATION_MODAL,
    userBadge
  };
}

export function openRelevancyRatingModal(card) {
  return {
    type: actionTypes.OPEN_RELEVANCY_RATING_MODAL,
    card
  };
}

export function openShareModal(card) {
  return {
    type: actionTypes.OPEN_SHARE_MODAL,
    card
  };
}

export function updateDynamicSelectionFilters(filter) {
  return {
    type: actionTypes.UPDATE_DYNAMIC_SELECTION_FILTERS,
    filter
  };
}

export function updateDynamicSelectionFiltersV2(filter, currentAction) {
  return {
    type: actionTypes.UPDATE_DYNAMIC_SELECTION_FILTERS_V2,
    filter,
    currentAction
  };
}

export function openPostToChannelModal(card) {
  return {
    type: actionTypes.OPEN_POST_TO_CHANNEL_MODAL,
    card
  };
}

export function openShowChannelsModal(removable) {
  return {
    type: actionTypes.OPEN_SHOW_CHANNELS_MODAL,
    removable
  };
}
export function openSkillCreationModal(skill) {
  return {
    type: actionTypes.OPEN_SHOW_SKILL_MODAL,
    skill: skill
  };
}

export function openPaymentModal(card) {
  return {
    type: actionTypes.OPEN_PAYMENT_MODAL
  };
}

export function openWalletPaymentModal(rechargeData) {
  return {
    type: actionTypes.OPEN_WALLET_PAYMENT_MODAL,
    rechargeData: rechargeData
  };
}

export function openCardWalletPaymentModal(paymentData) {
  return {
    type: actionTypes.OPEN_CARD_WALLET_PAYMENT_MODAL,
    paymentData: paymentData
  };
}

export function openCourseModal(courseStatus, courseType) {
  return {
    type: actionTypes.OPEN_COURSES_MODAL,
    courseStatus,
    courseType
  };
}

export function openPayPalSuccessModal(data) {
  return {
    type: actionTypes.OPEN_PAYPAL_SUCCESS_MODAL,
    paymentData: data
  };
}

export function openShareContentModal(card) {
  return {
    type: actionTypes.OPEN_SHARE_CONTENT_MODAL,
    card
  };
}

export function openChangeAuthorModal(
  card,
  removeCardFromList,
  changeAuthorOptionRemoval,
  removeCardFromCardContainer,
  cardSectionName
) {
  return {
    type: actionTypes.OPEN_CHANGE_AUTHOR_MODAL,
    card,
    removeCardFromList,
    changeAuthorOptionRemoval,
    removeCardFromCardContainer,
    cardSectionName
  };
}

export function openChannelEditModal(channel) {
  return {
    type: actionTypes.OPEN_CHANNEL_EDIT_MODAL,
    channel
  };
}
export function openSlideOutCardModal(
  card,
  authorName,
  cardUpdated,
  defaultImage,
  logoObj,
  dismissible,
  ratingCount,
  handleCardClicked,
  standaloneLinkClickHandler,
  rateCard,
  providerLogos
) {
  if (card && card.id) {
    return dispatch => {
      fetchCardSecuredUrls(card.id)
        .then(resp => {
          dispatch({
            type: actionTypes.OPEN_SLIDE_OUT_CARD_MODAL,
            card: { ...card, ...resp },
            authorName,
            cardUpdated,
            defaultImage,
            logoObj,
            dismissible,
            ratingCount,
            handleCardClicked,
            standaloneLinkClickHandler,
            rateCard,
            providerLogos
          });
        })
        .catch(err => {
          console.error(
            `Error in modalActions.openSmartBiteOverviewModal.fetchCardSecuredUrls.func: ${err}`
          );
        });
    };
  } else {
    return {
      type: actionTypes.OPEN_SLIDE_OUT_CARD_MODAL,
      card,
      authorName,
      cardUpdated,
      defaultImage,
      logoObj,
      dismissible,
      ratingCount,
      handleCardClicked,
      standaloneLinkClickHandler,
      rateCard,
      providerLogos
    };
  }
}
export function openUnbookmarkModal(bookmarked, confirmHandler) {
  return {
    type: actionTypes.OPEN_UNBOOKMARK_MODAL,
    bookmarked,
    confirmHandler
  };
}

export function openSkillsDirectoryModal(changeRole) {
  return {
    type: actionTypes.OPEN_SKILLS_DIRECTORY_MODAL,
    changeRole
  };
}

export const openChannelCurateCardModal = () => {
  return {
    type: actionTypes.OPEN_CHANNEL_CURATE_CARDS_MODAL
  };
};

export const openChannelCarouselCardsModal = payload => {
  return {
    type: actionTypes.OPEN_CHANNEL_CAROUSEL_CARDS_MODAL,
    carouselCards: payload.carouselCards,
    carouselData: payload.carouselData,
    totalCarouselCardsCount: payload.totalCarouselCardsCount,
    showChannelCardConfig: payload.showChannelCardConfig
  };
};
