import * as actionTypes from '../constants/actionTypes';
import { getDetails } from 'edc-web-sdk/requests/orgSettings';
import { DEFAULT_ORG_SETTING } from 'edc-web-sdk/requests/orgSettings.v2';
import { jwtExpiredInstance } from 'edc-web-sdk/helpers/jwtExpired';

export function getOrgInfo() {
  return dispatch => {
    return getDetails().then(
      data => {
        let webSessionConfig = data.configs.find(item => item.name === 'web_session_timeout');
        let webSessionTimeout = webSessionConfig && webSessionConfig.value;
        if (webSessionTimeout) {
          jwtExpiredInstance.mSeconds = Date.now() + webSessionTimeout * 60 * 1000;
        }
        let OrgConfig = {};
        let title = document.querySelector('title');
        title.innerText = data.name;
        window.__GATrackingOrganizationObject__ = data;

        data.configs.some(config => {
          if (config.name == 'OrgCustomizationConfig') {
            try {
              OrgConfig = JSON.parse(config.value);
            } catch (e) {
              OrgConfig = config.value;
            }
            return true;
          }
        });

        const VERSION = parseFloat(OrgConfig.version);

        if (parseFloat(OrgConfig.version) < parseFloat(DEFAULT_ORG_SETTING.version)) {
          // We need to ensure proper merging if possible
          Object.keys(DEFAULT_ORG_SETTING).forEach(key => {
            if (key != 'version') {
              OrgConfig[key] = Object.assign({}, DEFAULT_ORG_SETTING[key], OrgConfig[key]);
            }
          });
        } else {
          OrgConfig = Object.assign({}, DEFAULT_ORG_SETTING, OrgConfig);
        }

        // During migration need to move old configs to new configs
        // We ignore mobile discover config
        if (VERSION < 3 && OrgConfig.discover && OrgConfig.web.discover) {
          Object.keys(OrgConfig.web.discover).forEach(key => {
            let newKey = key.replace('web/', '');
            OrgConfig.discover[newKey] = Object.assign(
              {},
              OrgConfig.discover[newKey] || {},
              OrgConfig.web.discover[key]
            );
          });
        }

        // Migrate feed
        if (VERSION < 4 && OrgConfig.feed && OrgConfig.web.feed) {
          // Overwrite with web feed
          Object.keys(OrgConfig.web.feed).forEach(key => {
            let newKey = key.replace('web/', '');
            if (newKey == 'feed/required') {
              newKey = 'feed/myAssignments'; // Renamed key to match mobile
            }
            OrgConfig.feed[newKey] = Object.assign(
              {},
              OrgConfig.feed[newKey] || {},
              OrgConfig.web.feed[key]
            );
          });
        }
        if (OrgConfig && OrgConfig.web) {
          OrgConfig.web['profileShowUserContent'] =
            OrgConfig.web.profileShowUserContent === undefined
              ? true
              : OrgConfig.web.profileShowUserContent;
        }
        data['OrgConfig'] = OrgConfig;

        dispatch({
          type: actionTypes.RECEIVE_ORG_INFO,
          data
        });
      },
      error => {
        dispatch({
          type: actionTypes.ERROR_INIT_ORG,
          error
        });
      }
    );
  };
}

export function changeFeedCardView(viewRegime, feedKey) {
  return dispatch => {
    dispatch({
      type: actionTypes.RECEIVE_FEED_TYPE_VIEW,
      viewRegime,
      feedKey
    });
  };
}
