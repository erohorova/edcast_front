import * as actionTypes from '../constants/actionTypes';
import { curateItem as editContentState } from 'edc-web-sdk/requests/curate';
export function curateItem(cardId, channelId, channelName) {
  let publishMessage = `Published to ${channelName}.`;

  return dispatch => {
    return editContentState('curate', [cardId], channelId).then(
      () => {
        dispatch({
          type: actionTypes.RECEIVE_CURATE_CHANGE_STATE,
          cardId
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: publishMessage,
          autoClose: true
        });
      },
      () => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Failed to publish. Please try again.',
          autoClose: true
        });
      }
    );
  };
}
export function skipItem(cardId, channelId) {
  return dispatch => {
    return editContentState('skip', [cardId], channelId).then(() => {
      dispatch({
        type: actionTypes.RECEIVE_CURATE_CHANGE_STATE,
        cardId
      });
    });
  };
}
