import * as actionTypes from '../constants/actionTypes';

window.UPSHOTEVENT = Object.freeze({
  LOGIN: 1,
  REGISTER: 2,
  FORGOT_PASSWORD: 3,
  TERMS_CONDITIONS: 4,
  SKILL: 5,
  SEARCH: 6,
  FEATURED_CONTENT_PROVIDER: 7,
  SMARTCARD: 8,
  UNFOLLOW: 9,
  FOLLOW: 10,
  CLICK_EXPLORE_LEARN: 11,
  ASSIGNED_LEARNING: 12,
  BUY: 13,
  LEARNING_GOALS: 14,
  SKILL_COINS_PURCHASE: 15
});

export function updateUpshotUserProfile(properties_payload) {
  if (window.upshot) {
    let currentProfile = window.upshot.getCurrentProfile();
    Object.keys(currentProfile).forEach(function(key1) {
      Object.keys(properties_payload).forEach(function(key) {
        if (key == key1 && properties_payload[key] == currentProfile[key1])
          delete properties_payload[key];
      });
    });
    window.upshot.updateUserProfile(properties_payload);
  }
}

export function sendCustomEvent(event, payload) {
  if (window.upshot) {
    var eventId = window.upshot.createEvent('UpshotEventCustom', payload, event, false);
    window.upshot.closeEventForID(eventId);
  }
}
