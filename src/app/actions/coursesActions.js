import * as actionTypes from '../constants/actionTypes';
import { courses, edcastcloud } from 'edc-web-sdk/requests';
import * as coursesv2 from 'edc-web-sdk/requests/courses.v2';

export function recommendedCourses(limit, offset, includeEnrolled, query) {
  return dispatch => {
    return edcastcloud
      .recommendedCourses(limit, offset, includeEnrolled, query)
      .then(coursesData => {
        dispatch({
          type: actionTypes.RECEIVE_DISCOVERY_COURSES,
          courses: coursesData
        });
        return coursesData;
      });
  };
}

export function getCourses(limit, offset, includeEnrolled, query) {
  return dispatch => {
    return courses.getCourses(limit, offset, includeEnrolled, query).then(coursesData => {
      dispatch({
        type: actionTypes.RECEIVE_COURSES,
        courses: coursesData
      });
      return coursesData;
    });
  };
}

export function getCoursesV2(userId) {
  return dispatch => {
    return coursesv2.getCoursesV2({ userId }).then(coursesData => {
      return coursesData;
    });
  };
}
