import * as actionTypes from '../constants/actionTypes';
import { sociative } from 'edc-web-sdk/requests';

export function checkRegistration(email, coachEddyUrl) {
  return dispatch => {
    return sociative.checkRegistration(email, coachEddyUrl).then(status => {
      dispatch({
        type: actionTypes.RECEIVE_SOCIATIVE_REGISTRATION_STATUS,
        status
      });
    });
  };
}
