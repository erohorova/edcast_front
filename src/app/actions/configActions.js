import * as actionTypes from '../constants/actionTypes';

export function toggleTopNav(bool) {
  return dispatch => {
    dispatch({
      type: actionTypes.TOGGLE_TOPNAV,
      topNavToggle: bool
    });
  };
}
