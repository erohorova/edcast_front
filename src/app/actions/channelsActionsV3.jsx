import {
  SAVE_CHANNEL_DETAILS,
  SAVE_CHANNEL_FEATURED_CARDS,
  REMOVE_CHANNEL_FEATURED_CARDS,
  ADD_FEATURED_CARD_IN_CHANNEL,
  REMOVE_FEATURED_CARD_IN_CHANNEL,
  REMOVE_CARD_FROM_CHANNEL_CAROUSEL
} from '../constants/actionTypes';

export const saveChannelDetails = payload => {
  return {
    type: SAVE_CHANNEL_DETAILS,
    payload
  };
};

export const saveFeaturedCards = payload => {
  return {
    type: SAVE_CHANNEL_FEATURED_CARDS,
    payload
  };
};

export const removeFeaturedCards = () => {
  return {
    type: REMOVE_CHANNEL_FEATURED_CARDS
  };
};

export const addFeaturedCardInChannel = payload => {
  return {
    type: ADD_FEATURED_CARD_IN_CHANNEL,
    payload
  };
};

export const removeFeaturedCardInChannel = payload => {
  return {
    type: REMOVE_FEATURED_CARD_IN_CHANNEL,
    payload
  };
};

export const removeCardFromChannelCarousel = payload => {
  return {
    type: REMOVE_CARD_FROM_CHANNEL_CAROUSEL,
    payload
  };
};
