import * as actionTypes from '../constants/actionTypes';
import { journeys } from 'edc-web-sdk/requests';

export function getJourneys(payload) {
  return dispatch => {
    return journeys.getJourneys(payload).then(journeysParam => {
      dispatch({
        type: actionTypes.RECEIVE_JOURNEYS,
        journeys: journeysParam
      });
      return journeysParam;
    });
  };
}

export function saveTempJourney(card) {
  return {
    type: actionTypes.SAVE_TEMP_JOURNEY,
    card
  };
}

export function deleteTempJourney() {
  return {
    type: actionTypes.DELETE_TEMP_JOURNEY
  };
}

export function saveConsumptionJourney(card) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_JOURNEY,
    card
  };
}

export function saveConsumptionJourneyHistoryURL(url) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_JOURNEY_HISTORY_URL,
    url
  };
}

export function saveConsumptionJourneyOpenBlock(openBlock) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_JOURNEY_OPEN_BLOCK,
    openBlock
  };
}

export function saveConsumptionJourneyPathwayPayload(payload) {
  return {
    type: actionTypes.SAVE_CONSUMPTION_JOURNEY_PATHWAY_PAYLOAD,
    payload
  };
}

export function consumptionJourneyHistory(card) {
  return {
    type: actionTypes.CONSUMPTION_JOURNEY_HISTORY,
    card
  };
}

export function removeConsumptionJourney() {
  return {
    type: actionTypes.REMOVE_CONSUMPTION_JOURNEY
  };
}

export function removeConsumptionJourneyHistory() {
  return {
    type: actionTypes.REMOVE_CONSUMPTION_JOURNEY_HISTORY
  };
}

export function saveReorderCardIds(cardsIds) {
  return {
    type: actionTypes.SAVE_REORDER_CARD_IDS_JOURNEY,
    cardsIds
  };
}

export function removeReorderCardIds() {
  return {
    type: actionTypes.REMOVE_REORDER_CARD_IDS_JOURNEY
  };
}

export function isPreviewMode(bool) {
  return {
    type: actionTypes.IS_PREVIEW_MODE_JOURNEY,
    bool
  };
}
