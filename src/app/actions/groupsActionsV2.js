import * as actionTypes from '../constants/actionTypes';
import { groupsV2 } from 'edc-web-sdk/requests/index';
import { toggleTeamAdmin } from 'edc-web-sdk/requests/teams';
export function getGroups(limit, offset) {
  return dispatch => {
    return groupsV2.getList(limit, offset).then(groups => {
      dispatch({
        type: actionTypes.RECEIVE_GROUPS,
        groups
      });
      return groups.map(group => group.id + '');
    });
  };
}

export function getGroupDetail(slug) {
  return dispatch => {
    return groupsV2.getGroupDetails(slug).then(groupDetail => {
      let groups = [groupDetail];
      dispatch({
        type: actionTypes.RECEIVE_GROUPS,
        groups
      });
      return groupDetail;
    });
  };
}

export function addPendingMember(pendingMember) {
  return dispatch => {
    dispatch({
      type: actionTypes.ADD_PENDING_MEMBER,
      pendingMember
    });
  };
}

export function updateCardsModal(cardModalDetails) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_CARD_MODAL,
      desc: cardModalDetails.desc,
      count: cardModalDetails.count,
      carouselCardType: cardModalDetails.type,
      name: cardModalDetails.name
    });
  };
}

export function initializedGroupUser(payload) {
  return dispatch => {
    if (payload.user_type && payload.user_type === 'pending') {
      dispatch({
        type: actionTypes.RECEIVE_GROUP_PENDING,
        pending: { users: [] },
        limit: payload.limit,
        offset: payload.offset,
        isInitialized: true
      });
    } else {
      dispatch({
        type: actionTypes.RECEIVE_GROUP_MEMBERS,
        members: { users: [] },
        limit: payload.limit,
        offset: payload.offset,
        isInitialized: true
      });
    }
  };
}

export function getGroupUsers(groupID, payload) {
  return dispatch => {
    return groupsV2.getUsersInAGroupV2(groupID, payload).then(membersData => {
      if (payload.user_type && payload.user_type === 'pending') {
        dispatch({
          type: actionTypes.RECEIVE_GROUP_PENDING,
          pending: membersData.pending,
          limit: payload.limit,
          offset: payload.offset
        });
      } else {
        dispatch({
          type: actionTypes.RECEIVE_GROUP_MEMBERS,
          members: membersData.members,
          limit: payload.limit,
          offset: payload.offset
        });
      }
    });
  };
}

export function getTeamCards(groupID, cardAryName, payload, multilingualFilter) {
  if (multilingualFilter) {
    payload.filter_by_language = true;
  }
  return dispatch => {
    return groupsV2.getTeamCards(groupID, payload).then(cards => {
      dispatch({
        type: actionTypes.RECEIVE_GROUP_CARDS,
        cards,
        offset: payload.offset,
        cardAryName
      });
    });
  };
}

export function getFeaturedCards(payload, cardAryName) {
  return dispatch => {
    return groupsV2.fetchPinnedcards(payload).then(pins => {
      dispatch({
        type: actionTypes.RECEIVE_FEATURED_GROUP_CARDS,
        pins,
        offset: payload.offset
      });
    });
  };
}

export function getTeamCardsv2(groupID, cardAryName, payload, multilingualFilter) {
  if (multilingualFilter) {
    payload.filter_by_language = true;
  }
  return groupsV2.getTeamCards(groupID, payload).then(cards => {
    return cards;
  });
}

export function deleteSharedCard(groupID, payload) {
  return dispatch => {
    return groupsV2.removeSharedCardFromGroupV2(groupID, payload).then(groupDetail => {
      let cardID = payload.card_id;
      dispatch({
        type: actionTypes.REMOVE_SHARED_CARD,
        cardID
      });
    });
  };
}

export function deleteCard(cardID, cardArrayName) {
  return dispatch => {
    dispatch({
      type: actionTypes.DELETE_TEAM_CARD,
      cardID,
      cardArrayName
    });
  };
}

export function getTeamChannels(groupID, payload, isAfterRemove) {
  return dispatch => {
    return groupsV2.getTeamChannels(groupID, payload).then(channelObj => {
      let channels = channelObj.channels;
      // let groupIDData = channelObj.team;
      let total = channelObj.total;
      let offset = payload.offset;
      dispatch({
        type: actionTypes.GET_TEAM_CHANNELS,
        channels,
        total,
        offset,
        isAfterRemove
      });
    });
  };
}

export function promoteDemote(teamID, UserID, showSnackbar = false) {
  return dispatch => {
    return (
      toggleTeamAdmin(teamID, UserID)
        .then(data => {
          dispatch({
            type: actionTypes.UPDATE_MEMBER,
            member: data.user
          });
          if (showSnackbar) {
            dispatch({
              type: actionTypes.OPEN_SNACKBAR,
              message:
                data.user.role === 'admin' ? 'Promoted to Group Leader' : 'Demoted to Member',
              autoClose: true
            });
          }
        })
        /*eslint handle-callback-err: "off"*/
        .catch(err => {
          dispatch({
            type: actionTypes.OPEN_SNACKBAR,
            message: 'Error Updating!',
            autoClose: true
          });
        })
    );
  };
}

export function invite(info, groupUpdate) {
  let payload = {
    emails: info.emails,
    roles: info.roles
  };

  return dispatch => {
    return groupsV2.invite(info.id, payload).then(data => {
      dispatch({
        type: actionTypes.ADD_PENDING_MEMBER,
        pendingMember: info.userList
      });

      if (info.roles === 'admin') {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: "Find the group's non-members in pending list",
          autoClose: true
        });
      } else if (!groupUpdate) {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Your invitation has been sent!',
          autoClose: true
        });
      }
    });
  };
}

export function updateGroup(teamId, payload) {
  return dispatch => {
    return groupsV2.updateGroup(teamId, payload).then(group => {
      dispatch({
        type: actionTypes.UPDATE_GROUP_DETAILS,
        group
      });
    });
  };
}

export function updateMembers(info) {
  let payload = {
    role: info.role,
    user_ids: info.ids
  };
  return dispatch => {
    return groupsV2
      .addTeamMembers(info.groupID, payload)
      .catch(err => console.error(`Error in updateMembers.func : ${err}`));
  };
}

export function updateGroupExtended(teamId, payload) {
  return dispatch => {
    return groupsV2.updateGroup(teamId, payload).then(group => {
      dispatch({
        type: actionTypes.UPDATE_GROUP_DETAILS_V2,
        group
      });
    });
  };
}

export function updateGroupCarouselsOrder(teamId, carouselOrderChange) {
  return {
    type: actionTypes.UPDATE_GROUP_CAROUSEL_ORDER,
    carouselOrderChange
  };
}

export function deleteUserFromTeam(payload) {
  return dispatch => {
    let idArray;
    if (Array.isArray(payload.id)) {
      idArray = payload.id;
    } else {
      idArray = [payload.id];
    }
    return groupsV2
      .deleteUsersFromTeam(
        { user_ids: idArray, is_pending: payload.isPending || false },
        payload.teamID
      )
      .then(response => {
        dispatch({
          type: actionTypes.REMOVE_GROUP_MEMBER,
          removedUser: payload
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: `User${
            Array.isArray(payload.id) && payload.id.length > 1 ? 's' : ''
          } successfully removed from Group`,
          autoClose: true
        });
      });
  };
}

export function updatePendingMember(id, isFollowed) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_PENDING_MEMBER,
      id,
      isFollowed
    });
  };
}

export function addTeamUsers(info) {
  let payload = {
    role: info.role,
    user_ids: info.ids
  };
  return dispatch => {
    return groupsV2
      .addTeamMembers(info.groupID, payload)
      .then(data => {
        let { ids, groupID, role, newUsers } = info;
        dispatch({
          type: actionTypes.UPDATE_GROUP_USERS,
          ids,
          groupID,
          role,
          newUsers
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Users successfully added',
          autoClose: true
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function deleteTeamUsers(info) {
  let payload = {
    role: info.role,
    user_ids: info.ids
  };
  return dispatch => {
    return groupsV2
      .deleteTeamUsers(info.groupID, payload)
      .then(data => {
        let { ids, groupID, role } = info;
        dispatch({
          type: actionTypes.UPDATE_GROUP_USERS,
          ids,
          groupID,
          role
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Users successfully removed',
          autoClose: true
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function acceptInviteToGroup(teamId) {
  return dispatch => {
    return groupsV2
      .acceptInviteToGroup(teamId)
      .then(data => {
        dispatch({
          type: actionTypes.UPDATE_GROUP_INFORMATION,
          team: data
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function declineInviteToGroup(teamId) {
  return dispatch => {
    return groupsV2
      .declineInviteToGroup(teamId)
      .then(data => {
        dispatch({
          type: actionTypes.UPDATE_GROUP_INFORMATION,
          team: data
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function joinToGroup(teamId) {
  return dispatch => {
    return groupsV2
      .joinToGroup(teamId)
      .then(data => {
        dispatch({
          type: actionTypes.UPDATE_GROUP_INFORMATION,
          team: data
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function leaveFromGroup(teamId) {
  return dispatch => {
    return groupsV2
      .leaveFromGroup(teamId)
      .then(data => {
        dispatch({
          type: actionTypes.UPDATE_GROUP_INFORMATION,
          team: data
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function deletePendingUser(id, teamId) {
  return dispatch => {
    let payload = { user_ids: [id], is_pending: true };
    return groupsV2
      .deleteUsersFromTeam(payload, teamId)
      .then(() => {
        dispatch({
          type: actionTypes.REMOVE_PENDING_GROUP_MEMBER,
          id
        });
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Pending user successfully removed from Group',
          autoClose: true
        });
      })
      .catch(err => {
        dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };
}

export function fetchPinnedCardsv2(payload, cardAryName) {
  return dispatch => {
    return groupsV2.fetchPinnedcards(payload, cardAryName).then(pins => {
      dispatch({
        type: actionTypes.RECEIVE_PINNED_CARDS_GROUP,
        pins: pins.pins || [],
        offset: payload.offset
      });
    });
  };
}

export function pinUnpinCardv2(payload, pinnedStatus, cardTypes, card) {
  if (pinnedStatus) {
    return dispatch => {
      return groupsV2.pinCard(payload).then(() => {
        dispatch({
          type: actionTypes.ADD_TO_PINNED_CARD_CAROUSEL_GROUP,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  } else {
    return dispatch => {
      return groupsV2.unpinCard(payload).then(() => {
        dispatch({
          type: actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL_GROUP,
          payload,
          pinnedStatus,
          cardTypes,
          card
        });
      });
    };
  }
}
