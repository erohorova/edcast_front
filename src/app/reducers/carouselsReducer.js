import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function carouselsReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_CAROUSELS:
      newState = state;
      newState = newState.set('carousels', action.carousels.structures || action.carousels);
      return newState;
    case actionTypes.RECEIVE_CAROUSEL_ITEMS:
      newState = state;
      let carousels = [];
      newState.toJS().carousels.forEach(carousel => {
        if (carousel.id == action.items.structureId) {
          carousel['items'] = action.items.structuredItems.map(item => {
            return item.entity;
          });
        }
        carousels.push(carousel);
      });
      newState = newState.set('carousels', carousels);
      return newState;
    default:
      return state;
  }
}
