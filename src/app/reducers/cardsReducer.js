/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import { tr } from 'edc-web-sdk/helpers/translations';
import Immutable from 'immutable';

export default function cardsReducer(state = Immutable.Map(), action) {
  let newState, newCard, newComments;
  switch (action.type) {
    case actionTypes.RECEIVE_CARDS:
    case actionTypes.RECEIVE_CURATE_CARDS:
      newState = state;
      action.cards.forEach(card => {
        newCard = Immutable.fromJS(card);
        newComments = Immutable.List();
        if (newCard.get('comments')) {
          newComments = newCard.get('comments').map(comment => {
            return comment.set('userId', comment.getIn(['user', 'id']) + '').delete('user');
          });
        }
        newCard = newCard.set('comments', newComments);
        newState = newState.set(card.id, newCard);
      });
      return newState;
    case actionTypes.RECEIVE_PROMOTED_FEED:
      newState = state;
      action.feed.forEach(section => {
        if (section.type !== 'Promotion') {
          let rationale = section.title;
          section.items.forEach(card => {
            if (section.title == 'Content from channels you are following' && card.channels) {
              rationale =
                tr('Because you are following ') + card.channels[0].label + ` ${tr('channel')}`;
            }
            card.rationale = rationale;
            newCard = Immutable.fromJS(card);
            newComments = Immutable.List();
            if (newCard.get('comments')) {
              newCard.get('comments').map(comment => {
                return comment.set('userId', comment.getIn(['user', 'id']) + '').delete('user');
              });
            }
            newCard = newCard.set('comments', newComments);
            newState = newState.set(card.id, newCard);
            if (card.author && card.author.id && card.author.handle) {
              newState = newState
                .setIn([card.id, 'authorId'], card.author.id + '')
                .deleteIn([card.id, 'author']);
            }
          });
        }
      });
      return newState;
    case actionTypes.RECEIVE_USER_ASSIGNMENTS:
      newState = state;
      action.assignments.forEach(assignment => {
        let card = assignment.assignable;
        newCard = Immutable.fromJS(card);
        newComments = Immutable.List();
        if (newCard.get('comments')) {
          newCard.get('comments').map(comment => {
            return comment.set('userId', comment.getIn(['user', 'id']) + '').delete('user');
          });
        }
        newCard = newCard.set('comments', newComments);
        newState = newState.set(card.id, newCard);
        if (card.author && card.author.id && card.author.handle) {
          newState = newState
            .setIn([card.id, 'authorId'], card.author.id + '')
            .deleteIn([card.id, 'author']);
        }
      });
      return newState;
    case actionTypes.REQUEST_COMMENTS_FOR_CARD:
      newState = state.updateIn([action.cardId, 'comments'], comments => {
        let timestamp = new Date().toISOString();
        return comments.push(
          Immutable.Map({
            userId: action.currentUserId,
            message: action.message,
            updatedAt: timestamp,
            createdAt: timestamp
          })
        );
      });
      return newState;
    case actionTypes.REQUEST_LIKE_COMMENT:
      newState = state;
      newState.getIn([action.cardId, 'comments']).forEach((comment, index) => {
        if (comment.get('id') === action.commentId) {
          newState = newState.setIn([action.cardId, 'comments', index, 'upVoted'], action.isLike);
        }
      });
      return newState;
    case actionTypes.RECEIVE_LIKE_COMMENT:
      newState = state;
      newState.getIn([action.cardId, 'comments']).forEach((comment, index) => {
        if (comment.get('id') === action.commentId) {
          newState = newState.setIn(
            [action.cardId, 'comments', index, 'votesCount'],
            Math.max(comment.get('votesCount') + (action.isLike ? 1 : -1), 0)
          );
        }
      });
      return newState;
    case actionTypes.DELETE_CARD_COMMENT:
      return state
        .updateIn([action.cardId, 'comments'], comments => {
          return comments.filter(comment => {
            return comment.get('id') !== action.commentId;
          });
        })
        .updateIn([action.cardId, 'commentsCount'], num => num - 1);
    case actionTypes.REQUEST_ATTEMPT_POLL_CARD:
      return state.setIn([action.cardId, 'attemptPending'], true);
    case actionTypes.REQUEST_BOOKMARK_CARD:
      return state.setIn([action.cardId, 'isBookmarked'], action.isBookmark);
    case actionTypes.RECEIVE_CARD:
      newCard = Immutable.fromJS(action.card);
      newState = state.set(action.card.id, newCard);
      return newState;
    case actionTypes.RECEIVE_ATTEMPT_POLL_CARD:
      newState = state
        .setIn([action.cardId, 'hasAttempted'], true)
        .setIn([action.cardId, 'attemptPending'], false)
        .updateIn([action.cardId, 'attemptCount'], value => value + 1);
      newState.getIn([action.cardId, 'options']).forEach((option, index) => {
        if (option.get('id') === action.optionId) {
          newState = newState.updateIn(
            [action.cardId, 'options', index, 'count'],
            value => value + 1
          );
        }
      });
      return newState;
    case actionTypes.CHANGE_CARD_STATE:
      return state
        .setIn([action.cardId, 'completionState'], 'completed')
        .set('completedCard', action.cardId);
    case actionTypes.RECEIVE_DELETE_CARD:
      return state.setIn([action.cardId, 'dismissed'], true);
    case actionTypes.RECEIVE_DISMISS_CARD:
      return state.delete(action.cardId);
    case actionTypes.RECEIVE_CLEAR_CARD:
      return Immutable.Map();
    default:
      return state;
  }
}
