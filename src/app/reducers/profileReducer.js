import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import { uniqBy, differenceBy } from 'lodash';

const INIT = {
  userActivities: null,
  integrationList: [],
  externalCourses: [],
  inProgressArray: [],
  inProgressCount: 0,
  dashboardInfo: [
    {
      name: 'Learning Hours',
      visible: false
    },
    {
      name: 'My Badges',
      visible: false
    },
    {
      name: 'In Progress',
      visible: false
    },
    // {
    //   "name": "Active Topics",
    //   "visible": false
    // },
    {
      name: 'Top Content',
      visible: false
    },
    {
      name: 'Activity Stream',
      visible: false
    }
  ]
};

export default function profileReducer(state = Immutable.fromJS(INIT), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_INIT_PROFILE:
      return state;
    case actionTypes.RECEIVE_PROFILE_BADGES:
      return state
        .set('userCardBadges', action.userCardBadges)
        .set('userClcBadges', action.userClcBadges);
    case actionTypes.RECEIVE_PROFILE_SKILLS:
      return state;
    case actionTypes.RECEIVE_INTEGRATION:
      newState = state;
      newState = newState.set('integrationList', action.list);
      return newState;

    case actionTypes.INTEGRATION_CONNECT_STATUS:
      newState = state;
      let integrationList = newState.get('integrationList');
      let index = newState.get('integrationList').findIndex(i => i.id === action.integration.id);
      integrationList[index].connected = !this.state.integrationList[index].connected;
      newState = newState.set('integrationList', integrationList);
      return newState;

    case actionTypes.RECEIVE_PROFILE_COURSES:
      newState = state;
      newState = newState.set('externalCourses', action.externalCourses);
      return newState;

    case actionTypes.RECEIVE_PUBLIC_PROFILE:
      newState = state;
      newState = newState
        .set('externalCourses', action.data.externalCourses)
        .set('profile', action.data.profile)
        .set('skills', action.data.skills)
        .set('userCardBadges', action.data.userCardBadges)
        .set('userClcBadges', action.data.userClcBadges);
      return newState;

    case actionTypes.RECEIVE_USER_ACTIVITIES:
      newState = state;
      newState = newState.set('userActivities', action.userActivities);
      return newState;

    case actionTypes.GET_IN_PROGRESS:
      newState = state;
      newState = newState
        .set('inProgressArray', action.inProgressData.assignments)
        .set('inProgressCount', action.inProgressData.assignmentCount);
      return newState;

    case actionTypes.SET_TOPIC_SCORE:
      newState = state;
      let learningHours = {
        totalScore: action.totalScore,
        topics: action.topics,
        percentile: action.percentile,
        totalTopicAverage: action.totalTopicAverage
      };
      newState = newState.set('learningHours', learningHours);
      return newState;

    case actionTypes.UPDATE_PUBLIC_PROFILE_INFO:
      newState = state;
      let diff = differenceBy(INIT.dashboardInfo, action.dashboardInfo, 'name');
      newState = newState.set(
        'dashboardInfo',
        Immutable.List(
          action.dashboardInfo.length
            ? uniqBy([...action.dashboardInfo, ...diff], 'name')
            : INIT.dashboardInfo
        )
      );
      return newState;

    case actionTypes.SET_ACTIVE_TOPICS:
      newState = state;
      newState = newState.set('activeTopics', action.activeTopics);
      return newState;

    case actionTypes.RECEIVE_USER_CONTENT:
      newState = state;
      newState = newState.set('userTopContent', action.userTopContent);
      return newState;

    default:
      return state;
  }
}
