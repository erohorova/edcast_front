import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

// according to the cases type described in eclActions.js this reducer performs following actions
// if the search resultIds prop by using the map function on the results object got from api
// And it will update the facet_cards prop according to the new results but only for 2 cases
// Because when user checks / unchecks an origin type there is no need to update the facet_cards

export default function eclReducer(
  state = Immutable.Map({
    resultIds: []
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.ECL_RESULTS_FOUND:
      newState = state;
      newState = newState.set(
        'resultIds',
        Immutable.fromJS(action.results.results.map(result => result._id + ''))
      );
      newState = newState.set('facet_cards', action.results.facets.card_type.buckets);
      return newState;
    case actionTypes.CARD_TYPE_CLICKED:
      newState = state;
      newState = newState.set(
        'resultIds',
        Immutable.fromJS(action.results.results.map(result => result._id + ''))
      );
      newState = newState.set('facet_origins', action.results.facets['ecl.origin'].buckets);
      return newState;
    case actionTypes.ORIGIN_TYPE_CLICKED:
      newState = state;
      newState = newState.set(
        'resultIds',
        Immutable.fromJS(action.results.results.map(result => result._id + ''))
      );
      return newState;
    default:
      return state;
  }
}
