import Immutable from 'immutable';
import _ from 'lodash';

import * as actionTypes from '../constants/actionTypes';

export default function contentMultiaction(state = Immutable.Map(), action) {
  let card,
    teamsAssigned,
    newState,
    oldUsers,
    searchedUsers,
    searchedGroups,
    oldGroups,
    users,
    groups,
    index,
    removeUsersFromList,
    removeGroupsFromList,
    usersWithAccess,
    groupsWithAccess,
    assignedUserList,
    invitedUserList,
    groupUserList;
  let stateToJs = state.toJS();
  switch (action.type) {
    case actionTypes.FETCH_CONTENT_TO_ACTION:
      newState = state;
      card = action.card;
      newState = newState
        .set(card.id + '', Immutable.Map(card))
        .setIn([card.id + '', 'usersWithAccess'], card.usersWithAccess)
        .setIn([card.id + '', 'teams'], card.teams)
        .set('contentLoaded', true)
        .set('currentCard', card.id);
      return newState;

    case actionTypes.FETCH_MY_TEAM_FOR_ACTIONS:
      newState = state;
      let key = action.actionKey + 'Group';
      oldGroups = stateToJs[key] || [];
      groups = [...oldGroups, ...(action.groups.teams || [])];

      newState = newState
        .set('groups', _.uniqBy(groups, 'id'))
        .set(key, _.uniqBy(groups, 'id'))
        .set('groupsLoading', false)
        .set('totalGroups', action.groups.total);
      return newState;

    case actionTypes.FETCH_USERS_FOR_ACTION:
      newState = state;
      oldUsers = stateToJs[action.actionKey] || [];
      users = [...oldUsers, ...(action.users.users || [])];
      newState = newState
        .set('users', _.uniqBy(users, 'id'))
        .set(action.actionKey, _.uniqBy(users, 'id'))
        .set('usersLoading', false)
        .set('totalUsers', action.users.total);
      return newState;

    case actionTypes.FETCH_ASSIGNED_USERS_AND_GROUPS:
      newState = state;
      let assigned = action.assigned.body;
      newState = newState
        .set('currentlyAssignedUsers', assigned.users)
        .set('currentlyAssignedTeams', assigned.teams)
        .set('totalAssignedUsers', assigned.totalUsers)
        .set('totalAssignedTeams', assigned.totalTeams);
      return newState;

    case actionTypes.FETCH_SEARCHED_ASSIGNED:
      newState = state;

      if (action.searchType === 'User') {
        newState = newState
          .set('searchAssignedUserQuery', action.query)
          .set('searchAssignedUsers', (action.results && action.results.users) || [])
          .set('totalUsers', action.results.totalUsers);
      } else if (action.searchType === 'Team') {
        newState = newState
          .set('searchAssignedTeamQuery', action.query)
          .set('searchAssignedTeams', (action.results && action.results.teams) || [])
          .set('totalTeams', action.results.totalTeams);
      }
      return newState;

    case actionTypes.FETCH_ALL_ASSIGNED:
      newState = state;
      assigned = action.assigned.body;
      newState = newState
        .set('allAssignedUsers', assigned.users)
        .set('allAssignedTeams', assigned.teams)
        .set('totalAssignedUsers', assigned.totalUsers)
        .set('totalAssignedTeams', assigned.totalTeams);
      return newState;

    case actionTypes.ADD_SEARCHED_USER_FOR_ACTION:
      newState = state;
      users = stateToJs.actionWithUsers || [];
      let existingUserForAction;

      if (action.currentAction === 'assign') {
        assignedUserList = action.assignments.assignedUsers.users || [];
        existingUserForAction = assignedUserList;
      }

      if (action.currentAction === 'share') {
        usersWithAccess = (action.card && action.card.usersWithAccess) || [];
        existingUserForAction = usersWithAccess;
      }

      if (action.currentAction === 'invite') {
        invitedUserList =
          action.groups &&
          action.groups.currentGroupID &&
          action.groups[action.groups.currentGroupID] &&
          action.groups[action.groups.currentGroupID].pending.map(user => {
            return { id: user.userId };
          });
        groupUserList =
          (action.groups &&
            action.groups.currentGroupID &&
            action.groups[action.groups.currentGroupID] &&
            action.groups[action.groups.currentGroupID].members) ||
          [];
        existingUserForAction = [...invitedUserList, ...groupUserList];
      }

      searchedUsers = stateToJs.searchedUsers || [];

      if (!_.some([...existingUserForAction, ...users], ['id', action.user.id])) {
        searchedUsers.push(action.user);
      }

      newState = newState.set('searchedUsers', _.uniqBy(searchedUsers, 'id'));
      return newState;

    case actionTypes.ADD_SEARCHED_GROUP_FOR_ACTION:
      newState = state;

      groupsWithAccess = (action.card && action.card.teams) || [];
      groups = stateToJs.actionWithGroups || [];

      searchedGroups = stateToJs.searchedGroups || [];

      if (!_.some([...groupsWithAccess, ...groups], ['id', action.group.id])) {
        searchedGroups.push(action.group);
      }

      newState = newState.set('searchedGroups', _.uniqBy(searchedGroups, 'id'));
      return newState;

    case actionTypes.ADD_USER_FOR_ACTION:
      newState = state;
      usersWithAccess = (action.card && action.card.usersWithAccess) || [];
      users = stateToJs.actionWithUsers || [];
      removeUsersFromList = stateToJs.removeUsersFromList || [];
      assignedUserList =
        (action.assignments &&
          action.assignments.assignedUsers &&
          action.assignments.assignedUsers.users) ||
        [];
      invitedUserList =
        (action.groups &&
          action.groups.currentGroupID &&
          action.groups[action.groups.currentGroupID] &&
          action.groups[action.groups.currentGroupID].members) ||
        [];

      action.currentAction === 'share' &&
        !_.some(usersWithAccess, ['id', action.user.id]) &&
        users.push(action.user);
      action.currentAction === 'assign' &&
        !_.some(assignedUserList, ['id', action.user.id]) &&
        users.push(action.user);
      action.currentAction === 'invite' &&
        !_.some(invitedUserList, ['id', action.user.id]) &&
        users.push(action.user);
      index = _.findIndex(removeUsersFromList, ['id', action.user.id]);

      index > -1 && removeUsersFromList.splice(index, 1);

      newState = newState
        .set('actionWithUsers', _.uniqBy(users, 'id'))
        .set('removeUsersFromList', _.uniqBy(removeUsersFromList, 'id'));
      return newState;

    case actionTypes.ADD_GROUP_FOR_ACTION:
      newState = state;
      teamsAssigned =
        (stateToJs[stateToJs.currentCard] &&
          stateToJs[stateToJs.currentCard].assignment &&
          stateToJs[stateToJs.currentCard].assignment.teams) ||
        [];
      groupsWithAccess = (action.card && action.card.teams) || [];
      groups = stateToJs.actionWithGroups || [];
      removeGroupsFromList = stateToJs.removeGroupsFromList || [];

      action.currentAction === 'share' &&
        !_.some(groupsWithAccess, ['id', action.group.id]) &&
        groups.push(action.group);
      action.currentAction === 'assign' &&
        !_.some(teamsAssigned, ['id', action.group.id]) &&
        groups.push(action.group);
      index = _.findIndex(removeGroupsFromList, action.group);
      index > -1 && removeGroupsFromList.splice(index, 1);

      newState = newState
        .set('actionWithGroups', _.uniqBy(groups, 'id'))
        .set('removeGroupsFromList', _.uniqBy(removeGroupsFromList, 'id'));
      return newState;

    case actionTypes.REMOVE_SEARCHED_USER_FROM_ACTION:
      newState = state;
      searchedUsers = stateToJs.searchedUsers || [];

      index = _.findIndex(searchedUsers, action.user);
      index > -1 && searchedUsers.splice(index, 1);

      newState = newState.set('searchedUsers', _.uniqBy(searchedUsers, 'id'));
      return newState;

    case actionTypes.REMOVE_SEARCHED_GROUP_FROM_ACTION:
      newState = state;
      searchedGroups = stateToJs.searchedGroups || [];

      index = _.findIndex(searchedGroups, action.group);
      index > -1 && searchedGroups.splice(index, 1);

      newState = newState.set('searchedGroups', _.uniqBy(searchedGroups, 'id'));
      return newState;

    case actionTypes.REMOVE_USER_FROM_ACTION:
      newState = state;
      usersWithAccess =
        (stateToJs[stateToJs.currentCard] && stateToJs[stateToJs.currentCard].usersWithAccess) ||
        [];
      users = stateToJs.actionWithUsers || [];
      removeUsersFromList = stateToJs.removeUsersFromList || [];
      let assignedUserListForRemove = stateToJs.allAssignedUsers
        ? stateToJs.allAssignedUsers
        : (action.assignments &&
            action.assignments.assignedUsers &&
            action.assignments.assignedUsers.users) ||
          [];
      let invitedUserListForRemove =
        (action.groups &&
          action.groups.currentGroupID &&
          action.groups[action.groups.currentGroupID] &&
          action.groups[action.groups.currentGroupID].members) ||
        [];

      action.currentAction === 'share' &&
        _.some(usersWithAccess, ['id', action.user.id]) &&
        removeUsersFromList.push(action.user);
      action.currentAction === 'assign' &&
        _.some(assignedUserListForRemove, ['id', action.user.id]) &&
        removeUsersFromList.push(action.user);
      action.currentAction === 'invite' &&
        _.some(invitedUserListForRemove, ['id', action.user.id]) &&
        removeUsersFromList.push(action.user);

      index = _.findIndex(users, item => action.user.id == item.id);
      index > -1 && users.splice(index, 1);

      newState = newState
        .set('actionWithUsers', _.uniqBy(users, 'id'))
        .set('removeUsersFromList', _.uniqBy(removeUsersFromList, 'id'));
      return newState;

    case actionTypes.REMOVE_GROUP_FROM_ACTION:
      newState = state;
      teamsAssigned = stateToJs.allAssignedTeams || [];
      groupsWithAccess =
        (stateToJs[stateToJs.currentCard] && stateToJs[stateToJs.currentCard].teams) || [];
      groups = stateToJs.actionWithGroups || [];
      removeGroupsFromList = stateToJs.removeGroupsFromList || [];

      action.currentAction === 'assign' &&
        (_.some(teamsAssigned, ['id', action.group.id]) ||
          _.some(groups, ['id', action.group.id])) &&
        removeGroupsFromList.push(action.group);

      action.currentAction === 'share' &&
        (_.some(groupsWithAccess, ['id', action.group.id]) ||
          _.some(groups, ['id', action.group.id])) &&
        removeGroupsFromList.push(action.group);

      index = _.findIndex(groups, item => action.group.id == item.id);
      index > -1 && groups.splice(index, 1);

      newState = newState
        .set('actionWithGroups', _.uniqBy(groups, 'id'))
        .set('removeGroupsFromList', _.uniqBy(removeGroupsFromList, 'id'));
      return newState;

    case actionTypes.CLEAR_REMOVE_USERS_LIST:
      newState = state;
      newState = newState.set('removeUsersFromList', []);
      return newState;

    case actionTypes.CLEAR_REMOVE_GROUPS_LIST:
      newState = state;
      newState = newState.set('removeGroupsFromList', []);
      return newState;

    case actionTypes.FILL_REMOVE_USERS_LIST:
      newState = state;
      users = action.users;
      newState = newState.set('removeUsersFromList', users);
      return newState;

    case actionTypes.FILL_REMOVE_GROUPS_LIST:
      newState = state;
      groups = action.groups;
      newState = newState.set('removeGroupsFromList', groups);
      return newState;

    case actionTypes.FILL_ADD_USERS_LIST:
      newState = state;
      if (action.shouldUnCheck) {
        if (action.currentAction !== 'assign') {
          newState = newState
            .set('actionWithUsers', [])
            .set('removeUsersFromList', action.currentUsers);
        } else {
          let diff = _.differenceBy(action.users, action.currentUsers, 'email');
          newState = newState.set('actionWithUsers', []).set('removeUsersFromList', diff);
        }
      } else {
        users = action.users;
        let diff;
        if (action.currentAction !== 'invite') {
          diff = _.differenceBy(action.users, action.currentUsers, 'email');
        } else {
          diff = action.users;
        }

        newState = newState.set('removeUsersFromList', []).set('actionWithUsers', diff);
      }
      return newState;

    case actionTypes.FILL_ADD_GROUPS_LIST:
      newState = state;
      groups = action.groups;
      if (action.shouldUnCheck) {
        newState = newState.set('actionWithGroups', []);
      } else {
        newState = newState.set('actionWithGroups', _.uniqBy(groups, 'id'));
      }
      return newState;

    case actionTypes.CLEAR_CURRENT_SEARCH:
      newState = state;
      if (action.searchType === 'user') {
        newState = newState.set('searchAssignedUserQuery', '').set('searchAssignedUsers', []);
      } else if (action.searchType === 'team') {
        newState = newState
          .set('contents.searchAssignedTeamQuery', '')
          .set('searchAssignedTeams', []);
      }
      return newState;

    case actionTypes.CLEAR_ACTION_CONTENT_STATE:
      newState = state;
      card = action.card || {};
      if (card) {
        newState = newState
          .setIn([card.id + '', 'usersWithAccess'], card.usersWithAccess)
          .setIn([card.id + '', 'teams'], card.teams)
          .set('actionWithUsers', [])
          .set('removeUsersFromList', [])
          .set('actionWithGroups', [])
          .set('removeGroupsFromList', [])
          .set('searchedUsers', [])
          .set('searchedGroups', [])
          .set('currentCard', '')
          .set('contentLoaded', false)
          .set('searchSharedQuery', '')
          .set('searchAssignedUserQuery', '')
          .set('searchAssignedTeamQuery', '');
      } else {
        newState = newState
          .set('actionWithUsers', [])
          .set('removeUsersFromList', [])
          .set('actionWithGroups', [])
          .set('removeGroupsFromList', [])
          .set('searchedUsers', [])
          .set('searchedGroups', [])
          .set('currentCard', '')
          .set('contentLoaded', false);
      }
      return newState;

    default:
      return state;
  }
}
