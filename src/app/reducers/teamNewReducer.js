import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import _ from 'lodash';

let INIT = {
  pinnedCards: [],
  pinnedCardIDs: []
};

export default function groupsReducerV2(state = Immutable.fromJS(INIT), action) {
  let newState, pinnedCards, addCard, removeCard, index;
  switch (action.type) {
    case actionTypes.RECEIVE_PINNED_CARDS_GROUP:
      newState = state;
      newState = newState
        .set('pinnedCards', action.pins)
        .set('pinnedCardIDs', action.pins.map(card => card.id));
      return newState;

    case actionTypes.ADD_TO_PINNED_CARD_CAROUSEL_GROUP:
      newState = state;
      pinnedCards = state.get('pinnedCards');
      addCard = action.card;
      pinnedCards.unshift(addCard);
      newState = newState
        .set('pinnedCards', pinnedCards)
        .set('pinnedCardIDs', pinnedCards.map(card => card.id));
      return newState;

    case actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL_GROUP:
      newState = state;
      pinnedCards = state.get('pinnedCards');
      removeCard = action.card;
      index = _.findIndex(pinnedCards, ['id', removeCard.id]);
      if (index > -1) {
        pinnedCards.splice(index, 1);
      }
      newState = newState
        .set('pinnedCards', pinnedCards)
        .set('pinnedCardIDs', pinnedCards.map(card => card.id));
      return newState;

    default:
      return state;
  }
}
