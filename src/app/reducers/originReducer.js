import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function originReducer(state = Immutable.Map({}), action) {
  let newState;
  switch (action.type) {
    // dispatched from providerAction.js, assigning received content items for this provider to the originItems prop
    case actionTypes.RECEIVE_ECL_ITEMS:
      newState = state;
      newState = state.set('originItems', Immutable.fromJS(action.items));
      return newState;
    default:
      return state;
  }
}
