/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function usersReducer(
  state = Immutable.fromJS({ idMap: {}, handleToIdMap: {} }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_USERS:
    case actionTypes.RECEIVE_DISCOVERY_INFLUENCERS:
      newState = state;
      action.users.forEach(user => {
        newState = newState
          .setIn(['idMap', user.id], Immutable.fromJS(user))
          .setIn(['handleToIdMap', user.handle], user.id);
      });
      return newState;
    case actionTypes.RECEIVE_MENTION_USERS:
      newState = Immutable.fromJS({ idMap: {}, handleToIdMap: {} });
      action.users.forEach(user => {
        newState = newState
          .setIn(['idMap', user.id], Immutable.fromJS(user))
          .setIn(['handleToIdMap', user.handle], user.id);
      });
      return newState;
    case actionTypes.RECEIVE_LEADERBOARD_USERS:
      newState = state;
      action.users.forEach(user => {
        newState = newState
          .setIn(['idMap', user.user.id], Immutable.fromJS(user.user))
          .setIn(['handleToIdMap', user.user.handle], user.user.id);
      });
      return newState;
    case actionTypes.RECEIVE_CARDS:
      newState = state;
      action.cards.forEach(card => {
        if (card.author && card.author.handle) {
          let user = Object.assign({}, card.author, { id: card.author.id + '' });
          newState = newState
            .setIn(['idMap', user.id], Immutable.fromJS(user))
            .setIn(['handleToIdMap', user.handle], user.id);
        }
        if (card.mentions) {
          card.mentions.forEach(mentionUser => {
            if (mentionUser.id && mentionUser.handle) {
              newState = newState
                .setIn(['idMap', mentionUser.id], Immutable.fromJS(mentionUser))
                .setIn(['handleToIdMap', mentionUser.handle], mentionUser.id + '');
            }
          });
        }
        if (card.comments) {
          card.comments.forEach(comment => {
            if (comment.user.id && comment.user.handle) {
              newState = newState
                .setIn(['idMap', comment.user.id], Immutable.fromJS(comment.user))
                .setIn(['handleToIdMap', comment.user.handle], comment.user.id + '');
            }
            if (comment.mentions) {
              comment.mentions.forEach(mentionUser => {
                if (mentionUser.id && mentionUser.handle) {
                  newState = newState
                    .setIn(['idMap', mentionUser.id], Immutable.fromJS(mentionUser))
                    .setIn(['handleToIdMap', mentionUser.handle], mentionUser.id + '');
                }
              });
            }
          });
        }
      });
      return newState;
    case actionTypes.RECEIVE_USER_ASSIGNMENTS:
      newState = state;
      action.assignments.forEach(assignment => {
        let card = assignment.assignable;
        if (card.author && card.author.id && card.author.handle) {
          newState = newState
            .setIn(['idMap', card.author.id], Immutable.fromJS(card.author))
            .setIn(['handleToIdMap', card.author.handle], card.author.id + '');
        }
        if (card.mentions) {
          card.mentions.forEach(mentionUser => {
            if (mentionUser.id && mentionUser.handle) {
              newState = newState
                .setIn(['idMap', mentionUser.id], Immutable.fromJS(mentionUser))
                .setIn(['handleToIdMap', mentionUser.handle], mentionUser.id + '');
            }
          });
        }
        if (card.comments) {
          card.comments.forEach(comment => {
            if (comment.user.id && comment.user.handle) {
              newState = newState
                .setIn(['idMap', comment.user.id], Immutable.fromJS(comment.user))
                .setIn(['handleToIdMap', comment.user.handle], comment.user.id + '');
            }
            if (comment.mentions) {
              comment.mentions.forEach(mentionUser => {
                if (mentionUser.id && mentionUser.handle) {
                  newState = newState
                    .setIn(['idMap', mentionUser.id], Immutable.fromJS(mentionUser))
                    .setIn(['handleToIdMap', mentionUser.handle], mentionUser.id + '');
                }
              });
            }
          });
        }
      });
      return newState;
    case actionTypes.RECEIVE_COMMENTS_FOR_CARD:
      newState = state;
      action.comments.forEach(comment => {
        if (comment.user.id && comment.user.handle) {
          newState = newState
            .setIn(['idMap', comment.user.id], Immutable.fromJS(comment.user))
            .setIn(['handleToIdMap', comment.user.handle], comment.user.id + '');
        }
        if (comment.mentions) {
          comment.mentions.forEach(user => {
            if (user.id && user.handle) {
              newState = newState
                .setIn(['idMap', user.id], Immutable.fromJS(user))
                .setIn(['handleToIdMap', user.handle], user.id);
            }
          });
        }
      });
      return newState;
    case actionTypes.RECEIVE_RECOMMENDED_INFLUENCERS:
      newState = state;
      action.influencers.forEach(user => {
        if (user.id && user.handle) {
          newState = newState
            .setIn(['idMap', user.id], Immutable.fromJS(user))
            .setIn(['handleToIdMap', user.handle], user.id);
        }
      });
      return newState;
    case actionTypes.REQUEST_TOGGLE_USER_FOLLOW:
      newState = state.setIn(['idMap', action.id, 'followPending'], true);
      return newState;
    case actionTypes.RECEIVE_TOGGLE_USER_FOLLOW:
      newState = state
        .setIn(['idMap', action.id, 'following'], action.isFollow)
        .setIn(['idMap', action.id, 'followPending'], false);
      return newState;
    case actionTypes.UPDATE_REPORTED_CARDS:
      let cardIds = state.reported_card_ids || [];
      cardIds.push(action.cardId);

      newState = state;
      newState = newState.set('reported_card_ids', cardIds);
      return newState;
    case actionTypes.UPDATE_REPORTED_COMMENTS:
      let commentIds = state.reported_comment_ids || [];
      commentIds.push(action.commentId);

      newState = state;
      newState = newState.set('reported_comment_ids', commentIds);
      return newState;
    default:
      return state;
  }
}
