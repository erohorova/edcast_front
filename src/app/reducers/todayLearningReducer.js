/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function todayLearningReducer(
  state = Immutable.fromJS({
    cards: [],
    topics: [],
    expertiseTopics: [],
    skills: {},
    expertiseSkills: {}
  }),
  action
) {
  let newState;

  switch (action.type) {
    case actionTypes.REQUEST_USER_INTEREST:
      return state.set('pending', true);
    case actionTypes.RECEIVE_USER_INTEREST:
      return state
        .set('pending', false)
        .set('userInterest', action.interests && action.interests.length ? action.interests : null);
    case actionTypes.RECEIVE_TOPIC_SKILLS:
      newState = state;
      let skills = action.data.topics;
      let obj = state.toJS().skills;
      if (skills.length > 0) {
        if (obj.hasOwnProperty(`skills-${action.rowIndex}`)) {
          if (!obj[`skills-${action.rowIndex}`].hasOwnProperty(`skill-${action.data.domain.id}`)) {
            obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`] = {
              isLastSkillPage: false,
              data: []
            };
          }
          obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`]['data'] = obj[
            `skills-${action.rowIndex}`
          ][`skill-${action.data.domain.id}`]['data'].concat(skills);
        } else {
          obj[`skills-${action.rowIndex}`] = {};
          obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`] = {
            isLastSkillPage: false,
            data: skills
          };
        }
      } else {
        if (obj.hasOwnProperty(`skills-${action.rowIndex}`)) {
          obj[`skills-${action.rowIndex}`][`skill-${action.topicId}`] = {
            isLastSkillPage: true,
            data: obj[`skills-${action.rowIndex}`][`skill-${action.topicId}`]['data']
          };
        }
      }

      newState = newState.set('skills', Immutable.fromJS(obj));
      return newState;
    case actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER_ONBOARDING:
      newState = state;
      let topics = [];

      topics = action.section == 'learningTopics' ? action.topics.domains : [];

      if (topics.length < 12) {
        newState =
          action.section == 'expertiseSection'
            ? newState.set('isLastExpertiseTopicPage', true)
            : newState.set('isLastLearningTopicPage', true);
      }

      let newTopics = [];

      newTopics = state.toJS().topics;
      topics.forEach(function(element) {
        newTopics.push(element);
      });

      newState = newState.set('topics', Immutable.fromJS(newTopics));
      return newState;
    default:
      return state;
  }
}
