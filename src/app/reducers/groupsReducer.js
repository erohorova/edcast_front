import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function groupsReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_GROUPS:
      newState = state;
      action.groups.forEach(group => {
        newState = newState.set(group.id + '', Immutable.fromJS(group));
      });
      return newState;
    default:
      return state;
  }
}
