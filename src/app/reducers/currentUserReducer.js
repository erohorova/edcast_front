/**
 * Created by ypling on 7/5/16.
 */

import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function currentUserReducer(
  state = Immutable.fromJS({
    teams: [],
    invalidTeamName: false,
    walletTransactions: [],
    followingUsers: []
  }),
  action
) {
  let newState;
  switch (action.type) {
    // case actionTypes.RECEIVE_USER_AUTHENTICATED:
    case actionTypes.RECEIVE_INIT_USER_INFO:
      /**
       * Create and set new user for grabbing config data
       * Init is created in src/main.jsx
       */
      let client = {
        key: action.user.id,
        firstName: action.user.firstName,
        lastName: action.user.lastName,
        email: action.user.email,
        custom: {
          org: action.user.organization.hostName
        }
      };
      window.ldclient.identify(client, window.tempHash);

      localStorage.setItem('isLoggedIn', true);
      if (
        window.__GATrackingOrganizationObject__ &&
        action.user &&
        action.user.organization &&
        !action.user.organization.configs
      ) {
        action.user.organization = window.__GATrackingOrganizationObject__;
      }
      window.__ED__ = action.user;
      return state.merge({
        isLoggedIn: true,
        invalidLogin: false,
        isAdmin: action.user.isAdmin === true,
        id: action.user.id + '',
        picture: action.user.picture,
        avatar: action.user.picture,
        name: action.user.fullName,
        handle: action.user.handle && action.user.handle.substr(1),
        email: action.user.email,
        isGroupLeader: action.user.isGroupLeader,
        profile: action.user.profile,
        orgAnnualSubscriptionPaid: action.user.orgAnnualSubscriptionPaid,
        jwtToken: action.user.jwtToken,
        isSuperAdmin: action.user.isSuperAdmin,
        isLoaded: true,
        teams: action.user.activeOrgs,
        countryCode: action.user.countryCode
      });
    case actionTypes.RECEIVE_GROUPLEADER_USER_INFO:
      return state.set('isGroupLeader', action.state);
    case actionTypes.SET_CURRENT_USER_INFO:
      return state.merge(action.userData);
    case actionTypes.ERROR_INIT_USER:
      localStorage.setItem('isLoggedIn', false);
      return state.set('isLoggedIn', false);
    case actionTypes.ERROR_INIT_USER_PUBLIC:
      localStorage.setItem('isLoggedIn', false);
      return state.set('isLoggedIn', false);
    case actionTypes.USER_LOG_OUT:
      localStorage.setItem('isLoggedIn', false);
      localStorage.setItem('afterLoginContentURL', '/');
      localStorage.setItem('afterOnboardingURL', '/');
      return state.set('isLoggedIn', false);
    case actionTypes.ERROR_USER_LOGIN:
      const mergeObj = {
        invalidEmailInput: true,
        invalidPasswordInput: true,
        locked: false,
        isLoggedIn: false
      };
      if (action.errorMsg == 'Your account is locked.') {
        mergeObj.locked = true;
      }
      return state.merge(mergeObj);
    case actionTypes.ERROR_UPDATED_USER_INFO:
      return state.set('updateError', true);
    case actionTypes.CHANGE_EMAIL_INPUT:
      return state.set('invalidEmailInput', false);
    case actionTypes.GET_FOLLOWING_USERS:
      return state.merge({
        followingUsers: action.users,
        needToUpdateFollowing: false
      });
    case actionTypes.SET_USER_BADGES:
      return state.setIn(['userBadges', action.badgeType], action.badges);
    case actionTypes.FOLLOWING_CHANNEL_STATE:
      let followingChannelsState = state.get('followingChannels');
      const actionChannelIdx = followingChannelsState.findIndex(
        channel => channel.id === action.channel.id
      );
      return state.withMutations(mutableState => {
        if (actionChannelIdx !== -1) {
          // delete old one anyway
          mutableState.deleteIn(['followingChannels', actionChannelIdx]);
        }
        if (!action.follow) {
          mutableState.update('followingChannels', channels => channels.push(action.channel));
        }
      });
    case actionTypes.FOLLOWING_USER_STATE:
      let followingUserStateIdx = state
        .get('followingUsers')
        .findIndex(user => user.id === action.userId);
      let followerUserStateIdx = state
        .get('followerUsers')
        .findIndex(user => user.id === action.userId);
      return state.withMutations(mutableState => {
        if (followingUserStateIdx !== -1) {
          mutableState.setIn(
            ['followingUsers', followingUserStateIdx, 'isFollowing'],
            action.state
          );
        }

        if (followerUserStateIdx !== -1) {
          mutableState.setIn(['followerUsers', followerUserStateIdx, 'isFollowing'], action.state);
        }
      });
    case actionTypes.FOLLOWER_USER_STATE:
      let followingUserIdx = state.get('followingUsers').find(user => user.id === action.userId);
      let followerUserIdx = state.get('followerUsers').find(user => user.id === action.userId);
      return state.withMutations(mutableState => {
        if (followerUserStateIdx !== -1) {
          mutableState.setIn(['followerUsers', followerUserIdx, 'isFollowing'], action.state);
        }
        if (followingUserStateIdx !== -1) {
          mutableState.setIn(['followingUsers', followingUserIdx, 'isFollowing'], action.state);
        } else if (followerUserIdx !== -1) {
          const followerUser = state.getIn('followerUsers', followerUserIdx);
          mutableState.updateIn('followingUsers', users => users.push(followerUser));
        }
      });
    case actionTypes.GET_FOLLOWER_USERS:
      return state.set('followerUsers', action.users);
    case actionTypes.UPDATE_USER_DETAILS:
      return state.merge({
        picture: action.user.picture,
        avatar: action.user.picture,
        first_name: action.user.firstName,
        last_name: action.user.lastName,
        name: action.user.fullName,
        email: action.user.email,
        bio: action.user.bio,
        coverImage: action.user.coverimages.banner_url || action.user.coverimages.banner
      });
    case actionTypes.CHANGE_PASSWORD_INPUT:
      return state.set('invalidPasswordInput', false);
    case actionTypes.UPDATE_FOLLOWING_USERS_COUNT:
      return state.withMutations(mutableState => {
        if (action.state) {
          mutableState.update('followingCount', c => c + 1);
        } else {
          mutableState.update('followingCount', c => c - 1);
        }
        mutableState.set('needToUpdateFollowing', true);
      });
    case actionTypes.RECEIVE_SKILLS:
      return state.set('userSkills', action.data);
    case actionTypes.RECEIVE_PUBLIC_PROFILE:
      return state.set('publicProfile', action.data);
    case actionTypes.LOADING_PUBLIC_PROFILE_CARDS:
      return state.set('loadingPublicProfileCards', action.loading);
    case actionTypes.REMOVE_PUBLIC_PROFILE:
      return state.set('publicProfile', false);
    case actionTypes.RECEIVE_PUBLIC_PROFILE_BASIC_INFO:
      return state.set('publicProfileBasicInfo', action.userParam);
    case actionTypes.UPDATE_USER_EXPERTISE_AND_INTEREST:
      return state.set('profile', action.profile);
    case actionTypes.RECEIVE_WALLET_BALANCE:
      return state.set(
        'walletBalance',
        (action.walletbalance && action.walletbalance.balance) || 0
      );
    case actionTypes.RECEIVE_WALLET_TRANSACTIONS:
      let walletTransactions = !action.reload
        ? state.get('walletTransactions').concat(action.walletTransactions || [])
        : action.walletTransactions;
      return state.merge({
        walletTransactions: walletTransactions,
        walletTransactionsCount: action.walletTransactionsCount
      });
    case actionTypes.SHOW_WALLET_LOADING:
      return state.set('showWalletLoading', true);
    case actionTypes.HIDE_WALLET_LOADING:
      return state.set('showWalletLoading', false);
    case actionTypes.ORG_SUBSCRIPTION_PAID:
      return state.set('orgAnnualSubscriptionPaid', true);
    case actionTypes.FETCH_NOTIFICATION_CONFIG:
      return state.merge({
        notificationConfig: action.notificationConfig,
        showNotificationTab: action.showNotificationTab
      });
    case actionTypes.GET_CUSTOM_TOPICS:
      return state.set('customTopics', action.customTopics.topicRequests || action.customTopics);
    default:
      return state;
  }
}
