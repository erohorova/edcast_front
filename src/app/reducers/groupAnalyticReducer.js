import * as actionTypes from '../constants/actionTypes';
import { tr } from 'edc-web-sdk/helpers/translations';
import Immutable from 'immutable';
import moment from 'moment';
import _ from 'lodash';

export default function groupAnalytic(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.SET_GROUP_DETAILS:
      newState = state;
      newState = newState.set('groupID', Immutable.fromJS(action.group.id));
      newState = newState.set('graphData', Immutable.fromJS([]));
      newState = newState.set('assignmentLimit', 6);
      newState = newState.set('assignmentOffset', 0);
      newState = newState.set('assignmentTotal', 0);
      newState = newState.set('assignmentMetrics', []);
      return newState;

    case actionTypes.GET_GRAPH_DATA:
      newState = state;
      let newUsers = {
        name: 'newUsers',
        title: `New Users`,
        subTitle: action.graphData.new_users.value + ' New Users',
        graphValue: action.graphData.new_users.drilldown.values
      };

      let activeUsers = {
        name: 'activeUsers',
        title: `Active Users`,
        subTitle: action.graphData.active_users.value + ' Active Users',
        graphValue: action.graphData.active_users.drilldown.values
      };

      let subTitle = moment()
        .startOf('day')
        .seconds(action.graphData.average_session.value);
      let minVal = Number(subTitle.format('m'));
      let secVal = subTitle.format('s');
      subTitle = minVal ? minVal + 'm ' + secVal + 's' : secVal + 's';

      let avgSession = {
        name: 'avgSession',
        title: `Avg Session`,
        subTitle: subTitle,
        graphValue: action.graphData.average_session.drilldown.values
      };

      let consumption = {
        name: 'consumption',
        title: `Consumption`,
        subTitle: action.graphData.smartbites_consumed.value + ' SmartCards Consumed',
        graphValue: action.graphData.smartbites_consumed.drilldown.values
      };

      let contributions = {
        name: 'contributions',
        title: `Contributions`,
        subTitle: action.graphData.smartbites_created.value + ' SmartCards Created',
        graphValue: action.graphData.smartbites_created.drilldown.values
      };

      let engagement = {
        name: 'engagement',
        title: `Engagement Index`,
        subTitle: action.graphData.engagement_index.value + ' %',
        graphValue: action.graphData.engagement_index.drilldown.values
      };

      let clc_mins = action.graphData.clc_progress.drilldown.values;
      let total = 0;
      for (let i in clc_mins) {
        total += clc_mins[i];
      }
      let clc_value = parseInt(total / 60, 10) + parseInt((total % 60) * (10 / 6), 10) / 100;

      let clc = {
        name: 'clc',
        title: `Continuous Learning Credits`,
        subTitle: clc_value + ' hrs',
        graphValue: action.graphData.clc_progress.drilldown.values
      };

      let graphData = [
        newUsers,
        activeUsers,
        avgSession,
        consumption,
        contributions,
        engagement,
        clc
      ];

      newState = newState.setIn(['graphData'], graphData);
      return newState;

    case actionTypes.GET_TOP_CONTENT:
      newState = state;
      newState = newState.set('topContent', Immutable.fromJS(action.content.topContent));
      return newState;

    case actionTypes.GET_TOP_CONTRIBUTORS:
      newState = state;
      newState = newState.set(
        'topContributors',
        Immutable.fromJS(action.contributors.topContributors)
      );
      return newState;

    case actionTypes.GET_TEAM_ASSIGNMENT_METRICS:
      newState = state;
      let assignmentMetrics = [
        ...newState.toJS().assignmentMetrics,
        ...action.assignmentMetrics.metrics
      ];
      assignmentMetrics = _.uniqBy(assignmentMetrics, 'id');
      newState = newState.set('assignmentMetrics', Immutable.fromJS(assignmentMetrics));
      newState = newState.set('assignmentTotal', Immutable.fromJS(action.assignmentMetrics.total));
      newState = newState.set('assignmentOffset', Immutable.fromJS(action.offset));
      return newState;

    default:
      return state;
  }
}
