/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function pathwaysReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_PATHWAYS:
      newState = state.set('pathways', action.pathways);
      return newState;
    case actionTypes.RECEIVE_CARD_INTO_PATHWAYS:
      return state.set('updateCountCardsInPathways', action.arr_pathways);
    case actionTypes.SAVE_TEMP_PATHWAY:
      newState = state.set('tempPathway', action.card);
      return newState;
    case actionTypes.DELETE_TEMP_PATHWAY:
      newState = state.set('tempPathway', null);
      return newState;
    case actionTypes.SAVE_CONSUMPTION_PATHWAY:
      newState = state.set('consumptionPathway', action.card);
      return newState;
    case actionTypes.SAVE_CONSUMPTION_PATHWAY_HISTORY_URL:
      newState = state.set('consumptionPathwayHistoryUrl', action.url);
      return newState;
    case actionTypes.CONSUMPTION_PATHWAY_HISTORY:
      newState = state.set('consumptionPathwayHistory', action.card);
      return newState;
    case actionTypes.REMOVE_CONSUMPTION_PATHWAY:
      newState = state.remove('consumptionPathway').set('consumptionPathwayHistoryUrl', action.url);
      return newState;
    case actionTypes.REMOVE_CONSUMPTION_PATHWAY_HISTORY:
      newState = state.remove('consumptionPathwayHistory');
      return newState;
    case actionTypes.SAVE_REORDER_CARD_IDS_PATHWAY:
      newState = state.set('reorderCardIds', action.cardIds);
      return newState;
    case actionTypes.REMOVE_REORDER_CARD_IDS_PATHWAY:
      newState = state.remove('reorderCardIds');
      return newState;
    case actionTypes.IS_PREVIEW_MODE_PATHWAY:
      newState = state.set('isPreviewMode', action.bool);
      return newState;
    default:
      return state;
  }
}
