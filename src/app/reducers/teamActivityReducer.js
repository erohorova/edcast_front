import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function teamActivityReducer(state = Immutable.fromJS({ cardIds: [] }), action) {
  let newState;
  switch (action.type) {
    case actionTypes.GET_ACTIVITY_STREAM:
      newState = state
        .set('activityStreams', action.response.activityStreams)
        .set('streamFlag', action.streamFlag);
      return newState;
    case actionTypes.RECEIVE_NEW_ACTIVITY:
      newState = state.set('activityFlag', false); // currently we are disabling this action as it causes unnecessary API calls on irrelevant actions so it not proper solution
      return newState;
    case actionTypes.GET_ACTIVITY_TEAMS:
      newState = state
        .set('groups', Immutable.fromJS(action.data))
        .set('isLastGroup', action.data.length < 20);
      return newState;
    default:
      return state;
  }
}
