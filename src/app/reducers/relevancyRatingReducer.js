import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function relevancyRating(state = Immutable.Map(), action) {
  let newState, newCardIds;
  switch (action.type) {
    case actionTypes.UPDATE_RELEVANCY_RATING_QUEUE:
      let queue = [];
      if (state.toJS().queue) {
        let found = state.toJS().queue.some(function(el) {
          return el.id === action.card.id;
        });
        if (!found && !action.card.userRating) queue = state.toJS().queue.concat(action.card);
        else queue = state.toJS().queue;
      } else if (!action.card.userRating) {
        queue.push(action.card);
      }
      newState = state.set('queue', queue);
      return newState;

    case actionTypes.FLUSH_RELEVANCY_RATING_QUEUE:
      queue = [];
      newState = state.set('queue', queue);
      return newState;

    case actionTypes.UPDATE_RATED_QUEUE_AFTER_RATING:
      let ratedQueue = [];
      if (state.toJS().ratedQueue) {
        let found = state.toJS().ratedQueue.some(function(el) {
          return el.id === action.ratedQueue.id;
        });
        if (!found) ratedQueue = state.toJS().ratedQueue.concat(action.ratedQueue);
        else ratedQueue = state.toJS().ratedQueue;
      } else {
        ratedQueue.push(action.ratedQueue);
      }
      newState = state.set('ratedQueue', ratedQueue);
      return newState;
    default:
      return state;
  }
}
