import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import _ from 'lodash';

export default function shareContent(state = Immutable.Map(), action) {
  let card,
    newState,
    oldUsers,
    searchedUsers,
    searchedGroups,
    oldGroups,
    users,
    groups,
    index,
    removeUsersFromShare,
    removeGroupsFromShare,
    usersWithAccess,
    groupsWithAccess;
  switch (action.type) {
    case actionTypes.FETCH_CONTENT_TO_SHARE:
      newState = state;
      card = action.card;
      newState = newState
        .set(card.id + '', Immutable.Map(card))
        .setIn([card.id + '', 'usersWithAccess'], card.usersWithAccess)
        .setIn([card.id + '', 'teams'], card.teams)
        .set('contentLoaded', true)
        .set('currentCard', card.id);
      return newState;

    case actionTypes.FETCH_MY_TEAM:
      newState = state;
      oldGroups = state.toJS().groups || [];
      groups = [...oldGroups, ...(action.groups.teams || [])];
      newState = newState
        .set('groups', _.uniqBy(groups, 'id'))
        .set('groupsLoading', false)
        .set('totalGroups', action.groups.total);
      return newState;

    case actionTypes.FETCH_USERS_FOR_SHARE:
      newState = state;
      oldUsers = state.toJS().users || [];
      users = [...oldUsers, ...(action.users.users || [])];
      newState = newState
        .set('users', _.uniqBy(users, 'id'))
        .set('usersLoading', false)
        .set('totalUsers', action.users.total);
      return newState;

    case actionTypes.ADD_SEARCHED_USER_FOR_SHARE:
      newState = state;

      usersWithAccess = action.card.usersWithAccess || [];
      users = state.toJS().shareWithUsers || [];

      searchedUsers = state.toJS().searchedUsers || [];

      if (!_.some([...usersWithAccess, ...users], ['id', action.user.id])) {
        searchedUsers.push(action.user);
      }

      newState = newState.set('searchedUsers', _.uniqBy(searchedUsers, 'id'));
      return newState;

    case actionTypes.ADD_SEARCHED_GROUP_FOR_SHARE:
      newState = state;

      groupsWithAccess = action.card.teams || [];
      groups = state.toJS().shareWithGroups || [];

      searchedGroups = state.toJS().searchedGroups || [];

      if (!_.some([...groupsWithAccess, ...groups], ['id', action.group.id])) {
        searchedGroups.push(action.group);
      }

      newState = newState.set('searchedGroups', _.uniqBy(searchedGroups, 'id'));
      return newState;

    case actionTypes.ADD_USER_FOR_SHARE:
      newState = state;
      usersWithAccess = action.card.usersWithAccess || [];
      users = state.toJS().shareWithUsers || [];
      removeUsersFromShare = state.toJS().removeUsersFromShare || [];

      !_.some(usersWithAccess, ['id', action.user.id]) && users.push(action.user);
      index = _.findIndex(removeUsersFromShare, action.user);
      index > -1 && removeUsersFromShare.splice(index, 1);

      newState = newState
        .set('shareWithUsers', _.uniqBy(users, 'id'))
        .set('removeUsersFromShare', _.uniqBy(removeUsersFromShare, 'id'));
      return newState;

    case actionTypes.ADD_GROUP_FOR_SHARE:
      newState = state;
      groupsWithAccess = action.card.teams || [];
      groups = state.toJS().shareWithGroups || [];
      removeGroupsFromShare = state.toJS().removeGroupsFromShare || [];

      !_.some(groupsWithAccess, ['id', action.group.id]) && groups.push(action.group);
      index = _.findIndex(removeGroupsFromShare, action.group);
      index > -1 && removeGroupsFromShare.splice(index, 1);

      newState = newState
        .set('shareWithGroups', _.uniqBy(groups, 'id'))
        .set('removeGroupsFromShare', _.uniqBy(removeGroupsFromShare, 'id'));
      return newState;

    case actionTypes.REMOVE_SEARCHED_USER_FROM_SHARE:
      newState = state;
      searchedUsers = state.toJS().searchedUsers || [];

      index = _.findIndex(searchedUsers, action.user);
      index > -1 && searchedUsers.splice(index, 1);

      newState = newState.set('searchedUsers', _.uniqBy(searchedUsers, 'id'));
      return newState;

    case actionTypes.REMOVE_SEARCHED_GROUP_FROM_SHARE:
      newState = state;
      searchedGroups = state.toJS().searchedGroups || [];

      index = _.findIndex(searchedGroups, action.group);
      index > -1 && searchedGroups.splice(index, 1);

      newState = newState.set('searchedGroups', _.uniqBy(searchedGroups, 'id'));
      return newState;

    case actionTypes.REMOVE_USER_FROM_SHARE:
      newState = state;
      usersWithAccess = action.card.usersWithAccess || [];
      users = state.toJS().shareWithUsers || [];
      removeUsersFromShare = state.toJS().removeUsersFromShare || [];

      index = _.findIndex(users, action.user);
      index > -1 && users.splice(index, 1);

      _.some(usersWithAccess, ['id', action.user.id]) && removeUsersFromShare.push(action.user);

      newState = newState
        .set('shareWithUsers', _.uniqBy(users, 'id'))
        .set('removeUsersFromShare', _.uniqBy(removeUsersFromShare, 'id'));
      return newState;

    case actionTypes.REMOVE_GROUP_FROM_SHARE:
      newState = state;
      groupsWithAccess = action.card.teams || [];
      groups = state.toJS().shareWithGroups || [];
      removeGroupsFromShare = state.toJS().removeGroupsFromShare || [];

      index = _.findIndex(groups, action.group);
      index > -1 && groups.splice(index, 1);

      _.some(groupsWithAccess, ['id', action.group.id]) && removeGroupsFromShare.push(action.group);

      newState = newState
        .set('shareWithGroups', _.uniqBy(groups, 'id'))
        .set('removeGroupsFromShare', _.uniqBy(removeGroupsFromShare, 'id'));
      return newState;

    case actionTypes.CLEAR_SHARE_CONTENT_STATE:
      newState = state;
      card = action.card;
      newState = newState
        .setIn([card.id + '', 'usersWithAccess'], card.usersWithAccess)
        .setIn([card.id + '', 'teams'], card.teams)
        .set('shareWithUsers', [])
        .set('removeUsersFromShare', [])
        .set('shareWithGroups', [])
        .set('removeGroupsFromShare', [])
        .set('searchedUsers', [])
        .set('searchedGroups', [])
        .set('currentCard', '')
        .set('contentLoaded', false);
      return newState;

    default:
      return state;
  }
}
