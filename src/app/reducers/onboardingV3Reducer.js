import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import _ from 'lodash';

export default function onboardingV2Reducer(
  state = Immutable.Map({ multiLevelTaxonomy: null }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.SET_MULTILEVEL_TAXONOMY:
      newState = state;
      let domainNode = _.omitBy(action.taxonomy, _.isArray);
      newState = newState
        .set('multiLevelTaxonomy', action.taxonomy.data)
        .set('domainNode', domainNode);
      return newState;
    default:
      return state;
  }
}
