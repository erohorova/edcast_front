/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import uniqBy from 'lodash/uniqBy';
import orderBy from 'lodash/orderBy';

export default function channelsReducerV2(state = Immutable.Map(), action) {
  let newState;
  let cards;
  let channels;
  switch (action.type) {
    case actionTypes.RECEIVE_CHANNELS_V2:
      newState = state;
      channels = newState.toJS();
      action.channels.forEach(channel => {
        let currentChannel = channels[channel.id];
        newState = newState.set(channel.id + '', Immutable.fromJS(channel));
        newState = newState
          .setIn([channel.id + '', 'video_stream'], [])
          .setIn([channel.id + '', 'pack'], [])
          .setIn([channel.id + '', 'all'], [])
          .setIn([channel.id + '', 'course'], [])
          .setIn([channel.id + '', 'journey'], [])
          .setIn([channel.id + '', 'smartbite'], [])
          .setIn([channel.id + '', 'smartbiteOffset'], 0)
          .setIn([channel.id + '', 'courseOffset'], 0)
          .setIn([channel.id + '', 'allOffset'], 0)
          .setIn([channel.id + '', 'video_streamOffset'], 0)
          .setIn([channel.id + '', 'packOffset'], 0)
          .setIn([channel.id + '', 'journeyOffset'], 0)
          .setIn(
            [channel.id + '', 'pinnedCount'],
            action.isUpdatingCurrent ? currentChannel.pinnedCount : 0
          )
          .setIn(
            [channel.id + '', 'pinnedCards'],
            action.isUpdatingCurrent ? currentChannel.pinnedCards : []
          )
          .setIn(
            [channel.id + '', 'pinnedCardsIds'],
            action.isUpdatingCurrent ? currentChannel.pinnedCardsIds : []
          )
          .setIn([channel.id + '', 'smartbitesCount'], channel.smartbitesCount)
          .setIn([channel.id + '', 'coursesCount'], channel.coursesCount)
          .setIn([channel.id + '', 'videoStreamsCount'], channel.videoStreamsCount)
          .setIn([channel.id + '', 'publishedJourneysCount'], channel.publishedJourneysCount)
          .setIn([channel.id + '', 'publishedPathwaysCount'], channel.publishedPathwaysCount);
      });
      return newState;
    case actionTypes.REQUEST_TOGGLE_CHANNEL_FOLLOW_V2:
      return state.setIn([action.id + '', 'followPending'], true);
    case actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW_V2:
      return state
        .setIn([action.id + '', 'following'], action.isFollow)
        .setIn([action.id + '', 'isFollowing'], action.isFollow)
        .setIn([action.id + '', 'followPending'], false);
    case actionTypes.RECEIVE_CHANNEL_CARDS:
      let keyName = action.cardTypes
        ? !!~action.cardTypes.indexOf('video_stream')
          ? 'video_stream'
          : !!~action.cardTypes.indexOf('pack')
          ? 'pack'
          : !!~action.cardTypes.indexOf('course')
          ? 'course'
          : !!~action.cardTypes.indexOf('journey')
          ? 'journey'
          : 'smartbite'
        : 'all';
      channels = state.toJS();
      let channel = channels[action.channelId];
      cards = channel[keyName];
      cards = cards.concat(action.cards);
      cards = uniqBy(cards, 'id');
      if (!window.ldclient.variation('reorder-content', false)) {
        cards = orderBy(cards, 'createdAt', 'desc');
      }
      if (channel.pinnedCardsIds.length) {
        for (let i = 0; i < cards.length; i++) {
          if (!!~channel.pinnedCardsIds.indexOf(cards[i].id)) {
            cards[i].pinnedStatus = true;
          }
        }
      }
      return state.setIn([action.channelId + '', keyName], cards);
    case actionTypes.RECEIVE_CHANNEL_CARDS_AFTER_REORDERING:
      let cardType = action.cardTypes
        ? !!~action.cardTypes.indexOf('video_stream')
          ? 'video_stream'
          : !!~action.cardTypes.indexOf('pack')
          ? 'pack'
          : !!~action.cardTypes.indexOf('course')
          ? 'course'
          : !!~action.cardTypes.indexOf('journey')
          ? 'journey'
          : 'smartbite'
        : 'all';
      let reorderChannels = state.toJS();
      let reorderChannel = reorderChannels[action.channelId];
      cards = reorderChannel[cardType];
      cards = action.cards;
      cards = uniqBy(cards, 'id');
      return state.setIn([action.channelId + '', cardType], cards);
    case actionTypes.CHANGE_OFFSET:
      newState = state;
      let offsetKeyName = action.cardTypes
        ? !!~action.cardTypes.indexOf('video_stream')
          ? 'video_stream'
          : !!~action.cardTypes.indexOf('pack')
          ? 'pack'
          : !!~action.cardTypes.indexOf('course')
          ? 'course'
          : !!~action.cardTypes.indexOf('journey')
          ? 'journey'
          : 'smartbite'
        : 'all';
      return newState.setIn([action.channelId + '', offsetKeyName + 'Offset'], action.offset);
    case actionTypes.UPDATE_CHANNEL_DETAILS:
      newState = state;
      let id = action.newChannel.id;
      newState = newState
        .setIn([id + '', 'label'], action.newChannel.label)
        .setIn([id + '', 'description'], action.newChannel.description)
        .setIn([id + '', 'profileImageUrl'], action.newChannel.profileImageUrl);
      return newState;
    case actionTypes.UPDATE_CHANNEL_SLUG:
      newState = state;
      const slug = action.channel.slug;
      newState = newState.setIn([id + '', 'slug'], slug);
      return newState;
    case actionTypes.UPDATE_CURATORS:
      newState = state;
      channels = newState.toJS();
      channel = channels[action.channelId];
      let curators = channel.curators;
      curators = action.curators;
      return newState.setIn([action.channelId + '', 'curators'], curators);
    case actionTypes.REMOVE_CURATOR:
      newState = state;
      channels = newState.toJS();
      channel = channels[action.channelId];
      let index = null;
      for (let i = 0; i < channel.curators.length; i++) {
        if (channel.curators[i].id == action.curatorId) {
          index = i;
        }
      }
      if (index > -1) {
        channel.curators.splice(index, 1);
      }
      return newState.setIn([action.channelId + '', 'curators'], channel.curators);
    case actionTypes.UPDATE_CHANNEL_CARD_PINNED_STATUS:
      newState = state;
      let pinnedKeyName = action.cardTypes
        ? !!~action.cardTypes.indexOf('video_stream')
          ? 'video_stream'
          : !!~action.cardTypes.indexOf('pack')
          ? 'pack'
          : !!~action.cardTypes.indexOf('course')
          ? 'course'
          : !!~action.cardTypes.indexOf('journey')
          ? 'journey'
          : 'smartbite'
        : 'all';
      channels = newState.toJS();
      channel = channels[action.payload.pinnable_id];
      cards = channel[pinnedKeyName];
      let pinnedCards = channel.pinnedCards;
      let pinnedCount = channel.pinnedCount;
      let pinnedCardsIds = channel.pinnedCardsIds;
      for (let i = 0; i < cards.length; i++) {
        if (cards[i].id == action.payload.object_id) {
          cards[i].pinnedStatus = action.pinnedStatus;
          if (action.pinnedStatus) {
            pinnedCount++;
            pinnedCards.unshift(action.card);
            pinnedCardsIds.unshift(action.card.id);
          } else {
            pinnedCount--;
            pinnedCards = pinnedCards.filter(card => {
              return card.id != action.payload.object_id;
            });
            pinnedCardsIds = pinnedCardsIds.filter(idItem => {
              return idItem != action.payload.object_id;
            });
          }
          break;
        }
      }
      newState = newState
        .setIn([action.payload.pinnable_id + '', pinnedKeyName], cards)
        .setIn([action.payload.pinnable_id + '', 'pinnedCount'], pinnedCount)
        .setIn([action.payload.pinnable_id + '', 'pinnedCards'], pinnedCards)
        .setIn([action.payload.pinnable_id + '', 'pinnedCardsIds'], pinnedCardsIds);
      return newState;
    case actionTypes.UPDATE_CHANNEL_CARD_PINNED_STATUS_IN_ALL:
      newState = state;
      let keyNames = ['smartbite', 'pack', 'video_stream', 'course', 'journey'];
      channels = newState.toJS();
      channel = channels[action.payload.pinnable_id];
      pinnedCards = channel.pinnedCards;
      pinnedCount = channel.pinnedCount;
      pinnedCardsIds = channel.pinnedCardsIds;

      keyNameLoop: for (let i = 0; i < keyNames.length; i++) {
        cards = channel[keyNames[i]];
        cardsLoop: for (let j = 0; j < cards.length; j++) {
          if (cards[j].id == action.payload.object_id) {
            cards[j].pinnedStatus = false;
            pinnedCount--;
            pinnedCards = pinnedCards.filter(card => {
              return card.id != action.payload.object_id;
            });
            pinnedCardsIds = pinnedCardsIds.filter(idItem => {
              return idItem != action.payload.object_id;
            });

            newState = newState
              .setIn([action.payload.pinnable_id + '', keyNames[i]], cards)
              .setIn([action.payload.pinnable_id + '', 'pinnedCount'], pinnedCount)
              .setIn([action.payload.pinnable_id + '', 'pinnedCards'], pinnedCards)
              .setIn([action.payload.pinnable_id + '', 'pinnedCardsIds'], pinnedCardsIds);

            break keyNameLoop;
          }
        }
      }

      return newState;
    case actionTypes.FETCH_PINNED_CARDS:
      newState = state;
      channels = newState.toJS();
      channel = channels[action.channelId];
      pinnedCards = channel.pinnedCards;
      pinnedCards = pinnedCards.concat(action.cards);
      pinnedCardsIds = pinnedCards.map(card => {
        return card.id;
      });
      let smartbites = channel.smartbite;
      let packs = channel.pack;
      let allList = channel.all;
      let videoStreams = channel.video_stream;
      let journeys = channel.journey;
      let courses = channel.course;
      for (let i = 0; i < smartbites.length; i++) {
        if (!!~pinnedCardsIds.indexOf(smartbites[i].id)) {
          smartbites[i].pinnedStatus = true;
        }
      }
      for (let i = 0; i < packs.length; i++) {
        if (!!~pinnedCardsIds.indexOf(packs[i].id)) {
          packs[i].pinnedStatus = true;
        }
      }
      for (let i = 0; i < videoStreams.length; i++) {
        if (!!~pinnedCardsIds.indexOf(videoStreams[i].id)) {
          videoStreams[i].pinnedStatus = true;
        }
      }
      for (let i = 0; i < journeys.length; i++) {
        if (!!~pinnedCardsIds.indexOf(journeys[i].id)) {
          journeys[i].pinnedStatus = true;
        }
      }
      for (let i = 0; i < courses.length; i++) {
        if (!!~pinnedCardsIds.indexOf(courses[i].id)) {
          courses[i].pinnedStatus = true;
        }
      }
      for (let i = 0; i < allList.length; i++) {
        if (!!~pinnedCardsIds.indexOf(allList[i].id)) {
          allList[i].pinnedStatus = true;
        }
      }
      newState = newState
        .setIn([action.channelId + '', 'pinnedCards'], pinnedCards)
        .setIn([action.channelId + '', 'pinnedCardsIds'], pinnedCardsIds)
        .setIn([action.channelId + '', 'smartbite'], smartbites)
        .setIn([action.channelId + '', 'pack'], packs)
        .setIn([action.channelId + '', 'all'], allList)
        .setIn([action.channelId + '', 'course'], courses)
        .setIn([action.channelId + '', 'video_stream'], videoStreams)
        .setIn([action.channelId + '', 'journey'], journeys)
        .setIn([action.channelId + '', 'pinnedCount'], pinnedCardsIds.length);

      return newState;
    case actionTypes.REMOVE_CHANNEL_CARD:
      newState = state;
      let removalKey = action.cardTypes
        ? !!~action.cardTypes.indexOf('video_stream')
          ? 'videoStreamsCount'
          : !!~action.cardTypes.indexOf('pack')
          ? 'publishedPathwaysCount'
          : !!~action.cardTypes.indexOf('course')
          ? 'coursesCount'
          : !!~action.cardTypes.indexOf('journey')
          ? 'publishedJourneysCount'
          : 'smartbitesCount'
        : 'all';
      channels = newState.toJS();
      channel = channels[action.channelId];
      cards = channel[action.cardTypes || ['all']];
      cards = cards.filter(card => {
        return card.id !== action.cardId;
      });
      let cardsCount = channel[removalKey];
      newState = newState
        .setIn([action.channelId + '', action.cardTypes || ['all']], cards)
        .setIn([action.channelId + '', removalKey], cardsCount - 1);
      return newState;
    case actionTypes.OPEN_CURATE_CARD_MODAL:
      newState = state;
      if (action.response) {
        let allCards = action.response.cards;
        let existingCards = action.existingCards;
        let newCards = action.response.cards;
        channel = state.toJS()[action.channel.id];
        if (existingCards && !action.isInitialize && !action.isReopen) {
          allCards = existingCards.concat(newCards);
        }
        if (action.isReopen) {
          allCards = action.response.cards;
        }
        newState = newState.set('activeChannelId', action.channel.id);
        newState = newState
          .setIn([action.channel.id + '', 'curateCards'], allCards)
          .setIn([action.channel.id + '', 'curatedCardsTotal'], action.response.total);
      }

      if (action.publishedCard != undefined) {
        let publishedKeyName =
          action.publishedCard.cardType && !!~action.publishedCard.cardType.indexOf('video_stream')
            ? 'video_stream'
            : action.publishedCard.cardType && !!~action.publishedCard.cardType.indexOf('pack')
            ? 'pack'
            : action.publishedCard.cardTypes && !!~action.publishedCard.cardTypes.indexOf('journey')
            ? 'journey'
            : 'smartbite';
        let publishedCardTypeCount =
          action.publishedCard.cardType && !!~action.publishedCard.cardType.indexOf('video_stream')
            ? 'videoStreamsCount'
            : action.publishedCard.cardType && !!~action.publishedCard.cardType.indexOf('pack')
            ? 'publishedPathwaysCount'
            : action.publishedCard.cardTypes && !!~action.publishedCard.cardTypes.indexOf('journey')
            ? 'publishedJourneysCount'
            : 'smartbitesCount';
        cards = channel[publishedKeyName];
        cards.unshift(action.publishedCard);
        cards.splice(cards.length - 1, 1);
        newState = newState
          .setIn([action.channel.id + '', publishedKeyName], cards)
          .setIn(
            [action.channel.id + '', publishedCardTypeCount],
            ++channel[publishedCardTypeCount]
          );
      }
      return newState;
    case actionTypes.UPDATE_CHANNEL:
      newState = state;
      channels = newState.toJS();
      channel = channels[action.channel.id];

      newState = newState.setIn([action.channel.id + ''], Immutable.fromJS(action.channel));
      newState = newState
        .setIn([action.channel.id + '', 'video_stream'], channel.video_stream)
        .setIn([action.channel.id + '', 'pack'], channel.pack)
        .setIn([action.channel.id + '', 'all'], channel.all)
        .setIn([action.channel.id + '', 'course'], channel.course)
        .setIn([action.channel.id + '', 'journey'], channel.journey)
        .setIn([action.channel.id + '', 'smartbite'], channel.smartbite)
        .setIn([action.channel.id + '', 'smartbiteOffset'], channel.smartbiteOffset)
        .setIn([action.channel.id + '', 'courseOffset'], channel.courseOffset)
        .setIn([action.channel.id + '', 'allOffset'], channel.allOffset)
        .setIn([action.channel.id + '', 'video_streamOffset'], channel.video_streamOffset)
        .setIn([action.channel.id + '', 'packOffset'], channel.packOffset)
        .setIn([action.channel.id + '', 'journeyOffset'], channel.journeyOffset)
        .setIn([action.channel.id + '', 'pinnedCount'], channel.pinnedCount)
        .setIn([action.channel.id + '', 'pinnedCards'], channel.pinnedCards)
        .setIn([action.channel.id + '', 'pinnedCardsIds'], channel.pinnedCardsIds)
        .setIn([action.channel.id + '', 'smartbitesCount'], channel.smartbitesCount)
        .setIn([action.channel.id + '', 'coursesCount'], channel.coursesCount)
        .setIn([action.channel.id + '', 'videoStreamsCount'], channel.videoStreamsCount)
        .setIn([action.channel.id + '', 'profileImageUrl'], channel.profileImageUrl)
        .setIn([action.channel.id + '', 'publishedJourneysCount'], channel.publishedJourneysCount)
        .setIn([action.channel.id + '', 'publishedPathwaysCount'], channel.publishedPathwaysCount);
      return newState;
    default:
      return state;
  }
}
