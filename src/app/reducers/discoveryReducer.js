/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import findIndex from 'lodash/findIndex';

export default function discoveryReducer(
  state = Immutable.Map({
    userIds: [],
    courses: [],
    videos: [],
    channelIds: [],
    pathways: [],
    journeys: [],
    carousels: {}
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_DISCOVERY_COURSES:
    case actionTypes.RECEIVE_COURSES:
      newState = state.set('courses', action.courses);
      return newState;
    case actionTypes.RECEIVE_DISCOVERY_VIDEOS:
      newState = state.set('videos', action.videos);
      return newState;
    case actionTypes.RECEIVE_DISCOVERY_PATHWAYS:
      newState = state.set('pathways', action.pathways);
      return newState;
    case actionTypes.RECEIVE_DISCOVERY_JOURNEYS:
      newState = state.set('journeys', action.journeys);
      return newState;
    case actionTypes.RECEIVE_FEATURED_PROVIDERS:
      let providers = action.featuredProviders;
      newState = state.set('featuredProviders', providers);
      return newState;

    case actionTypes.GET_ALL_DISCOVER_CAROUSEL:
      let carouselsObj = state.get('carousels');
      let carouselDetails = action.isCustomCarousels ? action.customCarousel : action.carosuel;
      let carouselDataArray = action.data;
      let uid = '';

      if (action.isCustomCarousels) {
        carouselDetails.forEach(carousel => {
          uid = carousel.uid;
          carousel['data'] = Object.values(carouselDataArray[carousel.id]).map(data => data.entity);
          carouselsObj[uid] = Immutable.Map(carousel);
        });
      } else {
        uid = carouselDetails.uid;
        carouselDetails['data'] = carouselDataArray;
        carouselsObj[uid] = Immutable.Map(carouselDetails);
      }

      newState = state.merge(carouselsObj);
      return newState;

    case actionTypes.UPDATE_DISCOVER_DATA:
      newState = state;
      let carouselUID = action.carosuel.uid;
      let updatedData = action.data;
      let carouselData = state.get('carousels');
      let existingDataArr = state.get('carousels')[carouselUID].get('data');
      let index = -1;

      index = findIndex(existingDataArr, ['id', updatedData.id]);
      if (index > -1) {
        existingDataArr[index] = updatedData;
      }

      carouselData[carouselUID] = carouselData[carouselUID].set('data', existingDataArr);
      newState = state.set(carouselUID, carouselData[carouselUID]).set('carousels', carouselData);

      return newState;
    default:
      return state;
  }
}
