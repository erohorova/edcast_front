import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

// according to the cases type described in eclActions.js this reducer performs following actions
// it will insert all the results into the results prop which are dispatched from the eclActions.js
// this also handles the getting & adding of cards into to userpathways
// the code is self explanatory

export default function resultsReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.ECL_RESULTS_FOUND:
      newState = state;
      action.results.results.forEach(result => {
        newState = newState.set(result._id + '', Immutable.fromJS(result));
      });
      return newState;
    case actionTypes.CARD_TYPE_CLICKED:
      newState = state;
      action.results.results.forEach(result => {
        newState = newState.set(result._id + '', Immutable.fromJS(result));
      });
      return newState;
    case actionTypes.ORIGIN_TYPE_CLICKED:
      newState = state;
      action.results.results.forEach(result => {
        newState = newState.set(result._id + '', Immutable.fromJS(result));
      });
      return newState;
    case actionTypes.PATHWAYS_FOUND:
      newState = state;
      if (action.pathways.length > 0)
        newState = newState.setIn([action.id + '', 'pathways'], action.pathways);
      newState = newState.setIn([action.id + '', 'pathwayMessage'], '');
      return newState;
    case actionTypes.ADDED_CARD_TO_PATHWAY:
      newState = state;
      newState = newState.setIn(
        [action.cardId + '', 'pathwayMessage'],
        'This Insight has been added to your Pathway.'
      );
      newState = newState.setIn([action.cardId + '', 'pathways'], []);
      return newState;
    case actionTypes.ERROR_WHILE_ADDING_TO_PATHWAY:
      newState = state;
      newState = newState.setIn(
        [action.cardId + '', 'pathwayMessage'],
        'There was an error while adding this insight to your pathway.'
      );
      newState = newState.setIn([action.cardId + '', 'pathways'], []);
      return newState;
    default:
      return state;
  }
}
