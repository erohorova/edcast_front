import { fromJS } from 'immutable';
import {
  SAVE_CHANNEL_DETAILS,
  SAVE_CHANNEL_FEATURED_CARDS,
  REMOVE_CHANNEL_FEATURED_CARDS,
  ADD_FEATURED_CARD_IN_CHANNEL,
  REMOVE_FEATURED_CARD_IN_CHANNEL,
  REMOVE_CARD_FROM_CHANNEL_CAROUSEL,
  REMOVE_CHANNEL_CARD_FROM_CAROUSELS
} from '../constants/actionTypes';

const INITIAL_STATE = fromJS({
  channel: null,
  featuredCards: null,
  featuredCardsIds: null,
  removedCardInfo: null,
  deletedCardId: null
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SAVE_CHANNEL_DETAILS:
      let newState = state.set('channel', action.payload);
      return newState;
    case SAVE_CHANNEL_FEATURED_CARDS:
      newState = state
        .set('featuredCards', action.payload)
        .set('featuredCardsIds', action.payload.map(card => card.id));
      return newState;
    case ADD_FEATURED_CARD_IN_CHANNEL:
      let featuredCards = [action.payload, ...state.get('featuredCards')];
      let featuredCardsIds = [action.payload.id, ...state.get('featuredCardsIds')];
      newState = state
        .set('featuredCards', featuredCards)
        .set('featuredCardsIds', featuredCardsIds);
      return newState;
    case REMOVE_FEATURED_CARD_IN_CHANNEL:
      featuredCards = state.get('featuredCards').filter(card => card.id !== action.payload.id);
      featuredCardsIds = state.get('featuredCardsIds').filter(id => id !== action.payload.id);
      newState = state
        .set('featuredCards', featuredCards)
        .set('featuredCardsIds', featuredCardsIds);
      return newState;
    case REMOVE_CHANNEL_FEATURED_CARDS:
      newState = state.set('featuredCards', null).set('featuredCardsIds', null);
      return newState;
    case REMOVE_CARD_FROM_CHANNEL_CAROUSEL:
      newState = state.set('removedCardInfo', action.payload);
      return newState;
    case REMOVE_CHANNEL_CARD_FROM_CAROUSELS:
      featuredCards = state.get('featuredCards').filter(card => card.id !== action.cardId);
      featuredCardsIds = state.get('featuredCardsIds').filter(id => id !== action.cardId);
      newState = state
        .set('featuredCards', featuredCards)
        .set('featuredCardsIds', featuredCardsIds)
        .set('deletedCardId', action.cardId);
      return newState;
    default:
      return state;
  }
};
