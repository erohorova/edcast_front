export default function routerLocations(state = [], action) {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
      let newState = [...state, action.payload.pathname].slice(-2);
      if (newState.length === 2) {
        localStorage.setItem('prevPath', newState[0]);
      }
      return newState;
    default:
      return state;
  }
}
