/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function coursesReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_COURSES:
      newState = state;
      action.courses.forEach(course => {
        newState = newState.set(course, Immutable.fromJS(course));
      });
      return newState;
    default:
      return state;
  }
}
