import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function configReducer(
  state = Immutable.fromJS({
    topNav: false
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.TOGGLE_TOPNAV:
      newState = state.set('topNav', action.topNavToggle);
      return newState;
    default:
      return state;
  }
}
