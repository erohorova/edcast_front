/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function snackbarReducer(state = Immutable.Map({ open: false }), action) {
  // let newState;
  switch (action.type) {
    case actionTypes.OPEN_SNACKBAR:
      return Immutable.fromJS({
        open: true,
        message: action.message,
        autoClose: true
      });
    case actionTypes.CLOSE_SNACKBAR:
      return state.set('open', false);
    default:
      return state;
  }
}
