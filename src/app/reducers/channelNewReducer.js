import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import findIndex from 'lodash/findIndex';

let INIT = {
  pinnedCards: null,
  pinnedCardIDs: null
};

export default function channelsReducerV2(state = Immutable.fromJS(INIT), action) {
  let newState, pinnedCards, addCard, removeCard, index;
  switch (action.type) {
    case actionTypes.RECIEVE_PINNED_CARDS:
      newState = state;
      newState = newState
        .set('pinnedCards', action.cards)
        .set('pinnedCardIDs', action.cards.map(card => card.id));

      return newState;

    case actionTypes.ADD_TO_PINNED_CARD_CAROUSEL:
      newState = state;
      pinnedCards = state.get('pinnedCards') || [];
      addCard = action.card;
      pinnedCards.unshift(addCard);
      newState = newState
        .set('pinnedCards', pinnedCards)
        .set('pinnedCardIDs', pinnedCards.map(card => card.id));

      return newState;

    case actionTypes.REMOVE_FROM_PINNED_CARD_CAROUSEL:
      newState = state;
      pinnedCards = state.get('pinnedCards') || [];
      removeCard = action.card;
      index = findIndex(pinnedCards, ['id', removeCard.id]);
      if (index > -1) {
        pinnedCards.splice(index, 1);
      }
      newState = newState
        .set('pinnedCards', pinnedCards)
        .set('pinnedCardIDs', pinnedCards.map(card => card.id));

      return newState;
    case actionTypes.REMOVE_ALL_PINNED_CARD:
      newState = state;
      newState = newState.set('pinnedCards', null).set('pinnedCardIDs', null);
    default:
      return state;
  }
}
