import * as actionTypes from '../constants/actionTypes';
import * as modalTypes from '../constants/modalTypes';
import { LOCATION_CHANGE } from 'react-router-redux';
import Immutable from 'immutable';
export default function modalReducer(
  state = Immutable.Map({ open: false, openCard: false, typeBefore: null, dataBefore: {} }),
  action
) {
  let newState;
  switch (action.type) {
    case LOCATION_CHANGE:
    case actionTypes.CLOSE_MODAL:
      return Immutable.fromJS({ open: false, typeBefore: null, dataBefore: {} });
    case actionTypes.CLOSE_CARD_MODAL:
      if (state.toJS().dataBefore == undefined) {
        return Immutable.fromJS({ open: false, typeBefore: null, dataBefore: {} });
      } else {
        return Immutable.fromJS({
          openCard: false,
          open: state.toJS().open,
          typeBefore: state.toJS().typeBefore,
          dataBefore: state.toJS().dataBefore
        });
      }
    case actionTypes.CLOSE_INTEREST_MODAL:
      return Immutable.fromJS({
        open: false,
        reloadInterests: true,
        typeBefore: null,
        dataBefore: {}
      });
    case actionTypes.OPEN_ADD_TO_PATHWAY_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.ADD_TO_PATHWAY,
        cardId: action.cardId,
        card: action.card,
        cardType: action.cardType,
        typeBefore: null,
        dataBefore: {},
        isAddingToJourney: action.isAddingToJourney
      });
    case actionTypes.OPEN_ADD_TO_JOURNEY_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.ADD_TO_JOURNEY,
        cardId: action.cardId
      });
    case actionTypes.OPEN_CONFIRMATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CONFIRM,
        title: action.title ? action.title : 'Confirm',
        message: action.message,
        callback: action.callback,
        hideCancelBtn: action.hideCancelBtn
      });
    case actionTypes.OPEN_PRIVATE_CARD_CONFIRMATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CONFIRM_PRIVATE_CARD,
        title: action.title ? action.title : 'Confirm',
        message: action.message,
        isPrivate: action.isPrivate,
        callback: action.callback
      });
    case actionTypes.OPEN_ASSIGN_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.ASSIGN,
        card: action.card,
        selfAssign: action.selfAssign,
        assignedStateChange: action.assignedStateChange
      });
    case actionTypes.OPEN_CARD_STATS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CARD_STATS,
        card: action.card,
        isViewsModal: action.isViewsModal
      });
    case actionTypes.OPEN_INTEGRATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.INTEGRATION,
        integration: action.integration,
        importClickHandler: action.importClickHandler
      });
    case actionTypes.OPEN_INLINE_CREATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.INLINE_CREATION,
        enabled: action.enabled
      });
    case actionTypes.OPEN_FOLLOW_USER_LIST_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.FOLLOW_USER_LIST,
        typeOfModal: action.typeOfModal
      });
    case actionTypes.OPEN_STATUS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.STATUS_MODAL,
        statusMessage: action.statusMessage,
        handleCloseModal: action.handleCloseModal,
        statusBlock: action.statusBlock
      });
    case actionTypes.OPEN_PATHWAY_PREVIEW_MODAL:
      return Immutable.fromJS({
        openCard: true,
        open: state.toJS().open,
        typeBefore: state.toJS().typeBefore,
        dataBefore: state.toJS().dataBefore,
        type: modalTypes.PATHWAY_PREVIEW_MODAL,
        card: action.card,
        logoObj: action.logoObj,
        defaultImage: action.defaultImage,
        cardUpdated: action.cardUpdated,
        checkedCardId: action.checkedCardId,
        openChannelModal: action.openChannelModal,
        channelSetting: action.channelSetting,
        deleteSharedCard: action.deleteSharedCard,
        groupSetting: action.groupSetting,
        isStandaloneModal: action.isStandaloneModal,
        dueAt: action.dueAt,
        startDate: action.startDate
      });
    case actionTypes.OPEN_JOURNEY_PREVIEW_MODAL:
      return Immutable.fromJS({
        openCard: true,
        open: state.toJS().open,
        typeBefore: state.toJS().typeBefore,
        dataBefore: state.toJS().dataBefore,
        type: modalTypes.JOURNEY_PREVIEW_MODAL,
        card: action.card,
        logoObj: action.logoObj,
        defaultImage: action.defaultImage,
        cardUpdated: action.cardUpdated,
        checkedCardId: action.checkedCardId,
        openChannelModal: action.openChannelModal,
        channelSetting: action.channelSetting,
        deleteSharedCard: action.deleteSharedCard,
        groupSetting: action.groupSetting,
        currentSection: action.currentSection,
        inStandalone: action.inStandalone,
        isStandaloneModal: action.isStandaloneModal,
        dueAt: action.dueAt,
        startDate: action.startDate
      });
    case actionTypes.OPEN_SMARTBITE_PREVIEW_MODAL:
      return Immutable.fromJS({
        openCard: true,
        open: state.toJS().open,
        typeBefore: state.toJS().typeBefore,
        dataBefore: state.toJS().dataBefore,
        type: modalTypes.SMARTBITE_PREVIEW_MODAL,
        card: action.card,
        logoObj: action.logoObj,
        defaultImage: action.defaultImage,
        cardUpdated: action.cardUpdated,
        openChannelModal: action.openChannelModal,
        channelSetting: action.channelSetting,
        deleteSharedCard: action.deleteSharedCard,
        groupSetting: action.groupSetting,
        dueAt: action.dueAt,
        startDate: action.startDate
      });
    case actionTypes.UPDATE_DYNAMIC_SELECTION_FILTERS:
      return Immutable.fromJS({
        filter: action.filter,
        open: true,
        card: state.toJS().card,
        type: modalTypes.SHARE_CONTENT_MODAL
      });
    case actionTypes.UPDATE_DYNAMIC_SELECTION_FILTERS_V2:
      return Immutable.fromJS({
        filter: action.filter,
        open: true,
        card: state.toJS().card,
        currentAction: action.currentAction,
        type: modalTypes.MULTIACTIONS_MODAL
      });
    case actionTypes.OPEN_SMARTBITE_CREATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SMARTBITE_CREATION,
        card: action.card,
        cardUpdated: action.cardUpdated
      });
    case actionTypes.OPEN_PATHWAY_CREATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.PATHWAY_CREATION,
        card: action.card,
        cardUpdated: action.cardUpdated
      });
    case actionTypes.OPEN_JOURNEY_CREATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.JOURNEY_CREATION,
        card: action.card,
        cardUpdated: action.cardUpdated
      });
    case actionTypes.OPEN_REASON_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.REASON_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_MDP_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.MDP_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_COMMENT_REASON_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.REASON_MODAL,
        comment: action.comment
      });
    case actionTypes.OPEN_UPDATE_INTERESTS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_INTERESTS,
        callback: action.callback
      });
    case actionTypes.OPEN_UPDATE_INTERESTS_ONBOARD_V2_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_INTERESTS_ONBOARD_V2,
        callback: action.callback
      });
    case actionTypes.OPEN_UPDATE_MULTILEVEL_INTERESTS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_MULTILEVEL_INTERESTS
      });
    case actionTypes.OPEN_UPDATE_MULTILEVEL_EXPERTISE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_MULTILEVEL_EXPERTISE,
        profileTopic: action.profileTopic
      });
    case actionTypes.OPEN_UPDATE_EXPERTISE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_EXPERTISE,
        callback: action.callback
      });
    case actionTypes.OPEN_INSIGHT_EDIT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.INSIGHT_EDIT,
        card: action.card,
        cardUpdated: action.cardUpdated
      });
    case actionTypes.OPEN_UPLOAD_IMAGE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPLOAD_IMAGE,
        imageType: action.imageType,
        sizes: action.sizes,
        updatedImage: action.updatedImage
      });
    case actionTypes.OPEN_INVITE_USER_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.INVITE_USER,
        group: action.groupInfo
      });
    case actionTypes.OPEN_INVITE_V2_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.INVITE_V2_USER_TO_GROUP
      });
    case actionTypes.OPEN_UPDATE_CHANNEL_DETAILS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_CHANNEL_DETAILS,
        id: action.id,
        label: action.label,
        description: action.description
      });
    case actionTypes.OPEN_UPDATE_GROUP_DETAILS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UPDATE_GROUP_DETAILS
      });
    case actionTypes.OPEN_GROUP_CREATION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.GROUP_CREATION,
        updateGroupsList: action.updateGroupsList,
        groupId: action.groupId
      });
    case actionTypes.OPEN_CHANNEL_CARDS_MODAL:
      return Immutable.fromJS({
        open: true,
        typeBefore: modalTypes.CHANNEL_CARDS,
        dataBefore: action,
        type: modalTypes.CHANNEL_CARDS,
        modalTitle: action.modalTitle,
        count: action.count,
        cards: action.cards,
        channel: action.channel,
        editMode: action.editMode,
        cardModalDetails: action.cardModalDetails
      });
    case actionTypes.OPEN_TEAM_CARD_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.GROUP_CARDS,
        deleteSharedCard: action.deleteSharedCard,
        cards: action.cards,
        cardModalDetails: action.cardModalDetails,
        group: action.group,
        isViewAll: action.isViewAll
      });
    case actionTypes.OPEN_GROUP_MEMBER_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.GROUP_MEMBERS,
        userType: action.userType,
        isInitilized: action.isInitilized
      });
    case actionTypes.OPEN_ADD_CURATORS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CHANNEL_ADD_CURATORS,
        channelId: action.channelId,
        curators: action.curators,
        currentUserId: action.currentUserId
      });
    case actionTypes.OPEN_GROUP_INVITE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.GROUP_ADD_LEADER,
        userType: action.userType
      });
    case actionTypes.OPEN_CURATE_CARD_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CURATE_CARD_MODAL,
        channel: action.channel,
        channelId: action.channel.id
      });
    case actionTypes.OPEN_CHANNEL_CARD_REMOVE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CHANNEL_CARD_REMOVE,
        card: action.card,
        channel: action.channel,
        cardTypes: action.cardTypes,
        isReject: action.isReject,
        rejectCallback: action.rejectCallback
      });
    case actionTypes.OPEN_CARD_ACTION_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CARD_ACTION,
        card: action.card,
        typeOfModal: action.typeOfModal
      });
    case actionTypes.UPDATE_DATA_IN_MODAL:
      newState = state;
      newState = newState.set('dataBefore', Immutable.fromJS(action));
      return newState;
    case actionTypes.OPEN_PATHWAY_CONGRATULATION_MODAL:
      return Immutable.fromJS({
        open: true,
        userBadge: action.userBadge,
        type: modalTypes.CONGRATULATION_MODAL
      });
    case actionTypes.OPEN_RELEVANCY_RATING_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.RELEVANCY_RATING_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_SHOW_CHANNELS_MODAL:
      newState = state;
      newState = newState.set('removable', action.removable);
      newState = newState.set('open', true);
      newState = newState.set('type', modalTypes.SHOW_CHANNELS_MODAL);
      return newState;
    case actionTypes.OPEN_SHARE_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SHARE_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_MULTIACTIONS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.MULTIACTIONS_MODAL,
        currentAction: action.currentAction,
        card: action.card,
        selfAssign: action.selfAssign
      });
    case actionTypes.OPEN_POST_TO_CHANNEL_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.POST_TO_CHANNEL_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_SHOW_SKILL_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SKILL_CREATION,
        skill: action.skill
      });
    case actionTypes.OPEN_PAYMENT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.ORG_PAYMENT
      });
    case actionTypes.OPEN_WALLET_PAYMENT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.WALLET_PAYMENT,
        rechargeData: action.rechargeData
      });
    case actionTypes.OPEN_CARD_WALLET_PAYMENT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CARD_WALLET_PAYMENT,
        paymentData: action.paymentData
      });
    case actionTypes.OPEN_PAYPAL_SUCCESS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.PAYPAL_SUCCESS_MODAL,
        paymentData: action.paymentData
      });
    case actionTypes.OPEN_COURSES_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.COURSES_MODAL,
        courseStatus: action.courseStatus,
        courseType: action.courseType
      });
    case actionTypes.OPEN_TEAM_ANALYTICS_USER_LIST_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.TEAM_ANALYTICS_USER_LIST_MODAL,
        metric: action.metric,
        modalState: action.modalState
      });
    case actionTypes.OPEN_CHANNEL_EDIT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CHANNEL_EDIT_MODAL,
        channel: action.channel
      });
    case actionTypes.OPEN_SHARE_CONTENT_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SHARE_CONTENT_MODAL,
        card: action.card
      });
    case actionTypes.OPEN_CHANGE_AUTHOR_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CHANGE_AUTHOR_MODAL,
        card: action.card,
        removeCardFromList: action.removeCardFromList,
        changeAuthorOptionRemoval: action.changeAuthorOptionRemoval,
        removeCardFromCardContainer: action.removeCardFromCardContainer,
        cardSectionName: action.cardSectionName
      });
    case actionTypes.OPEN_SLIDE_OUT_CARD_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SLIDE_OUT_CARD_MODAL,
        card: action.card,
        authorName: action.authorName,
        toggleChannelModal: action.toggleChannelModal,
        cardUpdated: action.cardUpdated,
        defaultImage: action.defaultImage,
        logoObj: action.logoObj,
        dismissible: action.dismissible,
        ratingCount: action.ratingCount,
        handleCardClicked: action.handleCardClicked,
        standaloneLinkClickHandler: action.standaloneLinkClickHandler,
        rateCard: action.rateCard,
        providerLogos: action.providerLogos
      });
    case actionTypes.OPEN_UNBOOKMARK_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.UNBOOKMARK_MODAL,
        bookmarked: action.bookmarked,
        confirmHandler: action.confirmHandler
      });
    case actionTypes.OPEN_SKILLS_DIRECTORY_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.SKILLS_DIRECTORY_MODAL,
        changeRole: action.changeRole
      });
    case actionTypes.OPEN_CHANNEL_CURATE_CARDS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.OPEN_CHANNEL_CURATE_CARDS_MODAL
      });
    case actionTypes.OPEN_CHANNEL_CAROUSEL_CARDS_MODAL:
      return Immutable.fromJS({
        open: true,
        type: modalTypes.CHANNEL_CAROUSEL_CARDS_MODAL,
        carouselCards: action.carouselCards,
        carouselData: action.carouselData,
        totalCarouselCardsCount: action.totalCarouselCardsCount,
        showChannelCardConfig: action.showChannelCardConfig
      });
    default:
      return state;
  }
}
