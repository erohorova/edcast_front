import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import remove from 'lodash/remove';
let currentYearVal = new Date().getFullYear();

let INIT = {
  searchQuery: '',
  activeContentTab: 'current',
  activeContentTypeTab: 'quarter',
  userInterest: Immutable.Map([]),
  completed: Immutable.Map({
    quarter: Immutable.Map({}),
    competency: Immutable.Map({})
  }),
  current: Immutable.Map({
    quarter: Immutable.Map({}),
    competency: Immutable.Map({})
  }),
  currentYearObj: { value: currentYearVal + '', label: currentYearVal + '' }
};

export default function mlpv5(state = Immutable.Map(INIT), action) {
  let activeContentTab = 'current';
  let activeContentTypeTab = 'quarter';
  let contentData = {};
  let existingContentData = {};
  let newState, assignmentKey;

  switch (action.type) {
    case actionTypes.CONTENT_TAB_CHANGE:
      newState = Immutable.Map(INIT);
      return newState.set('activeContentTab', action.activeTab);
    case actionTypes.CHANGE_YEAR_OBJ:
      newState = state;
      return newState.set('currentYearObj', action.currentYearObj);

    case actionTypes.CHANGE_YEAR:
      newState = state;
      return newState.set('currentYear', action.currentYear);

    case actionTypes.CONTENT_TYPE_TAB_CHANGE:
      newState = state;
      return newState.set('activeContentTypeTab', action.activeTab);

    case actionTypes.GET_USER_INTEREST_FOR_MLP:
      newState = state;
      return newState.set('userInterest', action.userInterest);

    case actionTypes.STORE_QUARTERLY_ASSIGNMENTS:
      newState = state;
      activeContentTab = action.activeContentTab;
      contentData = {};
      existingContentData = state.toJS()[activeContentTab];
      contentData[action.data.mlpPeriod] = action.data;
      newState = newState
        .setIn([activeContentTab, 'quarter'], { ...existingContentData['quarter'], ...contentData })
        .set('searchQuery', action.searchQuery);
      return newState;

    case actionTypes.STORE_COMPETENCY_ASSIGNMENTS:
      newState = state;
      activeContentTab = action.activeContentTab;
      contentData = {};
      existingContentData = state.toJS()[activeContentTab];
      assignmentKey =
        action.data.mlpPeriod == 'other' ? action.data.mlpPeriod : action.data.learningTopicsName;
      contentData[assignmentKey] = action.data;
      newState = newState
        .setIn([activeContentTab, 'competency'], {
          ...existingContentData['competency'],
          ...contentData
        })
        .set('searchQuery', action.searchQuery);
      return newState;

    case actionTypes.LOAD_MORE_ASSIGNMENTS:
      newState = state;
      activeContentTab = action.activeContentTab;
      activeContentTypeTab = state.toJS().activeContentTypeTab;
      existingContentData = state.toJS()[activeContentTab];
      assignmentKey = action.data.learningTopicsName || action.data.mlpPeriod;
      let existingAssignmentData =
        existingContentData[activeContentTypeTab][assignmentKey]['assignments'];
      let newAssignmentData = [...existingAssignmentData, ...action.data.assignments];
      existingContentData[activeContentTypeTab][assignmentKey]['assignments'] = newAssignmentData;
      newState[activeContentTab] = existingContentData;
      newState = newState.setIn(
        [activeContentTab, activeContentTypeTab],
        existingContentData[activeContentTypeTab]
      );
      return newState;

    case actionTypes.SEARCH_RESULT:
      newState = state;
      return newState;
    case actionTypes.CLEAN_ASSIGNMENTS:
      newState = state;
      activeContentTab = action.activeContentTab;
      contentData = {};
      existingContentData = state.toJS()[activeContentTab];
      activeContentTypeTab = state.toJS().activeContentTypeTab;
      newState = newState.setIn([activeContentTab, activeContentTypeTab], {
        ...existingContentData[activeContentTypeTab],
        ...contentData
      });
      return newState;
    // reducer for remove item from assessment list in my learning plan
    case actionTypes.REMOVE_DISMISS_CARD:
      let data = state.toJS();
      // flag for checking removed card from list
      let removedCard = false;
      // check card available in quarter wise assign section
      for (let quarterNum = 1; quarterNum < 5; quarterNum++) {
        if (!removedCard) {
          if (data.current.quarter[`quarter-${quarterNum}`]) {
            remove(data.current.quarter[`quarter-${quarterNum}`].assignments, assessmentCard => {
              if (assessmentCard.assignable.id === action.card.id) {
                removedCard = true;
              }
              return assessmentCard.assignable.id === action.card.id;
            });
          }
        } else {
          break;
        }
      }
      // check for card available in untimed section or card already removed
      if (data.current.quarter['untimed'] && !removedCard) {
        remove(data.current.quarter[`untimed`].assignments, assessmentCard => {
          return assessmentCard.assignable.id === action.card.id;
        });
      }
      newState = Immutable.fromJS(data);
      return newState;
    default:
      return state;
  }
}
