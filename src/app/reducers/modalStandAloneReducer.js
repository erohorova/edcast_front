import * as actionTypes from '../constants/actionTypes';
// import * as modalTypes from "../constants/modalTypes";
import { LOCATION_CHANGE } from 'react-router-redux';
import Immutable from 'immutable';
export default function modalStandAloneReducer(state = Immutable.Map({ open: false }), action) {
  switch (action.type) {
    case LOCATION_CHANGE:
    case actionTypes.CLOSE_STANDALONE_CARD_MODAL:
      if (action.dataCard && action.dataCard.cardSplat) {
        return Immutable.fromJS({
          open: true,
          card: { slug: action.dataCard.cardSplat },
          cardType: action.dataCard.fromType
        });
      } else {
        return Immutable.fromJS({
          open: false
        });
      }
    case actionTypes.OPEN_STANDALONE_MODAL:
      return Immutable.fromJS({
        open: true,
        card: action.card,
        cardType: action.cardType,
        dataCard: action.dataCard,
        cardUpdated: action.cardUpdated
      });
    default:
      return state;
  }
}
