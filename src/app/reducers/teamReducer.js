/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function teamReducer(state = Immutable.Map({ pending: true }), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_ORG_INFO:
      newState = state;
      action.data.ssos.map(function(sso) {
        if (sso.name === 'email') {
          newState = newState.set('hasEmailLogin', true);
        }
        if (sso.name !== 'email') {
          newState = newState.set('hasNonEmailLogin', true);
        }
      });
      let configs = {};
      action.data.configs.forEach(config => {
        configs[config.name] = config.value;
      });
      newState = newState
        .set('config', configs)
        .set('pending', false)
        .set('ssos', action.data.ssos)
        .set('OrgConfig', action.data.OrgConfig.web)
        .set('Feed', action.data.OrgConfig.feed)
        .set('Discover', action.data.OrgConfig.discover)
        .set('coBrandingLogo', action.data.coBrandingLogo)
        .set('bannerImage', action.data.bannerUrl)
        .set('favicons', action.data.favicons)
        .set('orgId', action.data.id)
        .set('name', action.data.name)
        .set('orgSubscriptions', action.data.orgSubscriptions)
        .set('memberPay', action.data.memberPay)
        .set('readableCardTypes', action.data.readableCardTypes)
        .set('fromEmailAddress', action.data.fromAddress)
        .set('paymentGateways', configs.payment_gateway);
      return newState;
    case actionTypes.ERROR_INIT_ORG:
      return state.set('invalidTeamName', true).set('pending', false);
    case actionTypes.RECEIVE_FEED_TYPE_VIEW:
      newState = state;
      let Feed = Object.assign({}, newState.get('Feed'));
      Feed[action.feedKey].defaultCardView = action.viewRegime;
      newState = newState.set('Feed', Feed);
      return newState;
    case actionTypes.RECEIVE_WALLET_RECHARGES:
      newState = state;
      newState = newState.set(
        'walletRecharges',
        (action.walletRecharges && action.walletRecharges.recharges) || []
      );
      return newState;
    default:
      return state;
  }
}
