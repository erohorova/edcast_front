import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function assignmentsReducer(
  state = Immutable.fromJS({
    cardIds: [],
    informalAssignments: [],
    formalAssignments: [],
    formalAssignmentsCount: 0,
    informalAssignmentsCount: 0,
    assignorsNameMappings: {},
    assignorsIdList: [],
    assignmentCountShouldBeUpdated: false,
    assignedUsers: []
  }),
  action
) {
  let newState, newCardIds;
  switch (action.type) {
    case actionTypes.REQUEST_USER_ASSIGNMENTS:
      newState = state.set('pending', true);
      if (action.isInit) {
        newState = newState.set('cardIds', Immutable.List()).set('isLastPage', false);
      }
      return newState;
    case actionTypes.RECEIVE_USER_ASSIGNMENTS:
      newState = state;
      newCardIds = state.get('cardIds');
      if (action.assignments.length === 0) {
        newState = newState.set('isLastPage', true);
      }
      action.assignments.forEach(assignment => {
        newCardIds = newCardIds.push(assignment.assignable.id + '');
      });
      newState = newState.set('cardIds', newCardIds).set('pending', false);
      return newState;
    case actionTypes.UPDATE_ASSIGNMENT_COUNT:
      newState = state.set('assignmentCountShouldBeUpdated', true);
      return newState;
    case actionTypes.UPDATE_ASSIGNMENT_COUNT_DONE:
      newState = state.set('assignmentCountShouldBeUpdated', false);
      return newState;
    case actionTypes.RECEIVE_ASSIGNEMENTS:
      let assignments = [];
      let newAssignments = [];
      let assignorsIdList = [];
      let assignorsNameMappings = {};
      let section = action.section;
      newState = state;

      if (section === 'informal' || section === 'formal') {
        assignments = state.toJS()[`${section}Assignments`];
        if (action.offset === 0) {
          assignments = [];
        }
        assignments = assignments.concat(action.data.assignments);
        assignments = assignments.filter(assignment => {
          return assignment.teams[0] && assignment.teams[0].assignor.id != action.currentUserId;
        });

        newAssignments = assignments;
        assignments.map((assignment, index) => {
          if (
            assignment.teams[0] &&
            assignment.teams[0].assignor.id &&
            !assignorsIdList.includes(assignment.teams[0].assignor.id)
          ) {
            assignorsIdList.push(assignment.teams[0].assignor.id);

            assignorsNameMappings[assignment.teams[0].assignor.id] = `${
              assignment.teams[0] ? assignment.teams[0].assignor.email : 'Anonymous'
            }<NAME>${assignment.teams[0] ? assignment.teams[0].assignor.name : 'Anonymous'}`;
          }
        });

        newState = state
          .set(`${section}Assignments`, newAssignments)
          .set(`${section}AssignmentsCount`, action.data.assignmentCount)
          .set('assignorsIdList', assignorsIdList)
          .set('assignorsNameMappings', assignorsNameMappings);
      }

      return newState;

    case actionTypes.GET_ASSIGNED_USERS_BY_METRICS:
      newState = state;
      newState = state.set('assignedUsers', action.metricData);
      return newState;

    default:
      return state;
  }
}
