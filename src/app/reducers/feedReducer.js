/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function feedReducer(state = Immutable.fromJS({ cardIds: [] }), action) {
  let newState, newCardIds;

  switch (action.type) {
    //feed
    case actionTypes.REQUEST_USER_FEED:
      return state.set('pending', true);
    case actionTypes.FIRST_FEED_TAB:
      return state.set('firstFeedTab', action.tab);
    case actionTypes.RECEIVE_PROMOTED_FEED:
      let numberOfNewPromotedCards = 0;
      newState = state;
      if (action.isInit) {
        newCardIds = Immutable.List();
        newState = newState.set('isLastPage', false);
      } else {
        newCardIds = newState.get('cardIds');
      }
      let cardsAndStreams = action.feed[0].items.concat(action.feed[1].items);
      cardsAndStreams.sort((a, b) => {
        return new Date(b.timestampWithZone) - new Date(a.timestampWithZone);
      });
      cardsAndStreams.forEach(item => {
        newCardIds = newCardIds.push(item.id + '');
        numberOfNewPromotedCards++;
      });
      newState = newState.set('cardIds', newCardIds).set('pending', false);
      if (numberOfNewPromotedCards === 0) {
        newState = newState.set('isLastPage', true);
      }
      return newState;
    default:
      return state;
  }
}
