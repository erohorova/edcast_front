/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function curateReducer(
  state = Immutable.fromJS({ cardIds: [], totalCount: 0 }),
  action
) {
  let newState, newCardIds;
  switch (action.type) {
    case actionTypes.REQUEST_CURATE_CARDS:
      return state.set('pending', true);
    case actionTypes.RECEIVE_CURATE_CARDS:
      newState = state;
      newCardIds = Immutable.List([]);
      action.cards.forEach(card => {
        newCardIds = newCardIds.push(card.id + '');
      });
      newState = newState
        .set('cardIds', newCardIds)
        .set('pending', false)
        .set('totalCount', action.totalCount);
      return newState;
    case actionTypes.RECEIVE_CURATE_CHANGE_STATE:
      newState = state;
      newCardIds = newState.get('cardIds').forEach((id, index) => {
        if (id === action.cardId) {
          newState = newState.deleteIn(['cardIds', index]);
        }
      });
      newState = newState.update('totalCount', count => count - 1);
      return newState;
    default:
      return state;
  }
}
