import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function journeyReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_JOURNEYS:
      newState = state.set('journeys', action.journeys);
      return newState;
    case actionTypes.SAVE_TEMP_JOURNEY:
      newState = state.set('tempJourney', action.card);
      return newState;
    case actionTypes.DELETE_TEMP_JOURNEY:
      newState = state.set('tempJourney', null);
      return newState;
    case actionTypes.SAVE_REORDER_CARD_IDS_JOURNEY:
      newState = state.set('reorderCardIds', action.cardsIds);
      return newState;
    case actionTypes.REMOVE_REORDER_CARD_IDS_JOURNEY:
      newState = state.remove('reorderCardIds');
      return newState;
    case actionTypes.IS_PREVIEW_MODE_JOURNEY:
      newState = state.set('isPreviewMode', action.bool);
      return newState;
    case actionTypes.SAVE_CONSUMPTION_JOURNEY:
      newState = state.set('consumptionJourney', action.card);
      return newState;
    case actionTypes.SAVE_CONSUMPTION_JOURNEY_HISTORY_URL:
      if (
        state.get('consumptionJourneyOpenBlock') &&
        state.get('consumptionJourneyPathwayPayload')
      ) {
        newState = state
          .remove('consumptionJourneyOpenBlock')
          .remove('consumptionJourneyPathwayPayload')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else if (state.get('consumptionJourneyOpenBlock')) {
        newState = state
          .remove('consumptionJourneyOpenBlock')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else if (state.get('consumptionJourneyPathwayPayload')) {
        newState = state
          .remove('consumptionJourneyPathwayPayload')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else {
        newState = state.set('consumptionJourneyHistoryUrl', action.url);
      }
      return newState;
    case actionTypes.SAVE_CONSUMPTION_JOURNEY_OPEN_BLOCK:
      newState = state.set('consumptionJourneyOpenBlock', action.openBlock);
      return newState;
    case actionTypes.SAVE_CONSUMPTION_JOURNEY_PATHWAY_PAYLOAD:
      newState = state.set('consumptionJourneyPathwayPayload', action.payload);
      return newState;
    case actionTypes.CONSUMPTION_JOURNEY_HISTORY:
      newState = state.set('consumptionJourneyHistory', action.card);
      return newState;
    case actionTypes.REMOVE_CONSUMPTION_JOURNEY:
      if (
        state.get('consumptionJourneyOpenBlock') &&
        state.get('consumptionJourneyPathwayPayload')
      ) {
        newState = state
          .remove('consumptionJourney')
          .remove('consumptionJourneyOpenBlock')
          .remove('consumptionJourneyPathwayPayload')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else if (state.get('consumptionJourneyOpenBlock')) {
        newState = state
          .remove('consumptionJourney')
          .remove('consumptionJourneyOpenBlock')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else if (state.get('consumptionJourneyPathwayPayload')) {
        newState = state
          .remove('consumptionJourney')
          .remove('consumptionJourneyPathwayPayload')
          .set('consumptionJourneyHistoryUrl', action.url);
      } else {
        newState = state
          .remove('consumptionJourney')
          .set('consumptionJourneyHistoryUrl', action.url);
      }
      return newState;
    case actionTypes.REMOVE_CONSUMPTION_JOURNEY_HISTORY:
      newState = state.remove('consumptionJourneyHistory');
      return newState;
    default:
      return state;
  }
}
