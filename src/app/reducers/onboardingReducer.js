import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function onboardingReducer(
  state = Immutable.Map({
    step: 0,
    user: {},
    topics: [],
    expertiseTopics: [],
    skills: {},
    expertiseSkills: {},
    sources: {},
    influencers: [],
    onboarding_completed: false,
    isLoaded: false,
    isLastLearningTopicPage: false,
    isLastExpertiseTopicPage: false
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_INIT_ONBOARDING_STATE:
      newState = state
        .set('step', action.onboardingState.step)
        .set('user', Immutable.fromJS(action.onboardingState.user))
        .set('onboarding_completed', action.onboardingState.onboarding_completed)
        .set('isLoaded', true);
      return newState;
    case actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER:
      newState = state.set('topics', action.topics);
      return newState;
    case actionTypes.RECEIVE_SOURCE_SUGGESTIONS_USER:
      newState = state.set('sources', action.sources.websites);
      return newState;
    case actionTypes.RECEIVE_UPDATED_USER_INFO:
      if (!action.user.avatar) {
        action.user.avatar = state.toJS().user.avatar;
      }
      newState = state.set('user', Immutable.fromJS(action.user));
      return newState;
    case actionTypes.RECEIVE_UPDATED_USER_PREFS:
      let user = state.get('user').toJS();
      user.user_interests = action.user.user_interests;
      newState = state.set('user', Immutable.fromJS(user));
      return newState;
    case actionTypes.UPDATE_ONBOARDING_USER:
      newState = state.set('user', Immutable.fromJS(action.user));
      break;
    case actionTypes.RECEIVE_RECOMMENDED_INFLUENCERS:
      newState = state.set(
        'influencerIds',
        Immutable.fromJS(action.influencers.map(userItem => userItem.id + ''))
      );
      return newState;
    case actionTypes.RECEIVE_RECOMMENDED_CHANNELS:
      newState = state.set(
        'channelIds',
        Immutable.fromJS(action.channels.map(channel => channel.id + ''))
      );
      return newState;
    case actionTypes.UPDATE_ONBOARDING_STEP:
      newState = state.set('step', action.step);
      return newState;
    case actionTypes.RECEIVE_ORG_INFO:
      newState = state;
      newState = newState
        .set('coBrandingLogo', action.data.coBrandingLogo)
        .set('teamName', action.data.name);

      if (action.data.configs) {
        for (let i = 0; i < action.data.configs.length; i++) {
          if (action.data.configs[i].name == 'default_topics') {
            newState = newState.set('defaultTopics', action.data.configs[i].value.defaults);
          } else if (action.data.configs[i].name == 'acceptancePromptEnabled') {
            newState = newState.set('acceptancePromptEnabled', action.data.configs[i].value);
          } else if (action.data.configs[i].name == 'acceptancePromptMessage') {
            newState = newState.set('acceptancePromptMessage', action.data.configs[i].value);
          } else if (action.data.configs[i].name == 'acceptancePromptLink') {
            newState = newState.set('acceptancePromptLink', action.data.configs[i].value);
          }
        }
      }

      return newState;
    case actionTypes.ERROR_INIT_ORG:
      return state.set('invalidTeamName', true).set('pending', false);
    case actionTypes.RECEIVE_TOPIC_SUGGESTIONS_USER_ONBOARDING:
      newState = state;
      let topics = [];

      if (action.section == 'expertiseSection') {
        topics = action.expertiseTopics.domains;
      } else {
        topics = action.topics.domains;
      }

      if (topics.length < 12) {
        newState =
          action.section == 'expertiseSection'
            ? newState.set('isLastExpertiseTopicPage', true)
            : newState.set('isLastLearningTopicPage', true);
      }

      let newTopics = [];

      newTopics =
        action.section == 'expertiseSection' ? state.toJS().expertiseTopics : state.toJS().topics;
      topics.forEach(function(element) {
        newTopics.push(element);
      });

      if (action.section == 'expertiseSection') {
        newState = newState.set('expertiseTopics', Immutable.fromJS(newTopics));
      } else {
        newState = newState.set('topics', Immutable.fromJS(newTopics));
      }

      return newState;
    case actionTypes.RECEIVE_TOPIC_SKILLS:
      newState = state;
      let skills = action.data.topics;

      let obj =
        action.section == 'expertiseSection' ? state.toJS().expertiseSkills : state.toJS().skills;

      // Need structuring as follows:
      // {
      //   skills: {
      //    skills-0(row = 0): {
      //      skill-aaaa(topic_id = aaaa): {
      //        isLastSkillPage: false,
      //        data: []
      //      }
      //    }
      //   }
      // }

      // If skills returned are 0 for particular topic
      // then set flag for last page and data will be same as earlier
      if (skills.length > 0) {
        // Check if key for Topic row skills exists or not
        // If not initialise one first then add data
        if (obj.hasOwnProperty(`skills-${action.rowIndex}`)) {
          // Check if key for particular topic skills exists or not
          // If not initialise first with default state then add data
          if (!obj[`skills-${action.rowIndex}`].hasOwnProperty(`skill-${action.data.domain.id}`)) {
            obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`] = {
              isLastSkillPage: false,
              data: []
            };
          }
          obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`]['data'] = obj[
            `skills-${action.rowIndex}`
          ][`skill-${action.data.domain.id}`]['data'].concat(skills);
        } else {
          obj[`skills-${action.rowIndex}`] = {};
          obj[`skills-${action.rowIndex}`][`skill-${action.data.domain.id}`] = {
            isLastSkillPage: false,
            data: skills
          };
        }
      } else {
        if (obj.hasOwnProperty(`skills-${action.rowIndex}`)) {
          obj[`skills-${action.rowIndex}`][`skill-${action.topicId}`] = {
            isLastSkillPage: true,
            data: obj[`skills-${action.rowIndex}`][`skill-${action.topicId}`]['data']
          };
        }
      }

      newState =
        action.section == 'expertiseSection'
          ? newState.set('expertiseSkills', Immutable.fromJS(obj))
          : newState.set('skills', Immutable.fromJS(obj));
      return newState;
    case actionTypes.ERROR_FETCHING_SKILLS:
      return state.set('Error', true);
    case actionTypes.RECEIVE_USER_PROFILE:
      user = state.toJS().user;
      user.profile = action.user.profile;
      newState = state.set('user', Immutable.fromJS(user));
      return newState;
    default:
      return state;
  }
}
