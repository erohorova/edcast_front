import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import _ from 'lodash';

export default function groupsReducerV2(state = Immutable.Map(), action) {
  let newState;
  let currentGroupID;
  switch (action.type) {
    case actionTypes.RECEIVE_GROUPS:
      newState = state;
      action.groups.forEach(group => {
        let groupID = group.id;
        newState = newState.set('currentGroupID', Immutable.fromJS(groupID));
        newState = newState.set(groupID + '', Immutable.fromJS(group));
        newState = newState
          .setIn([groupID + '', 'sharedCards'], [])
          .setIn([groupID + '', 'sharedCardsLoaded'], false)
          .setIn([groupID + '', 'teamAssignments'], [])
          .setIn([groupID + '', 'teamAssignmentsLoaded'], false)
          .setIn([groupID + '', 'teamChannels'], [])
          .setIn([groupID + '', 'teamChannelsLoaded'], false)
          .setIn([groupID + '', 'featured'], [])
          .setIn([groupID + '', 'featuredLoaded'], false)
          .setIn([groupID + '', 'members'], group.members || [])
          .setIn([groupID + '', 'pending'], group.pending || [])
          .setIn([groupID + '', 'sharedCardsCount'], 0)
          .setIn([groupID + '', 'teamAssignmentsCount'], 0)
          .setIn([groupID + '', 'featuredCount'], 0)
          .setIn([groupID + '', 'teamChannelsCount'], 0)
          .setIn([groupID + '', 'allMembersCount'], group.membersCount)
          .setIn([groupID + '', 'membersTotal'], group.membersCount)
          .setIn([groupID + '', 'pendingTotal'], group.pendingMembersCount)
          .setIn([groupID + '', 'cardsLimit'], 12)
          .setIn([groupID + '', 'channelsLimit'], 12)
          .setIn([groupID + '', 'membersLimit'], 15)
          .setIn([groupID + '', 'pendingLimit'], 15)
          .setIn([groupID + '', 'teamChannelsLimit'], 15)
          .setIn([groupID + '', 'sharedCardsOffset'], 0)
          .setIn([groupID + '', 'teamAssignmentsOffset'], 0)
          .setIn([groupID + '', 'teamChannelsOffset'], 0)
          .setIn([groupID + '', 'membersOffset'], 0)
          .setIn([groupID + '', 'pendingOffset'], 0)
          .setIn([groupID + '', 'cardAryName'], '')
          .setIn([groupID + '', 'cardModalDesc'], '')
          .setIn([groupID + '', 'cardModalCount'], 0)
          .setIn([groupID + '', 'cardModalType'], '')
          .setIn([currentGroupID + '', 'owners'], group.owners || []);
      });
      return newState;

    case actionTypes.UPDATE_GROUP_DETAILS:
      newState = state;
      currentGroupID = state.toJS().currentGroupID;
      newState = newState
        .setIn([currentGroupID + '', 'name'], action.group.name)
        .setIn([currentGroupID + '', 'description'], action.group.description)
        .setIn([currentGroupID + '', 'imageUrls'], action.group.imageUrls);
      return newState;

    case actionTypes.UPDATE_GROUP_DETAILS_V2:
      newState = state;
      currentGroupID = state.toJS().currentGroupID;
      newState = newState
        .setIn([currentGroupID + '', 'name'], action.group.name)
        .setIn([currentGroupID + '', 'description'], action.group.description)
        .setIn([currentGroupID + '', 'imageUrls'], action.group.imageUrls)
        .setIn([currentGroupID + '', 'isPrivate'], action.group.isPrivate);
      return newState;

    case actionTypes.UPDATE_MEMBERS:
      newState = state;
      currentGroupID = state.toJS().currentGroupID;
      newState = newState
        .setIn([currentGroupID + '', 'owners'], action.group.owners)
        .setIn([currentGroupID + '', 'subAdmins'], action.group.subAdmins)
        .setIn([currentGroupID + '', 'members'], action.group.members);

      return newState;

    case actionTypes.UPDATE_GROUP_CAROUSEL_ORDER:
      newState = state;
      currentGroupID = state.toJS().currentGroupID;
      newState = newState.setIn(
        [currentGroupID + '', 'carousels_order_change'],
        action.carouselOrderChange
      );
      return newState;

    case actionTypes.GET_TEAM_CHANNELS:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      let groupD = newState.toJS()[currentGroupID];
      let channels = groupD['teamChannels'];
      channels =
        action.isAfterRemove === true ? action.channels : [...channels, ...action.channels];
      channels = _.uniqBy(channels, 'id');
      newState = newState.setIn([currentGroupID + '', 'teamChannels'], channels);
      newState = newState.setIn([currentGroupID + '', 'teamChannelsLoaded'], true);
      newState = newState.setIn([currentGroupID + '', 'teamChannelsCount'], action.total);
      newState = newState.setIn([currentGroupID + '', 'teamChannelsOffset'], action.offset);
      return newState;

    case actionTypes.ADD_PENDING_MEMBER:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      let groupPendingMember = newState.toJS()[currentGroupID];
      if (!groupPendingMember) {
        groupPendingMember = {};
        groupPendingMember.pending = [];
        groupPendingMember.pendingTotal = 0;
      }
      let pendingMember = [...action.pendingMember, ...groupPendingMember.pending];
      pendingMember = _.uniqBy(pendingMember, 'id');
      let lengthDiff = Math.abs(groupPendingMember.pending.length - pendingMember.length);
      let pendingTotal = groupPendingMember.pendingTotal + lengthDiff;
      newState = newState.setIn([currentGroupID + '', 'pending'], pendingMember);
      newState = newState.setIn([currentGroupID + '', 'pendingTotal'], pendingTotal);
      return newState;

    case actionTypes.UPDATE_CARD_MODAL:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      newState = newState.setIn([currentGroupID + '', 'cardModalDesc'], action.desc);
      newState = newState.setIn([currentGroupID + '', 'cardModalCount'], action.count);
      newState = newState.setIn([currentGroupID + '', 'cardModalType'], action.carouselCardType);
      newState = newState.setIn([currentGroupID + '', 'cardAryName'], action.name);
      return newState;

    case actionTypes.UPDATE_MEMBER:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      let membersArry = newState.toJS()[currentGroupID].members;
      let ownerList = newState.toJS()[currentGroupID].owners;
      let shouldRemove;

      membersArry = membersArry.filter(obj => {
        if (obj.id == action.member.id) {
          obj.role = action.member.role;
        }
        return obj;
      });

      shouldRemove = _.some(ownerList, function(owner) {
        return owner.id == action.member.id;
      });

      if (shouldRemove) {
        ownerList = _.without(
          ownerList,
          _.find(ownerList, owner => {
            return owner.id == action.member.id;
          })
        );
      } else {
        ownerList = _.uniqBy([...ownerList, ...[action.member]], 'id');
      }

      newState = newState.setIn([currentGroupID + '', 'members'], membersArry);
      newState = newState.setIn([currentGroupID + '', 'owners'], ownerList);

      return newState;

    case actionTypes.REMOVE_SHARED_CARD:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      let groupObj = newState.toJS()[currentGroupID];
      let revisedCards = groupObj.sharedCards;
      revisedCards = revisedCards.filter(c => {
        return c.id !== action.cardID;
      });
      newState = newState.setIn([currentGroupID + '', 'sharedCards'], revisedCards);
      newState = newState.setIn(
        [currentGroupID + '', 'sharedCardsCount'],
        groupObj.sharedCardsCount - 1
      );
      return newState;

    case actionTypes.DELETE_TEAM_CARD:
      newState = state;
      currentGroupID = newState.toJS().currentGroupID;
      let currentGroup = newState.toJS()[currentGroupID];
      let cardsArray = currentGroup[action.cardArrayName];
      let countName = action.cardArrayName + 'Count';
      cardsArray = _.without(
        cardsArray,
        _.find(cardsArray, card => {
          return card.id == action.cardID;
        })
      );
      newState = newState.setIn([currentGroupID + '', action.cardArrayName], cardsArray);
      newState = newState.setIn([currentGroupID + '', countName], currentGroup[countName] - 1);
      return newState;

    case actionTypes.RECEIVE_GROUP_CARDS:
      newState = state;
      let groupsC = newState.toJS();
      currentGroupID = groupsC.currentGroupID;
      let groupC = groupsC[currentGroupID];
      let cards = groupC[action.cardAryName];
      cards = [...cards, ...action.cards.cards];
      cards = _.uniqBy(cards, 'id');
      newState = newState.setIn([currentGroupID + '', action.cardAryName], cards);
      newState = newState.setIn([currentGroupID + '', action.cardAryName + 'Loaded'], true);
      newState = newState.setIn(
        [currentGroupID + '', action.cardAryName + 'Offset'],
        action.offset
      );
      newState = newState.setIn(
        [currentGroupID + '', action.cardAryName + 'Count'],
        action.cards.total
      );
      return newState;

    case actionTypes.RECEIVE_FEATURED_GROUP_CARDS:
      newState = state;
      let groupsF = newState.toJS();
      currentGroupID = groupsF.currentGroupID;
      let groupF = groupsF[currentGroupID];
      let featured = groupF['featured'];
      featured = [...featured, ...action.pins.pins];
      featured = _.uniqBy(featured, 'id');
      newState = newState
        .setIn([currentGroupID + '', 'featured'], featured)
        .setIn([currentGroupID + '', 'featuredLoaded'], true)
        .setIn([currentGroupID + '', 'featuredCount'], action.pins.pins.length);
      return newState;

    case actionTypes.RECEIVE_GROUP_MEMBERS:
      newState = state;
      let groupsA = newState.toJS();
      currentGroupID = groupsA.currentGroupID;
      let groupA = groupsA[currentGroupID];
      let members = groupA['members'];
      //On initialization make members array empty
      if (action.isInitialized) {
        members = [];
      } else {
        members = [...members, ...action.members.users];
      }
      members = _.uniqBy(members, 'id');
      newState = newState.setIn([currentGroupID + '', 'members'], members);
      newState = newState.setIn([currentGroupID + '', 'membersLimit'], action.limit);
      newState = newState.setIn([currentGroupID + '', 'membersOffset'], action.offset);
      return newState;

    case actionTypes.RECEIVE_GROUP_PENDING:
      newState = state;
      let groupsB = newState.toJS();
      currentGroupID = groupsB.currentGroupID;
      let groupB = groupsB[currentGroupID];
      let pending = groupB['pending'];
      //On initialization make pending array empty
      if (action.isInitialized) {
        pending = [];
      } else {
        pending = [...pending, ...action.pending.users];
      }
      pending = _.uniqBy(pending, 'id');
      newState = newState.setIn([currentGroupID + '', 'pending'], pending);
      newState = newState.setIn([currentGroupID + '', 'pendingLimit'], action.limit);
      newState = newState.setIn([currentGroupID + '', 'pendingOffset'], action.offset);
      return newState;

    case actionTypes.REMOVE_GROUP_MEMBER:
      newState = state;
      let orgGroups = newState.toJS();
      let currentGrp = orgGroups[orgGroups.currentGroupID];
      let currentGrpMembers = currentGrp.members;
      let currentGrpSubAdmins = currentGrp.subAdmins;
      let currentGrpOwners = currentGrp.owners;
      let isMember = currentGrp.isMember;
      let isTeamAdmin = currentGrp.isTeamAdmin;
      let isTeamSubAdmin = currentGrp.isTeamSubAdmin;
      if (currentGrpMembers.some(member => member.id === action.removedUser.id)) {
        isMember = false;
        currentGrpMembers = _.without(
          currentGrpMembers,
          _.find(currentGrpMembers, member => {
            return member.id === action.removedUser.id;
          })
        );
      }
      if (currentGrpSubAdmins.some(subAdmin => subAdmin.id === action.removedUser.id)) {
        isTeamSubAdmin = false;
        currentGrpSubAdmins = _.without(
          currentGrpSubAdmins,
          _.find(currentGrpSubAdmins, subAdmin => {
            return subAdmin.id === action.removedUser.id;
          })
        );
      }
      if (currentGrpOwners.some(owner => owner.id === action.removedUser.id)) {
        isTeamAdmin = false;
        currentGrpOwners = _.without(
          currentGrpOwners,
          _.find(currentGrpOwners, owner => {
            return owner.id === action.removedUser.id;
          })
        );
      }
      newState = newState.setIn([orgGroups.currentGroupID + '', 'members'], currentGrpMembers);
      newState = newState.setIn([orgGroups.currentGroupID + '', 'subAdmins'], currentGrpSubAdmins);
      newState = newState.setIn([orgGroups.currentGroupID + '', 'owners'], currentGrpOwners);
      newState = newState.setIn(
        [orgGroups.currentGroupID + '', 'membersTotal'],
        --currentGrp.membersTotal
      );
      newState = newState.setIn(
        [orgGroups.currentGroupID + '', 'membersOffset'],
        --currentGrp.membersOffset
      );
      newState = newState.setIn([orgGroups.currentGroupID + '', 'isMember'], isMember);
      newState = newState.setIn([orgGroups.currentGroupID + '', 'isTeamAdmin'], isTeamAdmin);
      newState = newState.setIn([orgGroups.currentGroupID + '', 'isTeamSubAdmin'], isTeamSubAdmin);
      return newState;

    case actionTypes.UPDATE_PENDING_MEMBER:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = orgGroups.currentGroupID;
      currentGrp = orgGroups[orgGroups.currentGroupID];
      pending = currentGrp['pending'];
      pending = pending.map(user => {
        if ((user.userId || user.id) == action.id) {
          user.isFollowed = action.isFollowed;
        }
        return user;
      });
      newState = newState.setIn([currentGroupID + '', 'pending'], pending);
      return newState;
    case actionTypes.UPDATE_GROUP_USERS:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = orgGroups.currentGroupID;
      currentGrp = orgGroups[currentGroupID];
      let subAdmins = currentGrp.subAdmins;
      let owners = currentGrp.owners;
      if (action.newUsers && action.newUsers.length) {
        if (action.role === 'sub_admin') {
          subAdmins.push(...action.newUsers);
        } else {
          owners.push(...action.newUsers);
        }
      } else {
        if (action.role === 'sub_admin') {
          subAdmins = subAdmins.filter(item => !~action.ids.indexOf(item.id));
        } else {
          owners = owners.filter(item => !~action.ids.indexOf(item.id));
        }
      }
      newState = newState.setIn([currentGroupID + '', 'subAdmins'], subAdmins);
      newState = newState.setIn([currentGroupID + '', 'owners'], owners);
      return newState;
    case actionTypes.ACCEPT_INVITE:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = action.teamId;
      currentGrp = orgGroups[currentGroupID];
      newState = newState
        .setIn([currentGroupID + '', 'isPending'], false)
        .setIn([currentGroupID + '', 'isMember'], true);
      return newState;
    case actionTypes.DECLINE_INVITE:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = action.teamId;
      currentGrp = orgGroups[currentGroupID];
      newState = newState
        .setIn([currentGroupID + '', 'isPending'], false)
        .setIn([currentGroupID + '', 'isMember'], false);
      return newState;
    case actionTypes.LEAVE_GROUP:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = action.teamId;
      currentGrp = orgGroups[currentGroupID];
      newState = newState
        .setIn([currentGroupID + '', 'isMember'], false)
        .setIn([currentGroupID + '', 'isTeamAdmin'], false)
        .setIn([currentGroupID + '', 'isTeamSubAdmin'], false);
      return newState;
    case actionTypes.JOIN_GROUP:
      newState = state;
      orgGroups = newState.toJS();
      currentGroupID = action.teamId;
      currentGrp = orgGroups[currentGroupID];
      newState = newState.setIn([currentGroupID + '', 'isMember'], true);
      return newState;
    case actionTypes.UPDATE_GROUP_INFORMATION:
      newState = state;
      orgGroups = newState.toJS();
      let updatedGroup = action.team;
      let groupID = action.team.id;
      newState = newState
        .setIn([groupID + '', 'isMember'], updatedGroup.isMember)
        .setIn([groupID + '', 'isTeamAdmin'], updatedGroup.isTeamAdmin)
        .setIn([groupID + '', 'isTeamSubAdmin'], updatedGroup.isTeamSubAdmin)
        .setIn([groupID + '', 'isPending'], updatedGroup.isPending)
        .setIn([groupID + '', 'owners'], updatedGroup.owners)
        .setIn([groupID + '', 'membersCount'], updatedGroup.membersCount)
        .setIn([groupID + '', 'pendingMembersCount'], updatedGroup.pendingMembersCount)
        .setIn([groupID + '', 'subAdmins'], updatedGroup.subAdmins)
        .setIn([groupID + '', 'membersTotal'], updatedGroup.membersCount)
        .setIn([groupID + '', 'pendingTotal'], updatedGroup.pendingMembersCount);

      return newState;
    case actionTypes.REMOVE_PENDING_GROUP_MEMBER:
      newState = state;
      let groupsPending = newState.toJS();
      currentGroupID = groupsPending && groupsPending.currentGroupID;
      let groupPending = groupsPending[currentGroupID];
      let pendingMembers = groupPending['pending'];
      if (
        pendingMembers &&
        action.id &&
        pendingMembers.some(member => (member.userId || member.id) == action.id)
      ) {
        isMember = false;
        pendingMembers = _.without(
          pendingMembers,
          _.find(pendingMembers, member => {
            return (member.userId || member.id) == action.id;
          })
        );
      }
      newState = newState.setIn([groupsPending.currentGroupID + '', 'pending'], pendingMembers);
      newState = newState.setIn(
        [groupsPending.currentGroupID + '', 'pendingTotal'],
        pendingMembers.length
      );
      return newState;

    default:
      return state;
  }
}
