import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';

export default function myLearningPlan(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.QUARTERLY_ASSIGNMENTS:
      newState = state;
      newState = newState.set(action.data.mlpPeriod, action.data);
      return newState;

    case actionTypes.COMPETENCY_ASSIGNMENTS:
      newState = state;
      newState = newState.setIn(
        ['skills', action.data.learningTopicsName],
        action.data.assignments
      );
      return newState;

    default:
      return state;
  }
}
