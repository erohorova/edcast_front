import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function learningQueueReducer(
  state = Immutable.fromJS({
    items: [],
    bookmarkedItems: [],
    pending: false,
    bookmarkPending: false
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_LEARNING_QUEUE_ITEMS:
      newState = state.set('items', action.items).set('pending', action.pending);
      return newState;
    case actionTypes.RECEIVE_BOOKMARK_LEARNING_QUEUE:
      newState = state
        .set('bookmarkedItems', action.bookmarked)
        .set('bookmarkPending', action.pending);
      return newState;
    case actionTypes.REMOVE_BOOKMARK_FROM_LEARNING_QUEUE:
      newState = state;
      let stateItems = state.toJS().items;
      for (let i = 0; i < stateItems.length; i++) {
        if (stateItems[i].queueable.id === action.cardId && stateItems[i].source === 'bookmarks') {
          stateItems.splice(i, 1);
        }
      }
      newState = newState.set('items', stateItems).set('pending', false);
      return newState;
    default:
      return state;
  }
}
