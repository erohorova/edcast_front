import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function sociativeReducer(
  state = Immutable.fromJS({
    status: {}
  }),
  action
) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_SOCIATIVE_REGISTRATION_STATUS:
      newState = state.set('status', action.status);
      return newState;
    default:
      return state;
  }
}
