/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import omit from 'lodash/omit';

export default function channelsReducer(state = Immutable.Map(), action) {
  let newState;
  switch (action.type) {
    case actionTypes.RECEIVE_CHANNELS:
    case actionTypes.RECEIVE_DISCOVERY_CHANNELS:
    case actionTypes.RECEIVE_RECOMMENDED_CHANNELS:
      newState = state;
      action.channels.forEach(channel => {
        newState = newState.set(channel.id + '', Immutable.fromJS(channel));
      });
      return newState;
    case actionTypes.REQUEST_TOGGLE_CHANNEL_FOLLOW:
      return state.setIn([action.id + '', 'followPending'], true);
    case actionTypes.RECEIVE_TOGGLE_CHANNEL_FOLLOW:
      return state
        .setIn([action.id + '', 'following'], action.isFollow)
        .setIn([action.id + '', 'isFollowing'], action.isFollow)
        .setIn([action.id + '', 'followPending'], false);
    case actionTypes.FOLLOWING_CHANNEL_STATE:
      if (action.follow) {
        newState = state.toJS();
        newState = omit(newState, [action.channel.id]);
        newState = Immutable.fromJS(newState);
      } else {
        newState = state.set(action.channel.id, action.channel);
      }
      return newState;
    case actionTypes.CREATE_CHANNEL:
      newState = state;
      newState = newState.set(
        action.createdChannel.id + '',
        Immutable.fromJS(action.createdChannel)
      );
      return newState;
    default:
      return state;
  }
}
