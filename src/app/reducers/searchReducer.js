import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
import _ from 'lodash';

export default function searchReducer(
  state = Immutable.Map({
    cards: [],
    pathways: [],
    journeys: [],
    channels: [],
    users: [],
    typeFilters: [],
    paidContentFilters: [],
    freeContentFilter: {},
    sourceFilters: [],
    providerFilters: [],
    languageFilters: [],
    subjectFilters: [],
    levelFilters: {},
    ratingFilters: {},
    currencyFilters: '',
    amountRangeFilters: {},
    isPaidFilters: '',
    openWebSources: [],
    totalCount: 0,
    searchTotalCount: 0,
    contentCount: 0,
    contentSectionCount: 0,
    pathwaysCount: 0,
    channelCount: 0,
    peopleCount: 0,
    openWebSourcesCount: 0,
    loading: true,
    contentChecked: false,
    paidContentChecked: false,
    postedByFilters: {
      followers: false,
      following: false,
      user_ids: null
    },
    likedByFilters: {
      followers: false,
      following: false,
      user_ids: null
    },
    commentedByFilters: {
      followers: false,
      following: false,
      user_ids: null
    },
    bookmarkedByFilters: {
      followers: false,
      following: false,
      user_ids: null
    },
    shouldResetInline: false,
    hideFilters: false,
    isFetchingResultsFailed: false
  }),
  action
) {
  let newState, cards, pathways, journeys, users, channels;
  switch (action.type) {
    case actionTypes.RECEIVE_SEARCH_ITEMS:
      newState = state;
      let contentSectionCount = 0;
      let pathwaysCount = 0;
      let journeysCount = 0;
      let channelCount = 0;
      let userCount = 0;
      let searchTotalCount = 0;
      let totalCount = 0;

      action &&
        action.items &&
        action.items.aggregations &&
        action.items.aggregations.forEach(item => {
          if (
            item.type == 'content_type' &&
            !['Channels', 'Users', 'Pathway', 'Journey'].includes(item.display_name)
          ) {
            contentSectionCount += item.count;
          }
          if (item.type == 'content_type') {
            totalCount += item.count;
          }
          if (item.display_name == 'Channels') {
            channelCount += item.count;
          } else if (item.display_name == 'Users') {
            userCount += item.count;
          } else if (item.display_name == 'Pathway') {
            pathwaysCount += item.count;
          } else if (item.display_name == 'Journey') {
            journeysCount += item.count;
          }
        });
      searchTotalCount =
        contentSectionCount + channelCount + userCount + pathwaysCount + journeysCount;

      newState = newState.merge({
        cards: Immutable.fromJS(action.items.cards),
        pathways: Immutable.fromJS(action.items.pathways || []),
        journeys: Immutable.fromJS(action.items.journeys || []),
        users: Immutable.fromJS(action.items.users),
        channels: Immutable.fromJS(action.items.channels),
        searchTotalCount: Immutable.fromJS(searchTotalCount),
        contentSectionCount: Immutable.fromJS(contentSectionCount),
        pathwaysCount: Immutable.fromJS(pathwaysCount),
        journeysCount: Immutable.fromJS(journeysCount),
        channelCount: Immutable.fromJS(channelCount),
        peopleCount: Immutable.fromJS(userCount),
        totalCount: Immutable.fromJS(totalCount),
        shouldResetInline: false,
        hideFilters: false,
        loading: false,
        isFetchingResultsFailed: false
      });
      return newState;
    case actionTypes.RECEIVE_FILTERS:
      newState = state;
      let sources = [];
      let providers = [];
      let languages = [];
      let subjects = [];
      let openWebSources = [];
      let openWebSourcesList = [];
      let openWebSourcesCount = 0;
      let contentFilters = [];
      totalCount = 0;
      let contentCount = 0;

      action &&
        action.items &&
        action.items.aggregations &&
        action.items.aggregations.forEach(item => {
          if (item.type == 'source_id') {
            sources.push({
              id: item.id,
              display_name: item.display_name,
              count: item.count,
              type: item.type
            });
          } else if (item.type == 'provider_name') {
            providers.push({
              id: item.id,
              display_name: item.display_name,
              count: item.count,
              type: item.type
            });
          } else if (item.type == 'languages') {
            languages.push({
              id: item.id,
              display_name: item.display_name,
              count: item.count,
              type: item.type
            });
          } else if (item.type == 'subjects') {
            subjects.push({
              id: item.id,
              display_name: item.display_name,
              count: item.count,
              type: item.type
            });
          } else if (item.type == 'source_type_name') {
            openWebSources.push({
              id: item.id,
              display_name: item.display_name,
              count: item.count,
              type: item.type
            });
            openWebSourcesList.push(item.display_name);
          } else if (
            item.type != 'source_id' &&
            item.type != 'source_type_name' &&
            item.type != 'provider_name' &&
            item.type != 'languages' &&
            item.type != 'subjects'
          ) {
            contentFilters.push({
              display_name: item.display_name,
              count: item.count,
              type: item.type,
              subType: ['Channels', 'Users', 'Pathway', 'Journey'].includes(item.display_name)
                ? item.display_name
                : 'Content'
            });
          } else if (item.type == 'content_type') {
            totalCount += item.count;

            if (!['Channels', 'Users', 'Pathway', 'Journey'].includes(item.display_name)) {
              contentCount += item.count;
            }
          }
        });
      openWebSources.forEach(item => {
        openWebSourcesCount += item.count;
      });
      let paidContentPlans = [
        {
          display_name: 'Paid'
        },
        {
          display_name: 'Premium'
        },
        {
          display_name: 'Subscription'
        }
      ];

      newState = newState.merge({
        paidContentFilters: Immutable.fromJS(paidContentPlans),
        typeFilters: Immutable.fromJS(contentFilters),
        openWebSourcesCount: Immutable.fromJS(openWebSourcesCount),
        openWebSources: Immutable.fromJS(openWebSourcesList),
        sourceFilters: Immutable.fromJS(sources),
        providerFilters: Immutable.fromJS(providers),
        languageFilters: Immutable.fromJS(languages),
        subjectFilters: Immutable.fromJS(subjects),
        totalCount: Immutable.fromJS(totalCount),
        contentSectionCount: Immutable.fromJS(contentCount),
        isFetchingResultsFailed: false
      });

      return newState;

    case actionTypes.LOAD_MORE_SEARCH_ITEMS:
      newState = state;
      cards = newState.get('cards');
      cards = cards.concat(action.items.cards);

      pathways = newState.get('pathways');
      pathways = pathways.concat(action.items.pathways || []);

      journeys = newState.get('journeys');
      journeys = journeys.concat(action.items.journeys || []);

      users = newState.get('users');
      users = users.concat(action.items.users);

      channels = newState.get('channels');
      channels = channels.concat(action.items.channels);

      newState = newState.merge({
        cards: Immutable.fromJS(cards),
        pathways: Immutable.fromJS(pathways),
        journeys: Immutable.fromJS(journeys),
        users: Immutable.fromJS(users),
        channels: Immutable.fromJS(channels),
        isFetchingResultsFailed: false
      });

      return newState;
    case actionTypes.UPDATE_MINIMAL_COUNTS:
      newState = state;
      let searchedCardsCount = 0;
      let searchedPathwaysCount = 0;
      let searchedJourneysCount = 0;
      let searchedChannelCount = 0;
      let searchedUsersCount = 0;
      let searchedTotalCount = 0;

      action &&
        action.items &&
        action.items.aggregations &&
        action.items.aggregations.forEach(item => {
          if (
            item.type == 'content_type' &&
            !['Channels', 'Users', 'Pathway', 'Journey'].includes(item.display_name)
          ) {
            searchedCardsCount += item.count;
          }
          if (item.display_name == 'Channels') {
            searchedChannelCount += item.count;
          }
          if (item.display_name == 'Users') {
            searchedUsersCount += item.count;
          }
          if (item.display_name == 'Pathway') {
            searchedCardsCount += item.count;
          }
          if (item.display_name == 'Journey') {
            searchedCardsCount += item.count;
          }
        });

      searchedTotalCount =
        searchedCardsCount +
        searchedChannelCount +
        searchedUsersCount +
        searchedPathwaysCount +
        searchedJourneysCount;
      newState = newState.merge({
        searchTotalCount: Immutable.fromJS(searchedTotalCount),
        contentSectionCount: Immutable.fromJS(searchedCardsCount),
        pathwaysCount: Immutable.fromJS(searchedPathwaysCount),
        journeysCount: Immutable.fromJS(searchedJourneysCount),
        channelCount: Immutable.fromJS(searchedChannelCount),
        peopleCount: Immutable.fromJS(searchedUsersCount),
        isFetchingResultsFailed: false
      });

      return newState;
    case actionTypes.MODIFY_TYPE_FILTERS:
      newState = state;
      let typeFilters = newState.get('typeFilters').toJS();
      let count = 0;

      let newTypeFilters = typeFilters.map(item => {
        if (
          !['Channels', 'Users', 'Pathway', 'Journey'].includes(item.display_name) &&
          action.filter == item.display_name.toLowerCase().replace(' ', '_')
        ) {
          item.checked = action.status;
        }
        if (item.checked === true) {
          ++count;
        }
        return item;
      });

      newState = newState
        .set('typeFilters', Immutable.fromJS(newTypeFilters))
        .set('contentChecked', count > 0 ? true : false);

      return newState;
    case actionTypes.MODIFY_PLAN_FILTERS:
      newState = state;
      let count1 = 0;
      let planFilters = newState.get('paidContentFilters').toJS();

      let newPlanFilters = planFilters.map(item => {
        if (
          item.display_name != 'Free' &&
          action.filter == item.display_name.toLowerCase().replace(' ', '_')
        ) {
          item.checked = action.status;
        }
        if (item.checked === true) {
          ++count1;
        }
        return item;
      });

      let freeContentFilter = Object.assign({}, newState.toJS().freeContentFilter);
      if (action.filter == 'free') {
        freeContentFilter[`${action.filter}`] = action.status;
      }
      newState = newState
        .set('paidContentFilters', Immutable.fromJS(newPlanFilters))
        .set('paidContentChecked', count1 === planFilters.length)
        .set('freeContentFilter', Immutable.fromJS(freeContentFilter));

      return newState;
    case actionTypes.MODIFY_SOURCE_FILTERS:
      newState = state;
      let sourceFilters = newState.get('sourceFilters').toJS();
      let selectedSourceFilters = newState.get('selectedSourceFilters') || [];
      let newSourceFilters = sourceFilters.map(item => {
        if (action.filter == item.id) {
          item.checked = action.status;
        }
        return item;
      });

      if (action.status) {
        selectedSourceFilters.push(action.filter);
      } else {
        let index = selectedSourceFilters.indexOf(action.filter);
        !!~index && selectedSourceFilters.splice(index, 1);
      }

      newState = newState
        .set('sourceFilters', Immutable.fromJS(newSourceFilters))
        .set('selectedSourceFilters', selectedSourceFilters);

      return newState;
    case actionTypes.MODIFY_PROVIDER_FILTERS:
      newState = state;
      let providerFilters = newState.get('providerFilters').toJS();
      let selectedProviderFilters = newState.get('selectedProviderFilters') || [];
      let newProviderFilters = providerFilters.map(item => {
        if (action.filter == item.id) {
          item.checked = action.status;
        }
        return item;
      });

      if (action.status) {
        selectedProviderFilters.push(action.filter);
      } else {
        let index = selectedProviderFilters.indexOf(action.filter);
        !!~index && selectedProviderFilters.splice(index, 1);
      }

      newState = newState
        .set('providerFilters', Immutable.fromJS(newProviderFilters))
        .set('selectedProviderFilters', selectedProviderFilters);
      return newState;
    case actionTypes.MODIFY_SUBJECTS_FILTERS:
      newState = state;
      let subjectFilters = newState.get('subjectFilters').toJS();
      let selectedSubjectFilters = newState.get('selectedSubjectFilters') || [];
      let newSubjectFilters = subjectFilters.map(item => {
        if (action.filter == item.id) {
          item.checked = action.status;
        }
        return item;
      });

      if (action.status) {
        selectedSubjectFilters.push(action.filter);
      } else {
        let index = selectedSubjectFilters.indexOf(action.filter);
        !!~index && selectedSubjectFilters.splice(index, 1);
      }

      newState = newState
        .set('subjectFilters', Immutable.fromJS(newSubjectFilters))
        .set('selectedSubjectFilters', selectedSubjectFilters);
      return newState;
    case actionTypes.MODIFY_LANGUAGE_FILTERS:
      newState = state;
      let languageFilters = newState.get('languageFilters').toJS();
      let selectedLanguageFilters = newState.get('selectedLanguageFilters') || [];
      let newLanguageFilters = languageFilters.map(item => {
        if (action.filter == item.id) {
          item.checked = action.status;
        }
        return item;
      });

      if (action.status) {
        selectedLanguageFilters.push(action.filter);
      } else {
        let index = selectedLanguageFilters.indexOf(action.filter);
        !!~index && selectedLanguageFilters.splice(index, 1);
      }

      newState = newState
        .set('languageFilters', Immutable.fromJS(newLanguageFilters))
        .set('selectedLanguageFilters', selectedLanguageFilters);
      return newState;
    case actionTypes.MODIFY_ACTION_BY_FILTERS:
      newState = state;
      switch (action.filterType) {
        case 'level':
          let levelFilters = Object.assign({}, newState.toJS().levelFilters);
          levelFilters[`${action.filter}`] = action.status;
          newState = newState.set('levelFilters', Immutable.fromJS(levelFilters));
          break;
        case 'rating':
          let ratingFilters = Object.assign({}, newState.toJS().ratingFilters);
          ratingFilters[`${action.filter}`] = action.status;
          newState = newState.set('ratingFilters', Immutable.fromJS(ratingFilters));
          break;
        case 'currency':
          let currencyFilters = action.status;
          newState = newState.set('currencyFilters', Immutable.fromJS(currencyFilters));
          break;
        case 'free_paid':
          let isPaidFilters = action.status;
          newState = newState.set('isPaidFilters', Immutable.fromJS(isPaidFilters));
          break;
        case 'amount_range':
          let amountRangeFilters = Object.assign({}, newState.toJS().amountRangeFilters);
          amountRangeFilters[`${action.filter}`] = action.status;
          newState = newState.set('amountRangeFilters', Immutable.fromJS(amountRangeFilters));
          break;
        case 'amount_range_empty':
          let amountRangeEmptyFilters = {};
          newState = newState.set('amountRangeFilters', Immutable.fromJS(amountRangeEmptyFilters));
          break;
        case 'clear_price':
          let isPaidEmptyFilters = '';
          newState = newState.set('isPaidFilters', Immutable.fromJS(isPaidEmptyFilters));
          break;
        case 'posted_by':
          let postedByFilters = Object.assign({}, newState.toJS().postedByFilters);
          postedByFilters[`${action.filter}`] = action.status;
          newState = newState.set('postedByFilters', Immutable.fromJS(postedByFilters));
          break;
        case 'liked_by':
          let likedByFilters = Object.assign({}, newState.toJS().likedByFilters);
          likedByFilters[`${action.filter}`] = action.status;
          newState = newState.set('likedByFilters', Immutable.fromJS(likedByFilters));
          break;
        case 'commented_by':
          let commentedByFilters = Object.assign({}, newState.toJS().commentedByFilters);
          commentedByFilters[`${action.filter}`] = action.status;
          newState = newState.set('commentedByFilters', Immutable.fromJS(commentedByFilters));
          break;
        case 'bookmarked_by':
          let bookmarkedByFilters = Object.assign({}, newState.toJS().bookmarkedByFilters);
          bookmarkedByFilters[`${action.filter}`] = action.status;
          newState = newState.set('bookmarkedByFilters', Immutable.fromJS(bookmarkedByFilters));
          break;
        default:
          break;
      }
      return newState;
    case actionTypes.RESET_STATE:
      newState = state;
      let resetState = {
        followers: false,
        following: false,
        user_ids: null
      };
      newState = newState.merge({
        contentChecked: false,
        postedByFilters: resetState,
        likedByFilters: resetState,
        commentedByFilters: resetState,
        bookmarkedByFilters: resetState,
        levelFilters: {},
        freeContentFilter: {},
        currencyFilters: '',
        ratingFilters: {},
        amountRangeFilters: {},
        isPaidFilters: '',
        isFetchingResultsFailed: false
      });

      return newState;
    case actionTypes.ERROR_FETCHING_SEARCH_RESULTS:
      newState = state;
      resetState = {
        followers: false,
        following: false,
        user_ids: null
      };
      newState = newState.merge({
        contentChecked: false,
        contentLoading: false,
        pathwayLoading: false,
        channelLoading: false,
        peopleLoading: false,
        loading: false,
        totalCount: 0,
        searchTotalCount: 0,
        contentCount: 0,
        contentSectionCount: 0,
        pathwaysCount: 0,
        channelCount: 0,
        peopleCount: 0,
        isFetchingResultsFailed: true
      });

      return newState;
    case actionTypes.SHOW_LOADER_WITH_RESET:
      newState = state;
      resetState = {
        followers: false,
        following: false,
        user_ids: null
      };
      newState = newState.merge({
        loading: true,
        cards: [],
        pathways: [],
        channels: [],
        users: [],
        typeFilters: [],
        paidContentFilters: [],
        sourceFilters: [],
        providerFilters: [],
        languageFilters: [],
        subjectFilters: [],
        openWebSources: [],
        totalCount: 0,
        searchTotalCount: 0,
        contentCount: 0,
        contentSectionCount: 0,
        pathwaysCount: 0,
        channelCount: 0,
        peopleCount: 0,
        openWebSourcesCount: 0,
        contentChecked: false,
        levelFilters: {},
        freeContentFilter: {},
        ratingFilters: {},
        currencyFilters: '',
        amountRangeFilters: {},
        isPaidFilters: '',
        postedByFilters: resetState,
        likedByFilters: resetState,
        commentedByFilters: resetState,
        bookmarkedByFilters: resetState,
        shouldResetInline: action.shouldResetInline,
        hideFilters: false,
        isFetchingResultsFailed: false
      });
      return newState;
    case actionTypes.HIDE_FILTERS:
      newState = state;
      newState = newState.set('hideFilters', true).set('isFetchingResultsFailed', false);
      return newState;
    case actionTypes.UPDATE_CARD_IN_RESULT:
      newState = state;
      cards = newState.toJS().cards;
      let oldCard, elemID;
      if (action.updatedCard && action.updatedCard.eclId && cards) {
        _.forEach(cards, (elem, index) => {
          if (
            (elem && elem.id === 'ECL-' + action.updatedCard.eclId) ||
            (elem && elem.id === action.updatedCard.id)
          ) {
            oldCard = elem;
            elemID = index;
            return;
          }
        });
      }
      cards[elemID] = action.updatedCard;

      newState = newState.merge({
        cards: Immutable.fromJS(cards)
      });
      return newState;

    default:
      return state;
  }
}
