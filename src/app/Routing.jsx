import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Router, Route, IndexRoute, Redirect } from 'react-router';
import request from 'superagent';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Loadable from 'react-loadable';
import Spinner from './components/common/spinner';
import DiscoveryContainer from './components/discovery/DiscoveryContainer.jsx';
import DiscoveryContainerV2 from './components/discovery/DiscoveryContainerV2.js';
import ProviderContainer from './components/provider/ProviderContainer.jsx';
import ProfileContainerv3 from './components/profile/ProfileContainerv3.jsx';
import ProfileContainer from './components/profilev2/ProfileContainer.jsx';
import EmptyContainer from './components/EmptyContainer.jsx';
import SignupContainer from './components/login/SignupContainer.jsx';
import FeedContainer from './components/feed/FeedContainer.jsx';
import TodayLearningContainer from './components/feed/todayLearning/TodayLearningContainer.jsx';
import TodayLearningContainerSearch from './components/feed/todayLearning/TodayLearningContainerSearch.jsx';
import FeaturedContainer from './components/feed/featured/FeaturedContainer.jsx';
import TopNavContainer from './components/TopNav/TopNavContainer.jsx';
import CurateContainer from './components/feed/curate/curateContainer.jsx';
import Submission from './components/feed/submission/submission.jsx';
import OnboardingContainer from './components/onboarding/OnboardingContainer.jsx';
import HomeContainerv2 from './components/home/HomeContainerv2.jsx';
import FooterContainer from './components/footer/FooterContainer.jsx';
import AssignmentsContainer from './components/feed/assignments/AssignmentsContainer.jsx';
import RecommendedContainer from './components/feed/RecommendedContainer.jsx';
import HistoryContainer from './components/profile/history/HistoryContainer';
import MyContentContainerv2 from './components/profile/MyContent/MyContentContainerv2';
import LearnersDashboard from './components/profile/LearnersDashboard/index';
import MyChannelsContainer from './components/profile/MyChannels/MyChannelsContainer';
import ChannelsListContainer from './components/discovery/ChannelsListContainer';
import PathwaysListContainer from './components/discovery/PathwaysListContainer';
import JourneysListContainer from './components/discovery/JourneysListContainer';
import CoursesListContainer from './components/discovery/CoursesListContainer';
import { users, usersv2 } from 'edc-web-sdk/requests/index';
import { getDefaultImageS3 } from 'edc-web-sdk/requests/cards.v2';
import { JWT } from 'edc-web-sdk/requests/csrfToken';
import InsightContainer from './components/insight/InsightContainer.jsx';
import VideoStreamsContainer from './components/discovery/videoStreams/VideoStreamsContainer.jsx';
import { NotificationsContainer } from 'blackbox-notifications';
import PremiumContentContainer from './components/provider/PremiumContentContainer.jsx';
import CustomContainer from './components/feed/customTab/CustomContainer.jsx';
import BookmarkStandalone from './components/profile/BookmarkStandalone.jsx';
import * as actionTypes from './constants/actionTypes';
import listMenu from './utils/orderMenu';
import Dashboard from './components/profilev2/Dashboard/index';
import { setCookie, readCookie, deleteCookie } from 'edc-web-sdk/helpers/cookies';
import LoginContainerV2 from './components/login/LoginContainerV2.jsx';
import PreRegisterContainer from './components/login/PreRegisterContainer.jsx';
import ForgotPasswordContainer from './components/forgotPassword/ForgotPasswordContainer.jsx';
import SendLinkContainer from './components/forgotPassword/SendLinkContainer.jsx';
import SentLinkContainer from './components/forgotPassword/SentLinkContainer.jsx';
import ChangedPasswordContainer from './components/forgotPassword/ChangedPasswordContainer.jsx';
import InvalidPasswordContainer from './components/forgotPassword/InvalidPasswordContainer.jsx';
import MobileBanner from './components/mobile/MobileBanner.jsx';
import { setFirstFeedTab } from './actions/feedActions';
import { getFilestackDefaultImages } from 'edc-web-sdk/requests/filestack';
import { addNewStringForTranslation } from 'edc-web-sdk/requests/translation';
import { refreshTranslations } from 'edc-web-sdk/helpers/translations';
import ProfileMeTabComponents from './components/profilev2/ProfileMeTabComponents.js';
import ChannelEdit from './components/channel/edit/ChannelEdit';

const WidgetContainer = Loadable({
  loader: () => import('./components/widget/WidgetContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SkillsGraph = Loadable({
  loader: () => import('./components/skillsGraph/SkillsGraph'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SkillsDirectory = Loadable({
  loader: () => import('./components/skillsDirectory/SkillsDirectory'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const MyTeamContainerv2 = Loadable({
  loader: () => import('./components/profile/MyTeam/MyTeamV2'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const TeamContainer = Loadable({
  loader: () => import('./components/team/TeamContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SearchContainer = Loadable({
  loader: () => import('./components/search/SearchContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SearchContainerV2 = Loadable({
  loader: () => import('./components/search/SearchContainerV2.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const GroupsContainerv2 = Loadable({
  loader: () => import('./components/team/groups/GroupsContainerv2'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const GroupsContainerv3 = Loadable({
  loader: () => import('./components/team/groups/GroupsContainer_v3'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const JourneyDetailsContainer = Loadable({
  loader: () => import('./components/journey/JourneyDetailsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const JourneyDetailsContainerV2 = Loadable({
  loader: () => import('./components/journey/JourneyDetailsContainerV2'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const PathwayDetailsContainer = Loadable({
  loader: () => import('./components/pathway/PathwayDetailsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const PathwayConsumptionContainer = Loadable({
  loader: () => import('./components/pathway/PathwayConsumptionContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const JourneyConsumptionContainer = Loadable({
  loader: () => import('./components/journey/JourneyConsumptionContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const GroupPageContainer = Loadable({
  loader: () => import('./components/team/SingleTeam/GroupPageContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const GroupPageContainerv2 = Loadable({
  loader: () => import('./components/team/SingleTeamv2/GroupPageContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const LeaderBoardContainerv2 = Loadable({
  loader: () => import('./components/team/leaderboard/v2/LeaderboardContainerv2'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const ChannelBase = Loadable({
  loader: () => import('./components/channel/ChannelBase.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const ChannelBaseV2 = Loadable({
  loader: () => import('./components/channel/v2/ChannelBase.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const ChannelRedesignBase = Loadable({
  loader: () => import('./components/channel/channel_redesigning/ChannelRedesignBase'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});

const ChannelAnalyticsContainer = Loadable({
  loader: () => import('./components/channel/analytics/ChannelAnalyticsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const TeamAnalyticsBase = Loadable({
  loader: () => import('./components/team/SingleTeam/analytics/TeamAnalyticsBase.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const TeamAssignmentAnalyticsContainer = Loadable({
  loader: () => import('./components/team/SingleTeam/analytics/TeamAssignmentAnalyticsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const MyProfileContainerv3 = Loadable({
  loader: () => import('./components/profile/v3/MyProfile/index'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});

const MyProfileContainer = Loadable({
  loader: () => import('./components/profilev2/MyProfile/index'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

const CareerAdvisorContainer = Loadable({
  loader: () => import('./components/profile/CareerAdvisor'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const AccountDetailsContainer = Loadable({
  loader: () => import('./components/settings/MyAccountDetails/AccountDetailsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SettingsContainer = Loadable({
  loader: () => import('./components/settings/SettingsContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const PublicProfileContent = Loadable({
  loader: () => import('./components/profile/PublicProfileContent'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const UserTriggersContainer = Loadable({
  loader: () => import('./components/settings/MyTriggers/TriggersContainer'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const IntegrationsContainer = Loadable({
  loader: () => import('./components/settings/MyIntegrations/index'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const MyChannelsContainerv2 = Loadable({
  loader: () => import('./components/profile/MyChannels/v2/MyChannelsContainer_v2'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const MyLearningContainerV5 = Loadable({
  loader: () => import('./components/profile/v5/LearningQueue/MyLearningPlanContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const SkillCoinsContainer = Loadable({
  loader: () => import('./components/profile/SkillCoinsContainer/index'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});
const DocsContainer = Loadable({
  loader: () => import('./components/docs/DocsContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});

const ConfirmPasswordContainer = Loadable({
  loader: () => import('./components/forgotPassword/ConfirmPasswordContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});

const LoginContainer = Loadable({
  loader: () => import('./components/login/LoginContainer.jsx'),
  loading() {
    return (
      <div className="route-loading-spinner">
        <Spinner />
      </div>
    );
  }
});

const UserActivityStandalone = Loadable({
  loader: () => import('./components/profilev2/UserActivity/Standalone/UserActivityStandalone'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});
class Routing extends Component {
  constructor(props, context) {
    super(props, context);
    this.rubyHandledPath = [
      'get',
      'oauth',
      'auth',
      'apipie',
      'my_content',
      'channels',
      'users',
      'oexchange',
      'collections',
      'search',
      'smartsearch',
      'more_users',
      'video_streams',
      '401',
      '403',
      '404',
      '500',
      'join',
      'sign_out',
      'choose_organization',
      'find_organization',
      'log_in',
      'sign_up',
      'confirmation',
      'profile',
      'about',
      'careers',
      'contact',
      'tos',
      'privacy',
      'ferpa',
      'partners',
      'eula',
      'developer',
      'developers',
      'dashboard_stream',
      'posts',
      'groups',
      'cards',
      'private_groups',
      'addon_stats',
      'current_user',
      'comments',
      'events',
      'clients',
      'manage',
      'content_sources',
      'home',
      'create_team',
      'organizations',
      'ui',
      'create',
      'insights',
      'forum',
      'arrr',
      'lti',
      'unsubscribe',
      'preferences',
      'user_preferences',
      'file_resources',
      'shares',
      'confirm_email',
      'er',
      'vimeo',
      'channel',
      'learn',
      'dialog',
      'promotions',
      'team',
      'cms',
      'landing',
      'referral_landing',
      'go',
      'releases',
      'widget',
      'redirect',
      'vredirect',
      'modal_templates',
      'invitations',
      'courses',
      'user_completions',
      'goals',
      'pathways',
      'crop_image',
      'savannah',
      'sso_login',
      'org_ssos',
      'brightcove_videos',
      'styleguide',
      'superadmin',
      's',
      'results'
    ];
    this.loginContainerV2 = window.ldclient.variation('login-container-v2', false);
    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
    this.mlpVersion = window.ldclient.variation('mlp-version', 'v3');
    this.mobileAppBanner = window.ldclient.variation('mobile-app-banner', false);
    this.teamAnalyticsEnabled = window.ldclient.variation('team-analytics', false);
    this.showProjectCard = window.ldclient.variation('project-card', false);
    this.isNewProfileNavigation = window.ldclient.variation('is-me-new-navigation', false);
    this.isGroupPageV2 = window.ldclient.variation('group-page-v2', false);
    this.isShowRecommended = window.ldclient.variation('winterstorm', false);
    this.customDefaultImages = window.ldclient.variation('custom-default-images', false);
    this.isRevisedSearch = window.ldclient.variation('revised-search-with-sociative', false);
    this.isLeaderboardConfigEnabled =
      this.props.team.get('config') && this.props.team.get('config').leaderboard;
    this.performanceImprovedUI = window.ldclient.variation('new-performance-improved-ui', false);
    this.journeyEnhancement = window.ldclient.variation('journey-enhancement', false);
    this.cardV3 = window.ldclient.variation('card-v3', false);
    this.pathwayConsumptionV2 = window.ldclient.variation('pathway-consumption-v2', false);
    this.journeyConsumptionV2 = window.ldclient.variation('journey-consumption-v2', false);
    this.bookmarkStandalone = window.ldclient.variation('bookmark-standalone', false);
    this.discoverPagePerfChngs = window.ldclient.variation('discover-page-perf-changes', 'OFF');

    this.channelRedesign = window.ldclient.variation('channel-redesigning', false);
    this.groupChannelUserNewNav = window.ldclient.variation('group-channel-users-new-nav', false);

    let firstFeedMap = {
      'feed/todaysLearning': TodayLearningContainer,
      'feed/todaysLearningSearch': TodayLearningContainerSearch,
      'feed/curated': CurateContainer,
      'feed/myAssignments': AssignmentsContainer,
      'feed/teamLearning': FeedContainer,
      'feed/featured': FeaturedContainer,
      'feed/recommended': RecommendedContainer
    };

    let customFeedColumns = {};

    // Get first index option
    this.feedData = this.props.team.get('Feed') || {};
    this.OrgConfigData = this.props.team.get('OrgConfig') || {};
    this.configData = this.props.team.get('config') || {};
    this.pathName = window.location.pathname;
    let feedConfig = new Array(Object.keys(this.feedData).length);
    let customFeed = [];
    let customUrl = [];
    let HomeContentContainerName = HomeContainerv2;
    let MyContentContainerName;
    let GroupsContainerName;
    let ProfileContainerName;
    let MyProfileContainerName;
    let MyDashboard;
    let newProfileLD = window.ldclient.variation('new-profile', false);

    let LoginContainerName = this.loginContainerV2 ? LoginContainerV2 : LoginContainer;
    let SignupContainerName = this.loginContainerV2 ? LoginContainerV2 : SignupContainer;

    let MyTeamContainerName = MyTeamContainerv2;

    ProfileContainerName = newProfileLD ? ProfileContainer : ProfileContainerv3;
    MyProfileContainerName = newProfileLD ? MyProfileContainer : MyProfileContainerv3;
    MyDashboard = newProfileLD ? Dashboard : LearnersDashboard;
    MyContentContainerName = MyContentContainerv2;
    GroupsContainerName = GroupsContainerv2;

    let LearningPathContainerName = MyLearningContainerV5;

    // let SearchContainerName = SearchContainer;
    // if(this.isRevisedSearch) {
    //     SearchContainerName = SearchContainerV2;
    // }

    Object.keys(this.feedData).map(key => {
      let feed = this.feedData[key] || {};
      if (feed.visible != false) {
        feedConfig.splice(feed.index, 0, key);
      }
      if (
        key
          .split('/')
          .pop()
          .indexOf('Z-') > -1
      ) {
        // Custom tab
        let route =
          '/feed/' +
          feed.label
            .split(' ')
            .join('-')
            .toLowerCase();
        let columns = feed.url == 'Courses' ? 2 : 3;
        firstFeedMap[key] = CustomContainer;
        customFeedColumns[key] = columns;
        customUrl[key] = feed.url;
        customFeed.push(
          <Route
            key={key}
            feedKey={key}
            path={route}
            component={CustomContainer}
            columns={columns}
            url={feed.url}
          />
        );
      }
    });
    const firstRouteKey = feedConfig[Object.keys(feedConfig)[0]];

    this.firstFeedChild = (
      <IndexRoute
        component={firstFeedMap[firstRouteKey]}
        feedKey={firstRouteKey}
        columns={customFeedColumns[firstRouteKey] || null}
        url={customUrl[firstRouteKey] || null}
      />
    );

    let TopMenu = listMenu(this.OrgConfigData.topMenu);

    // We allow the first page to be Discover or Home (Currently);
    let InitRoutePath = null;
    let HomeFeedPath = '/';
    try {
      switch (TopMenu[0].defaultLabel) {
        case 'Home':
          InitRoutePath = null;
          HomeFeedPath = '/';
          break;
        case 'Discover':
          InitRoutePath = (
            <Route
              path="/"
              component={
                this.discoverPagePerfChngs === 'OFF' ? DiscoveryContainer : DiscoveryContainerV2
              }
            />
          );
          HomeFeedPath = '(feed)';
          break;
        case 'Me': // Doesn't work right now
          InitRoutePath = (
            <Route path="/" component={ProfileContainerName}>
              <IndexRoute component={MyProfileContainerName} />
            </Route>
          );
          HomeFeedPath = '(feed)';
          break;
        case 'Custom Tab':
          InitRoutePath = (
            <Route path={TopMenu[0].url} component={ProfileContainerName}>
              <IndexRoute component={MyDashboard} />
            </Route>
          );
          HomeFeedPath = '(feed)';
          break;
        default:
          // Custom route
          HomeFeedPath = '(feed)';
          this.props.dispatch(push(TopMenu[0].url));
      }
    } catch (e) {}

    this.props.dispatch(setFirstFeedTab(firstRouteKey));

    this.ROUTES = (
      <Router history={this.props.history}>
        <Route path="log_in" component={LoginContainerName} />
        <Route path="forgot_password" component={ForgotPasswordContainer}>
          <IndexRoute component={SendLinkContainer} />
          <Route path="sent" component={SentLinkContainer} />
          <Route path="confirm/:token" component={ConfirmPasswordContainer} />
          <Route path="changed/:token" component={ChangedPasswordContainer} />
          <Route path="invalid" component={InvalidPasswordContainer} />
        </Route>
        <Route path="create_account" component={SignupContainerName} />
        <Route path="pre_register" component={PreRegisterContainer} />
        <Route path="onboarding" component={OnboardingContainer} />
        <Route path="widgets/v1" component={WidgetContainer} />
        {/* pages render w/o TopNav and Footer go above here */}
        <Route component={TopNavContainer}>
          <Route path="search" component={SearchContainer} />
          <Route path="smartsearch" component={SearchContainerV2} />
          {/* pages render w/ TopNav w/o Footer go above here */}
          <Route component={FooterContainer}>
            {InitRoutePath}
            <Route path={HomeFeedPath} component={HomeContentContainerName}>
              {this.firstFeedChild}
              {customFeed}
              <Route
                path="(feed/)todays-learning"
                feedKey={'feed/todaysLearning'}
                component={TodayLearningContainer}
              />
              <Route
                path="(feed/)todays-learning-search"
                feedKey={'feed/todaysLearningSearch'}
                component={TodayLearningContainerSearch}
              />
              <Route
                path="(feed/)team-learning"
                feedKey={'feed/teamLearning'}
                component={FeedContainer}
              />
              <Route path="(feed/)curate" feedKey={'feed/curate'} component={CurateContainer} />
              {this.showProjectCard && (
                <Route
                  path="(feed/)submission"
                  feedKey={'feed/submission'}
                  component={Submission}
                />
              )}
              <Route
                path="(feed/)my-assignments"
                feedKey={'feed/myAssignments'}
                component={AssignmentsContainer}
              />
              <Route
                path="(feed/)featured"
                feedKey={'feed/featured'}
                component={FeaturedContainer}
              />
              {this.isShowRecommended && (
                <Route
                  path="(feed/)recommended"
                  feedKey={'feed/recommended'}
                  component={RecommendedContainer}
                />
              )}
            </Route>

            <Route path="/me" component={ProfileContainerName}>
              <IndexRoute component={MyProfileContainerName} />
              <Route path="content(/:filter)" component={MyContentContainerName} />
              <Route path="dashboard" component={HistoryContainer} />
              <Route path="career-advisor" component={CareerAdvisorContainer} />
              <Route
                path="groups(/:filter)(/search)"
                component={
                  this.groupChannelUserNewNav ? ProfileMeTabComponents : GroupsContainerName
                }
              />
              <Route path="learning(/:filter)" component={LearningPathContainerName} />
              <Route path="team(/:filter)" component={ProfileMeTabComponents} />
              {this.isLeaderboardConfigEnabled && (
                <Route path="leaderboard" component={LeaderBoardContainerv2} />
              )}
              <Route path="channels(/:filter)(/search)" component={ProfileMeTabComponents} />
              <Route path="learners-dashboard" component={MyDashboard} />
              <Route path="skill-coins" component={SkillCoinsContainer} />
              <Route path="**" component={EmptyContainer} />
            </Route>

            <Route path="/teams">
              <IndexRoute
                component={this.performanceImprovedUI ? GroupPageContainerv2 : GroupPageContainer}
              />
              <Route
                path="(:slug)"
                component={this.performanceImprovedUI ? GroupPageContainerv2 : GroupPageContainer}
              />
              {this.teamAnalyticsEnabled && (
                <Route path="(:slug)/analytics" component={TeamAnalyticsBase} />
              )}
              {this.teamAnalyticsEnabled && (
                <Route
                  path="(:slug)/analytics/assignment"
                  component={TeamAssignmentAnalyticsContainer}
                />
              )}
              <Route path="**" component={EmptyContainer} />
            </Route>

            <Route path="/settings" component={SettingsContainer}>
              <IndexRoute component={AccountDetailsContainer} />
              <Route path="details" component={AccountDetailsContainer} />
              <Route path="triggers" component={UserTriggersContainer} />
              <Route path="integrations" component={IntegrationsContainer} />
              <Route path="**" component={EmptyContainer} />
            </Route>
            <Route
              path="/journey/:slug(/:mode)"
              component={
                this.journeyEnhancement ? JourneyDetailsContainerV2 : JourneyDetailsContainer
              }
            />
            <Route
              path="/journey/:slug/cards(/:cardId)"
              component={
                this.cardV3 && this.journeyConsumptionV2
                  ? JourneyConsumptionContainer
                  : this.journeyEnhancement
                  ? JourneyDetailsContainerV2
                  : JourneyDetailsContainer
              }
            />

            {this.isNewProfileNavigation && this.isLeaderboardConfigEnabled && (
              <Route path="/leaderboard" component={LeaderBoardContainerv2} />
            )}
            {this.bookmarkStandalone && <Route path="/bookmarks" component={BookmarkStandalone} />}
            {(this.isNewProfileNavigation || this.isGroupPageV2) && (
              <Route
                path="/org-groups(/:filter)(/search)"
                component={this.isGroupPageV2 ? GroupsContainerv3 : GroupsContainerv2}
              />
            )}

            {this.isNewProfileNavigation && (
              <Route path="/team(/:filter)" component={MyTeamContainerv2} />
            )}
            <Route path="/channels(/:filter)" component={MyChannelsContainerv2} />
            <Route path="/discover-channels(/:filter)" component={MyChannelsContainerv2} />
            <Route path="/notifications" component={NotificationsContainer} />
            <Route path="/insights/*" component={InsightContainer} />
            <Route path="/video_streams/*" component={InsightContainer} />
            <Route path="/docs" component={DocsContainer} />
            <Route path="/discover/provider/:providerName" component={ProviderContainer} />
            <Route
              path="/channel/:slug"
              component={this.channelRedesign && this.cardV3 ? ChannelRedesignBase : ChannelBaseV2}
            />
            <Route path="/channel/edit/:slug" component={ChannelEdit} />
            <Route path="/channel/:slug/analytics" component={ChannelAnalyticsContainer} />
            <Route path="/discover/videostreams" component={VideoStreamsContainer} />
            <Route path="/discover/channels" component={ChannelsListContainer} />
            <Route path="/discover/pathways" component={PathwaysListContainer} />
            <Route path="/discover/journeys" component={JourneysListContainer} />
            <Route path="/discover/courses" component={CoursesListContainer} />
            <Route path="/discover/:provider" component={PremiumContentContainer} />

            <Route path="/pathways/:slug(/:mode)" component={PathwayDetailsContainer} />
            <Route
              path="/pathways/:slug/cards(/:cardId)"
              component={
                this.cardV3 && this.pathwayConsumptionV2
                  ? PathwayConsumptionContainer
                  : PathwayDetailsContainer
              }
            />
            <Route
              path="/discover"
              component={
                this.discoverPagePerfChngs === 'OFF' ? DiscoveryContainer : DiscoveryContainerV2
              }
            />
            <Route path="/home(/**)" component={TeamContainer} />
            <Route path="skills-graph" component={SkillsGraph} />
            <Route path="skills-directory" component={SkillsDirectory} />
            {/* pages render w/ TopNav and Footer go above here */}
            {this.rubyHandledPath.map((path, index) => {
              return <Route key={index} path={`/${path}(/**)`} component={EmptyContainer} />;
            }) /*Path blow this will be masked by rubyHandledPath*/}
            <Redirect from="/@my_content" to="/" />
            <Route path="/@(:handle)" component={ProfileContainerName}>
              <IndexRoute component={MyProfileContainerName} />
              <Route path="/@(:handle)/content" component={PublicProfileContent} />
            </Route>
            <Route path="/activity/@(:handle)" component={UserActivityStandalone} />
          </Route>
        </Route>
      </Router>
    );
  }

  getChildContext() {
    return {
      team: this.props.team.toJS()
    };
  }

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.map(tab => tab.key);
  }

  componentDidMount() {
    window.addEventListener('storage', this.checkLogUserState);
    this.setFilestackDefaultImages();
    if (
      this.customDefaultImages &&
      this.configData &&
      this.configData.custom_images_bucket_name &&
      this.configData.custom_images_region
    ) {
      let payload = {
        bucket_info: {
          bucket_name: this.configData.custom_images_bucket_name,
          bucket_region: this.configData.custom_images_region
        }
      };
      getDefaultImageS3(payload)
        .then(data => {
          if (data && data.image_links && data.image_links.length) {
            if (localStorage.getItem('prevImage') === 'S3') {
              localStorage.setItem('defaultImagesS3', JSON.stringify(data.image_links));
            } else {
              localStorage.removeItem('cardImages');
              localStorage.setItem('defaultImagesS3', JSON.stringify(data.image_links));
              localStorage.setItem('prevImage', 'S3');
            }
          } else {
            this.checkS3Image();
          }
        })
        .catch(err => {
          console.error(`Error in Routing.getDefaultImageS3.func : ${err}`);
          this.checkS3Image();
        });
    } else {
      this.checkS3Image();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.isLoggedIn && this.props.pathname !== nextProps.pathname) {
      // CLear localstorage used for storing untranslated strings
      // TODO: Remove this line once we migrate away from localstorage
      localStorage.removeItem('untranslatedStrings');
      refreshTranslations();
    }
    if (this.props.isLoggedIn !== nextProps.isLoggedIn) {
      return true;
    } else if (
      this.props.team.get('Feed') !== nextProps.team.get('Feed') ||
      this.props.team.get('OrgConfig') !== nextProps.team.get('OrgConfig') ||
      this.props.team.get('config') !== nextProps.team.get('config')
    ) {
      return true;
    } else if (!nextProps.isLoggedIn) {
      this.pathName = window.location.pathname;
      return true;
    } else return false;
  }

  setFilestackDefaultImages = () => {
    let stored = JSON.parse(localStorage.getItem('filestackDefaultImages'));
    let now = new Date().getTime().toString();
    // refresh localStorage after every 24 hours
    if (stored && (now - stored.timestamp) / 1000 / 60 / 60 < 24) {
      return true;
    } else {
      let defaultImageUrl =
        window.process.env.FilestackDefaultImagesUrl ||
        'https://dol6fvsidtwsq.cloudfront.net/default_images/non_prod_default_images.json';
      if (defaultImageUrl) {
        getFilestackDefaultImages(defaultImageUrl)
          .then(resp => {
            let object = { value: resp, timestamp: new Date().getTime() };
            localStorage.setItem('filestackDefaultImages', JSON.stringify(object));
          })
          .catch(err => {
            console.error(`Error in Routing.getFilestackDefaultImages.func : ${err}`);
          });
      }
    }
  };

  checkS3Image = () => {
    localStorage.setItem('defaultImagesS3', false);
    if (localStorage.getItem('prevImage') === 'S3') {
      localStorage.removeItem('cardImages');
    }
    localStorage.setItem('prevImage', 'filestack');
  };

  checkLogUserState = event => {
    if (
      event.key === 'isLoggedIn' &&
      event.newValue !== event.oldValue &&
      (event.newValue === 'true' || (event.newValue === 'false' && this.pathName !== '/log_in'))
    ) {
      // this fixes IE redundant page reload on login
      if (!document.hasFocus()) {
        window.location.reload();
      }
    }
  };

  render() {
    let isMobileDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
      navigator.userAgent
    );

    if (
      this.mobileAppBanner &&
      (this.pathName.indexOf('/forgot_password') == -1 && this.pathName.indexOf('/visit') == -1) &&
      isMobileDevice
    ) {
      return <MobileBanner />;
    }

    if (this.pathName === '/onboarding' && this.onboardingVersion !== 'v1') {
      window.location.href = '/onboard/v2';
      return <span />;
    }
    if (this.props.isLoggedIn === true) {
      if (this.pathName === '/log_in' || this.pathName === '/create_account') {
        let link = localStorage.getItem('redirectTo_App');
        if (link && JWT.token) {
          localStorage.removeItem('redirectTo_App');
          let fullName = this.props.name
            ? this.props.name.split(' ')
            : [this.props.firstName, this.props.lastName];
          link = `${link}?jwtToken=${JWT.token}&firstname=${fullName[0]}&lastname=${fullName[1]}`;
          window.location.href = link;
        } else {
          this.props.dispatch(push(listMenu(this.OrgConfigData.topMenu)[0].defaultUrl));
        }
      }
      if (readCookie('redirectRoute')) {
        window.location = readCookie('redirectRoute');
        deleteCookie('redirectRoute');
      } else {
        return this.ROUTES;
      }
    } else if (this.props.isLoggedIn === false) {
      if (
        this.pathName === '/create_account' ||
        this.pathName === '/widgets/v1' ||
        this.pathName.indexOf('/forgot_password') > -1 ||
        this.pathName.indexOf('/@') > -1
      ) {
        return this.ROUTES;
      } else {
        return this.loginContainerV2 ? <LoginContainerV2 /> : <LoginContainer />;
      }
    } else {
      return <div />;
    }
  }
}

Routing.propTypes = {
  team: PropTypes.object,
  isLoggedIn: PropTypes.bool,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  name: PropTypes.string,
  pathname: PropTypes.string,
  history: PropTypes.object,
  profile: PropTypes.object
};

Routing.childContextTypes = {
  team: PropTypes.object
};

function mapStateToProps(state) {
  return {
    isLoggedIn: state.currentUser.get('isLoggedIn'),
    firstName: state.currentUser.get('first_name'),
    lastName: state.currentUser.get('last_name'),
    name: state.currentUser.get('name'),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team
  };
}
export default connect(mapStateToProps)(Routing);
