import React, { Component } from 'react';
import { connect } from 'react-redux';

import EditIcon from 'material-ui/svg-icons/image/edit';

import { getSkills } from 'edc-web-sdk/requests/skillsDirectory';

import Skill from './Skill';
import { openSkillsDirectoryModal } from '../../actions/modalActions';

class SkillsDirectory extends Component {
  constructor(props) {
    super(props);

    this.state = {
      skills: [],
      role: {}
    };

    this.styles = {
      editIcon: {
        height: '20px',
        width: '20px',
        cursor: 'pointer'
      }
    };
  }

  componentDidMount() {
    let skills = getSkills().topics;
    this.setState({ skills });
    this.props.dispatch(openSkillsDirectoryModal(this.changeRole));
  }

  changeRole = role => {
    let skills = getSkills().topics;
    this.setState({ role, skills });
  };

  editClickHandler = () => {
    this.props.dispatch(openSkillsDirectoryModal(this.changeRole));
  };

  skillClickHandler = () => {};

  render() {
    return (
      <div className="skills-direcrory-container">
        <h3>
          {this.state.role.name
            ? `Skill related to the role "${this.state.role.name}"`
            : 'All Skills'}{' '}
          <span>
            <EditIcon style={this.styles.editIcon} onClick={this.editClickHandler} />
          </span>
        </h3>
        <div className="skills">
          {this.state.skills.map(skill => {
            return <Skill skill={skill} skillClickHandler={this.skillClickHandler} />;
          })}
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  modal: state.modal.toJS()
}))(SkillsDirectory);
