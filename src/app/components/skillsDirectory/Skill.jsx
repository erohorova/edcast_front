import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Skill extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.styles = {
      skillbox: {
        backgroundImage: `url(${this.props.skill.image_url})`
      },
      title: {}
    };
  }

  render() {
    return (
      <div
        style={this.styles.skillbox}
        className="skillbox pointer"
        onClick={this.props.skillClickHandler}
      >
        <span>{this.props.skill.label}</span>
      </div>
    );
  }
}

Skill.propTypes = {
  skill: PropTypes.object,
  skillClickHandler: PropTypes.func
};

export default Skill;
