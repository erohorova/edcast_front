import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton/IconButton';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import Comment from 'edc-web-sdk/components/icons/Comment';
import { toggleLikeCardAsync } from '../../actions/cardsActions';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import CardModal from '../modals/CardModal';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import { CardHeader } from 'material-ui/Card';
import { Avatar } from 'material-ui';
import Poll from '../feed/Poll';
import {
  openCardStatsModal,
  openStatusModal,
  openPathwayOverviewModal,
  openSmartBiteOverviewModal
} from '../../actions/modalActions';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import CompletedAssignment from 'edc-web-sdk/components/icons/CompletedAssignmentGrey';
import { markAsComplete, markAsUncomplete } from 'edc-web-sdk/requests/cards.v2';
import MarkdownRenderer from '../common/MarkdownRenderer';
import CreationDate from '../common/CreationDate';
import Download from 'edc-web-sdk/components/icons/Download';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import * as logoType from '../../constants/logoTypes';
import getCardType from '../../utils/getCardType';
import getVideoStatus from '../../utils/getVideoStatus';
import getFleIcon from '../../utils/getFileIcon';
import getCardImage from '../../utils/getCardImage';
import checkResources from '../../utils/checkResources';
import linkPrefix from '../../utils/linkPrefix';
import map from 'lodash/map';
import * as upshotActions from '../../actions/upshotActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import pdfPreviewUrl from '../../utils/previewPdf';

const logoObj = logoType.LOGO;

class LearningCard extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      insightDropDownActions: {
        paddingRight: '5px',
        width: 'auto'
      },
      download: {
        width: '100%',
        height: '100%',
        padding: 0
      },
      tooltipStyles: {
        left: '-50%'
      },
      cardImgContainerImg: {
        zIndex: 2
      },
      cardImgContainerSvg: {
        zIndex: 2,
        position: 'relative'
      },
      avatarLink: {
        marginRight: 0,
        display: 'inline-block'
      },
      avatar: {
        marginRight: '9px'
      },
      cardHeader: {
        padding: '10px 8px 0'
      },
      clearfix: {
        width: '100%',
        height: '100%'
      },
      actionBtn: {
        verticalAlign: 'middle',
        width: 19,
        height: 19,
        padding: 0
      },
      iconStyle: {
        height: 19,
        width: 19
      },
      tooltipActiveBts: {
        marginTop: -32
      },
      actionsCounter: {
        marginRight: '-8px'
      }
    };
    this.state = {
      card: props.card,
      pendingLike: false,
      isUpvoted: props.card.isUpvoted,
      votesCount: props.card.votesCount,
      commentsCount: props.card.commentsCount,
      defaultImage: '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg',
      modalOpen: false,
      isCompleted:
        props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED',
      showTopic: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      isShowTooltip: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
    this.cardLikeHandler = this.cardLikeHandler.bind(this);
    this.completeClickHandler = this.completeClickHandler.bind(this);
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
  }

  setStatePending = (state, val) => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  componentWillReceiveProps(nextProps) {
    let isCompleted =
      nextProps.card.completionState &&
      nextProps.card.completionState.toUpperCase() === 'COMPLETED';
    if (isCompleted != this.state.isCompleted) {
      this.setState({ isCompleted });
    }
  }

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise((resolve, reject) => {
      func(id, cardType, up, this.props.dispatch).then(result => {
        resolve(result);
      });
    });
  };

  async cardLikeHandler() {
    if (this.state.pendingLike) {
      return;
    }
    await this.setStatePending('pendingLike', true);
    await this.asyncDispatch(
      toggleLikeCardAsync,
      this.state.card.id,
      this.state.card.cardType,
      !this.state.isUpvoted
    ).then(() => {
      this.setState({
        votesCount: Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0),
        isUpvoted: !this.state.isUpvoted
      });
      if (this.state.upshotEnabled) {
        var name;
        if (this.state.card.resource && this.state.card.resource.title) {
          name = this.state.card.resource.title;
        } else {
          name = this.state.card.message;
        }
        upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
          name: name,
          category: this.state.card.cardType,
          type: this.state.card.cardSubtype,
          description: this.state.card.resource.description,
          status: !!this.state.card.completionState
            ? this.state.card.completionState
            : 'Incomplete',
          rating: this.state.card.averageRating,
          like: this.state.isUpvoted ? 'Yes' : 'No',
          event: this.state.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
        });
      }
    });
    await this.setStatePending('pendingLike', false);
  }

  updateCommentCount = count => {
    this.setState({ commentsCount: count });
  };

  cardUpdated() {
    if (this.state.card) {
      fetchCard(this.state.card.id).then(data => {
        this.setState({
          card: data
        });
      });
    }
  }

  openModal = () => {
    if (this.state.card.cardType === 'pack') {
      this.props.dispatch(
        openPathwayOverviewModal(
          this.state.card,
          logoObj,
          this.state.defaultImage,
          this.cardUpdated.bind(this)
        )
      );
    } else if (this.state.card.cardType === 'journey') {
      this.standaloneLinkClickHandler();
    } else {
      this.props.dispatch(
        openSmartBiteOverviewModal(
          this.state.card,
          logoObj,
          this.state.defaultImage,
          this.cardUpdated.bind(this)
        )
      );
    }
  };

  closeModal = () => {
    this.setState({ modalOpen: false }, () => {
      document.body.style.overflow = '';
    });
  };

  standaloneLinkClickHandler = () => {
    let linkPrefixValue = linkPrefix(this.state.card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${this.state.card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${this.state.card.id}`
    ) {
      return;
    }
    // If coming from modal search, remove overflow.
    // document.getElementsByTagName('body')[0].style.overflow = '';
    this.props.dispatch(push(`/${linkPrefixValue}/${this.state.card.slug}`));
    if (this.props.toggleSearch) {
      this.props.toggleSearch();
    }
  };

  channelMessage = (channel, index, card) => {
    let message = '';
    if (card.channels.length <= 2 || (card.channels.length > 2 && index === 0)) {
      message = card.channels[index].label;
    }
    message = message + (index === 0 && card.channels.length === 2 ? ' and ' : '');
    return message;
  };

  handleCardAnalayticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.card));
  };

  completeClickHandler() {
    let isPack = this.state.card.cardType === 'pack' || this.state.card.cardType === 'journey';
    if (
      (!isPack && !this.state.isCompleted) ||
      (isPack &&
        this.state.card.state === 'published' &&
        +this.state.card.completedPercentage === 98)
    ) {
      markAsComplete(this.state.card.id, { state: 'complete' }).then(() => {
        let compoundMessage = [
          'You have completed this task. You can view it under',
          '/me/content/completed',
          'Me',
          'Tab'
        ];
        // getting if this is a standlone insight page & redirecting to the previous link.
        this.state.card.featuredHidden = true;
        this.setState({ isCompleted: true }, () => {
          this.props.dispatch(openStatusModal('', null, compoundMessage));
        });
      });
    } else if (
      (!isPack && this.state.isCompleted) ||
      (isPack && +this.state.card.completedPercentage === 100)
    ) {
      markAsUncomplete(this.state.card.id).then(() => {
        this.setState({ isCompleted: false }, () => {
          setTimeout(() => {
            this.props.dispatch(openStatusModal('You have marked this task as incomplete'));
          }, 1500);
        });
      });
    } else {
      let msg =
        this.state.card.state === 'published'
          ? `Please complete all pending cards before marking the ${
              this.state.card.cardType === 'pack' ? 'Pathway' : 'Journey'
            } as complete.`
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  }

  showTopicToggleClick = () => {
    this.setState({ showTopic: !this.state.showTopic });
  };

  downloadBlock(file) {
    return (
      <span
        className="roll"
        onMouseEnter={() => {
          this.setState({ [file.handle]: true });
        }}
        style={{ display: this.state[file.handle] ? 'inherit' : 'none' }}
      >
        <a href={file.url} download={file.handle} target="_blank">
          <IconButton
            style={this.styles.download}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            aria-label={tr('download')}
            tooltip={tr('Download')}
          >
            <Download />
          </IconButton>
        </a>
      </span>
    );
  }

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.state.card.dismissed) {
      return null;
    }
    let card = checkResources(this.state.card);
    let markFeatureDisabled = card.markFeatureDisabledForSource;
    let message = card.title || card.message || card.name;
    let videoData = {},
      iconFileSrc = '';
    let disableTopics = card.tags.length > 0;
    let isFileAttached = card.filestack && !!card.filestack.length;
    let videoFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('video/')
    );
    let imageFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('image/')
    );
    let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
    let cardType = getCardType(card, videoFileStack, fileFileStack);
    message = message ? message.replace(/\\\*/g, '*') : '';
    let imageExists =
      (card.resource &&
        card.resource.imageUrl &&
        !~card.resource.imageUrl.indexOf('default_card_image')) ||
      imageFileStack;

    if (cardType === 'VIDEO') {
      videoData = getVideoStatus(card);
      imageExists =
        (card.videoStream &&
          card.videoStream.imageUrl &&
          !~card.videoStream.imageUrl.indexOf('default_card_image')) ||
        imageExists;
    }

    if (cardType === 'ARTICLE') {
      let gDriveLink =
        card.resource &&
        card.resource.url &&
        (!!~card.resource.url.indexOf('docs.google.com') ||
          !!~card.resource.url.indexOf('drive.google.com') ||
          !!~card.resource.url.indexOf('sharepoint.com'));
    }

    let cardImage = getCardImage(card, imageFileStack);

    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let cardSvgBackground = imageExists ? cardImage : this.state.defaultImage;
    let showBlurryBackground = true;
    if (imageExists && cardImage && cardImage.slice(-3) === 'png') {
      showBlurryBackground = false;
    }
    let isOwner = this.props.author && this.props.author.id == this.props.currentUser.id;
    let isCompleted = this.state.isCompleted;
    if (isFileAttached) {
      let fileType = card.filestack[0].mimetype && card.filestack[0].mimetype.slice(-3);
      iconFileSrc = getFleIcon(fileType);
      showBlurryBackground = !(fileType === 'png');
    }
    let viewMore = this.state.truncateTitle ? (
      <span>
        ...
        <a className="viewMore" onTouchTap={this.viewMoreTitleHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let viewMoreMessage = this.state.truncateMessage ? (
      <span>
        ...
        <a className="viewMore" target="_blank" onTouchTap={this.viewMoreMessageHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let isShowTopImage =
      (cardType === 'TEXT' && imageExists) ||
      (cardType !== 'TEXT' && cardType !== 'POLL' && cardType !== 'QUIZ' && cardType !== 'FILE') ||
      (cardType === 'QUIZ' && imageExists) ||
      (cardType === 'POLL' && imageExists);
    let svgStyle = {
      filter: `url(#blur-effect-learning-${card.id})`
    };
    let moreThanTwoChannels = card.channels && card.channels.length > 2;
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    return (
      <div className="card-v3 small-12 column">
        <Paper className="paper-card">
          {isShowTopImage && (
            <div className="card-img-container" onTouchTap={this.openModal}>
              {card.cardType === 'pack' && <small className="pathway-label">{tr('PATHWAY')}</small>}
              {showBlurryBackground && (
                <div className="card-img button-icon card-blurred-background">
                  <svg width="100%" height="100%">
                    <image
                      style={svgStyle}
                      xlinkHref={cardSvgBackground}
                      x="-30%"
                      y="-30%"
                      width="160%"
                      height="160%"
                    />
                    <filter id={`blur-effect-learning-${card.id}`}>
                      <feGaussianBlur stdDeviation="10" />
                    </filter>
                  </svg>
                </div>
              )}
              {videoFileStack ? (
                <div className="fp fp_video fp_video_card">
                  <video
                    src={card.filestack[0].url}
                    poster={card.filestack && card.filestack[1] && card.filestack[1].url}
                    preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                    controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                  />
                </div>
              ) : (
                <svg width="100%" height="100%" style={this.styles.cardImgContainerSvg}>
                  <image
                    xlinkHref={cardSvgBackground}
                    width="100%"
                    style={this.styles.cardImgContainerImg}
                    height="100%"
                  />
                </svg>
              )}
              {(cardType === 'VIDEO' || card.cardSubtype === 'video') && (
                <div>
                  <PlayIcon className="center-play-icon" color={colors.white} />
                  <div className={videoData.videoLabelClass}>{tr(videoData.videoLabelText)}</div>
                </div>
              )}
            </div>
          )}
          {fileFileStack && cardType !== 'QUIZ' && (
            <div>
              {card.filestack.map(file => {
                if (file.mimetype && ~file.mimetype.indexOf('image/')) {
                  return;
                } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                  return (
                    <div className="card-img-container">
                      <div key={file.handle} className="fp fp_video">
                        <span>
                          <video
                            onMouseEnter={() => {
                              this.setState({ [file.handle]: true });
                            }}
                            onMouseLeave={() => {
                              this.setState({ [file.handle]: false });
                            }}
                            controls
                            src={file.url}
                            controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                          />
                          {!isDownloadContentDisabled && this.downloadBlock(file)}
                        </span>
                      </div>
                    </div>
                  );
                } else {
                  return (
                    <div className="card-img-container">
                      <div key={file.handle} className="fp fp_preview">
                        <span>
                          <iframe
                            height="480px"
                            width="255px"
                            src={pdfPreviewUrl(file.url, expireAfter, this.props.currentUser.id)}
                            onMouseEnter={() => {
                              this.setState({ [file.handle]: true });
                            }}
                            onMouseLeave={() => {
                              this.setState({ [file.handle]: false });
                            }}
                          />
                          {!isDownloadContentDisabled && this.downloadBlock(file)}
                        </span>
                      </div>
                    </div>
                  );
                }
              })}
            </div>
          )}
          <div className="card-details">
            {card.author && (
              <CardHeader
                title={
                  <div className="channels-block">
                    <div className="author-info-container">
                      <div className="author-info">
                        <a
                          className="user-name"
                          onTouchTap={() => {
                            this.props.dispatch(push(`/${card.author.handle}`));
                          }}
                        >
                          {`${card.author.firstName ? card.author.firstName : ''} ${
                            card.author.lastName ? card.author.lastName : ''
                          }`}
                        </a>
                        {card.channels && !!card.channels.length && (
                          <div>
                            <span> &nbsp; posted in </span>
                            {card.channels.slice(0, 2).map((channel, index) => {
                              return (
                                <a
                                  className="channel-label"
                                  title={channel.label}
                                  key={index}
                                  onTouchTap={() => {
                                    this.props.dispatch(push(`/channel/${channel.id}`));
                                  }}
                                >
                                  {this.channelMessage(channel, index, card)}
                                </a>
                              );
                            })}
                            {moreThanTwoChannels && (
                              <a
                                className="matte insight-other-channels"
                                title={card.channels
                                  .slice(1)
                                  .map(item => {
                                    return item.label;
                                  })
                                  .join(', ')}
                                style={!this.props.author ? { margin: '0' } : {}}
                                onMouseEnter={() => this.setState({ channelTooltip: true })}
                                onMouseLeave={() => this.setState({ channelTooltip: false })}
                              >
                                {`${tr('and')} ${card.channels.length - 1} ${tr('other')} ${tr(
                                  card.channels.length > 2 ? 'channels' : 'channel'
                                )}`}
                              </a>
                            )}
                          </div>
                        )}
                      </div>
                      <span>
                        <CreationDate
                          card={card}
                          standaloneLinkClickHandler={this.standaloneLinkClickHandler}
                        />
                      </span>
                    </div>
                  </div>
                }
                subtitle={
                  card.publishedAt && (
                    <div className="header-secondary-text">
                      <span className="matte" />
                    </div>
                  )
                }
                avatar={
                  <a
                    style={this.styles.avatarLink}
                    onTouchTap={() => {
                      this.props.dispatch(push(`/${card.author.handle}`));
                    }}
                    target="_blank"
                  >
                    <Avatar
                      src={
                        card.author &&
                        (card.author.picture ||
                          (card.author.avatarimages && card.author.avatarimages.small) ||
                          defaultUserImage)
                      }
                      size={26}
                      style={this.styles.avatar}
                    />
                  </a>
                }
                style={this.styles.cardHeader}
              />
            )}
            <div className="cursor" onTouchTap={this.openModal}>
              <div className="image-exists vertical-spacing-small">
                <div className="horizontal-spacing-small">
                  {(card.eclSourceLogoUrl ||
                    (card.eclSourceTypeName && logoObj[card.eclSourceTypeName] !== undefined)) && (
                    <div className="inline-block horizontal-spacing-small">
                      <div className="inline-block">
                        <img
                          className="provider-logo"
                          src={card.eclSourceLogoUrl || logoObj[card.eclSourceTypeName]}
                        />
                      </div>
                      <div className="mid-align inline-block">
                        <small>|</small>
                      </div>
                    </div>
                  )}
                  {iconFileSrc && (
                    <div className="inline-block horizontal-spacing-small">
                      <div className="inline-block">
                        <img className="provider-logo" src={iconFileSrc} />
                      </div>
                      <div className="mid-align inline-block">
                        <small>|</small>
                      </div>
                    </div>
                  )}
                  <div className="inline-block">
                    <small className="label-text">
                      {tr(cardType.toUpperCase())}{' '}
                      {cardType === 'PATHWAY' && !!card.packCards.length && (
                        <span>({card.packCards.length})</span>
                      )}
                    </small>
                  </div>
                </div>
                <div
                  className={
                    cardType === 'POLL' || cardType === 'QUIZ'
                      ? 'main-card-content-container'
                      : !!card.author
                      ? 'card-container-with-image'
                      : 'card-container-with-image-no-author'
                  }
                >
                  {/*Topics are Tags. We show the word 'Tags' to user.*/}
                  <div className={this.state.showTopic ? 'topic-block' : 'without-topics-block'}>
                    <div className={this.state.showTopic ? 'insight-topics' : 'without-topics'}>
                      <div className="topics-list">
                        {map(card.tags, 'name')
                          .slice(0, 5)
                          .join(', ') || tr('Sorry, no tags!')}{' '}
                        <span>{card.tags.length > 5 && '...'}</span>
                      </div>
                    </div>
                  </div>
                  {cardType === 'TEXT' ? (
                    <div className="vertical-spacing-large">
                      <div
                        className={
                          imageExists
                            ? 'openCardFromSearch button-icon title title-with-image'
                            : 'openCardFromSearch button-icon title text-title'
                        }
                      >
                        {card.title && (
                          <div className="card-title">
                            <MarkdownRenderer
                              markdown={
                                this.state.truncateTitle ? card.title.substr(0, 260) : card.title
                              }
                            />
                            {viewMore}
                          </div>
                        )}
                        <div className="button-icon title text-title" />
                        {message && (
                          <div className="markdown-text">
                            <MarkdownRenderer
                              markdown={
                                this.state.truncateMessage
                                  ? this.truncateMessageText(message)
                                  : message
                              }
                            />
                            {viewMoreMessage}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : (
                    <div className="vertical-spacing-large">
                      <div
                        className={
                          card.author
                            ? 'button-icon title has-author'
                            : 'button-icon title no-author'
                        }
                      >
                        {!!card.resource &&
                        (!!card.resource.title || !!card.resource.description) ? (
                          <div>
                            {card.resource.title && (
                              <div
                                className="text-title"
                                dangerouslySetInnerHTML={{ __html: card.resource.title }}
                              />
                            )}
                            {card.resource.description && (
                              <div
                                className="text-title"
                                dangerouslySetInnerHTML={{ __html: card.resource.description }}
                              />
                            )}
                          </div>
                        ) : (
                          message
                        )}
                      </div>
                      {(cardType === 'POLL' || cardType === 'QUIZ') && !isShowTopImage && (
                        <div className="poll-content">
                          <Poll card={this.state.card} cardUpdated={this.cardUpdated.bind(this)} />
                        </div>
                      )}
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="actions-bar">
              <div className="clearfix" style={this.styles.clearfix}>
                {Permissions['enabled'] !== undefined && Permissions.has('LIKE_CONTENT') && (
                  <div className="icon-inline-block">
                    <IconButton
                      tooltip={!this.state.isUpvoted ? tr('Like') : tr('Liked')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.actionBtn}
                      className="like"
                      onTouchTap={this.cardLikeHandler}
                      iconStyle={this.styles.iconStyle}
                    >
                      {!this.state.isUpvoted && <LikeIcon />}
                      {this.state.isUpvoted && <LikeIconSelected />}
                    </IconButton>
                    <small className="actions-counter">
                      {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : ''}
                    </small>
                  </div>
                )}
                {Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT') && (
                  <div className="icon-inline-block">
                    <IconButton
                      tooltip={tr('Comment')}
                      aria-label={tr('Comment')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.actionBtn}
                      className="comment"
                      onTouchTap={this.openModal}
                      iconStyle={this.styles.iconStyle}
                    >
                      <Comment className="mid-align" color={colors.gray} />
                    </IconButton>
                    <small style={this.styles.actionsCounter} className="actions-counter">
                      {this.state.commentsCount ? abbreviateNumber(this.state.commentsCount) : ''}
                    </small>
                  </div>
                )}
                {(isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && cardType !== 'QUIZ' && (
                  <div className="icon-inline-block">
                    <IconButton
                      tooltip={tr('Card Statistics')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      className="statistics"
                      style={this.styles.actionBtn}
                      onTouchTap={this.handleCardAnalayticsModal}
                      iconStyle={this.styles.iconStyle}
                    >
                      <CardAnalyticsV2 />
                    </IconButton>
                  </div>
                )}
                {Permissions.has('MARK_AS_COMPLETE') && cardType !== 'QUIZ' && cardType !== 'POLL' && (
                  <div className="icon-inline-block">
                    <IconButton
                      tooltip={
                        !markFeatureDisabled &&
                        tr(
                          isCompleted
                            ? this.isUncompleteEnabled
                              ? 'Mark as Incomplete'
                              : 'Completed'
                            : 'Mark as Complete'
                        )
                      }
                      aria-label={
                        !markFeatureDisabled &&
                        tr(
                          isCompleted
                            ? this.isUncompleteEnabled
                              ? 'Mark as Incomplete'
                              : 'Completed'
                            : 'Mark as Complete'
                        )
                      }
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.actionBtn}
                      className="completed"
                      onTouchTap={
                        !markFeatureDisabled && (this.isUncompleteEnabled || !isCompleted)
                          ? this.completeClickHandler
                          : () => {
                              return null;
                            }
                      }
                      iconStyle={this.styles.iconStyle}
                    >
                      <CompletedAssignment color={isCompleted ? colors.primary : colors.darkGray} />
                    </IconButton>
                  </div>
                )}
                <div className="float-right button-icon inline-block">
                  <InsightDropDownActions
                    card={this.state.card}
                    author={this.state.card.author}
                    dismissible={false}
                    disableTopics={disableTopics}
                    showTopicToggleClick={this.showTopicToggleClick}
                    style={this.styles.insightDropDownActions}
                    isStandalone={this.props.isStandalone}
                    cardUpdated={this.cardUpdated.bind(this)}
                    removeCardFromList={this.props.removeCardFromList}
                    markAsComplete={this.props.markAsComplete}
                    iconStyle={this.styles.iconStyle}
                  />
                </div>
              </div>
            </div>
          </div>
        </Paper>

        {this.state.modalOpen && !this.props.withoutCardModal && (
          <CardModal
            logoObj={logoObj}
            defaultImage={this.state.defaultImage}
            card={card}
            isUpvoted={this.state.isUpvoted}
            updateCommentCount={this.updateCommentCount}
            commentsCount={this.state.commentsCount}
            votesCount={this.state.votesCount}
            closeModal={this.closeModal}
            likeCard={this.cardLikeHandler}
          />
        )}
      </div>
    );
  }
}

LearningCard.propTypes = {
  withoutCardModal: PropTypes.bool,
  author: PropTypes.object,
  team: PropTypes.object,
  card: PropTypes.object,
  currentUser: PropTypes.object,
  pathname: PropTypes.string,
  isStandalone: PropTypes.bool,
  markAsComplete: PropTypes.func,
  toggleSearch: PropTypes.func,
  removeCardFromList: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(LearningCard);
