import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import find from 'lodash/find';
import upperFirst from 'lodash/upperFirst';
import TooltipLabel from '../common/TooltipLabel';
let LocaleCurrency = require('locale-currency');
import MarkdownRenderer from '../common/MarkdownRenderer';

class SearchCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBrokenImage: false
    };

    this.styles = {
      skillCoin: {
        borderTop: '3px solid #448AFF'
      },
      paid: {
        borderTop: '3px solid #4CAF50'
      },
      free: {
        borderTop: '3px solid #FFA500'
      },
      skillCoinPrice: {
        backgroundColor: '#448AFF'
      },
      paidPrice: {
        backgroundColor: '#4CAF50'
      },
      freePrice: {
        backgroundColor: '#FFA500'
      },
      cardTypeIcon: {
        backgroundColor:
          this.props.team.config.lxpCustomCSS.configs.header.headerBackgroundColor.styles
            .backgroundColor || '#454560'
      },
      noBorder: {
        borderTop: 'none'
      }
    };
  }

  getDefaultImage = () => {
    let image = '';
    switch (this.props.card.content_type) {
      case 'article':
        image = '/i/images/ARTICLE.png';
        break;
      case 'poll':
        image = '/i/images/POLL.png';
        break;
      case 'journey':
        image = '/i/images/JOURNEY.png';
        break;
      case 'pathway':
        image = '/i/images/PATHWAY.png';
        break;
      case 'file':
        image = '/i/images/FILE.png';
        break;
      case 'video':
        image = '/i/images/VIDEO.png';
        break;
      case 'insight':
        image = '/i/images/INSIGHT.png';
        break;
      case 'video_stream':
        image = '/i/images/livestream.png';
        break;
      case 'course':
        image = '/i/images/COURSES.png';
        break;
      default:
        image = '/i/images/DEFAULT.png';
        break;
    }
    return image;
  };

  isBrokenImage = () => {
    this.setState({ isBrokenImage: true });
  };

  getDuration = () => {
    let secs = this.props.card.duration;
    let hrs = Math.floor(secs / 3600);
    let mins = Math.floor((secs % 3600) / 60);
    let duration = '';
    if (hrs && mins) {
      duration = `${hrs} Hr(s) ${mins} Min(s)`;
    } else if (!hrs) {
      duration = `${mins} Min(s)`;
    } else {
      duration = `${hrs} Hr(s)`;
    }
    return duration;
  };

  getCardTypeImage = () => {
    let image = '';
    switch (this.props.card.content_type) {
      case 'article':
        image = '/i/images/ARTICLE.svg';
        break;
      case 'poll':
        image = '/i/images/poll.svg';
        break;
      case 'journey':
        image = '/i/images/journey.svg';
        break;
      case 'pathway':
        image = '/i/images/pathway.svg';
        break;
      case 'file':
        image = '/i/images/file.svg';
        break;
      case 'video':
        image = '/i/images/video.svg';
        break;
      case 'insight':
        image = '/i/images/insight.svg';
        break;
      case 'link':
        image = '/i/images/link.svg';
        break;
      case 'video_stream':
        image = '/i/images/video_stream.svg';
        break;
      case 'course':
        image = '/i/images/course.svg';
        break;
      default:
        image = '/i/images/default.svg';
        break;
    }
    return image;
  };

  getCountrySpecificPrice = () => {
    let prices = this.props.card.prices;
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData =
      find(prices, { currency: 'SKILLCOIN' }) ||
      find(prices, { currency: currency }) ||
      find(prices, { currency: 'USD' }) ||
      find(prices, function(price) {
        return price.currency != 'SKILLCOIN';
      });
    return priceData.currency + ' ' + priceData.amount;
  };

  cardClickHandler = () => {
    let type = '';
    let id = this.props.card.id;
    switch (this.props.card.content_type) {
      case 'journey':
        type = 'journey';
        break;
      case 'pathway':
        type = 'pathways';
        break;
      default:
        type = 'insights';
        break;
    }
    this.props.dispatch(push(`/${type}/${id}`));
  };

  render() {
    let card = this.props.card;
    let cardImage = this.props.card.image || this.getDefaultImage(this.props.currentUser.id);
    if (this.state.isBrokenImage) {
      cardImage = this.getDefaultImage(this.props.currentUser.id);
    }
    let price = card.prices.length ? this.getCountrySpecificPrice() : 'Free';
    let duration = card.duration ? this.getDuration() : null;
    let cardTypeImage = this.getCardTypeImage();
    let cardType = card.content_type === 'video_stream' ? 'Video Stream' : card.content_type;
    return (
      <div className="searchCard" onClick={this.cardClickHandler}>
        <img
          className="cardImage"
          src={cardImage}
          style={
            !this.props.isPublicContent
              ? price === 'Free'
                ? this.styles.free
                : price.indexOf('SKILL') >= 0
                ? this.styles.skillCoin
                : this.styles.paid
              : this.styles.noBorder
          }
          alt={card.title}
          onError={this.isBrokenImage}
        />
        {!this.props.isPublicContent && (
          <div
            className="price"
            style={
              price === 'Free'
                ? this.styles.freePrice
                : price.indexOf('SKILL') >= 0
                ? this.styles.skillCoinPrice
                : this.styles.paidPrice
            }
          >
            {price}
          </div>
        )}
        <div className="cardType" style={this.styles.cardTypeIcon}>
          <img src={cardTypeImage} alt="Cardtype" />
        </div>
        <div className="detailContainer">
          <p className="typeAndTime">
            {upperFirst(cardType)} {duration && <span>| {duration}</span>}
          </p>
          <TooltipLabel
            text={
              (card.title && card.title.length > 100
                ? card.title.slice(0, 100) + '...'
                : card.title) || 'No Title'
            }
            html={true}
            position={'bottom'}
          >
            <h5 className="cardTitle">
              {card.title ? <MarkdownRenderer markdown={card.title} /> : 'No Title'}
            </h5>
          </TooltipLabel>
          <TooltipLabel
            text={
              (card.description && card.description.length > 100
                ? card.description.slice(0, 100) + '...'
                : card.description) || 'No Description'
            }
            html={true}
            position={'bottom'}
          >
            <p className="desc">{card.description || 'No Description'}</p>
          </TooltipLabel>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

SearchCard.propTypes = {
  isPublicContent: PropTypes.bool,
  card: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

export default connect(mapStoreStateToProps)(SearchCard);
