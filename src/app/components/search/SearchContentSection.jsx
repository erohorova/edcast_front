import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DiscoverCard from '../common/Card';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Card from './../cards/Card';
import { ecl } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';

class SearchContentSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLastPage: props.isLastPage,
      providerLogos: {},
      isOpenBlock: true,
      cards: props.cards || []
    };

    this.styles = {
      accordionTitle: {
        textTransform: 'capitalize',
        fontSize: '16px',
        color: '#454560',
        paddingLeft: '0'
      },
      accordionTitleIcon: {
        marginLeft: '-10px',
        fill: '#454560',
        height: '34px',
        width: '34px'
      }
    };
  }

  toggleBlock = () => {
    this.setState({ isOpenBlock: !this.state.isOpenBlock });
  };

  componentDidMount() {
    ecl
      .getProviderLogos()
      .then(providerLogos => {
        this.setState({
          providerLogos: providerLogos
        });
      })
      .catch(err => {
        console.error(`Error in SearchContentSection.getProviderLogos.func : ${err}`);
      });
  }

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({ cards: newCardsList });
  };

  componentWillReceiveProps(nextProps) {
    if (
      this.props.cards &&
      Array.isArray(this.props.cards) &&
      nextProps.cards &&
      Array.isArray(nextProps.cards)
    ) {
      this.setState({
        cards: nextProps.cards,
        isLastPage: nextProps.isLastPage
      });
    }
  }

  render() {
    let labelName;
    let viewMoreLabel;
    let disableWhileLoading = false;
    if (this.props.label === 'pathway') {
      labelName = 'Pathways';
      viewMoreLabel = 'View More Pathways';
    } else if (this.props.label === 'journey') {
      labelName = 'Journeys';
      viewMoreLabel = 'View More Journeys';
    } else if (this.props.label === 'content') {
      labelName = 'SmartCards';
      viewMoreLabel = 'View More SmartCards';
    } else {
      viewMoreLabel = 'Loading...';
      disableWhileLoading = true;
    }

    return (
      <div>
        {this.props.isNewStyle && (
          <div>
            {this.state.cards && this.state.cards.length > 0 && (
              <div className="search-section-header">
                <FlatButton
                  hoverColor="transparent"
                  rippleColor="transparent"
                  className="accordion-title"
                  onClick={this.toggleBlock}
                  label={
                    <span>
                      {tr(labelName)} ({this.props.contentSectionCount})
                    </span>
                  }
                  labelStyle={this.styles.accordionTitle}
                  secondary={true}
                  icon={
                    (this.state.isOpenBlock && (
                      <NavigationArrowDropDown style={this.styles.accordionTitleIcon} />
                    )) ||
                    (!this.state.isOpenBlock && (
                      <NavigationArrowDropUp style={this.styles.accordionTitleIcon} />
                    ))
                  }
                />
              </div>
            )}
            {this.state.isOpenBlock && (
              <div>
                {this.state.cards && (
                  <div>
                    <div className="custom-card-container">
                      <div className="five-card-column vertical-spacing-medium">
                        {this.state.cards.map((card, index) => {
                          return (
                            <Card
                              key={card.id}
                              toggleSearch={this.toggleSearch}
                              author={card.author}
                              card={card}
                              dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                              startDate={
                                card.startDate || (card.assignment && card.assignment.startDate)
                              }
                              isMarkAsCompleteDisabled={true}
                              providerLogos={this.state.providerLogos}
                              moreCards={false}
                              removeCardFromList={this.removeCardFromList}
                              cardUpdated={this.props.cardUpdated}
                              isNotNeedUpdateCardReducer={true}
                            />
                          );
                        })}
                      </div>
                    </div>
                  </div>
                )}
                {this.state.cards.length > 0 &&
                  this.state.cards.length < this.props.contentSectionCount &&
                  !this.state.isLastPage && (
                    <div className="search-content-section veiw-more__container">
                      <div className="small-12 columns text-center">
                        <SecondaryButton
                          disabled={disableWhileLoading}
                          label={tr(viewMoreLabel)}
                          onClick={this.props.loadMore.bind(this, this.props.label)}
                        />
                      </div>
                    </div>
                  )}
              </div>
            )}
          </div>
        )}
        {!this.props.isNewStyle && (
          <div className="result-block">
            <h6 className="search-section-header">
              {tr(labelName)} ({this.props.contentSectionCount})
            </h6>
            {this.state.cards && (
              <div>
                <div className="custom-card-container">
                  <div className="three-card-column vertical-spacing-medium">
                    {this.state.cards.map((card, index) => {
                      return (
                        <Card
                          key={card.id}
                          toggleSearch={this.toggleSearch}
                          author={card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          isMarkAsCompleteDisabled={true}
                          providerLogos={this.state.providerLogos}
                          moreCards={false}
                          removeCardFromList={this.removeCardFromList}
                          cardUpdated={this.props.cardUpdated}
                          isNotNeedUpdateCardReducer={true}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
            {this.state.cards.length > 0 &&
              this.state.cards.length < this.props.contentSectionCount &&
              !this.state.isLastPage && (
                <div className="row search-content-section veiw-more__container">
                  <div className="small-12 columns text-center">
                    <SecondaryButton
                      disabled={disableWhileLoading}
                      label={tr(viewMoreLabel)}
                      onClick={this.props.loadMore.bind(this, this.props.label)}
                    />
                  </div>
                </div>
              )}
          </div>
        )}
      </div>
    );
  }
}

SearchContentSection.propTypes = {
  isNewStyle: PropTypes.bool,
  isLastPage: PropTypes.bool,
  label: PropTypes.string,
  contentSectionCount: PropTypes.number,
  cards: PropTypes.object,
  currentUser: PropTypes.object,
  loadMore: PropTypes.func,
  cardUpdated: PropTypes.func
};

export default SearchContentSection;
