import React, { PropTypes, Component } from 'react';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';
import { Menu, MenuItem, AsyncTypeahead } from 'react-bootstrap-typeahead';
import { usersv2, search } from 'edc-web-sdk/requests/index';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import { tr } from 'edc-web-sdk/helpers/translations';

class SearchUserComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      selectedUsers: props.selectedUsers || [],
      selectedUserIds: props.selectedUserIds || []
    };

    this.styles = {
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chipLabel: {
        fontSize: '12px',
        color: '#fff'
      },
      customIcon: {
        position: 'absolute',
        top: '-1px',
        maxHeight: '18px',
        right: '7px',
        padding: 0,
        maxWidth: '18px'
      },
      searchIcon: {
        width: '15px',
        height: '15px'
      }
    };
  }

  handleUserClick = user => {
    if (!this.state.selectedUserIds.includes(user.id)) {
      let users = this.state.selectedUsers;
      users.push(user);
      let userIds = users.map(userItem => {
        return userItem.id;
      });
      this.setState({ selectedUsers: users, selectedUserIds: userIds });
      this.props.handleUserChange({ selectedUsers: users, selectedUserIds: userIds });
      this.props.handleFilterChange('user_ids', userIds.toString(), this.props.type);
      this.refs.userSearchTypeahead.getInstance().blur();
    }
  };

  _handleSearch = query => {
    let that = this;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

    if (!query) {
      return;
    }
    usersv2
      .searchUsers({ q: query })
      .then(data => {
        let users = data.users.map(user => {
          return {
            id: user.id,
            name: user.name,
            slug: user.handle,
            avatar: user.avatarimages.tiny || defaultUserImage,
            type: 'User'
          };
        });

        that.setState({ dataSource: users });
      })
      .catch(err => {
        console.error(`Error in SearchUserComponent.searchUsers.func : ${err}`);
      });
  };

  _renderMenu = (results, menuProps) => {
    const users = results.map(user => {
      return [
        <li key={user.id}>
          <a onClick={this.handleUserClick.bind(this, user)}>
            <img src={user.avatar} height="20" className="user-search-image" />
            <div className="user-search-name">{user.name}</div>
          </a>
        </li>
      ];
    });

    return <Menu {...menuProps}>{users}</Menu>;
  };

  handleRequestDelete = userId => {
    let existSelectedUsers = this.state.selectedUsers;
    let selectedUsers = [];
    let selectedUserIds = [];
    existSelectedUsers.forEach(user => {
      let searchedListSize = existSelectedUsers.length;
      if (user.id !== userId) {
        selectedUsers.push(user);
        selectedUserIds.push(user.id);
      }
      if (searchedListSize <= 1) {
        this.refs.userSearchTypeahead.getInstance().clear();
      }
    });
    this.setState({ selectedUsers: selectedUsers, selectedUserIds: selectedUserIds });
    this.props.handleFilterChange('user_ids', selectedUserIds.toString(), this.props.type);
    this.props.handleUserChange({ selectedUsers: selectedUsers, selectedUserIds: selectedUserIds });
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    return (
      <div>
        <div className="list-users">
          {this.state.selectedUsers.length > 0 && (
            <div style={this.styles.wrapper}>
              {this.state.selectedUsers.map(user => {
                return (
                  <Chip
                    key={user.id}
                    style={this.styles.chip}
                    backgroundColor={'#45b8e6'}
                    labelColor={'white'}
                    labelStyle={this.styles.chipLabel}
                    className={'user-chip'}
                  >
                    <small style={this.styles.chipLabel}>{user.name}</small>
                    <IconButton
                      className={'cancel-icon delete'}
                      aria-label={tr('close')}
                      ref={'cancelIcon'}
                      tooltip={tr('Remove Selection')}
                      disableTouchRipple
                      tooltipPosition="top-center"
                      style={this.styles.customIcon}
                      onTouchTap={this.handleRequestDelete.bind(this, user.id)}
                    >
                      <Close color="#fff" />
                    </IconButton>
                  </Chip>
                );
              })}
            </div>
          )}
        </div>
        <div className="user-search-input" style={{ marginTop: '7px' }}>
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={this.state.dataSource}
            placeholder={this.props.placeholder ? tr(this.props.placeholder) : tr('Search')}
            onSearch={this._handleSearch}
            submitFormOnEnter={false}
            ref="userSearchTypeahead"
          />
          <button className="search-button" type="submit" onClick={this.handleRedirect}>
            <SearchIcon style={this.styles.searchIcon} />
          </button>
        </div>
      </div>
    );
  }
}

SearchUserComponent.propTypes = {
  placeholder: PropTypes.string,
  type: PropTypes.string,
  handleFilterChange: PropTypes.func,
  handleUserChange: PropTypes.func,
  selectedUsers: PropTypes.any,
  selectedUserIds: PropTypes.any
};

export default SearchUserComponent;
