import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import SearchUserComponent from './SearchUserComponent.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class ActionByFilterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        }
      };
    }
  }

  handleChange = (e, isInputChecked, type) => {
    if (e.target.value == 'following') {
      this.setState({ isFollowingChecked: isInputChecked });
    }
    if (e.target.value == 'followers') {
      this.setState({ isFollowerChecked: isInputChecked });
    }
    this.props.handleFilterChange(e.target.value, isInputChecked, type);
  };

  handleUserChange = selection => {
    this.setState({
      selectedUserIds: selection.selectedUserIds,
      selectedUsers: selection.selectedUsers
    });
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  render() {
    let uncheckFilter = this.props.uncheckFilter;
    return (
      <div className="new-search__paper-container">
        {this.props.isNewStyle && (
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title">{tr(this.props.type.replace('_', ' '))}</div>
              {this.state.genpactUI && (
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              )}
            </div>
            {this.state.showMenu && (
              <div>
                <div>
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Following')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, this.props.type)
                      }
                      value={'following'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'following' &&
                            uncheckFilter.type === this.props.type)))
                        ? { checked: false }
                        : {})}
                      checked={this.state.isFollowingChecked}
                    />
                  </div>
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Followers')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, this.props.type)
                      }
                      value={'followers'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'followers' &&
                            uncheckFilter.type === this.props.type)))
                        ? { checked: false }
                        : {})}
                      checked={this.state.isFollowerChecked}
                    />
                  </div>
                </div>

                <div>
                  <SearchUserComponent
                    handleUserChange={this.handleUserChange}
                    handleFilterChange={this.props.handleFilterChange}
                    type={this.props.type}
                    placeholder={tr('By name')}
                    selectedUsers={this.state.selectedUsers}
                    selectedUserIds={this.state.selectedUserIds}
                  />
                </div>
              </div>
            )}

            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
        {!this.props.isNewStyle && (
          <Paper style={this.styles.paperStyle}>
            <div className="flex-space-between">
              <div className="filter-title">{tr(this.props.type.replace('_', ' '))}</div>
              <div className="pointer" onClick={this.showHideFilter}>
                {this.state.showMenu ? <HardwareKeyboardArrowDown /> : <HardwareKeyboardArrowUp />}
              </div>
            </div>
            {this.state.showMenu && (
              <div>
                <div>
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Following')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, this.props.type)
                      }
                      value={'following'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'following' &&
                            uncheckFilter.type === this.props.type)))
                        ? { checked: false }
                        : {})}
                      checked={this.state.isFollowingChecked}
                    />
                  </div>
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Followers')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, this.props.type)
                      }
                      value={'followers'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'followers' &&
                            uncheckFilter.type === this.props.type)))
                        ? { checked: false }
                        : {})}
                      checked={this.state.isFollowerChecked}
                    />
                  </div>
                </div>
                <div>
                  <SearchUserComponent
                    handleUserChange={this.handleUserChange}
                    handleFilterChange={this.props.handleFilterChange}
                    type={this.props.type}
                    selectedUsers={this.state.selectedUsers}
                    selectedUserIds={this.state.selectedUserIds}
                  />
                </div>
              </div>
            )}

            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
      </div>
    );
  }
}

ActionByFilterComponent.propTypes = {
  hideFilters: PropTypes.bool,
  type: PropTypes.string,
  isNewStyle: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default ActionByFilterComponent;
