import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import { tr } from 'edc-web-sdk/helpers/translations';
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class TypeFilterContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };

    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxInner: {
        width: '15px',
        height: '15px',
        marginBottom: '10px',
        marginTop: '3px',
        marginRight: '10px',
        fill: '#6f708b'
      },
      checkboxInnerLabel: {
        fontSize: '12px',
        fontWeight: '300',
        color: '#6f708b',
        marginTop: '1px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '1.5px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxInner: {
          width: '13px',
          height: '13px',
          marginRight: '4px'
        },
        checkboxInnerLabel: {
          lineHeight: '2.17',
          fontSize: '12px',
          fontWeight: '300',
          color: '#6f708b',
          marginTop: '1px'
        },
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        checkboxIconStyle: {
          width: '19px',
          height: '19px'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        },
        uncheckedIcon_inner: {
          width: '13px',
          height: '13px'
        }
      };
    }
  }

  handleChange = (e, isInputChecked, type) => {
    this.props.handleFilterChange(e.target.value, isInputChecked, type);
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  render() {
    let typeFilters = this.props.filters;
    let uncheckFilter = this.props.uncheckFilter;
    let smartCardsCount = 0;

    if (this.props.contentCount == 0 && typeFilters.Content) {
      typeFilters.Content.map(typeFilter => {
        smartCardsCount += typeFilter.count;
      });
    } else {
      smartCardsCount = this.props.contentCount;
    }
    return (
      <div className="new-search__paper-container">
        {this.props.isNewStyle && (
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title" role="heading" aria-level="4">
                {tr('Type')}
              </div>
              {this.state.genpactUI && (
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              )}
            </div>
            {typeFilters && this.state.showMenu && (
              <div>
                {typeFilters.hasOwnProperty('Content') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      style={this.styles.checkbox}
                      label={`${tr('SmartCards')} (${smartCardsCount})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'content'}
                      checked={!!this.props.contentChecked}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'content' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.Content &&
                  typeFilters.Content.map((typeFilter, index) => {
                    return (
                      <div className="inner-checkbox" key={index}>
                        <Checkbox
                          style={this.styles.checkbox}
                          label={`${tr(typeFilter.display_name)} (${typeFilter.count})`}
                          iconStyle={this.styles.checkboxInner}
                          labelStyle={this.styles.checkboxInnerLabel}
                          checkedIcon={<CheckOn1 color="#6f708b" />}
                          uncheckedIcon={
                            <CheckOff style={this.styles.uncheckedIcon_inner} color="#6f708b" />
                          }
                          onCheck={(event, isInputChecked) =>
                            this.handleChange(event, isInputChecked, 'content_type')
                          }
                          value={`${typeFilter.display_name.toLowerCase().replace(' ', '_')}`}
                          checked={typeFilter.checked ? typeFilter.checked : false}
                          {...(!!uncheckFilter &&
                          (uncheckFilter.clearAll === true ||
                            (uncheckFilter.filter !== false &&
                              (uncheckFilter.filter ===
                                `${typeFilter.display_name.toLowerCase().replace(' ', '_')}` &&
                                uncheckFilter.type === 'content_type')))
                            ? { checked: false }
                            : {})}
                        />
                      </div>
                    );
                  })}

                {typeFilters.hasOwnProperty('Pathway') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Pathways')} (${typeFilters['Pathway'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'pathway'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'pathway' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Journey') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Journey')} (${typeFilters['Journey'].length &&
                        typeFilters['Journey'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'journey'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'journey' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Channels') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Channels')} (${typeFilters['Channels'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'channel'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'channel' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Users') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('People')} (${typeFilters['Users'][0].count})`}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'user'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'user' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
        {!this.props.isNewStyle && (
          <Paper style={this.styles.paperStyle}>
            <h6 className="filter-title" role="heading" aria-level="4">
              {tr('Type')}
            </h6>
            {typeFilters && (
              <div>
                {typeFilters.hasOwnProperty('Content') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('SmartCards')} (${smartCardsCount})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'content'}
                      checked={!!this.props.contentChecked}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'content' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.Content &&
                  typeFilters.Content.map((typeFilter, index) => {
                    return (
                      <div className="inner-checkbox" key={index}>
                        <Checkbox
                          label={`${tr(typeFilter.display_name)} (${typeFilter.count})`}
                          iconStyle={this.styles.checkboxInner}
                          labelStyle={this.styles.checkboxInnerLabel}
                          onCheck={(event, isInputChecked) =>
                            this.handleChange(event, isInputChecked, 'content_type')
                          }
                          value={`${typeFilter.display_name.toLowerCase().replace(' ', '_')}`}
                          checked={typeFilter.checked ? typeFilter.checked : false}
                          {...(!!uncheckFilter &&
                          (uncheckFilter.clearAll === true ||
                            (uncheckFilter.filter !== false &&
                              (uncheckFilter.filter ===
                                `${typeFilter.display_name.toLowerCase().replace(' ', '_')}` &&
                                uncheckFilter.type === 'content_type')))
                            ? { checked: false }
                            : {})}
                        />
                      </div>
                    );
                  })}

                {typeFilters.hasOwnProperty('Pathway') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Pathways')} (${typeFilters['Pathway'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'pathway'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'pathway' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Journey') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Journeys')} (${typeFilters['Journey'].length &&
                        typeFilters['Journey'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'journey'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'journey' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Channels') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('Channels')} (${typeFilters['Channels'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'channel'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'channel' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {typeFilters.hasOwnProperty('Users') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={`${tr('People')} (${typeFilters['Users'][0].count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'content_type')
                      }
                      value={'user'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'user' &&
                            uncheckFilter.type === 'content_type')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
      </div>
    );
  }
}

TypeFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  contentChecked: PropTypes.any,
  contentCount: PropTypes.number,
  filters: PropTypes.object,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default TypeFilterContainer;
