import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import _ from 'lodash';
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class PriceFilterContainerFreePaid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      disabled: true,
      free_checked: false,
      paid_checked: false,
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };

    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      radioLabel: {
        fontSize: '14px',
        color: '#6f708b'
      },
      radioIcon: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      clearIcon: {
        display: 'none'
      }
    };
    if (this.props.isNewStyle) {
      this.styles = {
        paperStyle: {
          borderRadius: '4px',
          padding: '12px 13px',
          marginBottom: '15px'
        },
        radioLabel: {
          fontSize: 'inherit'
        },
        radioIcon: {
          width: '18px',
          height: '18px',
          marginRight: '10px',
          marginBottom: '10px',
          marginTop: '2px',
          fill: '#6f708b'
        },
        clearIcon: {
          display: 'none'
        }
      };
    }
  }

  componentDidMount() {}

  handleClick = (evt, value) => {
    if (this.state.free_checked == false && evt.target.value == 'free') {
      this.setState({
        free_checked: true,
        paid_checked: false
      });
      this.props.handleFilterChange(evt.target.value, false, 'free_paid');
    }
    if (this.state.free_checked == true && evt.target.value == 'free') {
      this.setState({
        free_checked: false,
        paid_checked: false
      });
      this.props.handleFilterChange('clear_price', null, 'clear_price');
    }
    if (this.state.paid_checked == false && evt.target.value == 'paid') {
      this.setState({
        paid_checked: true,
        free_checked: false
      });
      this.props.handleFilterChange(evt.target.value, true, 'free_paid');
    }
    if (this.state.paid_checked == true && evt.target.value == 'paid') {
      this.setState({
        paid_checked: false,
        free_checked: false
      });
      this.props.handleFilterChange('clear_price', null, 'clear_price');
    }
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  render() {
    let uncheckFilter = this.props.uncheckFilter;
    if (this.props.isNewStyle) {
      return (
        <div className="new-search__paper-container">
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title">{tr('Price')}</div>
              {this.state.genpactUI && (
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              )}
            </div>
            {this.state.showMenu && (
              <div>
                <RadioButton
                  key={1}
                  value="free"
                  label={tr('Free')}
                  checked={this.state.free_checked}
                  iconStyle={this.styles.radioIcon}
                  labelStyle={this.styles.radioLabel}
                  className="price-radio-button"
                  onClick={(event, value) => this.handleClick(event, value)}
                  {...(!!uncheckFilter &&
                  (uncheckFilter.clearAll === true ||
                    (uncheckFilter.filter !== false && uncheckFilter.filter === 'free'))
                    ? { checked: false }
                    : {})}
                />
                <RadioButton
                  key={2}
                  value="paid"
                  label={tr('Paid')}
                  checked={this.state.paid_checked}
                  iconStyle={this.styles.radioIcon}
                  labelStyle={this.styles.radioLabel}
                  className="price-radio-button"
                  onClick={(event, value) => this.handleClick(event, value)}
                  {...(!!uncheckFilter &&
                  (uncheckFilter.clearAll === true ||
                    (uncheckFilter.filter !== false && uncheckFilter.filter === 'paid'))
                    ? { checked: false }
                    : {})}
                />
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    } else {
      return (
        <div className="new-search__paper-container">
          <Paper style={this.styles.paperStyle}>
            <div className="filter-title">{tr('Price')}</div>
            <div>
              <RadioButton
                key={1}
                value="free"
                label={tr('Free')}
                checked={this.state.free_checked}
                iconStyle={this.styles.radioIcon}
                labelStyle={this.styles.radioLabel}
                className="price-radio-button"
                onClick={(event, value) => this.handleClick(event, value)}
                {...(!!uncheckFilter &&
                (uncheckFilter.clearAll === true ||
                  (uncheckFilter.filter !== false && uncheckFilter.filter === 'free'))
                  ? { checked: false }
                  : {})}
              />
              <RadioButton
                key={2}
                value="paid"
                label={tr('Paid')}
                checked={this.state.paid_checked}
                iconStyle={this.styles.radioIcon}
                labelStyle={this.styles.radioLabel}
                className="price-radio-button"
                onClick={(event, value) => this.handleClick(event, value)}
                {...(!!uncheckFilter &&
                (uncheckFilter.clearAll === true ||
                  (uncheckFilter.filter !== false && uncheckFilter.filter === 'paid'))
                  ? { checked: false }
                  : {})}
              />
            </div>

            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    }
  }
}

PriceFilterContainerFreePaid.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default PriceFilterContainerFreePaid;
