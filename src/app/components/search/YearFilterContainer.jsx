import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class YearFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      today: null,
      years: [],
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      radioLabel: {
        fontSize: '14px',
        color: '#6f708b'
      },
      radioIcon: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      clearIcon: {
        display: 'none'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        }
      };
    }
  }

  componentDidMount() {
    this.fetchYears();
  }

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  fetchYears() {
    let d = new Date();
    let date = `${d.getDate()}/${d.getMonth()}/${d.getFullYear()}`;
    let yearsArray = [d.getFullYear(), d.getFullYear() - 1, d.getFullYear() - 2];
    this.setState({ today: date, years: yearsArray });
  }

  //for new search page.
  fetchCheckboxes = uncheckFilter => {
    let checkboxArray = [];
    let dynCheckboxArray = [];
    checkboxArray.push(
      <Checkbox
        label={tr('Last 30 Days')}
        iconStyle={this.styles.checkboxOuter}
        labelStyle={this.styles.checkboxOuterLabel}
        checkedIcon={<CheckOn1 color="#6f708b" />}
        uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
        onCheck={(event, isInputChecked) =>
          this.handleCheckboxChange(event, isInputChecked, 'year')
        }
        value="30"
        {...(!!uncheckFilter &&
        (uncheckFilter.clearAll === true ||
          (uncheckFilter.filter !== false &&
            (uncheckFilter.filter === '30' && uncheckFilter.type === 'year')))
          ? { checked: false }
          : {})}
      />,
      <Checkbox
        label={tr('Last 90 Days')}
        iconStyle={this.styles.checkboxOuter}
        labelStyle={this.styles.checkboxOuterLabel}
        checkedIcon={<CheckOn1 color="#6f708b" />}
        uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
        onCheck={(event, isInputChecked) =>
          this.handleCheckboxChange(event, isInputChecked, 'year')
        }
        value="90"
        {...(!!uncheckFilter &&
        (uncheckFilter.clearAll === true ||
          (uncheckFilter.filter !== false &&
            (uncheckFilter.filter === '90' && uncheckFilter.type === 'year')))
          ? { checked: false }
          : {})}
      />
    );

    if (this.state.years.length > 0) {
      dynCheckboxArray = this.state.years.map((year, index) => {
        return (
          <Checkbox
            key={index}
            label={year}
            iconStyle={this.styles.checkboxOuter}
            labelStyle={this.styles.checkboxOuterLabel}
            checkedIcon={<CheckOn1 color="#6f708b" />}
            uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
            onCheck={(event, isInputChecked) =>
              this.handleCheckboxChange(event, isInputChecked, 'year')
            }
            value={year}
            {...(!!uncheckFilter &&
            (uncheckFilter.clearAll === true ||
              (uncheckFilter.filter !== false &&
                (uncheckFilter.filter === year && uncheckFilter.type === 'year')))
              ? { checked: false }
              : {})}
          />
        );
      });
    }

    if (dynCheckboxArray.length > 0) {
      checkboxArray = checkboxArray.concat(dynCheckboxArray);
    }

    return checkboxArray;
  };

  fetchRadioButtons = () => {
    let radioArray = [];
    let dynRadioArray = [];
    radioArray.push(
      <RadioButton
        key={1}
        value="30"
        label={tr('Last 30 Days')}
        iconStyle={this.styles.radioIcon}
        labelStyle={this.styles.radioLabel}
        className="year-radio-button"
      />
    );
    radioArray.push(
      <RadioButton
        key={2}
        value="90"
        label={tr('Last 90 Days')}
        iconStyle={this.styles.radioIcon}
        labelStyle={this.styles.radioLabel}
        className="year-radio-button"
      />
    );

    if (this.state.years.length > 0) {
      dynRadioArray = this.state.years.map((year, index) => {
        return (
          <RadioButton
            key={index + 3}
            label={year}
            value={year}
            iconStyle={this.styles.radioIcon}
            labelStyle={this.styles.radioLabel}
            className="year-radio-button"
          />
        );
      });
    }

    if (dynRadioArray.length > 0) {
      radioArray = radioArray.concat(dynRadioArray);
    }

    radioArray.push(
      <RadioButton
        key={6}
        label={tr('Clear All')}
        iconStyle={this.styles.clearIcon}
        labelStyle={this.styles.radioLabel}
        className="yearClear"
        value="clear"
      />
    );

    return radioArray;
  };

  handleChange = value => {
    this.props.handleFilterChange(value, null, 'year');
  };

  handleCheckboxChange = (e, isInputChecked, type) => {
    this.props.handleFilterChange(e.target.value, isInputChecked, type);
  };

  render() {
    let uncheckFilter = this.props.uncheckFilter;
    if (this.props.isNewStyle) {
      let checkboxArray = this.fetchCheckboxes(uncheckFilter);
      return (
        <div className="new-search__paper-container">
          {checkboxArray.length > 0 && (
            <Paper className="new-search__paper">
              <div className="flex-space-between">
                <div className="filter-title">{tr('Year')}</div>
                {this.state.genpactUI && (
                  <div className="pointer" onClick={this.showHideFilter}>
                    {this.state.showMenu ? (
                      <HardwareKeyboardArrowDown />
                    ) : (
                      <HardwareKeyboardArrowUp />
                    )}
                  </div>
                )}
              </div>
              {this.state.showMenu && <div className="outer-checkbox">{checkboxArray}</div>}

              {this.props.hideFilters && <div className="translucent-layer" />}
            </Paper>
          )}
        </div>
      );
    } else {
      let radioArray = this.fetchRadioButtons();
      return (
        <div className="new-search__paper-container">
          {radioArray.length > 0 && (
            <Paper style={this.styles.paperStyle}>
              <div className="flex-space-between">
                <div className="filter-title">{tr('Year')}</div>
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              </div>

              <div>
                <RadioButtonGroup
                  name="yearFilter"
                  onChange={(event, value) => this.handleChange(value)}
                >
                  {radioArray}
                </RadioButtonGroup>
              </div>
              {this.props.hideFilters && <div className="translucent-layer" />}
            </Paper>
          )}
        </div>
      );
    }
  }
}

YearFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default YearFilterContainer;
