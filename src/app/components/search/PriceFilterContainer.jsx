import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import remove from 'lodash/remove';
import find from 'lodash/find';

class PriceFilterContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currency: 'INR',
      priceArray: [],
      priceCheckedArray: [],
      disabled: true,
      minPriceValue: '',
      minPriceError: false,
      maxPriceError: false,
      maxPriceLessThanMinValue: false,
      maxPriceValue: '',
      previousPriceValue: ''
    };

    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      },
      priceDropDown: {
        textAlign: 'left',
        height: '26px',
        verticalAlign: 'top',
        border: '1px solid #d6d6e2',
        fontSize: '14px',
        paddingLeft: '5px',
        width: '100%',
        maxWidth: '256px',
        margin: '8px 0px',
        lineHeight: '5px'
      },
      iconSelect: {
        height: '26px',
        width: '26px',
        padding: '0px'
      },
      menuItemStyle: {
        fontSize: '14px'
      },
      labelStyleSelect: {
        fontSize: '14px',
        lineHeight: '30px',
        paddingRight: '18px'
      },
      minPrice: {
        border: '1px solid #86879e'
      },
      minPriceError: {
        border: '1px solid red'
      },
      maxPrice: {
        border: '1px solid #86879e',
        marginLeft: '10px'
      },
      maxPriceError: {
        border: '1px solid red',
        marginLeft: '10px'
      },
      goButton: {
        boxSizing: 'content-box',
        borderColor: '#acadc1',
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        height: '24px',
        lineHeight: 'normal',
        color: '#6f708b',
        marginLeft: '15px',
        backgroundColor: '#F0F0F5'
      },
      priceErrorStyle: {
        fontSize: '12px',
        color: 'red'
      },
      btnGoLabel: {
        textTransform: 'none',
        lineHeight: '25px'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        },
        priceDropDown: {
          textAlign: 'left',
          height: '30px',
          verticalAlign: 'top',
          border: '1px solid #d6d6e2',
          fontSize: '16px',
          paddingLeft: '5px',
          width: '100%',
          maxWidth: '256px',
          marginBottom: '8px',
          lineHeight: '5px'
        },
        iconSelect: {
          height: '30px',
          width: '30px',
          padding: '0px'
        },
        menuItemStyle: {
          fontSize: '16px'
        },
        labelStyleSelect: {
          fontSize: '16px',
          lineHeight: '34px',
          paddingRight: '18px'
        },
        minPrice: {
          border: '1px solid #86879e'
        },
        minPriceError: {
          border: '1px solid red'
        },
        maxPrice: {
          border: '1px solid #86879e',
          marginLeft: '10px'
        },
        maxPriceError: {
          border: '1px solid red',
          marginLeft: '10px'
        },
        goButton: {
          boxSizing: 'content-box',
          borderColor: '#acadc1',
          borderStyle: 'solid',
          borderWidth: '1px',
          textTransform: 'none',
          height: '24px',
          lineHeight: 'normal',
          color: '#6f708b',
          marginLeft: '15px',
          backgroundColor: '#F0F0F5'
        },
        priceErrorStyle: {
          fontSize: '12px',
          color: 'red'
        },
        btnGoLabel: {
          textTransform: 'none',
          lineHeight: '25px'
        }
      };
    }
  }

  componentDidMount() {
    this.handlePricingCurrencyChange(this.state.currency);
  }

  handleChange = (e, key, isInputChecked, type) => {
    let priceArray = this.state.priceArray;
    let priceCheckedArray = this.state.priceCheckedArray;
    let previousPriceValue = this.state.previousPriceValue;
    priceCheckedArray = remove(priceCheckedArray, function(n) {
      return n !== e.target.value;
    });
    if (isInputChecked) {
      priceCheckedArray.push(e.target.value);
    }
    if (previousPriceValue) {
      if (!this.state.minPriceValue && !this.state.maxPriceValue) {
        this.props.handleFilterChange(
          previousPriceValue,
          null,
          'amount_range',
          this.state.currency
        );
        priceCheckedArray = remove(priceCheckedArray, function(n) {
          return n !== previousPriceValue;
        });
        previousPriceValue = '';
      }
    }
    priceArray[key].checked = isInputChecked;
    this.setState({
      priceArray,
      priceCheckedArray,
      previousPriceValue
    });
    this.props.handleFilterChange(e.target.value, isInputChecked, type, this.state.currency);
  };

  handlePricingCurrencyChange = currency => {
    let priceArray = [];
    if (currency == 'INR') {
      priceArray = [
        {
          label: 'Free',
          value: "['free', 'free']",
          checked: false
        },
        {
          label: 'Paid',
          value: "[1,'above']",
          checked: false
        },
        {
          label: 'Under ₹​1000',
          value: '[0, 999.99]',
          checked: false
        },
        {
          label: '₹​1000 to ₹​10000',
          value: '[1000, 10000]',
          checked: false
        },
        {
          label: '₹​10000 to ₹​25000',
          value: '[10000, 25000]',
          checked: false
        },
        {
          label: '₹​25000 to ₹​50000',
          value: '[25000, 50000]',
          checked: false
        },
        {
          label: '₹50000 & Above',
          value: "[50000, 'above']",
          checked: false
        }
      ];
    } else {
      priceArray = [
        {
          label: 'Free',
          value: "['free', 'free']",
          checked: false
        },
        {
          label: 'Paid',
          value: "[1,'above']",
          checked: false
        },
        {
          label: 'Under $​100',
          value: '[0, 99.99]',
          checked: false
        },
        {
          label: '$​100 to $​250',
          value: '[100, 250]',
          checked: false
        },
        {
          label: '$​250 to $​500',
          value: '[250, 500]',
          checked: false
        },
        {
          label: '$​500 to $​1000',
          value: '[500, 1000]',
          checked: false
        },
        {
          label: '$​1000 & Above',
          value: "[1000, 'above']",
          checked: false
        }
      ];
    }
    if (this.state.priceCheckedArray && this.state.priceCheckedArray.length > 0) {
      this.props.handleFilterChange([], false, 'amount_range_empty');
    }

    this.setState({
      currency,
      priceArray: priceArray,
      priceCheckedArray: [],
      previousPriceValue: '',
      disabled: true,
      minPriceValue: '',
      minPriceError: false,
      maxPriceError: false,
      maxPriceLessThanMinValue: false,
      maxPriceValue: ''
    });
  };

  minInputHandler = e => {
    if (
      !new RegExp('^\\d+(?:\\.\\d{1,2}|\\.|)$').test(this.state.minPriceValue + `${e.key}`) &&
      `${e.key}`.length === 1
    ) {
      e.preventDefault();
    }
  };

  setMinPriceValue = e => {
    if (e.target.value == '' && this.state.maxPriceValue == '') {
      this.setState({
        minPriceValue: e.target.value,
        disabled: true,
        minPriceError: false,
        maxPriceError: false,
        maxPriceLessThanMinValue: false
      });
    } else {
      if (!new RegExp('^\\d+(?:\\.\\d{1,2}|)$').test(e.target.value)) {
        if (e.target.value == '') {
          if (this.state.maxPriceValue && !this.state.maxPriceError) {
            this.setState({
              minPriceValue: e.target.value,
              disabled: false,
              minPriceError: false,
              maxPriceLessThanMinValue: false
            });
          } else {
            this.setState({
              minPriceValue: e.target.value,
              disabled: true,
              minPriceError: false,
              maxPriceLessThanMinValue: false
            });
          }
        } else {
          this.setState({
            minPriceValue: e.target.value,
            disabled: true,
            minPriceError: true,
            maxPriceLessThanMinValue: false
          });
        }
      } else {
        if (this.state.maxPriceError) {
          this.setState({
            minPriceValue: e.target.value,
            disabled: true,
            minPriceError: false,
            maxPriceLessThanMinValue: false
          });
        } else {
          if (
            this.state.maxPriceValue !== '' &&
            parseFloat(this.state.maxPriceValue) < parseFloat(e.target.value)
          ) {
            this.setState({
              minPriceValue: e.target.value,
              disabled: true,
              minPriceError: false,
              maxPriceLessThanMinValue: true
            });
          } else {
            this.setState({
              minPriceValue: e.target.value,
              disabled: false,
              minPriceError: false,
              maxPriceLessThanMinValue: false
            });
          }
        }
      }
    }
  };

  maxInputHandler = e => {
    if (
      !new RegExp('^\\d+(?:\\.\\d{1,2}|\\.|)$').test(this.state.maxPriceValue + `${e.key}`) &&
      `${e.key}`.length === 1
    ) {
      e.preventDefault();
    }
  };

  setMaxPriceValue = e => {
    if (e.target.value == '' && this.state.minPriceValue == '') {
      this.setState({
        maxPriceValue: e.target.value,
        disabled: true,
        minPriceError: false,
        maxPriceError: false,
        maxPriceLessThanMinValue: false
      });
    } else {
      if (!new RegExp('^\\d+(?:\\.\\d{1,2}|)$').test(e.target.value)) {
        if (e.target.value == '') {
          if (this.state.minPriceValue && !this.state.minPriceError) {
            this.setState({
              maxPriceValue: e.target.value,
              disabled: false,
              maxPriceError: false,
              maxPriceLessThanMinValue: false
            });
          } else {
            this.setState({
              maxPriceValue: e.target.value,
              disabled: true,
              maxPriceError: false,
              maxPriceLessThanMinValue: false
            });
          }
        } else {
          this.setState({
            maxPriceValue: e.target.value,
            disabled: true,
            maxPriceError: true,
            maxPriceLessThanMinValue: false
          });
        }
      } else {
        if (this.state.minPriceError) {
          this.setState({
            maxPriceValue: e.target.value,
            disabled: true,
            maxPriceError: false,
            maxPriceLessThanMinValue: false
          });
        } else {
          if (
            this.state.minPriceValue !== '' &&
            parseFloat(e.target.value) < parseFloat(this.state.minPriceValue)
          ) {
            this.setState({
              maxPriceValue: e.target.value,
              disabled: true,
              maxPriceError: false,
              maxPriceLessThanMinValue: true
            });
          } else {
            this.setState({
              maxPriceValue: e.target.value,
              disabled: false,
              maxPriceError: false,
              maxPriceLessThanMinValue: false
            });
          }
        }
      }
    }
  };

  handlePrice = () => {
    let priceData = '';
    let priceCheckedArray = this.state.priceCheckedArray;
    let priceArray = this.state.priceArray;
    let previousPriceValue = this.state.previousPriceValue;
    let previousPriceElementExist = '';
    if (!this.state.minPriceValue && this.state.maxPriceValue) {
      priceData = '[0, ' + this.state.maxPriceValue + ']';
    } else if (this.state.minPriceValue && !this.state.maxPriceValue) {
      priceData = '[' + this.state.minPriceValue + ', ' + "'above'" + ']';
    } else if (this.state.minPriceValue && this.state.maxPriceValue) {
      priceData = '[' + this.state.minPriceValue + ', ' + this.state.maxPriceValue + ']';
    }
    previousPriceElementExist = find(priceArray, el => {
      if (el.checked) {
        return el.value === previousPriceValue;
      }
    });
    if (previousPriceValue && previousPriceValue !== priceData && !previousPriceElementExist) {
      this.props.handleFilterChange(previousPriceValue, null, 'amount_range', this.state.currency);
      priceCheckedArray = remove(priceCheckedArray, function(n) {
        return n !== previousPriceValue;
      });
    }
    priceCheckedArray = remove(priceCheckedArray, function(n) {
      return n !== priceData;
    });
    priceCheckedArray.push(priceData);
    previousPriceValue = priceData;
    this.setState({
      priceCheckedArray,
      previousPriceValue
    });
    this.props.handleFilterChange(priceData, true, 'amount_range', this.state.currency);
  };

  render() {
    let priceCurrencyOptions = [
      {
        label: 'INR',
        value: 'INR'
      },
      {
        label: 'USD',
        value: 'USD'
      }
    ];
    let PriceFilterElementsArray = [];
    if (this.props.isNewStyle) {
      this.state.priceArray.map((element, index) => {
        PriceFilterElementsArray.push(
          <div className="outer-checkbox" key={index}>
            <Checkbox
              label={tr(element.label)}
              checked={this.state.priceArray[index].checked}
              iconStyle={this.styles.checkboxOuter}
              labelStyle={this.styles.checkboxOuterLabel}
              checkedIcon={<CheckOn1 color="#6f708b" />}
              uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
              onCheck={(event, isInputChecked) =>
                this.handleChange(event, index, isInputChecked, 'amount_range')
              }
              value={element.value}
            />
          </div>
        );
      });
      return (
        <div className="new-search__paper-container">
          <Paper className="new-search__paper">
            <div className="filter-title">{tr('Price')}</div>
            <div>
              <SelectField
                style={this.styles.priceDropDown}
                hintText={tr('Select Currency')}
                maxHeight={80}
                value={this.state.currency}
                underlineStyle={{ display: 'none' }}
                labelStyle={this.styles.labelStyleSelect}
                iconStyle={this.styles.iconSelect}
                menuItemStyle={this.styles.menuItemStyle}
              >
                {priceCurrencyOptions.sort().map((currency, index) => {
                  return (
                    <MenuItem
                      key={index}
                      value={currency.value}
                      innerDivStyle={{ padding: '0 8px' }}
                      primaryText={tr(currency.label)}
                      onTouchTap={this.handlePricingCurrencyChange.bind(this, currency.value)}
                    />
                  );
                })}
              </SelectField>
            </div>
            <div>
              {PriceFilterElementsArray}
              {this.state.currency == 'INR' && (
                <div style={{ marginTop: '8px' }}>
                  <input
                    type="text"
                    placeholder="₹​min"
                    className="price-filter-input"
                    style={
                      this.state.minPriceError ? this.styles.minPriceError : this.styles.minPrice
                    }
                    value={this.state.minPriceValue}
                    onKeyDown={this.minInputHandler}
                    onChange={this.setMinPriceValue}
                  />
                  <input
                    type="text"
                    placeholder="₹​max"
                    className="price-filter-input"
                    style={
                      this.state.maxPriceError ? this.styles.maxPriceError : this.styles.maxPrice
                    }
                    value={this.state.maxPriceValue}
                    onKeyDown={this.maxInputHandler}
                    onChange={this.setMaxPriceValue}
                  />
                  <FlatButton
                    label={tr('Go')}
                    disabled={this.state.disabled}
                    style={this.styles.goButton}
                    labelStyle={this.styles.btnGoLabel}
                    onTouchTap={this.handlePrice.bind(this)}
                  />
                  {this.state.maxPriceLessThanMinValue && (
                    <div>
                      <span style={this.styles.priceErrorStyle}>
                        {tr('Max value should be greater than min value.')}
                      </span>
                    </div>
                  )}
                </div>
              )}
              {this.state.currency == 'USD' && (
                <div style={{ marginTop: '8px' }}>
                  <input
                    type="text"
                    placeholder="$min"
                    className="price-filter-input"
                    style={
                      this.state.minPriceError ? this.styles.minPriceError : this.styles.minPrice
                    }
                    value={this.state.minPriceValue}
                    onKeyDown={this.minInputHandler}
                    onChange={this.setMinPriceValue}
                  />
                  <input
                    type="text"
                    placeholder="$max"
                    className="price-filter-input"
                    style={
                      this.state.maxPriceError ? this.styles.maxPriceError : this.styles.maxPrice
                    }
                    value={this.state.maxPriceValue}
                    onKeyDown={this.maxInputHandler}
                    onChange={this.setMaxPriceValue}
                  />
                  <FlatButton
                    label={tr('Go')}
                    disabled={this.state.disabled}
                    style={this.styles.goButton}
                    labelStyle={this.styles.btnGoLabel}
                    onTouchTap={this.handlePrice.bind(this)}
                  />
                  {this.state.maxPriceLessThanMinValue && (
                    <div>
                      <span style={this.styles.priceErrorStyle}>
                        {tr('Max value should be greater than min value.')}
                      </span>
                    </div>
                  )}
                </div>
              )}
            </div>
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    } else {
      this.state.priceArray.map((element, index) => {
        PriceFilterElementsArray.push(
          <div className="outer-checkbox" key={index}>
            <Checkbox
              label={tr(element.label)}
              checked={this.state.priceArray[index].checked}
              iconStyle={this.styles.checkboxOuter}
              labelStyle={this.styles.checkboxOuterLabel}
              onCheck={(event, isInputChecked) =>
                this.handleChange(event, index, isInputChecked, 'amount_range')
              }
              value={element.value}
            />
          </div>
        );
      });
      return (
        <div className="new-search__paper-container">
          <Paper style={this.styles.paperStyle}>
            <div className="filter-title">{tr('Price')}</div>
            <div>
              <SelectField
                style={this.styles.priceDropDown}
                hintText={tr('Select Currency')}
                maxHeight={80}
                value={this.state.currency}
                underlineStyle={{ display: 'none' }}
                labelStyle={this.styles.labelStyleSelect}
                iconStyle={this.styles.iconSelect}
                menuItemStyle={this.styles.menuItemStyle}
              >
                {priceCurrencyOptions.sort().map((currency, index) => {
                  return (
                    <MenuItem
                      key={index}
                      value={currency.value}
                      innerDivStyle={{ padding: '0 8px' }}
                      primaryText={tr(currency.label)}
                      onTouchTap={this.handlePricingCurrencyChange.bind(this, currency.value)}
                    />
                  );
                })}
              </SelectField>
            </div>
            <div>
              {PriceFilterElementsArray}
              {this.state.currency == 'INR' && (
                <div style={{ marginTop: '8px' }}>
                  <input
                    type="text"
                    placeholder="₹​min"
                    className="price-filter-input"
                    style={
                      this.state.minPriceError ? this.styles.minPriceError : this.styles.minPrice
                    }
                    value={this.state.minPriceValue}
                    onKeyDown={this.minInputHandler}
                    onChange={this.setMinPriceValue}
                  />
                  <input
                    type="text"
                    placeholder="₹​max"
                    className="price-filter-input"
                    style={
                      this.state.maxPriceError ? this.styles.maxPriceError : this.styles.maxPrice
                    }
                    value={this.state.maxPriceValue}
                    onKeyDown={this.maxInputHandler}
                    onChange={this.setMaxPriceValue}
                  />
                  <FlatButton
                    label={tr('Go')}
                    disabled={this.state.disabled}
                    style={this.styles.goButton}
                    labelStyle={this.styles.btnGoLabel}
                    onTouchTap={this.handlePrice.bind(this)}
                  />
                  {this.state.maxPriceLessThanMinValue && (
                    <div>
                      <span style={this.styles.priceErrorStyle}>
                        {tr('Max value should be greater than min value.')}
                      </span>
                    </div>
                  )}
                </div>
              )}
              {this.state.currency == 'USD' && (
                <div style={{ marginTop: '8px' }}>
                  <input
                    type="text"
                    placeholder="$min"
                    className="price-filter-input"
                    style={
                      this.state.minPriceError ? this.styles.minPriceError : this.styles.minPrice
                    }
                    value={this.state.minPriceValue}
                    onKeyDown={this.minInputHandler}
                    onChange={this.setMinPriceValue}
                  />
                  <input
                    type="text"
                    placeholder="$max"
                    className="price-filter-input"
                    style={
                      this.state.maxPriceError ? this.styles.maxPriceError : this.styles.maxPrice
                    }
                    value={this.state.maxPriceValue}
                    onKeyDown={this.maxInputHandler}
                    onChange={this.setMaxPriceValue}
                  />
                  <FlatButton
                    label={tr('Go')}
                    disabled={this.state.disabled}
                    style={this.styles.goButton}
                    labelStyle={this.styles.btnGoLabel}
                    onTouchTap={this.handlePrice.bind(this)}
                  />
                  {this.state.maxPriceLessThanMinValue && (
                    <div>
                      <span style={this.styles.priceErrorStyle}>
                        {tr('Max value should be greater than min value.')}
                      </span>
                    </div>
                  )}
                </div>
              )}
            </div>
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    }
  }
}

PriceFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func
};

export default PriceFilterContainer;
