import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import ReactStars from 'react-stars';
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class RatingFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        }
      };
    }
  }

  handleChange = (e, isInputChecked, type) => {
    this.props.handleFilterChange(e.target.value, isInputChecked, type);
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  render() {
    let uncheckFilter = this.props.uncheckFilter;
    if (this.props.isNewStyle) {
      let RatingFilterElementsArray = [];
      for (let i = 5; i >= 0; i--) {
        RatingFilterElementsArray.push(
          <div className="outer-checkbox" key={i}>
            <Checkbox
              label={
                <ReactStars
                  count={5}
                  size={16}
                  half={false}
                  edit={false}
                  color1={'#d6d6e1'}
                  color2={'#6f708b'}
                  className="filter-stars"
                  value={i}
                />
              }
              iconStyle={this.styles.checkboxOuter}
              labelStyle={this.styles.checkboxOuterLabel}
              checkedIcon={<CheckOn1 color="#6f708b" />}
              uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
              onCheck={(event, isInputChecked) =>
                this.handleChange(event, isInputChecked, 'rating')
              }
              value={i}
              {...(!!uncheckFilter &&
              (uncheckFilter.clearAll === true ||
                (uncheckFilter.filter !== false &&
                  (uncheckFilter.filter == i && uncheckFilter.type === 'rating')))
                ? { checked: false }
                : {})}
            />
          </div>
        );
      }
      return (
        <div className="new-search__paper-container">
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title">{tr('Rating')}</div>
              {this.state.genpactUI && (
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              )}
            </div>
            {this.state.showMenu && <div>{RatingFilterElementsArray}</div>}

            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    } else {
      let RatingFilterElementsArray = [];
      for (let i = 5; i >= 0; i--) {
        RatingFilterElementsArray.push(
          <div className="outer-checkbox" key={i}>
            <Checkbox
              label={
                <ReactStars
                  count={5}
                  size={16}
                  half={false}
                  edit={false}
                  color1={'#d6d6e1'}
                  color2={'#6f708b'}
                  className="filter-stars"
                  value={i}
                />
              }
              iconStyle={this.styles.checkboxOuter}
              labelStyle={this.styles.checkboxOuterLabel}
              onCheck={(event, isInputChecked) =>
                this.handleChange(event, isInputChecked, 'rating')
              }
              value={i}
              {...(!!uncheckFilter &&
              (uncheckFilter.clearAll === true ||
                (uncheckFilter.filter !== false &&
                  (uncheckFilter.filter == i && uncheckFilter.type === 'rating')))
                ? { checked: false }
                : {})}
            />
          </div>
        );
      }
      return (
        <div className="new-search__paper-container">
          <Paper style={this.styles.paperStyle}>
            <div className="filter-title">{tr('Rating')}</div>
            {this.state.showMenu && <div>{RatingFilterElementsArray}</div>}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        </div>
      );
    }
  }
}

RatingFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default RatingFilterContainer;
