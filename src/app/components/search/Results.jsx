import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index'; // same as above this component is used for using predefined color variables
import Pathway from 'edc-web-sdk/components/icons/Pathway';
import { getUserPathways, addToPathway } from '../../actions/resultsActions'; //loads the actions related to this component defined in eclActions.jsx
import { tr } from 'edc-web-sdk/helpers/translations';

class Results extends Component {
  constructor(props, context) {
    super(props, context);
    this.getUserPathways = this.getUserPathways.bind(this);
    this.assignToPathway = this.assignToPathway.bind(this);
    this.closePathwayContainer = this.closePathwayContainer.bind(this);
    this.state = {
      showPathways: false
    };
  }

  // function get user pathways
  getUserPathways(id, itemType) {
    this.setState({ showPathways: true });
    this.props.dispatch(getUserPathways(id, itemType));
  }

  // close pathway container
  closePathwayContainer() {
    this.setState({ showPathways: false });
  }

  // function get user pathways
  assignToPathway(pathwayId, cardId, type) {
    this.props.dispatch(addToPathway(pathwayId, cardId, type));
  }

  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
  }

  render() {
    let id = this.props.id ? this.props.id : '';
    let title = this.props.title ? this.props.title.substr(0, 100) : '';
    let message = this.props.message
      ? this.props.message.replace(/<(?:.|\n)*?>/gm, '').substr(0, 300)
      : '';
    let cardType = this.props.cardType ? this.props.cardType.split('_').join(' ') : '';
    let authorName = this.props.authorName ? this.props.authorName : '';
    let authorImage = this.props.authorImage ? this.props.authorImage : '';
    let imageUrl = this.props.imageUrl
      ? this.props.imageUrl
      : '/assets/default_banner_image_min.jpg';

    let pathways = this.props.pathways;
    // console.log(pathways);

    return (
      <Paper>
        <div className="ecl-search-card">
          <div className="card-header">
            <div className="card-title">
              <a href="#">{title}...</a>
            </div>
            <div className="card-cat">
              <span>{cardType.replace('pack', 'pathways').replace('media', 'insights')}</span>
            </div>
          </div>
          <div className="card-description row">
            <div
              className="columns small-4 card-img"
              style={{ backgroundImage: 'url(' + imageUrl + ')' }}
            />
            <div className="columns small-8 card-desc">{message}...</div>
          </div>
          <div className="card-meta">
            {authorName && (
              <div className="author-info">
                {authorImage && (
                  <span className="author-image">
                    <img src={authorImage} />
                  </span>
                )}
                <span className="author-name">{authorName}</span>
              </div>
            )}
            <div className="card-actions">
              {cardType === 'media' && (
                <span>
                  <a onClick={this.getUserPathways.bind(this, id, 'Card')}>
                    <Pathway />
                  </a>
                  {this.state.showPathways && (
                    <Paper className="pathway-list-container">
                      <div className="pathway-message">{this.props.pathwayMessage}</div>
                      <div>
                        {pathways.length > 0 && (
                          <div>
                            <div className="pathway-list-heading">{tr('Add to your Pathways')}</div>
                            <ul>
                              {pathways.map((pathway, index) => {
                                return (
                                  <li key={index}>
                                    <a
                                      className="addToPathway"
                                      onClick={this.assignToPathway.bind(
                                        this,
                                        pathway.id,
                                        id,
                                        'Card'
                                      )}
                                    >
                                      {pathway.title}
                                    </a>
                                  </li>
                                );
                              })}
                            </ul>
                          </div>
                        )}
                      </div>
                      <div className="close-link">
                        <a className="close" onClick={this.closePathwayContainer.bind(this)}>
                          X
                        </a>
                      </div>
                    </Paper>
                  )}
                </span>
              )}
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

Results.defaultProps = {
  pathways: [],
  pathwayMessage: ''
};

Results.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  message: PropTypes.string,
  cardType: PropTypes.string,
  authorName: PropTypes.string,
  authorImage: PropTypes.string,
  pathways: PropTypes.array,
  pathwayMessage: PropTypes.string
};

export default connect()(Results);
