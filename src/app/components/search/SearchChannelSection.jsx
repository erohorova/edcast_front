import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Channel from '../discovery/Channel.jsx';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';

class SearchChannelSection extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpenBlock: true
    };

    this.styles = {
      accordionTitle: {
        textTransform: 'capitalize',
        fontSize: '16px',
        color: '#454560',
        paddingLeft: '0'
      },
      accordionTitleIcon: {
        marginLeft: '-10px',
        fill: '#454560',
        height: '34px',
        width: '34px'
      }
    };
  }

  toggleBlock = () => {
    this.setState({ isOpenBlock: !this.state.isOpenBlock });
  };

  render() {
    return (
      <div>
        {this.props.isNewStyle && (
          <div>
            <div>
              <FlatButton
                hoverColor="transparent"
                rippleColor="transparent"
                className="accordion-title"
                onClick={this.toggleBlock}
                label={
                  <span>
                    {tr(`Channel${this.props.channelCount > 1 ? 's' : ''}`)} (
                    {this.props.channelCount})
                  </span>
                }
                labelStyle={this.styles.accordionTitle}
                secondary={true}
                icon={
                  (this.state.isOpenBlock && (
                    <NavigationArrowDropDown style={this.styles.accordionTitleIcon} />
                  )) ||
                  (!this.state.isOpenBlock && (
                    <NavigationArrowDropUp style={this.styles.accordionTitleIcon} />
                  ))
                }
              />
            </div>
            {this.state.isOpenBlock && (
              <div>
                {this.props.channels && (
                  <div className="custom-card-container">
                    <div className="five-card-column vertical-spacing-medium">
                      {this.props.channels.map(channel => {
                        return (
                          <div className="card-v2 search-channel" key={channel.id}>
                            <Channel
                              channel={channel}
                              id={channel.id}
                              isFollowing={channel.isFollowing}
                              title={channel.label}
                              imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
                              slug={channel.slug}
                              allowFollow={channel.allowFollow}
                            />
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
                {this.props.channelCount != this.props.channels.length && (
                  <div className="veiw-more__container">
                    <div className="small-12 columns text-center">
                      <SecondaryButton
                        disabled={this.props.loading}
                        label={this.props.loading ? tr('Loading...') : tr('View More Channels')}
                        onClick={this.props.loadMore.bind(this, 'channel')}
                      />
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        )}
        {!this.props.isNewStyle && (
          <div className="result-block">
            <h6>
              {tr(`Channel${this.props.channelCount > 1 ? 's' : ''}`)} ({this.props.channelCount})
            </h6>
            {this.props.channels && (
              <div className="row">
                {this.props.channels.map(channel => {
                  return (
                    <div
                      className="small-4 columns search-channel info-block-in-search"
                      key={channel.id}
                    >
                      <Channel
                        channel={channel}
                        id={channel.id}
                        isFollowing={channel.isFollowing}
                        title={channel.label}
                        imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
                        slug={channel.slug}
                        allowFollow={channel.allowFollow}
                      />
                    </div>
                  );
                })}
              </div>
            )}
            {this.props.channelCount != this.props.channels.length && (
              <div className="row veiw-more__container">
                <div className="small-12 columns text-center">
                  <SecondaryButton
                    disabled={this.props.loading}
                    label={this.props.loading ? tr('Loading...') : tr('View More Channels')}
                    onClick={this.props.loadMore.bind(this, 'channel')}
                  />
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

SearchChannelSection.propTypes = {
  channels: PropTypes.object,
  channelCount: PropTypes.number,
  isNewStyle: PropTypes.bool,
  loading: PropTypes.bool,
  loadMore: PropTypes.func
};

export default SearchChannelSection;
