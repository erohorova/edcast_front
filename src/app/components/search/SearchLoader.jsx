import React, { Component } from 'react';
import Paper from 'edc-web-sdk/components/Paper';
import SearchLoaderCard from './SearchLoaderCard.jsx';

class SearchLoader extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      }
    };
  }

  render() {
    return (
      <div>
        <div className="row" style={{ marginBottom: '17px', marginTop: '7px' }}>
          <div className="small-3 columns">
            <div className="top-heading animated-background">
              <div className="grey-background-masker top-header left" />
            </div>
          </div>
          <div className="small-9 columns" style={{ paddingLeft: '11px' }}>
            <div className="top-heading animated-background right">
              <div className="grey-background-masker top-header right" />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-3 columns">
            <Paper style={this.styles.paperStyle} className="filter-container">
              <div className="animated-background ht-257">
                <div className="background-masker filter-line-1" />
                <div className="background-masker filter-line-2" />
                <div className="background-masker checkbox-outer first" />
                <div className="background-masker outer-checkbox-label-gap first" />
                <div className="background-masker after-label-1" />
                <div className="background-masker filter-line-4" />
                <div className="background-masker checkbox-inner-before first" />
                <div className="background-masker checkbox-inner first" />
                <div className="background-masker inner-checkbox-label-gap first" />
                <div className="background-masker after-label-2" />
                <div className="background-masker filter-line-5" />
                <div className="background-masker checkbox-inner-before second" />
                <div className="background-masker checkbox-inner second" />
                <div className="background-masker inner-checkbox-label-gap second" />
                <div className="background-masker after-label-3" />
                <div className="background-masker filter-line-6" />
                <div className="background-masker checkbox-inner-before third" />
                <div className="background-masker checkbox-inner third" />
                <div className="background-masker inner-checkbox-label-gap third" />
                <div className="background-masker after-label-4" />
                <div className="background-masker filter-line-7" />
                <div className="background-masker checkbox-inner-before fourth" />
                <div className="background-masker checkbox-inner fourth" />
                <div className="background-masker inner-checkbox-label-gap fourth" />
                <div className="background-masker after-label-5" />
                <div className="background-masker filter-line-8" />
                <div className="background-masker checkbox-inner-before fifth" />
                <div className="background-masker checkbox-inner fifth" />
                <div className="background-masker inner-checkbox-label-gap fifth" />
                <div className="background-masker after-label-6" />
                <div className="background-masker filter-line-9" />
                <div className="background-masker checkbox-outer second" />
                <div className="background-masker outer-checkbox-label-gap second" />
                <div className="background-masker after-label-7" />
                <div className="background-masker filter-line-10" />
                <div className="background-masker checkbox-outer third" />
                <div className="background-masker outer-checkbox-label-gap third" />
                <div className="background-masker after-label-8" />
              </div>
            </Paper>
            <Paper style={this.styles.paperStyle} className="filter-container">
              <div className="animated-background ht-180">
                <div className="background-masker filter-line-1" style={{ left: '54px' }} />
                <div className="background-masker filter-line-2" />
                <div className="background-masker checkbox-outer first" />
                <div className="background-masker outer-checkbox-label-gap first" />
                <div className="background-masker after-label-9" />
                <div className="background-masker filter-line-4" />
                <div className="background-masker checkbox-outer fourth" />
                <div className="background-masker outer-checkbox-label-gap fourth" />
                <div className="background-masker after-label-10" />
                <div className="background-masker filter-line-11" />
                <div className="background-masker checkbox-outer fifth" />
                <div className="background-masker outer-checkbox-label-gap fifth" />
                <div className="background-masker after-label-11" />
                <div className="background-masker filter-line-12" />
                <div className="background-masker checkbox-outer sixth" />
                <div className="background-masker outer-checkbox-label-gap sixth" />
                <div className="background-masker after-label-12" />
                <div className="background-masker filter-line-13" />
                <div className="background-masker checkbox-outer seventh" />
                <div className="background-masker outer-checkbox-label-gap seventh" />
                <div className="background-masker after-label-13" />
              </div>
            </Paper>
          </div>
          <div className="small-9 columns" style={{ paddingLeft: '11px' }}>
            <div className="content-section-heading animated-background">
              <div className="grey-background-masker content-header" />
            </div>
            <div className="row" style={{ marginTop: '15px' }}>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
              <div className="small-4 columns search-loader-card-wrapper">
                <SearchLoaderCard />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SearchLoader;
