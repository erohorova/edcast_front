import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Influencer from '../discovery/Influencer.jsx';
import UserSquareItemV2 from '../common/UserSquareItemV2';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';

let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

class SearchPeopleSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenBlock: true,
      isPeopleCardV2: window.ldclient.variation('people-card-v2', false)
    };

    this.styles = {
      accordionTitle: {
        textTransform: 'capitalize',
        fontSize: '16px',
        color: '#454560',
        paddingLeft: '0'
      },
      accordionTitleIcon: {
        marginLeft: '-10px',
        fill: '#454560',
        height: '34px',
        width: '34px'
      }
    };
  }
  toggleBlock = () => {
    this.setState({ isOpenBlock: !this.state.isOpenBlock });
  };

  render() {
    return (
      <div>
        {this.props.isNewStyle && (
          <div>
            <div>
              <FlatButton
                hoverColor="transparent"
                rippleColor="transparent"
                className="accordion-title"
                onClick={this.toggleBlock}
                label={
                  <span>
                    {tr('People')} ({this.props.peopleCount})
                  </span>
                }
                labelStyle={this.styles.accordionTitle}
                secondary={true}
                icon={
                  (this.state.isOpenBlock && (
                    <NavigationArrowDropDown style={this.styles.accordionTitleIcon} />
                  )) ||
                  (!this.state.isOpenBlock && (
                    <NavigationArrowDropUp style={this.styles.accordionTitleIcon} />
                  ))
                }
              />
            </div>
            {this.state.isOpenBlock && (
              <div>
                {this.props.users && (
                  <div className="custom-card-container">
                    <div className="five-card-column vertical-spacing-medium">
                      {this.props.users.map((user, index) => {
                        return this.state.isPeopleCardV2 ? (
                          <UserSquareItemV2
                            key={`team_user_${index}`}
                            user={user}
                            isCurrentUser={
                              this.props.currentUser
                                ? user.id === parseInt(this.props.currentUser.id)
                                : false
                            }
                            currentUser={this.props.currentUser}
                          />
                        ) : (
                          <div className="card-v2 search-people" key={user.id}>
                            <Influencer
                              id={user.id}
                              name={user.name}
                              handle={user.handle}
                              imageUrl={
                                (user.avatarimages && user.avatarimages.medium) || defaultUserImage
                              }
                              expertSkills={user.expertSkills}
                              roles={user.roles}
                              rolesDefaultNames={user.rolesDefaultNames}
                              position={'top'}
                              following={user.isFollowing}
                            />
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
                {this.props.peopleCount != this.props.users.length && (
                  <div className="veiw-more__container">
                    <div className="small-12 columns text-center">
                      <SecondaryButton
                        disabled={this.props.loading}
                        label={this.props.loading ? tr('Loading...') : tr('View More People')}
                        onClick={this.props.loadMore.bind(this, 'user')}
                      />
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        )}
        {!this.props.isNewStyle && (
          <div className="result-block">
            <h6>
              {tr('People')} ({this.props.peopleCount})
            </h6>
            {this.props.users && (
              <div className="row">
                {this.props.users.map((user, index) => {
                  return this.state.isPeopleCardV2 ? (
                    <UserSquareItemV2
                      key={`team_user_${index}`}
                      user={user}
                      isCurrentUser={
                        this.props.currentUser
                          ? user.id === parseInt(this.props.currentUser.id)
                          : false
                      }
                      currentUser={this.props.currentUser}
                    />
                  ) : (
                    <div
                      className="small-3 columns search-people info-block-in-search"
                      key={user.id}
                    >
                      <Influencer
                        id={user.id}
                        name={user.name}
                        handle={user.handle}
                        imageUrl={
                          (user.avatarimages && user.avatarimages.medium) || defaultUserImage
                        }
                        expertSkills={user.expertSkills}
                        roles={user.roles}
                        rolesDefaultNames={user.rolesDefaultNames}
                        position={'top'}
                        following={user.isFollowing}
                      />
                    </div>
                  );
                })}
              </div>
            )}
            {this.props.peopleCount != this.props.users.length && (
              <div className="row veiw-more__container">
                <div className="small-12 columns text-center">
                  <SecondaryButton
                    disabled={this.props.loading}
                    label={this.props.loading ? tr('Loading...') : tr('View More People')}
                    onClick={this.props.loadMore.bind(this, 'user')}
                  />
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

SearchPeopleSection.propTypes = {
  isNewStyle: PropTypes.bool,
  peopleCount: PropTypes.number,
  loading: PropTypes.bool,
  users: PropTypes.object,
  loadMore: PropTypes.func,
  currentUser: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser
}))(SearchPeopleSection);
