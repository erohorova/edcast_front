import React, { Component } from 'react'; // Reacts core property type & component class
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getSearchResults,
  modifyTypeFilters,
  loadMoreSearchResults,
  hideFilters,
  populateSelectedFilters,
  updateCardInReducer
} from '../../actions/searchActions'; //loads the actions related to this component defined in eclActions.jsx
import TypeFilterContainer from './TypeFilterContainer.jsx';
import LevelFilterContainer from './LevelFilterContainer.jsx';
import RatingFilterContainer from './RatingFilterContainer.jsx';
import PriceFilterContainer from './PriceFilterContainer.jsx';
import PriceFilterContainerFreePaid from './PriceFilterContainerFreePaid.jsx';
import PriceFilterContainerPricePlan from './PriceFilterContainerPricePlan.jsx';
import SourcesFilterContainer from './SourcesFilterContainer.jsx';
import ProvidersFilterContainer from './ProvidersFilterContainer.jsx';
import LanguagesFilterContainer from './LanguagesFilterContainer.jsx';
import SubjectsFilterContainer from './SubjectsFilterContainer.jsx';
import YearFilterContainer from './YearFilterContainer.jsx';
import ActionByFilterComponent from './ActionByFilterComponent.jsx';
import SearchContentSection from './SearchContentSection.jsx';
import SearchChannelSection from './SearchChannelSection.jsx';
import SearchPeopleSection from './SearchPeopleSection.jsx';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import SearchLoader from './SearchLoader.jsx';
import Spinner from '../common/spinner.jsx';
import { routeActions, push } from 'react-router-redux'; // used to perform Route actions for e.g redirection
import { tr } from 'edc-web-sdk/helpers/translations';
import { groupBy, startCase } from 'lodash';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import { Permissions } from '../../utils/checkPermissions';

class SearchContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      chip: {
        //margin: '4px',
        background: '#d6d6e1',
        color: '#6f708b',
        borderRadius: '0px',
        margin: '.25rem .25rem .25rem .25rem',
        paddingRight: '1rem'
      },
      toggle: {
        maxWidth: '16rem'
      },
      toggleLabel: {
        fontSize: '1rem'
      },
      closeButton: {
        borderWidth: '0px',
        borderRadius: '50%',
        border: 'none',
        color: '#6f708b',
        verticalAlign: 'middle',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        padding: '0px',
        position: 'absolute',
        marginTop: '2px'
      },
      chipContainer: {
        flexWrap: 'wrap',
        display: 'flex'
      },
      chipClose: {
        background: '#6f708b',
        border: 'none',
        color: '#6f708b',
        fill: '#6f708b',
        width: '18px',
        height: '18px',
        marginTop: '5px'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      },
      capitalizeText: {
        textTransform: 'capitalize'
      },
      clearAllLink: {
        lineHeight: '2.5rem',
        marginLeft: '0.5rem'
      }
    };
    this.state = {
      filters: {},
      offset: 0,
      contentOffset: 0,
      pathwayOffset: 0,
      journeyOffset: 0,
      channelOffset: 0,
      userOffset: 0,
      cardsLimit: 18,
      limit: 5,
      sourceLineShowCount: 5,
      q: props.location.query.q,
      type: props.location.query.type,
      typeFilters: groupBy(props.typeFilters, r => r.subType),
      paidContentFilters: props.paidContentFilters,
      sourceFilters: props.sourceFilters,
      providerFilters: props.providerFilters,
      languageFilters: props.languageFilters,
      subjectFilters: props.subjectFilters,
      yearFilters: { fromDate: null, tillDate: null },
      hideFilters: false,
      isShowNewSearchUi: window.ldclient.variation('new-search-page-ui', false),
      showPriceFilterUi: window.ldclient.variation('show-price-filter-ui', false),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      contentLoading: false,
      pathwayLoading: false,
      journeyLoading: false,
      channelLoading: false,
      peopleLoading: false,
      contentLastPage: false,
      pathwayLastPage: false,
      journeyLastPage: false,
      channelLastPage: false,
      peopleLastPage: false,
      AllFiltersArry: [],
      uncheckFilter: { clearAll: false, filter: false, type: false },
      detailedSearch: window.ldclient.variation('detailed-global-search', true)
    };
    this.showBIA = !!this.props.team.config.enabled_bia;
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      {}
    );
    this.props.dispatch(userInfoCallBack);
    let params = {
      q: this.props.location.query.q,
      limit: this.state.limit,
      cards_limit: this.state.cardsLimit
    };
    switch (this.props.location.query.type) {
      case 'content':
        params[`content_type[]`] = [
          'article',
          'video',
          'course',
          'book',
          'poll',
          'insight',
          'video_stream'
        ];
        break;
      case 'user':
        params[`content_type[]`] = ['user'];
        break;
      case 'channels':
        params[`content_type[]`] = ['channel'];
        break;
      default:
        params[`content_type[]`] = params[`content_type[]`] || [];
        break;
    }

    this.setState({ q: params.q, loading: true });

    this.props
      .dispatch(getSearchResults(params, true, this.state.detailedSearch))
      .then(response => {
        this.setState({
          loading: false
        });
      })
      .catch(e => {
        console.error(e);
      });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextProps.immutableSearch !== this.props.immutableSearch ||
      nextProps.immutableTeam !== this.props.immutableTeam ||
      nextProps.immutableResults !== this.props.immutableResults ||
      nextState.contentLoading !== this.state.contentLoading ||
      nextState.pathwayLoading !== this.state.pathwayLoading ||
      nextState.journeyLoading !== this.state.journeyLoading ||
      nextState.channelLoading !== this.state.channelLoading ||
      nextState.peopleLoading !== this.state.peopleLoading ||
      nextState.loading !== this.state.loading
    ) {
      return true;
    }
    return false;
  }

  componentWillReceiveProps(nextProps) {
    this.state.uncheckFilter && this.state.uncheckFilter.clearAll === true
      ? this.setState({ uncheckFilter: { clearAll: false, filter: false, type: false } })
      : '';
    this.setState({
      loading: nextProps.loading,
      contentChecked: nextProps.contentChecked,
      paidContentChecked: nextProps.paidContentChecked,
      hideFilters: nextProps.hideFilters,
      typeFilters: groupBy(nextProps.typeFilters, r => r.subType),
      sourceFilters: nextProps.sourceFilters,
      providerFilters: nextProps.providerFilters,
      languageFilters: nextProps.languageFilters,
      subjectFilters: nextProps.subjectFilters,
      paidContentFilters: nextProps.paidContentFilters
    });
    if (
      nextProps.location.query.q !== this.state.q ||
      nextProps.location.query.type !== this.state.type
    ) {
      let params = {
        q: nextProps.location.query.q,
        limit: this.state.limit,
        cards_limit: this.state.cardsLimit
      };
      this.setState({
        q: params.q,
        type: nextProps.location.query.type,
        loading: true,
        filters: {},
        userOffset: 0,
        channelOffset: 0,
        contentOffset: 0,
        pathwayOffset: 0,
        journeyOffset: 0,
        yearFilters: { fromDate: null, tillDate: null }
      });

      switch (nextProps.location.query.type) {
        case 'content':
          params[`content_type[]`] = [
            'article',
            'video',
            'course',
            'book',
            'poll',
            'insight',
            'video_stream'
          ];
          break;
        case 'user':
          params[`content_type[]`] = ['user'];
          break;
        case 'channels':
          params[`content_type[]`] = ['channel'];
          break;
        default:
          params[`content_type[]`] = params[`content_type[]`] || [];
          break;
      }

      nextProps
        .dispatch(getSearchResults(params, true, this.state.detailedSearch))
        .then(response => {
          this.setState({
            loading: false
          });
        })
        .catch(e => {
          console.error(e);
        });
    }

    if (nextProps.shouldResetInline) {
      this.setState({
        q: nextProps.location.query.q,
        type: nextProps.location.query.type,
        loading: true,
        filters: {},
        userOffset: 0,
        channelOffset: 0,
        contentOffset: 0,
        pathwayOffset: 0,
        journeyOffset: 0,
        yearFilters: { fromDate: null, tillDate: null }
      });
    }
  }

  preparePayload = (payload, filter, status, type, currency = null) => {
    if (this.state.sociativeSearch) {
      payload['sociative'] = true;
    }

    let typeFilters = this.state.typeFilters;
    if (['posted_by', 'liked_by', 'commented_by', 'bookmarked_by'].includes(type)) {
      if (!payload.hasOwnProperty(`${type}`)) {
        payload[`${type}`] = {};
      }
      if (status) {
        payload[`${type}`][`${filter}`] = status;
      } else {
        delete payload[`${type}`][`${filter}`];
        if (Object.keys(payload[`${type}`]) === 0) {
          delete payload[`${type}`];
        }
      }
    } else if (type === 'year') {
      if (filter === 'clear' || !status) {
        delete payload['from_date'];
        delete payload['till_date'];
        this.setState({ yearFilters: { fromDate: null, tillDate: null } });
      } else {
        let dateRange = this.calcRange(filter);
        payload['from_date'] = dateRange[0];
        payload['till_date'] = dateRange[1];
        let yearFilter = { fromDate: dateRange[0], tillDate: dateRange[1] };
        this.setState({ yearFilters: yearFilter });
      }
    } else if (type === 'amount_range_empty') {
      if (payload['amount_range[]'] && payload['amount_range[]'].length) {
        delete payload['amount_range[]'];
        if (payload['currency']) {
          delete payload['currency'];
        }
      }
    } else {
      if (!payload.hasOwnProperty(`${type}[]`)) {
        payload[`${type}[]`] = [];
      }
      if (currency) {
        payload['currency'] = currency;
      }
      if (type == 'free_paid') {
        payload['is_paid'] = filter == 'paid' ? true : false;
      }
      if (type == 'clear_price') {
        if (typeof payload['is_paid'] === 'boolean') {
          delete payload['is_paid'];
        }
      }
      if (status) {
        if (!payload[`${type}[]`].includes(filter)) {
          payload[`${type}[]`].push(filter);
          if (filter === 'paid') {
            payload[`${type}[]`].push('free/paid');
          }
        }
      } else {
        var index = payload[`${type}[]`].indexOf(filter);
        if (index > -1) {
          payload[`${type}[]`].splice(index, 1);
        }
        if (payload['plan[]'] && filter == 'paid') {
          var index1 = payload[`${type}[]`].indexOf('free/paid');
          if (index1 > -1) {
            payload[`${type}[]`].splice(index1, 1);
          }
        }
        if (!payload[`${type}[]`].length) {
          delete payload[`${type}[]`];
          if (payload['currency'] && type == 'amount_range') {
            delete payload['currency'];
          }
        }
      }
    }
  };

  formattedDate = date => {
    return `${date.getDate()}/${
      date.getMonth() < 12 ? date.getMonth() + 1 : 1
    }/${date.getFullYear()}`;
  };

  calcRange = filter => {
    let d = new Date();
    let fromDate = this.formattedDate(d);
    let tillDate = this.formattedDate(d);
    switch (filter) {
      case '30':
        fromDate = this.formattedDate(new Date(d.setDate(d.getDate() - 30)));
        break;
      case '90':
        fromDate = this.formattedDate(new Date(d.setDate(d.getDate() - 90)));
        break;
      default:
        fromDate = `01/01/${filter}`;
        tillDate = `31/12/${filter}`;
        break;
    }
    return [fromDate, tillDate];
  };

  handleFilterChange = (filter, status, type, currency = null, sourceDisplayName = null) => {
    this.setState({
      contentLastPage: false,
      pathwayLastPage: false,
      journeyLastPage: false,
      channelLastPage: false,
      peopleLastPage: false
    });
    let payload = this.state.filters;
    let typeFilters = this.state.typeFilters;
    let planFilters = this.state.paidContentFilters;

    let clonedAllFiltersArry = this.state.AllFiltersArry.slice();
    let AllFiltersArry;
    let uncheckFilter;

    if (status) {
      clonedAllFiltersArry.push({
        filter: filter,
        type: type,
        sourceDisplayName: sourceDisplayName
      });
      AllFiltersArry = clonedAllFiltersArry;
      uncheckFilter = { clearAll: false, filter: false, type: false };
    } else {
      AllFiltersArry = clonedAllFiltersArry.filter(
        filterObj => !(filterObj.filter == filter && filterObj.type == type)
      );
      uncheckFilter = { clearAll: false, filter: filter, type: type };
    }

    if (type === 'year') {
      if (this.state.isShowNewSearchUi) {
        this.preparePayload(payload, filter, status, type);
        this.props.dispatch(modifyTypeFilters(filter, status, type));
      } else {
        this.preparePayload(payload, filter, null, type);
      }
    }

    // Handle Content Check Case
    if (filter === 'content' && type === 'content_type') {
      typeFilters.Content.forEach(typeFilter => {
        this.preparePayload(
          payload,
          `${typeFilter.display_name.toLowerCase().replace(' ', '_')}`,
          status,
          type
        );
        this.props.dispatch(
          modifyTypeFilters(
            `${typeFilter.display_name.toLowerCase().replace(' ', '_')}`,
            status,
            type
          )
        );
      });
    } else if (filter === 'paid_content' && type === 'plan') {
      planFilters.forEach(planFilter => {
        this.preparePayload(
          payload,
          `${planFilter.display_name.toLowerCase().replace(' ', '_')}`,
          status,
          type
        );
        this.props.dispatch(
          modifyTypeFilters(
            `${planFilter.display_name.toLowerCase().replace(' ', '_')}`,
            status,
            type
          )
        );
      });
    } else {
      if (!!currency) {
        this.preparePayload(payload, filter, status, type, currency);
        this.props.dispatch(modifyTypeFilters(currency, currency, 'currency'));
        if (status == null) {
          this.props.dispatch(modifyTypeFilters(filter, false, type));
          return;
        }
      } else {
        this.preparePayload(payload, filter, status, type);
      }
      this.props.dispatch(modifyTypeFilters(filter, status, type));
    }

    if (
      filter === 'channel' ||
      filter === 'user' ||
      (this.state.detailedSearch && (filter === 'pathway' || filter === 'journey'))
    ) {
      this.setState({ channelOffset: 0, userOffset: 0, pathwayOffset: 0, journeyOffset: 0 });
      payload = Object.assign(payload, {
        q: this.state.q,
        limit: this.state.limit,
        offset: 0,
        cards_limit: this.state.cardsLimit,
        cards_offset: this.state.contentOffset
      });
    } else {
      this.setState({
        contentOffset: 0,
        pathwayOffset: 0,
        journeyOffset: 0,
        channelOffset: 0,
        userOffset: 0
      });
      payload = Object.assign(payload, {
        q: this.state.q,
        limit: this.state.limit,
        offset: 0,
        cards_limit: this.state.cardsLimit,
        cards_offset: 0
      });
    }

    this.setState({ hideFilters: true });
    this.setState({ AllFiltersArry, uncheckFilter });
    this.props.dispatch(hideFilters());
    this.props.dispatch(getSearchResults(payload, false, this.state.detailedSearch));
  };

  clearAllFilters = () => {
    this.setState({
      AllFiltersArry: [],
      uncheckFilter: { clearAll: true, filter: false, type: false }
    });
    let params = { q: this.state.q, limit: this.state.limit, cards_limit: this.state.cardsLimit };
    this.props.dispatch(getSearchResults(params, true));
  };

  fetchSelectedFilters = (payload, type) => {
    this.setState({
      contentLastPage: false,
      pathwayLastPage: false,
      journeyLastPage: false,
      channelLastPage: false,
      peopleLastPage: false
    });

    let sourceFilters = this.state.sourceFilters;
    let levelFilters = this.props.levelFilters;
    let providerFilters = this.state.providerFilters;
    let languageFilters = this.state.languageFilters;
    let subjectFilters = this.state.subjectFilters;
    let freeContentFilter = this.props.freeContentFilter;
    let paidContentFilters = this.props.paidContentFilters;
    let ratingFilters = this.props.ratingFilters;
    let currencyFilters = this.props.currencyFilters;
    let isPaidFilters = this.props.isPaidFilters;
    let amountRangeFilters = this.props.amountRangeFilters;
    let postedByFilters = this.props.postedByFilters;
    let likedByFilters = this.props.likedByFilters;
    let commentedByFilters = this.props.commentedByFilters;
    let bookmarkedByFilters = this.props.bookmarkedByFilters;

    payload['content_type[]'] = [];

    if (type === 'content') {
      let typeFilters = this.state.typeFilters;
      let filters = typeFilters.Content.filter(item => {
        return item.checked;
      });
      if (filters.length === 0) {
        typeFilters.Content.forEach(item => {
          payload['content_type[]'].push(item.display_name.toLowerCase().replace(' ', '_'));
        });
      } else {
        filters.forEach(item => {
          payload['content_type[]'].push(item.display_name.toLowerCase().replace(' ', '_'));
        });
      }
    }

    let sourceFilterIds = sourceFilters
      .filter(item => {
        return item.checked;
      })
      .map(item => {
        return item.id;
      });
    if (sourceFilterIds.length != 0) {
      payload['source_id[]'] = [];
      sourceFilterIds.forEach(item => {
        payload['source_id[]'].push(item);
      });
    }

    let providerFilterIds = providerFilters
      .filter(item => {
        return item.checked;
      })
      .map(item => {
        return item.id;
      });
    if (providerFilterIds.length != 0) {
      payload['provider_name[]'] = [];
      providerFilterIds.forEach(item => {
        payload['provider_name[]'].push(item);
      });
    }

    let languageFilterIds = languageFilters
      .filter(item => {
        return item.checked;
      })
      .map(item => {
        return item.id;
      });
    if (languageFilterIds.length != 0) {
      payload['languages[]'] = [];
      languageFilterIds.forEach(item => {
        payload['languages[]'].push(item);
      });
    }

    let subjectFilterIds = subjectFilters
      .filter(item => {
        return item.checked;
      })
      .map(item => {
        return item.id;
      });
    if (subjectFilterIds.length != 0) {
      payload['subjects[]'] = [];
      subjectFilterIds.forEach(item => {
        payload['subjects[]'].push(item);
      });
    }

    if (this.state.yearFilters.fromDate && this.state.yearFilters.tillDate) {
      payload['from_date'] = this.state.yearFilters.fromDate;
      payload['till_date'] = this.state.yearFilters.tillDate;
    }

    Object.keys(paidContentFilters).filter(item => {
      if (paidContentFilters[item]) {
        if (!payload.hasOwnProperty('plan[]')) {
          payload['plan[]'] = [];
        }
        if (paidContentFilters[item].checked) {
          payload['plan[]'].push(
            paidContentFilters[item].display_name.toLowerCase().replace(' ', '_')
          );
          if (paidContentFilters[item].display_name == 'Paid') {
            payload['plan[]'].push('free/paid');
          }
        }
      }
    });
    Object.keys(freeContentFilter).filter(item => {
      if (freeContentFilter[item]) {
        if (!payload.hasOwnProperty('plan[]')) {
          payload['plan[]'] = [];
        }
        payload['plan[]'].push('free');
      }
    });

    Object.keys(levelFilters).filter(item => {
      if (levelFilters[item]) {
        if (!payload.hasOwnProperty('level[]')) {
          payload['level[]'] = [];
        }
        payload['level[]'].push(item);
      }
    });

    Object.keys(ratingFilters).filter(item => {
      if (ratingFilters[item]) {
        if (!payload.hasOwnProperty('rating[]')) {
          payload['rating[]'] = [];
        }
        payload['rating[]'].push(item);
      }
    });
    if (currencyFilters) {
      payload['currency'] = currencyFilters;
    }

    Object.keys(amountRangeFilters).filter(item => {
      if (amountRangeFilters[item]) {
        if (!payload.hasOwnProperty('amount_range[]')) {
          payload['amount_range[]'] = [];
        }
        payload['amount_range[]'].push(item);
      }
    });

    if (typeof isPaidFilters === 'boolean') {
      payload['is_paid'] = isPaidFilters;
    }

    Object.keys(postedByFilters).filter(item => {
      if (postedByFilters[item]) {
        if (!payload.hasOwnProperty('posted_by')) {
          payload['posted_by'] = {};
        }
        payload['posted_by'][item] = postedByFilters[item];
      }
    });

    Object.keys(likedByFilters).forEach(item => {
      if (likedByFilters[item]) {
        if (!payload.hasOwnProperty('liked_by')) {
          payload['liked_by'] = {};
        }
        payload['liked_by'][item] = true;
      }
    });

    Object.keys(commentedByFilters).forEach(item => {
      if (commentedByFilters[item]) {
        if (!payload.hasOwnProperty('commented_by')) {
          payload['commented_by'] = {};
        }
        payload['commented_by'][item] = true;
      }
    });

    Object.keys(bookmarkedByFilters).forEach(item => {
      if (bookmarkedByFilters[item]) {
        if (!payload.hasOwnProperty('bookmarked_by')) {
          payload['bookmarked_by'] = {};
        }
        payload['bookmarked_by'][item] = true;
      }
    });

    return payload;
  };

  loadMore = type => {
    let payload = {};
    if (type === 'content') {
      payload = this.fetchSelectedFilters(payload, type);
      this.setState({
        contentOffset: this.state.contentOffset + this.state.cardsLimit,
        contentLoading: true
      });
      payload = Object.assign(payload, {
        q: this.state.q,
        cards_limit: this.state.cardsLimit,
        cards_offset: this.state.contentOffset + this.state.cardsLimit
      });
    }

    if (type === 'pathway') {
      payload = this.fetchSelectedFilters(payload, type);
      payload['content_type[]'] = [type];
      this.setState({
        pathwayOffset: this.state.pathwayOffset + this.state.cardsLimit,
        pathwayLoading: true
      });
      payload = Object.assign(payload, {
        q: this.state.q,
        cards_limit: this.state.cardsLimit,
        cards_offset: this.state.pathwayOffset + this.state.cardsLimit
      });
    }

    if (type === 'journey') {
      payload = this.fetchSelectedFilters(payload, type);
      payload['content_type[]'] = [type];
      this.setState({
        journeyOffset: this.state.journeyOffset + this.state.cardsLimit,
        journeyLoading: true
      });
      payload = Object.assign(payload, {
        q: this.state.q,
        cards_limit: this.state.cardsLimit,
        cards_offset: this.state.journeyOffset + this.state.cardsLimit
      });
    }

    if (type === 'channel') {
      payload['content_type[]'] = [type];
      this.setState({
        channelOffset: this.state.channelOffset + this.state.limit,
        channelLoading: true
      });
      payload = Object.assign(payload, {
        q: this.state.q,
        limit: this.state.limit,
        offset: this.state.channelOffset + this.state.limit
      });
    }

    if (type === 'user') {
      payload['content_type[]'] = [type];
      this.setState({ userOffset: this.state.userOffset + this.state.limit, peopleLoading: true });
      payload = Object.assign(payload, {
        q: this.state.q,
        limit: this.state.limit,
        offset: this.state.userOffset + this.state.limit
      });
    }
    this.props
      .dispatch(loadMoreSearchResults(payload, this.state.detailedSearch))
      .then(items => {
        let stateObject = {
          contentLoading: false,
          pathwayLoading: false,
          journeyLoading: false,
          channelLoading: false,
          peopleLoading: false
        };
        switch (type) {
          case 'content':
            stateObject.contentLastPage = !items.cards.length;
            break;
          case 'pathway':
            stateObject.pathwayLastPage = !items.pathways.length;
            break;
          case 'journey':
            stateObject.journeyLastPage = !items.journeys.length;
            break;
          case 'channel':
            stateObject.channelLastPage = !items.channels.length;
            break;
          case 'user':
            stateObject.peopleLastPage = !items.users.length;
            break;
          default:
            break;
        }
        this.setState(stateObject);
      })
      .catch(err => {
        console.error(`Error in SearchContainer.loadMoreSearchResults.func : ${err}`);
      });
  };

  cardUpdated = data => {
    this.props.dispatch(updateCardInReducer(data));
  };

  render() {
    let q = this.props.location.query.q; // getting the value of query params
    let typeFilters = this.state.typeFilters;
    let paidContentFilters = this.state.paidContentFilters;
    let loading = this.state.loading;
    let AllFiltersArry = this.state.AllFiltersArry;

    let startCasedSourceFilters = this.state.sourceFilters.map(filter => {
      let startCaseName = startCase(filter.display_name);
      filter.display_name = startCaseName;
      return filter;
    });
    let startCasedProviderFilters = this.state.providerFilters.map(filter => {
      let startCaseName = startCase(filter.display_name);
      filter.display_name = startCaseName;
      return filter;
    });
    let startCasedLanguageFilters = this.state.languageFilters.map(filter => {
      let startCaseName = startCase(filter.display_name);
      filter.display_name = startCaseName;
      return filter;
    });
    let startCasedSubjectFilters = this.state.subjectFilters.map(filter => {
      let startCaseName = startCase(filter.display_name);
      filter.display_name = startCaseName;
      return filter;
    });
    return (
      <div>
        {this.state.isShowNewSearchUi && (
          <div id="ecl-search2">
            {!loading ? (
              <div>
                <div className="search-container">
                  <div className="search-container__left-column">
                    <div className="search-container__subtitle">{tr('Refine your search')}</div>
                    <TypeFilterContainer
                      filters={typeFilters}
                      contentCount={this.props.contentSectionCount}
                      handleFilterChange={this.handleFilterChange}
                      contentChecked={this.props.contentChecked}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />

                    {this.state.sourceFilters.length > 0 && (
                      <SourcesFilterContainer
                        selectedSourceFilters={this.props.selectedSourceFilters}
                        filters={startCasedSourceFilters}
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        sourceLineShowCount={this.state.sourceLineShowCount}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    )}
                    {this.state.providerFilters.length > 0 && (
                      <ProvidersFilterContainer
                        selectedProviderFilters={this.props.selectedProviderFilters}
                        filters={startCasedProviderFilters}
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        sourceLineShowCount={this.state.sourceLineShowCount}
                      />
                    )}
                    {this.state.languageFilters.length > 0 && (
                      <LanguagesFilterContainer
                        selectedLanguageFilters={this.props.selectedLanguageFilters}
                        filters={startCasedLanguageFilters}
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        sourceLineShowCount={this.state.sourceLineShowCount}
                      />
                    )}
                    {this.state.subjectFilters.length > 0 && (
                      <SubjectsFilterContainer
                        selectedSubjectFilters={this.props.selectedSubjectFilters}
                        filters={startCasedSubjectFilters}
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        sourceLineShowCount={this.state.sourceLineShowCount}
                      />
                    )}
                    {this.showBIA && (
                      <LevelFilterContainer
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    )}
                    {Permissions.has('CAN_RATE') && (
                      <RatingFilterContainer
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    )}
                    {this.state.showPriceFilterUi && this.state.edcastPlansForPricing && (
                      <PriceFilterContainerPricePlan
                        filters={paidContentFilters}
                        handleFilterChange={this.handleFilterChange}
                        paidContentChecked={this.props.paidContentChecked}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    )}
                    {this.state.showPriceFilterUi && !this.state.edcastPlansForPricing && (
                      <PriceFilterContainerFreePaid
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        isNewStyle={this.state.isShowNewSearchUi}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    )}
                    <YearFilterContainer
                      handleFilterChange={this.handleFilterChange}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />
                    <ActionByFilterComponent
                      handleFilterChange={this.handleFilterChange}
                      type={'posted_by'}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />
                    <ActionByFilterComponent
                      handleFilterChange={this.handleFilterChange}
                      type={'liked_by'}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />
                    <ActionByFilterComponent
                      handleFilterChange={this.handleFilterChange}
                      type={'commented_by'}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />
                    <ActionByFilterComponent
                      handleFilterChange={this.handleFilterChange}
                      type={'bookmarked_by'}
                      hideFilters={this.state.hideFilters}
                      isNewStyle={this.state.isShowNewSearchUi}
                      uncheckFilter={this.state.uncheckFilter}
                    />
                  </div>
                  <div className="search-container__right-column">
                    <div className="search-container__subtitle" role="heading" aria-level="3">
                      <span className="lighter">{tr('About')} </span>
                      <strong>{this.props.searchTotalCount}</strong>
                      <span className="lighter"> {tr('results for')} "</span>
                      <strong>{q}</strong>
                      <span className="lighter">"</span>
                      <div className="chip-container" style={this.styles.chipContainer}>
                        {AllFiltersArry.length > 0 &&
                          AllFiltersArry.map(selectedFilter => {
                            return (
                              <Chip
                                style={this.styles.chip}
                                labelStyle={this.styles.chipLabel}
                                backgroundColor={'#fff'}
                              >
                                <small style={this.styles.capitalizeText}>
                                  {selectedFilter.filter === 'content' &&
                                  selectedFilter.type === 'content_type'
                                    ? tr('SmartCards')
                                    : selectedFilter.filter === 'user' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('People')
                                    : selectedFilter.filter === 'pathway' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('Pathways')
                                    : selectedFilter.filter === 'channel' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('Channels')
                                    : selectedFilter.sourceDisplayName
                                    ? selectedFilter.sourceDisplayName
                                    : selectedFilter.type === 'rating'
                                    ? `${tr('Rating')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'year' &&
                                      (selectedFilter.filter === '30' ||
                                        selectedFilter.filter === '90')
                                    ? tr(`Last ${selectedFilter.filter} Days`)
                                    : selectedFilter.type === 'posted_by'
                                    ? `${tr('Posted by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'liked_by'
                                    ? `${tr('Liked by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'commented_by'
                                    ? `${tr('Commented by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'bookmarked_by'
                                    ? `${tr('Bookmarked by')} - ${selectedFilter.filter}`
                                    : selectedFilter.filter.replace('_', ' ')}{' '}
                                </small>
                                <IconButton
                                  className="cancel-icon delete"
                                  ref={'cancelIcon'}
                                  tooltip={tr('Remove Filter')}
                                  disableTouchRipple
                                  tooltipPosition="top-center"
                                  style={this.styles.closeButton}
                                  onTouchTap={e =>
                                    this.handleFilterChange(
                                      selectedFilter.filter,
                                      false,
                                      selectedFilter.type
                                    )
                                  }
                                >
                                  <Close style={this.styles.chipClose} />
                                </IconButton>
                              </Chip>
                            );
                          })}
                        {AllFiltersArry.length > 0 ? (
                          <a
                            href="#"
                            style={this.styles.clearAllLink}
                            onClick={() => this.clearAllFilters()}
                          >
                            {tr('Clear all')}
                          </a>
                        ) : (
                          ''
                        )}
                      </div>
                    </div>
                    {this.state.detailedSearch && this.props.contentSectionCount > 0 && (
                      <SearchContentSection
                        cards={this.props.cards}
                        contentSectionCount={this.props.contentSectionCount}
                        loadMore={this.loadMore}
                        cardUpdated={this.cardUpdated}
                        label={this.state.contentLoading ? 'Loading...' : 'content'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.contentLastPage}
                      />
                    )}
                    {this.state.detailedSearch && this.props.pathwaysCount > 0 && (
                      <SearchContentSection
                        cards={this.props.pathways}
                        contentSectionCount={this.props.pathwaysCount}
                        loadMore={this.loadMore}
                        label={this.state.pathwayLoading ? 'Loading...' : 'pathway'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.pathwayLastPage}
                      />
                    )}
                    {this.state.detailedSearch && this.props.journeysCount > 0 && (
                      <SearchContentSection
                        cards={this.props.journeys}
                        contentSectionCount={this.props.journeysCount}
                        loadMore={this.loadMore}
                        label={this.state.journeyLoading ? 'Loading...' : 'journey'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.journeyLastPage}
                      />
                    )}
                    {!this.state.detailedSearch && this.props.contentSectionCount > 0 && (
                      <SearchContentSection
                        cards={this.props.cards}
                        contentSectionCount={this.props.contentSectionCount}
                        loadMore={this.loadMore}
                        cardUpdated={this.cardUpdated}
                        label={this.state.contentLoading ? 'Loading...' : 'content'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.contentLastPage}
                      />
                    )}
                    {!this.state.detailedSearch && this.props.pathwaysCount > 0 && (
                      <SearchContentSection
                        cards={this.props.cards}
                        contentSectionCount={this.props.pathwaysCount}
                        loadMore={this.loadMore}
                        label={this.state.pathwayLoading ? 'Loading...' : 'pathway'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.pathwayLastPage}
                      />
                    )}
                    {!this.state.detailedSearch && this.props.journeysCount > 0 && (
                      <SearchContentSection
                        cards={this.props.cards}
                        contentSectionCount={this.props.journeysCount}
                        loadMore={this.loadMore}
                        label={this.state.journeyLoading ? 'Loading...' : 'journey'}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.journeyLastPage}
                      />
                    )}
                    {this.props.channelCount > 0 && (
                      <SearchChannelSection
                        channels={this.props.channels}
                        channelCount={this.props.channelCount}
                        loadMore={this.loadMore}
                        loading={this.state.channelLoading}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.channelLastPage}
                      />
                    )}
                    {this.props.peopleCount > 0 && (
                      <SearchPeopleSection
                        users={this.props.users}
                        peopleCount={this.props.peopleCount}
                        loadMore={this.loadMore}
                        loading={this.state.peopleLoading}
                        isNewStyle={this.state.isShowNewSearchUi}
                        isLastPage={this.state.peopleLastPage}
                      />
                    )}
                    {(this.props.contentSectionCount ||
                      this.props.channelCount ||
                      this.props.peopleCount ||
                      this.props.pathwaysCount ||
                      this.props.journeysCount) === 0 && (
                      <div className="text-center data-not-available-msg">
                        {tr('Sorry! No results found ')}.
                      </div>
                    )}
                  </div>
                </div>
              </div>
            ) : (
              loading && (
                <div className="search-spinner">
                  <Spinner />
                </div>
              )
            )}
            {!loading && !this.props.isFetchingResultsFailed && this.props.totalCount === 0 && (
              <div className="text-center data-not-available-msg">
                {tr("Sorry! We couldn't find what you're looking for")}.
              </div>
            )}
            {this.props.isFetchingResultsFailed && (
              <div className="text-center data-not-available-msg">
                {tr('Something went wrong. Please try again')}
              </div>
            )}
          </div>
        )}
        {!this.state.isShowNewSearchUi && (
          <div id="ecl-search">
            {!loading && (
              <ReactCSSTransitionGroup
                transitionName="alert"
                transitionAppear={true}
                transitionAppearTimeout={300}
                transitionEnter={false}
                transitionLeave={false}
              >
                <div>
                  <div className="row row_big filter-row">
                    <div className="small-3 columns">
                      <span>{tr('Refine your search')}</span>
                    </div>
                    <div
                      className="small-9 columns filter-row__title"
                      role="heading"
                      aria-level="3"
                    >
                      <span className="lighter">{tr('About')} </span>
                      <strong>{this.props.searchTotalCount}</strong>
                      <span className="lighter"> {tr('results for')} "</span>
                      <strong>{q}</strong>
                      <span className="lighter">"</span>
                      <div className="chip-container" style={this.styles.chipContainer}>
                        {AllFiltersArry.length > 0 &&
                          AllFiltersArry.map(selectedFilter => {
                            return (
                              <Chip
                                style={this.styles.chip}
                                labelStyle={this.styles.chipLabel}
                                backgroundColor={'#fff'}
                              >
                                <small style={this.styles.capitalizeText}>
                                  {selectedFilter.filter === 'content' &&
                                  selectedFilter.type === 'content_type'
                                    ? tr('SmartCards')
                                    : selectedFilter.filter === 'user' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('People')
                                    : selectedFilter.filter === 'pathway' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('Pathways')
                                    : selectedFilter.filter === 'channel' &&
                                      selectedFilter.type === 'content_type'
                                    ? tr('Channels')
                                    : selectedFilter.sourceDisplayName
                                    ? selectedFilter.sourceDisplayName
                                    : selectedFilter.type === 'rating'
                                    ? `${tr('Rating')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'year' &&
                                      (selectedFilter.filter === '30' ||
                                        selectedFilter.filter === '90')
                                    ? tr(`Last ${selectedFilter.filter} Days`)
                                    : selectedFilter.type === 'posted_by'
                                    ? `${tr('Posted by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'liked_by'
                                    ? `${tr('Liked by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'commented_by'
                                    ? `${tr('Commented by')} - ${selectedFilter.filter}`
                                    : selectedFilter.type === 'bookmarked_by'
                                    ? `${tr('Bookmarked by')} - ${selectedFilter.filter}`
                                    : selectedFilter.filter.replace('_', ' ')}{' '}
                                </small>
                                <IconButton
                                  className="cancel-icon delete"
                                  ref={'cancelIcon'}
                                  tooltip={tr('Remove Filter')}
                                  disableTouchRipple
                                  tooltipPosition="top-center"
                                  style={this.styles.closeButton}
                                  onTouchTap={e =>
                                    this.handleFilterChange(
                                      selectedFilter.filter,
                                      false,
                                      selectedFilter.type
                                    )
                                  }
                                >
                                  <Close style={this.styles.chipClose} />
                                </IconButton>
                              </Chip>
                            );
                          })}
                        {AllFiltersArry.length > 0 ? (
                          <a
                            href="#"
                            style={this.styles.clearAllLink}
                            onClick={() => this.clearAllFilters()}
                          >
                            {tr('Clear all')}
                          </a>
                        ) : (
                          ''
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row row_big">
                    <div className="small-3 columns">
                      <TypeFilterContainer
                        filters={typeFilters}
                        contentCount={this.props.contentSectionCount}
                        handleFilterChange={this.handleFilterChange}
                        contentChecked={this.props.contentChecked}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />

                      {this.state.sourceFilters.length > 0 && (
                        <SourcesFilterContainer
                          filters={startCasedSourceFilters}
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                          uncheckFilter={this.state.uncheckFilter}
                        />
                      )}
                      {this.state.providerFilters.length > 0 && (
                        <ProvidersFilterContainer
                          filters={startCasedProviderFilters}
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                        />
                      )}
                      {this.state.languageFilters.length > 0 && (
                        <LanguagesFilterContainer
                          filters={startCasedLanguageFilters}
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                        />
                      )}
                      {this.state.subjectFilters.length > 0 && (
                        <SubjectsFilterContainer
                          filters={startCasedSubjectFilters}
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                        />
                      )}
                      {this.showBIA && (
                        <LevelFilterContainer
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                          uncheckFilter={this.state.uncheckFilter}
                        />
                      )}
                      {Permissions.has('CAN_RATE') && (
                        <RatingFilterContainer
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                          uncheckFilter={this.state.uncheckFilter}
                        />
                      )}
                      {this.state.showPriceFilterUi && this.state.edcastPlansForPricing && (
                        <PriceFilterContainerPricePlan
                          filters={paidContentFilters}
                          handleFilterChange={this.handleFilterChange}
                          paidContentChecked={this.props.paidContentChecked}
                          hideFilters={this.state.hideFilters}
                          isNewStyle={this.state.isShowNewSearchUi}
                          uncheckFilter={this.state.uncheckFilter}
                        />
                      )}
                      {this.state.showPriceFilterUi && !this.state.edcastPlansForPricing && (
                        <PriceFilterContainerFreePaid
                          handleFilterChange={this.handleFilterChange}
                          hideFilters={this.state.hideFilters}
                          isNewStyle={this.state.isShowNewSearchUi}
                          uncheckFilter={this.state.uncheckFilter}
                        />
                      )}
                      <YearFilterContainer
                        handleFilterChange={this.handleFilterChange}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                      <ActionByFilterComponent
                        handleFilterChange={this.handleFilterChange}
                        type={'posted_by'}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                      <ActionByFilterComponent
                        handleFilterChange={this.handleFilterChange}
                        type={'liked_by'}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                      <ActionByFilterComponent
                        handleFilterChange={this.handleFilterChange}
                        type={'commented_by'}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                      <ActionByFilterComponent
                        handleFilterChange={this.handleFilterChange}
                        type={'bookmarked_by'}
                        hideFilters={this.state.hideFilters}
                        uncheckFilter={this.state.uncheckFilter}
                      />
                    </div>
                    <div className="small-9 columns">
                      {this.props.contentSectionCount > 0 && (
                        <SearchContentSection
                          cards={this.props.cards}
                          cardUpdated={this.cardUpdated}
                          contentSectionCount={this.props.contentSectionCount}
                          loadMore={this.loadMore}
                          label={this.state.contentLoading ? 'Loading...' : 'content'}
                        />
                      )}

                      {this.props.pathwaysCount > 0 && (
                        <SearchContentSection
                          cards={this.props.pathways}
                          contentSectionCount={this.props.pathwaysCount}
                          loadMore={this.loadMore}
                          label={this.state.pathwayLoading ? 'Loading...' : 'pathway'}
                        />
                      )}
                      {this.props.journeysCount > 0 && (
                        <SearchContentSection
                          cards={this.props.journeys}
                          contentSectionCount={this.props.journeysCount}
                          loadMore={this.loadMore}
                          label={this.state.journeyLoading ? 'Loading...' : 'pathway'}
                        />
                      )}
                      {this.props.channelCount > 0 && (
                        <SearchChannelSection
                          channels={this.props.channels}
                          channelCount={this.props.channelCount}
                          loadMore={this.loadMore}
                          loading={this.state.channelLoading}
                        />
                      )}
                      {this.props.peopleCount > 0 && (
                        <SearchPeopleSection
                          users={this.props.users}
                          peopleCount={this.props.peopleCount}
                          loadMore={this.loadMore}
                          loading={this.state.peopleLoading}
                        />
                      )}
                      {(this.props.contentSectionCount ||
                        this.props.channelCount ||
                        this.props.peopleCount ||
                        this.props.pathwaysCount ||
                        this.props.journeysCount) === 0 && (
                        <div className="text-center data-not-available-msg">
                          {tr('No results found')}.
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </ReactCSSTransitionGroup>
            )}
            {loading && (
              <div className="search-spinner">
                <Spinner />
              </div>
            )}
            {!loading && !this.props.isFetchingResultsFailed && this.props.totalCount === 0 && (
              <div className="text-center data-not-available-msg">
                {tr("Sorry! We couldn't find what you're looking for.")}.
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

// These are the default values of this component's props
SearchContainer.defaultProps = {
  cards: [],
  channels: [],
  users: [],
  contentSectionCount: 0,
  channelCount: 0,
  peopleCount: 0,
  totalCount: 0,
  searchTotalCount: 0,
  loading: true,
  isFetchingResultsFailed: false,
  contentChecked: false,
  paidContentChecked: false
};

// defining thr proptypes for this component
SearchContainer.propTypes = {
  typeFilters: PropTypes.array,
  paidContentFilters: PropTypes.array,
  pathways: PropTypes.array,
  journeys: PropTypes.array,
  sourceFilters: PropTypes.array,
  location: PropTypes.any,
  levelFilters: PropTypes.any,
  freeContentFilter: PropTypes.any,
  ratingFilters: PropTypes.any,
  currencyFilters: PropTypes.any,
  amountRangeFilters: PropTypes.any,
  isPaidFilters: PropTypes.any,
  postedByFilters: PropTypes.any,
  likedByFilters: PropTypes.any,
  providerFilters: PropTypes.any,
  languageFilters: PropTypes.any,
  subjectFilters: PropTypes.any,
  commentedByFilters: PropTypes.any,
  bookmarkedByFilters: PropTypes.any,
  cards: PropTypes.array,
  channels: PropTypes.array,
  users: PropTypes.array,
  contentSectionCount: PropTypes.number,
  pathwaysCount: PropTypes.number,
  journeysCount: PropTypes.number,
  channelCount: PropTypes.number,
  peopleCount: PropTypes.number,
  totalCount: PropTypes.number,
  searchTotalCount: PropTypes.number,
  loading: PropTypes.bool,
  hideFilters: PropTypes.bool,
  shouldResetInline: PropTypes.bool,
  contentChecked: PropTypes.bool,
  paidContentChecked: PropTypes.bool,
  team: PropTypes.object,
  isFetchingResultsFailed: PropTypes.bool,
  selectedSourceFilters: PropTypes.array,
  selectedSubjectFilters: PropTypes.array,
  selectedLanguageFilters: PropTypes.array,
  selectedProviderFilters: PropTypes.array
};

// function to map the component state to its prop values
function mapStoreStateToProps(state) {
  return Object.assign(
    {},
    state.search.toJS(),
    {
      team: state.team.toJS(),
      results: state.results.toJS()
    },
    {
      immutableSearch: state.search,
      immutableTeam: state.team,
      immutableResults: state.results
    }
  );
}

// exporting the component map the it's state to its prop values
export default connect(mapStoreStateToProps)(SearchContainer);
