import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import { tr } from 'edc-web-sdk/helpers/translations';
import { HardwareKeyboardArrowDown, HardwareKeyboardArrowUp } from 'material-ui/svg-icons';

class PriceFilterContainerPricePlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: true,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxInner: {
        width: '15px',
        height: '15px',
        marginBottom: '10px',
        marginTop: '3px',
        marginRight: '10px',
        fill: '#6f708b'
      },
      checkboxInnerLabel: {
        fontSize: '12px',
        fontWeight: '300',
        color: '#6f708b',
        marginTop: '1px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '1.5px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      }
    };

    if (this.props.isNewStyle) {
      this.styles = {
        checkboxInner: {
          width: '13px',
          height: '13px',
          marginRight: '4px'
        },
        checkboxInnerLabel: {
          lineHeight: '2.17',
          fontSize: '12px',
          fontWeight: '300',
          color: '#6f708b',
          marginTop: '1px'
        },
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        checkboxIconStyle: {
          width: '19px',
          height: '19px'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        },
        uncheckedIcon_inner: {
          width: '13px',
          height: '13px'
        }
      };
    }
  }

  handleChange = (e, isInputChecked, type) => {
    this.props.handleFilterChange(e.target.value, isInputChecked, type);
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  render() {
    let PlanFilters = this.props.filters;
    let uncheckFilter = this.props.uncheckFilter;
    return (
      <div className="new-search__paper-container">
        {this.props.isNewStyle && (
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title">{tr('Price')}</div>
              {this.state.genpactUI && (
                <div className="pointer" onClick={this.showHideFilter}>
                  {this.state.showMenu ? (
                    <HardwareKeyboardArrowDown />
                  ) : (
                    <HardwareKeyboardArrowUp />
                  )}
                </div>
              )}
            </div>
            {PlanFilters && this.state.showMenu && (
              <div>
                <div className="outer-checkbox">
                  <Checkbox
                    label={tr('Free')}
                    iconStyle={this.styles.checkboxOuter}
                    labelStyle={this.styles.checkboxOuterLabel}
                    checkedIcon={<CheckOn1 color="#6f708b" />}
                    uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                    onCheck={(event, isInputChecked) =>
                      this.handleChange(event, isInputChecked, 'plan')
                    }
                    value={'free'}
                    {...(!!uncheckFilter &&
                    (uncheckFilter.clearAll === true ||
                      (uncheckFilter.filter !== false &&
                        (uncheckFilter.filter === 'free' && uncheckFilter.type === 'plan')))
                      ? { checked: false }
                      : {})}
                  />
                </div>

                <div className="outer-checkbox">
                  <Checkbox
                    style={this.styles.checkbox}
                    label={tr('Paid Content')}
                    iconStyle={this.styles.checkboxOuter}
                    labelStyle={this.styles.checkboxOuterLabel}
                    checkedIcon={<CheckOn1 color="#6f708b" />}
                    uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                    onCheck={(event, isInputChecked) =>
                      this.handleChange(event, isInputChecked, 'plan')
                    }
                    value={'paid_content'}
                    checked={!!this.props.paidContentChecked}
                    {...(!!uncheckFilter &&
                    (uncheckFilter.clearAll === true ||
                      (uncheckFilter.filter !== false &&
                        (uncheckFilter.filter === 'paid_content' && uncheckFilter.type === 'plan')))
                      ? { checked: false }
                      : {})}
                  />
                </div>

                {PlanFilters &&
                  PlanFilters.map((PlanFilter, index) => {
                    return (
                      <div className="inner-checkbox" key={index}>
                        <Checkbox
                          style={this.styles.checkbox}
                          label={tr(PlanFilter.display_name)}
                          iconStyle={this.styles.checkboxInner}
                          labelStyle={this.styles.checkboxInnerLabel}
                          checkedIcon={<CheckOn1 color="#6f708b" />}
                          uncheckedIcon={
                            <CheckOff style={this.styles.uncheckedIcon_inner} color="#6f708b" />
                          }
                          onCheck={(event, isInputChecked) =>
                            this.handleChange(event, isInputChecked, 'plan')
                          }
                          value={`${PlanFilter.display_name.toLowerCase().replace(' ', '_')}`}
                          checked={PlanFilter.checked ? PlanFilter.checked : false}
                          {...(!!uncheckFilter &&
                          (uncheckFilter.clearAll === true ||
                            (uncheckFilter.filter !== false &&
                              (uncheckFilter.filter ===
                                `${PlanFilter.display_name.toLowerCase().replace(' ', '_')}` &&
                                uncheckFilter.type === 'plan')))
                            ? { checked: false }
                            : {})}
                        />
                      </div>
                    );
                  })}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
        {!this.props.isNewStyle && (
          <Paper style={this.styles.paperStyle}>
            <h6 className="filter-title">{tr('Price')}</h6>
            {PlanFilters && (
              <div>
                {PlanFilters.hasOwnProperty('Free') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Free')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'plan')
                      }
                      value={'pathway'}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'pathway' && uncheckFilter.type === 'plan')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}
                {PlanFilters.hasOwnProperty('PaidContent') && (
                  <div className="outer-checkbox">
                    <Checkbox
                      label={tr('Paid Content')}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      onCheck={(event, isInputChecked) =>
                        this.handleChange(event, isInputChecked, 'plan')
                      }
                      value={'content'}
                      checked={!!this.props.paidContentChecked}
                      {...(!!uncheckFilter &&
                      (uncheckFilter.clearAll === true ||
                        (uncheckFilter.filter !== false &&
                          (uncheckFilter.filter === 'content' && uncheckFilter.type === 'plan')))
                        ? { checked: false }
                        : {})}
                    />
                  </div>
                )}

                {PlanFilters &&
                  PlanFilters.map((planFilter, index) => {
                    return (
                      <div className="inner-checkbox" key={index}>
                        <Checkbox
                          label={tr(planFilter.display_name)}
                          iconStyle={this.styles.checkboxInner}
                          labelStyle={this.styles.checkboxInnerLabel}
                          onCheck={(event, isInputChecked) =>
                            this.handleChange(event, isInputChecked, 'plan')
                          }
                          value={`${planFilter &&
                            planFilter.display_name.toLowerCase().replace(' ', '_')}`}
                          checked={planFilter.checked ? planFilter.checked : false}
                          {...(!!uncheckFilter &&
                          (uncheckFilter.clearAll === true ||
                            (uncheckFilter.filter !== false &&
                              (uncheckFilter.filter ===
                                `${planFilter &&
                                  planFilter.display_name.toLowerCase().replace(' ', '_')}` &&
                                uncheckFilter.type === 'plan')))
                            ? { checked: false }
                            : {})}
                        />
                      </div>
                    );
                  })}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
      </div>
    );
  }
}

PriceFilterContainerPricePlan.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  filters: PropTypes.object,
  paidContentChecked: PropTypes.any,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any
};

export default PriceFilterContainerPricePlan;
