import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { orgSearch, publicSearch, getSearchAggregations } from 'edc-web-sdk/requests/search';
import { getAllSources, updateSource } from 'edc-web-sdk/requests/ecl';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import SearchCard from './SearchCard';
import * as upshotActions from '../../actions/upshotActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import { CamelCase } from '../../utils/textTransformations';
import Card from '../cards/Card';

import { searchForUsers } from 'edc-web-sdk/requests/users.v2';
import { getSearchChannels } from 'edc-web-sdk/requests/channels.v2';
import { searchGroups } from 'edc-web-sdk/requests/groups.v2';

import SearchPeopleSection from './SearchPeopleSection.jsx';
import SearchChannelSection from './SearchChannelSection.jsx';
import GroupItem from '../team/groups/GroupItem';

import * as languages from '../../constants/languages';

class SearchContainerV2 extends Component {
  static contextTypes = {
    router: React.PropTypes.object
  };

  constructor(props, context) {
    super(props, context);

    this.disableUserSearch = false;
    if (this.props.team && this.props.team.config && this.props.team.config.disable_user_search) {
      this.disableUserSearch = true;
    }
    // Set initial state as well as merge in anything from history
    this.defaultState = {
      orgCards: [],
      publicCards: [],
      showViewMoreOrg: false,
      showViewMorePublic: false,
      totalCountOrg: 0,
      totalCountPublic: 0,
      isOrgCardsLoading: false,
      isPublicCardsLoading: false,
      viewMoreTextOrg: 'View More',
      viewMoreTextPublic: 'View More',
      viewMoreLoadingOrg: false,
      viewMoreLoadingPublic: false,
      candidates: localStorage.getItem('candidates'),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      isDefaultSourceEnabled:
        localStorage.getItem('isDefaultSourceEnabled') === 'true' ? true : false,
      offsetPublic: 0,
      contentBuckets: [],
      sourceBuckets: [],
      searchUsers: [],
      searchGroups: [],
      searchChannels: [],
      showUsers: !this.disableUserSearch,
      showGroups: true,
      showChannels: true,
      contentBucketsSelected: [],
      sourceBucketsSelected: [],
      orgAggregations: {},
      showInternalContent: true,
      showOpenContent: true,
      totalContentCount: null,
      enableSearchOptions: window.ldclient.variation('smartsearch-enable-search-options', false),
      enableSearchOptionLanguage: window.ldclient.variation(
        'smartsearch-enable-language-option',
        false
      ),
      enabledLanguages: localStorage.getItem('enabledLanguages')
        ? localStorage.getItem('enabledLanguages').split(',')
        : [],
      enablePlanFilter: window.ldclient.variation('smartsearch-enable-plan-filter', false),
      enableYearFilter: window.ldclient.variation('smartsearch-enable-year-filter', false),
      enableLevelFilter: window.ldclient.variation('smartsearch-enable-level-option', false),
      enableRatingsFilter: window.ldclient.variation('smartsearch-enable-ratings-filter', false),
      suggestedTerm: null
    };

    this.languages = languages.langs;

    // If location.state, then we're coming back to page
    // Skip API calls and load with this information
    this.state = { ...this.defaultState, ...this.props.location.state };

    this.styles = {
      heading: {
        color: '#555555',
        margin: '5px 0px 10px'
      },
      viewMore: {
        marginTop: '10px',
        marginBottom: '20px'
      },
      noResult: {
        width: '400px',
        height: '400px',
        margin: '0px auto',
        display: 'block'
      },
      noResultText: {
        textAlign: 'center',
        marginTop: '-100px',
        fontSize: '30px',
        fontWeight: 200,
        color: '#b0b1c3'
      }
    };
  }

  // Record search results to history to no repeat on back button
  recordToHistory = () => {
    // TODO: Warning has come up that object is too large. Leaving in case more debugging is needed
    // let bytes = encodeURI(JSON.stringify(this.state)).split(/%..|./).length - 1;
    // console.log(bytes);
    this.context.router.replace({
      pathname: document.location.pathname,
      state: this.state,
      search: document.location.search
    });
  };

  componentDidMount() {
    if (this.state.orgCards.length === 0 && this.state.publicCards.length === 0) {
      let payload = {
        candidates: this.state.candidates,
        limit: 5,
        search_term: this.props.location.query.q
      };
      this.loadResults(payload);
    }

    // Search other items
    this.searchUsersGroupsChannels(this.props.location.query.q);
  }

  componentDidUpdate(prevProps) {
    let payload = {
      candidates: this.state.candidates,
      limit: 5,
      search_term: this.props.location.query.q
    };
    if (this.props.location.query.q != prevProps.location.query.q) {
      this.setState(this.defaultState, () => {
        this.loadResults(payload);
      });
    }
  }

  fixCardKeys = cards => {
    return cards.map(card => {
      card.cardType = card.content_type === 'pathway' ? 'pack' : card.content_type;
      card.isPublic = true;
      // Make sure card.title is a String
      card.title = card.title || '';

      card.eclSourceTypeName = card.source_type_name;

      // Update with additional information
      if (card.additional_metadata) {
        if (card.additional_metadata.cpe_credits) {
          card.eclSourceCpeCredits = card.additional_metadata.cpe_credits;
        }
        if (card.additional_metadata.cpe_subject) {
          card.eclSourceCpeSubArea = card.additional_metadata.cpe_subject;
        }
      }
      return card;
    });
  };

  loadOrgResults = (payload, updateAggregations = true) => {
    /*eslint no-param-reassign: "off"*/
    if (!payload) {
      payload = {
        candidates: this.state.candidates,
        limit: 5,
        search_term: this.props.location.query.q,
        aggs_enabled: true
      };
    }

    payload = this.setCommonSearchFilters(payload);

    orgSearch({
      ...payload,
      source_id: this.state.sourceBucketsSelected,
      content_type: this.state.contentBucketsSelected
    })
      .then(data => {
        let orgCards = this.fixCardKeys(data.cards);
        let totalCountOrg = data.total;
        let showViewMoreOrg = totalCountOrg > orgCards.length;
        let isOrgCardsLoading = false;
        let orgAggregations = updateAggregations ? {} : this.state.orgAggregations;
        let suggestedTerm = data.suggestion ? data.suggestion : null;

        // If there is an issue providing back aggregations
        // We also do not update aggregations when the source is checkmarked, only when content is checkmarked
        if (updateAggregations) {
          try {
            data.aggregations.source_id.source_id.buckets.forEach(o => {
              orgAggregations[o.key] = o.doc_count;
            });
          } catch (err) {
            console.error(
              `Error in SearchContainerV2.componentDidUpdate.orgSearch.Aggregations.func: ${err}`
            );
          }
        }

        this.setState(
          {
            orgCards,
            totalCountOrg,
            showViewMoreOrg,
            isOrgCardsLoading,
            orgAggregations,
            suggestedTerm
          },
          () => this.recordToHistory()
        );
      })
      .catch(err => {
        this.setState({ isOrgCardsLoading: false });
        console.error(`Error in SearchContainerV2.componentDidUpdate.orgSearch.func: ${err}`);
      });
  };

  searchSuggestedTerm = () => {
    let payload = {
      candidates: this.state.candidates,
      limit: 5,
      search_term: this.state.suggestedTerm
    };
    window.history.pushState(null, null, `/smartsearch?q=${this.state.suggestedTerm}`);
    this.loadResults(payload);
    this.setState({ suggestedTerm: null });
  };

  loadContentResults = payload => {
    /*eslint no-param-reassign: "off"*/
    if (!payload) {
      payload = {
        candidates: this.state.candidates,
        limit: 5,
        search_term: this.props.location.query.q
      };
    }

    payload = this.setCommonSearchFilters(payload);

    let isDefaultSourceEnabled = this.state.isDefaultSourceEnabled;
    this.setState({ isPublicCardsLoading: isDefaultSourceEnabled });
    isDefaultSourceEnabled &&
      publicSearch({ ...payload, content_type: this.state.contentBucketsSelected })
        .then(data => {
          let publicCards = this.fixCardKeys(data.cards);
          let totalCountPublic = data.total;
          let showViewMorePublic = totalCountPublic > publicCards.length;
          let isPublicCardsLoading = false;
          let offsetPublic = data.next_offset;
          this.setState(
            {
              publicCards,
              totalCountPublic,
              showViewMorePublic,
              isPublicCardsLoading,
              offsetPublic
            },
            () => this.recordToHistory()
          );
        })
        .catch(err => {
          this.setState({ isPublicCardsLoading: false });
          console.error(`Error in SearchContainerV2.componentDidUpdate.publicSearch.func: ${err}`);
        });
  };

  setCommonSearchFilters = payload => {
    if (this.state.enableSearchOptionLanguage) {
      var languagesToSearch = this.state.enabledLanguages.map(l => this.languages[l]);
      payload['language'] = languagesToSearch;
    }

    if (
      this.state.enablePlanFilter &&
      this.state.planFilterOptions &&
      this.state.planFilterOptions.length > 0
    ) {
      payload['plan'] = this.state.planFilterOptions.map(p => p.toLowerCase());
    }

    if (
      this.state.enableLevelFilter &&
      this.state.levelFilterOptions &&
      this.state.levelFilterOptions.length > 0
    ) {
      payload['level'] = this.state.levelFilterOptions.map(l => l.toLowerCase());
    }

    if (this.state.enableYearFilter && (this.state.filterYearFrom || this.state.filterYearTo)) {
      let startYear = this.state.filterYearFrom || new Date().getFullYear() - 50;
      let endYear = this.state.filterYearTo || new Date().getFullYear();
      endYear++;
      payload['from_date'] = new Date(startYear.toString()).toISOString();
      payload['till_date'] = new Date(endYear.toString()).toISOString();
    }

    if (
      this.state.enableRatingsFilter &&
      this.state.ratingsFilterOption &&
      this.state.ratingsFilterOption.length > 0
    ) {
      payload['rating'] = this.state.ratingsFilterOption;
    }

    return payload;
  };

  loadResults = async payload => {
    // Set default payload if not specific

    this.setState({ isOrgCardsLoading: true });
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['SEARCH'], {
        keyword: this.props.location.query.q,
        event: 'Search'
      });
    }

    this.loadOrgResults({ ...payload, aggs_enabled: true });
    this.loadContentResults(payload);

    this.searchUsersGroupsChannels(payload.search_term);

    // Get aggregations and source_id names
    let contentBuckets = [];
    let sourceBuckets = [];
    try {
      let aggData = await getSearchAggregations(payload);

      contentBuckets = aggData.aggregations.content_type.content_type.buckets;
      sourceBuckets = aggData.aggregations.source_id.source_id.buckets;

      // Make call to get source names
      if (sourceBuckets.length > 0) {
        let source_ids = sourceBuckets.map(bucket => bucket.key);
        let nameData = await getAllSources({ 'source_ids[]': source_ids });
        for (const index in sourceBuckets) {
          try {
            sourceBuckets[index].name = nameData.filter(
              o => o.id === sourceBuckets[index].key
            )[0].display_name;
            sourceBuckets[index].logo_url = nameData.filter(
              o => o.id === sourceBuckets[index].key
            )[0].logo_url;
          } catch (e) {
            sourceBuckets[index].name = 'UGC'; // UGC content doesn't have a display_name
            sourceBuckets[index].logo_url = null;
          }
        }
      }

      // Articles and Videos will always be present in open content
      if (contentBuckets.filter(o => o.key === 'article').length === 0) {
        contentBuckets.unshift({ key: 'article' }); // Make first in array
      }
      if (contentBuckets.filter(o => o.key === 'video').length === 0) {
        contentBuckets.push({ key: 'video' }); // Make last in array
      }

      this.setState(
        {
          contentBuckets,
          sourceBuckets,
          totalContentCount: aggData.aggregations.content_type.doc_count
        },
        () => this.recordToHistory()
      );
    } catch (err) {
      console.error(
        `Error in SearchContainerV2.componentDidUpdate.getSearchAggregations.func: ${err}`
      );
    }
  };

  searchUsersGroupsChannels = term => {
    if (this.state.showUsers) {
      searchForUsers({ q: term })
        .then(data => {
          let result = data.users || [];
          this.setState({ searchUsers: result });
        })
        .catch(err => {
          console.error(`Error in byName SearchContainerV2.searchForUsers.func: ${err}`);
        });
    }

    searchGroups({ q: term })
      .then(data => {
        let result = data.teams;
        this.setState({ searchGroups: result });
      })
      .catch(err => {
        console.error(`Error in SearchContainerV2.searchGroups.func: ${err}`);
      });

    getSearchChannels({ q: term })
      .then(data => {
        let result = data.channels;
        this.setState({ searchChannels: result });
      })
      .catch(err => {
        console.error(`Error in SearchContainerV2.getSearchChannels.func: ${err}`);
      });
  };

  viewMoreClickHandlerOrg = () => {
    this.setState({ viewMoreTextOrg: 'Loading...', viewMoreLoadingOrg: true });
    let offset = this.state.orgCards.length;
    let payload = {
      candidates: this.state.candidates,
      limit: 10,
      offset: offset,
      search_term: this.props.location.query.q
    };
    payload = this.setCommonSearchFilters(payload);
    orgSearch({
      ...payload,
      source_id: this.state.sourceBucketsSelected,
      content_type: this.state.contentBucketsSelected
    })
      .then(data => {
        let orgCards = this.state.orgCards;
        orgCards.push(...this.fixCardKeys(data.cards));
        let showViewMoreOrg = data.total > orgCards.length;
        let viewMoreTextOrg = 'View More';
        let viewMoreLoadingOrg = false;
        this.setState({ orgCards, showViewMoreOrg, viewMoreTextOrg, viewMoreLoadingOrg }, () =>
          this.recordToHistory()
        );
      })
      .catch(err => {
        console.error(`Error in SearchContainerV2.viewMoreClickHandlerOrg.func: ${err}`);
      });
  };

  viewMoreClickHandlerPublic = () => {
    this.setState({ viewMoreTextPublic: 'Loading...', viewMoreLoadingPublic: true });
    let offset = this.state.offsetPublic;
    let payload = {
      limit: 10,
      offset: offset,
      search_term: this.props.location.query.q
    };

    payload = this.setCommonSearchFilters(payload);
    publicSearch({ ...payload, content_type: this.state.contentBucketsSelected })
      .then(data => {
        let publicCards = this.state.publicCards;
        publicCards.push(...this.fixCardKeys(data.cards));
        let showViewMorePublic = data.cards.length !== 0;
        let viewMoreTextPublic = 'View More';
        let viewMoreLoadingPublic = false;
        let offsetPublic = data.next_offset;
        this.setState(
          {
            publicCards,
            showViewMorePublic,
            viewMoreTextPublic,
            viewMoreLoadingPublic,
            offsetPublic
          },
          () => this.recordToHistory()
        );
      })
      .catch(err => {
        console.error(`Error in SearchContainerV2.viewMoreClickHandlerPublic.func: ${err}`);
      });
  };

  handleContentBuckets = e => {
    let updatedContentBuckets = [...this.state.contentBucketsSelected];
    if (!~updatedContentBuckets.indexOf(e.target.value)) {
      // Add new content type
      updatedContentBuckets.push(e.target.value);
    } else {
      // Remove content type
      updatedContentBuckets.splice(updatedContentBuckets.indexOf(e.target.value), 1);
    }
    this.setState({ contentBucketsSelected: updatedContentBuckets }, () => {
      this.loadContentResults();
      this.loadOrgResults();
    });
  };

  handleSourceBuckets = e => {
    let updatedSourceBuckets = [...this.state.sourceBucketsSelected];
    if (!~updatedSourceBuckets.indexOf(e.target.value)) {
      // Add new source
      updatedSourceBuckets.push(e.target.value);
    } else {
      // Remove source
      updatedSourceBuckets.splice(updatedSourceBuckets.indexOf(e.target.value), 1);
    }
    this.setState({ sourceBucketsSelected: updatedSourceBuckets }, () =>
      this.loadOrgResults(null, false)
    );
  };

  getProviderLogo = card => {
    let providerLogo = null;
    if (card && card.source_id) {
      let logoHolder = this.state.sourceBuckets.filter(o => o.key === card.source_id)[0];
      if (logoHolder) {
        providerLogo = logoHolder.logo_url;
      }
    }
    return providerLogo;
  };

  handleEnabledLanguages = e => {
    let language = e.target.value;
    let enabledLanguages = this.state.enabledLanguages;
    if (enabledLanguages.indexOf(language) === -1) {
      enabledLanguages.push(language);
    } else {
      enabledLanguages = [...enabledLanguages.filter(l => l !== language)];
    }
    this.setState({ enabledLanguages }, () => {
      this.loadContentResults();
      this.loadOrgResults();
      this.defaultState.enabledLanguages = enabledLanguages;
      window.localStorage.setItem('enabledLanguages', enabledLanguages);
    });
  };

  /**
   * if value is 'stateField', we're updating a value, not an array
   */
  handleFilterOptions = (value, stateKey, e) => {
    let val = value;
    if (value === 'stateField') {
      val = e.target.value;
    }

    let stateArray = this.state[stateKey] || [];
    if (value !== 'stateField') {
      if (stateArray.indexOf(val) === -1) {
        stateArray.push(val);
      } else {
        stateArray = [...stateArray.filter(s => s !== val)];
      }
    }

    if (value === 'stateField') {
      stateArray = val;
    }
    this.setState({ [stateKey]: stateArray }, () => {
      this.loadContentResults();
      this.loadOrgResults();
    });
  };

  getDuration = secs => {
    if (!secs) {
      return null;
    }
    let duration = '';
    let hrs = Math.floor(secs / 3600);
    let mins = Math.floor((secs % 3600) / 60);
    if (hrs && mins) {
      duration = `${hrs} hr${hrs > 1 ? 's' : ''} ${mins} min${mins > 1 ? 's' : ''}`;
    } else if (!hrs) {
      duration = `${mins} min${mins > 1 ? 's' : ''}`;
    } else {
      duration = `${hrs} hr${hrs > 1 ? 's' : ''}`;
    }
    // Return based on expected metadata
    return {
      calculated_duration: secs,
      calculated_duration_display: duration
    };
  };

  render() {
    let orgCards = this.state.orgCards || [];
    let publicCards = this.state.publicCards || [];
    let containerCSS = window.ldclient.variation('card-v3', false)
      ? 'five-card-column vertical-spacing-medium'
      : 'search-v2_row';
    let noResults =
      orgCards.length === 0 &&
      publicCards.length === 0 &&
      !this.state.isOrgCardsLoading &&
      !this.state.isPublicCardsLoading;

    let orgArray = ['Users', 'Channels', 'Groups'];
    if (this.disableUserSearch) {
      orgArray = ['Channels', 'Groups'];
    }

    return (
      <div className="search-v2_container">
        <div className="search-v2_left-col">
          <h6>{tr('Refine your search')}</h6>

          {this.state.contentBuckets.length > 0 && (
            <div className="search-v2__bucket">
              <h6>{tr('Content Type')}</h6>
              <div key={`contentBucket-internal`}>
                <input
                  type="checkbox"
                  id={'cbinternal'}
                  value={'internal'}
                  disabled={this.state.orgCards.length === 0 ? true : false}
                  defaultChecked={this.state.showInternalContent}
                  onChange={() =>
                    this.setState({ showInternalContent: !this.state.showInternalContent })
                  }
                />
                <label htmlFor="cbinternal">{tr('Internal')}</label>
              </div>
              {this.state.isDefaultSourceEnabled && (
                <div key={`contentBucket-open`}>
                  <input
                    type="checkbox"
                    id={'cbopen'}
                    value={'open'}
                    disabled={this.state.publicCards.length === 0 ? true : false}
                    defaultChecked={this.state.showOpenContent}
                    onChange={() => this.setState({ showOpenContent: !this.state.showOpenContent })}
                  />
                  <label htmlFor="cbopen">{tr('Content Library')}</label>
                </div>
              )}
              {this.state.contentBuckets.map((bucket, i) => {
                return (
                  <div key={`contentBucket-${i}`}>
                    <input
                      type="checkbox"
                      id={bucket.key}
                      value={bucket.key}
                      disabled={
                        !this.state.showInternalContent &&
                        (bucket.key !== 'article' && bucket.key !== 'video')
                      }
                      defaultChecked={
                        this.state.contentBucketsSelected.indexOf(bucket.key) > -1 ? true : false
                      }
                      onChange={this.handleContentBuckets}
                    />
                    <label htmlFor={bucket.key}>{tr(CamelCase(bucket.key))}</label>
                  </div>
                );
              })}
            </div>
          )}

          {this.state.sourceBuckets.length > 0 && (
            <div className="search-v2__bucket">
              <h6>{tr('Content Source')}</h6>
              {this.state.sourceBuckets.map((bucket, i) => {
                let sourceHasContentType = this.state.orgAggregations[bucket.key] ? false : true;
                let bucketName = bucket.name;
                if (bucketName === 'UGC') {
                  bucketName = tr('User Generated Content');
                }
                return (
                  <div key={`sourceBucket-${i}`}>
                    <input
                      type="checkbox"
                      id={bucket.key}
                      value={bucket.key}
                      disabled={sourceHasContentType}
                      defaultChecked={
                        this.state.sourceBucketsSelected.indexOf(bucket.key) > -1 ? true : false
                      }
                      onChange={this.handleSourceBuckets}
                    />
                    <label htmlFor={bucket.key}>
                      {bucketName} ({bucket.doc_count})
                    </label>
                  </div>
                );
              })}
            </div>
          )}

          <div className="search-v2__bucket">
            <h6>{tr('Organization')}</h6>
            {orgArray.map((tag, i) => {
              return (
                <div key={`search-${i}`}>
                  <input
                    type="checkbox"
                    id={tag}
                    value={tag}
                    disabled={this.state[`search${tag}`].length === 0}
                    defaultChecked={true}
                    onChange={() => {
                      this.setState({ [`show${tag}`]: !this.state[`show${tag}`] });
                    }}
                  />
                  <label htmlFor={tag}>{tr(tag)}</label>
                </div>
              );
            })}
          </div>

          {this.state.enableSearchOptions && (
            <div className="search-v2__bucket">
              <h6>{tr('Options')}</h6>

              {this.state.enableSearchOptionLanguage && (
                <span>
                  <p className="search-option-tag">{tr('Languages')}</p>
                  {this.state.enabledLanguages.map((language, i) => {
                    return (
                      <div key={`${language}-${i}`}>
                        <input
                          type="checkbox"
                          id={language}
                          value={language}
                          defaultChecked={true}
                          onChange={this.handleEnabledLanguages}
                        />
                        <label htmlFor={language}>{language}</label>
                      </div>
                    );
                  })}
                  <div style={{ marginTop: '4px' }}>
                    <select onChange={this.handleEnabledLanguages}>
                      <option value="" default="">
                        {tr('Add another language')}
                      </option>
                      {Object.keys(this.languages)
                        .filter(l => this.state.enabledLanguages.indexOf(l) === -1)
                        .sort()
                        .map((language, i) => {
                          return (
                            <option key={`${language}-${i}`} value={language}>
                              {language}
                            </option>
                          );
                        })}
                    </select>
                  </div>
                </span>
              )}

              {this.state.enableLevelFilter && (
                <span>
                  <p className="search-option-tag">{tr('Level')}</p>
                  {['Beginner', 'Intermediate', 'Advanced'].map((tag, i) => {
                    return (
                      <div key={`${tag}-${i}`}>
                        <input
                          type="checkbox"
                          id={tag}
                          value={tag}
                          defaultChecked={false}
                          onChange={this.handleFilterOptions.bind(this, tag, 'levelFilterOptions')}
                        />
                        <label htmlFor={tag}>{tr(tag)}</label>
                      </div>
                    );
                  })}
                </span>
              )}

              {this.state.enableRatingsFilter && (
                <span>
                  <p className="search-option-tag">{tr('Ratings')}</p>
                  {['5', '4', '3'].map((tag, i) => {
                    return (
                      <div key={`rating-${tag}-${i}`}>
                        <input
                          type="checkbox"
                          id={`rating-${tag}`}
                          value={tag}
                          defaultChecked={false}
                          onChange={this.handleFilterOptions.bind(this, tag, 'ratingsFilterOption')}
                        />
                        <label htmlFor={`rating-${tag}`}>
                          {tr(tag)} {'★'.repeat(parseInt(tag))}
                        </label>
                      </div>
                    );
                  })}
                </span>
              )}

              {this.state.enablePlanFilter && (
                <span>
                  <p className="search-option-tag">{tr('Pricing')}</p>
                  {['Free', 'Paid', 'Premium', 'Subscription'].map((tag, i) => {
                    return (
                      <div key={`${tag}-${i}`}>
                        <input
                          type="checkbox"
                          id={tag}
                          value={tag}
                          defaultChecked={false}
                          onChange={this.handleFilterOptions.bind(this, tag, 'planFilterOptions')}
                        />
                        <label htmlFor={tag}>{tr(tag)}</label>
                      </div>
                    );
                  })}
                </span>
              )}

              {this.state.enableYearFilter && (
                <span>
                  <p className="search-option-tag">{tr('Year Range')}</p>
                  {['filterYearFrom', 'filterYearTo'].map((tag, i) => {
                    let year = new Date().getFullYear();
                    let years = new Array(50).fill(undefined).map((y, ind) => year - ind);
                    return (
                      <div key={`${tag}-${i}`}>
                        <select
                          className="filter-select"
                          onChange={this.handleFilterOptions.bind(this, 'stateField', tag)}
                        >
                          <option value="" default>
                            {tag === 'filterYearFrom' ? tr('From') : tr('To')}
                          </option>
                          {years.map(y => {
                            return <option value={y}>{y}</option>;
                          })}
                        </select>
                      </div>
                    );
                  })}
                </span>
              )}
            </div>
          )}
        </div>
        <div className="search-v2_right-col">
          {noResults && (
            <div>
              {tr('No results for')} <em>"{this.props.location.query.q}"</em>
            </div>
          )}
          {this.state.suggestedTerm && (
            <div>
              <strong>
                {tr('Did you mean: ')}{' '}
                <a onClick={this.searchSuggestedTerm}>{this.state.suggestedTerm}</a>?
              </strong>
            </div>
          )}
          {!noResults && (
            <div>
              {this.state.totalContentCount ? tr('Results for') : tr('Loading results for')}{' '}
              <em>"{this.props.location.query.q}"</em>
            </div>
          )}
          {this.state.showInternalContent && this.state.orgCards.length ? (
            <div style={this.styles.heading}>
              <b>{tr('Internal content')}</b> |{' '}
              <span style={{ fontSize: '12px' }}>
                {tr('Content created by users and from integrations')}
              </span>
            </div>
          ) : null}
          <div className={containerCSS}>
            {this.state.showInternalContent &&
              orgCards &&
              orgCards.map(card => {
                let providerLogo = this.getProviderLogo(card);
                if (card.provider_name) {
                  card.eclSourceTypeName = card.provider_name;
                }
                if (card.duration) {
                  card.eclDurationMetadata = this.getDuration(card.duration);
                }
                return (
                  <Card
                    key={card.id}
                    card={card}
                    author={card.author}
                    dueAt={null}
                    startDate={card.date}
                    userInterest={false}
                    dismissible={false}
                    removeCardFromList={null}
                    type="Tile"
                    providerLogo={providerLogo}
                    moreCards={false}
                    fullView={true}
                    showComment={false}
                    isStandalone={true}
                  />
                );
              })}
          </div>
          {this.state.showInternalContent &&
            this.state.orgCards.length > 0 &&
            this.state.showViewMoreOrg && (
              <div className="small-12 columns text-center" style={this.styles.viewMore}>
                <SecondaryButton
                  label={tr(this.state.viewMoreTextOrg)}
                  onClick={this.viewMoreClickHandlerOrg}
                  disabled={this.state.viewMoreLoadingOrg}
                />
              </div>
            )}
          {this.state.showOpenContent &&
          this.state.publicCards.length &&
          this.state.sourceBucketsSelected.length === 0 ? (
            <div style={this.styles.heading}>
              <b>{tr('Content library')}</b> |{' '}
              <span style={{ fontSize: '12px' }}>{tr('Content from all over the web')}</span>
            </div>
          ) : null}
          <div className={containerCSS}>
            {this.state.showOpenContent &&
              publicCards &&
              this.state.sourceBucketsSelected.length === 0 &&
              publicCards.map(card => {
                let providerLogo = this.getProviderLogo(card);
                if (card.provider_name) {
                  card.eclSourceTypeName = card.provider_name;
                }
                if (card.duration) {
                  card.eclDurationMetadata = this.getDuration(card.duration);
                }
                return (
                  <Card
                    key={card.id}
                    card={card}
                    author={card.author}
                    dueAt={null}
                    startDate={card.date}
                    userInterest={false}
                    dismissible={false}
                    removeCardFromList={null}
                    type="Tile"
                    providerLogo={providerLogo}
                    moreCards={false}
                    fullView={true}
                    showComment={false}
                    isStandalone={true}
                  />
                );
              })}
          </div>
          {this.state.showOpenContent &&
            this.state.publicCards.length > 0 &&
            this.state.showViewMorePublic &&
            this.state.sourceBucketsSelected.length === 0 && (
              <div className="small-12 columns text-center" style={this.styles.viewMore}>
                <SecondaryButton
                  label={tr(this.state.viewMoreTextPublic)}
                  onClick={this.viewMoreClickHandlerPublic}
                  disabled={this.state.viewMoreLoadingPublic}
                />
              </div>
            )}
          {!orgCards.length &&
            !publicCards.length &&
            !this.state.isOrgCardsLoading &&
            !this.state.isPublicCardsLoading && (
              <div>
                <img style={this.styles.noResult} src="/i/images/empty_state_search.png" />
                <div style={this.styles.noResultText}>{tr('No Result Found')}</div>
              </div>
            )}

          {!this.state.showInternalContent && !this.state.showOpenContent && (
            <h5>{tr('Please select Internal or Content Library content type to view results.')}</h5>
          )}

          {!this.state.isDefaultSourceEnabled && !this.state.showInternalContent && (
            <h5>{tr('Please select Internal content type to view results.')}</h5>
          )}

          {this.state.showUsers && this.state.searchUsers.length > 0 && (
            <section className="header-overwrite">
              <div style={this.styles.heading}>
                <b>{tr('Users')}</b>
              </div>
              <SearchPeopleSection
                users={this.state.searchUsers}
                peopleCount={this.state.searchUsers.length}
                loadMore={() => {}}
                loading={false}
                isNewStyle={window.ldclient.variation('new-search-page-ui', false)}
                isLastPage={true}
              />
            </section>
          )}

          {this.state.showChannels && this.state.searchChannels.length > 0 && (
            <section className="header-overwrite">
              <div style={this.styles.heading}>
                <b>{tr('Channels')}</b>
              </div>
              <SearchChannelSection
                channels={this.state.searchChannels}
                channelCount={this.state.searchChannels.length}
                loadMore={() => {}}
                loading={false}
                isNewStyle={window.ldclient.variation('new-search-page-ui', false)}
                isLastPage={true}
              />
            </section>
          )}

          {this.state.showGroups && this.state.searchGroups.length > 0 && (
            <section className="header-overwrite">
              <div style={this.styles.heading}>
                <b>{tr('Groups')}</b>
              </div>
              <div className="custom-card-container">
                <div className={containerCSS}>
                  {this.state.searchGroups.map((group, i) => {
                    return (
                      <GroupItem
                        key={`group-item-${group.id}`}
                        index={i}
                        removeGroupFromArray={null}
                        group={group}
                      />
                    );
                  })}
                </div>
              </div>
            </section>
          )}
        </div>
      </div>
    );
  }
}

SearchContainerV2.propTypes = {
  location: PropTypes.object
};

function mapStoreStateToProps(state) {
  return Object.assign({}, { team: state.team.toJS() });
}

export default connect(mapStoreStateToProps)(SearchContainerV2);
