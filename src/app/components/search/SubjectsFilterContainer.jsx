import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton/FlatButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import {
  HardwareKeyboardArrowDown,
  HardwareKeyboardArrowUp,
  AvSortByAlpha
} from 'material-ui/svg-icons';
import sortBy from 'lodash/sortBy';
import AlphaDown from 'edc-web-sdk/components/icons/AlphaDown';
import AlphaUp from 'edc-web-sdk/components/icons/AlphaUp';
import NumericDown from 'edc-web-sdk/components/icons/NumericDown';
import NumericUp from 'edc-web-sdk/components/icons/NumericUp';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';

class SubjectsFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: true,
      filters: props.filters,
      numbersSort: 'asc',
      namesSort: 'asc',
      genpactUI: window.ldclient.variation('genpact-ui', false),
      showAllView: true
    };
    this.styles = {
      paperStyle: {
        borderRadius: '4px',
        padding: '12px 13px',
        marginBottom: '15px'
      },
      checkboxOuter: {
        width: '18px',
        height: '18px',
        marginRight: '10px',
        marginBottom: '10px',
        marginTop: '2px',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        fontSize: '14px',
        color: '#6f708b'
      },
      searchIcon: {
        width: '15px',
        height: '15px'
      }
    };
    this.noOfSources = 10;
    if (this.props.isNewStyle) {
      this.styles = {
        checkboxOuter: {
          width: '18px',
          height: '18px',
          marginRight: '8px',
          marginBottom: '4px'
        },
        checkboxOuterLabel: {
          padding: '6px 0',
          lineHeight: '1.4'
        },
        uncheckedIcon: {
          width: '18px',
          height: '18px'
        },
        sortIconStyle: {
          width: '30px',
          height: '17px'
        }
      };
    }
  }

  componentDidMount() {
    let defaultSort = sortBy(this.state.filters, ['count', 'display_name']);
    this.setState({
      filters: defaultSort,
      showAllView: this.state.filters.length > this.noOfSources
    });
  }

  handleChange = (e, isInputChecked, type, displayName) => {
    this.props.handleFilterChange(e.target.value, isInputChecked, type, null, displayName);
  };

  showHideFilter = () => {
    this.setState({
      showMenu: !this.state.showMenu
    });
  };

  sortByNumber = sort => {
    this.setState({
      numbersSort: sort
    });
    let numbersSort = [];
    if (sort == 'asc') {
      numbersSort = sortBy(this.state.filters, ['count', 'display_name']);
    } else {
      numbersSort = sortBy(this.state.filters, ['count', 'display_name']).reverse();
    }
    this.setState({
      filters: numbersSort
    });
  };

  sortByName = sort => {
    this.setState({
      namesSort: sort
    });
    let namesSort = [];
    if (sort == 'asc') {
      namesSort = sortBy(this.state.filters, ['display_name', 'count']);
    } else {
      namesSort = sortBy(this.state.filters, ['display_name', 'count']).reverse();
    }
    this.setState({
      filters: namesSort
    });
  };

  _handleSearch = e => {
    let searchedFilters = this.props.filters.filter(filter => {
      return filter.display_name.toLowerCase().includes(e.target.value.toLowerCase());
    });
    this.setState({
      filters: searchedFilters,
      showAllView: searchedFilters.length > this.noOfSources
    });
  };

  setViewAllVisibility = () => {
    this.setState({ showAllView: false });
  };

  render() {
    let filters = this.state.filters;
    let uncheckFilter = this.props.uncheckFilter;
    let selectedSubjectFilters = this.props.selectedSubjectFilters || [];
    return (
      <div className="new-search__paper-container sources-filter-container">
        {this.props.isNewStyle && (
          <Paper className="new-search__paper">
            <div className="flex-space-between">
              <div className="filter-title">{tr('Subjects')}</div>
              {this.state.genpactUI && (
                <div style={{ display: 'flex' }}>
                  <div>
                    {this.state.numbersSort == 'desc' && (
                      <NumericDown
                        style={this.styles.sortIconStyle}
                        onClick={() => {
                          this.sortByNumber('asc');
                        }}
                      />
                    )}
                    {this.state.numbersSort == 'asc' && (
                      <NumericUp
                        style={this.styles.sortIconStyle}
                        onClick={() => {
                          this.sortByNumber('desc');
                        }}
                      />
                    )}
                    {this.state.namesSort == 'desc' && (
                      <AlphaDown
                        style={this.styles.sortIconStyle}
                        onClick={() => {
                          this.sortByName('asc');
                        }}
                      />
                    )}
                    {this.state.namesSort == 'asc' && (
                      <AlphaUp
                        style={this.styles.sortIconStyle}
                        onClick={() => {
                          this.sortByName('desc');
                        }}
                      />
                    )}
                  </div>
                  <div className="pointer" onClick={this.showHideFilter}>
                    {this.state.showMenu ? (
                      <HardwareKeyboardArrowDown />
                    ) : (
                      <HardwareKeyboardArrowUp />
                    )}
                  </div>
                </div>
              )}
            </div>

            {filters && this.state.showMenu && (
              <div>
                {this.state.genpactUI && (
                  <div className="relative" style={{ marginBottom: '10px' }}>
                    <input
                      className="bootstrap-typeahead-input-main search-source-input"
                      placeholder={tr('Search by source')}
                      onChange={this._handleSearch}
                    />
                    <button className="search-button" type="submit" onClick={this.handleRedirect}>
                      <SearchIcon style={this.styles.searchIcon} />
                    </button>
                  </div>
                )}

                <div
                  style={
                    this.state.showAllView
                      ? { overflow: 'hidden', height: `${this.noOfSources * 33.5}px` }
                      : { overflow: 'unset', height: '100%' }
                  }
                >
                  {filters.map((sourceFilter, index) => {
                    return (
                      <div className="outer-checkbox" key={index}>
                        <Checkbox
                          label={`${sourceFilter.display_name} (${sourceFilter.count})`}
                          iconStyle={this.styles.checkboxOuter}
                          labelStyle={this.styles.checkboxOuterLabel}
                          checkedIcon={<CheckOn1 color="#6f708b" />}
                          uncheckedIcon={
                            <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                          }
                          onCheck={(event, isInputChecked) =>
                            this.handleChange(
                              event,
                              isInputChecked,
                              'subjects',
                              sourceFilter.display_name
                            )
                          }
                          value={`${sourceFilter.id}`}
                          {...(!!uncheckFilter &&
                          (uncheckFilter.clearAll === true ||
                            (uncheckFilter.filter !== false &&
                              (uncheckFilter.filter === `${sourceFilter.id}` &&
                                uncheckFilter.type === 'subjects')))
                            ? { checked: false }
                            : {})}
                          checked={!!~selectedSubjectFilters.indexOf(sourceFilter.id)}
                        />
                      </div>
                    );
                  })}
                </div>
                {this.state.showAllView && (
                  <div style={{ textAlign: 'right' }}>
                    <span
                      style={{ color: '#4a90e2', fontSize: '12px', cursor: 'pointer' }}
                      onClick={this.setViewAllVisibility}
                    >
                      {tr('View More')}
                    </span>
                  </div>
                )}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
        {!this.props.isNewStyle && (
          <Paper style={this.styles.paperStyle}>
            <h6 className="filter-title">{tr('Subjects')}</h6>
            {filters && (
              <div>
                {filters.map((sourceFilter, index) => {
                  return (
                    <div className="outer-checkbox" key={index}>
                      <Checkbox
                        label={`${sourceFilter.display_name} (${sourceFilter.count})`}
                        iconStyle={this.styles.checkboxOuter}
                        labelStyle={this.styles.checkboxOuterLabel}
                        onCheck={(event, isInputChecked) =>
                          this.handleChange(
                            event,
                            isInputChecked,
                            'subjects',
                            sourceFilter.display_name
                          )
                        }
                        value={`${sourceFilter.id}`}
                        {...(!!uncheckFilter &&
                        (uncheckFilter.clearAll === true ||
                          (uncheckFilter.filter !== false &&
                            (uncheckFilter.filter === `${sourceFilter.id}` &&
                              uncheckFilter.type === 'subjects')))
                          ? { checked: false }
                          : {})}
                      />
                    </div>
                  );
                })}
              </div>
            )}
            {this.props.hideFilters && <div className="translucent-layer" />}
          </Paper>
        )}
      </div>
    );
  }
}

SubjectsFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  filters: PropTypes.object,
  handleFilterChange: PropTypes.func,
  uncheckFilter: PropTypes.any,
  selectedSubjectFilters: PropTypes.array
};

export default SubjectsFilterContainer;
