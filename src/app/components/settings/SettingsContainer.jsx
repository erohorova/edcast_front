import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Tabs, Tab } from 'material-ui/Tabs';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import ReactDOM from 'react-dom';
import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import { toggleTopNav } from '../../actions/configActions';
import { getNotificationConfig, renderNotificationConfig } from '../../actions/currentUserActions';

import { tr } from 'edc-web-sdk/helpers/translations';

/*
 Tab container components
 */

class SettingsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      activeTab: {},
      open: false,
      showNotificationTab: false
    };
    this.styles = {
      popover: {
        width: ''
      }
    };
    this.tabs = [];
  }
  componentDidMount() {
    this.props
      .dispatch(getNotificationConfig())
      .then(response => {
        this.setState({
          showNotificationTab: renderNotificationConfig(response.user_config, response.org_config)
        });
      })
      .catch(err => {
        console.error(`Error in SettingsContainer.getNotificationConfig.func : ${err}`);
      });
  }
  /*
   MUI handler to push to a new route
   */
  handleTabChange = path => {
    this.setState({
      open: false
    });
    if (this.props.pathname === path) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  handleTabHover = tab => {
    if (tab.options) {
      this.setState({
        activeTab: tab,
        open: true,
        hoveredTab: tab.label
      });
    } else {
      this.setState({ hoveredTab: tab.label });
    }
  };

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  handleTabLeave = tab => {
    this.setState({ hoveredTab: false });
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(
      function(tab) {
        if (currentPath === tab.path || (tab.pathsList && tab.pathsList.includes(currentPath))) {
          value = tab.path;
        }
      }.bind(this)
    );
    return value;
  };

  setTabs = () => {
    this.tabs = [
      {
        path: '/settings/details',
        label: 'Account Details',
        pathsList: ['/settings', '/settings/details']
      }
    ];

    if (this.props.team.OrgConfig.sections['web/sections/continuousLearning'].visible) {
      this.tabs.push({ path: '/settings/integrations', label: 'Integrations' });
    }

    if (
      window.ldclient.variation('users-notification-settings', false) &&
      this.state.showNotificationTab
    ) {
      this.tabs.push({ path: '/settings/triggers', label: 'Notifications' });
    }
  };

  render() {
    this.setTabs();
    let tabValue = this.selectTabValue(this.props.pathname);
    return (
      <section>
        <Popover
          open={this.state.open}
          anchorEl={this.state.activeTab.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
          useLayerForClickAway={false}
        >
          <Menu onMouseLeave={this.handleRequestClose}>
            {this.state.activeTab.options &&
              this.state.activeTab.options.map((option, index) => {
                return (
                  <MenuItem
                    key={index}
                    primaryText={tr(option.label)}
                    onTouchTap={this.handleTabChange.bind(this, option.path)}
                  />
                );
              })}
          </Menu>
        </Popover>
        <div className="row">
          <div className="small-12 sub-nav">
            <div className="settings-tab">
              {!this.props.topNav.topNav && (
                <Paper className="settings-paper">
                  <Tabs value={tabValue}>
                    {this.tabs.map((tab, index) => {
                      let tabHoverColor =
                        this.state.hoveredTab === tab.label ? { color: colors.primary200 } : {};
                      return (
                        <Tab
                          ref={node => {
                            tab.anchorEl = ReactDOM.findDOMNode(node);
                          }}
                          label={tr(tab.label)}
                          style={tabHoverColor}
                          className={this.toCamelCase('settings ' + tab.label)}
                          value={tab.path}
                          key={index}
                          onClick={this.handleTabChange.bind(this, tab.path)}
                          onMouseOver={this.handleTabHover.bind(this, tab)}
                          onMouseLeave={this.handleTabLeave.bind(this, tab)}
                        />
                      );
                    })}
                  </Tabs>
                </Paper>
              )}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 container-padding">{this.props.children}</div>
        </div>
      </section>
    );
  }
}

SettingsContainer.propTypes = {
  userNotifications: PropTypes.object,
  team: PropTypes.object,
  children: PropTypes.any,
  pathname: PropTypes.string,
  topNav: PropTypes.object,
  currentUser: PropTypes.object
};

/*
 @pathname: render updates based on the path changing
 */
function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    topNav: state.config.toJS(),
    team: state.team.toJS(),
    userNotifications: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(SettingsContainer);
