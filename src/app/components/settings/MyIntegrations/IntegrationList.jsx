import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { openIntegrationModal } from '../../../actions/modalActions';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { toggleIntegration } from 'edc-web-sdk/requests/integrations';
import { tr } from 'edc-web-sdk/helpers/translations';

class IntegrationList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      integrationList: props.integrationList
    };

    this.importClickHandler = this.importClickHandler.bind(this);
  }

  handleConnectClick(integration) {
    this.props.dispatch(openIntegrationModal(integration, this.importClickHandler));
  }

  importClickHandler(integration) {
    let index = this.state.integrationList.findIndex(i => i.id === integration.id);
    this.state.integrationList[index].connected = !this.state.integrationList[index].connected;
    this.setState({
      integrationList: this.state.integrationList
    });
  }

  handleDisConnectClick(integration, idx) {
    toggleIntegration(integration.id, { fields: {}, connect: false })
      .then(() => {
        this.state.integrationList[idx].connected = !this.state.integrationList[idx].connected;
        this.setState({
          integrationList: this.state.integrationList
        });
      })
      .catch(err => {
        console.error(`Error in IntegrationList.toggleIntegration.func : ${err}`);
      });
  }

  render() {
    return (
      <div className="row">
        {this.state.integrationList.map((integration, idx) => {
          return (
            <div className="small-2 columns text-center" key={idx}>
              <div className="integration-image">
                <img src={integration.logoUrl} />
              </div>
              <div>{integration.name}</div>
              <div>
                {!integration.connected && (
                  <SecondaryButton
                    label={tr('Connect')}
                    className="create"
                    onTouchTap={this.handleConnectClick.bind(this, integration)}
                  />
                )}
                {integration.connected && (
                  <PrimaryButton
                    label={tr('Connected')}
                    className="delete"
                    onTouchTap={this.handleDisConnectClick.bind(this, integration, idx)}
                  />
                )}
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

IntegrationList.propTypes = {
  integrationList: PropTypes.any
};

function mapStoreStateToProps(state) {
  return { currentUser: state.currentUser.toJS() };
}

export default connect(mapStoreStateToProps)(IntegrationList);
