import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IntegrationList from './IntegrationList';
import { getIntegrationList } from '../../../actions/usersActions';
import { connect } from 'react-redux';
import NeutralIcon from 'material-ui/svg-icons/social/sentiment-neutral';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../../common/spinner';

class IntegrationsContainer extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      integrationList: [],
      pending: true
    };
  }

  componentDidMount() {
    this.fetchIntegrations();
  }

  fetchIntegrations() {
    this.props
      .dispatch(getIntegrationList(this.props.currentUser.id))
      .then(result => {
        this.setState({ integrationList: result, pending: false });
      })
      .catch(err => {
        console.error(`Error in IntegrationsContainer.getIntegrationList.func : ${err}`);
      });
  }

  render() {
    return (
      <div className="settings-content">
        <div className="row">
          <div className="small-12 container-padding">
            <div
              id="integrations"
              className="vertical-spacing-large column large-10 large-offset-1 small-12"
            >
              <div className="">
                {tr(
                  'Contribute to your overall learning progress. Connect through available integrations to monitor all your progress in one place.'
                )}
              </div>
              <div className="profile-container vertical-spacing-large">
                {this.state.integrationList && !this.state.pending && (
                  <IntegrationList integrationList={this.state.integrationList} />
                )}
                {!this.state.integrationList.length && !this.state.pending && (
                  <div style={{ margin: 'auto' }}>
                    <div className="text-center">
                      <NeutralIcon style={{ height: '12rem', width: '6rem' }} />
                    </div>
                    <div className="text-center data-not-available-msg">
                      {tr('No integrations yet. Check back later!')}
                    </div>
                  </div>
                )}
                {this.state.pending && (
                  <div className="text-center" style={{ marginTop: '5rem' }}>
                    <Spinner />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

IntegrationsContainer.propTypes = {
  integrationList: PropTypes.any,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(IntegrationsContainer);
