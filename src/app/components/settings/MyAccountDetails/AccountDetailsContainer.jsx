import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import keys from 'lodash/keys';
import pick from 'lodash/pick';

import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import Checkbox from 'material-ui/Checkbox';
import ListItem from 'material-ui/List/ListItem';
import TextField from 'material-ui/TextField';
import Dialog from 'material-ui/Dialog';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import colors from 'edc-web-sdk/components/colors/index';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { sociative } from 'edc-web-sdk/requests';
import Paper from 'edc-web-sdk/components/Paper';
import {
  sendDataRequest,
  requestUserData,
  checkUserData,
  deleteUsersAccount,
  getAllCustomFields,
  getUserCustomFields,
  editUserCustomField
} from 'edc-web-sdk/requests/users.v2';
import { deleteSearchHistory } from 'edc-web-sdk/requests/search';
import { initOnboardingStateSettings } from '../../../actions/onboardingActions';

import { updateUserInfo, getSpecificUserInfo } from '../../../actions/currentUserActions';
import { openUploadImageModal } from '../../../actions/modalActions';
import { checkRegistration } from '../../../actions/sociativeActions';

import * as languages from '../../../constants/languages';
import * as actionTypes from '../../../constants/actionTypes';

import BlurImage from '../../common/BlurImage';
import Spinner from '../../common/spinner';

import { Permissions } from '../../../utils/checkPermissions';
import { setCookie } from 'edc-web-sdk/helpers/cookies';

import ConfirmationModal from '../../modals/ConfirmationModal';
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min';
import { updateUpshotUserProfile } from '../../../actions/upshotActions';

const languageAbbreviations = languages.langs;
const MAX_JOB_LENGTH = 50;

class AccountDetailsContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userCustomFields: [],
      isEditAccount: true,
      isEditPassword: false,
      passwordError: '',
      passwordConfirmError: '',
      passwordCurrentError: '',
      updateError: false,
      isWaiting: false,
      isUpload: false,
      avatar: null,
      banner: null,
      availableLanguages: keys(languages.langs),
      letUserConnectToSlack: window.ldclient.variation('connect-to-slack', false),
      showCompany: window.ldclient.variation('show-company', false),
      defaultLanguage: this.setDefaultLanguage(),
      freeChars: 1000,
      freeJobChars: 50,
      user: this.props.currentUser,
      requestingData: false,
      userDataDownload: window.ldclient.variation('user-data-download', false),
      deleteSearchHistory: window.ldclient.variation('delete-search-history', false),
      firstNameError: '',
      lastNameError: '',
      viewDetails: false,
      showDeleteMyAccount: window.ldclient.variation('delete-my-account', false),
      hideFromLeaderboard: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      encryption_payload:
        this.props.team && this.props.team.config && this.props.team.config.encryption_payload,
      isShowUserJob: window.ldclient.variation('is-show-user-job', false)
    };
    this.defaultAvatar = '/i/images/anonymous_user_white.jpeg';
    this.avatarIndex = 'anonymous_user_white.jpeg';
    this.defaultBanner = '/i/images/default_banner_user_image.png';
    this.bannerIndex = 'default_banner_user_image.png';
    this.coachEddyUrl =
      (this.props.team && this.props.team.config && this.props.team.config.coach_eddy_api_url) ||
      'http://api-gql.soc.edcast.com';
    this.styles = {
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      },
      inputStyle: {
        fontSize: '14px',
        color: 'rgba(0, 0, 0, 0.541176)'
      },
      inputDisableStyle: {
        fontSize: '14px',
        color: 'rgba(0, 0, 0, 0.541176)',
        cursor: 'not-allowed',
        paddingLeft: '15px',
        background: colors.silverSand
      },
      confirmationModal: {
        zIndex: 1502
      },
      confirmationContent: {
        transform: 'none'
      },
      avatarBox: {
        height: '5.625rem',
        width: '5.625rem',
        margin: '0 auto 1rem',
        position: 'relative'
      },
      mainModal: {
        maxHeight: '80vh',
        maxWidth: '48rem',
        margin: '0 auto'
      }
    };
    this.editToggleHandler = this.editToggleHandler.bind(this);
    this.validatePassword = this.validatePassword.bind(this);
    this.validatePasswordConfirm = this.validatePasswordConfirm.bind(this);
    this.validateCurrentPassword = this.validateCurrentPassword.bind(this);
    this.saveClickHandler = this.saveClickHandler.bind(this);
    this.passwordKeydownHandler = this.passwordKeydownHandler.bind(this);
    this.passwordConfirmKeydownHandler = this.passwordConfirmKeydownHandler.bind(this);
    this.passwordCurrentKeydownHandler = this.passwordCurrentKeydownHandler.bind(this);
    this.firstNameKeydownHandler = this.firstNameKeydownHandler.bind(this);
    this.lastNameKeydownHandler = this.lastNameKeydownHandler.bind(this);
    this.bioKeydownHandler = this.bioKeydownHandler.bind(this);
    this.passwordToggleHandler = this.passwordToggleHandler.bind(this);
    this.handleKeydownHandler = this.handleKeydownHandler.bind(this);
    this.connectToSlackClickHandler = this.connectToSlackClickHandler.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.jobTitle =
      (this.props.currentUser &&
        this.props.currentUser.profile &&
        this.props.currentUser.profile.jobTitle) ||
      '';
  }

  async componentDidMount() {
    this.props
      .dispatch(
        getSpecificUserInfo(
          [
            'handle',
            'first_name',
            'last_name',
            'bio',
            'userSubscriptions',
            'coverImage',
            'hideFromLeaderboard',
            'company'
          ],
          this.props.currentUser
        )
      )
      .then(() => {
        const { user, letUserConnectToSlack } = this.state;
        const { dispatch } = this.props;
        if (letUserConnectToSlack) {
          dispatch(checkRegistration(user.email, this.coachEddyUrl));
        }
        this.setState({
          freeChars:
            this.props.currentUser && this.props.currentUser.bio
              ? 1000 -
                (this.props.currentUser &&
                  this.props.currentUser.bio &&
                  this.props.currentUser.bio.length)
              : 1000,
          freeJobChars: MAX_JOB_LENGTH - this.jobTitle.length,
          viewDetails: true,
          user: this.props.currentUser,
          hideFromLeaderboard:
            this.props.currentUser && this.props.currentUser.hideFromLeaderboard ? true : false
        });
      })
      .catch(err => {
        console.error(`Error in AccountDetailsContainer.componentDidMount.func: ${err}`);
      });
    this.props.dispatch(initOnboardingStateSettings());
  }

  componentWillMount() {
    if (this.state.userDataDownload) {
      this.getRequestedData();
    }
  }

  editToggleHandler = () => {
    this.setState({
      isEditAccount: !this.state.isEditAccount,
      isEditPassword: false,
      passwordError: '',
      passwordCurrentError: '',
      firstNameError: '',
      lastNameError: '',
      passwordConfirmError: '',
      jobTitleError: '',
      freeChars: this.props.currentUser.bio ? 1000 - this.props.currentUser.bio.length : 1000,
      freeJobChars: MAX_JOB_LENGTH - this.jobTitle.length
    });
  };

  passwordToggleHandler = e => {
    e.preventDefault();
    this.setState({ isEditPassword: !this.state.isEditPassword });
  };

  replaceImageClickHandler = (imageType, image, e) => {
    e && e.preventDefault();
    if (this.state.isEditAccount) {
      this._imageTypeClicked = imageType;
      let sizes = {
        maxWidth: 0,
        maxHeight: 0
      };
      switch (imageType) {
        case 'avatar':
          sizes.maxHeight = 120;
          sizes.maxWidth = 120;
          break;
        case 'banner':
          sizes.maxHeight = 300;
          sizes.maxWidth = 1440;
          break;
        default:
          break;
      }
      this.props.dispatch(
        openUploadImageModal(imageType, sizes, newUrl => {
          this.setState({
            [imageType]: newUrl
          });
        })
      );
    }
  };

  removeImageClickHandler = (e, removeImageType) => {
    e.preventDefault();
    this.setState({ removeImageType, openConfirm: true });
  };

  confirmRemoveImage = () => {
    this.setState({
      [this.state.removeImageType]:
        this.state.removeImageType === 'avatar' ? this.defaultAvatar : this.defaultBanner,
      openConfirm: false
    });
  };

  cancelRemoveImage = () => {
    this.setState({ openConfirm: false });
  };

  validatePassword() {
    if (this._passwordLabel.input.value.trim() === '') {
      this.setState({ passwordError: 'Password is required' });
      return false;
    } else if (this._passwordLabel.input.value.trim().length < 8) {
      this.setState({ passwordError: 'Minimum 8 character length password' });
      return false;
    } else {
      this.setState({ passwordError: '' });
      return true;
    }
  }

  validateFirstname() {
    let firstName = this._firstNameLabel.input.value.trim();
    if (firstName === '') {
      this.setState({ firstNameError: tr('First Name is required') });
      return false;
    }

    let fieldNameReg = /^[a-zA-Z0-9-_' ]+$/;
    let validInput = firstName.search(fieldNameReg) > -1;
    if (!validInput) {
      this.setState({
        firstNameError: tr(
          'First Name cannot contain the following characters: + . ( ) [ ] ; ! @  / : * ? " # % < > | ~ &'
        )
      });
      return false;
    }

    this.setState({ firstNameError: '' });
    return true;
  }

  validateLastname() {
    let lastName = this._lastNameLabel.input.value.trim();
    if (this._lastNameLabel.input.value.trim() === '') {
      this.setState({ lastNameError: tr('Last Name is required') });
      return false;
    }

    let fieldNameReg = /^[a-zA-Z0-9-_' ]+$/;
    let validInput = lastName.search(fieldNameReg) > -1;
    if (!validInput) {
      this.setState({
        lastNameError: tr(
          'Last Name cannot contain the following characters: + . ( ) [ ] ; ! @  / : * ? " # % < > | ~ &'
        )
      });
      return false;
    }

    this.setState({ lastNameError: '' });
    return true;
  }

  validateCurrentPassword() {
    if (this._passwordCurrentLabel.input.value.trim() === '') {
      this.setState({ passwordCurrentError: 'Current Password is required' });
      return false;
    } else if (this._passwordCurrentLabel.input.value.trim().length < 8) {
      this.setState({ passwordCurrentError: 'Minimum 8 character length password' });
      return false;
    } else {
      this.setState({ passwordCurrentError: '' });
      return true;
    }
  }

  validatePasswordConfirm() {
    if (this._passwordConfirmLabel.input.value.trim() === '') {
      this.setState({ passwordConfirmError: 'Confirm Password is required' });
      return false;
    } else if (this._passwordLabel.input.value !== this._passwordConfirmLabel.input.value) {
      this.setState({ passwordConfirmError: "Passwords don't match" });
      return false;
    } else {
      this.setState({ passwordConfirmError: '' });
      return true;
    }
  }

  setDefaultLanguage() {
    let languageAbbreviation =
      this.props.currentUser.profile && this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    let language = Object.keys(languageAbbreviations).find(
      key => languageAbbreviations[key] === languageAbbreviation
    );
    setCookie('selectedLanguage', languageAbbreviation);
    return language;
  }

  saveClickHandler() {
    let user = this.state.user;
    let shouldRefresh =
      this.props.currentUser.profile.language !== languageAbbreviations[this.state.defaultLanguage];
    let job_title =
      this._jobTitleLabel &&
      this._jobTitleLabel.input &&
      this._jobTitleLabel.input.value &&
      this._jobTitleLabel.input.value.trim();
    let companyName =
      this._companyNameLabel &&
      this._companyNameLabel.input &&
      this._companyNameLabel.input.value &&
      this._companyNameLabel.input.value.trim();
    if (job_title && job_title.length > MAX_JOB_LENGTH) {
      this.setState({ jobTitleError: `Job title should be less than ${MAX_JOB_LENGTH}` });
      return;
    }

    if (
      (!this.state.isEditPassword ||
        (this.state.isEditPassword &&
          this.validatePassword() &&
          this.validatePasswordConfirm() &&
          this.validateCurrentPassword())) &&
      this.validateFirstname() &&
      this.validateLastname()
    ) {
      this.setState({ updateError: false, isWaiting: true });
      let newUserData = {
        first_name: this._firstNameLabel.input.value,
        last_name: this._lastNameLabel.input.value,
        bio: this._bioLabel.input.value,
        handle: this._handleLabel.input.value,
        hide_from_leaderboard: this.state.hideFromLeaderboard,
        profile_attributes: {
          language: languageAbbreviations[this.state.defaultLanguage],
          job_title:
            this._jobTitleLabel && this._jobTitleLabel.input
              ? job_title
              : user.profile
              ? user.profile.jobTitle
              : '',
          company: companyName
        }
      };

      if (this.state.isEditPassword) {
        newUserData.password = {
          password: this._passwordLabel.input.value,
          password_confirmation: this._passwordConfirmLabel.input.value,
          current_password: this._passwordCurrentLabel.input.value
        };
      }
      if (this.state.avatar && this.state.avatar !== this.defaultAvatar) {
        newUserData.avatar = null;
        newUserData.picture_url = this.state.avatar;
      }
      if (this.state.banner) {
        newUserData.coverimage = this.state.banner;
      }
      // Start Encryption of password in payload
      if (this.state.isEditPassword && this.state.encryption_payload && JSEncrypt) {
        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(window.process.env.JsPublic);
        newUserData.password = Object.entries(newUserData.password).reduce(
          (a, [k, v]) => ((a[k] = encrypt.encrypt(v)), a),
          {}
        );
      }
      if (this.state.upshotEnabled) {
        updateUpshotUserProfile({
          firstName: this._firstNameLabel.input.value,
          lastName: this._lastNameLabel.input.value,
          email: this.props.currentUser.email
        });
      }
      // End encryption of password in payload
      this.props
        .dispatch(updateUserInfo(this.props.currentUser, newUserData))
        .then(data => {
          this.updateSpecificData(data);
          this.editToggleHandler();

          if (data.response && data.response.text) {
            let message = {};
            try {
              message = JSON.parse(data.response.text).message;
              this.props.dispatch({
                type: actionTypes.OPEN_SNACKBAR,
                message,
                autoClose: true
              });
            } catch (e) {
              console.error('JSON parse error in AccountDetailsContainer');
            }
            this.setState({
              avatar: this.state.user.avatar
            });
          } else {
            user.first_name = data.firstName;
            user.last_name = data.lastName;
            user.bio = data.bio;
            user.handle = data.handle && data.handle.replace('@', '');
            user.avatar = data.avatar;
            if (!user.profile) {
              user.profile = {};
            }
            user.profile.jobTitle = data.profile ? data.profile.jobTitle : '';
            user.company = data.profile ? data.profile.company : '';
            this.setState({ user: user });
          }
          this.setState({
            isWaiting: false
          });

          if (shouldRefresh) {
            window.location.reload();
          }
        })
        .catch(err => {
          console.error(`Error in AccountDetailsContainer.updateUserInfo.func : ${err}`);
        });
    }
  }

  updateSpecificData = data => {
    var model = {
      firstName: null,
      lastName: null,
      bio: null,
      handle: null,
      avatar: null,
      hideFromLeaderboard: null,
      company: null
    };

    var result = pick(data, keys(model));
  };

  connectToSlackClickHandler() {
    window.location = sociative.connectToSlackURL(window.location.href, this.coachEddyUrl);
  }

  firstNameKeydownHandler(e) {
    if (e.keyCode === 13) {
      this._lastNameLabel.focus();
    }
    this.setState({ updateError: false });
  }

  lastNameKeydownHandler(e) {
    if (e.keyCode === 13) {
      this._bioLabel.focus();
    }
    this.setState({ updateError: false });
  }

  handleKeydownJobTitle = e => {
    let input = e.target.value;
    this.setState({
      freeJobChars: MAX_JOB_LENGTH - input.length,
      updateError: false
    });
  };

  handleKeydownHandler(e) {
    if (e.keyCode === 13) {
      this.saveClickHandler();
    }

    this.setState({ updateError: false });
  }

  languageSelectHandler(language) {
    this.setState({ defaultLanguage: language });
    this.setState({ updateError: false });
  }

  bioKeydownHandler(e) {
    if (e.keyCode === 13) {
      if (this.state.isEditPassword) {
        this._passwordCurrentLabel.focus();
      } else {
        this._handleLabel.focus();
      }
    }
    this.setState({ updateError: false });
  }

  passwordKeydownHandler(e) {
    if (e.keyCode === 13) {
      this._passwordConfirmLabel.focus();
    }
    this.setState({ updateError: false });
  }

  passwordCurrentKeydownHandler(e) {
    if (e.keyCode === 13) {
      this._passwordLabel.focus();
    }
    this.setState({ updateError: false });
  }

  passwordConfirmKeydownHandler(e) {
    if (e.keyCode === 13) {
      this.saveClickHandler();
    }
    this.setState({ updateError: false });
  }

  handleChange(event) {
    let input = event.target.value;
    this.setState({
      freeChars: 1000 - input.length
    });
  }

  handleCheckboxChange(event, isInputChecked) {
    this.setState({
      hideFromLeaderboard: isInputChecked
    });
  }

  async getRequestedData() {
    try {
      let req = await checkUserData();
      let status = {};
      switch (req.data_export_status) {
        case 'started':
          status.requestingData = true;
          status.requestedDataUrl = null;
          break;
        case 'done':
          status.requestingData = false;
          status.requestedDataUrl = req.data_export_url;
          status.requestedDataOnDate = req.last_data_export_time;
          break;
        case 'error':
        default:
          status.requestingData = false;
          status.requestedDataUrl = null;
      }
      this.setState(status);
      if (status.requestingData) {
        setTimeout(() => {
          this.getRequestedData();
        }, 60000);
      }
    } catch (e) {}
  }

  sendDataRequest = () => {
    if (this.state.userDataDownload) {
      sendDataRequest();
      let message =
        'Your request has been sent to support@edcast.com. You will receive a copy of your data soon.';
      this.props.dispatch({
        type: actionTypes.OPEN_SNACKBAR,
        message,
        autoClose: true
      });
    }
  };

  requestData = () => {
    if (!this.state.requestingData) {
      this.setState(
        { requestingData: true, requestedDataUrl: null, requestedDataOnDate: null },
        async function() {
          try {
            let req = await requestUserData();
            setTimeout(() => {
              this.getRequestedData();
            }, 60000);
          } catch (e) {}
        }
      );
    }
  };

  downloadData = () => {
    if (this.state.requestedDataUrl) {
      let link = document.createElement('a');
      link.href = this.state.requestedDataUrl;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
    this.setState({ modalDataDownload: false });
  };

  openBioInfoModal = () => {
    this.setState({ modalBioInfo: true });
  };

  closeBioInfoModal = () => {
    this.setState({ modalBioInfo: false });
  };

  openDataModal = () => {
    this.setState({ modalDataDownload: true });
  };

  closeDataModal = () => {
    this.setState({ modalDataDownload: false });
  };

  openDeleteAccountModal = () => {
    this.setState({ modalDeleteAccount: true });
  };

  closeDeleteAccountModal = () => {
    this.setState({ modalDeleteAccount: false });
  };

  deleteAccount = () => {
    // Delete the users account and then reload, session should be destroyed
    deleteUsersAccount()
      .then(() => {
        window.location.reload();
      })
      .catch(err => {
        console.error(`Error in AccountDetailsContainer.deleteUsersAccount.func : ${err}`);
      });
  };

  deleteSearchHistoryClickHandler = () => {
    deleteSearchHistory()
      .then(() => {
        let message = 'Your search history has been deleted.';
        this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message,
          autoClose: true
        });
      })
      .catch(err => {
        console.error(`Error in AccountDetailsContainer.deleteSearchHistory.func : ${err}`);
      });
  };

  render() {
    let user = this.state.user;
    let { onBoarding } = this.props;
    let sociativeProp = this.props.sociative;
    let avatar = this.state.avatar || user.avatar || this.defaultAvatar;
    let banner = this.state.banner || user.coverImage || this.defaultBanner;
    let start_date =
      this.props.currentUser.userSubscription && this.props.currentUser.userSubscription.start_date
        ? new Date(this.props.currentUser.userSubscription.start_date)
        : new Date();
    let end_date =
      this.props.currentUser.userSubscription && this.props.currentUser.userSubscription.end_date
        ? new Date(this.props.currentUser.userSubscription.end_date)
        : new Date();
    var monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ];

    return (
      <div className="settings-content">
        <div className="row">
          <div className="small-12 container-padding">
            <div id="accountDetails" className="horizontal-spacing-xlarge">
              {!this.state.isWaiting && (
                <div className="profile-container vertical-spacing-large">
                  <div className="settings-paper">
                    <div className="row">
                      <div className="small-10">
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('First Name')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <ListItem
                                style={{ paddingLeft: 0 }}
                                disabled
                                secondaryText={user.first_name}
                              />
                            )) ||
                              (this.state.viewDetails && (
                                <TextField
                                  hintText={tr('First Name')}
                                  aria-label={tr('First Name')}
                                  inputStyle={this.styles.inputStyle}
                                  fullWidth
                                  underlineStyle={{ bottom: '3px' }}
                                  onKeyDown={this.firstNameKeydownHandler}
                                  ref={node => (this._firstNameLabel = node)}
                                  defaultValue={user.first_name}
                                  errorText={tr(this.state.firstNameError)}
                                  disabled={
                                    onBoarding.user.password_changeable ? false : 'disabled'
                                  }
                                />
                              ))}
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Last Name')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <ListItem
                                disabled
                                style={{ paddingLeft: 0 }}
                                secondaryText={user.last_name}
                              />
                            )) ||
                              (this.state.viewDetails && (
                                <TextField
                                  hintText={tr('Last Name')}
                                  aria-label={tr('Last Name')}
                                  inputStyle={this.styles.inputStyle}
                                  onKeyDown={this.lastNameKeydownHandler}
                                  fullWidth
                                  underlineStyle={{ bottom: '3px' }}
                                  ref={node => (this._lastNameLabel = node)}
                                  defaultValue={user.last_name}
                                  errorText={tr(this.state.lastNameError)}
                                  disabled={
                                    onBoarding.user.password_changeable ? false : 'disabled'
                                  }
                                />
                              ))}
                          </div>
                        </div>
                        <div className={this.state.isEditAccount ? 'bio-field row' : 'row'}>
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Bio')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <div className="row">
                                <div className="small-10">
                                  <ListItem
                                    disabled
                                    style={{ paddingLeft: 0 }}
                                    secondaryText={user.bio}
                                  />
                                </div>
                                <div className="small-2">
                                  <div
                                    className="small-font inline-block"
                                    style={{ paddingTop: '1.5rem' }}
                                  >
                                    <a
                                      className="cursor-pointer close"
                                      onClick={this.openBioInfoModal}
                                    >
                                      {tr('More')}
                                    </a>
                                  </div>
                                </div>
                              </div>
                            )) ||
                              (this.state.viewDetails && (
                                <div>
                                  <TextField
                                    id="bioInput"
                                    hintText={tr('Bio')}
                                    aria-label={tr('Bio')}
                                    onChange={this.handleChange.bind(this)}
                                    maxLength="1000"
                                    underlineStyle={{ bottom: '3px' }}
                                    fullWidth
                                    onKeyDown={this.bioKeydownHandler}
                                    inputStyle={this.styles.inputStyle}
                                    ref={node => (this._bioLabel = node)}
                                    defaultValue={user.bio || ''}
                                  />
                                  <p className="char-counter">
                                    <small>
                                      {this.state.freeChars}/1000 {tr('Characters Remaining')}
                                    </small>
                                  </p>
                                </div>
                              ))}
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Email')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <ListItem
                                disabled
                                style={{ paddingLeft: 0 }}
                                secondaryText={user.email}
                              />
                            )) || (
                              <TextField
                                hintText="name@domain.com"
                                aria-label={tr('enter email with format, name@domain.com')}
                                disabled
                                ref={node => (this._emailLabel = node)}
                                underlineStyle={{ border: 'none' }}
                                inputStyle={this.styles.inputDisableStyle}
                                fullWidth
                                defaultValue={user.email}
                              />
                            )}
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Password')} />
                          </div>
                          <div className="small-6">
                            {!this.state.isEditAccount && !this.state.isEditPassword && (
                              <ListItem
                                disabled
                                style={{ paddingLeft: 0 }}
                                secondaryText={'**********'}
                              />
                            )}
                            {this.state.isEditAccount && !this.state.isEditPassword && (
                              <div className="password-block">
                                <span className="password-edit">**********</span>
                                {onBoarding.user && onBoarding.user.password_changeable && (
                                  <a
                                    href="#"
                                    className="small-font"
                                    onClick={this.passwordToggleHandler}
                                  >
                                    {tr('set new password')}
                                  </a>
                                )}
                              </div>
                            )}
                            {this.state.isEditAccount && this.state.isEditPassword && (
                              <div>
                                <TextField
                                  hintText={tr('Current Password')}
                                  aria-label={tr('Current Password')}
                                  type="password"
                                  errorText={tr(this.state.passwordCurrentError)}
                                  ref={node => (this._passwordCurrentLabel = node)}
                                  underlineStyle={{ bottom: '3px' }}
                                  fullWidth
                                  onBlur={this.validateCurrentPassword}
                                  onKeyDown={this.passwordCurrentKeydownHandler}
                                  inputStyle={this.styles.inputStyle}
                                />
                                <TextField
                                  hintText={tr('New Password')}
                                  aria-label={tr('New Password')}
                                  errorText={tr(this.state.passwordError)}
                                  ref={node => (this._passwordLabel = node)}
                                  underlineStyle={{ bottom: '3px' }}
                                  fullWidth
                                  type="password"
                                  onBlur={this.validatePassword}
                                  onKeyDown={this.passwordKeydownHandler}
                                  inputStyle={this.styles.inputStyle}
                                />
                                <TextField
                                  hintText={tr('Confirm Password')}
                                  aria-label={tr('Confirm Password')}
                                  errorText={tr(this.state.passwordConfirmError)}
                                  ref={node => (this._passwordConfirmLabel = node)}
                                  underlineStyle={{ bottom: '3px' }}
                                  fullWidth
                                  type="password"
                                  onBlur={this.validatePasswordConfirm}
                                  onKeyDown={this.passwordConfirmKeydownHandler}
                                  inputStyle={this.styles.inputStyle}
                                />
                              </div>
                            )}
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Handle')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <ListItem
                                disabled
                                style={{ paddingLeft: 0 }}
                                secondaryText={user.handle}
                              />
                            )) ||
                              (this.state.viewDetails && (
                                <TextField
                                  hintText={tr('Handle')}
                                  aria-label={tr('Handle')}
                                  inputStyle={this.styles.inputStyle}
                                  onKeyDown={this.handleKeydownHandler}
                                  fullWidth
                                  underlineStyle={{ bottom: '3px' }}
                                  ref={node => (this._handleLabel = node)}
                                  defaultValue={user.handle}
                                />
                              ))}
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-6">
                            <ListItem disabled secondaryText={tr('Preferred Language')} />
                          </div>
                          <div className="small-6">
                            {(!this.state.isEditAccount && (
                              <ListItem
                                disabled
                                style={{ paddingLeft: 0 }}
                                secondaryText={this.state.defaultLanguage}
                              />
                            )) || (
                              <SelectField
                                floatingLabelText={tr('Select Prefered Language')}
                                value={this.state.defaultLanguage}
                                aria-label={tr('Select Prefered Language')}
                              >
                                {this.state.availableLanguages.sort().map((language, index) => {
                                  return (
                                    <MenuItem
                                      key={index}
                                      value={language}
                                      primaryText={language}
                                      onTouchTap={this.languageSelectHandler.bind(this, language)}
                                    />
                                  );
                                })}
                              </SelectField>
                            )}
                          </div>
                        </div>
                        {this.state.showCompany && (
                          <div className="row">
                            <div className="small-6">
                              <ListItem disabled secondaryText={tr('Company Name')} />
                            </div>
                            <div className="small-6">
                              {(!this.state.isEditAccount && (
                                <ListItem
                                  disabled
                                  style={{ paddingLeft: 0 }}
                                  secondaryText={user.company || ''}
                                />
                              )) ||
                                (this.state.viewDetails && (
                                  <div>
                                    <TextField
                                      hintText={tr('Company Name')}
                                      aria-label={tr('Company Name')}
                                      inputStyle={this.styles.inputStyle}
                                      fullWidth
                                      maxLength="50"
                                      underlineStyle={{ bottom: '3px' }}
                                      ref={node => (this._companyNameLabel = node)}
                                      defaultValue={user.company || ''}
                                    />
                                  </div>
                                ))}
                            </div>
                          </div>
                        )}
                        {this.props.team.config &&
                          this.props.team.config.leaderboard &&
                          Permissions.has('USER_OPT_OUT_OF_LEADERBOARD') && (
                            <div className="row">
                              <div className="small-6">
                                <ListItem
                                  disabled
                                  secondaryText={tr('Hide me from Leaderboards')}
                                />
                              </div>
                              <div className="small-6">
                                <div style={{ paddingTop: '20px' }}>
                                  <Checkbox
                                    style={this.styles.checkbox}
                                    label=""
                                    value={this.state.hideFromLeaderboard}
                                    checked={this.state.hideFromLeaderboard}
                                    onCheck={(event, isInputChecked) =>
                                      this.handleCheckboxChange(event, isInputChecked)
                                    }
                                  />
                                </div>
                              </div>
                            </div>
                          )}
                        <div className="row">
                          <div className="small-6">
                            <ListItem
                              disabled
                              secondaryText={
                                <span>
                                  {tr('Profile Image')} <br />
                                  <small>{tr('Recommended Size')}: 120px x 120px</small>
                                </span>
                              }
                            />
                          </div>
                          <div className="small-6">
                            <div className="image-block">
                              <BlurImage
                                style={this.styles.avatarBox}
                                id={user.id}
                                image={avatar}
                                imageClickHandler={this.replaceImageClickHandler.bind(
                                  this,
                                  'avatar',
                                  avatar
                                )}
                                styleImage={
                                  this.state.isEditAccount
                                    ? { cursor: 'pointer', zIndex: 2 }
                                    : { zIndex: 2 }
                                }
                              />
                              {this.state.isEditAccount && (
                                <div className="row">
                                  <div className="small-font small-6">
                                    <a
                                      className="cursor-pointer uploadImage"
                                      href="#"
                                      onClick={e =>
                                        this.replaceImageClickHandler('avatar', avatar, e)
                                      }
                                    >
                                      {tr('Change Profile Image')}
                                    </a>
                                  </div>
                                  {!~avatar.indexOf(this.avatarIndex) &&
                                    !~avatar.indexOf('medium/photo.jpg') && (
                                      <div className="small-font inline-block">
                                        <a
                                          href="#"
                                          className="cursor-pointer uploadImage"
                                          onClick={e => this.removeImageClickHandler(e, 'avatar')}
                                        >
                                          {tr('Remove Image')}
                                        </a>
                                      </div>
                                    )}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                        {this.state.isShowUserJob && (
                          <div className="row">
                            <div className="small-6">
                              <ListItem disabled secondaryText={tr('Job Title')} />
                            </div>
                            <div className="small-6">
                              {(!this.state.isEditAccount && (
                                <ListItem
                                  disabled
                                  style={{ paddingLeft: 0 }}
                                  secondaryText={user.profile.jobTitle}
                                />
                              )) ||
                                (this.state.viewDetails && (
                                  <div>
                                    <TextField
                                      hintText={tr('Job Title')}
                                      aria-label={tr('Job Title')}
                                      errorText={tr(this.state.jobTitleError)}
                                      inputStyle={this.styles.inputStyle}
                                      onKeyDown={this.handleKeydownJobTitle}
                                      fullWidth
                                      maxLength="50"
                                      underlineStyle={{ bottom: '3px' }}
                                      ref={node => (this._jobTitleLabel = node)}
                                      defaultValue={user.profile ? user.profile.jobTitle : ''}
                                    />
                                    <p className="char-counter">
                                      <small>
                                        {this.state.freeJobChars}/{MAX_JOB_LENGTH}{' '}
                                        {tr('Characters Remaining')}
                                      </small>
                                    </p>
                                  </div>
                                ))}
                            </div>
                          </div>
                        )}

                        <div className="row">
                          <div className="small-6">
                            <ListItem
                              disabled
                              secondaryText={
                                <span>
                                  {tr('Banner Image')} <br />
                                  <small>{tr('Recommended Size')}: 1440px x 300px</small>
                                </span>
                              }
                            />
                          </div>
                          <div className="small-6">
                            <div className="image-block">
                              <a
                                href="#"
                                onClick={e => this.replaceImageClickHandler('banner', banner, e)}
                              >
                                <img
                                  alt="update banner image"
                                  className="cover-image replaceImage"
                                  style={this.state.isEditAccount ? { cursor: 'pointer' } : {}}
                                  src={banner}
                                />
                              </a>
                              {this.state.isEditAccount && (
                                <div className="row">
                                  <div className="small-font small-6">
                                    <a
                                      href="#"
                                      className="cursor-pointer replaceImage"
                                      onClick={e =>
                                        this.replaceImageClickHandler('banner', banner, e)
                                      }
                                    >
                                      {tr('Change Banner Image')}
                                    </a>
                                  </div>
                                  {!~banner.indexOf(this.bannerIndex) &&
                                    !~banner.indexOf('default_banner_user_image') && (
                                      <div className="small-font small-6">
                                        <a
                                          href="#"
                                          className="cursor-pointer uploadImage"
                                          onClick={e => this.removeImageClickHandler(e, 'banner')}
                                        >
                                          {tr('Remove Banner')}
                                        </a>
                                      </div>
                                    )}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        {this.props.currentUser.userSubscription && (
                          <div className="row">
                            <div className="small-6">
                              <ListItem disabled secondaryText={tr('Current Subscription')} />
                            </div>
                            <div className="small-6">
                              <div
                                className="password-block"
                                style={{ color: 'green', fontWeight: '600', lineHeight: '20px' }}
                              >
                                You paid{' '}
                                <strong>
                                  {this.props.currentUser.userSubscription.currency}{' '}
                                  {this.props.currentUser.userSubscription.amount}
                                </strong>{' '}
                                for your subscription from{' '}
                                <strong>
                                  {monthNames[start_date.getMonth()]} {start_date.getDay()},{' '}
                                  {start_date.getFullYear()}
                                </strong>{' '}
                                to{' '}
                                <strong>
                                  {monthNames[end_date.getMonth()]} {end_date.getDay()},{' '}
                                  {end_date.getFullYear()}
                                </strong>
                              </div>
                            </div>
                          </div>
                        )}

                        {this.state.userDataDownload && (
                          /* Request data download */
                          <div className="row">
                            <div className="small-6">
                              <ListItem disabled secondaryText={tr('Your EdCast Data')} />
                            </div>
                            <div className="small-6 settings-data">
                              <SecondaryButton
                                label={tr('Request a copy')}
                                className="close"
                                onTouchTap={this.sendDataRequest}
                              />
                            </div>
                          </div>
                        )}

                        {this.state.deleteSearchHistory && (
                          /* Request data download */
                          <div className="row">
                            <div className="small-6">
                              <ListItem disabled secondaryText={tr('Delete Search History')} />
                            </div>
                            <div className="small-6 settings-data">
                              <SecondaryButton
                                label={tr('Delete')}
                                className="close"
                                onTouchTap={this.deleteSearchHistoryClickHandler}
                              />
                            </div>
                          </div>
                        )}

                        {this.state.letUserConnectToSlack &&
                          (sociativeProp.status.connectedToSlack ? (
                            <div className="row">
                              <div className="small-6">
                                <ListItem disabled secondaryText={tr('Connect to Slack')} />
                              </div>
                              <div className="small-6">
                                <ListItem
                                  style={{ paddingLeft: 0 }}
                                  disabled
                                  secondaryText={tr('Connected')}
                                />
                              </div>
                            </div>
                          ) : (
                            <div className="row">
                              <PrimaryButton
                                label={tr('Connect to Slack')}
                                onTouchTap={this.connectToSlackClickHandler}
                              />
                            </div>
                          ))}
                        {this.state.modalBioInfo && (
                          <div
                            className="card-modal-container"
                            style={this.styles.confirmationModal}
                          >
                            <div className="backdrop" onClick={this.closeBioInfoModal} />
                            <div className="modal">
                              <Paper style={this.styles.mainModal}>
                                <div className="container">
                                  <div className="container-padding">
                                    <div className="vertical-spacing-large">{user.bio}</div>
                                    <div className="modal-actions text-right">
                                      <SecondaryButton
                                        label={tr('Close')}
                                        className="close"
                                        onTouchTap={this.closeBioInfoModal}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </Paper>
                            </div>
                          </div>
                        )}

                        {/* DELETE USERS ACCOUNT */
                        this.state.showDeleteMyAccount && (
                          <div className="row">
                            <div className="small-6 settings-data">
                              <SecondaryButton
                                label={tr('Delete my account')}
                                className="close"
                                onTouchTap={this.openDeleteAccountModal}
                              />
                              {this.state.modalDeleteAccount ? (
                                <div className="settings-data-modal-container">
                                  <div className="settings-data-modal">
                                    <h6>
                                      {tr(
                                        'This will delete all of your data and your account. This action is permanent and irreversible. Do you want to continue?'
                                      )}
                                    </h6>
                                    <p>
                                      <SecondaryButton
                                        label={tr('No')}
                                        className="close"
                                        onTouchTap={this.closeDeleteAccountModal}
                                      />
                                      <PrimaryButton
                                        label={tr('Yes')}
                                        onTouchTap={this.deleteAccount}
                                      />
                                    </p>
                                  </div>
                                </div>
                              ) : null}
                            </div>
                          </div>
                        )}
                      </div>
                      {!this.state.isEditAccount && (
                        <div className="small-2" style={{ textAlign: 'right' }}>
                          <IconButton
                            aria-label="edit"
                            className="edit"
                            onTouchTap={this.editToggleHandler}
                            iconStyle={this.styles.smallIcon}
                            style={this.styles.smallIconButton}
                          >
                            <EditIcon color={colors.silverSand} />
                          </IconButton>
                        </div>
                      )}
                    </div>
                    {this.state.isEditAccount && this.state.updateError && (
                      <div className="text-right error-text data-not-available-msg">
                        {tr('Sorry, some server error! Please, try again later!')}
                      </div>
                    )}
                    {this.state.isEditAccount ? (
                      <div className="text-right">
                        <SecondaryButton
                          className="close"
                          onTouchTap={this.editToggleHandler}
                          label={tr('Cancel')}
                        />
                        <PrimaryButton
                          className="create"
                          disabled={this.state.isUpload}
                          onTouchTap={this.saveClickHandler}
                          label={tr('Save')}
                        />
                      </div>
                    ) : (
                      <div className="text-right">
                        <SecondaryButton
                          className="close"
                          onTouchTap={() => {
                            window.history.back();
                          }}
                          label="Back"
                        />
                      </div>
                    )}
                  </div>
                </div>
              )}
              {this.state.isWaiting && (
                <div className="progress text-center">
                  <Spinner />
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.openConfirm && (
          <Dialog
            open={this.state.openConfirm}
            autoScrollBodyContent={false}
            style={this.styles.confirmationModal}
            contentStyle={this.styles.confirmationContent}
          >
            <ConfirmationModal
              title={null}
              noNeedClose={true}
              message={`Are you sure you want to remove your ${
                this.state.removeImageType
              }? This can't be undone.`}
              cancelClick={this.cancelRemoveImage}
              callback={this.confirmRemoveImage}
              confirmBtnTitle="Yes"
              cancelBtnTitle="No"
            />
          </Dialog>
        )}
      </div>
    );
  }
}

AccountDetailsContainer.propTypes = {
  onBoarding: PropTypes.object,
  sociative: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    onBoarding: state.onboarding.toJS(),
    sociative: state.sociative.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(AccountDetailsContainer);
