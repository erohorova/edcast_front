import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import * as actions from 'edc-web-sdk/requests/orgSettings';

import * as actionTypes from '../../../constants/actionTypes';
import { close as closeSnackBar } from '../../../actions/snackBarActions';
import { getNotificationConfig } from '../../../actions/currentUserActions';

class TriggersContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      triggerObj: {}
    };
    this.styles = {
      lateralPadding: {
        paddingLeft: '0.9375rem',
        paddingRight: '0.9375rem'
      },
      notificationNameHeader: {
        width: '45%'
      },
      notificationsName: {
        whiteSpace: 'normal',
        fontWeight: '600'
      }
    };
  }

  componentDidMount() {
    if (this.props.userNotifications) {
      this.setState({
        triggerObj: this.props.userNotifications.user_config,
        orgConfig: this.props.userNotifications.org_config
      });
    } else {
      this.props
        .dispatch(getNotificationConfig())
        .then(response => {
          if (this.props.userNotifications) {
            this.setState({
              triggerObj: response.user_config,
              orgConfig: response.org_config
            });
          }
        })
        .catch(err => {
          console.error(`Error in SettingsContainer.getNotificationConfig.func : ${err}`);
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.userNotifications &&
      (nextProps.userNotifications.user_config != this.state.triggerObj ||
        nextProps.userNotifications.org_config != this.state.orgConfig)
    ) {
      this.setState({
        orgConfig: nextProps.userNotifications.org_config
      });
    }
  }

  changeConfig(notification_id, medium_id, event, index, value) {
    let triggerObj = this.state.triggerObj;
    let _this = this;
    triggerObj.options[notification_id][medium_id].map(option => {
      if (option.id == value) {
        option.selected = true;
      } else {
        option.selected = false;
      }
    });
    this.setState({ triggerObj });
    actions
      .putUserNotificationConfig(triggerObj.options)
      .then(data => {
        this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Configuration Updated',
          autoClose: true
        });
        window.setTimeout(function() {
          _this.props.dispatch(closeSnackBar());
        }, 400);
      })
      .catch(err => {
        console.error(`Error in TriggersContainer.putUserNotificationConfig.func : ${err}`);
      });
  }

  getValue = (notification, medium) => {
    let selected;
    let userConfigObj = this.state.triggerObj.options[notification][medium];
    let orgConfigObj = this.state.orgConfig.options[notification][medium];

    if (this.disableForUser(notification, medium)) {
      orgConfigObj.map((option, index) => {
        if (option.selected) {
          selected = option.id;
        }
      });
    } else {
      userConfigObj.map((option, index) => {
        if (option.selected) {
          selected = option.id;
        }
      });
    }

    return selected;
  };

  getOptions = (notification, medium) => {
    return this.state.orgConfig.options[notification][medium].filter(function(obj) {
      if (obj.label === 'Off' || (obj.label !== 'Off' && obj.selected)) {
        if (obj.label !== 'Off') {
          obj.label = 'On';
        }
        return obj;
      }
    });
  };

  disableForUser = (notification, medium) => {
    let disabled;
    disabled =
      !this.state.orgConfig.options[notification].user_configurable ||
      this.state.orgConfig.options[notification][medium].findIndex(function(obj) {
        return obj.label === 'Off' && obj.selected;
      }) >= 0;
    return disabled;
  };

  render() {
    let triggerObj = this.state.triggerObj;
    let orgConfig = this.state.orgConfig;
    let template;
    let _this = this;
    return (
      <div id="notifications">
        {this.state.showAlert && (
          <FlashAlert
            message={tr(this.state.errorMessage)}
            toShow={this.state.showAlert}
            removeAlert={this.removeAlert}
          />
        )}
        {triggerObj.groups &&
          triggerObj.groups.map((group, index) => {
            return (
              <div>
                <div className="row">
                  <div className="col-12">
                    <div className="scrollable-table-container">
                      <Table>
                        <TableBody displayRowCheckbox={false}>
                          <TableRow>
                            <TableRowColumn
                              style={{
                                ...this.styles.lateralPadding,
                                ...this.styles.notificationNameHeader
                              }}
                            >
                              <strong>{tr(group.label)}</strong>
                            </TableRowColumn>
                            {triggerObj.mediums &&
                              triggerObj.mediums.map((medium, index2) => {
                                return (
                                  <TableRowColumn key={index2} style={this.styles.lateralPadding}>
                                    <strong>{tr(medium.label)}</strong>
                                  </TableRowColumn>
                                );
                              })}
                          </TableRow>
                          {group.notifications &&
                            group.notifications.map((notification, index3) => {
                              if (
                                _this.state.orgConfig.options[notification.id].user_configurable
                              ) {
                                return (
                                  <TableRow key={index3} style={{ border: '0' }}>
                                    <TableRowColumn
                                      style={{
                                        ...this.styles.lateralPadding,
                                        ...this.styles.notificationsName
                                      }}
                                    >
                                      {tr(notification.label)}
                                    </TableRowColumn>
                                    {triggerObj.mediums &&
                                      triggerObj.mediums.map((medium, index4) => {
                                        return (
                                          <TableRowColumn
                                            key={index4}
                                            style={this.styles.lateralPadding}
                                          >
                                            {_this.disableForUser(notification.id, medium.id) ? (
                                              _this
                                                .getOptions(notification.id, medium.id)
                                                .map((option, index5) => {
                                                  template = option.selected ? (
                                                    <span>{tr(option.label)}</span>
                                                  ) : (
                                                    ''
                                                  );
                                                  return template;
                                                })
                                            ) : (
                                              <SelectField
                                                style={{ width: '100%' }}
                                                className="form-control"
                                                disabled={_this.disableForUser(
                                                  notification.id,
                                                  medium.id
                                                )}
                                                onChange={_this.changeConfig.bind(
                                                  _this,
                                                  notification.id,
                                                  medium.id
                                                )}
                                                value={_this.getValue(notification.id, medium.id)}
                                              >
                                                {_this
                                                  .getOptions(notification.id, medium.id)
                                                  .map((option, index5) => {
                                                    return (
                                                      <MenuItem
                                                        value={option.id}
                                                        primaryText={tr(option.label)}
                                                      />
                                                    );
                                                  })}
                                              </SelectField>
                                            )}
                                          </TableRowColumn>
                                        );
                                      })}
                                  </TableRow>
                                );
                              }
                            })}
                        </TableBody>
                      </Table>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    );
  }
}
TriggersContainer.propTypes = {
  userNotifications: PropTypes.object
};
function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    userNotifications: state.currentUser.toJS().notificationConfig
  };
}

export default connect(mapStoreStateToProps)(TriggersContainer);
