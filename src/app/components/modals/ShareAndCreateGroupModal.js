import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Checkbox from 'material-ui/Checkbox';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';

import { createGroup } from 'edc-web-sdk/requests/groups.v2';

class ShareAndCreateGroupModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      name: '',
      description: ''
    };
    this.styles = {
      create_group: {
        backgroundColor: '#454560',
        color: '#fff',
        border: '0',
        width: '7.625rem',
        height: '1.75rem',
        borderRadius: '0.125rem'
      },
      back_button: {
        width: '5.6875rem',
        height: '1.75rem',
        borderRadius: '0.125rem',
        border: 'solid 1px #acadc1',
        marginRight: '1rem'
      }
    };
  }
  closeModal = () => {
    this.props.dispatch(close());
  };

  backHandle = () => {
    this.props.backHandle();
  };

  createHandle = async () => {
    let payload = {
      name: this.state.name,
      description: this.state.description
    };
    if (this.props.isDynamicGroup) {
      payload.is_dynamic = true;
    }
    let team = await createGroup(payload);
    this.props.createHandle(team);
  };

  render() {
    let charForTitle = 150;
    let charForDescription = 2000;
    return (
      <div className="group-creation-modal share-and-create-modal">
        <div className="share-and-create-modal-header">
          <span className="header-title">{tr('Group Creation')}</span>
          <div className="close close-button">
            <IconButton
              style={{ paddingRight: 0, width: 'auto' }}
              aria-label="close"
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div
          style={{ margin: '0 auto' }}
          className={`container-padding vertical-spacing-large container__v2`}
        >
          <div className={'input-row__margin-bottom'}>
            <input
              className="group-name"
              type="text"
              placeholder={tr('Title')}
              onChange={e => {
                this.setState({ name: e.target.value });
              }}
            />
            <small>{`${charForTitle - this.state.name.length}/${charForTitle} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
          <div className={'input-row__margin-bottom'}>
            <input
              className="group-description"
              maxLength={2000}
              value={this.state.description}
              placeholder={tr('Description')}
              onChange={e => {
                this.setState({ description: e.target.value });
              }}
            />
            <small>{`${charForDescription -
              this.state.description.length}/${charForDescription} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
        </div>
        <div style={{ textAlign: 'center' }}>
          <SecondaryButton
            style={this.styles.back_button}
            label={tr('Back')}
            onTouchTap={this.backHandle}
          />
          <SecondaryButton
            style={this.styles.create_group}
            label={tr('Create Group')}
            onTouchTap={this.createHandle}
            disabled={!this.state.description.trim().length || !this.state.name.trim().length}
          />
        </div>
      </div>
    );
  }
}
ShareAndCreateGroupModal.propTypes = {
  backHandle: PropTypes.func,
  createHandle: PropTypes.func,
  isDynamicGroup: PropTypes.bool
};

export default connect()(ShareAndCreateGroupModal);
