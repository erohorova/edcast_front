import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactTags from 'react-tag-autocomplete';

import _ from 'lodash';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import Checkbox from 'material-ui/Checkbox';

import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import {
  createGroup,
  deleteTeamUsers,
  getUsersInAGroupV2,
  deleteUsersFromTeam
} from 'edc-web-sdk/requests/groups.v2';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import { close, openStatusModal } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import {
  updateMembers,
  getGroupDetail,
  updateGroupExtended,
  getTeamChannels,
  getTeamCards,
  invite,
  getGroupUsers,
  deleteUserFromTeam
} from '../../actions/groupsActionsV2';
import * as channelSDK from 'edc-web-sdk/requests/channels';
import * as channelsV2Sdk from 'edc-web-sdk/requests/channels.v2';
import { filestackClient } from '../../utils/filestack';
import { fileStackSources } from '../../constants/fileStackSource';
import TextField from 'material-ui/TextField';

import CustomTag from '../common/CustomTag';
import { push } from 'react-router-redux';

class GroupCreationModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      imageContainer: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%',
        display: 'block'
      }
    };
    this.state = {
      isForUpdate: !!props.groupId,
      name: '',
      description: '',
      pending: false,
      fileStack: [],
      createErrorText: '',
      auto_assign_content: true,
      adminCanShare: false,
      memberCanShare: false,
      adminCanAssign: false,
      memberCanAssign: false,
      groupIsOpen: false,
      isMandatory: false,
      suggestionsLimit: 10,
      leadersOffset: 0,
      adminsOffset: 0,
      membersOffset: 0,
      channelsOffset: 0,
      leadersCurrent: [],
      adminsCurrent: [],
      membersCurrent: [],
      channelsCurrent: [],
      leadersRemove: [],
      adminsRemove: [],
      membersRemove: [],
      channelsRemove: [],
      leadersOptions: [],
      adminsOptions: [],
      membersOptions: [],
      channelsOptions: [],
      leadersOld: [],
      adminsOld: [],
      membersOld: [],
      channelsOld: [],
      currentGroup: {},
      currentGroupId: '',
      uiV2: window.ldclient.variation('group-create-modal-v2', false),
      workWithMandatoryGroup: window.ldclient.variation('mandatory-group', false),
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      useFormLabels:
        window.ldclient.variation('use-form-labels', false) ||
        process.env.NODE_ENV === 'development'
    };
  }

  componentDidMount() {
    let id = this.props && this.props.groupId;
    let group = this.props.groupsV2[id];
    if (this.state.isForUpdate && group) {
      this.setState(() => ({
        currentGroupId: id,
        name: group.name,
        currentGroup: group,
        groupIsOpen: !group.isPrivate,
        isMandatory: group.isMandatory,
        isDynamic: group.isDynamic,
        auto_assign_content: group.autoAssignContent,
        shouldShareToAdmin: group.onlyAdminCanPost,
        description: group.description,
        showMainImage: true
      }));
      let pendingPayload = {
        offset: '0',
        limit: (group && group.pendingMembersCount) || 10,
        user_type: 'pending'
      };
      this.props.dispatch(getGroupUsers(id, pendingPayload));
      this.props
        .dispatch(getTeamChannels(id, { limit: group.teamChannelsLimit, offset: 0 }))
        .then(() => {
          let leadersOld =
            group && group.owners
              ? group.owners.map(item => {
                  return { id: item.id, name: this.concatLabel(item) };
                })
              : [];
          let adminsOld =
            group && group.subAdmins
              ? group.subAdmins.map(item => {
                  return { id: item.id, name: this.concatLabel(item) };
                })
              : [];
          let channelsOld =
            group &&
            group.teamChannels &&
            group.teamChannels.map(item => {
              return { id: item.id, name: item.label, type: 'channel' };
            });
          let membersOld;
          getUsersInAGroupV2(id, { user_type: 'member' })
            .then(data => {
              membersOld =
                data && data.members && data.members.users
                  ? data.members.users.map(item => {
                      return { id: item.id, name: this.concatLabel(item), email: item.email };
                    })
                  : [];
              let leadersCurrent = leadersOld.slice();
              let adminsCurrent = adminsOld.slice();
              let membersCurrent = membersOld.slice();
              let channelsCurrent = channelsOld.slice();
              this.setState({
                leadersCurrent,
                adminsCurrent,
                membersCurrent,
                channelsCurrent,
                leadersOld,
                adminsOld,
                membersOld,
                channelsOld
              });
            })
            .catch(e => console.error("Can't load initializedGroupUser"));
        })
        .catch(e => console.error("Can't load Team Channels"));
    }
  }

  closeModal = () => {
    this.props.dispatch(close());
  };
  changegroupImg = (e, accept, max, callback) => {
    e.preventDefault();
    this.fileStackHandler(accept, max, callback);
  };

  fileStackHandler = (accept, max, callback) => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: accept,
            maxFiles: max,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in GroupCreationModal.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModal.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  updateFileStackFile(fileStack) {
    let tempPath = fileStack.filesUploaded;
    let url = tempPath[0].url;
    let securedUrl;
    refreshFilestackUrl(url)
      .then(resp => {
        securedUrl = resp.signed_url;
        this.setState({
          fileStack: tempPath,
          securedUrl: securedUrl,
          showMainImage: tempPath.length
        });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModal.updateFileStackFile.refreshFilestackUrl.func : ${err}`
        );
      });
  }

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton aria-label={tr('Change Image')} style={{ paddingBottom: '0', height: 'auto' }}>
          <ImagePhotoCamera color="white" viewBox="0 0 24 24" />
        </IconButton>
        <div className="roll-text">{tr('Change Image')}</div>
      </span>
    );
  }

  displayImageInput() {
    let backgroundImg;
    if (this.state.showMainImage) {
      if (this.state.currentGroup) {
        backgroundImg =
          this.state.securedUrl ||
          (this.state.fileStack[0] && this.state.fileStack[0].url) ||
          this.state.currentGroup.imageUrls.medium;
      } else {
        backgroundImg = this.state.securedUrl || this.state.fileStack[0].url;
      }
    } else {
      backgroundImg = '';
    }

    return (
      <div>
        {!this.state.uiV2 && (
          <div>
            <strong>{tr('Upload a Banner Image (Optional)')}</strong>
            <br />
            <small>{tr('Recommended Size: 1440px x 300px')}</small>
          </div>
        )}
        {!this.state.showMainImage && (
          <div
            className="preview-banner"
            onClick={this.fileStackHandler.bind(
              this,
              ['image/*'],
              1,
              this.updateFileStackFile.bind(this)
            )}
          >
            <div className="text-center">
              <ImagePhotoCamera />
              <div className="empty-image">
                {this.state.uiV2 ? tr('Upload Group Image') : tr('Upload Image')}
              </div>
            </div>
          </div>
        )}
        {this.state.showMainImage && (
          <a
            href="#"
            onClick={e =>
              this.changegroupImg(e, ['image/*'], 1, this.updateFileStackFile.bind(this))
            }
            className="preview-banner preview-upload"
          >
            <div
              className="card-img-container"
              onMouseEnter={() => {
                this.setState({ mainImage: true });
              }}
              onMouseLeave={() => {
                this.setState({ mainImage: false });
              }}
            >
              <div
                className="card-img button-icon"
                style={{
                  ...this.styles.imageContainer,
                  ...{ backgroundImage: `url(\'${backgroundImg}\')` }
                }}
              >
                {this.hoverBlock(
                  this.fileStackHandler.bind(
                    this,
                    ['image/*'],
                    1,
                    this.updateFileStackFile.bind(this)
                  )
                )}
              </div>
            </div>
          </a>
        )}
        {this.state.uiV2 && <small>{tr('Recommended Size: 480px x 320px')}</small>}
      </div>
    );
  }

  createClickHandler = () => {
    this.setState({ pending: true });
    let imageUrl = this.state.fileStack.length ? this.state.fileStack[0].url : undefined;

    let is_mandatory = this.state.isMandatory;

    if (!this.state.groupIsOpen) {
      is_mandatory = this.state.workWithMandatoryGroup ? this.state.isMandatory : true;
    }

    if (this.state.groupIsOpen && is_mandatory) {
      is_mandatory = false;
    }

    let payload = {
      name: this.state.name,
      description: this.state.description,
      auto_assign_content: this.state.auto_assign_content,
      image: imageUrl,
      is_private: !this.state.groupIsOpen || false,
      is_mandatory: is_mandatory // group may be mandatory only when group is private
    };
    if (this.state.isForUpdate) {
      this.props
        .dispatch(updateGroupExtended(this.state.currentGroupId, payload))
        .then(() => {
          this.updateChannels()
            .then(() => {
              this.updateFieldContent('member', 'member')
                .then(() => {
                  this.updateFieldContent('sub_admin', 'admin')
                    .then(() => {
                      this.updateFieldContent('admin', 'leader')
                        .then(() => {
                          this.props
                            .dispatch(
                              getGroupDetail(this.props.groupsV2[this.state.currentGroupId].slug)
                            )
                            .then(() => {
                              this.props
                                .dispatch(
                                  getTeamCards(this.state.currentGroupId, 'teamAssignments', {
                                    limit: this.props.groupsV2[this.state.currentGroupId]
                                      .cardsLimit,
                                    offset: 0,
                                    type: 'assigned'
                                  })
                                )
                                .then(() => {
                                  this.props
                                    .dispatch(
                                      getTeamCards(this.state.currentGroupId, 'sharedCards', {
                                        limit: this.props.groupsV2[this.state.currentGroupId]
                                          .cardsLimit,
                                        offset: 0,
                                        type: 'shared'
                                      })
                                    )
                                    .then(() => {
                                      this.props
                                        .dispatch(
                                          getTeamChannels(this.state.currentGroupId, {
                                            limit: this.props.groupsV2[this.state.currentGroupId]
                                              .teamChannelsLimit,
                                            offset: 0
                                          })
                                        )
                                        .then(() => {
                                          this.closeModal();
                                        })
                                        .catch(err =>
                                          console.error(
                                            `Error in GroupCreationModal.createClickHandler.getTeamChannels.func member: ${err}`
                                          )
                                        );
                                    })
                                    .catch(err =>
                                      console.error(
                                        `Error in GroupCreationModal.getTeamSharedCards.func member: ${err}`
                                      )
                                    );
                                })
                                .catch(err =>
                                  console.error(
                                    `Error in GroupCreationModal.getTeamAssignmentsCards.func member: ${err}`
                                  )
                                );
                            })
                            .catch(err =>
                              console.error(
                                `Error in GroupCreationModal.createClickHandler.getGroupDetail.func member: ${err}`
                              )
                            );
                        })
                        .catch(err =>
                          console.error(
                            `Error in GroupCreationModal.updateFieldContent.func member: ${err}`
                          )
                        );
                    })
                    .catch(err =>
                      console.error(
                        `Error in GroupCreationModal.updateFieldContent.func sub_admin: ${err}`
                      )
                    );
                })
                .catch(err =>
                  console.error(`Error in GroupCreationModal.updateFieldContent.func admin: ${err}`)
                );
            })
            .catch(err =>
              console.error(`Error in GroupCreationModal.updateChannels.func : ${err}`)
            );
        })
        .catch(res => {
          this.setState({
            pending: false,
            createErrorText: (res && res.body && res.body.message) || res.statusText
          });
        });
    } else {
      createGroup(payload)
        .then(group => {
          this.setState({ currentGroupId: group.id }, () => {
            this.updateChannels()
              .then(() => {
                if (this.state.uiV2) {
                  this.updateAllFields(group.id)
                    .then(() => {
                      if (!!group && !!group.slug) {
                        this.props
                          .dispatch(getGroupDetail(group.slug))
                          .then(updated_group => {
                            this.props.updateGroupsList &&
                              this.props.updateGroupsList(updated_group);
                          })
                          .catch(err => console.error('Error while getGroupDetail: \n', err));
                      }
                      this.closeModal();
                    })
                    .catch(err => console.error('Error while adding users to new group: \n', err));
                } else {
                  this.closeModal();
                }
                if (!!group && !!group.slug) {
                  setTimeout(() => {
                    let link = `/teams/${group.slug}`;
                    let isRedirect = false;
                    if (window.location.pathname && !~window.location.pathname.indexOf('/teams/')) {
                      this.props.dispatch(push(link));
                      isRedirect = true;
                    }
                    let compoundMessage = ['Your Group has been created!'];
                    if (this.state.newModalAndToast) {
                      this.props.dispatch(
                        openSnackBar(compoundMessage[0], () => {
                          isRedirect
                            ? !this.state.uiV2 &&
                              this.props.updateGroupsList &&
                              this.props.updateGroupsList(group)
                            : (window.location.pathname = link);
                        })
                      );
                    } else {
                      this.props.dispatch(
                        openStatusModal(
                          '',
                          () => {
                            isRedirect
                              ? !this.state.uiV2 &&
                                this.props.updateGroupsList &&
                                this.props.updateGroupsList(group)
                              : (window.location.pathname = link);
                          },
                          compoundMessage
                        )
                      );
                    }
                  }, 500);
                }
              })
              .catch(err =>
                console.error(`Error in GroupCreationModal.updateChannels.func : ${err}`)
              );
          });
        })
        .catch(res => {
          this.setState({
            pending: false,
            createErrorText: (res && res.body && res.body.message) || res.statusText
          });
        });
    }
  };

  getRemoveTagsIds = itemsType => {
    return this.state[`${itemsType}sRemove`].map(item => item.id);
  };
  getNewTagsIds = itemsType => {
    return this.state[`${itemsType}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('isNew');
      })
      .map(item => item.id);
  };

  updateFieldContent(role, fieldName) {
    return new Promise(resolve => {
      let newAdminIds = this.getNewTagsIds(fieldName);
      let oldIds = this.getRemoveTagsIds(fieldName);
      let pendingUsers =
        (this.props.groupsV2 &&
          this.props.groupsV2.currentGroupID &&
          this.props.groupsV2[this.props.groupsV2.currentGroupID] &&
          this.props.groupsV2[this.props.groupsV2.currentGroupID].pending) ||
        [];
      let pendingIds = _.map(pendingUsers, 'id');
      if (!!newAdminIds.length) {
        let invitedList = [];
        let changeRoleList = [];
        let changePendingRoleList = [];
        _.forEach(newAdminIds, item => {
          if (
            _.indexOf(_.map(this.state.membersCurrent, member => member.id), item) === -1 &&
            _.indexOf(_.map(this.state.adminsCurrent, admin => admin.id), item) === -1 &&
            _.indexOf(_.map(this.state.leadersCurrent, leader => leader.id), item) === -1 &&
            _.indexOf(pendingIds, item) === -1
          ) {
            invitedList.push(
              _.find(this.state[`${fieldName}sCurrent`], elem => {
                return elem.id == item;
              })
            );
          } else if (_.indexOf(pendingIds, item) === -1) {
            changeRoleList.push(item);
          } else {
            changePendingRoleList.push(item);
          }
        });
        if (invitedList.length > 0) {
          let invited = invitedList.map(item => item.email);
          this.props.dispatch(
            invite({
              emails: invited,
              roles: role,
              id: this.state.currentGroupId,
              userList: invitedList
            })
          );
        }
        if (changeRoleList.length > 0) {
          let data = { groupID: this.state.currentGroupId, ids: changeRoleList, role: role };
          this.props
            .dispatch(updateMembers(data))
            .then(() => {
              if (!oldIds.length) {
                resolve();
              }
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModal.updateFieldContent.updateMembers.func : ${err}`
              )
            );
        }
        if (changePendingRoleList.length > 0) {
          let payload = { id: changePendingRoleList, isPending: true };
          this.props
            .dispatch(deleteUserFromTeam(payload, this.state.currentGroupId))
            .then(() => {
              let userList = [];
              let emailList = [];
              _.forEach(changePendingRoleList, item => {
                userList.push(
                  _.find(pendingUsers, elem => {
                    return elem.id == item;
                  })
                );
              });
              emailList = userList.map(user => user.email);
              this.props.dispatch(
                invite({ emails: emailList, roles: role, id: this.state.currentGroupId, userList })
              );
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModal.updateFieldContent.deleteUserFromTeam.func : ${err}`
              )
            );
        }
      }
      if (!!oldIds.length) {
        if (role === 'member') {
          let payload = {
            user_ids: oldIds
          };
          deleteUsersFromTeam(payload, this.state.currentGroupId)
            .then(() => {
              resolve();
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModal.updateFieldContent.deleteUsersFromTeam.func : ${err}`
              )
            );
        } else {
          deleteTeamUsers(this.state.currentGroupId, { user_ids: oldIds, role: role })
            .then(() => {
              resolve();
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModal.updateFieldContent.deleteTeamUsers.func : ${err}`
              )
            );
        }
      }
      if (!newAdminIds.length && !oldIds.length) {
        resolve();
      }
    });
  }
  updateChannels = async () => {
    return new Promise(resolve => {
      let newChannesIds = this.state.channelsCurrent
        .filter(item => {
          return item.hasOwnProperty('isNew');
        })
        .map(item => item.id);
      let oldChannelsIds = this.state.channelsRemove.map(item => item.id);
      let allPromises = [];
      if (!!newChannesIds.length) {
        newChannesIds.map(channel => {
          let payload = {
            channel: { followers: { group_ids: [this.state.currentGroupId] } }
          };
          allPromises.push(
            new Promise((resolve2, reject) => {
              channelSDK
                .addFollowers(channel, payload)
                .then(() => {
                  resolve2();
                })
                .catch(err => {
                  console.error(`Error in GroupCreationModal.addFollowers.func : ${err}`);
                });
            })
          );
        });
      }
      if (!!oldChannelsIds.length) {
        oldChannelsIds.map(channel => {
          allPromises.push(
            new Promise((resolve3, reject) => {
              channelSDK
                .groupsUnfollowChannel(channel, { 'ids[]': this.state.currentGroupId })
                .then(() => {
                  resolve3();
                })
                .catch(err => {
                  console.error(`Error in GroupCreationModal.groupsUnfollowChannel.func : ${err}`);
                });
            })
          );
        });
      }
      Promise.all(allPromises)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in GroupCreationModal.Promise.all.func : ${err}`);
        });
    });
  };

  updateAllFields = async id => {
    let roles = { member: 'member', leader: 'admin', admin: 'sub_admin' };
    for (let item in roles) {
      if (roles.hasOwnProperty(item)) {
        let newAdminIds = this.getNewTagsIds(item);
        if (newAdminIds && !!newAdminIds.length) {
          let invitedList = [];
          _.forEach(newAdminIds, user => {
            invitedList.push(
              _.find(this.state[`${item}sCurrent`], elem => {
                return elem.id == user;
              })
            );
          });
          let invited = invitedList.map(user => user.email);
          await this.props.dispatch(
            invite({
              emails: invited,
              roles: roles[item],
              id: this.state.currentGroupId,
              userList: invitedList
            })
          );
          this.updateChannels();
        }
      }
    }
  };

  handleContentAutoAssignment = event => {
    this.setState({
      auto_assign_content: event.target.checked
    });
  };

  handleOpenGroup = () => {
    this.setState(prevState => {
      return { groupIsOpen: !prevState.groupIsOpen };
    });
  };

  handleMandatoryGroup = () => {
    this.setState(prevState => {
      return { isMandatory: !prevState.isMandatory };
    });
  };

  handleCanShare = role => {
    this.setState(prevState => {
      return { [`${role}CanShare`]: !prevState[`${role}CanShare`] };
    });
  };

  handleCanAssign = role => {
    this.setState(prevState => {
      return { [`${role}CanAssign`]: !prevState[`${role}CanAssign`] };
    });
  };

  handleAddition(role, tag) {
    let itemsRemove = this.state[`${role}sRemove`];
    let oldList = _.remove(itemsRemove, function(item) {
      return item.id.toString() === tag.id.toString();
    });
    tag.type = role;
    if (oldList.length === 0) {
      tag.isNew = true;
    }
    let current = this.state[`${role}sCurrent`];
    current.push(tag);
    this.setState({ [`${role}sCurrent`]: current, [`${role}sRemove`]: itemsRemove });
  }

  handleDelete = (role, i) => {
    let current = this.state[`${role}sCurrent`].slice(0);
    let itemsRemove = this.state[`${role}sRemove`];
    let remove = current.splice(i, 1);
    if (remove[0] && !remove[0].isNew) {
      itemsRemove.push({ id: remove[0].id, name: this.concatLabel(remove[0]) });
    }
    this.setState({ [`${role}sCurrent`]: current, [`${role}sRemove`]: itemsRemove });
  };

  getOptions = (role, input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchUserText;
    let user_ids = this.state[`${role}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('email');
      })
      .map(item => {
        return item.id;
      });
    this.setState(
      {
        searchUserText: input,
        [`${role}sOptions`]: [],
        [`${role}sOffset`]:
          isScrollToBottom && !isChanged
            ? this.state[`${role}sOffset`] + this.state.suggestionsLimit
            : 0
      },
      () => {
        usersSDK
          .getItems({
            q: input,
            limit: this.state.suggestionsLimit || 10,
            offset: this.state[`${role}sOffset`] || 0,
            'exclude_user_ids[]': user_ids
          })
          .then(users => {
            let options = users.items.filter(
              item => !~_.findIndex(this.state[`${role}sCurrent`], user => +user.id === +item.id)
            );

            if (role === 'member') {
              options = users.items.filter(
                item =>
                  !~_.findIndex(this.state.adminsCurrent, admin => +admin.id === +item.id) &&
                  !~_.findIndex(this.state.leadersCurrent, leader => +leader.id === +item.id)
              );
            }
            options = options.map(item => {
              return { id: item.id, name: this.concatLabel(item), email: item.email };
            });
            this.setState({
              [`${role}sOptions`]: options
            });
          })
          .catch(err => {
            console.error(`Error in GroupCreationModal.usersSDK.getItems.func : ${err}`);
          });
      }
    );
  };
  getChannels = (isScrollToBottom, input) => {
    let isChanged = input !== this.state.searchUserText;
    this.setState(
      {
        searchUserText: input,
        channelsOptions: [],
        channelsOffset:
          isScrollToBottom && !isChanged
            ? this.state.channelsOffset + this.state.suggestionsLimit
            : 0
      },
      () => {
        if (this.state.isForUpdate) {
          channelsV2Sdk
            .getAddableChannels(this.state.currentGroupId, {
              q: input,
              limit: this.state.suggestionsLimit || 10,
              offset: this.state.channelsOffset || 0
            })
            .then(data => {
              let options = data.channels.filter(
                item =>
                  !~_.findIndex(this.state.channelsCurrent, channel => +channel.id === +item.id)
              );
              options = options.map(item => {
                return { id: item.id, name: item.label };
              });
              this.setState({
                channelsOptions: options
              });
            })
            .catch(err => {
              console.error(`Error in GroupCreationModal.getChannels.func : ${err}`);
            });
        } else {
          channelsV2Sdk
            .fetchChannels({
              q: input,
              limit: this.state.suggestionsLimit || 10,
              offset: this.state.channelsOffset || 0
            })
            .then(data => {
              let options = data.filter(
                item =>
                  !~_.findIndex(this.state.channelsCurrent, channel => +channel.id === +item.id)
              );
              options = options.map(item => {
                return { id: item.id, name: item.label };
              });
              this.setState({
                channelsOptions: options
              });
            })
            .catch(err => {
              console.error(`Error in GroupCreationModal.getSearchChannels.func : ${err}`);
            });
        }
      }
    );
  };

  concatLabel = item => {
    let label;
    if (item.firstName || item.lastName) {
      label = `${item.firstName ? item.firstName : ''} ${item.lastName ? item.lastName : ''} ${
        item.email
      }`;
    } else {
      label = `${item.name ? item.name : ''} ${item.email}`;
    }
    return label;
  };

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        showAdSettings: !prevState.showAdSettings
      };
    });
  };

  render() {
    let charForName = this.state.uiV2 ? 150 : 50;
    let charForDescription = this.state.uiV2 ? 2000 : 200;

    return (
      <div className="group-creation-modal">
        <div className="modal-header">
          <TextField autoFocus={true} name="groupcreation" className="hiddenTextField" />
          <span className="header-title">
            {this.state.uiV2 ? tr('Group') : tr('Create a Group')}
          </span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div
          className={`container-padding vertical-spacing-large ${
            this.state.uiV2 ? 'container__v2' : ''
          }`}
        >
          {this.state.uiV2 && (
            <div className="input-row__margin-bottom">{this.displayImageInput()}</div>
          )}

          <div className={this.state.uiV2 ? 'input-row__margin-bottom' : ''}>
            {!this.state.uiV2 && <strong>{tr('Name')}</strong>}
            <input
              disabled={this.state.isDynamic}
              className="group-name"
              type="text"
              placeholder={this.state.uiV2 ? `${tr('Title')}*` : ''}
              maxLength={charForName}
              value={this.state.name}
              onChange={e => {
                this.setState({ name: e.target.value, createErrorText: '' });
              }}
            />
            <small>{`${charForName - this.state.name.length}/${charForName} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
          <div className={this.state.uiV2 ? 'input-row__margin-bottom' : ''}>
            {!this.state.uiV2 && <strong>{tr('Description')}</strong>}
            {this.state.uiV2 ? (
              <input
                className="group-description"
                placeholder={`${tr('Description')}*`}
                maxLength={2000}
                value={this.state.description}
                onChange={e => {
                  this.setState({ description: e.target.value });
                }}
              />
            ) : (
              <textarea
                className="group-description"
                rows="4"
                maxLength={200}
                value={this.state.description}
                onChange={e => {
                  this.setState({ description: e.target.value });
                }}
              />
            )}
            <small>{`${charForDescription -
              this.state.description.length}/${charForDescription} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
          {this.state.uiV2 && (
            <div>
              <div className="input-tags-row_v2">
                <ReactTags
                  tags={this.state.leadersCurrent}
                  suggestions={this.state.leadersOptions}
                  placeholder={this.state.leadersCurrent.length ? '' : tr('Add Group Leader')}
                  handleAddition={this.handleAddition.bind(this, 'leader')}
                  handleDelete={this.handleDelete.bind(this, 'leader')}
                  handleInputChange={this.getOptions.bind(this, 'leader')}
                  tagComponent={CustomTag}
                  autofocus={false}
                />
                {!!this.state.leadersCurrent.length && <small>{tr('Group Leader')}</small>}
              </div>
              <div className="input-tags-row_v2">
                <ReactTags
                  tags={this.state.adminsCurrent}
                  suggestions={this.state.adminsOptions}
                  placeholder={this.state.adminsCurrent.length ? '' : tr('Add Group Admin')}
                  handleAddition={this.handleAddition.bind(this, 'admin')}
                  handleDelete={this.handleDelete.bind(this, 'admin')}
                  handleInputChange={this.getOptions.bind(this, 'admin')}
                  tagComponent={CustomTag}
                  autofocus={false}
                />
                {!!this.state.adminsCurrent.length && <small>{tr('Group Admin')}</small>}
              </div>
              <div className="input-tags-row_v2">
                <ReactTags
                  tags={this.state.membersCurrent}
                  suggestions={this.state.membersOptions}
                  placeholder={this.state.membersCurrent.length ? '' : tr('Add Group Member')}
                  handleAddition={this.handleAddition.bind(this, 'member')}
                  handleDelete={this.handleDelete.bind(this, 'member')}
                  handleInputChange={this.getOptions.bind(this, 'member')}
                  tagComponent={CustomTag}
                  autofocus={false}
                />
                {!!this.state.membersCurrent.length && <small>{tr('Group Members')}</small>}
              </div>
              <div className="input-tags-row_v2">
                <ReactTags
                  tags={this.state.channelsCurrent}
                  suggestions={this.state.channelsOptions}
                  placeholder={this.state.channelsCurrent.length ? '' : tr('Add Channels')}
                  handleAddition={this.handleAddition.bind(this, 'channel')}
                  handleDelete={this.handleDelete.bind(this, 'channel')}
                  handleInputChange={this.getChannels.bind(this, 'channel')}
                  tagComponent={CustomTag}
                  autofocus={false}
                />
                {!!this.state.channelsCurrent.length && <small>{tr('Channels')}</small>}
              </div>
              {false && (
                <div className="input-row__margin-bottom">
                  <span className="group__inline-text">{tr('Who can Share')}:</span>
                  <Checkbox
                    className="group__checkbox group__checkbox-inline group__checkbox-inline_small"
                    checked={this.state.adminCanShare}
                    onCheck={this.handleCanShare.bind(this, 'admin')}
                    label={tr('Admin(s)')}
                    aria-label={tr('Admin(s)')}
                  />
                  <Checkbox
                    className="group__checkbox group__checkbox-inline group__checkbox-inline_small"
                    checked={this.state.memberCanShare}
                    onCheck={this.handleCanShare.bind(this, 'member')}
                    label={tr('Member(s)')}
                    aria-label={tr('Member(s)')}
                  />
                </div>
              )}
              {false && (
                <div className="input-row__margin-bottom">
                  <span className="group__inline-text">{tr('Who can Assign')}:</span>
                  <Checkbox
                    className="group__checkbox group__checkbox-inline group__checkbox-inline_small"
                    checked={this.state.adminCanAssign}
                    onCheck={this.handleCanAssign.bind(this, 'admin')}
                    label={tr('Admin(s)')}
                    aria-label={tr('Admin(s)')}
                  />
                  <Checkbox
                    className="group__checkbox group__checkbox-inline group__checkbox-inline_small"
                    checked={this.state.memberCanAssign}
                    onCheck={this.handleCanAssign.bind(this, 'member')}
                    label={tr('Member(s)')}
                    aria-label={tr('Member(s)')}
                  />
                </div>
              )}
            </div>
          )}

          <div>
            {this.state.uiV2 && !this.state.isDynamic && (
              <Checkbox
                className={this.state.uiV2 ? 'group__checkbox' : ''}
                checked={this.state.groupIsOpen}
                onCheck={this.handleOpenGroup.bind(this)}
                label={tr('Open Group')}
                aria-label={tr('Open Group (Can be Searched)')}
              />
            )}
            <div className="advanced-settings">
              <a href="#" className="advanced-label" onClick={this.toggleAdvancedSettings}>
                {tr('Advanced Settings')}
              </a>
              {this.state.showAdSettings && (
                <div>
                  <Checkbox
                    className={this.state.uiV2 ? 'group__checkbox' : ''}
                    checked={this.state.auto_assign_content}
                    onCheck={this.handleContentAutoAssignment.bind(this)}
                    label={
                      this.state.uiV2
                        ? tr('Auto assign content of this group members')
                        : tr('Auto assign content of this group to group members')
                    }
                    aria-label={
                      this.state.uiV2
                        ? tr('Auto assign content of this group members')
                        : tr('Auto assign content of this group to group members')
                    }
                  />

                  {this.state.workWithMandatoryGroup &&
                    !this.state.groupIsOpen &&
                    !this.state.isDynamic && (
                      <Checkbox
                        className={this.state.uiV2 ? 'group__checkbox' : ''}
                        checked={this.state.isMandatory}
                        onCheck={this.handleMandatoryGroup.bind(this)}
                        label={tr('Mandatory group')}
                        aria-label={tr('Mandatory group')}
                      />
                    )}
                </div>
              )}
            </div>
          </div>
          {!this.state.uiV2 && this.displayImageInput()}
          <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
          <div className="action-bar">
            <SecondaryButton label={tr('Cancel')} className="cancel" onTouchTap={this.closeModal} />
            <PrimaryButton
              label={this.state.isForUpdate ? tr('Update') : tr('Create')}
              className="create"
              onTouchTap={this.createClickHandler}
              disabled={!this.state.description || !this.state.name}
              pending={this.state.pending}
              pendingLabel={this.state.isForUpdate ? tr('Updating...') : tr('Creating...')}
            />
          </div>
        </div>
      </div>
    );
  }
}

GroupCreationModal.propTypes = {
  groupsV2: PropTypes.object,
  modal: PropTypes.object,
  currentUser: PropTypes.object,
  updateGroupsList: PropTypes.func,
  groupId: PropTypes.any,
  pathname: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    modal: state.modal.toJS(),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}
export default connect(mapStoreStateToProps)(GroupCreationModal);
