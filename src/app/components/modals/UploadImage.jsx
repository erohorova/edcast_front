import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactCrop from 'react-image-crop';
import { tr } from 'edc-web-sdk/helpers/translations';
import { close } from '../../actions/modalActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import ImageIcon from 'material-ui/svg-icons/image/image';
import RotateRightIcon from 'material-ui/svg-icons/image/rotate-right';
import RotateLeftIcon from 'material-ui/svg-icons/image/rotate-left';
import colors from 'edc-web-sdk/components/colors/index';
import { uploadImage } from 'edc-web-sdk/requests/imageUpload';
import BlurImage from '../common/BlurImage';
import TextField from 'material-ui/TextField';

class UploadImage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      submitPending: false,
      uploadedImage: null,
      previewImage: null,
      crop: {
        x: 0,
        y: 0,
        aspect: props.sizes.maxWidth == 120 ? 1 : 2.5,
        width: 70,
        height: 70
      },
      maxHeight: props.sizes.maxHeight,
      maxWidth: props.sizes.maxWidth,
      errorMessage: null
    };
    this.styles = {
      avatarBox: {
        userSelect: 'none',
        height: '10rem',
        width: '10rem',
        position: 'relative'
      },
      avatarBoxSquare: {
        backgroundColor: 'rgb(188, 188, 188)',
        userSelect: 'none',
        width: '10rem'
      }
    };
  }

  cancelClickHandler = () => {
    this.props.dispatch(close());
  };

  dataURLtoFile = (dataUrl, filename) => {
    let arr = dataUrl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    let newFile = new Blob([u8arr], { type: mime });
    newFile.name = filename;
    return newFile;
  };

  createClickHandler = () => {
    if (this.state.previewImage && this.state.filename) {
      this.setState({ submitPending: true }, () => {
        uploadImage(this.dataURLtoFile(this.state.previewImage, this.state.filename))
          .then(newUrl => {
            this.setState({ submitPending: false }, () => {
              this.props.updatedImage(newUrl);
              this.props.dispatch(close());
            });
          })
          .catch(err => {
            console.error(`Error in UploadImage.uploadImage.func : ${err}`);
          });
      });
    } else {
      this.setState({
        errorMessage: 'Upload Image!'
      });
    }
  };

  replaceImageClickHandler = e => {
    e.preventDefault();
    this.setState({
      errorMessage: null
    });
    this._imageFileInput.click();
  };

  imageFileInputChangeHandler = e => {
    if (e.target.files.length) {
      if (e.target.files[0].size <= 15000000) {
        // accepts image size upto 15 Mb
        let reader = new FileReader();
        reader.onload = elem => {
          this.setState({ uploadedImage: elem.target.result });
        };
        this.setState({ filename: e.target.files[0].name });
        reader.readAsDataURL(e.target.files[0]);
        this._imageFileInput.value = '';
      } else {
        this.setState({ errorMessage: 'The image size should not exceed 15 MB' });
      }
    }
  };

  rotateImageAngle = degree => {
    let image = new Image();
    image.src = this.state.uploadedImage;
    let canvas = document.createElement('canvas');
    canvas.width = image.height;
    canvas.height = image.width;
    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.translate(image.height / 2, image.width / 2);
    ctx.rotate((degree * Math.PI) / 180);
    ctx.drawImage(image, -image.width / 2, -image.height / 2);
    let src = canvas.toDataURL('image/png');
    this.setState({ uploadedImage: src });
  };

  convertImage = () => {
    let image = new Image();
    image.src = this.state.uploadedImage;
    let imageWidth = image.naturalWidth;
    let imageHeight = image.naturalHeight;
    let cropX, cropY, cropWidth, cropHeight;
    let crop = this.state.crop;
    crop.aspect =
      this.props.imageType === 'channelImage' || this.props.imageType === 'groupImage'
        ? 1.6
        : this.props.imageType === 'avatar'
        ? 1
        : image.naturalWidth / image.naturalHeight;
    this.setState({ crop });
    if (this.state.crop.width && this.state.crop.height) {
      cropX = (this.state.crop.x / 100) * imageWidth;
      cropY = (this.state.crop.y / 100) * imageHeight;
      cropWidth = (this.state.crop.width / 100) * imageWidth;
      cropHeight = (this.state.crop.height / 100) * imageHeight;
    } else {
      cropX = 0;
      cropY = 0;
      cropWidth = imageWidth;
      cropHeight = imageHeight;
    }
    let destWidth = cropWidth;
    let destHeight = cropHeight;
    let canvas = document.createElement('canvas');
    canvas.width = destWidth;
    canvas.height = destHeight;
    let ctx = canvas.getContext('2d');
    ctx.drawImage(image, cropX, cropY, cropWidth, cropHeight, 0, 0, cropWidth, cropHeight);
    let src = canvas.toDataURL('image/png');
    this.setState({ previewImage: src });
  };

  onCropComplete = crop => {
    this.setState({ crop }, this.convertImage);
  };

  beforeConvert = crop => {
    if (
      this.props.imageType === 'channelImage' ||
      this.props.imageType === 'groupImage' ||
      this.props.imageType === 'avatar'
    ) {
      this.setState({ crop }, this.convertImage);
    } else {
      this.convertImage();
    }
  };

  rotateImage = angle => {
    this.rotateImageAngle(angle);
  };

  render() {
    return (
      <div id="uploadImage" className="vertical-spacing-large">
        <TextField name="skillmodal" autoFocus={true} className="hiddenTextField" />
        <div className="upload-image-content">
          <input
            type="file"
            accept="image/*"
            ref={node => (this._imageFileInput = node)}
            onChange={this.imageFileInputChangeHandler.bind(this)}
          />
          <div className="row">
            <div className="small-12 medium-12 columns">
              <div className={this.state.uploadedImage ? 'image-crop-area' : ''}>
                <h5>{tr('Adjust Image')}</h5>
                <small>
                  {tr(
                    'Drag the square to change position and size (the image size should not exceed 15 MB). '
                  )}
                  {this.state.uploadedImage && (
                    <a
                      href="#"
                      onClick={this.replaceImageClickHandler}
                      style={{ color: colors.primary }}
                      className="pointer"
                    >
                      {tr('Replace image.')}
                    </a>
                  )}
                </small>
                {this.state.uploadedImage && (
                  <div className="crop-block">
                    <ReactCrop
                      {...this.state}
                      src={this.state.uploadedImage}
                      onImageLoaded={crop => this.beforeConvert(crop)}
                      onComplete={crop => this.onCropComplete(crop)}
                    />

                    <div className="text-center">
                      <RotateLeftIcon
                        onClick={this.rotateImage.bind(this, -90)}
                        color="#bababa"
                        style={{ width: 40, height: 40 }}
                      />
                      <RotateRightIcon
                        onClick={this.rotateImage.bind(this, 90)}
                        color="#bababa"
                        style={{ width: 40, height: 40 }}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
            {!this.state.uploadedImage && (
              <div className="small-12">
                <a href="#" className="image-editor-block" onClick={this.replaceImageClickHandler}>
                  <div>
                    <ImageIcon color="#bababa" style={{ width: 80, height: 80 }} />
                    <div className="upload-text">{tr('Click to upload new image')}</div>
                  </div>
                </a>
              </div>
            )}
            {this.state.uploadedImage && (
              <div className="small-12 medium-5 columns">
                <div className="preview-block">
                  <h5>{tr('Preview')}</h5>
                  <small>{tr('How image appears. ')}</small>
                  <div className="crop-block">
                    {this.props.imageType == 'channelImage' ? (
                      <div style={this.styles.avatarBoxSquare}>
                        <img className="uploadImage upload-preview" src={this.state.previewImage} />
                      </div>
                    ) : (
                      <div>
                        {this.props.imageType === 'avatar' ? (
                          <BlurImage
                            style={this.styles.avatarBox}
                            id={1}
                            image={this.state.previewImage}
                          />
                        ) : (
                          <div>
                            <img
                              className="uploadImage upload-preview"
                              src={this.state.previewImage}
                            />
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                  <div />
                </div>
              </div>
            )}
            {this.state.errorMessage && (
              <div className="error-upload-image">{tr(this.state.errorMessage)}</div>
            )}
          </div>
        </div>
        <div className="actions-block clearfix">
          <div className="actions float-right">
            <SecondaryButton
              label={tr('Cancel')}
              className="close"
              onTouchTap={this.cancelClickHandler}
              theme="purple"
            />
            <PrimaryButton
              label={tr('Save')}
              pendingLabel={tr('Saving...')}
              className="create"
              pending={this.state.submitPending}
              onTouchTap={this.createClickHandler.bind(this)}
              theme="purple"
            />
          </div>
        </div>
      </div>
    );
  }
}

UploadImage.propTypes = {
  updatedImage: PropTypes.func,
  sizes: PropTypes.object,
  imageType: PropTypes.string
};

export default connect()(UploadImage);
