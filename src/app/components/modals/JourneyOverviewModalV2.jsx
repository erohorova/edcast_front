import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import unescape from 'lodash/unescape';
import { CardHeader } from 'material-ui/Card';
import CommentList from '../../components/feed/CommentList.v2';
import CreationDate from '../common/CreationDate';
import {
  startAssignment,
  updateCurrentCard,
  loadComments,
  toggleLikeCardAsync
} from '../../actions/cardsActions';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import RightArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import { close, closeCard, openCardStatsModal } from '../../actions/modalActions';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import MainInfoSmartBite from '../common/MainInfoSmartBite';
import { getResizedUrl } from '../../utils/filestack';
import linkPrefix from '../../utils/linkPrefix';

import Spinner from '../common/spinner';
import JourneyOverviewModalSectionV2 from './JourneyOverviewModalSectionV2';
import findIndex from 'lodash/findIndex';
import { reOpenCurateModal } from '../../actions/channelsActionsV2';
import { updateRelevancyRatingQ, checkRatedCard } from '../../actions/relevancyRatings';
import { markAsComplete } from 'edc-web-sdk/requests/cards.v2';
import { fetchPathwayCardsInsideJourney } from 'edc-web-sdk/requests/journeys';
import getCardType from '../../utils/getCardType';
import BlurImage from '../common/BlurImage';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import TextField from 'material-ui/TextField';
import JourneyCardOverviewModal from './JourneyCardOverviewModal';
import remove from 'lodash/remove';

class JourneyOverviewModalV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      journey: props.card,
      pendingLike: false,
      loadingPackCards: false,
      loadChecked: false,
      showCardOverview: false,
      checkedCard: null,
      currentIndex: 0,
      isUpvoted: props.card.isUpvoted,
      votesCount: props.card.votesCount,
      postThisCard: {},
      commentsCount: props.card.commentsCount,
      isOpenBlock: false,
      autoComplete: true,
      limit: 1000,
      offset: 0,
      journeyStarted: false,
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true),
      isMarkedAsCompleted: true,
      isInProgressToComplete: false
    };
    this.newComplete = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    let config = this.props.team.get('OrgConfig');
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;

    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      dropDownAction: {
        paddingRight: 0,
        width: 'auto'
      },
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      svgStyle: {
        filter: 'url(#blur-effect-1)'
      },
      userAvatar: {
        marginRight: 0,
        display: 'inline-block'
      },
      avatar: {
        marginRight: '9px'
      },
      popoverClose: {
        padding: 0,
        width: 'auto',
        height: 'auto',
        float: 'right'
      },
      cardHeader: {
        padding: '8px 0 0',
        float: 'left'
      },
      likeIcon: {
        paddingLeft: 0,
        width: 'auto'
      },
      leftArrow: {
        float: 'left',
        width: '48px',
        height: '48px',
        padding: 0,
        border: 0,
        cursor: 'pointer'
      },
      rightArrow: {
        float: 'right',
        width: '48px',
        height: '48px',
        padding: 0,
        border: 0,
        cursor: 'pointer'
      },
      primary: {
        minWidth: '144px'
      },
      tooltipActiveBts: {
        marginTop: -10,
        marginLeft: -10
      },
      avatarBox: {
        height: '1.625rem',
        width: '1.625rem',
        marginRight: '0.5625rem',
        position: 'relative'
      },
      actionIcon: {
        paddingLeft: 0,
        paddingRight: 0,
        width: 'auto'
      },
      pricing: {
        fontSize: '0.75rem',
        color: '#454560',
        margin: '10px 0px'
      }
    };
  }

  componentDidMount() {
    let autoComplete = true;
    if (this.newComplete) {
      switch (this.completeMethodConf) {
        case 'creatorChoose':
          autoComplete =
            this.state.journey.autoComplete !== undefined ? this.state.journey.autoComplete : true;
          break;
        case 'creatorNotChoose':
          autoComplete = true;
          break;
        case 'disabledNext':
          autoComplete = false;
          break;
        default:
          // FIXME: implement default case
          break;
      }
    }
    this.setState({ autoComplete });
    this.fetchComments();
    if (this.props.checkedCardId && this.props.checkedCardId != this.props.card.id) {
      this.setState({ loadChecked: true });
      this.loadPackCards()
        .then(() => {
          this.chooseCard(
            this.state.journeySmartBites[this.props.modal.get('currentSection')].cards[
              this.props.checkedCardId - 1
            ],
            this.props.checkedCardId - 1,
            this.props.modal.get('currentSection')
          );
        })
        .catch(err => {
          console.error(`Error in JourneyOverviewModal.loadPackCards.func : ${err}`);
        });
    } else {
      this.loadPackCards();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState !== this.state) {
      return true;
    } else if (this.props.currentUser !== nextProps.currentUser) {
      return true;
    } else if (this.props.modal !== nextProps.modal) {
      return true;
    } else if (this.props.relevancyRating !== nextProps.relevancyRating) {
      return true;
    } else if (this.props.team !== nextProps.team) {
      return true;
    } else {
      return false;
    }
  }

  fetchComments = () => {
    let cardId = this.state.journey.cardId || this.state.journey.id;
    let isECL = /^ECL-/.test(cardId);
    if (!isECL && this.state.commentsCount) {
      loadComments(
        cardId,
        this.state.commentsCount,
        this.state.journey.cardType,
        cardId ? cardId : null
      )
        .then(
          data => {
            this.setState({ comments: data, showComment: true });
          },
          error => {
            console.warn(`error when loadComments for journey card ${cardId}. Error: ${error}`);
          }
        )
        .catch(err => {
          console.error(`Error in JourneyOverviewModal.loadComments.func : ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  };

  cardUpdated(card) {
    if (this.props.card) {
      fetchCard(this.props.card.id)
        .then(data => {
          this.setState({
            journey: data
          });
          this.props.cardUpdated && this.props.cardUpdated();
        })
        .catch(err => {
          console.error(`Error in JourneyOverviewModal.cardUpdated.fetchCard.func : ${err}`);
        });
    }
  }

  smartBiteUpdated = (id, index, section, isPrivate) => {
    if (isPrivate) {
      return new Promise(resolve => {
        resolve();
      });
    } else {
      return new Promise(resolve => {
        if (id) {
          fetchCard(id)
            .then(data => {
              let journeySmartBites = this.state.journeySmartBites;
              data.isCompleted =
                data.completionState && data.completionState.toUpperCase() === 'COMPLETED';
              journeySmartBites[section].cards[index] = data;
              this.setState(
                {
                  journeySmartBites,
                  checkedCard: journeySmartBites[section].cards[index]
                },
                this.props.dispatch(updateCurrentCard(data))
              );
              resolve();
            })
            .catch(err => {
              console.error(
                `Error in JourneyOverviewModal.smartBiteUpdated.fetchCard.func : ${err}`
              );
            });
        }
      });
    }
  };

  closeModal = async () => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    this.props.cardUpdated && this.props.cardUpdated();
    if (this.state.showCardOverview) {
      let index = this.state.currentIndex;
      let section = this.state.currentSection;
      let card = this.state.journeySmartBites[section].cards[index];
      this.changeCompleteStatus(card.id, index, section, card.isPrivate);
    }
    this.props.dispatch(closeCard());
    if (
      !checkRatedCard(this.props.card, this.props.relevancyRating.get('ratedQueue')) &&
      this.state.relevancyRatings
    ) {
      this.props.dispatch(updateRelevancyRatingQ(this.props.card));
    }
    if (this.props.modal.get('dataBefore') === undefined) {
      let channelsV2JSON = this.props.channelsV2.toJS();
      let channelId = channelsV2JSON.activeChannelId;
      let cardsData = {
        cards: channelsV2JSON[channelId].curateCards,
        total: channelsV2JSON[channelId].curatedCardsTotal
      };
      this.props.dispatch(reOpenCurateModal(channelsV2JSON[channelId], cardsData));
    }
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise(resolve => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in JourneyOverviewModal.asyncDispatch.func : ${err}`);
        });
    });
  };

  openBlock = index => {
    let journeySmartBites = this.state.journeySmartBites;
    journeySmartBites[index].isOpenBlock = !this.state.journeySmartBites[index].isOpenBlock;
    this.setState({ journeySmartBites });
  };

  likeCardHandler = () => {
    if (this.state.pendingLike) {
      return;
    }
    this.setState({ pendingLike: true }, () => {
      this.asyncDispatch(
        toggleLikeCardAsync,
        this.state.journey.id,
        this.state.journey.cardType,
        !this.state.isUpvoted
      )
        .then(() => {
          this.cardUpdated();
          this.setState({
            votesCount: Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0),
            isUpvoted: !this.state.isUpvoted,
            pendingLike: false
          });
        })
        .catch(err => {
          console.error(`Error in JourneyOverviewModal.toggleLikeCardAsync.func : ${err}`);
        });
    });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count }, () => {
      if (count === -1) {
        this.setState({ showComment: false }, this.fetchComments);
      }
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}` ||
      this.props.isStandaloneModal
    ) {
      return;
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };

  fetchCardById = id => {
    return fetchCard(id)
      .then(data => {
        return data;
      })
      .catch(err => {
        return err;
      })
      .catch(err => {
        console.error(`Error in JourneyOverviewModal.fetchCardById.fetchCard.func : ${err}`);
      });
  };

  linkToPush = (card, handle, e) => {
    e && e.preventDefault();
    this.props.dispatch(close());
    closeCard();
    if (card) {
      this.props.cardUpdated && this.props.cardUpdated();
      this.standaloneLinkClickHandler(card);
    } else if (handle) {
      this.props.dispatch(push(`/${handle}`));
    }
  };

  isWeekly = i => {
    let cardDate = new Date(this.state.journey.journeySection[i].start_date);
    let nowDate = new Date();
    let nextSevenDay = new Date(cardDate);
    let dateValue = nextSevenDay.getDate() + 7;
    nextSevenDay.setDate(dateValue);
    return (
      this.state.journey.cardSubtype === 'weekly' &&
      !(nowDate >= cardDate && nowDate <= nextSevenDay)
    );
  };

  uploadChildJourneyCards = (i, j, data, isOwner, card) => {
    return new Promise(resolve => {
      if (!isOwner && this.isWeekly(i)) {
        data.cards.push({ locked: true, isLocked: false });
        resolve();
      } else {
        if (card && card.message === 'You are not authorized to access this card') {
          card.isPrivate = true;
        }
        data.cards[j] = card;
        resolve();
      }
    });
  };

  uploadJourneySectionCards = (i, journeySmartBites, isOwner) => {
    return new Promise(resolve => {
      let data = this.state.journey.journeySection[i];
      data.cards = [];
      journeySmartBites[i] = data;
      let countChild = 0;
      let onDemandFields =
        'channel_ids,non_curated_channel_ids,channels,teams,paid_by_user,users_with_access,completed_percentage,file_resources,channels(id,label),teams(id,name)';
      let payload = {
        pathway_id: data.id,
        is_standalone_page: false,
        limit: this.state.limit,
        offset: this.state.offset,
        fields: onDemandFields
      };
      if (this.state.journey.journeySection[i].visible) {
        fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
          .then(cardData => {
            data.totalCount = cardData.totalCount;
            if (cardData.cards.length === 0) {
              resolve();
            }
            for (let k = 0; k < cardData.cards.length; k++) {
              this.uploadChildJourneyCards(i, k, data, isOwner, cardData.cards[k])
                .then(() => {
                  countChild++;
                  if (countChild === cardData.cards.length) {
                    resolve();
                  }
                })
                .catch(err => {
                  console.error(
                    `Error in JourneyOverviewModalV2.uploadJourneySectionCards.uploadChildJourneyCards.func : ${err}`
                  );
                });
            }
          })
          .catch(err => {
            console.error(
              `Error in JourneyOverviewModalV2.uploadJourneySectionCards.fetchPathwayCardsInsideJourney.func : ${err}`
            );
          });
      } else {
        resolve();
      }
    });
  };

  loadPackCards = () => {
    return new Promise(resolve => {
      if (this.state.journey.journeySection && this.state.journey.journeySection.length) {
        this.setState({ loadingPackCards: true });
        let isOwner =
          this.state.journey.author &&
          this.state.journey.author.id == this.props.currentUser.get('id');
        let journeySmartBites = [];
        let countCard = 0;
        for (let i = 0; i < this.state.journey.journeySection.length; i++) {
          this.uploadJourneySectionCards(i, journeySmartBites, isOwner)
            .then(() => {
              countCard++;
              let lockedIndex = findIndex(
                this.state.journey.journeySection[i].cards,
                item => !!item.locked
              );
              if (lockedIndex > -1) {
                journeySmartBites[i].cards[lockedIndex].isLocked = true;
                journeySmartBites[i].cards = this.checkToShowLocked(
                  journeySmartBites[i].cards,
                  lockedIndex
                );
              }
              if (countCard === this.state.journey.journeySection.length) {
                journeySmartBites = remove(journeySmartBites, obj => {
                  return obj.visible === true;
                });
                this.setState(
                  {
                    loadChecked: false,
                    loadingPackCards: false,
                    journeySmartBites
                  },
                  resolve
                );
              }
            })
            .catch(err => {
              console.error(
                `Error in JourneyOverviewModal.loadPackCards.uploadSectionCards.func : ${err}`
              );
            });
        }
      }
    });
  };

  chooseCard = (card, i, section, e) => {
    if (e && e.target && e.target.name === 'marked-link') {
      return;
    }
    this.setState({
      currentIndex: i,
      currentSection: section,
      showCardOverview: true,
      loadChecked: false,
      checkedCard: card
    });
  };

  setStartedReview = () => {
    let isStandalone = !!~this.props.pathname.indexOf('journey/');
    if (this.state.journey.completionState === null && isStandalone && !this.state.journeyStarted) {
      startAssignment(this.state.journey.id)
        .then(userContentCompletion => {
          if (userContentCompletion && userContentCompletion.state === 'started') {
            this.setState({
              journeyStarted: true
            });
          }
        })
        .catch(err => {
          console.error(`Error in JourneyOverviewModalV2.setStartedReview.func : ${err}`);
        });
    }
  };

  arrowClick = async direction => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    this.setState({ loadingPackCards: false });
    let section = this.state.currentSection;
    let index = this.state.currentIndex;
    let card = this.state.journeySmartBites[section].cards[index];
    let cards = this.state.journeySmartBites[section].cards;
    let cardType = getCardType(card);
    if (
      cardType === 'QUIZ' &&
      card.attemptedOption &&
      card.leaps &&
      card.leaps.inPathways &&
      direction === 1
    ) {
      let correctAnswer = card.quizQuestionOptions.find(obj => {
        return obj.isCorrect === true;
      });
      let selected = card.attemptedOption && card.attemptedOption.id;
      let result = correctAnswer && selected === correctAnswer.id;

      let inPathwayLeap = card.leaps.inPathways.find(
        item =>
          item.pathwayId ==
          (this.state.journeySmartBites[section].id ||
            this.state.journeySmartBites[section].card_id)
      );
      let nextCardId = result ? inPathwayLeap.correctId : inPathwayLeap.wrongId;
      if (nextCardId) {
        let nextCard = cards.find(item => item.id == nextCardId);
        let newIndex = cards.indexOf(nextCard);
        this.changeCompleteStatus(nextCardId, newIndex, section, nextCard.isPrivate);
        this.setState({
          currentIndex: newIndex,
          checkedCard: cards[newIndex]
        });
      }
    } else {
      if (index === 0 && direction === -1) {
        this.backToJourney();
      } else if (
        index + 1 === this.state.journeySmartBites[section].cards.length &&
        section + 1 === this.state.journeySmartBites.length &&
        direction === 1
      ) {
        this.linkToPush(this.state.journey);
      } else if (
        this.props.modal.get('inStandalone') &&
        index + 1 === this.state.journeySmartBites[section].cards.length &&
        section !== this.state.journeySmartBites.length &&
        direction === 1
      ) {
        let nextSection = this.state.currentSection + direction;
        let nextIndex = 0;
        let nextCard = this.state.journeySmartBites[nextSection].cards[nextIndex];
        this.changeCompleteStatus(nextCard.id, nextIndex, nextSection, nextCard.isPrivate);
        this.setState({
          currentIndex: nextIndex,
          currentSection: nextSection,
          checkedCard: this.state.journeySmartBites[nextSection].cards[nextIndex]
        });
      } else if (
        index + 1 === this.state.journeySmartBites[section].cards.length &&
        section !== this.state.journeySmartBites.length &&
        direction === 1
      ) {
        this.backToJourney();
      } else {
        let newIndex = this.state.currentIndex + direction;
        let oldIndex = this.state.currentIndex;
        if (newIndex >= 0 && newIndex <= this.state.journeySmartBites[section].cards.length - 1) {
          let prevCard = this.state.journeySmartBites[this.state.currentSection].cards[oldIndex];
          this.changeCompleteStatus(prevCard.id, oldIndex, section, prevCard.isPrivate);
          this.setState({
            currentIndex: newIndex,
            checkedCard: this.state.journeySmartBites[section].cards[newIndex]
          });
          try {
            if (this.state.checkedCard.id) {
              recordVisit(this.state.checkedCard.id);
            }
          } catch (e) {}
        }
      }
    }
  };

  changeCompleteStatus = (id, i, section, isPrivate) => {
    let journeySmartBites = this.state.journeySmartBites;
    if (
      this.state.completedCardStatus &&
      this.state.completedCardStatus.completableId == id &&
      !this.state.checkedCard.isLocked
    ) {
      journeySmartBites[section].cards[i].isCompleted = true;
      journeySmartBites[section].cards[i].completionState = 'COMPLETED';
    }
    this.setState(
      {
        journeySmartBites,
        checkedCard: journeySmartBites[section].cards[i]
      },
      this.props.dispatch(updateCurrentCard(journeySmartBites[section].cards[i]))
    );
  };

  backToJourney = () => {
    let index = this.state.currentIndex;
    let section = this.state.currentSection;
    let card = this.state.journeySmartBites[section].cards[index];
    this.changeCompleteStatus(card.id, index, section, card.isPrivate);
    this.setState({
      currentIndex: 0,
      showCardOverview: false,
      checkedCard: null
    });
  };

  async checkToCompletedCard() {
    return new Promise((resolve, reject) => {
      let isCompleted =
        this.state.checkedCard &&
        this.state.checkedCard.completionState &&
        this.state.checkedCard.completionState.toUpperCase() === 'COMPLETED';
      let cardType = getCardType(this.state.checkedCard);

      let isLinkCard =
        !this.state.showMarkAsComplete &&
        this.state.checkedCard &&
        this.state.checkedCard.resource &&
        (this.state.checkedCard.resource.url ||
          this.state.checkedCard.resource.description ||
          this.state.checkedCard.resource.fileUrl) &&
        this.state.checkedCard.resource.type !== 'Video' &&
        ((this.state.checkedCard.readableCardType &&
          this.state.checkedCard.readableCardType.toUpperCase() === 'ARTICLE') ||
          cardType === 'ARTICLE');

      if (
        !isCompleted &&
        this.state.autoComplete &&
        this.state.checkedCard &&
        (this.state.checkedCard.cardType === 'poll' ? this.state.checkedCard.hasAttempted : true) &&
        !this.state.checkedCard.isLocked &&
        !isLinkCard
      ) {
        try {
          this.setState({ isInProgressToComplete: true }, async () => {
            await markAsComplete(this.state.checkedCard.id, { state: 'complete' }).then(
              completedCardStatus => {
                let checkedCard = this.state.checkedCard;
                if (completedCardStatus && completedCardStatus.state) {
                  checkedCard.completionState = completedCardStatus.state.toUpperCase();
                }
                this.setState({
                  checkedCard: checkedCard,
                  completedCardStatus: completedCardStatus,
                  isMarkedAsCompleted: true,
                  isInProgressToComplete: false
                });
                resolve(true);
              }
            );
          });
        } catch (error) {
          console.error(`Error in JourneyOverviewModalV2.checkToCompletedCard.func : ${error}`);
          reject();
        }
      } else {
        resolve(true);
      }
    });
  }

  checkToShowLocked = (cards, lockedCardIndex) => {
    for (let ind = 0; ind < cards.length; ind++) {
      if (ind === lockedCardIndex) {
        continue;
      }
      if (cards[ind].isLocked) {
        break;
      }
      let showLocked = !!cards[lockedCardIndex].isLocked;
      cards[ind].showLocked = ind > lockedCardIndex ? showLocked : false;
    }
    return cards;
  };
  handleCardAnalyticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.journey));
  };

  getPricingPlans() {
    return this.state.journey.cardMetadatum && this.state.journey.cardMetadatum.plan
      ? tr(this.state.journey.cardMetadatum.plan)
      : tr('Free');
  }

  render() {
    let journey = this.state.journey;
    let cardSvgBackground =
      journey.filestack && journey.filestack.length
        ? getResizedUrl(journey.filestack[0].url, 'height:360')
        : this.props.defaultImage;
    let disableTopics = journey.tags.length > 0;
    let isOwner = journey.author && journey.author.id == this.props.currentUser.get('id');
    let isCompleted =
      journey.completionState && journey.completionState.toUpperCase() === 'COMPLETED';
    let checked = this.props.checkedCardId || this.state.checkedCardId;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let smartBites =
      this.state.journeySmartBites &&
      this.state.journeySmartBites.length &&
      this.state.currentSection !== undefined &&
      this.state.journeySmartBites[this.state.currentSection].cards;
    let smartBitesBeforeCurrent = smartBites && smartBites.slice(0, this.state.currentIndex);
    let isShowLockedCardContent =
      (this.state.currentIndex !== undefined && this.state.currentIndex === 0) ||
      (smartBitesBeforeCurrent &&
        smartBitesBeforeCurrent.length &&
        smartBitesBeforeCurrent.every(
          item => item.completionState && item.completionState.toUpperCase() === 'COMPLETED'
        ));
    let title = journey.title || journey.message;
    if (title) {
      title = title.replace(/amp;/gi, '');
    }

    return (
      <div className="card-overview">
        {this.state.showCardOverview && (
          <div>
            <div className="preview-arrows">
              {!(this.state.currentIndex === 0 && checked) && (
                <LeftArrow
                  color="#d6d6e1"
                  onClick={this.arrowClick.bind(null, -1)}
                  style={this.styles.leftArrow}
                />
              )}
              <RightArrow
                color="#d6d6e1"
                onClick={this.arrowClick.bind(null, 1)}
                style={this.styles.rightArrow}
              />
            </div>
            <div className="preview-count">
              {this.state.currentIndex + 1} /{' '}
              {this.state.journeySmartBites[this.state.currentSection].cards.length}
            </div>
          </div>
        )}
        <div className="card-overview-header">
          <span className="header-title">
            {tr('Journey')}{' '}
            {this.state.journey.state === 'draft' && (
              <span className="header-draft-label">{tr('Draft')}</span>
            )}
          </span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeBtn}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
          <TextField name="journeymodal" autoFocus={true} className="hiddenTextField" />
        </div>
        {this.state.loadChecked || this.state.isInProgressToComplete ? (
          <div className="text-center pathway-overview-spinner">
            <Spinner />
          </div>
        ) : this.state.showCardOverview ? (
          <div>
            <JourneyCardOverviewModal
              autoComplete={this.state.journey.autoComplete}
              showComment={
                Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')
              }
              card={this.state.checkedCard}
              dueAt={this.props.dueAt}
              startDate={this.props.startDate}
              cardUpdated={() => {
                this.smartBiteUpdated(
                  this.state.checkedCard.id,
                  this.state.currentIndex,
                  this.state.currentSection
                );
              }}
              isLock={
                this.state.checkedCard.isLocked ||
                this.state.checkedCard.showLocked ||
                (this.state.currentIndex >= this.state.lockedIndex && this.state.lockedIndex !== -1)
              }
              isPathwayOwner={isOwner}
              isShowLockedCardContent={isShowLockedCardContent}
              isPrivate={this.state.checkedCard.isPrivate}
              isCompleted={this.state.isMarkedAsCompleted}
            />
          </div>
        ) : (
          <div className="card-overview-content">
            <div className="row">
              <div className="small-12 medium-8 large-8 left-content card-modal-left-column">
                <a
                  href="#"
                  className="main-image anchorAlignment"
                  onClick={e => {
                    this.linkToPush(journey, null, e);
                  }}
                >
                  <div className="card-blurred-background">
                    <svg id="svg-image-blur" width="100%" height="100%">
                      <title>{unescape(title)}</title>
                      <image
                        id="svg-image"
                        style={this.styles.svgStyle}
                        xlinkHref={cardSvgBackground}
                        x="-30%"
                        y="-30%"
                        width="160%"
                        height="160%"
                      />
                      <filter id="blur-effect-1">
                        <feGaussianBlur stdDeviation="10" />
                      </filter>
                    </svg>
                  </div>
                  <svg width="100%" height="100%" style={this.styles.mainSvg}>
                    <title>{unescape(title)}</title>
                    <image
                      xlinkHref={cardSvgBackground}
                      width="100%"
                      style={this.styles.svgImage}
                      height="100%"
                    />
                  </svg>
                </a>
                <div className="top-block clearfix">
                  {journey.author && (
                    <CardHeader
                      title={
                        <div className="channels-block">
                          <div className="author-info-container modal-author">
                            <a
                              href="#"
                              className="user-name"
                              onClick={e => {
                                this.linkToPush(null, journey.author.handle, e);
                              }}
                            >
                              {`${journey.author.fullName ? journey.author.fullName : ''}`}
                            </a>
                            <br />
                            <CreationDate
                              card={journey}
                              standaloneLinkClickHandler={this.standaloneLinkClickHandler.bind(
                                this,
                                journey
                              )}
                            />
                          </div>
                        </div>
                      }
                      subtitle={
                        journey.publishedAt && (
                          <div className="header-secondary-text">
                            <span className="matte" />
                          </div>
                        )
                      }
                      avatar={
                        <div
                          style={this.styles.userAvatar}
                          onClick={this.linkToPush.bind(null, null, journey.author.handle)}
                        >
                          <BlurImage
                            style={this.styles.avatarBox}
                            id={journey.id}
                            image={
                              journey.author &&
                              (journey.author.picture ||
                                (journey.author.avatarimages &&
                                  journey.author.avatarimages.small) ||
                                defaultUserImage)
                            }
                          />
                        </div>
                      }
                      style={this.styles.cardHeader}
                    />
                  )}
                </div>
                <div style={this.styles.pricing}>
                  <span>Price : </span>
                  <span className="pathway-paid-label">
                    {this.state.journey.isPaid ? tr('Paid') : this.getPricingPlans()}
                  </span>
                </div>
                <MainInfoSmartBite
                  smartBite={this.state.journey}
                  isOwner={this.state.isOwner}
                  type="journey"
                  dueAt={this.props.dueAt}
                  startDate={this.props.startDate}
                  linkToPush={this.linkToPush.bind(null, journey, null)}
                  isOverviewModal={true}
                />
                <div className="pack-cards-block">
                  {this.state.loadingPackCards && (
                    <div className="text-center">
                      <Spinner />
                    </div>
                  )}
                  {!this.state.loadingPackCards && !!this.state.journey.journeySection.length && (
                    <div>
                      {this.state.journeySmartBites &&
                        this.state.journeySmartBites.map((obj, indexSection) => {
                          return (
                            <JourneyOverviewModalSectionV2
                              journeySection={obj}
                              indexSection={indexSection}
                              isPathwayOwner={isOwner}
                              isOwner={this.state.isOwner}
                              openBlock={this.openBlock}
                              chooseCard={this.chooseCard}
                              smartBiteUpdated={this.smartBiteUpdated}
                              linkToPush={this.linkToPush}
                            />
                          );
                        })}
                    </div>
                  )}
                </div>
              </div>
              <div className="comments-container small-12 medium-4 large-4">
                <div className="comments">
                  <div>
                    <small className="label">{tr('COMMENTS')}</small>
                  </div>
                  <div className="break-line" />
                  {this.state.showComment && (
                    <div>
                      <CommentList
                        cardId={journey.id + ''}
                        cardOwner={journey.author ? journey.author.id : ''}
                        cardType={journey.cardType}
                        comments={this.state.comments}
                        pending={journey.commentPending}
                        numOfComments={this.state.commentsCount}
                        overViewModal={true}
                        updateCommentCount={this.updateCommentCount}
                        videoId={journey.id ? journey.id : null}
                        inModal={true}
                      />
                    </div>
                  )}
                  <div className="modal-actions-bar">
                    <div className="border-element" />
                    {Permissions.has('LIKE_CONTENT') && (
                      <span>
                        <IconButton
                          style={this.styles.likeIcon}
                          tooltip={!this.state.isUpvoted ? tr('Like') : tr('Liked')}
                          tooltipStyles={this.styles.tooltipActiveBts}
                          onTouchTap={this.likeCardHandler}
                        >
                          {!this.state.isUpvoted && <LikeIcon />}
                          {this.state.isUpvoted && <LikeIconSelected />}
                        </IconButton>
                        <small className="votes-count">
                          {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : ''}
                        </small>
                      </span>
                    )}
                    {(isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                      <div className="icon-inline-block">
                        <IconButton
                          className="statistics"
                          style={this.styles.actionIcon}
                          tooltip={tr('Card Statistics')}
                          tooltipStyles={this.styles.tooltipActiveBts}
                          onTouchTap={this.handleCardAnalyticsModal}
                        >
                          <CardAnalyticsV2 />
                        </IconButton>
                      </div>
                    )}
                    <div className="float-right button-icon">
                      <InsightDropDownActions
                        card={this.state.journey}
                        author={this.state.journey.author}
                        dismissible={false}
                        style={this.styles.dropDownAction}
                        disableTopics={disableTopics}
                        isStandalone={false}
                        cardUpdated={this.cardUpdated.bind(this)}
                        assignable={false}
                        deleteSharedCard={this.props.deleteSharedCard}
                        isCompleted={isCompleted}
                        isOverviewModal={true}
                        type={'JourneyStandAlone'}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="preview-back" onClick={this.linkToPush.bind(null, journey, null)}>
          {tr('Journey Detail')}
        </div>
      </div>
    );
  }
}

JourneyOverviewModalV2.propTypes = {
  card: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  relevancyRating: PropTypes.object,
  channelsV2: PropTypes.object,
  checkedCardId: PropTypes.number,
  cardUpdated: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  pathname: PropTypes.string,
  isStandaloneModal: PropTypes.bool,
  defaultImage: PropTypes.string,
  dueAt: PropTypes.string,
  startDate: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser,
    modal: state.modal,
    channelsV2: state.channelsV2,
    relevancyRating: state.relevancyRating,
    team: state.team
  };
}

export default connect(mapStoreStateToProps)(JourneyOverviewModalV2);
