import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { close } from '../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import IconButton from 'material-ui/IconButton/IconButton';
import Paper from 'edc-web-sdk/components/Paper';
import AddToGroup from '../team/SingleTeam/AddToGroup';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { getAssignmentsMetricUsers } from 'edc-web-sdk/requests/assignments';
import _ from 'lodash';
import CircularProgress from 'material-ui/CircularProgress';
import colors from 'edc-web-sdk/components/colors/index';
import TextField from 'material-ui/TextField';
class TeamAnalyticsUserList extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      users: [],
      initialLoad: true,
      isLastPage: false,
      loadMore: false
    };

    this.styles = {
      paper: {
        padding: '14px'
      }
    };
  }

  fetchUsers = offset => {
    let contentID = this.props.metric.cardId;
    let groupID = this.props.groupAnalytic.groupID;
    let payload = {
      offset,
      limit: 15,
      content_id: contentID,
      team_id: groupID
    };

    switch (this.props.modalState) {
      case 'Total Assignees':
        payload['state[]'] = ['assigned', 'started', 'completed'];
        break;
      case 'Not Started':
        payload['state[]'] = ['assigned'];
        break;
      case 'Started':
        payload['state[]'] = ['started'];
        break;
      case 'Completed':
        payload['state[]'] = ['completed'];
        break;
      default:
        break;
    }

    getAssignmentsMetricUsers(payload)
      .then(data => {
        let users = [...this.state.users, ...data.users];
        this.setState({
          users,
          initialLoad: false,
          loadMore: false,
          isLastPage: data.isLastPage
        });
        offset === 0 &&
          document.getElementById('user-list') &&
          document.getElementById('user-list').addEventListener('scroll', this.handleScroll);
      })
      .catch(err => {
        console.error(`Error in TeamAnalyticsUserList.getAssignmentsMetricUsers.func : ${err}`);
      });
  };

  handleScroll = _.throttle(
    e => {
      if (
        !this.state.isLastPage &&
        e.target.clientHeight + e.target.scrollTop === e.target.scrollHeight
      ) {
        this.setState({ loadMore: true });
        this.fetchUsers(this.state.users.length);
      }
    },
    150,
    { leading: false }
  );

  closeModal = () => {
    this.props.dispatch(close());
  };

  componentDidMount() {
    this.fetchUsers(0);
  }

  componentWillUnmount() {
    document.getElementById('user-list').removeEventListener('scroll', this.handleScroll);
  }

  render() {
    let users = this.state.users;
    return (
      <div style={{ maxWidth: '436px', margin: '0 auto' }}>
        <div className="row">
          <TextField name="teamAnalytics" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '10px' }}>
            <div style={{ color: 'white', fontSize: '18px' }}>{tr(this.props.modalState)}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              <div>
                {this.state.initialLoad ? (
                  <div className="text-center">
                    <CircularProgress
                      size={80}
                      color={colors.primary}
                      thickness={5}
                      className="course-loader"
                    />
                    <div>Loading...</div>
                  </div>
                ) : (
                  <div className="row">
                    <div className="small-12 columns" id="user-list">
                      {users.map(user => {
                        return (
                          <div className="user-container">
                            <div className="user-avatar">
                              <img src={user.avatarimages.tiny} />
                            </div>
                            <div className="user-name">
                              {user.firstName} {user.lastName}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                    {this.state.loadMore && (
                      <div className="text-center loadmore-loader">
                        <CircularProgress
                          size={80}
                          color={colors.primary}
                          thickness={5}
                          className="course-loader"
                        />
                        <div>Loading...</div>
                      </div>
                    )}
                  </div>
                )}
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

TeamAnalyticsUserList.propTypes = {
  state: PropTypes.string,
  groupAnalytic: PropTypes.any,
  currentUser: PropTypes.any,
  metric: PropTypes.any,
  modalState: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    groupAnalytic: state.groupAnalytic.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(TeamAnalyticsUserList);
