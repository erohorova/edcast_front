/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { addCardToPathways } from '../../actions/pathwaysActions';
import colors from 'edc-web-sdk/components/colors/index';
import Paper from 'edc-web-sdk/components/Paper';
import getFormattedDateTime from '../../utils/getFormattedDateTime';
import Checkbox from 'material-ui/Checkbox';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { pathways } from 'edc-web-sdk/requests/index';
import { close } from '../../actions/modalActions';
import Spinner from '../common/spinner';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import concat from 'lodash/concat';
import uniqBy from 'lodash/uniqBy';
import { getAddableJourneys } from 'edc-web-sdk/requests/cards.v2';
import Select from 'react-select';
import TextField from 'material-ui/TextField';

class AddToPathwayModalNew extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      query: '',
      pathways: [],
      journeys: [],
      selectedPathway: [],
      selectedSections: {},
      buttonPending: false,
      sortBy: 'message',
      orderBy: 'asc'
    };
    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      checkbox: {
        color: colors.primary
      },
      header: {
        color: '#ffffff',
        width: '100%',
        position: 'absolute',
        top: '-25px',
        left: 0
      },
      close: {
        cursor: 'pointer',
        float: 'right',
        top: 0
      },
      table: {
        width: '100%',
        textAlign: 'center'
      },
      tableScroll: {
        maxHeight: '300px',
        display: 'block',
        overflowY: 'scroll'
      },
      firstColumn: {
        textAlign: 'left'
      },
      tdBorder: {
        borderLeft: '2px solid rgba(0,0,0,.06)'
      },
      tableSelectScroll: {
        maxHeight: '300px',
        display: 'block',
        overflowY: 'scroll',
        paddingBottom: '50px'
      },
      footerButtons: {
        marginTop: '10px'
      }
    };
    this.addClickHandler = this.addClickHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
  }

  componentDidMount() {
    if (this.props.isAddingToJourney) {
      getAddableJourneys(this.props.cardId)
        .then(journeys => {
          this.setState({
            journeys,
            isAll: journeys.length < 10,
            pending: false
          });
        })
        /*eslint handle-callback-err: "off"*/
        .catch(err => {
          this.setState({
            journeys: [],
            isAll: false,
            pending: false
          });
        });
    } else {
      pathways
        .getPathwaysToAddToNew(
          this.props.cardId,
          15,
          this.state.pathways.length,
          this.state.sortBy,
          this.state.orderBy
        )
        .then(pathwaysData => {
          this.setState({
            pathways: pathwaysData,
            isAll: pathwaysData.length < 10,
            pending: false
          });
        })
        .catch(err => {
          this.setState({
            pathways: [],
            isAll: false,
            pending: false
          });
        });
    }
  }

  selectPathway(pathwayId) {
    let selectedPathway = this.state.selectedPathway;
    if (~this.state.selectedPathway.indexOf(pathwayId)) {
      let index = this.state.selectedPathway.indexOf(pathwayId);
      selectedPathway.splice(index, 1);
    } else {
      selectedPathway.push(pathwayId);
    }

    this.setState({ selectedPathway });
  }
  addClickHandler() {
    this.setState({ buttonPending: true, err: false });
    if (this.props.isAddingToJourney) {
      let selectedSections = this.state.selectedSections;
      let sectionsArr = concat(...Object.values(selectedSections)).map(item => `${item.value}`);
      pathways
        .addToPathways(sectionsArr, this.props.cardId)
        .then(() => {
          this.setState({ buttonPending: false });
          this.closeModalHandler();
          this.props.dispatch(openSnackBar('Card successfully added to Journey(s)!', true));
        })
        .catch(() => {
          this.setState({
            err: true,
            buttonPending: false
          });
        });
    } else {
      pathways
        .addToPathways(this.state.selectedPathway, this.props.cardId)
        .then(() => {
          this.setState({ buttonPending: false });
          this.closeModalHandler();
          this.props.dispatch(openSnackBar('Card successfully added to Pathway(s)!', true));
          let payload = [];
          this.state.selectedPathway.map(pathwayId => {
            let current = this.state.pathways.find(x => x.id == pathwayId);
            if (current) {
              current.packCards && current.packCards.push(this.props.card);
              payload.push({
                id: current.id,
                pathway: current
              });
            }
          });
          payload.length && this.props.dispatch(addCardToPathways(payload));
        })
        .catch(() => {
          this.setState({
            err: true,
            buttonPending: false
          });
        });
    }
  }

  closeModalHandler() {
    this.props.dispatch(close());
  }

  checkTitle(item) {
    let name = this.props.isAddingToJourney ? item.message : item.title || item.message;
    name = name.length < 53 ? name : `${name.slice(0, 52)}...`;
    return name.replace(/amp;/gi, '');
  }

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.checkScroll(node));
    }
  };

  checkScroll = node => {
    let obj = ReactDOM.findDOMNode(node);
    let isBottom =
      obj.scrollHeight - Math.ceil(obj.scrollTop) === obj.offsetHeight ||
      obj.scrollHeight - Math.floor(obj.scrollTop) === obj.offsetHeight;
    if (!this.state.pendingFetch && isBottom) {
      this.fetchMore();
    }
  };

  fetchMore() {
    if (!this.state.isAll) {
      this.setState({ pendingFetch: true });
      if (this.props.isAddingToJourney) {
        getAddableJourneys(this.props.cardId, 10, this.state.journeys.length)
          .then(journeys => {
            this.setState({
              journeys: uniqBy(this.state.journeys.concat(journeys), 'id'),
              isAll: journeys && journeys.length < 10,
              pendingFetch: false
            });
          })
          .catch(err => {
            console.error(`Error in AddToPathwayModalNew.getAddableJourneys.getItems.func: ${err}`);
          });
      } else {
        pathways
          .getPathwaysToAddToNew(
            this.props.cardId,
            10,
            this.state.pathways.length,
            this.state.sortBy,
            this.state.orderBy
          )
          .then(pathwaysData => {
            this.setState({
              pathways: uniqBy(this.state.pathways.concat(pathwaysData), 'id'),
              pendingFetch: false,
              isAll: pathwaysData && pathwaysData.length < 10
            });
          })
          .catch(err => {
            console.error(
              `Error in AddToPathwayModalNew.getPathwaysToAddToNew.getItems.func: ${err}`
            );
          });
      }
    }
  }

  handleChangeSection = (journey, newSections) => {
    let selectedSections = { ...this.state.selectedSections };
    selectedSections[journey.id] = newSections;
    this.setState({ selectedSections });
  };

  render() {
    let isAddingToJourney = this.props.isAddingToJourney;
    let isNoItems =
      (!isAddingToJourney && !this.state.pathways.length) ||
      (isAddingToJourney && !this.state.journeys.length);
    let pathwaysVar = this.state.pathways
      .filter(pathway => {
        if (pathway.title) {
          return pathway.title.toLowerCase().indexOf(this.state.query.toLowerCase()) !== -1;
        }
        if (pathway.message) {
          return pathway.message.toLowerCase().indexOf(this.state.query.toLowerCase()) !== -1;
        }
      })
      .sort((a, b) => {
        a.title = a.title || a.message;
        b.title = b.title || b.message;
        if (a.title && b.title) {
          if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
          if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
        } else if (a.title && !b.title) {
          return 1;
        } else if (!a.title && b.title) {
          return -1;
        }
        return 0;
      });
    let journeys = this.state.journeys;
    return (
      <div>
        <div style={this.styles.header}>
          {' '}
          {tr(`Add to ${isAddingToJourney ? 'Journey' : 'Pathway'}(s)`)}
          <span style={this.styles.close} className="close-icon-btn">
            <NavigationClose color={colors.grey} onTouchTap={this.closeModalHandler} />
          </span>
        </div>
        <Paper>
          <div className="container">
            <div className="container-padding">
              <div>
                {!this.state.pending && isNoItems && (
                  <div className="text-center container-padding">
                    {tr(
                      `Create a ${
                        isAddingToJourney ? 'Journey' : 'Pathway'
                      } before you start adding content to it`
                    )}
                  </div>
                )}
                {this.state.pending && isNoItems && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
                {!this.state.pending && !!this.state.pathways.length && !isAddingToJourney && (
                  <div>
                    <div style={this.styles.tableScroll} ref={this.mountScroll}>
                      <TextField
                        name="addtopathwaymodal"
                        autoFocus={true}
                        className="hiddenTextField"
                      />
                      <table style={this.styles.table}>
                        <thead>
                          <tr>
                            <th style={this.styles.firstColumn}>{tr('Pathway Title')}</th>
                            <th>{tr('Creator')}</th>
                            <th>{tr('Modified')}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {pathwaysVar.map((pathway, index) => {
                            let selected = ~this.state.selectedPathway.indexOf(pathway.id);
                            return (
                              <tr key={index}>
                                <td style={this.styles.firstColumn}>
                                  <Checkbox
                                    label={
                                      <a
                                        className="matte"
                                        onClick={() => {
                                          this.props.dispatch(push(`/pathways/${pathway.id}`));
                                        }}
                                      >
                                        {this.checkTitle(pathway)}
                                      </a>
                                    }
                                    onCheck={() => {
                                      this.selectPathway(pathway.id);
                                    }}
                                    className="checkbox"
                                    style={this.styles.checkbox}
                                    checked={selected}
                                    aria-label={`pathway, ${this.checkTitle(pathway)}`}
                                  />
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <a
                                    className="data-ava matte"
                                    onClick={() => {
                                      this.props.dispatch(push(`/${pathway.author.handle}`));
                                    }}
                                    target="_blank"
                                  >
                                    {pathway.author.name.length < 19
                                      ? pathway.author.name
                                      : `${pathway.author.name.slice(0, 18)}...`}
                                  </a>
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <div className="matte">
                                    {getFormattedDateTime(pathway.updatedAt, 'DD/MM/YYYY')}
                                  </div>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                      {this.state.pendingFetch && (
                        <div className="text-center">
                          <Spinner />
                        </div>
                      )}
                      {this.state.err && (
                        <div className="text-center error-text data-not-available-msg">
                          {tr('Something went wrong! Please try later.')}
                        </div>
                      )}
                    </div>
                    <div className="text-center" style={this.styles.footerButtons}>
                      <SecondaryButton
                        label={tr('Cancel')}
                        className="cancel"
                        onTouchTap={this.closeModalHandler}
                      />
                      <PrimaryButton
                        label={tr('Add')}
                        className="create"
                        onTouchTap={this.addClickHandler}
                        pending={this.state.buttonPending}
                        pendingLabel={tr('Adding...')}
                        disabled={!this.state.selectedPathway.length}
                      />
                    </div>
                  </div>
                )}
                {!this.state.pending && !!this.state.journeys.length && isAddingToJourney && (
                  <div>
                    <div style={this.styles.tableSelectScroll} ref={this.mountScroll}>
                      <TextField
                        name="addtopathwaymodal"
                        autoFocus={true}
                        className="hiddenTextField"
                      />
                      <table style={this.styles.table} className="journeys-table">
                        <thead>
                          <tr>
                            <th style={this.styles.firstColumn}>{tr('Journey Title')}</th>
                            <th className="journey-section-th">{tr('Section')}</th>
                            <th>{tr('Creator')}</th>
                            <th>{tr('Modified')}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {journeys.map((journey, index) => {
                            let journeySections = journey.journeySections;
                            let options = journeySections.map(item => ({
                              label: item.sectionTitle,
                              value: item.cardId
                            }));
                            let values = this.state.selectedSections[journey.id];
                            return (
                              <tr key={index}>
                                <td style={this.styles.firstColumn}>
                                  <a
                                    className="matte"
                                    onClick={() => {
                                      this.props.dispatch(push(`/journey/${journey.id}`));
                                    }}
                                  >
                                    {this.checkTitle(journey)}
                                  </a>
                                </td>
                                <td style={this.styles.tdBorder} className="journey-section-select">
                                  <Select
                                    value={values}
                                    multi={true}
                                    onChange={this.handleChangeSection.bind(this, journey)}
                                    options={options}
                                    placeholder={tr('Pathway')}
                                    searchable={true}
                                    clearable={false}
                                    aria-label="select pathway"
                                  />
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <a
                                    className="data-ava matte"
                                    onClick={() => {
                                      this.props.dispatch(
                                        push(`/@${this.props.currentUser.handle}`)
                                      );
                                    }}
                                    target="_blank"
                                  >
                                    {this.props.currentUser.name.length < 19
                                      ? this.props.currentUser.name
                                      : `${this.props.currentUser.name.slice(0, 18)}...`}
                                  </a>
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <div className="matte">
                                    {getFormattedDateTime(journey.updatedAt, 'DD/MM/YYYY')}
                                  </div>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                      {this.state.pendingFetch && (
                        <div className="text-center">
                          <Spinner />
                        </div>
                      )}
                      {this.state.err && (
                        <div className="text-center error-text">
                          {tr('Something went wrong! Please try later.')}
                        </div>
                      )}
                    </div>
                    <div className="text-center" style={this.styles.footerButtons}>
                      <SecondaryButton
                        label={tr('Cancel')}
                        className="cancel"
                        onTouchTap={this.closeModalHandler}
                      />
                      <PrimaryButton
                        label={tr('Add')}
                        className="create"
                        onTouchTap={this.addClickHandler}
                        pending={this.state.buttonPending}
                        pendingLabel={tr('Adding...')}
                        disabled={
                          Object.values(this.state.selectedSections).every(item => !item.length) ||
                          !Object.values(this.state.selectedSections).length
                        }
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

AddToPathwayModalNew.propTypes = {
  cardId: PropTypes.string,
  isAddingToJourney: PropTypes.bool,
  cardType: PropTypes.string,
  card: PropTypes.object,
  currentUser: PropTypes.object
};

export default connect(state => ({ currentUser: state.currentUser.toJS() }))(AddToPathwayModalNew);
