import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import colors from 'edc-web-sdk/components/colors/index';
import { close, openStatusModal } from '../../actions/modalActions';
import Paper from 'edc-web-sdk/components/Paper';
import { getList, shareToGroup } from 'edc-web-sdk/requests/groups.v2';
import Checkbox from 'material-ui/Checkbox';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton.jsx';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { open as openSnackBar } from '../../actions/snackBarActions';
import map from 'lodash/map';
import uniqBy from 'lodash/uniqBy';
import { tr } from 'edc-web-sdk/helpers/translations';
import { updateCurrentCard } from '../../actions/cardsActions';
import Spinner from '../common/spinner';
import TextField from 'material-ui/TextField';

class ShareToGroupModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      header: {
        color: '#ffffff',
        width: '100%',
        position: 'absolute',
        top: '-30px',
        left: 0
      },
      close: {
        cursor: 'pointer',
        float: 'right',
        top: 0
      },
      footerButtons: {
        marginTop: '10px'
      },
      containerScroll: {
        maxHeight: '216px',
        display: 'block',
        overflowY: 'scroll'
      },
      label: {
        color: '#454560',
        fontSize: '14px'
      },
      firstColumn: {
        textAlign: 'left'
      },
      table: {
        width: '100%',
        textAlign: 'center'
      }
    };
    this.state = {
      groups: [],
      groupsSelected: [],
      buttonPending: false,
      pending: true,
      err: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
    this.alreadyShared = this.alreadyShared.bind(this);
  }

  componentDidMount() {
    getList(10, 0, null, null, null, true)
      .then(groups => {
        this.setState({
          groups: groups,
          pending: false,
          isAll: groups.length < 10
        });
      })
      .catch(err => {
        console.error(`Error in ShareToGroupModal.getList.func : ${err}`);
      });
    let alreadySharedIds = map(this.props.card.teams, 'id');
    this.setState({
      alreadySharedIds: alreadySharedIds
    });
  }

  closeModalHandler = () => {
    this.props.dispatch(close());
  };

  selectGroup(group) {
    let groupsSelected = this.state.groupsSelected;
    let selectedGroupIndex = groupsSelected.findIndex(g => g.id === group.id);
    if (~selectedGroupIndex) {
      groupsSelected.splice(selectedGroupIndex, 1);
    } else {
      groupsSelected.push(group);
    }
    this.setState({ groupsSelected });
  }

  alreadyShared = id => {
    return this.state.alreadySharedIds.indexOf(id) >= 0;
  };

  addClickHandler = () => {
    this.setState({ buttonPending: true, err: false });
    let groupsSelectedIds = map(this.state.groupsSelected, 'id');
    let payload = {
      team_ids: groupsSelectedIds
    };
    shareToGroup(this.props.card.id, payload)
      .then(response => {
        this.setState({ buttonPending: false });
        this.closeModalHandler();
        this.props.dispatch(openSnackBar('Card successfully shared to the Group(s)!', true));
        this.props.dispatch(updateCurrentCard(response.card));
      })
      .catch(() => {
        let msg = 'This card is restricted and can be shared only by the author.';
        if (this.state.newModalAndToast) {
          this.props.dispatch(openSnackBar(msg));
        } else {
          this.props.dispatch(openStatusModal(msg));
        }
      });
  };

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.checkScroll(node));
    }
  };

  checkScroll = node => {
    let obj = ReactDOM.findDOMNode(node);
    let isBottom =
      obj.scrollHeight - Math.ceil(obj.scrollTop) === obj.offsetHeight ||
      obj.scrollHeight - Math.floor(obj.scrollTop) === obj.offsetHeight;
    if (!this.state.pending && isBottom) {
      this.fetchMore();
    }
  };

  fetchMore() {
    getList(10, this.state.groups.length)
      .then(groups => {
        this.setState({
          groups: uniqBy(this.state.groups.concat(groups), 'id'),
          pending: false,
          isAll: groups.length < 10
        });
      })
      .catch(err => {
        console.error(`Error in ShareToGroupModal.fetchMore.getList.func : ${err}`);
      });
  }

  render() {
    let groups = this.state.groups;
    return (
      <div>
        {groups && (
          <div>
            <div style={this.styles.header}>
              {tr('Share to Groups')}
              <span style={this.styles.close} className="close-icon-btn">
                <NavigationClose color={colors.white} onTouchTap={this.closeModalHandler} />
              </span>
            </div>
            <Paper>
              <div className="container">
                <div className="container-padding">
                  <div style={this.styles.containerScroll} ref={this.mountScroll}>
                    {this.state.pending && (
                      <div className="text-center">
                        <Spinner />
                      </div>
                    )}
                    {!this.state.pending && (
                      <div>
                        <TextField
                          disabled={false}
                          name="sharetogrp"
                          autoFocus={true}
                          className="hiddenTextField"
                        />
                        <table style={this.styles.table}>
                          <thead>
                            <tr>
                              <th style={this.styles.firstColumn}>{tr('Group Title')}</th>
                              <th>{tr('Creator')}</th>
                            </tr>
                          </thead>
                          <tbody>
                            {!this.state.pending &&
                              this.state.groups.map(group => {
                                return (
                                  <tr key={group.id}>
                                    <td style={this.styles.firstColumn}>
                                      <Checkbox
                                        label={group.name}
                                        aria-label={group.name}
                                        key={group.id}
                                        className="checkbox"
                                        onCheck={() => {
                                          this.selectGroup(group);
                                        }}
                                        disabled={this.alreadyShared(group.id)}
                                        labelStyle={this.styles.label}
                                      />
                                    </td>
                                    <td style={this.styles.label}>{group.createdBy}</td>
                                  </tr>
                                );
                              })}
                          </tbody>
                        </table>
                      </div>
                    )}
                  </div>
                  <div>
                    {this.state.err && (
                      <div className="text-center error-text data-not-available-msg">
                        {tr('Something went wrong! Please try later.')}
                      </div>
                    )}
                    <div className="text-center" style={this.styles.footerButtons}>
                      <SecondaryButton
                        label={tr('Cancel')}
                        className="cancel"
                        onTouchTap={this.closeModalHandler}
                      />
                      <PrimaryButton
                        label={tr('Share')}
                        className="create"
                        onTouchTap={this.addClickHandler}
                        pending={this.state.buttonPending}
                        pendingLabel={tr('Sharing...')}
                        disabled={!this.state.groupsSelected.length}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </Paper>
          </div>
        )}
      </div>
    );
  }
}

ShareToGroupModal.propTypes = {
  card: PropTypes.object
};

export default connect(state => ({
  cards: state.cards.toJS()
}))(ShareToGroupModal);
