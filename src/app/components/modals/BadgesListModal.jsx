import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import { fetchUserBadges } from 'edc-web-sdk/requests/badges';
import { setUserBadges } from '../../actions/currentUserActions';
import Spinner from '../common/spinner';
import TextField from 'material-ui/TextField';

class BadgesListModal extends Component {
  constructor(props, context) {
    super(props, context);
    let publicBadges = this.props.currentUser.publicProfile.userCardBadges;
    let badges = publicBadges ? publicBadges : this.props.currentUser.userBadges[this.props.type];
    this.state = {
      pending: false,
      isLast: false,
      limit: 4,
      offset: badges.length,
      buttonPending: false,
      badges,
      publicBadges: !!publicBadges
    };

    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      tableScroll: {
        maxHeight: '500px',
        overflowY: 'auto'
      },
      shareToLinkedIn: {
        cursor: 'pointer',
        fontSize: '12px  '
      },
      linkedInLogo: {
        height: '15px',
        width: '20px'
      }
    };
  }

  fetchCards = () => {
    this.setState({ buttonPending: true });
    let userBadges = this.state.badges || [];
    fetchUserBadges(this.props.currentUser.id, {
      limit: this.state.limit,
      offset: this.state.offset,
      type: this.props.type
    })
      .then(data => {
        if (data && data.userBadges && data.userBadges.length) {
          this.setState({
            offset: this.state.offset + this.state.limit,
            buttonPending: false,
            isLast: data.userBadges.length < this.state.limit
          });
          userBadges = userBadges.concat(data.userBadges);
          this.setState({ badges: userBadges });
          this.props.dispatch(setUserBadges(userBadges, this.props.type));
        } else {
          this.setState({
            isLast: true,
            buttonPending: false
          });
        }
      })
      .catch(err => {
        console.error(`Error in BadgesListModal.fetchUserBadges.func: ${err}`);
      });
  };

  shareOnLinkedIn = (identifier, badge_title) => {
    let LinkedInUrl = 'https://www.linkedin.com/shareArticle';
    let mini = 'mini=true';
    let title =
      'title=' +
      'Posted from EdCast: ' +
      this.props.currentUser.name +
      ' earned a badge for ' +
      badge_title;
    let shared_url = 'url=' + window.location.origin + '/verify_badge/' + identifier;
    let source = 'edCast';
    let url = LinkedInUrl + '?' + mini + '&' + title + '&' + shared_url + '&' + source;
    window.open(encodeURI(url), 'Share on Linkedin', 'height=520,width=570');
  };

  closeModal = () => {
    this.props.closeModal();
  };

  render() {
    let showShareonLinkedIn =
      this.props.team.OrgConfig &&
      !!this.props.team.OrgConfig.badgings['web/badgings'] &&
      !!this.props.team.OrgConfig.badgings['web/badgings'].sharePathwayBadgesonLinkedIn;
    return (
      <div className="pathway-badges-modal">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="modal">
          <div className="modal-content">
            <div className="modal-title">
              {this.props.type == 'CardBadge' && tr('Pathway Badges')}
              {this.props.type == 'ClcBadge' && tr('CLC Badges')}
            </div>
            <div className="close close-button">
              <IconButton
                onTouchTap={this.closeModal}
                aria-label="close"
                style={this.styles.closeBtn}
              >
                <CloseIcon color="#fff" />
              </IconButton>
            </div>
            <div className="container">
              <div className="row">
                <TextField name="badgeslistmodal" autoFocus={true} className="hiddenTextField" />
                {this.state.badges.map(badge => {
                  return (
                    <div
                      key={badge.id}
                      className="small-6 medium-4 large-3"
                      style={{ marginBottom: '10px' }}
                    >
                      <Paper className="modal-badge-item">
                        <img src={badge.imageUrl} />
                        <div className="ellipsis">{badge.title}</div>
                        {this.props.type === 'CardBadge' && showShareonLinkedIn && (
                          <p
                            style={this.styles.shareToLinkedIn}
                            onClick={this.shareOnLinkedIn.bind(this, badge.identifier, badge.title)}
                          >
                            {tr('Share it on')}
                            <span>
                              {' '}
                              <img
                                src="https://d1iwkfmdo6oqxx.cloudfront.net/organizations/co_branding_logos/000/001/454/original/LinkedInLogo.png"
                                style={this.styles.linkedInLogo}
                              />
                            </span>
                          </p>
                        )}
                      </Paper>
                    </div>
                  );
                })}
              </div>
              {this.state.pending && (
                <div className="text-center">
                  <Spinner />
                </div>
              )}
              {!this.state.publicBadges && !this.state.pending && !this.state.isLast && (
                <div className="text-center">
                  <button
                    className="view-more-v2"
                    disabled={this.state.buttonPending}
                    onClick={this.fetchCards}
                  >
                    {this.state.buttonPending ? tr('Loading...') : tr('View More')}
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

BadgesListModal.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  type: PropTypes.string,
  closeModal: PropTypes.func
};

export default connect(mapStoreStateToProps)(BadgesListModal);
