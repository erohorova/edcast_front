import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import AutoComplete from 'material-ui/AutoComplete';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import concat from 'lodash/concat';
import without from 'lodash/without';
import { closeInterestModal } from '../../actions/modalActions';
import { updateUserExpertise } from '../../actions/usersActions';
import { users, topics } from 'edc-web-sdk/requests/index';
import FlashAlert from '../common/FlashAlert';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class UpdateExpertiseModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      question: {
        padding: '13px 0 8px 0',
        fontSize: '14px',
        fontWeight: '600'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      rectangularChip: {
        margin: '4px',
        borderRadius: '0px',
        border: '1px solid #424242',
        backgroundColor: colors.white
      },
      suggestedChip: {
        margin: '4px',
        border: '1px solid #00A1E1'
      },
      infoIcon: {
        height: '20px',
        width: '20px'
      },
      helpText: {
        fontSize: '14px',
        padding: '8px 0'
      },
      skillSpan: {
        border: '1px solid',
        borderRadius: '20px',
        padding: '6px 16px',
        marginRight: '10px',
        display: 'inline-block',
        marginTop: '15px',
        width: '100%',
        position: 'relative',
        fontSize: '12px',
        textAlign: 'center',
        fontWeight: '600',
        backgroundColor: '#00c853',
        borderColor: '#00c853',
        color: 'white',
        maxWidth: '150px',
        height: '44px',
        lineHeight: '29px'
      },
      cancleSkillIcon: {
        position: 'absolute',
        top: 'calc(50% - 14px)',
        right: '7px',
        color: 'white'
      },
      selectedSkillLabel: {
        lineHeight: 1.3,
        verticleAlign: 'middle'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`,
        background: 'white'
      },
      chipLabel: {
        paddingRight: '36px'
      },
      selectedLabel: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        maxWidth: 'calc(100% - 0px)',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        color: `${colors.primary}`
      },
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer'
      }
    };

    this.state = {
      userExpertise: [],
      suggestedExpertise: [],
      expertiseSearchText: '',
      expertiseSearch: [],
      error: '',
      postPending: false,
      topics: [],
      topicId: '',
      skillsArray: [],
      page: 1,
      pageNo: 1,
      selectedSkillsIds: [],
      isLastTopicPage: true,
      showAlert: false,
      errorMessage: '',
      learningTopics: [],
      skillSource: [],
      searchText: ''
    };

    this.handleExpertiseInput = this.handleExpertiseInput.bind(this);
    this.addExpertiseHandler = this.addExpertiseHandler.bind(this);
    this.selectExpertise = this.selectExpertise.bind(this);
    this.deleteExpertise = this.deleteExpertise.bind(this);
    this.handleClickCancel = this.handleClickCancel.bind(this);
    this.handleClickUpdate = this.handleClickUpdate.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
  }

  componentDidMount() {
    topics
      .getSuggestedTopicsForUserOnboarding(1, 12)
      .then(topicsData => {
        this.setState({
          topics: topicsData.domains,
          isLastTopicPage: topicsData.domains.length < 12 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateExpertiseModal.getSuggestedTopicsForUserOnboarding.func : ${err}`
        );
      });
    let skillIds = [];
    (
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.expertTopics
    ).map(topic => {
      skillIds.push(topic.topic_id);
    });
    this.setState({
      skillsArray: (
        (this.props.currentUser &&
          this.props.currentUser.profile &&
          this.props.currentUser.profile.expertTopics) ||
        []
      ).map(ele => ele),
      selectedSkillsIds: skillIds
    });

    this.handleSkillInput();
  }

  handleExpertiseInput(text) {
    this.setState({ expertiseSearchText: text });
    if (text.length >= 3) {
      this.fetchTopicSkills(text);
    }
  }

  addExpertiseHandler(text, index) {
    this.selectExpertise(text);
    setTimeout(() => {
      this.refs.expertiseField.setState({ searchText: '' });
      this.refs.expertiseField.focus();
    }, 300);
  }

  selectExpertise(exp) {
    if (this.state.expertise.length < 3) {
      // FIXME: expTopics is not defined
      // let updatedUserExpertise = concat(this.state.expertise, expTopics);
      // let updatedSuggestedExpertise = without(this.state.suggestedExpertise, expTopics);
      let updatedUserExpertise = concat(this.state.expertise);
      let updatedSuggestedExpertise = without(this.state.suggestedExpertise);
      this.setState({
        expertise: updatedUserExpertise,
        suggestedExpertise: updatedSuggestedExpertise,
        error: ''
      });
    } else {
      this.setState({ error: 'You can only select 3 at this time' });
    }
  }

  deleteExpertise(expertise) {
    // FIXME: skill is not defined
    // let updatedUserExpertise = without(this.state.skillsArray, skill);
    let updatedUserExpertise = without(this.state.skillsArray);

    this.setState({
      skillsArray: updatedUserExpertise
    });
  }

  handleClickTopic(topicId, pageNo) {
    this.state.topicId = topicId;
    let page = 1;

    topics
      .fetchTopicSkills(topicId, page)
      .then(data => {
        this.setState({
          skills: data.topics,
          [`${topicId}-lastPage`]: data.topics.length < 7 ? true : false
        });
      })
      .catch(err => {
        console.error(`Error in UpdateExpertiseModal.fetchTopicSkills.func : ${err}`);
      });
    this.setState({ [`pageNo-${topicId}`]: page });
  }

  handleClickSkill(skill) {
    let skillObj = {
      topic_name: skill.name,
      topic_label: skill.label,
      topic_id: skill.id,
      domain_name: skill.domain.name,
      domain_id: skill.domain.id,
      domain_label: skill.domain.label
    };
    let skillsArray = this.state.skillsArray;

    let selectedSkillsIds = this.state.selectedSkillsIds;
    if (selectedSkillsIds.indexOf(skill.id) == -1) {
      skillsArray.push(skillObj);
      selectedSkillsIds.push(skill.id);

      this.setState({
        skillsArray: skillsArray,
        selectedSkillsIds: selectedSkillsIds
      });
    }
  }

  handleRemoveSkill(id) {
    let selectedSkillsIds = this.state.selectedSkillsIds;
    let skillsArray = this.state.skillsArray;
    var index = selectedSkillsIds.indexOf(id);
    let newSkillsArray = [];
    if (index > -1) {
      selectedSkillsIds.splice(index, 1);
      skillsArray.map(skill => {
        if (selectedSkillsIds.indexOf(skill.topic_id) > -1) {
          newSkillsArray.push(skill);
        }
      });
      skillsArray = newSkillsArray;
    }
    this.setState({
      selectedSkillsIds: selectedSkillsIds,
      skillsArray: skillsArray
    });
  }

  handleClickCancel() {
    this.props.dispatch(closeInterestModal());
  }

  handleClickUpdate() {
    this.setState({ postPending: true });
    let skillsArray = this.state.skillsArray;

    for (let skill of skillsArray) {
      if (!this.showBIA && skill.level) {
        delete skill.level;
      }
    }

    this.props
      .dispatch(
        updateUserExpertise(
          skillsArray,
          this.props.currentUser.id,
          this.props.currentUser.profile.id
        )
      )
      .then(response => {
        this.setState({ postPending: false });
        this.handleClickCancel();
      })
      .catch(err => {
        console.error(`Error in UpdateExpertiseModal.updateUserExpertise.func : ${err}`);
      });
  }

  handleClickViewMoreTopic() {
    this.setState({ page: this.state.page + 1 });
    let topicsArray = this.state.topics;
    return topics
      .getSuggestedTopicsForUserOnboarding(this.state.page + 1, 12)
      .then(topicsData => {
        this.setState({
          topics: topicsArray.concat(topicsData.domains),
          isLastTopicPage: topicsData.domains.length < 12 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateExpertiseModal.handleClickViewMoreTopic.getSuggestedTopicsForUserOnboarding.func : ${err}`
        );
      });
  }

  handleClickViewMoreSkill() {
    let topicId = this.state.topicId;
    let currentSkills = this.state.skills;
    let page = this.state[`pageNo-${topicId}`] + 1;
    topics
      .fetchTopicSkills(this.state.topicId, page)
      .then(data => {
        this.setState({
          skills: currentSkills.concat(data.topics),
          [`${topicId}-lastPage`]: data.topics.length < 7 ? true : false
        });
      })
      .catch(err => {
        console.error(`Error in UpdateExpertiseModal.fetchTopicSkills.func : ${err}`);
      });
    this.setState({ [`pageNo-${topicId}`]: page });
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  handleSkillInput = (value = '') => {
    this.setState({
      searchText: value
    });
    topics
      .queryTopics(value)
      .then(topicsData => {
        this.setState({
          skillSource: topicsData.topics.slice(0, 20)
        });
      })
      .catch(err => {
        console.error(`Error in UpdateExpertiseModal.handleSkillInput.queryTopics.func : ${err}`);
      });
  };

  selectSkillInput = skill => {
    if (skill.id) {
      let learningTopics = this.state.skillsArray;
      let filteredTopics = learningTopics.filter(function(obj) {
        return skill.id == obj.topic_id;
      });
      let expertiseLimit =
        (this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options.expertise_limit) ||
        null;
      if (expertiseLimit && learningTopics.length > expertiseLimit - 1) {
        let expertiseLimitMsg = expertiseLimit === 1 ? 'one topic' : `${expertiseLimit} topics`;
        this.setState({
          showAlert: true,
          errorMessage: `You can select a maximum of ${expertiseLimitMsg} . Don't worry you can always change them again here.`,
          searchText: ''
        });
        return;
      }
      if (filteredTopics.length > 0) {
        this.setState({ showAlert: true, errorMessage: 'The topic is already added!' });
        this.setState(
          {
            searchText: ''
          },
          function() {
            this.handleSkillInput();
          }
        );
        return;
      }
      let learningTopic = {
        domain_id: skill.domain.id,
        domain_label: skill.domain.label,
        domain_name: skill.domain.name,
        topic_id: skill.id,
        topic_label: skill.label,
        topic_name: skill.name
      };
      learningTopics.push(learningTopic);
      this.setState(
        {
          skillsArray: learningTopics,
          searchText: ''
        },
        function() {
          this.handleSkillInput();
        }
      );
    }
  };

  removeTopicHandler(id) {
    let learningTopics = this.state.skillsArray;
    learningTopics = learningTopics.filter(function(obj) {
      return obj.topic_id !== id;
    });
    this.setState({
      skillsArray: learningTopics
    });
  }

  render() {
    // let topicsParam = this.state.topics !== undefined ? this.state.topics : [];
    let skills = this.state.skills !== undefined ? this.state.skills : [];
    let skillsArray = this.state.skillsArray != undefined ? this.state.skillsArray : [];
    let _this = this;

    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let expertiseLabel = tr('UPDATE SKILLS');
    if (labels) {
      let expertiseLabelFlag =
        labels['web/labels/expertise'] && labels['web/labels/expertise'].label.length > 0;
      expertiseLabel = `${tr('UPDATE')} ${
        expertiseLabelFlag ? labels['web/labels/expertise'].label : tr('skills')
      }`;
    }

    return (
      <div className="vertical-spacing-large">
        {this.state.showAlert && (
          <FlashAlert
            message={this.state.errorMessage}
            toShow={this.state.showAlert}
            removeAlert={this.removeAlert}
          />
        )}
        <h5 style={{ marginBottom: '0px' }}>{expertiseLabel.toUpperCase()}</h5>
        <div className="clearfix" />
        <div className="row">
          <TextField className="hiddenTextField" autoFocus={true} name="updateexpertise" />
          <div className="small-12 columns">
            <div className="row">
              <div className="small-12 columns text-center">
                <div style={this.styles.chipsWrapper}>
                  {this.state.skillsArray.map((skill, idx) => {
                    return (
                      <Chip
                        key={idx}
                        style={this.styles.chip}
                        labelStyle={this.styles.chipLabel}
                        backgroundColor={'#fff'}
                      >
                        <small style={this.styles.selectedLabel}>{skill.topic_label}</small>
                        <IconButton
                          className="tick-icon"
                          ref={'tickIcon'}
                          tooltip={tr('Selected')}
                          disableTouchRipple
                          tooltipPosition="top-center"
                          style={this.styles.customPlusIcon}
                          aria-label={`${skill.topic_label} selected`}
                        >
                          <Close color={colors.primary} />
                        </IconButton>
                        <IconButton
                          className="cancel-icon delete"
                          ref={'cancelIcon'}
                          tooltip={tr('Remove Item')}
                          disableTouchRipple
                          tooltipPosition="top-center"
                          style={this.styles.customPlusIcon}
                          onTouchTap={this.removeTopicHandler.bind(_this, skill.topic_id)}
                          aria-label={`${skill.topic_label} remove`}
                        >
                          <Close color={colors.primary} />
                        </IconButton>
                      </Chip>
                    );
                  })}
                </div>
              </div>
            </div>
            <br />
            <div className="clearfix" />

            <div className="row skill-input-row">
              <div className="small-12 columns text-center">
                <p className="onboarding-label text-left">
                  {tr('What skills do you want to share?')}
                </p>
                <div className="auto-complete-as-input">
                  <AutoComplete
                    popoverProps={{
                      style: {
                        bottom: '0.7rem',
                        overflowY: 'auto',
                        background: 'transparent',
                        boxShadow: 'none'
                      },
                      className: 'auto-complete-list'
                    }}
                    menuStyle={{ background: 'white' }}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSource={this.state.skillSource}
                    onUpdateInput={this.handleSkillInput}
                    fullWidth={true}
                    dataSourceConfig={{ text: 'label', value: 'name' }}
                    openOnFocus={true}
                    onNewRequest={this.selectSkillInput}
                    searchText={this.state.searchText}
                    aria-label={tr('What skills do you want to share?')}
                  />
                </div>
              </div>
            </div>

            <br />
            <div style={{ padding: '8px' }} className="clearfix">
              <PrimaryButton
                label={tr('Update')}
                className="float-right create"
                pendingLabel={tr('Updating...')}
                pending={this.state.postPending}
                onTouchTap={this.handleClickUpdate}
              />
              <SecondaryButton
                label={tr('Cancel')}
                className="float-right close"
                onTouchTap={this.handleClickCancel}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: Object.assign({}, state.team.toJS()),
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

UpdateExpertiseModal.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  callback: PropTypes.func
};

export default connect(mapStoreStateToProps)(UpdateExpertiseModal);
