import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import LeapIcon from 'edc-web-sdk/components/icons/Leap';
import CardSearch from '../common/CardSearch';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import Select from '../common/LeapSelect';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { createLeap, updateLeap } from 'edc-web-sdk/requests/cards.v2';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import Spinner from '../common/spinner';
import { List, ListItem } from 'material-ui/List';
import getCardType from '../../utils/getCardType';
import TextField from 'material-ui/TextField';

class LeapModal extends Component {
  constructor(props, context) {
    super(props, context);
    let correctValue, incorrectValue;
    let foundLeap =
      this.props.pathway &&
      props.card.leaps &&
      props.card.leaps.inPathways &&
      props.card.leaps.inPathways.length &&
      props.card.leaps.inPathways.find(
        item =>
          item.pathwayId == this.props.pathway.id || item.pathwayId == this.props.pathway.card_id
      );
    if (foundLeap) {
      correctValue = `${foundLeap.correctId}`;
      incorrectValue = `${foundLeap.wrongId}`;
    }
    this.state = {
      card: props.card,
      cardsList: props.cardsList,
      nextCorrectAnswer: correctValue,
      nextIncorrectAnswer: incorrectValue,
      searchCards: []
    };

    this.styles = {
      leapModalStyle: {
        zIndex: 15000,
        alignItems: 'flex-start',
        top: '0',
        paddingTop: '55px',
        overflowY: 'auto'
      },
      leapModalContentStyle: {
        width: '567px'
      },
      dialogRoot: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1502,
        paddingTop: 0
      },
      dialogContent: {
        position: 'relative',
        width: '90vw',
        transform: 'none'
      },
      dialogBody: {
        paddingBottom: 0,
        overflowX: 'hidden'
      },
      listItem: {
        padding: '5px 10px',
        background: 'rgba(172, 173, 193, 0.5)'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.isOpen &&
      !this.props.isOpen &&
      this.state.card.leaps &&
      this.state.card.leaps.standalone
    ) {
      this.setState({ pendingCorrect: true, pendingWrong: true });
      let leapStandalone = this.state.card.leaps.standalone;
      leapStandalone.correctId &&
        fetchCard(leapStandalone.correctId)
          .then(correctCard => {
            this.setState({ pendingCorrect: false, correctCard });
          })
          .catch(err => {
            console.error(`Error in LeapModal.fetchCard.func correctId : ${err}`);
          });
      leapStandalone.wrongId &&
        fetchCard(leapStandalone.wrongId)
          .then(wrongCard => {
            this.setState({ pendingWrong: false, wrongCard });
          })
          .catch(err => {
            console.error(`Error in LeapModal.fetchCard.func wrongId : ${err}`);
          });
    }
    if (
      !nextProps.isOpen &&
      this.props.isOpen &&
      this.state.card.leaps &&
      this.state.card.leaps.standalone
    ) {
      this.setState({
        leapCorrect: null,
        leapIncorrect: null,
        correctCard: null,
        wrongCard: null,
        correctError: null,
        wrongError: null
      });
    }
    if (nextProps.isOpen && !this.props.isOpen && this.props.pathwayEditor) {
      let cardsList = nextProps.cardsList.slice();
      cardsList.forEach((item, i) => {
        if (item.id == this.state.card.id) {
          cardsList[i].isHidden = true;
        } else {
          cardsList[i].isHidden = false;
        }
      });
      this.setState({
        cardsList,
        card: nextProps.card
      });
    }
    if (!nextProps.isOpen && this.props.isOpen && this.props.pathwayEditor) {
      let cardsList = nextProps.cardsList.slice();
      cardsList.forEach((item, i) => {
        cardsList[i].isHidden = false;
      });
      this.setState({
        cardsList,
        leapError: null
      });
    }
    if (nextProps.card) {
      this.setState({
        card: nextProps.card
      });
    }
  }

  getCorrectAnswerForQuiz = () => {
    if (!!this.state.card.quizQuestionOptions) {
      const correctAnswer = this.state.card.quizQuestionOptions.find(o => o.isCorrect);
      return correctAnswer ? correctAnswer.label : '';
    }
    return '';
  };

  getIncorrectAnswerForQuiz = () => {
    return this.state.card.quizQuestionOptions
      .filter(item => !item.isCorrect)
      .map(item => item.label)
      .join(', ');
  };

  checkCard = card => {
    let leapCorrect, leapIncorrect;
    leapCorrect = card.nextCorrectAnswer ? card : this.state.leapCorrect;
    leapIncorrect = card.nextIncorrectAnswer ? card : this.state.leapIncorrect;
    if (!this.props.pathwayEditor) {
      if (
        (card.nextCorrectAnswer &&
          this.state.leapIncorrect &&
          card.id == this.state.leapIncorrect.id) ||
        (card.nextIncorrectAnswer &&
          this.state.leapCorrect &&
          card.id == this.state.leapCorrect.id) ||
        (card.nextCorrectAnswer && this.state.wrongCard && card.id == this.state.wrongCard.id) ||
        (card.nextIncorrectAnswer && this.state.correctCard && card.id == this.state.correctCard.id)
      ) {
        this.setState({
          leapError: 'Leap cards should be different for the correct and wrong answers'
        });
      } else {
        if (this.state.leapError) this.setState({ leapError: null });
      }
      if (leapCorrect && this.state.correctError) this.setState({ correctError: null });
      if (leapIncorrect && this.state.wrongError) this.setState({ wrongError: null });
    }
    this.setState({ leapCorrect, leapIncorrect });
  };

  selectCorrectCard = value => {
    let leaps = this.state.cardsList
      .filter(card => {
        return (
          card &&
          card.leaps &&
          card.leaps.inPathways &&
          card.leaps.inPathways.length &&
          card.leaps.inPathways.find(
            item =>
              item.pathwayId == this.props.pathway.id ||
              item.pathwayId == this.props.pathway.card_id
          )
        );
      })
      .map(card =>
        card.leaps.inPathways.find(
          item =>
            item.pathwayId == this.props.pathway.id || item.pathwayId == this.props.pathway.card_id
        )
      );
    let arrToLeap = this.props.arrToLeap.slice();
    let leapsArrayToSearch =
      arrToLeap.length && leaps.length
        ? arrToLeap.concat(leaps)
        : arrToLeap.length
        ? arrToLeap
        : leaps.length
        ? leaps
        : [];
    if (
      leapsArrayToSearch.length &&
      leapsArrayToSearch.some(
        item =>
          item.cardId == value &&
          (item.correctId == this.state.card.id || item.wrongId == this.state.card.id)
      )
    ) {
      this.setState({
        warningText:
          'You selected card, that already chosen for another leap. Possible infinite loop!'
      });
    } else {
      this.setState({ warningText: null });
    }
    if (
      value == this.state.toIncorrectAnswer ||
      (value == this.state.nextIncorrectAnswer && !this.state.toIncorrectAnswer)
    ) {
      this.setState({
        toCorrectAnswer: value,
        leapError: 'Leap cards should be different for the correct and wrong answers'
      });
      return;
    } else {
      this.setState({ toCorrectAnswer: value, leapError: null });
    }
  };

  selectWrongCard = value => {
    let leaps = this.state.cardsList
      .filter(card => {
        return (
          card &&
          card.leaps &&
          card.leaps.inPathways &&
          card.leaps.inPathways.length &&
          card.leaps.inPathways.find(
            item =>
              item.pathwayId == this.props.pathway.id ||
              item.pathwayId == this.props.pathway.card_id
          )
        );
      })
      .map(card =>
        card.leaps.inPathways.find(
          item =>
            item.pathwayId == this.props.pathway.id || item.pathwayId == this.props.pathway.card_id
        )
      );
    let arrToLeap = this.props.arrToLeap.slice();
    let leapsArrayToSearch =
      arrToLeap.length && leaps.length
        ? arrToLeap.concat(leaps)
        : arrToLeap.length
        ? arrToLeap
        : leaps.length
        ? leaps
        : [];
    if (
      leapsArrayToSearch.length &&
      leapsArrayToSearch.some(
        item =>
          item.cardId == value &&
          (item.correctId == this.state.card.id || item.wrongId == this.state.card.id)
      )
    ) {
      this.setState({
        warningText:
          'You selected card, that already chosen for another leap. Possible infinite loop!'
      });
    } else {
      this.setState({ warningText: null });
    }
    if (
      value == this.state.toCorrectAnswer ||
      (value == this.state.nextCorrectAnswer && !this.state.toCorrectAnswer)
    ) {
      this.setState({
        toIncorrectAnswer: value,
        leapError: 'Leap cards should be different for the correct and wrong answers'
      });
      return;
    } else {
      this.setState({ toIncorrectAnswer: value, leapError: null });
    }
  };

  saveLeap = () => {
    if (this.props.pathwayEditor) {
      if (
        (!this.state.toCorrectAnswer || !this.state.toIncorrectAnswer) &&
        (!this.state.nextCorrectAnswer && !this.state.nextIncorrectAnswer)
      ) {
        this.setState({ leapError: 'There should be leaps for correct and incorrect answers' });
        return;
      }
      if (this.state.leapError) {
        return;
      }
      let leapId =
        this.state.card.leaps &&
        this.state.card.leaps.inPathways &&
        this.state.card.leaps.inPathways.length &&
        this.state.card.leaps.inPathways.find(
          item =>
            item.pathwayId == this.props.pathway.id || item.pathwayId == this.props.pathway.card_id
        )
          ? this.state.card.leaps.inPathways.find(
              item =>
                item.pathwayId == this.props.pathway.id ||
                item.pathwayId == this.props.pathway.card_id
            ).id
          : null;
      if (
        this.state.nextCorrectAnswer &&
        this.state.nextIncorrectAnswer &&
        !this.state.toCorrectAnswer &&
        !this.state.toIncorrectAnswer
      ) {
        this.props.toggleHandleLeapModal();
        return;
      }
      this.setState(
        {
          nextCorrectAnswer: this.state.toCorrectAnswer || this.state.nextCorrectAnswer,
          nextIncorrectAnswer: this.state.toIncorrectAnswer || this.state.nextIncorrectAnswer
        },
        () => {
          this.props.addToLeap(
            this.state.nextCorrectAnswer,
            this.state.nextIncorrectAnswer,
            this.state.card.id,
            leapId
          );
          this.props.toggleHandleLeapModal();
        }
      );
    } else {
      let isCorrectError =
        (!this.state.leapCorrect && !this.state.correctCard) ||
        (this.state.leapCorrect && !this.state.leapCorrect.nextCorrectAnswer);
      let isWrongError =
        (!this.state.leapIncorrect && !this.state.wrongCard) ||
        (this.state.leapIncorrect && !this.state.leapIncorrect.nextIncorrectAnswer);
      if (isCorrectError) {
        this.setState({ correctError: "Leap card for the correct answer can't be blank" });
      }
      if (isWrongError) {
        this.setState({ wrongError: "Leap card for the wrong answer can't be blank" });
      }
      if (isCorrectError || isWrongError || this.state.leapError) return;
      let payload = {
        correct_id: this.state.leapCorrect ? this.state.leapCorrect.id : this.state.correctCard.id,
        wrong_id: this.state.leapIncorrect ? this.state.leapIncorrect.id : this.state.wrongCard.id
      };
      if (this.state.card.leaps && this.state.card.leaps.standalone) {
        updateLeap(this.state.card.id, this.state.card.leaps.standalone.id, payload)
          .then(() => {
            let card = this.state.card;
            card.leaps.standalone.correctId = payload.correct_id;
            card.leaps.standalone.wrongId = payload.wrong_id;
            this.setState({ card }, this.props.toggleHandleLeapModal());
            if (this.props.cardUpdated) {
              this.props.cardUpdated();
            }
          })
          .catch(err => {
            console.error(`Error in LeapModal.updateLeap.func : ${err}`);
          });
      } else {
        createLeap(this.state.card.id, payload)
          .then(() => {
            let card = this.state.card;
            card.leaps = {};
            card.leaps.standalone = {};
            card.leaps.standalone.correctId = payload.correct_id;
            card.leaps.standalone.wrongId = payload.wrong_id;
            this.setState({ card }, this.props.toggleHandleLeapModal());
            if (this.props.cardUpdated) {
              this.props.cardUpdated();
            }
          })
          .catch(err => {
            console.error(`Error in LeapModal.createLeap.func : ${err}`);
          });
      }
    }
  };

  getLeapedCard(item) {
    let image = `/i/images/courses/course1.jpg`;
    let isFileAttached = item.filestack && !!item.filestack.length;
    let videoFileStack = !!(
      isFileAttached &&
      item.filestack[0].mimetype &&
      ~item.filestack[0].mimetype.indexOf('video/')
    );
    let imageFileStack = !!(
      isFileAttached &&
      item.filestack[0].mimetype &&
      ~item.filestack[0].mimetype.indexOf('image/')
    );
    let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
    let type = getCardType(item, videoFileStack, fileFileStack);
    let message = item.message;
    if (message && message.length > 60) {
      message = message.substring(0, 60) + '...';
    }
    if (imageFileStack) {
      image = item.filestack[0].url;
    }
    if (item.resource && item.resource.imageUrl) {
      image = item.resource.imageUrl;
    }
    return (
      <ListItem
        innerDivStyle={this.styles.listItem}
        primaryText={
          <div className="search-block">
            <div className="row">
              <div className="small-2">
                <div
                  className="search-image"
                  style={{
                    backgroundImage: `url(\'${image}\')`
                  }}
                />
              </div>
              <div className="small-7">
                <div className="search-text">{message}</div>
              </div>
              <div className="small-3">
                <div className="search-type">{tr(type)}</div>
              </div>
            </div>
          </div>
        }
      />
    );
  }

  render() {
    let correctValue = this.state.nextCorrectAnswer;
    let incorrectValue = this.state.nextIncorrectAnswer;
    return (
      <Dialog
        className="modalContainer modal"
        open={this.props.isOpen}
        autoScrollBodyContent={false}
        autoDetectWindowHeight={false}
        contentStyle={{ ...this.styles.dialogContent, ...this.styles.leapModalContentStyle }}
        bodyStyle={{ ...this.styles.dialogBody }}
        style={{ ...this.styles.dialogRoot, ...this.styles.leapModalStyle }}
        repositionOnUpdate={false}
      >
        <div className="leap-modal">
          <div className="leap-modal__header">
            <span className="header-title">{tr('Leap to another SmartCard')}</span>
            <div className="close close-button">
              <IconButton
                style={this.styles.closeBtn}
                onTouchTap={this.props.toggleHandleLeapModal}
              >
                <CloseIcon color="white" />
              </IconButton>
              <TextField name="leapmodal" autoFocus={true} className="hiddenTextField" />
            </div>
          </div>
          <div className="leap-modal__content">
            <div className="common-block">
              <div className="leap-modal__row">
                <div className="right-block">
                  <span className="leap-text">{tr('If Answer is Correct')}</span>
                </div>
                <div className="left-block">
                  <input
                    className="leap-modal__input"
                    type="text"
                    value={this.getCorrectAnswerForQuiz()}
                    disabled="true"
                  />
                  <i className="leap-modal__icon">
                    <LeapIcon style={{ ...this.styles.lockLeapIcon_black }} color="#6f708b" />
                  </i>
                </div>
              </div>
              <div className="leap-modal__row">
                <div className="right-block">
                  <span className="leap-text">{tr('Search for SmartCard or Pathway')}</span>
                </div>
                <div className="left-block">
                  {this.props.pathwayEditor && (
                    <Select
                      className="card-number-dropdown small-8"
                      onSelectChange={this.selectCorrectCard}
                      initValue={correctValue}
                      selectOptions={this.state.cardsList}
                    />
                  )}
                  {!this.props.pathwayEditor && (
                    <CardSearch
                      activeType={this.state.activeType}
                      isLeap={true}
                      checkCard={this.checkCard}
                      correctLeap={true}
                    />
                  )}
                  {this.state.pendingCorrect && (
                    <div className="text-center" style={{ paddingBottom: '2rem' }}>
                      <Spinner />
                    </div>
                  )}
                  {!this.state.pendingCorrect && this.state.correctCard && (
                    <List>{this.getLeapedCard(this.state.correctCard)}</List>
                  )}
                  {this.state.correctError && (
                    <div className="text-center error-text"> {tr(this.state.correctError)}</div>
                  )}
                </div>
              </div>
              <div>
                <hr />
              </div>
              <div className="leap-modal__row">
                <div className="right-block">
                  <span className="leap-text">{tr('If Answer is Incorrect')}</span>
                </div>
                <div className="left-block">
                  <input
                    className="leap-modal__input"
                    type="text"
                    value={this.getIncorrectAnswerForQuiz()}
                    disabled="true"
                  />
                  <i className="leap-modal__icon">
                    <LeapIcon style={{ ...this.styles.lockLeapIcon_black }} color="#6f708b" />
                  </i>
                </div>
              </div>
              <div className="leap-modal__row">
                <div className="right-block">
                  <span className="leap-text">{tr('Search for SmartCard or Pathway')}</span>
                </div>
                <div className="left-block">
                  {this.props.pathwayEditor && (
                    <Select
                      className="card-number-dropdown small-8"
                      onSelectChange={this.selectWrongCard}
                      initValue={incorrectValue}
                      selectOptions={this.state.cardsList}
                    />
                  )}
                  {!this.props.pathwayEditor && (
                    <CardSearch
                      activeType={this.state.activeType}
                      isLeap={true}
                      checkCard={this.checkCard}
                      incorrectLeap={true}
                    />
                  )}
                  {this.state.pendingWrong && (
                    <div className="text-center" style={{ paddingBottom: '2rem' }}>
                      <Spinner />
                    </div>
                  )}
                  {!this.state.pendingWrong && this.state.correctCard && (
                    <List>{this.getLeapedCard(this.state.wrongCard)}</List>
                  )}
                  {this.state.wrongError && (
                    <div className="text-center error-text"> {tr(this.state.wrongError)}</div>
                  )}
                </div>
              </div>
              {this.state.leapError && (
                <div className="text-center error-text"> {tr(this.state.leapError)}</div>
              )}
              {this.state.warningText && (
                <div className="text-center warning-message"> {tr(this.state.warningText)}</div>
              )}
            </div>
            <div className="action-block text-center">
              <RaisedButton
                style={{ ...this.styles.actionBtn, ...this.styles.cancel }}
                labelStyle={this.styles.actionBtnLabel}
                className="leap-modal__btn leap-modal__btn_cancel"
                label={tr('Cancel')}
                onClick={this.props.toggleHandleLeapModal}
                labelPosition="before"
              />
              <RaisedButton
                className="leap-modal__btn leap-modal__btn_ok"
                label={tr('Save')}
                onClick={this.saveLeap}
                labelPosition="before"
                style={{ ...this.styles.actionBtn, ...this.styles.previewBtn }}
                labelStyle={{ ...this.styles.actionBtnLabel, ...this.styles.previewBtnLabel }}
                backgroundColor="#00a1e1"
              />
            </div>
          </div>
        </div>
      </Dialog>
    );
  }
}

LeapModal.propTypes = {
  pathway: PropTypes.object,
  card: PropTypes.object,
  cardsList: PropTypes.object,
  arrToLeap: PropTypes.object,
  pathwayEditor: PropTypes.bool,
  isOpen: PropTypes.bool,
  toggleHandleLeapModal: PropTypes.func,
  addToLeap: PropTypes.func,
  cardUpdated: PropTypes.func
};

export default LeapModal;
