import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import PointerArrow from 'edc-web-sdk/components/icons/PointerArrow';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { tr } from 'edc-web-sdk/helpers/translations';
class OnBoardV2BIAContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      targetPosition:
        this.props.targetPosition &&
        this.props.targetPosition.getBoundingClientRect &&
        this.props.targetPosition.getBoundingClientRect(),
      parentContainer: document
        .querySelectorAll('.update-learning-goals-on-board-v2')[0]
        .getBoundingClientRect()
    };

    this.styles = {
      biaRadio: {
        marginRight: '5px',
        fill: '#bbbbc5'
      },
      okLink: {
        float: 'right',
        fontSize: '12px',
        border: '1px solid',
        padding: '0.3125rem',
        cursor: 'pointer',
        outline: 'none',
        borderRadius: '4px'
      }
    };
  }

  componentWillMount = () => {
    this.updateStates(this.props);
  };

  updateStates = nextProps => {
    this.setState({
      targetPosition:
        nextProps.targetPosition &&
        nextProps.targetPosition.getBoundingClientRect &&
        nextProps.targetPosition.getBoundingClientRect(),
      selectedInterestWithBIA: nextProps.selectedInterestWithBIA,
      selectedBia:
        nextProps.selectedInterestWithBIA && nextProps.selectedInterestWithBIA.level
          ? nextProps.selectedInterestWithBIA.level
          : 1
    });
  };

  componentWillReceiveProps = nextProps => {
    this.updateStates(nextProps);
  };

  changeBia = level => {
    this.setState({
      selectedBia: level
    });
  };

  handleOkClick = e => {
    e.stopPropagation();
    e.preventDefault();
    let selectedInterestWithBIA = this.props.selectedInterestWithBIA;
    let selectedBia = this.state.selectedBia;
    selectedInterestWithBIA.level = selectedBia;
    this.props.handleOkClick(selectedInterestWithBIA);
  };

  render() {
    let _this = this;
    let marginTop;
    if (this.state.targetPosition) {
      marginTop = -(this.state.targetPosition.top - this.state.targetPosition.height - 8);
    }
    return (
      <div
        className="row"
        style={{
          border: '1px solid #DDDDE2',
          backgroundColor: '#DDDDE2',
          display: 'block',
          position: 'relative',
          top: '10px',
          padding: '1rem 0.8rem 0.4rem'
        }}
      >
        {this.state.targetPosition && (
          <div
            style={{
              position: 'relative',
              marginLeft: Math.round(
                this.state.targetPosition.left -
                  this.state.parentContainer.left +
                  this.state.targetPosition.width / 2 -
                  20
              ),
              display: 'block',
              marginTop: '-30px',
              height: '20px'
            }}
          >
            <PointerArrow color="#DDDDE2" style={{ width: '20px', height: '12px' }} />
          </div>
        )}
        <div>
          <div className="bia-block">
            <RadioButtonGroup
              name="bia-radio"
              style={{ display: 'flex' }}
              valueSelected={this.state.selectedBia}
              onChange={(e, value) => {
                this.changeBia(value);
              }}
            >
              <RadioButton
                value={1}
                label="Beginner"
                className="bia-radio"
                style={{ width: 'auto' }}
                iconStyle={
                  this.state.selectedBia !== 1 ? this.styles.biaRadio : { marginRight: '5px' }
                }
              />
              <RadioButton
                value={2}
                label="Intermediate"
                className="bia-radio"
                style={{ width: 'auto' }}
                iconStyle={
                  this.state.selectedBia !== 2 ? this.styles.biaRadio : { marginRight: '5px' }
                }
              />
              <RadioButton
                value={3}
                label="Advanced"
                className="bia-radio"
                style={{ width: 'auto' }}
                iconStyle={
                  this.state.selectedBia !== 3 ? this.styles.biaRadio : { marginRight: '5px' }
                }
              />
            </RadioButtonGroup>
            <button
              style={this.styles.okLink}
              onClick={e => {
                _this.handleOkClick(e);
              }}
            >
              {tr('OK')}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

OnBoardV2BIAContainer.propTypes = {
  targetPosition: PropTypes.any,
  selectedInterestWithBIA: PropTypes.object,
  handleOkClick: PropTypes.func
};
export default connect()(OnBoardV2BIAContainer);
