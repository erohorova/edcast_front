import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class ConfirmationModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.confirmClickHandler = this.confirmClickHandler.bind(this);

    this.styles = {
      mainModal: {
        maxHeight: '80vh',
        maxWidth: '768px',
        margin: '0 auto'
      }
    };
  }

  cancelClickHandler() {
    this.props.closeModal && this.props.closeModal();
  }

  confirmClickHandler() {
    this.props.callback();
  }

  render() {
    return (
      <div className="card-modal-container">
        <div className="backdrop" onClick={this.cancelClickHandler} />
        <div className="modal">
          <Paper style={this.styles.mainModal}>
            <div className="container">
              <div className="container-padding">
                <div>
                  <h5>{this.props.title ? tr(this.props.title) : tr('Confirm')}</h5>
                  <div className="vertical-spacing-large">
                    <TextField name="confirmComment" autoFocus={true} className="hiddenTextField" />
                    <div>{tr(this.props.message)}</div>
                  </div>
                  <div className="modal-actions text-right">
                    <SecondaryButton
                      label={tr('Cancel')}
                      className="close"
                      onTouchTap={this.cancelClickHandler}
                    />
                    <PrimaryButton
                      label={tr('Confirm')}
                      className="confirm"
                      onTouchTap={this.confirmClickHandler}
                    />
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

ConfirmationModal.propTypes = {
  message: PropTypes.string,
  callback: PropTypes.func,
  closeModal: PropTypes.func,
  title: PropTypes.string
};

export default connect()(ConfirmationModal);
