import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import { push } from 'react-router-redux';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import AddIcon from 'material-ui/svg-icons/content/add-circle-outline';
import Chip from 'material-ui/Chip';
import BookmarkIcon from 'material-ui/svg-icons/action/bookmark';
import SearchIcon from 'material-ui/svg-icons/action/search';

import UploadIcon from 'edc-web-sdk/components/icons/Upload';
import DynamicIcon from 'edc-web-sdk/components/icons/DynamicPathway';
import LinkIcon from 'edc-web-sdk/components/icons/Link';
import TextIcon from 'edc-web-sdk/components/icons/Text';
import PollIcon from 'edc-web-sdk/components/icons/Pollv2';
import Project from 'edc-web-sdk/components/icons/Project';
import Scorm from 'edc-web-sdk/components/icons/Scorm';
import colors from 'edc-web-sdk/components/colors/index';
import TileIcon from 'edc-web-sdk/components/icons/TileView';
import ListIcon from 'edc-web-sdk/components/icons/ListView';

import { close, openStatusModal } from '../../actions/modalActions';
import { open as openSnackBar, close as closeSnackBar } from '../../actions/snackBarActions';
import TextFieldCustom from '../common/SmartBiteInput';
import SelectField from '../common/SmartBiteDropdown';
import MultilingualContent from '../common/MultilingualContent';
import Card from '../cards/Card';
import CardListView from '../cards/CardListView';

import addTabInRadioButtons from '../../utils/addTabInRadioButtons';

const SmartBite = Loadable({
  loader: () => import('./SmartBite'),
  loading: () => null
});
import CardSharingSection from '../common/CardSharingSection';
import { addToPathway, pathwayEdit } from 'edc-web-sdk/requests/pathways';
import {
  publishPathway,
  batchAddToPathway,
  batchRemoveFromPathway,
  customReorderPathwayCards
} from 'edc-web-sdk/requests/pathways.v2';
import { postv2 } from 'edc-web-sdk/requests/cards';
import {
  lockPathwayCard,
  createLeap,
  updateLeap,
  getRestrictToUserOrGroup,
  showPreviousCardVersions,
  getDefaultImageS3
} from 'edc-web-sdk/requests/cards.v2';
import { eclSearch } from 'edc-web-sdk/requests/ecl';
import { fetchBadges, createBadge } from 'edc-web-sdk/requests/badges';
import _ from 'lodash';
import { getItems } from 'edc-web-sdk/requests/users.v2';
import ReactTags from 'react-tag-autocomplete';
import Sortable from 'sortablejs';
import { updateCurrentCard, clearCurrentCard } from '../../actions/cardsActions';
import { getListV2 } from 'edc-web-sdk/requests/groups.v2';
import Popover from 'react-simple-popover';
import Checkbox from 'material-ui/Checkbox';
import EditIcon from 'material-ui/svg-icons/image/edit';
import getDefaultImage from '../../utils/getDefaultCardImage';
import ConfirmationModal from '../modals/ConfirmationModal';
import Dialog from 'material-ui/Dialog';
import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import TeamActiveIcon from 'edc-web-sdk/components/icons/TeamActiveIcon';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import Tag from 'edc-web-sdk/components/icons/Tag';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import calculateSeconds from '../../utils/calculateSecondsForDuration';
import calculateFromSeconds from '../../utils/calculateFromSecondsForDuration';
import {
  saveTempPathway,
  deleteTempPathway,
  saveReorderCardIds,
  isPreviewMode
} from '../../actions/pathwaysActions';
import { browserHistory } from 'react-router';
import { filestackClient, getResizedUrl } from '../../utils/filestack';
import updateChannel from '../../utils/updateChannel';
import { fileStackSources } from '../../constants/fileStackSource';
import { Permissions } from '../../utils/checkPermissions';
import getCardType from '../../utils/getCardType';
import SmartCardVersions from './SmartCardVersions';
import TextField from 'material-ui/TextField';
import ActionInfo from 'react-material-icons/icons/action/info';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';
const viewColor = '#454560';

class PathwayEditor extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      providerBtnImage: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem',
        margin: '0 4px'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      cancel: {
        color: darkPurp,
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0',
        maxWidth: '100%'
      },
      chip: {
        margin: 0,
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '0 37px 0 8px',
        width: '100%'
      },
      chipClose: {
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        borderRadius: '50%',
        color: colors.primary,
        verticalAlign: 'middle',
        marginLeft: '1rem',
        marginBottom: '2px',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        position: 'absolute',
        right: '8px',
        top: '5px'
      },
      mainImage: {
        height: '32px',
        width: '32px',
        marginTop: '74px',
        fill: darkPurp
      },
      imageBtn: {
        paddingBottom: '0',
        height: 'auto'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      pathwayImage: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%'
      },
      labelChip: {
        paddingRight: '8px'
      },
      tileView: {
        border: 0,
        padding: 0,
        marginRight: 10,
        width: 14,
        height: 14
      },
      popover: {
        position: 'relative',
        zIndex: '12',
        width: '33.0625rem',
        padding: '1rem',
        overflow: 'auto',
        top: '23rem',
        margin: '0',
        pointerEvents: 'auto'
      },
      popoverContainer: {
        padding: '0 2.5rem',
        pointerEvents: 'none'
      },
      activeView: {
        opacity: 1
      },
      inactiveView: {
        opacity: 0.5
      },
      actionBtnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      pathwayBanner: {
        background: 'url("/i/images/folder.png") 50% 50% no-repeat'
      },
      flatButton: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      flexBlock: {
        display: 'flex'
      },
      checkbox: {
        color: colors.primary,
        width: 'auto'
      },
      privateContent: {
        fontSize: '12px',
        marginTop: '3px'
      },
      labelCheckbox: {
        cursor: 'pointer',
        fontSize: '12px',
        fontWeight: 300,
        textAlign: 'left',
        color: '#6f708b',
        marginLeft: '-8px',
        width: 'auto',
        lineHeight: '24px'
      },
      labelEditBlock: {
        display: 'inline-flex',
        alignItems: 'flex-end',
        top: '0.1875rem',
        position: 'relative'
      },
      labelBlock: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      labelEdit: {
        cursor: 'pointer',
        fontSize: '0.75rem',
        position: 'relative',
        lineHeight: '1'
      },
      labelIcon: {
        cursor: 'pointer',
        height: '18px',
        marginLeft: '-2px'
      },
      badgesIconBlock: {
        display: 'flex',
        marginBottom: '20px'
      },
      badgeIcon: {
        marginTop: '15px',
        display: 'block',
        marginRight: '20px',
        width: '50px',
        height: '50px'
      },
      providerIcon: {
        marginTop: '15px',
        display: 'block',
        height: '50px'
      },
      badgeText: {
        maxWidth: '385px'
      },
      currentBadge: {
        fontSize: '14px',
        color: '#6f708b'
      },
      badgeLabel: {
        textAlign: 'left',
        width: '100%',
        marginTop: '5px'
      },
      badgeLabelCenter: {
        textAlign: 'center',
        width: '50px',
        marginTop: '5px'
      },
      selectedIcon: {
        border: '5px solid #adaec2',
        borderRadius: '30px',
        width: '60px',
        height: '60px'
      },
      icons: {
        width: '14px',
        height: '14px'
      },
      confirmationModal: {
        zIndex: 1503
      },
      confirmationContent: {
        transform: 'none'
      },
      iconRadio: {
        marginRight: '5px',
        fill: '#d6d6e1'
      },
      mr5: {
        marginRight: '5px'
      },
      radioBtn: {
        width: 'auto'
      },
      badgeBlock: {
        display: 'flex'
      },
      providerImageContainer: {
        position: 'relative',
        display: 'inline-block'
      },
      deleteProviderImageBtn: {
        width: '1.25rem',
        height: '1.25rem',
        padding: '0',
        background: '#000000',
        borderRadius: '50%'
      },
      deleteProviderImageBtnIcon: {
        width: '1.25rem',
        height: '1.25rem'
      },
      tagIconStyles: {
        height: '14px',
        width: '34px'
      },
      restrictToErrorStyle: {
        color: '#999aad',
        fontSize: '11px',
        bottom: '5px',
        paddingLeft: '10px'
      },
      privateChannelStyle: {
        color: '#999aad',
        fontSize: '12px',
        paddingTop: '8px',
        paddingBottom: '8px'
      },
      privateChannelIconStyle: {
        top: '2px',
        width: '14px',
        height: '14px',
        verticalAlign: 'middle',
        float: 'left'
      },
      disableForm: {
        opacity: '0.5',
        pointerEvents: 'none'
      }
    };

    let titleLabel = 'Create';
    let createLabel = 'Save for later';
    let previewLabel = 'Preview';
    let publishLabel = 'Publish';
    let pathway = {};
    let mainPathway = {};
    let selectedChannels = [];
    let nonCuratedChannelIds = [];
    let topics = [];
    let fileStack = [];
    let smartBites = [];
    let showMainImage = false;
    let advSettings = {};
    let fileStackProvider = false;
    let durationString;
    let selectedBia;
    let oldLockedCardId = false;
    let restrictedToUsers = [];
    let userOrTeamSuggestion = [];
    let shareOnSmartCardCreation = window.ldclient.variation('share-on-smart-card-creation', false);
    let isAbleToMarkPrivate =
      Permissions['enabled'] !== undefined && Permissions.has('MARK_AS_PRIVATE');
    let multilingualContent = window.ldclient.variation('multilingual-content', false);

    let language = props.pathway && props.pathway.language ? props.pathway.language : 'unspecified';

    if (props.pathway) {
      pathway = props.pathway;
      titleLabel = 'Update';
      mainPathway = props.pathway;
      if (!props.pathway.title) {
        mainPathway.title = props.pathway.message;
        mainPathway.message = '';
      }
      fileStack = props.pathway.filestack;
      showMainImage = !!props.pathway.filestack.length;
      if (props.pathway.channelIds && props.pathway.channelIds.length) {
        selectedChannels = props.pathway.channelIds;
      }
      if (props.pathway.tags) {
        topics = props.pathway.tags.map(tag => tag.name);
      }
      if (props.pathway.provider || props.pathway.provider === '') {
        advSettings.provider = props.pathway.provider;
        fileStackProvider = props.pathway.providerImage;
      }
      if (props.pathway.eclDurationMetadata) {
        durationString = calculateFromSeconds(
          props.pathway.eclDurationMetadata.calculated_duration,
          true
        );
        advSettings.duration = props.pathway.eclDurationMetadata.calculated_duration;
      }
      selectedBia = props.pathway.skillLevel || props.pathway.selectedBia;

      if (props.pathway.nonCuratedChannelIds && props.pathway.nonCuratedChannelIds.length) {
        nonCuratedChannelIds = props.pathway.nonCuratedChannelIds;
      }
    }
    this.isCreatePrivateContentByDefaultEnable =
      shareOnSmartCardCreation &&
      isAbleToMarkPrivate &&
      this.props.team.config &&
      this.props.team.config.create_private_card_by_default;
    this.showBIA = !!this.props.team.config.enabled_bia;
    this.state = {
      titleLabel,
      oldLockedCardId,
      createLabel,
      previewLabel,
      publishLabel,
      pathway,
      mainPathway,
      selectedChannels,
      nonCuratedChannelIds,
      multilingualContent,
      language,
      showMainImage,
      fileStack,
      fileStackProvider,
      topics,
      smartBites,
      restrictedToUsers,
      userOrTeamSuggestion,
      showRestrict:
        this.props.pathway && this.props.pathway.hasOwnProperty('showRestrict')
          ? this.props.pathway.showRestrict
          : true,
      disableSubmit: false,
      activeType: '',
      openEmpty: false,
      openAutoGeneratePopover: false,
      pendingAutoGenerate: false,
      titleErrorText: '',
      viewRegime: 'Tile',
      createErrorText: '',
      currentUser: props.currentUser,
      clicked: false,
      openBadgeOptions: false,
      badges: [],
      badgesText: '',
      currentBadge: {},
      tempBadge: {},
      openConfirm: false,
      countNewCards: 0,
      autoGenerateNumberCalc: 0,
      maxTagErrorText: '',
      isPrivateContent: this.isCreatePrivateContentByDefaultEnable
        ? !!props.pathway
          ? !props.pathway.isPublic
          : true
        : !!props.pathway
        ? !props.pathway.isPublic
        : false,
      isPathwayStandalone: !!~props.pathname.indexOf('/pathways/'),
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      selectedBia,
      showBadgeWarning: '',
      cardsToRemove: [],
      cardsToDelete: [],
      showAdvancedSettings: false,
      advSettings,
      durationString,
      arrToLeap: [],
      arrToReorder: [],
      limit: 12,
      shareOnSmartCardCreation: shareOnSmartCardCreation,
      showProjectCard: window.ldclient.variation('project-card', false),
      removeProviderImage: false,
      previewMode: false,
      tags: [],
      topicTags: [],
      userSuggestions: [],
      channelsSuggestions: [],
      groupsSuggestions: [],
      groupsForRestrict: [],
      usersForRestrict: [],
      cardIdsToLock: {},
      previousBadge: {},
      isEditCard: false,
      updatedCard: {},
      showVersions: window.ldclient.variation('card-versions', false),
      previousVersions: [],
      showPreviousversionsToggle: false,
      customBadgesUrlFlag: window.ldclient.variation('custom-badges-url', false),
      disableForm: false,
      isAbleToMarkPrivate: isAbleToMarkPrivate,
      popoverAutofocus: false,
      isCardV3: window.ldclient.variation('card-v3', false),
      popoverElm: null,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      uploadIteration: 0
    };
    this.updateFileStackFile = this.updateFileStackFile.bind(this);
    this.confirmRemoveCardFromPathway = this.confirmRemoveCardFromPathway.bind(this);
    this.cancelRemoveCardFromPathway = this.cancelRemoveCardFromPathway.bind(this);
    this.confirmDeleteFromPathway = this.confirmDeleteFromPathway.bind(this);
    this.enableBadge = window.ldclient.variation('pathway-badge', false);
    this.enablePreview = window.ldclient.variation('enable-edit-preview', true);
    this.changeBia = this.changeBia.bind(this);
    this.isCompleteField = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    let config = this.props.team.OrgConfig;
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;
    if (props.pathway) {
      this.state.completeMethod =
        props.pathway.autoComplete === false ? 'manually' : 'automatically';
    } else {
      this.state.completeMethod =
        this.completeMethodConf == 'disabledNext' ? 'manually' : 'automatically';
    }
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
    this.restrictToFlag = window.ldclient.variation('restrict-to', false);
    this.disablePublish = this.disablePublish.bind(this);
    this.closeRemoveCardFromPathwayModal = this.closeRemoveCardFromPathwayModal.bind(this);
  }

  handleTouchTap = event => {
    event.preventDefault();
    this.setState(
      {
        openEmpty: true,
        closeClick: false,
        createErrorText: '',
        popoverElm: event.target
      },
      () => {
        setTimeout(this.setState({ popoverAutofocus: true }), 200);
      }
    );
  };

  openCardEdit = (e, card, index) => {
    let cardType = getCardType(card) || '';
    let pollPermission =
      (cardType === 'POLL' || cardType === 'QUIZ') && Permissions.has('MANAGE_CARD');
    let textCardPermission = Permissions.has('CREATE_TEXT_CARD');
    if ((pollPermission && card.hasAttempted) || (cardType === 'TEXT' && !textCardPermission)) {
      return;
    }
    this.setState(
      {
        openEmpty: true,
        closeClick: false,
        createErrorText: '',
        isEditCard: true,
        updatedCard: card,
        updatedCardIndex: index - 1,
        activeType: cardType.toLowerCase(),
        popoverElm: e.target
      },
      () => {
        setTimeout(this.setState({ popoverAutofocus: true }), 200);
      }
    );
  };

  handleRequestClose = e => {
    let targetElement, targetIdent;
    if (e) {
      targetElement = e.target || e.srcElement;
      targetIdent = targetElement.id || targetElement.className;
    }
    if (
      !e ||
      (targetIdent &&
        !~targetIdent.indexOf('fsp') &&
        !~targetIdent.indexOf('fst') &&
        !~targetIdent.indexOf('cropper') &&
        !~targetElement.textContent.indexOf('Upload'))
    ) {
      this.setState({
        openEmpty: false,
        openAutoGeneratePopover: false,
        createErrorText: '',
        updatedCard: {},
        popoverAutofocus: false,
        activeType: ''
      });
    }
  };

  async handleAutoGenerateContent(offset) {
    if (!this.state.autoGenerateInput || !this.state.autoGenerateInputNumber) {
      let createErrorText =
        !this.state.autoGenerateInput && !this.state.autoGenerateInputNumber
          ? 'Enter topic and number of cards'
          : !this.state.autoGenerateInput
          ? 'Enter topic'
          : 'Enter number of cards';
      this.setState({ createErrorText });
      return;
    }
    let generateNumber = !!this.state.autoGenerateNumberCalc
      ? this.state.autoGenerateNumberCalc
      : this.state.autoGenerateInputNumber;
    this.setState({ pendingAutoGenerate: true, createErrorText: '' });
    let params = { q: this.state.autoGenerateInput, limit: generateNumber, offset: offset };
    params['content_type[]'] = ['article', 'poll', 'video', 'insight'];
    let items = await eclSearch(params); //.then(items => {
    if (!items.cards.length && !this.state.recallGenerate) {
      this.setState({
        createErrorText: 'Error while adding card to pathway. No cards with this topic',
        openEmpty: false,
        openAutoGeneratePopover: false,
        pendingAutoGenerate: false,
        autoGenerateNumberCalc: 0,
        countNewCards: 0,
        popoverAutofocus: false
      });
      return;
    }
    if (!items.cards.length && this.state.recallGenerate) {
      this.props.dispatch(
        openSnackBar(
          `We have less cards by your request. We’ve just generated ${
            this.state.countNewCards
          } cards.`,
          true
        )
      );
      this.setState({
        activeType: '',
        pendingAutoGenerate: false,
        recallGenerate: false,
        countNewCards: 0,
        autoGenerateNumberCalc: 0
      });
      await this.handleRequestClose();
      return;
    }
    let smartBites = _.uniqBy(this.state.smartBites.concat(items.cards), 'id');
    let CountDiff = smartBites.length - this.state.smartBites.length;
    if (generateNumber > CountDiff) {
      await this.setState({
        countNewCards: this.state.countNewCards + CountDiff,
        recallGenerate: true,
        autoGenerateNumberCalc: generateNumber - CountDiff,
        smartBites
      });
      await this.handleAutoGenerateContent(offset + items.cards.length);
      return;
    }
    this.setState({
      smartBites,
      activeType: '',
      pendingAutoGenerate: false,
      countNewCards: 0,
      autoGenerateNumberCalc: 0
    });
    this.handleRequestClose();
  }

  componentDidMount() {
    this.props.dispatch(closeSnackBar());
    this.fetchPathwayDetails();
    setTimeout(() => {
      addTabInRadioButtons('bia-radio', this.changeBia);
    }, 500);
  }

  fetchPathwayDetails = () => {
    if (this.props.pathway && this.props.pathway.id) {
      pathwayEdit(this.props.pathway.id)
        .then(data => {
          if (data) {
            data.packCards.map(smartBite => {
              if (smartBite.resource === null) {
                delete smartBite.resource;
              }
              return smartBite;
            });
          }
          let mergePathwaysResponse = { ...this.props.pathway, ...data };
          let mainPathway = mergePathwaysResponse;
          let selectedChannels = [];
          let nonCuratedChannelIds = [];
          let topics = [];
          let fileStack = [];
          let smartBites = [];
          let showMainImage = false;
          let advSettings = {};
          let fileStackProvider = false;
          let durationString;
          let selectedBia;
          let oldLockedCardId = false;

          if (!mergePathwaysResponse.title) {
            mainPathway.title = mergePathwaysResponse.message;
            mainPathway.message = '';
          }
          fileStack = mergePathwaysResponse.filestack;
          showMainImage = !!mergePathwaysResponse.filestack.length;
          if (mergePathwaysResponse.channelIds.length) {
            selectedChannels = mergePathwaysResponse.channelIds;
          }
          topics = mergePathwaysResponse.tags.map(tag => tag.name);
          if (mergePathwaysResponse.provider || mergePathwaysResponse.provider === '') {
            advSettings.provider = mergePathwaysResponse.provider;
            fileStackProvider = mergePathwaysResponse.providerImage;
          }
          if (mergePathwaysResponse.eclDurationMetadata) {
            durationString = calculateFromSeconds(
              mergePathwaysResponse.eclDurationMetadata.calculated_duration,
              true
            );
            advSettings.duration = mergePathwaysResponse.eclDurationMetadata.calculated_duration;
          }
          smartBites = mergePathwaysResponse.packCards ? mergePathwaysResponse.packCards : [];
          selectedBia = mergePathwaysResponse.skillLevel || mergePathwaysResponse.selectedBia;

          if (mergePathwaysResponse.packCards && mergePathwaysResponse.packCards.length) {
            let cardLocked = mergePathwaysResponse.packCards.find(item => item.locked === true);
            oldLockedCardId = cardLocked ? cardLocked.id : false;
          }
          if (
            mergePathwaysResponse.nonCuratedChannelIds &&
            mergePathwaysResponse.nonCuratedChannelIds.length
          ) {
            nonCuratedChannelIds = mergePathwaysResponse.nonCuratedChannelIds;
          }

          let language = mergePathwaysResponse.language
            ? mergePathwaysResponse.language
            : 'unspecified';
          this.setState(
            {
              pathway: mergePathwaysResponse,
              oldLockedCardId,
              mainPathway,
              selectedChannels,
              nonCuratedChannelIds,
              showMainImage,
              fileStack,
              language,
              fileStackProvider,
              topics,
              smartBites,
              selectedBia,
              advSettings,
              durationString,
              completeMethod:
                mergePathwaysResponse.autoComplete === false ? 'manually' : 'automatically',
              showRestrict: mergePathwaysResponse.hasOwnProperty('showRestrict')
                ? mergePathwaysResponse.showRestrict
                : true,
              isPrivateContent: this.isCreatePrivateContentByDefaultEnable
                ? !!this.props.pathway
                  ? !mergePathwaysResponse.isPublic
                  : true
                : !!this.props.pathway
                ? !mergePathwaysResponse.isPublic
                : false
            },
            () => {
              if (!!this.state.pathway.packCards && !!this.state.pathway.packCards.length) {
                smartBites = [];
                let countCard = 0;
                for (let i = 0; i < this.state.pathway.packCards.length; i++) {
                  countCard++;
                  if (
                    !this.state.pathway.packCards[i].id &&
                    this.state.pathway.packCards[i].message ===
                      'You are not authorized to access this card'
                  ) {
                    this.state.pathway.packCards[i].isPrivate = true;
                    this.state.pathway.packCards[i].id =
                      this.state.pathway.packCards[i].card_id || this.state.pathway.packCards[i].id;
                  }
                  smartBites[i] = this.state.pathway.packCards[i];
                  if (countCard === this.state.pathway.packCards.length) {
                    smartBites.forEach((item, index) => {
                      item.isLocked = this.state.pathway.packCards[index].locked;
                    });
                  }
                  let initialSmartbitesIds = smartBites.map(item => item.id);
                  this.setState({ smartBites, initialSmartbitesIds });
                }
              }
              this.getApiCalls();
            }
          );
        })
        .catch(err => {
          console.error(`Error in PathwayEditor.fetchPathwayDetails.func : ${err}`);
        });
    } else {
      this.getApiCalls();
    }
  };

  setPathwayCurrentBadge(badges) {
    if (this.props.pathway && this.state.pathway.badging) {
      let currentBadge = {
        text: this.state.pathway.badging.title,
        id: this.state.pathway.badging.badgeId,
        imageUrl: this.state.pathway.badging.imageUrl
      };

      let previousBadge = {
        text: this.state.pathway.badging.title,
        id: this.state.pathway.badging.badgeId,
        imageUrl: this.state.pathway.badging.imageUrl
      };

      this.setState({
        currentBadge,
        badgeOn: true,
        tempBadge: currentBadge,
        badges,
        previousBadge
      });
    } else {
      this.setState({ badges });
    }
  }

  getApiCalls = () => {
    if (this.enableBadge) {
      let teamConfig = this.props.team.config;
      let isCustomBadgesEnabled =
        this.state.customBadgesUrlFlag &&
        teamConfig &&
        teamConfig['custom_badges_bucket_name'] &&
        teamConfig['custom_badges_region'];
      if (isCustomBadgesEnabled) {
        let query = {
          bucket_info: {
            bucket_name: teamConfig['custom_badges_bucket_name'],
            bucket_region: teamConfig['custom_badges_region']
          }
        };
        getDefaultImageS3(query)
          .then(data => {
            if (data && data.image_links && data.image_links.length) {
              let badges = data.image_links.map(item => ({ imageUrl: item }));
              this.setPathwayCurrentBadge(badges);
            }
          })
          .catch(err => {
            console.error(`Error in PathwayEditor.getDefaultImageS3.func : ${err}`);
          });
      } else {
        fetchBadges({ type: 'CardBadge', is_default: true })
          .then(data => {
            this.setPathwayCurrentBadge(data.badges);
          })
          .catch(err => {
            console.error(`Error in PathwayEditor.fetchBadges.func : ${err}`);
          });
      }
    }

    let payloadGroup = { limit: 1000, offset: 0 };

    if (!(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) {
      payloadGroup['writables'] = true;
    }

    getListV2(payloadGroup)
      .then(groups => {
        let suggestionState = this.state.groupsSuggestions;
        let suggestionStateForRestrict = this.state.groupsForRestrict;
        groups.teams.map(group => {
          let suggestion = {
            name: group.name,
            id: group.id,
            label: 'group'
          };
          suggestionState.push(suggestion);
          suggestionStateForRestrict.push(suggestion);
        });
        this.setState(
          { groupsSuggestions: suggestionState, groupsForRestrict: suggestionStateForRestrict },
          () => {
            let teamsPermitted =
              this.state.pathway && this.state.pathway.teamsPermitted
                ? this.state.pathway.teamsPermitted
                : [];
            teamsPermitted.forEach(team => {
              let teamsObj = team;
              teamsObj.label = 'group';
              this.handleUserAddition(teamsObj);
            });
          }
        );
      })
      .catch(err => {
        console.error(`Error in PathwayEditor.getListV2.func : ${err}`);
      });

    /*----- Finding the channel suggestions-------*/
    let channelsSuggestions = this.props.currentUser.writableChannels;
    if (channelsSuggestions) {
      let suggestionState = this.state.channelsSuggestions;
      channelsSuggestions.map(channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };

        suggestionState.push(suggestion);
      });
      this.setState({ channelsSuggestions: suggestionState });
    }

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.pathway && !!this.state.pathway.tags) {
      let existingTags = this.state.pathway.tags;
      this.setState({ topicTags: existingTags });
    }

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.pathway) {
      let usersPermitted = this.state.pathway.usersPermitted
        ? this.state.pathway.usersPermitted
        : [];
      usersPermitted.forEach(user => {
        let userObj = user;
        userObj.label = 'member';
        this.handleUserAddition(userObj);
      });
    }
    /*---------- Pushing users to tags--------- */
    if (!!this.props.pathway && !!this.state.pathway.usersWithAccess) {
      let existingMembers = this.state.pathway.usersWithAccess;
      _.map(existingMembers, member => {
        let suggestion = {
          name: member.fullName,
          id: member.id,
          label: 'member'
        };
        this.handleAddition(suggestion);
      });
    }

    /*----- Pushing channels to tags -----------*/
    if (!!this.props.pathway && !!this.state.pathway.channels) {
      let existingChannels = this.state.pathway.channels;
      _.map(existingChannels, channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };
        this.handleAddition(suggestion, false);
      });
    }
    if (
      !(!!this.props.pathway && !!this.props.pathway.channels) &&
      /channel\//i.test(this.props.pathname)
    ) {
      let channel = _.values(this.props.channelsV2)[0];
      let suggestion = {
        name: channel.label,
        id: channel.id,
        label: 'channel',
        isPrivate: channel.isPrivate
      };
      this.handleAddition(suggestion, false);
    }

    /*----------Pushing groups to tags----------- */
    if (!!this.props.pathway && !!this.state.pathway.teams) {
      _.map(this.state.pathway.teams, group => {
        let suggestion = {
          name: group.label,
          id: group.id,
          label: 'group'
        };
        this.handleAddition(suggestion);
      });
    }

    let list = document.getElementById('listWithHandle');
    Sortable.create(list, {
      handle: '.move-card-btn',
      animation: 150,
      forceFallback: true,
      group: {
        name: 'smartbites',
        pull: 'clone',
        revertClone: true
      },
      onUpdate: this.changeOrder
    });

    /*-----Fetching pathway previous versions-------*/
    if (this.props.pathway && this.props.pathway.id) {
      let payload = {
        entity_type: 'card',
        entity_id: this.props.pathway.id
      };
      showPreviousCardVersions(payload)
        .then(data => {
          this.setState({
            previousVersions: data.versions
          });
        })
        .catch(err => {
          console.error(`Error in fetching previous versions of cards : ${err}`);
        });
    }
  };

  toggleVersionSettings = e => {
    e.preventDefault();
    this.setState({
      showPreviousversionsToggle: !this.state.showPreviousversionsToggle
    });
  };

  changeViewMode = viewRegime => {
    this.setState({ viewRegime });
  };

  changeOrder = evt => {
    let smartBites = this.state.smartBites;
    this.setState({ smartBites: [] });
    let removedElement = smartBites.splice(evt.oldIndex, 1);
    smartBites.splice(evt.newIndex, 0, removedElement[0]);
    this.setState({ smartBites });
    this.saveOrder(smartBites);
  };

  saveOrder = smartBites => {
    if (this.state.pathway.id) {
      let arrToReorder = [];
      smartBites.forEach(card => {
        arrToReorder.push(+card.id);
      });
      this.props.dispatch(saveReorderCardIds(arrToReorder));
    }
  };

  closeModal = () => {
    this.setState({
      selectedChannels: [],
      topics: [],
      fileStack: [],
      mainPathway: {},
      pathway: {},
      showMainImage: false,
      createLabel: 'Save for later',
      publishLabel: 'Publish',
      disableForm: false,
      previewLabel: 'Preview'
    });
    if (this.props.cardUpdated) {
      this.props.cardUpdated();
    }
    if (this.props.isPreviewMode) {
      browserHistory.go(-2);
      this.props.dispatch(isPreviewMode(false));
    }
    this.props.dispatch(close('pathway'));
    document.getElementsByClassName('createButton')[0].focus();
  };

  createClickHandler = () => {
    if (!this.state.mainPathway.title || !this.state.mainPathway.title.replace(/\s/g, '').length) {
      this.setState({ titleErrorText: "Title can't be empty!" });
      return;
    }
    if (
      (this.state.tags && this.state.tags.length) ||
      (this.state.selectedChannels && this.state.selectedChannels.length)
    ) {
      this.setState({
        createErrorText:
          'You can’t share card in draft state! Please clear Share/Post to field or publish the card!'
      });
      return;
    }
    this.setState({
      createLabel: this.props.pathway ? 'Updating...' : 'Creating...',
      createErrorText: '',
      disableForm: 'true'
    });
    this.savePathway(async data => {
      if (data) {
        if (this.state.arrToLeap.length) {
          this.state.arrToLeap.forEach(leapObj => {
            let payload = {
              correct_id: leapObj.correctId,
              wrong_id: leapObj.wrongId,
              pathway_id: data.id
            };
            if (leapObj.leapId) {
              updateLeap(leapObj.cardId, leapObj.leapId, payload);
            } else {
              createLeap(leapObj.cardId, payload);
            }
          });
        }
        if (this.props.cardUpdated) {
          this.props.cardUpdated(data);
        }
        if (this.props.reorderCardIds && this.props.reorderCardIds.length) {
          await customReorderPathwayCards(this.state.pathway.id, this.props.reorderCardIds);
        }
        let updateContentTab = !!~this.props.pathname.indexOf('me/content') && this.props.pathway;
        this.props.dispatch(this.closeModal);
        if (updateContentTab) {
          this.props.dispatch(updateCurrentCard(data.card));
        }
        setTimeout(() => {
          if (this.state.newModalAndToast) {
            this.props.dispatch(
              openSnackBar(
                `Your draft Pathway is saved!${
                  !this.state.isPathwayStandalone ? ' Go to Me/Content to finish' : ''
                }`,
                () => {
                  !updateContentTab &&
                    !this.state.isPathwayStandalone &&
                    !this.state.isAddingToCurrentChannel &&
                    this.openContentTab();
                }
              )
            );
          } else {
            this.props.dispatch(
              openStatusModal(
                `Your draft Pathway is saved!${
                  !this.state.isPathwayStandalone ? ' Go to Me/Content to finish' : ''
                }`,
                () => {
                  !updateContentTab &&
                    !this.state.isPathwayStandalone &&
                    !this.state.isAddingToCurrentChannel &&
                    this.openContentTab();
                }
              )
            );
          }
        }, 500);
      }
    });
  };

  async savePathway(callback) {
    let sharedtoMembers = [];
    let sharedtoGroups = [];
    let sharedtoChannels = []; //this.state.selectedChannels;
    let teams_with_permission_ids = [];
    let users_with_permission_ids = [];

    this.state.tags.forEach(tag => {
      switch (tag.label) {
        case 'member':
          sharedtoMembers.push(tag.id);
          break;
        case 'group':
          sharedtoGroups.push(tag.id);
          break;
        case 'channel':
          sharedtoChannels.push(tag.id);
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });

    let payload = {
      message: this.state.mainPathway.message || this.state.mainPathway.title,
      title: this.state.mainPathway.title,
      state: this.props.pathway ? this.state.pathway.state : 'draft',
      card_type: 'pack',
      card_subtype: this.props.pathway ? this.state.pathway.cardSubtype : 'simple',
      users_with_access_ids: sharedtoMembers,
      team_ids: sharedtoGroups,
      auto_complete: this.state.completeMethod === 'automatically',
      is_public: !this.state.isPrivateContent + ''
    };
    if (this.state.selectedBia) {
      let cardMetadatum = this.state.pathway.cardMetadatum;
      if (cardMetadatum) {
        payload['card_metadatum_attributes'] = {
          id: cardMetadatum.id,
          level: this.state.selectedBia
        };
      } else {
        payload['card_metadatum_attributes'] = { level: this.state.selectedBia };
      }
    } else if (
      this.state.selectedBia === '' &&
      this.props.pathway &&
      (this.props.pathway.skillLevel || this.props.pathway.selectedBia) &&
      this.state.pathway.cardMetadatum
    ) {
      payload['card_metadatum_attributes'] = {
        id: this.state.pathway.cardMetadatum.id,
        level: null
      };
    }
    if (
      this.enableBadge &&
      this.state.currentBadge &&
      this.state.currentBadge.text &&
      !!this.state.currentBadge.text.trim()
    ) {
      if (this.state.currentBadge.id) {
        payload['card_badging_attributes'] = {
          title: this.state.currentBadge.text.trim(),
          badge_id: this.state.currentBadge.id,
          all_quizzes_answered: this.state.enableBadgeForQuizzes
        };
      } else if (this.state.currentBadge.imageUrl) {
        let newBadge = await createBadge({
          type: 'CardBadge',
          image_url: this.state.currentBadge.imageUrl
        });
        payload['card_badging_attributes'] = {
          title: this.state.currentBadge.text.trim(),
          badge_id: newBadge.id
        };
      }
    }

    if (this.state.multilingualContent) {
      this.state.language === 'unspecified'
        ? (payload.language = null)
        : (payload.language = this.state.language);
    }

    if (this.state.topics || !!this.state.pathway.tags.length) {
      payload.topics = this.state.topics;
    }
    if (
      this.state.selectedChannels ||
      !!this.state.nonCuratedChannelIds.length ||
      !!sharedtoChannels.length
    ) {
      sharedtoChannels = _.merge(this.state.selectedChannels, sharedtoChannels);
      if (!!this.state.nonCuratedChannelIds.length) {
        sharedtoChannels = _.uniq(sharedtoChannels.concat(this.state.nonCuratedChannelIds));
      }
      payload.channel_ids = sharedtoChannels;
    }
    //=====restricted to payload
    if (this.state.isPrivateContent) {
      this.state.restrictedToUsers.map(restrictTo => {
        if (restrictTo.label == 'group') {
          teams_with_permission_ids.push(restrictTo.id);
        }
        if (restrictTo.label == 'member') {
          users_with_permission_ids.push(restrictTo.id);
        }
      });

      if (teams_with_permission_ids.length > 0) {
        payload['teams_with_permission_ids'] = teams_with_permission_ids;
      }

      if (users_with_permission_ids.length > 0) {
        payload['users_with_permission_ids'] = users_with_permission_ids;
      }
    }

    if (
      (this.restrictToFlag && !this.state.isPrivateContent) ||
      (this.restrictToFlag && this.state.restrictedToUsers.length == 0)
    ) {
      payload['teams_with_permission_ids'] = [];
      payload['users_with_permission_ids'] = [];
    }

    if (!!this.state.fileStack.length) {
      payload.filestack = this.state.fileStack;
    } else {
      const defaultImg = getDefaultImage(this.props.currentUser.id);
      payload.filestack = [defaultImg];
    }
    if (this.state.advSettings.provider || this.state.advSettings.provider === '') {
      payload.provider = this.state.advSettings.provider;
    }
    if (this.state.removeProviderImage || this.state.fileStackProvider) {
      payload.provider_image = this.state.fileStackProvider;
      this.setState({ removeProviderImage: false });
    }
    if (this.state.advSettings.duration) {
      payload.duration = this.state.advSettings.duration;
    }
    if (this.state.cardsToRemove.length) {
      await batchRemoveFromPathway(this.state.pathway.id, this.state.cardsToRemove, false);
    }
    if (this.state.cardsToDelete.length) {
      await batchRemoveFromPathway(this.state.pathway.id, this.state.cardsToDelete, true);
    }
    this.setState({ clicked: true }, () => {
      postv2({ card: payload }, this.state.pathway && this.state.pathway.id)
        .then(data => {
          if (data && data.embargoErr) {
            this.props.dispatch(
              openSnackBar(
                'Sorry, the content you are trying to post is from unapproved website or words.',
                true
              )
            );
            this.setState({
              clicked: false,
              publishLabel: 'Publish',
              createLabel: 'Save for later',
              disableForm: false
            });
          } else {
            if (!!this.state.smartBites.length) {
              let ids = _.uniq(this.state.smartBites.map(card => card.id || card.cardId));
              let uniqCards = _.uniqBy(this.state.smartBites, 'id');
              if (this.props.pathway && this.props.pathway.packCards) {
                let currentIds = this.props.pathway.packCards.map(item => item.id + '');
                let idsToAdd = _.difference(ids, currentIds);
                if (!idsToAdd.length) {
                  ids = [];
                  if (Object.keys(this.state.cardIdsToLock).length && this.props.pathway) {
                    this.lockCards(callback, data);
                  } else {
                    return callback(data);
                  }
                } else {
                  ids = idsToAdd;
                }
              }
              if (ids.length) {
                return batchAddToPathway(data.id, ids, 'Card')
                  .then(() => {
                    if (Object.keys(this.state.cardIdsToLock).length) {
                      this.lockCards(callback, data);
                    } else {
                      return callback(data);
                    }
                  })
                  .catch(err => {
                    console.error(`Error in PathwayEditor.batchAddToPathway.func : ${err}`);
                  });
              }
            } else {
              return callback(data);
            }
          }
        })
        .catch(err => {
          console.error(`Error in PathwayEditor.postv2.func : ${err}`);
          return callback();
        });
    });
  }

  handleUserAddition = tag => {
    switch (tag.label) {
      case 'member':
        _.remove(this.state.usersForRestrict, existingMember => {
          return existingMember.id + '' === tag.id + '';
        });
        break;
      case 'group':
        _.remove(this.state.groupsForRestrict, existingGroup => {
          return existingGroup.id + '' === tag.id + '';
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }

    let restrictedToUsers = this.state.restrictedToUsers;
    restrictedToUsers.push(tag);
    this.setState({ restrictedToUsers });
  };

  handleUserDelete = tagInfo => {
    let tag = typeof tagInfo === 'number' ? this.state.restrictedToUsers[tagInfo] : tagInfo;

    switch (tag.label) {
      case 'member':
        let newUserSuggestions = _.concat(this.state.usersForRestrict, tag);
        this.setState({
          usersForRestrict: newUserSuggestions
        });
        break;
      case 'group':
        let newGroupSuggestions = _.concat(this.state.groupsForRestrict, tag);
        this.setState({
          groupsForRestrict: newGroupSuggestions
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    let restrictedToUsers = this.state.restrictedToUsers.filter(data => data.id != tag.id);
    this.setState({ restrictedToUsers });
  };

  handleRestrictedInputChange = query => {
    if (!!!query) {
      return;
    }
    let team_ids = [];
    let channel_ids = [];
    let payload = {
      limit: 5,
      offset: 0,
      q: query
    };
    this.state.tags.map(tag => {
      if (tag.label === 'group') {
        team_ids.push(tag.id);
      } else if (tag.label === 'channel') {
        channel_ids.push(tag.id);
      }
    });

    if (team_ids.length > 0) {
      payload['team_ids[]'] = team_ids;
    }

    if (channel_ids.length > 0) {
      payload['channel_ids[]'] = channel_ids;
    }

    if (!!this.props.pathway) {
      payload['card_id'] = this.state.pathway.id;
    }

    getRestrictToUserOrGroup(payload)
      .then(restrictedToData => {
        let users = restrictedToData.users.map(user => {
          let editedUsers = user;
          editedUsers['label'] = 'member';
          return editedUsers;
        });
        let groups = restrictedToData.groups.map(group => {
          let editedGroup = group;
          editedGroup['label'] = 'group';
          return editedGroup;
        });
        let userOrTeamSuggestion = [...users, ...groups];

        this.setState({ userOrTeamSuggestion });
      })
      .catch(() => {});
  };

  lockCards = (callback, data) => {
    let cardIdsToLock = this.state.cardIdsToLock;
    let arrLockPromises = [];
    for (let prop in cardIdsToLock) {
      if (!~this.state.smartBites.findIndex(item => item.id === prop)) {
        continue;
      }
      let payloadForLockPathwayCard = {
        pathway_id: data.id,
        locked: `${cardIdsToLock[prop]}`
      };
      arrLockPromises.push(lockPathwayCard(prop, payloadForLockPathwayCard));
    }
    Promise.all(arrLockPromises)
      .then(() => {
        return callback(data);
      })
      .catch(err => {
        console.error(`Error in PathwayEditor.arrLockPromises.func : ${err}`);
      });
  };

  previewClickHandler = async () => {
    if (!this.state.mainPathway.title || !this.state.mainPathway.title.replace(/\s/g, '').length) {
      this.setState({ titleErrorText: "Title can't be empty!" });
      return;
    }

    this.setState({ previewLabel: 'Previewing...' });
    let payload = {};
    if (this.state.pathway && this.state.pathway.id) {
      payload = this.state.pathway;
      if (this.state.initialSmartbitesIds && this.state.initialSmartbitesIds.length) {
        let changedCardsIds = _.differenceWith(
          this.state.smartBites,
          this.state.initialSmartbitesIds,
          (a, b) => a.id === b
        );
        let needToReorder = this.props.reorderCardIds && !!this.props.reorderCardIds.length;
        payload.isPathwayChanged =
          this.state.initialSmartbitesIds.length !== this.state.smartBites.length ||
          !!changedCardsIds.length ||
          needToReorder ||
          (this.state.arrToLeap && this.state.arrToLeap.length) ||
          (this.state.cardIdsToLock && this.state.cardIdsToLock.length);
        payload.cardsToRemove = this.state.cardsToRemove;
      }
      payload.pathwayPackCards = this.state.pathway.packCards;
    } else {
      payload = {
        message: this.state.mainPathway.message || this.state.mainPathway.title,
        title: this.state.mainPathway.title,
        cardType: 'pack',
        cardSubtype: this.state.mainPathway.pathwayType,
        state: this.props.mainPathway ? this.props.mainPathway.state : 'draft',
        pathwayPackCards: []
      };
    }
    if (
      this.enableBadge &&
      this.state.currentBadge &&
      this.state.currentBadge.text &&
      !!this.state.currentBadge.text.trim()
    ) {
      if (this.state.currentBadge.id) {
        payload.badging = {
          id: this.state.currentBadge.id,
          title: this.state.currentBadge.text,
          imageUrl: this.state.currentBadge.imageUrl
        };
      } else if (this.state.currentBadge.imageUrl) {
        payload.badging = {
          ...(await createBadge({
            type: 'CardBadge',
            image_url: this.state.currentBadge.imageUrl
          })),
          ...{ title: this.state.currentBadge.text }
        };
      }
    }
    if (this.state.topics || !!this.state.pathway.tags.length) {
      let tags = [];
      for (let i = 0; i < this.state.topics.length; i++) {
        let temp = {};
        temp.name = this.state.topics[i];
        tags[i] = temp;
      }
      payload.tags = tags;
    }

    if (this.state.selectedChannels) {
      let channels = [];
      for (let i = 0; i < this.state.selectedChannels.length; i++) {
        for (let j = 0; j < this.props.currentUser.writableChannels.length; j++) {
          let channel = {};
          if (this.state.selectedChannels[i] === this.props.currentUser.writableChannels[j].id) {
            channel.label = this.props.currentUser.writableChannels[j].label;
            channel.id = this.props.currentUser.writableChannels[j].id;
            channels.push(channel);
          }
        }
      }
      payload.channelIds = this.state.selectedChannels;
      payload.channels = channels;
    }
    if (!!this.state.fileStack.length) {
      payload.filestack = this.state.fileStack;
    } else {
      const defaultImg = getDefaultImage(this.props.currentUser.id);
      payload.filestack = [defaultImg];
    }
    if (this.state.advSettings.provider) {
      payload.provider = this.state.advSettings.provider;
    }
    if (
      this.state.removeProviderImage ||
      (this.state.fileStackProvider && !!this.state.fileStackProvider.length)
    ) {
      payload.providerImage = this.state.fileStackProvider;
    }
    if (this.state.advSettings.duration) {
      payload.eclDurationMetadata = {};
      payload.eclDurationMetadata.calculated_duration = this.state.advSettings.duration;
      payload.eclDurationMetadata.calculated_duration_display = calculateFromSeconds(
        this.state.advSettings.duration
      );
    }
    if (this.props.currentUser) {
      payload.author = this.props.currentUser;
    }
    if (this.state.smartBites) {
      payload.packCards = this.state.smartBites;
    }
    if (this.state.selectedBia) {
      payload.selectedBia = this.state.selectedBia;
    }
    if (this.state.arrToLeap) {
      payload.arrToLeap = this.state.arrToLeap;
    }
    if (this.state.cardIdsToLock) {
      payload.cardIdsToLock = this.state.cardIdsToLock;
    }
    if (this.state.oldLockedCardId) {
      payload.oldLockedCardId = this.state.oldLockedCardId;
    }
    if (this.state.tags) {
      payload.teams = [];
      this.state.tags.map(tag => {
        if (tag.label === 'group') {
          payload.teams.push(tag);
        }
      });
    }
    if (this.state.multilingualContent) {
      payload.language = this.state.language;
    }
    this.props.dispatch(isPreviewMode(true));
    if (!~this.props.pathname.indexOf('/pathways/')) {
      this.props.dispatch(saveTempPathway(payload));
      this.props.pathway && this.state.pathway.id
        ? this.props.dispatch(push(`/pathways/${this.state.pathway.slug}`))
        : this.props.dispatch(push(`/pathways/${payload.title}`));
    } else {
      this.props.dispatch(saveTempPathway(payload));
      this.props.dispatch(close());
    }
  };

  publishClickHandler = () => {
    if (!this.state.mainPathway.title || !this.state.mainPathway.title.replace(/\s/g, '').length) {
      this.setState({ titleErrorText: "Title can't be empty!" });
      return;
    }
    this.setState({ publishLabel: 'Publishing...', disableForm: true });

    this.savePathway(data => {
      this.props.dispatch(deleteTempPathway());
      if (data) {
        if (this.state.arrToLeap.length) {
          this.state.arrToLeap.forEach(leapObj => {
            let payload = {
              correct_id: leapObj.correctId,
              wrong_id: leapObj.wrongId,
              pathway_id: data.id
            };
            if (leapObj.leapId) {
              updateLeap(leapObj.cardId, leapObj.leapId, payload);
            } else {
              createLeap(leapObj.cardId, payload);
            }
          });
        }
        if (data.card && data.card.publishedAt) {
          this.afterPublish(data);
        } else {
          publishPathway(data.id)
            .then(() => {
              this.afterPublish(data);
            })
            .catch(err => {
              console.error(`Error in PathwayEditor.publishPathway.func : ${err}`);
              this.props.dispatch(this.closeModal);
            });
        }
      } else {
        this.props.dispatch(this.closeModal);
      }
    });
  };

  readyToViewCallback = (data, updateContentTab) => {
    let slugOrId, currentChannel, isAddingToCurrentChannel;
    if (data && data.card && this.props.pathname.indexOf('/channel/') > -1) {
      let routeParam = this.props.pathname.split('/');
      slugOrId = routeParam[routeParam.length - 1];
      currentChannel = this.props.channelsV2 && Object.values(this.props.channelsV2);
      currentChannel = currentChannel.length && currentChannel[0];
      isAddingToCurrentChannel =
        currentChannel && data.card.channelIds.indexOf(currentChannel.id) > -1;
    }
    if (
      currentChannel &&
      slugOrId &&
      (slugOrId == currentChannel.id || slugOrId === currentChannel.slug)
    ) {
      if (isAddingToCurrentChannel) {
        updateChannel(this.props.dispatch, currentChannel, this.state.limit, true);
      }
    } else {
      !updateContentTab && !this.state.isPathwayStandalone && this.openContentTab();
    }
  };

  afterPublish = async data => {
    if (this.props.cardUpdated) {
      this.props.cardUpdated();
    }
    if (this.props.reorderCardIds && this.props.reorderCardIds.length) {
      customReorderPathwayCards(this.state.pathway.id, this.props.reorderCardIds);
    }
    let updateContentTab = !!~this.props.pathname.indexOf('me/content') && this.props.pathway;
    if (updateContentTab) {
      this.props.dispatch(updateCurrentCard(data.card));
    }

    setTimeout(() => {
      let message = 'Your Pathway is ready to view!';
      if (
        !data.card.isPublic &&
        !data.card.teams.length &&
        !data.card.channels.length &&
        !data.card.usersWithAccess.length &&
        !data.card.teamsPermitted.length &&
        !data.card.usersPermitted.length
      ) {
        message = message + '<br> P.S. This pathway will not be visible to anyone but the author.';
      }
      if (data.card.isPublic) {
        message =
          message + ' This Pathway has been published publicly and will be accessible to everyone.';
      }

      if (this.state.newModalAndToast) {
        this.props.dispatch(
          openSnackBar(message, () => {
            this.readyToViewCallback(data, updateContentTab);
          })
        );
      } else {
        this.props.dispatch(
          openStatusModal(message, () => {
            this.readyToViewCallback(data, updateContentTab);
          })
        );
      }
    }, 500);
    this.props.dispatch(this.closeModal(true));
  };

  getNewChannels = (selectedChannels, channel, pathname) => {
    let isAddingToCurrentChannel = false;
    let currentChannel;
    if (
      channel &&
      channel.slug &&
      pathname &&
      (channel.slug === pathname[2] || `${channel.id}` === pathname[2])
    ) {
      isAddingToCurrentChannel = true;
      currentChannel = { id: channel.id, slug: channel.slug };
    }

    this.setState(
      { showPostToChannelMsg: false, selectedChannels, isAddingToCurrentChannel, currentChannel },
      () =>
        this.state.selectedChannels.map(id => {
          if (this.props.currentUser.writableChannels.find(e => e.id == id && e.isPrivate)) {
            this.setState({ showPostToChannelMsg: true });
          }
        })
    );
  };

  changeTag = event => {
    this.setState({
      maxTagErrorText:
        event.target.value.length <= 150
          ? ''
          : 'Max tag length exceeded. It must be shorter than 150 characters.'
    });
  };

  addTagHandler = event => {
    if (!~this.state.topics.indexOf(event.target.value.trim()) && !!event.target.value.trim()) {
      this.setState({ topics: this.state.topics.concat(event.target.value.trim()) });
    }
  };

  changeActiveType = (e, activeType) => {
    e.preventDefault();
    if (this.state.updatedCard.id) {
      return;
    }
    let uploadIteration = this.state.uploadIteration;
    if (activeType === 'upload') {
      ++uploadIteration;
    }
    this.setState({ activeType, uploadIteration });
  };

  pathwayMainChange = (type, event) => {
    let mainPathway = this.state.mainPathway;
    mainPathway[type] = event.target.value;
    this.setState({ mainPathway, titleErrorText: '' });
  };

  advSettingsChange = (type, event) => {
    let advSettings = this.state.advSettings;
    if (type === 'duration') {
      advSettings[type] = calculateSeconds(event.target.value);
      this.setState({
        durationString: event.target.value
      });
    } else {
      advSettings[type] = event.target.value;
    }
    this.setState({ advSettings });
  };

  openContentTab = () => {
    if (this.state.isAddingToCurrentChannel && this.state.currentChannel) {
      this.props.dispatch(
        close('pathway'),
        updateChannel(this.props.dispatch, this.state.currentChannel, this.state.limit)
      );
    } else if (this.props.pathname.includes('/search')) {
      this.props.dispatch(close('pathway'));
    } else if (this.props.pathname.includes('/me/')) {
      this.props.dispatch(push('/me/content'));
    } else {
      window.location.href = '/me/content';
    }
  };

  saveOutClick = (tempCard, searchText) => {
    this.setState({ tempCard, searchText });
  };

  renderTypeUI = () => {
    switch (this.state.activeType) {
      case '':
        return null;
      case 'autopathway':
        return (
          <div className="row autopathway-block">
            <input
              className="small-12"
              onChange={e => {
                this.setState({ autoGenerateInput: e.target.value });
              }}
              value={this.state.autoGenerateInput}
              type="text"
              placeholder={tr('Enter topic here')}
            />
            <select
              className="card-number-dropdown small-4"
              onChange={e => {
                this.setState({ autoGenerateInputNumber: e.target.value });
              }}
              value={this.state.autoGenerateInputNumber}
            >
              <option value=""># Cards</option>
              {[...new Array(10).keys()].map(i => {
                return <option value={i + 1}>{i + 1}</option>;
              })}
            </select>
            <div className="auto-generate-action-buttons">
              <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
              <FlatButton
                label={tr(this.state.pendingAutoGenerate ? 'Generating...' : 'Generate')}
                className="preview"
                disabled={this.state.pendingAutoGenerate}
                onTouchTap={this.handleAutoGenerateContent.bind(this, 0)}
                labelStyle={this.styles.flatButton}
                style={
                  this.state.autoGenerateInput &&
                  this.state.autoGenerateInputNumber &&
                  !this.state.pendingAutoGenerate
                    ? this.styles.create
                    : this.styles.disabled
                }
              />
            </div>
          </div>
        );
      default:
        return this.state.updatedCard.id ? (
          <SmartBite
            createdCard={this.createdCard}
            smartBites={this.state.smartBites}
            activeType={this.state.activeType}
            cardType="smartbite"
            card={this.state.updatedCard}
            closeClick={true}
            isPathwayPart={true}
          />
        ) : (
          <SmartBite
            tempCard={this.state.tempCard}
            closeClick={this.state.closeClick}
            searchText={this.state.searchText}
            saveOutClick={this.saveOutClick}
            createdCard={this.createdCard}
            smartBites={this.state.smartBites}
            activeType={this.state.activeType}
            cardType="pathway"
            isPathwayPart={true}
            uploadIteration={this.state.uploadIteration}
          />
        );
    }
  };

  fileStackHandler = isProvider => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: ['image/*'],
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(fileStack => this.updateFileStackFile(fileStack, isProvider))
          .catch(err => {
            console.error(`Error in PathwayEditor.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in PathwayEditor.uploadPolicyAndSignature.func : ${err}`);
      });
  };

  pathwayBannerFileStackHandler = (e, isProvider) => {
    e && e.preventDefault();
    this.fileStackHandler(isProvider);
  };

  updateFileStackFile(fileStack, isProvider) {
    let securedUrl;
    let tempPath = fileStack.filesUploaded;
    refreshFilestackUrl(tempPath[0].url)
      .then(resp => {
        securedUrl = resp.signed_url;
        if (isProvider) {
          this.setState({ fileStackProvider: tempPath[0].url, securedProviderUrl: securedUrl });
        } else {
          this.setState({
            fileStack: tempPath,
            showMainImage: tempPath.length,
            securedUrl: securedUrl
          });
        }
      })
      .catch(err => {
        console.error(`Error in PathwayEditor.updateFileStackFile.func : ${err}`);
      });
  }

  clearFileStackProviderInfo = () => {
    this.setState({ fileStackProvider: null, securedProviderUrl: null, removeProviderImage: true });
  };

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton aria-label={tr('Change Image')} style={this.styles.imageBtn}>
          <ImagePhotoCamera color="white" />
        </IconButton>
        <div className="roll-text">{tr('Change Image')}</div>
      </span>
    );
  }

  addCardToPathway = card => {
    return new Promise(resolve => {
      let smartBites = this.state.smartBites;
      let lastCard = smartBites.length ? smartBites[smartBites.length - 1] : null;
      smartBites.push(card);
      this.setState({ smartBites, activeType: '', closeClick: true }, resolve);
      this.saveOrder(smartBites);
    });
  };

  batchAddCardToPathway = cards => {
    return new Promise(resolve => {
      let smartBites = this.state.smartBites.concat(cards);
      if (this.state.pathway.id) {
        let ids = _.uniq(cards.map(card => card.id || card.cardId));
        batchAddToPathway(this.state.pathway.id, ids, 'Card');
      }
      this.setState({ smartBites, activeType: '', closeClick: true }, resolve);
    });
  };

  createdCard = (data, bia) => {
    if (this.state.isEditCard) {
      let smartBites = this.state.smartBites;
      if (data && !!data.card) smartBites[this.state.updatedCardIndex] = data && data.card;
      this.setState({
        smartBites,
        closeClick: false,
        createErrorText: '',
        updatedCard: {},
        activeType: '',
        updatedCardIndex: -1,
        isEditCard: false
      });
      this.handleRequestClose();
      return;
    }
    if (this.state.activeType === 'bookmark' || this.state.activeType === 'smart') {
      if (bia) {
        data.map(card => {
          card.newSkillLevel = bia;
        });
      }
      this.batchAddCardToPathway(data)
        .then(() => {
          this.setState({ activeType: '' });
          this.handleRequestClose();
        })
        .catch(() => {
          this.handleRequestClose();
          this.setState({ createErrorText: 'Error while adding card to pathway' });
        });
    } else {
      if (data && data.id) {
        this.addCardToPathway(data.card)
          .then(() => {
            this.handleRequestClose();
          })
          .catch(() => {
            this.handleRequestClose();
            this.setState({ createErrorText: 'Error while adding card to pathway' });
          });
      }
    }
    if (!data) {
      this.handleRequestClose();
    }
  };

  handleClose = () => {
    this.setState({ openConfirm: false });
  };

  removeCardFromPathway = (cardId, cardType, hidden) => {
    let externalCard = false;
    if (hidden === false) {
      externalCard = true;
    }
    this.setState({
      openConfirm: !this.state.openConfirm,
      cardToRemove: { cardId, cardType },
      externalCard: externalCard
    });
  };

  addToRemovalList = (cardId, cb) => {
    let cardsToRemove = this.state.cardsToRemove;
    cardsToRemove.push(cardId);
    return this.setState({ cardsToRemove }, cb(cardId));
  };

  addToDeleteList = (cardId, cb) => {
    let cardsToDelete = this.state.cardsToDelete;
    cardsToDelete.push(cardId);
    return this.setState({ cardsToDelete }, cb(cardId));
  };

  handlePrivateContent = event => {
    this.setState({ userActionedPrivateContent: true });
    this.setState(
      {
        isPrivateContent: event.target.checked
      },
      () => {
        this.setPrivateChannelMessage();
      }
    );
  };

  cancelRemoveCardFromPathway = () => {
    this.setState({ openConfirm: !this.state.openConfirm });
  };

  closeRemoveCardFromPathwayModal() {
    if (this.state.openConfirm) {
      this.setState({
        openConfirm: !this.state.openConfirm
      });
    }
  }

  confirmRemoveCardFromPathway() {
    this.props.dispatch(clearCurrentCard());
    let { cardId, cardType } = this.state.cardToRemove;
    this.setState({ openConfirm: !this.state.openConfirm });
    const removeFrontendCard = cardIdParam => {
      let smartBites = this.state.smartBites;
      smartBites = smartBites.filter(item => item.id !== cardIdParam);
      this.setState({ smartBites });
    };
    if (
      this.state.pathway.id &&
      this.state.pathway.packCards &&
      this.state.pathway.packCards.some(item => item.id === cardId)
    ) {
      this.addToRemovalList(cardId, removeFrontendCard);
    } else {
      removeFrontendCard(cardId);
    }
  }

  confirmDeleteFromPathway() {
    this.props.dispatch(clearCurrentCard());
    let { cardId, cardType } = this.state.cardToRemove;
    this.setState({ openConfirm: !this.state.openConfirm });
    const removeFrontendCard = cardIdParam => {
      let smartBites = this.state.smartBites;
      smartBites = smartBites.filter(item => item.id !== cardIdParam);
      this.setState({ smartBites });
    };
    if (
      this.state.pathway.id &&
      this.state.pathway.packCards &&
      this.state.pathway.packCards.some(item => item.id == cardId)
    ) {
      this.addToDeleteList(cardId, removeFrontendCard);
    } else {
      removeFrontendCard(cardId);
    }
  }

  removeTag = tag => {
    let newTagArray = this.state.topics.filter(topic => topic !== tag);
    this.setState({ topics: newTagArray });
  };
  openBadgeOption = () => {
    if (!this.props.pathway || (this.props.pathway && !this.state.currentBadge.id)) {
      let openBadgeOptions = !this.state.openBadgeOptions;
      this.setState({ openBadgeOptions, badgeOn: openBadgeOptions });
      if (openBadgeOptions) {
        let currentBadge = this.state.tempBadge;
        this.setState({
          currentBadge,
          tempBadge: {},
          showBadgeWarning: currentBadge && currentBadge.text && currentBadge.text.length > 25
        });
      } else {
        const currentBadge = this.state.currentBadge;
        this.setState({
          tempBadge: currentBadge,
          currentBadge: {},
          showBadgeWarning: currentBadge && currentBadge.text && currentBadge.text.length > 25
        });
      }
    }
  };
  enableBadgeForQuizzes = () => {
    this.setState({ enableBadgeForQuizzes: !this.state.enableBadgeForQuizzes });
  };
  triggerChangeBadge = e => {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.badgeOn && !this.state.openBadgeOptions) {
      let openBadgeOptions = !this.state.openBadgeOptions;
      this.setState({ openBadgeOptions, showBadgeWarning: false });
    }
  };
  revertBadgeChanges = () => {
    let openBadgeOptions = !this.state.openBadgeOptions;
    let previousBadge = {
      text: this.state.previousBadge.text,
      id: this.state.previousBadge.id,
      imageUrl: this.state.previousBadge.imageUrl
    };
    this.setState({
      openBadgeOptions,
      currentBadge: previousBadge,
      badgeOn: true,
      tempBadge: previousBadge
    });
  };

  applyBadgeChanges = () => {
    let openBadgeOptions = !this.state.openBadgeOptions;
    this.setState({ openBadgeOptions, tempBadge: this.state.currentBadge });
  };

  addBadgeHandler = event => {
    let currentBadge = this.state.currentBadge;
    currentBadge.text = event.target.value.trim();
    this.setState({ currentBadge, showBadgeWarning: currentBadge.text.length > 25 });
  };
  selectIconBadge = (e, badge) => {
    e && e.preventDefault();
    let currentBadge = this.state.currentBadge;
    currentBadge.imageUrl = badge.imageUrl;
    currentBadge.id = badge.id;
    this.setState({ currentBadge });
  };

  lockCard = (card, index) => {
    let smartBites = this.state.smartBites;
    let cardId = smartBites[index].id;
    let pathway = this.state.pathway;
    let cardIdsToLock = this.state.cardIdsToLock;
    let packCard = pathway && pathway.id && pathway.packCards.find(item => item.id === cardId);
    smartBites[index].isLocked = !smartBites[index].isLocked;
    if (pathway && pathway.id && packCard) {
      if (smartBites[index].isLocked === packCard.locked) {
        delete cardIdsToLock[cardId];
      } else {
        cardIdsToLock[cardId] = smartBites[index].isLocked;
      }
    } else {
      if (smartBites[index].isLocked) {
        cardIdsToLock[cardId] = true;
      } else {
        delete cardIdsToLock[cardId];
      }
    }
    this.setState({ smartBites, cardIdsToLock });
  };

  changeBia(event, value) {
    this.setState({
      selectedBia: value
    });
  }

  changeCompleteMethod(event, value) {
    this.setState({
      completeMethod: value
    });
  }

  showPathwayBadges = showCheckbox => {
    return (
      <div className="input-block">
        <div className="row">
          <div className="small-12 medium-4 large-3 align-label-top">
            <div className="pathway-label pathway-badge-label">{tr('Badge')}</div>
          </div>
          <div className="small-12 medium-8 large-9">
            <div style={this.styles.badgeBlock}>
              {showCheckbox && (
                <Checkbox
                  labelStyle={this.styles.labelCheckbox}
                  onCheck={this.openBadgeOption}
                  checked={this.state.badgeOn}
                  className="checkbox"
                  aria-label={tr('Badge')}
                  style={this.styles.checkbox}
                />
              )}
              <div style={this.styles.labelBlock}>
                <div onClick={this.openBadgeOption} style={this.styles.labelCheckbox}>
                  {showCheckbox
                    ? tr('Upon completing this pathway, assignee will get a badge.')
                    : ''}
                  <a
                    href="#"
                    aria-label={tr('Edit')}
                    style={this.styles.labelEditBlock}
                    onClick={this.triggerChangeBadge}
                  >
                    <EditIcon
                      color={this.state.badgeOn ? '#6f708b' : '#d0d0d9'}
                      style={this.styles.labelIcon}
                    />
                    <span
                      style={{
                        ...this.styles.labelEdit,
                        ...{ color: this.state.badgeOn ? '#6f708b' : '#d0d0d9' }
                      }}
                    >
                      {' '}
                      {tr('Edit')}
                    </span>
                  </a>
                </div>
              </div>
            </div>
            {this.state.openBadgeOptions && (
              <div style={{ display: 'flex', marginLeft: '7%' }}>
                {showCheckbox && (
                  <Checkbox
                    labelStyle={this.styles.labelCheckbox}
                    onCheck={this.enableBadgeForQuizzes}
                    checked={this.state.enableBadgeForQuizzes}
                    className="checkbox"
                    style={this.styles.checkbox}
                  />
                )}
                <div style={{ display: 'flex', flexWrap: 'wrap', lineHeight: '24px' }}>
                  <div style={this.styles.labelCheckbox}>
                    {showCheckbox
                      ? tr('Unlock the Badge after the Quiz is answered correctly')
                      : ''}
                  </div>
                </div>
              </div>
            )}
            {this.state.openBadgeOptions ? (
              <div>
                <div style={this.styles.badgesIconBlock}>
                  {!!this.state.badges &&
                    this.state.badges.map(badge => {
                      let selected =
                        (badge.id && badge.id === this.state.currentBadge.id) ||
                        (badge.imageUrl && badge.imageUrl === this.state.currentBadge.imageUrl)
                          ? this.styles.selectedIcon
                          : '';
                      return (
                        <div>
                          <a href="#" onClick={e => this.selectIconBadge(e, badge)}>
                            <img
                              className="card-view-icon"
                              style={{ ...this.styles.badgeIcon, ...selected }}
                              src={badge.imageUrl}
                            />
                          </a>
                        </div>
                      );
                    })}
                </div>
                <div style={this.styles.badgeText}>
                  <TextFieldCustom
                    hintText={tr('Enter a Badge name')}
                    aria-label={tr('Enter a Badge name')}
                    value={this.state.currentBadge.text}
                    inputChangeHandler={this.addBadgeHandler}
                    fullWidth={true}
                    disabled={this.state.disableForm}
                  />
                  {this.state.showBadgeWarning && (
                    <div className="text-center error-text">
                      {' '}
                      {tr('Cannot enter more than 25 characters')}
                    </div>
                  )}
                </div>
                <div className="action-buttons">
                  <FlatButton
                    label={tr('Cancel')}
                    className="close"
                    style={this.styles.cancel}
                    labelStyle={this.styles.actionBtnLabel}
                    onTouchTap={this.revertBadgeChanges}
                  />

                  <FlatButton
                    label={tr('Done')}
                    className="preview"
                    disabled={
                      !this.state.currentBadge.text ||
                      !this.state.currentBadge.imageUrl ||
                      this.state.showBadgeWarning
                    }
                    onTouchTap={this.applyBadgeChanges}
                    labelStyle={this.styles.actionBtnLabel}
                    style={
                      !this.state.currentBadge.text ||
                      !this.state.currentBadge.id ||
                      this.state.showBadgeWarning
                        ? this.styles.disabled
                        : this.styles.create
                    }
                  />
                </div>
              </div>
            ) : (
              !!this.state.currentBadge.imageUrl &&
              this.state.badgeOn && (
                <div style={this.styles.currentBadge}>
                  <div>
                    <img
                      className="card-view-icon"
                      style={this.styles.badgeIcon}
                      src={this.state.currentBadge.imageUrl}
                    />
                  </div>
                  <div
                    style={
                      this.state.currentBadge.text.length > 6
                        ? this.styles.badgeLabel
                        : this.styles.badgeLabelCenter
                    }
                  >
                    {this.state.currentBadge.text}
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </div>
    );
  };

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        showAdvancedSettings: !prevState.showAdvancedSettings
      };
    });
  };

  addToLeap = (correctId, wrongId, cardId, leapId) => {
    let leapObj = {
      correctId,
      wrongId,
      cardId,
      leapId
    };
    let arrToLeap = this.state.arrToLeap;
    let leapIndex = _.findIndex(arrToLeap, item => item.cardId === cardId);
    if (leapIndex > -1) {
      arrToLeap[leapIndex] = leapObj;
    } else {
      arrToLeap.push(leapObj);
    }
    this.setState({ arrToLeap });
  };

  handleAddition(tag, showMessage = true) {
    let selectedChannels = this.state.selectedChannels.slice();
    switch (tag.label) {
      case 'member':
        _.remove(this.state.userSuggestions, existingMember => {
          return existingMember.id + '' === tag.id + '';
        });
        break;
      case 'channel':
        _.remove(this.state.channelsSuggestions, existingChannel => {
          return existingChannel.id + '' === tag.id + '';
        });

        selectedChannels.push(tag.id);
        this.setState({ selectedChannels });
        break;
      case 'group':
        _.remove(this.state.groupsSuggestions, existingGroup => {
          return existingGroup.id + '' === tag.id + '';
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    let existingTags = this.state.tags;
    existingTags.push(tag);

    this.setState({ tags: existingTags }, () => {
      if (tag.label === 'channel' && showMessage) {
        this.setPrivateChannelMessage(tag);
      }
    });
  }

  setPrivateChannelMessage = tagAdded => {
    let channels = this.state.tags.filter(e => e.label === 'channel');
    if (channels.length === 0) {
      this.setState({
        showPrivateChannelMsg: false,
        privateChannelMsg: ''
      });
    } else {
      let privateChannels = channels.filter(e => e.isPrivate);
      let publicChannels = channels.filter(e => !e.isPrivate);
      let privateContent = this.state.isPrivateContent;
      if (!privateContent && privateChannels.length && publicChannels.length) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Card will remain public until explicitly marked as private.'
        });
      } else if (!privateContent && privateChannels.length) {
        let setStateObject = {};
        if (!this.state.userActionedPrivateContent) {
          setStateObject['isPrivateContent'] = true;
        }
        setStateObject['showPrivateChannelMsg'] = true;
        setStateObject['privateChannelMsg'] =
          'Card shared with private channel will need to be marked as private to control its visibility.';
        this.setState(setStateObject);
      } else if (
        privateContent &&
        privateChannels.length &&
        tagAdded &&
        !tagAdded.isPrivate &&
        publicChannels.length === 1
      ) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Visibility of card increases when shared with public channel'
        });
      } else {
        this.setState({
          showPrivateChannelMsg: false,
          privateChannelMsg: ''
        });
      }
    }
  };

  filterRestrictedGroupsOrUser = tags => {
    let restrictedToUsers = this.state.restrictedToUsers;
    let tempRestricted = this.state.restrictedToUsers;
    let finalUsersOrGroups = [];
    tags.map(tag => {
      switch (tag.label) {
        case 'channel':
          tempRestricted = restrictedToUsers.filter(restricted => {
            let restrictedChannelID = restricted.channelIds.filter(
              channelID => channelID === tag.id
            );
            restrictedChannelID = restrictedChannelID.length ? restrictedChannelID[0] : '';
            //if channel mentioned in share is present in the restrested-to user then it should be included in this array
            return restrictedChannelID === tag.id;
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'group':
          tempRestricted = restrictedToUsers.filter(restricted => {
            if (restricted.label === 'member') {
              let restrictedGroupID = restricted.teamIds.filter(teamID => teamID === tag.id);
              restrictedGroupID = restrictedGroupID.length ? restrictedGroupID[0] : '';
              //if group mentioned in share is present in the restrested-to user then it should be included in this array
              return restrictedGroupID === tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'member':
          tempRestricted = restrictedToUsers.filter(restricted => {
            //if member mentioned in share is present in the restrested-to user then it should be included in this array
            if (restrictedToUsers.label === 'member') {
              return restricted.id !== tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];

          break;
        default:
          break;
      }
    });

    finalUsersOrGroups = _.uniq(finalUsersOrGroups);
    this.setState({ restrictedToUsers: finalUsersOrGroups });
  };

  handleDelete(tagInfo) {
    let tag = typeof tagInfo === 'number' ? this.state.tags[tagInfo] : tagInfo;

    if (typeof tagInfo === 'undefined') {
      return;
    }
    switch (tag.label) {
      case 'member':
        _.concat(this.state.userSuggestions, existingMember => {
          return existingMember.id == tag.id;
        });
        break;
      case 'channel':
        _.concat(this.state.channelsSuggestions, existingChannel => {
          return existingChannel.id == tag.id;
        });
        break;
      case 'group':
        _.concat(this.state.groupsSuggestions, existingGroup => {
          return existingGroup.id == tag.id;
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    if (tag.label === 'channel') {
      let selectedChannels = _.uniq(this.state.selectedChannels);
      selectedChannels.splice(selectedChannels.indexOf(tag.id), 1);
      this.setState({ selectedChannels });
    }
    let tags = this.state.tags;

    let index = _.findIndex(tags, existingTag => {
      return existingTag.id === tag.id && existingTag.label === tag.label;
    });
    let suggestionState = this.state.channelsSuggestions;
    suggestionState.push(tag);
    tags.splice(index, 1);
    this.setState({ tags }, () => {
      if (tag.label === 'channel') {
        this.setPrivateChannelMessage();
      }
    });
  }

  handleDeleteTags(tag) {
    let topicTags = this.state.topicTags;

    let index = _.findIndex(topicTags, existingTag => {
      return existingTag.name === tag.name;
    });

    topicTags.splice(index, 1);
    this.setState({ topicTags });
  }

  selectedRestrictedToTags(tag) {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={this.handleUserDelete.bind(this, tag.tag)}
      >
        {tag && tag.tag && tag.tag.label === 'group' && (
          <GroupActiveIcon viewBox="0 0 45 30" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'channel' && (
          <TeamActiveIcon viewBox="0 2 50 28" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'member' && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  }

  handleInputChange(sourceField, query) {
    let payload = {
      limit: 15,
      offset: 0,
      q: query
    };

    let mainTags = [];

    if (sourceField === 'Restrict') {
      mainTags = this.state.restrictedToUsers;
    } else if (sourceField === 'Share') {
      mainTags = this.state.tags;
    }

    getItems(payload)
      .then(members => {
        _.map(members.items, member => {
          let suggestion = {
            name: member.name,
            id: parseInt(member.id, 10),
            label: 'member'
          };

          let index = _.findIndex(mainTags, existingTag => {
            return (
              parseInt(suggestion.id, 10) == parseInt(existingTag.id, 10) &&
              existingTag.label == 'member'
            );
          });
          if (index < 0) {
            let suggestionState = this.state.userSuggestions;
            if (sourceField === 'Restrict') {
              suggestionState = this.state.usersForRestrict;
              suggestionState.push(suggestion);
              this.setState({ usersForRestrict: suggestionState });
            } else if (sourceField === 'Share') {
              suggestionState.push(suggestion);
              this.setState({ userSuggestions: suggestionState });
            }
          }
        });
      })
      .catch(err => {
        console.error(`Error in PathwayEditor.handleInputChange.getItems.func : ${err}`);
      });
  }

  cleanLevel = () => {
    this.setState({ selectedBia: '' });
  };

  disablePublish() {
    return !!(
      this.state.disableSubmit ||
      this.state.clicked ||
      (this.state.openBadgeOptions &&
        (!this.state.currentBadge.text ||
          !this.state.currentBadge.id ||
          this.state.showBadgeWarning))
    );
  }

  getRestrictedErrorMsg = () => {
    let msg = '';

    if (!this.state.isPrivateContent) {
      msg = 'You must mark the card as private to be able to add restrictions';
    }

    return msg;
  };

  checkToShare = (e, item) => {
    let tags = this.state.tags;
    let index = _.findIndex(tags, tag => tag.label === item.label && +tag.id === +item.id);
    if (index > -1) {
      tags.splice(index, 1);
    } else {
      tags.push(item);
    }
    this.setState({ tags });
  };

  handleLanguageChange = data => {
    this.setState({ language: data });
  };

  render() {
    let showCheckbox = !this.props.pathway || (this.props.pathway && !this.state.currentBadge.id);
    let isCurator =
      this.props.currentUser.roles && !!~this.props.currentUser.roles.indexOf('curator');
    let isAbleUpload = Permissions['enabled'] !== undefined && Permissions.has('UPLOAD');
    let isAbleUploadScorm =
      Permissions['enabled'] !== undefined && Permissions.has('UPLOAD_SCORM_CONTENT'); //EP-15959: removed autoMarkAsCompleteScorm
    let isAbleToUploadTextCard =
      Permissions['enabled'] !== undefined && Permissions.has('CREATE_TEXT_CARD');
    let tagSuggestions = _.uniqWith(
      [
        ...this.state.groupsSuggestions,
        ...this.state.channelsSuggestions,
        ...this.state.userSuggestions
      ],
      _.isEqual
    );
    let restrictedToUsersTags = _.uniqWith(
      [...this.state.groupsForRestrict, ...this.state.usersForRestrict],
      _.isEqual
    );
    let restrictAble = this.state.isPrivateContent;
    let restrictToError = this.getRestrictedErrorMsg();
    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    return (
      <div
        className="pathway-creation"
        style={this.state.disableForm ? this.styles.disableForm : {}}
      >
        <div className="my-modal-header">
          <span className="header-title">{tr(this.state.titleLabel + ' Pathway')}</span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeBtn}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
            <TextField name="pathwayeditor" autoFocus={true} className="hiddenTextField" />
          </div>
        </div>
        <div className="pathway-content">
          <div className="common-block">
            <div className="row">
              <div className="small-12 medium-4">
                {!this.state.showMainImage && (
                  <a
                    href="#"
                    aria-label="upload image"
                    className="pathway-banner"
                    style={this.styles.pathwayBanner}
                    onClick={e => this.pathwayBannerFileStackHandler(e, false)}
                  >
                    <div className="text-center">
                      <ImagePhotoCamera style={this.styles.mainImage} />
                      <div className="empty-image">{tr('Upload Image')}</div>
                    </div>
                  </a>
                )}
                {this.state.showMainImage && (
                  <a
                    href="#"
                    onClick={e => this.pathwayBannerFileStackHandler(e, false)}
                    className="pathway-banner preview-upload"
                  >
                    <div
                      className="card-img-container"
                      onMouseEnter={() => {
                        this.setState({ mainImage: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ mainImage: false });
                      }}
                    >
                      <div
                        className="card-img button-icon"
                        style={{
                          ...this.styles.pathwayImage,
                          ...{
                            backgroundImage: `url(\'${getResizedUrl(
                              this.state.securedUrl || this.state.fileStack[0].url,
                              'height:183'
                            )}\')`
                          }
                        }}
                      >
                        {this.hoverBlock(this.fileStackHandler.bind(this, false))}
                      </div>
                    </div>
                  </a>
                )}
              </div>
              <div className="small-12 medium-8">
                <div className="input-block">
                  <div className="row">
                    {!useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">
                          {tr('Pathway Title')}
                          <span>*</span>
                        </div>
                      </div>
                    )}

                    <div className="small-12 medium-8 large-9">
                      {useFormLabels && (
                        <label required htmlFor={tr('Pathway Title')}>
                          {tr('Pathway Title')}
                        </label>
                      )}
                      <TextFieldCustom
                        hintText={useFormLabels ? '' : 'Start typing a title for your pathway'}
                        value={this.state.mainPathway.title}
                        inputChangeHandler={this.pathwayMainChange.bind(this, 'title')}
                        errorText={this.state.titleErrorText}
                        rowsMax={4}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                    </div>
                  </div>
                </div>
                <div className="input-block">
                  <div className="row">
                    {!useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">{tr('Description')}</div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      <TextFieldCustom
                        hintText={`Add title/description ${!useFormLabels && '(Optional)'}`}
                        value={this.state.pathway.message}
                        inputChangeHandler={this.pathwayMainChange.bind(this, 'message')}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                    </div>
                  </div>
                </div>
                {(!this.state.shareOnSmartCardCreation || !this.state.isAbleToMarkPrivate) &&
                  this.state.activeType !== 'smart' &&
                  this.state.activeType !== 'bookmark' && (
                    <div className="input-block">
                      <div className="row">
                        {!useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label-input-level">
                            <div className="pathway-label">{tr('Post to')}</div>
                          </div>
                        )}

                        <div className="small-12 medium-8 large-9">
                          <SelectField
                            hintText="Channel"
                            aria-label="select Channel"
                            cardType="smartbite"
                            onChange={this.getNewChannels}
                            fullWidth={false}
                            currentChannels={this.state.selectedChannels}
                            nonCuratedChannelIds={this.state.nonCuratedChannelIds}
                            cardChannel={(this.props.pathway && this.state.pathway.channels) || []}
                          />
                        </div>
                        <div style={this.styles.privateChannelStyle}>
                          {this.state.showPostToChannelMsg && (
                            <div style={{ margin: '10px 0 0 155px' }}>
                              <ActionInfo style={this.styles.privateChannelIconStyle} />
                              <p style={{ verticalAlign: 'middle', margin: '0 0 0 15px' }}>
                                {tr('Posting to private channel will not make card private.')}
                              </p>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}

                {this.showBIA && (
                  <div className="input-block">
                    <div className="row">
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">{tr('Level')}</div>
                      </div>
                      <div className="small-12 medium-8 large-9">
                        <div className="bia-block">
                          <RadioButtonGroup
                            name="bia-radio"
                            style={this.styles.flexBlock}
                            valueSelected={this.state.selectedBia}
                            onChange={this.changeBia.bind(this)}
                          >
                            <RadioButton
                              value="beginner"
                              label={tr('Beginner')}
                              aria-label={tr('Beginner')}
                              className="bia-radio"
                              style={this.styles.radioBtn}
                              iconStyle={
                                this.state.selectedBia !== 'beginner'
                                  ? this.styles.iconRadio
                                  : this.styles.mr5
                              }
                            />
                            <RadioButton
                              value="intermediate"
                              label={tr('Intermediate')}
                              aria-label={tr('Intermediate')}
                              className="bia-radio"
                              style={this.styles.radioBtn}
                              iconStyle={
                                this.state.selectedBia !== 'intermediate'
                                  ? this.styles.iconRadio
                                  : this.styles.mr5
                              }
                            />
                            <RadioButton
                              value="advanced"
                              label={tr('Advanced')}
                              aria-label={tr('Advanced')}
                              className="bia-radio"
                              style={this.styles.radioBtn}
                              iconStyle={
                                this.state.selectedBia !== 'advanced'
                                  ? this.styles.iconRadio
                                  : this.styles.mr5
                              }
                            />
                          </RadioButtonGroup>
                          {this.state.selectedBia && (
                            <button className="my-button" onClick={this.cleanLevel}>
                              {tr('Clear level')}
                            </button>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                <div className="input-block">
                  <div className="row">
                    {!useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label-top">
                        <div className="pathway-label pathway-label__tag">{tr('Tag')}</div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      {!!this.state.topics.length && (
                        <div className="text-block chip-tag-pathway">
                          <div style={this.styles.chipsWrapper}>
                            {this.state.topics.map((name, idx) => {
                              return (
                                <div className="chip-tag-pathway-item" key={name + idx}>
                                  <Chip
                                    style={this.styles.chip}
                                    className="channel-item-insight"
                                    backgroundColor={colors.white}
                                    labelColor={colors.primary}
                                    labelStyle={this.styles.labelChip}
                                  >
                                    {' '}
                                    {name}
                                    <CloseIcon
                                      onClick={this.removeTag.bind(this, name)}
                                      style={this.styles.chipClose}
                                      aria-label="cancel"
                                    />
                                  </Chip>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      )}
                      <TextFieldCustom
                        hintText={tr('Enter desired tag')}
                        clearDown={!this.state.maxTagErrorText.length}
                        onKeyDown={!this.state.maxTagErrorText.length && this.addTagHandler}
                        inputChangeHandler={this.changeTag}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                      {!this.state.maxTagErrorText.length && (
                        <div>
                          <p className="tag-info">{tr('Press Return (Enter) to add the tag.')}</p>
                        </div>
                      )}
                      <div className="error-text"> {tr(this.state.maxTagErrorText)}</div>
                    </div>
                  </div>
                </div>

                {this.state.shareOnSmartCardCreation &&
                  this.state.isAbleToMarkPrivate &&
                  this.state.activeType !== 'smart' &&
                  this.state.activeType !== 'bookmark' && (
                    <div className="input-block">
                      <div className="row" style={{ marginBottom: '10px' }}>
                        {!useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label-input-level">
                            <div className="pathway-label">{tr('Mark as Private')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <Checkbox
                            style={this.styles.privateContent}
                            checked={this.state.isPrivateContent}
                            onCheck={this.handlePrivateContent.bind(this)}
                            aria-label={tr('Mark as Private')}
                            label={useFormLabels && tr('Mark as Private')}
                          />
                        </div>
                      </div>
                      <div className="row">
                        {!useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label-top">
                            <div className="pathway-label">{tr('Share')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <CardSharingSection
                            tags={this.state.tags}
                            tagSuggestions={tagSuggestions}
                            handleAddition={this.handleAddition.bind(this)}
                            handleDelete={this.handleDelete.bind(this)}
                            handleInputChange={this.handleInputChange.bind(this, 'Share')}
                            pathwayPart={false}
                            styles={this.styles}
                            showPrivateChannelMsg={this.state.showPrivateChannelMsg}
                            privateChannelMsg={this.state.privateChannelMsg}
                            groupsSuggestions={this.state.groupsSuggestions}
                            channelsSuggestions={this.state.channelsSuggestions}
                            checkToShare={this.checkToShare}
                          />
                        </div>
                      </div>
                    </div>
                  )}

                {typeof this.props.team.OrgConfig.badgings === 'undefined'
                  ? this.enableBadge &&
                    !!this.state.badges.length &&
                    this.showPathwayBadges(showCheckbox)
                  : this.props.team.OrgConfig.badgings['web/badgings'] &&
                    this.props.team.OrgConfig.badgings['web/badgings'].pathwayBadgings &&
                    this.enableBadge &&
                    !!this.state.badges.length &&
                    this.showPathwayBadges(showCheckbox)}

                {this.isCompleteField &&
                  this.completeMethodConf &&
                  this.completeMethodConf === 'creatorChoose' && (
                    <div className="input-block">
                      <div className="row">
                        <div className="small-12 medium-4 large-3 align-label">
                          <div className="pathway-label">{tr('Mark as complete')}</div>
                        </div>
                        <div className="small-12 medium-8 large-9">
                          <div className="complete-method-block">
                            <RadioButtonGroup
                              name="completeMethod"
                              style={this.styles.flexBlock}
                              valueSelected={this.state.completeMethod}
                              onChange={this.changeCompleteMethod.bind(this)}
                            >
                              <RadioButton
                                value="manually"
                                label={tr('Manually')}
                                className="complete-radio"
                                style={this.styles.radioBtn}
                                iconStyle={
                                  this.state.completeMethod !== 'manually'
                                    ? this.styles.iconRadio
                                    : this.styles.mr5
                                }
                                aria-label={tr('Manually')}
                              />
                              <RadioButton
                                value="automatically"
                                label={tr('Automatically')}
                                className="complete-radio"
                                style={this.styles.radioBtn}
                                iconStyle={
                                  this.state.completeMethod !== 'automatically'
                                    ? this.styles.iconRadio
                                    : this.styles.mr5
                                }
                                aria-label={tr('automatically')}
                              />
                            </RadioButtonGroup>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}

                <div>
                  <div className="input-block">
                    <div className="row align-right">
                      <div className="small-12 medium-8 large-9">
                        <a
                          href="#"
                          className="advanced-toggle"
                          onClick={this.toggleAdvancedSettings}
                        >
                          {tr('Advanced Settings')}
                        </a>
                      </div>
                    </div>
                  </div>
                  {this.state.showAdvancedSettings && (
                    <div>
                      {this.restrictToFlag &&
                        this.state.shareOnSmartCardCreation &&
                        this.state.isAbleToMarkPrivate && (
                          <div className="input-block">
                            <div className="row">
                              {!useFormLabels && (
                                <div className="small-12 medium-4 large-3">
                                  <div className="pathway-label">{tr('Restricted to')}</div>
                                </div>
                              )}

                              {!restrictAble && (
                                <div className="small-12 medium-8 large-8">
                                  <TextFieldCustom
                                    disabled={true}
                                    hintText={`${tr('Restrict content with @user, @group')}`}
                                    errorText={restrictToError}
                                    errorStyle={this.styles.restrictToErrorStyle}
                                    fullWidth={true}
                                  />
                                </div>
                              )}
                              {restrictAble && (
                                <div className="small-12 medium-8 large-8">
                                  {useFormLabels && (
                                    <label htmlFor={tr('Restrict content with @user, @group')}>
                                      {tr('Restrict content with @user, @group')}
                                    </label>
                                  )}
                                  <ReactTags
                                    tags={this.state.restrictedToUsers}
                                    suggestions={restrictedToUsersTags}
                                    handleAddition={this.handleUserAddition.bind(this)}
                                    handleDelete={this.handleUserDelete.bind(this)}
                                    placeholder={
                                      useFormLabels
                                        ? ''
                                        : !!this.state.restrictedToUsers.length > 0
                                        ? ''
                                        : tr('Restrict content with @user, @group')
                                    }
                                    tagComponent={this.selectedRestrictedToTags.bind(this)}
                                    handleInputChange={this.handleInputChange.bind(
                                      this,
                                      'Restrict'
                                    )}
                                    autofocus={false}
                                  />
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      {this.state.multilingualContent && (
                        <div className="input-block">
                          <MultilingualContent
                            labelContainerClasses="small-12 medium-4 large-3 align-label"
                            labelClasses="pathway-label"
                            inputContainerClasses={!useFormLabels && 'small-12 medium-6 large-6'}
                            handleLanguageChange={this.handleLanguageChange}
                            language={this.state.language}
                          />
                        </div>
                      )}
                      {isCurator && (
                        <div className="input-block">
                          <div className="row">
                            <div className="small-12 medium-4 large-3 align-label">
                              <div className="pathway-label">{tr('Provider')}</div>
                            </div>
                            <div className="small-12 medium-6 large-6">
                              <TextFieldCustom
                                hintText={tr('Enter Provider Name')}
                                value={this.state.advSettings.provider}
                                inputChangeHandler={this.advSettingsChange.bind(this, 'provider')}
                                fullWidth={true}
                                disabled={this.state.disableForm}
                              />
                            </div>
                            <div className="small-12 medium-2 large-3">
                              <FlatButton
                                label={tr('Image')}
                                className="preview"
                                onTouchTap={this.fileStackHandler.bind(this, true)}
                                labelStyle={this.styles.actionBtnLabel}
                                style={this.styles.providerBtnImage}
                              />
                            </div>
                          </div>
                          {this.state.fileStackProvider && (
                            <div className="row align-right">
                              <div className="small-12 medium-8 large-9">
                                <div style={this.styles.providerImageContainer}>
                                  <div className="close close-button provider-image-btn">
                                    <IconButton
                                      style={this.styles.deleteProviderImageBtn}
                                      iconStyle={this.styles.deleteProviderImageBtnIcon}
                                      onTouchTap={this.clearFileStackProviderInfo}
                                      aria-label="clear"
                                    >
                                      <CloseIcon color="white" viewBox="0 0 24 24" />
                                    </IconButton>
                                  </div>
                                  <img
                                    className="card-view-icon"
                                    style={this.styles.providerIcon}
                                    src={
                                      this.state.securedProviderUrl || this.state.fileStackProvider
                                    }
                                    alt=""
                                  />
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                      <div className="input-block">
                        <div className="row">
                          {!useFormLabels && (
                            <div className="small-12 medium-4 large-3 align-label">
                              <div className="pathway-label">{tr('Duration')}</div>
                            </div>
                          )}
                          <div className="small-12 medium-6 large-6">
                            <TextFieldCustom
                              hintText={
                                useFormLabels
                                  ? `Duration ${tr('(MONTH : DAYS : HRS : MINS)')}`
                                  : this.state.durationString
                                  ? this.state.durationString
                                  : tr('Enter time (MONTH : DAYS : HRS : MINS)')
                              }
                              value={this.state.durationString}
                              inputChangeHandler={this.advSettingsChange.bind(this, 'duration')}
                              pipe={'mm dd HH MM'}
                              fullWidth={true}
                              mask={[/\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                              keepCharPositions={true}
                              disabled={this.state.disableForm}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>

                {this.state.showVersions && this.state.previousVersions.length > 0 && (
                  <div className="input-block advanced-settings">
                    <div className="row align-right">
                      <div className="small-12 medium-8 large-9">
                        <a
                          href="#"
                          className="advanced-toggle"
                          onClick={this.toggleVersionSettings}
                        >
                          {tr('Previous Versions')}
                        </a>
                      </div>
                    </div>
                    {this.state.showPreviousversionsToggle && (
                      <SmartCardVersions versions={this.state.previousVersions} />
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
          <hr className="custom-hr" />
          <div className="tile-block">
            <IconButton
              aria-label="tile view"
              className="editor-image-btn-icon"
              onTouchTap={this.changeViewMode.bind(this, 'Tile')}
              iconStyle={this.styles.icons}
              style={{
                ...this.styles.tileView,
                ...{ opacity: this.state.viewRegime === 'Tile' ? 1 : 0.5 }
              }}
            >
              <span tabIndex={-1} className="hideOutline">
                <TileIcon style={this.styles.icons} color={viewColor} viewBox="0 0 18 18" />
              </span>
            </IconButton>
            <IconButton
              aria-label="list view"
              className="editor-image-btn-icon"
              onTouchTap={this.changeViewMode.bind(this, 'List')}
              iconStyle={this.styles.icons}
              style={{
                ...this.styles.tileView,
                ...{ opacity: this.state.viewRegime === 'List' ? 1 : 0.5 }
              }}
            >
              <span tabIndex={-1} className="hideOutline">
                <ListIcon style={this.styles.icons} color={viewColor} viewBox="0 0 18 18" />
              </span>
            </IconButton>
            <span className="tile-text">{tr(this.state.viewRegime + ' View')}</span>
          </div>

          <div className="smartbites-block">
            <div
              className={
                this.state.viewRegime === 'Tile'
                  ? this.state.isCardV3
                    ? 'tile-view'
                    : 'custom-card-container tile-view'
                  : 'custom-card-container'
              }
            >
              <div className="three-card-column" id="listWithHandle">
                {this.state.smartBites.map((card, index) => {
                  let cardType = getCardType(card) || '';
                  let pollPermission =
                    (cardType === 'POLL' || cardType === 'QUIZ') && Permissions.has('MANAGE_CARD');
                  let textCardPermission = Permissions.has('CREATE_TEXT_CARD');
                  let hideEdit = '';
                  if (
                    (!!card.attemptCount && card.attemptCount > 0) ||
                    (pollPermission && card.hasAttempted)
                  ) {
                    hideEdit = "Can't edit completed poll/quiz";
                  } else if (cardType === 'TEXT' && !textCardPermission) {
                    hideEdit = "You don't have permission to edit this card";
                  }
                  if (this.state.viewRegime === 'Tile') {
                    return (
                      <Card
                        card={card}
                        showControls={true}
                        key={index}
                        pathwayEditor={true}
                        index={index + 1}
                        dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                        startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                        author={card.author}
                        user={this.state.currentUser}
                        removeCardFromPathway={this.removeCardFromPathway.bind(
                          this,
                          card.id,
                          card.cardType,
                          card.hidden
                        )}
                        moreCards={true}
                        lockCard={this.lockCard.bind(this, card, index)}
                        openCardEdit={this.openCardEdit}
                        cardsList={this.state.smartBites}
                        pathway={this.state.pathway}
                        addToLeap={this.addToLeap}
                        arrToLeap={this.state.arrToLeap}
                        newSkillLevel={card.newSkillLevel}
                        isPrivate={card.isPrivate}
                        hideEdit={hideEdit}
                      />
                    );
                  } else {
                    return (
                      <CardListView
                        card={card}
                        key={index}
                        dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                        startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                        index={index + 1}
                        author={card.author}
                        user={this.state.currentUser}
                        removeCardFromPathway={this.removeCardFromPathway.bind(
                          this,
                          card.id,
                          card.cardType,
                          card.hidden
                        )}
                        lockCard={this.lockCard.bind(this, card, index)}
                        pathwayEditor={true}
                        openCardEdit={this.openCardEdit}
                        isPrivate={card.isPrivate}
                        hideEdit={hideEdit}
                      />
                    );
                  }
                })}
                <div
                  className={`${
                    this.state.viewRegime === 'Tile'
                      ? `${
                          this.state.isCardV3
                            ? 'card-v3 card-v3__tile-view'
                            : 'card-v2 small-12 column more-cards'
                        }`
                      : ''
                  } ${this.isNewTileCard ? 'card-v2__wide' : 'card-v2__simple-size'}`}
                >
                  <a
                    href="#"
                    className={
                      this.state.viewRegime === 'Tile'
                        ? `${this.state.isCardV3 ? 'empty-smartbite-v3' : ''} empty-smartbite`
                        : 'empty-smartbite-list'
                    }
                    key={'empty'}
                    ref="target"
                    onClick={this.handleTouchTap}
                  >
                    <div className={this.state.viewRegime === 'Tile' ? 'text-center' : 'text-left'}>
                      <AddIcon style={{ verticalAlign: 'top' }} color={viewColor} />
                      <span className="empty-title">{tr('Add SmartCard')}</span>
                    </div>
                  </a>
                </div>
                <Popover
                  placement="top"
                  container={this}
                  style={this.styles.popover}
                  containerStyle={this.styles.popoverContainer}
                  target={this.state.popoverElm}
                  show={this.state.openEmpty}
                  onHide={this.handleRequestClose}
                >
                  <div>
                    <div className="row cardTypePopupIsActive">
                      {this.state.popoverAutofocus && (
                        <TextField
                          autoFocus={true}
                          name="PathwayEditor"
                          className="hiddenTextField"
                        />
                      )}
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'link' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'link')}
                        style={
                          this.state.activeType === 'link' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card link-card">
                          <LinkIcon fill="white" viewBox="-1 0 20 20" />
                        </div>
                        <div className="text-center popover-card-text">{tr('Link')}</div>
                      </a>
                      {isAbleUpload ? (
                        <a
                          href="#"
                          className={`${
                            this.state.activeType !== 'upload' && this.state.isEditCard
                              ? 'not-allowed icon-opacity'
                              : ''
                          } small-3 card-circle`}
                          onClick={e => this.changeActiveType(e, 'upload')}
                          style={
                            this.state.activeType === 'upload' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card upload-card">
                            <UploadIcon color={'white'} viewBox="0 0 24 24" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Upload')}</div>
                        </a>
                      ) : null}
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'poll' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'poll')}
                        style={
                          this.state.activeType === 'poll' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card poll-card">
                          <PollIcon fill="white" viewBox="-3 0 24 24" />
                        </div>
                        <div className="text-center popover-card-text">{tr('Poll')}</div>
                      </a>
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'quiz' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'quiz')}
                        style={
                          this.state.activeType === 'quiz' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card quiz-card">
                          <div className="quiz-card-icon">Q</div>
                        </div>
                        <div className="text-center popover-card-text">{tr('Quiz')}</div>
                      </a>
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'autopathway' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'autopathway')}
                        style={
                          this.state.activeType === 'autopathway' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card auto-generate-card">
                          <DynamicIcon viewBox="0 0 24 24" color={'white'} />
                        </div>
                        <div className="text-center popover-card-text">{tr('Dynamic Pathway')}</div>
                      </a>
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'bookmark' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'bookmark')}
                        style={
                          this.state.activeType === 'bookmark' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card bookmark-card">
                          <BookmarkIcon viewBox="-1 0 26 25" color={'white'} />
                        </div>
                        <div className="text-center popover-card-text">{tr('From Bookmark')}</div>
                      </a>
                      <a
                        href="#"
                        className={`${
                          this.state.activeType !== 'smart' && this.state.isEditCard
                            ? 'not-allowed icon-opacity'
                            : ''
                        } small-3 card-circle`}
                        onClick={e => this.changeActiveType(e, 'smart')}
                        style={
                          this.state.activeType === 'smart' || !this.state.activeType
                            ? this.styles.activeView
                            : this.styles.inactiveView
                        }
                      >
                        <div className="icon-card smart-card">
                          <SearchIcon color={'white'} />
                        </div>
                        <div className="text-center popover-card-text">
                          {tr('Search SmartCard')}
                        </div>
                      </a>
                      {isAbleToUploadTextCard && (
                        <a
                          href="#"
                          className={`${
                            this.state.activeType !== 'text' && this.state.isEditCard
                              ? 'not-allowed icon-opacity'
                              : ''
                          } small-3 card-circle`}
                          onClick={e => this.changeActiveType(e, 'text')}
                          style={
                            this.state.activeType === 'text' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card text-card">
                            <TextIcon fill="white" viewBox="0 0 27 15" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Text')}</div>
                        </a>
                      )}
                      {this.state.showProjectCard && (
                        <a
                          href="#"
                          className={`${
                            this.state.activeType !== 'project' && this.state.isEditCard
                              ? 'not-allowed icon-opacity'
                              : ''
                          } small-3 card-circle`}
                          onClick={e => this.changeActiveType(e, 'project')}
                          style={
                            this.state.activeType === 'project' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card project-card svg-align">
                            <Project fill="white" viewBox="12 -5 46 70" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Project')}</div>
                        </a>
                      )}
                      {isAbleUploadScorm && (
                        <a
                          href="#"
                          className={`${
                            this.state.activeType !== 'scorm' && this.state.isEditCard
                              ? 'not-allowed icon-opacity'
                              : ''
                          } small-3 card-circle`}
                          onClick={e => this.changeActiveType(e, 'scorm')}
                          style={
                            this.state.activeType === 'scorm' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card text-card">
                            <Scorm fill="white" viewBox="-4 0 27 17" />
                          </div>
                          <div className="text-center popover-card-text">{tr('SCORM')}</div>
                        </a>
                      )}
                    </div>

                    <div className="additional-block">{this.renderTypeUI()}</div>
                  </div>
                </Popover>
              </div>
            </div>
          </div>
          <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
          <div className="action-buttons">
            <FlatButton
              label={tr('Cancel')}
              className="close"
              disabled={this.state.clicked}
              style={this.styles.cancel}
              labelStyle={this.styles.actionBtnLabel}
              onTouchTap={this.closeModal}
            />

            {(!this.props.pathway ||
              (this.props.pathway && this.state.pathway.state !== 'published')) && (
              <FlatButton
                label={tr(this.state.createLabel)}
                className="saveLater"
                disabled={this.disablePublish()}
                onTouchTap={this.createClickHandler}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.cancel}
              />
            )}

            {this.enablePreview && (
              <FlatButton
                label={tr(this.state.previewLabel)}
                className="preview"
                disabled={this.disablePublish()}
                onTouchTap={this.previewClickHandler}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.create}
              />
            )}
            {!!this.state.smartBites.length && (
              <FlatButton
                label={tr(this.state.publishLabel)}
                className="publish"
                disabled={this.disablePublish()}
                onTouchTap={this.publishClickHandler.bind(
                  this,
                  this.props.card ? this.props.card.id : null
                )}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.create}
              />
            )}
          </div>
        </div>

        {this.state.externalCard
          ? this.state.openConfirm && (
              <Dialog
                open={this.state.openConfirm}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={'Are you sure you want to remove this card from the pathway?'}
                  cancelClick={this.cancelRemoveCardFromPathway}
                  callback={this.confirmRemoveCardFromPathway}
                  confirmBtnTitle="Yes"
                  cancelBtnTitle="No"
                />
              </Dialog>
            )
          : this.state.openConfirm && (
              <Dialog
                open={this.state.openConfirm}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={'What action would you like to take on this SmartCard?'}
                  cancelClick={this.confirmDeleteFromPathway}
                  callback={this.confirmRemoveCardFromPathway}
                  confirmBtnTitle="Remove from Pathway"
                  cancelBtnTitle="Permanently Delete"
                  closeModal={this.closeRemoveCardFromPathwayModal}
                />
              </Dialog>
            )}
      </div>
    );
  }
}

PathwayEditor.propTypes = {
  currentUser: PropTypes.object,
  pathname: PropTypes.string,
  team: PropTypes.object,
  reorderCardIds: PropTypes.array,
  isPreviewMode: PropTypes.bool,
  pathway: PropTypes.object,
  mainPathway: PropTypes.object,
  card: PropTypes.object,
  cardUpdated: PropTypes.func,
  channelsV2: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS(),
    reorderCardIds: state.pathways.toJS().reorderCardIds,
    isPreviewMode: state.pathways ? state.pathways.toJS().isPreviewMode : false,
    channelsV2: state.channelsV2.toJS()
  };
}
export default connect(mapStoreStateToProps)(PathwayEditor);
