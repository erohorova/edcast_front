import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import _ from 'lodash';
import moment from 'moment';

import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { PrimaryButton } from 'edc-web-sdk/components/index';
import colors from 'edc-web-sdk/components/colors/index';
import {
  searchForUsers,
  getUserCustomFields,
  getAllCustomFields
} from 'edc-web-sdk/requests/users.v2';
import { shareToGroup, addTeamMembers } from 'edc-web-sdk/requests/groups.v2';
import { assignTo } from 'edc-web-sdk/requests/assignments';
import { deleteUsersAssignment } from 'edc-web-sdk/requests/assignments.v2';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { Calendar } from 'react-date-range';

import IndividualsView from '../../components/multiactionContent/individualsView';
import GroupsView from '../../components/multiactionContent/groupsView';
import MultiActionDynamicSelections from '../../components/multiactionContent/DynamicSelection';
import CurrentState from '../../components/multiactionContent/CurrentState';
import MultiActionSearchBox from '../../components/multiactionContent/SearchBox';

import { open as openSnackBar } from '../../actions/snackBarActions';
import { updateCurrentCard } from '../../actions/cardsActions';
import * as multiActions from '../../actions/multiactionActions';
import * as assignmentsAction from '../../actions/assignmentsActions';
import {
  close,
  openStatusModal,
  updateDynamicSelectionFiltersV2
} from '../../actions/modalActions';
import { getGroupUsers, invite, deleteUserFromTeam } from '../../actions/groupsActionsV2';

import CreateGroupForMultiaction from '../../components/multiactionContent/CreateGroupForMultiaction';
import PreviewDSModal from './PreviewDSModal';
import { Permissions } from '../../utils/checkPermissions';

const emptyFilter = [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }];

class MultiactionsModal extends Component {
  constructor(props, context) {
    super(props, context);
    let tabs = ['Individuals'];
    props.currentAction !== 'invite' && tabs.push('Groups');
    let enableDynamicSelection =
      window.ldclient.variation('dynamic-selections', false) &&
      (Permissions['enabled'] !== undefined && Permissions.has('USE_DYNAMIC_SELECTION'));

    if (enableDynamicSelection) {
      tabs.push('Dynamic Selections');
    }

    if (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) {
      props.currentAction === 'share'
        ? tabs.push('Currently Shared')
        : props.currentAction === 'assign'
        ? tabs.push('Currently Assigned')
        : tabs.push('Currently Invited');
    }

    this.state = {
      tabs,
      activeTab: 'Individuals',
      dynamicSelections: window.ldclient.variation('dynamic-selections', false),
      previewData: [],
      groupModal: false,
      showCreateGroupModal: false,
      showPreview: false,
      previewFilter: '',
      startDate: '',
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      dueDate: '',
      checkedItemsCount: 0,
      toState: { dynamicFilters: emptyFilter, individualFilters: emptyFilter },
      isPendingPreview: false,
      totalNumber: 0,
      totalGroupNumber: 0
    };
    this.styles = {
      back_button: {
        width: '4.875rem',
        backgroundColor: '#acadc1',
        color: '#fff',
        borderRadius: '0.125rem',
        border: '0'
      },
      calendar: {
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: '1000',
        boxShadow: '0 0.125rem 0.25rem 0 rgba(0,0,0,.5)'
      },
      cancel_button: {
        color: '#acadc1',
        border: '1px solid #acadc1'
      },
      share_button: {
        width: '7rem',
        backgroundColor: '#6f708b',
        color: '#fff',
        border: '0'
      },
      preview_button: {
        backgroundColor: '#acadc1',
        color: '#fff',
        border: '0'
      },
      share_and_create: {
        backgroundColor: '#454560',
        color: '#fff',
        border: '0'
      },
      dateInput: {
        backgroundColor: '#f0f0f5',
        width: '4.9375rem',
        height: '1.875rem',
        borderTopRightRadius: '0.125rem',
        borderBottomRightRadius: '0.125rem',
        fontSize: '0.875rem',
        color: '#6f708b',
        cursor: 'pointer'
      },
      dateContainer: {
        display: 'inline-block',
        height: '1.875rem',
        color: '#6f708b',
        cursor: 'pointer'
      },
      assignCalenderReset: {
        borderColor: colors.followColor,
        marginRight: '5px'
      }
    };

    this.assignMessage = '';
    this.currentAction = props.currentAction;
    this.selfAssign = props.selfAssign || false;
    this.actionButtonName = `${this.currentAction
      .charAt(0)
      .toUpperCase()}${this.currentAction.slice(1)}`;
    this.usersLimit = 200;
    this.isAllPreviewLoaded = false;
  }

  componentDidMount() {
    let payload = { limit: 15, offset: 0, full_response: true };
    let usersPayload = { ...payload, ...{ sort: 'first_name', order: 'asc' } };
    if (this.props.currentAction !== 'invite') {
      this.props
        .dispatch(multiActions._fetchCard(this.props.card.id))
        .then(() => {
          // list of groups for sharing -> writable groups of a user
          // list of groups for assigning -> groups the user is a leader in
          let groupKey = this.props.currentAction + 'Group';
          if (
            !(
              this.props.contentMultiaction &&
              this.props.contentMultiaction[groupKey] &&
              this.props.contentMultiaction[groupKey].length > 0
            )
          ) {
            let payloadGroups = { limit: 15, offset: 0 };

            if (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) {
              this.props.dispatch(
                multiActions._fetchOrgGroups(payloadGroups, this.props.currentAction)
              );
            } else {
              if (this.props.currentAction === 'assign') {
                payloadGroups['role'] = 'admin';
                usersPayload['only_from_my_teams'] = true;
              } else {
                payloadGroups['writables'] = true;
              }
              this.props.dispatch(
                multiActions._fetchGroups(payloadGroups, this.props.currentAction)
              );
            }
          }

          if (
            !(
              this.props.contentMultiaction &&
              this.props.contentMultiaction[this.props.currentAction] &&
              this.props.contentMultiaction[this.props.currentAction].length > 0
            )
          ) {
            this.props.dispatch(multiActions._fetchUsers(usersPayload, this.props.currentAction));
          }
        })
        .catch(err => {
          console.error(
            `An error has occurred while processing multiActions._fetchCard in Multiaction modal: ${err}`
          );
        });
    } else {
      this.props.dispatch(multiActions._fetchUsers(usersPayload, 'invite'));
      this.props.dispatch(
        getGroupUsers(this.props.groupsV2.currentGroupID, { limit: 150, offset: 0 })
      );
      this.props.dispatch(
        getGroupUsers(this.props.groupsV2.currentGroupID, {
          limit: 150,
          offset: 0,
          user_type: 'pending'
        })
      );
    }
    if (this.currentAction === 'assign') {
      this.props.dispatch(
        assignmentsAction.getAssignmentsMetricUsers({ content_id: this.props.card.id })
      );
      this.props.dispatch(multiActions._fetchAllAssigned({ content_id: this.props.card.id }));
      this.state.activeTab === 'Individuals' &&
        this.props.contentMultiaction &&
        this.props.contentMultiaction.users &&
        this.props.contentMultiaction.users.length &&
        this.getCurrentlyChecked(this.props);
    }
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeTab === 'Individuals' &&
      this.state.dynamicSelections &&
      ((nextProps.modal &&
        nextProps.modal.filter &&
        this.props.modal.filter &&
        this.props.modal.filter.length !== nextProps.modal.filter.length) ||
        (!nextProps.modal.filter && !this.props.modal.filter && this.state.previewFilter))
    ) {
      this.applyFilter(
        nextProps.modal && nextProps.modal.filter && nextProps.modal.filter.length > 1
      );
    }
    this.getCurrentlyChecked(nextProps);
  }

  _tabToggle = activeTab => {
    if (this.state.dynamicSelections) {
      let toState = { activeTab };
      switch (activeTab) {
        case 'Individuals':
          if (this.state.activeTab === 'Dynamic Selections') {
            toState.dynamicFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
            toState.individualFilters = emptyFilter;
          }
          this.props.dispatch(
            updateDynamicSelectionFiltersV2(this.state.individualFilters, this.currentAction)
          );
          this.setState(toState);
          break;
        case 'Dynamic Selections':
          if (this.state.activeTab === 'Individuals') {
            toState.individualFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
            toState.dynamicFilters = emptyFilter;
          }
          this.props.dispatch(
            updateDynamicSelectionFiltersV2(this.state.dynamicFilters, this.currentAction)
          );
          this.setState(toState);
          break;
        default:
          if (this.state.activeTab === 'Dynamic Selections') {
            toState.dynamicFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
          }
          if (this.state.activeTab === 'Individuals') {
            toState.individualFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
          }
          this.setState(toState);
      }
    } else {
      this.setState({ activeTab });
    }
  };

  _closeModal = () => {
    this.props.dispatch(
      multiActions._clearState(
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard]
      )
    );
    this.props.dispatch(close());
  };

  getUsers = async (idsOnly, offset = 0, searchValue) => {
    if (this.state.previewFilter || (this.props.modal && this.props.modal.filter)) {
      let filters =
        this.state.previewFilter ||
        (this.props.modal && this.props.modal.filter && this.props.modal.filter.length > 1
          ? this.props.modal.filter
          : []);
      let payload = { limit: 20, q: '' };

      if (idsOnly) {
        payload.only_user_ids = true;
      }
      filters.forEach(filter => {
        let options = [];
        if (filter.options && filter.options.length) {
          payload.limit = this.usersLimit;
          payload.offset = offset;
          filter.options.forEach(option => {
            if (option.checked) {
              options.push(option.option.toLowerCase());
              options.push(option.option);
            }
          });
          if (!filter.isCheckedAll || searchValue) {
            if (filter.type === 'name') {
              payload['names_for_filtering[]'] = options;
            } else {
              if (!payload.custom_fields) {
                payload.custom_fields = [];
              }
              payload.custom_fields.push({ name: filter.type, values: options });
            }
          }
        }
      });
      payload.fields = 'id,email,handle,avatarimages,name';
      if (payload.custom_fields) {
        payload.custom_fields = JSON.stringify(payload.custom_fields);
      }
      return await searchForUsers(payload);
    } else {
      return [];
    }
  };

  _openGroupModal = async () => {
    if (this.state.activeTab === 'Individuals') {
      let users = this.state.users || [];
      let addedUsers =
        (this.props.contentMultiaction && this.props.contentMultiaction.actionWithUsers) || [];
      if (this.props.contentMultiaction && this.props.contentMultiaction.searchedUsers) {
        addedUsers = addedUsers.concat(this.props.contentMultiaction.searchedUsers);
      }
      let usersArray = [];
      if (this.props.currentAction === 'invite') {
        usersArray = _.map(usersArray.concat(_.intersectionBy(addedUsers, users, 'id')), 'id');
      } else {
        usersArray = _.map(addedUsers, 'id');
      }
      this.setState({
        userListForNewGroup: usersArray
      });
    }

    this.setState({ showCreateGroupModal: true });
  };

  _submitData = async data => {
    let payload = {};
    this.setState({ buttonPending: true, err: false });
    payload.selfAssign = false;
    let startDate =
      this.state.startDate === '' ? '' : moment(this.state.startDate).format('MM/DD/YYYY');
    let dueDate = this.state.dueDate === '' ? '' : moment(this.state.dueDate).format('MM/DD/YYYY');
    let message = (this.assignMessage && this.assignMessage.value) || '';
    let successMessage = '';
    let {
      actionWithUsers,
      actionWithGroups,
      removeUsersFromList,
      removeGroupsFromList,
      searchedUsers,
      searchedGroups
    } = data;
    switch (this.props.currentAction) {
      case 'share':
        payload = {
          remove_user_ids: removeUsersFromList ? _.map(removeUsersFromList, 'id') : [],
          remove_team_ids: removeGroupsFromList ? _.map(removeGroupsFromList, 'id') : [],
          team_ids: _.map([...(actionWithGroups || []), ...(searchedGroups || [])], 'id'),
          user_ids: _.map(
            [...(actionWithUsers || []), ...(searchedUsers || [])] || this.state.previewData,
            'id'
          )
        };
        if (this.state.activeTab !== 'Dynamic Selections') {
          payload.user_ids = _.map([...(actionWithUsers || []), ...(searchedUsers || [])], 'id');
        } else {
          payload.user_ids = this.state.previewData ? _.map(this.state.previewData, 'id') : [];
        }
        break;

      case 'assign':
        payload = {
          team_ids: _.map([...(actionWithGroups || []), ...(searchedGroups || [])], 'id'),
          assignee_ids:
            this.state.activeTab === 'Dynamic Selections'
              ? this.state.users
              : _.map([...(actionWithUsers || []), ...(searchedUsers || [])], 'id'),
          assignType: 'individual'
        };
        if (this.state.activeTab === 'Groups') {
          payload.selfAssign = true;
          payload.excludeUsers = [];
          payload.assignType = 'group';
        }
        if (this.state.activeTab === 'Currently Assigned') {
          if (
            this.props.contentMultiaction &&
            ((this.props.contentMultiaction.removeUsersFromList &&
              this.props.contentMultiaction.removeUsersFromList.length) ||
              (this.props.contentMultiaction.removeGroupsFromList &&
                this.props.contentMultiaction.removeGroupsFromList.length))
          ) {
            payload = {
              assignment_ids:
                _.map(this.props.contentMultiaction.removeUsersFromList, 'assignmentId') || [],
              team_ids: _.map(this.props.contentMultiaction.removeGroupsFromList, 'id') || []
            };
          } else {
            payload = {};
          }
        }
        break;

      case 'invite':
        let groupID = this.props.groupsV2 && this.props.groupsV2.currentGroupID;
        let emails = [];
        if (this.state.activeTab === 'Dynamic Selections') {
          emails = this.state.previewData ? _.map(this.state.previewData, 'email') : [];
        } else {
          let pending =
            this.props.groupsV2 &&
            this.props.groupsV2[groupID] &&
            this.props.groupsV2[groupID].pending;
          let pendingList = pending ? _.map(pending, 'email') : [];
          emails = _.map([...(actionWithUsers || []), ...(searchedUsers || [])], 'email');
          emails = _.difference(emails, pendingList);
        }
        emails.length
          ? (payload = {
              emails,
              roles: 'member',
              id: groupID,
              userList: [...(actionWithUsers || []), ...(searchedUsers || [])]
            })
          : (payload = {});
        if (this.state.activeTab === 'Currently Invited') {
          payload.id = removeUsersFromList ? _.map(removeUsersFromList, 'id') : [];
          payload.teamID = this.props.groupsV2 && this.props.groupsV2.currentGroupID;
        }
        break;

      default:
        break;
    }
    if (Object.keys(payload).length > 0) {
      if (this.props.currentAction === 'share') {
        let newSharedTeams = [...(data.actionWithGroups || []), ...(data.searchedGroups || [])];
        let newSharedUsers = [...(data.actionWithUsers || []), ...(data.searchedUsers || [])];
        shareToGroup(this.props.card.id, payload)
          .then(response => {
            this.props.dispatch(multiActions._clearState(response.card));
            this.setState({ buttonPending: false });
            let cardType =
              response.card.cardType === 'pack'
                ? 'Pathway'
                : response.card.cardType === 'journey'
                ? 'Journey'
                : 'Smartcard';
            let messageSuccess = cardType + ' successfully shared';
            if (this.state.newModalAndToast) {
              let all_shared = this.state.activeTab === 'Groups' ? newSharedTeams : newSharedUsers;
              let first_shared_name =
                this.state.activeTab === 'Groups'
                  ? all_shared[0] && all_shared[0].name
                  : all_shared[0].fullName
                  ? all_shared[0].fullName
                  : all_shared[0].firstName + all_shared[0].lastName;
              messageSuccess = `You have shared this ${cardType} with ${first_shared_name}`;
              if (all_shared.length > 1) {
                let other_shared_count = all_shared.length - 1;
                let plural_shared_label = all_shared.length > 2 ? 's' : '';
                messageSuccess +=
                  ` and ${other_shared_count} other` +
                  (this.state.activeTab === 'Groups'
                    ? ` group${plural_shared_label}`
                    : `${plural_shared_label}`);
              }
            }

            this.props.dispatch(openSnackBar(messageSuccess, true));
            this.props.dispatch(updateCurrentCard(response.card));
            this.props.dispatch(close());
          })
          .catch(err => {
            console.error(`Error while sharing in MultiactionsModal: ${err}`);
            if (this.state.newModalAndToast) {
              let msg = 'This card is restricted and can be shared only by the author.';
              this.props.dispatch(openSnackBar(msg));
            } else {
              let msg = 'An error occurred while sharing the card';
              this.props.dispatch(openStatusModal(msg));
            }
          });
      } else if (this.props.currentAction === 'assign') {
        if (this.state.activeTab === 'Currently Assigned') {
          deleteUsersAssignment(this.props.card.id, payload)
            .then(() => {
              this.props.dispatch(multiActions._clearState(this.props.card));
              this.props.dispatch(close());
              this.props.dispatch(assignmentsAction.markAssignmentCountToBeUpdated());
              successMessage = 'Card assignments had been updated';
              this.props.dispatch(openSnackBar(successMessage, true));
            })
            .catch(() => {
              let msg = 'An error occurred while delete assigning from users';
              if (this.state.newModalAndToast) {
                this.props.dispatch(openSnackBar(msg));
              } else {
                this.props.dispatch(openStatusModal(msg));
              }
            });
          return;
        }
        assignTo(
          data.currentCard,
          'Card',
          payload.assignType,
          payload.team_ids,
          payload.assignee_ids,
          dueDate,
          message,
          payload.selfAssign,
          startDate,
          payload.excludeUsers
        )
          .then(res => {
            this.setState({ buttonPending: false });
            this.props.dispatch(multiActions._clearState(this.props.card));
            this.props.dispatch(close());
            this.props.dispatch(assignmentsAction.markAssignmentCountToBeUpdated());
            let card_type = data[data.currentCard].cardType;
            if (this.state.newModalAndToast && payload.assignee_ids.length) {
              let type = 'SmartCard';
              if (card_type === 'pack') {
                type = 'Pathway';
              }
              if (card_type === 'journey') {
                type = 'Journey';
              }
              let assignee = [...(data.actionWithUsers || []), ...(data.searchedUsers || [])];
              let first_user_name = assignee[0].fullName
                ? assignee[0].fullName
                : assignee[0].firstName + assignee[0].lastName;
              successMessage = `You have assigned this ${type} to ${first_user_name}`;
              if (assignee.length > 1) {
                let other_users_count = assignee.length - 1;
                let plural_users_label = assignee.length > 2 ? 's' : '';
                successMessage += ` and ${other_users_count} other${plural_users_label}`;
              }
            } else {
              successMessage = 'The card had been assigned';
            }
            this.props.dispatch(openSnackBar(successMessage, true));
          })
          .catch(err => {
            let msg = 'An error occurred while assigning this card';
            console.error(`Error while assigning in MultiactionsModal: ${err}`);
            if (this.state.newModalAndToast) {
              this.props.dispatch(openSnackBar(msg));
            } else {
              this.props.dispatch(openStatusModal(msg));
            }
          });
      } else {
        if (this.state.activeTab === 'Currently Invited') {
          this.props
            .dispatch(deleteUserFromTeam(payload))
            .then(res => {
              this.props.dispatch(multiActions._clearState());
              this.props.dispatch(close());
            })
            .catch(() => {
              let msg = 'An error occurred while removing members from this group';
              if (this.state.newModalAndToast) {
                this.props.dispatch(openSnackBar(msg));
              } else {
                this.props.dispatch(openStatusModal(msg));
              }
            });
        } else {
          this.props
            .dispatch(invite(payload))
            .then(() => {
              this.props.dispatch(multiActions._clearState());
            })
            .catch(err => {
              let msg = 'An error occurred while inviting users';
              if (this.state.newModalAndToast) {
                this.props.dispatch(openSnackBar(msg));
              } else {
                this.props.dispatch(openStatusModal(msg, err));
              }
            });
          this.props.dispatch(close());
        }
      }
    } else {
      let msg =
        this.state.activeTab === 'Currently Assigned'
          ? 'Nothing had been changed!'
          : 'No user was found for the current filters.';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  _openPreview = async () => {
    let data = await this.getUsers();
    let previewData = data.users;
    if (this.props.currentAction === 'invite') {
      let currentGroup =
        this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID];
      let alreadyInvited = [
        ...((currentGroup && currentGroup.members) || []),
        ...((currentGroup && currentGroup.pending) || [])
      ];
      previewData = _.differenceBy(data.users, alreadyInvited, 'email');
    }
    let ids = previewData.map(item => item.id);
    let usersCF;
    let allCF;
    let customFieldsArray = [];
    if (this.state.dynamicSelections) {
      usersCF = await getUserCustomFields({ 'user_ids[]': ids });
      allCF = await getAllCustomFields();
      allCF = allCF.customFields;
      allCF = _.filter(allCF, 'enableInFilters');
      allCF = _.orderBy(allCF, 'id', 'asc');
      previewData = previewData.map(user => {
        if (usersCF && usersCF[user.id]) {
          customFieldsArray = Object.keys(usersCF[user.id]).map(i => {
            return { cfId: +i, cfContent: usersCF[user.id][i] };
          });
        }
        if (allCF && allCF.length) {
          _.forEach(allCF, header => {
            let index = _.findIndex(customFieldsArray, ['cfId', header.id]);
            if (!~index) {
              customFieldsArray.push({ cfId: +header.id, cfContent: '-', allowed: true });
            } else {
              customFieldsArray[index].allowed = true;
            }
          });
        }
        customFieldsArray = _.sortBy(_.filter(customFieldsArray, 'allowed'), 'cfId');
        user.customFields = customFieldsArray;
        return user;
      });
    }
    this.setState({
      previewData,
      showPreview: true,
      userCustomFields: usersCF || [],
      customFieldsList: allCF || []
    });
  };

  loadMoreUsers = async () => {
    this.setState({ isPendingPreview: true });
    let data = await this.getUsers(false, this.state.previewData.length);
    this.isAllPreviewLoaded = data.users.length < this.usersLimit;
    this.setState({
      isPendingPreview: false,
      previewData: this.state.previewData.concat(data.users)
    });
  };

  backHandle = () => {
    this.setState({
      showCreateGroupModal: false,
      showPreview: false
    });
  };

  createHandle = async team => {
    let dataUsers = [];
    if (this.state.activeTab === 'Individuals') {
      dataUsers = this.state.userListForNewGroup;
    } else {
      dataUsers = await this.getUsers(true);
    }
    if (dataUsers && dataUsers.user_ids && dataUsers.user_ids.length && this.props.currentUser) {
      let arr = dataUsers.user_ids.filter(el => el != this.props.currentUser.id);
      if (arr.length) {
        let payload = {
          user_ids: arr,
          role: 'member'
        };
        await addTeamMembers(team.id, payload);
      }
    } else if (dataUsers.length) {
      dataUsers = dataUsers.filter(el => el != this.props.currentUser.id);
      if (dataUsers.length) {
        let payload = {
          user_ids: dataUsers,
          role: 'member'
        };
        await addTeamMembers(team.id, payload);
      }
    }
    this.setState({ buttonPending: true, err: false });
    if (this.props.currentAction === 'share') {
      shareToGroup(this.props.card.id, { team_ids: [team.id] })
        .then(response => {
          this.props.dispatch(multiActions._clearState(response.card));
          this.setState({ buttonPending: false });
          let cardType =
            response.card.cardType === 'pack'
              ? 'Pathway'
              : response.card.cardType === 'journey'
              ? 'Journey'
              : 'Smartcard';
          this.props.dispatch(openSnackBar(`${cardType} successfully shared`, true));
          this.props.dispatch(updateCurrentCard(response.card));
          this.props.dispatch(close());
        })
        .catch(() => {
          let msg = 'This card is restricted and can be shared only by the author.';
          if (this.state.newModalAndToast) {
            this.props.dispatch(openSnackBar(msg));
          } else {
            this.props.dispatch(openStatusModal(msg));
          }
        });
    } else if (this.props.currentAction === 'assign') {
      let startDate =
        this.state.startDate === '' ? '' : moment(this.state.startDate).format('MM/DD/YYYY');
      let dueDate =
        this.state.dueDate === '' ? '' : moment(this.state.dueDate).format('MM/DD/YYYY');
      let message = this.assignMessage ? this.assignMessage.value : '';
      assignTo(
        this.props.contentMultiaction.currentCard,
        'Card',
        'group',
        [team.id],
        [],
        dueDate,
        message,
        true,
        startDate,
        []
      )
        .then(res => {
          this.setState({ buttonPending: false });
          this.props.dispatch(multiActions._clearState(this.props.card));
          this.props.dispatch(close());
          this.props.dispatch(assignmentsAction.markAssignmentCountToBeUpdated());
          this.props.dispatch(
            openSnackBar(
              `Assigned to a ${this.state.activeTab !== 'Individuals' ? 'dynamic ' : ''}group`,
              true
            )
          );
        })
        .catch(() => {
          let msg = 'An error occurred while assigning this card';
          if (this.state.newModalAndToast) {
            this.props.dispatch(openSnackBar(msg));
          } else {
            this.props.dispatch(openStatusModal(msg));
          }
        });
    }
  };

  applyFilter = async (isFiltersExisted, searchValue) => {
    let data = isFiltersExisted
      ? await this.getUsers(this.state.activeTab === 'Dynamic Selections', 0, searchValue)
      : this.props.dispatch(
          multiActions._fetchUsers({
            limit: 15,
            offset: 0,
            full_response: true,
            sort: 'first_name',
            order: 'asc'
          })
        );
    this.setState({
      users: this.state.activeTab === 'Dynamic Selections' ? data.user_ids : data.users
    });
  };

  getPreviewList = (data, searchValue) => {
    this.setState(
      {
        previewFilter: data || this.props.modal.filter
      },
      () => {
        if (data.length && data[0].id !== -1) {
          this.applyFilter(true, searchValue);
        } else {
          this.setState({
            users: undefined
          });
        }
        this.props.dispatch(
          updateDynamicSelectionFiltersV2(this.state.previewFilter, this.currentAction)
        );
      }
    );
  };

  getContinuousButtonName = name => {
    let lastChar = name.substr(name.length - 1);
    let newName = name;
    if (lastChar === 'e') {
      newName = name.substring(0, name.length - 1);
    }
    return `${newName}ing...`;
  };

  handleStartDateChange = event => {
    this.setState({
      startDate: event.format('MM/DD/YYYY'),
      calendarOpen: false
    });
  };

  handleDueDateChange = event => {
    this.setState({
      startDate: this.state.startDate === '' ? moment(new Date()) : this.state.startDate,
      dueDate: event.format('MM/DD/YYYY'),
      calendarEndDateOpen: false
    });
  };

  getCurrentlyChecked(props) {
    let multiAction = props.contentMultiaction || {};
    let groups = props.groupsV2 || {};
    let users = (multiAction && multiAction.users) || [];
    let newAddedUsers = (multiAction && multiAction.actionWithUsers) || [];
    let removedUsers = (multiAction && multiAction.removeUsersFromList) || [];
    if (props.currentAction === 'assign') {
      if (multiAction[multiAction.currentCard]) {
        this.setState({
          checkedItemsCount: newAddedUsers.length
        });
      }
    } else if (props.currentAction === 'share') {
      let sharedUsers =
        (multiAction &&
          multiAction.currentCard &&
          multiAction[multiAction.currentCard] &&
          multiAction[multiAction.currentCard].usersWithAccess) ||
        [];
      let allShared = _.xorBy(
        _.uniqBy([...sharedUsers, ...newAddedUsers], 'email'),
        removedUsers,
        'email'
      );
      if (multiAction[multiAction.currentCard]) {
        this.setState({
          checkedItemsCount: _.intersectionBy(users, allShared, 'email').length
        });
      }
    } else {
      if (groups[groups.currentGroupId] && multiAction.actionWithUsers) {
        this.setState({
          checkedItemsCount: newAddedUsers.length
        });
      }
    }
  }

  getShownUsers = usersNumber => {
    this.setState({ totalNumber: usersNumber || 0 });
  };

  getShownGroups = groupNumber => {
    this.setState({ totalGroupNumber: groupNumber || 0 });
  };

  render() {
    let users = (this.props.contentMultiaction && this.props.contentMultiaction.users) || [];
    let groups = (this.props.contentMultiaction && this.props.contentMultiaction.groups) || [];
    let groupsSelected =
      (this.props.contentMultiaction &&
        this.props.contentMultiaction.actionWithGroups &&
        this.props.contentMultiaction.actionWithGroups.length) ||
      0;
    if (this.props.currentAction === 'share') {
      let sharedWithGroups =
        (this.props.contentMultiaction &&
          this.props.contentMultiaction.currentCard &&
          this.props.contentMultiaction[this.props.contentMultiaction.currentCard] &&
          this.props.contentMultiaction[this.props.contentMultiaction.currentCard].teams) ||
        [];
      sharedWithGroups = _.intersectionBy(groups, sharedWithGroups, 'id');
      groupsSelected = groupsSelected + sharedWithGroups.length;
    }
    let modalFilter = (this.props.modal && this.props.modal.filter) || [];
    let currentFilters = [...modalFilter, ...(this.state.previewFilter || [])];
    let existFilter =
      currentFilters.filter(el => el.type !== '+' && el.type !== 'Add Filter +').length ||
      (this.state.activeTab === 'Individuals' &&
        this.props.contentMultiaction &&
        this.props.contentMultiaction.actionWithUsers &&
        this.props.contentMultiaction.actionWithUsers.length);
    let isCurrentlyActivatedTab = !(
      this.state.activeTab === 'Individuals' ||
      this.state.activeTab === 'Groups' ||
      this.state.activeTab === 'Dynamic Selections'
    );
    let showCurrentState = this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY');
    return !this.state.showCreateGroupModal && !this.state.showPreview ? (
      <div style={{ width: '58.125rem', margin: '0 auto' }}>
        <div className="row">
          <div
            className="small-12 columns"
            style={{ position: 'relative', marginBottom: '0.625rem' }}
          >
            <div style={{ color: '#ffffff', fontSize: '1.125rem' }}>
              {' '}
              {!this.state.dynamicSelections
                ? tr(`${this.actionButtonName} with`)
                : tr(this.actionButtonName)}
            </div>
            <div className="close close-button">
              <IconButton
                style={{ paddingRight: 0, width: 'auto' }}
                aria-label="close"
                onTouchTap={this._closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper>
              <div className="multiaction-modal-container">
                <div className="ma_tabs_container">
                  {this.state.tabs.map((tab, index) => {
                    return (
                      <div
                        className={this.state.activeTab === tab ? 'sc_tab active' : 'sc_tab'}
                        style={{ width: 'auto' }}
                        key={index}
                      >
                        <span
                          onClick={() => {
                            this._tabToggle(tab);
                          }}
                          className="pointer"
                        >
                          {tr(tab)}
                        </span>
                      </div>
                    );
                  })}
                </div>
                <div className="ma_tabs_content_container">
                  <div
                    className={
                      this.state.activeTab === 'Individuals' ? '' : 'inactive-tab-container'
                    }
                  >
                    <MultiActionDynamicSelections
                      {...this.props}
                      forIndividuals
                      getPreviewList={this.getPreviewList}
                      activeTab={this.state.activeTab}
                    />
                    <MultiActionSearchBox
                      {...this.props}
                      activeTab={this.state.activeTab}
                      currentAction={this.props.currentAction}
                    />
                    <IndividualsView
                      {...this.props}
                      users={this.state.users}
                      currentTab={this.state.activeTab}
                      currentAction={this.props.currentAction}
                      getShownUsers={this.getShownUsers}
                    />
                  </div>

                  <div
                    className={this.state.activeTab === 'Groups' ? '' : 'inactive-tab-container'}
                  >
                    <MultiActionSearchBox
                      {...this.props}
                      activeTab={this.state.activeTab}
                      currentAction={this.props.currentAction}
                    />
                    <GroupsView
                      currentAction={this.props.currentAction}
                      currentTab={this.state.activeTab}
                      {...this.props}
                      getShownGroups={this.getShownGroups}
                    />
                  </div>
                  {this.state.dynamicSelections && (
                    <div
                      className={
                        this.state.activeTab === 'Dynamic Selections'
                          ? ''
                          : 'inactive-tab-container'
                      }
                    >
                      <MultiActionDynamicSelections
                        {...this.props}
                        getPreviewList={this.getPreviewList}
                        activeTab={this.state.activeTab}
                      />
                    </div>
                  )}
                  {showCurrentState && (
                    <div
                      className={
                        this.state.activeTab === 'Currently Shared' ||
                        this.state.activeTab === 'Currently Assigned' ||
                        this.state.activeTab === 'Currently Invited'
                          ? ''
                          : 'inactive-tab-container'
                      }
                    >
                      <CurrentState
                        {...this.props}
                        currentAction={this.props.currentAction}
                        preview={this._openPreview}
                      />
                    </div>
                  )}
                  {this.props.currentAction === 'assign' &&
                    this.state.activeTab !== 'Currently Assigned' && (
                      <div>
                        <input
                          className="assign-message"
                          name="assign-message"
                          placeholder={tr('Include Message')}
                          ref={node => {
                            this.assignMessage = node;
                          }}
                        />
                        <div className="date-picker__block" style={{ marginRight: '2.875rem' }}>
                          <div className="date-picker__label">
                            {tr('Start Date:  ')}
                            <div
                              className="date-picker"
                              style={this.styles.dateContainer}
                              onClick={() => {
                                this.setState({ calendarOpen: !this.state.calendarOpen });
                              }}
                            >
                              {this.state.startDate === ''
                                ? 'MM/DD/YYYY'
                                : moment(this.state.startDate).format('MM/DD/YYYY')}
                            </div>
                          </div>
                          <div style={this.styles.calendar}>
                            {this.state.calendarOpen && (
                              <div>
                                <Calendar
                                  container="inline"
                                  minDate={moment()}
                                  maxDate={
                                    this.state.dueDate === '' ? '' : moment(this.state.dueDate)
                                  }
                                  date={
                                    this.state.startDate === ''
                                      ? moment(new Date())
                                      : moment(this.state.startDate)
                                  }
                                  onChange={this.handleStartDateChange}
                                />
                                <div className="text-center">
                                  <button
                                    className={'my-button'}
                                    disabled={!(this.state.dueDate === '')}
                                    style={this.styles.assignCalenderReset}
                                    onClick={() => {
                                      this.setState({ startDate: '' });
                                    }}
                                  >
                                    {tr('Reset')}
                                  </button>
                                  <button
                                    className="my-button"
                                    style={{ borderColor: colors.followColor }}
                                    onClick={() => {
                                      this.setState({ calendarOpen: false });
                                    }}
                                  >
                                    {tr('Cancel')}
                                  </button>
                                  {!(this.state.dueDate === '') && (
                                    <div className="start-date-error-message">
                                      Start date can&#39;t be reset if due date is present.
                                    </div>
                                  )}
                                </div>
                              </div>
                            )}
                          </div>
                        </div>

                        <div className="date-picker__block">
                          <div className="date-picker__label">
                            {tr('Due Date:  ')}
                            <div
                              className="date-picker"
                              style={this.styles.dateContainer}
                              onClick={() => {
                                this.setState({
                                  calendarEndDateOpen: !this.state.calendarEndDateOpen
                                });
                              }}
                            >
                              {this.state.dueDate === ''
                                ? 'MM/DD/YYYY'
                                : moment(this.state.dueDate).format('MM/DD/YYYY')}
                            </div>
                          </div>
                          <div style={this.styles.calendar}>
                            {this.state.calendarEndDateOpen && (
                              <div>
                                <Calendar
                                  container="inline"
                                  minDate={
                                    this.state.startDate === ''
                                      ? moment(new Date())
                                      : moment(this.state.startDate)
                                  }
                                  date={
                                    this.state.dueDate === ''
                                      ? moment(new Date())
                                      : moment(this.state.dueDate)
                                  }
                                  onChange={this.handleDueDateChange}
                                />
                                <div className="text-center">
                                  <button
                                    className="my-button"
                                    style={this.styles.assignCalenderReset}
                                    onClick={() => {
                                      this.setState({ dueDate: '' });
                                    }}
                                  >
                                    {tr('Reset')}
                                  </button>
                                  <button
                                    className="my-button"
                                    style={{ borderColor: colors.followColor }}
                                    onClick={() => {
                                      this.setState({ calendarEndDateOpen: false });
                                    }}
                                  >
                                    {tr('Cancel')}
                                  </button>
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    )}
                  <div className="inactive-tab-container" />
                </div>
                <div className="text-center sc-button-container">
                  {this.state.activeTab === 'Individuals'
                    ? !!users.length && (
                        <div className="individuals-select-info">
                          {tr(`%{count} of %{total} users selected`, {count: this.state.checkedItemsCount, total: this.state.totalNumber})}
                        </div>
                      )
                    : this.state.activeTab === 'Groups' &&
                      !!groups.length && (
                        <div className="individuals-select-info">
                          {tr(`%{count} of %{total} groups selected`, {count: groupsSelected, total: this.props.currentAction === 'share' ? groups.length : this.state.totalGroupNumber})}
                        </div>
                      )}
                  {this.state.err && (
                    <div className="text-center error-text data-not-available-msg">
                      {tr('Something went wrong! Please try later.')}
                    </div>
                  )}
                  <SecondaryButton
                    style={this.styles.cancel_button}
                    label={tr('Cancel')}
                    onTouchTap={this._closeModal}
                  />
                  {this.state.activeTab === 'Dynamic Selections' && (
                    <SecondaryButton
                      style={this.styles.preview_button}
                      label={tr('Preview Data')}
                      onTouchTap={this._openPreview}
                      disabled={!existFilter}
                    />
                  )}
                  <PrimaryButton
                    style={this.styles.share_button}
                    label={tr(!isCurrentlyActivatedTab ? this.actionButtonName : 'Update')}
                    onTouchTap={() => {
                      this._submitData(this.props.contentMultiaction);
                    }}
                    theme={colors.followColor}
                    pending={this.state.buttonPending}
                    pendingLabel={tr(
                      isCurrentlyActivatedTab
                        ? 'Updating...'
                        : this.getContinuousButtonName(this.actionButtonName)
                    )}
                    disabled={
                      (this.state.activeTab === 'Dynamic Selections' && !existFilter) ||
                      (isCurrentlyActivatedTab &&
                        this.props.contentMultiaction &&
                        !(
                          (this.props.contentMultiaction.removeUsersFromList &&
                            this.props.contentMultiaction.removeUsersFromList.length) ||
                          (this.props.contentMultiaction.removeGroupsFromList &&
                            this.props.contentMultiaction.removeGroupsFromList.length)
                        )) ||
                      (this.state.activeTab !== 'Dynamic Selections' &&
                        !isCurrentlyActivatedTab &&
                        this.props.contentMultiaction &&
                        !(
                          (this.props.contentMultiaction.actionWithUsers &&
                            this.props.contentMultiaction.actionWithUsers.length) ||
                          (this.props.contentMultiaction.actionWithGroups &&
                            this.props.contentMultiaction.actionWithGroups.length) ||
                          (this.props.contentMultiaction.searchedUsers &&
                            this.props.contentMultiaction.searchedUsers.length) ||
                          (this.props.contentMultiaction.searchedGroups &&
                            this.props.contentMultiaction.searchedGroups.length)
                        ))
                    }
                  />

                  {this.props.currentAction !== 'invite' &&
                    Permissions.has('CREATE_GROUP') &&
                    ((this.state.dynamicSelections &&
                      this.state.activeTab === 'Dynamic Selections') ||
                      this.state.activeTab === 'Individuals') && (
                      <SecondaryButton
                        style={this.styles.share_and_create}
                        label={tr(`${this.actionButtonName} and Create Group`)}
                        onTouchTap={this._openGroupModal}
                        disabled={
                          (this.state.activeTab === 'Dynamic Selections' && !existFilter) ||
                          (this.state.activeTab !== 'Dynamic Selections' &&
                            this.props.contentMultiaction &&
                            !(
                              (this.props.contentMultiaction.actionWithUsers &&
                                this.props.contentMultiaction.actionWithUsers.length) ||
                              (this.props.contentMultiaction.searchedUsers &&
                                this.props.contentMultiaction.searchedUsers.length)
                            ))
                        }
                      />
                    )}
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    ) : this.state.showPreview ? (
      <PreviewDSModal
        backHandle={this.backHandle.bind(this)}
        previewData={this.state.previewData}
        currentAction={this.props.currentAction}
        loadMoreUsers={this.loadMoreUsers}
        isPendingPreview={this.state.isPendingPreview}
        isAllPreviewLoaded={this.isAllPreviewLoaded}
        customFieldsList={this.state.customFieldsList}
        previewFilter={this.state.previewFilter}
      />
    ) : (
      <CreateGroupForMultiaction
        backHandle={this.backHandle.bind(this)}
        createHandle={this.createHandle.bind(this)}
        isDynamicGroup={this.state.activeTab === 'Dynamic Selections'}
        activeTab={this.state.activeTab}
        card={this.props.card}
        currentAction={this.props.currentAction}
        assignMesage={this.assignMessage && this.assignMessage.value}
        previewFilter={this.state.previewFilter}
      />
    );
  }
}

MultiactionsModal.propTypes = {
  currentAction: PropTypes.string,
  selfAssign: PropTypes.bool,
  contentMultiaction: PropTypes.any,
  currentUser: PropTypes.any,
  card: PropTypes.object,
  dispatch: PropTypes.any,
  modal: PropTypes.any,
  assignments: PropTypes.object,
  groupsV2: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    modal: state.modal.toJS(),
    contentMultiaction: state.contentMultiaction.toJS(),
    assignments: state.assignments.toJS(),
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(MultiactionsModal);
