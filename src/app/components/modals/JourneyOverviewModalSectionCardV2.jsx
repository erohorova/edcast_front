import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LockedIcon from 'edc-web-sdk/components/icons/Lock';
import CardListViewPreview from '../cards/CardListViewPreview';
import { tr } from 'edc-web-sdk/helpers/translations';

class JourneyOverviewModalSectionCardV2 extends Component {
  constructor(props, context) {
    super(props, context);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.card !== nextProps.card;
  }

  render() {
    return (
      <div key={this.props.keyIndex}>
        {this.props.card.locked ? (
          <div className="list-block-preview">
            <div className="list-main-card">
              <div className="list-locked-card">
                <div className="locked-card-in-modal">
                  <LockedIcon />
                </div>
                <div>{tr('LOCKED')}</div>
              </div>
            </div>
          </div>
        ) : (
          <div
            onClick={e => {
              this.props.chooseCard(
                this.props.card,
                this.props.keyIndex,
                this.props.indexSection,
                e
              );
            }}
          >
            <CardListViewPreview
              card={this.props.card}
              index={this.props.keyIndex + 1}
              author={this.props.card.author}
              cardUpdated={() => {
                this.props.smartBiteUpdated(
                  this.props.card.id,
                  this.props.keyIndex,
                  this.props.indexSection,
                  this.props.card.isPrivate
                );
              }}
              linkToPush={this.props.linkToPush}
              isPathwayOwner={this.props.isPathwayOwner}
              isPrivate={this.props.card.isPrivate}
            />
          </div>
        )}
      </div>
    );
  }
}

JourneyOverviewModalSectionCardV2.propTypes = {
  card: PropTypes.object,
  keyIndex: PropTypes.number,
  indexSection: PropTypes.number,
  isPathwayOwner: PropTypes.any,
  chooseCard: PropTypes.func,
  smartBiteUpdated: PropTypes.func,
  linkToPush: PropTypes.func
};

export default JourneyOverviewModalSectionCardV2;
