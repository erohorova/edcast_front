/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import OrgPaymentDetails from '../../components/payment/OrgPaymentDetails';
import find from 'lodash/find';
import TextField from 'material-ui/TextField';
let LocaleCurrency = require('locale-currency');

class OrgPaymentModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      paymentError: '',
      titleLabel: '',
      sentDetails: false,
      showForm: false,
      disabled: true,
      priceData: null,
      orgSubscriptions: []
    };
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      }
    };
  }

  componentDidMount() {
    if (this.props.currentUser) {
      let countryCode = this.props.currentUser.countryCode || 'us';
      let currency = LocaleCurrency.getCurrency(countryCode);
      let orgSubscriptions = this.props.team.orgSubscriptions;
      let priceData = null;

      if (
        orgSubscriptions.length > 0 &&
        orgSubscriptions[0].prices &&
        orgSubscriptions[0].prices.length > 0
      ) {
        priceData =
          find(orgSubscriptions[0].prices, { currency: currency }) ||
          find(orgSubscriptions[0].prices, { currency: 'USD' });
      }

      this.setState({
        orgSubscriptions: orgSubscriptions,
        priceData: priceData
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.currentUser) {
      let countryCode = nextProps.currentUser.countryCode || 'us';
      let currency = LocaleCurrency.getCurrency(countryCode);
      let orgSubscriptions = this.props.team.orgSubscriptions;
      let priceData = null;

      if (
        orgSubscriptions.length > 0 &&
        orgSubscriptions[0].prices &&
        orgSubscriptions[0].prices.length > 0
      ) {
        priceData =
          find(orgSubscriptions[0].prices, { currency: currency }) ||
          find(orgSubscriptions[0].prices, { currency: 'USD' });
      }

      this.setState({
        orgSubscriptions: orgSubscriptions,
        priceData: priceData
      });
    }
  }

  openForm = e => {
    e.preventDefault();
    this.setState({
      showForm: true
    });
  };

  render() {
    let user = this.props.currentUser;
    return (
      <div id="org-payment-modal">
        <div className="modal-actions" style={{ marginBottom: '40px' }}>
          <div className="card-payment-modal">
            <div className="card-payment-header">
              {!this.state.showForm && <h5>Hi, {user.first_name} </h5>}
              {this.state.showForm && <h5>Complete your transaction</h5>}
              <TextField name="orgpaymentmodal" autoFocus={true} className="hiddenTextField" />
            </div>

            <div className="card-container">
              {!this.state.showForm && (
                <div className="subs-text">
                  <div
                    dangerouslySetInnerHTML={{
                      __html:
                        this.state.orgSubscriptions &&
                        this.state.orgSubscriptions[0] &&
                        this.state.orgSubscriptions[0].description
                    }}
                  />
                  <p style={{ marginTop: '10px' }}>
                    Kindly click on “Proceed to Pay” to continue learning.
                  </p>

                  {this.state.priceData && (
                    <div style={{ marginTop: '10px', textAlign: 'right' }}>
                      <a
                        href="#"
                        style={{ background: '#539453', color: '#ffffff', padding: '5px 10px' }}
                        onClick={this.openForm}
                      >
                        Proceed to pay{' '}
                        {`${this.state.priceData.symbol}${this.state.priceData.amount}`} >>>
                      </a>
                    </div>
                  )}
                </div>
              )}
              {this.state.showForm &&
                this.state.orgSubscriptions.length > 0 &&
                this.state.priceData && (
                  <div>
                    <OrgPaymentDetails
                      orgSubscriptions={this.state.orgSubscriptions}
                      priceData={this.state.priceData}
                    />
                  </div>
                )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

OrgPaymentModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  callback: PropTypes.func,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  title: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(OrgPaymentModal);
