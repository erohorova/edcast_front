import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import { close } from '../../actions/modalActions';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import UserBadgesContainer from '../common/UserBadgesContainer';
import { Avatar } from 'material-ui';
import Spinner from '../common/spinner';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getFollowingUsers, getFollowerUsers } from '../../actions/currentUserActions';
import { usersv2 } from 'edc-web-sdk/requests';
import TextField from 'material-ui/TextField';

class FollowingUserListModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      pending: true
    };

    this.cancelClickHandler = this.cancelClickHandler.bind(this);
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  componentDidMount() {
    let typeData = `${this.props.type}Users`;
    let user = this.props.currentUser;
    let isData = user && user[typeData] && !!user[typeData].length;
    if (!isData || user.needToUpdateFollowing) {
      this.getFollowingList();
    } else {
      this.setState({ pending: false });
    }
  }

  getFollowingList = (offset = 0, limit = 10, list = []) => {
    let listParam = list;
    if (this.props.type === 'following') {
      return usersv2.getfollowingUsers({ offset, limit }).then(users => {
        listParam = listParam.concat(users.users);
        if (users && users.users && users.users.length < 10) {
          this.props.dispatch(getFollowingUsers(listParam));
          this.setState({ pending: false });
        } else {
          this.getFollowingList(listParam.length, 10, listParam);
        }
      });
    } else {
      return usersv2.getfollowerUsers({ offset, limit }).then(users => {
        listParam = listParam.concat(users.users);
        if (users && users.users && users.users.length < 10) {
          this.props.dispatch(getFollowerUsers(listParam));
          this.setState({ pending: false });
        } else {
          this.getFollowingList(listParam.length, 10, listParam);
        }
      });
    }
  };
  goToHandle = (e, data) => {
    e.preventDefault();
    this.props.dispatch(push(`/${data.handle}`));
  };

  render() {
    let typeData = `${this.props.type}Users`;
    let user = this.props.currentUser;
    let isData = user && user[typeData] && !!user[typeData].length;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    return (
      <div className="cardData">
        <div className="cardData-header">
          {tr(`Show ${this.props.type} List`)}
          <span className={'close close-icon-btn'}>
            <NavigationClose color={'#ffffff'} onTouchTap={this.cancelClickHandler} />
          </span>
          <TextField name="followinglist" autoFocus={true} className="hiddenTextField" />
        </div>
        {this.state.pending ? (
          <div className="text-center">
            <Spinner />
          </div>
        ) : isData ? (
          <div>
            {user[typeData].map((data, index) => (
              <div className="data-user" key={index}>
                <a
                  href="#"
                  className="data-ava"
                  onClick={e => this.goToHandle(e, data)}
                  target="_blank"
                >
                  <Avatar
                    src={(data.avatarimages && data.avatarimages.small) || defaultUserImage}
                    size={26}
                  />
                </a>
                <div className="data-name">
                  {data.name && data.name.length > 25 ? data.name.slice(0, 25) + '...' : data.name}
                </div>
                {(data.roles || data.rolesDefaultNames) && (
                  <div className="data-labels">
                    <UserBadgesContainer
                      rolesDefaultNames={data.rolesDefaultNames}
                      roles={data.roles}
                    />
                  </div>
                )}
              </div>
            ))}
          </div>
        ) : (
          <div className="container-padding data-not-available-msg">{tr('No results found.')}</div>
        )}
      </div>
    );
  }
}

FollowingUserListModal.propTypes = {
  open: PropTypes.bool,
  currentUser: PropTypes.object,
  type: PropTypes.string
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(FollowingUserListModal);
