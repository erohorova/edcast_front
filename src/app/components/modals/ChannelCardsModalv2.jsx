import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { connect } from 'react-redux';
import Card from '../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTypeCardsv2, pinUnpinCardv2 } from '../../actions/channelsActionsV2';
import ChannelCardOverlay from '../channel/v2/ChannelCardOverlay';
import { close, openCardRemovalConfirmation, updateDataBefore } from '../../actions/modalActions';
import Spinner from '../common/spinner';
import TextField from 'material-ui/TextField';
import findIndex from 'lodash/findIndex';

class ChannelCardsModal extends Component {
  constructor(props) {
    super(props);

    this.cards =
      (props.modal && props.modal.dataBefore && props.modal.dataBefore.cards) ||
      (props.modal && props.modal.cards) ||
      [];
    this.state = {
      offset: this.cards.length,
      cards: this.cards,
      loading: false,
      pinnedCount: 0,
      overallCount:
        props.modal.count || (props.modal.dataBefore && props.modal.dataBefore.count) || 0,
      updateModal: false,
      isReordering: false,
      isCustomCarousel: this.props.modal.cardModalDetails.isCustomCarousel,
      autoPinCards: this.props.modal.cardModalDetails.autoPinCards,
      multilingualFiltering: window.ldclient.variation('multilingual-content', false)
    };
    this.styles = {
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.isReordering = this.isReordering.bind(this);
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
  }

  componentWillUnmount() {
    let modalTitle =
      this.props.modal.modalTitle ||
      (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle);
    let overallCount = this.state.overallCount;
    let channel = this.props.channel;
    let cards = this.state.cards;

    this.props.dispatch(updateDataBefore(modalTitle, overallCount, cards, channel, false));
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  fetchData = () => {
    let titleKeysObj = {};
    let cardType =
      this.props.modal.modalTitle ||
      (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle);
    switch (cardType) {
      case 'Courses':
        titleKeysObj.key = 'course';
        titleKeysObj.countKey = 'coursesCount';
        titleKeysObj.cardType = ['course'];
        break;
      case 'SmartCards':
        titleKeysObj.key = 'smartbite';
        titleKeysObj.countKey = 'smartbitesCount';
        titleKeysObj.cardType = ['media', 'poll'];
        break;
      case 'Streams':
        titleKeysObj.key = 'video_stream';
        titleKeysObj.countKey = 'videoStreamsCount';
        titleKeysObj.cardType = ['video_stream'];
        break;
      case 'Journeys':
        titleKeysObj.key = 'journey';
        titleKeysObj.countKey = 'publishedJourneysCount';
        titleKeysObj.cardType = ['journey'];
        break;
      case 'Pathways':
        titleKeysObj.key = 'pack';
        titleKeysObj.countKey = 'publishedPathwaysCount';
        titleKeysObj.cardType = ['pack'];
        break;
      default:
        titleKeysObj.key = 'custom_carousel';
        titleKeysObj.countKey = 'customCarouselCount';
        titleKeysObj.cardType = ['card'];
        break;
    }
    return titleKeysObj;
  };

  viewMoreClickHandler = isReordering => {
    this.setState({ loading: true, updateModal: true });
    let type = this.fetchData().cardType;
    let offset = !!isReordering ? 0 : this.state.cards.length;
    getTypeCardsv2(
      (this.props.modal.channel && this.props.modal.channel.id) ||
        (this.props.modal.dataBefore && this.props.modal.dataBefore.channel.id),
      type,
      12,
      offset,
      this.state.multilingualFiltering
    )
      .then(data => {
        let cards = !!isReordering ? data : [...(this.state.cards || []), ...data];
        this.setState({
          cards,
          loading: false,
          offset: offset,
          isReordering: false
        });
      })
      .catch(err => {
        console.error(`Error in ChannelCardsModal.getTypeCards.func: ${err}`);
      });
  };

  channelReOrderCallback = () => {
    this.viewMoreClickHandler(true);
  };

  pinUnpinCard = (card, pinnedStatus) => {
    let payload = {
      pinnable_id:
        this.props.modal.channel.id ||
        (this.props.modal.dataBefore && this.props.modal.dataBefore.channel.id),
      pinnable_type: 'Channel',
      object_id: card.id,
      object_type: 'Card'
    };
    let cardTypes = this.fetchData().key;
    this.props.dispatch(pinUnpinCardv2(payload, pinnedStatus, cardTypes, card));
  };

  removeCardLaunchModal = card => {
    let cardTypes = this.fetchData().key;
    this.props.dispatch(openCardRemovalConfirmation(card, this.props.modal.channel, cardTypes));
  };

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({ cards: newCardsList });
  };

  isReordering = value => {
    this.setState({
      isReordering: value
    });
  };

  setOffsetReorder = () => {
    this.setState({
      offset: 0
    });
  };

  removeCard = cardId => {
    let cards = this.state.cards;
    let index = findIndex(cards, ['id', cardId]);
    let overallCount = this.state.overallCount;
    if (index > -1) {
      cards.splice(index, 1);
      overallCount--;
    }
    this.setState({
      cards,
      offset: cards.length,
      overallCount
    });
  };

  render() {
    let editMode =
      this.props.modal.editMode ||
      (this.props.modal.dataBefore && this.props.modal.dataBefore.editMode);
    let channel = this.props.channel;
    let pinnedCardIDs = this.props.channelProps.get('pinnedCardIDs') || [];
    return (
      <div className="channel-cards-modal">
        <div className="row">
          <TextField name="channelcardmodal" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>
              {tr(
                this.props.modal.modalTitle ||
                  (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle)
              )}{' '}
              ({this.state.overallCount})
            </div>
            <div className="close close-button">
              <IconButton style={this.styles.iconButton} onTouchTap={this.closeModal}>
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row ">
                {this.state.cards &&
                  this.state.cards.length > 0 &&
                  !this.state.isReordering &&
                  this.state.cards.map(card => {
                    return (
                      <div
                        className={`channel-card-v2 card-v2 custom-medium-6 column ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12 ${(this.props.modal.editMode ||
                          (this.props.modal.dataBefore && this.props.modal.dataBefore.editMode)) &&
                          'show-overlay'}`}
                      >
                        <Card
                          key={card.id}
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={true}
                          moreCards={false}
                          hideModal={editMode}
                          channelSetting={true}
                          removeCardFromList={this.removeCardFromList}
                        />
                        <ChannelCardOverlay
                          card={card}
                          pinnedStatus={!!~pinnedCardIDs.indexOf(card.id)}
                          pinUnpinCard={this.pinUnpinCard}
                          pinnedCount={pinnedCardIDs.length}
                          removeCardLaunchModal={this.removeCardLaunchModal}
                          channel={channel}
                          allCardsCount={this.state.overallCount}
                          isCustomCarousel={this.state.isCustomCarousel}
                          reorderToggle={this.isReordering}
                          setOffset={this.setOffsetReorder}
                          channelReOrderCallback={this.channelReOrderCallback}
                          deleteCard={this.removeCard}
                          autoPinCards={this.state.autoPinCards}
                        />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {this.state.cards &&
              this.state.overallCount > this.state.cards.length &&
              !this.state.isReordering && (
                <button
                  className="view-more-v2"
                  onClick={() => {
                    this.viewMoreClickHandler(false);
                  }}
                >
                  {this.state.loading ? tr('Loading...') : tr('View More')}
                </button>
              )}
            {this.state.isReordering && <Spinner />}
          </div>
        </div>
      </div>
    );
  }
}

ChannelCardsModal.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  channel: PropTypes.object,
  channelProps: PropTypes.object,
  cardModalDetails: PropTypes.object
};

export default connect(state => ({
  modal: state.modal.toJS(),
  currentUser: state.currentUser.toJS(),
  channelProps: state.channelNewReducer
}))(ChannelCardsModal);
