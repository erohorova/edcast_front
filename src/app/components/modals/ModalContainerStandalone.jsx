import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import StandaloneModal from './StandaloneModal';
import isEqual from 'lodash/isEqual';

class ModalContainerStandalone extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      dialogRoot: {
        zIndex: 1501,
        paddingTop: 0
      },
      dialogContent: {
        position: 'relative',
        transform: 'none',
        width: '100%',
        maxWidth: '100%'
      },
      dialogBody: {
        paddingBottom: 0,
        overflowX: 'hidden'
      },
      overlayStyle: {
        display: 'none'
      }
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(nextProps, this.props);
  }

  render() {
    let open = this.props.open || this.props.openCard || false;
    let dialogRoot = { display: open ? 'block' : 'none' };

    let ComponentVar = (
      <StandaloneModal
        card={this.props.card}
        cardType={this.props.cardType}
        dataCard={this.props.dataCard}
        cardUpdated={this.props.cardUpdated}
      />
    );

    return (
      <Dialog
        className="standalone-root modal"
        open={open}
        autoScrollBodyContent={true}
        autoDetectWindowHeight={false}
        contentStyle={this.styles.dialogContent}
        bodyStyle={this.styles.dialogBody}
        overlayStyle={this.styles.overlayStyle}
        style={{ ...this.styles.dialogRoot, ...dialogRoot }}
        repositionOnUpdate={false}
        contentClassName="standalone-dialog"
      >
        {ComponentVar}
      </Dialog>
    );
  }
}

ModalContainerStandalone.propTypes = {
  cardType: PropTypes.string,
  card: PropTypes.object,
  dataCard: PropTypes.object,
  openCard: PropTypes.bool,
  open: PropTypes.bool,
  cardUpdated: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    ...state.modalStandAlone.toJS()
  };
}

export default connect(mapStoreStateToProps)(ModalContainerStandalone);
