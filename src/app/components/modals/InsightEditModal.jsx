/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import InsightEditor from './InsightEditor';

class InsightEditModal extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let card = this.props.card;
    return <InsightEditor card={card} cardUpdated={this.props.cardUpdated} />;
  }
}

InsightEditModal.propTypes = {
  cardUpdated: PropTypes.func,
  card: PropTypes.object,
  cardId: PropTypes.string
};

export default connect(state => ({}))(InsightEditModal);
