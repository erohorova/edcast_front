import React, { Component } from 'react';
import PropTypes from 'prop-types';
import convertRichText from '../../utils/convertRichText';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';
import JourneyOverviewModalSectionCardV2 from './JourneyOverviewModalSectionCardV2';
import { tr } from 'edc-web-sdk/helpers/translations';
import isEqual from 'lodash/isEqual';
import unescape from 'lodash/unescape';

class JourneyOverviewModalSectionV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      completeBar: {
        backgroundColor: '#ffffff',
        borderRadius: '0.125rem',
        height: '0.25rem'
      },
      accordionTitleIcon: {
        fill: 'inherit',
        height: '34px',
        width: '34px'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      },
      showSection: {
        display: 'block'
      },
      hideSection: {
        display: 'none'
      }
    };
    this.isUpdateJourneySection = true;
    this.journeySection = Object.assign({}, props.journeySection);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!isEqual(this.journeySection, nextProps.journeySection)) {
      this.journeySection = Object.assign({}, nextProps.journeySection);
      this.isUpdateJourneySection = true;
    } else {
      this.isUpdateJourneySection = false;
    }
    return this.isUpdateJourneySection;
  }

  openBlock = () => {
    this.journeySection = Object.assign({}, this.props.journeySection);
    this.props.openBlock(this.props.indexSection);
  };

  render() {
    return (
      <div className="journey-part__container" key={this.props.indexSection}>
        <div
          className={`${
            this.props.journeySection.isOpenBlock
              ? 'journey-section-header_open'
              : 'journey-section-header_close'
          } journey-section-header`}
          onClick={() => {
            this.openBlock(this.props.indexSection);
          }}
          style={{ cursor: 'pointer !important' }}
        >
          <input
            type="reset"
            style={{ textAlign: 'left' }}
            className={`journey-section-header__input ${
              !this.props.isOwner ? 'journey-section-header__input_notOwner' : null
            }`}
            value={unescape(
              this.props.journeySection.block_title || this.props.journeySection.block_message
            )}
          />
          {!this.props.isOwner && (
            <div className="progressContainer" style={{ cursor: 'pointer' }}>
              <div
                className="progressText"
                style={{
                  width:
                    (this.props.journeySection.completed_percentage >= 100
                      ? 100
                      : this.props.journeySection.completed_percentage) + '%'
                }}
              >
                {this.props.journeySection.completed_percentage}%
              </div>
              <LinearProgress
                min={0}
                style={this.styles.completeBar}
                max={100}
                mode="determinate"
                value={this.props.journeySection.completed_percentage}
              />
            </div>
          )}
          <span>|</span>
          <FlatButton
            hoverColor="transparent"
            rippleColor="transparent"
            className="accordion-title journey__accordion-title"
            secondary={true}
            icon={
              (this.props.journeySection.isOpenBlock && (
                <NavigationArrowDropUp
                  style={{
                    ...this.styles.accordionTitleIcon,
                    ...this.styles.accordionTitleIcon_open
                  }}
                />
              )) ||
              (!this.props.journeySection.isOpenBlock && (
                <NavigationArrowDropDown
                  style={{
                    ...this.styles.accordionTitleIcon,
                    ...this.styles.accordionTitleIcon_close
                  }}
                />
              ))
            }
          />
        </div>
        <div
          style={
            this.props.journeySection.isOpenBlock
              ? this.styles.showSection
              : this.styles.hideSection
          }
        >
          <div className="smartbites-block">
            <div className="custom-card-container">
              {this.props.journeySection.cards &&
                this.props.journeySection.cards.map((card, index) => {
                  return (
                    <JourneyOverviewModalSectionCardV2
                      card={card}
                      indexSection={this.props.indexSection}
                      keyIndex={index}
                      isPathwayOwner={this.props.isPathwayOwner}
                      chooseCard={this.props.chooseCard}
                      smartBiteUpdated={this.props.smartBiteUpdated}
                      linkToPush={this.props.linkToPush}
                    />
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

JourneyOverviewModalSectionV2.propTypes = {
  journeySection: PropTypes.object,
  indexSection: PropTypes.number,
  isOwner: PropTypes.any,
  isPathwayOwner: PropTypes.any,
  openBlock: PropTypes.func,
  chooseCard: PropTypes.func,
  smartBiteUpdated: PropTypes.func,
  linkToPush: PropTypes.func
};

export default JourneyOverviewModalSectionV2;
