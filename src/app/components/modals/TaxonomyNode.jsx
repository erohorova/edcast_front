import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { fetchUserExpertise } from 'edc-web-sdk/requests/expertise';
import MultilevelTaxonomyContainer from './MultilevelTaxonomyContainer';
import BIAModal from './BIAModal';
class TaxonomyNode extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      biaModalVisible: this.props.biaModalVisible,
      isUserSelected: this.props.isUserSelected,
      topicLevelObj: this.props.topicLevelObj,
      level: 1,
      levelStr: {
        1: 'Beginner',
        2: 'Intermediate',
        3: 'Advanced'
      }
    };

    this.styles = {
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer'
      },
      chipLabel: {
        paddingRight: '27px'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`
      },
      smallIcon: {
        width: '16px',
        height: '16px',
        margin: '6px'
      }
    };
  }

  componentWillReceiveProps = nextProps => {
    this.setState({
      biaModalVisible: nextProps.biaModalVisible,
      topicLevelObj: nextProps.topicLevelObj
    });
  };

  handleCancelClick = () => {
    this.props.closeBIA();
  };

  onSelectTopics = (topic, i, e) => {
    let topicID = topic.id || topic.topic_id;
    if (this.state.topicLevelObj[topicID]) {
      return;
    }
    topic.level = this.state.level;
    this.props.onSelectTopics(topic, i, e);
  };

  updateLevel = topic => {
    this.props.onSelectTopics(topic);
    this.handleCancelClick();
  };

  render() {
    let _this = this;
    let topic = this.props.topic;
    let topicID = topic.id || topic.topic_id;
    let topicLevelObj = this.state.topicLevelObj;
    let isUserSelected = topicLevelObj[topicID];
    let i = this.props.index;
    let chipLabel = this.styles.chipLabel;
    if (isUserSelected) {
      chipLabel.color = '#dddddd';
    } else {
      chipLabel.color = colors.primary;
    }

    return (
      <Chip
        className="taxonomy-chip"
        key={i}
        style={this.styles.chip}
        labelStyle={chipLabel}
        onRequestDelete={
          !this.props.shouldRemove ? null : _this.props.removeFromSelected.bind(_this, topic)
        }
        backgroundColor={isUserSelected ? colors.primary : '#ffffff'}
        onClick={e => {
          _this.onSelectTopics.call(_this, topic, i, e);
        }}
      >
        {topic.topic_label || topic.label || ''}
        {isUserSelected && !topic.data
          ? `${this.props.enableBIA ? '(' + this.state.levelStr[isUserSelected] + ')' : ''}`
          : ``}
        {isUserSelected && !topic.data && this.props.enableBIA && (
          <IconButton
            className="tick-icon"
            ref={'tickIcon'}
            aria-label="edit BIA"
            tooltip={tr('Selected')}
            disableTouchRipple
            tooltipPosition="top-center"
            style={
              this.props.shouldRemove
                ? { ...this.styles.customPlusIcon, ...{ right: '33px' } }
                : this.styles.customPlusIcon
            }
            iconStyle={this.styles.smallIcon}
            onClick={e => {
              _this.props.openBiaModal(e, topic, this.props.taxoName);
            }}
          >
            <EditIcon color={isUserSelected ? '#dddddd' : colors.primary} />
          </IconButton>
        )}
        {this.props.taxoName == this.state.biaModalVisible &&
          !topic.data &&
          this.props.enableBIA && (
            <BIAModal
              key={topicID}
              index={i}
              topic={topic}
              topicLevelObj={topicLevelObj}
              updateLevel={this.updateLevel.bind(this)}
              handleCancelClick={this.handleCancelClick.bind(this)}
            />
          )}
      </Chip>
    );
  }
}
TaxonomyNode.propTypes = {
  biaModalVisible: PropTypes.any,
  isUserSelected: PropTypes.bool,
  topicLevelObj: PropTypes.object,
  closeBIA: PropTypes.func,
  onSelectTopics: PropTypes.func,
  topic: PropTypes.object,
  index: PropTypes.number,
  shouldRemove: PropTypes.bool,
  enableBIA: PropTypes.bool,
  taxoName: PropTypes.any
};

export default connect()(TaxonomyNode);
