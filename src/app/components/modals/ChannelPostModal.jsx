import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import IconButton from 'material-ui/IconButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import CircularProgress from 'material-ui/CircularProgress';
import { openStatusModal } from '../../actions/modalActions';
import { postToChannel } from 'edc-web-sdk/requests/cards';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors/index';
import Checkbox from 'material-ui/Checkbox';
import { getChannels, follow } from 'edc-web-sdk/requests/channels.v2';
import { updateCurrentCard } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import _ from 'lodash';

class ChannelPostModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channels: [],
      selectedChannels: [],
      availableChannels: [],
      alreadyPostedChannels: [],
      pending: true,
      buttonPending: false,
      isAll: false,
      pendingAdd: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };

    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      checkbox: {
        color: colors.primary
      },
      tableScroll: {
        maxHeight: '500px',
        overflowY: 'auto'
      }
    };
  }

  componentDidMount() {
    let alreadyPostedChannels = !!this.props.card.channelIds ? this.props.card.channelIds : [];

    if (!!this.props.card.nonCuratedChannelIds) {
      alreadyPostedChannels = alreadyPostedChannels.concat(this.props.card.nonCuratedChannelIds);
      this.setState({ alreadyPostedChannels });
    }

    getChannels(21, undefined, 'writable', true).then(channels => {
      if (channels && !!channels.length) {
        this.setState({ channels, pending: false, isAll: channels.length < 21 });
      } else {
        getChannels(21).then(channels => {
          this.setState({
            availableChannels: channels,
            pending: false,
            isAll: channels && channels.length < 21
          });
        });
      }
    });
  }

  closeModal = () => {
    let cardModalDiv = document.querySelector('.card-modal-container');
    cardModalDiv && cardModalDiv.classList.toggle('hide');
    this.props.closeModal();
  };

  selectChannel = id => {
    let indexPos = this.state.selectedChannels.indexOf(id);
    let selectedChannels = this.state.selectedChannels;
    if (indexPos > -1) {
      selectedChannels.splice(indexPos, 1);
    } else {
      selectedChannels = selectedChannels.concat(id);
    }
    this.setState({ selectedChannels });
  };

  updateCardChannels = () => {
    let channelIdsToPost = this.state.selectedChannels.concat(this.state.alreadyPostedChannels);
    postToChannel(this.props.card.id, {
      card: {
        channel_ids: channelIdsToPost,
        message: this.props.card.message,
        isOfficial: false
      }
    }).then(channel => {
      this.setState({ buttonPending: false }, () => {
        this.closeModal();
        this.props.dispatch(updateCurrentCard(channel));
        let notShowCuratedMessage =
          channelIdsToPost &&
          channelIdsToPost.length &&
          channel.notRequiredCuration &&
          channel.notRequiredCuration.length &&
          channel.notRequiredCuration.length === channelIdsToPost.length;
        setTimeout(() => {
          if (this.state.newModalAndToast) {
            this.props.dispatch(
              openSnackBar(
                `Content has been posted to the selected channel(s).${
                  notShowCuratedMessage
                    ? ''
                    : ' If it is a curated channel, your post will be visible once approved by the curator.'
                }`
              )
            );
          } else {
            this.props.dispatch(
              openStatusModal(
                `Content has been posted to the selected channel(s).${
                  notShowCuratedMessage
                    ? ''
                    : ' If it is a curated channel, your post will be visible once approved by the curator.'
                }`
              )
            );
          }
        }, 500);
      });
    });
  };

  postClickHandler = () => {
    this.setState({ buttonPending: true });
    if (!this.state.channels.length) {
      Promise.all(
        this.state.selectedChannels.map(item => {
          return new Promise(resolve => {
            return follow(item).then(data => {
              resolve();
            });
          });
        })
      ).then(() => {
        this.updateCardChannels();
      });
    } else {
      this.updateCardChannels();
    }
  };

  cancelClickHandler = () => {
    this.closeModal();
  };

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.checkScroll(node));
    }
  };

  checkScroll = node => {
    let el = ReactDOM.findDOMNode(node);
    let toBottom = el.scrollHeight - el.scrollTop - 500;
    if (!toBottom) {
      this.fetchMore();
    }
  };

  fetchMore() {
    if (!this.state.isAll) {
      this.setState({ pendingAdd: true });

      if (this.state.availableChannels && !!this.state.availableChannels.length) {
        getChannels(10, this.state.availableChannels.length).then(channels => {
          this.setState({
            availableChannels: _.uniqBy(this.state.availableChannels.concat(channels), 'id'),
            pendingAdd: false,
            isAll: channels && channels.length < 10
          });
        });
      } else {
        getChannels(10, this.state.channels.length, 'writable', true).then(channels => {
          this.setState({
            channels: _.uniqBy(this.state.channels.concat(channels), 'id'),
            pendingAdd: false,
            isAll: channels.length < 10
          });
        });
      }
    }
  }

  tableRender = type => {
    return (
      <table className="channel-table">
        <thead>
          <tr>
            <th>{tr('Channel Title')}</th>
          </tr>
        </thead>
        <tbody>
          {!this.state.pending &&
            this.state[type].map(channel => {
              let alreadyPosted = ~this.state.alreadyPostedChannels.indexOf(channel.id);
              return (
                <tr key={channel.id}>
                  <div className="horizontal-spacing-large">
                    <Checkbox
                      label={
                        <span className="channel-name">
                          {channel.label.length < 53
                            ? channel.label
                            : `${channel.label.slice(0, 52)}...`}
                        </span>
                      }
                      onCheck={() => {
                        this.selectChannel(channel.id);
                      }}
                      className="checkbox"
                      style={this.styles.checkbox}
                      checked={alreadyPosted || ~this.state.selectedChannels.indexOf(channel.id)}
                      disabled={alreadyPosted}
                    />
                  </div>
                </tr>
              );
            })}
        </tbody>
      </table>
    );
  };

  render() {
    return (
      <div className="channel-modal">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="modal">
          <div className="modal-title">{tr('Post to Channel(s)')}</div>
          <div className="close close-button">
            <IconButton
              onTouchTap={this.closeModal}
              aria-label="close"
              style={this.styles.closeBtn}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
          <Paper>
            <div className="container-padding">
              <div style={this.styles.tableScroll} ref={this.mountScroll}>
                {!!this.state.channels.length &&
                  !this.state.pending &&
                  this.tableRender('channels')}
                {!this.state.channels.length &&
                  !this.state.pending &&
                  !!this.state.availableChannels.length && (
                    <div>
                      <div className="message-empty-channel text-center">
                        <div>
                          {tr('Please select a channel below to follow and post the content')}
                        </div>
                      </div>
                      {this.tableRender('availableChannels')}
                    </div>
                  )}
                {!this.state.channels.length &&
                  !this.state.pending &&
                  !this.state.availableChannels.length && (
                    <div className="message-empty-channel text-center">
                      <div className="data-not-available-msg">
                        {tr("Sorry, you don't have any available channels")}
                      </div>
                    </div>
                  )}
                {this.state.pendingAdd && (
                  <div className="text-center">
                    <CircularProgress size={30} />
                  </div>
                )}
              </div>
              {this.state.pending && (
                <div className="text-center">
                  <CircularProgress size={60} />
                </div>
              )}
              {!this.state.pending && (
                <div className="text-center">
                  <SecondaryButton
                    label={tr('Cancel')}
                    className="close"
                    onTouchTap={this.cancelClickHandler}
                  />
                  {(!!this.state.channels.length || !!this.state.availableChannels.length) && (
                    <PrimaryButton
                      label={tr('Post')}
                      className="create"
                      onTouchTap={this.postClickHandler}
                      pending={this.state.buttonPending}
                      pendingLabel={tr('Posting...')}
                      disabled={this.state.selectedChannels.length === 0}
                    />
                  )}
                </div>
              )}
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

ChannelPostModal.propTypes = {
  cardId: PropTypes.number,
  closeModal: PropTypes.func
};

export default connect(mapStoreStateToProps)(ChannelPostModal);
