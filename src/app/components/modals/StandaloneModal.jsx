import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import Spinner from '../common/spinner';
import { usersv2 } from 'edc-web-sdk/requests/index';
import TopNavContainer from '../TopNav/TopNavContainer.jsx';
import FooterContainer from '../footer/FooterContainer';
import InsightContainer from '../insight/InsightContainer.jsx';
const JourneyDetailsContainer = Loadable({
  loader: () => import('../journey/JourneyDetailsContainer'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});
const JourneyDetailsContainerV2 = Loadable({
  loader: () => import('../journey/JourneyDetailsContainerV2'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});
const PathwayDetailsContainer = Loadable({
  loader: () => import('../pathway/PathwayDetailsContainer'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});
import * as actionTypes from '../../constants/actionTypes';

class StandaloneModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: false
    };
    this.requireOnboarding = this.requireOnboarding.bind(this);
    this.journeyEnhancement = window.ldclient.variation('journey-enhancement', false);
    this.cardV3 = window.ldclient.variation('card-v3', false);
  }

  requireOnboarding(nextState, replace, callback) {
    usersv2
      .getOnboardingState()
      .then(onboardingState => {
        this.props.dispatch({
          type: actionTypes.RECEIVE_INIT_ONBOARDING_STATE,
          onboardingState
        });
        if (!onboardingState.onboarding_completed) {
          let url = this.onboardingMicroservice ? '/onboard/v2' : '/onboarding';
          window.location = url;
        }
        callback();
      })
      .catch(err => {
        console.error(`Error in StandaloneModal.getOnboardingState.func : ${err}`);
      });
  }

  render() {
    return (
      <div className="stand-alone-overview">
        <TopNavContainer isModal={true}>
          <FooterContainer>
            {this.state.pending ? (
              <div className="text-center pending-cards">
                <Spinner />
              </div>
            ) : (
              <div>
                {this.props.cardType === 'journey' ? (
                  this.journeyEnhancement ? (
                    <JourneyDetailsContainerV2
                      routeParams={{ splat: this.props.card.slug }}
                      isStandaloneModal={true}
                      cardUpdated={this.props.cardUpdated}
                    />
                  ) : (
                    <JourneyDetailsContainer
                      routeParams={{ splat: this.props.card.slug }}
                      isStandaloneModal={true}
                      cardUpdated={this.props.cardUpdated}
                    />
                  )
                ) : this.props.cardType === 'pathways' ? (
                  <PathwayDetailsContainer
                    routeParams={{ splat: this.props.card.slug }}
                    isStandaloneModal={true}
                    cardUpdated={this.props.cardUpdated}
                  />
                ) : (
                  <InsightContainer
                    routeParams={{ splat: this.props.card.slug }}
                    isStandaloneModal={true}
                    dataCard={this.props.dataCard}
                    cardUpdated={this.props.cardUpdated}
                  />
                )}
              </div>
            )}
          </FooterContainer>
        </TopNavContainer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.currentUser.get('isLoggedIn'),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS()
  };
}

StandaloneModal.propTypes = {
  cardType: PropTypes.string,
  card: PropTypes.object,
  dataCard: PropTypes.object,
  cardUpdated: PropTypes.func
};

export default connect(mapStateToProps)(StandaloneModal);
