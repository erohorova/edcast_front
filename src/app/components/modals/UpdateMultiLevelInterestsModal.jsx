import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import find from 'lodash/find';
import uniqBy from 'lodash/uniqBy';
import { close } from '../../actions/modalActions';
import { updateUserInterest, updateUserExpertise } from '../../actions/usersActions';
import Divider from 'material-ui/Divider';
import FlashAlert from '../common/FlashAlert';
import { tr } from 'edc-web-sdk/helpers/translations';
import MultilevelTaxonomyContainer from './MultilevelTaxonomyContainer';
import BIAModal from './BIAModal';
import TaxonomyNode from './TaxonomyNode';
import TextField from 'material-ui/TextField';
class UpdateMultilevelInterestsModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showAlert: false,
      errorMessage: '',
      postPending: false,
      biaModalVisible: false,
      showSubTopics: false,
      interestsLimit:
        (this.props.team &&
          this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options[this.props.topicLimit]) ||
        3,
      selectedTopics:
        (this.props.currentUser &&
          this.props.currentUser.profile &&
          this.props.currentUser.profile[this.props.profileTopic] &&
          this.props.currentUser.profile[this.props.profileTopic].slice()) ||
        [],
      topicLevelObj: {}
    };

    this.showBIA = !!this.props.team.config.enabled_bia;

    this.isLearningTopics = this.props.profileTopic === 'learningTopics';
    this.enableBIA = this.isLearningTopics && this.showBIA;
    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      }
    };
  }

  componentDidMount = () => {
    let selectedTopics =
      (this.props.currentUser &&
        this.props.currentUser.profile &&
        this.props.currentUser.profile[this.props.profileTopic].slice()) ||
      [];
    let topicLevelObj = this.getTopicLevelObj(selectedTopics);
    this.setState({ selectedTopics, topicLevelObj });
  };

  getTopicLevelObj = selectedTopics => {
    let topicObj = {};
    let topicLevelObj =
      selectedTopics &&
      selectedTopics.length > 0 &&
      selectedTopics.map(topic => {
        topicObj[topic.topic_id] = topic.level ? topic.level : 1;
      });
    return topicObj || {};
  };

  removeAlert = () => {
    this.setState({ showAlert: false });
  };

  handleClickUpdate() {
    let _this = this;
    let selectedTopics = this.state.selectedTopics;
    let msg = this.isLearningTopics
      ? 'You should have at least 1 Job role selected.'
      : 'You should have at least 1 expertise selected.';
    this.setState({ postPending: true });
    if (this.state.selectedTopics == 0) {
      this.setState({ showAlert: true, errorMessage: tr(msg), postPending: false });
      return;
    }
    let profileAction = this.isLearningTopics ? updateUserInterest : updateUserExpertise;

    for (let skill of selectedTopics) {
      if (!this.showBIA && skill.level) {
        delete skill.level;
      }
    }
    this.props
      .dispatch(
        profileAction(
          selectedTopics,
          _this.props.currentUser.id,
          _this.props.currentUser.profile.id
        )
      )
      .then(response => {
        _this.setState({ postPending: false });
        _this.props.dispatch(close());
      })
      .catch(err => {
        console.error(`Error in UpdateMultiLevelInterestsModal.profileAction.func: ${err}`);
      });
  }

  openBiaModal(e, topic, type) {
    e.stopPropagation();
    this.setState({
      biaModalVisible: type
    });
  }

  closeBIA = () => {
    this.setState({
      biaModalVisible: false
    });
  };

  handleClickCancel = () => {
    this.props.dispatch(close());
  };

  onSelectTopics = (topic, i, e) => {
    if (topic.data) {
      this.setState({
        showSubTopics: true,
        subTopics: topic.data,
        targetPosition: e.currentTarget
      });
    } else {
      this.addOrUpdateSelectedTopics(topic);
    }
  };

  addOrUpdateSelectedTopics = topic => {
    let domainNode = this.props.onboardingv3.domainNode;
    let topicObj = {
      topic_name: topic.topic_name || topic.path,
      topic_label: topic.topic_label || topic.label,
      topic_id: topic.topic_id || topic.id,
      domain_name: domainNode.path,
      domain_id: domainNode.id,
      domain_label: domainNode.label
    };
    if (this.showBIA) {
      topicObj.level = topic.level;
    }
    let selectedTopics = this.state.selectedTopics;
    if (find(selectedTopics, ['topic_id', topicObj.topic_id])) {
      this.updateSelectedTopics(topicObj);
      return;
    }
    let msg = this.isLearningTopics ? 'job role' : 'expertise';
    let interestsLimit = this.state.interestsLimit;
    let interestsLimitMsg =
      interestsLimit > 1 ? `${interestsLimit} ${tr(msg + 's')}` : tr('one ' + msg);
    let interestsLimitDiff = selectedTopics.length - interestsLimit;
    let interestsLimitDiffMsg =
      interestsLimitDiff > 1 ? `${interestsLimitDiff} ${tr(msg)}` : tr('one ' + msg);
    if (selectedTopics.length >= interestsLimit) {
      this.setState({
        showAlert: true,
        errorMessage: `${tr("You can select a maximum of only %{count}. Please deselect %{limit} and update", {count: interestsLimitMsg, limit: interestsLimitDiffMsg})}`
      });
      return;
    }

    this.setSelectedTopics(selectedTopics, topicObj);
  };

  updateSelectedTopics = topicObj => {
    let selectedTopics = this.state.selectedTopics;
    selectedTopics = selectedTopics.map(topic => {
      if (topic.topic_id == topicObj.topic_id) {
        return topicObj;
      } else {
        return topic;
      }
    });
    this.setSelectedTopics(selectedTopics);
  };

  setSelectedTopics = (selectedTopics, topicObj) => {
    let selected = selectedTopics;
    if (topicObj) {
      selected.push(topicObj);
      selected = uniqBy(selected, 'topic_id');
    }

    let topicLevelObj = this.getTopicLevelObj(selected);
    this.setState({ selectedTopics: selected, topicLevelObj });
  };

  removeFromSelected = topic => {
    let selectedTopics = this.state.selectedTopics;
    selectedTopics = selectedTopics.filter(obj => {
      if (!(obj.topic_id === topic.topic_id)) {
        return obj;
      }
    });

    let topicLevelObj = this.getTopicLevelObj(selectedTopics);
    this.setState({ selectedTopics, topicLevelObj });
  };

  render() {
    let _this = this;
    let selectedTopics = this.state.selectedTopics;
    let topicLevelObj = this.state.topicLevelObj;
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let isLearningTopics = this.isLearningTopics;
    let profileTopicLabel = isLearningTopics ? tr('UPDATE LEARNING GOALS') : tr('UPDATE EXPERTISE');
    let customLabelPath = isLearningTopics ? 'web/labels/interests' : 'web/labels/expertise';
    let multiLevelTaxonomy = this.props.onboardingv3 && this.props.onboardingv3.multiLevelTaxonomy;
    let interestsLimit = this.state.interestsLimit;
    if (labels) {
      let interestsLabelFlag = labels[customLabelPath] && labels[customLabelPath].label.length > 0;
      profileTopicLabel = `${tr('UPDATE')} ${
        interestsLabelFlag
          ? tr(labels[customLabelPath].label)
          : isLearningTopics
          ? tr('Learning Goals')
          : tr('Expertise')
      }`;
    }
    return (
      <div className="vertical-spacing-large update-learning-goals">
        {this.state.showAlert && (
          <FlashAlert
            message={this.state.errorMessage}
            toShow={this.state.showAlert}
            removeAlert={this.removeAlert}
          />
        )}
        <h5 style={{ marginBottom: '0px' }}>{profileTopicLabel.toUpperCase()}</h5>
        <div className="row">
          <TextField className="hiddenTextField" autoFocus={true} name="updatemultilevel" />
          <div className="small-12 columns text-center">
            <div style={this.styles.chipsWrapper}>
              {selectedTopics &&
                selectedTopics.map((topic, i) => {
                  let topicID = topic.id || topic.topic_id;
                  return (
                    <TaxonomyNode
                      index={i}
                      key={topicID}
                      taxoName={'selected_' + topicID}
                      topic={topic}
                      shouldRemove={true}
                      removeFromSelected={_this.removeFromSelected.bind(_this)}
                      topicLevelObj={topicLevelObj}
                      isUserSelected={true}
                      closeBIA={_this.closeBIA.bind(_this)}
                      openBiaModal={_this.openBiaModal.bind(_this)}
                      onSelectTopics={_this.onSelectTopics.bind(_this)}
                      enableBIA={_this.enableBIA}
                      biaModalVisible={_this.state.biaModalVisible}
                    />
                  );
                })}
            </div>
          </div>
        </div>

        {selectedTopics && selectedTopics.length > 0 && <Divider />}

        <div>
          {`Select ${interestsLimit} ${
            this.isLearningTopics ? 'Job role of your interest.' : 'expertise'
          } `}{' '}
        </div>
        <div className="small-12 columns text-center">
          <div style={this.styles.chipsWrapper}>
            {multiLevelTaxonomy &&
              multiLevelTaxonomy.map((topic, i) => {
                return (
                  <TaxonomyNode
                    index={i}
                    key={topic.id}
                    topic={topic}
                    taxoName={'taxoNode_' + topic.id}
                    topicLevelObj={topicLevelObj}
                    closeBIA={_this.closeBIA.bind(_this)}
                    openBiaModal={_this.openBiaModal.bind(_this)}
                    onSelectTopics={_this.onSelectTopics.bind(_this)}
                    enableBIA={_this.enableBIA}
                    biaModalVisible={_this.state.biaModalVisible}
                  />
                );
              })}
          </div>
        </div>
        {this.state.showSubTopics && (
          <MultilevelTaxonomyContainer
            openBiaModal={this.openBiaModal.bind(this)}
            onSelectTopics={this.onSelectTopics.bind(this)}
            subTopics={this.state.subTopics}
            topicLevelObj={topicLevelObj}
            targetPosition={this.state.targetPosition}
            closeBIA={this.closeBIA.bind(_this)}
            enableBIA={this.enableBIA}
            biaModalVisible={this.state.biaModalVisible}
          />
        )}

        <div style={{ padding: '8px' }} className="clearfix">
          <PrimaryButton
            label={tr('Update')}
            className="float-right create"
            pendingLabel={tr('Updating...')}
            pending={this.state.postPending}
            onTouchTap={this.handleClickUpdate.bind(this)}
          />
          <SecondaryButton
            label={tr('Cancel')}
            className="float-right close"
            onTouchTap={this.handleClickCancel.bind(this)}
          />
        </div>
      </div>
    );
  }
}

UpdateMultilevelInterestsModal.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  onboardingv3: PropTypes.object,
  topicLimit: PropTypes.string,
  profileTopic: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS(),
    onboardingv3: state.onboardingv3.toJS()
  };
}

export default connect(mapStoreStateToProps)(UpdateMultilevelInterestsModal);
