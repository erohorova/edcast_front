import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import { push, replace } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactTags from 'react-tag-autocomplete';
import _ from 'lodash';
import Select from 'react-select';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';
import List from 'material-ui/List';
import ListItem from 'material-ui/List/ListItem';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import {
  getNonChannelCurators,
  getNonChannelCollaborators,
  getTrustedCollaborators,
  addTopicsList,
  addEclSourceList,
  deleteTopics,
  deleteElSources,
  saveChannelTunningEclSources,
  saveChannelProgramming
} from 'edc-web-sdk/requests/channels.v2';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import * as channelsSDK from 'edc-web-sdk/requests/channels';
import * as eclSDK from 'edc-web-sdk/requests/ecl';
import * as topicsSdk from 'edc-web-sdk/requests/topics';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';
import { fileStackSources } from '../../constants/fileStackSource';

import { filestackClient } from '../../utils/filestack';

import * as actions from '../../actions/channelsActions';
import { getChannelSources } from '../../actions/eclActions';
import TextField from 'material-ui/TextField';
import debounce from 'lodash/debounce';
import { languageKeyVal } from './../../constants/languages';
import { snackBarOpenClose } from '../../actions/channelsActionsV2';
import ChannelTunning from '../channel/edit/ChannelTunning';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';
const viewColor = '#454560';

export const contentTypes = [
  {
    name: 'video',
    id: 'video'
  },
  {
    name: 'poll',
    id: 'poll'
  },
  {
    name: 'insight',
    id: 'insight'
  },
  {
    name: 'pathway',
    id: 'pathway'
  },
  {
    name: 'article',
    id: 'article'
  },
  {
    name: 'course',
    id: 'course'
  },
  {
    name: 'Video Stream',
    id: 'video_stream'
  },
  {
    name: 'journey',
    id: 'journey'
  },
  {
    name: 'project',
    id: 'project'
  }
];

class ChannelEditorModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      paper: {
        padding: '1rem'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      mainImage: {
        height: '2rem',
        width: '2rem',
        marginTop: '4.625rem',
        fill: darkPurp
      },
      checkboxLabelStyle: {
        fontSize: '0.75rem',
        lineHeight: 2,
        color: '#6f708b'
      },
      cancel: {
        color: '#6f708b',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px 12px 4px 4px',
        opacity: '0.6',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      actionBtnLabel: {
        textTransform: 'none',
        fontFamily: 'OpenSans-Regular, Open Sans !important',
        fontSize: '0.75rem'
      },
      tagIconStyles: {
        height: '0.75rem',
        width: '1rem'
      },
      groupIconStyles: {
        height: '0.75rem',
        width: '1.7rem'
      },
      groupIconStylesBig: {
        height: '0.75rem',
        width: '1.7rem',
        marginRight: '1rem'
      },
      tagIconStylesBig: {
        height: '2.3rem',
        width: '1.5rem',
        marginRight: '1rem'
      },
      closeIconStyle: {
        height: '1.125rem',
        width: '1.125rem'
      },
      addSource: {
        margin: '0 0 0 0.25rem',
        height: '2.06275rem',
        fontSize: '0.875rem'
      },
      eclSourcesCheckbox: {
        zIndex: '0'
      },
      removeBtn: {
        padding: '0',
        marginTop: '.75rem'
      },
      channelImg: { background: 'none' },
      hintText: {
        paddingLeft: '2.5rem',
        lineHeight: '1'
      },
      checkOff: {
        width: '1.15rem',
        height: '1.15rem',
        border: '.0625rem solid #acadc1',
        borderRadius: '.125rem',
        boxSizing: 'border-box',
        margin: '0.2rem 0 0 0.19rem'
      }
    };
    this.state = {
      allowTrustedCollaborators:
        window.ldclient.variation('trusted-collaborator-setting', false) &&
        props.team.config &&
        props.team.config['trusted_collaborators_enabled'],
      showAdvancedSettings: false,
      titleLabel: 'Channel Setup',
      activeTab: 'settings',
      label: '',
      description: '',
      imageInit: '',
      profileImageUrl: '',
      curateOnly: false,
      curateUgc: false,
      onlyAuthorsCanPost: true,
      channelSources: [],
      selectedSources: [],
      step: 1,
      id: null,
      limit: 10,
      channelTopics: [],
      channelTopicsIds: [],
      allTopics: [],
      allSkills: {},
      channelCollaborators: [],
      channelSourcesIds: [],
      updatingSources: false,
      isCreating: false,
      showFollowModal: false,
      isPrivate: false,
      isOpen: false,
      allowFollow: true,
      showMainImage: false, //!!props.pathway.filestack.length;,
      channel: this.props.channel
        ? this.props.channel
        : {
            label: '',
            description: ''
          },
      publishLabel: this.props.channel ? 'Update Channel' : 'Create Channel',
      curatorSuggestionsLimit: 10,
      curatorSuggestionsOffset: 0,
      curatorOptions: [],
      collaboratorOptions: [],
      trustedCollaboratorOptions: [],
      curatorsCurrent: [],
      collaboratorsCurrent: [],
      trustedCollaboratorsCurrent: [],
      topicsCurrent: [],
      eclSourcesCurrent: [],
      contentTypesCurrent: [],
      languagesCurrent: [],
      curatorsRemove: [],
      collaboratorsRemove: [],
      trustedCollaboratorsRemove: [],
      topicsRemove: [],
      eclSourcesRemove: [],
      contentTypesRemove: [],
      languagesRemove: [],
      errorMessage: '',
      isOpenViewMore: false,
      newTags: [],
      moreItemsRemove: [],
      moreItems: [],
      isLastPage: false,
      curatorsOffset: 0,
      collaboratorsOffset: 0,
      trustedCollaboratorsOffset: 0,
      sourcesSuggestions: [],
      sourcesToAdd: [],
      sourcesToRemove: [],
      topicsToAdd: [],
      topicsToRemove: [],
      skillsSelectKey: 0,
      showTuningTab: window.ldclient.variation('channel-tuning-tab', false),
      loadingTopics: true,
      selectedContentTypes: [],
      selectedLanguages: [],
      ecl_source: [],
      showRequiredTopic: false,
      allSources: [],
      sourcesLoading: true,
      channelTopicFilter: [],
      channelProgrammingV2: this.props.team.config.channel_programming_v2,
      openSourceId: '1d97649b-7969-4111-8e82-b770f942bcb3',
      savingChannel: false
    };
  }

  componentDidMount() {
    if (this.props.channel) {
      let channelId = this.props.channel.id;
      this.getChannel(channelId);
    }
  }

  getChannel = (channelId, fromCarousels) => {
    channelsSDK
      .getChannel(channelId)
      .then(data => {
        if (fromCarousels) {
          this.setState({
            channel: data,
            id: data.id
          });
        } else {
          this.setState({
            label: data.label,
            image: data.image,
            imageInit: data.bannerImageUrl,
            profileImageUrl: data.profileImageUrl,
            id: data.id,
            curateOnly: data.curateOnly,
            curateUgc: data.curateUgc,
            eclEnabled: data.eclEnabled,
            onlyAuthorsCanPost: data.onlyAuthorsCanPost,
            channel: data,
            description: data.description,
            isPrivate: data.isPrivate,
            isOpen: data.isOpen, // we don't have this field now
            allowFollow: data.allowFollow,
            contentTypesCurrent: contentTypes.filter(content => {
              if (!!data.contentType.length) {
                const index = data.contentType.findIndex(contentType => content.id === contentType);
                if (index !== -1) {
                  return content;
                }
              }
            }),
            languagesCurrent: languageKeyVal.filter(content => {
              if (!!data.language.length) {
                const index = data.language.findIndex(contentType => content.id === contentType);
                if (index !== -1) {
                  return content;
                }
              }
            })
          });
          this.getChannelCurators(channelId);
          this.getChannelCollaborators(channelId);
          this.getChannelTrustedCollaborators(channelId);
          this.getChannelTopics(channelId);
          this.getEclChannelSources(channelId);
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getChannel.func : ${err}`);
      });
  };

  getEclChannelSources(channelId) {
    getChannelSources(channelId)
      .then(channelSources => {
        this.setState(
          {
            updatingSources: true,
            sourcesLoading: true
          },
          () => {
            let channelSourcesIds = [];
            channelSources.map(source => {
              channelSourcesIds.push(source.eclSourceId);
            });
            eclSDK
              .getAllSources({ is_enabled: true })
              .then(data => {
                let suggs = [];
                let optionObj;
                data.map(item => {
                  if (channelSourcesIds.indexOf(item.id) === -1) {
                    optionObj = { id: item.id, name: item.display_name };
                    suggs.push(optionObj);
                  }
                });
                if (channelSourcesIds.indexOf(this.state.openSourceId) === -1) {
                  suggs.push({
                    name: 'Open Source',
                    id: this.state.openSourceId
                  });
                }
                this.setState({ allSources: suggs, sourcesLoading: false });
              })
              .catch(err => {
                this.setState({ sourcesLoading: false });
                console.error(`Error in ChannelEditModal.eclSDK.getAllSources.func : ${err}`);
              });
            this.setState({
              channelSources: channelSources,
              channelSourcesIds: channelSourcesIds,
              updatingSources: false
            });
          }
        );
      })
      .catch(err => {
        console.error(
          `Error in ChannelEditModal.getEclChannelSources.getEclChannelSources.func : ${err}`
        );
      });
  }

  openViewMore = (e, type) => {
    e.stopPropagation();
    let hintText = '';
    let moreItems = this.state[`${type}sCurrent`];
    switch (type) {
      case 'collaborator':
        hintText = 'Add Collaborators, Collaborators can post to this channel';
        break;
      case 'trustedCollaborator':
        hintText = 'Add Trusted Collaborators, Trusted Collaborators can post to this channel';
        break;
      case 'curator':
        hintText =
          'Add Curators, Curators can configure, curate, and post to this channel(unless role is modified by admin under RBAC settings)';
        break;
      default:
        break;
    }
    this.setState({ isOpenViewMore: true, viewMoreType: type, hintText, moreItems }, () => {
      this.loadViewMore(type);
    });
  };

  selectedRestrictedToTags(type, tag) {
    let moreCount = this.state[`${type}sMore`];
    return tag.tag.id === 'vm' ? (
      <a
        className="react-tags_link"
        key={`tag-${type}-${tag.tag.id}`}
        onClick={event => this.openViewMore(event, type)}
      >
        +{moreCount || 0} {tr('more')}
      </a>
    ) : (
      <span
        key={`tag-${type}-${tag.tag.id}`}
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={
          this.state.isOpenViewMore
            ? this.handleMoreDelete.bind(this, tag.tag)
            : this.handleDelete.bind(this, type, tag.tag)
        }
      >
        {(type === 'curator' || type === 'collaborator' || type === 'trustedCollaborator') && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  }

  viewMoreTag(type, tag, index) {
    return (
      <div className="items-row" key={`tag-view-more-${type}-${tag.id}`}>
        {(type === 'curator' || type === 'collaborator' || type === 'trustedCollaborator') && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStylesBig} />
        )}
        {tag.name}
        <button
          className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
          onClick={this.handleOldDelete.bind(this, type, index)}
        >
          <CloseIcon style={this.styles.closeIconStyle} color="black" />
          <span className="tooltiptext">{tr('Remove item')}</span>
        </button>
      </div>
    );
  }

  getChannelCurators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.curatorsTotal : this.state.limit,
      offset: this.state.curatorsOffset
    };

    channelsSDK
      .getCurators(channelId, payload, true)
      .then(data => {
        let curators = data.curators !== undefined ? data.curators : [];
        let curatorsOffset = this.state.curatorsOffset + curators.length;
        let curatorsList = _.uniqBy(this.state.curatorsCurrent.concat(curators), 'id');
        let isLastPage = data.total < curatorsList.length;
        let curatorsMore = data.total - curatorsList.length;
        if (data.total > curatorsList.length) {
          curatorsList.push({ id: 'vm', name: 'view more' });
        }
        if (loadMore) {
          this.setState({
            moreItems: curatorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            curatorsCurrent: curatorsList,
            curatorsTotal: data.total,
            curatorsMore,
            isLastPage,
            curatorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getCurators.func : ${err}`);
      });
  }

  getChannelCollaborators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.collaboratorsTotal : this.state.limit,
      offset: this.state.collaboratorsOffset
    };

    channelsSDK
      .getCollaborators(channelId, payload, true)
      .then(data => {
        let collaborators = data.authors;
        let collaboratorsOffset = this.state.collaboratorsOffset + collaborators.length;
        let collaboratorsList = _.uniqBy(
          this.state.collaboratorsCurrent.concat(collaborators),
          'id'
        );
        let isLastPage = data.total <= collaboratorsList.length;
        let collaboratorsMore = data.total - collaboratorsList.length;
        if (data.total > collaboratorsList.length) {
          collaboratorsList.push({ id: 'vm', name: 'view more' });
        }

        if (loadMore) {
          this.setState({
            moreItems: collaboratorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            collaboratorsCurrent: collaboratorsList,
            collaboratorsTotal: data.total,
            collaboratorsMore,
            isLastPage,
            collaboratorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getCollaborators.func : ${err}`);
      });
  }

  getChannelTrustedCollaborators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.trustedCollaboratorsTotal : this.state.limit,
      offset: this.state.collaboratorsOffset
    };

    getTrustedCollaborators(channelId, payload, true)
      .then(data => {
        let trustedCollaborators = data.trustedCollaborators;
        let trustedCollaboratorsOffset =
          this.state.trustedCollaboratorsOffset + trustedCollaborators.length;
        let collaboratorsList = _.uniqBy(
          this.state.trustedCollaboratorsCurrent.concat(trustedCollaborators),
          'id'
        );
        let isLastPage = data.total <= collaboratorsList.length;
        let trustedCollaboratorsMore = data.total - collaboratorsList.length;
        if (data.total > collaboratorsList.length) {
          collaboratorsList.push({ id: 'vm', name: 'view more' });
        }

        if (loadMore) {
          this.setState({
            moreItems: collaboratorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            trustedCollaboratorsCurrent: collaboratorsList,
            trustedCollaboratorsTotal: data.total,
            trustedCollaboratorsMore,
            isLastPage,
            trustedCollaboratorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getCollaborators.func : ${err}`);
      });
  }

  getChannelTopics(channelId) {
    channelsSDK
      .getTopics(channelId)
      .then(async data => {
        let channelTopics = (data && data.topics) || [];
        const channelTopicsIds = channelTopics.map(item => item.id);
        const queryData = await topicsSdk.getQueryTopics('');
        if (queryData.response === 'success') {
          let allTopics = [];
          queryData.topics.forEach(topic => {
            if (channelTopicsIds.indexOf(topic.id) === -1) {
              allTopics.push({ ...topic, id: topic.id, fullPath: topic.name, name: topic.label });
            }
          });
          this.setState(() => ({
            allTopics,
            loadingTopics: false
          }));
        } else {
          console.error(`Error in ChannelEditModal.topicsSdk.getQueryTopics.func`);
        }
        this.setState({
          channelTopics,
          channelTopicsIds: channelTopics.map(item => item.id) || []
        });
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getTopics.func : ${err}`);
      });
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  changeActiveTab = tabName => {
    if (tabName === 'tuning' && !this.state.channel.id) {
      eclSDK
        .getAllSources({ is_enabled: true })
        .then(result => {
          let sourcesSuggestions =
            result.data &&
            result.data
              .filter(item => ~this.state.channelSourcesIds.indexOf(item.id))
              .map(source => ({
                name: source.name,
                id: source.id,
                label: 'source'
              }));
          this.setState({ sourcesSuggestions });
        })
        .catch(err => {
          console.error(`Error in ChannelEditModal.changeActiveTab.func : ${err}`);
        });
      return;
    }
    this.setState({ activeTab: tabName });
  };

  handleProfileImageChange(image) {
    if (image.filesUploaded && image.filesUploaded.length) {
      let url = image.filesUploaded[0].url;
      let securedUrl;
      refreshFilestackUrl(url)
        .then(resp => {
          securedUrl = resp.signed_url;
          this.setState({ profileImageUrl: url, securedUrl: securedUrl });
        })
        .catch(err => {
          console.error(`Error in ChannelEditModal.handleProfileImageChange.func : ${err}`);
        });
    }
  }

  fileStackHandler = callback => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: ['image/*'],
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(fileStack => {
            callback(fileStack);
          })
          .catch(err => {
            console.error(`Error in ChannelEditModal.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(
          `Error in ChannelEditModal.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton aria-label={tr('Change Image')} style={this.styles.imageBtn}>
          <ImagePhotoCamera color="white" />
        </IconButton>
        <div className="roll-text">{tr('Change Image')}</div>
      </span>
    );
  }

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        showAdvancedSettings: !prevState.showAdvancedSettings
      };
    });
  };

  getCuratorUser = input => {
    let isChanged = input !== this.state.searchCuratorText;
    this.setState(
      {
        searchCuratorText: input,
        curatorOptions: [],
        curatorSuggestionsOffset: !isChanged
          ? this.state.curatorSuggestionsOffset + this.state.limit
          : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCurators(this.state.channel.id, {
            search_term: input,
            limit: this.state.limit,
            offset: this.state.curatorSuggestionsOffset || 0
          })
            .then(data => {
              let curatorOptions = data.notCurators
                .filter(
                  item => !~this.state.curatorsCurrent.findIndex(curator => curator.id == item.id)
                )
                .map(item => {
                  let label = item.firstName ? item.firstName + ' ' : '';
                  label += item.lastName ? item.lastName : '';
                  label += `( ${item.email} )`;
                  return { id: item.id, name: label };
                });

              this.setState({
                curatorOptions
              });
            })
            .catch(err => {
              console.error(`Error in ChannelEditModal.getNonChannelCurators.func : ${err}`);
            });
        } else {
          this.handleInputChange(input, 'curator');
        }
      }
    );
  };

  handleInputChange(query, itemsType) {
    let payload = {
      limit: this.state[`${itemsType}SuggestionsLimit`] || 10,
      offset: this.state[`${itemsType}SuggestionsOffset`] || 0,
      q: query
    };
    usersSDK
      .getItems(payload)
      .then(members => {
        if (members && members.items) {
          let options = members.items
            .filter(
              item =>
                !~this.state[`${itemsType}sCurrent`].findIndex(curator => curator.id == item.id)
            )
            .map(item => {
              let label = item.firstName ? item.firstName + ' ' : '';
              label += item.lastName ? item.lastName : '';
              label += `( ${item.email} )`;
              return { id: item.id, name: label };
            });
          this.setState({
            [`${itemsType}Options`]: options
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditor.usersSDK.getItems.func : ${err}`);
      });
  }

  handleDelete = (itemsType, tag) => {
    let itemType = itemsType;
    const current = this.state[`${itemType}sCurrent`].slice(0);
    const itemsRemove = this.state[`${itemType}sRemove`];
    let index = _.findIndex(current, function(item) {
      return item.id === tag.id;
    });
    let remove = current.splice(index, 1);
    if (remove[0].id === 'vm') {
      return;
    }
    if (remove[0] && !remove[0].isNew) {
      let name = remove[0].firstName ? remove[0].firstName + ' ' : '';
      name += remove[0].lastName ? remove[0].lastName : '';
      name += `(${remove[0].email})`;
      itemsRemove.push({ id: remove[0].id, name: name });
    }
    this.setState({ [`${itemType}sCurrent`]: current, [`${itemType}sRemove`]: itemsRemove });
  };

  handleDeleteByIds = (itemsType, tagId) => {
    const current = this.state[`${itemsType}sCurrent`].slice(0);
    current.splice(tagId, 1);
    this.setState({ [`${itemsType}sCurrent`]: current });
  };

  handleAddition(itemsType, tag) {
    let itemType = itemsType;
    let itemsRemove = this.state[`${itemType}sRemove`];
    let isOld = _.remove(itemsRemove, function(item) {
      return item.id.toString() === tag.id.toString();
    });

    if (isOld.length === 0) {
      tag.isNew = true;
    }
    let current = this.state[`${itemType}sCurrent`];
    current.push(tag);

    this.setState({ [`${itemType}sCurrent`]: current, [`${itemType}sRemove`]: itemsRemove });
  }

  getCollaboratorUsers = (input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchUserText;
    this.setState(
      {
        isLoadingCollaborator: true,
        searchUserText: input,
        collaboratorOptions: [],
        userSuggestionsOffset:
          isScrollToBottom && !isChanged
            ? this.state.collaboratorOptions + this.state.collaboratorOptions
            : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCollaborators(this.state.channel.id, {
            search_term: input,
            limit: 10,
            offset: this.state.userSuggestionsOffset || 0
          })
            .then(data => {
              let currentOptions = this.state.collaboratorOptions;
              let collaboratorOptions = data.notAuthors
                .filter(
                  item =>
                    !~this.state.collaboratorsCurrent.findIndex(
                      collaborator => collaborator.id == item.id
                    )
                )
                .map(item => {
                  let label = item.firstName ? item.firstName + ' ' : '';
                  label += item.lastName ? item.lastName : '';
                  label += `( ${item.email} )`;
                  return { id: item.id, name: label, label: 'member' };
                });
              collaboratorOptions = isScrollToBottom
                ? currentOptions.concat(collaboratorOptions)
                : collaboratorOptions;

              this.setState({
                collaboratorOptions,
                isLoadingCollaborator: false
              });
            })
            .catch(err => {
              console.error(
                `Error in ChannelEditor.getNonChannelCollaborators.getItems.func : ${err}`
              );
            });
        } else {
          this.handleInputChange(input, 'collaborator');
        }
      }
    );
  };

  getTrustedCollaboratorUsers = (input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchTrustedUserText;
    this.setState(
      {
        isLoadingTrustedCollaborator: true,
        searchTrustedUserText: input,
        trustedCollaboratorOptions: [],
        trustedUserSuggestionsOffset:
          isScrollToBottom && !isChanged
            ? this.state.trustedCollaboratorOptions + this.state.trustedCollaboratorOptions
            : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCollaborators(this.state.channel.id, {
            search_term: input,
            limit: 10,
            offset: this.state.trustedUserSuggestionsOffset || 0,
            trusted: true
          })
            .then(data => {
              let currentOptions = this.state.trustedCollaboratorOptions;
              let trustedCollaboratorOptions = data.notAuthors
                .filter(
                  item =>
                    !~this.state.trustedCollaboratorsCurrent.findIndex(
                      collaborator => collaborator.id == item.id
                    )
                )
                .map(item => {
                  let label = `${item.firstName ? `${item.firstName} ` : ''} ${item.lastName ||
                    ''}${item.email ? `( ${item.email || ''} )` : ''}`;
                  return { id: item.id, name: label, label: 'trustedCollaborator' };
                });
              trustedCollaboratorOptions = isScrollToBottom
                ? currentOptions.concat(trustedCollaboratorOptions)
                : trustedCollaboratorOptions;
              this.setState({
                trustedCollaboratorOptions,
                isLoadingTrustedCollaborator: false
              });
            })
            .catch(err => {
              console.error(
                `Error in ChannelEditor.getNonChannelCollaborators.getItems.func : ${err}`
              );
            });
        } else {
          this.handleInputChange(input, 'trustedCollaborator');
        }
      }
    );
  };

  getNewTagsIds = itemsType => {
    return this.state[`${itemsType}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('isNew');
      })
      .map(item => item.id);
  };

  getRemoveTagsIds = itemsType => {
    return this.state[`${itemsType}sRemove`].map(item => item.id);
  };

  programChannel = () => {
    this.setState(() => ({ savingChannel: true }));
    let openSourceIndex = this.state.eclSourcesCurrent.findIndex(obj => obj.name === 'Open Source');
    let openSourceTopic =
      openSourceIndex !== -1
        ? !!this.state.channelTopicFilter.length ||
          !!this.state.channelSources.length ||
          !!this.state.topicsCurrent.length
        : true;
    if (openSourceTopic) {
      let sourcesToAdd = [];
      let topicsToAdd = [];
      this.state.eclSourcesCurrent.map(item => {
        let tempObj = {};
        tempObj.source_id = item.id;
        tempObj.display_name = item.name;
        tempObj.private_content = item.privateContent;
        sourcesToAdd.push(tempObj);
      });
      this.state.topicsCurrent.forEach(topic => {
        let tempObj = {};
        tempObj.description = topic.description;
        tempObj.domain = topic.domain;
        tempObj.id = topic.id;
        tempObj.image_url = topic.image_url;
        tempObj.label = topic.label;
        tempObj.name = topic.fullPath;
        tempObj.taxo_id = topic.taxo_id;
        topicsToAdd.push(tempObj);
      });
      let channelPayload = {
        channel: {
          language: this.state.languagesCurrent.map(obj => obj.id),
          content_type: this.state.contentTypesCurrent.map(obj => obj.id),
          topics: topicsToAdd,
          ecl_sources: sourcesToAdd
        }
      };
      saveChannelProgramming(this.state.channel.id, channelPayload)
        .then(response => {
          this.getChannelTopics(this.state.channel.id);
          this.getEclChannelSources(this.state.channel.id);
          this.setState({ topicsCurrent: [], eclSourcesCurrent: [], savingChannel: false });
        })
        .catch(err => {
          console.error(`Error in ChannelEditModal.actions.programChannel : ${err}`);
        });
    } else {
      this.setState(
        () => ({ showRequiredTopic: true }),
        () => {
          setTimeout(() => {
            this.setState(() => ({ showRequiredTopic: false }));
          }, 5000);
        }
      );
    }
  };

  deleteSourceCall = item => {
    let channelSources = this.state.channelSources;
    let sourceIndex = this.state.channelSources.findIndex(
      source => source.eclSourceId === item.eclSourceId
    );
    if (~sourceIndex) {
      let payload = { ecl_source: { ecl_source_id: item.eclSourceId } };
      eclSDK.deleteChannelSource(this.state.channel.id, payload);
      channelSources.splice(sourceIndex, 1);
      this.setState({ channelSources });
    }
  };

  deleteTopicCall = item => {
    let channelTopics = this.state.channelTopics;
    let topicIndex = this.state.channelTopics.findIndex(topic => topic.id === item.id);
    if (~topicIndex) {
      channelsSDK.removeTopic(this.state.channel.id, item.id);
      channelTopics.splice(topicIndex, 1);
      this.setState({ channelTopics });
    }
  };

  markSourcePrivate = (e, index) => {
    let eclSourcesCurrent = this.state.eclSourcesCurrent;
    eclSourcesCurrent[index].privateContent = e.target.checked;
    this.setState({ eclSourcesCurrent });
  };

  saveChannel = () => {
    if (this.state.label.length === 0) {
      return;
    }
    this.setState({ showFollowModal: false });
    let channel = {};
    channel.label = this.state.label;
    channel.description = this.state.description;
    channel.profile_image_url =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.profileImageUrl =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.mobile_image =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.is_private = this.state.isPrivate;
    channel.allow_follow = this.state.allowFollow;
    channel.only_authors_can_post = this.state.onlyAuthorsCanPost;
    channel.curate_only = this.state.curateOnly;
    channel.curate_ugc = this.state.curateUgc;
    channel.ecl_enabled = this.state.eclEnabled;
    if (this.state.openChannel) {
      channel.is_open = this.state.isOpen;
    }

    if (!this.state.channel.id) {
      this.setState({ publishLabel: 'Processing...', isCreating: true });
      this.setState({ channelCreated: true });
      this.props
        .dispatch(actions.createChannel(channel))
        .then(createdChannel => {
          if (createdChannel.id) {
            this.updateCollaborators(createdChannel.id, false);
            this.updateCollaborators(createdChannel.id, true);
            this.updateCurators(createdChannel.id);
            this.setState({ isCreating: false, publishLabel: 'Update channel' });
            this.closeModal();
            this.props.dispatch(
              push(
                createdChannel && createdChannel.slug
                  ? `/channel/${createdChannel.slug}`
                  : `/channels/all`
              )
            );
          } else if (createdChannel.message) {
            this.setState({
              errorMessage: createdChannel.message.replace('Label', 'Name'),
              isCreating: false,
              publishLabel: 'Create channel'
            });
          }
        })
        .catch(err => {
          console.error(`Error in ChannelEditModal.actions.createChannel : ${err}`);
        });
    } else {
      channel.id = this.state.channel.id;
      (channel.language = this.state.languagesCurrent.map(obj => obj.id)),
        (channel.content_type = this.state.contentTypesCurrent.map(obj => obj.id));
      const openSourceIndex = Array.isArray(this.state.eclSourcesCurrent)
        ? this.state.eclSourcesCurrent.findIndex(obj => obj.name === 'Open Source')
        : -1;
      const openSourceTopic =
        openSourceIndex !== -1
          ? !!this.state.channelTopicFilter.length ||
            !!this.state.channelSources.length ||
            !!this.state.topicsCurrent.length
          : true;
      if (openSourceTopic) {
        this.setState({ publishLabel: 'Processing...', isCreating: true });
        this.props
          .dispatch(actions.saveChannelChanges(channel))
          .then(data => {
            if (data.message) {
              this.setState({
                errorMessage: data.message.replace('Label', 'Name'),
                isCreating: false,
                publishLabel: 'Update channel'
              });
            } else {
              if (this.state.channelProgrammingV2) {
                this.updateChannelContent();
              } else {
                this.updateTopicsAndSources();
              }
              this.updateCollaborators(channel.id, false);
              this.updateCollaborators(channel.id, true);
              this.updateCurators(channel.id);
              this.closeModal();
              this.props.dispatch(replace('/channel/' + channel.id));
            }
          })
          .catch(err => {
            console.error(`Error in ChannelEditModal.actions.saveChannelChanges : ${err}`);
          });
      } else {
        this.setState(
          () => ({ showRequiredTopic: true }),
          () => {
            setTimeout(() => {
              this.setState(() => ({ showRequiredTopic: false }));
            }, 5000);
          }
        );
      }
    }
  };

  updateChannelContent = async () => {
    const channelId = this.state.channel.id;

    const eclSourcePayload = {
      ecl_source: this.state.eclSourcesCurrent.map(obj => {
        const tempObj = {};
        tempObj.source_id = obj.id;
        tempObj.display_name = obj.name;
        return tempObj;
      })
    };
    let eclSourcesResponse;
    if (eclSourcePayload.ecl_source.length > 0) {
      eclSourcesResponse = await saveChannelTunningEclSources(channelId, eclSourcePayload);
    }
    if (
      (this.state.channelSources && !!this.state.channelSources.length) ||
      !!this.state.topicsCurrent.length ||
      (eclSourcesResponse && eclSourcesResponse.status === 200)
    ) {
      const topicPayLoad = {
        topic: this.state.topicsCurrent.map(obj => {
          return {
            id: obj.id,
            name: obj.name,
            label: obj.name,
            domain: {
              ...obj.domain,
              id: obj.domain.id || null
            }
          };
        })
      };
      if (!!topicPayLoad.topic.length) {
        await addTopicsList(channelId, topicPayLoad);
      }
    }
    let currentChannelSourcesIds = this.state.channelSources.map(item => item.eclSourceId);
    let currentChannelTopicsIds = this.state.channelTopics.map(item => item.id);
    let sourcesToRemoveIds = _.difference(this.state.channelSourcesIds, currentChannelSourcesIds);
    let topicsToRemoveIds = _.difference(this.state.channelTopicsIds, currentChannelTopicsIds);
    if (!!sourcesToRemoveIds.length) {
      deleteElSources(this.state.channel.id, {
        ecl_source: { ecl_source_ids: sourcesToRemoveIds }
      });
    }
    if (!!topicsToRemoveIds.length) {
      deleteTopics(this.state.channel.id, { topic: { topic_ids: topicsToRemoveIds } });
    }
  };

  updateCollaborators = (channelId, isTrusted) => {
    let collaboratorIds = this.getNewTagsIds(isTrusted ? 'trustedCollaborator' : 'collaborator');
    if (collaboratorIds.length > 0) {
      channelsSDK.addCollaborators(collaboratorIds, [], channelId, isTrusted).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.addCollaborators : ${err}`);
      });
    }
    let removeIds = this.getRemoveTagsIds(isTrusted ? 'trustedCollaborator' : 'collaborator');
    if (removeIds && removeIds.length) {
      channelsSDK.deleteCollaborators(channelId, removeIds, isTrusted).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.deleteCollaborators : ${err}`);
      });
    }
  };

  updateCurators = channelId => {
    let curatorIds = this.getNewTagsIds('curator');
    if (curatorIds.length > 0) {
      let payLoad = {
        id: channelId,
        user_ids: curatorIds
      };
      channelsSDK.addCurators(payLoad).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.addCurators : ${err}`);
      });
    }

    let removeIds = this.getRemoveTagsIds('curator');
    if (removeIds.length > 0) {
      channelsSDK.removeCurator(channelId, removeIds).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.removeCurator : ${err}`);
      });
    }
  };

  toggleCuration = () => {
    let curateOnly = !this.state.curateOnly;
    let curateUgc = this.state.curateUgc;
    if (this.state.curateOnly) {
      curateUgc = false;
    }
    this.setState({
      curateOnly: curateOnly,
      curateUgc: curateUgc
    });
  };

  toggleEcl = () => {
    this.setState(prevState => {
      return {
        eclEnabled: !prevState.eclEnabled
      };
    });
  };

  toggleUgcCuration = () => {
    this.setState(prevState => {
      return {
        curateUgc: !prevState.curateUgc
      };
    });
  };

  toggleOnlyAuthors = () => {
    this.setState(prevState => {
      return {
        onlyAuthorsCanPost: !prevState.onlyAuthorsCanPost
      };
    });
  };

  getSources(input, callback) {
    return eclSDK.getAllSources({ is_enabled: true }).then(data => {
      let suggs = [];
      let optionObj;
      data.map(item => {
        if (this.state.channelSourcesIds.indexOf(item.id) === -1) {
          optionObj = { value: item.id, label: item.display_name };
          suggs.push(optionObj);
        }
      });
      callback(null, {
        options: suggs,
        complete: true
      });
    });
  }

  getTopics(input, callback) {
    return topicsSdk.getSuggestedTopicsForChannel().then(topics => {
      let allTopics = {};
      let suggs = [];
      if (topics && topics.domains) {
        topics.domains.map(item => {
          allTopics[item.id] = item;
          suggs.push({ value: item.id, label: item.label });
        });
      }
      this.setState({
        allTopics: allTopics
      });
      callback(null, {
        options: suggs,
        complete: true
      });
    });
  }

  getSkills(input, callback) {
    let filterId = this.state.channelTopicFilter.value;
    return topicsSdk.getSuggestedTopicsSkillsForChannel(filterId).then(topics => {
      let allSkills = {};
      let suggs = [];
      if (topics && topics.topics) {
        topics.topics.map(item => {
          allSkills[item.id] = item;
          suggs.push({ value: item.id, label: item.label });
        });
      }
      this.setState({
        allSkills: allSkills
      });
      callback(null, {
        options: suggs,
        complete: true
      });
    });
  }

  handleSourceChange(source) {
    this.setState({ eclSourcesCurrent: source });
  }

  handleTopicChange(topic) {
    this.setState(prevState => ({
      channelTopicFilter: topic,
      channel_topic: null,
      skillsSelectKey: prevState.skillsSelectKey + 1
    }));
  }

  handleSkillChange(skill) {
    this.setState({ channel_topic: skill });
  }

  getSuggestionItem = value => {
    switch (this.state.viewMoreType) {
      case 'collaborator':
        this.getCollaboratorUsers(value, true);
        break;
      case 'trustedCollaborator':
        this.getTrustedCollaboratorUsers(value, true);
        break;
      case 'curator':
        this.getCuratorUser(value, true);
        break;
      default:
        break;
    }
  };

  saveItems = itemsType => {
    let removed = this.state[`${itemsType}sRemove`] || [];
    removed = _.uniqBy(removed.concat(this.state.moreItemsRemove), 'id');

    let current = this.state.moreItems || [];
    current = _.uniqBy(current.concat(this.state.newTags), 'id');

    _.remove(current, function(item) {
      return item.id.toString() === 'vm';
    });

    this.setState({
      [`${itemsType}sRemove`]: removed,
      [`${itemsType}sCurrent`]: current,
      [`${itemsType}sMore`]: 0,
      isOpenViewMore: false,
      moreItems: [],
      moreItemsRemove: [],
      newTags: []
    });
  };

  closeMoreModal = () => {
    this.setState({
      isOpenViewMore: false,
      moreItems: [],
      moreItemsRemove: [],
      newTags: [],
      isLastPage: false
    });
  };

  loadViewMore = type => {
    switch (type) {
      case 'collaborator':
        this.getChannelCollaborators(this.state.channel.id, true);
        break;
      case 'trustedCollaborator':
        this.getChannelTrustedCollaborators(this.state.channel.id, true);
        break;
      case 'curator':
        this.getChannelCurators(this.state.channel.id, true);
        break;
      default:
        break;
    }
  };

  handleOldDelete = (itemsType, i) => {
    let current = this.state.moreItems;
    let itemsRemove = this.state.moreItemsRemove;
    let remove = current.splice(i, 1);
    if (remove[0] && !remove[0].isNew) {
      let name = remove[0].firstName ? remove[0].firstName + ' ' : '';
      name += remove[0].lastName ? remove[0].lastName : '';
      name += `(${remove[0].email})`;
      itemsRemove.push({ id: remove[0].id, name: name, label: itemsType });
    }
    this.setState({ moreItems: current, moreItemsRemove: itemsRemove });
  };

  handleMoreDelete = tag => {
    let current = this.state.newTags;
    let index = _.findIndex(current, function(item) {
      return item.id == tag.id;
    });
    current.splice(index, 1);
    this.setState({ newTags: current });
  };

  handleMoreAddition(itemsType, tag) {
    let itemsRemove = this.state.moreItemsRemove;
    let isOld = _.remove(itemsRemove, function(item) {
      return item.id.toString() === tag.id.toString();
    });
    if (isOld.length === 0) {
      tag.isNew = true;
    }
    let current = this.state.newTags;
    current.push(tag);
    this.setState({ newTags: current, moreItemsRemove: itemsRemove });
  }

  removeChannelTopicSources = (item, type) => {
    let channelSources = this.state.channelSources;
    let channelTopics = this.state.channelTopics;
    if (type === 'source') {
      let sourceIndex = this.state.channelSources.findIndex(
        source => source.eclSourceId === item.eclSourceId
      );
      if (~sourceIndex) {
        channelSources.splice(sourceIndex, 1);
      }
    } else if (type === 'topic') {
      let topicIndex = this.state.channelTopics.findIndex(topic => topic.id === item.id);
      if (~topicIndex) {
        channelTopics.splice(topicIndex, 1);
      }
    }
    this.setState({
      channelSources,
      channelTopics
    });
  };

  addSourceTopic = () => {
    if (!this.state.channel_topic && !this.state.eclSourcesCurrent) return;
    let channelSources = this.state.channelSources;
    let channelTopics = this.state.channelTopics;
    if (!Array.isArray(this.state.eclSourcesCurrent)) {
      channelSources.push({
        eclSourceId: this.state.eclSourcesCurrent.value,
        displayName: this.state.eclSourcesCurrent.label
      });
    }
    if (this.state.channel_topic) {
      channelTopics.push({
        id: this.state.channel_topic.value,
        label: this.state.channel_topic.label
      });
    }
    this.setState({
      ecl_source: [],
      channelSources,
      channel_topic: null,
      channelTopics
    });
  };

  updateTopicsAndSources = () => {
    let currentChannelSourcesIds = this.state.channelSources.map(item => item.eclSourceId);
    let currentChannelTopicsIds = this.state.channelTopics.map(item => item.id);
    let sourcesToAddIds = _.difference(currentChannelSourcesIds, this.state.channelSourcesIds);
    let sourcesToRemoveIds = _.difference(this.state.channelSourcesIds, currentChannelSourcesIds);
    let topicsToAddIds = _.difference(currentChannelTopicsIds, this.state.channelTopicsIds);
    let topicsToRemoveIds = _.difference(this.state.channelTopicsIds, currentChannelTopicsIds);
    let topicsToAdd = this.state.channelTopics.filter(item => ~topicsToAddIds.indexOf(item.id));
    if (!!sourcesToAddIds.length) {
      addEclSourceList(this.state.channel.id, { ecl_source: { ecl_source_ids: sourcesToAddIds } });
    }
    if (sourcesToRemoveIds.length) {
      deleteElSources(this.state.channel.id, {
        ecl_source: { ecl_source_ids: sourcesToRemoveIds }
      });
    }
    if (topicsToAddIds.length) {
      addTopicsList(this.state.channel.id, { topic: topicsToAdd });
    }
    if (topicsToRemoveIds.length) {
      deleteTopics(this.state.channel.id, { topic: { topic_ids: topicsToRemoveIds } });
    }
  };

  handleSelectedLanguages = value => {
    this.setState(() => ({
      selectedLanguages: value
    }));
  };

  handleSelectedContentTypes = value => {
    this.setState(() => ({
      selectedContentTypes: value
    }));
  };

  render() {
    let collaboratorSuggestions = _.uniqWith(
      [...this.state.collaboratorOptions, ...this.state.collaboratorsRemove],
      _.isEqual
    );
    let trustedCollaboratorSuggestions = _.uniqWith(
      [...this.state.trustedCollaboratorOptions, ...this.state.trustedCollaboratorsRemove],
      _.isEqual
    );
    let curatorSuggestions = _.uniqWith(
      [...this.state.curatorOptions, ...this.state.curatorsRemove],
      _.isEqual
    );
    let itemsArray = [];
    switch (this.state.viewMoreType) {
      case 'collaborator':
        itemsArray = collaboratorSuggestions;
        break;
      case 'trustedCollaborator':
        itemsArray = trustedCollaboratorSuggestions;
        break;
      case 'curator':
        itemsArray = curatorSuggestions;
        break;
      default:
        break;
    }
    return (
      <div>
        <div className="channel-edit-modal">
          <div className="my-modal-header">
            <span className="header-title">{tr(this.state.titleLabel)}</span>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.closeBtn}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
              <TextField name="channeleditor" autoFocus={true} className="hiddenTextField" />
            </div>
          </div>

          <div className="my-modal-content">
            {this.state.showTuningTab && (
              <div className="tabs-header">
                <div className="tab-item">
                  <a
                    onClick={this.changeActiveTab.bind(this, 'settings')}
                    className={`tab-item__text ${
                      this.state.activeTab === 'settings' ? 'tab-item_active' : ''
                    }`}
                  >
                    {tr('Settings')}
                  </a>
                </div>
                {this.props.channel && (
                  <div className="tab-item">
                    <a
                      onClick={this.changeActiveTab.bind(this, 'tuning')}
                      disabled={this.state.channel && this.state.channel.id}
                      className={`tab-item__text ${
                        this.state.activeTab === 'tuning' ? 'tab-item_active' : ''
                      }`}
                    >
                      {tr('Tuning')}
                    </a>
                  </div>
                )}
              </div>
            )}
            <div
              className={`tab-content ${this.state.showTuningTab ? '' : 'tab-content-v3'}`}
              style={{ paddingBottom: '1.7rem' }}
            >
              {this.state.activeTab === 'settings' ? (
                <div className="common-block">
                  <div className="row" style={{ paddingBottom: '1.7rem' }}>
                    <div className="small-12 medium-4">
                      {!this.state.profileImageUrl && (
                        <div
                          className="item-banner"
                          style={this.styles.channelImg}
                          onClick={this.fileStackHandler.bind(
                            this,
                            this.handleProfileImageChange.bind(this)
                          )}
                        >
                          <div className="text-center">
                            <ImagePhotoCamera style={this.styles.mainImage} />
                            <div className="empty-image" style={{ marginTop: '-.5rem' }}>
                              {tr('Upload Channel Image')}
                            </div>
                          </div>
                        </div>
                      )}
                      {this.state.profileImageUrl && (
                        <div className="item-banner preview-upload">
                          <div
                            className="card-img-container"
                            onMouseEnter={() => {
                              this.setState({ mainImage: true });
                            }}
                            onMouseLeave={() => {
                              this.setState({ mainImage: false });
                            }}
                          >
                            <div
                              className="card-img button-icon"
                              style={{
                                ...this.styles.pathwayImage,
                                ...{
                                  backgroundImage: `url(\'${this.state.securedUrl ||
                                    this.state.profileImageUrl}\')`
                                }
                              }}
                            >
                              {this.hoverBlock(
                                this.fileStackHandler.bind(
                                  this,
                                  this.handleProfileImageChange.bind(this)
                                )
                              )}
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="hint-text">{tr('Recommended Size: 480px x 320px')}</div>
                    </div>
                    <div className="small-12 medium-8">
                      <div className="input-block" style={{ paddingBottom: '1.375rem' }}>
                        <input
                          className="my-input"
                          type="text"
                          maxLength={150}
                          value={this.state.label}
                          placeholder={tr('Title')}
                          ref={node => (this._label = node)}
                          onChange={e => {
                            this.setState({ label: e.target.value });
                          }}
                        />
                        <div className="hint-text">{`${150 - this.state.label.length}/150 ${tr(
                          'Characters Remaining'
                        )}`}</div>
                      </div>
                      <div className="input-block">
                        <input
                          className="my-input"
                          type="text"
                          maxLength={2000}
                          value={this.state.description}
                          placeholder={tr('Description')}
                          ref={node => (this._description = node)}
                          onChange={e => {
                            this.setState({ description: e.target.value });
                          }}
                        />
                        <div className="hint-text">{`${2000 -
                          this.state.description.length}/2000 ${tr('Characters Remaining')}`}</div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="small-12">
                      <div
                        className={`${
                          this.state.collaboratorsMore > 0 ? 'with-view-more' : ''
                        } input-block`}
                      >
                        <ReactTags
                          tags={this.state.collaboratorsCurrent}
                          suggestions={collaboratorSuggestions}
                          placeholder=""
                          tagComponent={this.selectedRestrictedToTags.bind(this, 'collaborator')}
                          handleAddition={this.handleAddition.bind(this, 'collaborator')}
                          handleDelete={this.handleDelete.bind(this, 'collaborator')}
                          handleInputChange={this.getCollaboratorUsers.bind(this)}
                        />
                        <div className="hint-text">
                          {tr('Add Collaborators, Collaborators can post to this channel')}
                        </div>
                      </div>
                    </div>
                  </div>
                  {this.state.allowTrustedCollaborators && (
                    <div className="row">
                      <div className="small-12">
                        <div
                          className={`${
                            this.state.trustedCollaboratorsMore > 0 ? 'with-view-more' : ''
                          } input-block`}
                        >
                          <ReactTags
                            tags={this.state.trustedCollaboratorsCurrent}
                            suggestions={trustedCollaboratorSuggestions}
                            placeholder=""
                            tagComponent={this.selectedRestrictedToTags.bind(
                              this,
                              'trustedCollaborator'
                            )}
                            handleAddition={this.handleAddition.bind(this, 'trustedCollaborator')}
                            handleDelete={this.handleDelete.bind(this, 'trustedCollaborator')}
                            handleInputChange={this.getTrustedCollaboratorUsers.bind(this)}
                          />
                          <div className="hint-text">
                            {tr(
                              'Add Trusted Collaborators, Trusted Collaborators can post to this channel'
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="row">
                    <div className="small-12">
                      <div
                        className={`${
                          this.state.curatorsMore > 0 ? 'with-view-more' : ''
                        } input-block`}
                        style={{ paddingBottom: '1.43rem' }}
                      >
                        <ReactTags
                          tags={this.state.curatorsCurrent}
                          suggestions={curatorSuggestions}
                          placeholder=""
                          tagComponent={this.selectedRestrictedToTags.bind(this, 'curator')}
                          handleAddition={this.handleAddition.bind(this, 'curator')}
                          handleDelete={this.handleDelete.bind(this, 'curator')}
                          handleInputChange={this.getCuratorUser.bind(this)}
                        />
                        <div className="hint-text">
                          {tr(
                            'Add Curators, Curators can configure, curate, and post to this channel(unless role is modified by admin under RBAC settings)'
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className="input-block" style={{ paddingBottom: '0.875rem' }}>
                      <div className="row">
                        <div className="small-12 medium-8 large-9">
                          <span
                            className="advanced-toggle"
                            onTouchTap={this.toggleAdvancedSettings}
                          >
                            {tr('Advanced Settings')}
                          </span>
                        </div>
                      </div>
                    </div>
                    {this.state.showAdvancedSettings && (
                      <div>
                        <div className="row">
                          <div className="small-12">
                            <div className="input-block" style={{ paddingBottom: '1rem' }}>
                              <Checkbox
                                style={this.styles.privateContent}
                                labelStyle={this.styles.checkboxLabelStyle}
                                checked={this.state.curateOnly}
                                label={tr('Curatable Channel')}
                                onCheck={this.toggleCuration}
                                aria-label={tr('Curatable Channel')}
                                uncheckedIcon={<div style={this.styles.checkOff} />}
                              />
                              <div className="hint-text" style={this.styles.hintText}>
                                {tr(
                                  `Content posted to this channel has to be moderated by Curator before making it visible in the channel`
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-12">
                            <div className="input-block">
                              <Checkbox
                                className="checkbox"
                                style={this.styles.privateContent}
                                labelStyle={this.styles.checkboxLabelStyle}
                                checked={this.state.curateUgc}
                                label={tr('Curate followers content')}
                                onCheck={this.toggleUgcCuration}
                                aria-label={tr('Curate followers content')}
                                uncheckedIcon={<div style={this.styles.checkOff} />}
                              />
                              <div className="hint-text" style={this.styles.hintText}>
                                {tr(
                                  `The Content posted by the followers of this channel can be curated`
                                )}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-3">
                            <span className="radio-channel">
                              {tr('Who can post to this channel?')}
                            </span>
                          </div>
                          <div className="small-9">
                            <div
                              className="input-block"
                              style={{ paddingLeft: '.5rem', paddingBottom: '1.375rem' }}
                            >
                              <Checkbox
                                className="checkbox"
                                style={{ paddingBottom: '.3rem' }}
                                labelStyle={this.styles.checkboxLabelStyle}
                                iconStyle={{ marginRight: '6px' }}
                                checked={!this.state.onlyAuthorsCanPost}
                                label={tr('Curators, Collaborators, and Followers')}
                                onCheck={this.toggleOnlyAuthors}
                                aria-label={tr('Curators, Collaborators, and Followers')}
                                uncheckedIcon={<div style={this.styles.checkOff} />}
                              />
                              <Checkbox
                                className="checkbox"
                                style={this.styles.privateContent}
                                labelStyle={this.styles.checkboxLabelStyle}
                                iconStyle={{ marginRight: '6px' }}
                                checked={this.state.onlyAuthorsCanPost}
                                label={tr('Curators and Collaborators only')}
                                onCheck={this.toggleOnlyAuthors}
                                aria-label={tr('Curators and Collaborators only')}
                                uncheckedIcon={<div style={this.styles.checkOff} />}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              ) : (
                <div>
                  {this.state.channelProgrammingV2 && (
                    <ChannelTunning channel={this.state.channel} />
                  )}
                  {!this.state.channelProgrammingV2 && (
                    <div className="common-block">
                      {!this.state.updatingSources && (
                        <div className="row">
                          <div className="small-12 medium-8 large-9">
                            <div className="input-block">
                              <div>
                                <Select.Async
                                  cache={false}
                                  loadOptions={this.getSources.bind(this)}
                                  value={this.state.eclSourcesCurrent}
                                  onChange={this.handleSourceChange.bind(this)}
                                  placeholder={tr('Content Sources')}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="row">
                        <div className="small-12 medium-8 large-9">
                          <div className="input-block">
                            <Select.Async
                              cache={false}
                              loadOptions={this.getTopics.bind(this)}
                              value={this.state.channelTopicFilter}
                              onChange={this.handleTopicChange.bind(this)}
                              placeholder={tr('Select Filter Category')}
                            />
                          </div>
                        </div>
                      </div>
                      {this.state.channelTopicFilter && (
                        <div className="row">
                          <div className="small-12 medium-8 large-9">
                            <div className="input-block">
                              <Select.Async
                                key={this.state.skillsSelectKey}
                                cache={false}
                                loadOptions={this.getSkills.bind(this)}
                                value={this.state.channel_topic}
                                onChange={this.handleSkillChange.bind(this)}
                                placeholder={tr('Filter with Topics')}
                              />
                            </div>
                          </div>
                        </div>
                      )}
                      <div className="row">
                        <div className="small-12 medium-6">
                          <div className="input-block">
                            <Checkbox
                              style={this.styles.eclSourcesCheckbox}
                              checked={this.state.eclEnabled}
                              labelStyle={this.styles.checkboxLabelStyle}
                              label={tr('ECL Sources')}
                              onCheck={this.toggleEcl.bind(this, this.state.channel.id)}
                            />
                          </div>
                        </div>
                        <div className="small-12 medium-6 large-3">
                          <FlatButton
                            style={{ ...this.styles.cancel, ...this.styles.addSource }}
                            className="pull-right"
                            labelStyle={this.styles.actionBtnLabel}
                            disabled={!this.state.eclSourcesCurrent && !this.state.channel_topic}
                            onClick={this.addSourceTopic}
                          >
                            {tr('Add')}
                          </FlatButton>
                        </div>
                      </div>
                      <div className="row">
                        <div className="small-12 medium-8 large-9">
                          {this.state.channelSources && !!this.state.channelSources.length && (
                            <div>
                              <p className="channel-sources">{tr('Channel Sources:')}</p>
                              <List className="wide-item-list">
                                {this.state.channelSources.map(item => {
                                  return (
                                    <div
                                      className="selected-source"
                                      key={`channelSources-${item.eclSourceId}`}
                                    >
                                      <ListItem
                                        rightIcon={
                                          <IconButton
                                            style={{
                                              ...this.styles.closeBtn,
                                              ...this.styles.removeBtn
                                            }}
                                            onTouchTap={this.removeChannelTopicSources.bind(
                                              this,
                                              item,
                                              'source'
                                            )}
                                          >
                                            <CloseIcon color="gray" />
                                          </IconButton>
                                        }
                                        innerDivStyle={this.styles.listItem}
                                        primaryText={
                                          <div className="selected-source__text">
                                            {item.displayName || ' '}
                                          </div>
                                        }
                                      />
                                    </div>
                                  );
                                })}
                              </List>
                            </div>
                          )}
                          {this.state.channelTopics && !!this.state.channelTopics.length && (
                            <div>
                              <p className="channel-sources">{tr('Channel Topics:')}</p>
                              <List className="wide-item-list">
                                {this.state.channelTopics.map(item => {
                                  return (
                                    <div
                                      className="selected-source"
                                      key={`channelTopics-${item.id}`}
                                    >
                                      <ListItem
                                        rightIcon={
                                          <IconButton
                                            style={{
                                              ...this.styles.closeBtn,
                                              ...this.styles.removeBtn
                                            }}
                                            onTouchTap={this.removeChannelTopicSources.bind(
                                              this,
                                              item,
                                              'topic'
                                            )}
                                          >
                                            <CloseIcon color="gray" />
                                          </IconButton>
                                        }
                                        innerDivStyle={this.styles.listItem}
                                        primaryText={
                                          <div className="selected-source__text">
                                            {item.label || ' '}
                                          </div>
                                        }
                                      />
                                    </div>
                                  );
                                })}
                              </List>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              )}
              {this.state.errorMessage.length > 0 && (
                <div className="error-text">{this.state.errorMessage}</div>
              )}
              {(this.state.activeTab === 'settings' ||
                (!this.state.channelProgrammingV2 && this.state.activeTab === 'tuning')) && (
                <div className="action-buttons">
                  <FlatButton
                    label={tr('Cancel')}
                    disabled={this.state.isCreating}
                    style={this.styles.cancel}
                    labelStyle={this.styles.actionBtnLabel}
                    onTouchTap={this.closeModal}
                  />

                  <PrimaryButton
                    label={tr(this.state.publishLabel)}
                    disabled={this.state.isCreating || this.state.label.trim().length === 0}
                    onTouchTap={this.saveChannel}
                    labelStyle={this.styles.actionBtnLabel}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
        {this.state.isOpenViewMore && (
          <div className="more-items channel-edit-modal">
            <div className="backdrop" onClick={this.closeModal} />
            <div className="modal">
              <div className="my-modal-header">
                <span className="header-title">
                  {tr(
                    this.state.viewMoreType === 'trustedCollaborator'
                      ? 'Trusted Collaborators'
                      : this.state.viewMoreType
                  )}
                </span>
                <div className="close close-button">
                  <IconButton
                    aria-label="close"
                    style={this.styles.closeBtn}
                    onTouchTap={this.closeMoreModal}
                  >
                    <CloseIcon color="white" />
                  </IconButton>
                </div>
              </div>
              <div className="my-modal-content">
                {this.state.moreItems &&
                  this.state.moreItems.map((item, index) => {
                    if (item.id === 'vm') return;
                    return this.viewMoreTag(this.state.viewMoreType, item, index);
                  })}
                {!this.state.isLastPage && (
                  <div className="text-right">
                    <a
                      className="react-tags_link"
                      key="view-more-tag"
                      onClick={event => this.loadViewMore(this.state.viewMoreType)}
                    >
                      {tr('View more')}
                    </a>
                  </div>
                )}
                <div className="row">
                  <div className="small-12">
                    <div className="input-block">
                      <ReactTags
                        tags={this.state.newTags}
                        suggestions={itemsArray}
                        maxSuggestionsLength={10}
                        placeholder=""
                        tagComponent={this.selectedRestrictedToTags.bind(
                          this,
                          this.state.viewMoreType
                        )}
                        handleAddition={this.handleMoreAddition.bind(this, this.state.viewMoreType)}
                        handleDelete={this.handleMoreDelete.bind(this)}
                        handleInputChange={this.getSuggestionItem.bind(this)}
                        handleFocus={() => {
                          itemsArray = [];
                        }}
                      />
                      <div className="hint-text">{tr(this.state.hintText)}</div>
                    </div>
                  </div>
                </div>
                <div className="action-buttons">
                  <FlatButton
                    label={tr('Cancel')}
                    disabled={this.state.isCreating}
                    style={this.styles.cancel}
                    labelStyle={this.styles.actionBtnLabel}
                    onTouchTap={this.closeMoreModal}
                  />
                  <PrimaryButton
                    label={tr('Save')}
                    disabled={this.state.isCreating || this.state.label.trim().length === 0}
                    onTouchTap={this.saveItems.bind(this, this.state.viewMoreType)}
                    labelStyle={this.styles.actionBtnLabel}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelEditorModal.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  channel: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(ChannelEditorModal);
