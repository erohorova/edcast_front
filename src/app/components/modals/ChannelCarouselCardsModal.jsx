import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../cards/Card';
import Spinner from '../common/spinner';
import ChannelCardOverlay from '../channel/channel_redesigning/ChannelCardOverlay';
import { close } from '../../actions/modalActions';
import { getChannleCarouselData } from 'edc-web-sdk/requests/channels.v2';

export class ChannelCarouselCardsModal extends Component {
  constructor(props) {
    super(props);
    const cards = this.props.modal.get('carouselCards').toJS() || [];
    this.state = {
      offset: cards.length,
      cards,
      loading: false,
      pinnedCount: 0,
      overallCount: this.props.modal.get('totalCarouselCardsCount') || 0,
      updateModal: false,
      isReordering: false,
      isCustomCarousel: this.props.modal.getIn(['carouselData', 'custom_carousel']),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      carouselCardsInfo: null,
      autoPinCards: this.props.channel.autoPinCards
    };
    this.styles = {
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      }
    };
  }

  componentDidMount() {
    this.setCardType();
  }

  setCardType = () => {
    let carouselTitle = this.props.modal.getIn(['carouselData', 'default_label']);
    let titleKeysObj = {
      carouselTitle
    };
    switch (carouselTitle) {
      case 'Courses':
        titleKeysObj.key = 'course';
        titleKeysObj.countKey = 'coursesCount';
        titleKeysObj.cardType = ['course'];
        break;
      case 'SmartCards':
        titleKeysObj.key = 'smartbite';
        titleKeysObj.countKey = 'smartbitesCount';
        titleKeysObj.cardType = ['media', 'poll'];
        break;
      case 'Streams':
        titleKeysObj.key = 'video_stream';
        titleKeysObj.countKey = 'videoStreamsCount';
        titleKeysObj.cardType = ['video_stream'];
        break;
      case 'Journeys':
        titleKeysObj.key = 'journey';
        titleKeysObj.countKey = 'publishedJourneysCount';
        titleKeysObj.cardType = ['journey'];
        break;
      case 'Pathways':
        titleKeysObj.key = 'pack';
        titleKeysObj.countKey = 'publishedPathwaysCount';
        titleKeysObj.cardType = ['pack'];
        break;
      default:
        titleKeysObj.key = 'custom_carousel';
        titleKeysObj.countKey = 'customCarouselCount';
        titleKeysObj.cardType = ['card'];
        break;
    }
    this.setState(() => ({ carouselCardsInfo: titleKeysObj }));
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true, updateModal: true });
    let type = this.state.carouselCardsInfo.cardType;
    let offset = !!this.state.isReordering ? 0 : this.state.cards.length;
    const payload = {
      'card_type[]': type,
      offset,
      limit: 12
    };
    getChannleCarouselData(payload, this.props.channel.id)
      .then(response => {
        let cards = !!this.state.isReordering
          ? response.cards
          : [...this.state.cards, ...response.cards];
        this.setState({
          cards,
          loading: false,
          offset: offset,
          isReordering: false
        });
      })
      .catch(error => {
        console.error('Error in ChannelCarousel.getChannleCarouselData', error);
      });
  };

  channelReOrderCallback = () => {
    this.setState(
      () => ({ isReordering: true }),
      () => {
        this.viewMoreClickHandler();
      }
    );
  };

  removeCardFromModal = cardId => {
    this.setState(() => ({
      cards: this.state.cards.filter(card => card.id !== cardId),
      overallCount: this.state.overallCount - 1
    }));
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    let featuredCardsIds = this.props.channelFeaturedCardsId || [];
    return (
      <div className="channel-cards-modal channel-cards-modal-V2">
        <div className="row">
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>
              {tr(this.props.modal.getIn(['carouselData', 'default_label']))} (
              {this.state.overallCount})
            </div>
            <div className="close close-button">
              <IconButton style={this.styles.iconButton} onTouchTap={this.closeModal}>
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row ">
                {this.state.cards &&
                  this.state.cards.length > 0 &&
                  !this.state.isReordering &&
                  this.state.cards.map(card => {
                    return (
                      <div
                        className={`channel-card-v2 card-v2 custom-medium-6 column large-4 small-12 
                                  ${this.props.modal.get('showChannelCardConfig') &&
                                    'show-overlay'} card-v3-container`}
                        key={card.id}
                      >
                        <Card
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser.toJS()}
                          isMarkAsCompleteDisabled={true}
                          moreCards={false}
                          hideModal={true}
                          channelSetting={true}
                          removeCardFromList={this.removeCardFromList}
                        />
                        <ChannelCardOverlay
                          card={card}
                          pinnedStatus={!!~featuredCardsIds.indexOf(card.id)}
                          pinUnpinCard={this.pinUnpinCard}
                          pinnedCount={featuredCardsIds.length}
                          channel={this.props.channel}
                          allCardsCount={this.state.overallCount}
                          isCustomCarousel={this.state.isCustomCarousel}
                          reorderToggle={this.isReordering}
                          setOffset={this.setOffsetReorder}
                          channelReOrderCallback={this.channelReOrderCallback}
                          deleteCard={this.removeCardFromModal}
                          autoPinCards={this.state.autoPinCards}
                          carouselCardsInfo={this.state.carouselCardsInfo}
                        />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {this.state.cards &&
              this.state.overallCount > this.state.cards.length &&
              !this.state.isReordering && (
                <button
                  className="view-more-v2"
                  onClick={() => {
                    this.viewMoreClickHandler(false);
                  }}
                >
                  {this.state.loading ? tr('Loading...') : tr('View More')}
                </button>
              )}
            {this.state.isReordering && <Spinner />}
          </div>
        </div>
      </div>
    );
  }
}

ChannelCarouselCardsModal.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  channel: PropTypes.object,
  channelFeaturedCardsId: PropTypes.any
};

const mapStateToProps = state => ({
  modal: state.modal,
  currentUser: state.currentUser,
  channel: state.channelReducerV3.get('channel'),
  channelFeaturedCardsId: state.channelReducerV3.get('featuredCardsIds')
});

export default connect(mapStateToProps)(ChannelCarouselCardsModal);
