/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
//actions
import { close } from '../../actions/modalActions';
import TextField from 'material-ui/TextField';
import { groups } from 'edc-web-sdk/requests/index';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { addPendingMember } from '../../actions/groupsActionsV2';
import { tr } from 'edc-web-sdk/helpers/translations';
import AutoComplete from 'material-ui/AutoComplete';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';

class InviteUserModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.confirmClickHandler = this.confirmClickHandler.bind(this);
    this.clearEmailError = this.clearEmailError.bind(this);
    this.getOptions = this.getOptions.bind(this);
    this.state = {
      emailError: false,
      inviteError: '',
      inviteLabel: 'Invite',
      sentInvite: false,
      disabled: true,
      usersToTeam: []
    };
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  handleUserInput = (value = '') => {
    console.log(value);
  };

  getOptions(input, callback) {
    return usersSDK
      .getItems({
        q: input,
        limit: 10,
        offset: 0,
        sort: 'created_at',
        order: 'desc',
        exclude_team_members: true,
        'team_ids[]': this.props.group.id
      })
      .then(users => {
        let suggs = users.items.map(item => {
          return {
            value: item.email,
            label: item.name + ' ' + item.handle,
            email: item.email,
            handle: item.handle,
            name: item.name,
            id: item.id,
            isFollowed: item.isFollowing,
            photo: item.avatarimages && item.avatarimages.small,
            role: 'member'
          };
        });
        let selectedUsers = this.state.usersToTeam.filter(function(obj) {
          return obj.label === input;
        });
        this.setState({
          usersToTeam: selectedUsers.length !== 0 ? selectedUsers : suggs,
          disabled: selectedUsers.length == 0
        });
      });
  }

  confirmClickHandler() {
    this.setState({ sentInvite: true });
    let markedUsers = this.state.usersToTeam.map(item => {
      return item.value;
    });
    groups
      .inviteUser(markedUsers, this.props.group.id)
      .then(() => {
        this.props.dispatch(close());
        this.setState({ sentInvite: false });
        this.props.dispatch(addPendingMember(this.state.usersToTeam));
        this.props.dispatch(openSnackBar('Invite successfully sent!', true));
      })
      .catch(err => {
        this.setState({ inviteLabel: 'Invite', inviteError: err.msg || err.message || err });
      });
  }

  handleSelectChange(usersToTeam) {
    this.setState({ usersToTeam: usersToTeam });
  }

  clearEmailError(e) {
    this.setState({ emailError: false });
  }

  render() {
    return (
      <div>
        <h5>{tr('Invite New Users')}</h5>
        <hr />
        <div className="modal-actions">
          <TextField name="inviteuser" autoFocus={true} className="hiddenTextField" />
          <AutoComplete
            hintText={tr('Enter users name to invite')}
            aria-label={tr('Enter users name to invite')}
            popoverProps={{
              style: {
                bottom: '0.7rem',
                overflowY: 'auto',
                background: 'transparent',
                boxShadow: 'none'
              },
              className: 'auto-complete-list'
            }}
            menuStyle={{ background: 'white' }}
            filter={AutoComplete.caseInsensitiveFilter}
            dataSource={this.state.usersToTeam.map(item => {
              return item.label;
            })}
            onUpdateInput={this.getOptions.bind(this)}
            fullWidth={true}
            dataSourceConfig={{ text: 'label', value: 'name' }}
            openOnFocus={true}
            searchText={this.state.searchText}
          />

          {this.state.inviteError && (
            <span className="error-text float-left">{tr(this.state.inviteError)}</span>
          )}

          <SecondaryButton
            label={tr('Cancel')}
            keyboardFocused={true}
            className="close"
            onTouchTap={this.cancelClickHandler}
          />

          <PrimaryButton
            label={tr(this.state.inviteLabel)}
            className="confirm"
            pendingLabel={tr('Sending invite...')}
            pending={this.state.sentInvite}
            disabled={this.state.disabled}
            onTouchTap={this.confirmClickHandler}
          />
        </div>
      </div>
    );
  }
}

InviteUserModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  group: PropTypes.object,
  callback: PropTypes.func,
  title: PropTypes.string
};

export default connect()(InviteUserModal);
