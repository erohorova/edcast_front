import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { CardHeader } from 'material-ui/Card';
import CommentList from '../../components/feed/CommentList.v2';
import {
  loadComments,
  toggleLikeCardAsync,
  toggleBookmarkCardAsync,
  dismissCard,
  setRateCardAsync
} from '../../actions/cardsActions';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import Comment from 'edc-web-sdk/components/icons/Comment';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import Resource from '../../components/feed/Resource';
import { tr } from 'edc-web-sdk/helpers/translations';
import Poll from '../../components/feed/Poll';
import VideoStream from '../../components/feed/VideoStream';
import PathwayCover from '../../components/feed/PathwayCover';
import { push } from 'react-router-redux';
import { Permissions } from '../../utils/checkPermissions';
import checkResources from '../../utils/checkResources';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import linkPrefix from '../../utils/linkPrefix';
import MarkdownRenderer from '../common/MarkdownRenderer';
import CreationDate from '../common/CreationDate';
import { updateRelevancyRatingQ, checkRatedCard } from '../../actions/relevancyRatings';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';

class CardModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      card: props.card,
      commentsCount: props.commentsCount,
      isUpvoted: props.isUpvoted,
      votesCount: props.votesCount
    };
    this.styles = {
      labelStyle: {
        border: '1px solid rgb(204, 204, 204)',
        height: '30px',
        lineHeight: '30px',
        display: 'inline-block',
        padding: '0px 12px',
        fontWeight: '500'
      },
      actionIcon: {
        paddingLeft: 0,
        paddingRight: '5px',
        width: 'auto'
      },
      tooltipActiveBts: {
        marginTop: -10,
        marginLeft: -10
      }
    };
  }

  componentDidMount() {}

  componentWillMount() {
    let isECL = /^ECL-/.test(this.props.card.id);
    if (!isECL && this.state.commentsCount) {
      loadComments(
        this.props.card.id,
        this.state.commentsCount,
        this.props.card.cardType,
        this.props.card.id ? this.props.card.id : null
      )
        .then(data => {
          this.setState({ comments: data, showComment: true });
        })
        .catch(err => {
          console.error(`Error in CardModal.loadComments.func: ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  }

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count }, () => {
      this.props.updateCommentCount(this.state.commentsCount);
    });
  };

  cardUpdated(card) {
    if (this.props.card) {
      fetchCard(this.props.card.id)
        .then(data => {
          this.setState({
            card: data
          });
        })
        .catch(err => {
          console.error(`Error in CardModal.fetchCard.func: ${err}`);
        });
    }
  }

  closeModal = () => {
    this.props.closeModal();
    if (
      !checkRatedCard(this.props.card, this.props.relevancyRating.ratedQueue) &&
      this.state.relevancyRatings
    ) {
      this.props.dispatch(updateRelevancyRatingQ(this.props.card));
    }
  };

  likeCardHandler = () => {
    this.setState(
      {
        isUpvoted: !this.state.isUpvoted,
        votesCount: Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0)
      },
      () => {
        this.props.likeCard();
      }
    );
  };

  standaloneLinkClickHandler = () => {
    let linkPrefixValue = linkPrefix(this.props.card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${this.props.card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${this.props.card.id}`
    ) {
      return;
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${this.props.card.slug}`));
    if (this.props.toggleSearch) {
      this.props.toggleSearch();
    }
  };

  urlify = text => {
    let urlRegex = /\i\b(http|https):\/\/(\S*)\b/i;
    return text.replace(urlRegex, function(url) {
      return '[' + url + '](' + url + ')';
    });
  };

  commentCardHandler = () => {
    var commentInputElement = document.getElementsByClassName('comment create');
    if (commentInputElement.length > 0) {
      setTimeout(function() {
        commentInputElement[0].focus();
      }, 10);
    }
  };

  truncateMessageText = message => {
    return message.substr(0, 260);
  };

  componentWillReceiveProps(nextProps) {}

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.state.dismissed) {
      return null;
    }
    let card = checkResources(this.props.card);
    if (card.video) {
      // FIXME: linkPrefix was not defined (assigning to undefined for eslint)
      let linkPrefixValue = undefined;
      card.resource = {
        description: card.video.description,
        embedHtml: card.video.embedHtml,
        site: card.video.site,
        title: card.video.title,
        slug: linkPrefixValue + card.slug
      };
    }

    if (typeof card.resource === 'undefined') {
      if (card.video) {
        card.resource = {
          description: card.video.description,
          embedHtml: card.video.embedHtml,
          site: card.video.site,
          title: card.video.title,
          slug: `/${linkPrefix(card.cardType)}/${card.slug}`
        };
      }
    }
    if (
      (!card.resource.imageUrl || card.resource.imageUrl.indexOf('default_card') > -1) &&
      !card.filestack &&
      this.props.defaultImage
    ) {
      card.resource.imageUrl = this.props.defaultImage;
    }
    let postVerb = card.cardType !== 'video_stream' ? 'posted' : 'streaming';
    let postedChannel = card.channels[0];
    let channelCount = Object.keys(card.channels).length;
    let title = card.title ? card.title.replace(/\\\*/g, '*') : '';
    let message = card.message;
    if (message) {
      message = this.urlify(card.message);
    }
    let disableTopics = card.tags.length > 0;
    let logoObj = this.props.logoObj;
    let htmlRender = !!card.resource && (!!card.resource.title || !!card.resource.description);
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    return (
      <div className="card-modal-container">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="modal">
          <Paper style={{ maxHeight: '80vh' }}>
            <div className="container">
              <div className="container-padding">
                <div className="row">
                  <div className="small-12 medium-8 large-8 columns card-modal-left-column">
                    {card.author && (
                      <CardHeader
                        title={
                          <div>
                            <strong
                              className="matte user"
                              onTouchTap={() => {
                                this.props.dispatch(push(`/${card.author.handle}`));
                              }}
                            >
                              {card.author.name}
                            </strong>
                            {postedChannel && (
                              <small>
                                {' '}
                                {postVerb} on
                                <a
                                  onTouchTap={() => {
                                    this.props.dispatch(push(`/channel/${postedChannel.id}`));
                                  }}
                                >
                                  {` ${postedChannel.label}`}
                                </a>
                                {channelCount > 1
                                  ? tr(
                                      ' and ' +
                                        (channelCount - 1) +
                                        ' other ' +
                                        (channelCount === 2 ? 'channel' : 'channels')
                                    )
                                  : ''}
                              </small>
                            )}
                          </div>
                        }
                        avatar={
                          card.author.pictureUrl ||
                          (card.author.avatarimages && card.author.avatarimages.medium)
                        }
                        subtitle={
                          <div
                            className="matte stand-alone"
                            onTouchTap={this.standaloneLinkClickHandler}
                          >
                            <CreationDate
                              card={card}
                              standaloneLinkClickHandler={this.standaloneLinkClickHandler}
                            />
                          </div>
                        }
                        style={{ padding: 0 }}
                        className="modal-header"
                      />
                    )}
                    <div className="card-container">
                      {(card.cardType === 'media' || card.cardType === 'poll') && (
                        <div>
                          <div className="card-title vertical-spacing-large">
                            {title && (
                              <div className="card-title">
                                {(htmlRender && (
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: this.state.truncateTitle
                                        ? title.substr(0, 260)
                                        : card.title
                                    }}
                                  />
                                )) || (
                                  <MarkdownRenderer
                                    markdown={
                                      this.state.truncateTitle ? title.substr(0, 260) : card.title
                                    }
                                  />
                                )}
                              </div>
                            )}
                            {message && (
                              <div>
                                {(htmlRender && (
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: this.state.truncateMessage
                                        ? this.truncateMessageText(message)
                                        : message
                                    }}
                                  />
                                )) || (
                                  <MarkdownRenderer
                                    markdown={
                                      this.state.truncateMessage
                                        ? this.truncateMessageText(message)
                                        : message
                                    }
                                  />
                                )}
                              </div>
                            )}
                          </div>
                          {card.filestack && (
                            <div>
                              {card.filestack.map(file => {
                                if (file.mimetype && ~file.mimetype.indexOf('image/')) {
                                  return (
                                    <div key={file.handle} className="fp fp_img">
                                      <img
                                        src={addSecurity(
                                          file.url,
                                          expireAfter,
                                          this.props.currentUser.id
                                        )}
                                      />
                                    </div>
                                  );
                                } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                                  return (
                                    <div key={file.handle} className="fp fp_video">
                                      <video
                                        controls
                                        src={file.url}
                                        controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                                      />
                                    </div>
                                  );
                                } else {
                                  return (
                                    <div key={file.handle} className="fp fp_preview">
                                      <iframe
                                        height="480px"
                                        width="100%"
                                        src={pdfPreviewUrl(
                                          file.url,
                                          expireAfter,
                                          this.props.currentUser.id
                                        )}
                                      />
                                    </div>
                                  );
                                }
                              })}
                            </div>
                          )}
                          {card.resource && card.cardSubtype !== 'text' && (
                            <div className="card-resource">
                              <Resource
                                resource={card.resource}
                                cardId={card.id}
                                providerLogos={this.props.providerLogos}
                                currentUser={this.props.currentUser}
                              />
                            </div>
                          )}
                        </div>
                      )}
                      {card.cardType === 'course' && (
                        <div className="vertical-spacing-large">
                          <div>{card.title && <MarkdownRenderer markdown={card.title} />}</div>
                          {card.resource && (
                            <div>
                              <Resource
                                resource={card.resource}
                                cardId={card.id}
                                providerLogos={this.props.providerLogos}
                                currentUser={this.props.currentUser}
                              />
                            </div>
                          )}
                        </div>
                      )}
                      {(card.cardType === 'pack' || card.cardType === 'pack_draft') && (
                        <PathwayCover
                          modal={true}
                          card={this.props.card}
                          defaultImage={this.props.defaultImage}
                          currentUser={this.props.currentUser}
                        />
                      )}
                      {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
                        <VideoStream card={this.props.card} />
                      )}
                      {card.ecl &&
                        card.ecl.origin &&
                        this.props.providerLogos[card.ecl.origin] != undefined && (
                          <span>
                            <img
                              className="origin-logos"
                              src={addSecurity(
                                this.props.providerLogos[card.ecl.origin],
                                expireAfter,
                                this.props.currentUser.id
                              )}
                            />
                          </span>
                        )}
                      {card.cardType === 'poll' && (
                        <Poll cardOverview={true} card={this.props.card} />
                      )}
                      <div className="card-info">
                        <div className="row">
                          <div className="medium-6 large-6 columns align-self-middle">
                            {(card.eclSourceLogoUrl ||
                              (card.eclSourceTypeName &&
                                logoObj[card.eclSourceTypeName] !== undefined)) && (
                              <span className="insight-img-logo">
                                <img
                                  src={addSecurity(
                                    card.eclSourceLogoUrl ||
                                      logoObj[card.eclSourceTypeName.toLowerCase()],
                                    expireAfter,
                                    this.props.currentUser.id
                                  )}
                                />
                              </span>
                            )}
                            {card.eclSourceTypeName &&
                              logoObj[card.eclSourceTypeName] === undefined && (
                                <span className="card-type">
                                  <div style={this.styles.labelStyle}>
                                    {card.eclSourceDisplayName}
                                  </div>
                                </span>
                              )}
                          </div>
                          <div className="medium-6 large-6 columns text-right align-self-middle">
                            <span className="card-type">
                              {card.cardType === 'VideoStream' ||
                                card.cardType === 'video_stream' ||
                                (card.cardType === 'media' && card.cardSubtype === 'video' && (
                                  <div style={this.styles.labelStyle}>{tr('VIDEO')}</div>
                                ))}
                              {card.cardType === 'media' && card.cardSubtype === 'link' && (
                                <div style={this.styles.labelStyle}>{tr('ARTICLE')}</div>
                              )}
                              {card.cardType === 'media' && card.cardSubtype === 'image' && (
                                <div style={this.styles.labelStyle}>{tr('IMAGE')}</div>
                              )}
                              {card.cardType === 'course' && (
                                <div style={this.styles.labelStyle}>{tr('COURSE')}</div>
                              )}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="comments-container small-12 medium-4 large-4 columns">
                    <div className="close close-button">
                      <IconButton aria-label="close" onTouchTap={this.closeModal}>
                        <CloseIcon color="white" />
                      </IconButton>
                    </div>
                    <div className="comments">
                      <div>
                        <small className="label">{tr('COMMENTS')}</small>
                      </div>
                      <div className="break-line" />
                      {this.state.showComment && (
                        <div>
                          <CommentList
                            cardId={card.id + ''}
                            cardOwner={card.author ? card.author.id : ''}
                            cardType={card.cardType}
                            comments={this.state.comments}
                            pending={card.commentPending}
                            numOfComments={this.state.commentsCount}
                            updateCommentCount={this.updateCommentCount}
                            videoId={card.id ? card.id : null}
                            inModal={true}
                          />
                        </div>
                      )}
                    </div>
                    <div className="modal-actions-bar horizontal-spacing-large">
                      <div className="border-element" />
                      {Permissions.has('LIKE_CONTENT') && (
                        <span>
                          <IconButton
                            style={this.styles.actionIcon}
                            tooltip={!this.state.isUpvoted ? tr('Like') : tr('Liked')}
                            tooltipStyles={this.styles.tooltipActiveBts}
                            onTouchTap={this.likeCardHandler}
                          >
                            {!this.state.isUpvoted && <LikeIcon />}
                            {this.state.isUpvoted && <LikeIconSelected />}
                          </IconButton>
                          <small className="votes-count">
                            {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : ''}
                          </small>
                        </span>
                      )}
                      {Permissions.has('CREATE_COMMENT') && (
                        <span>
                          <IconButton
                            onTouchTap={this.commentCardHandler}
                            style={this.styles.actionIcon}
                            tooltip={tr('Comment')}
                            aria-label={tr('Comment')}
                            tooltipStyles={this.styles.tooltipActiveBts}
                          >
                            <Comment />
                          </IconButton>
                          <small className="comments-count">
                            {this.state.commentsCount
                              ? abbreviateNumber(this.state.commentsCount)
                              : ''}
                          </small>
                        </span>
                      )}
                      <div className="float-right button-icon">
                        <InsightDropDownActions
                          card={this.props.card}
                          author={this.props.card.author}
                          dismissible={false}
                          style={{ paddingRight: 0, width: 'auto' }}
                          disableTopics={disableTopics}
                          isStandalone={this.props.isStandalone}
                          cardUpdated={this.cardUpdated.bind(this)}
                          assignable={false}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

CardModal.propTypes = {
  card: PropTypes.object,
  logoObj: PropTypes.object,
  relevancyRating: PropTypes.object,
  team: PropTypes.object,
  providerLogos: PropTypes.object,
  commentsCount: PropTypes.number,
  votesCount: PropTypes.number,
  isUpvoted: PropTypes.bool,
  isStandalone: PropTypes.bool,
  updateCommentCount: PropTypes.func,
  likeCard: PropTypes.func,
  toggleSearch: PropTypes.func,
  closeModal: PropTypes.func,
  pathname: PropTypes.string,
  defaultImage: PropTypes.string
};

export default connect(state => ({
  relevancyRating: state.relevancyRating.toJS(),
  team: state.team.toJS(),
  currentUser: state.currentUser.toJS()
}))(CardModal);
