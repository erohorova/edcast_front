import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import UploadIcon from 'edc-web-sdk/components/icons/Upload';
import { close } from '../../actions/modalActions';
import { push } from 'react-router-redux';
const SmartBite = Loadable({
  loader: () => import('./SmartBite'),
  loading: () => null
});
import LinkIcon from 'edc-web-sdk/components/icons/Link';
import TextIcon from 'edc-web-sdk/components/icons/Text';
import PollIcon from 'edc-web-sdk/components/icons/Pollv2';
import Course from 'edc-web-sdk/components/icons/Course';
import updateChannel from '../../utils/updateChannel';
import Scorm from 'edc-web-sdk/components/icons/Scorm';
import Project from 'edc-web-sdk/components/icons/Project';
import { Permissions } from '../../utils/checkPermissions';
import TextField from 'material-ui/TextField';
import { close as closeSnackBar } from '../../actions/snackBarActions';

class SmartbiteEditor extends Component {
  constructor(props, context) {
    super(props, context);

    let activeType =
      Permissions['enabled'] !== undefined
        ? Permissions.has('PUBLISH_LINKS')
          ? 'link'
          : Permissions.has('UPLOAD')
          ? 'upload'
          : 'poll'
        : 'poll';
    if (props.card) {
      if (props.card.cardType === 'course' && props.card.cardSubtype === 'link') {
        activeType = 'link';
      }
      if (
        props.card.cardType === 'media' &&
        (props.card.cardSubtype === 'link' ||
          (props.card.cardSubtype === 'video' && !props.card.filestack.length))
      ) {
        activeType = 'link';
      }
      if (
        (props.card.cardType === 'media' && props.card.cardSubtype === 'image') ||
        (props.card.cardSubtype === 'video' &&
          !!props.card.filestack.length &&
          props.card.filestack[0].mimetype &&
          ~props.card.filestack[0].mimetype.indexOf('video/')) ||
        (props.card.cardSubtype === 'audio' &&
          !!props.card.filestack.length &&
          props.card.filestack[0].mimetype &&
          ~props.card.filestack[0].mimetype.indexOf('audio/'))
      ) {
        if (props.card.filestack.length > 0 || props.card.cardSubtype === 'image') {
          activeType = 'upload';
        }
      }
      if (props.card.cardSubtype === 'text') {
        activeType = 'text';
      }

      if (props.card.cardType === 'project') {
        activeType = 'project';
      }

      if (props.card.cardType === 'poll' && !props.card.isAssessment) {
        activeType = 'poll';
      }
      if (props.card.cardType === 'poll' && props.card.isAssessment) {
        activeType = 'quiz';
      }
      if (
        props.card.cardType === 'media' &&
        (props.card.cardSubtype === 'file' || props.card.cardSubtype === null)
      ) {
        activeType = 'upload';
      }
    }

    this.state = {
      activeType,
      isQuizEnable: window.ldclient.variation('quiz-smart-card', false),
      titleLabel: props.card ? 'Update' : 'Create',
      limit: 12,
      titleBlank: false,
      uploadIteration: 0
    };
    this.openContentTab = this.openContentTab.bind(this);
    this.showCourseTab = false;
  }

  closeModal = () => {
    this.setState({ activeType: 'link' });
    this.props.dispatch(close());
    document.getElementsByClassName('createButton')[0].focus();
  };

  openContentTab = () => {
    if (this.props.pathname.indexOf('/me/') === -1) {
      this.props.dispatch(push('/me/content'));
    } else {
      window.location.href = '/me/content';
    }
  };

  createdCard = data => {
    this.setState({ activeType: 'link' });
    let slugOrId, currentChannel, isAddingToCurrentChannel;
    if (data && data.card && this.props.pathname.indexOf('/channel/') > -1) {
      let routeParam = this.props.pathname.split('/');
      slugOrId = routeParam[routeParam.length - 1];
      currentChannel = this.props.channelsV2 && Object.values(this.props.channelsV2);
      currentChannel = currentChannel.length && currentChannel[0];
      isAddingToCurrentChannel =
        currentChannel && data.card.channelIds.indexOf(currentChannel.id) > -1;
    }
    if (
      currentChannel &&
      slugOrId &&
      (slugOrId == currentChannel.id || slugOrId === currentChannel.slug)
    ) {
      this.props.dispatch(close());
      if (isAddingToCurrentChannel) {
        updateChannel(this.props.dispatch, currentChannel, this.state.limit, true);
      }
    } else if (data) {
      this.props.dispatch(
        close(),
        this.props.cardUpdated ? this.props.cardUpdated(data) : this.openContentTab()
      );
    } else {
      this.props.dispatch(close());
    }
  };

  changeActiveType = (e, type) => {
    e.preventDefault();
    //if this.props.card is being returned, the smartbite is being edited (and already created), do not let user change between tabs
    if (this.props.card) {
      return;
    }
    if (this.state.activeType != type || type === 'upload') {
      let uploadIteration =
        type === 'upload' ? ++this.state.uploadIteration : this.state.uploadIteration;
      this.setState({
        activeType: type,
        uploadIteration
      });
    }
  };

  calcIconBlockClass = (type, first) => {
    let className =
      this.state.titleLabel === 'Update' && this.showCourseTab
        ? `${first && 'small-offset-1'} small-2 type-block ${
            this.state.activeType !== type && this.props.card ? 'not-allowed' : ''
          }`
        : `small-auto type-block ${
            this.state.activeType !== type && this.props.card ? 'not-allowed' : ''
          }`;
    return className;
  };

  componentDidMount() {
    var modalElement = document.getElementById('smartbit-creation-editing-modal');
    if (modalElement != null) {
      modalElement.parentElement.style.maxHeight = '100%';
    }
    this.isTitleBlank();
    this.props.dispatch(closeSnackBar());
  }

  isTitleBlank = () => {
    if (this.state.activeType == 'upload') {
      if (
        (this.props.card && this.props.card.message == this.props.card.filestack[0].handle) ||
        (this.props.card && this.props.card.message === '')
      ) {
        this.setState({ titleBlank: true });
      } else {
        this.setState({ titleBlank: false });
      }
    }
  };

  render() {
    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    let isAbleUploadScorm =
      Permissions['enabled'] !== undefined && Permissions.has('UPLOAD_SCORM_CONTENT'); //EP-15959: removed autoMarkAsCompleteScorm
    let isAbleUpload = Permissions['enabled'] !== undefined && Permissions.has('UPLOAD');
    let allowLinkAdd = Permissions['enabled'] !== undefined && Permissions.has('PUBLISH_LINKS');
    let isAbleToUploadTextCard =
      Permissions['enabled'] !== undefined && Permissions.has('CREATE_TEXT_CARD');
    return (
      <div className="smartbite-creation" id="smartbit-creation-editing-modal">
        <div className="smartbite-header">
          <span className="header-title">{tr(this.state.titleLabel) + ' SmartCard'}</span>
          <div className="close close-button">
            <IconButton
              style={{ paddingRight: 0, width: 'auto' }}
              aria-label="close"
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
            <TextField
              aria-hidden="true"
              name="smartbite"
              autoFocus={true}
              className="hiddenTextField"
            />
          </div>
        </div>
        <div className="smartbite-content">
          <div>
            <div className="row card-types-list">
              <TextField autoFocus={true} name="smartBiteEditor" className="hiddenTextField" />
              {(allowLinkAdd || this.state.titleLabel === 'Update') && (
                <a
                  href="#"
                  role="button"
                  aria-label="create link smart card"
                  className={this.calcIconBlockClass('link', true)}
                  onClick={e => this.changeActiveType(e, 'link')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'link' ? 'icon-opacity' : ''}>
                    <div className="icon-card link-card">
                      <LinkIcon fill="white" viewBox="0 0 20 20" />
                    </div>
                    <div className="text-center">{tr('Link')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              )}
              {isAbleUpload || this.state.titleLabel === 'Update' ? (
                <a
                  href="#"
                  role="button"
                  aria-label="create upload smart card"
                  className={this.calcIconBlockClass('upload', false)}
                  onClick={e => this.changeActiveType(e, 'upload')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'upload' ? 'icon-opacity' : ''}>
                    <div className="icon-card upload-card">
                      <UploadIcon color={'white'} viewBox="0 0 24 24" />
                    </div>
                    <div className="text-center">{tr('Upload')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              ) : null}
              <a
                href="#"
                role="button"
                aria-label="create poll smart card"
                className={this.calcIconBlockClass('poll', false)}
                onClick={e => this.changeActiveType(e, 'poll')}
              >
                {!useFormLabels && <div className="vertical-line" />}
                <div className={this.state.activeType !== 'poll' ? 'icon-opacity' : ''}>
                  <div className="icon-card poll-card">
                    <PollIcon fill="white" viewBox="-3 0 24 24" />
                  </div>
                  <div className="text-center">{tr('Poll')}</div>
                </div>
                <div className="horizontal-line" />
              </a>
              {this.state.isQuizEnable && (
                <a
                  href="#"
                  role="button"
                  aria-label="create quiz smart card"
                  className={this.calcIconBlockClass('quiz', false)}
                  onClick={e => this.changeActiveType(e, 'quiz')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'quiz' ? 'icon-opacity' : ''}>
                    <div className="icon-card quiz-card">
                      <div className="quiz-card-icon">Q</div>
                    </div>
                    <div className="text-center">{tr('Quiz')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              )}
              {isAbleToUploadTextCard && (
                <a
                  href="#"
                  role="button"
                  aria-label="create text smart card"
                  className={this.calcIconBlockClass('text', false)}
                  onClick={e => this.changeActiveType(e, 'text')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'text' ? 'icon-opacity' : ''}>
                    <div className="icon-card text-card">
                      <TextIcon fill="white" viewBox="0 0 27 13" />
                    </div>
                    <div className="text-center">{tr('Text')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              )}
              {this.state.titleLabel === 'Update' && this.showCourseTab && (
                <a
                  href="#"
                  role="button"
                  aria-label="create course smart card"
                  className={this.calcIconBlockClass('course', false)}
                  onClick={e => this.changeActiveType(e, 'course')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'course' ? 'icon-opacity' : ''}>
                    <div className="icon-card course-card">
                      <Course color={'white'} viewBox="0 0 24 24" />
                    </div>
                    <div className="text-center">{tr('Course')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              )}
              {isAbleUploadScorm && (
                <a
                  href="#"
                  role="button"
                  aria-label="create scorm smart card"
                  className={this.calcIconBlockClass('scorm', false)}
                  onClick={e => this.changeActiveType(e, 'scorm')}
                >
                  {!useFormLabels && <div className="vertical-line" />}
                  <div className={this.state.activeType !== 'scorm' ? 'icon-opacity' : ''}>
                    <div className="icon-card text-card">
                      <Scorm fill="white" viewBox="-4 0 27 17" />
                    </div>
                    <div className="text-center">{tr('SCORM')}</div>
                  </div>
                  <div className="horizontal-line" />
                </a>
              )}
              {this.state.titleLabel === 'Update' &&
                (this.props.card && this.props.card.cardType === 'project') && (
                  <a
                    href="#"
                    role="button"
                    aria-label="create project smart card"
                    className={this.calcIconBlockClass('project', false)}
                    onClick={e => this.changeActiveType(e, 'project')}
                  >
                    {!useFormLabels && <div className="vertical-line" />}
                    <div className={this.state.activeType !== 'project' ? 'icon-opacity' : ''}>
                      <div className="icon-card project-card">
                        <Project fill="white" viewBox="9 -5 46 70" />
                      </div>
                      <div className="text-center">{tr('Project')}</div>
                    </div>
                    <div className="horizontal-line" />
                  </a>
                )}
            </div>
            <div className="overlined" />
          </div>
          <SmartBite
            card={this.props.card}
            channelsV2={this.props.channelsV2}
            closeClick={true}
            createdCard={this.createdCard}
            activeType={this.state.activeType}
            titleblank={this.state.titleBlank}
            cardType="smartbite"
            uploadIteration={this.state.uploadIteration}
          />
        </div>
      </div>
    );
  }
}

SmartbiteEditor.propTypes = {
  card: PropTypes.object,
  pathname: PropTypes.string,
  cardUpdated: PropTypes.func,
  team: PropTypes.object,
  channelsV2: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS(),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS(),
    channelsV2: state.channelsV2.toJS()
  };
}
export default connect(mapStoreStateToProps)(SmartbiteEditor);
