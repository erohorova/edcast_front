import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import TextField from 'material-ui/TextField';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';

import User from '../common/User';
import { close } from '../../actions/modalActions';
import { getGroupUsers } from '../../actions/groupsActionsV2';
import { Permissions } from '../../utils/checkPermissions';

class GroupMemberModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };

    this.styles = {
      roleTitle: { color: 'white', paddingBottom: '10px', fontSize: '18px' }
    };
  }

  componentDidMount() {
    let payloadObject = { limit: 15, offset: 0 };
    let groupIdentification = this.props.groupsV2.currentGroupID;
    let currentGroup = this.props.groupsV2[groupIdentification];
    let membersObj = (currentGroup && currentGroup.members) || [];
    let pendingObj = (currentGroup && currentGroup.pending) || [];

    //Get All Members API calls
    if (this.props.userType === 'members' && (!membersObj.length || this.props.isInitilized)) {
      this.setState({ loading: true });
      this.props
        .dispatch(getGroupUsers(groupIdentification, payloadObject))
        .then(() => {
          this.setState({
            loading: false
          });
        })
        .catch(err => {
          console.error(`Error in GroupMemberModal.getGroupUsers.func members: ${err}`);
        });
    }
    //Get Pending Members API calls
    if (this.props.userType === 'pending' && (!pendingObj.length || this.props.isInitilized)) {
      this.setState({ loading: true });
      payloadObject = { ...payloadObject, user_type: 'pending' };
      this.props
        .dispatch(getGroupUsers(groupIdentification, payloadObject))
        .then(() => {
          this.setState({
            loading: false
          });
        })
        .catch(err => {
          console.error(`Error in GroupMemberModal.getGroupUsers.func pending : ${err}`);
        });
    }
  }
  closeModal = () => {
    this.props.dispatch(close());
  };

  viewMoreClickHandler = currentOption => {
    this.setState({ loading: true });
    let groupID = this.props.groupsV2.currentGroupID;
    let limit = this.props.groupsV2[groupID].membersLimit;
    let offset = this.props.groupsV2[groupID].membersOffset + limit;
    let payload = {
      limit,
      offset
    };
    if (currentOption == 1) {
      limit = this.props.groupsV2[groupID].pendingLimit;
      offset = this.props.groupsV2[groupID].pendingOffset + limit;
      payload = {
        limit,
        offset,
        user_type: 'pending'
      };
    }
    this.props
      .dispatch(getGroupUsers(groupID, payload))
      .then(() => {
        this.setState({
          loading: false
        });
      })
      .catch(err => {
        console.error(`Error in GroupMemberModal.getGroupUsers.func : ${err}`);
      });
  };

  leadersCounting = () => {
    let currentGroup = this.props.groupsV2[this.props.groupsV2.currentGroupID];
    let membersObj = currentGroup && currentGroup.members;
    let count = 0;
    for (let i = 0; i < membersObj.length; i++) {
      if (membersObj[i].role === 'admin') {
        count++;
      }
    }
    return count === 1;
  };

  render() {
    let groupID = this.props.groupsV2.currentGroupID + '';
    let currentGroup = this.props.groupsV2[this.props.groupsV2.currentGroupID];
    let membersObj = currentGroup && currentGroup.members;
    let pendingObj = currentGroup && currentGroup.pending;
    let membersTotal = currentGroup && currentGroup.membersTotal;
    let pendingTotal = currentGroup && currentGroup.pendingTotal;
    let currentUserId = this.props.currentUser.id;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let containerHeight = 460;
    let displayRemoveButton = !currentGroup.isDynamic;
    return (
      <div className="channel-cards-modal padding-bottom-30">
        <div className="row">
          <TextField name="groupmembermodal" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>

          {this.props.userType === 'members' && (
            <div className="small-12">
              <div style={this.styles.roleTitle}>{`${tr('Group Members')} (${membersTotal})`}</div>

              <div className="small-12 columns">
                <div className="custom-card-container">
                  <div className="vertical-spacing-medium row ">
                    {membersObj &&
                      membersObj.length > 0 &&
                      membersObj.map(user => {
                        let roles = [user.role === 'admin' ? 'groupLeader' : 'groupMember'];
                        return (
                          <div
                            key={user.id}
                            className={`channel-card-v2 card-v2 custom-medium-6 group-members small-12 column show-overlay`}
                          >
                            <User
                              id={user.id}
                              name={user.name}
                              handle={user.handle}
                              imageUrl={user.photo || defaultUserImage}
                              pending={user.pending}
                              interests={user.interests}
                              roles={roles}
                              following={user.isFollowed}
                              teamID={groupID}
                              isCurrentUser={user.id == currentUserId}
                              isTeamAdmin={
                                currentGroup.isTeamAdmin ||
                                (currentGroup.isTeamSubAdmin && Permissions.has('MANAGE_USERS')) ||
                                Permissions.has('ADMIN_ONLY')
                              }
                              leadersCounting={this.leadersCounting}
                              displayRemoveButton={user.id != currentUserId && displayRemoveButton}
                            />
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>

              <div className="small-12 columns row">
                <div className="small-12 columns text-center">
                  {membersObj && membersObj.length < membersTotal && (
                    <PrimaryButton
                      label={this.state.loading ? tr('Loading...') : tr('View More')}
                      className="view-more"
                      disabled={this.state.loading}
                      onTouchTap={this.viewMoreClickHandler.bind(this, 2)}
                    />
                  )}
                </div>
              </div>
            </div>
          )}

          {this.props.userType === 'pending' && (
            <div className="small-12">
              <div style={this.styles.roleTitle}>
                {`${tr('Pending Members')} (${pendingTotal})`}
              </div>

              <div className="small-12 columns">
                <div className="custom-card-container">
                  <div className="vertical-spacing-medium row ">
                    {pendingObj &&
                      pendingObj.length > 0 &&
                      pendingObj.map(user => {
                        return (
                          <div
                            key={user.id}
                            className={`channel-card-v2 card-v2 custom-medium-6 group-members small-12 column show-overlay`}
                          >
                            <User
                              id={user.userId || user.id}
                              name={user.name || user.email}
                              handle={user.handle}
                              imageUrl={user.photo || defaultUserImage}
                              pending={true}
                              following={user.isFollowing || user.isFollowed}
                              disableLink={!user.handle}
                              isCurrentUser={user.id == currentUserId}
                              teamID={groupID}
                              isTeamAdmin={
                                currentGroup.isTeamAdmin ||
                                (this.props.currentUser && this.props.currentUser.isAdmin)
                              }
                              leadersCounting={this.leadersCounting}
                              displayRemoveButtonPending={
                                user.id != currentUserId && displayRemoveButton
                              }
                            />
                          </div>
                        );
                      })}
                  </div>
                </div>
                <div className="small-12 columns row">
                  <div className="small-12 columns text-center">
                    {pendingObj && pendingObj.length < pendingTotal && (
                      <button
                        disabled={this.state.loading}
                        className="view-more-v2"
                        onClick={this.viewMoreClickHandler.bind(this, 1)}
                      >
                        {this.state.loading ? tr('Loading...') : tr('View More')}
                      </button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

GroupMemberModal.propTypes = {
  groupsV2: PropTypes.object,
  currentUser: PropTypes.object,
  userType: PropTypes.string,
  isInitilized: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupMemberModal);
