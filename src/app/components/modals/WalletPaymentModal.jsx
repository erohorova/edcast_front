/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import WalletPaymentDetails from '../../components/payment/WalletPaymentDetails';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { close } from '../../actions/modalActions';
let LocaleCurrency = require('locale-currency');

class WalletPaymentModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      paymentError: '',
      titleLabel: '',
      sentDetails: false,
      showForm: false,
      disabled: true,
      priceData: null,
      orgSubscriptions: []
    };
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    let user = this.props.currentUser;
    return (
      <div id="org-payment-modal">
        <div className="payment-header">
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div className="modal-actions" style={{ marginBottom: '40px' }}>
          <div className="card-payment-modal">
            <div className="card-payment-header">
              <h5>Complete your transaction</h5>
            </div>
            <div className="card-container">
              <div>
                <WalletPaymentDetails rechargeData={this.props.rechargeData} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

WalletPaymentModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  callback: PropTypes.func,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  title: PropTypes.string,
  rechargeData: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(WalletPaymentModal);
