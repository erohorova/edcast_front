import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import throttle from 'lodash/throttle';
import orderBy from 'lodash/orderBy';
import { Scrollbars } from 'react-custom-scrollbars';
import { tr } from 'edc-web-sdk/helpers/translations';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import { close } from '../../actions/modalActions';
import Spinner from '../common/spinner';

import Paper from 'edc-web-sdk/components/Paper';
import { PrimaryButton } from 'edc-web-sdk/components/index';
import colors from 'edc-web-sdk/components/colors/index';

class PreviewDSModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    this.styles = {
      previewContainer: {
        width: '58.125rem',
        margin: '0 auto'
      },
      modalHeader: {
        position: 'relative',
        marginBottom: '0.625rem'
      },
      title: {
        color: 'white',
        fontSize: '1.125rem'
      },
      close: {
        paddingRight: 0,
        width: 'auto'
      },
      back_button: {
        width: '4.875rem',
        backgroundColor: '#acadc1',
        color: '#fff',
        borderRadius: '0.125rem',
        border: '0'
      },
      contentBlock: {
        border: 'none',
        whiteSpace: 'nowrap'
      },
      autoWidth: {
        width: 'auto'
      }
    };
    if (props.fromGroupCreation) {
      this.styles.title = { ...this.styles.title, display: 'flex', alignItems: 'center' };
    }
    this.name = props.fromGroupCreation
      ? 'Group'
      : props.currentAction === 'assign'
      ? 'Assign'
      : props.currentAction === 'invite'
      ? 'Invite New Users'
      : 'Share';
  }

  handleScroll = throttle(
    e => {
      if (this.props.isPendingPreview || this.props.isAllPreviewLoaded) {
        return;
      }
      if (e.clientHeight + e.scrollTop === e.scrollHeight) {
        this.props.loadMoreUsers();
      }
    },
    150,
    { leading: false }
  );

  backHandle = () => {
    this.props.backHandle();
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  previewUsers = () => {
    let customFieldsFilters = this.props.previewFilter
      .filter(item => item.id !== -1)
      .map(item => item.type);
    return (
      <table className="preview-users-table">
        <tr>
          <td>
            <span className="user-name preview-header">{tr('Name')}</span>
          </td>
          {customFieldsFilters.map(item => (
            <td key={item}>
              <span className="user-name preview-header">{item}</span>
            </td>
          ))}
        </tr>
        {this.props.previewData.map(user => {
          return (
            <tr key={user.id}>
              <td nowrap>
                <div>
                  <span className="user-name">{user.fullName}</span>
                  <span className="user-name user-name__light">{` (${user.handle})`}</span>
                </div>
              </td>
              {customFieldsFilters.map(item => {
                let foundField = user.customFieldsData.customFields.find(
                  field => field.displayName === item
                );
                return (
                  <td key={item}>
                    <span className={`user-name ${foundField ? '' : 'user-name__empty'}`}>
                      {(foundField && foundField.value) || '-'}
                    </span>
                  </td>
                );
              })}
            </tr>
          );
        })}
      </table>
    );
  };

  render() {
    let customHeaders =
      this.props.customFieldsList &&
      orderBy(this.props.customFieldsList, 'id', 'asc').filter(item =>
        this.props.previewFilter.find(filter => filter.type === item.abbreviation)
      );
    let previewDataExists = !!this.props.previewData && !!this.props.previewData.length;
    let containerWidth =
      (customHeaders && customHeaders.length && (customHeaders.length + 1) * 220 + 6) || 'auto';
    return (
      <div style={this.styles.previewContainer} className="ma-preview-data">
        <div className="row">
          <div
            className={`small-12 columns ${
              this.props.fromGroupCreation ? 'section-from-group' : ''
            }`}
            style={this.styles.modalHeader}
          >
            <div style={this.styles.title}>{tr(this.name)}</div>
            <div className="close close-button">
              <IconButton style={this.styles.close} aria-label="close" onTouchTap={this.closeModal}>
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper>
              <div
                className={`share-content-modal-container ${
                  this.props.currentAction ? 'ma-container' : ''
                }`}
              >
                <div
                  className={`sc_tabs_container ${
                    this.props.fromGroupCreation ? 'sc_tabs_container__extra-margin' : ''
                  }`}
                >
                  <div
                    className={`active  ${this.props.currentAction ? 'ma-tab' : ''}`}
                    style={this.styles.autoWidth}
                  >
                    <span className={this.props.currentAction ? 'ma-preview__header' : ''}>
                      {tr('Preview Data')}
                    </span>
                  </div>
                </div>
                <Scrollbars
                  style={{ height: 340 }}
                  className={`sc_tabs_content_container ${
                    this.props.fromGroupCreation ? 'sc_tabs_container__extra-margin' : ''
                  }`}
                >
                  {!!this.props.currentAction && previewDataExists && (
                    <div className="ma-row__header">
                      <span className="ma-preview__list-header">{tr('Name')}</span>
                      {customHeaders.map(item => {
                        return <span className="ma-preview__list-header">{item.displayName}</span>;
                      })}
                    </div>
                  )}
                  <Scrollbars
                    style={{ width: containerWidth, height: 280 }}
                    id="ma-individual-list-content"
                    className="ma-individual"
                    onScrollFrame={this.handleScroll}
                  >
                    {this.props.fromGroupCreation
                      ? this.previewUsers()
                      : previewDataExists && (
                          <div className="ma-individual">
                            {this.props.previewData.map(user => {
                              return (
                                <div
                                  className="clip-container clip-container-block list-item-wrapper"
                                  style={this.styles.contentBlock}
                                  key={`user-${user.id}`}
                                >
                                  {!this.props.currentAction && (
                                    <img
                                      className="user-avatar"
                                      src={
                                        (user.avatarimages && user.avatarimages.tiny) ||
                                        this.defaultUserImage
                                      }
                                    />
                                  )}
                                  <div className="preview-row-item">
                                    <div className="clip-main-text">
                                      {user.fullName || user.name}
                                    </div>
                                    <div className="clip-handle-text">({user.handle})</div>
                                  </div>
                                  {user.customFields &&
                                    user.customFields
                                      .filter(field =>
                                        customHeaders.find(header => header.id == field.cfId)
                                      )
                                      .map(field => {
                                        return (
                                          <div className="clip-main-text preview-row-item">
                                            {field.cfContent}
                                          </div>
                                        );
                                      })}
                                </div>
                              );
                            })}
                          </div>
                        )}
                    {this.props.isPendingPreview && (
                      <div className="channel-loading">
                        <Spinner />
                      </div>
                    )}
                  </Scrollbars>
                </Scrollbars>

                <div className="text-center sc-button-container">
                  <PrimaryButton
                    style={this.styles.back_button}
                    onTouchTap={this.backHandle}
                    label={tr('Back')}
                    theme={colors.followColor}
                  />
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

PreviewDSModal.propTypes = {
  backHandle: PropTypes.func,
  previewData: PropTypes.array,
  customFieldsList: PropTypes.array,
  currentAction: PropTypes.string,
  loadMoreUsers: PropTypes.func,
  isPendingPreview: PropTypes.bool,
  isAllPreviewLoaded: PropTypes.bool,
  fromGroupCreation: PropTypes.bool,
  previewFilter: PropTypes.array
};

export default connect()(PreviewDSModal);
