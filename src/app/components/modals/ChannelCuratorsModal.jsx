import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import AddCurators from '../channel/AddCurators';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';
class ChannelCuratorsModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      paper: {
        padding: '16px'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    return (
      <div className="channel-curators-modal">
        <div className="row">
          <TextField name="channelcurate" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            <div style={{ color: 'white' }}>{tr('Edit Curators')}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              <div className="row">
                <div className="small-12 medium-2 columns">
                  <div
                    style={{
                      color: '#454560',
                      fontSize: '12px',
                      height: '24px',
                      lineHeight: '24px'
                    }}
                  >
                    Curators
                  </div>
                </div>
                <div className="small-12 medium-10 columns">
                  <AddCurators
                    currentUserId={this.props.currentUserId}
                    channelId={this.props.channelId}
                    curators={this.props.curators}
                    closeModal={this.closeModal}
                  />
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

ChannelCuratorsModal.propTypes = {
  currentUserId: PropTypes.any,
  channelId: PropTypes.number,
  curators: PropTypes.any
};

export default connect()(ChannelCuratorsModal);
