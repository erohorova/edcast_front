import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import ConfirmationModal from './ConfirmationModal.jsx';
import AssignModal from './AssignModal.jsx';
import CardStatsModal from './CardStatsModal.jsx';
import CardLikesModal from './CardActionsModal.jsx';
import FollowingUserListModal from './FollowingUserListModal';
import AddToPathwayModalNew from './AddToPathwayModalNew';
import AddToJourneyModal from './AddToJourneyModal';
import InlineCreation from './InsightCreation';
import SmartbiteEditor from './SmartbiteEditor';
import SlideOutCardModal from './SlideOutCardModal';
import PathwayEditor from './PathwayEditor';
import JourneyEditor from './JourneyEditor';
import JourneyEditorV2 from './JourneyEditorV2';
import InviteUserModal from './InviteUserModal';
import InviteV2UserModal from './InviteV2UserModal';
import UploadImage from './UploadImage';
import UpdateInterestsModal from './UpdateInterestsModal';
import UpdateInterestsOnBoardV2Modal from './UpdateInterestsOnBoardV2Modal';
import UpdateMultiLevelInterestsModal from './UpdateMultiLevelInterestsModal';
import UpdateExpertiseModal from './UpdateExpertiseModal';
import UpdateChannelDetailsModal from './UpdateChannelDetailsModal';
import UpdateGroupDetailsModal from './UpdateGroupDetailsModal';
import InsightEditModal from './InsightEditModal';
import IntegrationModal from './IntegrationModal';
import StatusModal from './StatusModal';
import PathwayOverviewModal from './PathwayOverviewModal';
import JourneyOverviewModal from './JourneyOverviewModal';
import JourneyOverviewModalV2 from './JourneyOverviewModalV2';
import SmartBiteOverviewModal from './SmartBiteOverviewModal';
import GroupCreationModal from './GroupCreationModal';
import GroupCreationModalV3 from './GroupCreationModalV3';
import ChannelCardsModal from './ChannelCardsModal';
import ChannelCardsModalv2 from './ChannelCardsModalv2';
import ChannelEditorModal from './ChannelEditorModal';
import BookmarkConfirm from './BookmarkConfirm';
import TeamCardsModal from './TeamCardsModal';
import TeamCardsModalv2 from './TeamCardsModalv2';
import GroupMemberModal from './GroupMemberModal';
import ChannelCuratorsModal from './ChannelCuratorsModal';
import GroupInviteModal from './GroupInviteModal';
import ChannelCardRemoveConfirmationModal from './ChannelCardRemoveConfirmationModal';
import * as modalTypes from '../../constants/modalTypes';
import Dialog from 'material-ui/Dialog';
import { close } from '../../actions/modalActions';
import CurateCardsModal from './CurateCardsModal';
import CongratulationModal from './CongratulationModal';
import RelevancyRatingModal from './RelevancyRatingModal';
import ShareToGroupModal from './ShareToGroupModal';
import PostToChannelModal from './PostToChannelModal';
import ShowChannelsModal from './ShowChannelsModal';
import SkillModal from './SkillModal';
import PaypalSuccessModal from './PaypalSuccessModal';
import TeamAnalyticsUserList from './TeamAnalyticsUserList';
import ReasonModal from './ReasonModal';
import ShareContentModal from './ShareContentModal';
import ShareAndCreateGroupModal from './ShareAndCreateGroupModal';
import MultiactionsModal from './MultiactionsModal';
import MDPModal from './MDPModal';
import SkillsDirectoryModal from './SkillsDirectoryModal';
import Spinner from '../common/spinner';

import CourseCardModal from './CourseCardModal';

import ChangeAuthorModal from './ChangeAuthorModal';
import CurateCardsModalV2 from './CurateCardsModalV2';
import ChannelCarouselCardsModal from './ChannelCarouselCardsModal';

const OrgPaymentModal = Loadable({
  loader: () => import('./OrgPaymentModal'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

const WalletPaymentModal = Loadable({
  loader: () => import('./WalletPaymentModal'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

const CardWalletPaymentModal = Loadable({
  loader: () => import('./CardWalletPaymentModal'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

class ModalContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      dialogRoot: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1502,
        paddingTop: 0
      },
      dialogContent: {
        position: 'relative',
        width: '90vw',
        transform: 'none'
      },
      dialogBody: {
        paddingBottom: 0,
        overflowX: 'hidden'
      }
    };
    this.performanceImprovedUI = window.ldclient.variation('new-performance-improved-ui', false);
    this.journeyEnhancement = window.ldclient.variation('journey-enhancement', false);
    this.groupCreationModalV3 = window.ldclient.variation('group-creation-modal-v3', false);
    this.inlineCreationPathnames = ['/', '/required', '/featured', '/curate', '/teamLearning'];
  }

  componentDidMount() {
    window.addEventListener('keydown', this.closeModalOnESC);
  }

  closeModalOnESC = e => {
    let fileStackModalOpen = document.getElementsByClassName('fsp-picker').length;
    let cardTypePopupobj = document.getElementsByClassName('cardTypePopupIsActive').length;
    let type = this.props.type || this.props.typeBefore;
    if (e.keyCode == 27 && !fileStackModalOpen && !cardTypePopupobj) {
      this.props.dispatch(close());
      if (
        type == modalTypes.SMARTBITE_CREATION ||
        type == modalTypes.PATHWAY_CREATION ||
        type == modalTypes.JOURNEY_CREATION
      ) {
        document.getElementsByClassName('createButton')[0].focus();
      }
    }
  };

  render() {
    let scrollY = true;
    let autoHeight = true;
    let ComponentVar;
    let open = this.props.open || this.props.openCard || false;
    let clickAway = false;
    let customBodyClass;
    let pathwayStyle = {};
    let overlayStyle = {};
    let statusModalStyle = {};
    let smartBiteStyle = {};
    let bodyStyle = {};
    let customContentClass;
    let customModalClass = '';
    let type = this.props.type || this.props.typeBefore;
    let cardV3 = window.ldclient.variation('card-v3', false);
    switch (type) {
      case modalTypes.ASSIGN:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <AssignModal
            open={this.props.open}
            card={this.props.card}
            selfAssign={this.props.selfAssign}
            assignedStateChange={this.props.assignedStateChange}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CARD_STATS:
        ComponentVar = (
          <CardStatsModal
            open={this.props.open}
            card={this.props.card}
            isViewsModal={this.props.isViewsModal}
          />
        );
        customBodyClass = 'stats-dialog';
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CARD_ACTION:
        ComponentVar = (
          <CardLikesModal
            open={this.props.open}
            card={this.props.card}
            typeOfModal={this.props.typeOfModal}
          />
        );
        customBodyClass = 'standard-dialog';
        customModalClass = 'modalContainer';
        pathwayStyle = {
          width: '400px'
        };
        break;
      case modalTypes.FOLLOW_USER_LIST:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <FollowingUserListModal open={this.props.open} type={this.props.typeOfModal} />
        );
        customModalClass = 'modalContainer';
        pathwayStyle = {
          width: '400px'
        };
        break;
      case modalTypes.INTEGRATION:
        ComponentVar = (
          <IntegrationModal
            open={this.props.open}
            importClickHandler={this.props.importClickHandler}
            integration={this.props.integration}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CONFIRM:
        ComponentVar = (
          <ConfirmationModal
            title={this.props.title}
            message={this.props.message}
            callback={this.props.callback}
            hideCancelBtn={!!this.props.hideCancelBtn}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CONFIRM_PRIVATE_CARD:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <ConfirmationModal
            title={this.props.title}
            message={this.props.message}
            isPrivate={this.props.isPrivate}
            callback={this.props.callback}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.ADD_TO_PATHWAY:
        clickAway = true;
        customBodyClass = 'standard-dialog';
        customModalClass = 'modalContainer';
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <AddToPathwayModalNew
            card={this.props.card}
            cardId={this.props.cardId}
            cardType={this.props.cardType}
            isAddingToJourney={this.props.isAddingToJourney}
          />
        );
        break;
      case modalTypes.ADD_TO_JOURNEY:
        clickAway = true;
        customBodyClass = 'standard-dialog';
        customModalClass = 'modalContainer';
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = <AddToJourneyModal cardId={this.props.cardId} />;
        break;
      case modalTypes.INLINE_CREATION:
        ComponentVar = <InlineCreation expand={true} />;
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.STATUS_MODAL:
        overlayStyle = {
          backgroundColor: 'transparent'
        };
        statusModalStyle = {
          padding: 0
        };
        ComponentVar = (
          <StatusModal
            expand={true}
            show={open}
            statusMessage={this.props.statusMessage}
            handleCloseModal={this.props.handleCloseModal}
            statusBlock={this.props.statusBlock}
          />
        );
        open = true;
        customContentClass = 'status-message-modal';
        break;
      case modalTypes.PATHWAY_PREVIEW_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '1016px',
          bottom: '16px'
        };
        statusModalStyle = {
          padding: '16px',
          paddingBottom: '16px'
        };
        bodyStyle = {
          overflow: 'visible'
        };
        if (this.props.channelSetting || this.props.groupSetting) {
          customContentClass = 'channel-overview-modal';
        }

        ComponentVar = (
          <PathwayOverviewModal
            card={this.props.card}
            checkedCardId={this.props.checkedCardId}
            logoObj={this.props.logoObj}
            openChannelModal={this.props.openChannelModal}
            cardUpdated={this.props.cardUpdated}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            defaultImage={this.props.defaultImage}
            deleteSharedCard={this.props.deleteSharedCard}
            isStandaloneModal={this.props.isStandaloneModal}
          />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.JOURNEY_PREVIEW_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '1016px',
          bottom: '16px'
        };
        statusModalStyle = {
          padding: '16px',
          paddingBottom: '16px'
        };
        bodyStyle = {
          overflow: 'visible'
        };
        if (this.props.channelSetting || this.props.groupSetting) {
          customContentClass = 'channel-overview-modal';
        }
        ComponentVar = this.journeyEnhancement ? (
          <JourneyOverviewModalV2
            card={this.props.card}
            checkedCardId={this.props.checkedCardId}
            logoObj={this.props.logoObj}
            openChannelModal={this.props.openChannelModal}
            cardUpdated={this.props.cardUpdated}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            defaultImage={this.props.defaultImage}
            deleteSharedCard={this.props.deleteSharedCard}
            isStandaloneModal={this.props.isStandaloneModal}
          />
        ) : (
          <JourneyOverviewModal
            card={this.props.card}
            checkedCardId={this.props.checkedCardId}
            logoObj={this.props.logoObj}
            openChannelModal={this.props.openChannelModal}
            cardUpdated={this.props.cardUpdated}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            defaultImage={this.props.defaultImage}
            deleteSharedCard={this.props.deleteSharedCard}
            isStandaloneModal={this.props.isStandaloneModal}
          />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.SMARTBITE_PREVIEW_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '1016px',
          bottom: '16px'
        };
        statusModalStyle = {
          padding: '16px',
          paddingBottom: '16px'
        };
        bodyStyle = {
          overflow: 'visible'
        };

        if (this.props.channelSetting || this.props.groupSetting) {
          customContentClass = ' channel-overview-modal';
        }
        ComponentVar = (
          <SmartBiteOverviewModal
            openChannelModal={this.props.openChannelModal}
            card={this.props.card}
            logoObj={this.props.logoObj}
            cardUpdated={this.props.cardUpdated}
            defaultImage={this.props.defaultImage}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            deleteSharedCard={this.props.deleteSharedCard}
          />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.SMARTBITE_CREATION:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <SmartbiteEditor card={this.props.card} cardUpdated={this.props.cardUpdated} />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.PATHWAY_CREATION:
        scrollY = false;
        autoHeight = false;
        pathwayStyle = {
          width: cardV3 ? '65.3rem' : '70vw',
          maxWidth: cardV3 ? 'auto' : '62.5rem'
        };
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <PathwayEditor pathway={this.props.card} cardUpdated={this.props.cardUpdated} />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.JOURNEY_CREATION:
        scrollY = false;
        autoHeight = false;
        pathwayStyle = {
          width: cardV3 ? '65.3rem' : '70vw',
          maxWidth: cardV3 ? 'auto' : '62.5rem'
        };
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = this.journeyEnhancement ? (
          <JourneyEditorV2 journey={this.props.card} cardUpdated={this.props.cardUpdated} />
        ) : (
          <JourneyEditor journey={this.props.card} cardUpdated={this.props.cardUpdated} />
        );
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.REASON_MODAL:
        scrollY = false;
        autoHeight = false;
        ComponentVar = <ReasonModal card={this.props.card} comment={this.props.comment} />;
        pathwayStyle = {
          width: '20.875rem',
          maxWidth: '400px'
        };
        statusModalStyle = {
          paddingTop: '1rem',
          paddingBottom: '1.2rem',
          paddingRight: '1rem',
          paddingLeft: '1rem'
        };
        bodyStyle = {
          overflow: 'visible'
        };
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.MDP_MODAL:
        scrollY = false;
        autoHeight = false;
        ComponentVar = <MDPModal card={this.props.card} comment={this.props.comment} />;
        pathwayStyle = {
          width: '33.875rem',
          maxWidth: '31.25rem'
        };
        statusModalStyle = {
          paddingTop: '1rem',
          paddingBottom: '1.2rem',
          paddingRight: '1rem',
          paddingLeft: '1rem'
        };
        bodyStyle = {
          overflow: 'visible'
        };
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_INTERESTS:
        clickAway = true;
        ComponentVar = <UpdateInterestsModal callback={this.props.callback} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_INTERESTS_ONBOARD_V2:
        clickAway = true;
        ComponentVar = <UpdateInterestsOnBoardV2Modal callback={this.props.callback} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_MULTILEVEL_INTERESTS:
        clickAway = true;
        ComponentVar = (
          <UpdateMultiLevelInterestsModal
            profileTopic="learningTopics"
            topicLimit={'interests_limit'}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_MULTILEVEL_EXPERTISE:
        clickAway = true;
        ComponentVar = (
          <UpdateMultiLevelInterestsModal
            profileTopic="expertTopics"
            topicLimit={'expertise_limit'}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_EXPERTISE:
        clickAway = true;
        ComponentVar = <UpdateExpertiseModal callback={this.props.callback} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPDATE_CHANNEL_DETAILS:
        customContentClass = 'channel-curators-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        ComponentVar = (
          <UpdateChannelDetailsModal
            id={this.props.id}
            label={this.props.label}
            description={this.props.description}
          />
        );
        break;
      case modalTypes.UPDATE_GROUP_DETAILS:
        customContentClass = 'channel-curators-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        ComponentVar = <UpdateGroupDetailsModal />;
        break;
      case modalTypes.INSIGHT_EDIT:
        ComponentVar = (
          <InsightEditModal card={this.props.card} cardUpdated={this.props.cardUpdated} />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.UPLOAD_IMAGE:
        scrollY = true;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = (
          <UploadImage
            sizes={this.props.sizes}
            imageType={this.props.imageType}
            updatedImage={this.props.updatedImage}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.INVITE_USER:
        ComponentVar = <InviteUserModal group={this.props.group} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.INVITE_V2_USER_TO_GROUP:
        ComponentVar = <InviteV2UserModal />;
        customContentClass = 'group-invite-dialog';
        customModalClass = 'modalContainer group-modal-root';
        break;
      case modalTypes.GROUP_CREATION:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: this.groupCreationModalV3 ? '8.625rem' : '55px',
          overflowY: 'auto'
        };
        bodyStyle = this.groupCreationModalV3
          ? {}
          : {
              overflow: 'visible'
            };
        pathwayStyle = this.groupCreationModalV3
          ? {
              maxWidth: '62.5rem'
            }
          : {};
        customBodyClass = 'group-creation-body';
        ComponentVar = this.groupCreationModalV3 ? (
          <GroupCreationModalV3
            updateGroupsList={this.props.updateGroupsList}
            groupId={this.props.groupId}
          />
        ) : (
          <GroupCreationModal
            updateGroupsList={this.props.updateGroupsList}
            groupId={this.props.groupId}
          />
        );
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CHANNEL_CARDS:
        ComponentVar = this.performanceImprovedUI ? (
          <ChannelCardsModalv2 channel={this.props.channel} />
        ) : (
          <ChannelCardsModal />
        );
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.UNBOOKMARK_MODAL:
        ComponentVar = (
          <BookmarkConfirm
            bookmarked={this.props.bookmarked}
            confirmHandler={this.props.confirmHandler}
          />
        );
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.GROUP_CARDS:
        let TeamCardsModalv2Comp = (
          <TeamCardsModalv2
            deleteSharedCard={this.props.deleteSharedCard}
            cards={this.props.cards}
            cardModalDetails={this.props.cardModalDetails}
            group={this.props.group}
          />
        );
        ComponentVar = this.performanceImprovedUI ? (
          TeamCardsModalv2Comp
        ) : (
          <TeamCardsModal deleteSharedCard={this.props.deleteSharedCard} />
        );
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.GROUP_MEMBERS:
        scrollY = false;
        smartBiteStyle = {
          height: 'auto',
          overflowY: 'auto'
        };
        bodyStyle = {
          overflowX: 'hidden',
          overflowY: 'auto'
        };
        ComponentVar = (
          <GroupMemberModal userType={this.props.userType} isInitilized={this.props.isInitilized} />
        );
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.GROUP_ADD_LEADER:
        ComponentVar = <GroupInviteModal />;
        customContentClass = 'channel-curators-dialog';
        customModalClass = 'channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.CHANNEL_ADD_CURATORS:
        ComponentVar = (
          <ChannelCuratorsModal
            channelId={this.props.channelId}
            curators={this.props.curators}
            currentUserId={this.props.currentUserId}
          />
        );
        customContentClass = 'channel-curators-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.CURATE_CARD_MODAL:
        ComponentVar = <CurateCardsModal channelId={this.props.channelId} />;
        customContentClass = 'curate-card-dialog';
        customModalClass = 'modalContainer curate-card-root channelGroupModalContainer';
        break;
      case modalTypes.CHANNEL_CARD_REMOVE:
        ComponentVar = (
          <ChannelCardRemoveConfirmationModal
            channel={this.props.channel}
            card={this.props.card}
            cardTypes={this.props.cardTypes}
            isReject={this.props.isReject}
            rejectCallback={this.props.rejectCallback}
          />
        );
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        customContentClass = 'channel-curators-dialog';
        break;
      case modalTypes.CONGRATULATION_MODAL:
        scrollY = false;
        autoHeight = true;
        pathwayStyle = {
          position: 'relative',
          width: '34vw',
          transform: 'none'
        };
        ComponentVar = <CongratulationModal userBadge={this.props.userBadge} />;
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.RELEVANCY_RATING_MODAL:
        ComponentVar = <RelevancyRatingModal card={this.props.card} />;
        customModalClass = 'relevancy-rating-root';
        customContentClass = 'relevancy-rating-dialog';
        break;
      case modalTypes.COURSES_MODAL:
        ComponentVar = (
          <CourseCardModal
            courseStatus={this.props.courseStatus}
            courseType={this.props.courseType}
          />
        );
        customModalClass = 'course-card-root';
        customContentClass = 'course-card-dialog';
        break;
      case modalTypes.SHARE_MODAL:
        ComponentVar = <ShareToGroupModal card={this.props.card} />;
        customModalClass = 'modalContainer relevancy-rating-root';
        customContentClass = 'relevancy-rating-dialog';
        break;
      case modalTypes.MULTIACTIONS_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '3.4375rem',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '62.5rem'
        };
        ComponentVar = (
          <MultiactionsModal
            card={this.props.card}
            currentAction={this.props.currentAction}
            selfAssign={this.props.selfAssign}
          />
        );
        customModalClass = 'modalContainer multiactions-root';
        customContentClass = 'multiactions-dialog';
        break;
      case modalTypes.POST_TO_CHANNEL_MODAL:
        ComponentVar = <PostToChannelModal card={this.props.card} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.SHOW_CHANNELS_MODAL:
        ComponentVar = <ShowChannelsModal removable={this.props.removable} />;
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      case modalTypes.SKILL_CREATION:
        scrollY = false;
        autoHeight = false;
        pathwayStyle = {
          width: '70vw',
          maxWidth: '1000px'
        };
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        ComponentVar = <SkillModal skill={this.props.skill} />;
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.ORG_PAYMENT:
        ComponentVar = <OrgPaymentModal />;
        customModalClass = 'modalContainer paywall-modal-root';
        break;
      case modalTypes.WALLET_PAYMENT:
        ComponentVar = <WalletPaymentModal rechargeData={this.props.rechargeData} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.CARD_WALLET_PAYMENT:
        bodyStyle = {
          padding: 0
        };
        ComponentVar = <CardWalletPaymentModal paymentData={this.props.paymentData} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.PAYPAL_SUCCESS_MODAL:
        bodyStyle = {
          padding: 0
        };
        ComponentVar = <PaypalSuccessModal paymentData={this.props.paymentData} />;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.TEAM_ANALYTICS_USER_LIST_MODAL:
        ComponentVar = (
          <TeamAnalyticsUserList metric={this.props.metric} modalState={this.props.modalState} />
        );
        customContentClass = 'group-share-dialog';
        customModalClass = 'modalContainer group-modal-root';
        break;
      case modalTypes.SHARE_CONTENT_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '1000px'
        };
        ComponentVar = <ShareContentModal card={this.props.card} />;
        customModalClass = 'modalContainer share-content-modal-root';
        customContentClass = 'share-content-modal-dialog';
        break;
      case modalTypes.CHANGE_AUTHOR_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '55px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '1000px'
        };
        ComponentVar = (
          <ChangeAuthorModal
            card={this.props.card}
            removeCardFromList={this.props.removeCardFromList}
            removeCardFromCardContainer={this.props.removeCardFromCardContainer}
            cardSectionName={this.props.cardSectionName}
            changeAuthorOptionRemoval={this.props.changeAuthorOptionRemoval}
          />
        );
        customModalClass = 'modalContainer share-content-modal-root';
        customContentClass = 'share-content-modal-dialog';
        break;
      case modalTypes.CHANNEL_EDIT_MODAL:
        scrollY = false;
        autoHeight = false;
        ComponentVar = <ChannelEditorModal channel={this.props.channel} />;
        clickAway = true;
        bodyStyle = {
          padding: 0,
          overflow: 'visible'
        };
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '3.4375rem',
          overflowY: 'auto'
        };
        open = true;
        customModalClass = 'modalContainer';

        break;
      case modalTypes.SLIDE_OUT_CARD_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '3.4375rem',
          overflowY: 'auto'
        };
        pathwayStyle = {
          width: 0
        };
        ComponentVar = (
          <SlideOutCardModal
            card={this.props.card}
            authorName={this.props.authorName}
            toggleChannelModal={this.props.toggleChannelModal}
            cardUpdated={this.props.cardUpdated}
            defaultImage={this.props.defaultImage}
            logoObj={this.props.logoObj}
            dismissible={this.props.dismissible}
            ratingCount={this.props.ratingCount}
            handleCardClicked={this.props.handleCardClicked}
            standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
            rateCard={this.props.rateCard}
            providerLogos={this.props.providerLogos}
          />
        );
        open = true;
        customModalClass = 'modalContainer modalContainer_slideout';
        break;
      case modalTypes.SKILLS_DIRECTORY_MODAL:
        scrollY = false;
        autoHeight = false;
        smartBiteStyle = {
          alignItems: 'flex-start',
          top: '0',
          paddingTop: '200px',
          overflowY: 'auto'
        };
        pathwayStyle = {
          maxWidth: '500px'
        };
        ComponentVar = <SkillsDirectoryModal changeRole={this.props.changeRole} />;
        open = true;
        customModalClass = 'modalContainer';
        break;
      case modalTypes.OPEN_CHANNEL_CURATE_CARDS_MODAL:
        ComponentVar = <CurateCardsModalV2 />;
        customContentClass = 'curate-card-dialog';
        customModalClass = 'modalContainer curate-card-root channelGroupModalContainer';
        break;
      case modalTypes.CHANNEL_CAROUSEL_CARDS_MODAL:
        ComponentVar = <ChannelCarouselCardsModal />;
        customContentClass = 'channel-cards-dialog';
        customModalClass = 'modalContainer channel-modal-root channelGroupModalContainer';
        break;
      default:
        ComponentVar = null;
        break;
    }
    if (ComponentVar !== null) {
      let leftValue = 'auto';
      let uA = window.navigator.userAgent,
        isIE =
          /msie\s|trident\/|edge\//i.test(uA) &&
          !!(
            document.uniqueID ||
            document.documentMode ||
            window.ActiveXObject ||
            window.MSInputMethodContext
          );

      if (isIE) {
        leftValue = 0;
      }
      overlayStyle = { ...overlayStyle, left: leftValue };
    }
    return (
      <Dialog
        className={`${customModalClass} modal`}
        open={open}
        autoScrollBodyContent={scrollY}
        autoDetectWindowHeight={autoHeight}
        contentStyle={{ ...this.styles.dialogContent, ...pathwayStyle }}
        bodyStyle={{ ...this.styles.dialogBody, ...statusModalStyle, ...bodyStyle }}
        overlayStyle={overlayStyle}
        style={{ ...this.styles.dialogRoot, ...smartBiteStyle }}
        repositionOnUpdate={false}
        onRequestClose={clickAway ? () => this.props.dispatch(close()) : null}
        bodyClassName={customBodyClass}
        contentClassName={customContentClass}
        disableAutoFocus={true}
      >
        {ComponentVar}
      </Dialog>
    );
  }
}

ModalContainer.propTypes = {
  isPrivate: PropTypes.bool,
  comment: PropTypes.object,
  metric: PropTypes.any,
  modalState: PropTypes.any,
  authorName: PropTypes.string,
  toggleChannelModal: PropTypes.func,
  dismissible: PropTypes.bool,
  ratingCount: PropTypes.any,
  handleCardClicked: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  rateCard: PropTypes.any,
  providerLogos: PropTypes.object,
  groupId: PropTypes.any,
  type: PropTypes.string,
  typeBefore: PropTypes.string,
  cardTypes: PropTypes.string,
  pathname: PropTypes.string,
  channelId: PropTypes.any,
  assignedStateChange: PropTypes.any,
  dueAt: PropTypes.string,
  startDate: PropTypes.string,
  currentUserId: PropTypes.string,
  userType: PropTypes.string,
  imageType: PropTypes.string,
  cardType: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  label: PropTypes.string,
  description: PropTypes.string,
  typeOfModal: PropTypes.string,
  statusBlock: PropTypes.string,
  statusMessage: PropTypes.string,
  defaultImage: PropTypes.string,
  id: PropTypes.number,
  cardId: PropTypes.string,
  checkedCardId: PropTypes.number,
  openCard: PropTypes.bool,
  isReject: PropTypes.bool,
  selfAssign: PropTypes.bool,
  isViewsModal: PropTypes.bool,
  isAddingToJourney: PropTypes.bool,
  isStandaloneModal: PropTypes.bool,
  skill: PropTypes.object,
  card: PropTypes.object,
  curators: PropTypes.any,
  channel: PropTypes.object,
  group: PropTypes.object,
  sizes: PropTypes.object,
  logoObj: PropTypes.object,
  integration: PropTypes.object,
  channelSetting: PropTypes.bool,
  groupSetting: PropTypes.bool,
  userBadge: PropTypes.object,
  rejectCallback: PropTypes.func,
  deleteSharedCard: PropTypes.any,
  updateGroupsList: PropTypes.func,
  updatedImage: PropTypes.func,
  cardUpdated: PropTypes.func,
  callback: PropTypes.func,
  openChannelModal: PropTypes.func,
  handleCloseModal: PropTypes.func,
  importClickHandler: PropTypes.func,
  open: PropTypes.bool,
  courseStatus: PropTypes.string,
  courseType: PropTypes.string,
  currentAction: PropTypes.string,
  isInitilized: PropTypes.bool,
  rechargeData: PropTypes.object,
  paymentData: PropTypes.object,
  removable: PropTypes.bool,
  hideCancelBtn: PropTypes.bool,
  removeCardFromCardContainer: PropTypes.func,
  removeCardFromList: PropTypes.func,
  cardSectionName: PropTypes.any,
  changeAuthorOptionRemoval: PropTypes.func,
  bookmarked: PropTypes.object,
  cardModalDetails: PropTypes.any,
  cards: PropTypes.any,
  confirmHandler: PropTypes.func,
  changeRole: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    ...state.modal.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(ModalContainer);
