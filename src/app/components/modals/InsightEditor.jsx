import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { close } from '../../actions/modalActions';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/internal/Tooltip';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import UploadIcon from 'edc-web-sdk/components/icons/Upload';
import VideoIcon from 'edc-web-sdk/components/icons/Video';
import TopicIcon from 'edc-web-sdk/components/icons/TopicIcon';
import colors from 'edc-web-sdk/components/colors/index';
import { uploadImage, uploadFileResource } from 'edc-web-sdk/requests/imageUpload';
import { getFileResource } from 'edc-web-sdk/requests/imageUploadv2';
import { uploadVideo } from 'edc-web-sdk/requests/liveStream';
import Resource from '../feed/Resource';
import Video from '../feed/Video';
import ChannelPostComponent from '../common/ChannelPostComponent';
import AddIcon from 'material-ui/svg-icons/content/add';
import Chip from 'material-ui/Chip';
import RemoveIcon from 'material-ui/svg-icons/content/clear';
import { postv2, updateResource, createResource } from 'edc-web-sdk/requests/cards';
import * as actionTypes from '../../constants/actionTypes';
import { push } from 'react-router-redux';
import { close as closeSnackBar, open as openSnackBar } from '../../actions/snackBarActions';
import moment from 'moment';
import isEqual from 'lodash/isEqual';
import { tr } from 'edc-web-sdk/helpers/translations';
import { filestackClient } from '../../utils/filestack';
import { fileStackSources } from '../../constants/fileStackSource';
import Spinner from '../common/spinner';
import { uploadPolicyAndSignature } from 'edc-web-sdk/requests/filestack';

class InsightEditor extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      action: {
        backgroundColor: colors.gray,
        borderRadius: '50%',
        padding: 0,
        height: '25px',
        width: '25px'
      },
      removeResourceButton: {
        position: 'absolute',
        top: '0.25rem',
        right: '0.25rem',
        background: 'rgba(31, 29, 29, 0.5)'
      },
      removeResourceIcon: {
        color: colors.white,
        filter: `drop-shadow( 0px 0px 1px ${colors.black})`
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0 -4px'
      },
      channelSelect: {
        verticalAlign: 'bottom',
        position: 'absolute',
        bottom: '0'
      },
      chip: {
        margin: '1rem .25rem .25rem .25rem'
      }
    };
    let message = '';
    let showRemoveResourceIcon = false;
    let showPreview = false;
    let showVideoPreview = false;
    let previewData = {};
    let isPoll = false;
    let isVideo = false;
    let pollOptions = [{ label: '', is_correct: false }, { label: '', is_correct: false }];
    let file_ids = [];
    let topics = [];
    let videoUrl = '';
    let resourceId = null;
    let video_stream = '';
    let selectedChannels = [];
    let fileStack = [];

    if (props.card) {
      message = props.card.message;
      let resource;
      if (props.card.resource !== undefined) {
        resource = props.card.resource;
      }
      if (resource) {
        showPreview = true;
        previewData = resource;
        resourceId = resource.id;
      }
      if (props.card.filestack && props.card.filestack.length) {
        showPreview = true;
        fileStack = props.card.filestack;
        if (props.card.filestack[0].handle === props.card.message) {
          message = '';
        }
      }
      if (props.card.cardSubtype === 'link' || props.card.cardSubtype === 'image') {
        showRemoveResourceIcon = true;
      }
      if (props.card.cardType === 'poll') {
        isPoll = true;
        let options =
          props.card.quizQuestionOptions !== undefined
            ? props.card.quizQuestionOptions
            : props.card.options;
        pollOptions = options.map(option => {
          return { id: option.id, label: option.label, is_correct: option.is_correct };
        });
      }

      if (props.card.cardType === 'video_stream') {
        showVideoPreview = true;
        videoUrl =
          (props.card.recording && props.card.recording.mp4Location) ||
          (props.card.videoStream &&
            props.card.videoStream.recording &&
            props.card.videoStream.recording.mp4Location);
        video_stream = props.card.videoStream;
      }
      let fileResourceId;
      if (props.card.fileResources !== undefined && props.card.fileResources[0] !== undefined) {
        fileResourceId = props.card.fileResources[0].id;
      }
      if (fileResourceId) {
        file_ids = [fileResourceId];
      }

      if (props.card.channelIds.length) {
        selectedChannels = props.card.channelIds;
      }
      topics = props.card.tags.map(tag => tag.name);
    }

    this.state = {
      message,
      selectedChannel: null,
      isPoll,
      isVideo,
      pollOptions,
      openChannelsMenu: false,
      selectedChannels,
      showPreview,
      showRemoveResourceIcon,
      showVideoPreview,
      previewData,
      submitPending: false,
      optionsError: '',
      videoUrl,
      video_stream,
      msgError: '',
      pendingImageUpload: false,
      pendingVideoUpload: false,
      disableSubmit: false,
      file_ids: file_ids,
      topics,
      tagSearchText: '',
      resourceId,
      createMessage: 'Your insight has been created, Please check your',
      updateMessage: 'Your SmartCard has been saved. Please check your',
      showWarningMessage: false,
      warningMessage: '',
      fileStack,
      isQuestion: false,
      optionSelected: 0,
      maxTagErrorText: ''
    };
    this.INITIAL_STATE = this.state;
  }

  componentDidMount() {
    if (this.props.requireUploadImage) {
      this._imageFileInput.click();
      if (typeof this.props.onUploadImage === 'function') {
        this.props.onUploadImage();
      }
    }

    if (this.props.card && this.props.card.isAssessment !== undefined) {
      let isAssessment = this.props.card.isAssessment;
      let questionOptions = this.props.card.quizQuestionOptions;

      if (isAssessment) {
        this.setState({ isQuestion: this.props.card.isAssessment });
      }

      questionOptions.map((option, index) => {
        if (option.isCorrect) {
          this.setState({ optionSelected: index });
        }
      });
    }
  }

  fileStackHandler = () => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: ['image/*', '.pdf', 'video/*', '.docx', '.pptx'],
            maxFiles: 5,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(result => {
            this.setState({
              showPreview: true,
              fileStack: [...this.state.fileStack, ...result.filesUploaded]
            });
          })
          .catch(err => {
            console.error(`Error in InsightEditor.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in InsightEditor.uploadPolicyAndSignature.func : ${err}`);
      });
  };

  onQuestionCheckedHandler = () => {
    if (this.state.isQuestion) {
      this.setState({ isQuestion: false });

      this.state.pollOptions.map(option => {
        option.is_correct = false;
      });
    } else {
      this.setState({ isQuestion: true });
    }
  };

  removeFile = handle => {
    let newFiles = this.state.fileStack.filter(file => file.handle != handle);
    this.setState({
      fileStack: newFiles
    });
  };

  uploadImageClickHandler = () => {
    this._imageFileInput.click();
  };

  imageFileInputChangeHandler = () => {
    if (this._imageFileInput.files.length) {
      this.setState({ isImageUploading: true, pendingImageUpload: true, disableSubmit: true });
      let file = this._imageFileInput.files[0];
      let formData = new FormData();
      uploadImage(file)
        .then(url => {
          formData.append('file', url);
          uploadFileResource(formData)
            .then(response => {
              this.uploadPoller(response.id);
              let updatedFileIds = this.state.file_ids;
              updatedFileIds.push(response.id);
              this.setState({ file_ids: updatedFileIds, disableSubmit: false });
            })
            .catch(err => {
              console.error(`Error in InsightEditor.uploadFileResource.func : ${err}`);
            });
        })
        .catch(err => {
          console.error(`Error in InsightEditor.uploadImage.func : ${err}`);
        });
    }
  };

  videoFileInputChangeHandler = () => {
    if (this._videoFileInput.files.length) {
      this.setState({ isVideoUploading: true, pendingVideoUpload: true, disableSubmit: true });
      let file = this._videoFileInput.files[0];
      uploadVideo(file)
        .then(data => {
          this.setState({
            showVideoPreview: true,
            videoUrl: data.location,
            pendingVideoUpload: false,
            disableSubmit: false,
            video_stream: {
              status: 'past',
              start_time: moment()
                .utc()
                .format()
                .replace('Z', '.000Z'),
              recording: data
            }
          });
        })
        .catch(err => {
          console.error(`Error in InsightEditor.uploadVideo.func : ${err}`);
        });
    }
  };

  uploadPoller = id => {
    getFileResource(id)
      .then(response => {
        if (response.available) {
          if (this.state.resourceId) {
            updateResource(this.state.resourceId, { image_url: 'https:' + response.fileUrl });
          }
          this.setState(
            {
              showPreview: true,
              previewData: Object.assign({}, this.state.previewData, {
                imageUrl: response.fileUrl
              }),
              pendingImageUpload: false,
              showRemoveResourceIcon: true
            },
            () => {}
          );
        } else {
          setTimeout(() => {
            this.uploadPoller(id);
          }, 1000);
        }
      })
      .catch(err => {
        console.error(`Error in InsightEditor.getFileResource.func : ${err}`);
      });
  };

  togglePollClickHandler = () => {
    this.setState({ isPoll: !this.state.isPoll });
  };

  uploadVideoClickHandler = () => {
    this._videoFileInput.click();
  };

  addPollOptionClickHandler = () => {
    this.setState({ pollOptions: this.state.pollOptions.concat({ label: '', is_correct: false }) });
  };

  removePollOptionClickHandler = index => {
    let options = this.state.pollOptions;
    let loopOptions = ((this.state.optionSelected - 1) % options.length) % options.length;
    if (loopOptions < 0) {
      loopOptions = 0;
    }

    if (options.length <= 2) {
      this.setState({ isPoll: false });
    } else {
      let newselection =
        index <= this.state.optionSelected ? loopOptions : this.state.optionSelected;
      this.setState({ optionSelected: newselection });
      options[index]['_destroy'] = true;
      options.splice(index, 1);
      if (this.state.isQuestion) {
        options[newselection]['is_correct'] = true;
      }
      this.setState({ pollOptions: options });
    }
  };

  pollOptionChangeHandler = (index, _null, value) => {
    let options = this.state.pollOptions;
    options[index] = { label: value, is_correct: false };
    if (this.state.isQuestion) {
      options[this.state.optionSelected].is_correct = true;
    }
    this.setState({ pollOptions: options });
  };

  assessmentOptionChangeHandler = (e, value) => {
    let curVal = value;
    setTimeout(() => {
      this.setState({ optionSelected: curVal });

      this.state.pollOptions.map(option => {
        option.is_correct = false;
      });
      this.state.pollOptions[curVal].is_correct = this.state.optionSelected == curVal;
    }, 50);
  };

  pasteLinkHandler = event => {
    if (this.state.showPreview) {
      return;
    }
    let pastedText = event.clipboardData.getData('text');
    let regex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;
    let isUrl = regex.test(pastedText);
    if (isUrl) {
      this.setState({ disableSubmit: true, pendingImageUpload: true });
      createResource(pastedText)
        .then(data => {
          data.url = pastedText;
          if (data.embedHtml) {
            delete data.imageUrl;
          }
          this.setState({
            showPreview: true,
            previewData: data,
            disableSubmit: false,
            pendingImageUpload: false,
            resourceId: data.id,
            showRemoveResourceIcon: true
          });
        })
        .catch(err => {
          console.error(`Error in InsightEditor.createResource.func : ${err}`);
        });
    }
  };

  messageChangeHandler = event => {
    let expression = /(([^\[\(]|^)https?:)+[^\s]+\S/g;
    let message = event.target.value.replace(expression, function(url) {
      return '[' + url + '](' + url + ')';
    });
    this.setState({ message: message, msgError: '', optionsError: '' });
  };

  removeResourceClickHandler = () => {
    if (this.props.card && this.props.card.cardSubtype === 'link') {
      let newPreviewData = Object.assign({}, this.state.previewData);
      newPreviewData.imageUrl = null;
      this.setState({ previewData: newPreviewData, showRemoveResourceIcon: false });
    } else if (this.props.card && this.props.card.cardSubtype === 'image') {
      let newPreviewData = Object.assign({}, this.state.previewData);
      newPreviewData.imageUrl = null;
      this.setState({ previewData: newPreviewData, file_ids: [], showRemoveResourceIcon: false });
    } else {
      this.setState({
        showPreview: false,
        file_ids: [],
        previewData: null,
        showRemoveResourceIcon: false
      });
    }
  };

  removeVideoClickHandler = () => {
    this.setState({ showVideoPreview: false, videoUrl: null, video_stream: null });
  };

  cancelClickHandler = () => {
    let warningMessage = 'Did you really mean to cancel? Click Cancel again to confirm.';
    let showCancelWarning = this.state.warningMessage !== warningMessage;
    if (!isEqual(this.state, this.INITIAL_STATE) && showCancelWarning) {
      this.setState({ showWarningMessage: true, warningMessage: warningMessage });
      return;
    }

    this.setState({
      message: '',
      selectedChannel: null,
      isPoll: false,
      pollOptions: ['', ''],
      previewData: {},
      submitPending: false,
      showPreview: false,
      showRemoveResourceIcon: false
    });
    this.props.dispatch(close());
  };

  createClickHandler = (id = null) => {
    if (!this.state.message.replace(/\n|[\u200B]/gi, '').trim()) {
      this.setState({
        msgError: 'Please share something.'
      });
      return;
    }
    // checking if options are entered or not, for poll card.
    if (this.state.isPoll) {
      var numberOfPollOptions = 0;
      let isLabelEmpty;
      for (var i = 0; i < this.state.pollOptions.length; i++) {
        if (this.state.pollOptions[i]) {
          numberOfPollOptions++;
        }
      }

      if (numberOfPollOptions < 2) {
        this.setState({ optionsError: 'Please add options' });
        return;
      }

      function checkLabel(option, index, array) {
        return option['label'].length === 0;
      }

      isLabelEmpty = this.state.pollOptions.filter(checkLabel);
      if (isLabelEmpty.length != 0) {
        this.setState({ showWarningMessage: true, warningMessage: "Options can't be empty" });
        return;
      } else {
        this.setState({ showWarningMessage: false, warningMessage: '' });
      }
    }

    this.setState({ submitPending: true });
    let payload = {
      message: this.state.message
    };
    if (this.state.resourceId) {
      payload['resource_id'] = this.state.resourceId;
    } else if (this.state.previewData && this.state.previewData.id) {
      payload['resource_id'] = this.state.previewData.id;
    } else {
      if (this.state.file_ids) {
        payload['file_resource_ids'] = this.state.file_ids;
      }
    }
    if (this.state.selectedChannels) {
      payload['channel_ids'] = this.state.selectedChannels;
    }
    if (this.state.isPoll && this.state.pollOptions) {
      payload['options'] = this.state.pollOptions.filter(option => option['label'].length != 0);
    }
    if (this.state.topics) {
      payload['topics'] = this.state.topics;
    }
    if (this.state.video_stream) {
      payload['video_stream'] = this.state.video_stream;
    }
    if (this.state.fileStack) {
      payload['filestack'] = this.state.fileStack;
    }
    postv2({ card: payload }, id)
      .then(data => {
        this.setState({
          message: '',
          selectedChannel: null,
          isPoll: false,
          pollOptions: ['', ''],
          previewData: {},
          topics: [],
          video_stream: null,
          submitPending: false,
          showPreview: false,
          showRemoveResourceIcon: false,
          showVideoPreview: false,
          fileStack: []
        });
        this.props.dispatch(close());
        if (data && data.embargoErr) {
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
        } else {
          let notificationMessage = this.props.card ? (
            <span>
              {tr(this.state.updateMessage)}{' '}
              <a className="meContent" onTouchTap={this.openContentTab}>
                {' '}
                {tr('Content')}
              </a>
            </span>
          ) : (
            <span>
              {tr(this.state.createMessage)}{' '}
              <a className="meContent" onTouchTap={this.openContentTab}>
                {' '}
                {tr('Content tab')}
              </a>
            </span>
          );
          this.props.dispatch({
            type: actionTypes.OPEN_SNACKBAR,
            message: notificationMessage,
            autoClose: true
          });
          this.props.cardUpdated(payload);
        }
      })
      .catch(() => {
        this.props.dispatch(close());
      });
  };

  // to open the content tab when clicked on toaster
  openContentTab = () => {
    if (this.props.pathname.indexOf('/me/') === -1) {
      this.props.dispatch(push('/me/content'));
    } else {
      window.location.href = '/me/content';
    }
    this.props.dispatch(closeSnackBar());
  };

  removeTag = tag => {
    let newTagArray = this.state.topics.filter(topic => topic !== tag);
    this.setState({ topics: newTagArray });
  };

  getNewChannels = channelIds => {
    this.setState({ selectedChannels: channelIds });
  };

  topicsChangeHandler = topics => {
    this.setState({ topics });
  };

  addTagHandler = e => {
    if (e.keyCode === 13) {
      if (this.state.topics.indexOf(e.target.value.trim()) === -1) {
        this.setState({
          topics: this.state.topics.concat(e.target.value.trim()),
          tagSearchText: ''
        });
      } else {
        this.setState({ tagSearchText: '' });
      }
    }
  };

  updateUserPrefs = searchText => {
    this.setState({ inputText: searchText });
  };

  render() {
    const useFormLabels = window.ldclient.variation('use-form-labels', false);

    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    return (
      <div id="insight_editor" className="container-padding vertical-spacing-large">
        <div className="actions-block">
          {this.state.showWarningMessage && (
            <div className="actions float-left">
              <p className="warning-message">{tr(this.state.warningMessage)}</p>
            </div>
          )}
          <TextField name="insighteditorModal" autoFocus={true} className="hiddenTextField" />
          <div className="actions float-right">
            <SecondaryButton
              label={tr('Cancel')}
              className="close"
              onTouchTap={this.cancelClickHandler}
            />
            {this.props.card && (
              <PrimaryButton
                disabled={this.state.disableSubmit}
                label={tr('Save')}
                pendingLabel={tr('Saving...')}
                className="create"
                pending={this.state.submitPending}
                onTouchTap={this.createClickHandler.bind(this, this.props.card.id)}
              />
            )}
            {!this.props.card && (
              <PrimaryButton
                disabled={this.state.disableSubmit}
                label={tr('Create')}
                className="create"
                pendingLabel={tr('Creating...')}
                pending={this.state.submitPending}
                onTouchTap={this.createClickHandler.bind(this, null)}
              />
            )}
          </div>
        </div>
        <div className="horizontal-spacing-large">
          <div className="button-options">{tr('Add')}: </div>
          <span>
            <IconButton
              style={this.styles.action}
              className="fileStack"
              aria-label={tr('Upload Files, Videos, and Images')}
              onTouchTap={this.fileStackHandler}
              onMouseEnter={() => {
                this.setState({ hoveredTooltipStack: true });
              }}
              onMouseLeave={() => {
                this.setState({ hoveredTooltipStack: false });
              }}
            >
              <UploadIcon color={colors.white} viewBox="4 -4 17 32" />
              <Tooltip
                show={this.state.hoveredTooltipStack}
                label={tr('Upload Files, Videos, and Images')}
                horizontalPosition="center"
                onMouseEnter={() => {
                  this.setState({ hoveredTooltipStack: false });
                }}
                verticalPosition="bottom"
              />
            </IconButton>
          </span>
          {!this.state.showRemoveResourceIcon && !this.state.pendingImageUpload && (
            <span>
              <IconButton
                style={this.styles.action}
                onTouchTap={this.uploadImageClickHandler}
                className="uploadImage"
                onMouseEnter={() => {
                  this.setState({ hoveredTooltipImage: true });
                }}
                onMouseLeave={() => {
                  this.setState({ hoveredTooltipImage: false });
                }}
                aria-label={tr('Upload Image')}
              >
                <ImagePhotoCamera color={colors.white} viewBox="5 -6 16 35" />
                <Tooltip
                  show={this.state.hoveredTooltipImage}
                  label={tr('Upload Image')}
                  horizontalPosition="center"
                  onMouseEnter={() => {
                    this.setState({ hoveredTooltipImage: false });
                  }}
                  verticalPosition="bottom"
                />
              </IconButton>
            </span>
          )}
          {/*{!disablePoll && (*/}
          <span>
            <IconButton
              style={{
                ...this.styles.action,
                backgroundColor: this.state.isPoll ? colors.primary : colors.gray
              }}
              onTouchTap={this.togglePollClickHandler}
              onMouseEnter={() => {
                this.setState({ hoveredTooltipPoll: true });
              }}
              onMouseLeave={() => {
                this.setState({ hoveredTooltipPoll: false });
              }}
              aria-label={tr('Toggle Poll')}
            >
              <PollIcon color={colors.white} viewBox="2 -6 19 35" />
              <Tooltip
                show={this.state.hoveredTooltipPoll}
                label={tr('Toggle Poll')}
                horizontalPosition="center"
                onMouseEnter={() => {
                  this.setState({ hoveredTooltipPoll: false });
                }}
                verticalPosition="bottom"
              />
            </IconButton>
          </span>
          {/*)}*/}
          {!this.state.showVideoPreview && !this.state.pendingVideoUpload && (
            <span>
              <IconButton
                style={{ ...this.styles.action, backgroundColor: colors.gray }}
                onTouchTap={this.uploadVideoClickHandler}
                onMouseEnter={() => {
                  this.setState({ hoveredTooltipVideo: true });
                }}
                onMouseLeave={() => {
                  this.setState({ hoveredTooltipVideo: false });
                }}
                className="uploadVideo"
                aria-label={tr('Upload Video')}
              >
                <VideoIcon color={colors.white} />
                <Tooltip
                  show={this.state.hoveredTooltipVideo}
                  label={tr('Upload Video')}
                  horizontalPosition="center"
                  onMouseEnter={() => {
                    this.setState({ hoveredTooltipVideo: false });
                  }}
                  verticalPosition="bottom"
                />
              </IconButton>
            </span>
          )}
        </div>
        <input
          type="file"
          className="hide"
          accept="image/*"
          ref={node => (this._imageFileInput = node)}
          onChange={this.imageFileInputChangeHandler}
        />
        <input
          type="file"
          className="hide"
          accept="video/*"
          ref={node => (this._videoFileInput = node)}
          onChange={this.videoFileInputChangeHandler}
        />
        <div>
          <textarea
            className="smartbite-textarea"
            placeholder={tr('Share your knowledge here')}
            onChange={this.messageChangeHandler}
            autoFocus={true}
            rows="4"
            onPaste={this.pasteLinkHandler}
            defaultValue={this.state.message}
          />
        </div>
        {this.state.pendingImageUpload && this.state.pendingVideoUpload && (
          <div className="text-center">
            <Spinner />
          </div>
        )}
        {this.state.showPreview && (
          <div className="inline-creation-resource">
            <Resource
              cardId={this.props.card && this.props.card.id}
              currentUser={this.props.currentUser}
              resource={this.state.previewData}
            />
            {this.state.showRemoveResourceIcon && (
              <IconButton
                style={this.styles.removeResourceButton}
                iconStyle={this.styles.removeResourceIcon}
                className="delete"
                onTouchTap={this.removeResourceClickHandler}
                aria-label="remove"
              >
                <RemoveIcon />
              </IconButton>
            )}
            {this.state.fileStack.map(file => {
              if (file.mimetype && ~file.mimetype.indexOf('image/')) {
                return (
                  <div key={file.handle} className="fp fp_img">
                    <span className="delete" onClick={this.removeFile.bind(this, file.handle)}>
                      &times;
                    </span>
                    <img src={file.url} />
                  </div>
                );
              } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                return (
                  <div key={file.handle} className="fp fp_video">
                    <span className="delete" onClick={this.removeFile.bind(this, file.handle)}>
                      &times;
                    </span>
                    <video
                      controls
                      src={file.url}
                      controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                    />
                  </div>
                );
              } else {
                return (
                  <div key={file.handle} className="fp fp_preview">
                    <span className="delete" onClick={this.removeFile.bind(this, file.handle)}>
                      &times;
                    </span>
                    <span className="fp_file-type">
                      {file.mimetype
                        .split('/')
                        .pop()
                        .split('.')
                        .pop()}
                    </span>
                    <img
                      src={`https://process.filestackapi.com/output=format:png,p:1/${file.handle}`}
                    />
                  </div>
                );
              }
            })}
          </div>
        )}
        {this.state.showVideoPreview && (
          <div className="inline-creation-resource">
            <Video
              videoUrl={this.state.videoUrl}
              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
            />
            <IconButton
              style={this.styles.removeResourceButton}
              iconStyle={this.styles.removeResourceIcon}
              onTouchTap={this.removeVideoClickHandler}
              className="delete"
              aria-label="remove"
            >
              <RemoveIcon />
            </IconButton>
          </div>
        )}
        {(this.state.pendingImageUpload || this.state.pendingVideoUpload) && (
          <div className="text-center">
            <Spinner />
          </div>
        )}
        {this.state.isPoll && (
          <div>
            <span>
              <Checkbox
                aria-label={tr('Enable question card')}
                label={tr('Enable question card')}
                checked={this.state.isQuestion}
                onCheck={this.onQuestionCheckedHandler}
                labelStyle={{ fontSize: '15' }}
                iconStyle={{ width: '17', height: '17', marginTop: '2', marginRight: '6' }}
              />
            </span>
            <div style={{ marginBottom: '10' }} />

            <h6>{tr(`${this.state.isQuestion ? 'Answer' : 'Poll'} options`)}</h6>
            <div className="row">
              {this.state.isQuestion && (
                <RadioButtonGroup
                  key="radio-button-group"
                  name="poll-radio-button-group"
                  className="vertical-spacing-medium col-2"
                  defaultSelected="0"
                  valueSelected={this.state.optionSelected}
                  onChange={(e, value) => {
                    this.assessmentOptionChangeHandler(e, value);
                  }}
                >
                  {this.state.pollOptions.map((option, index) => {
                    if (option !== undefined && !option._destroy) {
                      return (
                        <RadioButton
                          style={{ marginTop: '20px', marginBottom: '29px' }}
                          key={index}
                          value={index}
                        />
                      );
                    }
                  })}
                </RadioButtonGroup>
              )}
              <div className="col-10">
                {this.state.pollOptions.map((option, index) => {
                  if (option !== undefined && !option._destroy) {
                    return (
                      <div key={index}>
                        <TextField
                          value={option.label}
                          onChange={this.pollOptionChangeHandler.bind(this, index)}
                          name="option"
                          hintText={tr('Option')}
                          aria-label={tr('Option')}
                        />
                        <IconButton
                          className="delete"
                          onTouchTap={this.removePollOptionClickHandler.bind(this, index)}
                          aria-label="remove"
                        >
                          <RemoveIcon />
                        </IconButton>
                      </div>
                    );
                  }
                })}
              </div>
            </div>

            {this.state.optionsError && (
              <div className="error-text">{tr(this.state.optionsError)}</div>
            )}
            {this.state.pollOptions.length < 5 && (
              <div>
                <SecondaryButton
                  onTouchTap={this.addPollOptionClickHandler}
                  className="create"
                  icon={<AddIcon />}
                  label={tr('Add option')}
                />
              </div>
            )}
          </div>
        )}
        <div>
          <div className="row">
            <div className="small-6">
              <ChannelPostComponent
                onChange={this.getNewChannels}
                currentChannels={this.state.selectedChannels}
              />
            </div>
            <div className="small-6 topics-container">
              <span style={this.styles.channelSelect}>
                <div style={this.styles.chipsWrapper}>
                  {this.state.topics.map((name, idx) => {
                    return (
                      <Chip
                        key={name + idx}
                        style={this.styles.chip}
                        className="channel-item-insight"
                        backgroundColor={colors.primary}
                        labelColor={colors.white}
                        labelStyle={{ width: 'calc(100% - 20px)' }}
                        onRequestDelete={() => {
                          this.removeTag(name);
                        }}
                      >
                        <TopicIcon style={{ verticalAlign: 'middle', color: colors.white }} />{' '}
                        {name}
                      </Chip>
                    );
                  })}
                </div>
                <TextField
                  hintText={
                    <span>
                      <TopicIcon style={{ verticalAlign: 'middle', color: colors.gray }} />
                      {tr('Enter desired tag')}
                    </span>
                  }
                  aria-label={tr('Enter desired tag')}
                  value={this.state.tagSearchText}
                  onChange={e => {
                    this.setState({
                      tagSearchText: e.target.value,
                      maxTagErrorText:
                        e.target.value.length <= 150
                          ? ''
                          : 'Max tag length exceeded. It must be shorter than 150 characters.'
                    });
                  }}
                  onKeyDown={!this.state.maxTagErrorText.length && this.addTagHandler}
                />
                <div className="error-text"> {tr(this.state.maxTagErrorText)}</div>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

InsightEditor.propTypes = {
  cardId: PropTypes.string,
  pathname: PropTypes.string,
  modal: PropTypes.object,
  card: PropTypes.object,
  team: PropTypes.object,
  cardUpdated: PropTypes.func,
  onUploadImage: PropTypes.func,
  requireUploadImage: PropTypes.bool,
  defaultValue: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(InsightEditor);
