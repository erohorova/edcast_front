import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactMarkdown from 'react-markdown';
import { close } from '../../actions/modalActions';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { toggleIntegration } from 'edc-web-sdk/requests/integrations';
import ListItem from 'material-ui/List/ListItem';
import TextField from 'material-ui/TextField';
import { tr } from 'edc-web-sdk/helpers/translations';
import MarkdownRenderer from '../common/MarkdownRenderer';

class IntegrationModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      importBtnText: 'Import Courses',
      importError: ''
    };

    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.importClickHandler = this.importClickHandler.bind(this);
    this.getParams = this.getParams.bind(this);
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  getParams() {
    let params = {};
    this.props.integration.fields.map(field => {
      params[field.name] = this['_' + field.name + 'Label'].input.value;
    });
    return params;
  }

  importClickHandler() {
    let params = {};
    params['fields'] = this.getParams();
    params['provider'] = this.props.integration.name;
    params['connect'] = !this.props.integration.connected;
    this.setState({ importBtnText: 'Importing Courses ...', importError: '' });
    toggleIntegration(this.props.integration.id, params)
      .then(() => {
        this.props.importClickHandler(this.props.integration);
        this.props.dispatch(close());
      })
      .catch(err => {
        this.setState({ importError: err.message || err.errors, importBtnText: 'Import Courses' });
      });
  }

  render() {
    return (
      <div>
        <div className="row">
          <TextField name="addtochannel" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns">
            <div className="section-header">
              <div className="section-title">{this.props.integration.name}</div>
            </div>
            {this.props.integration.description && (
              <div className="description-section">
                <div
                  style={{ fontSize: '13px' }}
                  dangerouslySetInnerHTML={{ __html: this.props.integration.description }}
                />
              </div>
            )}
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns small-centered">
            {this.props.integration.fields &&
              this.props.integration.fields.length > 0 &&
              this.props.integration.fields.map((field, idx) => {
                return (
                  <div className="row section-fields" key={idx}>
                    <div className="small-4 columns text-right">
                      <ListItem disabled secondaryText={field.label || field.name} />
                    </div>
                    <div className="small-4 pull-4 columns">
                      <TextField
                        hintText={tr(field.placeholder)}
                        aria-label={tr(field.placeholder)}
                        fullWidth
                        ref={node => (this['_' + field.name + 'Label'] = node)}
                        defaultValue={field.value}
                        autoFocus={true}
                      />
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
        <div className="row actions-integration">
          <div className="small-6 columns small-centered">
            <div className="text-left">
              <ListItem
                disabled
                style={{ padding: '16px 0' }}
                primaryText={<span style={{ color: 'red' }}>{tr(this.state.importError)}</span>}
              />
            </div>
          </div>
          <div className="small-6 columns small-centered">
            <div className="text-right">
              {this.props.integration.callbackUrl == 'static' && (
                <SecondaryButton
                  label={tr('Cancel')}
                  className="close"
                  onTouchTap={this.cancelClickHandler}
                />
              )}
              {this.props.integration.callbackUrl != 'static' && (
                <div>
                  <PrimaryButton
                    label={tr(this.state.importBtnText)}
                    className="import"
                    onTouchTap={this.importClickHandler}
                  />
                  <SecondaryButton
                    label={tr('Cancel')}
                    className="close"
                    onTouchTap={this.cancelClickHandler}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

IntegrationModal.propTypes = {
  integration: PropTypes.object,
  importClickHandler: PropTypes.func,
  handleCallback: PropTypes.func
};

export default connect()(IntegrationModal);
