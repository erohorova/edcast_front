import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { addCardToPathways } from '../../actions/pathwaysActions';
import colors from 'edc-web-sdk/components/colors/index';
import Paper from 'edc-web-sdk/components/Paper';
import getFormattedDateTime from '../../utils/getFormattedDateTime';
import Checkbox from 'material-ui/Checkbox';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { journeys as Journeys } from 'edc-web-sdk/requests/index';
import { close } from '../../actions/modalActions';
import Spinner from '../common/spinner';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import uniqBy from 'lodash/uniqBy';
import TextField from 'material-ui/TextField';
import Select from 'react-select';

class AddToJourneyModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      query: '',
      pathways: [],
      journeys: [],
      selectedJourneys: [],
      selectedSections: {},
      buttonPending: false
    };
    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      checkbox: {
        color: colors.primary
      },
      header: {
        color: '#ffffff',
        width: '100%',
        position: 'absolute',
        top: '-25px',
        left: 0
      },
      close: {
        cursor: 'pointer',
        float: 'right',
        top: 0
      },
      table: {
        width: '100%',
        textAlign: 'center'
      },
      tableScroll: {
        maxHeight: '300px',
        display: 'block',
        overflowY: 'scroll'
      },
      firstColumn: {
        textAlign: 'left'
      },
      tdBorder: {
        borderLeft: '2px solid rgba(0,0,0,.06)'
      },
      footerButtons: {
        marginTop: '10px'
      }
    };
    this.addClickHandler = this.addClickHandler.bind(this);
    this.closeModalHandler = this.closeModalHandler.bind(this);
  }

  componentDidMount() {
    Journeys.getAddableJourneysForPathway(this.props.cardId)
      .then(journeys => {
        this.setState({
          journeys,
          isAll: journeys.length < 10,
          pending: false
        });
      })
      .catch(() => {
        this.setState({
          pathways: [],
          isAll: false,
          pending: false
        });
      });
  }

  selectPathway(journeyId) {
    let selectedJourneys = this.state.selectedJourneys;
    if (~this.state.selectedJourneys.indexOf(journeyId)) {
      let index = this.state.selectedJourneys.indexOf(journeyId);
      selectedJourneys.splice(index, 1);
    } else {
      selectedJourneys.push(journeyId);
    }

    this.setState({ selectedJourneys });
  }
  addClickHandler() {
    this.setState({ buttonPending: true, err: false });
    Journeys.addToJourneys(this.state.selectedJourneys, this.props.cardId)
      .then(() => {
        this.setState({ buttonPending: false });
        this.closeModalHandler();
        this.props.dispatch(openSnackBar('Card successfully added to Journey(s)!', true));
      })
      .catch(() => {
        this.setState({
          err: true,
          buttonPending: false
        });
      });
  }

  closeModalHandler() {
    this.props.dispatch(close());
  }

  checkTitle(journey) {
    let name = journey.title || journey.message;
    name = name.length < 53 ? name : `${name.slice(0, 52)}...`;
    return name.replace(/amp;/gi, '');
  }

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.checkScroll(node));
    }
  };

  checkScroll = node => {
    let obj = ReactDOM.findDOMNode(node);
    let isBottom =
      obj.scrollHeight - Math.ceil(obj.scrollTop) === obj.offsetHeight ||
      obj.scrollHeight - Math.floor(obj.scrollTop) === obj.offsetHeight;
    if (!this.state.pendingFetch && isBottom) {
      this.fetchMore();
    }
  };

  fetchMore() {
    if (!this.state.isAll) {
      this.setState({ pendingFetch: true });
      Journeys.getAddableJourneysForPathway(this.props.cardId, 10, this.state.journeys.length)
        .then(journeys => {
          this.setState({
            journeys: uniqBy(this.state.journeys.concat(journeys), 'id'),
            isAll: journeys && journeys.length < 10,
            pendingFetch: false
          });
        })
        .catch(err => {
          console.error(`Error in AddToJourneyModal.getAddableJourneysForPathway.func: ${err}`);
        });
    }
  }

  render() {
    let isNoItems = !this.state.journeys.length;
    let journeys = this.state.journeys;
    return (
      <div>
        <div style={this.styles.header}>
          {' '}
          {tr(`Add to Journey`)}
          <span style={this.styles.close} className="close-icon-btn">
            <NavigationClose color={colors.grey} onTouchTap={this.closeModalHandler} />
          </span>
        </div>
        <Paper>
          <div className="container">
            <div className="container-padding">
              <div>
                <TextField name="addtojourney" autoFocus={true} className="hiddenTextField" />
                {!this.state.pending && isNoItems && (
                  <div className="text-center container-padding">
                    {tr(`Create a Journey before you start adding content to it`)}
                  </div>
                )}
                {this.state.pending && isNoItems && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
                {!this.state.pending && !!this.state.journeys.length && (
                  <div>
                    <div style={this.styles.tableScroll} ref={this.mountScroll}>
                      <table style={this.styles.table}>
                        <thead>
                          <tr>
                            <th style={this.styles.firstColumn}>{tr('Journey Title')}</th>
                            <th>{tr('Creator')}</th>
                            <th>{tr('Modified')}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {journeys.map((journey, index) => {
                            let selected = ~this.state.selectedJourneys.indexOf(journey.id);
                            return (
                              <tr key={index}>
                                <td style={this.styles.firstColumn}>
                                  <Checkbox
                                    label={
                                      <a
                                        className="matte"
                                        onClick={() => {
                                          this.props.dispatch(push(`/journeys/${journey.id}`));
                                        }}
                                      >
                                        {this.checkTitle(journey)}
                                      </a>
                                    }
                                    onCheck={() => {
                                      this.selectPathway(journey.id);
                                    }}
                                    className="checkbox"
                                    style={this.styles.checkbox}
                                    checked={selected}
                                    aria-label={`journey, ${this.checkTitle(journey)}`}
                                  />
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <a
                                    className="data-ava matte"
                                    onClick={() => {
                                      this.props.dispatch(
                                        push(`/@${this.props.currentUser.handle}`)
                                      );
                                    }}
                                    target="_blank"
                                  >
                                    {this.props.currentUser.name.length < 19
                                      ? this.props.currentUser.name
                                      : `${this.props.currentUser.name.slice(0, 18)}...`}
                                  </a>
                                </td>
                                <td style={this.styles.tdBorder}>
                                  <div className="matte">
                                    {getFormattedDateTime(journey.updatedAt, 'DD/MM/YYYY')}
                                  </div>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                      {this.state.pendingFetch && (
                        <div className="text-center">
                          <Spinner />
                        </div>
                      )}
                      {this.state.err && (
                        <div className="text-center error-text data-not-available-msg">
                          {tr('Something went wrong! Please try later.')}
                        </div>
                      )}
                    </div>
                    <div className="text-center" style={this.styles.footerButtons}>
                      <SecondaryButton
                        label={tr('Cancel')}
                        className="cancel"
                        onTouchTap={this.closeModalHandler}
                      />
                      <PrimaryButton
                        label={tr('Add')}
                        className="create"
                        onTouchTap={this.addClickHandler}
                        pending={this.state.buttonPending}
                        pendingLabel={tr('Adding...')}
                        disabled={!this.state.selectedJourneys.length}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

AddToJourneyModal.propTypes = {
  cardId: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect(state => ({ currentUser: state.currentUser.toJS() }))(AddToJourneyModal);
