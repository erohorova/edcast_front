import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import TimeAgo from 'react-timeago';
import translateTA from '../../utils/translateTimeAgo';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
  TableHeader,
  TableHeaderColumn
} from 'material-ui/Table';
import upperFirst from 'lodash/upperFirst';
import flatten from 'lodash/flatten';
import includes from 'lodash/includes';
import calculateFromSeconds from '../../utils/calculateFromSecondsForDuration';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import convertRichText from '../../utils/convertRichText';

const DURATION_FORMAT = '[Month:Days:Hours:Mins]';
const fieldNames = {
  can_be_reanswered: 'Can be reanswered',
  image_url: 'Image',
  is_public: 'Private content',
  is_paid: 'Paid',
  prices_attributes: 'Price',
  readable_card_type: 'Card type',
  url: 'URL',
  video_url: 'Video'
};

class SmartCardVersions extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      messageRteOriginalValue: {
        width: '40%',
        marginRight: '10px'
      },
      messageRteChangedValue: {
        width: '40%'
      },
      field: {
        width: '20%',
        wordWrap: 'break-word',
        fontWeight: 'bold'
      },
      originalValue: {
        width: '40%',
        wordWrap: 'break-word',
        marginRight: '10px'
      },
      newValue: {
        width: '40%',
        wordWrap: 'break-word'
      },
      header: {
        fontWeight: 'bold'
      }
    };
  }

  fieldName = field => {
    return fieldNames[field] || upperFirst(field);
  };

  anchorTag = (name, url) => {
    return "<a target='_blank' href='" + url + "'>" + name + '</a>';
  };

  clickableImageTag = url => {
    return (
      "<a target='_blank' href='" +
      url +
      "'><img border='0' style='border: solid 1px #d6d6e1' width='100' height='100' src='" +
      url +
      "'>"
    );
  };

  durationHTML = duration => {
    let formattedDuration = calculateFromSeconds(duration, true).replace(/\s/g, ':');
    let html1 = '<span>' + formattedDuration + '</span>';
    let html2 = "<span style='color:#ccc;'>" + DURATION_FORMAT + '</span>';

    return html1 + '<br>' + html2;
  };

  isImage = url => {
    return url.match(/\.(jpeg|jpg|gif|png)$/) != null;
  };

  getCardChanges = updates => {
    var originalValue, changedValue;
    let keys = Object.keys(updates);
    let changes = [];

    keys.map((key, index) => {
      let updatedField = updates[key];

      switch (key.toUpperCase()) {
        case 'DURATION':
          originalValue = this.durationHTML(updatedField[0]);
          changedValue = this.durationHTML(updatedField[1]);
          changes.push({ name: this.fieldName(key), changes: [originalValue, changedValue] });
          break;

        case 'IS_PUBLIC':
          originalValue = (!updatedField[0]).toString();
          changedValue = (!updatedField[1]).toString();
          changes.push({ name: this.fieldName(key), changes: [originalValue, changedValue] });
          break;

        case 'FILESTACK':
          let file_type = 'Image';
          if (updatedField[0].length > 0) {
            if (
              (updatedField[0][0].mimetype && updatedField[0][0].mimetype.includes('image')) ||
              this.isImage(updatedField[0][0].url)
            ) {
              originalValue = this.clickableImageTag(updatedField[0][0].url);
            } else {
              originalValue = this.anchorTag(updatedField[0][0].filename, updatedField[0][0].url);
            }
          } else {
            originalValue = '';
          }
          if (updatedField[1].length > 0) {
            if (
              (updatedField[1][0].mimetype && updatedField[1][0].mimetype.includes('image')) ||
              this.isImage(updatedField[1][0].url)
            ) {
              changedValue = this.clickableImageTag(updatedField[1][0].url);
            } else {
              changedValue = this.anchorTag(updatedField[1][0].filename, updatedField[1][0].url);
              file_type = 'File';
            }
          } else {
            changedValue = '';
          }
          changes.push({ name: file_type, changes: [originalValue, changedValue] });
          break;

        case 'RESOURCE':
          changes.push(this.getResourceChanges(updatedField));
          break;

        case 'CARD_METADATUM_ATTRIBUTES':
          let changedValues;
          if (updatedField.level) {
            changedValues = updatedField.level.map((value, i) => {
              return upperFirst(value);
            });
            changes.push({ name: 'Level', changes: changedValues });
          }
          if (updatedField.plan) {
            changedValues = updatedField.plan.map((value, i) => {
              return upperFirst(value);
            });
            changes.push({ name: 'Plan', changes: changedValues });
          }
          break;
        case 'LEVEL':
          let levelChanges = updatedField.map((value, i) => {
            return upperFirst(value);
          });
          changes.push({ name: 'Level', changes: levelChanges });
          break;

        default:
          originalValue = updatedField[0] == null ? '' : updatedField[0].toString();
          changedValue = updatedField[1] == null ? '' : updatedField[1].toString();
          changes.push({ name: this.fieldName(key), changes: [originalValue, changedValue] });
          break;
      }
    });
    return flatten(changes);
  };

  getResourceChanges = changes => {
    var before, after, previousChange, newChange;
    let keys = Object.keys(changes[0]);
    let arr = [];
    if (includes(keys, 'embed_html')) {
      keys.splice(keys.indexOf('embed_html'), 1);
    }
    if (changes.length == 1) {
      keys.map((key, index) => {
        previousChange = changes[0][key][0] || '';
        newChange = changes[0][key][1] || '';
        if (key == 'image_url') {
          if (previousChange != '') {
            previousChange = this.clickableImageTag(previousChange);
          }
          if (newChange != '') {
            newChange = this.clickableImageTag(newChange);
          }
        } else {
          if (key.includes('url')) {
            if (previousChange != '') {
              previousChange = this.anchorTag(previousChange, previousChange);
            }
            if (newChange != '') {
              newChange = this.anchorTag(newChange, newChange);
            }
          }
        }
        arr.push({ name: this.fieldName(key), changes: [previousChange, newChange] });
      });
    } else {
      before = changes[0];
      after = changes[1];

      keys.map((key, index) => {
        previousChange = before[key] || '';
        newChange = after[key] || '';
        if (previousChange != newChange) {
          if (key == 'image_url') {
            if (previousChange != '') {
              previousChange = this.clickableImageTag(previousChange);
            }
            if (newChange != '') {
              newChange = this.clickableImageTag(newChange);
            }
          } else {
            if (key.includes('url')) {
              if (previousChange != '') {
                previousChange = this.anchorTag(previousChange, previousChange);
              }
              if (newChange != '') {
                newChange = this.anchorTag(newChange, newChange);
              }
            }
          }
          arr.push({ name: this.fieldName(key), changes: [previousChange, newChange] });
        }
      });
    }
    return arr;
  };

  render() {
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);
    return (
      <div>
        <div className="flex content versions-headers" style={this.styles.header}>
          <div style={this.styles.field}>Field</div>
          <div style={this.styles.originalValue}>Original Value</div>
          <div style={this.styles.newValue}>New Value</div>
        </div>
        {this.props.versions.map(version => {
          let changes = this.getCardChanges(version.updatedFields[0]);

          return (
            <div className="previous-versions-content">
              <div className="changesMadeBy">
                <span>{version.user} made changes - </span>
                <span>
                  <TimeAgo date={version.createdAt} live={false} formatter={formatter} />
                </span>
              </div>
              {changes.map((change, index) => {
                let name = change.name;
                let originalValue = change.changes[0];
                let changedValue = change.changes[1];

                if (originalValue != changedValue) {
                  if (name == 'Message') {
                    return (
                      <div>
                        <div className="flex content previous-versions">
                          <p style={this.styles.field}>{name}</p>
                          <div
                            className="previous-message-rte"
                            style={this.styles.messageRteOriginalValue}
                          >
                            <RichTextReadOnly text={convertRichText(originalValue)} />
                          </div>
                          <div
                            className="next-message-rte"
                            style={this.styles.messageRteChangedValue}
                          >
                            <RichTextReadOnly text={convertRichText(changedValue)} />
                          </div>
                        </div>
                      </div>
                    );
                  } else {
                    return (
                      <div>
                        <div className="flex content previous-versions">
                          <p style={this.styles.field}>{name}</p>
                          <p
                            style={this.styles.originalValue}
                            dangerouslySetInnerHTML={{ __html: originalValue }}
                          />
                          <p
                            style={this.styles.newValue}
                            dangerouslySetInnerHTML={{ __html: changedValue }}
                          />
                        </div>
                      </div>
                    );
                  }
                }
              })}
            </div>
          );
        })}
      </div>
    );
  }
}

SmartCardVersions.propTypes = {
  versions: PropTypes.array,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(SmartCardVersions);
