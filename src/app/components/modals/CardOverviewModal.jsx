import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { push } from 'react-router-redux';
import isEmpty from 'lodash/isEmpty';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import find from 'lodash/find';
let LocaleCurrency = require('locale-currency');

import { CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import CheckedIcon from 'material-ui/svg-icons/action/done';

import colors from 'edc-web-sdk/components/colors/index';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import { tr } from 'edc-web-sdk/helpers/translations';
import { markAsComplete, markAsUncomplete } from 'edc-web-sdk/requests/cards.v2';
import CompletedAssignment from 'edc-web-sdk/components/icons/CompletedAssignmentGrey';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import Downloadv2 from 'edc-web-sdk/components/icons/Downloadv2';
import LockIcon from 'edc-web-sdk/components/icons/Lock';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import CommentList from '../../components/feed/CommentList.v2';
import LiveCommentList from '../../components/feed/LiveCommentList';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';

import { loadComments, toggleLikeCardAsync } from '../../actions/cardsActions';
import { close, closeCard, openCardStatsModal } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import * as upshotActions from '../../actions/upshotActions';

import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import linkPrefix from '../../utils/linkPrefix';
import getCardType from '../../utils/getCardType';
import getVideoStatus from '../../utils/getVideoStatus';
import getFleIcon from '../../utils/getFileIcon';
import getCardImage from '../../utils/getCardImage';
import checkResources from '../../utils/checkResources';
import convertRichText from '../../utils/convertRichText';
import getTranscodedVideo from '../../utils/getTranscodedVideo';
import getCardParams from '../../utils/getCardParams';
import { parseUrl } from '../../utils/urlParser';

import Poll from '../feed/Poll';
import VideoStream from '../feed/VideoStream';

import MarkdownRenderer from '../common/MarkdownRenderer';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});
import CreationDate from '../common/CreationDate';
import DateConverter from '../common/DateConverter';
import BlurImage from '../common/BlurImage';
import Spinner from '../common/spinner';
import SvgImageResized from '../common/ImageResized';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';
import * as logoType from '../../constants/logoTypes';

class CardOverviewModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      card: props.card,
      pendingLike: false,
      isUpvoted: props.card.isUpvoted,
      isCompleted:
        (props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED') ||
        (props.card.isCompleted && !props.card.isLocked),
      votesCount: props.card.votesCount,
      commentsCount: props.card.commentsCount,
      dueAt: props.dueAt,
      clc: window.ldclient.variation('clc', false),
      isLiveStream: false,
      liveCommentsCount: 0,
      showTopic: false,
      autoComplete: props.autoComplete !== undefined ? props.autoComplete : true,
      showNewDesignForCompleteButton: window.ldclient.variation('complete-button-v-2', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      customConfig:
        (this.props.team.config.lxpCustomCSS &&
          this.props.team.config.lxpCustomCSS.configs &&
          this.props.team.config.lxpCustomCSS.configs.header) ||
        {},
      configurableHeader: window.ldclient.variation('new-configurable-header', false),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      disableCompleteBtn: false,
      configureCompleteButton: window.ldclient.variation(
        'configurable-colors-for-complete-button',
        false
      ),
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true),
      markAsCompleteEnableForLink: false
    };
    this.newComplete = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    let config = this.props.team.OrgConfig;
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
    this.styles = {
      dropDownAction: {
        paddingRight: 0,
        width: 'auto'
      },
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      userAvatar: {
        marginRight: 0,
        display: 'inline-block'
      },
      cardHeader: {
        padding: '16px 0 0',
        float: 'left'
      },
      lockedContent: {
        height: '619px'
      },
      lockedCard: {
        background: '#fff no-repeat',
        backgroundSize: '100% 100%'
      },
      lockedOwnerCard: {
        background: 'rgba(255, 255, 255, 0.5) no-repeat',
        backgroundSize: '100% 100%'
      },
      bigLockIcon: {
        width: '70px',
        height: '70px',
        marginTop: '25%'
      },
      labelInfoStyle: {
        fontSize: '14px',
        color: '#26273b',
        fontWeight: 'normal'
      },
      audio: {
        width: '100%'
      },
      avatarBox: {
        height: '1.625rem',
        width: '1.625rem',
        marginRight: '0.5625rem',
        position: 'relative'
      },
      privateLabel: {
        marginTop: '25%',
        color: colors.primary
      },
      downloadButton: {
        width: '100%',
        height: '100%',
        padding: 0,
        background: '#454560',
        cursor: 'pointer'
      },
      tooltipStyles: {
        left: '-50%'
      },
      btnIcons: {
        verticalAlign: 'middle',
        width: '1rem',
        height: '0.875rem',
        padding: 0
      }
    };
  }

  componentDidMount() {
    let setStateObject = {};
    if (this.props.card.cardType == 'VideoStream' || this.props.card.cardType == 'video_stream') {
      let isLiveStream = false;
      if (this.props.card && this.props.card.videoStream !== undefined) {
        isLiveStream = this.props.card.videoStream.status === 'live';
        setStateObject['isLiveStream'] = isLiveStream;
      } else if (this.props.card.status) {
        isLiveStream = this.props.card.status === 'live';
        setStateObject['isLiveStream'] = isLiveStream;
      }
    }
    let autoComplete = true;
    if (this.newComplete) {
      switch (this.completeMethodConf) {
        case 'creatorChoose':
          autoComplete = this.props.autoComplete !== undefined ? this.props.autoComplete : true;
          setStateObject['autoComplete'] = autoComplete;
          break;
        case 'creatorNotChoose':
          autoComplete = true;
          setStateObject['autoComplete'] = autoComplete;
          break;
        case 'disabledNext':
          autoComplete = false;
          setStateObject['autoComplete'] = autoComplete;
          break;
        default:
          // FIXME: implement default case
          break;
      }
    }

    let setStateFlag = false;

    if (!isEmpty(setStateObject)) {
      setStateFlag = true;
    } else if (this.state.autoComplete !== autoComplete) {
      setStateFlag = true;
      setStateObject['autoComplete'] = autoComplete;
    }

    if (setStateFlag) {
      if (
        this.props.pathwayPreview &&
        setStateObject['autoComplete'] &&
        !this.props.shouldComplete
      ) {
        setStateObject['isCompleted'] = this.checkCompleteOnRender(this.props.card);
        this.setState(setStateObject);
      } else {
        this.setState(setStateObject);
      }
    }
    this.videoEle = document.querySelectorAll('.slick-slide .card-overview-content');
  }

  componentWillMount() {
    this.fetchComments();
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  checkCompleteOnRender = card => {
    let isCompleted =
      (card.completionState && card.completionState.toUpperCase() === 'COMPLETED') ||
      card.isCompleted;
    let params = getCardParams(card, this.props, 'card') || {};
    let isLinkCard =
      !this.state.showMarkAsComplete &&
      card.resource &&
      (card.resource.url || card.resource.description || card.resource.fileUrl) &&
      card.resource.type !== 'Video' &&
      ((card.readableCardType && card.readableCardType.toUpperCase() === 'ARTICLE') ||
        params.cardType === 'ARTICLE');
    if (
      !isCompleted &&
      (card.cardType !== 'poll' && card.cardType !== 'quiz') &&
      !card.isPrivate &&
      !card.isLocked &&
      !isLinkCard
    ) {
      markAsComplete(card.id, { state: 'complete' });
      isCompleted = true;
    }
    return isCompleted;
  };

  checkComplete = (card, isAnswer) => {
    if (this.state.autoComplete || isAnswer) {
      let isCompleted =
        (card.completionState && card.completionState.toUpperCase() === 'COMPLETED') ||
        card.isCompleted;
      if (
        !isCompleted &&
        ((card.cardType !== 'poll' && card.cardType !== 'quiz') || isAnswer) &&
        !card.isPrivate
      ) {
        markAsComplete(card.id, { state: 'complete' });
        isCompleted = true;
      }
      this.setState({ isCompleted });
    }
  };

  componentWillReceiveProps(nextProps) {
    if (this.state.card.id !== nextProps.card.id) {
      if (nextProps.pathwayPreview) {
        this.setState(
          {
            card: nextProps.card,
            pendingLike: false,
            showComment: false,
            isUpvoted: nextProps.card.isUpvoted,
            votesCount: nextProps.card.votesCount,
            commentsCount: nextProps.card.commentsCount,
            isCompleted:
              (nextProps.card.completionState &&
                nextProps.card.completionState.toUpperCase() === 'COMPLETED') ||
              nextProps.card.isCompleted ||
              (nextProps.card.isCompleted && this.state.autoComplete && !nextProps.card.isLocked),
            markAsCompleteEnableForLink: false
          },
          this.fetchComments
        );
      } else if (!nextProps.pathwayPreview) {
        this.setState({
          card: nextProps.card,
          markAsCompleteEnableForLink: false
        });
      }
    }
    if (this.props.card && this.props.card.id !== nextProps.card.id) {
      getTranscodedVideo.call(
        this,
        nextProps.card,
        nextProps.card,
        this.props.currentUser.id,
        false
      );
    }
  }

  fetchComments = () => {
    if (this.props.showComment) {
      let cardId = this.state.card.cardId || this.state.card.id;
      let isECL = /^ECL-/.test(cardId);
      if (!isECL && this.state.commentsCount) {
        loadComments(
          cardId,
          this.state.commentsCount,
          this.state.card.cardType,
          cardId ? cardId : null
        )
          .then(
            data => {
              this.setState({ comments: data, showComment: true });
            },
            error => {
              console.warn(`error when loadComments for card ${cardId}. Error: ${error}`);
            }
          )
          .catch(err => {
            console.error(`Error in CardOverviewModal.loadComments.func: ${err}`);
          });
      } else {
        this.setState({ comments: [], showComment: true });
      }
    }
  };

  cardUpdated = () => {
    this.props.cardUpdated && this.props.cardUpdated();
  };

  markAsCompleteState = () => {
    !this.state.isCompleted && this.setState({ isCompleted: true });
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise(resolve => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in CardOverviewModal.asyncDispatch.func: ${err}`);
        });
    });
  };

  likeCardHandler = () => {
    if (this.state.pendingLike) {
      return;
    }
    this.setState({ pendingLike: true }, () => {
      this.asyncDispatch(
        toggleLikeCardAsync,
        this.state.card.id,
        this.state.card.cardType,
        !this.state.isUpvoted
      )
        .then(() => {
          this.cardUpdated();
          this.setState({
            votesCount: Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0),
            isUpvoted: !this.state.isUpvoted,
            pendingLike: false
          });
          if (this.state.upshotEnabled) {
            var name;
            if (this.state.card.resource && this.state.card.resource.title) {
              name = this.state.card.resource.title;
            } else {
              name = this.state.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.state.card.cardType,
              type: this.state.card.cardSubtype,
              description: this.state.card.resource.description,
              status: !!this.state.card.completionState
                ? this.state.card.completionState
                : 'Incomplete',
              rating: this.state.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: this.state.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
            });
          }
        })
        .catch(err => {
          console.error(`Error in CardOverviewModal.toggleLikeCardAsync.func: ${err}`);
        });
    });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count }, () => {
      if (count === -1) {
        this.setState({ showComment: false }, this.fetchComments);
      }
    });
  };

  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}`
    ) {
      return;
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };

  linkToPush = (card, handle, e) => {
    e.preventDefault();
    const { currentUser } = this.props;
    if (e && e.target && e.target.name === 'marked-link') {
      return;
    }
    this.props.dispatch(close());
    closeCard();
    e.stopPropagation();
    if (card) {
      this.standaloneLinkClickHandler(card);
    } else if (handle) {
      this.props.dispatch(
        push(`/${handle.replace('@', '') === currentUser.handle ? 'me' : handle}`)
      );
    }
    this.setState(
      {
        markAsCompleteEnableForLink: true
      },
      () => {
        if (
          !this.state.showMarkAsComplete &&
          this.state.autoComplete &&
          this.state.card &&
          this.state.card.completionState !== 'COMPLETED' &&
          !this.state.card.isLinkCard
        ) {
          this.completeClickHandler();
        }
      }
    );
  };

  linkToPushDescription = (card, e) => {
    if (e.target && e.target.className && e.target.className === 'read-more') {
      e.stopPropagation();
      this.setState(
        {
          markAsCompleteEnableForLink: true
        },
        () => {
          if (
            !this.state.showMarkAsComplete &&
            this.state.autoComplete &&
            this.state.card &&
            this.state.card.completionState !== 'COMPLETED'
          ) {
            this.completeClickHandler();
          }
        }
      );
    } else {
      let params = getCardParams(card, this.props, 'card') || {};

      let isLinkCard =
        !this.state.showMarkAsComplete &&
        card.resource &&
        (card.resource.url || card.resource.description || card.resource.fileUrl) &&
        card.resource.type !== 'Video' &&
        ((card.readableCardType && card.readableCardType.toUpperCase() === 'ARTICLE') ||
          params.cardType === 'ARTICLE');

      if (isLinkCard) {
        card.isLinkCard = true;
      } else {
        card.isLinkCard = false;
      }

      this.linkToPush(card, null, e);
    }
  };

  completeClickHandler = () => {
    let hideMarkAsActive =
      this.props.modal.card &&
      this.props.modal.card.cardType == 'pack' &&
      this.props.modal.card.completionState == 'COMPLETED';
    if (!this.state.isCompleted) {
      this.setState({ disableCompleteBtn: true });
      markAsComplete(this.state.card.id, { state: 'complete' })
        .then(() => {
          let card = this.state.card;
          card.completionState = 'COMPLETED';
          this.setState({ isCompleted: true, disableCompleteBtn: false, card: card }, () => {
            this.props.dispatch(openSnackBar('Marked as complete', true));
          });
          if (this.state.upshotEnabled) {
            var name;
            if (this.state.card.resource && this.state.card.resource.title) {
              name = this.state.card.resource.title;
            } else {
              name = this.state.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.state.card.cardType,
              type: this.state.card.cardSubtype,
              description: this.state.card.resource.description,
              status: 'COMPLETED',
              rating: this.state.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: 'Mark Completed'
            });
          }
          this.cardUpdated();
        })
        .catch(err => {
          console.error(`Error in CardOverviewModal.markAsComplete.func: ${err}`);
        });
    } else {
      if (hideMarkAsActive) {
        return;
      }
      this.setState({ disableCompleteBtn: true });
      markAsUncomplete(this.state.card.id)
        .then(() => {
          let card = this.state.card;
          card.completionState = 'INITIALIZED';
          this.setState(
            {
              isCompleted: false,
              disableCompleteBtn: false,
              markAsCompleteEnableForLink: false,
              card: card
            },
            () => {
              this.props.dispatch(openSnackBar('Your have marked this task as incomplete', true));
            }
          );
          if (this.state.upshotEnabled) {
            var name;
            if (this.state.card.resource && this.state.card.resource.title) {
              name = this.state.card.resource.title;
            } else {
              name = this.state.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.state.card.cardType,
              type: this.state.card.cardSubtype,
              description: this.state.card.resource.description,
              status: !!this.state.card.completionState
                ? this.state.card.completionState
                : 'Incomplete',
              rating: this.state.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: 'Mark Incomplete'
            });
          }
          this.cardUpdated();
        })
        .catch(err => {
          console.error(`Error in CardOverviewModal.markAsUncomplete.func: ${err}`);
        });
    }
  };

  handleCardAnalyticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.card));
  };

  filestackFilesRender = (audioFileStack, isDownloadContentDisabled) => {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.state.card.filestack && !!this.state.card.filestack.length) {
      return (
        <div>
          {audioFileStack ? (
            <audio
              className="audio-preview"
              controls
              src={this.state.card.filestack[0].url}
              style={this.styles.audio}
              onError={() => {
                this.refreshAudioUrl(this.state.card.filestack[0].handle);
              }}
            />
          ) : (
            this.state.card.filestack.map(file => {
              if (file.mimetype && ~file.mimetype.indexOf('image/')) {
                return null;
              } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                return (
                  <div className="card-text-block" key={file.handle}>
                    <div className="fp fp_video">
                      <span>
                        {this.state.transcodedVideoStatus &&
                          (this.state.transcodedVideoStatus === 'completed' ? (
                            <video
                              controls
                              src={this.state.transcodedVideoUrl}
                              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                            />
                          ) : (
                            <img
                              className="waiting-video"
                              src="/i/images/video_processing_being_processed.jpg"
                            />
                          ))}
                      </span>
                    </div>
                  </div>
                );
              } else {
                return (
                  <div className="card-text-block" key={file.handle}>
                    <div className="fp fp_preview">
                      <span>
                        <iframe
                          height="480px"
                          width="100%"
                          src={pdfPreviewUrl(file.url, expireAfter, this.props.currentUser.id)}
                        />
                        {!isDownloadContentDisabled && this.downloadBlock(file)}
                      </span>
                    </div>
                  </div>
                );
              }
            })
          )}
        </div>
      );
    } else return null;
  };

  downloadBlock(file) {
    let downloadLabel = tr('Download');
    return (
      <span className="roll">
        <a href={file.url} download={file.handle} target="_blank">
          <IconButton
            aria-label={downloadLabel}
            iconStyle={this.styles.btnIcons}
            style={this.styles.downloadButton}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={downloadLabel}
          >
            <Downloadv2 />
          </IconButton>
        </a>
      </span>
    );
  }

  linkExtract = (resource, cardId) => {
    return typeof resource.url !== 'undefined'
      ? cardId
        ? `/insights/${encodeURIComponent(cardId)}/visit`
        : resource.url
      : `/insights/${cardId}`;
  };

  imageRender = cardSvgBackground => {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    return (
      <svg width="100%" height="100%" style={this.styles.mainSvg}>
        <title>
          <RichTextReadOnly text={this.state.card.message} />
        </title>
        <SvgImageResized
          cardId={`${this.state.card.id}`}
          xlinkHref={addSecurity(cardSvgBackground, expireAfter, this.props.currentUser.id)}
          width="100%"
          style={this.styles.svgImage}
          height="100%"
          resizeOptions={'height:360'}
        />
      </svg>
    );
  };

  liveCommentsCount = commentCount => {
    this.setState({ liveCommentsCount: commentCount });
  };

  getCardStatus() {
    if (this.props.card && this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.status;
    } else if (this.props.card && this.props.card.status) {
      return this.props.card.status;
    } else {
      return '';
    }
  }

  getCardUuid() {
    if (this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.uuid;
    } else if (this.props.card.uuid !== undefined) {
      return this.props.card.uuid;
    } else {
      return '';
    }
  }

  showTopicToggleClick = () => {
    this.setState({ showTopic: !this.state.showTopic });
  };

  getPricingPlans() {
    return this.state.edcastPlansForPricing &&
      this.props.card.cardMetadatum &&
      this.props.card.cardMetadatum.plan
      ? tr(startCase(toLower(this.props.card.cardMetadatum.plan)))
      : tr('Free');
  }

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  cardStatusButton = (e, card, isCompleted) => {
    e.preventDefault();
    if (
      !this.state.disableCompleteBtn &&
      (!card.markFeatureDisabledForSource && (this.isUncompleteEnabled || !isCompleted))
    ) {
      this.completeClickHandler();
    }
  };

  refreshVideoUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let video = document.getElementsByClassName('ml-video-cards')[0];
          let seen = video.currentTime;
          video.setAttribute('src', newUrl);
          video.currentTime = seen;
          if (seen !== 0) {
            video.play();
          }
        })
        .catch(err => {
          console.error(
            `Error in CardOverviewModal.refreshVideoUrl.refreshFilestackUrl.func : ${err}`
          );
        });
    }
  };

  refreshAudioUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let audio = document.getElementsByClassName('audio-preview')[0];
          let seen = audio.currentTime;
          audio.setAttribute('src', newUrl);
          audio.currentTime = seen;
          if (seen !== 0) {
            audio.play();
          }
        })
        .catch(err => {
          console.error(
            `Error in CardOverviewModal.refreshAudioUrl.refreshFilestackUrl.func : ${err}`
          );
        });
    }
  };

  handleResourceClicked = (e, url) => {
    e.preventDefault();
    e.stopPropagation();
    if (url) {
      window.open(url, '_blank');
      this.setState(
        {
          markAsCompleteEnableForLink: true
        },
        () => {
          if (
            !this.state.showMarkAsComplete &&
            this.state.autoComplete &&
            this.state.card &&
            this.state.card.completionState !== 'COMPLETED'
          ) {
            this.completeClickHandler();
          }
        }
      );
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let card = checkResources(this.state.card);
    let params = getCardParams(card, this.props, 'card') || {};
    let message = this.state.card.message || this.state.card.title || this.state.card.name;
    let description;
    if (card.cardType == 'course' && card.resource.title) {
      message = card.resource.title;
    }

    let logoObj = logoType.LOGO;
    let videoData = {},
      iconFileSrc = '';
    let svgStyle = {
      filter: `url(#blur-effect-overview-${card.id})`
    };
    let isFileAttached = card.filestack && !!card.filestack.length;
    let videoFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('video/')
    );
    let imageFileStack = !!(
      isFileAttached &&
      ((card.filestack[0].mimetype && ~card.filestack[0].mimetype.indexOf('image/')) ||
        (!card.filestack[0].mimetype && !card.filestack[0].handle))
    );
    let audioFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('audio/')
    );
    let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
    let cardType = audioFileStack ? 'AUDIO' : getCardType(card, videoFileStack, fileFileStack);
    let imageExists =
      (card.resource &&
        card.resource.imageUrl &&
        !~card.resource.imageUrl.indexOf('default_card_image')) ||
      imageFileStack;
    if (cardType === 'VIDEO') {
      videoData = getVideoStatus(card);
      imageExists =
        (card.videoStream &&
          card.videoStream.imageUrl &&
          !~card.videoStream.imageUrl.indexOf('default_card_image')) ||
        imageExists;
    }
    let gDriveLink;
    if (cardType === 'ARTICLE') {
      gDriveLink =
        card.resource &&
        card.resource.url &&
        (!!~card.resource.url.indexOf('docs.google.com') ||
          !!~card.resource.url.indexOf('drive.google.com') ||
          !!~card.resource.url.indexOf('sharepoint.com'));
    }
    let cardImage = getCardImage(card, imageFileStack);
    let defaultImage =
      this.props.defaultImage ||
      '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg';
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let cardSvgBackground = imageExists ? cardImage : defaultImage;
    let isCompleted = this.state.isCompleted;
    let htmlRender = !!card.resource && (!!card.resource.title || !!card.resource.description);
    if (isFileAttached) {
      let fileType = card.filestack[0].mimetype && card.filestack[0].mimetype.slice(-3);
      iconFileSrc = getFleIcon(fileType);
      if (card.filestack[0].handle === message) {
        message = '';
      }
    }
    if (cardType === 'AUDIO') {
      imageExists = !!(
        card.filestack &&
        card.filestack[1] &&
        card.filestack[1].mimetype &&
        !!~card.filestack[1].mimetype.indexOf('image/')
      );
    }
    let isOwner = card.author && card.author.id == this.props.currentUser.id;
    let resourceUrl = htmlRender || gDriveLink ? this.linkExtract(card.resource, card.id) : null;
    let disableTopics = card.tags && card.tags.length > 0;

    let isYoutubeVideo =
      card.resource &&
      card.resource.siteName &&
      (card.resource.siteName.toLowerCase() === 'youtube' ||
        card.resource.url.indexOf('https://youtube.com') > -1 ||
        card.resource.url.indexOf('https://www.youtube.com') > -1 ||
        card.resource.url.indexOf('https://www.youtu.be') > -1 ||
        card.resource.url.indexOf('https://youtu.be') > -1);

    let isVimeoVideo =
      card.resource &&
      card.resource.siteName &&
      (card.resource.siteName.toLowerCase() === 'vimeo' ||
        card.resource.url.indexOf('https://vimeo.com') > -1);

    if (htmlRender) {
      card.resource && card.resource.url && card.resource.url.includes('/api/scorm/') ? (
        card.state == 'published' ? (
          (description = `<a title='${tr('open in new tab')}' className="link-to-original" href='${
            card.resource.url
          }' target="_blank"> ${tr('Click here to access the course')}</a>`)
        ) : (
          <div className="scorm-processing">
            <Spinner />
            <p>{this.setScormStateMsg(card)}</p>
          </div>
        )
      ) : (
        (description = card.resource.description)
      );
      if (message === card.resource.url || message === card.resource.description) message = '';
      if (card.resource.description && !~card.resource.description.indexOf(`target='_blank'>`)) {
        if (card.resource && !isYoutubeVideo && !isVimeoVideo) {
          description += `<a class="read-more" href='${resourceUrl}' target='_blank'> ${tr(
            ' Read More'
          )} </a>`;
        }
      }
    }
    let config = this.props.team.config;
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    let showCreator =
      config &&
      ((typeof config.show_creator_in_card == 'undefined' && !config.show_creator_in_card) ||
        !!config.show_creator_in_card);
    let scormCard = card.filestack && card.filestack[0] && card.filestack[0].scorm_course;
    let resourceImage = params.poster || cardSvgBackground;

    let customStyle = this.state.configurableHeader ? this.state.customConfig : {};
    let markAsCompleteColor = colors.primary;
    if (this.state.configurableHeader) {
      markAsCompleteColor =
        (customStyle.utilities && customStyle.utilities.markAsCompleteColor) || colors.primary;
    }

    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = {};
    let skillcoin_image = '/i/images/skillcoin-new.png';
    let hideMarkAsActive =
      this.props.modal.card &&
      this.props.modal.card.cardType == 'pack' &&
      this.props.modal.card.completionState == 'COMPLETED';

    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: 'SKILLCOIN' }) ||
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }

    if (priceData && priceData.currency == 'SKILLCOIN') {
      priceData.symbol = <img className="pricing_skill_coins_label_icon" src={skillcoin_image} />;
    }
    let isAbleUploadScorm =
      Permissions['enabled'] !== undefined && Permissions.has('UPLOAD_SCORM_CONTENT'); //EP-15959: removed autoMarkAsCompleteScorm

    let background =
      (this.state.configureCompleteButton &&
        (isCompleted ? colors.completedBackground : colors.markAsCompleteBackground)) ||
      null;

    let textColor =
      (this.state.configureCompleteButton &&
        (isCompleted ? colors.completedText : colors.markAsCompleteText)) ||
      null;

    let markAsCompleteDisabledForLink =
      !this.state.showMarkAsComplete &&
      !this.state.markAsCompleteEnableForLink &&
      (card &&
        card.resource &&
        (card.resource.url || card.resource.description || card.resource.fileUrl)) &&
      card.resource.type !== 'Video' &&
      ((card.readableCardType && card.readableCardType.toUpperCase() === 'ARTICLE') ||
        params.cardType === 'ARTICLE') &&
      !((card.assignment && card.assignment.state == 'COMPLETED') || isCompleted);

    return (
      <div className="card-overview-content">
        <div className="blankContainer" tabIndex={0} />
        {(this.props.isLock && !this.props.isPathwayOwner && !this.props.isShowLockedCardContent) ||
        this.props.isPrivate ? (
          <div className="row card-content-inner-wrapper" style={this.styles.lockedContent}>
            <div className="locked-card" style={this.styles.lockedCard}>
              {this.props.isPrivate ? (
                <h4 style={this.styles.privateLabel}>
                  {tr('You do not have permission to view this card')}
                </h4>
              ) : (
                <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
              )}
            </div>
          </div>
        ) : (
          <div className="row card-content-inner-wrapper">
            {this.props.isLock && this.props.isPathwayOwner && (
              <div className="locked-card" style={this.styles.lockedOwnerCard}>
                <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
              </div>
            )}
            <div className="small-12 medium-8 large-8 left-content card-modal-left-column">
              {(cardType.toUpperCase() === 'VIDEO' || card.cardSubtype === 'video') &&
                !videoFileStack && (
                  <div>
                    <VideoStream
                      hideTitle={true}
                      card={card}
                      sourceType={
                        this.props.isPinnedInChannel ? 'channelPinnedCard' : 'cardOverviewModal'
                      }
                    />
                    <div className={videoData.videoLabelClass}>{tr(videoData.videoLabelText)}</div>
                  </div>
                )}
              {videoFileStack && (
                <div className="fp fp_video fp_video_card">
                  {this.state.transcodedVideoStatus &&
                    (this.state.transcodedVideoStatus === 'completed' ? (
                      <video
                        preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                        id={`video-${card.filestack && card.filestack[0].handle}`}
                        poster={
                          (card.filestack &&
                            card.filestack[1] &&
                            card.filestack[1].mimetype &&
                            card.filestack[1].mimetype.includes('image/') &&
                            card.filestack[1] &&
                            addSecurity(
                              card.filestack[1].url,
                              expireAfter,
                              this.props.currentUser.id
                            )) ||
                          addSecurity(params.poster, expireAfter, this.props.currentUser.id)
                        }
                        controls
                        src={this.state.transcodedVideoUrl}
                        controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                        autoPlay={
                          this.videoEle ? false : this.props.team.config['enable-video-auto-play']
                        }
                        className="ml-video-cards"
                        onError={() => {
                          this.refreshVideoUrl(card.filestack[0].handle);
                        }}
                      />
                    ) : (
                      <img
                        className="waiting-video"
                        src="/i/images/video_processing_being_processed.jpg"
                      />
                    ))}
                </div>
              )}
              {(imageExists || cardType === 'ARTICLE' || scormCard) &&
                (cardType !== 'VIDEO' && card.cardSubtype !== 'video') && (
                  <a
                    href="#"
                    className="main-image card-image-block anchorAlignment"
                    onClick={
                      card.id.startsWith('ECL') ? null : this.linkToPush.bind(this, card, null)
                    }
                  >
                    <div className="card-blurred-background">
                      <svg width="100%" height="100%">
                        <title>
                          {' '}
                          <MarkdownRenderer markdown={this.state.card.message} />
                        </title>
                        <SvgImageResized
                          cardId={`${this.state.card.id}`}
                          resizeOptions={'height:360'}
                          style={svgStyle}
                          xlinkHref={addSecurity(
                            resourceImage,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          x="-30%"
                          y="-30%"
                          width="160%"
                          height="160%"
                        />
                        <filter id={`blur-effect-overview-${card.id}`}>
                          <feGaussianBlur stdDeviation="10" />
                        </filter>
                      </svg>
                    </div>
                    {resourceUrl && (cardType === 'ARTICLE' || cardType === 'COURSE') ? (
                      <a
                        title={tr('open in new tab')}
                        className="matte-hover"
                        href="#"
                        onClick={e =>
                          this.handleResourceClicked(
                            e,
                            card.resource && isYoutubeVideo
                              ? card.resource.url
                              : scormCard
                              ? card.resource.url
                              : resourceUrl
                          )
                        }
                      >
                        {this.imageRender(resourceImage)}
                      </a>
                    ) : (
                      this.imageRender(resourceImage)
                    )}
                  </a>
                )}
              <div className="smartbite-overview">
                {card.author && (
                  <CardHeader
                    title={
                      <div className="channels-block">
                        <div className="author-info-container">
                          {showCreator && (
                            <a
                              href="#"
                              className="user-name"
                              onClick={this.linkToPush.bind(null, null, card.author.handle)}
                            >
                              {this.props.card.author.fullName
                                ? this.props.card.author.fullName
                                : `${
                                    this.props.card.author.firstName
                                      ? this.props.card.author.firstName
                                      : ''
                                  } ${
                                    this.props.card.author.lastName
                                      ? this.props.card.author.lastName
                                      : ''
                                  }`}
                            </a>
                          )}
                          {showCreator && <br />}
                          {this.state.cardClickHandle === 'modal' && (
                            <CreationDate
                              card={card}
                              standaloneLinkClickHandler={this.standaloneLinkClickHandler.bind(
                                this,
                                card
                              )}
                            />
                          )}
                        </div>
                      </div>
                    }
                    subtitle={
                      card.publishedAt && (
                        <div className="header-secondary-text">
                          <span className="matte" />
                        </div>
                      )
                    }
                    avatar={
                      showCreator ? (
                        <a
                          style={this.styles.userAvatar}
                          onClick={this.linkToPush.bind(null, null, card.author.handle)}
                        >
                          <BlurImage
                            style={this.styles.avatarBox}
                            id={card.id}
                            image={
                              card.author &&
                              (card.author.picture ||
                                (card.author.avatarimages && card.author.avatarimages.small) ||
                                defaultUserImage)
                            }
                          />
                        </a>
                      ) : null
                    }
                    style={this.styles.cardHeader}
                  />
                )}

                <div className="logo-type">
                  {(card.eclSourceLogoUrl ||
                    (card.eclSourceTypeName && logoObj[card.eclSourceTypeName] !== undefined)) && (
                    <img
                      onClick={this.linkToPush.bind(
                        null,
                        null,
                        `discover/${card.eclSourceTypeName}`
                      )}
                      className="provider-logo"
                      src={addSecurity(
                        card.eclSourceLogoUrl || logoObj[card.eclSourceTypeName],
                        expireAfter,
                        this.props.currentUser.id
                      )}
                    />
                  )}
                  {iconFileSrc && (
                    <img
                      className="provider-logo "
                      src={addSecurity(iconFileSrc, expireAfter, this.props.currentUser.id)}
                    />
                  )}
                  <span className="label-text">
                    {tr((card.readableCardType || cardType).toUpperCase())}
                  </span>
                </div>
                {this.state.clc &&
                  this.props.card.eclDurationMetadata &&
                  !!this.props.card.eclDurationMetadata.calculated_duration &&
                  this.props.card.eclDurationMetadata.calculated_duration_display &&
                  this.props.card.eclDurationMetadata.calculated_duration > 0 && (
                    <div className="time-to-read">
                      {tr('Duration')}:{' '}
                      {this.props.card.eclDurationMetadata.calculated_duration_display}
                    </div>
                  )}
              </div>
              {this.state.edcastPricing &&
                this.props.card.readableCardType != 'jobs' &&
                !params.isHideSpecialInfo && (
                  <div className="edcast-pricing">
                    {tr('Price')} :{' '}
                    {this.props.card.isPaid && priceData && priceData.amount ? (
                      <span>
                        {priceData.symbol}
                        {priceData.amount}
                      </span>
                    ) : (
                      this.getPricingPlans()
                    )}
                  </div>
                )}
              {this.props.startDate && (
                <div className="card-text-block">
                  {tr('Start Date')}:{' '}
                  <strong>
                    <DateConverter isMonthText="true" date={this.props.startDate} />
                  </strong>
                </div>
              )}
              {this.props.dueAt && (
                <div className="card-text-block">
                  {tr('Due Date')}:{' '}
                  <strong>
                    <DateConverter isMonthText="true" date={this.props.dueAt} />
                  </strong>
                </div>
              )}
              <div className="card-text-block">
                {cardType === 'TEXT' ? (
                  <div className="">
                    <div>
                      {card.title && (
                        <div
                          className="smartbite-preview-text card-title"
                          style={this.styles.truncate}
                          onClick={
                            card.id.startsWith('ECL')
                              ? null
                              : this.linkToPush.bind(this, card, null)
                          }
                        >
                          <MarkdownRenderer markdown={card.title} />
                        </div>
                      )}
                      <div className="" />
                      {message && (
                        <div
                          className="smartbite-preview-text card-desc"
                          style={this.styles.truncate}
                          onClick={
                            card.id.startsWith('ECL')
                              ? null
                              : this.linkToPush.bind(this, card, null)
                          }
                        >
                          <RichTextReadOnly text={convertRichText(message)} />
                        </div>
                      )}
                    </div>
                  </div>
                ) : (
                  <div className="">
                    {htmlRender ? (
                      <div className="article-card-block">
                        {(message || card.resource.title) &&
                          (card.resource && (isYoutubeVideo || isVimeoVideo) ? (
                            <div
                              className="smartbite-preview-text card-title"
                              dangerouslySetInnerHTML={{ __html: message || card.resource.title }}
                            />
                          ) : (
                            <a
                              title={tr('open in new tab')}
                              href="#"
                              onClick={e =>
                                this.handleResourceClicked(
                                  e,
                                  card.resource && (isYoutubeVideo || isVimeoVideo)
                                    ? ''
                                    : scormCard
                                    ? card.resource.url
                                    : resourceUrl
                                )
                              }
                              className={`matte-hover`}
                            >
                              {card.resource &&
                              card.resource.url &&
                              card.resource.url.includes('/api/scorm/') ? (
                                card.message !== card.resource.title ? (
                                  <div
                                    className="smartbite-preview-text card-title"
                                    dangerouslySetInnerHTML={{ __html: message }}
                                  />
                                ) : (
                                  ''
                                )
                              ) : (
                                <div
                                  className="smartbite-preview-text card-title"
                                  dangerouslySetInnerHTML={{
                                    __html: message || card.resource.title
                                  }}
                                />
                              )}
                            </a>
                          ))}

                        <div
                          onClick={
                            card.id.startsWith('ECL')
                              ? null
                              : this.linkToPushDescription.bind(this, card)
                          }
                          className="smartbite-preview-text article-card-block card-desc"
                          dangerouslySetInnerHTML={{ __html: description }}
                        />
                      </div>
                    ) : gDriveLink ? (
                      <a
                        title={tr('open in new tab')}
                        href="#"
                        onClick={e => this.handleResourceClicked(e, resourceUrl)}
                        className="matte-hover"
                      >
                        <div
                          className="smartbite-preview-text card-title"
                          dangerouslySetInnerHTML={{ __html: card.title || card.resource.title }}
                        />
                      </a>
                    ) : (
                      <div>
                        <div
                          className="smartbite-preview-text card-desc"
                          onClick={
                            card.id.startsWith('ECL')
                              ? null
                              : this.linkToPush.bind(this, card, null)
                          }
                        >
                          {card.title ? (
                            <MarkdownRenderer markdown={card.title} />
                          ) : (
                            <RichTextReadOnly text={convertRichText(message)} />
                          )}
                          {card.cardType === 'project' && card.message ? (
                            <div>
                              <RichTextReadOnly text={convertRichText(card.message)} />
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                    )}
                    {(cardType === 'POLL' || cardType === 'QUIZ') && (
                      <div className="smartbite-poll-content">
                        <Poll
                          card={card}
                          cardOverview={true}
                          cardUpdated={this.cardUpdated.bind(this)}
                          checkComplete={this.checkComplete}
                          currentUserId={this.props.currentUser.id}
                        />
                      </div>
                    )}
                  </div>
                )}
                {fileFileStack &&
                  !scormCard &&
                  this.filestackFilesRender(audioFileStack, isDownloadContentDisabled)}
              </div>
              {(card.tags && card.tags.length) > 0 && this.state.showTopic && (
                <div className="tags medium-8 large-8 columns overview-tags-container">
                  <span className="info-bar-name" style={this.styles.labelInfoStyle}>
                    {tr('Tags:')}
                    {'\u00A0'}
                  </span>
                  {card.tags.map((tag, index) => {
                    return (
                      <span key={tag.id}>
                        <a className="tag-name" title={tag.name}>
                          {tag.name}
                          {index !== card.tags.length - 1 && <span>,</span>}
                        </a>
                        {'\u00A0'}
                      </span>
                    );
                  })}
                </div>
              )}
              {!params.hideProvider && (card.provider || card.providerImage) && (
                <div className="provider-info-bar">
                  <span className="info-bar-name" style={this.styles.labelInfoStyle}>
                    {tr('Provider')}: {'\u00A0'}
                  </span>
                  {card.provider && <span className="provider-name">{card.provider}</span>}
                  {card.providerImage && (
                    <img
                      className="provider-image inline-block"
                      src={addSecurity(card.providerImage, expireAfter, this.props.currentUser.id)}
                    />
                  )}
                </div>
              )}
            </div>
            <div className="comments-container small-12 medium-4 large-4">
              <div className="comments">
                <div>
                  {this.state.showComment && !this.state.isLiveStream && (
                    <small
                      className="comments-count"
                      style={{
                        verticalAlign: 'middle',
                        display: 'inline-block',
                        marginTop: '11px'
                      }}
                    >
                      {this.state.commentsCount ? abbreviateNumber(this.state.commentsCount) : ''}
                      {this.state.commentsCount > 1 ? tr(' COMMENTS') : tr(' COMMENT')}
                    </small>
                  )}
                  {this.state.showComment && this.state.isLiveStream && (
                    <small
                      className="comments-count"
                      style={{
                        verticalAlign: 'middle',
                        display: 'inline-block',
                        marginTop: '11px'
                      }}
                    >
                      {this.state.liveCommentsCount
                        ? abbreviateNumber(this.state.liveCommentsCount)
                        : ''}
                      {this.state.liveCommentsCount > 1 ? tr(' COMMENTS') : tr(' COMMENT')}
                    </small>
                  )}
                </div>
                {this.state.showComment && <div className="break-line" />}
                <div className="comments-block-list">
                  {this.state.showComment && this.state.isLiveStream && (
                    <LiveCommentList
                      isPinned={this.props.isPinned}
                      cardId={card.cardId ? card.cardId + '' : card.id}
                      cardOwner={this.props.author ? this.props.author.id : ''}
                      cardType={card.cardType}
                      pending={card.commentPending}
                      videoId={card.cardId ? card.id : null}
                      updateCommentCount={this.updateCommentCount}
                      cardStatus={this.getCardStatus()}
                      cardUuid={this.getCardUuid()}
                      liveCommentsCount={this.liveCommentsCount}
                      overViewModal={true}
                      inModal={true}
                      hideCommentModal={this.props.hideCommentModal}
                      showConfirmationModal={this.props.showConfirmationModal}
                    />
                  )}
                  {this.state.showComment && !this.state.isLiveStream && (
                    <CommentList
                      isPinned={this.props.isPinned}
                      cardId={card.id + ''}
                      cardOwner={card.author ? card.author.id : ''}
                      cardType={card.cardType}
                      comments={this.state.comments}
                      pending={card.commentPending}
                      numOfComments={this.state.commentsCount}
                      updateCommentCount={this.updateCommentCount}
                      overViewModal={true}
                      pathwayPreview={this.props.pathwayPreview}
                      videoId={card.id ? card.id : null}
                      inModal={true}
                      hideCommentModal={this.props.hideCommentModal}
                      showConfirmationModal={this.props.showConfirmationModal}
                    />
                  )}
                </div>
                <div className="modal-actions-bar">
                  {Permissions.has('LIKE_CONTENT') && (
                    <div className="icon-inline-block my-icon-button-block">
                      <button
                        className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                        onClick={this.likeCardHandler}
                      >
                        {!this.state.isUpvoted && (
                          <span tabIndex={-1} className="hideOutline" aria-label="like">
                            <LikeIcon />
                          </span>
                        )}
                        {this.state.isUpvoted && (
                          <span tabIndex={-1} className="hideOutline" aria-label="liked">
                            <LikeIconSelected />
                          </span>
                        )}
                        <span className="tooltiptext">{tr('Like')}</span>
                      </button>
                      <small className="votes-count">
                        {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : ''}
                      </small>
                    </div>
                  )}

                  {cardType !== 'QUIZ' &&
                    card.author &&
                    (isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                      <div className="icon-inline-block my-icon-button-block">
                        <button
                          aria-label={tr('Card Statistics')}
                          className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center statistics"
                          onClick={this.handleCardAnalyticsModal}
                        >
                          <span tabIndex={-1} className="hideOutline">
                            <CardAnalyticsV2 />
                          </span>
                          <span className="tooltiptext">{tr('Card Statistics')}</span>
                        </button>
                      </div>
                    )}
                  {cardType !== 'QUIZ' &&
                    cardType !== 'PROJECT' &&
                    cardType !== 'POLL' &&
                    Permissions.has('MARK_AS_COMPLETE') &&
                    !(scormCard && isAbleUploadScorm) &&
                    ((card.assignment && card.assignment.state == 'COMPLETED') ||
                      !card.markFeatureDisabledForSource) &&
                    !this.state.showNewDesignForCompleteButton && (
                      <div className="icon-inline-block my-icon-button-block">
                        <button
                          className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center completed"
                          onClick={
                            !card.markFeatureDisabledForSource &&
                            (this.isUncompleteEnabled || !isCompleted)
                              ? this.completeClickHandler
                              : () => {
                                  return null;
                                }
                          }
                        >
                          <span tabIndex={-1} className="hideOutline">
                            <CompletedAssignment
                              color={isCompleted ? markAsCompleteColor : colors.gray}
                            />
                          </span>
                          <span className="tooltiptext">
                            {!card.markFeatureDisabledForSource &&
                              tr(
                                isCompleted
                                  ? this.isUncompleteEnabled
                                    ? hideMarkAsActive
                                      ? 'Completed'
                                      : 'Mark as Incomplete'
                                    : 'Completed'
                                  : 'Mark as Complete'
                              )}
                          </span>
                        </button>
                      </div>
                    )}
                  {cardType !== 'QUIZ' &&
                    cardType !== 'PROJECT' &&
                    cardType !== 'POLL' &&
                    Permissions.has('MARK_AS_COMPLETE') &&
                    !(scormCard && isAbleUploadScorm) &&
                    ((card.assignment && card.assignment.state == 'COMPLETED') ||
                      !card.markFeatureDisabledForSource) &&
                    this.state.showNewDesignForCompleteButton && (
                      <div className="icon-mark-block my-icon-button-block">
                        <button
                          style={{ backgroundColor: background, color: textColor }}
                          className={`my-icon-button tooltip tooltip_bottom-center completed ${
                            !this.state.configureCompleteButton ? 'action-btn-color' : ''
                          }  ${
                            isCompleted
                              ? 'my-icon-button_mark_incomplete'
                              : `my-icon-button_mark_complete ${
                                  markAsCompleteDisabledForLink
                                    ? 'mark_as_complete_button_disabled'
                                    : ''
                                }`
                          }`}
                          onClick={e => this.cardStatusButton(e, card, isCompleted)}
                          disabled={this.state.disableCompleteBtn || markAsCompleteDisabledForLink}
                        >
                          <CheckedIcon
                            color={textColor || '#fff'}
                            className="mark-complete-incomplete-button"
                          />
                          {tr(isCompleted ? 'Completed' : 'Mark as Complete')}
                          <span className="tooltiptext">
                            {!card.markFeatureDisabledForSource &&
                              tr(
                                markAsCompleteDisabledForLink
                                  ? 'Please visit link url'
                                  : isCompleted
                                  ? this.isUncompleteEnabled
                                    ? hideMarkAsActive
                                      ? 'Completed'
                                      : 'Mark as Incomplete'
                                    : 'Completed'
                                  : 'Mark as Complete'
                              )}
                          </span>
                        </button>
                      </div>
                    )}
                  <div className="float-right icon-inline-block icon-dropdown">
                    <InsightDropDownActions
                      card={this.state.card}
                      author={this.state.card.author}
                      dismissible={false}
                      style={this.styles.dropDownAction}
                      disableTopics={disableTopics}
                      isStandalone={false}
                      cardUpdated={this.cardUpdated.bind(this)}
                      markAsComplete={this.markAsCompleteState.bind(this)}
                      assignable={false}
                      showUnPinOption={this.props.showUnPinOption}
                      unpinClickHandler={this.props.unpinClickHandler}
                      deleteSharedCard={this.props.deleteSharedCard}
                      showTopicToggleClick={this.showTopicToggleClick}
                      isCompleted={isCompleted}
                      isOverviewModal={true}
                      callCompleteClickHandler={this.completeClickHandler}
                      type={'OverViewModal'}
                      removeCardFromCarousel={this.props.removeCardFromCarousel}
                      markAsCompleteDisabledForLink={markAsCompleteDisabledForLink}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

CardOverviewModal.propTypes = {
  card: PropTypes.object,
  currentUser: PropTypes.object,
  author: PropTypes.object,
  team: PropTypes.object,
  hideCommentModal: PropTypes.bool,
  showUnPinOption: PropTypes.bool,
  autoComplete: PropTypes.bool,
  pathwayPreview: PropTypes.bool,
  isPinned: PropTypes.bool,
  showComment: PropTypes.bool,
  isLock: PropTypes.bool,
  isPrivate: PropTypes.bool,
  isPathwayOwner: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  showConfirmationModal: PropTypes.func,
  deleteSharedCard: PropTypes.any,
  cardUpdated: PropTypes.func,
  unpinClickHandler: PropTypes.func,
  pathname: PropTypes.string,
  defaultImage: PropTypes.string,
  dueAt: PropTypes.string,
  startDate: PropTypes.string,
  modal: PropTypes.object,
  parentSource: PropTypes.string,
  isPinnedInChannel: PropTypes.bool,
  isCompleted: PropTypes.bool,
  removeCardFromCarousel: PropTypes.func,
  shouldComplete: PropTypes.bool
};

CardOverviewModal.defaultProps = {
  showUnPinOption: false,
  unpinClickHandler: function() {},
  hideCommentModal: false,
  showConfirmationModal: function() {}
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS(),
    modal: state.modal.toJS()
  };
}
export default connect(mapStoreStateToProps)(CardOverviewModal);
