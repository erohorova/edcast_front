import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { close } from '../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getUserSabaCourses } from 'edc-web-sdk/requests/users.v2';
import CourseCard from '../cards/CourseCard';
import CircularProgress from 'material-ui/CircularProgress';
import TextField from 'material-ui/TextField';

class CourseCardModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aggregations: [],
      totalCourses: 0,
      courses: [],
      page: 1,
      limit: 12,
      showViewMore: false,
      loadingCards: false,
      firstTimeLoad: true
    };
  }

  componentDidMount() {
    this.loadCourseCards(true);
  }

  loadCourseCards = firstTimeLoad => {
    let payload = {
      course_type: this.props.courseType,
      status: this.props.courseStatus,
      per_page: this.state.limit
    };
    let page = this.state.page;
    this.setState({ loadingCards: true, firstTimeLoad });
    getUserSabaCourses(this.props.currentUser.id, page, payload)
      .then(result => {
        let courses = [...this.state.courses, ...result.data];
        let showViewMore =
          this.state.limit * this.state.page <= courses.length && result.data.length;

        this.setState({
          aggregations: result.aggregations,
          totalCourses: result.total_items,
          courses,
          showViewMore,
          firstTimeLoad: false,
          page: ++this.state.page,
          loadingCards: false
        });
      })
      .catch(err => {
        console.error(`Error in CourseCardModal.getUserSabaCourses.func : ${err}`);
      });
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    return (
      <div className="course-card-modal">
        <div className="row">
          <TextField name="courscardmodal" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            <div style={{ color: 'white' }}>{tr(this.props.courseStatus + ' Courses')}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          {this.state.firstTimeLoad ? (
            <div className="text-center loader">
              <CircularProgress
                size={80}
                color={'#ffffff'}
                thickness={5}
                className="course-loader"
              />
              <div>Loading...</div>
            </div>
          ) : (
            <div className="small-12 columns">
              {this.state.courses.map((course, i) => {
                return <CourseCard key={i} courseType={course.source_type_name} course={course} />;
              })}
            </div>
          )}
        </div>
        {this.state.showViewMore && !this.state.firstTimeLoad && (
          <div>
            <div className="text-center">
              <button
                className="view-more-v2"
                onClick={() => {
                  this.loadCourseCards(false);
                }}
                disabled={this.state.loadingCards}
              >
                {tr(this.state.loadingCards ? 'Loading...' : 'View More')}
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

CourseCardModal.propTypes = {
  currentUser: PropTypes.object,
  courseType: PropTypes.string,
  courseStatus: PropTypes.string
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(CourseCardModal);
