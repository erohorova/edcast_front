import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import without from 'lodash/without';
import orderBy from 'lodash/orderBy';
import uniqBy from 'lodash/uniqBy';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import ActionInfo from 'react-material-icons/icons/action/info';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import colors from 'edc-web-sdk/components/colors/index';
import Paper from 'edc-web-sdk/components/Paper';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton.jsx';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { follow, fetchChannels } from 'edc-web-sdk/requests/channels.v2';
import { postToChannel, fetchCard } from 'edc-web-sdk/requests/cards';
import BiDirectionalArrow from 'edc-web-sdk/components/icons/BiDirectionalArrow';
import { updateCurrentCard } from '../../actions/cardsActions';
import { close, openStatusModal } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import Spinner from '../common/spinner';
import getFormattedDateTime from '../../utils/getFormattedDateTime';

class PostToChannelModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      header: {
        color: '#ffffff',
        width: '100%',
        position: 'absolute',
        top: '-30px',
        left: 0
      },
      close: {
        cursor: 'pointer',
        float: 'right',
        top: 0
      },
      footerButtons: {
        marginTop: '10px'
      },
      label: {
        color: '#454560',
        fontSize: '14px'
      },
      firstColumn: {
        textAlign: 'left'
      },
      table: {
        width: '100%',
        textAlign: 'center'
      },
      tableScroll: {
        maxHeight: '500px',
        overflowY: 'auto'
      },
      checkbox: {
        color: colors.primary
      },
      tdBorder: {
        borderLeft: '2px solid rgba(0,0,0,.06)'
      },
      arrowIcon: {
        width: 18,
        height: 18,
        color: '#555555',
        marginLeft: 5,
        verticalAlign: 'middle'
      },
      privateChannelStyle: {
        color: '#999aad',
        fontSize: '12px',
        paddingTop: '8px',
        paddingBottom: '8px'
      },
      privateChannelIconStyle: {
        top: '2px',
        width: '14px',
        height: '14px',
        verticalAlign: 'middle',
        display: 'inline-block'
      }
    };
    this.state = {
      channels: [],
      selectedChannels: [],
      availableChannels: [],
      alreadyPostedChannels: [],
      channelsToUnpost: [],
      pending: true,
      buttonPending: false,
      isAll: false,
      pendingAdd: false,
      sortAndOrder: {
        label: 'asc',
        creator: '',
        updatedAt: ''
      },
      showPrivateChannelMsg: false,
      sortOptionKey: 'label',
      orderOptionKey: 'asc',
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
  }

  componentDidMount() {
    fetchCard(this.props.card && this.props.card.id)
      .then(card => {
        let alreadyPostedChannels = !!card.channelIds ? card.channelIds : [];
        if (!!card.nonCuratedChannelIds) {
          alreadyPostedChannels = alreadyPostedChannels.concat(card.nonCuratedChannelIds);
          this.setState({ alreadyPostedChannels });
        }

        let commonPayload = {
          limit: 21,
          fields: 'id,label,creator,updated_at,is_private',
          sort: this.state.sortOptionKey,
          order_label: this.state.orderOptionKey
        };
        let writableChannelPayload = {
          ...commonPayload,
          writables: true
        };
        let channelPayload = {
          ...commonPayload
        };

        fetchChannels(writableChannelPayload)
          .then(writableChannels => {
            if (writableChannels && !!writableChannels.length) {
              this.setState({
                channels: writableChannels,
                pending: false,
                isAll: writableChannels.length < 21
              });
            } else {
              fetchChannels(channelPayload)
                .then(channels => {
                  this.setState({
                    availableChannels: channels,
                    pending: false,
                    isAll: channels && channels.length < 21
                  });
                })
                .catch(err => {
                  console.error(`Error in ChannelPostModalNew.fetchChannels.func : ${err}`);
                });
            }
          })
          .catch(err => {
            console.error(`Error in ChannelPostModalNew.fetchChannels.func writables : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in ChannelPostModalNew.fetchCard.func writables : ${err}`);
      });
  }

  closeModalHandler = () => {
    this.props.dispatch(close());
  };

  selectChannel = id => {
    let indexPos = this.state.selectedChannels.indexOf(id);
    let alreadyPostedIndex = this.state.alreadyPostedChannels.indexOf(id);
    let selectedChannels = this.state.selectedChannels;
    let channelsToUnpost = this.state.channelsToUnpost;
    if (alreadyPostedIndex > -1) {
      let channelToUnpostIndex = this.state.channelsToUnpost.indexOf(id);
      if (~channelToUnpostIndex) {
        channelsToUnpost.splice(channelToUnpostIndex, 1);
      } else {
        channelsToUnpost.push(id);
      }
    } else if (indexPos > -1) {
      selectedChannels.splice(indexPos, 1);
    } else {
      selectedChannels = selectedChannels.concat(id);
    }
    this.setState({ selectedChannels, channelsToUnpost }, () => {
      this.setMessage();
    });
  };

  setMessage = () => {
    if (this.props.card.isPublic) {
      this.setState({ showPrivateChannelMsg: false });
      let selectedPrivateChannels = false;
      this.state.selectedChannels.map(id => {
        if (this.state.channels.find(e => e.id == id).isPrivate) {
          this.setState({ showPrivateChannelMsg: true });
        }
      });
    }
  };

  updateCardChannels = () => {
    postToChannel(this.props.card.id, {
      card: {
        channel_ids: this.state.selectedChannels,
        remove_channel_ids: this.state.channelsToUnpost
      }
    })
      .then(channel => {
        this.setState({ buttonPending: false }, () => {
          if (this.props.updateChannelCards)
            this.props.updateChannelCards(
              this.props.card.id,
              without(this.state.alreadyPostedChannels, ...this.state.channelsToUnpost).concat(
                this.state.selectedChannels
              )
            );
          this.closeModalHandler();
          this.props.dispatch(updateCurrentCard(channel));
          let notShowCuratedMessage =
            this.state.selectedChannels &&
            this.state.selectedChannels.length &&
            channel.notRequiredCuration &&
            channel.notRequiredCuration.length &&
            channel.notRequiredCuration.length === this.state.selectedChannels.length;
          setTimeout(() => {
            if (this.state.newModalAndToast) {
              this.props.dispatch(
                openSnackBar(
                  `Content has been posted to the selected channel(s).${
                    notShowCuratedMessage
                      ? ''
                      : ' If it is a curated channel, your post will be visible once approved by the curator.'
                  }`
                )
              );
            } else {
              this.props.dispatch(
                openStatusModal(
                  `Content has been posted to the selected channel(s).${
                    notShowCuratedMessage
                      ? ''
                      : ' If it is a curated channel, your post will be visible once approved by the curator.'
                  }`
                )
              );
            }
          }, 1500);
        });
      })
      .catch(err => {
        console.error(`Error in PostToChannelModal.postToChannel.func : ${err}`);
      });
  };

  postClickHandler = () => {
    this.setState({ buttonPending: true });
    if (!this.state.channels.length) {
      Promise.all(
        this.state.selectedChannels.map(item => {
          return new Promise(resolve => {
            return follow(item)
              .then(data => {
                resolve();
              })
              .catch(err => {
                console.error(`Error in PostToChannelModal.follow.func: ${err}`);
              });
          });
        })
      )
        .then(() => {
          this.updateCardChannels();
        })
        .catch(err => {
          console.error(`Error in PostToChannelModal.Promise.all.func: ${err}`);
        });
    } else {
      this.updateCardChannels();
    }
  };

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.checkScroll(node));
    }
  };

  checkScroll = node => {
    let obj = ReactDOM.findDOMNode(node);
    let isBottom =
      obj.scrollHeight - Math.ceil(obj.scrollTop) === obj.offsetHeight ||
      obj.scrollHeight - Math.floor(obj.scrollTop) === obj.offsetHeight;
    if (!this.state.pending && isBottom) {
      this.fetchMore();
    }
  };

  fetchMore() {
    if (!this.state.isAll && !this.state.pendingAdd) {
      this.setState({ pendingAdd: true });
      let sortOption =
        this.state.sortOptionKey === 'updated_at' ? 'updatedAt' : this.state.sortOptionKey;

      let commonPayload = {
        limit: 10,
        fields: 'id,label,creator,updated_at,is_private',
        sort: this.state.sortOptionKey,
        order_label: this.state.orderOptionKey
      };
      let channelPayload = {
        ...commonPayload,
        offset: this.state.availableChannels.length
      };
      let writableChannelPayload = {
        ...commonPayload,
        offset: this.state.channels.length,
        writables: true
      };

      if (this.state.availableChannels && !!this.state.availableChannels.length) {
        fetchChannels(channelPayload)
          .then(channels => {
            let concatAvailableChannels = this.state.availableChannels.concat(channels);
            let channelsArray = orderBy(
              concatAvailableChannels,
              [
                channel =>
                  sortOption == 'creator'
                    ? channel.creator != null
                      ? channel.creator.name.toLowerCase()
                      : ''
                    : channel[sortOption].toLowerCase()
              ],
              [this.state.orderOptionKey]
            );
            let sortAndOrder = { ...this.state.sortAndOrder };
            sortAndOrder.creator = '';
            sortAndOrder.updatedAt = '';
            sortAndOrder.label = this.state.orderOptionKey;
            this.setState({
              availableChannels: uniqBy(channelsArray, 'id'),
              pendingAdd: false,
              isAll: channels && channels.length < 10,
              sortAndOrder
            });
          })
          .catch(err => {
            console.error(`Error in PostToChannelModal.fetchMore.fetchChannels.func: ${err}`);
          });
      } else {
        fetchChannels(writableChannelPayload)
          .then(channels => {
            let concatChannels = this.state.channels.concat(channels);
            let channelsArray = orderBy(
              concatChannels,
              [
                channel =>
                  sortOption == 'creator'
                    ? channel.creator != null
                      ? channel.creator.name.toLowerCase()
                      : ''
                    : channel[sortOption].toLowerCase()
              ],
              [this.state.orderOptionKey]
            );
            let sortAndOrder = { ...this.state.sortAndOrder };
            sortAndOrder.creator = '';
            sortAndOrder.updatedAt = '';
            sortAndOrder.label = this.state.orderOptionKey;
            this.setState({
              channels: uniqBy(channelsArray, 'id'),
              pendingAdd: false,
              isAll: channels.length < 10,
              sortAndOrder
            });
          })
          .catch(err => {
            console.error(
              `Error in PostToChannelModal.fetchMore.fetchChannels.func: writable ${err}`
            );
          });
      }
    }
  }

  tableSortBy(sortValue, orderValue) {
    let sortAndOrder = { ...this.state.sortAndOrder };
    sortAndOrder[sortValue] = orderValue;
    if (sortValue == 'label') {
      sortAndOrder.creator = '';
      sortAndOrder.updatedAt = '';
    } else if (sortValue == 'creator') {
      sortAndOrder.label = '';
      sortAndOrder.updatedAt = '';
    } else {
      sortAndOrder.creator = '';
      sortAndOrder.label = '';
    }
    let sortOption = sortValue === 'updatedAt' ? 'updated_at' : sortValue;
    let channels = [];
    let channelPayload = {
      limit: 21,
      fields: 'id,label,creator,updated_at,is_private',
      sort: sortOption,
      order_label: orderValue
    };
    let writableChannelPayload = {
      ...channelPayload,
      writables: true
    };
    fetchChannels(writableChannelPayload)
      .then(writableChannels => {
        if (writableChannels && !!writableChannels.length) {
          this.setState({
            channels: writableChannels,
            pending: false,
            isAll: writableChannels.length < 21,
            sortOptionKey: sortOption,
            orderOptionKey: orderValue,
            sortAndOrder: sortAndOrder
          });
        } else {
          fetchChannels(channelPayload)
            .then(availableChannels => {
              this.setState({
                availableChannels: availableChannels,
                pending: false,
                isAll: availableChannels && availableChannels.length < 21,
                sortOptionKey: sortOption,
                orderOptionKey: orderValue,
                sortAndOrder: sortAndOrder
              });
            })
            .catch(err => {
              console.error(`Error in ChannelPostModalNew.fetchChannels.func : ${err}`);
            });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelPostModalNew.fetchChannels.func writables : ${err}`);
      });
  }

  tableRender = type => {
    const label = this.state.sortAndOrder.label;
    const creator = this.state.sortAndOrder.creator;
    const updatedAt = this.state.sortAndOrder.updatedAt;

    return (
      <div>
        <TextField name="posttochannel" autoFocus={true} className="hiddenTextField" />
        <table style={this.styles.table}>
          <thead>
            <tr>
              <th
                onClick={() =>
                  this.tableSortBy('label', !label || label == 'desc' ? 'asc' : 'desc')
                }
                style={this.styles.firstColumn}
                className="pointer"
              >
                {tr('Channel Title')}
                {label == '' && (
                  <BiDirectionalArrow label={''} color={'#555555'} style={this.styles.arrowIcon} />
                )}
                {label == 'asc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'desc'}
                    style={this.styles.arrowIcon}
                  />
                )}
                {label == 'desc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'asc'}
                    style={this.styles.arrowIcon}
                  />
                )}
              </th>
              <th
                onClick={() =>
                  this.tableSortBy('creator', !creator || creator == 'desc' ? 'asc' : 'desc')
                }
                className="pointer"
              >
                {tr('Creator')}
                {creator == '' && (
                  <BiDirectionalArrow label={''} color={'#555555'} style={this.styles.arrowIcon} />
                )}
                {creator == 'asc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'desc'}
                    style={this.styles.arrowIcon}
                  />
                )}
                {creator == 'desc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'asc'}
                    style={this.styles.arrowIcon}
                  />
                )}
              </th>
              <th
                onClick={() =>
                  this.tableSortBy('updatedAt', !updatedAt || updatedAt == 'desc' ? 'asc' : 'desc')
                }
                className="pointer"
              >
                {tr('Modified')}
                {updatedAt == '' && (
                  <BiDirectionalArrow label={''} color={'#555555'} style={this.styles.arrowIcon} />
                )}
                {updatedAt == 'asc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'desc'}
                    style={this.styles.arrowIcon}
                  />
                )}
                {updatedAt == 'desc' && (
                  <BiDirectionalArrow
                    color={'#C8C8C8'}
                    label={'asc'}
                    style={this.styles.arrowIcon}
                  />
                )}
              </th>
            </tr>
          </thead>
          <tbody>
            {!this.state.pending &&
              this.state[type].map((channel, index) => {
                let alreadyPosted = ~this.state.alreadyPostedChannels.indexOf(channel.id);
                let isChannelToUnpost = ~this.state.channelsToUnpost.indexOf(channel.id);
                let isChecked = !!(
                  (alreadyPosted && !isChannelToUnpost) ||
                  ~this.state.selectedChannels.indexOf(channel.id)
                );
                return (
                  <tr key={channel.id}>
                    <td style={this.styles.firstColumn}>
                      <Checkbox
                        label={
                          <span className="channel-name">
                            {channel.label.length < 53
                              ? channel.label
                              : `${channel.label.slice(0, 52)}...`}
                          </span>
                        }
                        aria-label={
                          <span className="channel-name">
                            {channel.label.length < 53
                              ? channel.label
                              : `${channel.label.slice(0, 52)}...`}
                          </span>
                        }
                        onCheck={() => {
                          this.selectChannel(channel.id);
                        }}
                        className="checkbox"
                        style={this.styles.checkbox}
                        checked={isChecked}
                      />
                    </td>
                    <td style={this.styles.tdBorder}>
                      {channel.creator && !channel.creator.isSuspended && (
                        <a
                          className="data-ava matte"
                          onClick={() => {
                            this.props.dispatch(push(`/${channel.creator.handle}`));
                          }}
                          target="_blank"
                        >
                          {channel.creator.name.length < 19
                            ? channel.creator.name
                            : `${channel.creator.name.slice(0, 18)}...`}
                        </a>
                      )}
                    </td>
                    <td style={this.styles.tdBorder}>
                      <div className="matte">
                        {getFormattedDateTime(channel.updatedAt, 'DD/MM/YYYY')}
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    );
  };

  render() {
    return (
      <div>
        <div style={this.styles.header}>
          {tr('Post to Channel(s)')}
          <span style={this.styles.close} className="close-icon-btn">
            <NavigationClose color={colors.white} onTouchTap={this.closeModalHandler} />
          </span>
        </div>
        <Paper>
          <div className="container">
            <div className="container-padding">
              <div style={this.styles.tableScroll} ref={this.mountScroll}>
                {this.state.showPrivateChannelMsg && (
                  <div>
                    <div style={this.styles.label}>
                      <div style={{ margin: '0 0 10px 0' }}>
                        <ActionInfo style={this.styles.privateChannelIconStyle} />
                        <p
                          style={{
                            verticalAlign: 'middle',
                            display: 'inline-block',
                            marginLeft: '5px'
                          }}
                        >
                          {tr('Posting to private channel will not make this card private.')}
                        </p>
                      </div>
                    </div>
                  </div>
                )}
                {!!this.state.channels.length &&
                  !this.state.pending &&
                  this.tableRender('channels')}
                {!this.state.channels.length &&
                  !this.state.pending &&
                  !!this.state.availableChannels.length && (
                    <div>
                      <div className="message-empty-channel text-center">
                        <div>
                          {tr('Please select a channel below to follow and post the content')}
                        </div>
                      </div>
                      {this.tableRender('availableChannels')}
                    </div>
                  )}
                {!this.state.channels.length &&
                  !this.state.pending &&
                  !this.state.availableChannels.length && (
                    <div className="message-empty-channel text-center">
                      <div className="data-not-available-msg">
                        {tr("Sorry, you don't have any available channels")}
                      </div>
                    </div>
                  )}
                {this.state.pendingAdd && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
              </div>
              {this.state.pending && (
                <div className="text-center">
                  <Spinner />
                </div>
              )}
              {!this.state.pending && (
                <div className="text-center">
                  <SecondaryButton
                    label={tr('Cancel')}
                    className="close"
                    onTouchTap={this.closeModalHandler}
                  />
                  {(!!this.state.channels.length || !!this.state.availableChannels.length) && (
                    <PrimaryButton
                      label={tr('Save')}
                      className="create"
                      onTouchTap={this.postClickHandler}
                      pending={this.state.buttonPending}
                      pendingLabel={tr('Saving...')}
                      disabled={
                        !(this.state.selectedChannels.length || this.state.channelsToUnpost.length)
                      }
                    />
                  )}
                </div>
              )}
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

PostToChannelModal.propTypes = {
  card: PropTypes.object,
  updateChannelCards: PropTypes.any
};

export default connect(state => ({
  cards: state.cards.toJS()
}))(PostToChannelModal);
