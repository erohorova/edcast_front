import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';

import isUrlValid from 'validator/lib/isURL';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import colors from 'edc-web-sdk/components/colors/index';
import { postSkill, updateSkill } from 'edc-web-sdk/requests/users';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import { getUserSkills } from '../../actions/usersActions';
import { close } from '../../actions/modalActions';
import { filestackClient } from '../../utils/filestack';
import { parseUrl } from '../../utils/urlParser';
import * as upshotActions from '../../actions/upshotActions';
import pdfPreviewUrl from '../../utils/previewPdf';
import moment from 'moment';
import { Calendar } from 'react-date-range';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';

class SkillModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      providerBtnImage: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem',
        margin: '0 4px'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      cancel: {
        color: darkPurp,
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0',
        maxWidth: '100%'
      },
      chip: {
        margin: 0,
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '0 37px 0 8px',
        width: '100%'
      },
      chipClose: {
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        borderRadius: '50%',
        color: colors.primary,
        verticalAlign: 'middle',
        marginLeft: '1rem',
        marginBottom: '2px',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        position: 'absolute',
        right: '8px',
        top: '5px'
      },
      checkLabel: {
        fontSize: '12px',
        fontWeight: 300,
        color: darkPurp
      },
      mainImage: {
        height: '32px',
        width: '32px',
        marginTop: '74px',
        fill: darkPurp
      },
      imageBtn: {
        paddingBottom: '0',
        height: 'auto'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      pathwayImage: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%'
      },
      pathwayImageWithoutCursor: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%',
        cursor: 'default'
      },
      labelChip: {
        paddingRight: '8px'
      },
      tileView: {
        border: 0,
        padding: 0,
        marginRight: 10,
        width: 14,
        height: 14
      },
      popover: {
        position: 'relative',
        zIndex: '3',
        width: '529px',
        padding: '16px',
        overflow: 'auto',
        margin: '40px 0 20px -170px'
      },
      activeView: {
        opacity: 1
      },
      inactiveView: {
        opacity: 0.5
      },
      actionBtnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      pathwayBanner: {
        background: 'url("/i/images/folder.png") 50% 50% no-repeat'
      },
      FlatButton: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      flexBlock: {
        display: 'flex'
      },
      checkbox: {
        color: colors.primary,
        width: 'auto'
      },
      labelCheckbox: {
        cursor: 'pointer',
        fontSize: '12px',
        fontWeight: 300,
        textAlign: 'left',
        color: '#6f708b',
        marginLeft: '-8px',
        width: 'auto',
        lineHeight: '24px'
      },
      labelEditBlock: {
        display: 'inline-flex',
        alignItems: 'flex-end'
      },
      labelBlock: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      labelEdit: {
        cursor: 'pointer',
        height: '15px',
        fontSize: '12px',
        position: 'relative',
        top: '3px'
      },
      labelIcon: {
        cursor: 'pointer',
        height: '18px',
        marginLeft: '-2px'
      },
      badgesIconBlock: {
        display: 'flex',
        marginBottom: '20px'
      },
      badgeIcon: {
        marginTop: '15px',
        display: 'block',
        marginRight: '20px',
        width: '50px',
        height: '50px'
      },
      providerIcon: {
        marginTop: '15px',
        display: 'block',
        height: '50px'
      },
      badgeText: {
        maxWidth: '385px'
      },
      currentBadge: {
        fontSize: '14px',
        color: '#6f708b'
      },
      badgeLabel: {
        textAlign: 'left',
        width: '100%',
        marginTop: '5px'
      },
      badgeLabelCenter: {
        textAlign: 'center',
        width: '50px',
        marginTop: '5px'
      },
      selectedIcon: {
        border: '5px solid #adaec2',
        borderRadius: '30px',
        width: '60px',
        height: '60px'
      },
      icons: {
        width: '14px',
        height: '14px'
      },
      confirmationContent: {
        transform: 'none'
      },
      iconRadio: {
        marginRight: '5px',
        fill: '#d6d6e1'
      },
      mr5: {
        marginRight: '5px'
      },
      radioBtn: {
        width: 'auto'
      },
      labelTop: {
        alignSelf: 'flex-start'
      },
      badgeBlock: {
        display: 'flex'
      },
      mainInputStyle: {
        fontSize: '14px',
        lineHeight: '14px',
        height: 'auto'
      },
      inputs: {
        border: '1px solid #d6d6e1',
        padding: '5px 10px',
        fontSize: '14px',
        lineHeight: '14px',
        color: '#999aad',
        borderRadius: '2px'
      },
      hint: {
        padding: '0 10px',
        fontSize: '12px',
        lineHeight: '31px',
        textAlign: 'left',
        color: '#acadc1',
        top: 0,
        bottom: 0,
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
      },
      biaRadio: {
        marginRight: '0.5rem',
        fill: '#d6d6e1'
      },
      inactiveBiaRadio: {
        marginRight: '0.5rem',
        fill: '#999aad'
      }
    };

    this.state = {
      fileStack: [],
      submitting: false,
      freeChars: 150,
      experienceYears: 'no',
      experienceMonths: 'no',
      experienceTotal: 'no experience',
      selectedBia: '',
      uploadPDF: false,
      enableBIA: this.props.team.config.enabled_bia,
      alreadyExist: false,
      errorMessage: '',
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      hideUploadCredential: window.ldclient.variation('hide-upload-credential', false),
      expiryDateHint: tr('Enter expiry date')
    };

    this.expYears = [
      { id: 'no', text: 'Select Year', disabled: true, pluralAdd: '' },
      { id: '0', text: 'less than a year', disabled: false, pluralAdd: '' },
      { id: '1', text: ' year', disabled: false, pluralAdd: '' },
      { id: '2', text: ' years', disabled: false, pluralAdd: 's' },
      { id: '3', text: ' years', disabled: false, pluralAdd: 's' },
      { id: '4', text: ' years', disabled: false, pluralAdd: 's' },
      { id: '5', text: ' years', disabled: false, pluralAdd: 's' },
      { id: '6', text: 'over 5 years', disabled: false, pluralAdd: 's' }
    ];

    this.expMonths = [
      { id: 'no', text: 'Select Month', disabled: true, pluralAdd: '' },
      { id: '0', text: 'less than a month', disabled: false, pluralAdd: '' },
      { id: '1', text: ' month', disabled: false, pluralAdd: '' },
      { id: '2', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '3', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '4', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '5', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '6', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '7', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '8', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '9', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '10', text: ' months', disabled: false, pluralAdd: 's' },
      { id: '11', text: ' months', disabled: false, pluralAdd: 's' }
    ];

    this.updateFileStackFile = this.updateFileStackFile.bind(this);
    let config = this.props.team.OrgConfig;
    this.newProfileLD = window.ldclient.variation('new-profile', false);
  }

  componentDidMount() {
    if (this.props.skill && this.props.skill.credential && this.props.skill.credential.length > 0) {
      this.setState({
        uploadPDF: this.props.skill.credential[0].mimetype !== 'image/jpeg',
        fileStack: this.props.skill.credential,
        showMainImage: true
      });
    }

    if (this.props.skill && this.props.skill.skillLevel) {
      this.setState({
        selectedBia: this.props.skill.skillLevel
      });
    }

    if (this.props.skill && this.props.skill.experience) {
      this.setExperience(this.props.skill.experience);
    }
  }

  setExperience(experience) {
    switch (experience) {
      case 'less than a year':
        this.setYearValue('0');
        break;
      case 'over 5 years':
        this.setYearValue('6');
        break;
      case 'less than a month':
        this.setMonthValue('0');
        break;
      default:
        let experienceArray = experience.split(' ');
        if (experienceArray.length == 2) {
          if (
            this.expYears.find(
              item => item.id === experienceArray[0] && item.text === ' ' + experienceArray[1]
            )
          ) {
            this.setYearValue(experienceArray[0]);
          } else if (
            this.expMonths.find(
              item => item.id === experienceArray[0] && item.text === ' ' + experienceArray[1]
            )
          ) {
            this.setMonthValue(experienceArray[0]);
          }
        } else if (experienceArray.length == 4) {
          if (
            this.expYears.find(
              item => item.id === experienceArray[0] && item.text === ' ' + experienceArray[1]
            ) &&
            this.expMonths.find(
              item => item.id === experienceArray[2] && item.text === ' ' + experienceArray[3]
            )
          ) {
            this.setYearValue(experienceArray[0]);
            setTimeout(() => {
              this.setMonthValue(experienceArray[2]);
            }, 300);
          }
        }
    }
  }

  handleDescriptionChange(event) {
    let input = event.target.value;
    this.setState({
      freeChars: 150 - input.length
    });
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  changeTag = event => {
    this.setState({
      maxTagErrorText:
        event.target.value.length <= 150
          ? ''
          : 'Max tag length exceeded. It must be shorter than 150 characters.'
    });
  };

  addTagHandler = event => {
    if (!~this.state.topics.indexOf(event.target.value.trim()) && !!event.target.value.trim()) {
      this.setState({ topics: this.state.topics.concat(event.target.value.trim()) });
    }
  };

  changeActiveType = activeType => {
    this.setState({ activeType });
  };

  pathwayMainChange = (type, event) => {
    let mainPathway = this.state.mainPathway;
    mainPathway[type] = event.target.value;
    this.setState({ mainPathway, titleErrorText: '' });
  };

  fileStackHandler = (isProvider, e) => {
    if (!this.state.hideUploadCredential) {
      e && e.preventDefault();
      let fromSources = [
        'local_file_system',
        'googledrive',
        'imagesearch',
        'dropbox',
        'box',
        'github',
        'gmail',
        'onedrive'
      ];
      if (location.protocol === 'https:') {
        fromSources.push('webcam');
      }

      uploadPolicyAndSignature()
        .then(data => {
          let policy = data.policy;
          let signature = data.signature;

          filestackClient(policy, signature)
            .pick({
              accept: ['image/*', '.pdf'],
              maxFiles: 1,
              storeTo: {
                location: 's3'
              },
              imageMax: [930, 505],
              imageMin: [200, 200],
              fromSources: fromSources
            })
            .then(fileStack => this.updateFileStackFile(fileStack, isProvider))
            .catch(err => {
              console.error(`Error in SkillModal.updateFileStackFile.func : ${err}`);
            });
        })
        .catch(err => {
          console.error(`Error in SkillModal.uploadPolicyAndSignature.func : ${err}`);
        });
    }
  };

  updateFileStackFile(fileStack, isProvider) {
    let extention = fileStack.filesUploaded[0].filename.split('.');
    extention = extention[extention.length - 1];
    this.setState({
      uploadPDF: extention === 'pdf'
    });

    let tempPath = fileStack.filesUploaded;
    let securedUrl;

    refreshFilestackUrl(tempPath[0].url)
      .then(resp => {
        securedUrl = resp.signed_url;
        if (isProvider) {
          this.setState({ fileStackProvider: tempPath[0].url, securedProviderUrl: securedUrl });
        } else {
          this.setState({
            fileStack: tempPath,
            securedUrl: securedUrl,
            showMainImage: tempPath.length
          });
        }
      })
      .catch(err => {
        console.error(`Error in SkillModal.updateFileStackFile.refreshFilestackUrl.func : ${err}`);
      });
  }

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton style={this.styles.imageBtn}>
          <ImagePhotoCamera color="white" />
        </IconButton>
        {!this.state.hideUploadCredential && <div className="roll-text">{tr('Change Image')}</div>}
      </span>
    );
  }

  experienceChangeHandle = (select, event) => {
    this.checkHideError('required');
    if (select === 'year') {
      this.setYearValue(event.target.value);
    } else {
      this.setMonthValue(event.target.value);
    }
  };

  changeBia = (event, value) => {
    this.setState({
      selectedBia: value
    });
  };

  setMonthValue(itemId) {
    let pluralYear = this.expYears[+this.state.experienceYears + 1]
      ? this.expYears[+this.state.experienceYears + 1].pluralAdd
      : '';
    let pluralMonth = this.expMonths[+itemId + 1].pluralAdd;
    let yearsToDisplay = this.state.experienceYears === '6' ? 'over 5' : this.state.experienceYears;
    let totalValue = '';

    let start = this.state.experienceYears === '0' || this.state.experienceYears === 'no';

    if (this.state.experienceYears === '6') {
      totalValue = this.expYears.find(item => item.id === this.state.experienceYears).text;
    } else if (itemId === '0' || itemId === 'no') {
      totalValue = start
        ? this.expMonths.find(item => item.id === itemId).text
        : `${this.state.experienceYears} year${pluralYear}`;
    } else {
      totalValue = start
        ? `${itemId} month${pluralMonth}`
        : `${yearsToDisplay} year${pluralYear} ${itemId} month${pluralMonth}`;
    }
    this.setState({
      experienceMonths: itemId,
      experienceTotal: totalValue
    });
  }

  setYearValue(itemId) {
    let pluralMonth = this.expMonths[+this.state.experienceMonths + 1]
      ? this.expMonths[+this.state.experienceMonths + 1].pluralAdd
      : '';
    let pluralYear = this.expYears[+itemId + 1].pluralAdd;
    let yearsToDisplay = itemId === '6' ? 'over 5' : itemId;
    let totalValue = '';

    let start = this.state.experienceMonths === 'no' || this.state.experienceMonths === '0';

    if (itemId === '0' || itemId === 'no' || itemId === '6') {
      totalValue =
        start || itemId === '6'
          ? this.expYears.find(item => item.id === itemId).text
          : `${this.state.experienceMonths} month${pluralMonth}`;
    } else {
      totalValue = start
        ? `${yearsToDisplay} year${pluralYear}`
        : `${yearsToDisplay} year${pluralYear} ${this.state.experienceMonths} month${pluralMonth}`;
    }
    this.setState({
      experienceYears: itemId,
      experienceTotal: totalValue
    });
  }

  createClickHandler = () => {
    let payload = {
      skill: {
        name: this._skillName.input.value,
        description: this._skillDescription.input.value,
        credential: this.state.fileStack,
        experience: this.state.experienceTotal,
        credential_name: this._credName.input.value,
        credential_url: this._credURL.input.value
      }
    };
    if (this.newProfileLD) {
      payload.skill.expiry_date = moment(
        moment(this._expiryDate.input.value).format('MM/DD/YYYY')
      ).format('YYYY/MM/DD HH:MM:SS');
    }

    if (this.state.enableBIA) {
      payload.skill['skill_level'] = this.state.selectedBia;
    }
    let skillCredURL = payload.skill.credential_url;
    let notValidUrl;

    skillCredURL ? (notValidUrl = !isUrlValid(skillCredURL)) : null;

    if (
      !payload.skill.name ||
      payload.skill.experience === 'no experience' ||
      (skillCredURL && notValidUrl)
    ) {
      let statePayload = {
        showError: true
      };
      !payload.skill.name || payload.skill.experience === 'no experience'
        ? (statePayload.noRequired = true)
        : null;
      notValidUrl ? (statePayload.notUrl = true) : null;
      this.setState(statePayload);
    } else {
      this.setState({
        submitting: true
      });

      if (!this.props.skill) {
        postSkill(payload)
          .then(data => {
            this.props.dispatch(getUserSkills(this.props.currentUser.id));
            if (this.state.upshotEnabled) {
              upshotActions.sendCustomEvent(window.UPSHOTEVENT['SKILL'], {
                name: this._skillName && this._skillName.input && this._skillName.input.value,
                description:
                  this._skillDescription &&
                  this._skillDescription.input &&
                  this._skillDescription.input.value,
                experience: this.state.experienceTotal,
                credential_name:
                  this._credName && this._credName.input && this._credName.input.value,
                credential_url: this._credURL && this._credURL.input && this._credURL.input.value,
                expiry_date:
                  this._expiryDate && this._expiryDate.input && this._expiryDate.input.value,
                event: 'Add Skill'
              });
            }
            this.closeModal();
          })
          .catch(err => {
            console.error(`Error in SkillModal.postSkill.func :`, err);
            if (err && err.message) {
              this.setState({
                errorMessage: err.message,
                alreadyExist: true,
                showError: true,
                submitting: false
              });
            }
          });
      } else {
        payload.skill.id = this.props.skill.id;
        updateSkill(payload)
          .then(data => {
            this.props.dispatch(getUserSkills(this.props.currentUser.id));
            this.closeModal();
          })
          .catch(err => {
            console.error(`Error in SkillModal.updateSkill.func : `, err);
            if (err && err.message) {
              this.setState({
                errorMessage: err.message,
                alreadyExist: true,
                showError: true,
                submitting: false
              });
            }
          });
      }
    }
  };

  checkHideError = errorType => {
    if (errorType === 'required') {
      this.state.noRequired ? this.setState({ noRequired: false }) : null;
    } else {
      this.state.notUrl ? this.setState({ notUrl: false }) : null;
    }
  };

  handleDateChange = event => {
    let expiryDate = event.format('MM/DD/YYYY');
    this._expiryDate.input.value = expiryDate;
    this.setState({ expiryDateHint: '' });
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let skill = this.props.skill || {};

    return (
      <div className="skill-creation">
        <div className="my-modal-header">
          <span className="header-title">
            {this.props.skill ? tr('Edit Skill') : tr('Add Skill')}
          </span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeBtn}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
            <TextField name="skillmodal" autoFocus={true} className="hiddenTextField" />
          </div>
        </div>
        <div className="pathway-content">
          <div className="common-block">
            <div className="row">
              <div className="small-12 medium-4">
                {!this.state.showMainImage && (
                  <a
                    href="#"
                    className="pathway-banner"
                    style={
                      !this.state.hideUploadCredential
                        ? this.styles.pathwayBanner
                        : Object.assign(this.styles.pathwayBanner, { cursor: 'default' })
                    }
                    onClick={e => this.fileStackHandler(false, e)}
                  >
                    <div className="text-center">
                      {!this.state.hideUploadCredential && (
                        <div>
                          <ImagePhotoCamera style={this.styles.mainImage} />
                          <div className="empty-image">{tr('Upload Credential')}</div>
                        </div>
                      )}
                    </div>
                  </a>
                )}
                {this.state.showMainImage && (
                  <div className="pathway-banner preview-upload">
                    {this.state.uploadPDF ? (
                      <div>
                        <iframe
                          className="filestack-preview-container"
                          height="202px"
                          width="100%"
                          src={pdfPreviewUrl(
                            this.state.securedUrl || this.state.fileStack[0].url,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          onMouseEnter={() => {
                            this.setState({ mainImage: true });
                          }}
                          onMouseLeave={() => {
                            this.setState({ mainImage: false });
                          }}
                        />
                        {!this.state.hideUploadCredential &&
                          this.hoverBlock(this.fileStackHandler.bind(this, false))}
                      </div>
                    ) : (
                      <div
                        className="card-img-container"
                        onMouseEnter={() => {
                          this.setState({ mainImage: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ mainImage: false });
                        }}
                      >
                        {this.state.hideUploadCredential && (
                          <div
                            className="card-img button-icon"
                            style={{
                              ...this.styles.pathwayImageWithoutCursor,
                              ...{
                                backgroundImage: `url(\'${this.state.securedUrl ||
                                  this.state.fileStack[0].url}\')`
                              }
                            }}
                          >
                            {!this.state.hideUploadCredential &&
                              this.hoverBlock(this.fileStackHandler.bind(this, false))}
                          </div>
                        )}
                        {!this.state.hideUploadCredential && (
                          <div
                            className="card-img button-icon"
                            style={{
                              ...this.styles.pathwayImage,
                              ...{
                                backgroundImage: `url(\'${this.state.securedUrl ||
                                  this.state.fileStack[0].url}\')`
                              }
                            }}
                          >
                            {this.hoverBlock(this.fileStackHandler.bind(this, false))}
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                )}
              </div>
              <div className="small-12 medium-8">
                <div className="input-block">
                  {this.state.showError && (
                    <div className="error-text">
                      {this.state.noRequired ? tr('Please fill all the required fields') : ''}
                      {this.state.notUrl && this.state.noRequired && <br />}
                      {this.state.notUrl ? tr('Credential URL is not valid') : ''}
                      {this.state.notUrl && this.state.alreadyExist && <br />}
                      {this.state.alreadyExist ? tr(this.state.errorMessage) : ''}
                    </div>
                  )}
                  <div className="row">
                    <div className="small-12 medium-4 large-3 align-label">
                      <div className="pathway-label">
                        {tr('Skill Name')}
                        <span>*</span>
                      </div>
                    </div>
                    <div className="small-12 medium-8 large-9">
                      <div className="text-block">
                        <TextField
                          hintText={tr('Enter skill name')}
                          aria-label={tr('Enter skill name')}
                          type="text"
                          style={this.styles.mainInputStyle}
                          hintStyle={this.styles.hint}
                          fullWidth={true}
                          inputStyle={this.styles.inputs}
                          className={'smartbite-input'}
                          ref={node => (this._skillName = node)}
                          underlineShow={false}
                          defaultValue={skill.name || ''}
                          onChange={this.checkHideError.bind(this, 'required')}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="input-block">
                  <div className="row">
                    <div className="small-12 medium-4 large-3 align-label">
                      <div className="pathway-label">{tr('Description')}</div>
                    </div>
                    <div className="small-12 medium-8 large-9">
                      <TextField
                        hintText={tr('Enter skill description')}
                        aria-label={tr('Enter skill description')}
                        type="text"
                        onChange={this.handleDescriptionChange.bind(this)}
                        style={this.styles.mainInputStyle}
                        hintStyle={this.styles.hint}
                        fullWidth={true}
                        inputStyle={this.styles.inputs}
                        className={'smartbite-input'}
                        ref={node => (this._skillDescription = node)}
                        underlineShow={false}
                        maxLength="150"
                        defaultValue={skill.description || ''}
                      />
                      <p className="char-counter">
                        <small>
                          {this.state.freeChars}/150 {tr('Characters Remaining')}
                        </small>
                      </p>
                    </div>
                  </div>
                </div>
                {this.state.enableBIA && (
                  <div className="input-block">
                    <div className="row">
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">{tr('Level')}</div>
                      </div>
                      <div className="small-12 medium-8 large-9">
                        <div className="bia-block">
                          <RadioButtonGroup
                            name="bia-radio"
                            style={{ display: 'flex' }}
                            valueSelected={this.state.selectedBia}
                            onChange={this.changeBia}
                          >
                            <RadioButton
                              value="beginner"
                              label={tr('Beginner')}
                              aria-label={tr('Beginner')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia !== 'beginner'
                                  ? this.styles.biaRadio
                                  : this.styles.inactiveBiaRadio
                              }
                            />
                            <RadioButton
                              value="intermediate"
                              aria-label={tr('intermediate')}
                              label={tr('Intermediate')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia !== 'intermediate'
                                  ? this.styles.biaRadio
                                  : this.styles.inactiveBiaRadio
                              }
                            />
                            <RadioButton
                              value="advanced"
                              aria-label={tr('advanced')}
                              label={tr('Advanced')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia !== 'advanced'
                                  ? this.styles.biaRadio
                                  : this.styles.inactiveBiaRadio
                              }
                            />
                          </RadioButtonGroup>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <div className="input-block">
                  <div className="row">
                    <div className="small-12 medium-4 large-3 align-label">
                      <div className="pathway-label">
                        {tr('Experience')}
                        <span>*</span>
                      </div>
                    </div>
                    <div className="small-12 medium-8 large-9">
                      <select
                        name="years"
                        value={this.state.experienceYears}
                        onChange={this.experienceChangeHandle.bind(this, 'year')}
                        className="input__select"
                      >
                        {this.expYears.map(item =>
                          item.id === 'no' || item.id === '0' || item.id === '6' ? (
                            <option key={item.id} value={item.id} disabled={item.disabled}>
                              {tr(item.text)}
                            </option>
                          ) : (
                            <option key={item.id} value={item.id} disabled={item.disabled}>{`${
                              item.id
                            } ${tr(item.text)}`}</option>
                          )
                        )}
                      </select>
                      <select
                        name="months"
                        value={this.state.experienceMonths}
                        onChange={this.experienceChangeHandle.bind(this, 'month')}
                        className="input__select"
                      >
                        {this.expMonths.map(item =>
                          item.id === 'no' || item.id === '0' ? (
                            <option key={item.id} value={item.id} disabled={item.disabled}>
                              {tr(item.text)}
                            </option>
                          ) : (
                            <option key={item.id} value={item.id} disabled={item.disabled}>{`${
                              item.id
                            } ${tr(item.text)}`}</option>
                          )
                        )}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="input-block">
                  <div className="row">
                    <div className="small-12 medium-4 large-3 align-label">
                      <div className="pathway-label">{tr('Credential Name')}</div>
                    </div>
                    <div className="small-12 medium-8 large-9">
                      <TextField
                        hintText={tr('Enter the Credential Name')}
                        aria-label={tr('Enter the Credential Name')}
                        type="text"
                        style={this.styles.mainInputStyle}
                        hintStyle={this.styles.hint}
                        fullWidth={true}
                        inputStyle={this.styles.inputs}
                        className={'smartbite-input'}
                        ref={node => (this._credName = node)}
                        underlineShow={false}
                        defaultValue={skill.credentialName || ''}
                      />
                    </div>
                  </div>
                </div>
                <div className="input-block">
                  <div className="row">
                    <div className="small-12 medium-4 large-3 align-label">
                      <div className="pathway-label">{tr('Credential URL')}</div>
                    </div>
                    <div className="small-12 medium-8 large-9">
                      <TextField
                        hintText={tr('Enter the Credential URL')}
                        aria-label={tr('Enter the Credential URL')}
                        type="text"
                        style={this.styles.mainInputStyle}
                        hintStyle={this.styles.hint}
                        fullWidth={true}
                        inputStyle={this.styles.inputs}
                        className={'smartbite-input'}
                        ref={node => (this._credURL = node)}
                        underlineShow={false}
                        defaultValue={skill.credentialUrl || ''}
                        onChange={this.checkHideError.bind(this, 'url')}
                      />
                    </div>
                  </div>
                </div>
                {this.newProfileLD && (
                  <div className="input-block">
                    <div className="row">
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">{tr('Expiry Date')}</div>
                      </div>
                      <div className="small-12 medium-8 large-9">
                        <TextField
                          hintText={this.state.expiryDateHint}
                          aria-label={tr('Enter expiry date')}
                          type="text"
                          style={this.styles.mainInputStyle}
                          hintStyle={this.styles.hint}
                          fullWidth={true}
                          inputStyle={this.styles.inputs}
                          className={'smartbite-input'}
                          ref={node => (this._expiryDate = node)}
                          underlineShow={false}
                          defaultValue={
                            skill.expiryDate ? moment(skill.expiryDate).format('MM/DD/YYYY') : ''
                          }
                          onChange={e => {
                            if (e.target.value.length == 0) {
                              this.setState({ expiryDateHint: tr('Enter expiry date') });
                            }
                          }}
                          onClick={() => {
                            this.setState({ calendarOpen: !this.state.calendarOpen });
                          }}
                        />
                      </div>
                      <div className="small-12 medium-4 large-3" />
                      {this.state.calendarOpen && (
                        <div className="small-12 medium-8 large-9">
                          <Calendar
                            container="inline"
                            minDate={moment()}
                            date={skill.expiryDate ? moment(skill.expiryDate) : ''}
                            onChange={this.handleDateChange}
                          />
                          <div className="text-center">
                            <button
                              className="my-button"
                              onClick={() => {
                                this.setState({ calendarOpen: false });
                              }}
                            >
                              {tr('Cancel')}
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>

          <div className="action-buttons">
            <FlatButton
              label={tr('Cancel')}
              className="close"
              disabled={this.state.clicked}
              style={this.styles.cancel}
              labelStyle={this.styles.actionBtnLabel}
              onTouchTap={this.closeModal}
            />
            <FlatButton
              label={this.state.submitting ? tr('Submitting') : tr('Submit')}
              className="publish"
              labelStyle={this.styles.actionBtnLabel}
              style={this.styles.create}
              onTouchTap={this.createClickHandler}
              disabled={this.state.submitting}
            />
          </div>
        </div>
      </div>
    );
  }
}

SkillModal.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  skill: PropTypes.object,
  pathname: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(SkillModal);
