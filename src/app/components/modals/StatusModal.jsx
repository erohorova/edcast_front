import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { close } from '../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class StatusModal extends Component {
  constructor(props, context) {
    super(props, context);
  }
  closeModal = () => {
    this.props.dispatch(close());
    this.props.handleCloseModal && this.props.handleCloseModal();
  };

  render() {
    //this.props.statusBlock = [firstMessagePart, link, linkName, secondMessagePart]
    return (
      <div className="StatusModal">
        {!!this.props.statusBlock && this.props.statusBlock.length ? (
          <div>
            {tr(this.props.statusBlock[0])}{' '}
            {!!this.props.statusBlock[1] && (
              <a href={this.props.statusBlock[1]}>{tr(this.props.statusBlock[2])}</a>
            )}{' '}
            {!!this.props.statusBlock[3] && tr(this.props.statusBlock[3])}
          </div>
        ) : (
          <p dangerouslySetInnerHTML={{ __html: tr(this.props.statusMessage) }} />
        )}
        <button tabIndex={0} className="confirmStatus" onClick={this.closeModal}>
          {tr('OK')}
        </button>
        <button className="closeStatusModal" onClick={this.closeModal}>
          ✕
        </button>
      </div>
    );
  }
}

StatusModal.propTypes = {
  statusMessage: PropTypes.string,
  statusBlock: PropTypes.array,
  handleCloseModal: PropTypes.func,
  show: PropTypes.bool.isRequired
};

export default connect()(StatusModal);
