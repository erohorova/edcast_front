import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CardOverviewModal from './CardOverviewModal';

class JourneyCardOverviewModal extends Component {
  constructor(props, context) {
    super(props, context);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.card !== nextProps.card;
  }

  render() {
    return (
      <div>
        <CardOverviewModal
          autoComplete={this.props.autoComplete}
          openChannelModal={this.props.openChannelModal}
          showComment={this.props.showComment}
          pathwayPreview={true}
          shouldComplete={true}
          card={this.props.card}
          dueAt={this.props.dueAt}
          startDate={this.props.startDate}
          cardUpdated={this.props.cardUpdated}
          isLock={this.props.isLock}
          isPathwayOwner={this.props.isPathwayOwner}
          isShowLockedCardContent={this.props.isShowLockedCardContent}
          isPrivate={this.props.isPrivate}
          isCompleted={this.props.isCompleted}
        />
      </div>
    );
  }
}

JourneyCardOverviewModal.propTypes = {
  autoComplete: PropTypes.bool,
  showComment: PropTypes.bool,
  card: PropTypes.object,
  dueAt: PropTypes.string,
  startDate: PropTypes.string,
  isLock: PropTypes.bool,
  isPathwayOwner: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  isPrivate: PropTypes.bool,
  openChannelModal: PropTypes.func,
  cardUpdated: PropTypes.func,
  isCompleted: PropTypes.bool
};

export default JourneyCardOverviewModal;
