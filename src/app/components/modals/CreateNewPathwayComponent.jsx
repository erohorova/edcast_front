import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextField from 'material-ui/TextField';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton.jsx';
import ChannelPostComponent from '../common/ChannelPostComponent';
import { createPathway, addToPathway, publishPathway } from 'edc-web-sdk/requests/pathways';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';

//actions
import { close, openPathwayCreationModal } from '../../actions/modalActions';

class CreateNewPathwayComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      createPending: false,
      titleError: false,
      selectedChannels: []
    };
    this.createClickHandler = this.createClickHandler.bind(this);
    this.clearTitleError = this.clearTitleError.bind(this);
    this.getNewChannels = this.getNewChannels.bind(this);
  }

  createClickHandler() {
    let title = this._pathwayTitleInput.getValue();
    if (title) {
      this.setState({ createPending: true });
      createPathway(title, this.state.selectedChannels)
        .then(data => {
          if (this.props.cardId) {
            addToPathway(data.id, this.props.cardId, this.props.cardType)
              .then(() => {
                this.props.dispatch(close());
                this.props.dispatch(openSnackBar(`SmartCard added to pathway "${title}".`, true));
              })
              .catch(err => {
                console.error(`Error in CreateNewPathwayComponent.addToPathway.func : ${err}`);
              });
          } else {
            this.props.dispatch(close());
            openPathwayCreationModal(data);
          }
        })
        .catch(err => {
          console.error(`Error in CreateNewPathwayComponent.createPathway.func : ${err}`);
        });
    } else {
      this.setState({ titleError: true });
    }
  }

  getNewChannels(channelIds) {
    this.setState({ selectedChannels: channelIds });
  }

  clearTitleError(e) {
    this.setState({ titleError: false });
  }

  render() {
    return (
      <div key="create" className="container-padding vertical-spacing-large">
        <TextField
          ref={node => (this._pathwayTitleInput = node)}
          aria-label={tr('Pathway Title')}
          hintText={
            <div>
              {tr('Pathway Title')} <span className="required-mark">*</span>
            </div>
          }
          onChange={e => this.clearTitleError}
          fullWidth={true}
          errorText={this.state.titleError ? tr('Title is required.') : null}
        />
        <ChannelPostComponent onChange={this.getNewChannels} />
        <div className="modal-actions float-right">
          <SecondaryButton
            label={tr('Cancel')}
            className="close"
            onTouchTap={() => {
              this.props.dispatch(close());
            }}
          />
          <PrimaryButton
            label={tr('Create')}
            className="create"
            onTouchTap={this.createClickHandler}
            pending={this.state.createPending}
            pendingLabel={tr('Creating...')}
          />
        </div>
      </div>
    );
  }
}

CreateNewPathwayComponent.propTypes = {
  toggleStyles: PropTypes.object,
  titleError: PropTypes.bool,
  cardId: PropTypes.string,
  cardType: PropTypes.string,
  createPending: PropTypes.bool
};

export default connect()(CreateNewPathwayComponent);
