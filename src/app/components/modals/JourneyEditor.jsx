import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import _ from 'lodash';
import { browserHistory } from 'react-router';

import Checkbox from 'material-ui/Checkbox';
import EditIcon from 'material-ui/svg-icons/image/edit';
import { Calendar } from 'react-date-range';
import moment from 'moment';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import colors from 'edc-web-sdk/components/colors/index';
import FlatButton from 'material-ui/FlatButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';
import RadioButton from 'material-ui/RadioButton/RadioButton';
import {
  saveTempJourney,
  deleteTempJourney,
  isPreviewMode,
  removeReorderCardIds
} from '../../actions/journeyActions';
import TextFieldCustom from '../common/SmartBiteInput';
import SelectField from '../common/SmartBiteDropdown';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import Chip from 'material-ui/Chip';
import JourneyPartEditor from './JourneyPartEditor';
import { getItems } from 'edc-web-sdk/requests/users.v2';
import ReactTags from 'react-tag-autocomplete';
import { getListV2 } from 'edc-web-sdk/requests/groups.v2';
import { fetchBadges, createBadge } from 'edc-web-sdk/requests/badges';
import { postv2, rateCard } from 'edc-web-sdk/requests/cards';
import { filestackClient, getResizedUrl } from '../../utils/filestack';
import getDefaultImage from '../../utils/getDefaultCardImage';
import { updateCurrentCard } from '../../actions/cardsActions';
import { close, openStatusModal } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import TeamActiveIcon from 'edc-web-sdk/components/icons/TeamActiveIcon';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import Tag from 'edc-web-sdk/components/icons/Tag';
import {
  publishPathway,
  deleteSectionFromJourney,
  deleteFromPathway,
  batchAddToPathway,
  batchAddToJourney,
  customReorderPathwayCards,
  deletePathwayFromJourney,
  addCardsToPathways
} from 'edc-web-sdk/requests/pathways.v2';
import calculateSeconds from '../../utils/calculateSecondsForDuration';
import calculateFromSeconds from '../../utils/calculateFromSecondsForDuration';
import addTabInRadioButtons from '../../utils/addTabInRadioButtons';
import {
  createLeap,
  updateLeap,
  lockPathwayCard,
  getRestrictToUserOrGroup,
  showPreviousCardVersions,
  getDefaultImageS3
} from 'edc-web-sdk/requests/cards.v2';
import updateChannel from '../../utils/updateChannel';
import { fileStackSources } from '../../constants/fileStackSource';
import SmartCardVersions from './SmartCardVersions';
import MultilingualContent from '../common/MultilingualContent';
import Sortable from 'sortablejs';
import { reorderJourney } from 'edc-web-sdk/requests/journeys';
import TextField from 'material-ui/TextField';
import CardSharingSection from '../common/CardSharingSection';
import ActionInfo from 'react-material-icons/icons/action/info';
import { Permissions } from '../../utils/checkPermissions';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';

class JourneyEditor extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      cancel: {
        color: darkPurp,
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0',
        maxWidth: '100%'
      },
      chip: {
        margin: 0,
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '0 37px 0 8px',
        width: '100%'
      },
      chipClose: {
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        borderRadius: '50%',
        color: colors.primary,
        verticalAlign: 'middle',
        marginLeft: '1rem',
        marginBottom: '2px',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        position: 'absolute',
        right: '8px',
        top: '5px'
      },
      mainImage: {
        height: '32px',
        width: '32px',
        marginTop: '74px',
        fill: darkPurp
      },
      imageBtn: {
        paddingBottom: '0',
        height: 'auto'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      journeyImage: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%'
      },
      labelChip: {
        paddingRight: '8px'
      },
      actionBtnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      journeyBanner: {
        background: 'url("/i/images/folder.png") 50% 50% no-repeat'
      },
      flexBlock: {
        display: 'flex'
      },
      checkbox: {
        color: colors.primary,
        width: 'auto'
      },
      labelCheckbox: {
        cursor: 'pointer',
        height: '24px',
        fontSize: '12px',
        fontWeight: 300,
        textAlign: 'left',
        color: '#6f708b',
        marginLeft: '-8px',
        width: 'auto'
      },
      privateContent: {
        fontSize: '12px',
        marginTop: '3px'
      },
      labelEditBlock: {
        display: 'inline-flex',
        alignItems: 'flex-end',
        top: '0.1875rem',
        position: 'relative'
      },
      labelBlock: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      labelEdit: {
        cursor: 'pointer',
        fontSize: '0.75rem',
        position: 'relative',
        lineHeight: '1'
      },
      labelIcon: {
        cursor: 'pointer',
        height: '18px',
        marginLeft: '-2px'
      },
      badgesIconBlock: {
        display: 'flex',
        marginBottom: '20px'
      },
      badgeIcon: {
        marginTop: '15px',
        display: 'block',
        marginRight: '20px',
        width: '50px',
        height: '50px'
      },
      badgeText: {
        maxWidth: '385px'
      },
      currentBadge: {
        fontSize: '14px',
        color: '#6f708b'
      },
      badgeLabel: {
        textAlign: 'left',
        width: '100%',
        marginTop: '5px'
      },
      badgeLabelCenter: {
        textAlign: 'center',
        width: '50px',
        marginTop: '5px'
      },
      selectedIcon: {
        border: '5px solid #adaec2',
        borderRadius: '30px',
        width: '60px',
        height: '60px'
      },
      radioButton: {
        display: 'inline-block',
        maxWidth: '50%',
        width: '150px',
        float: 'left',
        fontSize: '12px'
      },
      bigRadioButton: {
        width: '250px'
      },
      startDate: {
        width: '168px'
      },
      iconRadio: {
        marginRight: '5px',
        fill: '#d6d6e1'
      },
      mr5: {
        marginRight: '5px'
      },
      radioBtn: {
        width: 'auto'
      },
      providerBtnImage: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem',
        margin: '0 4px'
      },
      providerIcon: {
        marginTop: '15px',
        display: 'block',
        height: '50px'
      },
      labelTop: {
        alignSelf: 'flex-start'
      },
      providerImageContainer: {
        position: 'relative',
        display: 'inline-block'
      },
      deleteProviderImageBtn: {
        width: '1.25rem',
        height: '1.25rem',
        padding: '0',
        background: '#000000',
        borderRadius: '50%'
      },
      deleteProviderImageBtnIcon: {
        width: '1.25rem',
        height: '1.25rem'
      },
      helperText: {
        fontSize: '10px',
        color: '#9c9cab',
        padding: '0.625rem 0 0'
      },
      tagIconStyles: {
        height: '14px',
        width: '34px'
      },
      restrictToErrorStyle: {
        color: '#999aad',
        fontSize: '11px',
        bottom: '5px',
        paddingLeft: '10px'
      },
      privateChannelStyle: {
        color: '#999aad',
        fontSize: '12px',
        paddingTop: '8px',
        paddingBottom: '8px'
      },
      privateChannelIconStyle: {
        top: '2px',
        width: '14px',
        height: '14px',
        verticalAlign: 'middle',
        float: 'left'
      },
      journeyPartWrapper: {
        position: 'relative',
        height: '100%'
      },
      disableForm: {
        opacity: '0.5',
        pointerEvents: 'none'
      },
      calendar: {
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: '1000',
        left: '0',
        top: '2.5rem',
        boxShadow: '0 0.125rem 0.25rem 0 rgba(0,0,0,.5)'
      }
    };

    let titleLabel = 'Create';
    let createLabel = 'Save for later';
    let previewLabel = 'Preview';
    let publishLabel = 'Publish';
    let selectedChannels = [];
    let nonCuratedChannelIds = [];
    let topics = [];
    let restrictedToUsers = [];
    let userOrTeamSuggestion = [];
    let mainJourney = {
      journeyType: 'self_paced'
    };
    let journey = {};
    let fileStack = [];
    let showMainImage = false;
    let startDate = undefined;
    let completeMethod = 'automatically';
    let journeySections = [{}];
    let advSettings = {};
    let fileStackProvider = false;
    let durationString;
    let weeksValue = undefined;
    let viewPublishButton = false;
    let shareOnSmartCardCreation = window.ldclient.variation('share-on-smart-card-creation', false);
    let showPrivateChannelMsg = false;
    let privateChannelMsg = this.props.card
      ? ''
      : 'Card will remain public until explicitly marked as private.';
    let userActionedPrivateContent = false;
    let showPostToChannelMsg = false;
    let isAbleToMarkPrivate =
      Permissions['enabled'] !== undefined && Permissions.has('MARK_AS_PRIVATE');

    if (props.journey) {
      journey = props.journey;
      titleLabel = 'Update';
      mainJourney = journey;
      if (!journey.title) {
        mainJourney.title = journey.message;
        mainJourney.message = '';
      }
      fileStack = journey.filestack;
      showMainImage = !!journey.filestack.length;
      if (journey.channelIds.length) {
        selectedChannels = journey.channelIds;
      }
      topics = journey.tags.map(tag => tag.name);
      journeySections = journey.journeySection || journey.journeySections || [{}];
      mainJourney.journeyType = journey.cardSubtype;
      let journey_start_date = journey.journey_start_date
        ? new Date(journey.journey_start_date * 1000)
        : undefined;
      startDate =
        journey_start_date ||
        (journeySections && journeySections[0] && new Date(journeySections[0].start_date)) ||
        undefined;
      completeMethod = journey.autoComplete === false ? 'manually' : 'automatically';
      advSettings.provider = journey.provider;
      fileStackProvider = journey.providerImage;
      if (journey.eclDurationMetadata) {
        durationString = calculateFromSeconds(
          journey.eclDurationMetadata.calculated_duration,
          true
        );
        advSettings.duration = journey.eclDurationMetadata.calculated_duration;
      }
      if (journeySections && journeySections.length) {
        weeksValue = journeySections.length;
        viewPublishButton = this.checkSectionCards(journeySections);
      }
      if (props.journey.nonCuratedChannelIds && props.journey.nonCuratedChannelIds.length) {
        nonCuratedChannelIds = props.journey.nonCuratedChannelIds;
      }
    }

    this.isCreatePrivateContentByDefaultEnable =
      shareOnSmartCardCreation &&
      isAbleToMarkPrivate &&
      this.props.team.config &&
      this.props.team.config.create_private_card_by_default;

    this.state = {
      titleLabel,
      weeksValue,
      createLabel,
      previewLabel,
      publishLabel,
      selectedChannels,
      nonCuratedChannelIds,
      showMainImage,
      completeMethod,
      fileStack,
      journey,
      mainJourney,
      journeySections,
      topics,
      startDate,
      disableSubmit: false,
      badges: [],
      badgesText: '',
      currentBadge: {},
      arrIdsSections: [],
      tempBadge: {},
      titleErrorText: '',
      createErrorText: '',
      currentUser: props.currentUser,
      clicked: false,
      maxTagErrorText: '',
      showBadgeWarning: '',
      showAdvancedSettings: false,
      advSettings,
      fileStackProvider,
      durationString,
      isPrivateContent: this.isCreatePrivateContentByDefaultEnable
        ? !!props.journey
          ? !props.journey.isPublic
          : true
        : !!props.journey
        ? !props.journey.isPublic
        : false,
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      selectedBia: props.journey && props.journey.skillLevel,
      cardsToLock: [],
      arrayToLeap: [],
      limit: 12,
      shareOnSmartCardCreation: shareOnSmartCardCreation,
      removeProviderImage: false,
      restrictedToUsers,
      userOrTeamSuggestion,
      showRestrict:
        this.props.journey && this.props.journey.hasOwnProperty('showRestrict')
          ? this.props.journey.showRestrict
          : true,
      previewMode: false,
      tags: [],
      topicTags: [],
      userSuggestions: [],
      channelsSuggestions: [],
      groupsSuggestions: [],
      viewPublishButton,
      previousBadge: {},
      showVersions: window.ldclient.variation('card-versions', false),
      previousVersions: [],
      showPreviousversionsToggle: false,
      disableForm: false,
      sectionsToRemove: [],
      isAbleToMarkPrivate: isAbleToMarkPrivate,
      sectinosToDelete: [],
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      customBadgesUrlFlag: window.ldclient.variation('custom-badges-url', false),
      language: props.journey && props.journey.language ? props.journey.language : 'unspecified',
      useFormLabels:
        window.ldclient.variation('use-form-labels', false) ||
        process.env.NODE_ENV === 'development'
    };

    this.updateFileStackFile = this.updateFileStackFile.bind(this);
    this.selectStartDate = this.selectStartDate.bind(this);
    this.startCreateJourney = this.startCreateJourney.bind(this);
    this.changeBia = this.changeBia.bind(this);
    this.showBIA = !!this.props.team.config.enabled_bia;
    this.enableBadge = window.ldclient.variation('pathway-badge', false);
    this.enablePreview = window.ldclient.variation('enable-edit-preview', true);
    this.isCompleteField = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    let config = this.props.team.OrgConfig;
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;
    this.disablePublish = this.disablePublish.bind(this);
    this.restrictToFlag = window.ldclient.variation('restrict-to', false);
  }

  setJourneyCurrentBadge(badges) {
    if (this.props.journey && this.props.journey.badging) {
      let currentBadge = {
        text: this.props.journey.badging.title,
        id: this.props.journey.badging.badgeId || this.props.journey.badging.id,
        imageUrl: this.props.journey.badging.imageUrl
      };

      let previousBadge = {
        text: this.props.journey.badging.title,
        id: this.props.journey.badging.badgeId || this.props.journey.badging.id,
        imageUrl: this.props.journey.badging.imageUrl
      };

      this.setState({
        currentBadge,
        badgeOn: true,
        tempBadge: currentBadge,
        badges: badges,
        previousBadge
      });
    } else {
      this.setState({ badges });
    }
  }

  componentDidMount() {
    if (this.enableBadge) {
      let teamConfig = this.props.team.config;
      let isCustomBadgesEnabled =
        this.state.customBadgesUrlFlag &&
        teamConfig &&
        teamConfig['custom_badges_bucket_name'] &&
        teamConfig['custom_badges_region'];
      if (isCustomBadgesEnabled) {
        let query = {
          bucket_info: {
            bucket_name: teamConfig['custom_badges_bucket_name'],
            bucket_region: teamConfig['custom_badges_region']
          }
        };
        getDefaultImageS3(query)
          .then(data => {
            if (data && data.image_links && data.image_links.length) {
              let badges = data.image_links.map(item => ({ imageUrl: item }));
              this.setJourneyCurrentBadge(badges);
            }
          })
          .catch(err => {
            console.error(`Error in PathwayEditor.getDefaultImageS3.func : ${err}`);
          });
      } else {
        fetchBadges({ type: 'CardBadge', is_default: true })
          .then(data => {
            this.setJourneyCurrentBadge(data.badges);
          })
          .catch(err => {
            console.error(`Error in JourneyEditor.componentDidMount.fetchBadges.func : ${err}`);
          });
      }
    }

    let payloadGroup = { limit: 1000, offset: 0 };
    payloadGroup['writables'] = true;

    getListV2(payloadGroup)
      .then(groups => {
        groups.teams.map(group => {
          let suggestion = {
            name: group.name,
            id: group.id,
            label: 'group'
          };
          let suggestionState = this.state.groupsSuggestions;
          suggestionState.push(suggestion);
          this.setState({ groupsSuggestions: suggestionState });
        });
      })
      .catch(err => {
        console.error(`Error in JourneyEditor.getListV2.func : ${err}`);
      });

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.journey && !!this.props.journey.tags) {
      let existingTags = this.props.journey.tags;
      this.setState({ topicTags: existingTags });
    }

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.journey) {
      let teamsPermitted = this.props.journey.teamsPermitted
        ? this.props.journey.teamsPermitted
        : [];
      teamsPermitted = teamsPermitted.map(team => {
        let teamsObj = team;
        teamsObj.label = 'group';
        return teamsObj;
      });

      let usersPermitted = this.props.journey.usersPermitted
        ? this.props.journey.usersPermitted
        : [];
      usersPermitted = this.props.journey.usersPermitted.map(user => {
        let userObj = user;
        userObj.label = 'member';
        return userObj;
      });
      this.setState({ restrictedToUsers: [...teamsPermitted, ...usersPermitted] });
    }

    /*---------- Pushing users to tags--------- */
    if (!!this.props.journey && !!this.props.journey.usersWithAccess) {
      let existingMembers = this.props.journey.usersWithAccess;
      _.map(existingMembers, member => {
        let suggestion = {
          name: member.fullName,
          id: member.id,
          label: 'member'
        };
        this.handleAddition(suggestion);
      });
    }

    /*----------Pushing groups to tags----------- */
    if (!!this.props.journey && !!this.props.journey.teams) {
      let existingGroups = this.props.journey.teams;
      _.map(existingGroups, group => {
        let suggestion = {
          name: group.label,
          id: group.id,
          label: 'group'
        };
        this.handleAddition(suggestion);
      });
    }

    /*----- Pushing channels to tags -----------*/
    if (!!this.props.journey && !!this.props.journey.channels) {
      let existingChannels = this.props.journey.channels;
      _.map(existingChannels, channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };
        this.handleAddition(suggestion, false);
      });
    }
    if (
      !(!!this.props.journey && !!this.props.journey.channels) &&
      /channel\//i.test(this.props.pathname)
    ) {
      let channel = _.values(this.props.channelsV2)[0];
      let suggestion = {
        name: channel.label,
        id: channel.id,
        label: 'channel',
        isPrivate: channel.isPrivate
      };
      this.handleAddition(suggestion, false);
    }

    /*----- Finding the channel suggestions-------*/
    let channelsSuggestions = this.props.currentUser.writableChannels;
    if (channelsSuggestions) {
      channelsSuggestions.map(channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };
        let suggestionState = this.state.channelsSuggestions;
        suggestionState.push(suggestion);
        this.setState({ channelsSuggestions: suggestionState });
      });
    }

    /*-----Fetching journey previous versions-------*/
    if (this.props.journey && this.props.journey.id) {
      let payload = {
        entity_type: 'card',
        entity_id: this.props.journey.id
      };
      showPreviousCardVersions(payload)
        .then(data => {
          this.setState({
            previousVersions: data.versions
          });
        })
        .catch(err => {
          console.error(`Error in fetching previous versions of cards: ${err}`);
        });
    }

    let list = document.getElementById('journeyWithHandle');
    Sortable.create(list, {
      sort: true,
      handle: '.move-pathway-btn',
      animation: 150,
      forceFallback: true,
      group: {
        name: 'journey-container'
      },
      onUpdate: this.changeOrder
    });
    setTimeout(() => {
      addTabInRadioButtons('bia-radio');
    }, 500);
  }

  toggleVersionSettings = e => {
    e.preventDefault();
    this.setState({
      showPreviousversionsToggle: !this.state.showPreviousversionsToggle
    });
  };

  changeOrder = event => {
    let payload = {
      from: event.oldIndex + 1,
      to: event.newIndex + 1
    };
    reorderJourney(payload, this.state.journey.id);
  };

  changeBia(event, value) {
    this.setState({
      selectedBia: value
    });
  }

  openBadgeOption = () => {
    if (!this.props.journey || (this.props.journey && !this.state.currentBadge.id)) {
      let openBadgeOptions = !this.state.openBadgeOptions;
      this.setState({ openBadgeOptions, badgeOn: openBadgeOptions });
      if (openBadgeOptions) {
        let currentBadge = this.state.tempBadge;
        this.setState({
          currentBadge,
          tempBadge: {},
          showBadgeWarning: currentBadge.text && currentBadge.text.length > 25
        });
      } else {
        const currentBadge = this.state.currentBadge;
        this.setState({
          tempBadge: currentBadge,
          currentBadge: {},
          showBadgeWarning: currentBadge.text && currentBadge.text.length > 25
        });
      }
    }
  };

  triggerChangeBadge = e => {
    e.preventDefault();
    e.stopPropagation();
    if (this.state.badgeOn && !this.state.openBadgeOptions) {
      let openBadgeOptions = !this.state.openBadgeOptions;
      this.setState({ openBadgeOptions, showBadgeWarning: false });
    }
  };

  revertBadgeChanges = () => {
    let openBadgeOptions = !this.state.openBadgeOptions;
    let previousBadge = {
      text: this.state.previousBadge.text,
      id: this.state.previousBadge.id,
      imageUrl: this.state.previousBadge.imageUrl
    };
    this.setState({
      openBadgeOptions,
      currentBadge: previousBadge,
      badgeOn: true,
      tempBadge: previousBadge
    });
  };

  applyBadgeChanges = () => {
    let openBadgeOptions = !this.state.openBadgeOptions;
    this.setState({ openBadgeOptions, tempBadge: this.state.currentBadge });
  };

  addBadgeHandler = event => {
    let currentBadge = this.state.currentBadge;
    currentBadge.text = event.target.value.trim();
    this.setState({
      currentBadge,
      showBadgeWarning: currentBadge.text && currentBadge.text.length > 25
    });
  };

  selectIconBadge = (e, badge) => {
    e && e.preventDefault();
    let currentBadge = this.state.currentBadge;
    currentBadge.imageUrl = badge.imageUrl;
    currentBadge.id = badge.id;
    this.setState({ currentBadge });
  };

  closeModal = () => {
    this.setState({
      selectedChannels: [],
      topics: [],
      fileStack: [],
      mainJourney: {},
      journey: {},
      showMainImage: false,
      createLabel: 'Save for later',
      publishLabel: 'Publish',
      disableForm: false,
      previewLabel: 'Preview'
    });
    if (this.props.cardUpdated) {
      this.props.cardUpdated();
    }
    if (this.props.isPreviewMode) {
      browserHistory.go(-2);
      this.props.dispatch(isPreviewMode(false));
    }
    this.props.dispatch(close('journey'));
    document.getElementsByClassName('createButton')[0].focus();
  };

  validationJourney = isPublish => {
    this.setState({ clicked: true });
    let valid = true;
    if (isPublish && this.state.journeySections && !this.state.journeySections.length) {
      this.setState({
        createErrorText: 'Journey should contain at least one section!',
        clicked: false
      });
      return (valid = false);
    }
    if (!this.state.mainJourney.title || !this.state.mainJourney.title.replace(/\s/g, '').length) {
      this.setState({ titleErrorText: "Title can't be empty!", clicked: false });
      return (valid = false);
    }
    if (this.state.mainJourney.journeyType === 'weekly' && !this.state.startDate) {
      this.setState({ createErrorText: "Start Date can't be empty!", clicked: false });
      return (valid = false);
    }
    if (this.state.mainJourney.journeyType === 'weekly' && !this.state.weeksValue) {
      this.setState({ createErrorText: 'Select weeks value!', clicked: false });
      return (valid = false);
    }
    if (
      !isPublish &&
      ((this.state.tags && this.state.tags.length) ||
        (this.state.selectedChannels && this.state.selectedChannels.length))
    ) {
      this.setState({
        createErrorText:
          'You can’t share card in draft state! Please clear Share/Post to field or publish the card!',
        clicked: false
      });
      return (valid = false);
    }
    this.state.journeySections.map(section => {
      valid = !!section.block_title ? valid : false;
      if (isPublish) {
        valid = section.pack_cards && section.pack_cards.length ? valid : false;
      }
    });
    if (!valid) {
      let msg = isPublish
        ? 'Each section should have a title and at least one card!'
        : 'Each section should have a title!';
      this.setState({ createErrorText: msg, clicked: false });
    }
    return valid;
  };

  checkSectionCards = sections => {
    let viewPublishButton = true;
    if (sections) {
      sections.map(section => {
        viewPublishButton =
          section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      });
    } else {
      this.state.journeySections.map(section => {
        viewPublishButton =
          section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      });
    }
    return viewPublishButton;
  };

  async startCreateJourney(isPublish) {
    let sections = this.state.journeySections;
    let isEditor = this.props.journey && this.props.journey.id;
    let cardState = isPublish ? 'published' : 'draft';
    let batchAddToPathwaysData = [];
    if (!isEditor) {
      for (let i = 0; i < sections.length; i++) {
        let payload = {
          card: {
            message: '',
            title: sections[i].block_title,
            state: cardState,
            card_type: 'pack',
            card_subtype: 'simple',
            hidden: 1,
            topics: [],
            channel_ids: []
          }
        };
        let data = await postv2(payload);
        if (data && data.embargoErr) {
          this.setState({
            clicked: false,
            publishLabel: 'Publish',
            createLabel: 'Save for later',
            disableForm: false
          });
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
          return;
        } else if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(data.id);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await _.uniq(sections[i].pack_cards.map(card => card.id || card.cardId));

            batchAddToPathwaysData.push({ section_id: data.id, content_ids: ids });

            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payloadForLeap = {
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                if (leapObj.leapId) {
                  updateLeap(leapObj.cardId, leapObj.leapId, payloadForLeap);
                } else {
                  createLeap(leapObj.cardId, payloadForLeap);
                }
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      }

      if (batchAddToPathwaysData.length > 0) {
        // not writing await to allow execution of the following code
        addCardsToPathways({ data: batchAddToPathwaysData });
      }

      await this.createJourney(isPublish, false);
      return;
    } else {
      if (this.state.sectionsToRemove.length) {
        await deletePathwayFromJourney(
          this.state.mainJourney.id,
          this.state.sectionsToRemove,
          false
        );

        let snackMessage = 'You can view the removed pathway under me/content tab.';
        await this.props.dispatch(openSnackBar(snackMessage, true));
      }
      if (this.state.sectinosToDelete.length) {
        await deletePathwayFromJourney(
          this.state.mainJourney.id,
          this.state.sectinosToDelete,
          true
        );
      }
      for (let i = 0; i < sections.length; i++) {
        let cardId = sections[i].card_id;
        let current = this.props.journey.journeySection.find(item => item.card_id == cardId);
        let data = {};
        if (!cardId || sections[i].block_title !== current.block_title) {
          let payload = {
            card: {
              message: sections[i].block_message || '',
              title: sections[i].block_title,
              state: cardState,
              card_type: 'pack',
              card_subtype: 'simple',
              hidden: sections[i].hidden === false ? 0 : 1,
              topics: [],
              channel_ids: []
            }
          };
          data = await postv2(payload, cardId);
        } else {
          data.id = cardId;
        }
        if (data && data.embargoErr) {
          this.setState({
            clicked: false,
            publishLabel: 'Publish',
            createLabel: 'Save for later',
            disableForm: false
          });
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
          return;
        } else if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(`${data.id}`);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await _.uniq(
              sections[i].pack_cards.map(card => `${card.id || card.cardId || card.card_id}`)
            );
            let currentIds =
              current && current.pack_cards && current.pack_cards.map(item => item.id + '');
            let idsToAdd = _.difference(ids, currentIds);

            if (idsToAdd && idsToAdd.length) {
              batchAddToPathwaysData.push({ section_id: data.id, content_ids: idsToAdd });
            }

            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payload = {
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                if (leapObj.leapId) {
                  updateLeap(leapObj.cardId, leapObj.leapId, payload);
                } else {
                  createLeap(leapObj.cardId, payload);
                }
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with update journey!' });
        }
      }

      if (batchAddToPathwaysData.length > 0) {
        // not writing await to allow execution of the following code
        addCardsToPathways({ data: batchAddToPathwaysData });
      }

      await this.createJourney(isPublish, true);
      return;
    }
  }

  createJourney = async (isPublish, isEditor) => {
    let sharedtoMembers = [];
    let sharedtoGroups = [];
    let sharedtoChannels = this.state.selectedChannels;
    let teams_with_permission_ids = [];
    let users_with_permission_ids = [];
    let cardState = isPublish ? 'published' : 'draft';

    this.state.tags.forEach(tag => {
      switch (tag.label) {
        case 'member':
          sharedtoMembers.push(tag.id);
          break;
        case 'group':
          sharedtoGroups.push(tag.id);
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });
    let payload = {
      message: this.state.mainJourney.message || this.state.mainJourney.title,
      title: this.state.mainJourney.title,
      state: cardState,
      card_type: 'journey',
      users_with_access_ids: sharedtoMembers,
      team_ids: sharedtoGroups,
      card_subtype: this.state.mainJourney.journeyType,
      auto_complete: this.state.completeMethod === 'automatically',
      is_public: !this.state.isPrivateContent + ''
    };

    //=====restricted to payload
    if (this.state.showRestrict && this.state.isPrivateContent) {
      this.state.restrictedToUsers.map(restrictTo => {
        if (restrictTo.label == 'group') {
          teams_with_permission_ids.push(restrictTo.id);
        }
        if (restrictTo.label == 'member') {
          users_with_permission_ids.push(restrictTo.id);
        }
      });

      if (teams_with_permission_ids.length > 0) {
        payload['teams_with_permission_ids'] = teams_with_permission_ids;
      }

      if (users_with_permission_ids.length > 0) {
        payload['users_with_permission_ids'] = users_with_permission_ids;
      }
    }

    if (
      (this.restrictToFlag && !this.state.isPrivateContent) ||
      (this.restrictToFlag && this.state.tags.length === 0) ||
      (this.restrictToFlag && this.state.restrictedToUsers.length == 0)
    ) {
      payload['teams_with_permission_ids'] = [];
      payload['users_with_permission_ids'] = [];
    }

    if (this.state.mainJourney.journeyType === 'weekly') {
      payload['journey_start_date'] = +this.state.startDate / 1000;
    }

    if (
      this.enableBadge &&
      this.state.currentBadge &&
      this.state.currentBadge.text &&
      !!this.state.currentBadge.text.trim()
    ) {
      if (this.state.currentBadge.id) {
        payload['card_badging_attributes'] = {
          title: this.state.currentBadge.text.trim(),
          badge_id: this.state.currentBadge.id
        };
      } else if (this.state.currentBadge.imageUrl) {
        let newBadge = await createBadge({
          type: 'CardBadge',
          image_url: this.state.currentBadge.imageUrl
        });
        payload['card_badging_attributes'] = {
          title: this.state.currentBadge.text.trim(),
          badge_id: newBadge.id
        };
      }
    }
    if (this.state.topics || !!this.state.journey.tags.length) {
      payload.topics = this.state.topics;
    }
    if (this.state.selectedChannels || !!this.state.nonCuratedChannelIds.length) {
      sharedtoChannels = this.state.selectedChannels;
      if (!!this.state.nonCuratedChannelIds.length) {
        sharedtoChannels = _.uniq(sharedtoChannels.concat(this.state.nonCuratedChannelIds));
      }
      payload.channel_ids = sharedtoChannels;
    }
    if (!!this.state.fileStack.length) {
      payload.filestack = this.state.fileStack;
    } else {
      const defaultImg = getDefaultImage(this.props.currentUser.id);
      payload.filestack = [defaultImg];
    }
    if (this.state.advSettings.provider || this.state.advSettings.provider === '') {
      payload.provider = this.state.advSettings.provider;
    }
    if (
      this.state.removeProviderImage ||
      (this.state.fileStackProvider && !!this.state.fileStackProvider.length)
    ) {
      payload.provider_image = this.state.fileStackProvider;
      this.setState({ removeProviderImage: false });
    }
    if (this.state.advSettings.duration) {
      payload.duration = this.state.advSettings.duration;
    }
    if (this.state.selectedBia) {
      let cardMetadatum = this.state.journey.cardMetadatum;
      if (cardMetadatum) {
        payload['card_metadatum_attributes'] = {
          id: cardMetadatum.id,
          level: this.state.selectedBia
        };
      } else {
        payload['card_metadatum_attributes'] = { level: this.state.selectedBia };
      }
    }
    if (this.state.multilingualContent) {
      this.state.language === 'unspecified'
        ? (payload.language = null)
        : (payload.language = this.state.language);
    }
    postv2({ card: payload }, this.props.journey && this.props.journey.id)
      .then(data => {
        if (data && data.embargoErr) {
          this.setState({
            clicked: false,
            publishLabel: 'Publish',
            createLabel: 'Save for later',
            disableForm: false
          });
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
        } else if (data && data.id) {
          this.createJourneyCb(isEditor, data, isPublish);
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      })
      .catch(err => {
        console.error(`Error in JourneyEditor.postv2.func : ${err}`);
      });
  };

  createJourneyCb = (isEditor, data, isPublish) => {
    let idsToAdd;
    if (isEditor) {
      let currentIds = this.props.journey.journeySection.map(item => `${item.card_id}`);
      idsToAdd = _.difference(this.state.arrIdsSections, currentIds);
    } else {
      idsToAdd = this.state.arrIdsSections;
    }
    if (idsToAdd.length) {
      batchAddToJourney(data.id, idsToAdd)
        .then(() => {
          this.afterLastBatch(isPublish, data);
        })
        .catch(err => {
          console.error(`Error in JourneyEditor.batchAddToJourney.func : ${err}`);
        });
    } else {
      this.afterLastBatch(isPublish, data);
    }
  };

  afterLastBatch = (isPublish, data) => {
    if (this.state.cardsToLock.length) {
      this.state.cardsToLock.forEach((sectionCardsToLock, index) => {
        if (Object.keys(sectionCardsToLock).length > 0) {
          for (let prop in sectionCardsToLock) {
            lockPathwayCard(prop, {
              pathway_id: this.state.arrIdsSections[index],
              locked: sectionCardsToLock[prop].locked
            });
          }
        }
      });
    }
    if (this.props.reorderCardIds && this.props.reorderCardIds.length) {
      this.props.reorderCardIds.forEach(section => {
        customReorderPathwayCards(section.sectionId, section.cards);
      });
      this.props.dispatch(removeReorderCardIds());
    }
    if (isPublish) {
      publishPathway(data.id)
        .then(() => {
          this.afterPublish(data, true);
        })
        .catch(() => {
          this.props.dispatch(this.closeModal);
        });
    } else {
      this.afterPublish(data);
    }
  };

  readyToViewCallback = (data, isCurrentJourney, updateContentTab) => {
    let slugOrId, currentChannel, isAddingToCurrentChannel;
    if (data && data.card && this.props.pathname.indexOf('/channel/') > -1) {
      let routeParam = this.props.pathname.split('/');
      slugOrId = routeParam[routeParam.length - 1];
      currentChannel = this.props.channelsV2 && Object.values(this.props.channelsV2);
      currentChannel = currentChannel.length && currentChannel[0];
      isAddingToCurrentChannel =
        currentChannel && data.card.channelIds.indexOf(currentChannel.id) > -1;
    }
    if (
      currentChannel &&
      slugOrId &&
      (slugOrId == currentChannel.id || slugOrId === currentChannel.slug)
    ) {
      if (isAddingToCurrentChannel) {
        updateChannel(this.props.dispatch, currentChannel, this.state.limit, true);
      }
    } else if (data && !updateContentTab) {
      isCurrentJourney ? this.props.dispatch(push(`/journey/${data.slug}`)) : this.openContentTab();
    }
  };

  afterPublish = (data, isPublish) => {
    if (this.props.cardUpdated) {
      this.props.cardUpdated();
    }
    this.props.dispatch(this.closeModal);
    let updateContentTab = !!~this.props.pathname.indexOf('me/content') && this.props.journey;
    if (updateContentTab) {
      this.props.dispatch(updateCurrentCard(data.card));
    }
    let isCurrentJourney = !!~this.props.pathname.indexOf(this.state.mainJourney.title);
    if (isPublish) {
      setTimeout(() => {
        let message = 'Your Journey is ready to view!';
        if (
          !data.card.isPublic &&
          !data.card.teams.length &&
          !data.card.channels.length &&
          !data.card.usersWithAccess.length
        ) {
          message =
            message + '<br> P.S. This journey will not be visible to anyone but the author.';
        }
        if (this.state.newModalAndToast) {
          this.props.dispatch(
            openSnackBar(message, () => {
              this.readyToViewCallback(data, isCurrentJourney, updateContentTab);
            })
          );
        } else {
          this.props.dispatch(
            openStatusModal(message, () => {
              this.readyToViewCallback(data, isCurrentJourney, updateContentTab);
            })
          );
        }
      }, 500);
    } else {
      setTimeout(() => {
        if (this.state.newModalAndToast) {
          this.props.dispatch(
            openSnackBar(
              `Your draft Journey is saved!${
                !isCurrentJourney ? ' Go to Me/Content to finish' : ''
              }`,
              () => {
                isCurrentJourney
                  ? this.props.dispatch(push(`/journey/${data.slug}`))
                  : this.openContentTab();
              }
            )
          );
        } else {
          this.props.dispatch(
            openStatusModal(
              `Your draft Journey is saved!${
                !isCurrentJourney ? ' Go to Me/Content to finish' : ''
              }`,
              () => {
                isCurrentJourney
                  ? this.props.dispatch(push(`/journey/${data.slug}`))
                  : this.openContentTab();
              }
            )
          );
        }
      }, 500);
    }
  };

  openContentTab = () => {
    if (this.state.isAddingToCurrentChannel && this.state.currentChannel) {
      this.props.dispatch(
        close('journey'),
        updateChannel(this.props.dispatch, this.state.currentChannel, this.state.limit)
      );
    } else if (!~this.props.pathname.indexOf('/me/')) {
      this.props.dispatch(push('/me/content'));
    } else {
      window.location.href = '/me/content';
    }
    this.props.dispatch(deleteTempJourney());
  };

  changeCompleteMethod(event, value) {
    this.setState({
      completeMethod: value
    });
  }

  createClickHandler = () => {
    let valid = this.validationJourney();
    if (valid) {
      this.setState({ createLabel: this.props.journey ? 'Updating...' : 'Creating...' });
      this.props.dispatch(isPreviewMode(false));
      this.startCreateJourney();
    }
  };

  previewClickHandler = async () => {
    let valid = this.validationJourney();
    if (valid) {
      this.setState({ previewLabel: 'Previewing...' });
      let payload = {};
      if (this.state.journey && this.state.journey.id) {
        payload = this.state.journey;
      } else {
        payload = {
          message: this.state.mainJourney.message || this.state.mainJourney.title,
          title: this.state.mainJourney.title,
          state: this.props.mainJourney ? this.props.mainJourney.state : 'draft',
          cardType: 'journey',
          cardSubtype: this.state.mainJourney.journeyType
        };
      }
      if (this.state.mainJourney.journeyType === 'weekly') {
        let date = this.state.startDate;
        date = `${moment(+this.state.startDate).format('DD/MM/YYYY')}`;
        payload['journey_start_date'] = +this.state.startDate / 1000;
      }
      if (
        this.enableBadge &&
        this.state.currentBadge &&
        this.state.currentBadge.text &&
        !!this.state.currentBadge.text.trim()
      ) {
        if (this.state.currentBadge.id) {
          payload.badging = {
            id: this.state.currentBadge.id,
            title: this.state.currentBadge.text,
            imageUrl: this.state.currentBadge.imageUrl
          };
        } else if (this.state.currentBadge.imageUrl) {
          payload.badging = {
            ...(await createBadge({
              type: 'CardBadge',
              image_url: this.state.currentBadge.imageUrl
            })),
            ...{ title: this.state.currentBadge.text }
          };
        }
      }
      if (this.state.topics || !!this.state.journey.tags.length) {
        let tags = [];
        for (let i = 0; i < this.state.topics.length; i++) {
          let temp = {};
          temp.name = this.state.topics[i];
          tags[i] = temp;
        }
        payload.tags = tags;
      }
      if (this.state.selectedChannels) {
        let channels = [];
        for (let i = 0; i < this.state.selectedChannels.length; i++) {
          for (let j = 0; j < this.props.currentUser.writableChannels.length; j++) {
            let channel = {};
            if (this.state.selectedChannels[i] === this.props.currentUser.writableChannels[j].id) {
              channel.label = this.props.currentUser.writableChannels[j].label;
              channel.id = this.props.currentUser.writableChannels[j].id;
              channels.push(channel);
            }
          }
        }
        payload.channelIds = this.state.selectedChannels;
        payload.channels = channels;
      }
      if (!!this.state.fileStack.length) {
        payload.filestack = this.state.fileStack;
      } else {
        const defaultImg = getDefaultImage(this.props.currentUser.id);
        payload.filestack = [defaultImg];
      }
      if (this.state.advSettings.provider) {
        payload.provider = this.state.advSettings.provider;
      }
      if (
        this.state.removeProviderImage ||
        (this.state.fileStackProvider && !!this.state.fileStackProvider.length)
      ) {
        payload.providerImage = this.state.fileStackProvider;
      }
      if (this.state.advSettings.duration) {
        payload.eclDurationMetadata = {};
        payload.eclDurationMetadata.calculated_duration = this.state.advSettings.duration;
        payload.eclDurationMetadata.calculated_duration_display = calculateFromSeconds(
          this.state.advSettings.duration
        );
      }
      if (this.state.journeySections && this.state.journeySections.length) {
        let sections = this.state.journeySections.map(item => ({ ...item }));
        let journeySections = sections.map(item => ({ ...item }));
        for (let i = 0; i < sections.length; i++) {
          if (sections[i].pack_cards) {
            journeySections[i].cards = sections[i].pack_cards.map(item => ({ ...item }));
          }
        }
        payload.journeySections = journeySections;
      }
      if (this.props.currentUser) {
        payload.author = this.props.currentUser;
      }
      if (this.state.cardsToLock) {
        payload.cardsToLock = this.state.cardsToLock;
      }
      if (this.state.tags) {
        payload.teams = [];
        this.state.tags.map(tag => {
          if (tag.label === 'group') {
            payload.teams.push(tag);
          }
        });
      }
      if (this.state.multilingualContent) {
        payload.language = this.state.language;
      }
      this.props.dispatch(isPreviewMode(true));
      if (!~this.props.pathname.indexOf('/journey/')) {
        this.props.dispatch(saveTempJourney(payload));
        this.props.journey && this.props.journey.id
          ? this.props.dispatch(push(`/journey/${this.props.journey.slug}`))
          : this.props.dispatch(push(`/journey/${payload.title}`));
      } else {
        this.props.dispatch(saveTempJourney(payload));
        this.props.dispatch(close());
      }
    }
  };

  publishClickHandler = () => {
    let valid = this.validationJourney(true);
    if (valid) {
      this.setState({ publishLabel: 'Publishing...', disableForm: true });
      this.props.dispatch(isPreviewMode(false));
      this.startCreateJourney(true);
    }
  };

  getNewChannels = (selectedChannels, channel, pathname) => {
    let isAddingToCurrentChannel = false;
    let currentChannel;
    if (
      channel &&
      channel.slug &&
      pathname &&
      (channel.slug === pathname[2] || `${channel.id}` === pathname[2])
    ) {
      isAddingToCurrentChannel = true;
      currentChannel = { id: channel.id, slug: channel.slug };
    }

    this.setState(
      { showPostToChannelMsg: false, selectedChannels, isAddingToCurrentChannel, currentChannel },
      () =>
        this.state.selectedChannels.map(id => {
          if (this.props.currentUser.writableChannels.find(e => e.id == id && e.isPrivate)) {
            this.setState({ showPostToChannelMsg: true });
          }
        })
    );
  };

  changeTag = event => {
    this.setState({
      maxTagErrorText:
        event.target.value.length <= 150
          ? ''
          : 'Max tag length exceeded. It must be shorter than 150 characters.'
    });
  };

  addTagHandler = event => {
    if (!~this.state.topics.indexOf(event.target.value.trim()) && !!event.target.value.trim()) {
      this.setState({ topics: this.state.topics.concat(event.target.value.trim()) });
    }
  };

  fileStackHandler = isProvider => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: ['image/*'],
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(fileStack => this.updateFileStackFile(fileStack, isProvider))
          .catch(err => {
            console.error(`Error in JourneyEditor.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in JourneyEditor.uploadPolicyAndSignature.func : ${err}`);
      });
  };

  journeyBannerFileStackHandler = (e, isProvider) => {
    e && e.preventDefault();
    this.fileStackHandler(isProvider);
  };

  updateFileStackFile(fileStack, isProvider) {
    let tempPath = fileStack.filesUploaded;
    let securedUrl;
    refreshFilestackUrl(tempPath[0].url)
      .then(resp => {
        securedUrl = resp.signed_url;
        if (isProvider) {
          this.setState({ fileStackProvider: tempPath[0].url, securedProviderUrl: securedUrl });
        } else {
          this.setState({
            fileStack: tempPath,
            securedUrl: securedUrl,
            showMainImage: tempPath.length
          });
        }
      })
      .catch(err => {
        console.error(`Error in JourneyEditor.updateFileStackFile.func : ${err}`);
      });
  }

  clearFileStackProviderInfo = () => {
    this.setState({ fileStackProvider: null, securedProviderUrl: null, removeProviderImage: true });
  };

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton style={this.styles.imageBtn} aria-label={tr('Change Image')}>
          <ImagePhotoCamera color="white" />
        </IconButton>
        <div className="roll-text">{tr('Change Image')}</div>
      </span>
    );
  }

  journeyMainChange = (type, event) => {
    let mainJourney = this.state.mainJourney;
    mainJourney[type] = event.target.value;
    this.setState({ mainJourney, titleErrorText: '' });
  };

  removeTag = tag => {
    let topics = this.state.topics.filter(topic => topic !== tag);
    this.setState({ topics });
  };

  addNewCardsBlock = () => {
    let journeySections = this.state.journeySections;
    journeySections.push({});
    let viewPublishButton = this.checkSectionCards();
    this.setState({ journeySections, viewPublishButton });
  };

  changeWeeksHandler = e => {
    let newCount = e.target.value;
    let journeySections = this.state.journeySections;
    if (journeySections.length < newCount) {
      let diff = newCount - journeySections.length;
      _.times(diff, () => {
        journeySections.push({});
      });
    } else {
      let diff = journeySections.length - newCount;
      _.times(diff, () => {
        journeySections.pop();
      });
    }
    this.setState({ weeksValue: newCount, journeySections });
  };

  changeJourneyTypeHandler = (e, val) => {
    let mainJourney = this.state.mainJourney;
    mainJourney['journeyType'] = val;
    let createErrorText = '';
    this.setState({ mainJourney, createErrorText });

    if (
      val === 'weekly' &&
      this.state.weeksValue &&
      this.state.weeksValue !== this.state.journeySections.length
    ) {
      let journeySections = this.state.journeySections;
      if (journeySections.length < this.state.weeksValue) {
        let diff = this.state.weeksValue - journeySections.length;
        _.times(diff, () => {
          journeySections.push({});
        });
      } else {
        let diff = journeySections.length - this.state.weeksValue;
        _.times(diff, () => {
          journeySections.pop();
        });
      }
      this.setState({ journeySections });
    }
  };

  selectStartDate(e) {
    let createErrorText = '';
    this.setState({ calendarOpen: false, createErrorText, startDate: e.format('x') });
  }

  handlePrivateContent = event => {
    this.setState(
      {
        isPrivateContent: event.target.checked
      },
      () => {
        this.setPrivateChannelMessage();
      }
    );
  };

  removeBlock = index => {
    let journeySections = this.state.journeySections;
    if (journeySections[index]) {
      if (this.props.journey && this.props.journey.id && journeySections[index].card_id) {
        let sectionsToRemove = [];
        sectionsToRemove.push(journeySections[index].card_id);
        this.setState({ sectionsToRemove });
      }

      journeySections.splice(index, 1);
      this.setState({ journeySections });
    }
    let viewPublishButton = this.checkSectionCards();
    this.setState({ viewPublishButton });
  };

  deleteBlock = index => {
    let journeySections = this.state.journeySections;
    if (journeySections[index]) {
      if (this.props.journey && this.props.journey.id && journeySections[index].card_id) {
        let sectinosToDelete = [];
        sectinosToDelete.push(journeySections[index].card_id);
        this.setState({ sectinosToDelete });
      }

      journeySections.splice(index, 1);
      this.setState({ journeySections });
    }
    let viewPublishButton = this.checkSectionCards();
    this.setState({ viewPublishButton });
  };

  updateSectionCards = (smartBites, index) => {
    let journeySections = this.state.journeySections;
    if (journeySections[index]) {
      journeySections[index].pack_cards = smartBites;
    } else {
      journeySections[index] = {
        block_title: '',
        pack_cards: smartBites
      };
    }
    let viewPublishButton = this.checkSectionCards();
    this.setState({ journeySections, viewPublishButton });
  };

  updateSectionTitle = (title, index) => {
    let journeySections = this.state.journeySections.map(a => ({ ...a }));
    if (journeySections[index]) {
      journeySections[index].block_title = title;
    } else {
      journeySections[index] = {
        block_title: title,
        pack_cards: []
      };
    }
    this.setState({ journeySections, createErrorText: '' });
  };

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        showAdvancedSettings: !prevState.showAdvancedSettings
      };
    });
  };

  advSettingsChange = (type, event) => {
    let advSettings = this.state.advSettings;
    if (type === 'duration') {
      advSettings[type] = calculateSeconds(event.target.value);
      this.setState({
        durationString: event.target.value
      });
    } else {
      advSettings[type] = event.target.value;
    }
    this.setState({ advSettings });
  };

  lockCard = (smartBites, sectionIndex, index) => {
    let cardsToLock = this.state.cardsToLock;
    cardsToLock[sectionIndex] = cardsToLock[sectionIndex] ? cardsToLock[sectionIndex] : {};
    if (smartBites[index].hasOwnProperty('isLocked')) {
      let cardInPack =
        this.state.journeySections[sectionIndex] &&
        this.state.journeySections[sectionIndex].pack_cards &&
        this.state.journeySections[sectionIndex].pack_cards.find(
          item => item.card_id == smartBites[index].id || item.id == smartBites[index].id
        );
      let isInCardsToLock =
        cardsToLock[sectionIndex] && cardsToLock[sectionIndex][smartBites[index].id];
      if (
        (!cardInPack && !isInCardsToLock) ||
        (cardInPack &&
          !isInCardsToLock &&
          cardInPack.locked != smartBites[index].isLocked &&
          cardInPack.isLocked != smartBites[index].isLocked)
      ) {
        cardsToLock[sectionIndex][smartBites[index].id] = {
          locked: `${smartBites[index].isLocked}`
        };
      } else {
        if (cardsToLock[sectionIndex][smartBites[index].id]) {
          delete cardsToLock[sectionIndex][smartBites[index].id];
        }
      }
    }
    let journeySections = this.state.journeySections.map(item => ({ ...item }));
    journeySections[sectionIndex].pack_cards = smartBites;
    this.setState({ cardsToLock, journeySections });
  };

  addToLeap = leapObj => {
    let journeySections = this.state.journeySections;
    let arrayToLeap = journeySections[leapObj.sectionIndex].arrayToLeap
      ? journeySections[leapObj.sectionIndex].arrayToLeap
      : [];
    let leapIndex = _.findIndex(
      arrayToLeap,
      item => item.sectionIndex === leapObj.sectionIndex && item.cardId == leapObj.cardId
    );
    if (leapIndex > -1) {
      arrayToLeap[leapIndex] = leapObj;
    } else {
      arrayToLeap.push(leapObj);
    }
    journeySections[leapObj.sectionIndex].arrayToLeap = arrayToLeap;
    this.setState({ journeySections });
  };

  isWeekly = i => {
    let cardDate = new Date(this.state.journeySections[i].start_date);
    let nowDate = new Date();
    let nextSevenDay = new Date(cardDate);
    let dateValue = nextSevenDay.getDate() + 7;
    nextSevenDay.setDate(dateValue);
    return (
      this.state.journey.cardSubtype === 'weekly' &&
      !(nowDate >= cardDate && nowDate <= nextSevenDay)
    );
  };

  handleAdditionTags(tag) {
    let newTopicTags = this.state.topicTags;
    let index = _.findIndex(newTopicTags, existingTag => {
      return existingTag.name == tag.name;
    });
    if (!(index >= 0)) {
      newTopicTags.push(tag);
      this.setState({ topicTags: newTopicTags });
    }
  }

  handleAddition(tag, showMessage = true) {
    let selectedChannels = this.state.selectedChannels.slice();
    switch (tag.label) {
      case 'member':
        _.remove(this.state.userSuggestions, existingMember => {
          return existingMember.id + '' === tag.id + '';
        });
        break;
      case 'channel':
        _.remove(this.state.channelsSuggestions, existingChannel => {
          return existingChannel.id + '' === tag.id + '';
        });
        selectedChannels.push(tag.id);
        this.setState({ selectedChannels });
        break;
      case 'group':
        _.remove(this.state.groupsSuggestions, existingGroup => {
          return existingGroup.id + '' === tag.id + '';
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    let existingTags = this.state.tags;
    existingTags.push(tag);
    if (this.state.restrictedToUsers.length > 0) {
      this.filterRestrictedGroupsOrUser(existingTags);
    }

    this.setState({ tags: existingTags }, () => {
      if (tag.label == 'channel' && showMessage) {
        this.setPrivateChannelMessage(tag);
      }
    });
  }

  setPrivateChannelMessage = tagAdded => {
    let channels = this.state.tags.filter(e => e.label == 'channel');
    if (channels.length == 0) {
      this.setState({
        showPrivateChannelMsg: false,
        privateChannelMsg: ''
      });
    } else {
      let privateChannels = channels.filter(e => e.isPrivate);
      let publicChannels = channels.filter(e => !e.isPrivate);
      let privateContent = this.state.isPrivateContent;
      if (!privateContent && privateChannels.length && publicChannels.length) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Card will remain public until explicitly marked as private.'
        });
      } else if (!privateContent && privateChannels.length) {
        let setStateObject = {};
        if (!this.state.userActionedPrivateContent) {
          setStateObject['isPrivateContent'] = true;
        }
        setStateObject['showPrivateChannelMsg'] = true;
        setStateObject['privateChannelMsg'] =
          'Card shared with private channel will need to be marked as private to control its visibility.';
        this.setState(setStateObject);
      } else if (
        privateContent &&
        privateChannels.length &&
        tagAdded &&
        !tagAdded.isPrivate &&
        publicChannels.length == 1
      ) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Visibility of card increases when shared with public channel'
        });
      } else {
        this.setState({
          showPrivateChannelMsg: false,
          privateChannelMsg: ''
        });
      }
    }
  };

  filterRestrictedGroupsOrUser = tags => {
    let restrictedToUsers = this.state.restrictedToUsers;
    let tempRestricted = this.state.restrictedToUsers;
    let finalUsersOrGroups = [];
    tags.map(tag => {
      switch (tag.label) {
        case 'channel':
          tempRestricted = restrictedToUsers.filter(restricted => {
            let restrictedChannelID = restricted.channelIds.filter(
              channelID => channelID == tag.id
            );
            restrictedChannelID = restrictedChannelID.length ? restrictedChannelID[0] : '';
            //if channel mentioned in share is present in the restrested-to user then it should be included in this array
            return restrictedChannelID == tag.id;
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'group':
          tempRestricted = restrictedToUsers.filter(restricted => {
            if (restricted.label === 'member') {
              let restrictedGroupID = restricted.teamIds.filter(teamID => teamID == tag.id);
              restrictedGroupID = restrictedGroupID.length ? restrictedGroupID[0] : '';
              //if group mentioned in share is present in the restrested-to user then it should be included in this array
              return restrictedGroupID == tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'member':
          tempRestricted = restrictedToUsers.filter(restricted => {
            //if member mentioned in share is present in the restrested-to user then it should be included in this array
            if (restrictedToUsers.label === 'member') {
              return restricted.id != tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];

          break;
        default:
          break;
      }
    });

    finalUsersOrGroups = _.uniq(finalUsersOrGroups);
    this.setState({ restrictedToUsers: finalUsersOrGroups });
  };

  selectedRestrictedToTags(tag) {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={this.handleUserDelete.bind(this, tag.tag)}
      >
        {tag && tag.tag && tag.tag.label === 'group' && (
          <GroupActiveIcon viewBox="0 0 45 30" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'channel' && (
          <TeamActiveIcon viewBox="0 2 50 28" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'member' && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  }

  getRestrictedErrorMsg = () => {
    let msg = '';
    let showRestrict = this.state.showRestrict;

    if (!this.state.isPrivateContent && !!this.state.tags.length) {
      msg = 'You must mark the card as private to be able to add restrictions';
    }

    if (!showRestrict) {
      msg = 'Restriction disabled as the content has been assigned to/bookmarked by some users.';
    }

    return msg;
  };

  handleDelete(tagInfo) {
    let tag = typeof tagInfo === 'number' ? this.state.tags[tagInfo] : tagInfo;

    if (typeof tagInfo === 'undefined') {
      return;
    }
    switch (tag.label) {
      case 'member':
        _.concat(this.state.userSuggestions, existingMember => {
          return existingMember.id == tag.id;
        });
        break;
      case 'channel':
        _.concat(this.state.channelsSuggestions, existingChannel => {
          return existingChannel.id == tag.id;
        });
        break;
      case 'group':
        _.concat(this.state.groupsSuggestions, existingGroup => {
          return existingGroup.id == tag.id;
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    if (tag.label == 'channel') {
      let selectedChannels = _.uniq(this.state.selectedChannels);
      selectedChannels.splice(selectedChannels.indexOf(tag.id), 1);
      this.setState({ selectedChannels });
    }

    let tags = this.state.tags;

    let index = _.findIndex(tags, existingTag => {
      return existingTag.id == tag.id && existingTag.label == tag.label;
    });
    let suggestionState = this.state.channelsSuggestions;
    suggestionState.push(tag);
    tags.splice(index, 1);
    if (this.state.restrictedToUsers.length > 0) {
      this.filterRestrictedGroupsOrUser(tags);
    }
    this.setState({ tags }, () => {
      if (tag.label == 'channel') {
        this.setPrivateChannelMessage();
      }
    });
  }

  handleDeleteTags(tag) {
    let topicTags = this.state.topicTags;

    let index = _.findIndex(topicTags, existingTag => {
      return existingTag.name == tag.name;
    });

    topicTags.splice(index, 1);
    this.setState({ topicTags });
  }

  handleInputChange(query) {
    let payload = {
      limit: 15,
      offset: 0,
      q: query
    };

    getItems(payload)
      .then(members => {
        _.map(members.items, member => {
          let suggestion = {
            name: member.name,
            id: parseInt(member.id, 10),
            label: 'member'
          };

          let index = _.findIndex(this.state.tags, existingTag => {
            return (
              parseInt(suggestion.id, 10) == parseInt(existingTag.id, 10) &&
              existingTag.label == 'member'
            );
          });
          if (index < 0) {
            let suggestionState = this.state.userSuggestions;
            suggestionState.push(suggestion);
            this.setState({ userSuggestions: suggestionState });
          }
        });
      })
      .catch(err => {
        console.error(`Error in JourneyEditor.handleInputChange.getItems.func : ${err}`);
      });
  }

  selectedTopicTags(tag) {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={this.handleDeleteTags.bind(this, tag.tag)}
      >
        <Tag viewBox="0 0 30 25" style={this.styles.tagIconStyles} />
        {tag.tag.name}
      </span>
    );
  }

  cleanLevel = () => {
    this.setState({ selectedBia: '' });
  };

  disablePublish() {
    return !!(
      this.state.disableSubmit ||
      this.state.clicked ||
      (this.state.openBadgeOptions &&
        (!this.state.currentBadge.text ||
          !this.state.currentBadge.id ||
          this.state.showBadgeWarning))
    );
  }

  handleUserAddition = tag => {
    let restrictedToUsers = [...this.state.restrictedToUsers, ...[tag]];
    this.setState({ restrictedToUsers });
  };

  handleUserDelete = tagInfo => {
    let tag = typeof tagInfo === 'number' ? this.state.restrictedToUsers[tagInfo] : tagInfo;
    let restrictedToUsers = this.state.restrictedToUsers.filter(data => data.id != tag.id);
    this.setState({ restrictedToUsers });
  };

  handleRestrictedInputChange = query => {
    if (!!!query) {
      return;
    }
    let team_ids = [];
    let channel_ids = [];
    let payload = {
      limit: 5,
      offset: 0,
      q: query
    };
    this.state.tags.map(tag => {
      if (tag.label === 'group') {
        team_ids.push(tag.id);
      } else if (tag.label === 'channel') {
        channel_ids.push(tag.id);
      }
    });

    if (team_ids.length > 0) {
      payload['team_ids[]'] = team_ids;
    }

    if (channel_ids.length > 0) {
      payload['channel_ids[]'] = channel_ids;
    }

    if (!!this.props.journey) {
      payload['card_id'] = this.props.journey.id;
    }

    getRestrictToUserOrGroup(payload)
      .then(restrictedToData => {
        let users = restrictedToData.users.map(user => {
          let editedUsers = user;
          editedUsers['label'] = 'member';
          return editedUsers;
        });
        let groups = restrictedToData.groups.map(group => {
          let editedGroup = group;
          editedGroup['label'] = 'group';
          return editedGroup;
        });
        let userOrTeamSuggestion = [...users, ...groups];

        this.setState({ userOrTeamSuggestion });
      })
      .catch(() => {});
  };

  checkToShare = (e, item) => {
    let tags = this.state.tags;
    let index = _.findIndex(tags, tag => tag.label === item.label && +tag.id === +item.id);
    if (index > -1) {
      tags.splice(index, 1);
    } else {
      tags.push(item);
    }
    this.setState({ tags });
  };

  handleLanguageChange = data => {
    this.setState({ language: data });
  };

  render() {
    let showCheckbox = !this.props.journey || (this.props.journey && !this.state.currentBadge.id);
    let isCurator =
      this.props.currentUser &&
      this.props.currentUser.roles &&
      !!~this.props.currentUser.roles.indexOf('curator');
    let tagSuggestions = _.uniqWith(
      [
        ...this.state.groupsSuggestions,
        ...this.state.channelsSuggestions,
        ...this.state.userSuggestions
      ],
      _.isEqual
    );
    let restrictAble = false;
    let showRestrict = this.state.showRestrict;
    let restrictToError = this.getRestrictedErrorMsg();

    for (let i = 0; i < this.state.tags.length; i++) {
      if (this.state.tags[i].label === 'group' || this.state.tags[i].label === 'channel') {
        restrictAble = this.state.isPrivateContent && showRestrict;
        break;
      }
    }

    return (
      <div
        className="pathway-creation pathway-creation_journey"
        style={this.state.disableForm ? this.styles.disableForm : {}}
      >
        <div className="my-modal-header">
          <span className="header-title">{tr(`${this.state.titleLabel} Journey`)}</span>
          <div className="close close-button">
            <IconButton
              style={this.styles.closeBtn}
              aria-modal="close"
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
            <TextField name="journey-editor" autoFocus={true} className="hiddenTextField" />
          </div>
        </div>
        <div className="pathway-content">
          <div className="common-block">
            <div className="row">
              <div className="small-12 medium-4">
                {!this.state.showMainImage && (
                  <a
                    href="#"
                    aria-label="upload image"
                    className="pathway-banner"
                    style={this.styles.journeyBanner}
                    onClick={e => this.journeyBannerFileStackHandler(e, false)}
                  >
                    <div className="text-center">
                      <ImagePhotoCamera style={this.styles.mainImage} />
                      <div className="empty-image">{tr('Cover Image')}</div>
                    </div>
                  </a>
                )}
                {this.state.showMainImage && (
                  <a
                    href="#"
                    onClick={e => this.journeyBannerFileStackHandler(e, false)}
                    className="pathway-banner preview-upload"
                  >
                    <div
                      className="card-img-container"
                      onMouseEnter={() => {
                        this.setState({ mainImage: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ mainImage: false });
                      }}
                    >
                      <div
                        className="card-img button-icon"
                        style={{
                          ...this.styles.journeyImage,
                          ...{
                            backgroundImage: `url(\'${getResizedUrl(
                              this.securedUrl || this.state.fileStack[0].url,
                              'height:183'
                            )}\')`
                          }
                        }}
                      >
                        {this.hoverBlock(this.fileStackHandler.bind(this, false))}
                      </div>
                    </div>
                  </a>
                )}
              </div>
              <div className="small-12 medium-8">
                <div className="input-block">
                  <div className="row">
                    {!this.state.useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">
                          {tr('Journey Title')}
                          <span>*</span>
                        </div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      <TextFieldCustom
                        hintText={tr('Start typing a title for your journey')}
                        value={this.state.mainJourney.title}
                        inputChangeHandler={this.journeyMainChange.bind(this, 'title')}
                        errorText={this.state.titleErrorText}
                        rowsMax={4}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                    </div>
                  </div>
                </div>
                <div className="input-block">
                  <div className="row">
                    {!this.state.useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label">
                        <div className="pathway-label">{tr('Description')}</div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      <TextFieldCustom
                        hintText={tr('Add title/description (optional)')}
                        value={this.state.mainJourney.message}
                        inputChangeHandler={this.journeyMainChange.bind(this, 'message')}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                    </div>
                  </div>
                </div>
                {(!this.state.shareOnSmartCardCreation || !this.state.isAbleToMarkPrivate) &&
                  this.state.activeType !== 'smart' &&
                  this.state.activeType !== 'bookmark' && (
                    <div className="input-block">
                      <div className="row">
                        {!this.state.useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label">
                            <div className="pathway-label">{tr('Post to')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <SelectField
                            hintText={tr('Channel')}
                            cardType="smartbite"
                            aria-label="select channel"
                            onChange={this.getNewChannels}
                            fullWidth={false}
                            currentChannels={this.state.selectedChannels}
                            nonCuratedChannelIds={this.state.nonCuratedChannelIds}
                            cardChannel={(this.props.journey && this.props.journey.channels) || []}
                          />
                        </div>
                        <div style={this.styles.privateChannelStyle}>
                          {this.state.showPostToChannelMsg && (
                            <div style={{ margin: '10px 0 0 155px' }}>
                              <ActionInfo style={this.styles.privateChannelIconStyle} />
                              <p style={{ verticalAlign: 'middle', margin: '0 0 0 15px' }}>
                                {tr('Posting to private channel will not make card private.')}
                              </p>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                {this.showBIA && (
                  <div className="input-block">
                    <div className="row">
                      {!this.state.useFormLabels && (
                        <div className="small-12 medium-4 large-3 align-label">
                          <div className="pathway-label">{tr('Level')}</div>
                        </div>
                      )}
                      <div className="small-12 medium-8 large-9">
                        <div className="bia-block">
                          <RadioButtonGroup
                            name="bia-radio"
                            style={{ display: 'flex' }}
                            valueSelected={this.state.selectedBia}
                            onChange={this.changeBia}
                          >
                            <RadioButton
                              value="beginner"
                              label={tr('Beginner')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia != 'beginner'
                                  ? this.styles.biaRadio
                                  : { marginRight: '5px' }
                              }
                              aria-label="beginner"
                            />
                            <RadioButton
                              value="intermediate"
                              label={tr('Intermediate')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia != 'intermediate'
                                  ? this.styles.biaRadio
                                  : { marginRight: '5px' }
                              }
                              aria-label="intermediate"
                            />
                            <RadioButton
                              value="advanced"
                              label={tr('Advanced')}
                              className="bia-radio"
                              style={{ width: 'auto' }}
                              iconStyle={
                                this.state.selectedBia != 'advanced'
                                  ? this.styles.biaRadio
                                  : { marginRight: '5px' }
                              }
                              aria-label="advanced"
                            />
                          </RadioButtonGroup>
                          {!this.props.journey && this.state.selectedBia && (
                            <button className="my-button" onClick={this.cleanLevel}>
                              {tr('Clear level')}
                            </button>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                )}

                <div className="input-block">
                  <div className="row">
                    {!this.state.useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label-top">
                        <div className="pathway-label pathway-label__tag">{tr('Tag')}</div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      {!!this.state.topics.length && (
                        <div className="text-block chip-tag-pathway">
                          <div style={this.styles.chipsWrapper}>
                            {this.state.topics.map((name, idx) => {
                              return (
                                <div className="chip-tag-pathway-item" key={name + idx}>
                                  <Chip
                                    style={this.styles.chip}
                                    className="channel-item-insight"
                                    backgroundColor={colors.white}
                                    labelColor={colors.primary}
                                    labelStyle={this.styles.labelChip}
                                  >
                                    {' '}
                                    {name}
                                    <CloseIcon
                                      onClick={this.removeTag.bind(this, name)}
                                      style={this.styles.chipClose}
                                      aria-label="cancel"
                                    />
                                  </Chip>
                                </div>
                              );
                            })}
                          </div>
                        </div>
                      )}
                      <TextFieldCustom
                        hintText={tr('Add Tags/Keywords')}
                        clearDown={!this.state.maxTagErrorText.length}
                        onKeyDown={!this.state.maxTagErrorText.length && this.addTagHandler}
                        inputChangeHandler={this.changeTag}
                        fullWidth={true}
                        disabled={this.state.disableForm}
                      />
                      {!this.state.maxTagErrorText.length && (
                        <p style={this.styles.helperText}>
                          {tr('Add tags - e.g. Business, Consulting etc.')}
                        </p>
                      )}
                      <div className="error-text"> {tr(this.state.maxTagErrorText)}</div>
                    </div>
                  </div>
                </div>

                {this.state.shareOnSmartCardCreation &&
                  this.state.isAbleToMarkPrivate &&
                  this.state.activeType !== 'smart' &&
                  this.state.activeType !== 'bookmark' && (
                    <div className="input-block">
                      <div className="row" style={{ marginBottom: '10px' }}>
                        {!this.state.useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label-input-level">
                            <div className="pathway-label">{tr('Mark as Private')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <Checkbox
                            style={this.styles.privateContent}
                            checked={this.state.isPrivateContent}
                            onCheck={this.handlePrivateContent.bind(this)}
                            aria-label="Mark as Private"
                          />
                        </div>
                      </div>
                      <div className="row">
                        {!this.state.useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label-top">
                            <div className="pathway-label">{tr('Share')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <CardSharingSection
                            tags={this.state.tags}
                            tagSuggestions={tagSuggestions}
                            handleAddition={this.handleAddition.bind(this)}
                            handleDelete={this.handleDelete.bind(this)}
                            handleInputChange={this.handleInputChange.bind(this)}
                            pathwayPart={false}
                            styles={this.styles}
                            showPrivateChannelMsg={this.state.showPrivateChannelMsg}
                            privateChannelMsg={this.state.privateChannelMsg}
                            groupsSuggestions={this.state.groupsSuggestions}
                            channelsSuggestions={this.state.channelsSuggestions}
                            checkToShare={this.checkToShare}
                          />
                        </div>
                      </div>
                    </div>
                  )}

                {this.enableBadge && !!this.state.badges.length && (
                  <div className="input-block">
                    <div className="row">
                      {!this.state.useFormLabels && (
                        <div className="small-12 medium-4 large-3 align-label-top">
                          <div className="pathway-label">{tr('Badge')}</div>
                        </div>
                      )}
                      <div className="small-12 medium-8 large-9">
                        <div style={this.styles.flexBlock}>
                          {showCheckbox && (
                            <Checkbox
                              labelStyle={this.styles.labelCheckbox}
                              onCheck={this.openBadgeOption}
                              checked={this.state.badgeOn}
                              className="checkbox"
                              style={this.styles.checkbox}
                            />
                          )}
                          <div style={this.styles.labelBlock}>
                            <div onClick={this.openBadgeOption} style={this.styles.labelCheckbox}>
                              {showCheckbox
                                ? tr('Upon completing this journey, assignee will get a badge.')
                                : ''}
                              <a
                                href="#"
                                aria-label="edit"
                                style={this.styles.labelEditBlock}
                                onClick={this.triggerChangeBadge}
                              >
                                <EditIcon
                                  color={this.state.badgeOn ? '#6f708b' : '#d0d0d9'}
                                  style={this.styles.labelIcon}
                                />
                                <span
                                  style={{
                                    ...this.styles.labelEdit,
                                    ...{ color: this.state.badgeOn ? '#6f708b' : '#d0d0d9' }
                                  }}
                                >
                                  {' '}
                                  {tr('Edit')}
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        {this.state.openBadgeOptions ? (
                          <div>
                            <div style={this.styles.badgesIconBlock}>
                              {!!this.state.badges &&
                                this.state.badges.map(badge => {
                                  let selected =
                                    (badge.id && badge.id === this.state.currentBadge.id) ||
                                    (badge.imageUrl &&
                                      badge.imageUrl === this.state.currentBadge.imageUrl)
                                      ? this.styles.selectedIcon
                                      : '';
                                  return (
                                    <div>
                                      <a href="#" onClick={e => this.selectIconBadge(e, badge)}>
                                        <img
                                          className="card-view-icon"
                                          style={{ ...this.styles.badgeIcon, ...selected }}
                                          src={badge.imageUrl}
                                        />
                                      </a>
                                    </div>
                                  );
                                })}
                            </div>
                            <div style={this.styles.badgeText}>
                              <TextFieldCustom
                                hintText={tr('Enter a Badge name')}
                                value={this.state.currentBadge.text}
                                inputChangeHandler={this.addBadgeHandler}
                                fullWidth={true}
                                disabled={this.state.disableForm}
                              />
                              {this.state.showBadgeWarning && (
                                <div className="text-center error-text">
                                  {' '}
                                  {tr('Cannot enter more than 25 characters')}
                                </div>
                              )}
                            </div>
                            <div className="action-buttons">
                              <FlatButton
                                label={tr('Cancel')}
                                className="close"
                                style={this.styles.cancel}
                                labelStyle={this.styles.actionBtnLabel}
                                onTouchTap={this.revertBadgeChanges}
                              />

                              <FlatButton
                                label={tr('Done')}
                                className="preview"
                                disabled={
                                  !this.state.currentBadge.text ||
                                  !this.state.currentBadge.imageUrl ||
                                  this.state.showBadgeWarning
                                }
                                onTouchTap={this.applyBadgeChanges}
                                labelStyle={this.styles.actionBtnLabel}
                                style={
                                  !this.state.currentBadge.text ||
                                  !this.state.currentBadge.id ||
                                  this.state.showBadgeWarning
                                    ? this.styles.disabled
                                    : this.styles.create
                                }
                              />
                            </div>
                          </div>
                        ) : (
                          !!this.state.currentBadge.imageUrl &&
                          this.state.badgeOn && (
                            <div style={this.styles.currentBadge}>
                              <div>
                                <img
                                  className="card-view-icon"
                                  style={this.styles.badgeIcon}
                                  src={this.state.currentBadge.imageUrl}
                                />
                              </div>
                              <div
                                style={
                                  this.state.currentBadge.text.length > 6
                                    ? this.styles.badgeLabel
                                    : this.styles.badgeLabelCenter
                                }
                              >
                                {this.state.currentBadge.text}
                              </div>
                            </div>
                          )
                        )}
                      </div>
                    </div>
                  </div>
                )}
                {this.isCompleteField &&
                  this.completeMethodConf &&
                  this.completeMethodConf === 'creatorChoose' && (
                    <div className="input-block">
                      <div className="row">
                        {!this.state.useFormLabels && (
                          <div className="small-12 medium-4 large-3 align-label">
                            <div className="pathway-label">{tr('Mark as complete')}</div>
                          </div>
                        )}
                        <div className="small-12 medium-8 large-9">
                          <div className="complete-method-block">
                            <RadioButtonGroup
                              name="completeMethod"
                              style={this.styles.flexBlock}
                              valueSelected={this.state.completeMethod}
                              onChange={this.changeCompleteMethod.bind(this)}
                            >
                              <RadioButton
                                value="manually"
                                label={tr('Manually')}
                                className="complete-radio"
                                style={this.styles.radioBtn}
                                iconStyle={
                                  this.state.completeMethod !== 'manually'
                                    ? this.styles.iconRadio
                                    : this.styles.mr5
                                }
                                aria-label={tr('manually')}
                              />
                              <RadioButton
                                value="automatically"
                                label={tr('Automatically')}
                                className="complete-radio"
                                style={this.styles.radioBtn}
                                iconStyle={
                                  this.state.completeMethod !== 'automatically'
                                    ? this.styles.iconRadio
                                    : this.styles.mr5
                                }
                                aria-label={tr('automatically')}
                              />
                            </RadioButtonGroup>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                <div className="input-block">
                  <div className="row">
                    {!this.state.useFormLabels && (
                      <div className="small-12 medium-4 large-3 align-label-top">
                        <div className="pathway-label pathway-label_journey-type">
                          {tr('Choose type for Journey')}
                        </div>
                      </div>
                    )}
                    <div className="small-12 medium-8 large-9">
                      <RadioButtonGroup
                        className="journey-radiobutton-group"
                        name="journeyType"
                        defaultSelected={
                          this.state.mainJourney.journeyType
                            ? this.state.mainJourney.journeyType
                            : 'self_paced'
                        }
                        onChange={this.changeJourneyTypeHandler}
                      >
                        <RadioButton
                          disabled={this.props.journey && !!this.props.journey.id}
                          value="self_paced"
                          label={tr('Self Paced')}
                          style={this.styles.radioButton}
                          aria-label="type of journey, Self Paced"
                        />
                        <RadioButton
                          disabled={this.props.journey && !!this.props.journey.id}
                          value="weekly"
                          labelStyle={{ zIndex: 3 }}
                          iconStyle={{ alignSelf: 'center' }}
                          label={
                            <select
                              disabled={
                                this.state.mainJourney.journeyType === 'self_paced' ||
                                (this.props.journey && !!this.props.journey.id)
                              }
                              className="journey-weeks-dropdown"
                              onChange={this.changeWeeksHandler}
                              value={this.state.weeksValue}
                            >
                              <option value="0">{tr('Select Weeks')}</option>
                              <option value="1">{`1 ${tr('week')}`}</option>
                              {_.times(11, i => {
                                return (
                                  <option key={`option-${i + 2}`} value={i + 2}>{`${i + 2} ${tr(
                                    'weeks'
                                  )}`}</option>
                                );
                              })}
                            </select>
                          }
                          aria-label="type of journey, instructor- paced"
                          style={{ ...this.styles.radioButton, ...this.styles.bigRadioButton }}
                        />
                      </RadioButtonGroup>
                    </div>
                  </div>
                </div>
                {this.state.mainJourney.journeyType === 'weekly' && (
                  <div className="input-block">
                    <div className="row">
                      {!this.state.useFormLabels && (
                        <div className="small-12 medium-4 large-3 align-label">
                          <div className="pathway-label">{tr('Start Date')}</div>
                        </div>
                      )}
                      <div className="small-12 medium-8 large-9" style={{ position: 'relative' }}>
                        <input
                          disabled={this.props.journey && !!this.props.journey.id}
                          className="journey-weeks-dropdown"
                          value={
                            this.state.startDate
                              ? moment(+this.state.startDate).format('DD/MM/YYYY')
                              : ''
                          }
                          readOnly={true}
                          onClick={() => {
                            this.setState({ calendarOpen: !this.state.calendarOpen });
                          }}
                          placeholder={tr('Select a Date')}
                        />
                        <div style={this.styles.calendar}>
                          {this.state.calendarOpen && (
                            <div>
                              <Calendar
                                container="inline"
                                minDate={moment()}
                                textFieldStyle={this.styles.startDate}
                                date={
                                  this.state.startDate
                                    ? moment(+this.state.startDate).format('DD/MM/YYYY')
                                    : moment()
                                }
                                onChange={this.selectStartDate}
                              />
                              <div className="text-center">
                                <button
                                  className="my-button"
                                  style={{ borderColor: colors.followColor }}
                                  onClick={() => {
                                    this.setState({ calendarOpen: false, startDate: undefined });
                                  }}
                                >
                                  {tr('Clear date')}
                                </button>
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                <div>
                  <div className="input-block">
                    <div className="row align-right">
                      <div className="small-12 medium-8 large-9">
                        <a
                          href="#"
                          className="advanced-toggle"
                          onClick={this.toggleAdvancedSettings}
                        >
                          {tr('Advanced Settings')}
                        </a>
                      </div>
                    </div>
                  </div>
                  {this.state.showAdvancedSettings && (
                    <div>
                      {this.restrictToFlag &&
                        this.state.shareOnSmartCardCreation &&
                        this.state.isAbleToMarkPrivate && (
                          <div className="input-block">
                            <div className="row">
                              {!this.state.useFormLabels && (
                                <div className="small-12 medium-4 large-3">
                                  <div className="pathway-label">{tr('Restricted to')}</div>
                                </div>
                              )}

                              {!restrictAble && (
                                <div className="small-12 medium-8 large-8">
                                  <TextFieldCustom
                                    hintText={`${tr('Restrict content with @user, @group')}`}
                                    errorText={restrictToError}
                                    errorStyle={this.styles.restrictToErrorStyle}
                                    fullWidth={true}
                                    disabled={true}
                                  />
                                </div>
                              )}
                              {restrictAble && (
                                <div className="small-12 medium-8 large-8">
                                  <ReactTags
                                    tags={this.state.restrictedToUsers}
                                    suggestions={this.state.userOrTeamSuggestion}
                                    handleAddition={this.handleUserAddition.bind(this)}
                                    handleDelete={this.handleUserDelete.bind(this)}
                                    placeholder={
                                      !!this.state.restrictedToUsers.length > 0
                                        ? ''
                                        : tr('Restrict content with @user, @group')
                                    }
                                    tagComponent={this.selectedRestrictedToTags.bind(this)}
                                    handleInputChange={this.handleRestrictedInputChange.bind(this)}
                                  />
                                </div>
                              )}
                            </div>
                          </div>
                        )}
                      {this.state.multilingualContent && (
                        <div className="input-block">
                          <MultilingualContent
                            labelContainerClasses="small-12 medium-4 large-3 align-label"
                            labelClasses="pathway-label"
                            inputContainerClasses="small-12 medium-6 large-6"
                            handleLanguageChange={this.handleLanguageChange}
                            language={this.state.language}
                          />
                        </div>
                      )}
                      {isCurator && (
                        <div className="input-block">
                          <div className="row">
                            {!this.state.useFormLabels && (
                              <div
                                style={this.state.fileStackProvider ? this.styles.labelTop : {}}
                                className="small-12 medium-4 large-3 align-label"
                              >
                                <div className="pathway-label">{tr('Provider')}</div>
                              </div>
                            )}
                            <div className="small-12 medium-6 large-6">
                              <TextFieldCustom
                                hintText={tr('Enter Provider Name')}
                                value={this.state.advSettings.provider}
                                inputChangeHandler={this.advSettingsChange.bind(this, 'provider')}
                                fullWidth={true}
                                disabled={this.state.disableForm}
                              />
                            </div>
                            <div className="small-12 medium-2 large-3">
                              <FlatButton
                                label={tr('Image')}
                                className="preview"
                                onTouchTap={this.fileStackHandler.bind(this, true)}
                                labelStyle={this.styles.actionBtnLabel}
                                style={this.styles.providerBtnImage}
                              />
                            </div>
                          </div>
                          {this.state.fileStackProvider && (
                            <div className="row align-right">
                              <div className="small-12 medium-8 large-9">
                                <div style={this.styles.providerImageContainer}>
                                  <div className="close close-button provider-image-btn">
                                    <IconButton
                                      style={this.styles.deleteProviderImageBtn}
                                      iconStyle={this.styles.deleteProviderImageBtnIcon}
                                      onTouchTap={this.clearFileStackProviderInfo}
                                      aria-label="clear"
                                    >
                                      <CloseIcon color="white" viewBox="0 0 24 24" />
                                    </IconButton>
                                  </div>
                                  <img
                                    className="card-view-icon"
                                    style={this.styles.providerIcon}
                                    src={
                                      this.state.securedProviderUrl || this.state.fileStackProvider
                                    }
                                    alt=""
                                  />
                                </div>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                      <div className="input-block">
                        <div className="row">
                          {!this.state.useFormLabels && (
                            <div className="small-12 medium-4 large-3 align-label">
                              <div className="pathway-label">{tr('Duration')}</div>
                            </div>
                          )}
                          <div className="small-12 medium-6 large-6">
                            <TextFieldCustom
                              hintText={
                                this.state.durationString
                                  ? this.state.durationString
                                  : tr('Enter time (MONTH: DAYS : HRS : MINS)')
                              }
                              value={this.state.durationString}
                              inputChangeHandler={this.advSettingsChange.bind(this, 'duration')}
                              pipe={'mm dd HH MM'}
                              fullWidth={true}
                              mask={[/\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                              keepCharPositions={true}
                              disabled={this.state.disableForm}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
                {this.state.showVersions && this.state.previousVersions.length > 0 && (
                  <div className="input-block advanced-settings">
                    <div className="row align-right">
                      <div className="small-12 medium-8 large-9">
                        <a
                          href="#"
                          className="advanced-toggle"
                          onClick={this.toggleVersionSettings}
                        >
                          {tr('Previous Versions')}
                        </a>
                      </div>
                    </div>
                    {this.state.showPreviousversionsToggle && (
                      <SmartCardVersions versions={this.state.previousVersions} />
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div style={this.styles.journeyPartWrapper} id="journeyWithHandle">
            {this.state.journeySections &&
              this.state.journeySections.map((section, index) => {
                return (
                  <div key={index}>
                    <JourneyPartEditor
                      key={`section-${section.card_id}`}
                      parentThis={this}
                      sectionIndex={index}
                      section={section}
                      isEdit={this.props.journey}
                      removeBlock={this.removeBlock}
                      deleteBlock={this.deleteBlock}
                      updateSectionCards={this.updateSectionCards}
                      updateSectionTitle={this.updateSectionTitle}
                      isRemove={true}
                      lockCard={this.lockCard}
                      addToLeap={this.addToLeap}
                      arrToLeap={section.arrayToLeap || []}
                      dontEdit={
                        this.props.journey &&
                        this.props.journey.id &&
                        this.state.journeySections[index].start_date &&
                        this.props.journey.publishedAt
                          ? !this.isWeekly(index)
                          : false
                      }
                    />
                  </div>
                );
              })}
          </div>
          <div>
            <FlatButton label={tr('+ Add')} onTouchTap={this.addNewCardsBlock} />
          </div>
          <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
          <div className="action-buttons">
            <FlatButton
              label={tr('Cancel')}
              className="close"
              disabled={this.state.clicked}
              style={this.styles.cancel}
              labelStyle={this.styles.actionBtnLabel}
              onTouchTap={this.closeModal}
            />
            {this.state.journey.state !== 'published' && (
              <FlatButton
                label={tr(this.state.createLabel)}
                className="saveLater"
                disabled={this.disablePublish()}
                onTouchTap={this.createClickHandler}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.cancel}
              />
            )}
            {this.enablePreview && (
              <FlatButton
                label={tr(this.state.previewLabel)}
                className="preview"
                disabled={this.disablePublish()}
                onTouchTap={this.previewClickHandler}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.create}
              />
            )}
            {this.state.viewPublishButton && (
              <FlatButton
                label={tr(this.state.publishLabel)}
                className="publish"
                disabled={this.disablePublish()}
                onTouchTap={this.publishClickHandler.bind(
                  this,
                  this.props.card ? this.props.card.id : null
                )}
                labelStyle={this.styles.actionBtnLabel}
                style={this.disablePublish() ? this.styles.disabled : this.styles.create}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

JourneyEditor.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  reorderCardIds: PropTypes.array,
  pathname: PropTypes.string,
  card: PropTypes.object,
  journey: PropTypes.object,
  mainJourney: PropTypes.object,
  cardUpdated: PropTypes.func,
  isPreviewMode: PropTypes.bool,
  channelsV2: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS(),
    reorderCardIds: state.journey.toJS().reorderCardIds,
    isPreviewMode: state.journey.toJS().isPreviewMode || false,
    channelsV2: state.channelsV2.toJS()
  };
}
export default connect(mapStoreStateToProps)(JourneyEditor);
