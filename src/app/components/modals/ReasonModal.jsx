import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import TextField from 'material-ui/TextField';
import { reportCard } from 'edc-web-sdk/requests/cards.v2';
import { reportComment } from 'edc-web-sdk/requests/comments';
//actions
import { close } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { updateReportedCards, updateReportedComments } from '../../actions/usersActions';

class ReasonModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      buttonPending: false,
      disabled: true,
      reason: '',
      reasonText: '',
      disableTextField: true
    };
    this.style = {
      inputStyle: {
        marginRight: '0.5rem',
        width: '1rem'
      },
      labelStyle: {
        fontSize: '0.85rem',
        color: '#555555',
        fontWeight: 400
      },
      writeReason: {
        fontSize: '0.75rem'
      },
      chackBoxStyle: {
        width: '0.8rem',
        marginRight: '0.8rem',
        color: '#555555'
      },
      hintStyle: {
        fontSize: '0.8rem',
        color: '#555555'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  reportCard = () => {
    let reason = this.state.reason === 'Something else' ? this.state.reasonText : this.state.reason;
    let payload = {
      card_id: this.props.card.id,
      reason
    };
    this.setState(() => ({ buttonPending: true }));
    reportCard(payload)
      .then(report => {
        this.props.dispatch(updateReportedCards(this.props.card.id));
        this.setState({ buttonPending: false });
        this.closeModal();
        this.props.dispatch(
          openSnackBar(
            'Thank you for reporting the SmartCard. We will evaluate your feedback and make necessary changes.',
            true
          )
        );
      })
      .catch(err => {
        this.setState({ buttonPending: false });
        this.closeModal();
        this.props.dispatch(openSnackBar(err.message, true));
      });
  };

  reportComment = () => {
    let reason = this.state.reason === 'Something else' ? this.state.reasonText : this.state.reason;
    let payload = {
      comment_id: this.props.comment.id,
      reason: reason
    };
    reportComment(payload)
      .then(report => {
        this.props.dispatch(updateReportedComments(this.props.comment.id));
        this.setState({ buttonPending: false });
        this.closeModal();
        this.props.dispatch(
          openSnackBar(
            'Thank you for reporting the comment. We will evaluate your feedback and make necessary changes.',
            true
          )
        );
      })
      .catch(err => {
        this.setState({ buttonPending: false });
        this.closeModal();
        this.props.dispatch(openSnackBar(err.message, true));
      });
  };

  submitReason = () => {
    this.setState({ buttonPending: true });
    if (!this.props.card) {
      this.reportComment();
    } else {
      this.reportCard();
    }
  };

  radioCheckHandler = (e, value) => {
    this.setState({
      reason: value
    });
  };

  setSomeThingElseReason = event => {
    this.setState({
      reasonText: event.target.value
    });
  };

  disabledTextHandler = (e, reason) => {
    e.stopPropagation();
    if (reason.key === 'Something else') {
      this.setState({
        disableTextField: false
      });
    } else {
      this.setState({
        disableTextField: true,
        reasonText: ''
      });
    }
  };

  render() {
    let isCard = !!this.props.card;
    let displayName = isCard ? 'content' : 'comment';
    let reasons = [
      {
        key: 'technical-issue',
        value: 'technical-issue',
        label: 'Technical issues with opening the content'
      },
      {
        key: 'Spam',
        value: 'Spam',
        label: 'I think it’s fake, spam, or scam'
      },
      {
        key: 'Inappropriate',
        value: 'Inappropriate',
        label: 'I think it’s inappropriate ' + displayName
      }
    ];
    if (isCard) {
      reasons.push({
        key: 'Outdated',
        value: 'Outdated',
        label: 'I think the content is outdated'
      });
    }
    let submitButtonDisabled = true;
    if (
      (this.state.reason.length > 0 && this.state.reason !== 'Something else') ||
      this.state.reasonText.length > 0
    ) {
      submitButtonDisabled = false;
    }
    return (
      <div className="reason-modal">
        <div className="reason-header">
          <span className="header-title">{tr('Reason to Report')}</span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div>
          <div className="reason-sub-title">Help us understand the issue</div>
          <RadioButtonGroup
            key="radio-button-group"
            name="poll-radio-button-group"
            className="vertical-spacing-medium row"
            valueSelected={this.state.reason}
            onChange={(e, value) => {
              this.radioCheckHandler(e, value);
            }}
          >
            {reasons.map(reason => {
              return (
                <RadioButton
                  key={reason.key}
                  value={reason.value}
                  disabled={false}
                  className="poll-radio"
                  inputStyle={this.style.inputStyle}
                  labelStyle={this.style.labelStyle}
                  iconStyle={this.style.chackBoxStyle}
                  uncheckedIcon={this.style.chackBoxStyle}
                  label={tr(reason.label)}
                  aria-label={tr(reason.label)}
                  onClick={e => {
                    this.disabledTextHandler(e, reason);
                  }}
                />
              );
            })}
          </RadioButtonGroup>
          <div className="another-rsn-title">
            {tr('Did not find your reason? Tell us what is wrong with this content')}
          </div>
          {
            <TextField
              hintText={tr('Write the reason, please (max. 100 character)')}
              hintStyle={this.style.hintStyle}
              aria-label={tr('Write the reason, please')}
              value={this.state.reasonText}
              type="text"
              fullWidth={true}
              maxLength="100"
              multiLine={true}
              rows={2}
              onChange={this.setSomeThingElseReason}
              textareaStyle={this.style.writeReason}
              onFocus={() => this.setState(() => ({ reason: 'Something else' }))}
            />
          }
          <div className="btn-block text-right">
            <PrimaryButton
              className="remove-card"
              label={tr('Submit')}
              pending={this.state.buttonPending}
              pendingLabel={tr('Submitting...')}
              onTouchTap={this.submitReason}
              disabled={submitButtonDisabled}
            />
          </div>
        </div>
      </div>
    );
  }
}

ReasonModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  group: PropTypes.object,
  callback: PropTypes.func,
  title: PropTypes.string,
  card: PropTypes.object,
  comment: PropTypes.object
};

export default connect()(ReasonModal);
