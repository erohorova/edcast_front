import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { updateChannelDetails } from '../../actions/channelsActionsV2';
import { close } from '../../actions/modalActions';
import colors from 'edc-web-sdk/components/colors/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class UpdateChannelDetailsModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      id: this.props.id,
      label: this.props.label,
      description: this.props.description,
      labelChanged: false
    };
    this.styles = {
      updateChannelDetails: {
        width: '100%',
        border: '1px solid #d6d6e1',
        borderRadius: '2px',
        fontSize: '13px',
        color: '#6f708b'
      },
      updateChannelTitle: {
        padding: '6px 9px',
        height: '28px'
      },
      updateChannelDescription: {
        padding: '11px 40px 13px 9px',
        outline: 'none',
        lineHeight: '16px',
        height: '56px'
      },
      maxChar: {
        fontFamily: 'Open Sans',
        fontSize: '13px',
        color: '#d0011b',
        width: '100%',
        textAlign: 'right',
        padding: '4px 0px',
        fontWeight: '300'
      },
      updateChannelHeading: {
        color: '#fff',
        width: '100%',
        padding: '3px 0px 12px 0px'
      },
      closeButton: {
        position: 'absolute',
        top: '-10px',
        right: '-15px'
      },
      updateChannelDetailsContainer: {
        background: '#fff',
        padding: '16px 22px 14px 16px'
      },
      updateButton: {
        background: colors.purple,
        border: '1px solid #9276b5',
        color: colors.white
      }
    };

    this.handleClickUpdate = this.handleClickUpdate.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeDescription = this.changeDescription.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  changeTitle(e) {
    this.setState({ label: e.target.value });
    this.setState({ labelChanged: true });
  }

  changeDescription(e) {
    this.setState({ description: e.target.value });
  }

  handleClickUpdate() {
    let channel = {
      id: this.state.id,
      label: this.state.label,
      description: this.state.description
    };

    let labelChanged = this.state.labelChanged;

    this.props
      .dispatch(updateChannelDetails(channel, labelChanged))
      .then(() => {
        this.props.dispatch(close());
      })
      .catch(err => {
        console.error(`Error in UpdateChannelDetailsModal.updateChannelDetails.func : ${err}`);
      });
  }

  closeModal() {
    this.props.dispatch(close());
  }
  render() {
    return (
      <div>
        <div style={this.styles.updateChannelHeading}>
          <div style={{ fontSize: '18px' }}>{tr('Edit Title & Description')}</div>
          <div style={this.styles.closeButton}>
            <IconButton aria-label="close" onTouchTap={this.closeModal}>
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>

        <div style={this.styles.updateChannelDetailsContainer}>
          <div className="row">
            <TextField name="updatetochannel" autoFocus={true} className="hiddenTextField" />
            <div
              className="large-2"
              style={{ padding: '6px 0px 0px', color: '#454560', fontSize: '14px' }}
            >
              Title
            </div>
            <div className="large-10">
              <input
                aria-label={tr('Title')}
                type="text"
                style={{ ...this.styles.updateChannelTitle, ...this.styles.updateChannelDetails }}
                value={this.state.label}
                onChange={this.changeTitle}
                maxLength="150"
              />
            </div>
          </div>

          <div className="row">
            <p style={this.styles.maxChar}>{`(${tr('Max characters')}=150)`}</p>
          </div>

          <div
            className="row"
            style={{ padding: '28px 0px 0px', color: '#454560', fontSize: '14px' }}
          >
            <div
              className="large-2"
              style={{ padding: '6px 0px 0px', color: '#454560', fontSize: '14px' }}
            >
              {tr('Description')}
            </div>
            <div className="large-10">
              <textarea
                aria-label={tr('Description')}
                style={{
                  ...this.styles.updateChannelDescription,
                  ...this.styles.updateChannelDetails
                }}
                value={this.state.description}
                onChange={this.changeDescription}
                maxLength="2000"
              />
            </div>
          </div>

          <div className="row">
            <p style={this.styles.maxChar}>{`(${tr('Max characters')}=2000)`}</p>
          </div>

          <div style={{ padding: '8px' }} className="clearfix">
            <PrimaryButton
              label={tr('Update')}
              className="float-right create"
              pendingLabel={tr('Updating...')}
              onTouchTap={this.handleClickUpdate}
              style={this.styles.updateButton}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS()
  };
}

UpdateChannelDetailsModal.propTypes = {
  id: PropTypes.number,
  label: PropTypes.string,
  description: PropTypes.string
};

export default connect(mapStoreStateToProps)(UpdateChannelDetailsModal);
