import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import IconButton from 'material-ui/IconButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import CircularProgress from 'material-ui/CircularProgress';
import { tr } from 'edc-web-sdk/helpers/translations';
import { channelsv2 } from 'edc-web-sdk/requests';
import * as usersSdk from 'edc-web-sdk/requests/users.v2';
import Channel from '../discovery/Channel.jsx';

class UsersChannelsListModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userChannels: this.props.userChannels,
      pending: false,
      badgesCount: 0,
      buttonPending: false,
      loading: false,
      offset: this.props.userChannels.length
    };

    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      tableScroll: {
        maxHeight: '500px',
        overflowY: 'auto'
      }
    };
  }

  viewMoreClickHandler = () => {
    this.setState({ loading: true });
    let offset = this.state.offset + 16;
    channelsv2
      .getFollowingChannels(16, this.state.offset)
      .then(data => {
        let userChannels = this.state.userChannels;
        userChannels.push(...data);
        this.setState({ userChannels, loading: false, offset });
      })
      .catch(err => {
        console.error(`Error in UsersChannelsListModal.getFollowingChannels.func : ${err}`);
      });
  };

  closeModal = () => {
    this.props.closeModal();
  };

  render() {
    return (
      <div className="pathway-badges-modal">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="modal">
          <div className="modal-content modal-content-channel">
            <div className="modal-title">{tr('Channels')}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                onTouchTap={this.closeModal}
                style={this.styles.closeBtn}
              >
                <CloseIcon color="#fff" />
              </IconButton>
            </div>
            <div className="container">
              <div className="row">
                {this.state.userChannels.map(channel => {
                  return (
                    <div key={channel.id} className="column custom-column small-3">
                      <div className="channel-component">
                        <Channel
                          channel={channel}
                          id={channel.id}
                          isFollowing={channel.isFollowing}
                          title={channel.label}
                          imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
                          slug={channel.slug}
                          allowFollow={channel.allowFollow}
                        />
                      </div>
                    </div>
                  );
                })}
                <div className="small-12 columns text-center">
                  {this.props.channelsCount > this.state.userChannels.length && (
                    <button className="view-more-v2" onClick={this.viewMoreClickHandler}>
                      {this.state.loading ? tr('Loading...') : tr('View More')}
                    </button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

UsersChannelsListModal.propTypes = {
  closeModal: PropTypes.func,
  userChannels: PropTypes.array,
  channelsCount: PropTypes.number
};

export default connect(mapStoreStateToProps)(UsersChannelsListModal);
