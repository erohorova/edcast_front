import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors/index';
import { fetchCardStats, fetchCardStatsUsers } from 'edc-web-sdk/requests/cards';
import StatsListItem from '../common/StatsListItem';
import { close } from '../../actions/modalActions';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

class CardStatsModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: true,
      cardStats: {},
      cardStatsUsers: {},
      card: this.props.card
    };

    this.cancelClickHandler = this.cancelClickHandler.bind(this);
  }

  componentDidMount() {
    fetchCardStats(this.props.card.cardId || this.props.card.id)
      .then(cardStats => {
        this.setState({
          cardStats: cardStats
        });
      })
      .catch(err => {
        console.error(`Error in CardStatsModal.fetchCardStats.func: ${err}`);
      });
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  render() {
    let cardSTATS = this.state.cardStats;
    return (
      <div className="cardStats">
        <div
          style={{
            background: '#d6d6e1',
            paddingLeft: '24px',
            paddingRight: '13px',
            paddingTop: '14px',
            paddingBottom: '14px'
          }}
        >
          {!this.props.isViewsModal && tr('Users Activity')}
          <span
            className={this.props.isViewsModal ? 'float-right close-icon-btn' : 'float-right'}
            style={{ height: '22px', cursor: 'pointer', display: 'inline-block' }}
          >
            <NavigationClose color={colors.grey} onTouchTap={this.cancelClickHandler} />
          </span>
        </div>
        <div className="vertical-spacing-large">
          {this.state.cardStats && (
            <div style={{ paddingLeft: '24px', paddingRight: '24px', paddingBottom: '5px' }}>
              {Object.keys(this.state.cardStats).map((stats, index) => (
                <StatsListItem
                  key={index}
                  statsObject={stats}
                  value={this.state.cardStats[stats]}
                  cardId={this.props.card.cardId || this.props.card.id}
                  isViewsModal={this.props.isViewsModal}
                />
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}
CardStatsModal.propTypes = {
  open: PropTypes.bool,
  isViewsModal: PropTypes.bool,
  card: PropTypes.object
};

export default connect()(CardStatsModal);
