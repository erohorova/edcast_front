import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import IconButton from 'material-ui/IconButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors/index';
import * as channelsV2Sdk from 'edc-web-sdk/requests/channels.v2';
import * as channelsSdk from 'edc-web-sdk/requests/channels';
import Select from 'react-select';
import TextField from 'material-ui/TextField';

class AddChannelToGroupModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channels: [],
      availableChannels: [],
      alreadyPostedChannels: [],
      channelsToUnpost: [],
      buttonPending: false,

      isAllChannelsLoaded: false,

      isLoadingExternally: false,
      searchChannelText: '',
      options: [],
      channelSuggestionsOffset: 0,
      channelSuggestionsLimit: 10
    };

    this.styles = {
      closeBtn: {
        position: 'absolute',
        right: 0,
        paddingRight: 0,
        width: 'auto'
      },
      checkbox: {
        color: colors.primary
      }
    };
  }

  componentDidMount() {
    this.getChannels(false, this.state.searchChannelText);
  }

  getChannels(isScrollToBottom, input) {
    let isChanged = input !== this.state.searchChannelText;
    this.setState(
      {
        isLoadingExternally: true,
        searchChannelText: input,
        channelSuggestionsOffset:
          isScrollToBottom && !isChanged
            ? this.state.channelSuggestionsOffset + this.state.channelSuggestionsLimit
            : 0
      },
      () => {
        return channelsV2Sdk
          .getAddableChannels(this.props.groupID, {
            limit: this.state.channelSuggestionsLimit || 10,
            offset: this.state.channelSuggestionsOffset || 0,
            q: input || ''
          })
          .then(data => {
            this.isAllChannelsLoaded = data.isLastPage;
            let currentOptions = this.state.options;
            let options = data.channels.map(item => {
              return { value: item.id, label: item.label };
            });
            options = isScrollToBottom ? currentOptions.concat(options) : options;

            this.setState({
              options,
              isLoadingExternally: false
            });
          })
          .catch(err => {
            console.error(`Error in channels.V2.getChannels.func: ${err}`);
          });
      }
    );
  }

  closeModal = () => {
    this.props.closeModal();
  };

  addChannel = () => {
    this.setState({ buttonPending: true }, () => {
      let payload = {
        channel: {
          followers: {
            group_ids: [this.props.groupID]
          }
        }
      };
      channelsSdk
        .addFollowers(this.state.selectedChannel.value, payload)
        .then(() => {
          this.setState({ buttonPending: false }, () => {
            this.closeModal();
            this.props.updateChannelsList();
          });
        })
        .catch(err => {
          console.error(`Error in AddChannelToGroupModal.addFollowers.func : ${err}`);
        });
    });
  };

  handleChannelChange = selectedChannel => {
    this.setState({ selectedChannel });
  };

  clearSearchOptions = () => {
    this.channelId = [];
    this.isAllChannelsLoaded = false;
    this.setState({
      channelSuggestionsOffset: 0,
      options: []
    });
  };

  scrollToBottom = () => {
    if (this.isAllChannelsLoaded) return;
    this.getChannels(true, this.state.searchChannelText);
  };

  render() {
    return (
      <div className="channel-modal">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="modal">
          <div className="modal-title">{tr('Add Channel to Group')}</div>
          <div className="close close-button">
            <IconButton
              onTouchTap={this.closeModal}
              aria-label={tr('close')}
              style={this.styles.closeBtn}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
          <Paper>
            <div className="container-padding journeys-table">
              <TextField name="addtochannel" autoFocus={true} className="hiddenTextField" />
              <Select
                cache={false}
                onChange={this.handleChannelChange.bind(this)}
                value={this.state.selectedChannel}
                onMenuScrollToBottom={this.scrollToBottom}
                placeholder={tr('Select Channel')}
                onFocus={this.getChannels.bind(this, false, '')}
                options={this.state.options}
                onBlur={this.clearSearchOptions}
                isLoading={this.state.isLoadingExternally}
                onInputChange={this.getChannels.bind(this, false)}
                noResultsText={tr(
                  this.state.isLoadingExternally ? 'loading...' : 'No results found'
                )}
                filterOption={() => true}
                clearable={false}
                aria-label="select channel"
              />
              <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                <SecondaryButton
                  label={tr('Cancel')}
                  className="close"
                  onTouchTap={this.closeModal}
                />
                <PrimaryButton
                  label={tr('Add')}
                  className="create"
                  onTouchTap={this.addChannel}
                  pending={this.state.buttonPending}
                  pendingLabel={tr('Saving...')}
                  disabled={!this.state.selectedChannel}
                />
              </div>
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

AddChannelToGroupModal.propTypes = {
  cardId: PropTypes.number,
  currentUser: PropTypes.object,
  card: PropTypes.object,
  closeModal: PropTypes.func,
  updateChannelsList: PropTypes.func,
  groupID: PropTypes.string
};

export default connect(mapStoreStateToProps)(AddChannelToGroupModal);
