import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { close } from '../../actions/modalActions';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import ReactStars from 'react-stars';
import {
  submitRatings,
  flushRelevancyRatingQ,
  updateRatedQueueAfterRating
} from '../../actions/relevancyRatings';
import TextField from 'material-ui/TextField';

class RelevancyRatingModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      submitRatings: false,
      relevancyRate: 0,
      relevancyLevel: ''
    };
    this.styles = {
      paper: {
        padding: '16px'
      }
    };
    this.submitRatings = this.submitRatings.bind(this);
  }

  closeModal = () => {
    this.props.dispatch(flushRelevancyRatingQ());
    this.props.dispatch(close());
  };

  submitRatings = () => {
    let payload = {};
    if (this.state.allowConsumerModifyLevel) {
      payload = {
        rating: this.state.relevancyRate,
        level: this.state.relevancyLevel
      };
    } else {
      payload = {
        rating: this.state.relevancyRate
      };
    }
    submitRatings(this.props.card.id, payload)
      .then(response => {}, this)
      .catch(err => {
        console.error(`Error in RelevancyRatingModal.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.props.card));
    this.closeModal();
  };

  radioCheckHandler = (e, value) => {
    this.setState({
      relevancyLevel: value
    });
  };

  ratingChanged = newRating => {
    this.setState({
      relevancyRate: newRating
    });
  };

  render() {
    return (
      <div className="relevancy-rating-modal">
        <div className="row">
          <TextField name="relevancyrating" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            <div style={{ color: 'white' }}>{tr('Rate Content')}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              {this.state.allowConsumerModifyLevel && (
                <div className="row" style={{ margin: '10px 40px' }}>
                  <div className="small-12 columns">
                    <div style={{ fontWeight: 'bold', marginBottom: '15px' }}>Level</div>
                  </div>
                  <div className="small-12 columns">
                    <RadioButtonGroup
                      key="radio-button-group"
                      name="poll-radio-button-group"
                      className="vertical-spacing-medium row"
                      valueSelected={this.state.relevancyLevel}
                      onChange={(e, value) => {
                        this.radioCheckHandler(e, value);
                      }}
                    >
                      <RadioButton
                        key={'beginner'}
                        aria-label={tr('beginner')}
                        value={'beginner'}
                        disabled={false}
                        style={{ marginRight: '40px', width: 'auto' }}
                        className="poll-radio small-3"
                        label={tr('Beginner')}
                        onClick={e => {
                          e.stopPropagation();
                        }}
                      />
                      <RadioButton
                        key={'intermediate'}
                        aria-label={tr('intermediate')}
                        value={'intermediate'}
                        disabled={false}
                        style={{ marginRight: '40px', width: 'auto' }}
                        className="poll-radio small-3"
                        label={tr('Intermediate')}
                        onClick={e => {
                          e.stopPropagation();
                        }}
                      />
                      <RadioButton
                        key={'advanced'}
                        aria-label={tr('advanced')}
                        value={'advanced'}
                        disabled={false}
                        style={{ marginRight: '40px', width: 'auto' }}
                        className="poll-radio small-3"
                        label={tr('Advanced')}
                        onClick={e => {
                          e.stopPropagation();
                        }}
                      />
                    </RadioButtonGroup>
                  </div>
                </div>
              )}
              <div className="row" style={{ margin: '40px' }}>
                <div className="small-12 columns">
                  <span style={{ fontWeight: 'bold' }}>Relevancy</span>
                </div>
                <div className="small-12 columns">
                  <ReactStars
                    count={5}
                    onChange={this.ratingChanged}
                    size={24}
                    half={false}
                    color1={'#d6d6e1'}
                    color2={'#6f708b'}
                    className="relevancyRatingStars"
                    value={this.state.relevancyRate}
                  />
                  <p style={{ fontSize: '12px' }}>
                    1 Star (Not Relevant) to 5 Stars (Very Relevant){' '}
                  </p>
                </div>
              </div>
              <div className="row">
                <div className="small-12 columns text-center">
                  <div style={{ color: 'white' }}>
                    <SecondaryButton
                      label={tr('Cancel')}
                      className="remove-cancel"
                      onTouchTap={this.closeModal}
                    />
                    <PrimaryButton
                      className="remove-card"
                      label={tr('Submit')}
                      pending={this.state.submitRatings}
                      pendingLabel={tr('Submitting...')}
                      onTouchTap={this.submitRatings}
                      disabled={
                        !(
                          this.state.relevancyRate &&
                          (this.state.relevancyLevel || !this.state.allowConsumerModifyLevel)
                        )
                      }
                    />
                  </div>
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

RelevancyRatingModal.propTypes = {
  card: PropTypes.object
};

export default connect()(RelevancyRatingModal);
