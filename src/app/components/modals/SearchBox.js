import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import findIndex from 'lodash/findIndex';
import some from 'lodash/some';

class SearchBox extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      searchQuery: '',
      selectedSuggestion: [],
      selectedCustomString: [],
      expanded: false,
      suggestions: []
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this._handleClickOutside);
  }

  componentWillReceiveProps(nextProps) {
    let suggestions = nextProps.suggestions;
    this.setState({
      suggestions
    });
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this._handleClickOutside);
  }

  _onChangeCallBack = e => {
    this.setState({
      searchQuery: e.target.value,
      expanded: true
    });
    if (this.state.searchQuery !== e.target.value) this.props.onChangeCallBack(e.target.value);
  };

  _onSelectCallBack = (item, type, isRemove) => {
    let suggestionSelectLimit = this.props.suggestionSelectLimit;
    let selectedResults = {};
    let selectedSuggestion = this.state.selectedSuggestion;
    let selectedCustomString = this.state.selectedCustomString;
    let index = -1;

    if (typeof suggestionSelectLimit === 'undefined' || isRemove || suggestionSelectLimit) {
      switch (type) {
        case 'custom':
          index = selectedCustomString.indexOf(item);
          if (index == -1) {
            selectedCustomString.push(item);
          } else if (isRemove) {
            selectedCustomString.splice(index, 1);
          }
          this.setState({ selectedCustomString });
          break;

        case 'suggested':
          index = findIndex(selectedSuggestion, item);
          if (index > -1) {
            selectedSuggestion.splice(index, 1);
          } else {
            selectedSuggestion.push(item);
          }
          this.setState({ selectedSuggestion });
          break;

        default:
          break;
      }

      selectedResults['selectedSuggestion'] = selectedSuggestion;
      selectedResults['selectedCustomString'] = selectedCustomString;
      selectedResults['currentSelection'] = { item, type };
      this.props.onSelectCallBack(selectedResults);
    } else if (
      this.state.selectedSuggestion.length + this.state.selectedCustomString.length >=
      suggestionSelectLimit
    ) {
      selectedResults['selectedSuggestion'] = selectedSuggestion;
      selectedResults['selectedCustomString'] = selectedCustomString;
      selectedResults['currentSelection'] = { item, type };
      this.props.onlimitExceedCallBack && this.props.onlimitExceedCallBack(selectedResults);
    }
    this.setState({
      expanded: false,
      searchQuery: ''
    });
  };

  _handleClickOutside = e => {
    if (this.currentComponent.contains(e.target)) {
      return;
    } else {
      this.setState({
        expanded: false,
        suggestions: []
      });
    }
  };

  _removeSelectedInterest = (item, type) => {
    this._onSelectCallBack(item, type, true);
  };

  _clearState = (item, type) => {
    if (item && type) {
      let selectedSuggestion = this.state.selectedSuggestion;
      let selectedCustomString = this.state.selectedCustomString;
      let index = -1;
      switch (type) {
        case 'custom':
          index = selectedCustomString.indexOf(item);
          if (index > -1) {
            selectedCustomString.splice(index, 1);
          }
          this.setState({ selectedCustomString });
          break;
        case 'suggested':
          index = findIndex(selectedSuggestion, item);
          if (index > -1) {
            selectedSuggestion.splice(index, 1);
          }
          this.setState({ selectedSuggestion });
          break;
        default:
          break;
      }
    } else {
      this.setState({ selectedCustomString: [], selectedSuggestion: [] });
    }
  };

  render() {
    let suggestions = this.state.suggestions || [];
    let suggestionsLoading = this.props.loading;
    let ignoreInSuggestions = this.props.ignoreInSuggestions;

    return (
      <div
        className="search-box-container"
        ref={instance => {
          this.currentComponent = instance;
        }}
      >
        <input
          type="text"
          className="search-input-box"
          placeholder={
            this.props.placeholderText
              ? this.props.placeholderText
              : 'What would you like to learn about?'
          }
          onChange={e => {
            this._onChangeCallBack(e);
          }}
          onFocus={e => {
            this._onChangeCallBack(e);
          }}
          value={this.state.searchQuery}
        />
        {this.state.expanded && (this.state.searchQuery || suggestions.length > 0) && (
          <div className="search-suggestions">
            {!suggestions.filter(
              suggestion => suggestion.label.toUpperCase() === this.state.searchQuery.toUpperCase()
            ).length &&
              this.props.allowCustomString &&
              this.state.searchQuery && (
                <div className="custom-string-container">
                  <div className="custom-string">{this.state.searchQuery}</div>
                  <button
                    className="add-custom-string"
                    onClick={e => {
                      this._onSelectCallBack(this.state.searchQuery, 'custom');
                    }}
                  >
                    Add
                  </button>
                </div>
              )}

            <div className="suggestions-list-container">
              {!suggestionsLoading && suggestions.length > 0 ? (
                suggestions.map(skill => {
                  let suggestion =
                    this.props.resultFormationCallBack && this.props.resultFormationCallBack(skill);
                  let isChecked = some(this.state.selectedSuggestion, [
                    'topic_name',
                    suggestion.topic_name
                  ]);
                  return (
                    !some(ignoreInSuggestions, ['topic_name', suggestion.topic_name]) && (
                      <div className="suggestions-list" key={suggestion.topic_id + 'suggest'}>
                        {this.props.multiSelect ? (
                          <Checkbox
                            className="suggestion-with-checkbox"
                            label={suggestion.topic_label}
                            checked={isChecked}
                            onCheck={e => {
                              this._onSelectCallBack(suggestion, 'suggested', isChecked);
                            }}
                            labelStyle={{ margin: '5px 0 5px 0', color: '#434455' }}
                            iconStyle={{
                              width: '16px',
                              height: '16px',
                              marginRight: '10px',
                              marginTop: '5px',
                              fill: '#bfbec0'
                            }}
                          />
                        ) : (
                          <div
                            className="suggestion-without-checkbox suggestion-list-without-checkbox"
                            onClick={e => {
                              this._onSelectCallBack(suggestion, 'suggested', false);
                            }}
                          >
                            {suggestion.topic_label}
                          </div>
                        )}
                      </div>
                    )
                  );
                })
              ) : (
                <div className="suggestions-list">
                  <div className="suggestion-without-checkbox">
                    {suggestionsLoading ? 'Searching ...' : 'No Results found'}
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

SearchBox.propTypes = {
  suggestions: PropTypes.arrayOf(PropTypes.object),
  onChangeCallBack: PropTypes.any,
  suggestionSelectLimit: PropTypes.any,
  onSelectCallBack: PropTypes.any,
  onlimitExceedCallBack: PropTypes.any,
  loading: PropTypes.bool,
  ignoreInSuggestions: PropTypes.any,
  allowCustomString: PropTypes.any,
  multiSelect: PropTypes.any,
  resultFormationCallBack: PropTypes.any,
  placeholderText: PropTypes.any
};

export default SearchBox;
