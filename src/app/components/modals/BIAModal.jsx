import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';

class BIAModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      topicLevelObj: this.props.topicLevelObj,
      topic: this.props.topic
    };
    this.styles = {
      biabox: {
        position: 'absolute',
        zIndex: '1',
        padding: '12px 12px 10px 0',
        boxShadow: '0px 1px 4px #000000',
        background: '#ffffff',
        left: '0',
        top: '40px',
        height: '115px',
        fontSize: '12px'
      },
      radioIcon: {
        width: '16px',
        height: '16px',
        borderColor: '#979797',
        color: '#00a1e1',
        marginTop: '1px'
      },
      radioLabel: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '0.8125rem',
        lineHeight: 1.47,
        textAlign: 'left',
        color: '#6f708b',
        width: '100%',
        marginLeft: '1px'
      },
      cancelLink: {
        float: 'left',
        color: '#6f6f8b',
        fontSize: '14px',
        marginLeft: '5px'
      },
      okLink: {
        float: 'right',
        color: '#6f6f8b',
        fontWeight: 'bold',
        fontSize: '14px'
      }
    };
  }

  handleLevelClick = level => {
    let topic = this.props.topic;
    topic.level = level;
    this.setState({ topic });
  };

  componentWillReceiveProps = nextProps => {
    this.setState({ topicLevelObj: nextProps.topicLevelObj, topic: nextProps.topic });
  };

  handleCancelClick = (id, e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.handleCancelClick(id);
  };

  handleOkClick = (topic, e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.updateLevel(topic);
  };

  defaultSelected = topic => {
    let topicID = topic.topic_id || topic.id;
    return this.state.topicLevelObj[topicID];
  };

  render() {
    let _this = this;
    let topic = this.state.topic;
    let topicLevelObj = this.state.topicLevelObj;
    let topicID = topic.topic_id || topic.id;
    return (
      <div style={this.styles.biabox}>
        <div style={{ display: 'none' }}>
          <TextField className="hiddenTextField" autoFocus={true} name="bia" />
        </div>
        {topic && (
          <RadioButtonGroup
            key="radio-button-group"
            name="bia-radio"
            className="vertical-spacing-medium"
            onChange={(e, value) => {
              this.handleLevelClick(value);
            }}
            defaultSelected={this.defaultSelected(topic)}
          >
            <RadioButton
              value={1}
              label={tr('Beginner')}
              iconStyle={this.styles.radioIcon}
              labelStyle={this.styles.radioLabel}
              onClick={e => {
                e.stopPropagation();
              }}
              aria-label="Beginner"
            />
            <RadioButton
              value={2}
              label={tr('Intermediate')}
              iconStyle={this.styles.radioIcon}
              labelStyle={this.styles.radioLabel}
              onClick={e => {
                e.stopPropagation();
              }}
              aria-label="Intermediate"
            />
            <RadioButton
              value={3}
              label={tr('Advanced')}
              iconStyle={this.styles.radioIcon}
              labelStyle={this.styles.radioLabel}
              onClick={e => {
                e.stopPropagation();
              }}
              aria-label="Advanced"
            />
          </RadioButtonGroup>
        )}
        <div>
          <a
            href="#"
            style={this.styles.cancelLink}
            onClick={e => {
              _this.handleCancelClick(topicID, e);
            }}
          >
            {tr('Cancel')}
          </a>
          <a
            href="#"
            style={this.styles.okLink}
            onClick={e => {
              _this.handleOkClick(topic, e);
            }}
          >
            {tr('OK')}
          </a>
        </div>
      </div>
    );
  }
}
BIAModal.propTypes = {
  topicLevelObj: PropTypes.object,
  topic: PropTypes.object,
  handleCancelClick: PropTypes.func,
  updateLevel: PropTypes.func
};

export default connect()(BIAModal);
