import React, { Component } from 'react';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { PrimaryButton } from 'edc-web-sdk/components/index';
import colors from 'edc-web-sdk/components/colors/index';
import { searchForUsers } from 'edc-web-sdk/requests/users.v2';
import { shareToGroup, addTeamMembers } from 'edc-web-sdk/requests/groups.v2';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import Individuals from '../../components/shareContent/Individuals';
import Groups from '../../components/shareContent/Groups';
import DynamicSelections from '../../components/shareContent/DynamicSelections';
import CurrentlyShared from '../../components/shareContent/CurrentlyShared';
import SearchBox from '../../components/shareContent/SearchBox';

import { open as openSnackBar } from '../../actions/snackBarActions';
import { updateCurrentCard } from '../../actions/cardsActions';
import * as shareContentAction from '../../actions/shareContentActions';
import { close, openStatusModal, updateDynamicSelectionFilters } from '../../actions/modalActions';

import ShareAndCreateGroupModal from './ShareAndCreateGroupModal';
import PreviewDSModal from './PreviewDSModal';
import { Permissions } from '../../utils/checkPermissions';
import TextField from 'material-ui/TextField';

const emptyFilter = [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }];

class ShareContentModal extends Component {
  constructor(props, context) {
    super(props, context);
    let tabs = ['Individuals', 'Groups'];
    let enableDynamicSelection =
      window.ldclient.variation('dynamic-selections', false) &&
      (Permissions['enabled'] !== undefined && Permissions.has('USE_DYNAMIC_SELECTION'));

    if (enableDynamicSelection) {
      tabs.push('Dynamic Selections');
    }
    if (this.props.currentUser.isAdmin) {
      tabs.push('Currently Shared');
    }

    this.state = {
      tabs,
      activeTab: 'Individuals',
      dynamicSelections: window.ldclient.variation('dynamic-selections', false),
      previewData: [],
      groupModal: false,
      showCreateGroupModal: false,
      showPreview: false,
      individualFilters: emptyFilter,
      dynamicFilters: emptyFilter,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
    this.styles = {
      back_button: {
        width: '4.875rem',
        backgroundColor: '#acadc1',
        color: '#fff',
        borderRadius: '0.125rem',
        border: '0'
      },
      cancel_button: { color: '#acadc1', border: '1px solid #acadc1' },
      share_button: { width: '7rem', backgroundColor: '#6f708b', color: '#fff', border: '0' },
      preview_button: { backgroundColor: '#acadc1', color: '#fff', border: '0' },
      share_and_create: { backgroundColor: '#454560', color: '#fff', border: '0' }
    };
    this.isAllUsers = false;
  }

  _tabToggle = activeTab => {
    if (this.state.dynamicSelections) {
      let toState = { activeTab };
      switch (activeTab) {
        case 'Individuals':
          if (this.state.activeTab === 'Dynamic Selections') {
            toState.dynamicFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
            toState.individualFilters = emptyFilter;
          }
          this.props.dispatch(updateDynamicSelectionFilters(this.state.individualFilters));
          this.setState(toState);
          break;
        case 'Dynamic Selections':
          if (this.state.activeTab === 'Individuals') {
            toState.individualFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
            toState.dynamicFilters = emptyFilter;
          }
          this.props.dispatch(updateDynamicSelectionFilters(this.state.dynamicFilters));
          this.setState(toState);
          break;
        default:
          if (this.state.activeTab === 'Dynamic Selections') {
            toState.dynamicFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
          }
          if (this.state.activeTab === 'Individuals') {
            toState.individualFilters = this.props.modal ? this.props.modal.filter : emptyFilter;
          }
          this.setState(toState);
      }
    } else {
      this.setState({ activeTab });
    }
  };

  _closeModal = () => {
    this.props.dispatch(
      shareContentAction._clearState(this.props.shareContent[this.props.shareContent.currentCard])
    );
    this.props.dispatch(close());
  };

  getUsers = async (idsOnly, limit, offset) => {
    if (this.props.modal && this.props.modal.filter) {
      let filters = this.props.modal.filter;
      let payload = { custom_fields: [], limit: limit || 20, q: '', offset: offset || 0 };
      if (idsOnly) {
        payload.only_user_ids = true;
        payload.limit = 1000;
      }
      filters.forEach(filter => {
        let options = [];
        if (filter.options && filter.options.length) {
          filter.options.forEach(option => {
            if (option.checked) {
              options.push(option.option);
            }
          });
          payload.custom_fields.push({ name: filter.type, values: options });
        }
      });
      payload.custom_fields = JSON.stringify(payload.custom_fields);
      return await searchForUsers(payload);
    } else {
      return [];
    }
  };

  _openGroupModal = async () => {
    this.setState({ showCreateGroupModal: true });
  };

  _submitData = async data => {
    await this.setState({ buttonPending: true, err: false });
    let payload = {};
    if (this.state.activeTab !== 'Dynamic Selections') {
      let {
        shareWithUsers,
        shareWithGroups,
        removeUsersFromShare,
        removeGroupsFromShare,
        searchedUsers,
        searchedGroups
      } = data;
      payload = {
        team_ids: map([...(shareWithGroups || []), ...(searchedGroups || [])] || [], 'id'),
        user_ids: map([...(shareWithUsers || []), ...(searchedUsers || [])] || [], 'id'),
        remove_user_ids: map(removeUsersFromShare || [], 'id'),
        remove_team_ids: map(removeGroupsFromShare || [], 'id')
      };
    } else {
      let dataUsers = await this.getUsers(true);
      payload =
        dataUsers && dataUsers.user_ids && dataUsers.user_ids.length
          ? {
              user_ids: dataUsers.user_ids
            }
          : false;
    }
    if (payload) {
      shareToGroup(this.props.card.id, payload)
        .then(response => {
          this.props.dispatch(shareContentAction._clearState(response.card));
          this.setState({ buttonPending: false });
          let cardType =
            response.card.cardType === 'pack'
              ? 'Pathway'
              : response.card.cardType === 'journey'
              ? 'Journey'
              : 'Smartcard';
          this.props.dispatch(openSnackBar(cardType + ' successfully shared', true));
          this.props.dispatch(updateCurrentCard(response.card));
          this.props.dispatch(close());
        })
        .catch(() => {
          let msg = 'This card is restricted and can be shared only by the author.';
          if (this.state.newModalAndToast) {
            this.props.dispatch(openSnackBar(msg));
          } else {
            this.props.dispatch(openStatusModal(msg));
          }
        });
    } else {
      let msg = 'No user was found for the current filters.';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  _openPreview = async () => {
    let data = await this.getUsers();
    this.setState({
      previewData: data.users,
      showPreview: true
    });
  };

  componentDidMount() {
    let payload = { limit: 15, offset: 0 };
    let payloadGroup = { limit: 15, offset: 0, writables: true };

    this.props
      .dispatch(shareContentAction._fetchCard(this.props.card.id))
      .then(() => {
        if (
          !(
            this.props.shareContent &&
            this.props.shareContent.groups &&
            this.props.shareContent.groups.length > 0
          )
        ) {
          this.props.dispatch(shareContentAction._fetchGroups(payloadGroup));
        }

        if (
          !(
            this.props.shareContent &&
            this.props.shareContent.users &&
            this.props.shareContent.users.length > 0
          )
        ) {
          this.props.dispatch(shareContentAction._fetchUsers(payload));
        }
      })
      .catch(() => {});
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeTab === 'Individuals' &&
      this.state.dynamicSelections &&
      ((nextProps.modal.filter &&
        this.props.modal.filter &&
        this.props.modal.filter.length === nextProps.modal.filter.length) ||
        (!nextProps.modal.filter && !this.props.modal.filter))
    ) {
      this.isAllUsers = false;
      this.applyFilterForIndividual();
    }
  }

  backHandle = () => {
    this.setState({
      showCreateGroupModal: false,
      showPreview: false
    });
  };

  createHandle = async team => {
    let dataUsers = await this.getUsers(true);
    if (dataUsers && dataUsers.user_ids && dataUsers.user_ids.length && this.props.currentUser) {
      let arr = dataUsers.user_ids.filter(el => el != this.props.currentUser.id);
      if (arr.length) {
        let payload = {
          user_ids: arr,
          role: 'member'
        };
        await addTeamMembers(team.id, payload);
      }
    }
    await this.setState({ buttonPending: true, err: false });

    shareToGroup(this.props.card.id, { team_ids: [team.id] })
      .then(response => {
        this.props.dispatch(shareContentAction._clearState(response.card));
        this.setState({ buttonPending: false });
        let cardType =
          response.card.cardType === 'pack'
            ? 'Pathway'
            : response.card.cardType === 'journey'
            ? 'Journey'
            : 'Smartcard';
        this.props.dispatch(openSnackBar(cardType + ' successfully shared', true));
        this.props.dispatch(updateCurrentCard(response.card));
        this.props.dispatch(close());
      })
      .catch(() => {
        let msg = 'This card is restricted and can be shared only by the author.';
        if (this.state.newModalAndToast) {
          this.props.dispatch(openSnackBar(msg));
        } else {
          this.props.dispatch(openStatusModal(msg));
        }
      });
  };

  applyFilterForIndividual = async (offset, cb) => {
    this.isAllUsers = false;
    let data = await this.getUsers(false, 21, offset);
    if (data.users) {
      if (data.users.length && data.users.length < 20) {
        this.isAllUsers = true;
      } else {
        data.users.splice(-1, 1);
      }
    }
    this.setState(
      {
        users:
          this.state.users && this.state.users.length && offset
            ? this.state.users.concat(data.users)
            : data.users
      },
      () => {
        cb && cb();
      }
    );
  };

  render() {
    let curentFilters = (this.props.modal && this.props.modal.filter) || [];
    let existFilter = curentFilters.filter(el => el.type !== '+' && el.type !== 'Add Filter +')
      .length;
    return !this.state.showCreateGroupModal && !this.state.showPreview ? (
      <div style={{ width: '58.125rem', margin: '0 auto' }}>
        <div className="row">
          <TextField name="sharecontentmodal" autoFocus={true} className="hiddenTextField" />
          <div
            className="small-12 columns"
            style={{ position: 'relative', marginBottom: '0.625rem' }}
          >
            <div style={{ color: 'white', fontSize: '1.125rem' }}>
              {!this.state.dynamicSelections ? tr('Share with') : tr('Share ')}
            </div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this._closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper>
              <div className="share-content-modal-container">
                <div className="sc_tabs_container">
                  {this.state.tabs.map((tab, index) => {
                    return (
                      <div
                        className={this.state.activeTab === tab ? 'sc_tab active' : 'sc_tab'}
                        style={{ width: 100 / this.state.tabs.length + '%' }}
                        key={index}
                      >
                        <span
                          onClick={() => {
                            this._tabToggle(tab);
                          }}
                          className="pointer"
                        >
                          {tr(tab)}
                        </span>
                      </div>
                    );
                  })}
                </div>
                {this.state.activeTab !== 'Currently Shared' &&
                  this.state.activeTab !== 'Dynamic Selections' && (
                    <div>
                      <SearchBox {...this.props} activeTab={this.state.activeTab} />
                    </div>
                  )}
                <div className="sc_tabs_content_container">
                  <div
                    className={
                      this.state.activeTab === 'Individuals' ? '' : 'inactive-tab-container'
                    }
                  >
                    {this.state.dynamicSelections && (
                      <DynamicSelections {...this.props} forIndividuals />
                    )}
                    <Individuals
                      {...this.props}
                      users={this.state.users}
                      isAllUsers={this.isAllUsers}
                      applyFilterForIndividual={this.applyFilterForIndividual}
                    />
                  </div>

                  <div
                    className={this.state.activeTab === 'Groups' ? '' : 'inactive-tab-container'}
                  >
                    <Groups {...this.props} />
                  </div>
                  {this.state.dynamicSelections && (
                    <div
                      className={
                        this.state.activeTab === 'Dynamic Selections'
                          ? ''
                          : 'inactive-tab-container'
                      }
                    >
                      <DynamicSelections {...this.props} />
                    </div>
                  )}
                  {this.props.currentUser.isAdmin && (
                    <div
                      className={
                        this.state.activeTab === 'Currently Shared' ? '' : 'inactive-tab-container'
                      }
                    >
                      <CurrentlyShared {...this.props} />
                    </div>
                  )}
                  <div className="inactive-tab-container" />
                </div>
                <div className="text-center sc-button-container">
                  {this.state.err && (
                    <div className="text-center error-text data-not-available-msg">
                      {tr('Something went wrong! Please try later.')}
                    </div>
                  )}
                  <SecondaryButton
                    style={this.styles.cancel_button}
                    label={tr('Cancel')}
                    onTouchTap={this._closeModal}
                  />
                  {this.state.activeTab === 'Dynamic Selections' && (
                    <SecondaryButton
                      style={this.styles.preview_button}
                      label={tr('Preview Data')}
                      onTouchTap={this._openPreview}
                    />
                  )}
                  <PrimaryButton
                    style={this.styles.share_button}
                    label={tr(this.state.activeTab === 'Currently Shared' ? 'Update' : 'Share')}
                    onTouchTap={() => {
                      this._submitData(this.props.shareContent);
                    }}
                    theme={colors.followColor}
                    pending={this.state.buttonPending}
                    pendingLabel={tr(
                      this.state.activeTab === 'Currently Shared' ? 'Updating...' : 'Sharing...'
                    )}
                    disabled={this.state.activeTab === 'Dynamic Selections' && !existFilter}
                  />

                  {this.state.activeTab === 'Dynamic Selections' && (
                    <SecondaryButton
                      style={this.styles.share_and_create}
                      label={tr('Share and Create Group')}
                      onTouchTap={this._openGroupModal}
                      disabled={!existFilter}
                    />
                  )}
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    ) : this.state.showPreview ? (
      <PreviewDSModal
        backHandle={this.backHandle.bind(this)}
        previewData={this.state.previewData}
      />
    ) : (
      <ShareAndCreateGroupModal
        backHandle={this.backHandle.bind(this)}
        createHandle={this.createHandle.bind(this)}
        isDynamicGroup={true}
      />
    );
  }
}

ShareContentModal.propTypes = {
  currentUser: PropTypes.any,
  card: PropTypes.object,
  dispatch: PropTypes.any,
  shareContent: PropTypes.any,
  modal: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    modal: state.modal.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(ShareContentModal);
