import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { connect } from 'react-redux';
import Card from '../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTeamCardsv2, deleteSharedCard, pinUnpinCardv2 } from '../../actions/groupsActionsV2';
import { close, confirmation } from '../../actions/modalActions';
import TextField from 'material-ui/TextField';
import GroupCardOverlay from '../team/GroupCardOverlay';
import { Permissions } from '../../utils/checkPermissions';

class TeamCardsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      loading: false,
      cards: this.props.cards || [],
      title: this.props.cardModalDetails.desc || '',
      count: this.props.cardModalDetails.count || 0,
      arryName: this.props.cardModalDetails.name || '',
      cardModalType: this.props.cardModalDetails.type || '',
      groupID: this.props.cardModalDetails.groupID || 0,
      multilingualFiltering: window.ldclient.variation('multilingual-content', false)
    };
    this.styles = {
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true });
    let groupID = this.state.groupID;
    let type = this.state.cardModalType;
    let limit = 15;
    let offset = this.state.cards.length;
    let existingCards = this.state.cards;
    getTeamCardsv2(
      groupID,
      this.state.arryName,
      { limit, offset, type },
      this.state.multilingualFiltering
    )
      .then(response => {
        this.setState({
          loading: false,
          cards: [...existingCards, ...response.cards],
          count: response.total
        });
      })
      .catch(err => {
        console.error(`Error in TeamCardsModal.getTeamCards.func : ${err}`);
      });
  };

  deleteSharedCardHandler = payload => {
    let _this = this;
    let groupID = this.state.groupID;
    this.props.dispatch(
      confirmation(
        'Remove from Group',
        `Group members will no longer be able to see this card in the group.`,
        () => {
          _this.props.dispatch(deleteSharedCard(groupID, payload));
        }
      )
    );
  };

  fetchData = () => {
    let titleKeysObj = {};
    let cardType = this.state.title;
    switch (cardType) {
      case 'Featured':
        titleKeysObj.key = 'Featured';
        break;
      case 'Assigned':
        titleKeysObj.key = 'Assigned';
        break;
      case 'Shared':
        titleKeysObj.key = 'Shared';
        break;
      case 'Channel':
        titleKeysObj.key = 'Channel';
        break;
    }
    return titleKeysObj;
  };

  pinUnpinCard = (card, pinnedStatus) => {
    let payload = {
      pinnable_id:
        (this.props.modal.group && this.props.modal.group.id) ||
        (this.props.modal.cardModalDetails && this.props.modal.cardModalDetails.groupID),
      pinnable_type: 'Team',
      object_id: card.id,
      object_type: 'Card'
    };
    let cardTypes = this.fetchData().key;
    this.props.dispatch(pinUnpinCardv2(payload, pinnedStatus, cardTypes, card));
  };

  render() {
    let title = this.state.title;
    let count = this.state.count;
    let arryName = this.state.arryName;
    let cards = this.state.cards;
    let group = this.props.group;
    let pinnedCardIDs = this.props.teamProps.get('pinnedCardIDs') || [];
    let allowMarkFeatured = (group && group.isTeamAdmin) || Permissions.has('ADMIN_ONLY');
    let isViewAll = (this.props.modal && this.props.modal.isViewAll) || false;
    return (
      <div className="channel-cards-modal">
        <div className="row">
          <TextField name="teamcard" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>{tr(title) + '(' + count + ')'}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.iconButton}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row ">
                {cards.length > 0 &&
                  cards.map(card => {
                    return (
                      <div
                        key={card.id}
                        className={`channel-card-v2 card-v2 custom-medium-6 column show-overlay ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12`}
                      >
                        <Card
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={arryName !== 'teamAssignments'}
                          moreCards={false}
                          deleteSharedCard={
                            this.props.deleteSharedCard
                              ? this.deleteSharedCardHandler.bind(this, { card_id: card.id })
                              : false
                          }
                          groupSetting={true}
                        />
                        {allowMarkFeatured && !isViewAll && (
                          <GroupCardOverlay
                            card={card}
                            pinnedStatus={!!~pinnedCardIDs.indexOf(card.id)}
                            pinUnpinCard={this.pinUnpinCard}
                            pinnedCount={pinnedCardIDs.length}
                            group={group}
                          />
                        )}
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {cards.length < count && (
              <button
                disabled={this.state.loading}
                className="view-more-v2"
                onClick={this.viewMoreClickHandler}
              >
                {this.state.loading ? tr('Loading...') : tr('View More')}
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

TeamCardsModal.propTypes = {
  cards: PropTypes.arrayOf(PropTypes.object),
  cardModalDetails: PropTypes.shape({
    name: PropTypes.string,
    count: PropTypes.number,
    desc: PropTypes.string,
    groupID: PropTypes.number,
    type: PropTypes.string
  }),
  currentUser: PropTypes.object,
  deleteSharedCard: PropTypes.any,
  group: PropTypes.object,
  modal: PropTypes.object,
  teamProps: PropTypes.object
};

export default connect(state => ({
  modal: state.modal.toJS(),
  currentUser: state.currentUser.toJS(),
  teamProps: state.teamNewReducer
}))(TeamCardsModal);
