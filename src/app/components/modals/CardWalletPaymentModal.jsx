/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import OrgPaymentDetails from '../../components/payment/OrgPaymentDetails';
let LocaleCurrency = require('locale-currency');
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import { close } from '../../actions/modalActions';
import {
  initiateCoursePurchase,
  completedCoursePurchase,
  initiateCardPurchase,
  completedCardPurchase,
  cancelCardPurchase
} from 'edc-web-sdk/requests/cards.v2';
import { getWalletBalance } from '../../actions/currentUserActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import TextFieldCustom from '../../components/common/SmartBiteInput';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import InputMask from 'react-text-mask';
import {
  Icon_AmericanExpress,
  Icon_CreditCardOutline,
  Icon_DinersClub,
  Icon_Discover,
  Icon_JCB,
  Icon_Visa,
  Icon_MasterCard
} from 'material-ui-credit-card-icons';
import Spinner from '../common/spinner';
var braintree = require('braintree-web');
var creditCardType = require('credit-card-type');
var valid = require('card-validator');
import FontIcon from 'material-ui/FontIcon';
import TooltipLabel from '../common/TooltipLabel';

class CardWalletPaymentModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      disabled: true,
      courseUrl: '',
      paymentInitializationError: ''
    };
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      courseName: {
        display: 'inherit'
      }
    };

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onClickClose = this.onClickClose.bind(this);

    this.onSubmitWalletDetails = this.onSubmitWalletDetails.bind(this);
    this.cardChangeHandle = this.cardChangeHandle.bind(this);
    this.dateChangeHandle = this.dateChangeHandle.bind(this);
    this.cvvChangeHandle = this.cvvChangeHandle.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.showCVV = this.showCVV.bind(this);
    this.finalCardPurchase = this.finalCardPurchase.bind(this);
    this.finalWalletPurchase = this.finalWalletPurchase.bind(this);
  }

  componentDidMount() {
    let priceData = (this.props.paymentData && this.props.paymentData.priceData) || {};

    let payload = {};
    if (priceData.currency == 'SKILLCOIN') {
      payload = {
        orderable_type: 'card',
        price_id:
          this.props.paymentData &&
          this.props.paymentData.priceData &&
          this.props.paymentData.priceData.id,
        orderable_id:
          this.props.paymentData && this.props.paymentData.card && this.props.paymentData.card.id
      };
      initiateCoursePurchase(payload)
        .then(data => {
          let clientToken = data.transactionDetails.clientToken;
          this.setState(
            {
              token: data.transactionDetails.token,
              disabled: false,
              showForm: true,
              paymentInitializationError: ''
            },
            function() {}
          );
        })
        .catch(err1 => {
          if (err1.cardPurchased) {
            this.props.paymentData.cardPurchased();
          }
          this.setState({
            showForm: true,
            paymentInitializationError: err1.message
          });
        });
    } else {
      payload = {
        orderable_type: 'card',
        price_id:
          this.props.paymentData &&
          this.props.paymentData.priceData &&
          this.props.paymentData.priceData.id,
        orderable_id:
          this.props.paymentData && this.props.paymentData.card && this.props.paymentData.card.id
      };

      initiateCardPurchase(payload)
        .then(data => {
          let clientToken = data.transactionDetails.clientToken;
          this.setState(
            {
              token: data.transactionDetails.token,
              clientToken,
              client: new braintree.api.Client({ clientToken: clientToken }),
              showForm: true,
              paymentInitializationError: ''
            },
            function() {
              this.setUpBrainTree(data.transactionDetails.token, clientToken);
            }
          );
        })
        .catch(err1 => {
          if (err1.cardPurchased) {
            this.props.paymentData.cardPurchased();
          }
          this.setState({
            showForm: true,
            paymentInitializationError: err1.message
          });
        });
    }
  }

  setUpBrainTree = (token, clientToken) => {
    braintree.setup(clientToken, 'custom', {
      container: 'payment-form'
    });
  };

  finalCardPurchase = () => {
    let _this = this;
    _this.state.client.tokenizeCard(
      {
        number: _this._cardNumber.input.value,
        expirationMonth: _this._month.input.value,
        expirationYear: _this._year.input.value,
        cvv: _this._cvv.input.value
      },
      function(err, nonce) {
        let payload2 = {
          payment_method_nonce: nonce,
          token: _this.state.token,
          gateway: 'braintree'
        };

        completedCardPurchase(payload2)
          .then(data2 => {
            _this.props.paymentData.cardPurchased();
            _this.setState({
              paymentSuccess: true,
              paymentAttempted: true,
              courseUrl: data2.transactionDetails.redirectUrl
            });
          })
          .catch(err1 => {
            _this.setState({
              paymentError: err1.message,
              paymentAttempted: true,
              disabled: true
            });
          });
      }
    );
  };

  onSubmitCardDetails = () => {
    let _this = this;

    _this.setState({ disabled: true, paymentError: null });
    /*eslint handle-callback-err: "off"*/
    if (_this.state.paymentError != null) {
      let payload = {
        orderable_type: 'card',
        price_id:
          _this.props.paymentData &&
          _this.props.paymentData.priceData &&
          _this.props.paymentData.priceData.id,
        orderable_id:
          _this.props.paymentData && _this.props.paymentData.card && _this.props.paymentData.card.id
      };
      initiateCardPurchase(payload)
        .then(data => {
          let clientToken = data.transactionDetails.clientToken;
          _this.setState(
            {
              token: data.transactionDetails.token,
              clientToken,
              client: new braintree.api.Client({ clientToken: clientToken }),
              showForm: true,
              paymentInitializationError: ''
            },
            function() {
              _this.setUpBrainTree(data.transactionDetails.token, clientToken);
              _this.finalCardPurchase();
            }
          );
        })
        .catch(err1 => {
          if (err1.code == 'payment_already_processed') {
            _this.props.paymentData.cardPurchased();
          }
          _this.setState({
            showForm: true,
            paymentInitializationError: err1.message
          });
        });
    } else {
      _this.finalCardPurchase();
    }
  };

  disableButton() {
    let numberValidation = valid.number(this._cardNumber.input.value);
    let dateValidation = valid.expirationDate(
      `${this._month.input.value}/${this._year.input.value}`
    );

    let cvvLength =
      (numberValidation.card && numberValidation.card.code && numberValidation.card.code.size) || 3;

    let cvvValidation = valid.cvv(this._cvv.input.value, cvvLength);
    let monthValidation = valid.expirationMonth(this._month.input.value);
    let yearValidation = valid.expirationYear(this._year.input.value);

    let disabled = !numberValidation.isValid || !dateValidation.isValid || !cvvValidation.isValid;

    this.setState({
      disabled: disabled,
      cardNumberError: this._cardNumber.input.value && !numberValidation.isValid ? ' ' : null,
      monthError:
        (this._month.input.value && !monthValidation.isValid) ||
        (this._month.input.value && this._year.input.value && !dateValidation.isValid)
          ? ' '
          : null,
      yearError: this._year.input.value && !yearValidation.isValid ? ' ' : null,
      cvvError: this._cvv.input.value && !cvvValidation.isValid ? ' ' : null
    });
  }

  cardChangeHandle() {
    let numberValidation = valid.number(this._cardNumber.input.value);
    this.disableButton();

    if (numberValidation.isPotentiallyValid) {
      this.setState({
        cardType: (numberValidation.card && numberValidation.card.type) || null
      });
    }
  }

  dateChangeHandle() {
    this.disableButton();
  }

  cvvChangeHandle() {
    this.disableButton();
  }

  showCVV() {
    this.setState({
      showCVV: !this.state.showCVV
    });
  }

  finalWalletPurchase() {
    let _this = this;
    _this.setState({ disabled: true, paymentError: null });
    let payload = {
      token: _this.state.token,
      gateway: 'wallet'
    };

    completedCoursePurchase(payload)
      .then(data => {
        _this.props.dispatch(getWalletBalance());
        _this.props.paymentData.cardPurchased();
        _this.setState({
          paymentSuccess: true,
          paymentAttempted: true,
          courseUrl: data.transactionDetails.redirectUrl
        });
      })
      .catch(err1 => {
        _this.setState({
          paymentError: err1.message,
          paymentAttempted: true,
          disabled: true
        });
      });
  }

  onSubmitWalletDetails = () => {
    let _this = this;
    if (_this.state.paymentError != null) {
      let payload = {};
      payload = {
        orderable_type: 'card',
        price_id:
          _this.props.paymentData &&
          _this.props.paymentData.priceData &&
          _this.props.paymentData.priceData.id,
        orderable_id:
          _this.props.paymentData && _this.props.paymentData.card && _this.props.paymentData.card.id
      };
      initiateCoursePurchase(payload)
        .then(data => {
          let clientToken = data.transactionDetails.clientToken;
          _this.setState(
            {
              token: data.transactionDetails.token,
              disabled: false,
              showForm: true,
              paymentInitializationError: ''
            },
            function() {
              _this.finalWalletPurchase();
            }
          );
        })
        .catch(err1 => {
          if (err1.code == 'payment_already_processed') {
            _this.props.paymentData.cardPurchased();
          }
          _this.setState({
            showForm: true,
            paymentInitializationError: err1.message
          });
        });
    } else {
      _this.finalWalletPurchase();
    }
  };

  componentWillReceiveProps(nextProps) {}

  onClickClose() {
    let payload = {
      token: this.state.token
    };
    cancelCardPurchase(payload)
      .then(data => {
        this.onCloseModal();
      })
      .catch(err1 => {
        this.onCloseModal();
      });
  }
  onCloseModal() {
    this.props.dispatch(close());
  }

  tooltipText = tooltipText => {
    return `<p className="tooltip-text">${tooltipText}</p>`;
  };

  render() {
    let user = this.props.currentUser;

    let card = (this.props.paymentData && this.props.paymentData.card) || {};

    let priceData = (this.props.paymentData && this.props.paymentData.priceData) || {};

    let cardTitle = card.title || card.message;
    return (
      <div id="card-wallet-payment-modal">
        {!this.state.showForm && !this.state.paymentSuccess && (
          <div className="text-center">
            <div style={{ width: '50px', margin: 'auto', padding: '100px 0' }}>
              <Spinner />
            </div>
          </div>
        )}
        {this.state.showForm && this.state.paymentInitializationError && (
          <div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.onCloseModal.bind(this)}
              >
                <CloseIcon color="white" />
              </IconButton>
              <TextField name="cardwalletmodal" autoFocus={true} className="hiddenTextField" />
            </div>
            <div className="card-wallet-payment-modal-title">Error</div>
            <div className="row">
              <div className="small-12 columns payment-error-buttons">
                <div className="actions-container">
                  <div className="action-payment-header">
                    <h5>Error</h5>
                  </div>
                  <div className="text-center">
                    <div
                      className="error"
                      style={{ fontSize: '18px', color: 'red', marginBottom: '25px' }}
                    >
                      {this.state.paymentInitializationError}
                    </div>
                    <SecondaryButton
                      onClick={this.onCloseModal.bind(this)}
                      label={tr('OK')}
                      className="review-btn"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.showForm && !this.state.paymentInitializationError && (
          <div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={
                  this.state.paymentAttempted
                    ? this.onCloseModal.bind(this)
                    : this.onClickClose.bind(this)
                }
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
            {priceData.currency == 'SKILLCOIN' && (
              <div className="card-wallet-payment-modal-title">Buy with SkillCoins</div>
            )}
            {priceData.currency !== 'SKILLCOIN' && (
              <div className="card-wallet-payment-modal-title">Buy using Credit/Debit Card</div>
            )}
            <div className="row">
              {this.state.paymentSuccess && (
                <div className="small-7 columns">
                  <div className="payment-success">
                    Payment Successfull
                    <div>
                      <img width="50" src="/i/images/check-green.png" />
                    </div>
                    <div style={{ marginTop: '20px' }}>
                      <strong>
                        {parseInt(priceData.amount)} {priceData.currency}
                      </strong>
                    </div>
                    <div style={{ marginTop: '20px' }}>A receipt is sent to your Email</div>
                    <div style={{ marginTop: '30px' }}>
                      <a target="_blank" href={this.state.courseUrl}>
                        Go to the course
                      </a>
                    </div>
                  </div>
                </div>
              )}

              {!this.state.paymentSuccess && (
                <div
                  className={
                    priceData.currency == 'SKILLCOIN'
                      ? 'small-7 columns payment-buttons'
                      : 'small-7 columns'
                  }
                >
                  {priceData.currency == 'SKILLCOIN' && (
                    <div className="actions-container">
                      <div className="heading">
                        Do you want to confirm placing your order with Skillcoins?
                      </div>
                      <div className="text-center">
                        <SecondaryButton
                          onClick={
                            this.state.paymentAttempted
                              ? this.onCloseModal.bind(this)
                              : this.onClickClose.bind(this)
                          }
                          label={tr('CANCEL')}
                          className="review-btn"
                        />
                        <PrimaryButton
                          disabled={this.state.disabled}
                          label={tr('CONFIRM ORDER')}
                          className="review-btn"
                          onClick={this.onSubmitWalletDetails.bind(this)}
                        />
                        {this.state.paymentError && (
                          <div className="error" style={{ fontSize: '12px', color: 'red' }}>
                            {this.state.paymentError}
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {priceData.currency !== 'SKILLCOIN' && (
                    <div id="org-payment-modal" style={{ padding: '20px 25px' }}>
                      <form id="payment-form" method="post" className="payment-details-container">
                        <div className="row">
                          <div className=" column small-12 position-relative">
                            <TextField
                              hintText={'Card Number'}
                              aria-label={'Card Number'}
                              type="text"
                              hintStyle={this.styles.hint}
                              style={this.styles.cardNumberStyle}
                              inputStyle={this.styles.inputs}
                              fullWidth={true}
                              guide={false}
                              onChange={this.cardChangeHandle.bind(this)}
                              ref={node => (this._cardNumber = node)}
                              errorText={this.state.cardNumberError}
                              icon={
                                <IconButton>
                                  <Icon_CreditCardOutline />
                                </IconButton>
                              }
                            />
                            <div className="position-absolute">
                              {this.state.cardType == null && (
                                <IconButton>
                                  <Icon_CreditCardOutline />
                                </IconButton>
                              )}
                              {this.state.cardType == 'visa' && (
                                <IconButton>
                                  <Icon_Visa />
                                </IconButton>
                              )}
                              {this.state.cardType == 'master-card' && (
                                <IconButton>
                                  <Icon_MasterCard />
                                </IconButton>
                              )}
                            </div>
                          </div>
                        </div>
                        <div className=" row">
                          <div className=" column small-3">
                            <TextField
                              hintText={'MM'}
                              aria-label="enter month"
                              type="text"
                              onChange={this.dateChangeHandle.bind(this)}
                              style={this.styles.cardNumberStyle}
                              hintStyle={this.styles.hint}
                              fullWidth={true}
                              inputStyle={this.styles.inputs}
                              ref={node => (this._month = node)}
                              fullWidth={true}
                              errorText={this.state.monthError}
                              maxLength="2"
                            />
                          </div>
                          <input type="hidden" name="payment_method_nonce" />
                          <div className="column small-3 position-relative">
                            <TextField
                              hintText={'YYYY'}
                              aria-label="enter year"
                              type="text"
                              onChange={this.dateChangeHandle.bind(this)}
                              style={this.styles.cardNumberStyle}
                              hintStyle={this.styles.hint}
                              fullWidth={true}
                              inputStyle={this.styles.inputs}
                              ref={node => (this._year = node)}
                              errorText={this.state.yearError}
                              maxLength="4"
                            />
                          </div>
                        </div>
                        <div className="row ">
                          <div className="column small-3">
                            <TextField
                              hintText={'CVV'}
                              aria-label="enter CVV"
                              type="password"
                              onChange={this.cvvChangeHandle.bind(this)}
                              style={this.styles.cardNumberStyle}
                              hintStyle={this.styles.hint}
                              fullWidth={true}
                              ref={node => (this._cvv = node)}
                              inputStyle={this.styles.inputs}
                              errorText={this.state.cvvError}
                              maxLength="4"
                            />
                          </div>
                          <div className="column small-2" style={{ paddingTop: '14px' }}>
                            <a
                              onTouchTap={this.showCVV.bind(this)}
                              style={{
                                background: '#454460',
                                color: '#ffffff',
                                padding: '1px 8px',
                                fontSize: '12px',
                                border: '1px solid #bdbdbd',
                                borderRadius: '50%'
                              }}
                            >
                              ?
                            </a>
                          </div>
                        </div>
                        {this.state.showCVV && (
                          <div style={{ position: 'absolute', top: '65px', right: '100px' }}>
                            <div className="cvv-heading">What is CVV?</div>
                            <img src="/i/images/cvv.svg" />
                            <div className="cvv-fade" />
                          </div>
                        )}

                        <div className="row ">
                          <div className="column small-5" style={{ marginTop: '10px' }}>
                            <PrimaryButton
                              label={'Pay ' + priceData.symbol + priceData.amount}
                              className="confirm"
                              pendingLabel={tr('Confirming...')}
                              pending={false}
                              disabled={this.state.disabled}
                              onTouchTap={this.onSubmitCardDetails.bind(this)}
                              fullWidth={true}
                              aria-label={'Pay ' + priceData.symbol + priceData.amount}
                            />
                          </div>
                          {this.state.paymentError && (
                            <div className="error" style={{ fontSize: '12px', color: 'red' }}>
                              {this.state.paymentError}
                            </div>
                          )}
                        </div>
                      </form>
                    </div>
                  )}
                </div>
              )}
              <div className="small-5 order-details">
                <div className="details-heading">Order Details</div>
                <div className="order-info">
                  <div className="row" style={this.styles.courseName}>
                    <div>
                      {!card.readableCardType && <span>Course:</span>}

                      {card.readableCardType && <span>{card.readableCardType}:</span>}
                    </div>
                    <div className="columns" style={{ fontSize: '16px' }}>
                      <TooltipLabel html={true} text={this.tooltipText(cardTitle)}>
                        <p>
                          {cardTitle.length > 200 ? cardTitle.slice(0, 200) + '...' : cardTitle}
                        </p>
                      </TooltipLabel>
                      <div className="providerImage">
                        {card.providerImage && <img src={card.providerImage} />}
                      </div>
                    </div>
                  </div>
                  <div className="row flex-center">
                    <div className="small-3 columns">Price:</div>
                    <div
                      className="small-9 columns"
                      style={{ fontSize: '16px', fontWeight: '600' }}
                    >
                      {parseInt(priceData.amount)} {priceData.currency}
                    </div>
                  </div>
                  <div className="row flex-center" style={{ marginTop: '30px' }}>
                    <div className="small-3 columns">Total:</div>
                    <div
                      className="small-9 columns"
                      style={{ fontSize: '20px', fontWeight: '600' }}
                    >
                      {parseInt(priceData.amount)} {priceData.currency}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

CardWalletPaymentModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  callback: PropTypes.func,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  title: PropTypes.string,
  paymentData: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardWalletPaymentModal);
