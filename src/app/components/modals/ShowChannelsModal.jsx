import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { close } from '../../actions/modalActions';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Channel from './../../components/discovery/Channel.jsx';
import { getTeamChannels } from '../../actions/groupsActionsV2';
import TextField from 'material-ui/TextField';

class ShowChannelsModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      },
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      viewMore: {
        margin: '10px 0px'
      }
    };
    this.state = {
      loading: false,
      offset: 0,
      isAll: false
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true });
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let limit = group['teamChannelsLimit'];
    let grpChannelLength = group['teamChannels'].length;
    this.props
      .dispatch(getTeamChannels(groupID, { limit: limit, offset: grpChannelLength }))
      .then(() => {
        this.setState({ loading: false });
      })
      .catch(err => {
        console.error(`Error in ShowChannelsModal.getTeamChannels.func : ${err}`);
      });
  };

  render() {
    let group = this.props.groupsV2[this.props.groupsV2.currentGroupID];
    let count = group && group['teamChannelsCount'];
    let teamChannels = group && group['teamChannels'];
    return (
      <div className="channel-cards-modal">
        <div className="row">
          <TextField name="showchannel" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>
              {tr('Channels')} ({count})
            </div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.iconButton}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="five-card-column vertical-spacing-medium row">
                {group &&
                  count > 0 &&
                  teamChannels.map(channel => {
                    return (
                      <div
                        className={`channel-card-v2 card-v2 custom-medium-6 group-members small-12 column show-overlay`}
                        key={channel.id}
                      >
                        <Channel
                          channel={channel}
                          key={channel.id}
                          isFollowing={channel.isFollowing}
                          imageUrl={channel.profileImageUrl}
                          slug={channel.slug}
                          title={channel.label}
                          id={channel.id}
                          allowFollow={channel.allowFollow}
                          removable={this.props.removable}
                          group={group}
                          teamChannelsLimit={teamChannels.length}
                        />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="small-12 columns text-center" style={this.styles.viewMore}>
            {group && teamChannels.length < count && (
              <button
                disabled={this.state.loading}
                className="view-more-v2"
                onClick={this.viewMoreClickHandler.bind(this)}
              >
                {this.state.loading ? tr('Loading...') : tr('View More')}
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

ShowChannelsModal.propTypes = {
  groupsV2: PropTypes.object,
  removable: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(ShowChannelsModal);
