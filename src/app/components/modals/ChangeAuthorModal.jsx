import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { PrimaryButton } from 'edc-web-sdk/components/index';
import colors from 'edc-web-sdk/components/colors/index';
import { searchUsers } from 'edc-web-sdk/requests/users.v2';
import { transferAuthor } from 'edc-web-sdk/requests/cards.v2';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import AuthorList from '../../components/changeAuthor/authorList';

import { open as openSnackBar } from '../../actions/snackBarActions';
import { close, openStatusModal } from '../../actions/modalActions';

import { Permissions } from '../../utils/checkPermissions';

const emptyFilter = [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }];

class ChangeAuthorModal extends Component {
  constructor(props, context) {
    super(props, context);
    let enableDynamicSelection =
      window.ldclient.variation('dynamic-selections', false) &&
      (Permissions['enabled'] !== undefined && Permissions.has('USE_DYNAMIC_SELECTION'));
    let timeout = 0;
    let filterEnabled = false;
    let prefillUsers = {};
    let query = '';
    this.state = {
      activeTab: 'Individuals',
      dynamicSelections: window.ldclient.variation('dynamic-selections', false),
      previewData: [],
      groupModal: false,
      showCreateGroupModal: false,
      showPreview: false,
      individualFilters: emptyFilter,
      dynamicFilters: emptyFilter,
      searchBoxValue: '',
      users: [],
      totalUsers: null,
      selectedUser: null,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
    this.styles = {
      back_button: {
        width: '4.875rem',
        backgroundColor: '#acadc1',
        color: '#fff',
        borderRadius: '0.125rem',
        border: '0'
      },
      cancel_button: { color: '#acadc1', border: '1px solid #acadc1' },
      change_author_button: {
        width: '10rem',
        backgroundColor: '#6f708b',
        color: '#fff',
        border: '0'
      },
      preview_button: { backgroundColor: '#acadc1', color: '#fff', border: '0' },
      share_and_create: { backgroundColor: '#454560', color: '#fff', border: '0' },
      search_box: {
        width: '100%',
        borderRadius: '2px',
        backgroundColor: 'white !important',
        outline: '0',
        paddingLeft: '25px',
        fontSize: '14px',
        color: '#6f708b',
        height: '24px',
        border: '1px solid #86879e'
      }
    };
    this.isAllUsers = false;
  }

  _closeModal = () => {
    this.props.dispatch(close());
  };

  handleInstantSearchv2 = query => {
    this.query = query.target.value;
    if (this.query.length > 0) {
      this.filterEnabled = true;
      let payload = { limit: 15, offset: 0, q: this.query };
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        searchUsers(payload)
          .then(response => {
            let users = response.users;
            let totalUsers = response.total;
            this.setState({ users, totalUsers });
          })
          .catch(() => {});
      }, 300);
    } else {
      this.filterEnabled = false;
      this.setState({ users: this.prefillUsers.users, totalUsers: this.prefillUsers.totalUsers });
    }
  };

  getUsers = (idsOnly, limit, offset, query = null) => {
    let payload = { custom_fields: [], limit: limit || 20, q: query, offset: offset || 0 };
    payload.custom_fields = JSON.stringify(payload.custom_fields);
    return searchUsers(payload);
  };

  _submitData = async data => {
    await this.setState({ buttonPending: true, err: false });
    let payload = { user_id: this.state.selectedUser };

    if (payload) {
      transferAuthor(this.props.card.id, payload)
        .then(response => {
          if (response) {
            this.setState({ buttonPending: false });
            this.props.dispatch(openSnackBar('Author changed successfully', true));
            let isInsideMeContent = window.location.pathname.indexOf('/me/content') >= 0;
            let isInsideLearnersDashboard =
              window.location.pathname.indexOf('/me/learners-dashboard') >= 0;
            let isUserHandle =
              window.location.pathname.indexOf('/' + this.props.card.author.handle) >= 0;
            if (isInsideMeContent || isInsideLearnersDashboard || isUserHandle) {
              this.props.removeCardFromList && this.props.removeCardFromList(this.props.card.id);
              this.props.removeCardFromCardContainer &&
                this.props.removeCardFromCardContainer(this.props.cardSectionName);
            } else {
              this.props.changeAuthorOptionRemoval();
            }
            this.props.dispatch(close());
          } else {
            let msg = 'Permission denied';
            if (this.state.newModalAndToast) {
              this.props.dispatch(openSnackBar(msg));
            } else {
              this.props.dispatch(openStatusModal(msg));
            }
          }
        })
        .catch(e => {
          let msg = 'This card cannot be transferred.';
          if (this.state.newModalAndToast) {
            this.props.dispatch(openSnackBar(msg));
          } else {
            this.props.dispatch(openStatusModal(msg));
          }
        });
    } else {
      let msg = 'No user was selected.';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  async componentDidMount() {
    let payload = { limit: 15, offset: 0 };
    let users = await this.getUsers(true);

    this.prefillUsers = { users: users.users, totalUsers: users.total };
    this.setState({
      users: users.users,
      totalUsers: users.total
    });
  }

  backHandle = () => {
    this.setState({
      showCreateGroupModal: false,
      showPreview: false
    });
  };

  selectUser = (userId, card, selected = false) => {
    selected === true
      ? this.setState({ selectedUser: userId })
      : this.setState({ selectedUser: null });
  };

  applyFilterForIndividual = async (offset, cb, query = null) => {
    this.isAllUsers = false;
    let data = await this.getUsers(false, 21, offset, !!this.query ? this.query : query);

    if (data.users) {
      if (data.users.length && data.users.length < 20) {
        this.isAllUsers = true;
      } else {
        data.users.splice(-1, 1);
      }
    }
    this.setState(
      {
        users:
          this.state.users && this.state.users.length && offset
            ? this.state.users.concat(data.users)
            : data.users
      },
      () => {
        cb && cb();
      }
    );
  };

  render() {
    return (
      <div style={{ width: '58.125rem', margin: '0 auto' }}>
        <div className="row">
          <div
            className="small-12 columns"
            style={{ position: 'relative', marginBottom: '0.625rem' }}
          >
            <div style={{ color: 'white', fontSize: '1.125rem' }}>{tr('Change Author')}</div>
            <div className="close close-button">
              <IconButton style={{ paddingRight: 0, width: 'auto' }} onTouchTap={this._closeModal}>
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper>
              <div className="share-content-modal-container">
                <input
                  type="text"
                  name="search"
                  label="Search"
                  placeholder={tr('Search')}
                  style={this.styles.search_box}
                  onChange={this.handleInstantSearchv2.bind(this)}
                />

                <div className="sc_tabs_content_container">
                  <div
                    className={
                      this.state.activeTab === 'Individuals' ? '' : 'inactive-tab-container'
                    }
                  >
                    <AuthorList
                      {...this.props}
                      selectUser={this.selectUser}
                      selectedUser={this.state.selectedUser}
                      users={this.state.users}
                      totalUsers={this.state.totalUsers}
                      isAllUsers={this.isAllUsers}
                      applyFilterForIndividual={this.applyFilterForIndividual}
                      filterEnabled={this.filterEnabled}
                    />
                  </div>

                  <div className="inactive-tab-container" />
                </div>
                <div className="text-center sc-button-container">
                  {this.state.err && (
                    <div className="text-center error-text data-not-available-msg">
                      {tr('Something went wrong! Please try later.')}
                    </div>
                  )}
                  <SecondaryButton
                    style={this.styles.cancel_button}
                    label={tr('Cancel')}
                    onTouchTap={this._closeModal}
                  />
                  <PrimaryButton
                    style={this.styles.change_author_button}
                    label={tr('Change Author')}
                    onTouchTap={() => {
                      this._submitData(this.props.updateAuthor);
                    }}
                    theme={colors.followColor}
                    pending={this.state.buttonPending}
                    pendingLabel={tr('Updating')}
                    disabled={!this.state.selectedUser}
                  />
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

ChangeAuthorModal.propTypes = {
  open: PropTypes.bool,
  currentUser: PropTypes.object,
  selfAssign: PropTypes.bool,
  card: PropTypes.object,
  assignedStateChange: PropTypes.func,
  modal: PropTypes.any,
  removeCardFromList: PropTypes.func,
  removeCardFromCardContainer: PropTypes.func,
  cardSectionName: PropTypes.any,
  changeAuthorOptionRemoval: PropTypes.func
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(ChangeAuthorModal);
