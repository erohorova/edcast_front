import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import Popover from 'react-simple-popover';
import uniqBy from 'lodash/uniqBy';
import filter from 'lodash/filter';
import uniq from 'lodash/uniq';
import unescape from 'lodash/unescape';
import Sortable from 'sortablejs';
import IconButton from 'material-ui/IconButton';
import UploadIcon from 'edc-web-sdk/components/icons/Upload';
import DynamicIcon from 'edc-web-sdk/components/icons/DynamicPathway';
import LinkIcon from 'edc-web-sdk/components/icons/Link';
import TextIcon from 'edc-web-sdk/components/icons/Text';
import PollIcon from 'edc-web-sdk/components/icons/Pollv2';
import Scorm from 'edc-web-sdk/components/icons/Scorm';
import colors from 'edc-web-sdk/components/colors/index';
import TileIcon from 'edc-web-sdk/components/icons/TileView';
import ListIcon from 'edc-web-sdk/components/icons/ListView';
import { eclSearch } from 'edc-web-sdk/requests/ecl';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import { addToPathway } from 'edc-web-sdk/requests/pathways';
import {
  deleteFromPathway,
  batchAddToPathway,
  batchRemoveFromPathway
} from 'edc-web-sdk/requests/pathways.v2';

import FlatButton from 'material-ui/FlatButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import Dialog from 'material-ui/Dialog';
import AddIcon from 'material-ui/svg-icons/content/add-circle-outline';
import BookmarkIcon from 'material-ui/svg-icons/action/bookmark';
import SearchIcon from 'material-ui/svg-icons/action/search';

import Card from '../cards/Card';
import CardListView from '../cards/CardListView';
const SmartBite = Loadable({
  loader: () => import('./SmartBite'),
  loading: () => null
});
import convertRichText from '../../utils/convertRichText';

import { open as openSnackBar } from '../../actions/snackBarActions';
import { saveReorderCardIds } from '../../actions/journeyActions';

import ConfirmationModal from '../modals/ConfirmationModal';

import { Permissions } from '../../utils/checkPermissions';
import getCardType from '../../utils/getCardType';
import MoveIcon from 'material-ui/svg-icons/maps/zoom-out-map';
import TextField from 'material-ui/TextField';

const lightPurp = '#acadc1';
const viewColor = '#454560';

class JourneyPartEditor extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      tileView: {
        border: 0,
        padding: 0,
        marginRight: 10,
        width: 14,
        height: 14
      },
      popover: {
        width: '33.0625rem',
        padding: '1rem',
        zIndex: '15',
        position: 'relative',
        top: '23rem',
        margin: '0',
        pointerEvents: 'auto'
      },
      popoverContainer: {
        padding: '0 2.5rem',
        pointerEvents: 'none'
      },
      activeView: {
        opacity: 1
      },
      inactiveView: {
        opacity: 0.5
      },
      flatButton: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      icons: {
        width: '14px',
        height: '14px'
      },
      accordionTitleIcon: {
        fill: 'inherit',
        height: '34px',
        width: '34px'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      },
      addIcon: {
        verticalAlign: 'top'
      },
      confirmationModal: {
        zIndex: 1503
      },
      confirmationContent: {
        transform: 'none'
      },
      removeLabel: {
        textTransform: 'none',
        color: '#fff'
      }
    };

    this.state = {
      activeType: '',
      openEmpty: false,
      openAutoGeneratePopover: false,
      pendingAutoGenerate: false,
      viewRegime: 'Tile',
      createErrorText: '',
      isOpenBlock: true,
      isEditCard: false,
      updatedCard: {},
      smartBites: [],
      section: props.section || {},
      sectionIndex: props.sectionIndex,
      countNewCards: 0,
      popoverAutofocus: false,
      isCardV3: window.ldclient.variation('card-v3', false),
      popoverElm: null
    };
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
    this.confirmRemoveCardFromPathway = this.confirmRemoveCardFromPathway.bind(this);
    this.confirmRemovePathwayFromJourney = this.confirmRemovePathwayFromJourney.bind(this);
    this.confirmDeletePathwayFromJourney = this.confirmDeletePathwayFromJourney.bind(this);
    this.closeRemoveCardModal = this.closeRemoveCardModal.bind(this);
    this.closeRemovePathwayModal = this.closeRemovePathwayModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      section: nextProps.section,
      sectionIndex: nextProps.sectionIndex,
      isRemove: nextProps.isRemove,
      title: nextProps.section.block_title
    });
  }

  componentDidMount() {
    this.activateCardSort();
  }

  async componentWillMount() {
    if (this.props.isEdit) {
      if (this.props.section && this.props.section.block_title) {
        this.setState({ title: this.props.section.block_title });
      }
      if (
        this.props.section &&
        this.props.section.pack_cards &&
        this.props.section.pack_cards.length
      ) {
        let smartBites = [];
        for (let packCard of this.props.section.pack_cards) {
          let id = packCard.id || packCard.card_id;
          let card = await fetchCard(id);
          smartBites.push(card);
        }
        for (let j = 0; j < smartBites.length; j++) {
          if (this.props.section.pack_cards[j].locked) {
            smartBites[j].isLocked = true;
          }
        }
        smartBites.forEach((item, index) => {
          item.isLocked = this.props.section.pack_cards[index].locked;
        });
        this.setState({ smartBites }, () => {
          this.props.updateSectionCards(smartBites, this.state.sectionIndex);
        });
      }
    }
  }

  activateCardSort = () => {
    if (this.state.isOpenBlock) {
      let list = document.getElementById(`listWithHandle-${this.state.sectionIndex}`);
      Sortable.create(list, {
        sort: true,
        handle: '.move-card-btn',
        animation: 150,
        forceFallback: true,
        group: {
          name: `listWithHandle-${this.state.sectionIndex}`
        },
        onUpdate: this.changeOrder
      });
    }
  };

  removePathwayFromJourney = () => {
    let externalPathway = false;
    if (!this.state.section.hidden && this.state.section.state == 'published') {
      externalPathway = true;
    }
    this.setState({
      openConfirmationModal: !this.state.openConfirmationModal,
      externalPathway: externalPathway
    });
  };

  confirmRemovePathwayFromJourney() {
    this.setState({ openConfirmationModal: !this.state.openConfirmationModal });
    this.removeBlock();
  }

  closeRemoveCardModal() {
    if (this.state.openConfirm) {
      this.setState({
        openConfirm: !this.state.openConfirm
      });
    }
  }
  closeRemovePathwayModal() {
    if (this.state.openConfirmationModal) {
      this.setState({
        openConfirmationModal: !this.state.openConfirmationModal
      });
    }
  }

  confirmDeletePathwayFromJourney() {
    this.setState({ openConfirmationModal: !this.state.openConfirmationModal });
    this.deleteBlock();
  }

  removeCardFromPathway = (cardId, cardType, hidden) => {
    let externalCard = false;
    if (hidden === false) {
      externalCard = true;
    }
    this.setState({
      openConfirm: !this.state.openConfirm,
      cardToRemove: { cardId, cardType },
      externalCard: externalCard
    });
  };

  confirmRemoveCardFromPathway = dependentDelete => {
    let { cardId, cardType } = this.state.cardToRemove;
    this.setState({ openConfirm: !this.state.openConfirm });
    const removeFrontendCard = cardIdData => {
      let smartBites = this.state.smartBites;
      smartBites = smartBites.filter(item => item.id !== cardIdData);
      this.setState({ smartBites });
      this.props.updateSectionCards(smartBites, this.state.sectionIndex);
    };
    if (
      this.state.section &&
      this.state.section.card_id &&
      this.state.section.pack_cards &&
      this.state.section.pack_cards.some(item => item.id == cardId)
    ) {
      let type = cardType === 'VideoStream' || cardType === 'video_stream' ? 'VideoStream' : 'Card';
      batchRemoveFromPathway(this.state.section.card_id, [cardId], dependentDelete)
        .then(() => {
          removeFrontendCard(cardId);
        })
        /*eslint handle-callback-err: "off"*/
        .catch(err => {
          this.setState({ createErrorText: 'Error while removing card from section' });
        });
    } else {
      removeFrontendCard(cardId);
    }
  };
  cancelRemoveCardFromPathway = () => {
    this.setState({ openConfirm: !this.state.openConfirm });
  };

  cancelRemovePathwayFromJourney = () => {
    this.setState({ openConfirmationModal: !this.state.openConfirmationModal });
  };

  handleTouchTap = event => {
    event.preventDefault();
    this.setState(
      {
        openEmpty: true,
        closeClick: false,
        createErrorText: '',
        popoverElm: event.target
      },
      () => {
        setTimeout(this.setState({ popoverAutofocus: true }), 200);
      }
    );
  };

  handleRequestClose = e => {
    let targetElement, targetIdent;
    if (e) {
      targetElement = e.target || e.srcElement;
      targetIdent = targetElement.id || targetElement.className;
    }
    if (
      !e ||
      (targetIdent &&
        !~targetIdent.indexOf('fsp') &&
        !~targetIdent.indexOf('fst') &&
        !~targetIdent.indexOf('cropper') &&
        !~targetElement.textContent.indexOf('Upload'))
    ) {
      this.setState({
        openEmpty: false,
        openAutoGeneratePopover: false,
        createErrorText: '',
        updatedCard: {},
        popoverAutofocus: false,
        activeType: ''
      });
    }
  };

  openCardEdit = (e, card, index) => {
    let cardType = getCardType(card) || '';
    let pollPermission =
      (cardType === 'POLL' || cardType === 'QUIZ') && Permissions.has('MANAGE_CARD');

    if ((!!card.attemptCount && card.attemptCount > 0) || (pollPermission && card.hasAttempted)) {
      return;
    }
    this.setState(
      {
        openEmpty: true,
        closeClick: false,
        createErrorText: '',
        isEditCard: true,
        updatedCard: card,
        updatedCardIndex: index - 1,
        activeType: cardType.toLowerCase(),
        popoverElm: e.target
      },
      () => {
        setTimeout(this.setState({ popoverAutofocus: true }), 200);
      }
    );
  };

  changeViewMode = viewRegime => {
    this.setState({ viewRegime });
  };

  async handleAutoGenerateContent(offset) {
    if (!this.state.autoGenerateInput || !this.state.autoGenerateInputNumber) {
      let createErrorText =
        !this.state.autoGenerateInput && !this.state.autoGenerateInputNumber
          ? 'Enter topic and number of cards'
          : !this.state.autoGenerateInput
          ? 'Enter topic'
          : 'Enter number of cards';
      this.setState({ createErrorText });
      return;
    }
    let generateNumber = !!this.state.autoGenerateNumberCalc
      ? this.state.autoGenerateNumberCalc
      : this.state.autoGenerateInputNumber;
    this.setState({ pendingAutoGenerate: true, createErrorText: '' });
    let params = { q: this.state.autoGenerateInput, limit: generateNumber, offset };
    params['content_type[]'] = ['article', 'poll', 'video', 'insight'];
    let items = await eclSearch(params); //.then(items => {
    if (!items.cards.length && !this.state.recallGenerate) {
      this.setState({
        createErrorText: 'Error while adding card to journey. No cards with this topic',
        openEmpty: false,
        openAutoGeneratePopover: false,
        pendingAutoGenerate: false,
        autoGenerateNumberCalc: 0,
        countNewCards: 0,
        popoverAutofocus: false
      });
      return;
    }
    if (!items.cards.length && this.state.recallGenerate) {
      this.props.dispatch(
        openSnackBar(
          `We have less cards by your request. We’ve just generated ${
            this.state.countNewCards
          } cards.`,
          true
        )
      );
      this.setState({
        activeType: '',
        pendingAutoGenerate: false,
        recallGenerate: false,
        countNewCards: 0,
        autoGenerateNumberCalc: 0
      });
      await this.handleRequestClose();
      return;
    }
    let smartBites = uniqBy(this.state.smartBites.concat(items.cards), 'id');
    smartBites = filter(smartBites, el => {
      return el.cardType !== 'journey' && el.cardType !== 'pack';
    });
    let countDiff = smartBites.length - this.state.smartBites.length;
    if (this.state.section.card_id) {
      for (let card of items.cards) {
        let type =
          card.cardType === 'VideoStream' || card.cardType === 'video_stream'
            ? 'VideoStream'
            : 'Card';
        await addToPathway(this.state.section.card_id, card.id, type);
      }
    }
    if (generateNumber > countDiff) {
      await this.setState({
        countNewCards: this.state.countNewCards + countDiff,
        recallGenerate: true,
        autoGenerateNumberCalc: generateNumber - countDiff,
        smartBites
      });
      await this.handleAutoGenerateContent(offset + items.cards.length);
      this.props.updateSectionCards(smartBites, this.state.sectionIndex);
      return;
    }
    this.setState({
      smartBites,
      activeType: '',
      pendingAutoGenerate: false,
      countNewCards: 0,
      autoGenerateNumberCalc: 0
    });
    this.props.updateSectionCards(smartBites, this.state.sectionIndex);
    this.handleRequestClose();
  }

  changeActiveType = (e, activeType) => {
    e.preventDefault();
    if (this.state.updatedCard.id) {
      return;
    }
    this.setState({ activeType });
  };

  saveOutClick = (tempCard, searchText) => {
    this.setState({ tempCard, searchText });
  };

  renderTypeUI = () => {
    switch (this.state.activeType) {
      case '':
        return null;
      case 'autopathway':
        return (
          <div className="row autopathway-block">
            <input
              className="small-12"
              onChange={e => {
                this.setState({ autoGenerateInput: e.target.value });
              }}
              value={this.state.autoGenerateInput}
              type="text"
              placeholder={tr('Enter topic here')}
            />
            <select
              className="card-number-dropdown small-4"
              onChange={e => {
                this.setState({ autoGenerateInputNumber: e.target.value });
              }}
              value={this.state.autoGenerateInputNumber}
            >
              <option value="">{tr('# Cards')}</option>
              {[...new Array(10).keys()].map(i => {
                return (
                  <option key={`${this.props.id}-${i + 1}`} value={i + 1}>
                    {i + 1}
                  </option>
                );
              })}
            </select>
            <div className="auto-generate-action-buttons">
              <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
              <FlatButton
                label={tr(this.state.pendingAutoGenerate ? 'Generating...' : 'Generate')}
                className="preview"
                disabled={this.state.pendingAutoGenerate}
                onTouchTap={this.handleAutoGenerateContent.bind(this, 0)}
                labelStyle={this.styles.flatButton}
                style={
                  this.state.autoGenerateInput &&
                  this.state.autoGenerateInputNumber &&
                  !this.state.pendingAutoGenerate
                    ? this.styles.create
                    : this.styles.disabled
                }
              />
            </div>
          </div>
        );
      default:
        return this.state.updatedCard.id ? (
          <SmartBite
            createdCard={this.createdCard}
            smartBites={this.state.smartBites}
            activeType={this.state.activeType}
            cardType="smartbite"
            card={this.state.updatedCard}
            closeClick={true}
            isPathwayPart={true}
          />
        ) : (
          <SmartBite
            tempCard={this.state.tempCard}
            closeClick={this.state.closeClick}
            searchText={this.state.searchText}
            saveOutClick={this.saveOutClick}
            createdCard={this.createdCard}
            smartBites={this.state.smartBites}
            activeType={this.state.activeType}
            cardType="pathway"
            isPathwayPart={true}
          />
        );
    }
  };

  changeOrder = evt => {
    let smartBites = this.state.smartBites;
    this.setState({ smartBites: [] });
    let removedElement = smartBites.splice(evt.oldIndex, 1);
    smartBites.splice(evt.newIndex, 0, removedElement[0]);
    this.setState({ smartBites });
    this.saveReorder(smartBites);
  };

  saveReorder = smartBites => {
    if (this.state.section.card_id) {
      let arrSections = [];
      arrSections.push({ sectionId: this.state.section.card_id });
      let arrToReorder = [];
      this.state.smartBites.forEach(card => {
        arrToReorder.push(+card.card_id || +card.id);
      });
      arrSections[0].cards = arrToReorder;

      if (this.props.reorderCardIds && this.props.reorderCardIds.length) {
        let arr = this.props.reorderCardIds;
        for (let i = 0; i < arr.length; i++) {
          if (arr[i].sectionId === arrSections[0].sectionId) {
            arr.splice(i, 1);
            arr.push(arrSections[0]);
          } else if (i === arr.length - 1) {
            arr.push(arrSections[0]);
          }
        }
        this.props.dispatch(saveReorderCardIds(arr));
      } else {
        this.props.dispatch(saveReorderCardIds(arrSections));
      }
    }
    this.props.updateSectionCards(smartBites, this.state.sectionIndex);
  };

  lockCard = (card, index) => {
    if (card.showLocked) {
      return;
    }
    let smartBites = this.state.smartBites.map(item => ({ ...item }));
    smartBites[index].isLocked = !smartBites[index].isLocked;
    this.setState({ smartBites }, this.props.lockCard(smartBites, this.state.sectionIndex, index));
  };

  createdCard = data => {
    if (this.state.isEditCard) {
      let smartBites = this.state.smartBites;
      if (data && !!data.card) smartBites[this.state.updatedCardIndex] = data && data.card;
      this.setState({
        smartBites,
        closeClick: false,
        createErrorText: '',
        updatedCard: {},
        activeType: '',
        updatedCardIndex: -1,
        isEditCard: false
      });
      this.handleRequestClose();
      return;
    }

    if (this.state.activeType === 'bookmark' || this.state.activeType === 'smart') {
      this.batchAddCardToBlockJourney(data)
        .then(() => {
          this.setState({ activeType: '' });
          this.handleRequestClose();
        })
        .catch(() => {
          this.handleRequestClose();
          this.setState({ createErrorText: 'Error while adding card to journey' });
        });
    } else {
      if (data && data.id) {
        let smartBites = this.state.smartBites;
        smartBites.push(data.card);
        this.saveReorder(smartBites);
        this.props.updateSectionCards(smartBites, this.state.sectionIndex);
        this.setState({ smartBites, activeType: '', closeClick: true });
        this.handleRequestClose();
      }
    }
    if (!data) {
      this.handleRequestClose();
    }
  };

  removeBlock = () => {
    this.props.removeBlock(this.state.sectionIndex);
  };

  deleteBlock = () => {
    this.props.deleteBlock(this.state.sectionIndex);
  };

  openBlock = () => {
    this.setState({ isOpenBlock: !this.state.isOpenBlock }, () => {
      this.activateCardSort();
    });
  };

  batchAddCardToBlockJourney = cards => {
    return new Promise(resolve => {
      cards.forEach(card => {
        card.typeOfCreate = this.state.activeType;
      });
      let smartBites = this.state.smartBites.concat(cards);
      if (this.state.section && this.state.section.card_id) {
        let ids = uniq(cards.map(card => card.id || card.cardId));
        batchAddToPathway(this.state.section.card_id, ids, 'Card');
      }
      this.setState({ smartBites, activeType: '', closeClick: true }, resolve);
      this.props.updateSectionCards(smartBites, this.state.sectionIndex);
    });
  };

  changeTitle = e => {
    this.setState({ title: e.target.value });
    this.props.updateSectionTitle(e.target.value, this.state.sectionIndex);
  };

  addToLeap = (correctId, wrongId, cardId, leapId) => {
    let leapObj = {
      correctId,
      wrongId,
      cardId,
      leapId,
      sectionIndex: this.state.sectionIndex
    };
    this.props.addToLeap(leapObj);
  };

  render() {
    let isAbleUpload = Permissions['enabled'] !== undefined && Permissions.has('UPLOAD');
    let isAbleUploadScorm =
      Permissions['enabled'] !== undefined && Permissions.has('UPLOAD_SCORM_CONTENT'); //EP-15959: removed autoMarkAsCompleteScorm
    return (
      <div className="journey-part__container" id={this.props.id}>
        <div
          className={`flex-center ${
            this.state.isOpenBlock ? 'journey-section-header_open' : 'journey-section-header_close'
          } journey-section-header`}
        >
          <input
            type="text"
            className="journey-section-header__input"
            style={{ width: `calc(100% - ${this.props.isRemove ? '160px' : '70px'})` }}
            placeholder={tr('ENTER TITLE HERE...')}
            value={unescape(this.state.title || '')}
            onChange={this.changeTitle}
          />
          {this.props.isRemove && (
            <FlatButton
              hoverColor="transparent"
              label={tr('REMOVE')}
              labelPosition="before"
              className="accordion-title journey__accordion-title"
              style={this.styles.removeLabel}
              onClick={this.removePathwayFromJourney}
            />
          )}
          <span>|</span>
          <FlatButton
            hoverColor="transparent"
            className="accordion-title journey__accordion-title"
            onClick={this.openBlock}
            secondary={true}
            icon={
              (this.state.isOpenBlock && (
                <NavigationArrowDropDown
                  aria-label="Add title field"
                  style={{
                    ...this.styles.accordionTitleIcon,
                    ...this.styles.accordionTitleIcon_open
                  }}
                />
              )) ||
              (!this.state.isOpenBlock && (
                <NavigationArrowDropUp
                  aria-label="remove title field"
                  style={{
                    ...this.styles.accordionTitleIcon,
                    ...this.styles.accordionTitleIcon_close
                  }}
                />
              ))
            }
          />
          <div className="move-pathway-btn">
            <IconButton
              aria-label="drag pathway title"
              style={this.styles.move}
              iconStyle={this.styles.moveIcon}
            >
              <MoveIcon color="white" />
            </IconButton>
          </div>
        </div>
        {this.state.isOpenBlock && (
          <div>
            {this.props.dontEdit && (
              <div className="dont-edit-over-image">
                <div className="dont-edit-message">
                  {tr('You can’t update cards in this section. The start date has already begun.')}
                </div>
              </div>
            )}
            <div className="tile-block">
              <IconButton
                aria-label="tile view"
                className="editor-image-btn-icon"
                onTouchTap={this.changeViewMode.bind(this, 'Tile')}
                iconStyle={this.styles.icons}
                style={{
                  ...this.styles.tileView,
                  ...{ opacity: this.state.viewRegime === 'Tile' ? 1 : 0.5 }
                }}
              >
                <span tabIndex={-1} className="hideOutline">
                  <TileIcon style={this.styles.icons} color={viewColor} viewBox="0 0 18 18" />
                </span>
              </IconButton>
              <IconButton
                aria-label="list view"
                className="editor-image-btn-icon"
                onTouchTap={this.changeViewMode.bind(this, 'List')}
                iconStyle={this.styles.icons}
                style={{
                  ...this.styles.tileView,
                  ...{ opacity: this.state.viewRegime === 'List' ? 1 : 0.5 }
                }}
              >
                <span tabIndex={-1} className="hideOutline">
                  <ListIcon style={this.styles.icons} color={viewColor} viewBox="0 0 18 18" />
                </span>
              </IconButton>
              <span className="tile-text">{tr(`${this.state.viewRegime} View`)}</span>
            </div>
            <div className="smartbites-block">
              <div
                className={
                  this.state.viewRegime === 'Tile'
                    ? this.state.isCardV3
                      ? 'tile-view'
                      : 'custom-card-container tile-view'
                    : 'custom-card-container'
                }
              >
                <div className="three-card-column" id={`listWithHandle-${this.state.sectionIndex}`}>
                  {this.state.smartBites.map((card, index) => {
                    let cardType = getCardType(card) || '';
                    let pollPermission =
                      (cardType === 'POLL' || cardType === 'QUIZ') &&
                      Permissions.has('MANAGE_CARD');
                    let hideEdit = false;
                    if (
                      (!!card.attemptCount && card.attemptCount > 0) ||
                      (pollPermission && card.hasAttempted)
                    ) {
                      hideEdit = true;
                    }
                    if (this.state.viewRegime === 'Tile') {
                      return (
                        <Card
                          card={card}
                          showControls={true}
                          key={index}
                          pathwayEditor={true}
                          index={index + 1}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          author={card.author}
                          user={this.state.currentUser}
                          removeCardFromPathway={this.removeCardFromPathway.bind(
                            this,
                            card.id,
                            card.cardType,
                            card.hidden
                          )}
                          moreCards={true}
                          cardsList={this.state.smartBites}
                          lockCard={this.lockCard.bind(this, card, index)}
                          pathway={this.state.section}
                          addToLeap={this.addToLeap}
                          arrToLeap={this.props.arrToLeap}
                          openCardEdit={this.openCardEdit}
                          hideEdit={hideEdit}
                        />
                      );
                    } else {
                      return (
                        <CardListView
                          card={card}
                          key={index}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          index={index + 1}
                          author={card.author}
                          user={this.state.currentUser}
                          removeCardFromPathway={this.removeCardFromPathway.bind(
                            this,
                            card.id,
                            card.cardType,
                            card.hidden
                          )}
                          lockCard={this.lockCard.bind(this, card, index)}
                          pathwayEditor={true}
                          openCardEdit={this.openCardEdit}
                          hideEdit={hideEdit}
                        />
                      );
                    }
                  })}
                  {!this.props.dontEdit && (
                    <div
                      className={`${
                        this.state.viewRegime === 'Tile'
                          ? `${
                              this.state.isCardV3
                                ? 'card-v3 card-v3__tile-view'
                                : 'card-v2 small-12 column more-cards'
                            }`
                          : ''
                      } ${this.isNewTileCard ? 'card-v2__wide' : 'card-v2__simple-size'}`}
                    >
                      <a
                        href="#"
                        className={
                          this.state.viewRegime === 'Tile'
                            ? `${this.state.isCardV3 ? 'empty-smartbite-v3' : ''} empty-smartbite`
                            : 'empty-smartbite-list'
                        }
                        key={'empty'}
                        onClick={this.handleTouchTap}
                      >
                        <div
                          className={this.state.viewRegime === 'Tile' ? 'text-center' : 'text-left'}
                        >
                          <AddIcon style={this.styles.addIcon} color={viewColor} />
                          <span className="empty-title">{tr('Add SmartCard')}</span>
                        </div>
                      </a>
                    </div>
                  )}
                  <Popover
                    placement="top"
                    container={this.props.parentThis}
                    style={this.styles.popover}
                    containerStyle={this.styles.popoverContainer}
                    target={this.state.popoverElm}
                    show={this.state.openEmpty}
                    zDepth="100"
                    onHide={this.handleRequestClose}
                  >
                    <div>
                      <div className="row cardTypePopupIsActive">
                        <TextField
                          autoFocus={this.state.popoverAutofocus}
                          name="smartCardJourneyPartEditor"
                          className="hiddenTextField"
                        />
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'link')}
                          style={
                            this.state.activeType === 'link' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card link-card">
                            <LinkIcon fill="white" viewBox="-1 0 20 20" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Link')}</div>
                        </a>
                        {isAbleUpload ? (
                          <a
                            href="#"
                            role="button"
                            className={'small-3 card-circle'}
                            onClick={e => this.changeActiveType(e, 'upload')}
                            style={
                              this.state.activeType === 'upload' || !this.state.activeType
                                ? this.styles.activeView
                                : this.styles.inactiveView
                            }
                          >
                            <div className="icon-card upload-card">
                              <UploadIcon color={'white'} viewBox="0 0 24 24" />
                            </div>
                            <div className="text-center popover-card-text">{tr('Upload')}</div>
                          </a>
                        ) : null}
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'poll')}
                          style={
                            this.state.activeType === 'poll' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card poll-card">
                            <PollIcon fill="white" viewBox="-3 0 24 24" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Poll')}</div>
                        </a>
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'quiz')}
                          style={
                            this.state.activeType === 'quiz' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card quiz-card">
                            <div className="quiz-card-icon">Q</div>
                          </div>
                          <div className="text-center popover-card-text">{tr('Quiz')}</div>
                        </a>
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'autopathway')}
                          style={
                            this.state.activeType === 'autopathway' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card auto-generate-card">
                            <DynamicIcon viewBox="0 0 24 24" color={'white'} />
                          </div>
                          <div className="text-center popover-card-text">
                            {tr('Dynamic Pathway')}
                          </div>
                        </a>
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'bookmark')}
                          style={
                            this.state.activeType === 'bookmark' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card bookmark-card">
                            <BookmarkIcon viewBox="-1 0 26 25" color={'white'} />
                          </div>
                          <div className="text-center popover-card-text">{tr('From Bookmark')}</div>
                        </a>
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'smart')}
                          style={
                            this.state.activeType === 'smart' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card smart-card">
                            <SearchIcon color={'white'} />
                          </div>
                          <div className="text-center popover-card-text">
                            {tr('Search SmartCard')}
                          </div>
                        </a>
                        <a
                          href="#"
                          role="button"
                          className={'small-3 card-circle'}
                          onClick={e => this.changeActiveType(e, 'text')}
                          style={
                            this.state.activeType === 'text' || !this.state.activeType
                              ? this.styles.activeView
                              : this.styles.inactiveView
                          }
                        >
                          <div className="icon-card text-card">
                            <TextIcon fill="white" viewBox="0 0 27 15" />
                          </div>
                          <div className="text-center popover-card-text">{tr('Text')}</div>
                        </a>
                        {isAbleUploadScorm && (
                          <a
                            href="#"
                            role="button"
                            className="small-3 card-circle"
                            onClick={e => this.changeActiveType(e, 'scorm')}
                            style={
                              this.state.activeType === 'scorm' || !this.state.activeType
                                ? this.styles.activeView
                                : this.styles.inactiveView
                            }
                          >
                            <div className="icon-card text-card">
                              <Scorm fill="white" viewBox="-4 0 27 17" />
                            </div>
                            <div className="text-center popover-card-text">{tr('SCORM')}</div>
                          </a>
                        )}
                      </div>
                      <div className="additional-block">{this.renderTypeUI()}</div>
                    </div>
                  </Popover>
                </div>
              </div>
            </div>
          </div>
        )}
        {this.state.externalCard
          ? this.state.openConfirm && (
              <Dialog
                open={this.state.openConfirm}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={'Are you sure you want to remove this card from the pathway?'}
                  cancelClick={this.cancelRemoveCardFromPathway}
                  callback={() => this.confirmRemoveCardFromPathway(false)}
                  confirmBtnTitle="Yes"
                  cancelBtnTitle="No"
                />
              </Dialog>
            )
          : this.state.openConfirm && (
              <Dialog
                open={this.state.openConfirm}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={tr('What action would you like to take on this SmartCard?')}
                  cancelClick={() => this.confirmRemoveCardFromPathway(true)}
                  callback={() => this.confirmRemoveCardFromPathway(false)}
                  confirmBtnTitle="Remove from Journey"
                  cancelBtnTitle="Permanently Delete"
                  closeModal={this.closeRemoveCardModal}
                />
              </Dialog>
            )}
        {this.state.externalPathway
          ? this.state.openConfirmationModal && (
              <Dialog
                open={this.state.openConfirmationModal}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={'Are you sure you want to remove this pathway from the journey?'}
                  cancelClick={this.cancelRemovePathwayFromJourney}
                  callback={this.confirmRemovePathwayFromJourney}
                  confirmBtnTitle="Yes"
                  cancelBtnTitle="No"
                />
              </Dialog>
            )
          : this.state.openConfirmationModal && (
              <Dialog
                open={this.state.openConfirmationModal}
                autoScrollBodyContent={false}
                style={this.styles.confirmationModal}
                contentStyle={this.styles.confirmationContent}
              >
                <ConfirmationModal
                  title={null}
                  noNeedClose={true}
                  message={tr('What action would you like to take on this Pathway?')}
                  cancelClick={this.confirmDeletePathwayFromJourney}
                  callback={this.confirmRemovePathwayFromJourney}
                  confirmBtnTitle="Remove from Journey"
                  cancelBtnTitle="Permanently Delete"
                  closeModal={this.closeRemovePathwayModal}
                />
              </Dialog>
            )}
      </div>
    );
  }
}

JourneyPartEditor.propTypes = {
  section: PropTypes.object,
  currentUser: PropTypes.object,
  reorderCardIds: PropTypes.object,
  parentThis: PropTypes.object,
  dontEdit: PropTypes.bool,
  isRemove: PropTypes.bool,
  isEdit: PropTypes.bool,
  arrToLeap: PropTypes.any,
  id: PropTypes.number,
  sectionIndex: PropTypes.number,
  updateSectionCards: PropTypes.func,
  lockCard: PropTypes.func,
  addToLeap: PropTypes.func,
  updateSectionTitle: PropTypes.func,
  removeBlock: PropTypes.func,
  deleteBlock: PropTypes.func,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    reorderCardIds: state.journey.toJS().reorderCardIds,
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(JourneyPartEditor);
