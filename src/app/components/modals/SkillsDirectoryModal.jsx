import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { Menu, AsyncTypeahead } from 'react-bootstrap-typeahead';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';

import { getRoles } from 'edc-web-sdk/requests/skillsDirectory';

import { close } from '../../actions/modalActions';

class SkillsDirectoryModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      roles: [],
      disableSave: true,
      role: {}
    };
  }

  componentDidMount() {
    let roles = getRoles();
    this.setState({ roles });
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  SaveClickHandler = role => {
    this.setState({ role, disableSave: false });
    this.props.changeRole(role);
    this.props.dispatch(close());
  };

  _renderMenu = (results, menuProps) => {
    const menuData = (results || []).map(result => {
      return [
        <li
          onClick={() => {
            this.SaveClickHandler(result);
          }}
        >
          <a>
            <div className="user-search-name" style={{ fontSize: '0.75rem' }}>
              {result.name}
            </div>
          </a>
        </li>
      ];
    });

    return <Menu {...menuProps}>{menuData}</Menu>;
  };

  _handleSearch = () => {};

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    return (
      <div className="skills-directory-modal">
        <div className="skills-directory-header">
          <div className="skills-directory-title">{tr('Select a Role')}</div>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div className="user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={this.state.roles}
            placeholder={'Search User Role'}
            onSearch={this._handleSearch}
            submitFormOnEnter={false}
          />
          <p>{tr('Search Role')}</p>
          <div className="buttons">
            <SecondaryButton label={tr('Cancel')} onTouchTap={this.closeModal} />
            <PrimaryButton
              label={tr('Save & Go')}
              pending={false}
              pendingLabel={tr('Saving...')}
              onTouchTap={() => this.saveClickHandler()}
              disabled={this.state.disableSave}
            />
          </div>
        </div>
      </div>
    );
  }
}

SkillsDirectoryModal.propTypes = {
  changeRole: PropTypes.func
};

export default connect()(SkillsDirectoryModal);
