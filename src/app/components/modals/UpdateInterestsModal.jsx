import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import AutoComplete from 'material-ui/AutoComplete';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import _ from 'lodash';
import { closeInterestModal } from '../../actions/modalActions';
import { updateUserInterest } from '../../actions/usersActions';
import { users, topics } from 'edc-web-sdk/requests/index';
import FlashAlert from '../common/FlashAlert';
import { tr } from 'edc-web-sdk/helpers/translations';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { getUserPwcRecords } from 'edc-web-sdk/requests/users.v2';
import { fetchUserExpertise } from 'edc-web-sdk/requests/expertise';
import InterestModalOnboardV4 from './InterestModalOnboardV4';
import TextField from 'material-ui/TextField';
import * as upshotActions from '../../actions/upshotActions';
class UpdateInterestsModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '7px'
      },
      smallIcon: {
        width: '16px',
        height: '16px',
        margin: '6px'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`
      },
      chipLabel: {
        paddingRight: '27px'
      },
      selectedLabel: {
        maxWidth: 'calc(100% - 1px)',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        color: `${colors.primary}`
      },
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer'
      },
      selectedTopicLabel: {
        color: 'white'
      },
      biabox: {
        position: 'absolute',
        zIndex: '1',
        padding: '12px 12px 10px 0',
        boxShadow: '0px 1px 4px #000000',
        background: '#ffffff',
        left: '0',
        top: '40px',
        height: '115px',
        fontSize: '12px'
      },
      radioIcon: {
        width: '16px',
        height: '16px',
        borderColor: '#979797',
        color: '#00a1e1',
        marginTop: '1px'
      },
      radioLabel: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '0.8125rem',
        lineHeight: 1.47,
        textAlign: 'left',
        color: '#6f708b',
        width: '100%',
        marginLeft: '1px'
      },
      okLink: {
        float: 'right',
        color: '#6f6f8b',
        fontWeight: 'bold',
        fontSize: '14px'
      },
      cancelLink: {
        float: 'left',
        color: '#6f6f8b',
        fontSize: '14px',
        marginLeft: '5px'
      }
    };

    this.state = {
      userInterests: [],
      suggestedInterests: [],
      interestsSearchText: '',
      interestsSearch: [],
      error: '',
      postPending: false,
      topics: [],
      topicId: '',
      skillsArray: [],
      page: 1,
      pageNo: 1,
      selectedSkillsIds: [],
      isLastTopicPage: true,
      showAlert: false,
      errorMessage: '',
      learningTopics: [],
      skillSource: [],
      searchText: '',
      onboardingMicroservice: window.ldclient.variation('onboarding-version', 'v1') !== 'v1',
      onboardingVersion: window.ldclient.variation('onboarding-version', 'v1'),
      winterstorm: window.ldclient.variation('winterstorm', false),
      showPWCAssesment: window.ldclient.variation('show-pwc', false),
      learningTopicIds: [],
      defaultTopicIds: [],
      selectedInterest: [],
      biaModalVisible: null,
      topicLevelObj: {},
      topicLevelObjCopy: {},
      userLevelObj: {},
      levelStr: {
        1: 'Beginner',
        2: 'Intermediate',
        3: 'Advanced'
      },
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      pwcObj: {}
    };
    this.showBIA = !!this.props.team.config.enabled_bia;

    this.handleExpertiseInput = this.handleExpertiseInput.bind(this);
    this.addExpertiseHandler = this.addExpertiseHandler.bind(this);
    this.selectExpertise = this.selectExpertise.bind(this);
    this.deleteExpertise = this.deleteExpertise.bind(this);
    this.handleClickCancel = this.handleClickCancel.bind(this);
    this.handleClickUpdate = this.handleClickUpdate.bind(this);
    this.openBiaModal = this.openBiaModal.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
  }

  componentDidMount() {
    topics
      .getSuggestedTopicsForUserOnboarding(1, 12)
      .then(topicsData => {
        this.setState({
          topics: topicsData.domains,
          isLastTopicPage: topicsData.domains.length < 12 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateInterestsModal.getSuggestedTopicsForUserOnboarding.func : ${err}`
        );
      });

    let _this = this;

    if (this.props.currentUser.id) {
      let default_topics = _this.props.onboarding.defaultTopics || [];
      let defaultTopicIds = [];

      default_topics.map(function(topic) {
        defaultTopicIds.push(topic.topic_id);
      });
      let userLevelObj = {};
      let interests = (
        (this.props.currentUser &&
          this.props.currentUser.profile &&
          this.props.currentUser.profile.learningTopics) ||
        []
      ).map(ele => ele);
      interests.map((interest, index) => {
        if (interest.level) {
          userLevelObj[interest.topic_id] = interest.level;
        }

        if (
          this.state.onboardingVersion !== 'v4' &&
          !!this.state.onboardingMicroservice &&
          defaultTopicIds.length &&
          defaultTopicIds.indexOf(interest.topic_id) == -1
        ) {
          interests.splice(index, 1);
        }
      });

      let skillIds = [];
      if (interests) {
        for (let i = 0; i < interests.length; i++) {
          skillIds = skillIds.concat(interests[i].topic_id);
        }
      }

      let interestsLimit =
        (this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options.interests_limit) ||
        3;

      if (interests.length > interestsLimit) {
        let interestsLimitMsg = interestsLimit > 1 ? interestsLimit + ' topics' : 'one topic';
        let interestsLimitDiff = interests.length - interestsLimit;
        let interestsLimitDiffMsg =
          interestsLimitDiff > 1 ? interestsLimitDiff + ' topics' : 'one topic';
        this.setState({
          showAlert: true,
          errorMessage: `${tr('You can select a maximum of only %{limit}. Please deselect %{diff} and update', {limit: tr(interestsLimitMsg), diff: tr(interestsLimitDiffMsg)})}`
        });
      }

      this.setState({
        skillsArray: interests ? interests : [],
        selectedSkillsIds: skillIds,
        selectedInterest: skillIds,
        userLevelObj: userLevelObj
      });
    }

    this.handleSkillInput();

    // getting PWC records
    if (this.state.showPWCAssesment) {
      getUserPwcRecords(this.props.currentUser.id)
        .then(data => {
          let pwcRecords = data.pwcRecords || [];

          let pwcObj = {};

          pwcRecords.map(function(elem, index) {
            pwcObj[elem.skill.topic_id] = elem.level;
          });

          this.setState({
            pwcObj: pwcObj
          });
        })
        .catch(err => {
          console.error(`Error in UpdateInterestsModal.getUserPwcRecords.func : ${err}`);
        });
    }
  }

  handleExpertiseInput(text) {
    this.setState({ interestsSearchText: text });
    if (text.length >= 3) {
      this.fetchTopicSkills(text);
    }
  }

  addExpertiseHandler(text, index) {
    this.selectExpertise(text);
    setTimeout(() => {
      this.refs.interestsField.setState({ searchText: '' });
      this.refs.interestsField.focus();
    }, 300);
  }

  selectExpertise(interests) {
    if (this.state.interests.length < 3) {
      // FIXME: interest is not defined
      // let updatedUserInterests = _.concat(this.state.interests, interest);
      // let updatedSuggestedInterests = _.without(this.state.suggestedInterests, interest);
      let updatedUserInterests = _.concat(this.state.interests);
      let updatedSuggestedInterests = _.without(this.state.suggestedInterests);
      this.setState({
        interests: updatedUserInterests,
        suggestedInterests: updatedSuggestedInterests,
        error: ''
      });
    } else {
      this.setState({ error: 'You can only select 3 at this time' });
    }
  }

  deleteExpertise(interest) {
    // FIXME: skill is not defined
    // let updatedUserInterests = _.without(this.state.skillsArray, skill);
    let updatedUserInterests = _.without(this.state.skillsArray);

    this.setState({
      skillsArray: updatedUserInterests
    });
  }

  handleClickTopic(topicId, pageNo) {
    this.state.topicId = topicId;
    let page = 1;

    topics
      .fetchTopicSkills(topicId, page)
      .then(data => {
        this.setState({
          skills: data.topics,
          [`${topicId}-lastPage`]: data.topics.length < 7 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateInterestsModal.handleClickTopic.fetchTopicSkills.func : ${err}`
        );
      });

    this.setState({ [`pageNo-${topicId}`]: page });
  }

  handleClickSkill(skill) {
    let skillObj = {
      topic_name: skill.name,
      topic_label: skill.label,
      topic_id: skill.id,
      domain_name: skill.domain.name,
      domain_id: skill.domain.id,
      domain_label: skill.domain.label
    };
    let skillsArray = this.state.skillsArray;

    let selectedSkillsIds = this.state.selectedSkillsIds;

    if (selectedSkillsIds.length == 3) {
      this.setState({
        showAlert: true,
        errorMessage: tr(
          "You can select a maximum of three topics. Don't worry you can always change them again here."
        )
      });
    }
    if (selectedSkillsIds.indexOf(skill.id) == -1 && selectedSkillsIds.length < 3) {
      skillsArray.push(skillObj);
      selectedSkillsIds.push(skill.id);

      this.setState({
        skillsArray: skillsArray,
        selectedSkillsIds: selectedSkillsIds
      });
    }
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  handleRemoveSkill(id) {
    let selectedSkillsIds = this.state.selectedSkillsIds;
    let skillsArray = this.state.skillsArray;
    var index = selectedSkillsIds.indexOf(id);
    let newSkillsArray = [];
    if (index > -1) {
      selectedSkillsIds.splice(index, 1);
      skillsArray.map(skill => {
        if (selectedSkillsIds.indexOf(skill.topic_id) > -1) {
          newSkillsArray.push(skill);
        }
      });
      skillsArray = newSkillsArray;
    }
    this.setState({
      selectedSkillsIds: selectedSkillsIds,
      skillsArray: skillsArray
    });
  }

  handleClickCancel() {
    this.props.dispatch(closeInterestModal());
  }

  handleClickUpdate() {
    let skillsArray = this.state.skillsArray;
    this.setState({ postPending: true });
    if (this.state.skillsArray == 0) {
      this.setState({
        showAlert: true,
        errorMessage: tr('You should have at least 1 topic selected.'),
        postPending: false
      });
      return;
    }

    for (let skill of skillsArray) {
      if (!this.showBIA && skill.level) {
        delete skill.level;
      }
    }

    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['LEARNING_GOALS'], {
        learningGoals: skillsArray
      });
    }

    this.props
      .dispatch(
        updateUserInterest(
          skillsArray,
          this.props.currentUser.id,
          this.props.currentUser.profile.id
        )
      )
      .then(response => {
        this.setState({ postPending: false });
        this.handleClickCancel();
      })
      .catch(err => {
        console.error(`Error in UpdateInterestsModal.updateUserInterest.func : ${err}`);
      });
  }

  handleClickViewMoreTopic() {
    this.setState({ page: this.state.page + 1 });
    let topicsArray = this.state.topics;
    return topics
      .getSuggestedTopicsForUserOnboarding(this.state.page + 1, 12)
      .then(topicsData => {
        this.setState({
          topics: topicsArray.concat(topicsData.domains),
          isLastTopicPage: topicsData.domains.length < 12 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateInterestsModal.handleClickViewMoreTopic.getSuggestedTopicsForUserOnboarding.func : ${err}`
        );
      });
  }

  handleClickViewMoreSkill() {
    let topicId = this.state.topicId;
    let currentSkills = this.state.skills;
    let page = this.state[`pageNo-${topicId}`] + 1;
    topics
      .fetchTopicSkills(topicId, page)
      .then(data => {
        this.setState({
          skills: currentSkills.concat(data.topics),
          [`${topicId}-lastPage`]: data.topics.length < 7 ? true : false
        });
      })
      .catch(err => {
        console.error(
          `Error in UpdateInterestsModal.handleClickViewMoreSkill.fetchTopicSkills.func : ${err}`
        );
      });
    this.setState({ [`pageNo-${topicId}`]: page });
  }

  handleSkillInput = (value = '') => {
    this.setState({
      searchText: value
    });
    topics
      .queryTopics(value)
      .then(topicsData => {
        this.setState({
          skillSource: topicsData.topics
        });
      })
      .catch(err => {
        console.error(`Error in UpdateInterestsModal.handleSkillInput.queryTopics.func : ${err}`);
      });
  };

  selectSkillInput = skill => {
    if (skill.id) {
      let learningTopics = this.state.skillsArray;
      let filteredTopics = learningTopics.filter(function(obj) {
        return skill.id == obj.topic_id;
      });
      let interestsLimit =
        (this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options.interests_limit) ||
        3;
      if (learningTopics.length > interestsLimit - 1) {
        this.setState({
          showAlert: true,
          errorMessage: `${tr("You can select a maximum of %{limit} topics. Please deselect a topic before selecting another one. Don't worry you can always change them again here", {limit: interestsLimit})}`
        });
        this.setState({
          searchText: ''
        });
        return;
      }
      if (filteredTopics.length > 0) {
        this.setState({ showAlert: true, errorMessage: tr('The topic is already added!') });
        this.setState(
          {
            searchText: ''
          },
          function() {
            this.handleSkillInput();
          }
        );
        return;
      }
      let learningTopic = {
        domain_id: skill.domain.id,
        domain_label: skill.domain.label,
        domain_name: skill.domain.name,
        topic_id: skill.id,
        topic_label: skill.label,
        topic_name: skill.name
      };
      learningTopics.push(learningTopic);
      this.setState(
        {
          skillsArray: learningTopics,
          searchText: ''
        },
        function() {
          this.handleSkillInput();
        }
      );
    }
  };

  removeTopicHandler(id) {
    let learningTopics = this.state.skillsArray;
    learningTopics = learningTopics.filter(function(obj) {
      return obj.topic_id !== id;
    });

    this.setState({
      skillsArray: learningTopics,
      selectedInterest: learningTopics.map(topic => topic.domain_id)
    });
  }

  onSelectTopics = topic => {
    let topicObj = {
      topic_name: topic.topic_name,
      topic_label: topic.topic_label,
      topic_id: topic.topic_id,
      domain_name: topic.domain_name,
      domain_id: topic.domain_id,
      domain_label: topic.domain_label
    };
    if (this.showBIA) {
      topicObj['level'] =
        this.state.userLevelObj[topic.topic_id] ||
        this.state.topicLevelObj[topic.topic_id] ||
        this.state.pwcObj[topic.topic_id] ||
        1;
    }
    let interestsLimit =
      (this.props.team.config &&
        this.props.team.config.limit_options &&
        this.props.team.config.limit_options.interests_limit) ||
      3;
    let default_topics = this.props.onboarding.defaultTopics;

    let learning_topics = this.state.skillsArray;
    let learningTopicIds = learning_topics.map(function(learning_topic) {
      return learning_topic.topic_id;
    });
    let index = learningTopicIds.indexOf(topicObj.topic_id);
    let ary = this.state.selectedInterest;
    let shouldRemove =
      _.includes(learningTopicIds, topicObj.topic_id) && _.includes(ary, topicObj.topic_id);

    if (this.state.selectedInterest.length > interestsLimit - 1) {
      if (shouldRemove == false) {
        this.handleCancelClick(learning_topics[0].topic_id);
        ary.splice(0, 1);
        learning_topics.splice(0, 1);
        ary = _.uniq([...this.state.selectedInterest, topicObj.topic_id]);
        learning_topics = _.uniq([...learning_topics, topicObj]);
        let interestsLimitMsg =
          interestsLimit > 1 ? `${interestsLimit} ${tr('topics')}` : `${tr('one topic')}`;
        this.setState({
          showAlert: true,
          errorMessage: `${tr("You can select a maximum of %{limit}. Please deselect a topic before selecting another one. Don't worry you can always change them again here", {limit: interestsLimitMsg})}`
        });
      } else {
        this.handleCancelClick(learning_topics[index].topic_id);
        ary = _.without(ary, topicObj.topic_id);
        learning_topics.splice(index, 1);
      }
    } else {
      if (shouldRemove) {
        this.handleCancelClick(learning_topics[0].topic_id);
        ary = _.without(ary, topicObj.topic_id);
        learning_topics.splice(index, 1);
      } else {
        for (var element of learning_topics) {
          this.handleCancelClick(element.topic_id);
        }
        ary = _.uniq([...this.state.selectedInterest, topicObj.topic_id]);
        learning_topics = _.uniq([...learning_topics, topicObj]);
      }
    }
    this.setState({
      selectedInterest: ary,
      skillsArray: learning_topics
    });
  };

  setChipStyle = (topic, allow = true) => {
    return _.includes(this.state.selectedInterest, topic.topic_id) && allow
      ? `${colors.primary}`
      : '#ffffff';
  };

  ifTopicSelected = topic => {
    return _.includes(this.state.selectedInterest, topic.topic_id);
  };

  openBiaModal(e, topic) {
    e.stopPropagation();
    this.setState({
      biaModalVisible: topic.topic_id
    });
  }

  handleLevelClick(topic_id, topic_level) {
    let topicLevelObj = Object.assign(this.state.topicLevelObjCopy, this.state.topicLevelObj);
    topicLevelObj[topic_id] = topic_level;
    this.setState({ topicLevelObjCopy: topicLevelObj });
  }

  handleOkClick(e, topic_id) {
    e.stopPropagation();
    e.preventDefault();
    let topicLevelObj = this.state.topicLevelObj;
    let userLevelObj = this.state.userLevelObj;
    if (this.state.topicLevelObjCopy[topic_id] || this.state.pwcObj[topic_id]) {
      delete userLevelObj[topic_id];
      topicLevelObj[topic_id] =
        this.state.topicLevelObjCopy[topic_id] || this.state.pwcObj[topic_id];

      let skillsArray = this.state.skillsArray;
      skillsArray.map(function(elem, index) {
        if (elem.topic_id == topic_id) {
          skillsArray[index]['level'] = topicLevelObj[topic_id];
        }
      });

      this.setState({ topicLevelObj, userLevelObj, biaModalVisible: null, skillsArray });
    }
  }

  handleCancelClick(topic_id, e) {
    e && e.stopPropagation();
    e && e.preventDefault();

    let _this = this;

    let topicLevelObjCopy = this.state.topicLevelObjCopy;
    topicLevelObjCopy = Object.assign(
      topicLevelObjCopy,
      this.state.userLevelObj,
      this.state.topicLevelObj
    );
    this.setState({
      topicLevelObjCopy
    });
    let skillsArray = this.state.skillsArray;
    skillsArray.map(function(elem, index) {
      if (elem.topic_id == topic_id) {
        skillsArray[index]['level'] =
          _this.state.userLevelObj[topic_id] || _this.state.pwcObj[topic_id] || 1;
      }
    });

    this.setState({
      biaModalVisible: null,
      skillsArray
    });
  }

  selectTopicHandler = topic => () => {
    let topicObj = {
      topic_name: topic.topic_name,
      topic_label: topic.topic_label,
      topic_id: topic.topic_id,
      domain_name: topic.domain_name,
      domain_id: topic.domain_id,
      domain_label: topic.domain_label
    };

    let skillsArray = this.state.skillsArray;
    let selectedInterest = this.state.selectedInterest;
    let interestsLimit =
      (this.props.team.config &&
        this.props.team.config.limit_options &&
        this.props.team.config.limit_options.interests_limit) ||
      3;
    let index = _.findIndex(skillsArray, topicObj);

    if (!~index && skillsArray.length < interestsLimit) {
      skillsArray.push(topicObj);
      selectedInterest.push(topicObj.domain_id);
      this.setState({
        skillsArray,
        selectedInterest
      });
    } else if (skillsArray.length >= interestsLimit) {
      let interestsLimitMsg = interestsLimit > 1 ? interestsLimit + ' topics' : 'one topic';
      this.setState({
        showAlert: true,
        errorMessage: `${tr("You can select a maximum of %{limit}. Please deselect a topic before selecting another one. Don't worry you can always change them again here.", {limit: tr(interestsLimitMsg)})}`
      });
    } else {
      this.setState({
        showAlert: true,
        errorMessage: tr('Similar topic is already selected')
      });
    }
  };

  //TODO: remove 'isSuggested' when finish suggest
  goalChipRender(element, index, isSuggested = false) {
    if (this.state.onboardingMicroservice) {
      return (
        <div>
          <Chip
            className="active-chip"
            key={index}
            ref={'defaultTopics_' + index}
            style={this.styles.chip}
            labelStyle={this.styles.chipLabel}
            backgroundColor={this.setChipStyle(element, !isSuggested)}
            onClick={
              !isSuggested
                ? this.onSelectTopics.bind(this, element, index)
                : () => {
                    return null;
                  }
            }
          >
            <small
              className="selected-topic-label"
              style={
                _.includes(this.state.selectedInterest, element.topic_id) && !isSuggested
                  ? { color: '#ffffff' }
                  : this.styles.selectedLabel
              }
            >
              {element.topic_label}
              {!isSuggested &&
                this.ifTopicSelected(element) &&
                (this.state.userLevelObj[element.topic_id] ||
                  this.state.topicLevelObj[element.topic_id]) &&
                this.showBIA && (
                  <span style={{ color: '#cccccc' }}>
                    {' '}
                    (
                    {
                      this.state.levelStr[
                        this.state.userLevelObj[element.topic_id] ||
                          this.state.topicLevelObj[element.topic_id]
                      ]
                    }
                    )
                  </span>
                )}
            </small>
            {!isSuggested && this.ifTopicSelected(element) && this.showBIA && (
              <IconButton
                className="tick-icon"
                ref={'tickIcon'}
                tooltip={tr('Selected')}
                aria-label={`selected`}
                disableTouchRipple
                tooltipPosition="top-center"
                style={this.styles.customPlusIcon}
                iconStyle={this.styles.smallIcon}
                onClick={e => {
                  !isSuggested
                    ? this.openBiaModal(e, element)
                    : () => {
                        return null;
                      };
                }}
              >
                <EditIcon color="#ffffff" />
              </IconButton>
            )}
            {!isSuggested && element.topic_id == this.state.biaModalVisible && (
              <div style={this.styles.biabox}>
                <RadioButtonGroup
                  key="radio-button-group"
                  name="bia-radio"
                  className="vertical-spacing-medium"
                  onChange={(e, value) => {
                    this.handleLevelClick(element.topic_id, value);
                  }}
                  defaultSelected={
                    this.state.userLevelObj[element.topic_id] ||
                    this.state.topicLevelObj[element.topic_id] ||
                    this.state.pwcObj[element.topic_id]
                  }
                >
                  <RadioButton
                    value={1}
                    label={tr('Beginner')}
                    iconStyle={this.styles.radioIcon}
                    labelStyle={this.styles.radioLabel}
                    onClick={e => {
                      e.stopPropagation();
                    }}
                  />
                  <RadioButton
                    value={2}
                    label={tr('Intermediate')}
                    iconStyle={this.styles.radioIcon}
                    labelStyle={this.styles.radioLabel}
                    onClick={e => {
                      e.stopPropagation();
                    }}
                  />
                  <RadioButton
                    value={3}
                    label={tr('Advanced')}
                    iconStyle={this.styles.radioIcon}
                    labelStyle={this.styles.radioLabel}
                    onClick={e => {
                      e.stopPropagation();
                    }}
                  />
                </RadioButtonGroup>
                <div>
                  <a
                    href="#"
                    style={this.styles.cancelLink}
                    onClick={e => {
                      this.handleCancelClick(element.topic_id, e);
                    }}
                  >
                    {tr('Cancel')}
                  </a>
                  <a
                    href="#"
                    style={this.styles.okLink}
                    onClick={e => {
                      this.handleOkClick(e, element.topic_id);
                    }}
                  >
                    {tr('OK')}
                  </a>
                </div>
              </div>
            )}
          </Chip>
        </div>
      );
    } else {
      return (
        <Chip
          key={index}
          style={this.styles.chip}
          labelStyle={
            isSuggested
              ? { ...this.styles.chipLabel, ...{ paddingRight: '12px' } }
              : this.styles.chipLabel
          }
          backgroundColor={'#fff'}
          onClick={
            isSuggested
              ? this.selectTopicHandler(element)
              : () => {
                  return;
                }
          }
        >
          <small style={this.styles.selectedLabel}>{element.topic_label}</small>
          {!isSuggested && (
            <IconButton
              className="tick-icon"
              ref={'tickIcon'}
              aria-label={`selected ${element.topic_label}`}
              tooltip={tr('Selected')}
              disableTouchRipple
              tooltipPosition="top-center"
              style={this.styles.customPlusIcon}
            >
              <Close color={colors.primary} />
            </IconButton>
          )}
          {!isSuggested && (
            <IconButton
              className="cancel-icon delete"
              ref={'cancelIcon'}
              aria-label={`Remove ${element.topic_label}`}
              tooltip={tr('Remove Item')}
              disableTouchRipple
              tooltipPosition="top-center"
              style={this.styles.customPlusIcon}
              onTouchTap={
                !isSuggested
                  ? this.removeTopicHandler.bind(this, element.topic_id, isSuggested)
                  : () => {
                      return null;
                    }
              }
            >
              <Close color={colors.primary} />
            </IconButton>
          )}
        </Chip>
      );
    }
  }

  render() {
    let skillsArray = this.state.skillsArray != undefined ? this.state.skillsArray : [];

    let suggestArray =
      (this.props.onboarding &&
        this.props.onboarding.defaultTopics &&
        this.props.onboarding.defaultTopics) ||
      [];

    let goalsChipList = [];
    let suggestChipList = [];
    if (!this.state.onboardingMicroservice) {
      skillsArray.map((skill, idx) => {
        goalsChipList.push(this.goalChipRender(skill, idx));
      });
    } else {
      let defaultTopics = this.props.onboarding.defaultTopics || [];
      defaultTopics.map((topic, idx) => {
        _.every(this.state.selectedInterest, i => {
          return i === topic.topic_id;
        });
        goalsChipList.push(this.goalChipRender(topic, idx));
      });
    }
    suggestArray.map((skill, idx) => {
      if (!~_.findIndex(this.state.skillsArray, { topic_name: skill.topic_name })) {
        suggestChipList.push(this.goalChipRender(skill, idx, true));
      }
    });

    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let interestLabel = tr('UPDATE LEARNING GOALS');
    let suggestedInterestLabel = tr('Suggested Learning Goals');
    if (labels) {
      let interestsLabelFlag =
        labels['web/labels/interests'] && labels['web/labels/interests'].label.length > 0;
      interestLabel = `${tr('UPDATE')} ${
        interestsLabelFlag ? tr(labels['web/labels/interests'].label) : tr('Learning Goals')
      }`;
      suggestedInterestLabel = `${tr('Suggested')} ${
        interestsLabelFlag ? tr(labels['web/labels/interests'].label) : tr('Learning Goals')
      }`;
    }
    return (
      <div>
        <TextField name="updateInterest" autoFocus={true} className="hiddenTextField" />
        {this.state.onboardingVersion === 'v4' ? (
          <div>
            <InterestModalOnboardV4 />
          </div>
        ) : (
          <div className="vertical-spacing-large">
            {this.state.showAlert && (
              <FlashAlert
                message={this.state.errorMessage}
                toShow={this.state.showAlert}
                removeAlert={this.removeAlert}
              />
            )}
            <h5 style={{ marginBottom: '0px' }}>{interestLabel.toUpperCase()}</h5>
            <div className="clearfix" />
            <div className="row">
              <div className="small-12 columns">
                {!this.state.onboardingMicroservice && (
                  <div className="row">
                    <div className="small-12 columns text-center">
                      <div style={this.styles.chipsWrapper}>{goalsChipList}</div>
                    </div>
                  </div>
                )}
                <br />
                <div className="clearfix" />
                {!this.state.onboardingMicroservice && (
                  <div className="row skill-input-row">
                    <div className="small-12 columns text-center">
                      <p className="onboarding-label text-left">
                        {tr('What would you like to learn?')}
                      </p>
                      <div className="auto-complete-as-input">
                        <AutoComplete
                          popoverProps={{
                            style: {
                              bottom: '0.7rem',
                              overflowY: 'auto',
                              background: 'transparent',
                              boxShadow: 'none'
                            },
                            className: 'auto-complete-list'
                          }}
                          menuStyle={{ background: 'white' }}
                          filter={AutoComplete.caseInsensitiveFilter}
                          dataSource={this.state.skillSource}
                          onUpdateInput={this.handleSkillInput}
                          fullWidth={true}
                          dataSourceConfig={{ text: 'label', value: 'name' }}
                          onNewRequest={this.selectSkillInput}
                          searchText={this.state.searchText}
                          maxSearchResults={20}
                          aria-label={tr('What would you like to learn?')}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {this.state.onboardingMicroservice && (
                  <div className="row">
                    <div className="small-12 columns text-center">
                      <div style={this.styles.chipsWrapper}>{goalsChipList}</div>
                    </div>
                  </div>
                )}
                <br />
                {this.state.winterstorm && (
                  <div>
                    <div className="row">
                      <h6>{suggestedInterestLabel}:</h6>
                    </div>
                    <div className="clearfix" />
                    <div className="row">
                      <div className="small-12 columns text-center">
                        <div style={this.styles.chipsWrapper}>
                          {suggestChipList.length > 0 ? (
                            suggestChipList
                          ) : (
                            <div>{tr('We have nothing to suggest you yet')}</div>
                          )}
                        </div>
                      </div>
                    </div>
                    <br />
                  </div>
                )}
                <div style={{ padding: '8px' }} className="clearfix">
                  <PrimaryButton
                    label={tr('Update')}
                    className="float-right create"
                    pendingLabel={tr('Updating...')}
                    pending={this.state.postPending}
                    onTouchTap={this.handleClickUpdate}
                  />
                  <SecondaryButton
                    label={tr('Cancel')}
                    className="float-right close"
                    onTouchTap={this.handleClickCancel}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: Object.assign({}, state.team.toJS()),
    currentUser: Object.assign({}, state.currentUser.toJS()),
    onboarding: state.onboarding.toJS()
  };
}

UpdateInterestsModal.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  onboarding: PropTypes.object,
  callback: PropTypes.any
};

export default connect(mapStoreStateToProps)(UpdateInterestsModal);
