import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { openChannelCardsModal, close } from '../../actions/modalActions';
import {
  removeChannelCard,
  skipItem,
  getNonCurateCards,
  reOpenCurateModal
} from '../../actions/channelsActionsV2';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class ChannelCardRemoveConfirmationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pendingRemoval: false,
      pendingRejection: false
    };
    this.styles = {
      paper: {
        padding: '16px'
      }
    };
  }

  fetchData = key => {
    let titleKeysObj = {};
    switch (key) {
      case 'smartbite':
        titleKeysObj.modalTitle = 'SmartCards';
        titleKeysObj.countKey = 'smartbitesCount';
        break;
      case 'video_stream':
        titleKeysObj.modalTitle = 'Streams';
        titleKeysObj.countKey = 'videoStreamsCount';
        break;
      case 'pack':
        titleKeysObj.modalTitle = 'Pathways';
        titleKeysObj.countKey = 'publishedPathwaysCount';
        break;
      default:
        // FIXME: implement default case
        break;
    }
    return titleKeysObj;
  };

  openChannelCardsModal = () => {
    let editMode = true;
    let title = this.fetchData(this.props.cardTypes).modalTitle;
    this.props.dispatch(
      openChannelCardsModal(
        title,
        this.props.channel[this.fetchData(this.props.cardTypes).countKey],
        this.props.channel[this.props.cardTypes],
        this.props.channel,
        editMode
      )
    );
  };

  removeCard = () => {
    this.setState({ pendingRemoval: true });
    let card = this.props.card;
    this.props
      .dispatch(removeChannelCard(this.props.channel.id, card, this.props.cardTypes))
      .then(() => {
        this.openChannelCardsModal();
      })
      .catch(err => {
        console.error(`Error in ChannelCardRemoveConfirmationModal.removeChannelCard.func: ${err}`);
      });
  };

  rejectCard = () => {
    this.props
      .dispatch(skipItem(this.props.card.id, this.props.channel.id, this.props.channel.label))
      .then(() => {
        this.setState({ pendingRejection: true });
        let cardId = this.props.card.id;
        var cards = this.props.rejectCallback;
        cards.cards = cards.cards.filter(ele => {
          return ele.id != cardId;
        });
        cards.total--;
        this.props.dispatch(reOpenCurateModal(this.props.channel, cards));
      })
      .catch(err => {
        console.error(`Error in ChannelCardRemoveConfirmationModal.skipItem.func: ${err}`);
      });
  };

  closeModal = () => {
    this.props.dispatch(close());
    this.props.dispatch(reOpenCurateModal(this.props.channel, this.props.rejectCallback));
    if (!this.props.isReject) {
      this.openChannelCardsModal();
    }
  };

  render() {
    return (
      <div className="channel-card-remove-modal">
        <div className="row">
          <TextField name="channelconfirmation" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            {this.props.isReject ? (
              <div style={{ color: 'white' }}>{tr('Reject Card')}</div>
            ) : (
              <div style={{ color: 'white' }}>{tr('Remove Card')}</div>
            )}
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              <div className="row">
                <div className="small-12 columns">
                  {this.props.isReject ? (
                    <div>
                      {tr('Do you really want to reject selected card from')}{' '}
                      {this.props.channel.label} {tr('channel?')}
                    </div>
                  ) : (
                    <div>
                      {tr('Do you really want to remove selected card from')}{' '}
                      {this.props.channel.label} {tr('channel?')}
                    </div>
                  )}
                </div>
              </div>
              <br />
              <div className="row">
                <div className="small-12 columns">
                  <div style={{ color: 'white' }}>
                    {this.props.isReject ? (
                      <PrimaryButton
                        className="float-right remove-card"
                        label={tr('Reject')}
                        pending={this.state.pendingRejection}
                        pendingLabel={tr('Rejecting...')}
                        onTouchTap={this.rejectCard}
                      />
                    ) : (
                      <PrimaryButton
                        className="float-right remove-card"
                        label={tr('Remove')}
                        pending={this.state.pendingRemoval}
                        pendingLabel={tr('Removing...')}
                        onTouchTap={this.removeCard}
                      />
                    )}
                    <SecondaryButton
                      label={tr('Cancel')}
                      className="float-right remove-cancel"
                      onTouchTap={this.closeModal}
                    />
                  </div>
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

ChannelCardRemoveConfirmationModal.propTypes = {
  isReject: PropTypes.bool,
  rejectCallback: PropTypes.func,
  channel: PropTypes.object,
  card: PropTypes.object,
  cardTypes: PropTypes.string
};

export default connect()(ChannelCardRemoveConfirmationModal);
