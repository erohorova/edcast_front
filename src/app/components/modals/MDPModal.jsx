import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import TextField from 'material-ui/TextField';
import { TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
//actions
import { close } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { addtoMDP } from 'edc-web-sdk/requests/cards.v2';

class MDPModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.sources = Object.freeze(['Performance', 'Career Aspiration', 'Discussion with Manager']);
    this.subCompetencies = Object.freeze([
      'Analytical Thinking',
      'Financial Awareness',
      'Strategic Decission Making',
      'Understand Business Environment'
    ]);
    this.state = {
      buttonPending: false,
      disabled: true,
      disableTextField: true,
      sourceMDPvalue: this.sources[0],
      subCompetencyValue: this.subCompetencies[0]
    };
    this.style = {
      inputStyle: {
        marginRight: '0.8rem',
        fontSize: '0.75rem'
      },
      labelStyle: {
        fontSize: '0.75rem',
        color: '#6f708b',
        fontWeight: 300
      },
      writeComment: {
        fontSize: '0.75rem',
        paddingLeft: '1.5rem'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  submitMDPFORM = () => {
    if (!!this.props.card) {
      let payload = {
        form_details: {
          source_of_mdp: this.state.sourceMDPvalue,
          sub_competency_value: this.state.subCompetencyValue,
          additional_comments: this.state.additionalComments,
          learning_activity: 'GVC Learning'
        }
      };
      addtoMDP(this.props.card.id, payload)
        .then(report => {
          this.props.dispatch(close());
          this.props.dispatch(openSnackBar('Card has been successfully Added to MDP', true));
        })
        .catch(err => {
          console.error(`Error in MDPModal.submitMDPFORM.func: ${err}`);
          this.props.dispatch(
            openSnackBar('Error adding Card to MDP.Please contact Administrator', true)
          );
        });
    }
  };

  handleMDPSourceChange = (event, index, value) => this.setState({ sourceMDPvalue: value });

  handleChange = (event, index, value) => this.setState({ subCompetencyValue: value });

  handleCommentsChange = event => this.setState({ additionalComments: event.target.value });

  render() {
    return (
      <div className="mdp-modal">
        <div className="mdp-header">
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div className="mdp-title">{tr('Add to MDP')}</div>
        <div>
          <TextField name="mdpmodal" autoFocus={true} className="hiddenTextField" />
          <TableBody displayRowCheckbox={false} showRowHover={false}>
            <TableRow selectable={false}>
              <TableRowColumn>Source of MDP</TableRowColumn>
              <TableRowColumn style={{ float: 'right' }}>
                <DropDownMenu
                  value={this.state.sourceMDPvalue}
                  onChange={this.handleMDPSourceChange}
                >
                  {this.sources.map((source, index) => {
                    return <MenuItem key={index} value={source} primaryText={source} />;
                  })}
                </DropDownMenu>
              </TableRowColumn>
            </TableRow>
            <TableRow selectable={false}>
              <TableRowColumn>Sub-Competency selection</TableRowColumn>
              <TableRowColumn style={{ float: 'right' }}>
                <DropDownMenu value={this.state.subCompetencyValue} onChange={this.handleChange}>
                  {this.subCompetencies.map((subCompetency, index) => {
                    return (
                      <MenuItem key={index} value={subCompetency} primaryText={subCompetency} />
                    );
                  })}
                </DropDownMenu>
              </TableRowColumn>
            </TableRow>
            <TableRow selectable={false}>
              <TableRowColumn>Additional Comments</TableRowColumn>
              <TableRowColumn style={{ float: 'right' }}>
                <TextField
                  onChange={this.handleCommentsChange}
                  id={'additional-comments'}
                  type="text"
                  maxLength="50"
                  textareaStyle={this.style.writeComment}
                />
              </TableRowColumn>
            </TableRow>
          </TableBody>
          <div className="btn-block text-right">
            <PrimaryButton
              className="remove-card"
              label={tr('Submit')}
              pending={this.state.buttonPending}
              pendingLabel={tr('Submiting...')}
              onTouchTap={this.submitMDPFORM}
              disabled={false}
            />
          </div>
        </div>
      </div>
    );
  }
}

MDPModal.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  group: PropTypes.object,
  callback: PropTypes.func,
  title: PropTypes.string,
  card: PropTypes.object,
  comment: PropTypes.object
};

export default connect()(MDPModal);
