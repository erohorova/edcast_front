import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import Paper from 'edc-web-sdk/components/Paper';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';

class CardClassesModal extends Component {
  constructor(props, context) {
    super(props, context);
  }

  closeModal = () => {
    this.props.closeModal();
  };

  deeplink(deeplinkUrl) {
    window.open(deeplinkUrl, '_blank');
  }

  render() {
    let courseClasses = this.props.classes;
    return (
      <div className="card-modal-container card-class-modal-container">
        <div className="backdrop" />
        <div className="modal">
          <div className="modal-content">
            <div className="close close-button">
              <IconButton aria-label="close" onTouchTap={this.closeModal}>
                <CloseIcon color="black" />
              </IconButton>
            </div>
            <h4>{tr('Classes')}</h4>
            <br />
            <Paper>
              <div className="row">
                {courseClasses.map(classSubItem => {
                  let title =
                    classSubItem['title'].length > 47
                      ? `${classSubItem['title'].substr(0, 47)}...`
                      : classSubItem['title'].substr(0, 47);
                  return (
                    <div className="small-3 columns">
                      <div className="class-card-wrapper">
                        <div
                          className="card-img"
                          style={{
                            backgroundImage:
                              'url("/i/images/courses/course' +
                              (Math.floor(Math.random() * 48) + 1) +
                              '.jpg")'
                          }}
                        />
                        <p className="title">{title}</p>
                        <div className="text-center">
                          <PrimaryButton
                            className="button"
                            label={tr('GO TO COURSE')}
                            onTouchTap={() => {
                              this.deeplink(classSubItem['deeplink_url']);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

CardClassesModal.propTypes = {
  closeModal: PropTypes.func,
  classes: PropTypes.array
};

export default connect()(CardClassesModal);
