import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import colors from 'edc-web-sdk/components/colors/index';
import { closeInterestModal } from '../../actions/modalActions';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import IconButton from 'material-ui/IconButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { updateUserInterest } from '../../actions/usersActions';
import SearchBox from './SearchBox';
import { queryTopics } from 'edc-web-sdk/requests/topics';
import OnBoardV2BIAContainer from './OnBoardV2BIAContainer';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import TextField from 'material-ui/TextField';

class UpdateInterestsOnBoardV2Modal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      header: {
        color: '#ffffff',
        width: '100%',
        position: 'absolute',
        top: '-30px',
        left: 0
      },
      close: {
        cursor: 'pointer',
        float: 'right',
        top: 0
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '7px'
      },
      smallIcon: {
        width: '16px',
        height: '16px',
        margin: '6px'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '8px',
        maxHeight: '18px',
        right: '5px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`
      },
      chipLabel: {
        paddingRight: '27px'
      },
      selectedLabel: {
        maxWidth: 'calc(100% - 1px)',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        color: `${colors.primary}`
      },
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer',
        borderRadius: '2px'
      },
      trendingChip: {
        margin: '4px 8px 4px 0px',
        cursor: 'pointer',
        borderRadius: '2px'
      },
      selectedTopicLabel: {
        color: 'white'
      },
      biabox: {
        position: 'absolute',
        zIndex: '1',
        padding: '12px 12px 10px 0',
        boxShadow: '0px 1px 4px #000000',
        background: '#ffffff',
        left: '0',
        top: '40px',
        height: '115px',
        fontSize: '12px'
      },
      radioIcon: {
        width: '16px',
        height: '16px',
        borderColor: '#979797',
        color: '#00a1e1',
        marginTop: '1px'
      },
      radioLabel: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '0.8125rem',
        lineHeight: 1.47,
        textAlign: 'left',
        color: '#6f708b',
        width: '100%',
        marginLeft: '1px'
      },
      okLink: {
        float: 'right',
        color: '#6f6f8b',
        fontWeight: 'bold',
        fontSize: '14px'
      },
      cancelLink: {
        float: 'left',
        color: '#6f6f8b',
        fontSize: '14px',
        marginLeft: '5px'
      },
      notificationIcon: {
        width: '24px',
        height: '24px'
      },
      notificationMessage: {
        paddingLeft: '10px',
        display: 'inline-block',
        width: '92%',
        fontSize: '12px',
        verticalAlign: 'middle',
        color: '#f1736a'
      },
      notificationMessageBold: {
        fontWeight: '700'
      },
      notificationMessageNormal: {
        fontWeight: '600'
      }
    };
    this.state = {
      interests: [],
      userSelectedInterests: [],
      defaultTopics: [],
      selectedInterestWithBIA: null,
      interestFromTrending: [],
      suggestions: [],
      loadingSuggestions: false,
      selectedInterest: {
        selectedSuggestion: []
      },
      interestLimit:
        (this.props.team &&
          this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options.interests_limit) ||
        3,
      targetPosition: null,
      levelStr: {
        1: 'Beginner',
        2: 'Intermediate',
        3: 'Advanced'
      },
      showBIAModalForNewlyCreatedChips: false,
      showAlert: false,
      boldMessage: '',
      normalMessage: '',
      postPending: false,
      alertdisplayTimeOut: 1000
    };

    this.showBIA = !!(
      this.props.team &&
      this.props.team.config &&
      this.props.team.config.enabled_bia
    );
  }

  componentDidMount() {
    let interests =
      (this.props.currentUser &&
        this.props.currentUser.profile &&
        this.props.currentUser.profile.learningTopics) ||
      [];
    let defaultTopics =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.default_topics &&
        this.props.team.config.default_topics.defaults) ||
      [];
    this.setState({ interests, defaultTopics });
  }

  _handleClickCancel = () => {
    if (this.showAlertTimeout && this.state.showAlert) {
      clearTimeout(this.showAlertTimeout);
    }
    this.props.dispatch(closeInterestModal());
  };

  _handleInterestDelete = key => {
    let selectedInterestWithBIA = this.state.selectedInterestWithBIA;
    if (selectedInterestWithBIA && this.state.targetPosition) {
      if (key == selectedInterestWithBIA.topic_id) {
        this.setState({
          targetPosition: null
        });
      }
    }

    if (this.showAlertTimeout && this.state.showAlert) {
      clearTimeout(this.showAlertTimeout);
      this.setState({ showAlert: false });
    }

    let interests = this.state.interests;
    const chipToDelete = interests.findIndex(chip => chip.topic_id === key);
    interests.splice(chipToDelete, 1);
    this.setState({ interests });
  };

  showNotificationMessage = (boldMessage, normalMessage, alertDisplayTimeOut) => {
    this.setState(
      {
        showAlert: true,
        boldMessage: boldMessage,
        normalMessage: normalMessage
      },
      () => {
        this.showAlertTimeout = setTimeout(() => {
          this._removeAlert();
        }, alertDisplayTimeOut);
      }
    );
  };

  showHideNotificationMessage = (boldMessage, normalMessage, alertDisplayTimeOut) => {
    if (this.showAlertTimeout) {
      clearTimeout(this.showAlertTimeout);
      this.setState(
        {
          showAlert: false
        },
        () => {
          this.showNotificationMessage(boldMessage, normalMessage, alertDisplayTimeOut);
        }
      );
    } else {
      this.showNotificationMessage(boldMessage, normalMessage, alertDisplayTimeOut);
    }
  };

  _AddRemoveInterestFromTreding = data => {
    let interestFromTrending = this.state.interestFromTrending,
      index = _.findIndex(interestFromTrending, data),
      showBIAModalForNewlyCreatedChips = false,
      userSelectedInterests = this.state.userSelectedInterests;
    if (_.some(this.state.interests, ['topic_id', data.topic_id])) {
      this.showHideNotificationMessage(tr('Similar topic is already selected'), '', 2000);
      return;
    }
    if (index > -1) {
      let selectedInterestWithBIA = this.state.selectedInterestWithBIA;
      if (selectedInterestWithBIA && this.state.targetPosition) {
        if (data.topic_id == selectedInterestWithBIA.topic_id) {
          this.setState({
            targetPosition: null
          });
        }
      }
      let userSelectedInterestsIndex = _.findIndex(userSelectedInterests, data);
      if (userSelectedInterestsIndex > -1) {
        userSelectedInterests.splice(userSelectedInterestsIndex, 1);
      }
      interestFromTrending.splice(index, 1);
      if (this.showAlertTimeout && this.state.showAlert) {
        clearTimeout(this.showAlertTimeout);
        this.setState({ showAlert: false });
      }
    } else if (this._isAddInterestFlag()) {
      interestFromTrending.push(data);
      userSelectedInterests.push(data);
      showBIAModalForNewlyCreatedChips = data.topic_id;
    } else {
      this.showHideNotificationMessage(
        tr('You can select a maximum of %{count} topics only.', {count: this.state.interestLimit}),
        tr(
          "Please deselect a topic before selecting another one. Don't worry you can always change them again here."
        ),
        5000
      );
    }
    this.setState({
      interestFromTrending,
      showBIAModalForNewlyCreatedChips,
      userSelectedInterests
    });
  };

  openBiaModal = (e, topic) => {
    e && e.preventDefault();
    let eventTarget = e.currentTarget;
    if (this.showBIA) {
      let selectedInterestWithBIA = this.state.selectedInterestWithBIA;
      if (
        selectedInterestWithBIA &&
        !selectedInterestWithBIA.level &&
        !_.isEqual(selectedInterestWithBIA, topic)
      ) {
        selectedInterestWithBIA.level = 1;
        this.setState(
          {
            selectedInterestWithBIA
          },
          () => {
            this.setState({
              selectedInterestWithBIA: topic,
              targetPosition: eventTarget,
              showBIAModalForNewlyCreatedChips: false
            });
          }
        );
      } else {
        this.setState({
          selectedInterestWithBIA: topic,
          targetPosition: e.currentTarget,
          showBIAModalForNewlyCreatedChips: false
        });
      }
    }
  };

  handleOkClick = topic => {
    this.setState({
      selectedInterestWithBIA: topic,
      targetPosition: null
    });
  };

  showBIAForNewlyAddedChips = () => {
    let showBIAModalForNewlyCreatedChips = this.state.showBIAModalForNewlyCreatedChips;
    if (showBIAModalForNewlyCreatedChips) {
      setTimeout(() => {
        if (document.getElementById('renderNewlyCreatedChip' + showBIAModalForNewlyCreatedChips)) {
          document
            .getElementById('renderNewlyCreatedChip' + showBIAModalForNewlyCreatedChips)
            .click();
        }
      }, 100);
    }
  };

  renderSelectedInterestChip = (data, index) => {
    return (
      <Chip
        className="interest-chip"
        key={data.topic_id}
        style={this.styles.chip}
        backgroundColor={colors.primary}
        labelStyle={this.styles.chipLabel}
        labelColor={'#ffffff'}
        onClick={e => {
          this.openBiaModal(e, data);
        }}
      >
        {data.topic_label}
        {this.showBIA && data.level && (
          <span style={{ color: '#cccccc' }}>({this.state.levelStr[data.level]})</span>
        )}
        <IconButton
          className="cancel-icon delete cancel-chip-icon"
          ref={'cancelIcon'}
          tooltip={tr('Remove Item')}
          aria-label={tr('Remove Item')}
          disableTouchRipple
          tooltipPosition="top-center"
          style={this.styles.customPlusIcon}
          onTouchTap={() => this._handleInterestDelete(data.topic_id)}
        >
          <NavigationClose color={'#ffffff'} />
        </IconButton>
      </Chip>
    );
  };

  renderUserSelectedInterests = data => {
    let selectedInterest = this.state.selectedInterest;
    if (_.some(this.state.interestFromTrending, ['topic_id', data.topic_id])) {
      return (
        <Chip
          className="interest-chip"
          id={'renderNewlyCreatedChip' + data.topic_id}
          key={data.topic_id}
          style={this.styles.chip}
          backgroundColor={colors.primary}
          labelStyle={this.styles.chipLabel}
          labelColor={'#ffffff'}
          onClick={e => {
            this.openBiaModal(e, data);
          }}
        >
          {data.topic_label}
          {this.showBIA && data.level && (
            <span style={{ color: '#cccccc' }}>({this.state.levelStr[data.level]})</span>
          )}
          <IconButton
            className="cancel-icon delete cancel-chip-icon"
            ref={'cancelIcon'}
            aria-label={tr('Remove Item')}
            tooltip={tr('Remove Item')}
            disableTouchRipple
            tooltipPosition="top-center"
            style={this.styles.customPlusIcon}
            onTouchTap={() => {
              this._AddRemoveInterestFromTreding(data);
            }}
          >
            <NavigationClose color={'#ffffff'} />
          </IconButton>
        </Chip>
      );
    } else if (_.some(selectedInterest.selectedSuggestion || [], ['topic_id', data.topic_id])) {
      return (
        <Chip
          className="interest-chip"
          id={'renderNewlyCreatedChip' + data.topic_id}
          key={data.topic_id}
          style={this.styles.chip}
          backgroundColor={colors.primary}
          labelStyle={this.styles.chipLabel}
          labelColor={'#ffffff'}
          onClick={e => {
            this.openBiaModal(e, data);
          }}
        >
          {data.topic_label}
          {this.showBIA && data.level && (
            <span style={{ color: '#cccccc' }}>({this.state.levelStr[data.level]})</span>
          )}
          <IconButton
            className="cancel-icon delete cancel-chip-icon"
            ref={'cancelIcon'}
            tooltip={tr('Remove Item')}
            aria-label={tr('Remove Item')}
            disableTouchRipple
            tooltipPosition="top-center"
            style={this.styles.customPlusIcon}
            onTouchTap={() => {
              this._removeSelectedInterest(data, 'suggested');
            }}
          >
            <NavigationClose color={'#ffffff'} />
          </IconButton>
        </Chip>
      );
    }
  };

  renderTrendingInterestChip = data => {
    if (
      !_.some(
        [...this.state.interests, ...this.state.interestFromTrending],
        ['topic_id', data.topic_id]
      )
    ) {
      return (
        <Chip
          className="interest-chip"
          key={data.topic_id}
          style={this.styles.trendingChip}
          onClick={() => this._AddRemoveInterestFromTreding(data)}
          backgroundColor={
            _.some(this.state.interestFromTrending, ['topic_id', data.topic_id])
              ? `${colors.primary}`
              : `${colors.primary100}`
          }
          labelColor={
            !_.some(this.state.interestFromTrending, ['topic_id', data.topic_id])
              ? `${colors.primary}`
              : `${colors.primary100}`
          }
        >
          {data.topic_label}
        </Chip>
      );
    }
  };

  _onSearchQueryChange = q => {
    if (q) {
      this.setState({ loadingSuggestions: true });
      queryTopics(q)
        .then(topicsData => {
          this.setState({
            suggestions: topicsData.topics.slice(0, 20),
            loadingSuggestions: false
          });
        })
        .catch(err => {
          console.error(`Error in Expertise.queryTopics.func : ${err}`);
        });
    } else {
      this.setState({
        suggestions: []
      });
    }
  };

  _onInterestSelect = selectionData => {
    let selectedInterest = {},
      payload = {},
      showBIAModalForNewlyCreatedChips = false,
      userSelectedInterests = this.state.userSelectedInterests;
    selectedInterest['selectedSuggestion'] = selectionData.selectedSuggestion;
    if (selectionData.selectedSuggestion.length > 0) {
      showBIAModalForNewlyCreatedChips =
        selectionData.selectedSuggestion[selectionData.selectedSuggestion.length - 1].topic_id;
      if (
        !_.find(
          userSelectedInterests,
          selectionData.selectedSuggestion[selectionData.selectedSuggestion.length - 1]
        )
      ) {
        userSelectedInterests = [
          ...userSelectedInterests,
          selectionData.selectedSuggestion[selectionData.selectedSuggestion.length - 1]
        ];
      }
    }

    this.setState({
      selectedInterest,
      showBIAModalForNewlyCreatedChips,
      userSelectedInterests
    });
  };

  _removeSelectedInterest = (item, type) => {
    let selectedInterestWithBIA = this.state.selectedInterestWithBIA,
      userSelectedInterests = this.state.userSelectedInterests,
      index = -1;
    if (selectedInterestWithBIA && this.state.targetPosition) {
      if (item.topic_id == selectedInterestWithBIA.topic_id) {
        this.setState({
          targetPosition: null
        });
      }
    }
    index = _.findIndex(userSelectedInterests, item);
    if (index > -1) {
      userSelectedInterests.splice(index, 1);
      this.setState({
        userSelectedInterests
      });
    }

    if (this.showAlertTimeout && this.state.showAlert) {
      clearTimeout(this.showAlertTimeout);
      this.setState({ showAlert: false });
    }
    this.searchbox._removeSelectedInterest(item, type);
  };

  _onlimitExceed = selectedInterest => {
    this.setState({ selectedInterest });
    this.showHideNotificationMessage(
      tr('You can select a maximum of %{count} topics only.', {count: this.state.interestLimit}),
      tr(
        "Please deselect a topic before selecting another one. Don't worry you can always change them again here."
      ),
      5000
    );
  };

  _resultFormation = skill => {
    let responseResult = {};
    responseResult['domain_id'] = skill.domain ? skill.domain.id : skill.domain_id || null;
    responseResult['domain_label'] = skill.domain ? skill.domain.label : skill.domain_label || null;
    responseResult['domain_name'] = skill.domain ? skill.domain.name : skill.domain_name || null;
    responseResult['topic_id'] = skill.id ? skill.id : skill.topic_id || null;
    responseResult['topic_label'] = skill.label ? skill.label : skill.topic_label || null;
    responseResult['topic_name'] = skill.name ? skill.name : skill.topic_name || null;
    responseResult['topic_description'] = skill.description || null;
    return responseResult;
  };

  _isAddInterestFlag = () => {
    let interests = this.state.interests || [];
    let selectedInterest = this.state.selectedInterest;
    let interestFromTrending = this.state.interestFromTrending || [];
    let interestLimit = this.state.interestLimit;

    let selectedInterestsLimit =
      interests.length + selectedInterest.selectedSuggestion.length + interestFromTrending.length;

    return interestLimit > selectedInterestsLimit;
  };

  _submitInterests = () => {
    this.setState({ postPending: true });

    if (this.showAlertTimeout && this.state.showAlert) {
      clearTimeout(this.showAlertTimeout);
      this.setState({ showAlert: false });
    }

    let interests = this.state.interests || [];
    let userSelectedInterests = this.state.userSelectedInterests || [];
    let skillsArray = [...interests, ...userSelectedInterests];

    if (skillsArray && skillsArray.length === 0) {
      this.setState({ postPending: false });
      this.showHideNotificationMessage(tr('You should have at least 1 topic selected.'), '', 2000);
      return;
    }

    for (let skill of skillsArray) {
      if (!this.showBIA && skill.level) {
        delete skill.level;
      } else if (this.showBIA && !skill.level) {
        skill.level = 1;
      }
    }

    if (this.state.targetPosition) {
      this.setState({
        targetPosition: null
      });
    }

    this.props
      .dispatch(
        updateUserInterest(
          skillsArray,
          this.props.currentUser.id,
          this.props.currentUser.profile.id
        )
      )
      .then(response => {
        this.setState({ postPending: false });
        this._handleClickCancel();
      })
      .catch(err => {
        console.error(`Error in UpdateInterestsModal.updateUserInterest.func : ${err}`);
      });
  };

  _removeAlert = () => {
    this.setState({ showAlert: false });
  };

  closeModalHandler = () => {
    if (this.showAlertTimeout && this.state.showAlert) {
      clearTimeout(this.showAlertTimeout);
    }
    this.props.dispatch(closeInterestModal());
  };

  render() {
    let interests = this.state.interests || [];
    let selectedInterest = this.state.selectedInterest;
    let defaultTopics = this.state.defaultTopics || [];
    let userSelectedInterests = this.state.userSelectedInterests || [];

    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let interestLabel = tr('Update Learning Goals');
    if (labels) {
      let interestsLabelFlag =
        labels['web/labels/interests'] &&
        labels['web/labels/interests'].label &&
        labels['web/labels/interests'].label.length > 0;
      interestLabel = `${tr('Update')} ${
        interestsLabelFlag ? tr(labels['web/labels/interests'].label) : tr('Learning Goals')
      }`;
    }

    let selectedInterestLabel = tr('Select %{count} Job roles of your interest.', {count: this.state.interestLimit});
    let placeholderText = tr('What would you like to learn?');

    return (
      <div>
        <div style={this.styles.header}>
          {interestLabel}
          <span style={this.styles.close} className="close-icon-btn">
            <NavigationClose color={colors.white} onTouchTap={this.closeModalHandler} />
          </span>
          <TextField name="updateInterestonboardv2" autoFocus={true} className="hiddenTextField" />
        </div>
        <div className="update-learning-goals-on-board-v2">
          <div style={{ marginBottom: '25px' }}>
            <div style={this.styles.chipsWrapper} className="selected-interest-chip-container">
              {interests.map(this.renderSelectedInterestChip)}
              {userSelectedInterests.map(this.renderUserSelectedInterests)}
              {this.showBIAForNewlyAddedChips()}
            </div>
            {this.showBIA && this.state.targetPosition && (
              <OnBoardV2BIAContainer
                targetPosition={this.state.targetPosition}
                selectedInterestWithBIA={this.state.selectedInterestWithBIA}
                handleOkClick={() => {
                  this.handleOkClick();
                }}
              />
            )}
          </div>

          {this.state.showAlert && (
            <ReactCSSTransitionGroup
              transitionName="alert"
              transitionAppear={true}
              transitionAppearTimeout={100}
              transitionEnter={false}
              transitionLeave={false}
            >
              <div style={{ marginBottom: '25px' }}>
                <img style={this.styles.notificationIcon} src="/i/images/notification.png" />
                <span style={this.styles.notificationMessage}>
                  {this.state.boldMessage && (
                    <span style={this.styles.notificationMessageBold}>
                      {tr(this.state.boldMessage)}
                    </span>
                  )}
                  {this.state.normalMessage && (
                    <span style={this.styles.notificationMessageNormal}>
                      {tr(' ' + this.state.normalMessage)}
                    </span>
                  )}
                </span>
              </div>
            </ReactCSSTransitionGroup>
          )}

          <div style={{ marginBottom: '25px' }}>
            <SearchBox
              ref={instance => {
                this.searchbox = instance;
              }}
              multiSelect={false}
              allowCustomString={false}
              placeholderText={placeholderText}
              onChangeCallBack={this._onSearchQueryChange}
              onSelectCallBack={this._onInterestSelect}
              suggestions={this.state.suggestions}
              loading={this.state.loadingSuggestions}
              ignoreInSuggestions={[
                ...defaultTopics,
                ...interests,
                ...selectedInterest.selectedSuggestion
              ]}
              suggestionSelectLimit={this._isAddInterestFlag()}
              onlimitExceedCallBack={this._onlimitExceed}
              resultFormationCallBack={this._resultFormation}
            />
          </div>

          <div style={{ marginBottom: '25px' }}>
            <div>
              <h6>{selectedInterestLabel}</h6>
            </div>
            <div style={this.styles.chipsWrapper} className="selected-interest-chip-container">
              {defaultTopics.map(this.renderTrendingInterestChip)}
            </div>
          </div>

          <div style={{ padding: '0.5rem 0.5rem 1rem' }} className="text-center">
            <SecondaryButton
              label={tr('Cancel')}
              className="close"
              onTouchTap={() => {
                this._handleClickCancel();
              }}
            />
            <PrimaryButton
              label={tr('Update')}
              className="create"
              pendingLabel={tr('Updating...')}
              pending={this.state.postPending}
              onTouchTap={() => {
                this._submitInterests();
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: Object.assign({}, state.team.toJS()),
    currentUser: Object.assign({}, state.currentUser.toJS()),
    onboarding: state.onboarding.toJS()
  };
}

UpdateInterestsOnBoardV2Modal.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  onboarding: PropTypes.object,
  callback: PropTypes.any
};

export default connect(mapStoreStateToProps)(UpdateInterestsOnBoardV2Modal);
