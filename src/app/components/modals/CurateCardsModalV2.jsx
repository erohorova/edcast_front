import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Card from './../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import CurateCardButtons from './../channel/CurateCardButtons';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import { getNonCurateCardsModal } from 'edc-web-sdk/requests/channels.v2';
import RejectOrPublishCard from '../common/RejectOrPublishCard';
import { curateItem, skipItem } from '../../actions/channelsActionsV2';

class CurateCardsModalV2 extends Component {
  constants = {
    ugcFilter: [tr('All'), tr('User Generated Cards')],
    loadingText: tr('Loading...'),
    viewMoreText: tr('View More')
  };

  state = {
    ugcSelectedValue: 'All',
    total: null,
    loading: false,
    channel: this.props.channel,
    curateCards: [],
    currentCardCount: 0,
    isPublishedOrRejected: false
  };

  styles = {
    textStyle: {
      marginTop: '1rem',
      color: '#fff'
    },
    iconStyleDropDown: {
      width: '1.87rem',
      height: '1.87rem',
      top: '0.437rem',
      fill: '#fff'
    },
    dropDownLabelColor: {
      color: '#fff'
    },
    selectFieldStyle: {
      verticalAlign: 'middle'
    }
  };

  query = {
    limit: 12,
    offset: 0
  };

  componentDidMount() {
    this.fetchChannelCurateCards();
  }

  fetchChannelCurateCards = () => {
    const payload = {
      limit: this.query.limit,
      channel_card_state: 'new',
      offset: this.query.offset,
      ugc: this.state.ugcSelectedValue === tr('User Generated Cards')
    };
    this.setState(() => ({ loading: true }));
    getNonCurateCardsModal(this.props.channel.id, payload)
      .then(response => {
        if (response.cards && !!response.cards.length) {
          const curateCards = [...this.state.curateCards, ...response.cards];
          const curateCardsIds = [];
          this.setState(() => ({
            curateCards: curateCards.filter(card => {
              const index = curateCardsIds.indexOf(card.id);
              if (index === -1) {
                curateCardsIds.push(card.id);
                return card;
              }
            }),
            total: response.total,
            loading: false,
            currentCardCount: curateCards.length
          }));
        } else {
          this.setState(() => ({ loading: false, total: this.state.curateCards.length }));
        }
      })
      .catch(error => {
        console.error('CurateCardsModalV2.fetchChannelCurateCards.getNonCurateCardsModal', error);
      });
  };

  filterChange = (e, index, value) => {
    this.setState(
      () => ({
        ugcSelectedValue: value,
        curateCards: []
      }),
      () => {
        this.query.offset = 0;
        this.fetchChannelCurateCards();
      }
    );
  };

  filterCard = cardId => {
    const curateCards = this.state.curateCards.filter(card => card.id !== cardId);
    this.setState(() => ({ curateCards }));
  };

  publishCard = card => {
    this.props
      .dispatch(curateItem(card.id, this.props.channel.id, this.props.channel.label))
      .then(() => {
        this.filterCard(card.id);
        this.setState(() => ({ isPublishedOrRejected: true }));
      })
      .catch(error => {
        console.error('Error in CurateCardsModalV2.publishCard.func', error);
      });
  };

  rejectCard = card => {
    this.props
      .dispatch(skipItem(card.id, this.props.channel.id, this.props.channel.label))
      .then(() => {
        this.filterCard(card.id);
        this.setState(() => ({ isPublishedOrRejected: true }));
      })
      .catch(err => {
        console.error(`Error in CurateCardsModalV2.rejectCard.func: ${err}`);
      });
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  viewMoreClickHandler = () => {
    if (this.state.total - this.state.currentCardCount !== 1 || !this.state.isPublishedOrRejected) {
      this.query.offset += this.query.limit;
    }
    this.setState(
      () => ({
        loading: true,
        curateCards: !!this.query.offset ? this.state.curateCards : []
      }),
      () => {
        this.fetchChannelCurateCards();
      }
    );
  };

  render() {
    return (
      <div className="curate-cards-modal">
        <div className="row">
          <span className="select-bar-title" style={this.styles.textStyle}>
            {tr('Card Type')}
          </span>
          <SelectField
            style={this.styles.selectFieldStyle}
            labelStyle={this.styles.dropDownLabelColor}
            iconStyle={this.styles.iconStyleDropDown}
            maxHeight={200}
            onChange={this.filterChange}
            value={this.state.ugcSelectedValue}
          >
            {this.constants.ugcFilter.map((type, index) => {
              return <MenuItem value={type} primaryText={type} key={index} />;
            })}
          </SelectField>
        </div>
        <div className="row">
          <div className="small-12 columns mb-20">
            {!!this.state.curateCards.length && (
              <div className="white-text">
                {tr('Curate Content')} ({this.state.total})
              </div>
            )}
            <div className="close close-button">
              <IconButton aria-label="close" onTouchTap={this.closeModal}>
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row">
                {!!this.state.curateCards.length &&
                  this.state.curateCards.map(card => {
                    return (
                      <div
                        className={`channel-card-v2 custom-medium-6 column ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12`}
                      >
                        <Card
                          key={card.id}
                          author={card.author}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          card={card}
                          channelSetting={true}
                          removeCardFromList={this.removeCardFromList}
                        />
                        <RejectOrPublishCard
                          publishCard={this.publishCard}
                          rejectCard={this.rejectCard}
                          card={card}
                        />
                      </div>
                    );
                  })}
              </div>
              <div className="text-center">
                {this.state.curateCards.length == 0 &&
                  !this.state.loading &&
                  this.state.currentCardCount === this.state.total &&
                  this.state.total <= this.query.limit && (
                    <div className="text-center data-not-available-msg mb-20 white-text">
                      {tr('No items to curate')}
                    </div>
                  )}
              </div>
              <div className="text-center">
                {this.state.curateCards.length === 0 &&
                  !this.state.loading &&
                  this.state.currentCardCount !== this.state.total &&
                  this.state.total > this.query.limit && (
                    <div className="text-center data-not-available-msg mb-20 white-text">
                      {tr('There is more card to curate')}
                    </div>
                  )}
              </div>
              <div className="text-center">
                {this.state.loading && (
                  <div className="text-center data-not-available-msg mb-20 white-text">
                    {tr('Loading...')}
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {this.state.total > this.query.limit &&
              !this.state.loading &&
              this.state.total !== this.state.curateCards.length && (
                <button className="view-more-v2" onClick={this.viewMoreClickHandler}>
                  {this.state.loading ? this.constants.loadingText : this.constants.viewMoreText}
                </button>
              )}
          </div>
        </div>
      </div>
    );
  }
}

CurateCardsModalV2.propTypes = {
  channel: PropTypes.object
};

const mapStateToProps = ({ channelReducerV3 }) => ({
  channel: channelReducerV3.get('channel')
});

export default connect(mapStateToProps)(CurateCardsModalV2);
