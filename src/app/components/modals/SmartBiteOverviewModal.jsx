import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import CardOverviewModal from './CardOverviewModal';
import { tr } from 'edc-web-sdk/helpers/translations';
import { closeCard } from '../../actions/modalActions';
import { reOpenCurateModal } from '../../actions/channelsActionsV2';
import { updateRelevancyRatingQ, checkRatedCard } from '../../actions/relevancyRatings';
import { Permissions } from '../../utils/checkPermissions';
import TextField from 'material-ui/TextField';

class SmartBiteOverviewModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.state = {
      postThisCard: {}
    };
  }

  closeModal = () => {
    this.props.cardUpdated && this.props.cardUpdated();
    this.props.dispatch(closeCard());
    if (
      !checkRatedCard(this.props.card, this.props.relevancyRating.ratedQueue) &&
      this.state.relevancyRatings
    ) {
      this.props.dispatch(updateRelevancyRatingQ(this.props.card));
    }
    let channelId = this.props.channelsV2.activeChannelId;
    if (this.props.modal.dataBefore === undefined && this.props.channelsV2[channelId]) {
      let cardsData = {
        cards: this.props.channelsV2[channelId].curateCards,
        total: this.props.channelsV2[channelId].curatedCardsTotal
      };
      this.props.dispatch(reOpenCurateModal(this.props.channelsV2[channelId], cardsData));
    }
  };

  componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <div className="card-overview">
        <div className="card-overview-header">
          <span className="header-title">{tr('SmartCard')}</span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeBtn}
              aria-label={tr('Close')}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
          <TextField name="smartbiteoverview" autoFocus={true} className="hiddenTextField" />
        </div>
        <CardOverviewModal
          card={this.props.card || this.props.cards[this.props.card.id]}
          defaultImage={this.props.defaultImage}
          cardUpdated={this.props.cardUpdated}
          dueAt={this.props.dueAt}
          startDate={this.props.startDate}
          deleteSharedCard={this.props.deleteSharedCard}
          showComment={Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')}
        />
      </div>
    );
  }
}

SmartBiteOverviewModal.propTypes = {
  card: PropTypes.object,
  cards: PropTypes.object,
  modal: PropTypes.object,
  relevancyRating: PropTypes.object,
  channelsV2: PropTypes.object,
  cardUpdated: PropTypes.func,
  deleteSharedCard: PropTypes.any,
  defaultImage: PropTypes.string,
  startDate: PropTypes.string,
  dueAt: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS(),
    cards: state.cards.toJS(),
    channelsV2: state.channelsV2.toJS(),
    relevancyRating: state.relevancyRating.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(SmartBiteOverviewModal);
