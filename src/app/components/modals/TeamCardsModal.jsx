import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { connect } from 'react-redux';
import Card from './../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTeamCards, deleteSharedCard } from '../../actions/groupsActionsV2';
import { close, confirmation } from '../../actions/modalActions';
import TextField from 'material-ui/TextField';

class TeamCardsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      loading: false
    };
    this.styles = {
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true });
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let type = group.cardModalType;
    let limit = group['cardsLimit'];
    let offset = group[group.cardAryName + 'Offset'] + limit;
    this.props
      .dispatch(getTeamCards(groupID, group.cardAryName, { limit, offset, type }))
      .then(() => {
        this.setState({ loading: false, offset });
      })
      .catch(err => {
        console.error(`Error in TeamCardsModal.getTeamCards.func : ${err}`);
      });
  };

  deleteSharedCardHandler = payload => {
    let _this = this;
    let groupID = this.props.groupsV2.currentGroupID;
    this.props.dispatch(
      confirmation(
        'Remove from Group',
        `Group members will no longer be able to see this card in the group.`,
        () => {
          _this.props.dispatch(deleteSharedCard(groupID, payload));
        }
      )
    );
  };

  render() {
    let group = this.props.groupsV2[this.props.groupsV2.currentGroupID];
    let title = group && group['cardModalDesc'];
    let count = group && group['cardModalCount'];
    let arryName = group && group['cardAryName'];
    let cards = group && group[arryName];
    let limit = group && group['cardsLimit'];
    return (
      <div className="channel-cards-modal">
        <div className="row">
          <TextField name="teamcard" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>{group && tr(title) + '(' + count + ')'}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.iconButton}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row ">
                {group &&
                  cards.length > 0 &&
                  cards.map(card => {
                    return (
                      <div
                        key={card.id}
                        className={`channel-card-v2 card-v2 custom-medium-6 column  ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12`}
                      >
                        <Card
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={arryName !== 'teamAssignments'}
                          moreCards={false}
                          deleteSharedCard={
                            this.props.deleteSharedCard
                              ? this.deleteSharedCardHandler.bind(this, { card_id: card.id })
                              : false
                          }
                          groupSetting={true}
                        />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {group && cards.length < count && (
              <button
                disabled={this.state.loading}
                className="view-more-v2"
                onClick={this.viewMoreClickHandler}
              >
                {this.state.loading ? tr('Loading...') : tr('View More')}
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

TeamCardsModal.propTypes = {
  currentUser: PropTypes.object,
  groupsV2: PropTypes.object,
  deleteSharedCard: PropTypes.any
};

export default connect(state => ({
  modal: state.modal.toJS(),
  currentUser: state.currentUser.toJS(),
  groupsV2: state.groupsV2.toJS()
}))(TeamCardsModal);
