import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Card from './../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import CurateCardButtons from './../channel/CurateCardButtons';
import {
  getNonCurateCards,
  reOpenCurateModal,
  getTypeCards,
  pinUnpinCard
} from '../../actions/channelsActionsV2';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';

const ugcFilter = ['All', 'User Generated Cards'];
class CurateCardsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      channel: this.props.channelsV2[this.props.channelId],
      cards: this.props.channelsV2[this.props.channelId].curateCards || [],
      total: this.props.channelsV2[this.props.channelId].curatedCardsTotal || 0,
      loading: false,
      ugcSelectedValue: 'All'
    };
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
    this.curateCards = this.props.channelsV2[this.props.channelId].curateCards.cards;
    this.styles = {
      textStyle: {
        marginTop: '1rem',
        color: '#fff'
      }
    };
  }

  viewMoreClickHandler = () => {
    let isUgc = this.state.ugcSelectedValue == 'User Generated Cards';
    this.setState({ loading: true });
    this.callgetNonCurateCards(
      isUgc,
      this.state.channel,
      this.curateCards.length,
      false,
      this.curateCards
    );
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  rejectPublishCallback = (cardId, channelId) => {
    var cardsData = this.state.cards;
    var publishedCard = {};
    for (let i = 0; i < cardsData.length; i++) {
      if (cardsData[i].id == cardId) {
        publishedCard = cardsData[i];
        cardsData.splice(i, 1);
        break;
      }
    }
    this.setState({ cards: cardsData });
    this.setState({ total: --this.state.total });
    cardsData = { cards: this.state.cards, total: this.state.total };
    this.props.dispatch(reOpenCurateModal(this.state.channel, cardsData, publishedCard));
  };

  removeCardFromList = id => {
    let newCardsList = this.state.curateCards.filter(card => {
      return card.id !== id;
    });
    this.setState({ curateCards: newCardsList });
  };

  rejectCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({
      cards: newCardsList,
      total: --this.state.total
    });
    var cardsData = { cards: this.state.cards, total: this.state.total };
    this.props.dispatch(reOpenCurateModal(this.state.channel, cardsData));
  };

  filterChange = (e, index, value) => {
    let isUgc = value == 'User Generated Cards';
    this.callgetNonCurateCards(isUgc, this.state.channel, 0, true);
    this.setState({ ugcSelectedValue: value });
  };

  callgetNonCurateCards = (isUgc, channel, curateCardsLen, isInitialize, existingCards) => {
    this.props
      .dispatch(getNonCurateCards(isUgc, channel, curateCardsLen, isInitialize, existingCards))
      .then(() => {
        this.setState({ loading: false });
        this.setState({
          cards: this.props.channelsV2[this.props.channelId].curateCards,
          total: this.props.channelsV2[this.props.channelId].curatedCardsTotal
        });
      })
      .catch(err => {
        console.error(`Error in CurateCardsModal.getNonCurateCards.func : ${err}`);
      });
  };
  render() {
    this.curateCards = this.state.cards;
    return (
      <div className="curate-cards-modal">
        <div className="row">
          <TextField name="curatecardsmodal" autoFocus={true} className="hiddenTextField" />
          <span className="select-bar-title" style={this.styles.textStyle}>
            {tr('Card Type')}
          </span>
          <SelectField
            style={{ verticalAlign: 'middle' }}
            labelStyle={{ color: '#fff' }}
            iconStyle={{
              width: '1.87rem',
              height: '1.87rem',
              top: '0.437rem',
              fill: '#fff'
            }}
            maxHeight={200}
            onChange={this.filterChange}
            value={this.state.ugcSelectedValue}
          >
            {ugcFilter.map((type, index) => {
              return <MenuItem value={type} primaryText={tr(type)} key={index} />;
            })}
          </SelectField>
        </div>
        <div className="row">
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            {this.curateCards.length != 0 && (
              <div style={{ color: 'white' }}>
                {tr('Curate Content')} ({this.state.total})
              </div>
            )}
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row">
                {this.curateCards.length > 0 &&
                  this.curateCards.map(card => {
                    return (
                      <div
                        className={`channel-card-v2 custom-medium-6 column ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12`}
                      >
                        <Card
                          key={card.id}
                          author={card.author}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          card={card}
                          channelSetting={true}
                          removeCardFromList={this.removeCardFromList}
                        />
                        <CurateCardButtons
                          card={card}
                          rejectPublishCallback={this.rejectPublishCallback}
                          channel={this.state.channel}
                          cardList={{ cards: this.curateCards, total: this.state.total }}
                          rejectCardFromList={this.rejectCardFromList}
                        />
                      </div>
                    );
                  })}
              </div>
              <div className="text-center">
                {this.curateCards.length == 0 && (
                  <div className="text-center data-not-available-msg" style={{ color: 'white' }}>
                    {tr('No items to curate')}
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="small-12 columns text-center">
            {this.state.total > this.curateCards.length && (
              <button className="view-more-v2" onClick={this.viewMoreClickHandler}>
                {this.state.loading ? tr('Loading...') : tr('View More')}
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }
}

CurateCardsModal.propTypes = {
  channelsV2: PropTypes.object,
  channelId: PropTypes.string
};

export default connect(state => ({
  channelsV2: state.channelsV2.toJS()
}))(CurateCardsModal);
