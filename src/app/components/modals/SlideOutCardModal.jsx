import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import { push } from 'react-router-redux';
import remove from 'lodash/remove';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import find from 'lodash/find';
import isEmpty from 'lodash/isEmpty';
import ReactStars from 'react-stars';

import { recordVisit } from 'edc-web-sdk/requests/analytics';

import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import Downloadv2 from 'edc-web-sdk/components/icons/Downloadv2';

import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';

import { loadComments, toggleLikeCardAsync } from '../../actions/cardsActions';
import { submitRatings, updateRatedQueueAfterRating } from '../../actions/relevancyRatings';
import { close, openCardStatsModal } from '../../actions/modalActions';

import SmartBiteCardHeader from '../common/SmartBiteCardHeader';
import MarkdownRenderer from '../common/MarkdownRenderer';
import Spinner from '../common/spinner';
import SvgImageResized from '../common/ImageResized';
import InlineVideo from '../../components/feed/InlineVideo.jsx';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import CommentList from '../../components/feed/CommentList.v2';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import Poll from '../../components/feed/Poll';
import VideoStream from '../../components/feed/VideoStream';
import LiveCommentList from '../../components/feed/LiveCommentList';

import checkResources from '../../utils/checkResources';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import getCardType from '../../utils/getCardType';
import getCardImage from '../../utils/getCardImage';
import getTranscodedVideo from '../../utils/getTranscodedVideo';
import { Permissions } from '../../utils/checkPermissions';
import getCardParams from '../../utils/getCardParams';
import linkPrefix from '../../utils/linkPrefix';
import getDefaultImage from '../../utils/getDefaultCardImage';
import checkResourceURL from '../../utils/checkResourceURL';
import addTabInCardRating from '../../utils/addTabInCardRating';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import TextField from 'material-ui/TextField';
import * as upshotActions from '../../actions/upshotActions';
import convertRichText from '../../utils/convertRichText';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import thumbnailImage from '../../utils/thumbnailImage';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';

let GetVideoId = require('get-video-id');

let LocaleCurrency = require('locale-currency');

class SlideOutCardModal extends Component {
  constructor(props, context) {
    super(props, context);
    let isLiveStream = false;
    if (props.card.cardType == 'VideoStream' || props.card.cardType == 'video_stream') {
      if (props.card && props.card.videoStream !== undefined) {
        isLiveStream = props.card.videoStream.status == 'live';
      } else if (props.card.status) {
        isLiveStream = props.card.status == 'live';
      }
    }

    this.state = {
      isLiveStream,
      card: props.card,
      pendingLike: false,
      ratingCount: props.ratingCount,
      averageRating: props.card.averageRating,
      commentsCount: props.card.commentsCount,
      voters: props.card.voters,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      ratingBlock: {
        display: 'inline-flex',
        paddingLeft: '1rem'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      audio: {
        width: '100%'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '-0.2rem',
        marginBottom: 0,
        left: '1rem'
      },
      downloadButton: {
        width: '100%',
        height: '100%',
        padding: 0,
        background: '#454560',
        marginTop: '1rem',
        marginLeft: '-4rem',
        cursor: 'pointer'
      },
      tooltipStyles: {
        left: '-50%'
      },
      btnIcons: {
        verticalAlign: 'middle',
        width: '1rem',
        height: '0.875rem',
        padding: 0
      }
    };
  }

  componentDidMount = () => {
    recordVisit(this.state.card.id);
    fetchCard(this.state.card.id)
      .then(card => {
        this.setState({ card }, () => {
          setTimeout(() => {
            addTabInCardRating(this.ratingChanged);
          }, 500);
        });
      })
      .catch(err => {
        console.error(`Error in SlideOutCardModal.fetchCard.func : ${err}`);
      });
  };

  componentWillMount() {
    this.fetchComments();
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  fetchComments = () => {
    let cardId = this.state.card.cardId || this.state.card.id;
    let isECL = /^ECL-/.test(cardId);
    if (!isECL && this.state.commentsCount) {
      loadComments(
        cardId,
        this.state.commentsCount,
        this.state.card.cardType,
        cardId ? cardId : null
      )
        .then(
          data => {
            this.setState({ comments: data, showComment: true });
          },
          error => {
            console.warn(`error when loadComments for card ${cardId}. Error: ${error}`);
          }
        )
        .catch(err => {
          console.error(`Error in CardOverviewModal.loadComments.func: ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  };

  cardUpdated = () => {
    this.props.cardUpdated && this.props.cardUpdated();
  };

  likeCardHandler = () => {
    if (this.state.pendingLike) {
      return;
    }
    this.setState({ pendingLike: true }, () => {
      let card = this.state.card;
      toggleLikeCardAsync(
        this.state.card.cardId || this.state.card.id,
        this.state.card.cardType,
        !card.isUpvoted
      )
        .then(() => {
          if (card.isUpvoted) {
            remove(card.voters, voter => {
              return voter.id == this.props.currentUser.id;
            });
          } else {
            let newLike = {
              id: this.props.currentUser.id,
              name: this.props.currentUser.name,
              handle: `@${this.props.currentUser.handle}`,
              avatar: this.props.currentUser.avatar
            };
            card.voters.push(newLike);
          }
          card.isUpvoted = !card.isUpvoted;
          card.votesCount = Math.max(card.votesCount + (card.isUpvoted ? 1 : -1), 0);
          this.setState({
            card,
            pendingLike: false
          });
          if (this.state.upshotEnabled) {
            let name = (card.resource && card.resource.title) || card.message;
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: card.resource.title,
              category: card.cardType,
              type: card.cardSubtype,
              description: card.resource.description,
              status: !!card.completionState ? card.completionState : 'Incomplete',
              rating: card.averageRating,
              like: card.isUpvoted ? 'Yes' : 'No',
              event: card.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
            });
          }
        })
        .catch(err => {
          console.error(`Error in CardOverviewModal.toggleLikeCardAsync.func: ${err}`);
        });
    });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count }, () => {
      if (count === -1) {
        this.setState({ showComment: false }, this.fetchComments);
      }
    });
  };

  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}`
    ) {
      return;
    }
    this.closeModal();
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };

  downloadBlock(file) {
    let downloadLabel = tr('Download');
    return (
      <span className="roll">
        <a href={file.url} download={file.handle} target="_blank">
          <IconButton
            aria-label={downloadLabel}
            iconStyle={this.styles.btnIcons}
            style={this.styles.downloadButton}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={downloadLabel}
          >
            <Downloadv2 />
          </IconButton>
        </a>
      </span>
    );
  }

  linkExtract = (resource, cardId) => {
    return typeof resource.url !== 'undefined'
      ? cardId
        ? `/insights/${encodeURIComponent(cardId)}/visit`
        : resource.url
      : `/insights/${cardId}`;
  };

  liveCommentsCount = commentCount => {
    this.setState({ liveCommentsCount: commentCount });
  };

  getCardStatus() {
    if (this.props.card && this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.status;
    } else if (this.props.card && this.props.card.status) {
      return this.props.card.status;
    } else {
      return '';
    }
  }

  getCardUuid() {
    if (this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.uuid;
    } else if (this.props.card.uuid !== undefined) {
      return this.props.card.uuid;
    } else {
      return '';
    }
  }

  getPricingPlans() {
    return this.state.edcastPlansForPricing &&
      this.props.card.cardMetadatum &&
      this.props.card.cardMetadatum.plan
      ? tr(startCase(toLower(this.props.card.cardMetadatum.plan)))
      : tr('Free');
  }

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  closeModal = () => {
    this.props.dispatch(close());
  };

  // work with rating
  ratingChanged = userRating => {
    this.setState({
      averageRating: userRating
    });
    let payload = {
      rating: userRating
    };
    submitRatings(this.props.card.id, payload)
      .then(response => {
        this.setState({
          averageRating: response.averageRating
        });
        this.allCountRating(response);
      }, this)
      .catch(err => {
        console.error(`Error in CommentsBlockSmartBite.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.props.card));
  };

  allCountRating = smartBite => {
    if (smartBite.allRatings) {
      let ratingCount = 0;
      for (let i = 1; i <= 5; i++) {
        ratingCount += smartBite.allRatings[i] ? smartBite.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
    }
  };

  onUserHandle = handle => {
    this.closeModal();
    this.props.dispatch(
      push(`/${handle.replace('@', '') === this.props.currentUser.handle ? 'me' : handle}`)
    );
  };

  imageContainer = image => {
    let svgStyle = {
      filter: `url(#blur-effect-overview-${this.props.card.id})`
    };
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let img = addSecurity(image, expireAfter, this.props.currentUser.id);
    return (
      <div className="main-image card-image-block">
        <div style={this.styles.imgStyle} className="card-blurred-background">
          <svg width="100%" height="100%">
            <title>{this.props.card.message}</title>
            <SvgImageResized
              cardId={`${this.props.card.id}`}
              resizeOptions={'height:410'}
              style={svgStyle}
              xlinkHref={img}
              x="-30%"
              y="-30%"
              width="160%"
              height="160%"
            />
            <filter id={`blur-effect-overview-${this.props.card.id}`}>
              <feGaussianBlur stdDeviation="10" />
            </filter>
          </svg>
        </div>
        <svg width="100%" height="100%" style={this.styles.mainSvg}>
          <title>
            <RichTextReadOnly text={this.props.card.message || ''} />
          </title>
          <SvgImageResized
            cardId={`${this.props.card.id}`}
            resizeOptions={'height:410'}
            xlinkHref={img}
            width="100%"
            style={this.styles.svgImage}
            height="100%"
          />
        </svg>
      </div>
    );
  };

  embedUrl(url) {
    let sources = ['vimeo', 'youtube'];
    let embed_url = url;
    let json_data = GetVideoId(url);

    if (json_data && json_data.id && !!~sources.indexOf(json_data.service)) {
      if (json_data.service === 'vimeo') {
        embed_url = 'https://player.vimeo.com/video/' + json_data.id;
      } else if (json_data.service === 'youtube') {
        embed_url = '//www.youtube.com/embed/' + json_data.id;
      }
    }
    return embed_url;
  }

  truncateMessageText(message) {
    return message.substr(0, 260);
  }

  handleCardClicked = e => {
    this.closeModal();
    this.props.handleCardClicked(e);
  };

  standaloneLinkClickHandlerWithClose = () => {
    this.closeModal();
    this.props.standaloneLinkClickHandler();
  };

  handleCardAnalyticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.card));
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let card = checkResources(this.state.card);
    let voters = this.state.card.voters;
    let params = getCardParams(card, this.props, 'card') || {};
    let isFileAttached = card.filestack && !!card.filestack.length;
    let videoFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('video/')
    );
    let imageFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('image/')
    );
    let audioFileStack = !!(
      isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('audio/')
    );
    let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
    let cardType = audioFileStack ? 'AUDIO' : getCardType(card, videoFileStack, fileFileStack);
    let imageExists =
      (card.resource &&
        card.resource.imageUrl &&
        !~card.resource.imageUrl.indexOf('default_card_image')) ||
      imageFileStack;
    if (cardType === 'VIDEO') {
      imageExists =
        (card.videoStream &&
          card.videoStream.imageUrl &&
          !~card.videoStream.imageUrl.indexOf('default_card_image')) ||
        imageExists;
    }
    let gDriveLink;
    if (cardType === 'ARTICLE') {
      gDriveLink =
        card.resource &&
        card.resource.url &&
        (!!~card.resource.url.indexOf('docs.google.com') ||
          !!~card.resource.url.indexOf('drive.google.com') ||
          !!~card.resource.url.indexOf('sharepoint.com'));
    }
    let cardImage = getCardImage(card, imageFileStack);
    let defaultImage = this.props.defaultImage || getDefaultImage(this.props.currentUser.id).url;
    let cardSvgBackground = imageExists ? cardImage : defaultImage;
    let htmlRender = !!card.resource && (!!card.resource.title || !!card.resource.description);

    let resourceUrl = htmlRender || gDriveLink ? this.linkExtract(card.resource, card.id) : null;
    let scormCard = card.filestack && card.filestack[0] && card.filestack[0].scorm_course;
    let resourceImage = params.poster || cardSvgBackground;

    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = {};
    let skillcoin_image = '/i/images/skillcoin-new.png';

    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: 'SKILLCOIN' }) ||
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }

    if (priceData && priceData.currency == 'SKILLCOIN') {
      priceData.symbol = <img className="pricing_skill_coins_label_icon" src={skillcoin_image} />;
    }
    let maxNum = 9;

    const isPptFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/vnd.ms-powerpoint')
    );
    const isPptxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      )
    );
    const isDocFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/msword')
    );
    const isDocxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      )
    );
    const isPdfFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/pdf')
    );

    // If we have an image at position 0 in fileResources, we need to display instead of FileStack
    if (
      card.fileResources &&
      card.fileResources.length &&
      card.fileResources[0].fileType === 'image' &&
      !params.audioFileStack
    ) {
      params.fileFileStack = false;
    }

    let viewMore = this.state.truncateTitle ? (
      <span>
        ...
        <a className="viewMore" onTouchTap={this.viewMoreTitleHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let viewMoreMessage = this.state.truncateMessage ? (
      <span>
        ...
        <a className="viewMore" target="_blank" onTouchTap={this.viewMoreMessageHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;

    let isCompleted =
      this.props.card.completionState &&
      this.props.card.completionState.toUpperCase() === 'COMPLETED';

    let isOwner = card.author && card.author.id == this.props.currentUser.id;

    return (
      <div className="slide-out-card-modal-container">
        <div className="backdrop" onClick={this.closeModal} />
        <div className="slide-out-modal">
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeBtn}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="black" />
            </IconButton>
          </div>
          <TextField name="addtochannel" autoFocus={true} className="hiddenTextField" />
          <div className="slide-out__white-block">
            <div className="slide-out__file-block">
              {!params.isFileAttached &&
                params.imageFileResources &&
                this.imageContainer(
                  (card.fileResources &&
                    card.fileResources.length > 0 &&
                    card.fileResources[0].fileUrl) ||
                    card.image
                )}

              {params.isFileAttached && (
                <div>
                  {params.imageFileStack &&
                    !params.imageFileResources &&
                    !params.fileFileStack &&
                    this.imageContainer(card.filestack && card.filestack[0].url)}

                  {!(
                    params.cardType === 'POLL' &&
                    card.resource &&
                    card.resource.type &&
                    card.resource.type.toLowerCase() === 'video'
                  ) &&
                    (card.filestack[0].mimetype &&
                      card.filestack[0].mimetype.indexOf('video/') > -1) && (
                      <div key={`card-handle-${card.filestack[0].handle}`} className="fp fp_video">
                        <span>
                          <video
                            onMouseEnter={() => {
                              this.setState({ [card.filestack[0].handle]: true });
                            }}
                            onMouseLeave={() => {
                              this.setState({ [card.filestack[0].handle]: false });
                            }}
                            controls
                            src={card.filestack[0].url}
                            poster={
                              card.filestack &&
                              card.filestack[1] &&
                              addSecurity(
                                card.filestack[1].url,
                                expireAfter,
                                this.props.currentUser.id
                              )
                            }
                            preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                            autoPlay={this.props.team.config['enable-video-auto-play']}
                            controlsList={params.isDownloadContentDisabled ? 'nodownload' : ''}
                          />
                          {!params.isDownloadContentDisabled &&
                            this.downloadBlock(card.filestack[0])}
                        </span>
                      </div>
                    )}
                  {!(
                    params.cardType === 'POLL' &&
                    card.resource &&
                    card.resource.type &&
                    card.resource.type.toLowerCase() === 'video'
                  ) &&
                    (card.filestack[0].mimetype &&
                      card.filestack[0].mimetype.indexOf('video/') === -1) &&
                    card.filestack.map(file => {
                      if (
                        (isPptFile || isPptxFile || isDocFile || isDocxFile || isPdfFile) &&
                        (card &&
                          card.filestack &&
                          card.filestack[1] &&
                          card.filestack[1].mimetype &&
                          card.filestack[1].mimetype.includes('image/')) &&
                        file.mimetype &&
                        file.mimetype.includes('image/')
                      ) {
                        return;
                      }
                      if (
                        file.mimetype &&
                        ~file.mimetype.indexOf('image/') &&
                        (card.fileResources &&
                          card.fileResources.length === 0 &&
                          !card.filestack.scorm_course)
                      ) {
                        return;
                      } else if (
                        file.mimetype &&
                        ~file.mimetype.indexOf('image/') &&
                        (card.fileResources &&
                          card.fileResources.length > 0 &&
                          card.fileResources[0].filestack &&
                          !card.fileResources[0].filestack.scorm_course)
                      ) {
                        return this.imageContainer(file.url);
                      } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                        return (
                          <div key={`handle-${file.handle}`} className="fp fp_video">
                            <span>
                              {this.state.transcodedVideoStatus &&
                                (this.state.transcodedVideoStatus === 'completed' ? (
                                  <video
                                    onMouseEnter={() => {
                                      this.setState({ [file.handle]: true });
                                    }}
                                    onMouseLeave={() => {
                                      this.setState({ [file.handle]: false });
                                    }}
                                    controls
                                    src={this.state.transcodedVideoUrl}
                                    controlsList={
                                      params.isDownloadContentDisabled ? 'nodownload' : ''
                                    }
                                    autoPlay={this.props.team.config['enable-video-auto-play']}
                                  />
                                ) : (
                                  <img
                                    className="waiting-video"
                                    src="/i/images/video_processing_being_processed.jpg"
                                  />
                                ))}
                              {!params.isDownloadContentDisabled && this.downloadBlock(file)}
                            </span>
                          </div>
                        );
                      } else if (
                        !scormCard &&
                        !(file.mimetype && ~file.mimetype.indexOf('audio/'))
                      ) {
                        return (
                          <span>
                            <div key={`card-file-handle-${file.handle}`} className="fp fp_preview">
                              <span>
                                {params.cardType === 'FILE' ? (
                                  <iframe
                                    height="500px"
                                    width="100%"
                                    src={pdfPreviewUrl(
                                      file.url,
                                      expireAfter,
                                      this.props.currentUser.id
                                    )}
                                  />
                                ) : (
                                  <a className="fp inline-box" href={resourceUrl} target="_blank" />
                                )}
                                {!params.isDownloadContentDisabled && this.downloadBlock(file)}
                              </span>
                            </div>
                          </span>
                        );
                      }
                    })}
                </div>
              )}
              {((card.resource &&
                (card.resource.url || card.resource.description || card.resource.fileUrl)) ||
                this.isCsodCourse) && (
                <div>
                  <div className="resource">
                    {(card.resource.embedHtml || card.resource.videoUrl || card.resource.url) &&
                      card.resource.type === 'Video' && (
                        <InlineVideo
                          sourceType={'slideOut'}
                          cardId={card.id || card.cardId}
                          embedHtml={
                            card.resource.embedHtml ||
                            (checkResourceURL(
                              card.resource.videoUrl || this.embedUrl(card.resource.url)
                            )
                              ? `<iframe width="1" src="${card.resource.videoUrl ||
                                  this.embedUrl(card.resource.url)}"></iframe>`
                              : thumbnailImage(
                                  this.state.card.cardId || this.state.card.id,
                                  addSecurity(
                                    this.state.card.resource.imageUrl,
                                    expireAfter,
                                    this.props.currentUser.id
                                  ),
                                  this.props.currentUser.id
                                ))
                          }
                          videoUrl={card.resource.videoUrl}
                        />
                      )}
                    {!(card.resource.embedHtml || card.resource.videoUrl || card.resource.url) &&
                      params.isYoutubeVideo &&
                      card.resource.imageUrl && (
                        <a href={card.resourceUrl} target="_blank">
                          <img
                            style={this.styles.youtubeImg}
                            src={addSecurity(
                              card.resource.imageUrl,
                              expireAfter,
                              this.props.currentUser.id
                            )}
                          />
                        </a>
                      )}
                    {card.resource.fileUrl && (
                      <img
                        src={addSecurity(
                          card.resource.fileUrl,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                      />
                    )}
                  </div>
                </div>
              )}

              {scormCard &&
                !(!!card.resource && !!card.resource.url) &&
                card.state &&
                card.state !== 'published' && (
                  <div className="article-resource">
                    <div className="description">{this.imageContainer(resourceImage)}</div>
                  </div>
                )}

              {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
                <VideoStream
                  card={card}
                  standaloneLinkClickHandler={this.standaloneLinkClickHandlerWithClose}
                />
              )}

              {(params.cardType === 'ARTICLE' ||
                card.cardType === 'course' ||
                this.isCsodCourse) && (
                <div className="article-resource">
                  <div className="description">{this.imageContainer(resourceImage)}</div>
                </div>
              )}
            </div>
            <div>
              <SmartBiteCardHeader
                closeModal={this.closeModal}
                card={this.props.card}
                params={params}
                isSlideOutCard={true}
                authorName={this.props.authorName}
                cardClickHandle={this.state.cardClickHandle}
                standaloneLinkClickHandler={this.standaloneLinkClickHandlerWithClose}
                allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
                newSkillLevel={this.props.card.newSkillLevel}
                rateCard={this.props.rateCard}
                logoObj={this.props.logoObj}
                handleCardClicked={this.handleCardClicked}
                avatarDiameter="2.5rem"
              />
            </div>
            <div className={`my-card__text-info cursor`}>
              <div className="vertical-spacing-small">
                <div>
                  {params.cardType === 'TEXT' ||
                  params.cardType === 'IMAGE' ||
                  params.cardType === 'PROJECT' ? (
                    <div className="vertical-spacing-large">
                      <div
                        className={
                          params.imageExists
                            ? `openCardFromSearch button-icon title title-with-image`
                            : 'openCardFromSearch button-icon title text-title'
                        }
                      >
                        {this.props.card.title && (
                          <div className="card-title">
                            <MarkdownRenderer
                              markdown={(this.state.truncateTitle
                                ? card.title.substr(0, 260)
                                : card.title
                              ).replace(/\n/gi, '\n\n')}
                            />
                            {viewMore}
                          </div>
                        )}

                        {params.message && (
                          <div className="message">
                            <RichTextReadOnly
                              text={convertRichText(
                                this.state.truncateMessage
                                  ? this.truncateMessageText(params.message)
                                  : params.message
                              )}
                            />
                            {viewMoreMessage}
                          </div>
                        )}
                      </div>
                    </div>
                  ) : params.cardType === 'AUDIO' ? (
                    <audio
                      controls
                      src={addSecurity(
                        this.props.card.filestack[0].url,
                        expireAfter,
                        this.props.currentUser.id
                      )}
                      style={this.styles.audio}
                    />
                  ) : (
                    <div
                      className={`${!(params.cardType === 'POLL' || params.cardType === 'QUIZ') &&
                        'vertical-spacing-large'}`}
                    >
                      <div
                        className={`button-icon title ${(params.cardType === 'POLL' ||
                          params.cardType === 'QUIZ') &&
                          'quiz-title'} ${this.props.card.author ? 'has-author' : 'no-author'}`}
                      >
                        {!!this.props.card.resource &&
                        (!!this.props.card.resource.title ||
                          !!this.props.card.resource.description) ? (
                          <div>
                            {this.props.card.resource.title &&
                              this.props.card.resource.title !== this.props.card.resource.url && (
                                <div
                                  className="pointer card-details__title_bold"
                                  onClick={
                                    scormCard && this.props.card.hidden
                                      ? null
                                      : this.handleCardClicked
                                  }
                                  dangerouslySetInnerHTML={{
                                    __html:
                                      this.props.card.message || this.props.card.resource.title
                                  }}
                                />
                              )}
                            {this.props.card.resource.description &&
                              this.props.card.resource.description !== params.message && (
                                <div
                                  className="text-title pointer"
                                  onClick={this.handleCardClicked}
                                  dangerouslySetInnerHTML={{
                                    __html: this.props.card.resource.description
                                  }}
                                />
                              )}
                            {params.message && this.props.card.title && (
                              <div
                                className="text-title pointer"
                                dangerouslySetInnerHTML={{ __html: params.message }}
                              />
                            )}
                          </div>
                        ) : (
                          <div>
                            <div
                              onClick={
                                scormCard && this.props.card.hidden ? null : this.handleCardClicked
                              }
                              className={`pointer ${
                                this.props.card.cardType === 'pack' ||
                                this.props.card.cardType === 'journey'
                                  ? 'card-details__title_bold'
                                  : ''
                              }`}
                              dangerouslySetInnerHTML={{ __html: params.message }}
                            />
                            {(this.props.card.cardType === 'pack' ||
                              this.props.card.cardType === 'journey') &&
                              !!this.props.card.title && (
                                <div
                                  className="text-title pointer"
                                  onClick={this.handleCardClicked}
                                  dangerouslySetInnerHTML={{ __html: this.props.card.message }}
                                />
                              )}
                          </div>
                        )}
                      </div>
                      {(params.cardType === 'POLL' || params.cardType === 'QUIZ') &&
                        !params.isShowTopImage && (
                          <div
                            className="poll-content poll-results-container poll-slideout"
                            onClick={this.handleCardClicked}
                          >
                            <Poll card={this.props.card} cardUpdated={this.props.cardUpdated} />
                          </div>
                        )}
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="slide-out__bottom-block">
              <div>
                {this.props.card.eclDurationMetadata &&
                  this.props.card.eclDurationMetadata.calculated_duration_display &&
                  this.props.card.eclDurationMetadata.calculated_duration > 0 && (
                    <span className="read-time-ago__component">
                      <span className="read-time-ago">
                        {tr('Duration')}:{' '}
                        {this.props.card.eclDurationMetadata.calculated_duration_display} |
                      </span>
                    </span>
                  )}
                {this.state.edcastPricing && this.props.card.readableCardType != 'jobs' && (
                  <span className="edcast-pricing">
                    {tr('Price')} :{' '}
                    {this.props.card.isPaid && priceData && priceData.amount ? (
                      <span>
                        {priceData.symbol}
                        {priceData.amount}
                      </span>
                    ) : (
                      this.getPricingPlans()
                    )}
                  </span>
                )}
              </div>
              <div className="insight-action-bar main-actions-container">
                {Permissions.has('LIKE_CONTENT') && (
                  <div className="like-container">
                    <button
                      className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                      onClick={this.likeCardHandler}
                      aria-label="like"
                    >
                      {!this.state.card.isUpvoted && <LikeIcon />}
                      {this.state.card.isUpvoted && <LikeIconSelected />}
                      <span className="tooltiptext">{tr('Like')}</span>
                    </button>
                    <small className="count">
                      {card.votesCount ? abbreviateNumber(card.votesCount) : ''}
                    </small>
                  </div>
                )}
                {card.cardType !== 'QUIZ' &&
                  card.author &&
                  (isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                    <div className="icon-inline-block my-icon-button-block side-spacing">
                      <button
                        aria-label={tr('Card Statistics')}
                        className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center statistics"
                        onClick={this.handleCardAnalyticsModal}
                      >
                        <span tabIndex={-1} className="hideOutline">
                          <CardAnalyticsV2 />
                        </span>
                        <span className="tooltiptext">{tr('Card Statistics')}</span>
                      </button>
                    </div>
                  )}
                {!isEmpty(voters) && (
                  <span style={{ paddingLeft: '20px' }}>
                    {voters.slice(0, maxNum).map(voter => {
                      return (
                        <span style={{ padding: '0 4px' }}>
                          <button
                            className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                            onClick={this.onUserHandle.bind(this, voter.handle)}
                          >
                            <Avatar
                              size={24}
                              src={
                                (voter.avatarimages && voter.avatarimages.tiny) ||
                                voter.avatar ||
                                ''
                              }
                            />
                            <span className="tooltiptext">{voter.name}</span>
                          </button>
                        </span>
                      );
                    })}
                    {voters.length > maxNum && <span style={{ color: '#4a90e2' }}>{maxNum}+</span>}
                  </span>
                )}
                <div className="star-block" style={this.styles.ratingBlock}>
                  {Permissions.has('CAN_RATE') && (
                    <div>
                      <ReactStars
                        onChange={this.ratingChanged}
                        count={5}
                        size={16}
                        half={false}
                        color1={'#d6d6e1'}
                        color2={'#6f708b'}
                        className="relevancyRatingStars relevancyRatingStars_tab"
                        value={this.state.averageRating ? parseInt(this.state.averageRating) : 0}
                      />
                      <span>
                        ({this.state.ratingCount > 100 ? '99+' : +this.state.ratingCount})
                      </span>
                    </div>
                  )}
                </div>
                <InsightDropDownActions
                  card={card}
                  isCompleted={isCompleted}
                  author={this.state.card.author}
                  dismissible={this.props.dismissible}
                  disableTopics={false}
                  isStandalone={false}
                  cardUpdated={this.cardUpdated.bind(this)}
                  type={'SlideOut'}
                />
              </div>
            </div>
          </div>
          <div className="slide-out__comment-block">
            {this.state.showComment && !this.isCsodCourse && (
              <div className="comments-block">
                {!this.state.isLiveStream && (
                  <CommentList
                    isSlideout={true}
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.state.card.author ? this.state.card.author.id : ''}
                    cardType={card.cardType}
                    comments={this.state.comments}
                    pending={card.commentPending}
                    numOfComments={this.state.commentsCount}
                    videoId={card.cardId ? card.id : null}
                    isCardV3={this.state.isCardV3}
                    updateCommentCount={this.updateCommentCount}
                    slideOutAvatarBoxStyle={this.styles.avatarBox}
                  />
                )}
                {this.state.isLiveStream && (
                  <LiveCommentList
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.state.card.author ? this.state.card.author.id : ''}
                    cardType={card.cardType}
                    pending={card.commentPending}
                    videoId={card.cardId ? card.id : null}
                    updateCommentCount={this.updateCommentCount}
                    cardStatus={this.getCardStatus()}
                    cardUuid={this.getCardUuid()}
                    isCardV3={this.state.isCardV3}
                    liveCommentsCount={this.liveCommentsCount}
                    onMessageReceiveJoinLeave={this.onMessageReceiveJoinLeave}
                  />
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

SlideOutCardModal.propTypes = {
  card: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  cardUpdated: PropTypes.func,
  pathname: PropTypes.string,
  defaultImage: PropTypes.string,
  logoObj: PropTypes.object,
  dismissible: PropTypes.bool,
  ratingCount: PropTypes.number,
  handleCardClicked: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  rateCard: PropTypes.any,
  authorName: PropTypes.string,
  averageRating: PropTypes.any,
  providerLogos: PropTypes.object,
  isCompleted: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(SlideOutCardModal);
