import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import PointerArrow from 'edc-web-sdk/components/icons/PointerArrow';
import TaxonomyNode from './TaxonomyNode';
class MultilevelTaxonomyContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showAlert: false,
      errorMessage: '',
      postPending: false,
      targetPosition:
        this.props.targetPosition &&
        this.props.targetPosition.getBoundingClientRect &&
        this.props.targetPosition.getBoundingClientRect(),
      subTopics: this.props.subTopics,
      topicLevelObj: this.props.topicLevelObj,
      parentContainer: document
        .querySelectorAll('.update-learning-goals')[0]
        .getBoundingClientRect()
    };

    this.styles = {
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer'
      },
      chipLabel: {
        paddingRight: '27px'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      }
    };
  }

  componentWillMount = () => {
    this.updateStates(this.props);
  };

  updateStates = nextProps => {
    this.setState({
      targetPosition:
        nextProps.targetPosition &&
        nextProps.targetPosition.getBoundingClientRect &&
        nextProps.targetPosition.getBoundingClientRect(),
      subTopics: nextProps.subTopics,
      topicLevelObj: nextProps.topicLevelObj
    });
  };

  componentWillReceiveProps = nextProps => {
    this.updateStates(nextProps);
  };

  render() {
    let _this = this;
    let marginTop;
    let topicLevelObj = this.state.topicLevelObj;
    if (this.state.targetPosition) {
      marginTop = -(this.state.targetPosition.top - this.state.targetPosition.height - 8);
    }
    return (
      <div
        className="row"
        style={{
          border: '1px solid #dddddd',
          backgroundColor: '#dddddd',
          display: 'block',
          position: 'relative',
          padding: '1rem'
        }}
      >
        {this.state.targetPosition && (
          <div
            style={{
              position: 'relative',
              marginLeft: Math.round(
                this.state.targetPosition.left -
                  this.state.parentContainer.left +
                  this.state.targetPosition.width / 2 -
                  20
              ),
              display: 'block',
              marginTop: '-35px',
              height: '35px'
            }}
          >
            <PointerArrow color="#dddddd" style={{ width: '20px', height: '12px' }} />
          </div>
        )}
        {this.state.subTopics && (
          <div className="small-12 columns text-center">
            <div style={this.styles.chipsWrapper}>
              {this.state.subTopics.map((topic, i) => {
                let topicID = topic.id;
                return (
                  <TaxonomyNode
                    enableBIA={this.props.enableBIA}
                    index={i}
                    key={topicID}
                    id={topicID}
                    taxoName={'taxoNode_' + topicID}
                    topic={topic}
                    topicLevelObj={topicLevelObj}
                    openBiaModal={_this.props.openBiaModal}
                    closeBIA={_this.props.closeBIA}
                    onSelectTopics={_this.props.onSelectTopics}
                    biaModalVisible={_this.props.biaModalVisible}
                  />
                );
              })}
            </div>
          </div>
        )}
      </div>
    );
  }
}

MultilevelTaxonomyContainer.propTypes = {
  targetPosition: PropTypes.object,
  subTopics: PropTypes.array,
  topicLevelObj: PropTypes.object,
  enableBIA: PropTypes.bool
};
export default connect()(MultilevelTaxonomyContainer);
