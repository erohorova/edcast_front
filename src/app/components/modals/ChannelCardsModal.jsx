import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { connect } from 'react-redux';
import Card from './../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTypeCards, pinUnpinCard } from '../../actions/channelsActionsV2';
import ChannelCardOverlay from './../channel/ChannelCardOverlay';
import { close, openCardRemovalConfirmation, updateDataBefore } from '../../actions/modalActions';
import Spinner from '../common/spinner';
import TextField from 'material-ui/TextField';

class ChannelCardsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      cards:
        (props.modal && props.modal.cards) ||
        (props.modal && props.modal.dataBefore && props.modal.dataBefore.cards) ||
        [],
      loading: false,
      pinnedCount: 0,
      overallCount:
        props.modal.count || (props.modal.dataBefore && props.modal.dataBefore.count) || 0,
      updateModal: false,
      isCustomCarousel: false,
      isReordering: false,
      multilingualFiltering: window.ldclient.variation('multilingual-content', false)
    };
    this.styles = {
      columns: {
        position: 'relative',
        marginBottom: '15px'
      },
      whiteColor: {
        color: 'white'
      },
      iconButton: {
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.isReordering = this.isReordering.bind(this);
    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  componentDidMount = () => {
    if (this.props.channels) {
      let channel = this.props.channels[
        this.props.modal && this.props.modal.channel && this.props.modal.channel.id
      ];
      if (channel) {
        let key = this.fetchData().key;
        let overallCount = channel[this.fetchData().countKey];
        let modalCards = channel[key];
        if (this.fetchData().countKey === 'customCarouselCount') {
          overallCount = this.props.modal.count;
          modalCards = this.props.modal.cards;
          this.setState({
            isCustomCarousel: true
          });
        }
        if (!overallCount) {
          this.props.dispatch(close());
        }

        this.setState({
          cards: modalCards,
          offset: channel[`${key}Offset`],
          pinnedCount: channel.pinnedCount,
          overallCount: overallCount
        });
      }
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.channels) {
      let channel =
        nextProps.channels[
          (nextProps.modal && nextProps.modal.channel && nextProps.modal.channel.id) ||
            (nextProps.modal &&
              nextProps.modal.dataBefore &&
              nextProps.modal.dataBefore.channel &&
              nextProps.modal.dataBefore.channel.id)
        ];
      if (channel) {
        let key = this.fetchData().key;
        let overallCount = channel[this.fetchData().countKey];
        if (this.fetchData().countKey === 'customCarouselCount') {
          overallCount = this.props.modal.count;
          this.setState({
            isCustomCarousel: true
          });
        }
        if (!overallCount) {
          this.props.dispatch(close());
        }
        this.setState({
          cards: channel[key],
          offset: channel[`${key}Offset`],
          pinnedCount: channel.pinnedCount,
          overallCount: overallCount
        });
        if (this.state.updateModal) {
          this.setState({ updateModal: false });
          this.props.dispatch(
            updateDataBefore(
              this.props.modal.modalTitle ||
                (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle),
              overallCount,
              channel[key],
              channel,
              false
            )
          );
        }
      }
    }
  }

  fetchData = () => {
    let titleKeysObj = {};
    switch (
      this.props.modal.modalTitle ||
        (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle)
    ) {
      case 'SmartCards':
        titleKeysObj.key = 'smartbite';
        titleKeysObj.countKey = 'smartbitesCount';
        titleKeysObj.cardType = ['media', 'poll'];
        break;
      case 'Streams':
        titleKeysObj.key = 'video_stream';
        titleKeysObj.countKey = 'videoStreamsCount';
        titleKeysObj.cardType = ['video_stream'];
        break;
      case 'Сourses':
        titleKeysObj.key = 'course';
        titleKeysObj.countKey = 'coursesCount';
        titleKeysObj.cardType = ['course'];
        break;
      case 'Journeys':
        titleKeysObj.key = 'journey';
        titleKeysObj.countKey = 'publishedJourneysCount';
        titleKeysObj.cardType = ['journey'];
        break;
      case 'Pathways':
        titleKeysObj.key = 'pack';
        titleKeysObj.countKey = 'publishedPathwaysCount';
        titleKeysObj.cardType = ['pack'];
        break;
      default:
        titleKeysObj.key = 'custom_carousel';
        titleKeysObj.countKey = 'customCarouselCount';
        titleKeysObj.cardType = ['card'];
    }
    return titleKeysObj;
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true, updateModal: true });
    let type = this.fetchData().cardType;
    let offset = this.state.offset + 12;
    this.props
      .dispatch(
        getTypeCards(
          (this.props.modal.channel && this.props.modal.channel.id) ||
            (this.props.modal.dataBefore && this.props.modal.dataBefore.channel.id),
          type,
          12,
          offset,
          this.state.multilingualFiltering
        )
      )
      .then(() => {
        this.setState({ loading: false, offset: offset });
      })
      .catch(err => {
        console.error(`Error in ChannelCardsModal.getTypeCards.func: ${err}`);
      });
  };

  pinUnpinCard = (card, pinnedStatus) => {
    let payload = {
      pinnable_id:
        this.props.modal.channel.id ||
        (this.props.modal.dataBefore && this.props.modal.dataBefore.channel.id),
      pinnable_type: 'Channel',
      object_id: card.id,
      object_type: 'Card'
    };
    let cardTypes = this.fetchData().key;
    this.props.dispatch(pinUnpinCard(payload, pinnedStatus, cardTypes, card));
  };

  removeCardLaunchModal = card => {
    let cardTypes = this.fetchData().key;
    this.props.dispatch(openCardRemovalConfirmation(card, this.props.modal.channel, cardTypes));
  };

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({ cards: newCardsList });
  };

  isReordering = value => {
    this.setState({
      isReordering: value
    });
  };

  setOffsetReorder = () => {
    this.setState({
      offset: 0
    });
  };

  render() {
    let editMode =
      this.props.modal.editMode ||
      (this.props.modal.dataBefore && this.props.modal.dataBefore.editMode);
    let channel = this.props.channels[
      this.props.modal && this.props.modal.channel && this.props.modal.channel.id
    ];
    return (
      <div className="channel-cards-modal">
        <div className="row">
          <TextField name="channelcardmodal" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={this.styles.columns}>
            <div style={this.styles.whiteColor}>
              {tr(
                this.props.modal.modalTitle ||
                  (this.props.modal.dataBefore && this.props.modal.dataBefore.modalTitle)
              )}{' '}
              ({this.state.overallCount})
            </div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.iconButton}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <div className="custom-card-container">
              <div className="four-card-column vertical-spacing-medium row ">
                {this.state.cards &&
                  this.state.cards.length > 0 &&
                  !this.state.isReordering &&
                  this.state.cards.map(card => {
                    return (
                      <div
                        className={`channel-card-v2 card-v2 custom-medium-6 column ${
                          this.isNewTileCard ? 'large-4' : 'large-3 medium-4'
                        } small-12 ${(this.props.modal.editMode ||
                          (this.props.modal.dataBefore && this.props.modal.dataBefore.editMode)) &&
                          'show-overlay'}`}
                      >
                        <Card
                          key={card.id}
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={true}
                          moreCards={false}
                          hideModal={editMode}
                          channelSetting={true}
                          removeCardFromList={this.removeCardFromList}
                        />
                        <ChannelCardOverlay
                          card={card}
                          pinUnpinCard={this.pinUnpinCard}
                          pinnedCount={this.state.pinnedCount}
                          removeCardLaunchModal={this.removeCardLaunchModal}
                          channel={channel}
                          allCardsCount={this.state.overallCount}
                          isCustomCarousel={this.state.isCustomCarousel}
                          reorderToggle={this.isReordering}
                          setOffset={this.setOffsetReorder}
                        />
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns text-center">
            {this.state.cards &&
              (this.state.overallCount > this.state.cards.length &&
                this.state.cards.length >= 12 &&
                this.state.overallCount !== 12) &&
              !this.state.isReordering && (
                <button className="view-more-v2" onClick={this.viewMoreClickHandler}>
                  {this.state.loading ? tr('Loading...') : tr('View More')}
                </button>
              )}
            {this.state.isReordering && <Spinner />}
          </div>
        </div>
      </div>
    );
  }
}

ChannelCardsModal.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  channels: PropTypes.object
};

export default connect(state => ({
  modal: state.modal.toJS(),
  currentUser: state.currentUser.toJS(),
  channels: state.channelsV2.toJS()
}))(ChannelCardsModal);
