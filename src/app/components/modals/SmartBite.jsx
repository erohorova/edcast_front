import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactTags from 'react-tag-autocomplete';
import isUrlValid from 'validator/lib/isURL';
import { tr } from 'edc-web-sdk/helpers/translations';
// TODO: async import of RichTextEditor (100kb gzipped)
import RichTextEditor from 'react-rte';
import marked from 'marked';
import _ from 'lodash';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import AudioTrack from 'material-ui/svg-icons/image/audiotrack';
import AttachIcon from 'material-ui/svg-icons/editor/attach-file';
import {
  postv2,
  updateResource,
  createResource,
  checkDuplicateCard
} from 'edc-web-sdk/requests/cards';
import { getRestrictToUserOrGroup, showPreviousCardVersions } from 'edc-web-sdk/requests/cards.v2';
import { open as openSnackBar } from '../../actions/snackBarActions';
import Spinner from '../common/spinner';
import Chip from 'material-ui/Chip';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';

import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import colors from 'edc-web-sdk/components/colors/index';
import { getItems } from 'edc-web-sdk/requests/users.v2';
import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import TeamActiveIcon from 'edc-web-sdk/components/icons/TeamActiveIcon';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import Tag from 'edc-web-sdk/components/icons/Tag';
import {
  videoTranscode,
  uploadPolicyAndSignature,
  refreshFilestackUrl
} from 'edc-web-sdk/requests/filestack';
import { getListV2 } from 'edc-web-sdk/requests/groups.v2';

import TextFieldCustom from '../common/SmartBiteInput';
import SmartBiteDropdown from '../common/SmartBiteDropdown';
import CardSearch from '../common/CardSearch';
import CardSharingSection from '../common/CardSharingSection';
import convertRichText from '../../utils/convertRichText';
import MultilingualContent from '../common/MultilingualContent';

import getTranscodedVideo from '../../utils/getTranscodedVideo';
import addTabInRadioButtons from '../../utils/addTabInRadioButtons';
import getDefaultImage from '../../utils/getDefaultCardImage';
import calculateSeconds from '../../utils/calculateSecondsForDuration';
import calculateFromSeconds from '../../utils/calculateFromSecondsForDuration';
import addParamStart from '../../utils/addParamStartForYoutubeLink';
import { filestackClient, getResizedUrl } from '../../utils/filestack';
import { Permissions } from '../../utils/checkPermissions';
import { fileStackSources } from '../../constants/fileStackSource';
import SmartCardVersions from './SmartCardVersions';
import ActionInfo from 'react-material-icons/icons/action/info';
import { parseUrl } from '../../utils/urlParser';
import PdfViewer from '../common/PdfViewer';
import { isInternetExplorer } from '../../utils/isInternetExplorer';
import { isEdgeBrowser } from '../../utils/IsEdgeBrowser';

const maxTextLength = 50000;
const toolbarConfig = {
  display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS'],
  INLINE_STYLE_BUTTONS: [
    { label: 'Bold', style: 'BOLD', className: 'custom-text-editor-btn' },
    { label: 'Italic', style: 'ITALIC', className: 'custom-text-editor-btn' },
    { label: 'Strikethrough', style: 'STRIKETHROUGH', className: 'custom-text-editor-btn' },
    { label: 'Underline', style: 'UNDERLINE', className: 'custom-text-editor-btn' }
  ],
  BLOCK_TYPE_BUTTONS: [
    { label: 'UL', style: 'unordered-list-item', className: 'custom-text-editor-btn' },
    { label: 'OL', style: 'ordered-list-item', className: 'custom-text-editor-btn' }
  ]
};
const videoType = ['video/mp4', 'video/ogg', 'video/webm', 'video/quicktime', 'video/ogv'];
const docType = ['application/*', '.docx', '.doc'];
const pptType = ['application/*', '.pptx', '.ppt'];
const fullAccept = [...['image/*', '.pdf'], ...videoType, ...docType, ...pptType];

const lightPurp = '#acadc1';
const middlePurp = '#d6d6e1';
const renderer = new marked.Renderer();

renderer.link = (href, title, text) => {
  let out = '<a target="_blank" name="marked-link" href="' + href + '"';
  if (title) {
    out += ' title="' + title + '"';
  }
  out += '>' + text + '</a>';
  return out;
};

class Smartbite extends Component {
  constructor(props, context) {
    super(props, context);
    let durationString;
    let advSettings = {};
    let fileStackProvider = false;
    let securedFileStackProviderUrl;
    let message = '';
    let projectName = '';
    let linkUrl = '';
    let selectedChannels = [];
    let nonCuratedChannelIds = [];
    let linkCard = {};
    let topics = [];
    let removedOptions = [];
    let fileStack = [];
    let pollOptions = [{ label: '' }, { label: '' }];
    let quizOptions = [{ label: '', isCorrect: false }, { label: '', isCorrect: false }];
    let articleImage = false;
    let showPreviewLink = false;
    let cardPreview = false;
    let showPollLink = false;
    let createLabel = 'Create';
    let searchText = '';
    let pathwayLabel = 'Done';
    let titleLabel = 'Create';
    let activeType = props.activeType;
    let textCard = RichTextEditor.createEmptyValue();
    let resourceId = null;
    let closeClick = false;
    let disableSubmit = false;
    let currentReadableCardType = props.card
      ? _.toLower(_.replace(props.card.readableCardType, /\s/g, '_'))
      : '';
    let restrictedToUsers = [];
    let userOrTeamSuggestion = [];
    let shareOnSmartCardCreation = window.ldclient.variation('share-on-smart-card-creation', false);
    let showPrivateChannelMsg = false;
    let privateChannelMsg = this.props.card
      ? ''
      : 'Card will remain public until explicitly marked as private.';
    let showPostToChannelMsg = false;
    let isAbleToMarkPrivate =
      Permissions['enabled'] !== undefined && Permissions.has('MARK_AS_PRIVATE');
    let userActionedPrivateContent = false;

    if (props.activeType === 'bookmark' || props.activeType === 'smart') {
      disableSubmit = true;
    }

    if (props.searchText) {
      searchText = props.searchText;
    }

    if (props.closeClick) {
      closeClick = props.closeClick;
    }

    if (props.tempCard) {
      message = props.tempCard.message;
      let resource = props.tempCard.resource;
      if (resource) {
        linkCard = resource;
        resourceId = resource.id;
      }
      if (props.tempCard.filestack) {
        fileStack = props.tempCard.filestack;
      }
      switch (props.activeType) {
        case 'link':
          if (resource) {
            linkUrl = resource.url;
            message = resource.url !== props.tempCard.message ? props.tempCard.message : '';
          }
          break;
        case 'upload':
          cardPreview = true;
          break;
        case 'text':
          textCard = this.textTransform(props.tempCard.message);
          cardPreview = props.tempCard.filestack;
          break;
        case 'poll':
          if (props.tempCard.filestack) {
            cardPreview = true;
          }
          let options =
            props.tempCard.options !== undefined && props.tempCard.options.length
              ? props.tempCard.options
              : pollOptions;
          pollOptions = options.map(option => {
            return { id: option.id, label: option.label };
          });
          if (pollOptions.length < 2) {
            pollOptions.push({ label: '' });
          }
          break;
        case 'quiz':
          if (props.tempCard.filestack) {
            cardPreview = true;
          }
          let optionsQuiz =
            props.tempCard.options !== undefined && props.tempCard.options.length
              ? props.tempCard.options
              : quizOptions;
          quizOptions = optionsQuiz.map(option => {
            return { id: option.id, label: option.label, isCorrect: option.is_correct };
          });
          if (quizOptions.length < 2) {
            quizOptions.push({ label: '' });
          }
          break;
        default:
          // FIXME: implement default case
          break;
      }
      if (props.tempCard.channel_ids) {
        selectedChannels = props.tempCard.channel_ids;
      }
      topics = props.tempCard.topics || [];
    }

    if (props.card) {
      message = props.card.message;
      createLabel = 'Update';
      titleLabel = 'Update';
      let resource = props.card.resource;

      if (resource) {
        linkUrl = resource.url;
      }

      if (
        props.card.cardType === 'media' &&
        (props.card.cardSubtype === 'link' ||
          (props.card.cardSubtype === 'video' && !props.card.filestack.length))
      ) {
        activeType = 'link';
        if (resource) {
          linkUrl = resource.url;
          message = resource.url != props.card.message ? props.card.message : '';
        }
      }
      if (props.card.cardType === 'course') {
        if (resource) {
          linkUrl = resource.url;
        }
      }
      if (resource) {
        linkCard = resource;
        resourceId = resource.id;
      }
      if (props.card.filestack) {
        fileStack = props.card.filestack;
        if (props.card.filestack[0] && props.card.filestack[0].handle === props.card.message) {
          message = '';
        }
      }
      if (
        (props.card.cardType === 'media' &&
          (props.card.cardSubtype === 'text' ||
            props.card.cardSubtype === 'image' ||
            props.card.cardSubtype === 'file' ||
            props.card.cardSubtype === null)) ||
        (props.card.cardSubtype === 'video' &&
          !!props.card.filestack.length &&
          props.card.filestack[0].mimetype &&
          ~props.card.filestack[0].mimetype.indexOf('video/')) ||
        (props.card.cardSubtype === 'audio' &&
          !!props.card.filestack.length &&
          props.card.filestack[0].mimetype &&
          ~props.card.filestack[0].mimetype.indexOf('audio/'))
      ) {
        if (props.card.filestack.length > 1 || props.card.cardSubtype === 'image') {
          activeType = 'upload';
          cardPreview = true;
        } else {
          if (
            !props.card.filestack.length ||
            (props.card.filestack[0].mimetype &&
              ~props.card.filestack[0].mimetype.indexOf('image/'))
          ) {
            activeType = 'text';
            textCard = this.textTransform(props.card.message);
            cardPreview = props.card.filestack.length;
          } else {
            activeType = 'upload';
            cardPreview = true;
          }
        }
      }
      if (props.card.cardType === 'poll' && !props.card.isAssessment) {
        activeType = 'poll';
        if (props.card.filestack[0]) {
          cardPreview = true;
        }
        let options =
          props.card.quizQuestionOptions !== undefined
            ? props.card.quizQuestionOptions
            : props.card.options;
        pollOptions = options.map(option => {
          return { id: option.id, label: option.label };
        });
      }

      if (props.card.cardType === 'poll' && props.card.isAssessment) {
        activeType = 'quiz';
        if (props.card.filestack[0]) {
          cardPreview = true;
        }
        let options =
          props.card.quizQuestionOptions !== undefined
            ? props.card.quizQuestionOptions
            : props.card.options;
        quizOptions = options.map(option => {
          return { id: option.id, label: option.label, isCorrect: option.isCorrect };
        });
      }
      if (props.card.cardType === 'project') {
        activeType = 'project';
        textCard = this.textTransform(message);
        projectName = props.card.title;
        cardPreview = true;
      }
      if (props.card.channelIds && props.card.channelIds.length) {
        selectedChannels = props.card.channelIds;
      }
      if (!!props.card.tags && props.card.tags.length > 0) {
        topics = props.card.tags.map(tag => tag.name);
      }
      if (props.card.provider || props.card.provider === '') {
        advSettings.provider = props.card.provider;
        fileStackProvider = props.card.providerImage;
      }
      if (props.card.eclDurationMetadata) {
        durationString = calculateFromSeconds(
          props.card.eclDurationMetadata.calculated_duration,
          true
        );
        advSettings.duration = props.card.eclDurationMetadata.calculated_duration;
      }
      if (props.card.nonCuratedChannelIds && props.card.nonCuratedChannelIds.length) {
        nonCuratedChannelIds = props.card.nonCuratedChannelIds;
      }
    }
    let config = this.props.team.config;
    this.isCreatePrivateContentByDefaultEnable =
      shareOnSmartCardCreation &&
      isAbleToMarkPrivate &&
      config &&
      config.create_private_card_by_default;
    this.showBIA = !!config.enabled_bia;
    this.limitDuplicate = 10;
    this.state = {
      message,
      linkUrl,
      pollOptions,
      quizOptions,
      selectedChannels,
      nonCuratedChannelIds,
      linkCard,
      removedOptions,
      articleImage,
      showPreviewLink,
      cardPreview,
      showPollLink,
      titleLabel,
      createLabel,
      pathwayLabel,
      activeType,
      textCard,
      currentUser: props.currentUser,
      pendingImageUpload: false,
      clicked: false,
      closeClick,
      linkErrorText: '',
      longDescriptionErrorText: '',
      currencyErrorText: '',
      priceErrorText: '',
      uploadErrorText: '',
      NameErrorText: '',
      textErrorText: '',
      searchText,
      searchCards: [],
      pollErrorText: '',
      searchError: '',
      disableSubmit,
      limit: 10,
      offset: 0,
      topics,
      resourceId,
      fileStack,
      maxTagErrorText: '',
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      selectedBia: props.card && props.card.skillLevel,
      currentReadableCardType,
      customDefaultImages: window.ldclient.variation('custom-default-images', false),
      showAdSettings: false,
      advSettings,
      fileStackProvider,
      securedFileStackProviderUrl,
      durationString,
      groups: [],
      selectedGroupIds: [],
      dataSource: [],
      errorText: '',
      selectedMemberIds: [],
      members: [],
      isPrivateContent: !props.isPathwayPart
        ? this.isCreatePrivateContentByDefaultEnable
          ? !!props.card
            ? !props.card.isPublic
            : true
          : !!props.card
          ? !props.card.isPublic
          : false
        : false,
      groupsSuggestions: [],
      channelsSuggestions: [],
      userSuggestions: [],
      groupsForRestrict: [],
      usersForRestrict: [],
      tags: [],
      topicTags: [],
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      prices: (props.card && props.card.prices) || [],
      isPaid: props.card && props.card.isPaid,
      plan:
        props.card && props.card.cardMetadatum && props.card.cardMetadatum.plan
          ? props.card.cardMetadatum.plan
          : '',
      planId:
        props.card && props.card.cardMetadatum && props.card.cardMetadatum.id
          ? props.card.cardMetadatum.id
          : '',
      planChange: false,
      currency: '',
      price: '',
      shareOnSmartCardCreation: shareOnSmartCardCreation,
      projectName,
      flagTocheckTheCount: true,
      isFirstTextCardOfPathway: '',
      removeProviderImage: false,
      restrictedToUsers,
      userOrTeamSuggestion,
      canBeReanswered:
        props.card && props.card.canBeReanswered ? this.props.card.canBeReanswered : false,
      titleError: false,
      setCardTypeMandate: window.ldclient.variation('mandate-card-readable-type', false),
      showVersions: window.ldclient.variation('card-versions', false),
      previousVersions: [],
      showPreviousversionsToggle: false,
      disableForm: false,
      cardTypeNotSetError: false,
      linkTypeDescription: false,
      description: '',
      isAbleToMarkPrivate: isAbleToMarkPrivate,
      duplicateCards: [],
      duplicateTotal: 0,
      language: props.card && props.card.language ? props.card.language : 'unspecified'
    };
    //Set plan for existing Cards
    if (
      this.state.edcastPricing &&
      this.state.edcastPlansForPricing &&
      props.card &&
      !this.state.plan
    ) {
      this.state.plan = props.card.isPaid ? 'paid' : 'free';
    }

    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      image: {
        backgroundColor: '#acadc1',
        color: 'white',
        boxSizing: 'content-box',
        textTransform: 'none',
        borderRadius: '2px',
        margin: '4px',
        lineHeight: '12px',
        height: '28px',
        width: '50px',
        marginLeft: '15px'
      },
      btnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      cancel: {
        color: lightPurp,
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      chipsWrapper:
        this.props.cardType === 'pathway'
          ? {
              display: 'block',
              margin: '0 0 -0.25rem'
            }
          : {
              display: 'flex',
              flexWrap: 'wrap',
              margin: '0',
              maxWidth: '100%'
            },
      chip: {
        margin: this.props.cardType === 'pathway' ? '.25rem' : 0,
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '0 37px 0 8px',
        width: this.props.cardType === 'pathway' ? 'auto' : '100%'
      },
      chipClose: {
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        borderRadius: '50%',
        color: colors.primary,
        verticalAlign: 'middle',
        marginLeft: '1rem',
        marginBottom: '2px',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        position: 'absolute',
        right: '8px',
        top: '5px'
      },
      imageUpload: {
        borderColor: middlePurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '1rem',
        width: '100%',
        height: 'auto',
        color: '#999aad',
        backgroundColor: 'white',
        hoverColor: 'white',
        borderRadius: '3px'
      },
      attachUpload: {
        borderColor: middlePurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '10px 0 0',
        lineHeight: '14px',
        width: '100%',
        height: 'auto',
        color: '#999aad',
        backgroundColor: 'white',
        hoverColor: 'white',
        borderRadius: '3px'
      },
      attachUploadFile: {
        borderColor: middlePurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        lineHeight: '14px',
        width: '100%',
        height: 'auto',
        color: '#999aad',
        backgroundColor: 'white',
        hoverColor: 'white',
        borderRadius: '3px'
      },
      imageIcon: {
        width: '36px',
        height: '36px'
      },
      attachIcon: {
        width: '30px',
        height: '30px',
        transform: 'rotate(90deg)'
      },
      attachLabel: {
        fontSize: '14px',
        textTransform: 'none'
      },
      checkbox: {
        color: colors.primary,
        width: '24px',
        margin: '0 8px 0 16px'
      },
      quizUnderline: {
        color: lightPurp
      },
      quizInput: {
        color: lightPurp,
        width: '256px'
      },
      quizHint: {
        color: middlePurp,
        fontSize: '14px'
      },
      closeOption: {
        padding: 0,
        border: 0,
        width: 'auto',
        height: 'auto'
      },
      biaRadio: {
        marginRight: '5px',
        fill: '#d6d6e1'
      },
      dropDown: {
        borderColor: '#d6d6e2',
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '5px',
        fontSize: '12px',
        lineHeight: '14px',
        height: '31px',
        width: '100%',
        maxWidth: '256px',
        verticalAlign: 'top',
        borderRadius: '2px',
        marginTop: '8px'
      },
      hint: {
        padding: '0 10px',
        fontSize: '12px',
        color: '#acadc1',
        bottom: '5px'
      },
      iconStyle: {
        top: '2px',
        padding: 0,
        width: '24px',
        height: '24px'
      },
      selected: {
        lineHeight: '36px',
        top: 0,
        padding: '0 0.5rem',
        color: '#999aad'
      },
      providerIcon: {
        marginLeft: '15px',
        display: 'block',
        height: '50px'
      },
      imagePreview: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%'
      },
      mainStyle: {
        borderColor: '#d6d6e1',
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: 0,
        fontSize: '14px',
        lineHeight: '14px',
        height: '31px',
        width: '100%',
        maxWidth: '256px',
        verticalAlign: 'top',
        borderRadius: '2px'
      },
      tagIconStyles: {
        height: '14px',
        width: '34px'
      },
      privateContent: {
        fontSize: '0.75rem',
        marginTop: '0.5rem'
      },
      providerImageContainer: {
        position: 'relative',
        display: 'inline-block'
      },
      deleteProviderImageBtn: {
        width: '1.25rem',
        height: '1.25rem',
        padding: '0',
        background: '#000000',
        borderRadius: '50%'
      },
      deleteProviderImageBtnIcon: {
        width: '1.25rem',
        height: '1.25rem'
      },
      allowCheckbox: {
        color: colors.primary,
        width: '90%',
        marginLeft: '1rem'
      },
      allowCheckboxLabel: {
        marginLeft: '-0.5rem'
      },
      priceDropDown: {
        textAlign: 'left',
        height: '30px',
        verticalAlign: 'top',
        border: '1px solid #d6d6e2',
        fontSize: '12px',
        paddingLeft: '5px',
        width: '100%',
        maxWidth: '256px',
        marginTop: '8px',
        lineHeight: '5px'
      },
      iconSelect: {
        height: '30px',
        width: '30px',
        padding: 0
      },
      menuItemStyle: {
        fontSize: '12px'
      },
      labelStyleSelect: {
        fontSize: '12px',
        lineHeight: '38px',
        paddingRight: '18px'
      },
      restrictToErrorStyle: {
        color: '#999aad',
        fontSize: '11px',
        bottom: '5px',
        paddingLeft: '10px'
      },
      privateChannelStyle: {
        color: '#999aad',
        fontSize: '12px',
        paddingTop: '8px',
        paddingBottom: '8px'
      },
      privateChannelIconStyle: {
        top: '2px',
        width: '14px',
        height: '14px',
        verticalAlign: 'middle',
        display: 'inline-block'
      },
      disableForm: {
        opacity: '0.5',
        pointerEvents: 'none'
      },
      cancelButtonAnchorTag: {
        marginLeft: '5px'
      }
    };
    this.restrictToFlag = window.ldclient.variation('restrict-to', false);
    this.isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    let conf_detectDuplicateUrl = config && config.duplicate_card_creation_disabled;
    let ld_detectDuplicateUrl = window.ldclient.variation('detect-duplicate-url', false);
    this.typeOfDetectDuplicate =
      ld_detectDuplicateUrl && conf_detectDuplicateUrl
        ? 'auto'
        : ld_detectDuplicateUrl
        ? 'manual'
        : 'none';
    this.isAbleUpload = Permissions['enabled'] !== undefined && Permissions.has('UPLOAD');
    this.isAbleUploadCover =
      Permissions['enabled'] !== undefined && Permissions.has('UPLOAD_CONTENT_COVER_IMAGES');

    /* hardcoded projectCardIntroText for now */
    this.projectCardIntroText = this.textTransform(
      '<body><h2>Introduction</h2> <div>The demand for coding skills is skyrocketing, and not just for developers — programming is playing a bigger role in every career path. Add the right technical skills to your résumé so you can pursue a more fulfilling career.</div></body>'
    );

    this.updateLinkFile = this.updateLinkFile.bind(this);
    this.addFileStackImage = this.addFileStackImage.bind(this);
    this.addFileStackFiles = this.addFileStackFiles.bind(this);
    this.updateScormCardImage = this.updateScormCardImage.bind(this);
    this.changeBia = this.changeBia.bind(this);
    this.callCreatedCard = this.callCreatedCard.bind(this);
  }
  componentWillUpdate(nextProp) {
    let smartbites = nextProp.smartBites;
    let isFirstTextCard = {};
    if (this.state.flagTocheckTheCount) {
      this.setState({ smartBitesList: smartbites, flagTocheckTheCount: false });
      if (smartbites) {
        isFirstTextCard = smartbites.find(ele => {
          return ele.cardSubtype == 'text';
        });
        if (isFirstTextCard) this.setState({ isFirstTextCardOfPathway: 'text' });
      }
    }
  }
  componentWillReceiveProps(nextProp) {
    if (nextProp.activeType != this.state.activeType) {
      this.setState({
        activeType: nextProp.activeType,
        message: nextProp.activeType === 'text' ? this.state.textCard.toString('markdown') : '',
        linkUrl: '',
        linkErrorText: '',
        longDescriptionErrorText: '',
        currencyErrorText: '',
        priceErrorText: '',
        uploadErrorText: '',
        searchText: '',
        searchCards: [],
        removedOptions: [],
        pollErrorText: '',
        textErrorText: '',
        searchError: '',
        linkCard: {},
        pollOptions: [{ label: '' }, { label: '' }],
        quizOptions: [{ label: '', isCorrect: false }, { label: '', isCorrect: false }],
        fileStack: [],
        showPreviewLink: false,
        showPollLink: false,
        cardPreview: false,
        disableSubmit: false
      });
    }
    if (
      nextProp.activeType === 'upload' &&
      nextProp.uploadIteration != this.props.uploadIteration
    ) {
      this.setState({ disableSubmit: true });
      this.fileStackHandler(fullAccept, this.addFileStackFiles);
    }
    if (nextProp.activeType == 'bookmark' || nextProp.activeType == 'smart') {
      this.setState({ disableSubmit: true });
    }
  }

  componentDidMount() {
    if (this.props.cardType === 'pathway') {
      this.scrollToContentBox();
    }
    if (
      this.props.activeType === 'upload' &&
      this.props.cardType === 'pathway' &&
      !this.state.fileStack.length
    ) {
      this.fileStackHandler(fullAccept, this.addFileStackFiles);
    }
    if (!!this.props.titleblank && this.props.activeType === 'upload') {
      this.setState({ titleError: true, disableSubmit: true });
    } else {
      this.setState({ titleError: false, disableSubmit: false });
    }

    if (this.props.activeType === 'upload' && this.props.cardType === 'pathway') {
      this.setState({ titleError: false, disableSubmit: true });
    }

    if (this.state.searchText.length) {
      this.getSearchCards(this.state.limit, 0);
    }
    let payloadGroup = {
      limit: 1000,
      offset: 0
    };

    if (!(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) {
      payloadGroup['writables'] = true;
    }

    getListV2(payloadGroup)
      .then(groups => {
        let suggestionState = this.state.groupsSuggestions;
        let suggestionStateForRestrict = this.state.groupsForRestrict;
        groups.teams.map(group => {
          let suggestion = {
            name: group.name,
            id: group.id,
            label: 'group'
          };
          suggestionState.push(suggestion);
          suggestionStateForRestrict.push(suggestion);
        });
        this.setState(
          { groupsSuggestions: suggestionState, groupsForRestrict: suggestionStateForRestrict },
          () => {
            let teamsPermitted =
              this.props.card && this.props.card.teamsPermitted
                ? this.props.card.teamsPermitted
                : [];
            teamsPermitted.map(team => {
              let teamsObj = team;
              teamsObj.label = 'group';
              this.handleUserAddition(teamsObj);
            });
          }
        );
      })
      .catch(err => {
        console.error(`Error in SmartBite.componentDidMount.getList.func : ${err}`);
      });

    /*----- Finding the channel suggestions-------*/
    let channelsSuggestions = this.props.currentUser.writableChannels;
    if (channelsSuggestions) {
      channelsSuggestions.map(channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };
        let suggestionState = this.state.channelsSuggestions;
        suggestionState.push(suggestion);
        this.setState({ channelsSuggestions: suggestionState });
      });
    }

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.card && !!this.props.card.tags) {
      let existingTags = this.props.card.tags;
      this.setState({ topicTags: existingTags });
    }

    /*---------- Pushing users to tags--------- */
    if (!!this.props.card && !!this.props.card.usersWithAccess) {
      let existingMembers = this.props.card.usersWithAccess;
      _.map(existingMembers, member => {
        let suggestion = {
          name: member.fullName,
          id: member.id,
          label: 'member'
        };
        this.handleAddition(suggestion);
      });
    }

    /*----------Pushing groups to tags----------- */
    if (!!this.props.card && !!this.props.card.teams) {
      let existingGroups = this.props.card.teams;
      _.map(existingGroups, group => {
        let suggestion = {
          name: group.label,
          id: group.id,
          label: 'group'
        };
        this.handleAddition(suggestion);
      });
    }

    /*----- Pushing channels to tags -----------*/
    if (!!this.props.card && !!this.props.card.channels) {
      let existingChannels = this.props.card.channels;
      _.map(existingChannels, channel => {
        let suggestion = {
          name: channel.label,
          id: channel.id,
          label: 'channel',
          isPrivate: channel.isPrivate
        };
        this.handleAddition(suggestion, false);
      });
    }
    if (
      !(!!this.props.card && !!this.props.card.channels) &&
      /channel\//i.test(this.props.pathname)
    ) {
      let channel = _.values(this.props.channelsV2)[0];
      let suggestion = {
        name: channel.label,
        id: channel.id,
        label: 'channel',
        isPrivate: channel.isPrivate
      };
      this.handleAddition(suggestion, false);
    }

    /* Converting exisiting card data to the required tag format */
    if (!!this.props.card) {
      let usersPermitted = this.props.card.usersPermitted ? this.props.card.usersPermitted : [];
      usersPermitted = usersPermitted.map(user => {
        let userObj = user;
        userObj.label = 'member';
        this.handleUserAddition(userObj);
        return userObj;
      });
    }

    /*-----Fetching card previous versions-------*/
    if (this.props.card && this.props.card.id) {
      let payload = {
        entity_type: 'card',
        entity_id: this.props.card.id
      };
      showPreviousCardVersions(payload)
        .then(data => {
          this.setState({
            previousVersions: data.versions
          });
        })
        .catch(err => {
          console.error('Error in fetching previous versions of cards');
        });
    }

    setTimeout(() => {
      addTabInRadioButtons('bia-radio', this.changeBia);
    }, 500);
  }

  componentWillMount() {
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  componentDidUpdate(nextProps, nextState) {
    if (
      (this.props.cardType === 'pathway' || nextProps.cardType === 'pathway') &&
      this.props.activeType !== nextProps.activeType
    ) {
      if (isInternetExplorer() || isEdgeBrowser()) {
        _.delay(() => {
          this.scrollToContentBox();
        }, 150);
      } else {
        this.scrollToContentBox();
      }
    }
  }
  componentWillUnmount() {
    if (!this.state.closeClick) {
      this.createClickHandler(null, true);
    }
  }

  textTransform = message => {
    let markdown = convertRichText(message);
    return RichTextEditor.createValueFromString(markdown, 'markdown');
  };

  addOptionClickHandler = (e, type) => {
    e.preventDefault();
    if (
      (type == 'pollOptions' && this.state[type].length < 5) ||
      (type == 'quizOptions' && this.state[type].length < 30)
    ) {
      this.setState({ [type]: this.state[type].concat([{ label: '' }]) });
    }
  };

  recheckError = type => {
    let emptyOptions = this.state[type].filter(item => {
      return !item.label.trim().length;
    });
    let filteredOptions = this.state[type].filter(item => {
      return item.label.trim().length;
    });
    if (emptyOptions.length === 2 && filteredOptions.length === 2) {
      let errorMsg = "Choice can't be empty";
      this.setState({ optionsError: errorMsg, disableSubmit: true });
    } else {
      this.setState({ optionsError: '', disableSubmit: false });
    }
  };

  removeOptionClickHandler = (index, type) => {
    let options = this.state[type];
    if (options[index].id) {
      let removedOptions = this.state.removedOptions;
      removedOptions.push({ id: options[index].id, label: '', _destroy: true });
      this.setState({ removedOptions });
    }
    if (options.length > 2) {
      options.splice(index, 1);
      this.setState({ [type]: options });
      this.recheckError(type);
    } else {
      options[index].label = '';
      options[index].id = null;
      this.setState({ [type]: options });
    }
  };

  optionChangeHandler = (index, type, _null, value) => {
    let options = this.state[type];
    if (options[index]) {
      options[index].label = value;
      this.setState({ [type]: options, optionsError: '', disableSubmit: false });
    }
  };

  fileStackHandler = (accept, callback) => {
    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        let isVideo = this.props.card && this.props.card.cardSubtype === 'video';
        let isAudio = this.props.card && this.props.card.cardSubtype === 'audio';
        let acceptParam = accept;
        let isImageType = acceptParam.length === 1 && acceptParam[0] === 'image/*';
        let fromSourceWithImage = isImageType || (!isAudio && !isVideo);
        let fromSources = fileStackSources;
        if (!fromSourceWithImage) {
          fromSources.splice(1, 1);
        }
        if (
          location.protocol === 'https:' &&
          fromSourceWithImage &&
          !~fromSources.indexOf('webcam')
        ) {
          fromSources.push('webcam');
        }
        const audioType = ['.mp3', '.oga', '.m4a', '.aac', '.hls.variant.audio'];
        acceptParam = isImageType
          ? ['image/*']
          : isVideo
          ? videoType
          : isAudio
          ? audioType
          : [...acceptParam, ...audioType];
        filestackClient(policy, signature)
          .pick({
            accept: acceptParam,
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in SmartBite.fileStackHandler.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in SmartBite.fileStackHandler.uploadPolicyAndSignature.func : ${err}`);
      });
  };
  addName = event => {
    this.setState({ projectName: event.target.value, NameErrorText: '', disableSubmit: false });
  };

  messageCardChange = event => {
    this.setState({ message: event.target.value, uploadErrorText: '', disableSubmit: false });
  };

  descriptionChangeHandler = event => {
    let descriptionValue = event.target.value;
    let setStates = {};
    if (descriptionValue.length > 1000) {
      setStates = {
        description: descriptionValue,
        uploadErrorText: '',
        disableSubmit: true,
        longDescriptionErrorText: 'Maximum 1000 characters allowed in description'
      };
    } else {
      setStates = {
        description: event.target.value,
        uploadErrorText: '',
        disableSubmit: false,
        longDescriptionErrorText: ''
      };
    }
    this.setState(setStates);
  };

  uploadCardMessageChange = event => {
    if (event.target.value != '') {
      this.setState({
        titleError: false,
        message: event.target.value,
        uploadErrorText: '',
        disableSubmit: false
      });
    } else {
      this.setState({
        titleError: true,
        message: event.target.value,
        uploadErrorText: '',
        disableSubmit: true
      });
    }
  };
  checkCard = searchCards => {
    this.setState({
      disableSubmit: !searchCards.some(card => card.checked),
      searchCards,
      searchError: ''
    });
  };

  changeTag = event => {
    this.setState({
      maxTagErrorText:
        event.target.value.length <= 150
          ? ''
          : 'Max tag length exceeded. It must be shorter than 150 characters.'
    });
  };

  addTagHandler = event => {
    if (
      this.state.topics.indexOf(event.target.value.trim()) === -1 &&
      event.target.value.trim() !== ''
    ) {
      this.setState({ topics: this.state.topics.concat(event.target.value.trim()) });
    }
  };

  removeTag = tag => {
    let newTagArray = this.state.topics.filter(topic => topic !== tag);
    this.setState({ topics: newTagArray });
  };

  getNewChannels = (channelIds, channel, pathname) => {
    let isAddingToCurrentChannel = false;
    let currentChannel;
    if (
      channel &&
      channel.slug &&
      pathname &&
      (channel.slug === pathname[2] || `${channel.id}` === pathname[2])
    ) {
      isAddingToCurrentChannel = true;
      currentChannel = { id: channel.id, slug: channel.slug };
    }

    this.setState(
      {
        showPostToChannelMsg: false,
        selectedChannels: channelIds,
        isAddingToCurrentChannel,
        currentChannel
      },
      () =>
        this.state.selectedChannels.map(id => {
          if (
            this.props.currentUser.writableChannels &&
            this.props.currentUser.writableChannels.find(e => e.id == id && e.isPrivate)
          ) {
            this.setState({ showPostToChannelMsg: true });
          }
        })
    );
  };

  handleCardTypeChange = data => {
    this.setState({ currentReadableCardType: data, cardTypeNotSetError: false });
    if (data !== 'introduction') {
      return;
    }
    let textCard = this.projectCardIntroText;
    this.setState({ textCard });
  };

  handleLanguageChange = data => {
    this.setState({ language: data });
  };

  advSettingChange = (type, event) => {
    let advSettings = this.state.advSettings;
    if (type === 'duration') {
      advSettings[type] = calculateSeconds(event.target.value);
      this.setState({
        durationString: event.target.value
      });
    } else {
      advSettings[type] = event.target.value;
    }
    this.setState({ advSettings });
  };

  onChangeTextEditor = textCard => {
    let editorState = textCard.getEditorState();
    let selection = editorState.getSelection();
    let currentContent = editorState.getCurrentContent();
    let currentKey = selection.getStartKey();
    let lastBlock = currentContent.getLastBlock(),
      lastKey = lastBlock.getKey();
    let blockMap = currentContent.getBlockMap().toJS();
    this.setState(
      {
        textCard,
        message: textCard.toString('markdown'),
        textErrorText: '',
        disableSubmit: false
      },
      () => {
        if (
          currentKey == lastKey &&
          selection.getFocusOffset() == blockMap[currentKey].text.length
        ) {
          let elem = document.getElementById('editorRichText').firstChild;
          elem.scrollTop = elem.scrollHeight;
        }
      }
    );
  };

  addQuestion = event => {
    if (!event.target.value.trim().length) {
      this.setState({
        pollErrorText: "Question can't be empty",
        disableSubmit: true,
        showPollLink: false,
        linkCard: {}
      });
      return;
    }
    this.setState({
      message: event.target.value,
      disableSubmit: false,
      pollErrorText: '',
      lastClicked: Date.now()
    });
    this.delayHandler(event.target.value, 'pollErrorText', '', 'showPollLink', this.state.message);
  };

  addQuiz = event => {
    if (!event.target.value.trim().length) {
      this.setState({
        pollErrorText: "Question can't be empty",
        disableSubmit: true
      });
      return;
    }
    this.setState({ message: event.target.value, disableSubmit: false, pollErrorText: '' });
  };

  setStatePending = (state, val) => {
    return new Promise(resolve => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  delayHandler = (text, errorVar, errorText, showVar, textInState) => {
    if (text !== textInState) {
      setTimeout(() => {
        if (Date.now() - this.state.lastClicked >= 1000) {
          this.checkTextOnURL(text, errorVar, errorText, showVar);
        }
      }, 1000);
    }
  };

  addEventScrollForLinks = () => {
    this._linksBlock && this._linksBlock.addEventListener('scroll', this.scrollHandler);
  };

  removeEventScrollForLinks = () => {
    this._linksBlock && this._linksBlock.removeEventListener('scroll', this.scrollHandler);
  };

  scrollHandler = _.throttle(() => {
    if (this.state.pendingCheckDuplicate) {
      return;
    }

    let container = this._linksBlock;
    if (container.scrollTop === container.scrollHeight - container.offsetHeight) {
      if (this.state.duplicateCards.length < this.state.duplicateTotal) {
        this.checkDuplicate(true);
      }
    }
  }, 150);

  checkDuplicate = async (isLazyLoad, cb) => {
    let { linkUrl, duplicateCards, pendingCheckDuplicate } = this.state;
    if (pendingCheckDuplicate) {
      return;
    }
    await this.setStatePending('pendingCheckDuplicate', true);
    this.removeEventScrollForLinks();

    if (linkUrl.indexOf('http') !== 0) {
      linkUrl = `https://${linkUrl}`;
    }
    let payload = {
      resource_url: linkUrl,
      limit: this.limitDuplicate,
      offset: isLazyLoad ? duplicateCards.length : 0
    };
    let data = await checkDuplicateCard(payload);
    this.setState(
      {
        duplicateTotal: data.total,
        duplicateCards: isLazyLoad === true ? duplicateCards.concat(data.cards) : data.cards,
        pendingCheckDuplicate: false
      },
      () => {
        if (data.total) {
          this.addEventScrollForLinks();
          cb && cb(data.total);
        }
      }
    );
  };

  checkTextOnURL = async (text, errorVar, errorText, showVar, callback) => {
    let textParam = text.trim();
    let isUrl = isUrlValid(textParam);
    if (isUrl) {
      if (textParam.indexOf('http') !== 0) {
        textParam = `https://${textParam}`;
      }
      if (this.typeOfDetectDuplicate === 'auto') {
        let isDuplicate = false;
        await this.checkDuplicate(false, total => {
          if (total) {
            isDuplicate = true;
          }
        });
        if (isDuplicate) {
          this.setState({ [errorVar]: '', [showVar]: false });
          return callback && callback();
        }
      }
      await this.setStatePending('pendingImageUpload', true);
      this.setState({ disableSubmit: true, [errorVar]: '', [showVar]: false });
      try {
        let teamConfig = this.props.team && this.props.team.config;
        let isS3BucketEnabled =
          this.state.customDefaultImages &&
          teamConfig &&
          teamConfig['custom_images_bucket_name'] &&
          teamConfig['custom_images_region'];
        let data = await createResource(textParam, !!isS3BucketEnabled);
        data.url = textParam;
        if (
          !data.embedHtml &&
          (!data.imageUrl || (data.imageUrl && !data.imageUrl.trim().length))
        ) {
          data.imageUrl = getDefaultImage(this.props.currentUser.id).url;
          let updatedResource = await updateResource(data.id, { image_url: data.imageUrl });
          this.setState({
            [showVar]: true,
            fileStack: [],
            linkCard: data,
            disableSubmit: false,
            resourceId: data.id,
            message: data.title || '',
            linkTypeDescription: this.state.activeType === 'link' && data.description === null
          });
          if (updatedResource) await this.setStatePending('pendingImageUpload', false);
          /*eslint callback-return: "off"*/
          callback && callback();
        } else {
          this.checkDataOnImage(data)
            .then(checkData => {
              this.setState({
                [showVar]: true,
                fileStack: [],
                linkCard: checkData,
                disableSubmit: false,
                resourceId: checkData.id,
                message: checkData.title || '',
                linkTypeDescription: this.state.activeType === 'link' && data.description === null
              });
              this.setStatePending('pendingImageUpload', false);
              /*eslint callback-return: "off"*/
              callback && callback();
            })
            .catch(err => {
              console.error(`Error in SmartBite.checkDataOnImage.func : ${err}`);
            });
        }
      } catch (err) {
        if (err && err.response) {
          let error = JSON.parse(err.response.text).message;
          this.setState({ [errorVar]: error, disableSubmit: true });
          if (err.response.text && ~err.response.text.indexOf('Forbidden content')) {
            this.props.dispatch(
              openSnackBar(
                'Sorry, the content you are trying to post is from unapproved website or words.',
                true
              )
            );
          }
        }
        this.setState({ [showVar]: false });
        /*eslint callback-return: "off"*/
        callback && callback();
      }

      await this.setStatePending('pendingImageUpload', false);
    } else {
      this.setState({ [errorVar]: errorText, [showVar]: false });
      return callback && callback();
    }
  };

  checkDataOnImage = data => {
    let image = document.createElement('img');
    image.src = data.imageUrl;
    return new Promise(resolve => {
      image.onerror = () => {
        data.imageUrl = getDefaultImage(this.props.currentUser.id).url;
        updateResource(data.id, { image_url: data.imageUrl });
        resolve(data);
      };
      image.onload = () => {
        resolve(data);
      };
    });
  };

  messageChangeHandler = event => {
    if (!event.target.value.trim().length) {
      this.setState({
        linkErrorText: "Link can't be empty",
        disableSubmit: true,
        showPreviewLink: false,
        linkCard: {},
        linkUrl: '',
        duplicateTotal: 0,
        duplicateCards: []
      });
      return;
    }
    this.setState({
      linkUrl: event.target.value,
      disableSubmit: true,
      linkErrorText: '',
      lastClicked: Date.now(),
      duplicateTotal: 0,
      duplicateCards: []
    });
    this.delayHandler(
      event.target.value,
      'linkErrorText',
      'Incorrect url',
      'showPreviewLink',
      this.state.linkUrl
    );
  };

  updateLinkFile(fileStack) {
    if (this.state.resourceId) {
      let securedUrl;
      this.setState({ showPreviewLink: false, pendingImageUpload: true });
      refreshFilestackUrl(fileStack.filesUploaded[0].url)
        .then(resp => {
          securedUrl = resp.signed_url;
          updateResource(this.state.resourceId, { image_url: fileStack.filesUploaded[0].url })
            .then(() => {
              this.setState({
                showPreviewLink: true,
                linkCard: Object.assign({}, this.state.linkCard, {
                  imageUrl: fileStack.filesUploaded[0].url
                }),
                pendingImageUpload: false,
                securedUrl: securedUrl
              });
            })
            .catch(err => {
              console.error(`Error in SmartBite.updateResource.func : ${err}`);
            });
        })
        .catch(err => {
          console.error(`Error in SmartBite.updateLinkFile.refreshFilestackUrl.func : ${err}`);
        });
    }
  }

  addFileStackImage(fileStack) {
    refreshFilestackUrl(fileStack.filesUploaded[0].url)
      .then(resp => {
        this.setState({
          fileStack: fileStack.filesUploaded,
          cardPreview: true,
          securedUrl: resp.signed_url
        });
      })
      .catch(err => {
        console.error(`Error in SmartBite.addFileStackImage.refreshFilestackUrl.func : ${err}`);
      });
  }

  addFileStackFiles(fileStack) {
    let videoPreTranscodeUrl;
    let securedUrl;
    refreshFilestackUrl(fileStack.filesUploaded[0].url)
      .then(resp => {
        if (
          fileStack.filesUploaded[0].mimetype &&
          ~fileStack.filesUploaded[0].mimetype.indexOf('video')
        ) {
          let securedVideoUrl = resp.signed_url;
          let securedVideoPath = parseUrl(securedVideoUrl).pathname;
          videoTranscode(securedVideoPath)
            .then(data => {
              this.setState({
                transcodedVideoUrl: data.status === 'completed' ? data.data.url : null,
                transcodedThumbnail: data.status === 'completed' ? data.data.thumbnail : null,
                transcodedVideoStatus: data.status
              });
            })
            .catch(err => {
              console.error(`Error in SmartBite.videoTranscode.func : ${err}`);
            });
          videoPreTranscodeUrl = securedVideoUrl || fileStack.filesUploaded[0].url;
        } else {
          securedUrl = resp.signed_url;
        }

        this.setState({
          message:
            fileStack.filesUploaded && this.state.activeType === 'upload'
              ? fileStack.filesUploaded[0].filename
              : this.state.message,
          uploadErrorText: '',
          disableSubmit: false,
          fileStack: [...this.state.fileStack, ...fileStack.filesUploaded],
          cardPreview: true,
          securedUrl: securedUrl,
          videoPreTranscodeUrl
        });
      })
      .catch(err => {
        console.error(`Error in SmartBite.addFileStackFiles.refreshFilestackUrl.func : ${err}`);
      });
  }

  updateScormCardImage(filestack) {
    if (this.state.fileStack[0].scorm_course) {
      let fs = this.state.fileStack;
      fs[1] = filestack.filesUploaded[0];
      refreshFilestackUrl(filestack.filesUploaded[0].url)
        .then(resp => {
          this.setState({ fileStack: fs, cardPreview: true, securedUrl: resp.signed_url });
        })
        .catch(err => {
          console.error(
            `Error in SmartBite.updateScormCardImage.refreshFilestackUrl.func : ${err}`
          );
        });
    }
  }

  updateFileStackFile(i, fileStack) {
    let isPoster =
      fileStack.filesUploaded &&
      fileStack.filesUploaded[0] &&
      fileStack.filesUploaded[0].mimetype &&
      (~fileStack.filesUploaded[0].mimetype.indexOf('audio/') ||
        ~fileStack.filesUploaded[0].mimetype.indexOf('video/') ||
        ~fileStack.filesUploaded[0].mimetype.indexOf('/pdf') ||
        (~fileStack.filesUploaded[0].mimetype.indexOf('image/') && i > 0));
    let tempFiles = i === 0 && !isPoster ? [] : this.state.fileStack;
    tempFiles[i] = fileStack.filesUploaded[0];
    let videoPreTranscodeUrl, securedUrl, securedPosterUrl;
    let objToState = { fileStack: tempFiles, cardPreview: true };

    refreshFilestackUrl(tempFiles[i].url)
      .then(resp => {
        if (
          !i &&
          fileStack.filesUploaded[0].mimetype &&
          ~fileStack.filesUploaded[0].mimetype.indexOf('video')
        ) {
          let securedVideoUrl = resp.signed_url;
          let securedVideoPath = parseUrl(securedVideoUrl).pathname;
          videoTranscode(securedVideoPath)
            .then(data => {
              this.setState({
                transcodedVideoUrl: data.status === 'completed' ? data.data.url : null,
                transcodedThumbnail: data.status === 'completed' ? data.data.thumbnail : null,
                transcodedVideoStatus: data.status
              });
            })
            .catch(err => {
              console.error(`Error in SmartBite.updateFileStackFile.videoTranscode.func : ${err}`);
            });
          videoPreTranscodeUrl = securedVideoUrl || fileStack.filesUploaded[0].url;
        } else {
          if (isPoster) {
            objToState = { ...objToState, ...{ securedPosterUrl: resp.signed_url } };
          } else {
            objToState = { ...objToState, ...{ securedUrl: resp.signed_url } };
          }
        }
        videoPreTranscodeUrl ? (objToState.videoPreTranscodeUrl = videoPreTranscodeUrl) : null;
        this.setState(() => {
          objToState = {
            ...objToState,
            message:
              fileStack.filesUploaded && this.state.activeType === 'upload' && i === 0
                ? fileStack.filesUploaded[0].filename
                : this.state.message,
            uploadErrorText: '',
            disableSubmit: false
          };
          return objToState;
        });
      })
      .catch(err => {
        console.error(`Error in SmartBite.updateFileStackFile.refreshFilestackUrl.func : ${err}`);
      });
  }

  uploadProviderImage = fileStack => {
    let tempFiles = fileStack.filesUploaded;
    let url = tempFiles[0].url;
    refreshFilestackUrl(url)
      .then(resp => {
        this.setState({
          fileStackProvider: tempFiles[0].url,
          securedFileStackProviderUrl: resp.signed_url
        });
      })
      .catch(err => {
        console.error(`Error in SmartBite.uploadProviderImage.refreshFilestackUrl.func : ${err}`);
      });
  };

  clearFileStackProviderInfo = () => {
    this.setState({
      securedFileStackProviderUrl: null,
      fileStackProvider: null,
      removeProviderImage: true
    });
  };

  hoverBlock(onClick, handle, type) {
    let hoverClass = type === 'Video' ? 'roll-image-video' : 'roll-image';
    return (
      <span
        className={hoverClass}
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ [handle]: true });
        }}
        onFocus={() => {
          this.setState({ [handle]: true });
        }}
        onBlur={() => {
          this.setState({ [handle]: false });
        }}
        style={{ opacity: this.state[handle] ? '1' : '0' }}
      >
        <IconButton style={{ paddingBottom: '0', height: 'auto' }}>
          <ImagePhotoCamera color="white" viewBox="0 0 24 24" />
        </IconButton>
        <div className="roll-text">{tr('Change ' + type)}</div>
      </span>
    );
  }

  getEmbedHTML = embedHtml => {
    return {
      __html: embedHtml
        .replace(/width=\"[0-9]*\"/g, 'width="100%"')
        .replace(/height=\"[0-9]*\"/g, 'height="294"')
        .replace(/autoplay=[0-9]*/g, 'autoplay=0')
        .replace('feature=oembed', 'feature=oembed&rel=0')
    };
  };

  contentBlock = (item, full, spec, index, scorm) => {
    let item_filename = scorm ? item.filename : item.filename || item.handle;
    return (
      <div
        className={`text-center  ${spec ? 'overview-tags-container' : ''}`}
        key={index ? index : 0}
      >
        <div className="row">
          <div className={this.props.cardType === 'smartbite' ? 'small-6' : 'small-12 relative'}>
            {this.sourcePreview(item, index)}
          </div>
          <div
            className={
              this.props.cardType === 'smartbite' && !full
                ? 'small-6 file-title'
                : 'small-12 file-title'
            }
          >
            <span className="item-name" title={item_filename}>
              {item_filename}
            </span>
          </div>
        </div>
      </div>
    );
  };

  sourcePreview = (item, index) => {
    if (item.mimetype && !!~item.mimetype.indexOf('video/')) {
      return this.videoSourcePreview(item);
    } else if (item.mimetype && !!~item.mimetype.indexOf('audio/')) {
      return this.audioSourcePreview(item);
    } else if (item.mimetype && !!~item.mimetype.indexOf('/pdf')) {
      return this.pdfSourcePreview(item, index, 'PDF');
    } else {
      return this.fileSourceView(item, index);
    }
  };

  videoSourcePreview = item => {
    return (
      <div>
        <span>
          <div style={{ position: 'relative' }}>
            {this.state.fileStack[0] && (
              <video
                preload={this.state.filestack && this.state.filestack[1] ? 'none' : 'auto'}
                className="preview-upload-video"
                controls
                poster={
                  this.state.fileStack[1] &&
                  getResizedUrl(
                    this.state.securedPosterUrl || this.state.fileStack[1].url,
                    'height:225',
                    this.state.fileStack[1].mimetype
                  )
                }
                src={this.state.videoPreTranscodeUrl || item.url}
                onMouseEnter={() => {
                  this.setState({ [item.handle]: true });
                }}
                onMouseLeave={() => {
                  this.setState({ [item.handle]: false });
                }}
                onError={() => {
                  this.refreshVideoUrl(item.handle);
                }}
                controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
              />
            )}
            {!this.state.fileStack[0] && this.state.transcodedVideoStatus && (
              <video
                className="preview-upload-video"
                controls
                src={
                  this.state.transcodedVideoStatus === 'completed'
                    ? this.state.transcodedVideoUrl
                    : this.state.videoPreTranscodeUrl
                }
                onMouseEnter={() => {
                  this.setState({ [item.handle]: true });
                }}
                onMouseLeave={() => {
                  this.setState({ [item.handle]: false });
                }}
                onError={() => {
                  this.refreshVideoUrl(item.handle);
                }}
                controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
              />
            )}
            <div>
              {this.hoverBlock(
                this.fileStackHandler.bind(
                  this,
                  fullAccept,
                  this.updateFileStackFile.bind(this, 0)
                ),
                item.handle,
                'Video'
              )}
            </div>
          </div>
        </span>
        {this.isAbleUpload && (
          <div className="text-block">
            <FlatButton
              className="imageUpload replace-image-upload"
              icon={
                <ImagePhotoCamera
                  style={this.styles.imageIcon}
                  color="#bababa"
                  viewBox="0 0 24 24"
                />
              }
              onClick={this.fileStackHandler.bind(
                this,
                ['image/*'],
                this.updateFileStackFile.bind(this, 1)
              )}
              style={this.styles.imageUpload}
            >
              Replace Video Thumbnail
            </FlatButton>
          </div>
        )}
      </div>
    );
  };

  refreshAudioUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let audio = document.getElementsByClassName('audio-upload')[0];
          let seen = audio.currentTime;
          audio.setAttribute('src', newUrl);
          audio.currentTime = seen;
          if (seen !== 0) {
            audio.play();
          }
        })
        .catch(err => {
          console.error(`Error in SmartBite.refreshAudioUrl.refreshFilestackUrl.func : ${err}`);
        });
    }
  };

  refreshVideoUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let video = document.getElementsByClassName('preview-upload-video')[0];
          let seen = video.currentTime;
          video.setAttribute('src', newUrl);
          video.currentTime = seen;
          if (seen !== 0) {
            video.play();
          }
        })
        .catch(err => {
          console.error(`Error in SmartBite.refreshVideoUrl.refreshFilestackUrl.func : ${err}`);
        });
    }
  };

  audioSourcePreview = item => {
    if (item) {
      return (
        <div>
          <span>
            <div>
              {this.state.fileStack[1] && (
                <div className="preview-upload">
                  <div
                    className="card-img-container"
                    onClick={this.fileStackHandler.bind(
                      this,
                      ['image/*'],
                      this.updateFileStackFile.bind(this, 1)
                    )}
                    onMouseEnter={() => {
                      this.setState({ [this.state.fileStack[1].handle]: true });
                    }}
                    onMouseLeave={() => {
                      this.setState({ [this.state.fileStack[1].handle]: false });
                    }}
                  >
                    <div
                      className="card-img button-icon"
                      style={{
                        ...this.styles.imagePreview,
                        ...{
                          backgroundImage: `url(\'${getResizedUrl(
                            this.state.securedPosterUrl || this.state.fileStack[1].url,
                            'height:230',
                            this.state.fileStack[1].mimetype
                          )}\')`
                        }
                      }}
                    />
                  </div>
                </div>
              )}
              <audio
                className="audio-upload"
                controls
                src={this.state.securedUrl || item.url}
                controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
                onError={() => {
                  this.refreshAudioUrl(item.handle);
                }}
              />
              {this.isAbleUpload && (
                <div className="text-block">
                  <FlatButton
                    className="imageUpload replace-image-upload"
                    icon={
                      <AudioTrack
                        style={this.styles.imageIcon}
                        color="#bababa"
                        viewBox="0 0 24 24"
                      />
                    }
                    onClick={this.fileStackHandler.bind(
                      this,
                      fullAccept,
                      this.updateFileStackFile.bind(this, 0)
                    )}
                    style={this.styles.imageUpload}
                  >
                    {tr('Change Audio')}
                  </FlatButton>
                </div>
              )}
              {this.isAbleUpload && (
                <div className="text-block">
                  <FlatButton
                    className="imageUpload replace-image-upload"
                    icon={
                      <ImagePhotoCamera
                        style={this.styles.imageIcon}
                        color="#bababa"
                        viewBox="0 0 24 24"
                      />
                    }
                    onClick={this.fileStackHandler.bind(
                      this,
                      ['image/*'],
                      this.updateFileStackFile.bind(this, 1)
                    )}
                    style={this.styles.imageUpload}
                  >
                    {tr(`${this.state.fileStack[1] ? 'Replace' : 'Add'} Audio Thumbnail`)}
                  </FlatButton>
                </div>
              )}
            </div>
          </span>
        </div>
      );
    }
  };

  pdfSourcePreview = (item, index, typeFile) => {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    return (
      <div>
        {this.state.fileStack[1] && (
          <div className="preview-upload">
            <div
              className="card-img-container"
              onClick={this.fileStackHandler.bind(
                this,
                ['image/*'],
                this.updateFileStackFile.bind(this, 1)
              )}
              onMouseEnter={() => {
                this.setState({ [this.state.fileStack[1].handle]: true });
              }}
              onMouseLeave={() => {
                this.setState({ [this.state.fileStack[1].handle]: false });
              }}
            >
              <div
                className="card-img button-icon"
                style={{
                  ...this.styles.imagePreview,
                  ...{
                    backgroundImage: `url(\'${getResizedUrl(
                      this.state.securedPosterUrl || this.state.fileStack[1].url,
                      'height:230',
                      this.state.fileStack[1].mimetype
                    )}\')`
                  }
                }}
              />
            </div>
          </div>
        )}
        <div className="pdf-upload">
          {this.state.activeType !== 'scorm' ? (
            <PdfViewer
              height="230"
              width="330"
              src={this.state.securedUrl || item.url}
              expireAfter={expireAfter}
              isDownloadContentDisabled={this.isDownloadContentDisabled}
              currentUser={this.props.currentUser}
              onMouseEnterEvent={() => {
                this.setState({ [item.handle]: true });
              }}
              onMouseLeaveEvent={() => {
                this.setState({ [item.handle]: false });
              }}
            />
          ) : (
            this.renderLinkResource('showPreviewLink')
          )}
          <div>
            {this.hoverBlock(
              this.fileStackHandler.bind(this, fullAccept, this.updateFileStackFile.bind(this, 0)),
              item.handle,
              typeFile === 'Doc' ? typeFile : 'File'
            )}
          </div>
        </div>
        {this.isAbleUpload && (
          <div>
            <div className="text-block">
              <FlatButton
                className="imageUpload replace-image-upload"
                icon={
                  <ImagePhotoCamera
                    style={this.styles.imageIcon}
                    color="#bababa"
                    viewBox="0 0 24 24"
                  />
                }
                onClick={this.fileStackHandler.bind(
                  this,
                  ['image/*'],
                  this.updateFileStackFile.bind(this, 1)
                )}
                style={this.styles.imageUpload}
              >
                {tr(`${this.state.fileStack[1] ? 'Replace' : 'Add'} ${typeFile} Thumbnail`)}
              </FlatButton>
            </div>
          </div>
        )}
      </div>
    );
  };

  fileSourceView = (item, index) => {
    if (this.state.activeType === 'scorm') {
      return;
    } else if (item.mimetype && ~item.mimetype.indexOf('image/')) {
      return (
        <div className="preview-upload">
          <div
            className="card-img-container"
            onMouseEnter={() => {
              this.setState({ [item.handle]: true });
            }}
            onMouseLeave={() => {
              this.setState({ [item.handle]: false });
            }}
          >
            <div
              className="card-img button-icon"
              style={{
                backgroundImage: `url(\'${getResizedUrl(
                  this.state.securedUrl || item.url,
                  'height:230',
                  item.mimetype
                )}\')`,
                position: 'relative',
                backgroundSize: 'contain',
                backgroundPosition: '50% 50%',
                backgroundRepeat: 'no-repeat',
                height: '100%'
              }}
            >
              {this.hoverBlock(
                this.fileStackHandler.bind(
                  this,
                  fullAccept,
                  this.updateFileStackFile.bind(this, index)
                ),
                item.handle,
                'Image'
              )}
            </div>
          </div>
        </div>
      );
    } else if (item.mimetype && ~item.mimetype.indexOf('video/')) {
      return (
        <span>
          {this.state.transcodedVideoStatus &&
            (this.state.transcodedVideoStatus === 'completed' ? (
              <video
                className="preview-upload-video"
                controls
                src={this.state.transcodedVideoUrl}
                onError={() => {
                  this.refreshVideoUrl(item.handle);
                }}
                onMouseEnter={() => {
                  this.setState({ [item.handle]: true });
                }}
                onMouseLeave={() => {
                  this.setState({ [item.handle]: false });
                }}
                controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
              />
            ) : (
              <img className="waiting-video" src="/i/images/video_processing_being_processed.jpg" />
            ))}
          {this.hoverBlock(
            this.fileStackHandler.bind(
              this,
              fullAccept,
              this.updateFileStackFile.bind(this, index)
            ),
            item.handle,
            'Video'
          )}
          {this.isAbleUpload && (
            <div className="text-block">
              <FlatButton
                className="imageUpload"
                icon={
                  <ImagePhotoCamera
                    style={this.styles.imageIcon}
                    color="#bababa"
                    viewBox="0 0 24 24"
                  />
                }
                onClick={this.fileStackHandler.bind(this, fullAccept, this.addFileStackFiles)}
                style={this.styles.imageUpload}
                aria-label="upload image"
              />
            </div>
          )}
        </span>
      );
    } else {
      let typeFile =
        item.mimetype && (~item.mimetype.indexOf('.document') || ~item.mimetype.indexOf('msword'))
          ? 'Doc'
          : 'File';
      return this.pdfSourcePreview(item, index, typeFile);
    }
  };

  imageUploadUI = () => {
    if (this.state.cardPreview && this.state.fileStack[0]) {
      return (
        <div className="text-center relative">
          <div className="preview-upload">
            <div
              className="card-img-container"
              onMouseEnter={() => {
                this.setState({ [this.state.fileStack[0].handle]: true });
              }}
              onMouseLeave={() => {
                this.setState({ [this.state.fileStack[0].handle]: false });
              }}
            >
              <div
                className="card-img button-icon"
                style={{
                  backgroundImage: `url(\'${getResizedUrl(
                    this.state.securedUrl || this.state.fileStack[0].url,
                    'height:230',
                    this.state.fileStack[0].mimetype
                  )}\')`,
                  position: 'relative',
                  backgroundSize: 'contain',
                  backgroundPosition: '50% 50%',
                  backgroundRepeat: 'no-repeat',
                  height: '100%'
                }}
              >
                {this.hoverBlock(
                  this.fileStackHandler.bind(this, ['image/*'], this.addFileStackImage),
                  this.state.fileStack[0].handle,
                  'Image'
                )}
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      let fileUpdate =
        this.state.activeType === 'link' ? this.updateLinkFile : this.addFileStackImage;
      return (
        <div className="text-block">
          {this.isAbleUploadCover && (
            <FlatButton
              className="imageUpload"
              icon={
                <ImagePhotoCamera
                  style={this.styles.imageIcon}
                  color="#bababa"
                  viewBox="0 0 24 24"
                />
              }
              onClick={this.fileStackHandler.bind(this, ['image/*'], fileUpdate)}
              style={this.styles.imageUpload}
              aria-label="upload image"
            />
          )}
        </div>
      );
    }
  };

  renderLinkResource = showVar => {
    if (
      !this.state[showVar] &&
      (this.state.pendingImageUpload ||
        (this.state.pendingCheckDuplicate && this.typeOfDetectDuplicate === 'auto'))
    ) {
      return (
        <div className="text-center text-block">
          <Spinner />
        </div>
      );
    } else {
      let accept = this.state.linkCard.id ? ['image/*'] : fullAccept;
      let callback = this.state.linkCard.id ? this.updateLinkFile : this.addFileStackFiles;
      let url = this.state.linkCard.videoUrl || this.state.linkCard.url;
      return (
        <div className="text-center relative">
          {this.state.fileStack &&
            this.state.fileStack[0] &&
            !!this.state.fileStack[0].scorm_course && (
              <div className="preview-upload">
                <div
                  className="card-img-container"
                  onMouseEnter={() => {
                    this.setState({ [this.state.linkCard.id]: true });
                  }}
                  onMouseLeave={() => {
                    this.setState({ [this.state.linkCard.id]: false });
                  }}
                >
                  <div
                    className="card-img button-icon"
                    style={{
                      backgroundImage: `url(\'${getResizedUrl(
                        this.state.securedUrl || this.state.fileStack[1]['url'],
                        'height:230',
                        this.state.fileStack[1].mimetype
                      )}\')`,
                      position: 'relative',
                      backgroundSize: 'contain',
                      backgroundPosition: '50% 50%',
                      backgroundRepeat: 'no-repeat',
                      height: '100%'
                    }}
                  >
                    {this.hoverBlock(
                      this.fileStackHandler.bind(this, accept, this.updateScormCardImage),
                      this.state.linkCard.id,
                      'Image'
                    )}
                  </div>
                </div>
              </div>
            )}
          {this.state.linkCard &&
            this.state.linkCard.imageUrl &&
            this.state.linkCard.type !== 'Video' &&
            !(
              this.state.fileStack &&
              this.state.fileStack[0] &&
              !!this.state.fileStack[0].scorm_course
            ) && (
              <div className="preview-upload">
                <div
                  className="card-img-container"
                  onMouseEnter={() => {
                    this.setState({ [this.state.linkCard.id]: true });
                  }}
                  onMouseLeave={() => {
                    this.setState({ [this.state.linkCard.id]: false });
                  }}
                >
                  <div
                    className="card-img button-icon"
                    style={{
                      backgroundImage: `url(\'${getResizedUrl(
                        this.state.securedUrl || this.state.linkCard.imageUrl,
                        'height:230',
                        this.state.linkCard.mimetype
                      )}\')`,
                      position: 'relative',
                      backgroundSize: 'contain',
                      backgroundPosition: '50% 50%',
                      backgroundRepeat: 'no-repeat',
                      height: '100%'
                    }}
                  >
                    {this.hoverBlock(
                      this.fileStackHandler.bind(this, accept, callback),
                      this.state.linkCard.id,
                      'Image'
                    )}
                  </div>
                </div>
              </div>
            )}
          {this.state.linkCard && url && this.state.linkCard.type === 'Video' && (
            <div
              className="preview-video"
              dangerouslySetInnerHTML={addParamStart(
                this.getEmbedHTML(
                  this.state.linkCard.embedHtml || `<iframe width="1" src="${url}"></iframe>`
                ),
                url
              )}
            />
          )}
          {this.state.linkCard &&
            this.state.linkCard.id &&
            !this.state.linkCard.imageUrl &&
            this.state.linkCard.type !== 'Video' &&
            this.imageUploadUI()}
        </div>
      );
    }
  };

  closeCard = () => {
    this.setState({ closeClick: true }, this.props.createdCard);
  };

  chipRender = (name, idx) => {
    return (
      <Chip
        key={name + idx}
        style={this.styles.chip}
        className="channel-item-insight"
        backgroundColor={colors.white}
        labelColor={colors.primary}
      >
        {' '}
        {name}{' '}
        <CloseIcon
          onClick={() => {
            this.removeTag(name);
          }}
          style={this.styles.chipClose}
        />
      </Chip>
    );
  };

  checkQuizPoll = (type, payload, closeStatus) => {
    if (this.state.linkCard.id) {
      payload.resource_id = this.state.linkCard.id;
    } else {
      payload.resource_id = null;
    }
    if (!this.state.message && !closeStatus) {
      this.setState({ pollErrorText: "Question can't be empty", disableSubmit: true });
      return;
    } else {
      this.recheckError(type);
      let filteredOptions = this.state[type].filter(item => {
        return item.label.trim().length;
      });
      if (type === 'quizOptions') {
        let correctAnswer = this.state[type].filter(item => {
          return item.isCorrect;
        });
        if (!correctAnswer.length && !closeStatus) {
          this.setState({ optionsError: 'Please add correct answer!', disableSubmit: true });
          return;
        }
      }
      if (filteredOptions.length < 2 && !closeStatus) {
        this.setState({ optionsError: 'Please add options!', disableSubmit: true });
        return;
      } else {
        payload.options = filteredOptions.map(item => ({
          label: item.label,
          is_correct: item.isCorrect,
          id: item.id
        }));
        if (this.state.removedOptions.length) {
          payload.options = payload.options.concat(this.state.removedOptions);
        }
        payload.card_type = this.state.activeType;
        payload.can_be_reanswered = this.state.canBeReanswered;
      }
    }
    return payload;
  };

  isEmpty = () => {
    return !this.state.textCard
      .toString('html')
      .replace(/<p><br><\/p>|<p>&nbsp;|&nbsp;|\n|<\/p>/gm, '').length;
  };

  isSuitable = () => {
    return (
      this.state.textCard
        .toString('html')
        .replace(/<p><br><\/p>|<p>|<p>&nbsp;|&nbsp;|\n|<\/p>/gm, '').length <= maxTextLength
    );
  };

  createClickHandler = (id = null, closeStatus) => {
    let isECL = /^ECL-/.test(this.props.card ? this.props.card.id : '');
    if (this.state.activeType === 'smart' || this.state.activeType === 'bookmark') {
      if (closeStatus) {
        this.props.saveOutClick(null, this.state.searchText);
      } else {
        let checkedCards = _.uniqBy(this.state.searchCards.filter(item => item.checked), 'id');
        if (checkedCards.length) {
          this.setState({ pathwayLabel: 'Doing...', closeCard: true });
          this.props.createdCard(checkedCards, this.state.selectedBia);
        } else {
          this.setState({ searchError: 'Select at least one card!' });
        }
      }
    } else {
      let sharedtoMembers = [];
      let sharedtoChannels = [];
      let sharedtoGroups = [];
      let teams_with_permission_ids = [];
      let users_with_permission_ids = [];
      sharedtoChannels = _.uniq(sharedtoChannels.concat(this.state.nonCuratedChannelIds));
      this.state.tags.forEach(tag => {
        switch (tag.label) {
          case 'member':
            sharedtoMembers.push(tag.id);
            break;
          case 'channel':
            sharedtoChannels.push(tag.id);
            sharedtoChannels = _.uniq(sharedtoChannels.concat(this.state.nonCuratedChannelIds));
            break;
          case 'group':
            sharedtoGroups.push(tag.id);
            break;
          default:
            // FIXME: implement default case
            break;
        }
      });
      let temp_prices = this.state.prices.map(price => {
        delete price.symbol;
        return price;
      });

      let payload = {
        message: this.state.message,
        users_with_access_ids: sharedtoMembers,
        channel_ids: sharedtoChannels,
        team_ids: sharedtoGroups,
        is_public: !this.state.isPrivateContent + '',
        is_paid: this.state.isPaid,
        prices_attributes: temp_prices,
        title: ''
      };

      //=====restricted to payload
      if (this.state.isPrivateContent) {
        this.state.restrictedToUsers.map(restrictTo => {
          if (restrictTo.label == 'group') {
            teams_with_permission_ids.push(restrictTo.id);
          }
          if (restrictTo.label == 'member') {
            users_with_permission_ids.push(restrictTo.id);
          }
        });

        if (teams_with_permission_ids.length > 0) {
          payload['teams_with_permission_ids'] = teams_with_permission_ids;
        }

        if (users_with_permission_ids.length > 0) {
          payload['users_with_permission_ids'] = users_with_permission_ids;
        }
      }

      if (
        (this.restrictToFlag && !this.state.isPrivateContent) ||
        (this.restrictToFlag && this.state.restrictedToUsers.length == 0)
      ) {
        payload['teams_with_permission_ids'] = [];
        payload['users_with_permission_ids'] = [];
      }

      if (this.state.multilingualContent) {
        this.state.language === 'unspecified'
          ? (payload.language = null)
          : (payload.language = this.state.language);
      }

      if (
        this.state.edcastPricing &&
        this.state.edcastPlansForPricing &&
        this.state.plan &&
        this.state.planChange
      ) {
        if (this.state.planId) {
          payload['card_metadatum_attributes'] = {
            id: this.state.planId,
            plan: this.state.plan
          };
        } else {
          payload['card_metadatum_attributes'] = {
            plan: this.state.plan
          };
        }
      }
      if (
        this.state.selectedBia ||
        (this.state.selectedBia === '' && this.props.card && this.props.card.skillLevel)
      ) {
        let metadatum_attributes = payload['card_metadatum_attributes'];
        let selectedBia = this.state.selectedBia;
        if (selectedBia === '') {
          selectedBia = null;
        }
        if (metadatum_attributes) {
          payload['card_metadatum_attributes']['level'] = selectedBia;
        } else {
          payload['card_metadatum_attributes'] = { level: selectedBia };
          if (this.state.planId) {
            payload['card_metadatum_attributes']['id'] = this.state.planId;
          }
        }
      }
      this.setState({ currencyErrorText: '', priceErrorText: '' });
      if (this.state.isPaid && _.isEmpty(this.state.prices)) {
        if (!this.state.currency && this.state.isPaid) {
          this.setState({ currencyErrorText: 'Cant be empty if Paid content is selected' });
          return;
        }
        if (!this.state.amount && this.state.isPaid) {
          this.setState({ priceErrorText: 'Cant be empty if Paid content is selected' });
          return;
        }
        if (this.state.amount <= 0) {
          this.setState({ priceErrorText: 'Amount should be greater than 0' });
          return;
        } else if (this.state.amount >= 999999.99) {
          this.setState({ priceErrorText: 'Amount should be less than 999999.99' });
          return;
        } else {
          this.setState({ currencyErrorText: 'You need to click Add button' });
          return;
        }
      } else if (
        this.state.isPaid &&
        this.state.currency.length > 0 &&
        this.state.price.length > 0
      ) {
        if (this.state.price <= 0) {
          this.setState({ priceErrorText: 'Amount should be greater than 0' });
          return;
        } else if (this.state.price >= 999999.99) {
          this.setState({ priceErrorText: 'Amount should be less than 999999.99' });
          return;
        } else {
          this.setState({ currencyErrorText: 'You need to click Add button' });
          return;
        }
      } else if (this.state.isPaid && this.state.prices) {
        let pricesWithoutDestroyAttribute = _.filter(this.state.prices, function(price) {
          return !price._destroy;
        });
        if (_.isEmpty(pricesWithoutDestroyAttribute)) {
          if (!this.state.currency) {
            this.setState({ currencyErrorText: 'Cant be empty if Paid content is selected' });
            return;
          }
          if (!this.state.amount) {
            this.setState({ priceErrorText: 'Cant be empty if Paid content is selected' });
            return;
          }
        }
      }

      payload['readable_card_type_name'] = this.state.currentReadableCardType;
      if (this.state.activeType === 'link') {
        if (isECL) {
          payload['message'] = this.state.message;
        } else if (this.state.linkCard) {
          payload['resource_id'] = this.state.linkCard.id;
          payload['message'] =
            this.state.message || this.state.linkCard.title || this.state.linkCard.url;
          if (this.state.description !== '') {
            payload.resource_attributes = {
              id: this.state.linkCard.id,
              description: this.state.description
            };
          }

          if (closeStatus) {
            payload.resource = this.state.linkCard;
          }
        } else if (!closeStatus) {
          this.setState({ linkErrorText: "Link can't be empty", disableSubmit: true });
          return;
        }
      }
      if (this.props.card && this.props.card.cardSubtype === 'image') {
        payload.resource_id = null;
        payload.file_resource_ids = [];
      }
      if (this.state.activeType === 'poll') {
        payload = this.checkQuizPoll('pollOptions', payload, closeStatus);
        if (!(payload || closeStatus)) return;
      }
      if (this.state.activeType === 'quiz') {
        payload = this.checkQuizPoll('quizOptions', payload, closeStatus);
        if (!(payload || closeStatus)) return;
      }
      if (this.state.activeType === 'upload') {
        if (this.state.fileStack[0]) {
          payload.card_subtype =
            this.state.fileStack[0].mimetype && ~this.state.fileStack[0].mimetype.indexOf('image/')
              ? 'image'
              : ~this.state.fileStack[0].mimetype.indexOf('video/')
              ? 'video'
              : ~this.state.fileStack[0].mimetype.indexOf('audio/')
              ? 'audio'
              : 'file';
          if (this.state.message.trim() && !closeStatus) {
            payload.message = this.state.message;
            this.setState({ titleError: false });
          } else if (this.state.message.trim() === '' && !closeStatus) {
            this.setState({ titleError: true, disableSubmit: true });
          }
        } else if (
          !this.state.fileStack[0] &&
          this.props.card &&
          this.props.card.filestack &&
          this.props.card.filestack[0]
        ) {
          payload.card_subtype =
            this.props.card.filestack[0].mimetype &&
            ~this.props.card.filestack[0].mimetype.indexOf('image/')
              ? 'image'
              : ~this.props.card.filestack[0].mimetype.indexOf('video/')
              ? 'video'
              : ~this.state.fileStack[0].mimetype.indexOf('audio/')
              ? 'audio'
              : 'file';
          if (this.state.message.trim() && !closeStatus) {
            payload.message = this.state.message;
            this.setState({ titleError: false });
          } else if (this.state.message.trim() === '' && !closeStatus) {
            this.setState({ titleError: true, disableSubmit: true });
          }
        } else {
          this.setState({
            uploadErrorText: 'You didn’t add any file. Please, do it!',
            disableSubmit: true
          });
          return;
        }
      }
      if (this.state.activeType === 'text') {
        if (closeStatus) {
          payload.cardType = 'media';
          payload.cardSubtype = this.state.activeType;
        }
        if (this.isEmpty() && !closeStatus) {
          this.setState({ textErrorText: "Text can't be empty", disableSubmit: true });
          return;
        }
        if (!this.isSuitable()) {
          this.setState({
            textErrorText: "Text can't be larger than 50000 characters",
            disableSubmit: true
          });
          return;
        }
      }

      if (this.state.activeType === 'project') {
        if (!this.state.projectName) {
          this.setState({ disableSubmit: false, NameErrorText: 'This is required' });
          return;
        }
        payload.project_attributes = {
          reviewer_id: ''
        };
        payload['title'] = this.state.projectName;
        payload.card_type = this.state.activeType;
      }
      if (this.state.activeType === 'scorm') {
        if (!this.state.message.length) {
          this.setState({ scormErrorText: "Message can't be empty", disableSubmit: true });
          return;
        }
        if (!this.state.fileStack[0]) {
          this.setState({
            uploadErrorText: 'You didn’t add any file. Please, do it!',
            disableSubmit: true
          });
          return;
        }
      }

      if (
        this.state.setCardTypeMandate &&
        this.state.activeType !== 'poll' &&
        this.state.activeType !== 'quiz' &&
        this.state.currentReadableCardType === ''
      ) {
        this.setState({
          cardTypeNotSetError: true
        });
        return;
      }
      this.setState({
        createLabel: this.props.card ? 'Updating...' : 'Creating...',
        pathwayLabel: 'Doing...',
        disableForm: true
      });

      if (this.state.topics || (this.props.card && !!this.props.card.tags.length)) {
        if (this.state.shareOnSmartCardCreation && this.state.isAbleToMarkPrivate) {
          payload['topics'] = _.map(this.state.topicTags, topic => {
            return topic.name;
          });
        } else {
          payload['topics'] = this.state.topics;
        }
      }
      if (this.state.advSettings.provider || this.state.advSettings.provider === '') {
        payload.provider = this.state.advSettings.provider;
      }
      if (this.state.removeProviderImage || this.state.fileStackProvider) {
        payload.provider_image = this.state.fileStackProvider;
        this.setState({ removeProviderImage: false });
      }
      if (this.state.advSettings.duration) {
        payload.duration = this.state.advSettings.duration;
      } else {
        payload.duration = 0;
      }
      if (this.state.fileStack.length > 0) {
        payload['filestack'] = this.state.fileStack;
      } else if (
        this.props.card &&
        this.props.card.filestack &&
        this.props.card.filestack.length > 0
      ) {
        //this makes sure that an edited smartbite keeps its old filestack resources (from this.props.card)
        payload['filestack'] = this.props.card.filestack;
      }
      if (this.state.activeType === 'scorm') {
        const defaultImg = getDefaultImage(this.props.currentUser.id);
        payload.card_subtype = 'link';
        payload['filestack'][0]['scorm_course'] = true;
        payload['state'] = 'processing';
        payload['filestack'][1] = defaultImg;
      }
      if (
        this.state.selectedChannels &&
        (!this.state.shareOnSmartCardCreation || !this.state.isAbleToMarkPrivate)
      ) {
        payload['channel_ids'] = this.state.selectedChannels;
      }

      if (this.props.cardType === 'pathway') {
        payload['hidden'] = true;
      }
      if (closeStatus) {
        this.props.saveOutClick(payload, this.state.searchText);
      } else {
        this.setState({ clicked: true, closeClick: true });
        this.props.saveOutClick && this.props.saveOutClick(null, this.state.searchText);
        if (id) {
          let encodeId = encodeURIComponent(id);
          if (isECL) {
            let self = this;
            if (self.state.linkCard && self.state.linkCard.id) {
              payload['resource_id'] = self.state.linkCard.id;
              self.sendPostv2(payload, encodeId);
            } else {
              this.checkTextOnURL(
                this.state.linkUrl,
                'pollErrorText',
                '',
                'showPollLink',
                function() {
                  payload['resource_id'] = self.state.linkCard.id;
                  self.sendPostv2(payload, encodeId);
                }
              );
            }
          } else {
            this.sendPostv2(payload, encodeId);
          }
        } else {
          this.sendPostv2(payload);
        }
      }
    }
  };

  callCreatedCard(data) {
    this.props.createdCard(data);
    this.setState({
      selectedChannels: [],
      message: '',
      linkUrl: '',
      topics: [],
      fileStack: [],
      pollOptions: [{ label: '' }, { label: '' }],
      linkCard: {},
      createLabel: this.props.card ? 'Update' : 'Create',
      disableForm: false,
      pathwayLabel: 'Done',
      showPreviewLink: false,
      closeCard: true,
      clicked: false
    });
  }

  handleUserAddition = tag => {
    switch (tag.label) {
      case 'member':
        _.remove(this.state.usersForRestrict, existingMember => {
          return existingMember.id + '' === tag.id + '';
        });
        break;
      case 'group':
        _.remove(this.state.groupsForRestrict, existingGroup => {
          return existingGroup.id + '' === tag.id + '';
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }

    let restrictedToUsers = this.state.restrictedToUsers;
    restrictedToUsers.push(tag);
    this.setState({ restrictedToUsers });
  };

  handleUserDelete = tagInfo => {
    let tag = typeof tagInfo === 'number' ? this.state.restrictedToUsers[tagInfo] : tagInfo;

    switch (tag.label) {
      case 'member':
        let newUserSuggestions = _.concat(this.state.usersForRestrict, tag);
        this.setState({
          usersForRestrict: newUserSuggestions
        });
        break;
      case 'group':
        let newGroupSuggestions = _.concat(this.state.groupsForRestrict, tag);
        this.setState({
          groupsForRestrict: newGroupSuggestions
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }
    let restrictedToUsers = this.state.restrictedToUsers.filter(data => data.id != tag.id);
    this.setState({ restrictedToUsers });
  };

  handleRestrictedInputChange = query => {
    if (!!!query) {
      return;
    }
    let team_ids = [];
    let channel_ids = [];
    let payload = {
      limit: 5,
      offset: 0,
      q: query
    };
    this.state.tags.map(tag => {
      if (tag.label === 'group') {
        team_ids.push(tag.id);
      } else if (tag.label === 'channel') {
        channel_ids.push(tag.id);
      }
    });

    if (team_ids.length > 0) {
      payload['team_ids[]'] = team_ids;
    }

    if (channel_ids.length > 0) {
      payload['channel_ids[]'] = channel_ids;
    }

    if (!!this.props.card) {
      payload['card_id'] = this.props.card.id;
    }

    getRestrictToUserOrGroup(payload)
      .then(restrictedToData => {
        let users = restrictedToData.users.map(user => {
          let editedUsers = user;
          editedUsers['label'] = 'member';
          return editedUsers;
        });
        let groups = restrictedToData.groups.map(group => {
          let editedGroup = group;
          editedGroup['label'] = 'group';
          return editedGroup;
        });
        let userOrTeamSuggestion = [...users, ...groups];

        this.setState({ userOrTeamSuggestion });
      })
      .catch(() => {});
  };

  updateThumbnail(payload) {
    if (
      payload.filestack[1] &&
      payload.filestack[1].mimetype &&
      payload.filestack[1].mimetype.indexOf('image') !== -1
    ) {
      return payload.filestack[1].url;
    } else {
      return this.state.transcodedThumbnail !== null ? this.state.transcodedThumbnail : '';
    }
  }

  cardCreationMessage(card) {
    let message = '';
    let curationMessage =
      ' If the card is posted to a curated channel, it will be visible in channel once curated.';

    // Messages for private card
    if (card && !card.isPublic) {
      let team_count = card.teams.length;
      let channel_count = card.channels.length + card.nonCuratedChannelIds.length;
      message = 'Your private card has been created.';
      let plural_group_lable = team_count > 1 ? 's' : '';
      let plural_channel_lable = channel_count > 1 ? 's' : '';
      if (team_count && channel_count) {
        message +=
          ` It is shared with ${team_count} Group${plural_group_lable} and ${channel_count} Channel${plural_channel_lable}.` +
          curationMessage;
      } else {
        if (team_count) {
          message += ` It is shared with ${team_count} Group${plural_group_lable}.`;
        }
        if (channel_count) {
          message +=
            ` It is shared with ${channel_count} Channel${plural_channel_lable}.` + curationMessage;
        }
      }
    } else {
      if (
        card &&
        !card.isPublic &&
        !card.teams.length &&
        !card.channels.length &&
        !card.usersWithAccess.length &&
        !card.teamsPermitted.length &&
        !card.usersPermitted.length
      ) {
        message = 'P.S. This smartcard will not be visible to anyone but the author.';
      }
    }

    // Messages for public card
    if (card && !card.hidden && card.isPublic) {
      message = 'Your card has been published publicly and will be accessible to everyone.';
      if (card.channels.length || card.nonCuratedChannelIds.length) {
        message += curationMessage;
      }
    }
    return message;
  }

  sendPostv2(payload, id) {
    if (
      payload &&
      payload.filestack &&
      payload.filestack.length > 0 &&
      payload.filestack[0].mimetype &&
      payload.filestack[0].mimetype.indexOf('video') !== -1
    ) {
      let thumbnailurl = this.updateThumbnail(payload);
      payload.filestack[0].thumbnail = thumbnailurl;
    }
    postv2({ card: payload }, id)
      .then(dataCard => {
        if (dataCard && dataCard.embargoErr) {
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
          this.setState({
            createLabel: this.props.card ? 'Update' : 'Create',
            clicked: false,
            pathwayLabel: 'Done',
            disableForm: false
          });
        } else {
          if (this.state.selectedBia) {
            dataCard.card.skillLevel = this.state.selectedBia;
            this.callCreatedCard(dataCard);
          } else if (
            this.state.selectedBia === '' &&
            this.props.card &&
            this.props.card.skillLevel
          ) {
            dataCard.card.skillLevel = null;
            this.callCreatedCard(dataCard);
          } else {
            this.callCreatedCard(dataCard);
          }
          let cardCreationMsg = this.cardCreationMessage(dataCard.card);
          if (cardCreationMsg) {
            this.props.dispatch(openSnackBar(cardCreationMsg, true));
          }
        }
        /*eslint handle-callback-err: "off"*/
      })
      .catch(err => {
        console.error(`Error in SmartBite.sendPostv2.postv2.func : ${err}`);
        this.props.createdCard();
      });
  }

  setCorrectAnswer = index => {
    let quizOptions = this.state.quizOptions;
    if (quizOptions[index].label.trim() !== '') {
      quizOptions.forEach(item => {
        item.isCorrect = false;
      });
      quizOptions[index].isCorrect = true;
      this.setState({ quizOptions, optionsError: '', disableSubmit: false });
    } else {
      this.setState({ optionsError: 'Please add options!' });
      return;
    }
  };

  changeBia(event, value) {
    this.setState({
      selectedBia: value
    });
  }

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(prevState => {
      return {
        showAdSettings: !prevState.showAdSettings
      };
    });
  };

  toggleVersionSettings = e => {
    e.preventDefault();
    this.setState({
      showPreviousversionsToggle: !this.state.showPreviousversionsToggle
    });
  };

  handleDelete(tagInfo) {
    let tag = typeof tagInfo === 'number' ? this.state.tags[tagInfo] : tagInfo;

    if (typeof tag === 'undefined') {
      return;
    }

    switch (tag.label) {
      case 'member':
        let newUserSuggestions = _.concat(this.state.userSuggestions, tag);
        this.setState({
          userSuggestions: newUserSuggestions
        });
        break;
      case 'channel':
        let newChannelSuggestions = _.concat(this.state.channelsSuggestions, tag);
        this.setState({
          channelsSuggestions: newChannelSuggestions
        });
        break;
      case 'group':
        let newGroupSuggestions = _.concat(this.state.groupsSuggestions, tag);
        this.setState({
          groupsSuggestions: newGroupSuggestions
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }

    let tags = this.state.tags;

    let index = _.findIndex(tags, existingTag => {
      return existingTag.id == tag.id && existingTag.label == tag.label;
    });

    tags.splice(index, 1);
    this.setState({ tags }, () => {
      if (tag.label == 'channel') {
        this.setPrivateChannelMessage();
      }
    });
  }

  handleDeleteTags(tag) {
    let topicTags = this.state.topicTags;

    let index = _.findIndex(topicTags, existingTag => {
      return existingTag.name == tag.name;
    });

    topicTags.splice(index, 1);
    this.setState({ topicTags });
  }

  handleAddition(tag, showMessage = true) {
    switch (tag.label) {
      case 'member':
        _.remove(this.state.userSuggestions, existingMember => {
          return existingMember.id + '' === tag.id + '';
        });
        break;
      case 'channel':
        _.remove(this.state.channelsSuggestions, existingChannel => {
          return existingChannel.id + '' === tag.id + '';
        });
        break;
      case 'group':
        _.remove(this.state.groupsSuggestions, existingGroup => {
          return existingGroup.id + '' === tag.id + '';
        });
        break;
      default:
        // FIXME: implement default case
        break;
    }

    let existingTags = this.state.tags;
    existingTags.push(tag);

    this.setState({ tags: existingTags }, () => {
      if (tag.label == 'channel' && showMessage) {
        this.setPrivateChannelMessage(tag);
      }
    });
  }

  setPrivateChannelMessage = tagAdded => {
    let channels = this.state.tags.filter(e => e.label == 'channel');
    if (channels.length == 0) {
      this.setState({
        showPrivateChannelMsg: false,
        privateChannelMsg: ''
      });
    } else {
      let privateChannels = channels.filter(e => e.isPrivate);
      let publicChannels = channels.filter(e => !e.isPrivate);
      let privateContent = this.state.isPrivateContent;
      if (!privateContent && privateChannels.length && publicChannels.length) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Card will remain public until explicitly marked as private.'
        });
      } else if (!privateContent && privateChannels.length) {
        let setStateObject = {};
        if (!this.state.userActionedPrivateContent) {
          setStateObject['isPrivateContent'] = true;
        }
        setStateObject['showPrivateChannelMsg'] = true;
        setStateObject['privateChannelMsg'] =
          'Card shared with private channel will need to be marked as private to control its visibility.';
        this.setState(setStateObject);
      } else if (
        privateContent &&
        privateChannels.length &&
        tagAdded &&
        !tagAdded.isPrivate &&
        publicChannels.length == 1
      ) {
        this.setState({
          showPrivateChannelMsg: true,
          privateChannelMsg: 'Visibility of card increases when shared with public channel'
        });
      } else {
        this.setState({
          showPrivateChannelMsg: false,
          privateChannelMsg: ''
        });
      }
    }
  };

  filterRestrictedGroupsOrUser = tags => {
    let restrictedToUsers = this.state.restrictedToUsers;
    let tempRestricted = this.state.restrictedToUsers;
    let finalUsersOrGroups = [];
    tags.map(tag => {
      switch (tag.label) {
        case 'channel':
          tempRestricted = restrictedToUsers.filter(restricted => {
            let restrictedChannelID = restricted.channelIds.filter(
              channelID => channelID == tag.id
            );
            restrictedChannelID = restrictedChannelID.length ? restrictedChannelID[0] : '';
            //if channel mentioned in share is present in the restrested-to user then it should be included in this array
            return restrictedChannelID == tag.id;
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'group':
          tempRestricted = restrictedToUsers.filter(restricted => {
            if (restricted.label === 'member') {
              let restrictedGroupID = restricted.teamIds.filter(teamID => teamID == tag.id);
              restrictedGroupID = restrictedGroupID.length ? restrictedGroupID[0] : '';
              //if group mentioned in share is present in the restrested-to user then it should be included in this array
              return restrictedGroupID == tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];
          break;
        case 'member':
          tempRestricted = restrictedToUsers.filter(restricted => {
            //if member mentioned in share is present in the restrested-to user then it should be included in this array
            if (restrictedToUsers.label === 'member') {
              return restricted.id != tag.id;
            }
          });
          finalUsersOrGroups = [...tempRestricted, ...finalUsersOrGroups];

          break;
        default:
          break;
      }
    });

    finalUsersOrGroups = _.uniq(finalUsersOrGroups);
    this.setState({ restrictedToUsers: finalUsersOrGroups });
  };

  selectedTopicTags(tag) {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={this.handleDeleteTags.bind(this, tag.tag)}
      >
        <Tag viewBox="0 0 30 25" style={this.styles.tagIconStyles} />
        {tag.tag.name}
      </span>
    );
  }

  handleInputChange(sourceField, query) {
    let payload = {
      limit: 15,
      offset: 0,
      q: query
    };

    let mainTags = [];

    if (sourceField === 'Restrict') {
      mainTags = this.state.restrictedToUsers;
    } else if (sourceField === 'Share') {
      mainTags = this.state.tags;
    }

    getItems(payload)
      .then(members => {
        _.map(members.items, member => {
          let suggestion = {
            name: member.name,
            id: parseInt(member.id, 10),
            label: 'member'
          };

          let index = _.findIndex(mainTags, existingTag => {
            return (
              parseInt(suggestion.id, 10) == parseInt(existingTag.id, 10) &&
              existingTag.label == 'member'
            );
          });
          if (index < 0) {
            let suggestionState = this.state.userSuggestions;
            if (sourceField === 'Restrict') {
              suggestionState = this.state.usersForRestrict;
              suggestionState.push(suggestion);
              this.setState({ usersForRestrict: suggestionState });
            } else if (sourceField === 'Share') {
              suggestionState.push(suggestion);
              this.setState({ userSuggestions: suggestionState });
            }
          }
        });
      })
      .catch(err => {
        console.error(`Error in SmartBite.handleInputChange.getItems.func : ${err}`);
      });
  }

  handlePrivateContent = event => {
    this.setState({ userActionedPrivateContent: true });
    this.setState(
      {
        isPrivateContent: event.target.checked
      },
      () => {
        this.setPrivateChannelMessage();
      }
    );
  };

  handlePaidContent = event => {
    this.setState({
      isPaid: event.target.checked
    });
  };

  handlePricingOptionChange = data => {
    if (data == 'paid') {
      this.setState({
        isPaid: true
      });
    } else {
      this.setState({
        isPaid: false
      });
    }
    this.setState({
      plan: data,
      planChange: true
    });
  };

  handleAdditionTags(tag) {
    let newTopicTags = this.state.topicTags;
    let index = _.findIndex(newTopicTags, existingTag => {
      return existingTag.name == tag.name;
    });
    if (!(index >= 0)) {
      newTopicTags.push(tag);
      this.setState({ topicTags: newTopicTags });
    }
  }

  selectedRestrictedToTags(tag) {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={this.handleUserDelete.bind(this, tag.tag)}
      >
        {tag && tag.tag && tag.tag.label === 'group' && (
          <GroupActiveIcon viewBox="0 0 45 30" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'channel' && (
          <TeamActiveIcon viewBox="0 2 50 28" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'member' && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  }

  handleCurrencySelect = currency => {
    this.setState({ currency: currency });
  };

  onAddingPrice = e => {
    this.setState({ currencyErrorText: '', priceErrorText: '' });
    if (!this.state.currency && this.state.isPaid) {
      this.setState({ currencyErrorText: 'Cant be empty if Paid content is selected' });
      return;
    }

    if (!this.state.amount && this.state.isPaid) {
      this.setState({ priceErrorText: 'Cant be empty if Paid content is selected' });
      return;
    }

    if (this.state.amount <= 0) {
      this.setState({ priceErrorText: 'Amount should be greater than 0' });
      return;
    } else if (this.state.amount >= 999999) {
      this.setState({ priceErrorText: 'Amount should be less than 999999' });
      return;
    }

    if (this.state.currency && this.state.amount) {
      let prices = this.state.prices;
      let currencyExists = _.find(prices, { currency: this.state.currency });
      if (currencyExists) {
        currencyExists.amount = this.state.amount;
        currencyExists._destroy && delete currencyExists._destroy;
      } else {
        prices.push({
          currency: this.state.currency,
          amount: this.state.amount
        });
      }

      this.setState({
        prices,
        currency: '',
        price: ''
      });
    }
  };

  removePrice(e, idx, price) {
    e && e.preventDefault();
    let prices = this.state.prices;

    if (price.id) {
      prices[idx]['_destroy'] = true;
    } else {
      prices.splice(idx, 1);
    }

    this.setState({ prices });
  }

  cleanLevel = () => {
    this.setState({ selectedBia: '' });
  };

  setAllowReanswer() {
    this.setState(prevState => ({ canBeReanswered: !prevState.canBeReanswered }));
  }

  getRestrictedErrorMsg = () => {
    let msg = '';

    if (!this.state.isPrivateContent) {
      msg = 'You must mark the card as private to be able to add restrictions';
    }

    return msg;
  };

  checkToShare = (e, item) => {
    let tags = this.state.tags;
    let index = _.findIndex(tags, tag => tag.label === item.label && +tag.id === +item.id);
    if (index > -1) {
      tags.splice(index, 1);
    } else {
      tags.push(item);
    }
    this.setState({ tags });
  };
  scrollToContentBox = () => {
    this.contentBox.scrollIntoView({ behavior: 'smooth' });
  };
  render() {
    let smartbites = this.state.smartBitesList;
    let availableCardTypes = this.props.team.readableCardTypes;
    let filteredCardTypes = availableCardTypes
      ? this.state.isFirstTextCardOfPathway == 'text'
        ? availableCardTypes.filter(el => {
            return el.type != 'Introduction';
          })
        : availableCardTypes
      : [];
    let isCurator =
      this.props.currentUser.roles && !!~this.props.currentUser.roles.indexOf('curator');
    let tagSuggestions = _.uniqWith(
      [
        ...this.state.groupsSuggestions,
        ...this.state.channelsSuggestions,
        ...this.state.userSuggestions
      ],
      _.isEqual
    );
    let restrictedToUsersTags = _.uniqWith(
      [...this.state.groupsForRestrict, ...this.state.usersForRestrict],
      _.isEqual
    );
    let _this = this;
    let isCustomFileStackType =
      this.state.fileStack &&
      this.state.fileStack[0] &&
      (this.state.fileStack[0].mimetype &&
        (!!~this.state.fileStack[0].mimetype.indexOf('video/') ||
          !!~this.state.fileStack[0].mimetype.indexOf('audio/') ||
          !!~this.state.fileStack[0].mimetype.indexOf('/pdf')));
    let paymentOptionTypes = ['free', 'paid', 'premium', 'subscription'];

    let restrictAble = this.state.isPrivateContent;
    let restrictToError = this.getRestrictedErrorMsg();
    let { duplicateTotal, duplicateCards } = this.state;
    let multipleDuplicate = duplicateTotal > 1;
    let withS = multipleDuplicate ? 's' : '';
    let withoutS = multipleDuplicate ? '' : 's';
    const useFormLabels = window.ldclient.variation('use-form-labels', false);

    return (
      <div
        ref={el => {
          this.contentBox = el;
        }}
        className="smart-bite-cards"
        style={this.state.disableForm ? this.styles.disableForm : {}}
      >
        {this.state.activeType === 'link' && (
          <div className="inputs-block">
            <TextFieldCustom
              required
              inputChangeHandler={this.messageChangeHandler}
              value={this.state.linkUrl}
              errorText={this.state.linkErrorText}
              rowsMax={2}
              hintText={useFormLabels ? tr('Link') : tr('Paste link here ...')}
              fullWidth={true}
              disabled={
                (this.props.card && this.props.card.cardType == 'course') || this.state.disableForm
              }
            />
            {this.typeOfDetectDuplicate !== 'none' && duplicateTotal ? (
              <div className="duplicate-block">
                <p
                  className={
                    this.typeOfDetectDuplicate === 'auto'
                      ? 'duplicate-error-message'
                      : 'duplicate-info-message'
                  }
                >
                  {this.typeOfDetectDuplicate === 'auto'
                    ? tr(
                        `You can not create a SmartCard with the above-mentioned URL as the following SmartCard${withS} already exist${withoutS}:`
                      )
                    : tr(
                        `The following content${withS} with the above mentioned URL already exist${withoutS}.`
                      )}
                </p>
                <div className="duplicate-links" ref={node => (this._linksBlock = node)}>
                  {duplicateCards.map(card => {
                    return (
                      <a className="duplicate-link" href={card.shareUrl}>
                        {' '}
                        {card.shareUrl}{' '}
                      </a>
                    );
                  })}
                </div>
              </div>
            ) : (
              this.typeOfDetectDuplicate === 'manual' &&
              !this.state.linkErrorText &&
              !!this.state.linkUrl && (
                <p className="duplicate-check-btn" onClick={this.checkDuplicate}>
                  {' '}
                  {tr('Check for Duplicates')}
                </p>
              )
            )}
            {this.renderLinkResource('showPreviewLink')}
            {this.state.pendingCheckDuplicate &&
              this.typeOfDetectDuplicate === 'manual' &&
              !this.state.pendingImageUpload && (
                <div className="text-center text-block">
                  <Spinner />
                </div>
              )}
            <TextFieldCustom
              hintText={
                useFormLabels
                  ? this.linkTypeDescription
                    ? 'Title'
                    : 'Title/description'
                  : this.linkTypeDescription === false
                  ? 'Add title/description (optional)'
                  : 'Add title (optional)'
              }
              value={this.state.message}
              inputChangeHandler={this.messageCardChange}
              rowsMax={4}
              fullWidth={true}
              disabled={this.state.disableForm}
            />
            {this.state.linkTypeDescription ? (
              <TextFieldCustom
                hintText={useFormLabels ? 'Description' : 'Add description (optional)'}
                rowsMax={4}
                rows={4}
                multiLine={true}
                fullWidth={true}
                inputChangeHandler={this.descriptionChangeHandler}
                errorText={this.state.longDescriptionErrorText}
                value={this.state.description}
                disabled={this.state.disableForm}
              />
            ) : (
              ''
            )}
          </div>
        )}
        {this.props.activeType === 'scorm' && (
          <div className="inputs-block">
            <FlatButton
              className="imageUpload"
              label={<div>{tr('Select a File')}</div>}
              labelStyle={this.styles.attachLabel}
              onClick={this.fileStackHandler.bind(
                this,
                ['.zip'],
                this.updateFileStackFile.bind(this, 0)
              )}
              icon=""
              style={this.styles.attachUploadFile}
              aria-label="upload file"
            />
            <div className="upload-error">
              <span className="upload-error-text">{tr(this.state.uploadErrorText)}</span>
            </div>
            {!isCustomFileStackType &&
              this.state.fileStack.map((item, index) => {
                return this.contentBlock(item, true, true, index, 'scorm');
              })}
            <TextFieldCustom
              hintText="Message (Number of Characters)"
              value={this.state.message}
              inputChangeHandler={this.messageCardChange}
              rowsMax={4}
              errorText={tr(this.state.scormErrorText)}
              fullWidth={true}
              disabled={this.state.disableForm}
            />
          </div>
        )}
        {this.state.activeType === 'upload' && (
          <div className="inputs-block">
            {this.state.cardPreview && (
              <div>
                {isCustomFileStackType && this.contentBlock(this.state.fileStack[0], false, false)}
                {!isCustomFileStackType &&
                  this.state.fileStack.map((item, index) => {
                    return this.contentBlock(item, false, false, index);
                  })}
                {!this.state.fileStack.length &&
                  this.state.linkCard.imageUrl &&
                  this.renderLinkResource('showPreviewLink')}
              </div>
            )}
            <div className="upload-error">
              <span className="upload-error-text">{this.state.uploadErrorText}</span>
            </div>
            {!(!this.state.fileStack.length && this.state.linkCard.imageUrl) && (
              <TextFieldCustom
                required
                hintText={useFormLabels ? 'Title/description' : 'Add title/description'}
                style={{ verticalAlign: 'top' }}
                value={this.state.message}
                inputChangeHandler={this.uploadCardMessageChange}
                rowsMax={4}
                fullWidth={true}
                errorText={this.state.titleError ? 'Title is required.' : null}
                disabled={this.state.disableForm}
              />
            )}
          </div>
        )}
        {(this.state.activeType === 'text' || this.state.activeType === 'project') && (
          <div className="inputs-block">
            {this.state.activeType === 'project' && (
              <TextFieldCustom
                inputChangeHandler={this.addName}
                value={this.state.projectName}
                hintText="Project Name"
                fullWidth={true}
                disabled={this.state.disableForm}
              />
            )}
            {this.state.NameErrorText && this.state.activeType === 'project' && (
              <div className="error-text">{tr(this.state.NameErrorText)}</div>
            )}
            <div className="text-block" id="editorRichText">
              <RichTextEditor
                autoFocus={false}
                value={this.state.textCard}
                toolbarConfig={toolbarConfig}
                placeholder={tr('Start writing here (Number of Characters)')}
                onChange={this.onChangeTextEditor}
              />
            </div>
            <div className="error-text">{tr(this.state.textErrorText)}</div>
            {this.imageUploadUI()}
          </div>
        )}
        {(this.state.activeType === 'smart' || this.state.activeType === 'bookmark') && (
          <CardSearch activeType={this.state.activeType} checkCard={this.checkCard} />
        )}
        {this.state.activeType === 'poll' && (
          <div className="inputs-block">
            <TextFieldCustom
              required
              inputChangeHandler={this.addQuestion}
              value={this.state.message}
              errorText={this.state.pollErrorText}
              hintText={
                useFormLabels ? 'Question/link' : 'Start typing your question or paste link here'
              }
              fullWidth={true}
              disabled={this.state.disableForm}
            />
            {this.state.pollOptions.map((option, index) => {
              if (option !== undefined) {
                return (
                  <div key={index}>
                    <TextField
                      value={option.label}
                      underlineStyle={this.styles.quizUnderline}
                      inputStyle={this.styles.quizInput}
                      hintStyle={this.styles.quizHint}
                      onChange={this.optionChangeHandler.bind(this, index, 'pollOptions')}
                      name="option"
                      hintText={`${tr('Choice')} ${index + 1}`}
                      aria-label={`${tr('Choice')} ${index + 1}`}
                    />
                    <IconButton
                      className="delete"
                      style={this.styles.closeOption}
                      aria-label={`cancel ${tr('Choice')} ${index + 1}`}
                      onTouchTap={this.removeOptionClickHandler.bind(this, index, 'pollOptions')}
                    >
                      <CloseIcon color={middlePurp} />
                      <hr className="close-line" />
                    </IconButton>
                  </div>
                );
              }
            })}
            {this.state.optionsError && (
              <div className="error-text">{tr(this.state.optionsError)}</div>
            )}
            {this.state.pollOptions.length < 5 && (
              <div className="text-block">
                <a
                  href="#"
                  className="addPollChoice add-poll"
                  style={{ color: colors.primary }}
                  onClick={e => this.addOptionClickHandler(e, 'pollOptions')}
                >
                  {tr('+ Add Choices')}
                </a>
              </div>
            )}
            {!this.state.pendingImageUpload && !this.state.linkCard.id && this.imageUploadUI()}
            {(this.state.pendingImageUpload || this.state.linkCard.id) &&
              this.renderLinkResource('showPreviewLink')}
          </div>
        )}
        {this.state.activeType === 'quiz' && (
          <div className="inputs-block">
            <TextFieldCustom
              required
              inputChangeHandler={this.addQuiz}
              value={this.state.message}
              errorText={this.state.pollErrorText}
              hintText={useFormLabels ? 'Question' : 'Ask a question'}
              fullWidth={true}
              disabled={this.state.disableForm}
            />
            <div className="correct-label">{tr('Correct')}</div>
            {this.state.quizOptions.map((option, index) => {
              if (option !== undefined) {
                return (
                  <div key={index} className="quiz-item">
                    <Checkbox
                      style={this.styles.checkbox}
                      checked={option.isCorrect}
                      onCheck={this.setCorrectAnswer.bind(this, index)}
                      aria-label="select answer"
                    />
                    <TextField
                      value={option.label}
                      underlineStyle={this.styles.quizUnderline}
                      inputStyle={this.styles.quizInput}
                      hintStyle={this.styles.quizHint}
                      onChange={this.optionChangeHandler.bind(this, index, 'quizOptions')}
                      name="option"
                      hintText={`${tr('Answer')} ${index + 1}`}
                      aria-label={`${tr('Answer')} ${index + 1}`}
                    />
                    <IconButton
                      className="delete"
                      style={this.styles.closeOption}
                      onTouchTap={this.removeOptionClickHandler.bind(this, index, 'quizOptions')}
                      aria-label={`cancel ${tr('Answer')} ${index + 1}`}
                    >
                      <CloseIcon color={middlePurp} />
                      <hr className="close-line-quiz" />
                    </IconButton>
                  </div>
                );
              }
            })}
            {this.state.optionsError && (
              <div className="error-text">{tr(this.state.optionsError)}</div>
            )}
            {this.state.quizOptions.length < 30 && (
              <div className="text-block">
                <a
                  href="#"
                  className="addQuizChoice add-poll"
                  style={{ color: colors.primary }}
                  onClick={e => this.addOptionClickHandler(e, 'quizOptions')}
                >
                  {tr('+ Add Answer')}
                </a>
              </div>
            )}
            <div className="text-block">
              <Checkbox
                style={this.styles.allowCheckbox}
                labelStyle={this.styles.allowCheckboxLabel}
                checked={this.state.canBeReanswered}
                onCheck={this.setAllowReanswer.bind(this)}
                label={tr('Allow the user to answer the Quiz again')}
                aria-label="Allow the user to answer the Quiz again"
              />
            </div>
            {this.state.cardPreview && this.isAbleUpload && (
              <div>
                {isCustomFileStackType && this.contentBlock(this.state.fileStack[0], false, false)}
                {!isCustomFileStackType &&
                  this.state.fileStack.map((item, index) => {
                    return this.contentBlock(item, true, false, index);
                  })}
              </div>
            )}
            {!this.state.cardPreview && this.isAbleUpload && (
              <div className="text-block">
                <FlatButton
                  className="imageUpload"
                  label={<div>{tr('Attach')}</div>}
                  labelStyle={this.styles.attachLabel}
                  onClick={this.fileStackHandler.bind(this, fullAccept, this.addFileStackFiles)}
                  icon={
                    <AttachIcon
                      style={this.styles.attachIcon}
                      color="#bababa"
                      viewBox="0 0 24 24"
                    />
                  }
                  style={this.styles.attachUpload}
                  aria-label="attach file"
                />
              </div>
            )}
            {!this.isAbleUpload && this.isAbleUploadCover && this.imageUploadUI()}
          </div>
        )}
        {this.showBIA && (
          <div className="bia-block">
            <RadioButtonGroup
              name="bia-radio"
              style={{ display: 'flex' }}
              valueSelected={this.state.selectedBia}
              onChange={this.changeBia.bind(this)}
            >
              <RadioButton
                value="beginner"
                label={tr('Beginner')}
                aria-label={tr('Beginner')}
                className="bia-radio"
                style={{ width: 'auto' }}
                tabIndex="1"
                iconStyle={
                  this.state.selectedBia != 'beginner'
                    ? this.styles.biaRadio
                    : { marginRight: '5px' }
                }
              />
              <RadioButton
                value="intermediate"
                label={tr('Intermediate')}
                aria-label={tr('Intermediate')}
                className="bia-radio"
                style={{ width: 'auto' }}
                tabIndex="2"
                iconStyle={
                  this.state.selectedBia != 'intermediate'
                    ? this.styles.biaRadio
                    : { marginRight: '5px' }
                }
              />
              <RadioButton
                value="advanced"
                label={tr('Advanced')}
                aria-label={tr('Advanced')}
                className="bia-radio"
                style={{ width: 'auto' }}
                tabIndex="3"
                iconStyle={
                  this.state.selectedBia != 'advanced'
                    ? this.styles.biaRadio
                    : { marginRight: '5px' }
                }
              />
            </RadioButtonGroup>
            {this.state.selectedBia && (
              <button className="my-button" onClick={this.cleanLevel}>
                {tr('Clear level')}
              </button>
            )}
          </div>
        )}
        {this.state.shareOnSmartCardCreation &&
          this.state.isAbleToMarkPrivate &&
          this.state.activeType !== 'smart' &&
          this.state.activeType !== 'bookmark' && (
            <div className="common-block smartbite-inputs-container feild-space">
              {this.props.cardType !== 'pathway' &&
                (this.props.card ? this.props.card && this.props.card.hidden == false : true) && (
                  <CardSharingSection
                    tags={this.state.tags}
                    tagSuggestions={tagSuggestions}
                    handleAddition={this.handleAddition.bind(this)}
                    handleDelete={this.handleDelete.bind(this)}
                    handleInputChange={this.handleInputChange.bind(this, 'Share')}
                    pathwayPart={this.props.isPathwayPart}
                    styles={this.styles}
                    showPrivateChannelMsg={this.state.showPrivateChannelMsg}
                    privateChannelMsg={this.state.privateChannelMsg}
                    groupsSuggestions={this.state.groupsSuggestions}
                    channelsSuggestions={this.state.channelsSuggestions}
                    checkToShare={this.checkToShare}
                  />
                )}
              {!this.props.isPathwayPart && (
                <Checkbox
                  style={this.styles.privateContent}
                  checked={this.state.isPrivateContent}
                  onCheck={this.handlePrivateContent.bind(this)}
                  label={tr('Private content')}
                  aria-label={tr('Private content')}
                />
              )}
              <div style={{ margin: '10px 0px 0px 0px' }}>
                {useFormLabels && <label htmlFor="form-add-tags">{tr('Tags')}</label>}

                <ReactTags
                  id="form-add-tags"
                  tags={this.state.topicTags}
                  placeholder={
                    useFormLabels
                      ? ''
                      : !!this.state.topicTags.length > 0
                      ? ''
                      : tr('Enter desired tag')
                  }
                  allowNew={true}
                  handleAddition={this.handleAdditionTags.bind(this)}
                  tagComponent={this.selectedTopicTags.bind(this)}
                  handleDelete={this.handleDeleteTags.bind(this)}
                  autofocus={false}
                />
                <p className="tag-info">{tr('Press Return (Enter) to add the tag.')}</p>
              </div>
            </div>
          )}
        {(!this.state.shareOnSmartCardCreation || !this.state.isAbleToMarkPrivate) &&
          this.state.activeType !== 'smart' &&
          this.state.activeType !== 'bookmark' && (
            <div
              className={
                this.showBIA
                  ? 'feild-space common-block smartbite-inputs-container'
                  : 'common-block smartbite-inputs-container'
              }
            >
              <SmartBiteDropdown
                hintText={tr('Post to channel')}
                cardType={this.props.cardType}
                onChange={this.getNewChannels}
                fullWidth={false}
                cardChannel={(this.props.card && this.props.card.channels) || []}
                currentChannels={this.state.selectedChannels}
                nonCuratedChannelIds={this.state.nonCuratedChannelIds}
              />

              <div style={this.styles.privateChannelStyle}>
                {this.state.showPostToChannelMsg && (
                  <div style={{ display: 'flex' }}>
                    <ActionInfo style={this.styles.privateChannelIconStyle} />
                    <p
                      style={{
                        verticalAlign: 'middle',
                        margin: '0 0 0 5px'
                      }}
                    >
                      {tr('Posting to private channel will not make card private.')}
                    </p>
                  </div>
                )}
              </div>

              {!!this.state.topics.length && (
                <div className="text-block chip-tag-pathway">
                  <div style={this.styles.chipsWrapper}>
                    {this.state.topics.map((name, idx) => {
                      if (this.props.cardType === 'smartbite') {
                        return (
                          <div className="chip-tag-pathway-item">{this.chipRender(name, idx)}</div>
                        );
                      } else {
                        return this.chipRender(name, idx);
                      }
                    })}
                  </div>
                </div>
              )}
              <TextFieldCustom
                hintText={tr('Enter desired tag')}
                clearDown={!this.state.maxTagErrorText.length}
                onKeyDown={!this.state.maxTagErrorText.length && this.addTagHandler}
                inputChangeHandler={this.changeTag}
                fullWidth={false}
                disabled={this.state.disableForm}
              />
              <p className="tag-info-guidance">{tr('Press Return (Enter) to add the tag.')}</p>
            </div>
          )}
        {this.state.maxTagErrorText && (
          <div className="error-text"> {tr(this.state.maxTagErrorText)}</div>
        )}

        {this.state.setCardTypeMandate &&
          this.state.activeType !== 'poll' &&
          this.state.activeType !== 'quiz' &&
          !!filteredCardTypes.length && (
            <div className="row advanced-settings" style={{ margin: '0px 0' }}>
              <div className="small-12 medium-2 large-3 align-label">
                <div className="smartbite-label">{tr('Card Type')}</div>
              </div>
              <div className="small-12 medium-8 large-8">
                <SelectField
                  style={this.styles.priceDropDown}
                  hintText={tr('Select Card Type')}
                  maxHeight={170}
                  value={this.state.currentReadableCardType}
                  underlineStyle={{ display: 'none' }}
                  labelStyle={this.styles.labelStyleSelect}
                  iconStyle={this.styles.iconSelect}
                  menuItemStyle={this.styles.menuItemStyle}
                  aria-label={tr('Select Card Type')}
                >
                  {filteredCardTypes.sort().map((obj, index) => {
                    return (
                      <MenuItem
                        key={index}
                        value={_.toLower(obj)}
                        innerDivStyle={{ padding: '0 8px' }}
                        primaryText={tr(_.startCase(_.toLower(obj)))}
                        onTouchTap={this.handleCardTypeChange.bind(this, _.toLower(obj))}
                      />
                    );
                  })}
                </SelectField>
                {this.state.cardTypeNotSetError && (
                  <div className="error-text">Please add Card Type</div>
                )}
              </div>
            </div>
          )}

        <div className="advanced-settings">
          <a href="#" className="advanced-label" onClick={this.toggleAdvancedSettings}>
            {tr('Advanced Settings')}
          </a>
          {this.state.showAdSettings && (
            <div>
              {this.state.multilingualContent && (
                <MultilingualContent
                  commonStyle={this.styles.priceDropDown}
                  labelStyle={this.styles.labelStyleSelect}
                  iconStyle={this.styles.iconSelect}
                  menuItemStyle={this.styles.menuItemStyle}
                  handleLanguageChange={this.handleLanguageChange}
                  language={this.state.language}
                />
              )}
              {this.props.cardType !== 'pathway' &&
                (this.props.card ? this.props.card && this.props.card.hidden == false : true) &&
                this.restrictToFlag &&
                this.state.shareOnSmartCardCreation &&
                this.state.isAbleToMarkPrivate && (
                  <div className="row">
                    <div className="small-12 medium-2 large-3 align-label">
                      <div className="smartbite-label">{tr('Restricted to')}</div>
                    </div>

                    {!restrictAble && (
                      <div className="small-12 medium-8 large-8">
                        <TextFieldCustom
                          disabled={true}
                          hintText="Restrict content with @user, @group"
                          errorText={restrictToError}
                          errorStyle={this.styles.restrictToErrorStyle}
                          fullWidth={true}
                        />
                      </div>
                    )}
                    {restrictAble && (
                      <div className="small-12 medium-8 large-8">
                        <ReactTags
                          tags={this.state.restrictedToUsers}
                          suggestions={restrictedToUsersTags}
                          handleAddition={this.handleUserAddition.bind(this)}
                          handleDelete={this.handleUserDelete.bind(this)}
                          placeholder={
                            !!this.state.restrictedToUsers.length > 0
                              ? ''
                              : tr('Restrict content with @user, @group')
                          }
                          tagComponent={this.selectedRestrictedToTags.bind(this)}
                          handleInputChange={this.handleInputChange.bind(this, 'Restrict')}
                          autofocus={false}
                        />
                      </div>
                    )}
                  </div>
                )}
              {isCurator && (
                <div className="row">
                  <div className="small-12 medium-2 large-3 align-label">
                    <div className="smartbite-label">{tr('Provider')}</div>
                  </div>
                  <div className="small-12 medium-8 large-8 flex">
                    <TextFieldCustom
                      hintText="Enter Provider Name"
                      inputChangeHandler={this.advSettingChange.bind(this, 'provider')}
                      fullWidth={false}
                      value={this.state.advSettings.provider}
                      disabled={this.state.disableForm}
                    />
                    {this.isAbleUploadCover && (
                      <FlatButton
                        className="provider-image"
                        label={tr('Image')}
                        onClick={this.fileStackHandler.bind(
                          this,
                          ['image/*'],
                          this.uploadProviderImage
                        )}
                        style={this.styles.image}
                        labelStyle={this.styles.btnLabel}
                        aria-label={tr('upload Image')}
                      />
                    )}
                    {!!this.state.fileStackProvider && (
                      <div style={this.styles.providerImageContainer}>
                        <div className="close close-button provider-image-btn">
                          <IconButton
                            className="editor-image-btn-icon"
                            aria-label="clear"
                            style={this.styles.deleteProviderImageBtn}
                            iconStyle={this.styles.deleteProviderImageBtnIcon}
                            onTouchTap={this.clearFileStackProviderInfo}
                          >
                            <span tabIndex={-1} className="hideOutline">
                              <CloseIcon
                                style={this.styles.deleteProviderImageBtnIcon}
                                color="white"
                                viewBox="0 0 24 24"
                              />
                            </span>
                          </IconButton>
                        </div>
                        <img
                          className="card-view-icon"
                          style={this.styles.providerIcon}
                          src={
                            this.state.securedFileStackProviderUrl || this.state.fileStackProvider
                          }
                          alt=""
                        />
                      </div>
                    )}
                  </div>
                </div>
              )}
              <div className="row">
                <div className="small-12 medium-2 large-3 align-label">
                  <div className="smartbite-label">{tr('Duration')}</div>
                </div>
                <div className="small-12 medium-8 large-8">
                  <TextFieldCustom
                    hintText={
                      this.state.durationString
                        ? this.state.durationString
                        : 'Enter time (MONTHS: DAYS : HRS : MINS)'
                    }
                    inputChangeHandler={this.advSettingChange.bind(this, 'duration')}
                    fullWidth={false}
                    pipe={'mm dd HH MM'}
                    mask={[/\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                    value={this.state.durationString}
                    keepCharPositions={true}
                    disabled={this.state.disableForm}
                  />
                </div>
              </div>
              {!this.state.setCardTypeMandate &&
                this.state.activeType !== 'poll' &&
                this.state.activeType !== 'quiz' &&
                !!filteredCardTypes.length && (
                  <div className="row">
                    <div className="small-12 medium-2 large-3 align-label">
                      <div className="smartbite-label">{tr('Card Type')}</div>
                    </div>
                    <div className="small-12 medium-8 large-8">
                      <SelectField
                        style={this.styles.priceDropDown}
                        hintText={tr('Select Card Type')}
                        maxHeight={170}
                        value={this.state.currentReadableCardType}
                        underlineStyle={{ display: 'none' }}
                        labelStyle={this.styles.labelStyleSelect}
                        iconStyle={this.styles.iconSelect}
                        menuItemStyle={this.styles.menuItemStyle}
                        aria-label={tr('Select Card Type')}
                      >
                        {filteredCardTypes.sort().map((obj, index) => {
                          return (
                            <MenuItem
                              key={index}
                              value={_.toLower(obj)}
                              innerDivStyle={{ padding: '0 8px' }}
                              primaryText={tr(_.startCase(_.toLower(obj)))}
                              onTouchTap={this.handleCardTypeChange.bind(this, _.toLower(obj))}
                            />
                          );
                        })}
                      </SelectField>
                    </div>
                  </div>
                )}
              {this.state.edcastPricing &&
                this.state.activeType !== 'scorm' &&
                this.state.currentReadableCardType != 'jobs' &&
                !!paymentOptionTypes.length && (
                  <div>
                    <div>
                      {this.state.edcastPlansForPricing && (
                        <div className="row">
                          <div className="small-12 medium-2 large-3 align-label">
                            <div className="smartbite-label">{tr('Add Pricing Info')}</div>
                          </div>
                          <div className="small-12 medium-8 large-8">
                            <SelectField
                              style={this.styles.priceDropDown}
                              hintText={tr('Select')}
                              maxHeight={110}
                              value={this.state.plan}
                              underlineStyle={{ display: 'none' }}
                              labelStyle={this.styles.labelStyleSelect}
                              iconStyle={this.styles.iconSelect}
                              menuItemStyle={this.styles.menuItemStyle}
                              aria-label={tr('Select price info')}
                            >
                              {paymentOptionTypes.sort().map((payment, index) => {
                                return (
                                  <MenuItem
                                    key={index}
                                    value={payment}
                                    innerDivStyle={{ padding: '0 8px' }}
                                    primaryText={tr(_.startCase(_.toLower(payment)))}
                                    onTouchTap={this.handlePricingOptionChange.bind(this, payment)}
                                  />
                                );
                              })}
                            </SelectField>
                          </div>
                        </div>
                      )}
                      {!this.state.edcastPlansForPricing && (
                        <Checkbox
                          style={this.styles.privateContent}
                          checked={this.state.isPaid}
                          onCheck={this.handlePaidContent.bind(this)}
                          label="Paid Content"
                          aria-label="Paid Content"
                        />
                      )}
                    </div>
                    {this.state.isPaid && (
                      <div>
                        <div className="row">
                          <div className="small-12">
                            <Table selectable={false}>
                              <TableBody
                                displayRowCheckbox={false}
                                style={{ backgroundColor: colors.white }}
                              >
                                {this.state.prices.map((price, index) => {
                                  if (price._destroy !== true) {
                                    return (
                                      <TableRow key={index} displayBorder={true}>
                                        <TableRowColumn>{price.currency}</TableRowColumn>
                                        <TableRowColumn>{price.amount}</TableRowColumn>
                                        <TableRowColumn>
                                          <a
                                            href="#"
                                            style={_this.styles.cancelButtonAnchorTag}
                                            onClick={e => _this.removePrice(e, index, price)}
                                          >
                                            x
                                          </a>
                                        </TableRowColumn>
                                      </TableRow>
                                    );
                                  }
                                })}
                              </TableBody>
                            </Table>
                          </div>
                        </div>
                        <div className="row">
                          <div className="small-4">
                            <SelectField
                              style={{ verticalAlign: 'top', fontSize: '12px', width: '90%' }}
                              hintText={tr('Select Currency')}
                              value={this.state.currency}
                              errorText={this.state.currencyErrorText}
                              aria-label={tr('Select currency')}
                            >
                              <MenuItem
                                value={'USD'}
                                className="menu-item-custom postToChannel"
                                style={{ width: '256px' }}
                                primaryText={'USD'}
                                onTouchTap={this.handleCurrencySelect.bind(this, 'USD')}
                              />
                              <MenuItem
                                value={'INR'}
                                className="menu-item-custom postToChannel"
                                style={{ width: '256px' }}
                                primaryText={'INR'}
                                onTouchTap={this.handleCurrencySelect.bind(this, 'INR')}
                              />
                              <MenuItem
                                value={'SKILLCOIN'}
                                className="menu-item-custom postToChannel"
                                style={{ width: '256px' }}
                                primaryText={'SKILLCOIN'}
                                onTouchTap={this.handleCurrencySelect.bind(this, 'SKILLCOIN')}
                              />
                            </SelectField>
                          </div>
                          <div className="small-5">
                            <TextField
                              value={this.state.price}
                              onChange={e => {
                                let val = e.target.value.replace(
                                  /[.a-z-A-Z\s`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/g,
                                  ''
                                );
                                let a;
                                let fixedVal;
                                let splitAry = val.split('.');
                                if (splitAry.length < 2) {
                                  a = val;
                                } else {
                                  splitAry.map((n, i) => {
                                    if (i == 0) {
                                      a = n + '.';
                                    } else {
                                      a += n;
                                    }
                                  });

                                  fixedVal = splitAry[1].length > 2 ? 2 : splitAry[1].length;
                                  a = (fixedVal ? Number(a).toFixed(fixedVal) : a).toString();
                                }

                                this.setState({
                                  amount: a,
                                  price: a
                                });
                                return;
                              }}
                              labelStyle={this.styles.btnLabel}
                              errorText={this.state.priceErrorText}
                              hintText="Please type amount & press Add"
                              hintStyle={{ fontSize: '12px' }}
                              aria-label="Please type amount & press Add"
                              rowsMax={4}
                              fullWidth={true}
                            />
                          </div>
                          <div className="small-2 ">
                            <SecondaryButton
                              label={'Add'}
                              className="create"
                              onTouchTap={this.onAddingPrice}
                              labelStyle={this.styles.btnLabel}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                )}
            </div>
          )}
        </div>

        {this.state.showVersions && this.state.previousVersions.length > 0 && (
          <div className="advanced-settings">
            <a href="#" className="advanced-label" onClick={this.toggleVersionSettings}>
              {tr('Previous Versions')}
            </a>
            {this.state.showPreviousversionsToggle && (
              <SmartCardVersions versions={this.state.previousVersions} />
            )}
          </div>
        )}

        {this.props.cardType === 'pathway' && (
          <div className="action-buttons">
            <div className="text-right">
              <FlatButton
                label={tr(this.state.pathwayLabel)}
                className="create"
                disabled={this.state.disableSubmit || this.state.clicked}
                onTouchTap={this.createClickHandler.bind(
                  this,
                  this.props.card ? this.props.card.id : null,
                  false
                )}
                labelStyle={this.styles.btnLabel}
                style={this.state.disableSubmit ? this.styles.disabled : this.styles.create}
              />
            </div>
          </div>
        )}
        {this.props.cardType === 'smartbite' && (
          <div className="action-buttons">
            <FlatButton
              label={tr('Cancel')}
              className="close"
              disabled={this.state.clicked}
              style={this.styles.cancel}
              labelStyle={this.styles.btnLabel}
              onTouchTap={this.closeCard.bind(this)}
            />
            <FlatButton
              label={tr(this.state.createLabel)}
              className="create"
              disabled={
                this.state.disableSubmit ||
                this.state.clicked ||
                !!this.state.pollErrorText ||
                (this.state.activeType === 'link' &&
                  (this.state.linkErrorText ||
                    !this.state.linkUrl ||
                    (this.typeOfDetectDuplicate === 'auto' && duplicateTotal)))
              }
              onTouchTap={this.createClickHandler.bind(
                this,
                this.props.card ? this.props.card.id : null,
                false
              )}
              labelStyle={this.styles.btnLabel}
              style={
                this.state.disableSubmit ||
                !!this.state.pollErrorText ||
                (this.state.activeType === 'link' &&
                  (this.state.linkErrorText ||
                    !this.state.linkUrl ||
                    (this.typeOfDetectDuplicate === 'auto' && duplicateTotal)))
                  ? this.styles.disabled
                  : this.styles.create
              }
            />
          </div>
        )}
      </div>
    );
  }
}

Smartbite.propTypes = {
  card: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  tempCard: PropTypes.object,
  pathname: PropTypes.string,
  cardType: PropTypes.string,
  activeType: PropTypes.string,
  searchText: PropTypes.string,
  cardUpdated: PropTypes.func,
  createdCard: PropTypes.func,
  titleblank: PropTypes.bool,
  closeClick: PropTypes.bool,
  isPathwayPart: PropTypes.bool,
  saveOutClick: PropTypes.func,
  channelsV2: PropTypes.object,
  uploadIteration: PropTypes.number
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(Smartbite);
