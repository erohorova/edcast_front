import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Paper from 'edc-web-sdk/components/Paper';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import DatePicker from 'material-ui/DatePicker';
import Toggle from 'material-ui/Toggle';
import uniq from 'lodash/uniq';
import Spinner from '../common/spinner';
import AutoComplete from 'material-ui/AutoComplete';
import Avatar from 'material-ui/Avatar';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { getList } from 'edc-web-sdk/requests/groups.v2';
import { getItems, searchUsers } from 'edc-web-sdk/requests/users.v2';
import { assignTo } from 'edc-web-sdk/requests/assignments';
import { tr } from 'edc-web-sdk/helpers/translations';
import MaskedInput from 'react-text-mask';
import MarkdownRenderer from '../common/MarkdownRenderer';
import { getGroupsForOrg } from 'edc-web-sdk/requests/teams';
import IconButton from 'material-ui/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import moment from 'moment';
import TextField from 'material-ui/TextField';
import { Calendar } from 'react-date-range';
import FlatButton from 'material-ui/FlatButton';
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
//actions
import { close } from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { markAssignmentCountToBeUpdated } from '../../actions/assignmentsActions';
import { Permissions } from '../../utils/checkPermissions';

const MILLISECONDS_IN_MINUTE = 60000;

class AssignModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedGroupIds: [],
      dueDate: undefined,
      startDate: undefined,
      groups: {},
      toggleDueDatePicker: false,
      toggleStartDatePicker: false,
      loading: true,
      view: this.props.selfAssign ? '' : 'groups',
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      message: '',
      selectedMemberIds: [],
      selectedExcludedMemberIds: [],
      members: {},
      dataSource: [],
      errorText: '',
      excludeErrorText: '',
      confirm: false,
      pending: false,
      excludeMembersData: [],
      dueDateError: '',
      startDateError: '',
      startDateIsOpen: false,
      dueDateIsOpen: false,
      dueDatePlaceholder: true,
      startDatePlaceholder: true
    };
    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.assignClickHandler = this.assignClickHandler.bind(this);
    this.confirmClickHandler = this.confirmClickHandler.bind(this);
    this.handleSelectGroup = this.handleSelectGroup.bind(this);
    this.handleChipDelete = this.handleChipDelete.bind(this);
    this.disablePastDates = this.disablePastDates.bind(this);
    this.toggleDatePicker = this.toggleDatePicker.bind(this);
    this.selectDate = this.selectDate.bind(this);
    this.selectMember = this.selectMember.bind(this);
    this.changeView = this.changeView.bind(this);
    this.handleUpdateInput = this.handleUpdateInput.bind(this);

    this.styles = {
      message: {
        width: '100%',
        resize: 'none',
        border: `1px solid ${colors.lightGray}`,
        padding: '10px'
      },
      calendar: {
        backgroundColor: '#fff',
        position: 'absolute',
        zIndex: '1000',
        boxShadow: '0 0.125rem 0.25rem 0 rgba(0,0,0,.5)'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`
      },
      selectedLabel: {
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        paddingRight: '20px'
      },
      chip: {
        margin: '4px'
      },
      toggle: {
        maxWidth: '16rem'
      },
      toggleLabel: {
        fontSize: '1rem'
      },
      arrowStyle: {
        fill: `${colors.primary}`
      },
      errorStyle: {
        fontSize: '0.718rem',
        lineHeight: '0.5rem'
      },
      selectContainer: {
        width: '19.375rem',
        zIndex: '10',
        background: 'rgba(1, 1, 1, 0)',
        marginTop: '1.9rem',
        marginLeft: '2.5rem'
      },
      selectContent: {
        width: '100%',
        padding: 0
      },
      hide: {
        border: 'none',
        outline: 'none'
      }
    };
    this.excludedGroupMembersFlag = !!(
      this.props.team &&
      this.props.team.config &&
      this.props.team.config.enable_exclude_group_members
    );
    this.isSafari =
      /constructor/i.test(window.HTMLElement) ||
      (function(p) {
        return p.toString() === '[object SafariRemoteNotification]';
      })(!window['safari'] || window['safari'].pushNotification);
    this.isIE = /*@cc_on!@*/ false || !!document.documentMode;
    this.isEdge = !this.isIE && !!window.StyleMedia;
  }

  componentDidMount() {
    let cardType = this.props.card.cardType == 'VideoStream' ? 'VideoStream' : 'card',
      hashGroups;
    if (!this.props.selfAssign) {
      if (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) {
        getGroupsForOrg({ limit: 1000 })
          .then(groups => {
            hashGroups = groups.reduce((acc, curr) => {
              acc[curr['id']] = curr;
              return acc;
            }, {});
            this.setState({ groups: hashGroups, loading: false });
          })
          .catch(err => {
            console.error(`Error in AssignModal.getGroupsForOrg.func: ${err}`);
          });
      } else {
        getList(1000, 0, 'admin', this.props.card.id, cardType)
          .then(groups => {
            hashGroups = groups.reduce((acc, curr) => {
              acc[curr['id']] = curr;
              return acc;
            }, {});
            this.setState({ groups: hashGroups, loading: false });
          })
          .catch(err => {
            console.error(`Error in AssignModal.getList.func: ${err}`);
          });
      }
    }
    setTimeout(() => {
      this.refs['setFocus'].focus();
    }, 500);
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  assignClickHandler() {
    this.setState({ confirm: true });
  }

  confirmClickHandler() {
    // let cardType = (this.props.card.cardType == 'VideoStream' ? 'VideoStream' : 'Card');
    let cardType = 'Card';
    let cardId =
      this.props.card.cardType == 'VideoStream' ? this.props.card.cardId : this.props.card.id;
    let dueDate =
      this.state.toggleDueDatePicker &&
      this.state.dueDate &&
      moment(this.state.dueDate).format('MM/DD/YYYY');
    let startDate =
      this.state.toggleStartDatePicker &&
      this.state.startDate &&
      moment(this.state.startDate).format('MM/DD/YYYY');

    this.setState({ pending: true });

    if (this.props.selfAssign) {
      assignTo(
        cardId,
        cardType,
        'individual',
        undefined,
        [this.props.currentUser.id],
        dueDate,
        this.state.message,
        this.props.selfAssign,
        startDate
      )
        .then(response => {
          if (response) {
            this.setState({ pending: false });
            this.props.dispatch(close());
            this.props.dispatch(markAssignmentCountToBeUpdated());
            let typeOfCard = 'SmartCard';
            if (this.props.card.cardType === 'pack') {
              typeOfCard = 'Pathway';
            }
            if (this.props.card.cardType === 'journey') {
              typeOfCard = 'Journey';
            }
            this.props.dispatch(
              openSnackBar(
                this.state.newModalAndToast
                  ? `You have assigned this ${typeOfCard} to yourself.`
                  : `Assigned to you.`,
                true
              )
            );
            this.props.assignedStateChange();
          }
        })
        .catch(err => {
          console.error(`Error in AssignModal.assignTo.func individual: ${err}`);
        });
    }
    if (this.state.view === 'groups' && !this.props.selfAssign) {
      let selectedExcludedMemberIds =
        this.state.selectedExcludedMemberIds.length > 0
          ? this.state.selectedExcludedMemberIds.map(member => {
              return member[0];
            })
          : [];
      selectedExcludedMemberIds =
        this.state.selectedGroupIds.length > 0 ? selectedExcludedMemberIds : [];
      assignTo(
        cardId,
        cardType,
        'group',
        this.state.selectedGroupIds,
        undefined,
        dueDate,
        this.state.message,
        true,
        startDate,
        selectedExcludedMemberIds
      )
        .then(response => {
          if (response) {
            this.setState({ pending: false });
            this.props.dispatch(close());
            this.props.dispatch(markAssignmentCountToBeUpdated());
            this.props.dispatch(
              openSnackBar(`Assigned to ${this.state.selectedGroupIds.length} group(s).`, true)
            );
          }
        })
        .catch(err => {
          console.error(`Error in AssignModal.assignTo.func group: ${err}`);
        });
    } else if (this.state.view === 'individuals' && !this.props.selfAssign) {
      let individualIds = this.state.selectedMemberIds.map(member => {
        return member[0];
      });
      assignTo(
        cardId,
        cardType,
        'individual',
        undefined,
        individualIds,
        dueDate,
        this.state.message,
        this.props.selfAssign,
        startDate
      )
        .then(response => {
          if (response) {
            this.setState({ pending: false });
            this.props.dispatch(close());
            this.props.dispatch(markAssignmentCountToBeUpdated());
            let card_type = this.props.card.cardType;
            let successMessage = `Assigned to ${individualIds.length} individual(s).`;
            if (this.state.newModalAndToast && individualIds.length) {
              let type = 'SmartCard';
              if (card_type === 'pack') {
                type = 'Pathway';
              }
              if (card_type === 'journey') {
                type = 'Journey';
              }
              let first_user_name = this.state.selectedMemberIds[0][1];
              successMessage = `You have assigned this ${type} to ${first_user_name}`;
              if (individualIds.length > 1) {
                let other_users_count = individualIds.length - 1;
                let plural_users_label = individualIds.length > 2 ? 's' : '';
                successMessage += ` and ${other_users_count} other${plural_users_label}`;
              }
            }
            this.props.dispatch(openSnackBar(successMessage, true));
          }
        })
        .catch(err => {
          console.error(`Error in AssignModal.assignTo.func individual and not selfAssign: ${err}`);
        });
    }
  }

  handleSelectGroup(groupId) {
    if (groupId === 'add-all') {
      this.setState({ selectedGroupIds: Object.keys(this.state.groups) });
    } else {
      this.setState({ selectedGroupIds: this.state.selectedGroupIds.concat(groupId) });
    }
  }

  handleChipDelete(type, chipId) {
    if (type === 'group') {
      let newGroupArray = this.state.selectedGroupIds.filter(function(id) {
        if (id !== chipId) {
          return id;
        }
      });
      this.setState({ selectedGroupIds: newGroupArray });
      this.filterExcludedMembers(newGroupArray);
    } else {
      let newMemberArray = this.state.selectedMemberIds.filter(member => {
        if (member[0] !== chipId) {
          return member;
        }
      });
      this.setState({ selectedMemberIds: newMemberArray });
    }
  }

  handleExceptionChipDelete = chipId => {
    let newMemberArray = this.state.selectedExcludedMemberIds.filter(member => {
      if (member[0] !== chipId) {
        return member;
      }
    });
    this.setState({ selectedExcludedMemberIds: newMemberArray });
  };

  filterExcludedMembers = newSelectedGroupIds => {
    let memberArray = [];
    newSelectedGroupIds.map(group => {
      this.state.selectedExcludedMemberIds.map(member => {
        if (member[2].some(userGroup => userGroup.teamId == group)) {
          memberArray.push(member);
        }
      });
    });
    memberArray = uniq(memberArray);
    this.setState({ selectedExcludedMemberIds: memberArray });
  };

  disablePastDates(date) {
    if (date.toLocaleDateString() === new Date().toLocaleDateString()) {
      return false;
    } else {
      return date < new Date();
    }
  }

  checkDate = (x, y, err, isStart) => {
    return x && parseInt(x) > 4000
      ? 'The maximum value of date: 01.01.4000'
      : x &&
        moment(new Date())
          .startOf('day')
          .isAfter(x)
      ? `The minimum value of '${isStart ? 'Start' : 'Due'} Date': ${moment(new Date()).format(
          'MM/DD/YYYY'
        )}`
      : x && y && moment(isStart ? x : y).isAfter(isStart ? y : x) && !err
      ? `The ${isStart ? 'maximum' : 'minimum'} value of '${
          isStart ? 'Start' : 'Due'
        } Date': ${moment(y).format('MM/DD/YYYY')}`
      : '';
  };
  selectDate(param, e, date) {
    let dueDateError = this.checkDate(
      ...(param === 'startDate'
        ? [this.state.dueDate, date, this.state.dueDateError]
        : [date, this.state.startDate, this.state.startDateError, false]),
      false
    );
    let startDateError = this.checkDate(
      ...(param === 'startDate'
        ? [date, this.state.dueDate, this.state.dueDateError]
        : [this.state.startDate, date, dueDateError]),
      true
    );
    let errorObj = { startDateError, dueDateError };
    this.setState(prevState => ({
      [`${param}IsOpen`]: !prevState[`${param}IsOpen`],
      [param]: !date ? undefined : date,
      ...errorObj
    }));
  }

  selectMember(member, index) {
    if (this.state.members.items && this.state.members.items[index]) {
      if (
        this.state.selectedMemberIds.every(memberItem => {
          return memberItem[0] !== this.state.members.items[index].id;
        })
      ) {
        this.setState({
          selectedMemberIds: this.state.selectedMemberIds.concat([
            [this.state.members.items[index].id, this.state.members.items[index].name]
          ])
        });
      } else {
        this.setState({ errorText: `This user has already been selected.` });
      }
      this._autoComplete.setState({ searchText: '' });
      this._autoComplete.focus();
    }
  }

  onSelectExcludedMember = (member, index) => {
    if (this.state.excludedMembers.users && this.state.excludedMembers.users[index]) {
      if (
        this.state.selectedExcludedMemberIds.every(memberItem => {
          return memberItem[0] !== this.state.excludedMembers.users[index].id;
        })
      ) {
        this.setState({
          selectedExcludedMemberIds: this.state.selectedExcludedMemberIds.concat([
            [
              this.state.excludedMembers.users[index].id,
              this.state.excludedMembers.users[index].name,
              this.state.excludedMembers.users[index].userTeams
            ]
          ])
        });
      } else {
        this.setState({ excludeErrorText: `This user has already been selected to exclude.` });
      }
      this._excludeAutoComplete.setState({ searchText: '' });
      this._excludeAutoComplete.focus();
    }
  };

  toggleDatePicker(toggleParam, param) {
    let date = this.state[param];
    let dueDateError = this.checkDate(
      ...(param === 'startDate'
        ? [this.state.dueDate, date, this.state.dueDateError]
        : [date, this.state.startDate, this.state.startDateError, false]),
      false
    );
    let startDateError = this.checkDate(
      ...(param === 'startDate'
        ? [date, this.state.dueDate, this.state.dueDateError]
        : [this.state.startDate, date, dueDateError]),
      true
    );
    let clearStartDate =
      param === 'startDate' ? this.state.toggleStartDatePicker : !this.state.toggleStartDatePicker;
    let clearDueDate =
      param !== 'startDate' ? this.state.toggleDueDatePicker : !this.state.toggleDueDatePicker;
    this.setState({
      startDateError: clearStartDate ? '' : startDateError,
      dueDateError: clearDueDate ? '' : dueDateError,
      [toggleParam]: !this.state[toggleParam]
    });
  }

  changeView(type) {
    this.setState({ view: type });
  }

  handleExcludeUpdateInput = query => {
    if (this.fetchExcludedUsers) {
      clearTimeout(this.fetchExcludedUsers);
    }
    this.fetchExcludedUsers = setTimeout(() => {
      let payload = {
        'team_ids[]': this.state.selectedGroupIds,
        limit: 15,
        offset: 0,
        q: query
      };

      searchUsers(payload)
        .then(excludedMembers => {
          let newDataSource = [];
          if (excludedMembers.users) {
            newDataSource = excludedMembers.users
              .map(member => {
                return {
                  text: member.name + ' ' + member.handle,
                  id: member.id,
                  value: (
                    <MenuItem
                      primaryText={member.name}
                      leftIcon={<Avatar src={member.picture} />}
                    />
                  )
                };
              })
              .filter(
                member =>
                  !this.state.selectedExcludedMemberIds.some(
                    excludeMember => excludeMember[0] == member.id
                  )
              );
          }
          this.setState({
            excludedMembers,
            excludeMembersData: newDataSource,
            searchText: query,
            excludeErrorText: ''
          });
        })
        .catch(err => {
          console.error(`Error in AssignModal.handleExcludeUpdateInput.getItems.func: ${err}`);
        });
    }, 200);
  };

  handleUpdateInput(query) {
    if (this.fetchUsersInTeam) {
      clearTimeout(this.fetchUsersInTeam);
    }
    this.fetchUsersInTeam = setTimeout(() => {
      let payload = {
        limit: 15,
        offset: 0,
        q: query
      };

      // To fetch only users from Teams current_user owns for assigning
      if (!(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) {
        payload['only_from_my_teams'] = true;
      }
      getItems(payload)
        .then(members => {
          let newDataSource = [];
          if (members.items) {
            newDataSource = members.items.map(member => {
              return {
                text: member.name + ' ' + member.handle + ' ' + member.email,
                value: (
                  <MenuItem primaryText={member.name} leftIcon={<Avatar src={member.picture} />} />
                )
              };
            });
          }
          this.setState({ members, dataSource: newDataSource, searchText: query, errorText: '' });
        })
        .catch(err => {
          console.error(`Error in AssignModal.handleUpdateInput.getItems.func: ${err}`);
        });
    }, 200);
  }

  openCalendar = type => {
    this.setState(prevState => ({ [`${type}IsOpen`]: !prevState[`${type}IsOpen`] }));
  };

  chooseDate(value, type) {
    this.selectDate(type, null, moment(value).format('YYYY-MM-DD'));
  }

  handleChangeFieldWithMask = (type, event) => {
    let date = event.target.value
      .split('/')
      .filter(item => item.split('').every(character => character >= '0' && character <= '9'))
      .join('/');
    if (date.length === 10) {
      this.selectDate(type, null, moment(date, 'MM/DD/YYYY').format('YYYY-MM-DD'));
    }
    if (this.state[`${type}Placeholder`]) {
      this.setState(prevState => ({
        [`${type}Placeholder`]: !prevState[`${type}Placeholder`]
      }));
    }
  };

  render() {
    let imageUrl;
    if (this.props.card.image) {
      imageUrl = this.props.card.image.imageUrl;
    }
    if (this.props.card.imageUrls) {
      imageUrl = this.props.card.imageUrls.medium;
    }
    if (this.props.card.resource && this.props.card.resource.imageUrl) {
      imageUrl = this.props.card.resource.imageUrl;
    }
    if (this.props.card.videoStream && this.props.card.videoStream.imageUrl) {
      imageUrl = this.props.card.videoStream.imageUrl;
    }
    return (
      <div className="groups-assign-modal">
        <input type="text" ref="setFocus" style={this.styles.hide} className="hiddenTextField" />
        <h5>{tr('Assign')}</h5>
        {(this.state.newModalAndToast || !this.state.confirm) && (
          <div>
            <div className="container-padding">
              <div className="vertical-spacing-large">
                {!this.props.selfAssign && (
                  <div>
                    {tr('Assign content to an entire group or individuals within your group')}
                  </div>
                )}
                {this.props.selfAssign && <div>{tr('Add content to your My Learning Plan')}</div>}
                {(imageUrl ||
                  this.props.card.message ||
                  this.props.card.resource ||
                  this.props.card.videoStream ||
                  this.props.card.title) && (
                  <Paper>
                    <div className="assignment-info container-padding horizontal-spacing-medium">
                      {imageUrl && (
                        <div
                          className="card-image"
                          style={{
                            background: 'url("' + imageUrl + '") center center no-repeat',
                            backgroundSize: 'cover'
                          }}
                        />
                      )}
                      {this.props.card.title && (
                        <div className="card-title-container">
                          <div className="line-clamp-3">
                            <MarkdownRenderer markdown={this.props.card.title} />
                          </div>
                        </div>
                      )}
                      {((this.props.card.resource && this.props.card.resource.title) ||
                        this.props.card.message) && (
                        <div className="card-title-container">
                          <div className="line-clamp-3">
                            <div
                              className={
                                this.props.card.resource && this.props.card.resource.title
                                  ? 'card-title'
                                  : ''
                              }
                            >
                              <MarkdownRenderer
                                markdown={
                                  (this.props.card.resource && this.props.card.resource.title) ||
                                  this.props.card.message
                                }
                              />
                            </div>
                          </div>
                        </div>
                      )}
                      {this.props.card.resource && this.props.card.resource.description && (
                        <div className="card-description-container">
                          <div className="card-description line-clamp-3">
                            <MarkdownRenderer markdown={this.props.card.resource.description} />
                          </div>
                        </div>
                      )}
                    </div>
                  </Paper>
                )}
                {!this.props.selfAssign && (
                  <div className="groups-assign-modal">{tr('Include a Message')}</div>
                )}
                {!this.props.selfAssign && (
                  <div>
                    <textarea
                      onChange={e => {
                        this.setState({ message: e.target.value });
                      }}
                      className="message-textarea"
                      placeholder={tr('Type in your message here...')}
                      autoFocus={true}
                      rows="4"
                      maxLength="255"
                      style={this.styles.message}
                    />
                  </div>
                )}
                {!this.props.selfAssign && (
                  <div>
                    <RadioButtonGroup
                      name="shipSpeed"
                      defaultSelected={this.state.view}
                      onChange={e => {
                        this.setState({ view: e.target.value });
                      }}
                    >
                      <RadioButton
                        value="groups"
                        label={tr('Groups')}
                        style={{ display: 'inline-block', width: '150px' }}
                        aria-label={tr('Groups')}
                      />
                      <RadioButton
                        value="individuals"
                        label={tr('Individuals')}
                        style={{ display: 'inline-block', width: '150px' }}
                        aria-label={tr('individuals')}
                      />
                    </RadioButtonGroup>
                  </div>
                )}
                {!this.props.selfAssign && this.state.view === 'groups' && (
                  <div>
                    {this.state.loading && (
                      <div className="text-center">
                        <Spinner />
                      </div>
                    )}
                    {!this.state.loading && (
                      <div>
                        {Object.keys(this.state.groups).length === 0 && (
                          <div>
                            {tr(
                              'You can only assign content to a group where you have administrative privileges. Click'
                            )}{' '}
                            <a href="/teams/create">{tr('here')}</a> {tr('to create a group.')}
                          </div>
                        )}
                        {Object.keys(this.state.groups).length > 0 && (
                          <div className="group-select vertical-spacing-large">
                            <span>{tr('Selected Groups')}</span>
                            <div className="chip-container" style={{ marginBottom: 0 }}>
                              {this.state.selectedGroupIds.map((groupId, index) => {
                                return (
                                  <Chip
                                    key={index}
                                    backgroundColor={colors.primary}
                                    labelColor={colors.white}
                                    style={this.styles.chip}
                                  >
                                    <span style={this.styles.selectedLabel}>
                                      {this.state.groups[groupId]['name']}
                                    </span>
                                    <IconButton
                                      className="cancel-icon delete"
                                      ref={'cancelIcon'}
                                      tooltip={tr('Remove Item')}
                                      aria-label={tr('Remove Item')}
                                      disableTouchRipple
                                      tooltipPosition="top-center"
                                      style={this.styles.customPlusIcon}
                                      onTouchTap={this.handleChipDelete.bind(
                                        this,
                                        'group',
                                        groupId
                                      )}
                                    >
                                      <Close color="#9476C9" />
                                    </IconButton>
                                  </Chip>
                                );
                              })}
                            </div>
                            {Object.keys(this.state.groups).length >
                              this.state.selectedGroupIds.length && (
                              <SelectField hintText="Select Groups" aria-label="select group">
                                {Object.keys(this.state.groups).length -
                                  this.state.selectedGroupIds.length >
                                  0 && (
                                  <MenuItem
                                    key="add-all"
                                    value="add-all"
                                    primaryText={tr('Add All Groups')}
                                    onTouchTap={this.handleSelectGroup.bind(this, 'add-all')}
                                    style={{ fontWeight: 'bold' }}
                                  />
                                )}
                                {Object.keys(this.state.groups)
                                  .filter(id => {
                                    if (this.state.selectedGroupIds.indexOf(id) === -1) {
                                      return id;
                                    }
                                  })
                                  .map((filteredId, index) => {
                                    return (
                                      <MenuItem
                                        key={index}
                                        value={filteredId}
                                        primaryText={this.state.groups[filteredId]['name']}
                                        onTouchTap={this.handleSelectGroup.bind(this, filteredId)}
                                      />
                                    );
                                  })}
                              </SelectField>
                            )}
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                )}
                {!this.props.selfAssign &&
                  this.state.view === 'groups' &&
                  this.state.selectedGroupIds.length > 0 &&
                  this.excludedGroupMembersFlag && (
                    <div>
                      {this.state.loading && (
                        <div className="text-center">
                          <Spinner />
                        </div>
                      )}
                      {!this.state.loading && (
                        <div>
                          <div className="group-select vertical-spacing-large">
                            <span>{tr('Exclude Group members')}</span>
                            <div className="chip-container">
                              {this.state.selectedExcludedMemberIds.map((member, index) => {
                                return (
                                  <Chip
                                    key={index}
                                    backgroundColor={colors.primary}
                                    labelColor={colors.white}
                                    style={this.styles.chip}
                                  >
                                    <span style={this.styles.selectedLabel}>{member[1]}</span>
                                    <IconButton
                                      className="cancel-icon delete"
                                      ref={'cancelIcon'}
                                      tooltip={tr('Remove Item')}
                                      aria-label={tr('Remove Item')}
                                      disableTouchRipple
                                      tooltipPosition="top-center"
                                      style={this.styles.customPlusIcon}
                                      onTouchTap={this.handleExceptionChipDelete.bind(
                                        this,
                                        member[0]
                                      )}
                                    >
                                      <Close color="#9476C9" />
                                    </IconButton>
                                  </Chip>
                                );
                              })}
                            </div>
                            <AutoComplete
                              hintText={tr('Enter a name')}
                              style={{ width: '400px' }}
                              dataSource={this.state.excludeMembersData}
                              onUpdateInput={this.handleExcludeUpdateInput}
                              onNewRequest={this.onSelectExcludedMember}
                              filter={(searchText, key) =>
                                searchText !== '' &&
                                key.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
                              }
                              ref={node => (this._excludeAutoComplete = node)}
                              openOnFocus={true}
                              errorText={tr(this.state.excludeErrorText)}
                              menuStyle={{ maxHeight: '150px', overflowY: 'scroll' }}
                              onTouchTap={() => {
                                this.setState({ excludeErrorText: '' });
                              }}
                            />
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                {!this.props.selfAssign && this.state.view === 'individuals' && (
                  <div>
                    {this.state.loading && (
                      <div className="text-center">
                        <Spinner />
                      </div>
                    )}
                    {!this.state.loading && (
                      <div>
                        {Object.keys(this.state.groups).length === 0 && (
                          <div>
                            {tr(
                              'You can only assign to individuals in your group if you are an administrator. Click'
                            )}{' '}
                            <a href="/teams/create">{tr('here')}</a> {tr('to create a group.')}
                          </div>
                        )}
                        {Object.keys(this.state.groups).length > 0 && (
                          <div className="group-select vertical-spacing-large">
                            <span>{tr('Select one or more Individuals')}</span>
                            <div className="chip-container">
                              {this.state.selectedMemberIds.map((member, index) => {
                                return (
                                  <Chip
                                    key={index}
                                    backgroundColor={colors.primary}
                                    labelColor={colors.white}
                                    style={this.styles.chip}
                                  >
                                    <span style={this.styles.selectedLabel}>{member[1]}</span>
                                    <IconButton
                                      className="cancel-icon delete"
                                      ref={'cancelIcon'}
                                      tooltip={tr('Remove Item')}
                                      aria-label={tr('Remove Item')}
                                      disableTouchRipple
                                      tooltipPosition="top-center"
                                      style={this.styles.customPlusIcon}
                                      onTouchTap={this.handleChipDelete.bind(
                                        this,
                                        'individual',
                                        member[0]
                                      )}
                                    >
                                      <Close color="#9476C9" />
                                    </IconButton>
                                  </Chip>
                                );
                              })}
                            </div>
                            <AutoComplete
                              hintText={tr('Enter a name')}
                              style={{ width: '400px' }}
                              dataSource={this.state.dataSource}
                              onUpdateInput={this.handleUpdateInput}
                              onNewRequest={this.selectMember}
                              filter={(searchText, key) =>
                                searchText !== '' &&
                                key.toLowerCase().indexOf(searchText.toLowerCase()) !== -1
                              }
                              ref={node => (this._autoComplete = node)}
                              openOnFocus={true}
                              errorText={tr(this.state.errorText)}
                              menuStyle={{ maxHeight: '150px', overflowY: 'scroll' }}
                              onTouchTap={() => {
                                this.setState({ errorText: '' });
                              }}
                              aria-label={tr('Enter a name')}
                            />
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                )}
                <div className="row dates-container">
                  <div className="small-12 medium-6 columns">
                    <Toggle
                      labelPosition="left"
                      label={tr('Add Start Date')}
                      aria-label={tr('Add Start Date')}
                      onToggle={this.toggleDatePicker.bind(
                        this,
                        'toggleStartDatePicker',
                        'startDate'
                      )}
                      labelStyle={this.styles.toggleLabel}
                      style={this.styles.toggle}
                      defaultToggled={this.state.toggleStartDatePicker}
                    />
                    {this.state.toggleStartDatePicker && (
                      <div>
                        <div className="date-field-container">
                          <TextField
                            id="startDate"
                            hintText={this.state.startDatePlaceholder ? 'MM/DD/YYYY' : ''}
                            onChange={this.selectDate.bind(this, 'startDate')}
                            ref={node => (this._startInput = node)}
                            defaultValue={this.state.startDate}
                            min={moment(Date.now()).format('YYYY-DD-MM')}
                            max={this.state.dueDate || '4000-01-01'}
                            errorText={tr(this.state.startDateError)}
                            errorStyle={this.styles.errorStyle}
                            value={this.state.startDate || ''}
                            aria-label="enter start date"
                            onBlur={() => {
                              !this.state.startDate &&
                                this.setState({ startDatePlaceholder: true });
                            }}
                          >
                            <MaskedInput
                              keepCharPositions={true}
                              value={
                                this.state.startDate
                                  ? moment(this.state.startDate).format('MM/DD/YYYY')
                                  : ''
                              }
                              onChange={this.handleChangeFieldWithMask.bind(this, 'startDate')}
                              mask={[
                                /[0-1]/,
                                /\d/,
                                '/',
                                /[0-3]/,
                                /\d/,
                                '/',
                                /\d/,
                                /\d/,
                                /\d/,
                                /\d/
                              ]}
                            />
                          </TextField>
                          <FlatButton
                            hoverColor="transparent"
                            rippleColor="transparent"
                            className="accordion-title journey__accordion-title dates-arrow"
                            onClick={this.openCalendar.bind(this, 'startDate')}
                            secondary={true}
                            icon={
                              this.state.startDateIsOpen ? (
                                <NavigationArrowDropUp style={this.styles.arrowStyle} />
                              ) : (
                                <NavigationArrowDropDown style={this.styles.arrowStyle} />
                              )
                            }
                          />
                        </div>
                        {this.state.startDateIsOpen && (
                          <div style={this.styles.calendar}>
                            <Calendar
                              onChange={event => this.chooseDate(event, 'startDate')}
                              date={this.state.startDate ? moment(this.state.startDate) : moment()}
                              minDate={moment()}
                              firstDayOfWeek={1}
                              maxDate={this.state.dueDate ? moment(this.state.dueDate) : 'none'}
                            />
                            <div className="text-center">
                              <button
                                className="my-button"
                                style={{ margin: '0 0.5rem 0 0' }}
                                onClick={() => {
                                  this.setState({ startDate: '', startDatePlaceholder: true });
                                }}
                              >
                                {tr('Reset')}
                              </button>
                              <button
                                className="my-button"
                                onClick={() => {
                                  this.setState({ startDateIsOpen: false });
                                }}
                              >
                                {tr('Cancel')}
                              </button>
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                  <div className="small-12 medium-6 columns">
                    <Toggle
                      labelPosition="left"
                      label={tr('Add Due Date')}
                      aria-label={tr('Add Due Date')}
                      onToggle={this.toggleDatePicker.bind(this, 'toggleDueDatePicker', 'dueDate')}
                      labelStyle={this.styles.toggleLabel}
                      style={this.styles.toggle}
                      defaultToggled={this.state.toggleDueDatePicker}
                    />
                    {this.state.toggleDueDatePicker && (
                      <div>
                        <div className="date-field-container">
                          <TextField
                            id="dueDate"
                            hintText={this.state.dueDatePlaceholder ? 'MM/DD/YYYY' : ''}
                            onChange={this.selectDate.bind(this, 'dueDate')}
                            ref={node => (this._endInput = node)}
                            defaultValue={this.state.dueDate}
                            min={
                              moment(this.state.startDate).isAfter(Date.now())
                                ? this.state.startDate
                                : moment(Date.now()).format('MM/DD/YYYY')
                            }
                            max={'4000-01-01'}
                            errorText={tr(this.state.dueDateError)}
                            errorStyle={this.styles.errorStyle}
                            value={this.state.dueDate || ''}
                            aria-label="enter due date"
                            onBlur={() => {
                              !this.state.dueDate && this.setState({ dueDatePlaceholder: true });
                            }}
                          >
                            <MaskedInput
                              keepCharPositions={true}
                              value={
                                this.state.dueDate
                                  ? moment(this.state.dueDate).format('MM/DD/YYYY')
                                  : ''
                              }
                              onChange={this.handleChangeFieldWithMask.bind(this, 'dueDate')}
                              mask={[
                                /[0-1]/,
                                /\d/,
                                '/',
                                /[0-3]/,
                                /\d/,
                                '/',
                                /\d/,
                                /\d/,
                                /\d/,
                                /\d/
                              ]}
                            />
                          </TextField>
                          <FlatButton
                            hoverColor="transparent"
                            rippleColor="transparent"
                            className="accordion-title journey__accordion-title dates-arrow"
                            onClick={this.openCalendar.bind(this, 'dueDate')}
                            secondary={true}
                            icon={
                              this.state.dueDateIsOpen ? (
                                <NavigationArrowDropUp style={this.styles.arrowStyle} />
                              ) : (
                                <NavigationArrowDropDown style={this.styles.arrowStyle} />
                              )
                            }
                          />
                        </div>
                        {this.state.dueDateIsOpen && (
                          <div style={this.styles.calendar}>
                            <Calendar
                              onChange={event => this.chooseDate(event, 'dueDate')}
                              date={
                                this.state.dueDate
                                  ? moment(this.state.dueDate)
                                  : moment(this.state.startDate).isAfter(Date.now())
                                  ? moment(this.state.startDate)
                                  : moment()
                              }
                              minDate={
                                moment(this.state.startDate).isAfter(Date.now())
                                  ? moment(this.state.startDate)
                                  : moment()
                              }
                              firstDayOfWeek={1}
                              maxDate={moment(new Date('4000-01-01'))}
                            />
                            <div className="text-center">
                              <button
                                className="my-button"
                                style={{ margin: '0 0.5rem 0 0' }}
                                onClick={() => {
                                  this.setState({ dueDate: '', dueDatePlaceholder: true });
                                }}
                              >
                                {tr('Reset')}
                              </button>
                              <button
                                className="my-button"
                                onClick={() => {
                                  this.setState({ dueDateIsOpen: false });
                                }}
                              >
                                {tr('Cancel')}
                              </button>
                            </div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="text-right">
                <SecondaryButton
                  label={tr('Cancel')}
                  className="close"
                  onTouchTap={this.cancelClickHandler}
                />
                <PrimaryButton
                  label={tr('Assign')}
                  className="create"
                  onTouchTap={
                    this.state.newModalAndToast ? this.confirmClickHandler : this.assignClickHandler
                  }
                  disabled={
                    (((this.state.selectedGroupIds.length === 0 && this.state.view === 'groups') ||
                      (this.state.selectedMemberIds.length === 0 &&
                        this.state.view === 'individuals')) &&
                      !this.props.selfAssign) ||
                    this.state.startDateError ||
                    this.state.dueDateError
                  }
                />
              </div>
            </div>
          </div>
        )}
        {!this.state.newModalAndToast && this.state.confirm && (
          <div className="vertical-spacing-large">
            {this.state.view === 'groups' && (
              <div className="vertical-spacing-large">
                <div>{tr('Assigning to the following groups:')}</div>
                <div className="chip-container">
                  {this.state.selectedGroupIds.map((groupId, index) => {
                    return (
                      <Chip
                        key={index}
                        backgroundColor={colors.primary}
                        labelColor={colors.white}
                        style={this.styles.chip}
                      >
                        {this.state.groups[groupId]['name']}
                      </Chip>
                    );
                  })}
                </div>
                {this.excludedGroupMembersFlag && this.state.selectedExcludedMemberIds.length > 0 && (
                  <div>
                    <div style={{ marginBottom: '1rem' }}>
                      {tr('Excluded to the following users:')}
                    </div>
                    <div className="chip-container">
                      {this.state.selectedExcludedMemberIds.map((member, index) => {
                        return (
                          <Chip
                            key={index}
                            backgroundColor={colors.primary}
                            labelColor={colors.white}
                            style={this.styles.chip}
                          >
                            {member[1]}
                          </Chip>
                        );
                      })}
                    </div>
                  </div>
                )}
              </div>
            )}
            {this.props.selfAssign && (
              <div className="vertical-spacing-large">
                <div>{tr('Assign to yourself?')}</div>
              </div>
            )}
            {this.state.view === 'individuals' && (
              <div className="vertical-spacing-large">
                <div>{tr('Assigning to the following individuals')}:</div>
                <div className="chip-container">
                  {this.state.selectedMemberIds.map((member, index) => {
                    return (
                      <Chip
                        key={index}
                        backgroundColor={colors.primary}
                        labelColor={colors.white}
                        style={this.styles.chip}
                      >
                        {member[1]}
                      </Chip>
                    );
                  })}
                </div>
              </div>
            )}
            {this.state.toggleStartDatePicker && this.state.startDate && (
              <div>
                {tr('Start Date:')} {moment(this.state.startDate).format('MM/DD/YYYY')}
              </div>
            )}
            {this.state.toggleDueDatePicker && this.state.dueDate && (
              <div>
                {tr('Due At:')} {moment(this.state.dueDate).format('MM/DD/YYYY')}
              </div>
            )}
            <div className="container-padding">
              <div className="text-right">
                <SecondaryButton
                  label={tr('Back')}
                  className="back"
                  keyboardFocused={true}
                  onTouchTap={() => {
                    this.setState({ confirm: false, startDateIsOpen: false, dueDateIsOpen: false });
                  }}
                />
                <PrimaryButton
                  label={tr('Confirm')}
                  className="confirm"
                  onTouchTap={this.confirmClickHandler}
                  pending={this.state.pending}
                  pendingLabel={tr('Assigning...')}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

AssignModal.propTypes = {
  open: PropTypes.bool,
  currentUser: PropTypes.object,
  selfAssign: PropTypes.bool,
  card: PropTypes.object,
  assignedStateChange: PropTypes.func,
  team: PropTypes.object
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStateToProps)(AssignModal);
