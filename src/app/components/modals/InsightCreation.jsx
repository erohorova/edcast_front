import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { openInlineCreationModal } from '../../actions/modalActions';
import Avatar from 'edc-web-sdk/components/Avatar';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import colors from 'edc-web-sdk/components/colors/index';
import ListItem from 'material-ui/List/ListItem';
import { Tabs, Tab } from 'material-ui/Tabs';
import CreateNewPathwayComponent from './CreateNewPathwayComponent';
import InsightEditor from './InsightEditor';
import { tr } from 'edc-web-sdk/helpers/translations';

//This component was actually becomes Insight & Pathway creation modal container.
class InsightCreation extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      header: {
        paddingTop: '0',
        paddingBottom: '4px',
        margin: '8px 0'
      },
      miniStateAction: {
        backgroundColor: colors.gray,
        borderRadius: '50%',
        padding: '6px 5px 5px 5px',
        height: '34px',
        width: '34px',
        top: '10px',
        right: '16px'
      },
      tabs: {
        maxWidth: '400px'
      }
    };

    this.state = {
      currentUser: props.currentUser,
      type: props.modal.enabled
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currentUser: nextProps.currentUser
    });
  }

  activeTabHandler = creationType => {
    this.setState({ type: creationType });
  };

  miniStateClickHandler = () => {
    this.props.dispatch(openInlineCreationModal());
  };

  uploadImageClickHandler = () => {
    if (!this.props.expand) {
      this.props.dispatch(openInlineCreationModal());
      this.setState({ requireUploadImage: true });
    }
  };

  uploadImageHandler = () => {
    this.setState({ requireUploadImage: false });
  };

  render() {
    return (
      <div
        className="insight-creation"
        onTouchTap={!this.props.expand && this.miniStateClickHandler}
      >
        <TextField name="insightcreator" autoFocus={true} className="hiddenTextField" />
        {!this.props.expand && (
          <ListItem
            disabled
            style={this.styles.header}
            leftAvatar={<Avatar user={this.state.currentUser} />}
            primaryText={
              <TextField
                value={this.state.message}
                name="message"
                hintText={tr('Share your knowledge here...')}
                onChange={this.messageChangeHandler}
                fullWidth={true}
                onPaste={this.pasteLinkHandler}
                aria-label={tr('Share your knowledge here...')}
              />
            }
            rightIconButton={
              <IconButton
                aria-label="upload image"
                style={this.styles.miniStateAction}
                className="uploadImage"
                onTouchTap={this.uploadImageClickHandler}
              >
                <ImagePhotoCamera color={colors.white} />
              </IconButton>
            }
          />
        )}
        {this.props.expand && (
          <div>
            <div className="tabs-wrapper">
              <Tabs value={this.state.type}>
                {this.state.type === 'smartbite' && (
                  <Tab
                    className="smartbite"
                    label={tr('SMARTCARD')}
                    value="smartbite"
                    onActive={this.activeTabHandler.bind(this, 'smartbite')}
                  />
                )}
                {this.state.type === 'pathway' && (
                  <Tab
                    className="pathway"
                    label={tr('PATHWAY')}
                    value="pathway"
                    onActive={this.activeTabHandler.bind(this, 'pathway')}
                  />
                )}
              </Tabs>
            </div>
            {(this.state.type === 'smartbite' || this.state.type === 'both') && (
              <InsightEditor
                requireUploadImage={this.state.requireUploadImage}
                onUploadImage={this.uploadImageHandler}
              />
            )}
            {this.state.type === 'pathway' && (
              <div>
                <CreateNewPathwayComponent />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

InsightCreation.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  expand: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(InsightCreation);
