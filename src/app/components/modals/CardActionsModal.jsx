import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import { fetchCardStats, fetchCardStatsUsers } from 'edc-web-sdk/requests/cards';
import { close } from '../../actions/modalActions';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import UserBadgesContainer from '../common/UserBadgesContainer';
import { Avatar } from 'material-ui';
import Spinner from '../common/spinner';
import { push } from 'react-router-redux';
import uniqBy from 'lodash/uniqBy';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class cardActionsModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cardData: [],
      card: this.props.card,
      pending: true
    };

    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.fetchUsers = this.fetchUsers.bind(this);
  }
  componentWillMount() {
    this.fetchUsers();
  }

  fetchUsers() {
    const id = this.props.card.cardId || this.props.card.id;
    const type = this.props.typeOfModal;
    fetchCardStatsUsers(id, type, this.state.cardData.length)
      .then(cardData => {
        const allData = uniqBy(this.state.cardData.concat(cardData), 'id');
        this.setState({
          cardData: allData,
          showMore: cardData.length === 10,
          pending: false
        });
      })
      .catch(err => {
        console.error(`Error in CardActionsModal.fetchCardStatsUsers.func: ${err}`);
      });
  }

  cancelClickHandler() {
    this.props.dispatch(close());
  }

  render() {
    let isData = this.state.cardData && !!this.state.cardData.length;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    return (
      <div className="cardData">
        <div>
          <div className="cardData-header">
            {tr(`Show ${this.props.typeOfModal === 'view' ? 'Views' : 'Likes'}`)}{' '}
            {this.state.cardData.length && `(${this.state.cardData.length})`}
            <span className={'close close-icon-btn'}>
              <NavigationClose color={colors.grey} onTouchTap={this.cancelClickHandler} />
            </span>
            <TextField name="cardactionmodal" autoFocus={true} className="hiddenTextField" />
          </div>
          {this.state.pending ? (
            <div className="text-center">
              <Spinner />
            </div>
          ) : isData ? (
            <div className={`data-users ${this.state.cardData.length < 10 && 'without-scroll'}`}>
              {this.state.cardData.map((data, index) => (
                <div className="data-user" key={index}>
                  <a
                    className="data-ava"
                    onClick={() => {
                      this.props.dispatch(push(`/${data.handle}`));
                    }}
                    target="_blank"
                  >
                    <Avatar
                      src={(data.avatarimages && data.avatarimages.small) || defaultUserImage}
                      size={26}
                    />
                  </a>
                  <div className="data-name">{data.name}</div>
                  {(data.roles || data.rolesDefaultNames) && (
                    <div className="data-labels">
                      <UserBadgesContainer
                        rolesDefaultNames={data.rolesDefaultNames}
                        roles={data.roles}
                      />
                    </div>
                  )}
                </div>
              ))}
              {this.state.showMore && (
                <div className="text-center">
                  <SecondaryButton
                    label={tr('Show More')}
                    className="viewMore"
                    onTouchTap={this.fetchUsers}
                  />
                </div>
              )}
            </div>
          ) : (
            <div className="container-padding data-not-available-msg">
              {tr('No search results found.')}
            </div>
          )}
        </div>
      </div>
    );
  }
}

cardActionsModal.propTypes = {
  open: PropTypes.bool,
  typeOfModal: PropTypes.string,
  card: PropTypes.object
};

export default connect()(cardActionsModal);
