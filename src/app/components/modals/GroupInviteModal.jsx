import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Paper from 'edc-web-sdk/components/Paper';
import AddUsers from '../team/SingleTeam/AddUsers';
import { tr } from 'edc-web-sdk/helpers/translations';
import TextField from 'material-ui/TextField';

class GroupInviteModal extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      paper: {
        padding: '16px'
      },
      leaders: {
        color: '#454560',
        fontSize: '12px',
        height: '24px',
        lineHeight: '24px'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let userType = this.props.modal && this.props.modal.userType;
    return (
      <div className="channel-curators-modal">
        <div className="row">
          <TextField name="groupInvite" autoFocus={true} className="hiddenTextField" />
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '15px' }}>
            <div style={{ color: 'white' }}>
              {userType === 'group-admin' ? tr('Edit Admins') : tr('Edit Leaders')}
            </div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              <div className="row">
                <div className="small-12 medium-2 columns">
                  <div style={this.styles.leaders}>
                    {userType === 'group-admin' ? tr('Group Admins') : tr('Leaders')}
                  </div>
                </div>
                <div className="small-12 medium-10 columns">
                  <AddUsers
                    currentUserId={this.props.currentUserId}
                    userType={userType}
                    subAdmins={group.subAdmins}
                    leaders={group.owners}
                    closeModal={this.closeModal}
                  />
                </div>
              </div>
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

GroupInviteModal.propTypes = {
  groupsV2: PropTypes.object,
  currentUser: PropTypes.object,
  currentUserId: PropTypes.string,
  modal: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS(),
    modal: state.modal.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupInviteModal);
