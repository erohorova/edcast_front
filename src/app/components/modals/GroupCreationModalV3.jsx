import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactTags from 'react-tag-autocomplete';
import { push } from 'react-router-redux';

import _ from 'lodash';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
import MoveIcon from 'material-ui/svg-icons/maps/zoom-out-map';

import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import {
  createGroup,
  deleteTeamUsers,
  getUsersInAGroupV2,
  deleteUsersFromTeam,
  createDynamicSelectionGroup
} from 'edc-web-sdk/requests/groups.v2';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import * as channelSDK from 'edc-web-sdk/requests/channels';
import * as channelsV2Sdk from 'edc-web-sdk/requests/channels.v2';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import { close, openStatusModal } from '../../actions/modalActions';
import {
  updateMembers,
  getGroupDetail,
  updateGroupExtended,
  getTeamChannels,
  getTeamCards,
  invite,
  getGroupUsers,
  deleteUserFromTeam,
  updateGroupCarouselsOrder
} from '../../actions/groupsActionsV2';
import { filestackClient } from '../../utils/filestack';
import { fileStackSources } from '../../constants/fileStackSource';

import CustomTag from '../common/CustomTag';
import DynamicSelections from '../multiactionContent/DynamicSelection';
import PreviewDSModal from './PreviewDSModal';
import { open as openSnackBar } from '../../actions/snackBarActions';
import CheckOffSlim from 'edc-web-sdk/components/icons/CheckOffSlim';
import { Permissions } from '../../utils/checkPermissions';
import Sortable from 'sortablejs';
import { editTeamCarousels } from 'edc-web-sdk/requests/teams';
import Toggle from 'material-ui/Toggle';

const charForName = 150;
const charForDescription = 2000;

class GroupCreationModalV3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      imageContainer: {
        position: 'relative',
        backgroundSize: 'contain',
        backgroundPosition: '50% 50%',
        backgroundRepeat: 'no-repeat',
        height: '100%',
        display: 'block'
      },
      previewButton: {
        backgroundColor: '#acadc1',
        color: '#fff',
        border: '0',
        position: 'absolute',
        right: '1.2rem',
        bottom: '1rem'
      },
      hoverIcon: {
        paddingBottom: '0',
        height: 'auto'
      },
      closeIcon: {
        paddingRight: 0,
        width: 'auto'
      },
      moveIcon: {
        width: '1rem',
        height: '1rem',
        transform: 'rotate(45deg)'
      },
      carouselsPosition: {
        marginTop: '8px',
        marginBottom: '8px'
      }
    };
    this.state = {
      isForUpdate: !!props.groupId,
      name: '',
      description: '',
      pending: false,
      fileStack: [],
      createErrorText: '',
      auto_assign_content: true,
      adminCanShare: false,
      memberCanShare: false,
      adminCanAssign: false,
      memberCanAssign: false,
      groupIsOpen: false,
      isMandatory: false,
      suggestionsLimit: 10,
      leadersOffset: 0,
      adminsOffset: 0,
      membersOffset: 0,
      channelsOffset: 0,
      leadersCurrent: [],
      adminsCurrent: [],
      membersCurrent: [],
      channelsCurrent: [],
      leadersRemove: [],
      adminsRemove: [],
      membersRemove: [],
      channelsRemove: [],
      leadersOptions: [],
      adminsOptions: [],
      membersOptions: [],
      channelsOptions: [],
      leadersOld: [],
      adminsOld: [],
      membersOld: [],
      channelsOld: [],
      currentGroup: {},
      currentGroupId: '',
      workWithMandatoryGroup: window.ldclient.variation('mandatory-group', false),
      groupIsDynamic: false,
      isOpenDynamicSection: false,
      previewFilter: [],
      customFields: [],
      shouldShareToAdmin: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      dynamicSelections: window.ldclient.variation('dynamic-selections', false),
      carousels: [],
      isCarouselsModified: false
    };
    this.usersLimit = 10;
  }

  componentDidMount() {
    let id = this.props && this.props.groupId;
    let group = this.props.groupsV2[id];
    if (this.state.isForUpdate && group) {
      this.setState(() => ({
        currentGroupId: id,
        name: group.name,
        currentGroup: group,
        groupIsOpen: !group.isPrivate,
        isMandatory: group.isMandatory,
        isDynamic: group.isDynamic,
        auto_assign_content: group.autoAssignContent,
        shouldShareToAdmin: group.onlyAdminCanPost,
        description: group.description,
        showMainImage: true,
        carousels: group.carousels.sort((a, b) => a.index - b.index)
      }));
      let pendingPayload = {
        offset: '0',
        limit: group.pendingMembersCount || 10,
        user_type: 'pending'
      };
      this.props.dispatch(getGroupUsers(id, pendingPayload));
      this.props
        .dispatch(getTeamChannels(id, { limit: group.teamChannelsLimit, offset: 0 }))
        .then(() => {
          let leadersOld =
            (group.owners &&
              group.owners.map(item => {
                return { id: item.id, name: this.concatLabel(item) };
              })) ||
            [];
          let adminsOld =
            (group.subAdmins &&
              group.subAdmins.map(item => {
                return { id: item.id, name: this.concatLabel(item) };
              })) ||
            [];
          let channelsOld =
            (group.teamChannels &&
              group.teamChannels.map(item => {
                return { id: item.id, name: item.label, type: 'channel' };
              })) ||
            [];
          let membersOld;
          getUsersInAGroupV2(id, { user_type: 'member' })
            .then(data => {
              membersOld =
                data && data.members && data.members.users
                  ? data.members.users.map(item => {
                      return { id: item.id, name: this.concatLabel(item), email: item.email };
                    })
                  : [];
              let leadersCurrent = leadersOld.slice();
              let adminsCurrent = adminsOld.slice();
              let membersCurrent = membersOld.slice();
              let channelsCurrent = channelsOld.slice();
              this.setState({
                leadersCurrent,
                adminsCurrent,
                membersCurrent,
                channelsCurrent,
                leadersOld,
                adminsOld,
                membersOld,
                channelsOld
              });
            })
            .catch(e => console.error("Can't load initializedGroupUser"));
        })
        .catch(e => console.error("Can't load Team Channels"));
    }
  }

  closeModal = () => {
    this.props.dispatch(close());
  };
  changegroupImg = (e, accept, max, callback) => {
    e.preventDefault();
    this.fileStackHandler(accept, max, callback);
  };
  fileStackHandler = (accept, max, callback) => {
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: accept,
            maxFiles: max,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in GroupCreationModalV3.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModalV3.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  updateFileStackFile(fileStack) {
    let tempPath = fileStack.filesUploaded;
    let url = tempPath[0].url;
    let securedUrl;
    refreshFilestackUrl(url)
      .then(resp => {
        securedUrl = resp.signed_url;
        this.setState({
          fileStack: tempPath,
          securedUrl: securedUrl,
          showMainImage: tempPath.length
        });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModalV3.updateFileStackFile.refreshFilestackUrl.func : ${err}`
        );
      });
  }

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton aria-label={tr('Change Image')} style={this.styles.hoverIcon}>
          <ImagePhotoCamera color="white" viewBox="0 0 24 24" />
        </IconButton>
        <div className="roll-text">{tr('Change Image')}</div>
      </span>
    );
  }

  displayImageInput() {
    let backgroundImg;
    if (this.state.showMainImage) {
      if (this.state.currentGroup) {
        backgroundImg =
          this.state.securedUrl ||
          (this.state.fileStack[0] && this.state.fileStack[0].url) ||
          this.state.currentGroup.imageUrls.medium;
      } else {
        backgroundImg = this.state.securedUrl || this.state.fileStack[0].url;
      }
    } else {
      backgroundImg = '';
    }
    return (
      <div>
        {this.state.showMainImage ? (
          <a
            href="#"
            onClick={e =>
              this.changegroupImg(e, ['image/*'], 1, this.updateFileStackFile.bind(this))
            }
            className="preview-banner preview-upload"
          >
            <div
              className="card-img-container"
              onMouseEnter={() => {
                this.setState({ mainImage: true });
              }}
              onMouseLeave={() => {
                this.setState({ mainImage: false });
              }}
            >
              <div
                className="card-img button-icon"
                style={{
                  ...this.styles.imageContainer,
                  ...{ backgroundImage: `url(\'${backgroundImg}\')` }
                }}
              >
                {this.hoverBlock(
                  this.fileStackHandler.bind(
                    this,
                    ['image/*'],
                    1,
                    this.updateFileStackFile.bind(this)
                  )
                )}
              </div>
            </div>
          </a>
        ) : (
          <div
            className="preview-banner"
            onClick={this.fileStackHandler.bind(
              this,
              ['image/*'],
              1,
              this.updateFileStackFile.bind(this)
            )}
          >
            <div className="text-center">
              <ImagePhotoCamera style={{ width: '1.875rem', height: '1.875rem' }} />
              <div className="empty-image">{tr('Upload Group Image')}</div>
            </div>
          </div>
        )}
        <small>{tr('Recommended Size:')} 480px x 320px</small>
      </div>
    );
  }

  createClickHandler = () => {
    this.setState({ pending: true });
    let imageUrl = this.state.fileStack.length ? this.state.fileStack[0].url : undefined;

    let is_mandatory = this.state.isMandatory;

    if (!this.state.groupIsOpen) {
      is_mandatory = this.state.workWithMandatoryGroup ? this.state.isMandatory : true;
    }

    if (this.state.groupIsOpen && is_mandatory) {
      is_mandatory = false;
    }

    let payload = {
      name: this.state.name,
      description: this.state.description,
      auto_assign_content: this.state.auto_assign_content,
      image: imageUrl,
      is_private: !this.state.groupIsOpen || false,
      is_mandatory: is_mandatory, // group may be mandatory only when group is private
      only_admin_can_post: this.state.shouldShareToAdmin
    };
    if (this.state.isForUpdate) {
      this.props
        .dispatch(updateGroupExtended(this.state.currentGroupId, payload))
        .then(() => {
          this.updateChannels()
            .then(() => {
              this.updateFieldContent('member', 'member')
                .then(() => {
                  this.updateFieldContent('sub_admin', 'admin')
                    .then(() => {
                      this.updateFieldContent('admin', 'leader')
                        .then(() => {
                          this.props
                            .dispatch(
                              getGroupDetail(this.props.groupsV2[this.state.currentGroupId].slug)
                            )
                            .then(() => {
                              this.props
                                .dispatch(
                                  getTeamCards(this.state.currentGroupId, 'teamAssignments', {
                                    limit: this.props.groupsV2[this.state.currentGroupId]
                                      .cardsLimit,
                                    offset: 0,
                                    type: 'assigned'
                                  })
                                )
                                .then(() => {
                                  this.props
                                    .dispatch(
                                      getTeamCards(this.state.currentGroupId, 'sharedCards', {
                                        limit: this.props.groupsV2[this.state.currentGroupId]
                                          .cardsLimit,
                                        offset: 0,
                                        type: 'shared'
                                      })
                                    )
                                    .then(() => {
                                      this.props
                                        .dispatch(
                                          getTeamChannels(this.state.currentGroupId, {
                                            limit: this.props.groupsV2[this.state.currentGroupId]
                                              .teamChannelsLimit,
                                            offset: 0
                                          })
                                        )
                                        .then(() => {
                                          this.closeModal();
                                        })
                                        .catch(err =>
                                          console.error(
                                            `Error in GroupCreationModalV3.createClickHandler.getTeamChannels.func member: ${err}`
                                          )
                                        );
                                    })
                                    .catch(err =>
                                      console.error(
                                        `Error in GroupCreationModalV3.getTeamSharedCards.func member: ${err}`
                                      )
                                    );
                                })
                                .catch(err =>
                                  console.error(
                                    `Error in GroupCreationModalV3.getTeamAssignmentsCards.func member: ${err}`
                                  )
                                );
                            })
                            .catch(err =>
                              console.error(
                                `Error in GroupCreationModalV3.createClickHandler.getGroupDetail.func member: ${err}`
                              )
                            );
                        })
                        .catch(err =>
                          console.error(
                            `Error in GroupCreationModalV3.updateFieldContent.func member: ${err}`
                          )
                        );
                    })
                    .catch(err =>
                      console.error(
                        `Error in GroupCreationModalV3.updateFieldContent.func sub_admin: ${err}`
                      )
                    );
                })
                .catch(err =>
                  console.error(
                    `Error in GroupCreationModalV3.updateFieldContent.func admin: ${err}`
                  )
                );
            })
            .catch(err =>
              console.error(`Error in GroupCreationModalV3.updateChannels.func : ${err}`)
            );
        })
        .catch(res => {
          this.setState({
            pending: false,
            createErrorText: (res && res.body && res.body.message) || res.statusText
          });
        });
    } else {
      if (this.state.groupIsDynamic) {
        payload = {
          type: 'dynamic-selection-group',
          action: 'create',
          additionInfo: {
            objectType: 'card',
            objectId: '',
            action: 'share',
            message: ''
          },
          payload: {
            groupTitle: this.state.name,
            groupDescription: 'Group created using Workflow',
            profileFilterOptions: {
              filterConditionType: 'all',
              filterRules: _.flatten(
                this.state.previewFilter
                  .filter(item => item.id > -1)
                  .map(item =>
                    item.options
                      .filter(option => option.checked)
                      .map(option => ({
                        targetValue: option.option,
                        condition: 'equals',
                        colId: item.type
                      }))
                  )
              )
            }
          }
        };
        createDynamicSelectionGroup(payload)
          .then(data => {
            this.props.dispatch(
              openSnackBar((data && data.message) || 'The dynamic group was created.', true)
            );
            this.closeModal();
          })
          .catch(err => {
            console.error(
              `Error in CreateGroupForMultiaction.createHandle.createDynamicSelectionGroup.func: ${err}`
            );
            this.closeModal();
          });
      } else {
        createGroup(payload)
          .then(group => {
            this.setState({ currentGroupId: group.id }, () => {
              this.updateChannels()
                .then(() => {
                  this.updateAllFields(group.id)
                    .then(() => {
                      if (!!group && !!group.slug) {
                        let link = `/teams/${group.slug}`;
                        let isRedirect = false;
                        if (
                          window.location.pathname &&
                          !~window.location.pathname.indexOf('/teams/')
                        ) {
                          this.props.dispatch(push(link));
                          isRedirect = true;
                        }
                        let compoundMessage = ['Your Group has been created!'];
                        if (this.state.newModalAndToast) {
                          this.props.dispatch(
                            openSnackBar(compoundMessage[0], () => {
                              if (isRedirect) window.location.pathname = link;
                            })
                          );
                        } else {
                          this.props.dispatch(
                            openStatusModal(
                              '',
                              () => {
                                if (isRedirect) window.location.pathname = link;
                              },
                              compoundMessage
                            )
                          );
                        }
                        this.props
                          .dispatch(getGroupDetail(group.slug))
                          .then(updated_group => {
                            this.props.updateGroupsList &&
                              this.props.updateGroupsList(updated_group);
                          })
                          .catch(err => console.error('Error while getGroupDetail: \n', err));
                      }
                      this.closeModal();
                    })
                    .catch(err => console.error('Error while adding users to new group: \n', err));
                })
                .catch(err =>
                  console.error(`Error in GroupCreationModalV3.updateChannels.func : ${err}`)
                );
            });
          })
          .catch(res => {
            this.setState({
              pending: false,
              createErrorText: (res && res.body && res.body.message) || res.statusText
            });
          });
      }
    }
    if (this.state.isCarouselsModified) {
      this.updateGroupCarousels();
    }
  };

  getRemoveTagsIds = itemsType => {
    return this.state[`${itemsType}sRemove`].map(item => item.id);
  };
  getNewTagsIds = itemsType => {
    return this.state[`${itemsType}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('isNew');
      })
      .map(item => item.id);
  };

  updateFieldContent(role, fieldName) {
    return new Promise(resolve => {
      let newAdminIds = this.getNewTagsIds(fieldName);
      let oldIds = this.getRemoveTagsIds(fieldName);
      let currentGroup =
        this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID];
      let pendingUsers = (currentGroup && currentGroup.pending) || [];
      let pendingIds = _.map(pendingUsers, 'id');
      if (!!newAdminIds.length) {
        let invitedList = [];
        let changeRoleList = [];
        let changePendingRoleList = [];
        _.forEach(newAdminIds, item => {
          if (
            _.indexOf(_.map(this.state.membersOld, member => member.id), +item) === -1 &&
            _.indexOf(_.map(this.state.adminsOld, admin => admin.id), +item) === -1 &&
            _.indexOf(_.map(this.state.leadersOld, leader => leader.id), +item) === -1 &&
            _.indexOf(pendingIds, +item) === -1
          ) {
            invitedList.push(
              _.find(this.state[`${fieldName}sCurrent`], elem => {
                return elem.id == item;
              })
            );
          } else if (_.indexOf(pendingIds, item) === -1) {
            changeRoleList.push(item);
          } else {
            changePendingRoleList.push(item);
          }
        });
        if (invitedList.length > 0) {
          let invited = invitedList.map(item => item.email);
          this.props
            .dispatch(
              invite(
                {
                  emails: invited,
                  roles: role,
                  id: this.state.currentGroupId,
                  userList: invitedList
                },
                true
              )
            )
            .then(() => {
              if (!oldIds.length) {
                resolve();
              }
            })
            .catch(err =>
              console.error(`Error in GroupCreationModalV3.updateFieldContent.invite.func : ${err}`)
            );
        }
        if (changeRoleList.length > 0) {
          let data = { groupID: this.state.currentGroupId, ids: changeRoleList, role: role };
          this.props
            .dispatch(updateMembers(data))
            .then(() => {
              if (!oldIds.length) {
                resolve();
              }
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModalV3.updateFieldContent.updateMembers.func : ${err}`
              )
            );
        }
        if (changePendingRoleList.length > 0) {
          let payload = { id: changePendingRoleList, isPending: true };
          this.props
            .dispatch(deleteUserFromTeam(payload, this.state.currentGroupId))
            .then(() => {
              let userList = [];
              _.forEach(changePendingRoleList, item => {
                userList.push(
                  _.find(pendingUsers, elem => {
                    return elem.id == item;
                  })
                );
              });
              let emailList = userList.map(user => user.email);
              this.props.dispatch(
                invite(
                  {
                    emails: emailList,
                    roles: role,
                    id: this.state.currentGroupId,
                    userList
                  },
                  true
                )
              );
              if (!oldIds.length) {
                resolve();
              }
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModalV3.updateFieldContent.deleteUserFromTeam.func : ${err}`
              )
            );
        }
      }
      if (!!oldIds.length) {
        if (role === 'member') {
          let payload = {
            user_ids: oldIds
          };
          deleteUsersFromTeam(payload, this.state.currentGroupId)
            .then(() => {
              resolve();
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModalV3.updateFieldContent.deleteUsersFromTeam.func : ${err}`
              )
            );
        } else {
          deleteTeamUsers(this.state.currentGroupId, { user_ids: oldIds, role: role })
            .then(() => {
              resolve();
            })
            .catch(err =>
              console.error(
                `Error in GroupCreationModal.updateFieldContent.deleteTeamUsers.func : ${err}`
              )
            );
        }
      }
      if (!newAdminIds.length && !oldIds.length) {
        resolve();
      }
    });
  }

  updateChannels = async () => {
    return new Promise(resolve => {
      let newChannesIds = this.state.channelsCurrent
        .filter(item => {
          return item.hasOwnProperty('isNew');
        })
        .map(item => item.id);
      let oldChannelsIds = this.state.channelsRemove.map(item => item.id);
      let allPromises = [];
      if (!!newChannesIds.length) {
        newChannesIds.map(channel => {
          let payload = {
            channel: { followers: { group_ids: [this.state.currentGroupId] } }
          };
          allPromises.push(
            new Promise((resolve2, reject) => {
              channelSDK
                .addFollowers(channel, payload)
                .then(() => {
                  resolve2();
                })
                .catch(err => {
                  console.error(`Error in GroupCreationModalV3.addFollowers.func : ${err}`);
                });
            })
          );
        });
      }
      if (!!oldChannelsIds.length) {
        oldChannelsIds.map(channel => {
          allPromises.push(
            new Promise((resolve3, reject) => {
              channelSDK
                .groupsUnfollowChannel(channel, { 'ids[]': this.state.currentGroupId })
                .then(() => {
                  resolve3();
                })
                .catch(err => {
                  console.error(
                    `Error in GroupCreationModalV3.groupsUnfollowChannel.func : ${err}`
                  );
                });
            })
          );
        });
      }
      Promise.all(allPromises)
        .then(() => {
          resolve();
        })
        .catch(err => {
          console.error(`Error in GroupCreationModalV3V3.Promise.all.func : ${err}`);
        });
    });
  };

  updateAllFields = async id => {
    let roles = { member: 'member', leader: 'admin', admin: 'sub_admin' };
    for (let item in roles) {
      if (roles.hasOwnProperty(item)) {
        let newAdminIds = this.getNewTagsIds(item);
        if (newAdminIds && !!newAdminIds.length) {
          let invitedList = [];
          _.forEach(newAdminIds, user => {
            invitedList.push(
              _.find(this.state[`${item}sCurrent`], elem => {
                return elem.id == user;
              })
            );
          });
          let invited = invitedList.map(user => user.email);
          await this.props.dispatch(
            invite(
              {
                emails: invited,
                roles: roles[item],
                id: this.state.currentGroupId,
                userList: invitedList
              },
              true
            )
          );
          this.updateChannels();
        }
      }
    }
  };

  handleContentAutoAssignment = event => {
    this.setState({
      auto_assign_content: event.target.checked
    });
  };

  handleOpenGroup = () => {
    this.setState(prevState => {
      return { groupIsOpen: !prevState.groupIsOpen };
    });
  };

  handleShouldShareToAdmin = () => {
    this.setState(prevState => {
      return { shouldShareToAdmin: !prevState.shouldShareToAdmin };
    });
  };

  handleDynamicGroup = () => {
    this.setState(prevState => ({
      groupIsDynamic: !prevState.groupIsDynamic,
      isOpenDynamicSection: !prevState.groupIsDynamic,
      previewFilter: prevState.groupIsDynamic ? [] : prevState.previewFilter
    }));
  };

  handleMandatoryGroup = () => {
    this.setState(prevState => {
      return { isMandatory: !prevState.isMandatory };
    });
  };

  handleAddition(role, tag) {
    let itemsRemove = this.state[`${role}sRemove`];
    let oldList = _.remove(itemsRemove, function(item) {
      return item.id.toString() === tag.id.toString();
    });
    tag.type = role;
    if (oldList.length === 0) {
      tag.isNew = true;
    }
    let current = this.state[`${role}sCurrent`];
    current.push(tag);
    this.setState({ [`${role}sCurrent`]: current, [`${role}sRemove`]: itemsRemove });
  }

  handleDelete = (role, i) => {
    let current = this.state[`${role}sCurrent`].slice(0);
    let itemsRemove = this.state[`${role}sRemove`];
    let remove = current.splice(i, 1);
    if (remove[0] && !remove[0].isNew) {
      itemsRemove.push({ id: remove[0].id, name: this.concatLabel(remove[0]) });
    }
    this.setState({ [`${role}sCurrent`]: current, [`${role}sRemove`]: itemsRemove });
  };

  getOptions = (role, input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchUserText;
    let user_ids = this.state[`${role}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('email');
      })
      .map(item => {
        return item.id;
      });
    this.setState(
      {
        searchUserText: input,
        [`${role}sOptions`]: [],
        [`${role}sOffset`]:
          isScrollToBottom && !isChanged
            ? this.state[`${role}sOffset`] + this.state.suggestionsLimit
            : 0
      },
      () => {
        usersSDK
          .getItems({
            q: input,
            limit: this.state.suggestionsLimit || 10,
            offset: this.state[`${role}sOffset`] || 0,
            'exclude_user_ids[]': user_ids
          })
          .then(users => {
            let options = users.items.filter(
              item => !~_.findIndex(this.state[`${role}sCurrent`], user => +user.id === +item.id)
            );

            if (role === 'member') {
              options = users.items.filter(
                item =>
                  !~_.findIndex(this.state.adminsCurrent, admin => +admin.id === +item.id) &&
                  !~_.findIndex(this.state.leadersCurrent, leader => +leader.id === +item.id)
              );
            }
            options = options.map(item => {
              return { id: item.id, name: this.concatLabel(item), email: item.email };
            });
            this.setState({
              [`${role}sOptions`]: options
            });
          })
          .catch(err => {
            console.error(`Error in GroupCreationModalV3.usersSDK.getItems.func : ${err}`);
          });
      }
    );
  };

  getChannels = (isScrollToBottom, input) => {
    let isChanged = input !== this.state.searchUserText;
    this.setState(
      {
        searchUserText: input,
        channelsOptions: [],
        channelsOffset:
          isScrollToBottom && !isChanged
            ? this.state.channelsOffset + this.state.suggestionsLimit
            : 0
      },
      () => {
        if (this.state.isForUpdate) {
          channelsV2Sdk
            .getAddableChannels(this.state.currentGroupId, {
              q: input,
              limit: this.state.suggestionsLimit || 10,
              offset: this.state.channelsOffset || 0
            })
            .then(data => {
              let options = data.channels.filter(
                item =>
                  !~_.findIndex(this.state.channelsCurrent, channel => +channel.id === +item.id)
              );
              options = options.map(item => {
                return { id: item.id, name: item.label };
              });
              this.setState({
                channelsOptions: options
              });
            })
            .catch(err => {
              console.error(`Error in GroupCreationModalV3.getChannels.func : ${err}`);
            });
        } else {
          channelsV2Sdk
            .fetchChannels({
              q: input,
              limit: this.state.suggestionsLimit || 10,
              offset: this.state.channelsOffset || 0
            })
            .then(data => {
              let options = data.filter(
                item =>
                  !~_.findIndex(this.state.channelsCurrent, channel => +channel.id === +item.id)
              );
              options = options.map(item => {
                return { id: item.id, name: item.label };
              });
              this.setState({
                channelsOptions: options
              });
            })
            .catch(err => {
              console.error(`Error in GroupCreationModalV3.getSearchChannels.func : ${err}`);
            });
        }
      }
    );
  };

  concatLabel = item => {
    let label;
    if (item.firstName || item.lastName) {
      label = `${item.firstName ? item.firstName : ''} ${item.lastName ? item.lastName : ''} ${
        item.email
      }`;
    } else {
      label = `${item.name ? item.name : ''} ${item.email}`;
    }
    return label;
  };

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(
      prevState => {
        return {
          showAdSettings: !prevState.showAdSettings
        };
      },
      () => {
        const list = document.getElementById('re-arrange-carousels-group');
        Sortable.create(list, {
          handle: '.carousel-move-handler',
          animation: 150,
          forceFallback: true,
          onUpdate: this.changeOrder
        });
      }
    );
  };

  changeOrder = e => {
    let carousels = this.state.carousels;
    let new_carousel_position = carousels.findIndex(o => o.index === e.newIndex);
    carousels[new_carousel_position].index = e.oldIndex;
    let old_carousel_position = carousels.findIndex(o => o.default_label === e.item.classList[2]);
    carousels[old_carousel_position].index = e.newIndex;
    this.setState({
      carousels: carousels,
      isCarouselsModified: true
    });
  };

  updateGroupCarousels = () => {
    let carousels = this.state.carousels;
    editTeamCarousels(this.state.currentGroupId, { carousels })
      .then(data => {
        this.setState({
          carousels: data.carousels
        });
        this.props.dispatch(updateGroupCarouselsOrder(this.state.currentGroupId, true));
      })
      .catch(err => {
        console.error(`Error in GroupCreationModalV3.updateGroupCarousels.func : ${err}`);
      });
  };

  openDynamicSection = e => {
    e.preventDefault();
    this.setState(state => ({ isOpenDynamicSection: !state.isOpenDynamicSection }));
  };

  getPreviewList = data => {
    this.setState({
      previewFilter: data
    });
  };

  getUsers = async (idsOnly, offset = 0) => {
    if (this.state.previewFilter || (this.props.modal && this.props.modal.filter)) {
      let filters = this.state.previewFilter;
      let payload = { custom_fields: [], limit: 20, q: '' };
      if (idsOnly) {
        payload.only_user_ids = true;
      }
      filters.forEach(filter => {
        let options = [];
        if (filter.options && filter.options.length) {
          payload.limit = this.usersLimit;
          payload.offset = offset;
          filter.options.forEach(option => {
            if (option.checked) {
              options.push(option.option);
            }
          });
          payload.custom_fields.push({ name: filter.type, values: options });
        }
      });
      if (payload.custom_fields.length) {
        payload.custom_fields = JSON.stringify(payload.custom_fields);
      }
      return await usersSDK.searchForUsers(payload);
    } else {
      return [];
    }
  };

  openPreview = async () => {
    let data = await this.getUsers();
    usersSDK
      .getUserCustomFields({ 'user_ids[]': data.users.map(item => item.id), send_array: true })
      .then(customFieldsData => {
        let previewData = data.users.map(item => ({
          ...item,
          customFieldsData: customFieldsData.find(customField => customField.id == item.id)
        }));
        this.setState({
          previewData,
          showPreview: true
        });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModalV3.getPreviewList.getAllCustomFields.func : ${err}`
        );
      });
  };

  backHandle = () => {
    this.isAllPreviewLoaded = false;
    this.setState({
      showCreateGroupModal: false,
      showPreview: false
    });
  };

  loadMoreUsers = async () => {
    this.setState({ isPendingPreview: true });
    let data = await this.getUsers(false, this.state.previewData.length);
    this.isAllPreviewLoaded = data.users.length < this.usersLimit;
    let previewData = this.state.previewData.concat(data.users);
    usersSDK
      .getUserCustomFields({ 'user_ids[]': previewData.map(item => item.id), send_array: true })
      .then(customFieldsData => {
        previewData = previewData.map(item => ({
          ...item,
          customFieldsData: customFieldsData.find(customField => customField.id == item.id)
        }));
        this.setState({
          isPendingPreview: false,
          previewData
        });
      })
      .catch(err => {
        console.error(
          `Error in GroupCreationModalV3.getPreviewList.getAllCustomFields.func : ${err}`
        );
      });
  };

  carouselVisibilityToggle = (default_label, e) => {
    let carousels = this.state.carousels;
    carousels.find(o => o.default_label === default_label).visible = e.target.checked;
    this.setState({
      carousels: carousels,
      isCarouselsModified: true
    });
  };

  render() {
    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    let createGroupAdmin =
      this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')
        ? true
        : Permissions.has('CREATE_GROUP_ADMIN');
    let enableDynamicSelectionForRole =
      (Permissions['enabled'] !== undefined && Permissions.has('USE_DYNAMIC_SELECTION')) ||
      (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'));
    return this.state.showPreview ? (
      <PreviewDSModal
        backHandle={this.backHandle.bind(this)}
        previewData={this.state.previewData}
        loadMoreUsers={this.loadMoreUsers}
        isPendingPreview={this.state.isPendingPreview}
        isAllPreviewLoaded={this.isAllPreviewLoaded}
        fromGroupCreation={true}
        previewFilter={this.state.previewFilter}
      />
    ) : (
      <div className="group-creation-modal">
        <div className="modal-header">
          <TextField autoFocus={true} name="groupcreation" className="hiddenTextField" />
          <span className="header-title">{tr('Group')}</span>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={this.styles.closeIcon}
              onTouchTap={this.closeModal}
            >
              <CloseIcon color="white" />
            </IconButton>
          </div>
        </div>
        <div className="container-padding vertical-spacing-large container__v2">
          <div className="input-row__margin-bottom">{this.displayImageInput()}</div>
          <div className="input-row__margin-bottom input-row__right-align">
            {useFormLabels && (
              <label required htmlFor="form-title" style={{ float: 'left' }}>
                {tr('Title')}
              </label>
            )}
            <input
              id="form-title"
              className="group-name group-header"
              type="text"
              placeholder={useFormLabels ? '' : `${tr('Title')}*`}
              maxLength={charForName}
              value={this.state.name}
              onChange={e => {
                this.setState({ name: e.target.value, createErrorText: '' });
              }}
            />
            <small>{`${charForName - this.state.name.length}/${charForName} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
          <div className="input-row__margin-bottom input-row__right-align">
            {useFormLabels && (
              <label required htmlFor="form-description" style={{ float: 'left' }}>
                {tr('Description')}
              </label>
            )}

            <input
              id="form-description"
              className="group-description group-header"
              placeholder={useFormLabels ? '' : `${tr('Description')}*`}
              maxLength={2000}
              value={this.state.description}
              onChange={e => {
                this.setState({ description: e.target.value });
              }}
            />
            <small>{`${charForDescription -
              this.state.description.length}/${charForDescription} ${tr(
              'Characters Remaining'
            )}`}</small>
          </div>
          <div>
            <div className="input-tags-row_v3">
              {useFormLabels && <label htmlFor="form-group-leader">{tr('Group Leader')}</label>}

              <ReactTags
                id="form-group-leader"
                tags={this.state.leadersCurrent}
                suggestions={this.state.leadersOptions}
                placeholder={
                  useFormLabels ? '' : this.state.leadersCurrent.length ? '' : tr('Group Leader')
                }
                handleAddition={this.handleAddition.bind(this, 'leader')}
                handleDelete={this.handleDelete.bind(this, 'leader')}
                handleInputChange={this.getOptions.bind(this, 'leader')}
                tagComponent={CustomTag}
                autofocus={false}
              />
              {!!this.state.leadersCurrent.length && <small>{tr('Group Leader')}</small>}
            </div>
            {createGroupAdmin && (
              <div className="input-tags-row_v3">
                {useFormLabels && <label htmlFor="form-group-admin">{tr('Group Admin')}</label>}

                <ReactTags
                  id="form-group-admin"
                  tags={this.state.adminsCurrent}
                  suggestions={this.state.adminsOptions}
                  placeholder={
                    useFormLabels ? '' : this.state.adminsCurrent.length ? '' : tr('Group Admin')
                  }
                  handleAddition={this.handleAddition.bind(this, 'admin')}
                  handleDelete={this.handleDelete.bind(this, 'admin')}
                  handleInputChange={this.getOptions.bind(this, 'admin')}
                  tagComponent={CustomTag}
                  autofocus={false}
                />
                {!!this.state.adminsCurrent.length && <small>{tr('Group Admin')}</small>}
              </div>
            )}
            <div
              className={`input-tags-row_v3 ${
                this.state.groupIsDynamic ? ' members-field__disabled' : ''
              }`}
            >
              {useFormLabels && <label htmlFor="form-group-member">{tr('Group Member')}</label>}
              <ReactTags
                id="form-group-member"
                tags={this.state.membersCurrent}
                suggestions={this.state.membersOptions}
                placeholder={
                  useFormLabels ? '' : this.state.membersCurrent.length ? '' : tr('Group Member')
                }
                handleAddition={this.handleAddition.bind(this, 'member')}
                handleDelete={this.handleDelete.bind(this, 'member')}
                handleInputChange={this.getOptions.bind(this, 'member')}
                tagComponent={CustomTag}
                autofocus={false}
              />
              {!!this.state.membersCurrent.length && !this.state.groupIsDynamic && (
                <small>{tr('Group Members')}</small>
              )}
              {this.state.groupIsDynamic && (
                <small>{`${tr(
                  'For dynamic groups, members can be added using filters only'
                )}`}</small>
              )}
            </div>
            <div className="input-tags-row_v3">
              {useFormLabels && <label htmlFor="form-channels">{tr('Channels')}</label>}

              <ReactTags
                id="form-channels"
                tags={this.state.channelsCurrent}
                suggestions={this.state.channelsOptions}
                placeholder={
                  useFormLabels ? '' : this.state.channelsCurrent.length ? '' : tr('Channels')
                }
                handleAddition={this.handleAddition.bind(this, 'channel')}
                handleDelete={this.handleDelete.bind(this, 'channel')}
                handleInputChange={this.getChannels.bind(this, 'channel')}
                tagComponent={CustomTag}
                autofocus={false}
              />
              {!!this.state.channelsCurrent.length && <small>{tr('Channels')}</small>}
            </div>

            <div style={{ display: 'flex' }}>
              <div style={{ display: 'inline-flex' }}>Who can share:</div>
              <div style={{ display: 'inline-flex', marginLeft: '15px' }}>
                <Checkbox
                  className="share-admin-checkbox"
                  iconStyle={{ fontSize: '0.375rem' }}
                  checked={
                    this.state.shouldShareToAdmin !== undefined ||
                    this.state.shouldShareToAdmin != null
                      ? this.state.shouldShareToAdmin
                      : false
                  }
                  onCheck={this.handleShouldShareToAdmin.bind(this)}
                  label={
                    <div>
                      <p className="group-checkbox-label">{tr('Admin(s)')}</p>
                    </div>
                  }
                  uncheckedIcon={<div className="unchecked" />}
                  aria-label={tr('Share to the admin')}
                />
                <Checkbox
                  className="share-admin-checkbox"
                  iconStyle={{ fontSize: '0.375rem' }}
                  checked={
                    this.state.shouldShareToAdmin !== undefined ||
                    this.state.shouldShareToAdmin != null
                      ? !this.state.shouldShareToAdmin
                      : false
                  }
                  onCheck={this.handleShouldShareToAdmin.bind(this)}
                  label={
                    <div>
                      <p className="group-checkbox-label">{tr('Member(s)')}</p>
                    </div>
                  }
                  uncheckedIcon={<div className="unchecked" />}
                  aria-label={tr('Share to the members')}
                />
              </div>
            </div>
          </div>
          {!this.state.isDynamic && (
            <div>
              <Checkbox
                iconStyle={{ fontSize: '0.375rem' }}
                className="group__checkbox group-checkbox__top"
                checked={this.state.groupIsOpen}
                onCheck={this.handleOpenGroup.bind(this)}
                label={
                  <div>
                    <p className="group-checkbox-label">{tr('Open Group')}</p>
                    <p className="group-checkbox-label group-checkbox-label__thin">
                      {tr('Users will be able to find and join the group')}
                    </p>
                  </div>
                }
                uncheckedIcon={<div className="unchecked" />}
                aria-label={tr('Open Group (Can be Searched)')}
              />
              {this.state.workWithMandatoryGroup && !this.state.groupIsOpen && (
                <Checkbox
                  className="group__checkbox group-checkbox__top"
                  checked={this.state.isMandatory}
                  onCheck={this.handleMandatoryGroup.bind(this)}
                  label={
                    <div>
                      <p className="group-checkbox-label">{tr('Mandatory group')}</p>
                      <p className="group-checkbox-label group-checkbox-label__thin">
                        {tr(
                          "Users won't be able to leave the group once added, only group admin/leader can remove users"
                        )}
                      </p>
                    </div>
                  }
                  uncheckedIcon={<div className="unchecked" />}
                  aria-label={tr('Mandatory group')}
                />
              )}
            </div>
          )}
          <div>
            <div className="advanced-settings">
              <a
                href="#"
                className="advanced-label advanced-label__big"
                onClick={this.toggleAdvancedSettings}
              >
                {tr('Advanced Settings')}
              </a>
              {this.state.showAdSettings && (
                <div className="advanced-settings-container">
                  {this.state.dynamicSelections &&
                    enableDynamicSelectionForRole &&
                    !this.state.isDynamic && (
                      <div className="group__checkbox" style={{ position: 'relative' }}>
                        <Checkbox
                          className="group__checkbox group-checkbox__top group-checkbox-dynamic"
                          labelStyle={{ width: '23rem !important' }}
                          checked={this.state.groupIsDynamic}
                          onCheck={this.handleDynamicGroup.bind(this)}
                          label={
                            <div>
                              <p className="group-checkbox-label">{tr('Dynamic Group')}</p>
                              <p className="group-checkbox-label group-checkbox-label__thin">
                                {tr(
                                  'Group members will be auto maintained based on provided criteria. '
                                )}
                              </p>
                            </div>
                          }
                          uncheckedIcon={<div className="unchecked" />}
                          aria-label={tr('Dynamic')}
                        />
                        <a
                          className={`select-filters-link ${
                            this.state.groupIsDynamic ? '' : 'select-filters-link__disabled'
                          }`}
                          onClick={e => this.openDynamicSection(e)}
                        >
                          {tr('Select filters')}
                        </a>
                        {this.state.isOpenDynamicSection && (
                          <div className="dynamic-container-v3">
                            <DynamicSelections
                              getPreviewList={this.getPreviewList}
                              filterContent={this.state.previewFilter}
                              isGroupModal={true}
                            />
                            {this.state.previewFilter.length > 1 && (
                              <SecondaryButton
                                style={this.styles.previewButton}
                                label={tr('Preview Data')}
                                onTouchTap={this.openPreview}
                              />
                            )}
                          </div>
                        )}
                      </div>
                    )}
                  <Checkbox
                    className="group__checkbox group-checkbox__top"
                    checked={this.state.auto_assign_content}
                    onCheck={this.handleContentAutoAssignment.bind(this)}
                    label={
                      <div>
                        <p className="group-checkbox-label">
                          {tr('Auto assign content of this group members')}
                        </p>
                        <p className="group-checkbox-label group-checkbox-label__thin">
                          {tr('Only Group admin and leaders can assign and share to this group')}
                        </p>
                      </div>
                    }
                    uncheckedIcon={<div className="unchecked" />}
                    aria-label={tr('Auto assign content of this group members')}
                  />
                  <div style={this.styles.carouselsPosition}> Carousels </div>

                  <div id="re-arrange-carousels-group">
                    {this.state.carousels.map(item => {
                      return (
                        <div
                          className={`group-carousel group-checkbox-label ${item.default_label}`}
                        >
                          <div className="group-carousel-list">
                            <div className="carousel-move-handler">
                              <span tabIndex={-1} className="hideOutline">
                                <MoveIcon style={this.styles.moveIcon} color="grey" />
                              </span>
                            </div>
                            <Toggle
                              labelPosition="left"
                              label={tr(item.default_label)}
                              aria-label={tr(item.default_label)}
                              onToggle={this.carouselVisibilityToggle.bind(
                                this,
                                item.default_label
                              )}
                              defaultToggled={item.visible}
                              style={{ width: '44%', display: 'inline-block' }}
                            />
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="text-center error-text"> {tr(this.state.createErrorText)}</div>
          <div className="action-bar">
            <SecondaryButton
              label={tr('Cancel')}
              style={{ marginRight: '0.75rem' }}
              className="cancel"
              onTouchTap={this.closeModal}
            />
            <PrimaryButton
              label={this.state.isForUpdate ? tr('Update Group') : tr('Create Group')}
              className="create"
              onTouchTap={this.createClickHandler}
              disabled={
                !this.state.description ||
                !this.state.name ||
                (this.state.groupIsDynamic &&
                  !this.state.previewFilter.filter(item => item.id > -1).length)
              }
              pending={this.state.pending}
              pendingLabel={this.state.isForUpdate ? tr('Updating...') : tr('Creating...')}
            />
          </div>
        </div>
      </div>
    );
  }
}

GroupCreationModalV3.propTypes = {
  groupsV2: PropTypes.object,
  modal: PropTypes.object,
  currentUser: PropTypes.object,
  updateGroupsList: PropTypes.func,
  groupId: PropTypes.any,
  pathname: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    modal: state.modal.toJS(),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(GroupCreationModalV3);
