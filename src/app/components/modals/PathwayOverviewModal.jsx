import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CardHeader } from 'material-ui/Card';
import unescape from 'lodash/unescape';
import CommentList from '../../components/feed/CommentList.v2';
import CreationDate from '../common/CreationDate';
import colors from 'edc-web-sdk/components/colors/index';
import {
  startAssignment,
  loadComments,
  toggleLikeCardAsync,
  updateCurrentCard
} from '../../actions/cardsActions';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import RightArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import { fetchCard, submitProjectCard } from 'edc-web-sdk/requests/cards';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { close, closeCard, openCardStatsModal } from '../../actions/modalActions';
import { recordVisit } from 'edc-web-sdk/requests/analytics';

import MainInfoSmartBite from '../common/MainInfoSmartBite';

import SvgImageResized from '../common/ImageResized';
import Spinner from '../common/spinner';
import CardListViewPreview from '../cards/CardListViewPreview';
import CardOverviewModal from './CardOverviewModal';
import find from 'lodash/find';
import { reOpenCurateModal } from '../../actions/channelsActionsV2';
import { updateRelevancyRatingQ, checkRatedCard } from '../../actions/relevancyRatings';
import { markAsComplete } from 'edc-web-sdk/requests/cards.v2';
import getCardType from '../../utils/getCardType';
import linkPrefix from '../../utils/linkPrefix';
import BlurImage from '../common/BlurImage';
import {
  videoTranscode,
  uploadPolicyAndSignature,
  refreshFilestackUrl
} from 'edc-web-sdk/requests/filestack';
import FlatButton from 'material-ui/FlatButton';
import VideoIcon from 'edc-web-sdk/components/icons/Video';
import TextFieldCustom from '../common/SmartBiteInput';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import { open as openSnackBar } from '../../actions/snackBarActions';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import { filestackClient } from '../../utils/filestack';
import { fetchSumissionList } from 'edc-web-sdk/requests/submission';
import { fetchPathway } from 'edc-web-sdk/requests/pathways';

const lightPurp = '#acadc1';
const videoType = ['video/mp4', 'video/ogg', 'video/webm', 'video/quicktime', 'video/ogv'];
class PathwayOverviewModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pathway: props.card,
      pendingLike: false,
      loadingPackCards: false,
      loadChecked: false,
      showCardOverview: false,
      checkedCard: null,
      currentIndex: 0,
      isUpvoted: props.card.isUpvoted,
      votesCount: props.card.votesCount,
      commentsCount: props.card.commentsCount,
      autoComplete: true,
      createLabel: 'Submit',
      fileStack: [],
      description: '',
      disableSubmit: true,
      optionsError: '',
      currentCard: {},
      submissionList: [],
      isMarkedAsCompleted: true,
      isInProgressToComplete: false,
      securedUrl: null,
      pathwayStarted: false,
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true),
      videoSubmitted: false
    };
    this.newComplete = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    this.showProjectCard = window.ldclient.variation('project-card', false);
    let config = this.props.team.OrgConfig;
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;
    this.isDownloadContentDisabled =
      props.team &&
      props.team.OrgConfig &&
      props.team.OrgConfig.content &&
      props.team.OrgConfig.content['web/content/disableDownload'] &&
      props.team.OrgConfig.content['web/content/disableDownload'].value;

    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      dropDownAction: {
        paddingRight: 0,
        width: 'auto'
      },
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      svgStyle: {
        filter: 'url(#blur-effect-1)'
      },
      userAvatar: {
        marginRight: 0,
        display: 'inline-block'
      },
      avatar: {
        marginRight: '9px'
      },
      popoverClose: {
        padding: 0,
        width: 'auto',
        height: 'auto',
        float: 'right'
      },
      cardHeader: {
        padding: '8px 0 0',
        float: 'left'
      },
      likeIcon: {
        paddingLeft: 0,
        width: 'auto'
      },
      leftArrow: {
        float: 'left',
        width: '48px',
        height: '48px',
        padding: 0,
        border: 0,
        cursor: 'pointer'
      },
      rightArrow: {
        float: 'right',
        width: '48px',
        height: '48px',
        padding: 0,
        border: 0,
        cursor: 'pointer'
      },
      primary: {
        minWidth: '144px'
      },
      tooltipActiveBts: {
        marginTop: -10,
        marginLeft: -10
      },
      avatarBox: {
        height: '1.625rem',
        width: '1.625rem',
        marginRight: '0.5625rem',
        position: 'relative'
      },
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      actionIcon: {
        paddingLeft: 0,
        paddingRight: 0,
        width: 'auto'
      },
      submissionList: {
        fontSize: '0.7em',
        color: lightPurp
      },
      pricing: {
        fontSize: '0.75rem',
        color: '#454560',
        margin: '10px 0px'
      },
      videoIcon: {
        width: '34px',
        height: '34px',
        marginTop: '-5px'
      },
      hide: {
        width: '0px'
      }
    };
  }

  componentDidMount() {
    this.fetchComments();
    if (this.props.checkedCardId !== null) {
      this.setState({ loadChecked: true }, this.fetchPathwayDetails);
    } else this.fetchPathwayDetails();
  }

  fetchPathwayDetails = () => {
    if (this.props.card && this.props.card.id) {
      let payload = { is_standalone_page: false };
      fetchPathway(this.props.card.id, payload)
        .then(data => {
          this.setState(
            {
              pathway: data,
              securedUrl: data.filestack[0].url
            },
            () => {
              let autoComplete = true;
              if (this.newComplete) {
                switch (this.completeMethodConf) {
                  case 'creatorChoose':
                    autoComplete =
                      this.state.pathway.autoComplete !== undefined
                        ? this.state.pathway.autoComplete
                        : true;
                    break;
                  case 'creatorNotChoose':
                    autoComplete = true;
                    break;
                  case 'disabledNext':
                    autoComplete = false;
                    break;
                  default:
                    // FIXME: implement default case
                    break;
                }
              }
              this.setState({ autoComplete });
              if (this.props.checkedCardId !== null) {
                let currentCard = {};
                //To check public card
                if (typeof this.props.checkedCardId === 'string') {
                  currentCard = this.state.pathway.packCards.find(cards => {
                    return cards.id === this.props.checkedCardId;
                  });
                } else {
                  currentCard = this.state.pathway.packCards.find((cards, index) => {
                    return index === this.props.checkedCardId;
                  });
                }
                if (currentCard === undefined) {
                  currentCard = {};
                }
                this.setState({ currentCard: currentCard }, () => {
                  if (this.showProjectCard && this.state.currentCard.projectId) {
                    // should get called only for project card
                    fetchSumissionList({
                      project_id: this.state.currentCard.projectId,
                      filter: 'all'
                    })
                      .then(subList => {
                        if (subList) this.setState({ submissionList: subList.projectSubmissions });
                      })
                      .catch(err => {
                        console.error(
                          `Error in PathwayOverviewModal.fetchSumissionList.func : ${err}`
                        );
                      });
                  }
                });
              }
              this.loadPackCards();
            }
          );
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.fetchPathwayDetails.func : ${err}`);
        });
    }
  };

  fetchComments = () => {
    let cardId = this.state.pathway.cardId || this.state.pathway.id;
    let isECL = /^ECL-/.test(cardId);
    if (!isECL && this.state.commentsCount) {
      loadComments(
        cardId,
        this.state.commentsCount,
        this.state.pathway.cardType,
        cardId ? cardId : null
      )
        .then(
          data => {
            this.setState({ comments: data, showComment: true });
          },
          error => {
            console.warn(`error when loadComments for pathway card ${cardId}. Error: ${error}`);
          }
        )
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.loadComments.func : ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  };

  cardUpdated(card) {
    if (this.props.card) {
      let payload = { is_standalone_page: false };
      fetchPathway(this.props.card.id, payload)
        .then(data => {
          this.setState({
            pathway: data
          });
          this.props.cardUpdated && this.props.cardUpdated();
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.cardUpdated.fetchPathway.func : ${err}`);
        });
    }
  }
  submitClickHandler = cardData => {
    if (!this.state.fileStack[0]) {
      this.setState({ uploadErrorText: 'Please upload a video', disableSubmit: false });
      return;
    }
    if (!this.state.description) {
      this.setState({ disableSubmit: false, optionsError: 'This is required' });
      return;
    }
    let payload = {
      project_submission: {
        description: this.state.description,
        project_id: cardData.projectId,
        filestack: this.state.fileStack
      }
    };
    this.setState({ createLabel: 'Submitting..' });
    submitProjectCard(payload)
      .then(data => {
        this.setState({
          videoSubmitted: true,
          createLabel: 'Submit',
          description: '',
          fileStack: [],
          optionsError: '',
          disableSubmit: true
        });
        this.props.dispatch(openSnackBar('Submission Added', true));
        this.closeModal();
      })
      .catch(err => {
        console.error(`Error in PathwayOverviewModal.submitProjectCard.func : ${err}`);
      });
  };

  smartBiteUpdated = (id, index) => {
    if (id) {
      fetchCard(id)
        .then(data => {
          let smartBites = this.state.smartBites;
          data.isCompleted =
            data.completionState && data.completionState.toUpperCase() === 'COMPLETED';
          smartBites[index] = data;
          this.setState({ smartBites }, this.props.dispatch(updateCurrentCard(data)));
          this.setState({
            checkedCard: this.state.smartBites[this.state.currentIndex]
          });
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.smartBiteUpdated.func : ${err}`);
        });
    }
  };

  closeModal = async () => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    this.props.cardUpdated && this.props.cardUpdated();
    if (this.state.showCardOverview) {
      let index = this.state.currentIndex;
      this.changeCompleteStatus(this.state.smartBites[index].id, index);
    }
    this.props.dispatch(closeCard());
    if (
      !checkRatedCard(this.props.card, this.props.relevancyRating.ratedQueue) &&
      this.state.relevancyRatings
    ) {
      this.props.dispatch(updateRelevancyRatingQ(this.props.card));
    }
    if (this.props.modal.dataBefore == undefined) {
      let channelId = this.props.channelsV2.activeChannelId;
      let cardsData = {
        cards: this.props.channelsV2[channelId].curateCards,
        total: this.props.channelsV2[channelId].curatedCardsTotal
      };
      this.props.dispatch(reOpenCurateModal(this.props.channelsV2[channelId], cardsData));
    }
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise(resolve => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.asyncDispatch.func : ${err}`);
        });
    });
  };

  likeCardHandler = () => {
    if (this.state.pendingLike) {
      return;
    }
    this.setState({ pendingLike: true }, () => {
      this.asyncDispatch(
        toggleLikeCardAsync,
        this.state.pathway.id,
        this.state.pathway.cardType,
        !this.state.isUpvoted
      )
        .then(() => {
          this.cardUpdated();
          this.setState({
            votesCount: Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0),
            isUpvoted: !this.state.isUpvoted,
            pendingLike: false
          });
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.toggleLikeCardAsync.func : ${err}`);
        });
    });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count }, () => {
      if (count === -1) {
        this.setState({ showComment: false }, this.fetchComments);
      }
    });
  };

  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}` ||
      this.props.isStandaloneModal
    ) {
      return;
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };

  linkToPush = (card, handle, e) => {
    e && e.preventDefault();
    this.props.dispatch(close());
    closeCard();
    if (card) {
      this.props.cardUpdated && this.props.cardUpdated();
      this.standaloneLinkClickHandler(card);
    } else if (handle) {
      this.props.dispatch(push(`/${handle}`));
    }
  };

  loadPackCards = () => {
    if (this.state.pathway.packCards && this.state.pathway.packCards.length) {
      this.setState({ loadingPackCards: true }, function() {
        let smartBites = [];
        let countCard = 0;
        for (let i = 0; i < this.state.pathway.packCards.length; i++) {
          countCard++;
          if (
            this.state.pathway.packCards[i].message === 'You are not authorized to access this card'
          ) {
            this.state.pathway.packCards[i].isPrivate = true;
          }
          smartBites[i] = this.state.pathway.packCards[i];
          if (countCard === this.state.pathway.packCards.length) {
            smartBites.forEach((item, index) => {
              item.isLocked = this.state.pathway.packCards[index].locked;
            });
          }
          this.setState({ smartBites });
          if (countCard === this.state.pathway.packCards.length) {
            let checkedCardId = this.props.checkedCardId !== null || this.state.checkedCardId;
            if (checkedCardId) {
              let checkedCurrentCardId = this.props.checkedCardId || this.state.checkedCardId;
              let findCard = find(smartBites, el => el.id == checkedCurrentCardId);
              let index;
              //To check public card
              if (findCard) {
                index = smartBites.indexOf(findCard);
              } else {
                findCard = this.state.currentCard;
                index = this.props.checkedCardId;
              }
              this.setState(
                { checkedCardId: null, loadChecked: false },
                this.chooseCard.bind(this, findCard, index)
              );
            }
          }

          if (i === this.state.pathway.packCards.length - 1) {
            this.setState({ loadingPackCards: false });
          }
        }
      });
    }
  };

  chooseCard = (card, i, e) => {
    if (card.cardType == 'journey') {
      window.open('/journey' + `/${card.slug}`, '_blank');
      return;
    }
    if (e && e.target && e.target.name === 'marked-link') {
      return;
    }
    this.setState({
      currentIndex: i,
      showCardOverview: true,
      loadChecked: false,
      checkedCard: card
    });
  };

  setStartedReview = () => {
    let isStandalone = !!~this.props.pathname.indexOf('pathways/');
    if (this.state.pathway.completionState === null && isStandalone && !this.state.pathwayStarted) {
      startAssignment(this.state.pathway.id)
        .then(userContentCompletion => {
          if (userContentCompletion && userContentCompletion.state === 'started') {
            this.setState({
              pathwayStarted: true
            });
          }
        })
        .catch(err => {
          console.error(`Error in PathwayOverviewModal.setStartedReview.func : ${err}`);
        });
    }
  };

  arrowClick = async direction => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    this.setState({ loadingPackCards: false });
    let oldIndex = this.state.currentIndex;
    let smartBites = this.state.smartBites;
    let card = smartBites[oldIndex];
    let cardType = getCardType(card);
    if (
      cardType === 'QUIZ' &&
      card.attemptedOption &&
      card.leaps &&
      card.leaps.inPathways &&
      direction === 1
    ) {
      let correctAnswer = card.quizQuestionOptions.find(obj => {
        return obj.isCorrect === true;
      });
      let selected = card.attemptedOption && card.attemptedOption.id;
      let result = correctAnswer && selected === correctAnswer.id;
      let inPathwayLeap = card.leaps.inPathways.find(
        item => item.pathwayId == this.state.pathway.id
      );
      let nextCardId = result ? inPathwayLeap.correctId : inPathwayLeap.wrongId;
      if (nextCardId) {
        let newIndex = smartBites.indexOf(smartBites.find(item => item.id == nextCardId));
        this.changeCompleteStatus(smartBites[oldIndex].id, oldIndex);
        this.setState({
          currentIndex: newIndex,
          checkedCard: smartBites[newIndex]
        });
      }
    } else {
      if (this.state.currentIndex === 0 && direction === -1) {
        this.backToPathway();
      } else if (this.state.currentIndex + 1 === this.state.smartBites.length && direction === 1) {
        this.linkToPush(this.state.pathway);
      } else {
        let newIndex = this.state.currentIndex + direction;
        if (newIndex >= 0 && newIndex <= this.state.smartBites.length - 1) {
          this.changeCompleteStatus(this.state.smartBites[oldIndex].id, oldIndex);
          this.setState(
            {
              currentIndex: newIndex,
              checkedCard: this.state.smartBites[newIndex]
            },
            () => {
              try {
                recordVisit(this.state.checkedCard.id);
              } catch (e) {}
            }
          );
        }
      }
    }
  };

  backToPathway = () => {
    let index = this.state.currentIndex;
    this.changeCompleteStatus(this.state.smartBites[index].id, index);
    this.setState({
      currentIndex: 0,
      showCardOverview: false,
      checkedCard: null
    });
  };

  startClick = () => {
    if (this.state.pathway && this.state.pathway.completionState === null) {
      startAssignment(this.state.pathway.id);
    }
    if (this.state.smartBites && this.state.smartBites.length) {
      this.chooseCard(this.state.smartBites[0], 0);
    } else {
      this.setState(
        { loadChecked: true, checkedCardId: this.state.pathway.packCards[0].id },
        this.fetchPathwayDetails
      );
    }
  };

  async checkToCompletedCard() {
    return new Promise((resolve, reject) => {
      if (this.state.currentCard.cardType === 'project') {
        resolve(true);
      }
      let isCompleted =
        this.state.checkedCard &&
        this.state.checkedCard.completionState &&
        this.state.checkedCard.completionState.toUpperCase() === 'COMPLETED';

      let cardType = getCardType(this.state.checkedCard);

      let isLinkCard =
        !this.state.showMarkAsComplete &&
        this.state.checkedCard &&
        this.state.checkedCard.resource &&
        (this.state.checkedCard.resource.url ||
          this.state.checkedCard.resource.description ||
          this.state.checkedCard.resource.fileUrl) &&
        this.state.checkedCard.resource.type !== 'Video' &&
        ((this.state.checkedCard.readableCardType &&
          this.state.checkedCard.readableCardType.toUpperCase() === 'ARTICLE') ||
          cardType === 'ARTICLE');

      if (
        !isCompleted &&
        this.state.autoComplete &&
        this.state.checkedCard &&
        (this.state.checkedCard.cardType !== 'poll' || this.state.checkedCard.hasAttempted) &&
        !this.state.checkedCard.isPrivate &&
        !this.state.checkedCard.isLocked &&
        !isLinkCard
      ) {
        try {
          this.setState({ isInProgressToComplete: true }, async () => {
            await markAsComplete(this.state.checkedCard.id, { state: 'complete' }).then(
              completedCardStatus => {
                let checkedCard = this.state.checkedCard;
                if (completedCardStatus && completedCardStatus.state) {
                  checkedCard.completionState = completedCardStatus.state.toUpperCase();
                }
                this.setState({
                  checkedCard: checkedCard,
                  completedCardStatus: completedCardStatus,
                  isMarkedAsCompleted: true,
                  isInProgressToComplete: false
                });
                resolve(true);
              }
            );
          });
        } catch (error) {
          console.error(`Error in PathwayOverviewModal.checkToCompletedCard.func : ${error}`);
          reject();
        }
      } else {
        resolve(true);
      }
    });
  }

  changeCompleteStatus = (id, i) => {
    let smartBites = this.state.smartBites;
    if (
      this.state.completedCardStatus &&
      this.state.completedCardStatus.completableId == id &&
      !this.state.checkedCard.isLocked
    ) {
      smartBites[i].isCompleted = true;
      smartBites[i].completionState = 'COMPLETED';
    }
    this.setState({ smartBites }, this.props.dispatch(updateCurrentCard(smartBites[i])));
    this.setState({
      checkedCard: this.state.smartBites[this.state.currentIndex]
    });
  };

  descriptionCardChange = event => {
    this.setState({ description: event.target.value, optionsError: '' });
  };

  fileStackHandler = (accept, callback) => {
    let fromSources = [
      'local_file_system',
      'googledrive',
      'dropbox',
      'box',
      'github',
      'gmail',
      'onedrive',
      'webcam'
    ];

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: videoType,
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources: fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(
              `Error in PathwayOverviewModal.fileStackHandler.filestackClient.func : ${err}`
            );
          });
        this.setState({ uploadErrorText: '' });
      })
      .catch(err => {
        console.error(
          `Error in PathwayOverviewModal.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  addFileStackFiles(fileStack) {
    let securedUrl;
    let videoPreTranscodeUrl;
    refreshFilestackUrl(fileStack.filesUploaded[0].url)
      .then(resp => {
        securedUrl = resp.signed_url;
        if (
          fileStack.filesUploaded[0].mimetype &&
          ~fileStack.filesUploaded[0].mimetype.indexOf('video')
        ) {
          videoTranscode(fileStack.filesUploaded[0].handle)
            .then(data => {
              this.setState({
                transcodedVideoUrl: data.status === 'completed' ? data.data.url : null,
                transcodedVideoStatus: data.status
              });
            })
            .catch(err => {
              console.error(`Error in PathwayOverviewModal.videoTranscode.func : ${err}`);
            });
          videoPreTranscodeUrl = securedUrl || fileStack.filesUploaded[0].url;
        }
        this.setState({
          fileStack: [...this.state.fileStack, ...fileStack.filesUploaded],
          securedUrl: securedUrl,
          cardPreview: true,
          videoPreTranscodeUrl
        });
      })
      .catch(err => {
        console.error(
          `Error in PathwayOverviewModal.addFileStackFiles.refreshFilestackUrl.func : ${err}`
        );
      });
  }

  videoSourcePreview = item => {
    return (
      <div>
        <span>
          <div>
            <video
              preload="auto"
              poster={this.state.fileStack[1] && this.state.fileStack[1].url}
              className="preview-upload-video"
              controls
              src={this.state.videoPreTranscodeUrl || item.url}
              controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
            />
            <div />
          </div>
        </span>
      </div>
    );
  };

  handleCardAnalyticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.pathway));
  };

  getPricingPlans() {
    return this.state.pathway.cardMetadatum && this.state.pathway.cardMetadatum.plan
      ? tr(this.state.pathway.cardMetadatum.plan)
      : tr('Free');
  }

  render() {
    let pathway = this.state.pathway;
    let currentCard = this.state.currentCard;
    let cardSvgBackground = this.state.securedUrl || '';
    let disableTopics = pathway.tags && pathway.tags.length > 0;
    let isOwner = pathway.author && pathway.author.id == this.props.currentUser.id;
    let isCompleted =
      pathway.completionState && pathway.completionState.toUpperCase() === 'COMPLETED';
    let checked = this.props.checkedCardId !== null || this.state.checkedCardId;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let config = this.props.team.config;
    let showCreator =
      config &&
      ((typeof config.show_creator_in_card == 'undefined' && !config.show_creator_in_card) ||
        !!config.show_creator_in_card);
    let smartBitesBeforeCurrent =
      this.state.smartBites && this.state.smartBites.slice(0, this.state.currentIndex);
    let isShowLockedCardContent =
      this.state.currentIndex === 0 ||
      (smartBitesBeforeCurrent &&
        smartBitesBeforeCurrent.length &&
        smartBitesBeforeCurrent.every(
          item => item.completionState && item.completionState.toUpperCase() === 'COMPLETED'
        ));
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    let title = pathway.title || pathway.message;
    if (title) {
      title = title.replace(/amp;/gi, '');
    }

    return (
      <div className="card-overview">
        {this.state.showCardOverview && (
          <div>
            <div className="preview-arrows">
              {!(this.state.currentIndex === 0 && checked) && (
                <LeftArrow
                  color="#d6d6e1"
                  onClick={this.arrowClick.bind(null, -1)}
                  style={this.styles.leftArrow}
                />
              )}
              {!(this.state.currentIndex + 1 === this.state.smartBites.length && isCompleted) && (
                <RightArrow
                  color="#d6d6e1"
                  onClick={this.arrowClick.bind(null, 1)}
                  style={this.styles.rightArrow}
                />
              )}
            </div>
            <div className="preview-count">
              {this.state.currentIndex + 1} / {this.state.smartBites.length}
            </div>
          </div>
        )}
        <div className="card-overview-header">
          <div className="close-wrapper">
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={this.styles.closeBtn}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
          <span className="header-title">
            {tr('Pathway')}{' '}
            {this.state.pathway.state === 'draft' && (
              <span className="header-draft-label">{tr('Draft')}</span>
            )}
          </span>
          <TextField name="pathwayoverviewmodal" autoFocus={true} className="hiddenTextField" />
        </div>
        {this.state.loadChecked || this.state.isInProgressToComplete ? (
          <div className="text-center pathway-overview-spinner">
            <Spinner />
          </div>
        ) : this.state.showCardOverview ? (
          <div>
            <CardOverviewModal
              autoComplete={
                this.newComplete &&
                ((this.completeMethodConf === 'creatorChoose' &&
                this.state.pathway.autoComplete !== undefined
                  ? this.state.pathway.autoComplete
                  : true) ||
                  this.completeMethodConf === 'creatorNotChoose')
              }
              pathwayPreview={true}
              card={this.state.checkedCard}
              isLock={this.state.checkedCard.isLocked}
              cardUpdated={this.smartBiteUpdated.bind(
                this,
                this.state.checkedCard.id,
                this.state.currentIndex
              )}
              showComment={
                Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')
              }
              isPathwayOwner={isOwner}
              dueAt={this.props.dueAt}
              startDate={this.props.startDate}
              isShowLockedCardContent={isShowLockedCardContent}
              isPrivate={this.state.checkedCard.isPrivate}
              isCompleted={this.state.isMarkedAsCompleted}
            />

            {currentCard.cardType == 'project' && currentCard.filestack && (
              <div className="text-block small-12 medium-8 large-8 left-content">
                <div className="common-margin">
                  <Divider />
                </div>
                {((!this.state.videoSubmitted &&
                  (this.state.submissionList &&
                    this.state.submissionList.length > 0 &&
                    this.state.submissionList[0].projectSubmissionReviews &&
                    this.state.submissionList[0].projectSubmissionReviews.length > 0 &&
                    (this.state.submissionList[0].projectSubmissionReviews[0].status !==
                      'approved' ||
                      this.state.submissionList[0].projectSubmissionReviews[0].status ===
                        'rejected'))) ||
                  (this.state.submissionList && this.state.submissionList.length == 0)) && (
                  <div>
                    <span>{tr('ADD YOUR SUBMISSION')}</span>{' '}
                    <span style={this.styles.submissionList}>
                      {tr('You can submit only one file per project for approval.')}
                    </span>
                    <FlatButton
                      className="image-upload overview-tags-container"
                      icon={<VideoIcon style={this.styles.videoIcon} color="#bababa" />}
                      onClick={this.fileStackHandler.bind(
                        this,
                        videoType,
                        this.addFileStackFiles.bind(this)
                      )}
                    />
                    {this.state.fileStack &&
                      this.state.fileStack[0] &&
                      this.state.fileStack[0].mimetype &&
                      this.state.fileStack[0].mimetype.indexOf('video/') > -1 && (
                        <div className="text-center">
                          <div className="row">
                            <div className="small-12 relative">
                              {this.videoSourcePreview(
                                this.state.fileStack[0],
                                isDownloadContentDisabled
                              )}
                            </div>
                            <div className="small-12 file-title">
                              <span className="item-name">
                                {this.state.fileStack[0].filename || this.state.fileStack[0].handle}
                              </span>
                            </div>
                          </div>
                        </div>
                      )}
                    <div className="error-text">{this.state.uploadErrorText}</div>
                    <div className="overview-tags-container">
                      <TextFieldCustom
                        hintText="Description"
                        value={this.state.description}
                        inputChangeHandler={this.descriptionCardChange.bind(this)}
                        rowsMax={4}
                        rows={4}
                        multiLine={true}
                        fullWidth={true}
                      />
                      {this.state.optionsError && (
                        <div className="error-text">{tr(this.state.optionsError)}</div>
                      )}
                    </div>
                    <div className="action-buttons overview-tags-container">
                      <FlatButton
                        label={tr(this.state.createLabel)}
                        className={this.styles.create}
                        onTouchTap={this.submitClickHandler.bind(this, currentCard)}
                        labelStyle={this.styles.btnLabel}
                        style={
                          !this.state.disableSubmit ? this.styles.disabled : this.styles.create
                        }
                      />
                    </div>
                  </div>
                )}
                <div className="overview-tags-container">
                  {this.state.submissionList && this.state.submissionList.length > 0 && (
                    <div>{tr('Previous Submissions')}</div>
                  )}
                  {this.state.submissionList &&
                    this.state.submissionList.map(list => {
                      return (
                        <div style={this.styles.submissionList}>{list.filestack[0].filename}</div>
                      );
                    })}
                </div>
              </div>
            )}
          </div>
        ) : (
          <div className="card-overview-content">
            <div className="row">
              <div className="small-12 medium-8 large-8 left-content card-modal-left-column">
                <a
                  href="#"
                  className="main-image anchorAlignment"
                  onClick={e => this.linkToPush(pathway, null, e)}
                >
                  <div className="card-blurred-background">
                    <svg id="svg-image-blur" width="100%" height="100%">
                      <title>{unescape(title)}</title>
                      <SvgImageResized
                        cardId={`${pathway.id}`}
                        xlinkHref={cardSvgBackground}
                        width="100%"
                        style={this.styles.svgImage}
                        height="100%"
                        resizeOptions={'height:360'}
                      />
                      <filter id="blur-effect-1">
                        <feGaussianBlur stdDeviation="10" />
                      </filter>
                    </svg>
                  </div>
                  <svg width="100%" height="100%" style={this.styles.mainSvg}>
                    <title>{unescape(title)}</title>
                    <SvgImageResized
                      cardId={`${pathway.id}`}
                      xlinkHref={cardSvgBackground}
                      width="100%"
                      style={this.styles.svgImage}
                      height="100%"
                      resizeOptions={'height:360'}
                    />
                  </svg>
                </a>
                <div className="top-block clearfix">
                  {!(pathway.author && showCreator) && (
                    <span className="author-info-container">
                      <CreationDate
                        card={pathway}
                        standaloneLinkClickHandler={this.standaloneLinkClickHandler.bind(
                          this,
                          pathway
                        )}
                      />
                    </span>
                  )}
                  {pathway.author && showCreator && (
                    <CardHeader
                      title={
                        <div className="channels-block">
                          <div className="author-info-container modal-author">
                            <a
                              href="#"
                              className="user-name"
                              onClick={e => this.linkToPush(null, pathway.author.handle, e)}
                            >
                              {`${pathway.author.fullName ? pathway.author.fullName : ''}`}
                            </a>
                            <br />
                            <CreationDate
                              card={pathway}
                              standaloneLinkClickHandler={this.standaloneLinkClickHandler.bind(
                                this,
                                pathway
                              )}
                            />
                          </div>
                        </div>
                      }
                      subtitle={
                        pathway.publishedAt && (
                          <div className="header-secondary-text">
                            <span className="matte" />
                          </div>
                        )
                      }
                      avatar={
                        <a
                          style={this.styles.userAvatar}
                          onTouchTap={this.linkToPush.bind(null, null, pathway.author.handle)}
                        >
                          <BlurImage
                            style={this.styles.avatarBox}
                            id={pathway.id}
                            image={
                              pathway.author &&
                              ((pathway.author.avatarimages && pathway.author.avatarimages.small) ||
                                defaultUserImage)
                            }
                          />
                        </a>
                      }
                      style={this.styles.cardHeader}
                    />
                  )}
                  {!isOwner && pathway.packCards && !!pathway.packCards.length && !isCompleted && (
                    <div className="pull-right">
                      <PrimaryButton
                        label={tr('Start')}
                        style={this.styles.primary}
                        onTouchTap={this.startClick}
                        className="review-btn"
                      />
                    </div>
                  )}
                </div>
                <div style={this.styles.pricing}>
                  <span>Price : </span>
                  <span className="pathway-paid-label">
                    {this.state.pathway.isPaid ? tr('Paid') : this.getPricingPlans()}
                  </span>
                </div>
                <MainInfoSmartBite
                  smartBite={this.state.pathway}
                  dueAt={this.props.dueAt}
                  startDate={this.props.startDate}
                  isOwner={this.state.isOwner}
                  type="pathway"
                  linkToPush={this.linkToPush.bind(null, pathway, null)}
                  isOverviewModal="true"
                />
                <div className="pack-cards-block">
                  {this.state.loadingPackCards && (
                    <div className="text-center">
                      <Spinner />
                    </div>
                  )}
                  {!this.state.loadingPackCards &&
                    !!(this.state.pathway.packCards && this.state.pathway.packCards.length) && (
                      <div>
                        {this.state.smartBites &&
                          this.state.smartBites.map((card, index) => {
                            return (
                              <div
                                key={index}
                                onClick={!card.isLocked && this.chooseCard.bind(this, card, index)}
                              >
                                <CardListViewPreview
                                  card={card}
                                  index={index + 1}
                                  author={card.author}
                                  cardUpdated={this.smartBiteUpdated.bind(this, card.id, index)}
                                  linkToPush={this.linkToPush}
                                  isPathwayOwner={isOwner}
                                  isPrivate={card.isPrivate}
                                />
                              </div>
                            );
                          })}
                      </div>
                    )}
                </div>
              </div>
              <div className="comments-container small-12 medium-4 large-4">
                <div className="comments">
                  <div>
                    <small className="label">{tr('COMMENTS')}</small>
                  </div>
                  <div className="break-line" />
                  {this.state.showComment && (
                    <div>
                      <CommentList
                        cardId={pathway.id + ''}
                        cardOwner={pathway.author ? pathway.author.id : ''}
                        cardType={pathway.cardType}
                        comments={this.state.comments}
                        pending={pathway.commentPending}
                        numOfComments={this.state.commentsCount}
                        overViewModal={true}
                        updateCommentCount={this.updateCommentCount}
                        videoId={pathway.id ? pathway.id : null}
                        inModal={true}
                      />
                    </div>
                  )}
                  <div className="modal-actions-bar">
                    <div className="border-element" />
                    {Permissions.has('LIKE_CONTENT') && (
                      <span>
                        <IconButton
                          style={this.styles.likeIcon}
                          tooltip={!this.state.isUpvoted ? tr('Like') : tr('Liked')}
                          tooltipStyles={this.styles.tooltipActiveBts}
                          onTouchTap={this.likeCardHandler}
                        >
                          {!this.state.isUpvoted && <LikeIcon />}
                          {this.state.isUpvoted && <LikeIconSelected />}
                        </IconButton>
                        <small className="votes-count">
                          {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : ''}
                        </small>
                      </span>
                    )}
                    {(isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                      <div className="icon-inline-block">
                        <IconButton
                          className="statistics"
                          style={this.styles.actionIcon}
                          tooltip={tr('Card Statistics')}
                          tooltipStyles={this.styles.tooltipActiveBts}
                          onTouchTap={this.handleCardAnalyticsModal}
                        >
                          <CardAnalyticsV2 />
                        </IconButton>
                      </div>
                    )}
                    <div className="float-right button-icon">
                      <InsightDropDownActions
                        card={this.state.pathway}
                        author={this.state.pathway.author}
                        dismissible={false}
                        style={this.styles.dropDownAction}
                        disableTopics={disableTopics}
                        isStandalone={false}
                        cardUpdated={this.cardUpdated.bind(this)}
                        assignable={false}
                        deleteSharedCard={this.props.deleteSharedCard}
                        isCompleted={isCompleted}
                        isOverviewModal={true}
                        type={'PathwayStandAlone'}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="preview-back" onClick={this.linkToPush.bind(null, pathway, null)}>
          {tr('Pathway Detail')}
        </div>
      </div>
    );
  }
}

PathwayOverviewModal.propTypes = {
  card: PropTypes.object,
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  channelsV2: PropTypes.object,
  relevancyRating: PropTypes.object,
  team: PropTypes.object,
  pathname: PropTypes.string,
  checkedCardId: PropTypes.string,
  deleteSharedCard: PropTypes.func,
  cardUpdated: PropTypes.func,
  isStandaloneModal: PropTypes.bool,
  defaultImage: PropTypes.string,
  startDate: PropTypes.string,
  dueAt: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    modal: state.modal.toJS(),
    channelsV2: state.channelsV2.toJS(),
    relevancyRating: state.relevancyRating.toJS(),
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(PathwayOverviewModal);
