import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { close } from '../../actions/modalActions';
import colors from 'edc-web-sdk/components/colors/index';
import { updateGroup } from '../../actions/groupsActionsV2';
import { tr } from 'edc-web-sdk/helpers/translations';
import Checkbox from 'material-ui/Checkbox';
import TextField from 'material-ui/TextField';
class UpdateGroupDetailsModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      id: this.props.id,
      labelChanged: false,
      isDynamic: false,
      auto_assign_content: true
    };

    this.styles = {
      updateGroupDetails: {
        width: '100%',
        border: '1px solid #d6d6e1',
        borderRadius: '2px',
        fontSize: '13px',
        color: '#6f708b'
      },
      updateGroupTitle: {
        padding: '6px 9px',
        height: '28px'
      },
      updateGroupDescription: {
        padding: '11px 40px 13px 9px',
        outline: 'none',
        lineHeight: '16px',
        height: '56px'
      },
      maxChar: {
        fontFamily: 'Open Sans',
        fontSize: '13px',
        color: '#d0011b',
        width: '100%',
        textAlign: 'right',
        padding: '4px 0px',
        fontWeight: '300'
      },
      updateGroupHeading: {
        color: '#fff',
        width: '100%',
        padding: '3px 0px 12px 0px'
      },
      closeButton: {
        position: 'absolute',
        top: '-10px',
        right: '-15px'
      },
      updateGroupDetailsContainer: {
        background: '#fff',
        padding: '16px 22px 14px 16px'
      },
      updateButton: {
        background: colors.purple,
        border: '1px solid #9276b5',
        color: colors.white
      }
    };

    this.handleClickUpdate = this.handleClickUpdate.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.changeDescription = this.changeDescription.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount = () => {
    let currentGroupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[currentGroupID];

    this.setState({
      label: group.name,
      isDynamic: group.isDynamic,
      description: group.description,
      group,
      auto_assign_content: group.autoAssignContent
    });
  };

  changeTitle(e) {
    this.setState({ label: e.target.value });
    this.setState({ labelChanged: true });
  }

  changeDescription(e) {
    this.setState({ description: e.target.value });
  }

  handleClickUpdate() {
    let group = this.state.group;
    let payload = {};
    payload.name = this.state.label;
    payload.description = this.state.description;
    payload.auto_assign_content = this.state.auto_assign_content;
    this.props
      .dispatch(updateGroup(group.id, payload, false))
      .then(() => {
        this.props.dispatch(close());
      })
      .catch(err => {
        console.error(`Error in UpdateGroupDetailsModal.updateGroup.func : ${err}`);
      });
  }

  closeModal() {
    this.props.dispatch(close());
  }

  handleContentAutoAssignment = event => {
    this.setState({
      auto_assign_content: event.target.checked
    });
  };

  render() {
    return (
      <div>
        <div style={this.styles.updateGroupHeading}>
          <div style={{ fontSize: '18px' }}>{tr('Edit Title & Description')}</div>
          <div style={this.styles.closeButton}>
            <IconButton aria-label="close" onTouchTap={this.closeModal}>
              <CloseIcon color="white" />
            </IconButton>
            <TextField name="updategroup" autoFocus={true} className="hiddenTextField" />
          </div>
        </div>

        <div style={this.styles.updateGroupDetailsContainer}>
          <div className="row">
            <div
              className="large-2"
              style={{ padding: '6px 0px 0px', color: '#454560', fontSize: '14px' }}
            >
              {tr('Title')}
            </div>
            <div className="large-10">
              <input
                aria-label={tr('title')}
                disabled={this.state.isDynamic}
                type="text"
                style={{ ...this.styles.updateGroupTitle, ...this.styles.updateGroupDetails }}
                value={this.state.label}
                onChange={this.changeTitle}
                maxLength="55"
              />
            </div>
          </div>

          <div className="row">
            <p style={this.styles.maxChar}>{`(${tr('Max characters')}=55)`}</p>
          </div>

          <div
            className="row"
            style={{ padding: '28px 0px 0px', color: '#454560', fontSize: '14px' }}
          >
            <div
              className="large-2"
              style={{ padding: '6px 0px 0px', color: '#454560', fontSize: '14px' }}
            >
              {tr('Description')}
            </div>
            <div className="large-10">
              <textarea
                aria-label={tr('Description')}
                style={{ ...this.styles.updateGroupDescription, ...this.styles.updateGroupDetails }}
                value={this.state.description}
                onChange={this.changeDescription}
                maxLength="255"
              />
            </div>
          </div>

          <div className="row">
            <p style={this.styles.maxChar}>{`(${tr('Max characters')}=255)`}</p>
          </div>

          {!this.state.isDynamic && (
            <div>
              <Checkbox
                checked={this.state.auto_assign_content}
                onCheck={this.handleContentAutoAssignment.bind(this)}
                label={tr('Auto assign content of this group to group members')}
                aria-label={tr('Auto assign content of this group to group members')}
              />
            </div>
          )}

          <div style={{ padding: '8px' }} className="clearfix">
            <PrimaryButton
              label={tr('Update')}
              className="float-right create"
              pendingLabel={tr('Updating...')}
              onTouchTap={this.handleClickUpdate}
              style={this.styles.updateButton}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS(),
    groupsV2: state.groupsV2.toJS()
  };
}

UpdateGroupDetailsModal.propTypes = {
  modal: PropTypes.object,
  groupsV2: PropTypes.object,
  id: PropTypes.number
};

export default connect(mapStoreStateToProps)(UpdateGroupDetailsModal);
