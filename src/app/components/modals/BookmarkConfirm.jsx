import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { close } from '../../actions/modalActions';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';

const lightPurp = '#acadc1';

class BookmarkConfirm extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      modal: {
        width: '34rem',
        margin: 'auto',
        height: '12rem',
        position: 'absolute',
        top: '0',
        left: '0',
        bottom: '0',
        right: '0'
      },
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      cancel: {
        color: '#6f708b',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '0.25rem 0.75rem 0.25rem 0.25rem',
        opacity: '0.6',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      actionBtnLabel: {
        textTransform: 'none',
        fontFamily: 'OpenSans-Regular, Open Sans !important',
        fontSize: '0.85rem'
      },
      closeIconStyle: {
        height: '1.125rem',
        width: '1.125rem'
      }
    };
  }

  closeModal = () => {
    this.props.dispatch(close());
  };

  confirm = () => {
    this.props.confirmHandler(this.props.bookmarked);
    this.closeModal();
  };

  render() {
    return (
      <div>
        <div className="more-items channel-edit-modal" style={this.styles.modal}>
          <div className="backdrop" onClick={this.closeModal} style={{ background: 'none' }} />
          <div className="modal">
            <div className="my-modal-header">
              <span className="header-title">{tr('Unbookmark')}</span>
              <div className="close close-button">
                <IconButton
                  aria-label="close"
                  style={this.styles.closeBtn}
                  onTouchTap={this.closeModal}
                >
                  <CloseIcon color="white" />
                </IconButton>
              </div>
            </div>
            <div className="my-modal-content bookmark-modal">
              {tr('Are you sure you want Unbookmark this content from the list?')}
              <div className="action-buttons" style={{ paddingTop: '6rem' }}>
                <FlatButton
                  label={tr('CANCEL')}
                  style={this.styles.cancel}
                  labelStyle={this.styles.actionBtnLabel}
                  onTouchTap={this.closeModal}
                />
                <PrimaryButton
                  label={tr('YES')}
                  onTouchTap={this.confirm}
                  labelStyle={this.styles.actionBtnLabel}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BookmarkConfirm.propTypes = {
  bookmarked: PropTypes.object,
  confirmHandler: PropTypes.func
};

export default connect()(BookmarkConfirm);
