import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { searchForUsers } from 'edc-web-sdk/requests/users.v2';

import AddToGroup from '../team/SingleTeam/AddToGroup';
import DynamicSelections from '../../components/shareContent/DynamicSelections';
import PreviewDSModal from './PreviewDSModal';

import { close } from '../../actions/modalActions';
import { invite } from '../../actions/groupsActionsV2';
import TextField from 'material-ui/TextField';

class InviteV2UserModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      paper: {
        padding: '0.875rem'
      },
      buttons: {
        minWidth: '4.875rem',
        minHeight: '1.75rem'
      }
    };
    this.state = {
      activeTab: 'individuals',
      showPreview: false,
      selectedOrgUsers: [],
      previewFilter: []
    };
    this.enableDynamicSelection = window.ldclient.variation('dynamic-selections', false);
  }

  selectTab = tab => {
    this.setState({
      activeTab: tab,
      showAlert: false
    });
  };

  inviteUsers = () => {
    let groupID = this.props.groupsV2.currentGroupID;
    let emails = this.state.selectedOrgUsers.map(user => {
      return user.email;
    });
    let payload = {
      emails,
      roles: 'member',
      id: groupID,
      userList: this.state.selectedOrgUsers
    };

    if (emails.length > 0) {
      this.setState({ showAlert: false });
      this.props.dispatch(invite(payload));
      this.closeModal();
    } else {
      this.setState({ showAlert: true });
      setTimeout(() => {
        this.setState({ showAlert: false });
      }, 6000);
    }
  };

  getUsers = async idsOnly => {
    if (this.state.previewFilter) {
      let filters = this.state.previewFilter;
      let payload = { custom_fields: [], limit: 1000, q: '' };
      if (idsOnly) {
        payload.only_user_ids = true;
      }
      filters.forEach(filter => {
        if (filter && filter.options && filter.options.length) {
          filter.options.forEach(option => {
            if (option.checked) {
              payload.custom_fields.push({ name: filter.type, value: option.option });
            }
          });
        }
      });
      payload.custom_fields = JSON.stringify(payload.custom_fields);
      return await searchForUsers(payload);
    } else {
      return [];
    }
  };

  updateCheck = user => {
    let indexPos = findIndex(this.state.selectedOrgUsers, { id: user.id });
    let selectedOrgUsers = this.state.selectedOrgUsers.slice(0);
    if (indexPos > -1) {
      selectedOrgUsers.splice(indexPos, 1);
    } else {
      selectedOrgUsers = selectedOrgUsers.concat(user);
    }
    this.setState({ selectedOrgUsers });
  };

  getPreviewList = data => {
    this.setState({
      previewFilter: data
    });
  };

  showPreview = async () => {
    let data = await this.getUsers();
    if (data && data.users && data.users.length) {
      this.setState({
        previewData: data.users,
        selectedOrgUsers: data.users,
        showPreview: true
      });
    } else {
      this.setState({
        showPreview: true
      });
    }
  };

  backHandle = () => {
    this.setState({ showPreview: false });
  };
  closeModal = () => {
    this.props.dispatch(close());
  };

  render() {
    return !this.state.showPreview ? (
      <div className="group-modal-root">
        <div className="row">
          <div className="small-12 columns" style={{ position: 'relative', marginBottom: '10px' }}>
            <div className="group-modal-header">{tr('Invite New Users')}</div>
            <div className="close close-button">
              <IconButton
                aria-label="close"
                style={{ paddingRight: 0, width: 'auto' }}
                onTouchTap={this.closeModal}
              >
                <CloseIcon color="white" />
              </IconButton>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 columns">
            <Paper style={this.styles.paper}>
              <div className="row">
                <TextField name="inviteuserv2" autoFocus={true} className="hiddenTextField" />
                <div className="small-12 medium-6 columns text-center">
                  <div
                    className={`group-invite_tab ${this.state.activeTab === 'individuals' &&
                      'group-invite_tab__selected'}`}
                    onClick={this.selectTab.bind(this, 'individuals')}
                  >
                    {tr('Individuals')}
                  </div>
                </div>
                {this.enableDynamicSelection && (
                  <div className="small-12 medium-6 columns text-center">
                    <div
                      className={`group-invite_tab ${this.state.activeTab === 'dynamic' &&
                        'group-invite_tab__selected'}`}
                      onClick={this.selectTab.bind(this, 'dynamic')}
                    >
                      {tr('Dynamic Selection')}
                    </div>
                  </div>
                )}
                <div className="small-12 columns">
                  {this.state.activeTab === 'individuals' && (
                    <AddToGroup
                      closeModal={this.closeModal}
                      updateCheck={this.updateCheck}
                      selectedOrgUsers={this.state.selectedOrgUsers}
                    />
                  )}
                  {this.state.activeTab === 'dynamic' && (
                    <DynamicSelections
                      target="invite"
                      getPreviewList={this.getPreviewList}
                      filterContent={this.state.previewFilter}
                      {...this.props}
                    />
                  )}
                </div>
                <div className="small-12 columns text-center">
                  <SecondaryButton
                    label={tr('Cancel')}
                    className="save-users"
                    style={this.styles.buttons}
                    onTouchTap={this.closeModal}
                  />
                  {this.state.activeTab === 'dynamic' && (
                    <SecondaryButton
                      label={tr('Preview')}
                      className="preview-users"
                      style={this.styles.buttons}
                      onTouchTap={this.showPreview}
                    />
                  )}
                  <PrimaryButton
                    label={tr('Invite')}
                    className="save-users"
                    style={this.styles.buttons}
                    onTouchTap={this.inviteUsers}
                  />
                </div>
                {this.state.showAlert && (
                  <div className="column text-center" style={{ color: '#ff0000', fontWeight: 400 }}>
                    {tr('No User Selected!')}
                  </div>
                )}
              </div>
            </Paper>
          </div>
        </div>
      </div>
    ) : (
      <PreviewDSModal
        backHandle={this.backHandle}
        previewData={this.state.previewData}
        target="invite"
      />
    );
  }
}

InviteV2UserModal.propTypes = {
  message: PropTypes.string,
  callback: PropTypes.func,
  title: PropTypes.string,
  groupsV2: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(InviteV2UserModal);
