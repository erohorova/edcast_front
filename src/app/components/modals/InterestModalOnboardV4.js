import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import findIndex from 'lodash/findIndex';
import some from 'lodash/some';
import FlashAlert from '../common/FlashAlert';
import { tr } from 'edc-web-sdk/helpers/translations';
import { closeInterestModal, confirmation } from '../../actions/modalActions';
import SearchBox from './SearchBox';
import { queryTopics } from 'edc-web-sdk/requests/topics';
import { updateUserInterest } from '../../actions/usersActions';
import { requestTopic, addRequestTopic } from 'edc-web-sdk/requests/users.v2';
import IconButton from 'material-ui/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import TooltipLabel from '../common/TooltipLabel';

class InterestModalOnboardV4 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      interests: [],
      defaultTopics: [],
      customTopics: [],
      interestFromTrending: [],
      suggestions: [],
      loadingSuggestions: false,
      selectedInterest: {
        selectedSuggestion: [],
        selectedCustomString: []
      },
      interestLimit:
        (this.props.team &&
          this.props.team.config &&
          this.props.team.config.limit_options &&
          this.props.team.config.limit_options.interests_limit) ||
        3,

      message: '',
      postPending: false,
      alertdisplayTimeOut: 1000
    };

    this.styles = {
      chip: {
        margin: '4px 8px 4px 0px',
        border: `1px solid ${colors.primary}`,
        cursor: 'pointer'
      },
      chipLabel: {
        paddingRight: '27px'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px',
        color: `${colors.primary}`
      },
      customChipLabel: {
        paddingRight: '12px'
      }
    };
  }

  componentDidMount() {
    let interests =
      (this.props.currentUser &&
        this.props.currentUser.profile &&
        this.props.currentUser.profile.learningTopics) ||
      [];
    let defaultTopics =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.default_topics &&
        this.props.team.config.default_topics.defaults) ||
      [];
    let customTopics = this.props.currentUser.customTopics || [];
    this.setState({ interests, defaultTopics, customTopics });
  }

  _handleClickCancel = () => {
    this.props.dispatch(closeInterestModal());
  };

  _handleInterestDelete = key => {
    let interests = this.state.interests;
    const chipToDelete = interests.findIndex(chip => chip.topic_id === key);
    interests.splice(chipToDelete, 1);
    this.setState({ interests });
  };

  _AddRemoveInterestFromTreding = data => {
    let interestFromTrending = this.state.interestFromTrending;
    let index = findIndex(interestFromTrending, data);
    if (some(this.state.interests, ['topic_id', data.topic_id])) {
      this.setState({
        showAlert: true,
        message: tr('Similar topic is already selected'),
        alertdisplayTimeOut: 1000
      });
      return;
    }
    if (index > -1) {
      interestFromTrending.splice(index, 1);
    } else if (this._isAddInterestFlag()) {
      interestFromTrending.push(data);
    } else {
      this.setState({
        showAlert: true,
        message: tr(`Oops! Looks like you have too much in your basket. Please pick between 3 - %{count} interests. This does not mean your learning is restricted - Edcast lets you search new content all the time!  Interests can be changed later too.`, {count: this.state.interestLimit}),
        alertdisplayTimeOut: 5000
      });
    }
    this.setState({ interestFromTrending });
  };

  renderSelectedInterestChip = data => {
    return (
      <Chip
        className="interest-chip"
        key={data.topic_id}
        style={this.styles.chip}
        backgroundColor={colors.primary}
        labelStyle={this.styles.chipLabel}
        labelColor={'#ffffff'}
      >
        {data.topic_label}
        <IconButton
          className="cancel-icon delete"
          ref={'cancelIcon'}
          tooltip={tr('Remove Item')}
          aria-label={tr('Remove Item')}
          disableTouchRipple
          tooltipPosition="top-center"
          style={this.styles.customPlusIcon}
          onTouchTap={() => this._handleInterestDelete(data.topic_id)}
        >
          <Close color={'#ffffff'} />
        </IconButton>
      </Chip>
    );
  };

  renderTrendingInterestChip = data => {
    return (
      <Chip
        className="interest-chip"
        key={data.topic_id}
        style={this.styles.chip}
        onClick={() => this._AddRemoveInterestFromTreding(data)}
        backgroundColor={
          some(this.state.interestFromTrending, ['topic_id', data.topic_id])
            ? `${colors.primary}`
            : '#ffffff'
        }
        labelColor={
          !some(this.state.interestFromTrending, ['topic_id', data.topic_id])
            ? `${colors.primary}`
            : '#ffffff'
        }
      >
        {data.topic_label}
      </Chip>
    );
  };

  renderSearchedInterestChip = data => {
    return (
      <Chip
        className="interest-chip"
        key={data.topic_id}
        style={this.styles.chip}
        style={this.styles.chip}
        backgroundColor={'#ffffff'}
        labelStyle={this.styles.chipLabel}
        labelColor={colors.primary}
      >
        {data.topic_label}
        <IconButton
          className="cancel-icon delete"
          ref={'cancelIcon'}
          tooltip={tr('Remove Item')}
          aria-label={tr('Remove Item')}
          disableTouchRipple
          tooltipPosition="top-center"
          style={this.styles.customPlusIcon}
          onTouchTap={() => {
            this._removeSelectedInterest(data, 'suggested');
          }}
        >
          <Close color={colors.primary} />
        </IconButton>
      </Chip>
    );
  };

  renderCustomInterestChip = (data, index) => {
    return (
      <Chip
        className="interest-chip"
        key={index}
        style={this.styles.chip}
        backgroundColor={'#bcbcbc'}
        labelStyle={this.styles.chipLabel}
        labelColor={colors.primary}
      >
        {data}
        <IconButton
          className="cancel-icon delete"
          ref={'cancelIcon'}
          tooltip={tr('Remove Item')}
          aria-label={tr('Remove Item')}
          disableTouchRipple
          tooltipPosition="top-center"
          style={this.styles.customPlusIcon}
          onTouchTap={() => {
            this._removeSelectedInterest(data, 'custom');
          }}
        >
          <Close color={colors.primary} />
        </IconButton>
      </Chip>
    );
  };

  renderAlreadySelectedCustomInterestChip = (data, index) => {
    return (
      <TooltipLabel text={tr('Pending for Admin Approval')}>
        <Chip
          className="interest-chip"
          key={data.topic_id}
          style={this.styles.chip}
          backgroundColor={'#bcbcbc'}
          labelStyle={this.styles.customChipLabel}
          labelColor={'#ffffff'}
        >
          {data.label}
        </Chip>
      </TooltipLabel>
    );
  };

  _onSearchQueryChange = q => {
    if (q) {
      this.setState({ loadingSuggestions: true });
      queryTopics(q)
        .then(topicsData => {
          this.setState({
            suggestions: topicsData.topics.slice(0, 20),
            loadingSuggestions: false
          });
        })
        .catch(err => {
          console.error(`Error in Expertise.queryTopics.func : ${err}`);
        });
    } else {
      this.setState({
        suggestions: []
      });
    }
  };

  _onInterestSelect = selectionData => {
    let selectedInterest = {},
      payload = {};
    selectedInterest['selectedSuggestion'] = selectionData.selectedSuggestion;
    selectedInterest['selectedCustomString'] = selectionData.selectedCustomString;
    if (selectionData.currentSelection.type === 'custom') {
      payload = {
        'state[]': ['pending', 'approved', 'rejected'],
        q: selectionData.currentSelection.item
      };
      requestTopic(payload)
        .then(response => {
          if (response.total) {
            let item = selectionData.currentSelection.item;
            let type = selectionData.currentSelection.type;
            let isRejected = response.topicRequests.filter(
              topic => topic.label === item && topic.state === 'rejected'
            );
            this.searchbox._clearState(item, type);
            this.setState({
              showAlert: true,
              message: tr(
                !!isRejected.length
                  ? 'Topic is rejected by Admin'
                  : 'Similar topic is already created'
              ),
              postPending: false,
              alertdisplayTimeOut: 1500
            });
          } else {
            this.setState({ selectedInterest });
          }
        })
        .catch(e => {
          console.error(`Error in InterestModalOnboardV$.requestTopic.func : ${e}`);
        });
    } else {
      this.setState({ selectedInterest });
    }
  };

  _removeSelectedInterest = (item, type) => {
    this.searchbox._removeSelectedInterest(item, type);
  };

  _onlimitExceed = selectedInterest => {
    this.setState({ selectedInterest });
    this.setState({
      showAlert: true,
      message: tr(`Oops! Looks like you have too much in your basket. Please pick a maximum of %{count} interests. This does not mean your learning is restricted - Edcast lets you search new content all the time!  Interests can be changed later too.`, {count: this.state.interestLimit}),
      alertdisplayTimeOut: 5000
    });
  };

  _resultFormation = skill => {
    let responseResult = {};
    responseResult['domain_id'] = skill.domain ? skill.domain.id : skill.domain_id || null;
    responseResult['domain_label'] = skill.domain ? skill.domain.label : skill.domain_label || null;
    responseResult['domain_name'] = skill.domain ? skill.domain.name : skill.domain_name || null;
    responseResult['topic_id'] = skill.id ? skill.id : skill.topic_id || null;
    responseResult['topic_label'] = skill.label ? skill.label : skill.topic_label || null;
    responseResult['topic_name'] = skill.name ? skill.name : skill.topic_name || null;
    responseResult['topic_description'] = skill.description || null;
    return responseResult;
  };

  _isAddInterestFlag = () => {
    let interests = this.state.interests || [];
    let selectedInterest = this.state.selectedInterest;
    let interestFromTrending = this.state.interestFromTrending || [];
    let customTopics = this.state.customTopics || [];
    let interestLimit = this.state.interestLimit;

    let selectedInterestsLimit =
      interests.length +
      selectedInterest.selectedSuggestion.length +
      selectedInterest.selectedCustomString.length +
      interestFromTrending.length +
      customTopics.length;

    return interestLimit > selectedInterestsLimit;
  };

  _submitInterests = () => {
    this.setState({ postPending: true });

    let interests = this.state.interests || [];
    let selectedInterest = this.state.selectedInterest;
    let interestFromTrending = this.state.interestFromTrending || [];
    let customTopics = this.state.customTopics || [];
    let skillsArray = [
      ...interests,
      ...selectedInterest.selectedSuggestion,
      ...interestFromTrending
    ];
    let payload = { 'topics[]': selectedInterest.selectedCustomString };

    if (
      skillsArray.length + customTopics.length === 0 &&
      selectedInterest.selectedCustomString.length == 0
    ) {
      this.setState({
        showAlert: true,
        message: tr('You should have at least 1 topic selected.'),
        postPending: false,
        alertdisplayTimeOut: 1000
      });
      return;
    }

    if (selectedInterest.selectedCustomString.length) {
      addRequestTopic(payload, 'post')
        .then(() => {
          this.props
            .dispatch(
              updateUserInterest(
                skillsArray,
                this.props.currentUser.id,
                this.props.currentUser.profile.id
              )
            )
            .then(response => {
              this.setState({ postPending: false });
              this.props.dispatch({
                type: 'get_custom_topics',
                customTopics: [
                  ...this.state.customTopics,
                  ...(selectedInterest.selectedCustomString || []).map(interest => {
                    return { label: interest };
                  })
                ]
              });
            })
            .catch(err => {
              console.error(`Error in UpdateInterestsModal.updateUserInterest.func : ${err}`);
            });
        })
        .catch(err => {
          this.setState({
            showAlert: true,
            message: tr('Similar topic is already created'),
            postPending: false,
            alertdisplayTimeOut: 1000
          });
          console.error(`Error in InterestModalOnboardV$.addRequestTopic.func : ${err}`);
        });
    } else {
      this.props
        .dispatch(
          updateUserInterest(
            skillsArray,
            this.props.currentUser.id,
            this.props.currentUser.profile.id
          )
        )
        .then(response => {
          this.setState({ postPending: false });
        })
        .catch(err => {
          console.error(`Error in UpdateInterestsModal.updateUserInterest.func : ${err}`);
        });
    }
    let taxonomyInterest = [
      ...interests,
      ...interestFromTrending,
      ...selectedInterest.selectedSuggestion
    ];
    let customInterest = [
      ...this.state.customTopics.map(interest => {
        return interest.label;
      }),
      ...selectedInterest.selectedCustomString
    ];
    let message = '';
    if (!!taxonomyInterest.length && !!customInterest.length) {
      message =
        "The following topics: '" +
        customInterest.join("', '") +
        "' are not available for learning currently. However, we have taken your inputs and as soon as the admin approves the topics, it will appear in your learning. Till then, you can start your learning journey with '" +
        taxonomyInterest
          .map(topic => {
            return topic.topic_label;
          })
          .join("', '") +
        "'";
    } else if (!taxonomyInterest.length && !!customInterest.length) {
      message =
        'Well done!! You have done your part. As soon as admin approves your topics you can begin your learning journey';
    } else {
      message =
        'Well done!! You are ready to begin your learning journey based on the topics selected';
    }

    this.props.dispatch(confirmation(' ', message, () => {}, true));
  };

  _removeAlert = () => {
    this.setState({ showAlert: false });
  };

  render() {
    let interests = this.state.interests || [];
    let selectedInterest = this.state.selectedInterest;
    let defaultTopics = this.state.defaultTopics || [];
    let customTopics = this.state.customTopics || [];

    return (
      <div>
        {this.state.showAlert && (
          <FlashAlert
            message={this.state.message}
            toShow={this.state.showAlert}
            removeAlert={() => {
              this._removeAlert();
            }}
            displayTimeOut={this.state.alertdisplayTimeOut}
          />
        )}
        <div style={{ marginBottom: '25px' }}>
          <h6>Update Learning Interest</h6>
        </div>

        <div style={{ marginBottom: '25px' }}>
          <div style={this.styles.chipsWrapper} className="selected-interest-chip-container">
            {interests.map(this.renderSelectedInterestChip)}
            {!!customTopics.length &&
              this.state.customTopics.map(this.renderAlreadySelectedCustomInterestChip)}
          </div>
        </div>

        <div style={{ marginBottom: '25px' }}>
          <SearchBox
            ref={instance => {
              this.searchbox = instance;
            }}
            multiSelect={true}
            allowCustomString={true}
            onChangeCallBack={this._onSearchQueryChange}
            onSelectCallBack={this._onInterestSelect}
            suggestions={this.state.suggestions}
            loading={this.state.loadingSuggestions}
            ignoreInSuggestions={[...defaultTopics, ...interests]}
            suggestionSelectLimit={this._isAddInterestFlag()}
            onlimitExceedCallBack={this._onlimitExceed}
            resultFormationCallBack={this._resultFormation}
          />
          <div style={this.styles.chipsWrapper}>
            {(selectedInterest.selectedSuggestion || []).map(this.renderSearchedInterestChip)}
            {(selectedInterest.selectedCustomString || []).map(this.renderCustomInterestChip)}
          </div>
        </div>

        <div style={{ marginBottom: '25px' }}>
          <div>
            <h6>Trending interests</h6>
          </div>
          <div style={this.styles.chipsWrapper} className="selected-interest-chip-container">
            {defaultTopics.map(this.renderTrendingInterestChip)}
          </div>
        </div>

        <div style={{ padding: '8px' }} className="clearfix">
          <PrimaryButton
            label={tr('Update')}
            className="float-right create"
            onTouchTap={() => {
              this._submitInterests();
            }}
            pendingLabel={tr('Updating...')}
            pending={this.state.postPending}
          />
          <SecondaryButton
            label={tr('Cancel')}
            className="float-right close"
            onTouchTap={() => {
              this._handleClickCancel();
            }}
          />
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: Object.assign({}, state.team.toJS()),
    currentUser: Object.assign({}, state.currentUser.toJS()),
    onboarding: state.onboarding.toJS()
  };
}

InterestModalOnboardV4.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  onboarding: PropTypes.object,
  callback: PropTypes.any
};

export default connect(mapStoreStateToProps)(InterestModalOnboardV4);
