/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
//actions
import { close } from '../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class ConfirmationModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.cancelClickHandler = this.cancelClickHandler.bind(this);
    this.confirmClickHandler = this.confirmClickHandler.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.styles = {
      hide: {
        width: '0px'
      }
    };
  }

  cancelClickHandler() {
    !!this.props.cancelClick ? this.props.cancelClick() : this.props.dispatch(close());
  }

  handleClose() {
    !!this.props.closeModal ? this.props.closeModal() : this.props.dispatch(close());
  }

  confirmClickHandler() {
    if (this.props.isPrivate) {
      !this.props.noNeedClose && this.props.dispatch(close());
      this.props.callback();
    } else {
      this.props.callback();
      !this.props.noNeedClose && this.props.dispatch(close());
    }
  }

  render() {
    return (
      <div>
        <h5 style={{ display: 'inline-block' }}>
          {this.props.title ? tr(this.props.title) : tr('Confirm')}
        </h5>
        {this.props.closeModal && (
          <IconButton
            className="editor-image-btn-icon"
            onClick={this.handleClose}
            iconStyle={{ width: '20px' }}
            style={{ display: 'inline-block', right: '-16px', top: '-41px', position: 'absolute' }}
          >
            <span tabIndex={-1} className="hideOutline">
              <CloseIcon style={{ width: '20px' }} color="white" />
            </span>
          </IconButton>
        )}
        <div className="vertical-spacing-large">
          <TextField name="confirm" autoFocus={true} className="hiddenTextField" />
          <div>{tr(this.props.message)}</div>
        </div>
        <div className="modal-actions text-right">
          {!this.props.hideCancelBtn && (
            <SecondaryButton
              label={this.props.cancelBtnTitle ? tr(this.props.cancelBtnTitle) : tr('Cancel')}
              className="close"
              onTouchTap={this.cancelClickHandler}
            />
          )}
          <PrimaryButton
            label={this.props.confirmBtnTitle ? tr(this.props.confirmBtnTitle) : tr('Confirm')}
            className="confirm"
            onTouchTap={this.confirmClickHandler}
          />
        </div>
      </div>
    );
  }
}

ConfirmationModal.propTypes = {
  open: PropTypes.bool,
  noNeedClose: PropTypes.bool,
  isPrivate: PropTypes.bool,
  message: PropTypes.string,
  confirmBtnTitle: PropTypes.string,
  cancelBtnTitle: PropTypes.string,
  callback: PropTypes.func,
  cancelClick: PropTypes.func,
  closeModal: PropTypes.func,
  title: PropTypes.string,
  hideCancelBtn: PropTypes.bool
};

export default connect()(ConfirmationModal);
