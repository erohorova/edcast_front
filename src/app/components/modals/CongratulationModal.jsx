import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { close } from '../../actions/modalActions';
import onClickOutside from 'react-onclickoutside';

class CongratulationModal extends Component {
  constructor(props, context) {
    super(props, context);
  }

  handleClickOutside() {
    this.props.dispatch(close());
  }

  render() {
    return (
      <div className="congrats-modal-container">
        <h3 className="congrats-header">Congratulations!</h3>
        <p className="congrats-header badge-info">You earned a badge.</p>
        <img className="badge-icon" src={this.props.userBadge.imageUrl} alt="badge" />
        <h3 className="congrats-header">{this.props.userBadge.title}</h3>
      </div>
    );
  }
}

CongratulationModal.propTypes = {
  userBadge: PropTypes.object
};

export default connect()(onClickOutside(CongratulationModal));
