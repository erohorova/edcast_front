import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import find from 'lodash/find';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import { close } from '../../actions/modalActions';
let LocaleCurrency = require('locale-currency');
import TooltipLabel from '../common/TooltipLabel';
import TextField from 'material-ui/TextField';

class PaypalSuccessModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      }
    };
  }

  onCloseModal() {
    this.props.dispatch(close());
  }

  tooltipText = tooltipText => {
    return `<p className="tooltip-text">${tooltipText}</p>`;
  };

  render() {
    let card = this.props.paymentData.card;
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData =
      find(card.prices, { currency: currency }) || find(card.prices, { currency: 'USD' });
    let cardTitle = card.title || card.message;
    return (
      <div id="card-wallet-payment-modal">
        <div>
          <div className="close close-button">
            <IconButton
              aria-label="close"
              style={{ paddingRight: 0, width: 'auto' }}
              onTouchTap={this.onCloseModal.bind(this)}
            >
              <CloseIcon color="white" />
            </IconButton>
            <TextField name="paypalmodal" autoFocus={true} className="hiddenTextField" />
          </div>

          <div className="card-wallet-payment-modal-title">Payment Successful</div>

          <div className="row">
            <div className="small-7 columns">
              <div className="payment-success">
                Payment Successful
                <div>
                  <img width="50" src="/i/images/check-green.png" />
                </div>
                <div style={{ marginTop: '20px' }}>
                  <strong>Amount Data</strong>
                </div>
                <div style={{ marginTop: '20px' }}>A receipt is sent to your Email</div>
                <div style={{ marginTop: '30px' }}>
                  <a target="_blank" href={this.props.paymentData.redirectUrl}>
                    Go to the course
                  </a>
                </div>
              </div>
            </div>

            <div className="small-5 order-details">
              <div className="details-heading">Order Details</div>
              <div className="order-info">
                <div className="row">
                  <div className="small-3 columns">Course:</div>
                  <div className="small-9 columns" style={{ fontSize: '16px' }}>
                    <TooltipLabel html={true} text={this.tooltipText(cardTitle)}>
                      <p>{cardTitle.length > 200 ? cardTitle.slice(0, 200) + '...' : cardTitle}</p>
                    </TooltipLabel>
                    <div className="providerImage">
                      {card.providerImage && <img src={card.providerImage} />}
                    </div>
                  </div>
                </div>

                <div className="row flex-center">
                  <div className="small-3 columns">Price:</div>
                  <div className="small-9 columns" style={{ fontSize: '16px', fontWeight: '600' }}>
                    {priceData.amount}
                    {priceData.currency}
                  </div>
                </div>

                <div className="row flex-center" style={{ marginTop: '30px' }}>
                  <div className="small-3 columns">Total:</div>
                  <div className="small-9 columns" style={{ fontSize: '20px', fontWeight: '600' }}>
                    {priceData.amount}
                    {priceData.currency}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PaypalSuccessModal.propTypes = {
  open: PropTypes.bool,
  paymentData: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(PaypalSuccessModal);
