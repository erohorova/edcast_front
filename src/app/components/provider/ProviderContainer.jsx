import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Paper from 'edc-web-sdk/components/Paper';
import Result from './Result.jsx';

class ProviderContainer extends Component {
  constructor(props, context) {
    // just another constructor :D
    super(props, context);
  }

  componentWillReceiveProps(nextProps) {
    // nothing here as of now :D, can be used to catch the prop changes
  }

  render() {
    // initializing flags & variables

    let logoUrl; // the logo URL for the provider
    let showResults; // flag to show results

    // getting the array of items only if they are retrieved from the API
    let results = this.props.originItems.results.length > 0 ? this.props.originItems.results : [];
    let cardTypes =
      (this.props.originItems.facets.card_type != undefined &&
        this.props.originItems.facets.card_type.buckets.length) > 0
        ? this.props.originItems.facets.card_type.buckets
        : [];
    // let results = this.props.originItems.results.length > 0 ? this.props.originItems.results : [];

    // show results only if some results are present
    if (results.length > 0) showResults = true;

    // hardcoding logo URL's as of now as we are supporting only 2 providers on the discovery page which are BOX & GetAbstract
    // the logo files are uploaded in the s3 bucket called "edc-dev-web"
    let providerName = this.props.params.providerName ? this.props.params.providerName : '';
    let providerLogos = {
      box: 'https://s3.amazonaws.com/edc-dev-web/assets/box-logo_1.png',
      get_abstract: 'https://s3.amazonaws.com/edc-dev-web/assets/getabstract-logo.png',
      lynda: 'https://platformd-public.s3.amazonaws.com/media/lynda_logo-0-a.png'
    };

    return (
      <div id="provider" className="container-padding">
        <div className="row">
          <Paper className="provider-paper container-padding">
            <div className="row">
              <div className="small-6 columns provider-logo">
                <img src={providerLogos[providerName]} />
              </div>
            </div>
            {/*
                  iterating over the results array & loading the Result.JSX component with the data retrieved from the API
                  only of showResults flag is set to true
                */}
            {showResults &&
              results.map((result, index) => {
                let title = result.title ? result.title : '';
                let description = result.message ? result.message : '';
                let tags = result.tags != undefined ? result.tags : [];
                let indexVal = index;
                let visitUrl = '';
                if (result.slug != undefined && result.slug != '') {
                  visitUrl = '/insights/' + encodeURIComponent(result.slug) + '/visit';
                }
                return (
                  <div key={index}>
                    <Result
                      index={indexVal}
                      title={title}
                      description={description}
                      tags={tags}
                      visitUrl={visitUrl}
                    />
                  </div>
                );
              })}
          </Paper>
        </div>
      </div>
    );
  }
}

ProviderContainer.defaultProps = {
  originItems: {
    results: [],
    facets: {}
  }
};

ProviderContainer.propTypes = {
  originItems: PropTypes.object,
  params: PropTypes.object
};

function mapStoreStateToProps(state) {
  return Object.assign({}, state.origin.toJS());
}

export default connect(mapStoreStateToProps)(ProviderContainer);
