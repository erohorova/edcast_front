import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import colors from 'edc-web-sdk/components/colors/index';
import SearchIcon from 'material-ui/svg-icons/action/search';
import CardSection from '../common/CardSection';
import { eclSearch, getAllSources } from 'edc-web-sdk/requests/ecl';
import { providers } from 'edc-web-sdk/requests/providers';
import Card from '../cards/Card';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import { close, openStatusModal } from '../../actions/modalActions';
import { getUserById } from 'edc-web-sdk/requests/users';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';

class PremiumContentContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      labels: [],
      showSearchResults: false,
      pendingLike: false,
      providerName: props.routeParams.provider.replace(/-/g, '_'),
      searchCards: [],
      levels: [],
      currentProvider: null,
      sortInfo: { sort_attr: 'created_at', sort_order: 'desc' },
      searchTopic: '',
      sourceTypeId: null,
      pending: true,
      pendingShowMore: false,
      logo: null,
      banner: null,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      totalCardsCountReceived: 0
    };
    this.styles = {
      align: {
        margiTop: '0.6rem'
      },
      breadcrumbColor: {
        color: '#6f708b'
      }
    };
  }

  componentDidMount() {
    this.props.dispatch(
      getSpecificUserInfo(
        [
          'followingChannels',
          'roles',
          'rolesDefaultNames',
          'writableChannels',
          'first_name',
          'last_name'
        ],
        this.props.currentUser
      )
    );
    let newLabels = [];
    let providerName = this.props.routeParams.provider;
    let breadcrumbTitle = providerName.toLowerCase().replace(/(\b|_)\w/g, function(m) {
      return m.toUpperCase().replace(/_/, ' ');
    });
    this.setState({ providerName: providerName, breadcrumbTitle: breadcrumbTitle });
    let display_name = this.props.routeParams.provider.replace(/-/g, '_');
    let breadCrumbObj = this.props.team.OrgConfig.topMenu;
    let currentProvider =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')['discover/featuredProviders'] &&
      this.props.discovery
        .get('carousels')
        ['discover/featuredProviders'].get('data')
        .find(provider => {
          return (
            provider.source_type_name == this.props.routeParams.provider ||
            provider.display_name.toLowerCase().replace(/\s/g, '-') ==
              this.props.routeParams.provider
          );
        });

    if (!!currentProvider) {
      let banner_url =
        currentProvider.banner_url ||
        '"/i/images/banner' + (Math.floor(Math.random() * 8) + 1) + '.jpg"';
      let sortInfo =
        currentProvider.sort_info && currentProvider.sort_info.hasOwnProperty('sort_attr')
          ? currentProvider.sort_info
          : this.state.sortInfo;
      this.setState({
        currentProvider: currentProvider,
        sortInfo: sortInfo,
        sourceTypeId: currentProvider.id,
        pending: false,
        banner_url: banner_url,
        levels: [
          {
            name:
              breadCrumbObj['web/topMenu/discover'].label ||
              breadCrumbObj['web/topMenu/discover'].defaultLabel,
            url: '/discover'
          },
          { name: currentProvider.display_name, backToProvider: this.clearSearch }
        ]
      });
    } else {
      getUserById(this.props.currentUser.id)
        .then(user => {
          let teams = user.userTeams;
          let team_ids = teams.map(team => {
            return team.teamId;
          });
          if (team_ids.length == 0) {
            team_ids.push('0');
          }
          this.getCurrentProvider({
            display_name: display_name,
            'team_ids[]': team_ids
          });
        })
        .catch(err => {
          console.error(`Error in PremiumContentContainer.getUserById.func : ${err}`);
        });
    }

    if (!!this.props.currentUser.profile.learningTopics) {
      this.props.currentUser.profile.learningTopics.map(obj => {
        if (!newLabels.some(topic => topic.topic_label === obj.topic_label)) {
          newLabels.push({ topic_name: obj.topic_name, topic_label: obj.topic_label });
        }
      });
      this.setState({
        labels: newLabels
      });
    }
  }

  getCurrentProvider = payload => {
    let breadCrumbTitle = this.props.team.OrgConfig.topMenu;
    getAllSources(payload)
      .then(featuredProviders => {
        let currentProvider = featuredProviders.find(item => {
          return (
            item.source_type_name == this.props.routeParams.provider ||
            item.display_name.toLowerCase().replace(/\s/g, '-') == this.props.routeParams.provider
          );
        });
        if (currentProvider) {
          let banner_url =
            currentProvider.banner_url ||
            '"/i/images/banner' + (Math.floor(Math.random() * 8) + 1) + '.jpg"';
          let sortInfo =
            currentProvider.sort_info && currentProvider.sort_info.hasOwnProperty('sort_attr')
              ? currentProvider.sort_info
              : this.state.sortInfo;
          this.setState({
            currentProvider: currentProvider,
            sortInfo: sortInfo,
            sourceTypeId: currentProvider.id,
            pending: false,
            banner_url: banner_url,
            levels: [
              {
                name:
                  breadCrumbTitle['web/topMenu/discover'].label ||
                  breadCrumbTitle['web/topMenu/discover'].defaultLabel,
                url: '/discover'
              },
              { name: currentProvider.display_name, backToProvider: this.clearSearch }
            ]
          });
        } else {
          this.setState({ pending: false });
          if (this.state.newModalAndToast) {
            this.props.dispatch(
              openSnackBar(
                "You don't have access to this provider",
                () => (window.location.href = '/')
              )
            );
          } else {
            this.props.dispatch(
              openStatusModal(
                "You don't have access to this provider",
                () => (window.location.href = '/')
              )
            );
          }
        }
      })
      .catch(err => {
        console.error(`Error in PremiumContentContainer.getAllSources.func : ${err}`);
      });
  };

  searchEnterHandler = e => {
    if (e.keyCode === 13) {
      let query = e.target.value.trim();
      this.searchContent(query);
    }
  };

  loadMore = () => {
    let query = this._searchInput.value;
    this.setState({ showSearchResults: true, pendingShowMore: true });
    eclSearch({
      q: query,
      'source_id[]': this.state.sourceTypeId,
      limit: 40,
      offset: this.state.searchCards.length,
      sort_attr: this.state.sortInfo.sort_attr,
      sort_order: this.state.sortInfo.sort_order
    })
      .then(data => {
        let allCards = this.state.searchCards.concat(data.cards.slice(0, data.cards.length));
        this.setState({
          searchCards: allCards,
          offset: allCards.length,
          pendingShowMore: false,
          showSearchResults: true
        });
      })
      .catch(() => {
        this.setState({ pendingShowMore: false });
      });
  };

  searchContent = query => {
    let newLevels = this.state.levels;
    if (query === '') {
      if (this.state.levels[2].name !== this.state.currentProvider.display_name) {
        newLevels = this.state.levels.slice(0, 2);
      }
      this.setState({
        searchTopic: '',
        searchCards: [],
        showSearchResults: false,
        pending: false,
        levels: newLevels
      });
    } else {
      if (!newLevels[2] || newLevels[2].name !== query) {
        if (newLevels[2]) {
          newLevels.splice(2);
        }
        newLevels.push({ name: query });
        this.setState({
          pending: true,
          pendingShowMore: true,
          showSearchResults: true,
          levels: newLevels
        });
        eclSearch({
          q: query,
          'source_id[]': this.state.sourceTypeId,
          limit: 40,
          sort_attr: this.state.sortInfo.sort_attr,
          sort_order: this.state.sortInfo.sort_order
        })
          .then(data => {
            let source_id_cards_count =
              data.aggregations &&
              data.aggregations.filter(obj => {
                return obj.type == 'source_id' || obj.type == 'source_type_name';
              })[0].count;
            this.setState({
              searchCards: data.cards.slice(0, 40),
              pending: false,
              searchTopic: query,
              totalCardsCountReceived: source_id_cards_count,
              pendingShowMore: false
            });
          })
          .catch(() => {
            this.setState({ pending: false, pendingShowMore: false, searchCards: [] });
          });
      }
    }
  };

  clickSearchButton = () => {
    let query = this._searchInput.value;
    this.searchContent(query);
  };

  clearSearch = () => {
    let newLevels = this.state.levels;
    if (this.state.levels[2].name !== this.state.currentProvider.display_name) {
      newLevels = this.state.levels.slice(0, 2);
    }
    this.setState(
      {
        searchTopic: '',
        searchCards: [],
        showSearchResults: false,
        levels: newLevels,
        totalCardsCountReceived: 0
      },
      () => {
        this._searchInput.value = '';
      }
    );
  };

  hideCard = id => {
    let searchCards = this.state.searchCards.filter(card => card.id !== id);
    this.setState({ searchCards });
  };

  goBack = (e, path) => {
    e.preventDefault();
    path.backToProvider ? path.backToProvider() : this.props.dispatch(push(path.url));
  };

  render() {
    let banner_url = this.state.banner_url;
    return (
      <div id="premium-provider">
        {
          <div>
            <div className="breadcrumb-container">
              <div className="breadcrumb-padding row">
                <div className="small-12">
                  <div>
                    {this.state.levels.map((path, index) => {
                      if (index === this.state.levels.length - 1) {
                        return (
                          <div className="last breadcrumb" key={index}>
                            <div className="level">
                              &nbsp;
                              {path.name ||
                                (this.state.currentProvider &&
                                  this.state.currentProvider.source_type &&
                                  this.state.currentProvider.display_name)}
                            </div>
                          </div>
                        );
                      }
                      return (
                        <div
                          className={index === 0 ? 'breadcrumb first' : 'breadcrumb'}
                          key={index}
                        >
                          <a
                            style={this.styles.breadcrumbColor}
                            href="#"
                            className="level"
                            onClick={e => this.goBack(e, path)}
                          >
                            &nbsp;{path.name}
                          </a>
                          {' >'}
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>

            <div className="row provider">
              <div className="small-12 container-padding">
                <div className="provider-banner">
                  <div
                    className="banner"
                    style={{
                      backgroundSize: 'cover',
                      backgroundPosition: 'center center',
                      backgroundImage: 'url(' + banner_url + ')'
                    }}
                  >
                    <div className="provider-banner-info">
                      {(this.state.currentProvider &&
                        this.state.currentProvider.source_type &&
                        this.state.currentProvider.source_type.image_url && (
                          <img
                            className="logo"
                            src={
                              this.state.currentProvider.logo_url ||
                              this.state.currentProvider.source_type.image_url
                            }
                          />
                        )) ||
                        (providers[this.props.routeParams.provider] &&
                          providers[this.props.routeParams.provider]['image'] && (
                            <img
                              className="logo"
                              src={providers[this.props.routeParams.provider]['image']}
                            />
                          )) || (
                          <h4 className="logo">
                            {this.state.currentProvider && this.state.currentProvider.source_type
                              ? this.state.currentProvider.source_type.display_name
                              : this.state.breadcrumbTitle}
                          </h4>
                        )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="provider-search">
              <div className="row">
                <div className="small-12 search-column">
                  <div className="search-container">
                    <input
                      className="search-box"
                      style={{ color: `${colors.gray}` }}
                      onKeyUp={this.searchEnterHandler}
                      ref={node => (this._searchInput = node)}
                    />
                    <button
                      className="search-text text-center search"
                      onClick={this.clickSearchButton}
                    >
                      <small className="search" style={{ backgroundColor: colors.iron }}>
                        <SearchIcon style={{ verticalAlign: 'middle' }} />{' '}
                        <span style={this.styles.align}>{tr('Search')}</span>
                      </small>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {this.state.pending && (
              <div className="progress text-center">
                <Spinner />
              </div>
            )}
            {!this.state.pending && !this.state.showSearchResults && (
              <div style={{ display: !this.state.showSearchResults ? 'inline' : 'none' }}>
                <div className="provider-recommended">
                  <div className="row">
                    <div className="small-12 vertical-spacing-small">
                      {this.state.labels.length === 0 && (
                        <div className="container-padding">
                          {tr('Get personalized recommendations by')}{' '}
                          <a
                            className="meProfile"
                            onTouchTap={() => {
                              this.props.dispatch(push('/me/learners-dashboard/'));
                            }}
                          >
                            {tr('reviewing your learning goals.')}
                          </a>
                        </div>
                      )}
                      {this.state.labels.map(topic => {
                        return (
                          <CardSection
                            key={topic['topic_label']}
                            topic={topic}
                            providerLogos={banner_url}
                            currentUser={this.props.currentUser}
                            sortInfo={this.state.sortInfo}
                            sourceId={this.state.sourceTypeId}
                            provider={this.state.providerName}
                          />
                        );
                      })}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 vertical-spacing-small">
                    {!this.state.pending && (
                      <div className="text-center">
                        <br />
                        <br />
                        <strong>{tr('BROWSE ALL CARDS')}</strong>
                        <br />
                      </div>
                    )}
                    <CardSection
                      topic={''}
                      providerLogos={banner_url}
                      sortInfo={this.state.sortInfo}
                      currentUser={this.props.currentUser}
                      sourceId={this.state.sourceTypeId}
                      provider={this.props.routeParams.provider}
                    />
                  </div>
                </div>
              </div>
            )}
            <div className="row provider">
              {this.state.showSearchResults && (
                <div className="row">
                  <div className="small-12 vertical-spacing-small">
                    <div className="container-padding vertical-spacing-large">
                      <div className="custom-card-container">
                        {!this.state.pendingShowMore && this.state.searchCards.length == 0 && (
                          <div className="container-padding data-not-available-msg">
                            {tr('No search results found.')}
                          </div>
                        )}
                        <div className="four-card-column vertical-spacing-medium">
                          {this.state.searchCards.map(card => {
                            return (
                              <Card
                                key={card.id}
                                card={card}
                                author={card.author}
                                providerLogos={banner_url}
                                disableTopics={true}
                                providerCards={false}
                                dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                                startDate={
                                  card.startDate || (card.assignment && card.assignment.startDate)
                                }
                                moreCards={true}
                                removeCardFromList={this.hideCard}
                                hideComplete={this.hideCard}
                                user={this.props.currentUser}
                              />
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </div>
                  {!this.state.pendingShowMore &&
                    (this.state.searchCards.length != 0 &&
                      this.state.searchCards.length < this.state.totalCardsCountReceived) && (
                      <div style={{ textAlign: 'center', margin: '0 auto' }}>
                        <SecondaryButton
                          label={tr('Show More')}
                          className="viewMore"
                          onTouchTap={this.loadMore}
                        />
                      </div>
                    )}
                </div>
              )}
            </div>
            {this.state.pendingShowMore &&
              this.state.showSearchResults &&
              this.state.searchCards.length > 0 && (
                <div className="progress text-center">
                  <Spinner />
                </div>
              )}
          </div>
        }
      </div>
    );
  }
}

PremiumContentContainer.propTypes = {
  routeParams: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  discovery: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS(),
    discovery: state.discovery
  };
}

export default connect(mapStoreStateToProps)(PremiumContentContainer);
