import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index'; // same as above this component is used for using predefined color variables
import { tr } from 'edc-web-sdk/helpers/translations';

class Result extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillReceiveProps(nextProps) {
    // console.log(nextProps);
  }

  render() {
    // filling variables with passed prop values from ProviderContainer.jsx
    let index = this.props.index ? this.props.index + 1 : 0;
    let title = this.props.title ? this.props.title.substr(0, 100) : '';
    let tags = this.props.tags != undefined && this.props.tags.length > 0 ? this.props.tags : [];
    let description = this.props.description
      ? this.props.description.replace(/<(?:.|\n)*?>/gm, '').substr(0, 300)
      : '';
    let visitUrl = this.props.visitUrl ? this.props.visitUrl : '';

    // the same old flags ;)
    let showTags;
    let tagsLength = 0;

    // show tags only if available
    if (tags.length > 0) {
      showTags = true;
      tagsLength = tags.length;
    }

    // the code below is self explainatory (y)
    return (
      <div className="provider-content" key={index}>
        <div className="row content-row">
          <div className="columns content-number">
            <div>{index + 1}</div>
          </div>
          <div className="small-10 columns">
            <div className="content-heading">
              <a
                title={tr('open in new tab')}
                className="matte-hover"
                href={visitUrl}
                target="_blank"
              >
                {title}
              </a>
            </div>
            <div className="content-desc">{description}...</div>
            <div className="content-tags">
              {showTags &&
                tags.map((tag, index2) => {
                  let tagName = tag.name ? tag.name : '';
                  return (
                    <span key={index2}>
                      #{tagName}
                      {/*
												not showing the comma for the last item
											*/}
                      {index2 < tagsLength && <span>,</span>}
                    </span>
                  );
                })}
              <span>{tr('#cloud computing')}, </span>
              <span>{tr('#cloud computing')}, </span>
              <span>{tr('#cloud computing')}, </span>
              <span>{tr('#cloud computing')}, </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Result.defaultProps = {
  title: '',
  description: '',
  tags: [],
  visitUrl: ''
};

Result.propTypes = {
  title: PropTypes.string,
  index: PropTypes.number,
  description: PropTypes.string,
  tags: PropTypes.array,
  visitUrl: PropTypes.string
};

export default connect()(Result);
