const topics = [
  {
    topic_label: 'Application Development',
    topic_name: 'edcast.technology.software_development.application_development'
  },
  {
    topic_label: 'Business Strategy',
    topic_name: 'edcast.business.leadership_and_management.business_strategy'
  },
  {
    topic_label: 'Cloud Computing',
    topic_name: 'edcast.technology.it_infrastructure.cloud_computing'
  },
  {
    topic_label: 'Coaching',
    topic_name: 'edcast.business.corporate_learning_development.coaching'
  },
  { topic_label: 'Communication', topic_name: 'edcast.business.communication' },
  { topic_label: 'Compliance', topic_name: 'edcast.business.corporate_governance.compliance' },
  { topic_label: 'Content Marketing', topic_name: 'edcast.business.marketing.content_marketing' },
  {
    topic_label: 'Customer Service',
    topic_name: 'edcast.business.professional_development.customer_service'
  },
  { topic_label: 'Customer Success', topic_name: 'edcast.business.customer_success' },
  {
    topic_label: 'Data Analytics',
    topic_name: 'edcast.technology.data_science_and_analytics.data_analytics'
  },
  {
    topic_label: 'Data Science',
    topic_name: 'edcast.technology.data_science_and_analytics.data_science'
  },
  {
    topic_label: 'Database Administration',
    topic_name: 'edcast.technology.data_science_and_analytics.database_administration'
  },
  {
    topic_label: 'Design Thinking',
    topic_name: 'edcast.art_and_design.graphic_design.design_thinking'
  },
  { topic_label: 'Digital Marketing', topic_name: 'edcast.business.marketing.digital_marketing' },
  {
    topic_label: 'Entrepreneurship',
    topic_name: 'edcast.business.professional_development.entrepreneurship'
  },
  {
    topic_label: 'Game Development',
    topic_name: 'edcast.technology.game_design_and_development.game_development'
  },
  { topic_label: 'Human Resources', topic_name: 'edcast.business.human_resources' },
  { topic_label: 'Information Technology', topic_name: 'edcast.technology.information_technology' },
  { topic_label: 'Leadership', topic_name: 'edcast.business.leadership_and_management.leadership' },
  {
    topic_label: 'Learning and Development',
    topic_name: 'edcast.business.corporate_learning_development.learning_and_development_agenda'
  },
  {
    topic_label: 'Machine Learning',
    topic_name: 'edcast.technology.artificial_intelligence.machine_learning.machine_learning'
  },
  { topic_label: 'Management', topic_name: 'edcast.business.leadership_and_management.management' },
  { topic_label: 'Market Research', topic_name: 'edcast.business.marketing.market_research' },
  { topic_label: 'Marketing', topic_name: 'edcast.business.marketing.marketing' },
  { topic_label: 'Negotiation', topic_name: 'edcast.business.negotiation' },
  {
    topic_label: 'Network and Information Security',
    topic_name: 'edcast.technology.it_infrastructure.network_and_information_security'
  },
  { topic_label: 'Presentations', topic_name: 'edcast.business.business_software.presentations' },
  {
    topic_label: 'Project Management',
    topic_name: 'edcast.business.project_management.project_management'
  },
  { topic_label: 'Sales Management', topic_name: 'edcast.business.sales.sales_management' },
  {
    topic_label: 'Search Engine Marketing (SEM)',
    topic_name: 'edcast.business.marketing.search_engine_marketing_sem'
  },
  {
    topic_label: 'Search Engine Optimization (SEO)',
    topic_name: 'edcast.business.marketing.search_engine_optimization_seo'
  },
  {
    topic_label: 'Six Sigma',
    topic_name: 'edcast.manufacturing.Supply_Chain.operations.six_sigma'
  },
  { topic_label: 'Social Media', topic_name: 'edcast.business.digital_lifestyle.social_media' },
  {
    topic_label: 'Software Engineering',
    topic_name: 'edcast.technology.software_development.software_engineering'
  },
  { topic_label: 'Start Ups', topic_name: 'edcast.business.leadership_and_management.start_ups' },
  {
    topic_label: 'Training',
    topic_name: 'edcast.business.corporate_learning_development.training'
  },
  {
    topic_label: 'User Testing',
    topic_name: 'edcast.technology.software_development.user_testing'
  },
  {
    topic_label: 'Web Development',
    topic_name: 'edcast.technology.software_development.web_development'
  },
  {
    topic_label: 'Product Management',
    topic_name: 'edcast.business.professional_development.product_management'
  }
];

export function getDefaultTopics(userInterestsList = [], number = 6) {
  let numberParam = number;
  let results = [];
  let filteredTopics = topics.filter(item => {
    return !~userInterestsList.indexOf(item['topic_label']);
  });
  while (numberParam > 0) {
    let topic = filteredTopics[Math.floor(Math.random() * filteredTopics.length)];
    if (!results.some(topicItem => topicItem.topic_label === topic.topic_label)) {
      results.push(topic);
      numberParam--;
    }
  }
  return results;
}
