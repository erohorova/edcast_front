import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import PinIcon from 'edc-web-sdk/components/icons/Pin';
import UnpinIcon from 'edc-web-sdk/components/icons/Unpin';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class GroupCardOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinned: props.pinnedStatus || false,
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      iconStyle: {
        width: '30px',
        height: '30px'
      },
      iconPosition: {
        margin: '10px',
        'margin-top': '65px'
      },
      iconPositionV3: {
        margin: '10px',
        'margin-top': '130px'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card && nextProps.card.pinnedStatus) {
      this.setState({ pinned: nextProps.card.pinnedStatus });
    }
  }

  pinUnpinCard = () => {
    if (this.props.pinnedCount == 10 && !this.state.pinned) {
      this.props.dispatch(openSnackBar('You can pin max 10 cards!', true));
      return;
    }
    this.props.pinUnpinCard(this.props.card, !this.state.pinned);
    this.setState({ pinned: !this.state.pinned });
  };

  render() {
    return (
      <div className={`card-overlay-layer ${this.state.isCardV3 ? 'card-overlay-layer-v3' : ''}`}>
        <div className="row action-icons-row">
          <div className="small-12 columns icon-wrapper">
            {!this.state.pinned && (
              <div
                className="specific-icon-parent-wrapper"
                style={this.state.isCardV3 ? this.styles.iconPositionV3 : this.styles.iconPosition}
              >
                <IconButton onTouchTap={this.pinUnpinCard}>
                  <PinIcon customStyle={this.styles.iconStyle} />
                </IconButton>
                <div className="text-center icon-label">{tr('Mark Featured')}</div>
              </div>
            )}
            {this.state.pinned && (
              <div
                className="specific-icon-parent-wrapper"
                style={this.state.isCardV3 ? this.styles.iconPositionV3 : this.styles.iconPosition}
              >
                <IconButton className="unpin" onTouchTap={this.pinUnpinCard}>
                  <UnpinIcon customStyle={this.styles.iconStyle} color={'#4de8ce'} />
                </IconButton>
                <div className="text-center icon-label">{tr('Remove Featured')}</div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

GroupCardOverlay.propTypes = {
  card: PropTypes.object,
  pinUnpinCard: PropTypes.func,
  pinnedCount: PropTypes.number,
  group: PropTypes.object,
  pinnedStatus: PropTypes.bool,
  setOffset: PropTypes.func,
  deleteCard: PropTypes.func
};

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(GroupCardOverlay);
