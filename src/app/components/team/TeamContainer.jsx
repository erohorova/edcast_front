/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import GroupsContainer from './groups/GroupsContainer';

class TeamContainer extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="tabs-content" id="team">
        <div id="channels" className="content teamChannels" />
        <div id="pathways" className="content teamPathways" />
        <div id="courses" className="content teamCourses" />
        <div id="livestreams" className="content teamLivestreams" />
        <GroupsContainer />
        <div id="members" className="content teamMembers" />
        <div id="about" className="content teamAbout" />
        <div id="analytics" className="content teamAnalytics" />
      </div>
    );
  }
}

TeamContainer.propTypes = {};

export default connect()(TeamContainer);
