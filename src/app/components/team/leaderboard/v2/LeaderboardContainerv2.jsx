import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import moment from 'moment';

import { Calendar, DateRange } from 'react-date-range';
import AutoComplete from 'material-ui/AutoComplete';
import MenuItem from 'material-ui/MenuItem';
import Spinner from '../../../common/spinner';

import NavigationClose from 'react-material-icons/icons/navigation/close';
import CircularProgress from 'material-ui/CircularProgress';
import ActionSearch from 'react-material-icons/icons/action/search';

import { teams, topics } from 'edc-web-sdk/requests';
import { getList } from 'edc-web-sdk/requests/groups.v2';
import * as rolesSDK from 'edc-web-sdk/requests/roles';
import colors from 'edc-web-sdk/components/colors/index';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';

import { Permissions } from '../../../../utils/checkPermissions';
import { updateLeaderboardUser } from '../../../../actions/usersActions';
import LeaderboardListItemv2 from '../../../common/LeaderboardListItemv2';
import MultiSelect from '../../../common/MultiSelect';
import { getGroupDetail } from '../../../../actions/groupsActionsV2';
import { getSpecificUserInfo } from '../../../../actions/currentUserActions';
import ReactDOM from 'react-dom';

class LeaderboardContainerv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.format = 'MM/DD/YYYY';
    this.state = {
      currentUser: {},
      leaderboardList: [],
      scores: [],
      pending: true,
      rangePicker: {},
      isDataRangeOpen: false,
      isExpertiseOpen: false,
      isUserTypeOpen: false,
      isGroupsOpen: false,
      dates: ' All time',
      values: '',
      groups: [],
      skillsArray: [],
      selectedSkillsIds: [],
      skillSource: [],
      searchText: '',
      selectedGroups: [],
      selectedRoles: [],
      roles: [],
      expertise: {},
      filter: {
        period: 'all_time',
        show_my_result: true,
        order_attr: 'smartbites_score',
        limit: 10,
        offset: 0,
        q: ''
      },
      isLastRolePage: false,
      isLastGroupPage: false,
      groupIdFromURL: null,
      noScores: false,
      startDateRange: moment(),
      endDateRange: moment(),
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      searchParam: props.location ? props.location.search : '',
      skillLoader: false,
      genpactUI: window.ldclient.variation('genpact-ui', false),
      isLeaderboardV2: window.ldclient.variation('leaderboardV2', false)
    };
    this.styles = {
      popoverProps: {
        bottom: '0.7rem',
        overflowY: 'auto',
        background: 'transparent',
        boxShadow: 'none'
      },
      autoCompleteMenu: {
        background: 'white',
        overflowY: 'auto'
      },
      autoCompleteItem: {
        minHeight: '1.5625rem',
        lineHeight: '1.5625rem',
        fontSize: '0.625rem',
        color: '#454560'
      }
    };
    this.limit = 10;
    this.offsetUser = 0;
    this.pending = false;
    this.isLastPage = false;

    this.handleChangeQuery = this.handleChangeQuery.bind(this);
  }

  handleChangeQuery = _.debounce(
    () => {
      if (this.refs && this.refs.queryInput) {
        let query = this.refs.queryInput.value;
        this.waitingFor = query;
        let filter = this.state.filter;
        filter.q = query;
        filter.offset = 0;
        this.setState(
          {
            filter
          },
          function() {
            if (this.state.isLeaderboardV2) {
              delete filter.period;
              !filter.q && delete filter.q;
            }
            teams
              .getUserScores(filter, this.state.isLeaderboardV2)
              .then(scores => {
                if (query === this.waitingFor) {
                  let scoresUniq = scores.users;
                  let currentUser = scores.currentUser;
                  let scoresState = scoresUniq;
                  if (
                    currentUser &&
                    currentUser.user &&
                    scoresUniq.length &&
                    scoresUniq.map(obj => obj.user.id).filter(id => id == currentUser.user.id)
                      .length == 0
                  ) {
                    scoresState = scoresUniq.concat(currentUser);
                    this.props.dispatch(updateLeaderboardUser(scoresState));
                  } else {
                    this.props.dispatch(updateLeaderboardUser(scoresUniq));
                  }
                  this.pending = false;
                  this.offsetUser = scores.users.length;
                  this.isLastPage = scoresUniq.length >= scores.totalCount;
                  this.setState({
                    currentUser: currentUser,
                    leaderboardList: scoresUniq,
                    scores: scoresState,
                    pending: false,
                    noScores: !scoresUniq.length
                  });
                }
              })
              .catch(err => {
                console.error(`Error in LeaderboardContainerv2.getUserScores.func : ${err}`);
              });
          }
        );
      }
    },
    150,
    { leading: false, trailing: true }
  );

  handleChangeDateRange(which, dates) {
    let datesString = '';
    let filter = this.state.filter;
    if (dates['startDate'] == dates['endDate']) {
      return;
    }
    if (dates['startDate'] && dates['endDate']) {
      this.setState({
        startDateRange: dates['startDate'],
        endDateRange: dates['endDate']
      });
      datesString = `${dates['startDate'].format(this.format).toString()} - ${dates['endDate']
        .format(this.format)
        .toString()}`;
    }
    filter.from_date = dates['startDate'].format(this.format);
    filter.to_date = dates['endDate'].format(this.format);
    delete filter.period;

    this.setState(
      {
        rangePicker: dates,
        dates: datesString,
        filter,
        isDataRangeOpen: false
      },
      this.updateFilter('dates', filter)
    );
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(['defaultTeamId'], this.props.currentUser);
    await this.props.dispatch(userInfoCallBack);
    if (!(this.props.currentUser && this.props.currentUser.defaultTeamId)) {
      this.groupPreselect();
    }
    let filter = this.state.filter;
    this.pending = true;
    getList(this.limit, 0)
      .then(groups => {
        let isLastGroupPage = groups.length < this.limit;
        this.setState(
          { groups: _.uniqBy(this.state.groups.concat(groups), 'id'), isLastGroupPage },
          () => {
            if (this.props.currentUser && this.props.currentUser.defaultTeamId) {
              let group_ids = [this.props.currentUser.defaultTeamId];
              filter['group_ids[]'] = group_ids;
              this.groupPreselect();
            } else {
              if (this.state.isLeaderboardV2) {
                delete filter.period;
                !filter.q && delete filter.q;
              }
              teams
                .getUserScores(filter, this.state.isLeaderboardV2)
                .then(scores => {
                  let scoresUniq = scores.users;
                  let currentUser = scores.currentUser;
                  let scoresState = scoresUniq;
                  if (
                    currentUser &&
                    currentUser.user &&
                    scoresUniq.length &&
                    scoresUniq.map(obj => obj.user.id).filter(id => id == currentUser.user.id)
                      .length == 0
                  ) {
                    scoresState = scoresUniq.concat(currentUser);
                    this.props.dispatch(updateLeaderboardUser(scoresState));
                  } else {
                    this.props.dispatch(updateLeaderboardUser(scoresUniq));
                  }
                  this.pending = false;
                  this.offsetUser = scores.users.length;
                  this.isLastPage = scoresUniq.length >= scores.totalCount;
                  this.setState({
                    currentUser: currentUser,
                    leaderboardList: scoresUniq,
                    scores: scoresState,
                    pending: false,
                    noScores: !scoresUniq.length
                  });
                })
                .catch(err => {
                  console.error(`Error in LeaderboardContainerv2.getUserScores.func : ${err}`);
                });
            }
          }
        );
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.getList.func : ${err}`);
      });
    rolesSDK
      .getItems({ limit: this.limit })
      .then(data => {
        let isLastRolePage = data.length < this.limit;
        if (data) this.setState({ roles: data.roles, isLastRolePage });
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.getItems.func : ${err}`);
      });
    topics
      .getSuggestedTopicsForUserOnboarding(1, 12)
      .then(topicsData => {
        this.setState({
          topics: topicsData.domains,
          isLastTopicPage: topicsData.domains && topicsData.domains.length < 12
        });
      })
      .catch(err => {
        console.error(
          `Error in LeaderboardContainerv2.getSuggestedTopicsForUserOnboarding.func : ${err}`
        );
      });
    this.handleSkillInput();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = _.throttle(
    () => {
      if (
        !this.pending &&
        window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight &&
        !this.isLastPage
      ) {
        this.showMoreUsers();
      } else return null;
    },
    150,
    { leading: false }
  );

  showMoreUsers = () => {
    this.pending = true;
    this.setState({ pending: true });
    let filter = this.state.filter;
    filter.offset = this.offsetUser;
    filter.show_my_result = false;
    if (this.state.isLeaderboardV2) {
      delete filter.period;
      !filter.q && delete filter.q;
    }
    teams
      .getUserScores(filter, this.state.isLeaderboardV2)
      .then(data => {
        this.props.dispatch(updateLeaderboardUser(data.users));
        let users = this.state.leaderboardList.concat(data.users);
        let currentUser = this.state.currentUser;
        let scoresState = users;
        if (
          currentUser &&
          currentUser.user &&
          users.length &&
          users.map(obj => obj.user.id).filter(id => id == currentUser.user.id).length == 0
        ) {
          scoresState = users.concat(currentUser);
          this.props.dispatch(updateLeaderboardUser(scoresState));
        } else {
          this.props.dispatch(updateLeaderboardUser(users));
        }
        this.pending = false;
        this.offsetUser = users.length;
        this.isLastPage = users.length >= data.totalCount;
        this.setState({
          leaderboardList: users,
          scores: scoresState,
          pending: false,
          noScores: !users.length
        });
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.showMoreUsers.getUserScores.func : ${err}`);
      });
  };

  toogleDataRange = () => {
    this.setState(prevState => ({
      isDataRangeOpen: !prevState.isDataRangeOpen
    }));
  };

  toogleExpertise = () => {
    if (!this.state.skillLoader) {
      this.setState(
        prevState => ({
          isExpertiseOpen: !prevState.isExpertiseOpen
        }),
        () => {
          if (this.state.isExpertiseOpen) {
            let inputs = document.querySelector('.auto-complete-as-input input');
            if (inputs) {
              inputs.focus();
            }
          }
        }
      );
    }
  };

  showMoreGroups = callback => {
    let offset = this.state.groups.length;
    getList(this.limit, offset)
      .then(groups => {
        let isLastGroupPage = groups.length < this.limit;
        this.setState(
          { groups: _.uniqBy(this.state.groups.concat(groups), 'id'), isLastGroupPage },
          callback
        );
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.showMoreGroups.getList.func : ${err}`);
      });
  };

  showMoreRoles = callback => {
    let offset = this.state.roles.length;
    rolesSDK
      .getItems({ limit: this.limit, offset: offset })
      .then(data => {
        let roles = data ? data.roles : [];
        let isLastRolePage = roles.length < this.limit;
        this.setState(
          { roles: _.uniqBy(this.state.roles.concat(roles), 'id'), isLastRolePage },
          callback
        );
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.showMoreRoles.getItems.func : ${err}`);
      });
  };

  handleSkillInput = (value = '') => {
    this.setState({ searchText: value, skillLoader: true }, () => {
      let payload = {
        query: this.state.searchText,
        per_page: 5000,
        'topic_ids[]': []
      };
      topics
        .queryTopicsForLeaderBoard(payload)
        .then(topicsData => {
          this.setState({
            skillSource: topicsData.topics,
            skillLoader: false
          });
        })
        .catch(err => {
          console.error(`Error in LeaderboardContainerv2.queryTopicsForLeaderBoard.func : ${err}`);
          this.setState({
            skillSource: [],
            skillLoader: false
          });
        });
    });
  };

  selectSkillInput = skill => {
    let learningTopic = {
      domain_id: skill.domain.id,
      domain_label: skill.domain.label,
      domain_name: skill.domain.name,
      topic_id: skill.id,
      topic_label: skill.label,
      topic_name: skill.name
    };
    this.setState({
      expertise: learningTopic,
      searchText: '',
      isExpertiseOpen: false
    });
    this.updateFilter('expertise_names', skill);
  };

  removeTopicHandler = e => {
    e.stopPropagation();
    this.setState({
      expertise: {}
    });
    this.updateFilter('expertise_names', null);
  };

  clearDateRange = () => {
    let filter = this.state.filter;
    filter.period = 'all_time';
    delete filter.from_date;
    delete filter.to_date;

    this.setState(
      {
        dates: ' All time',
        filter,
        startDateRange: moment(),
        endDateRange: moment()
      },
      this.updateFilter
    );
  };

  updateFilter = (type, selectItems) => {
    if (this.state.groupIdFromURL === false) {
      this.setState({ groupIdFromURL: true });
    }
    let filter = this.state.filter;
    filter.offset = 0;
    switch (type) {
      case 'roles':
        let roles = [];
        selectItems.map(elem => {
          roles.push(elem.name);
        });
        filter['roles[]'] = roles;
        break;
      case 'group_ids':
        let group_ids = [];
        selectItems.map(elem => {
          group_ids.push(elem.id);
        });
        filter['group_ids[]'] = group_ids;
        break;
      case 'expertise_names':
        if (selectItems) {
          filter['expertise_names[]'] = [selectItems.name];
        } else {
          delete filter['expertise_names[]'];
        }
        break;
      default:
        // FIXME: implement default case
        break;
    }
    filter.show_my_result = true;
    if (this.state.isLeaderboardV2) {
      delete filter.period;
      !filter.q && delete filter.q;
    }
    teams
      .getUserScores(filter, this.state.isLeaderboardV2)
      .then(scores => {
        let scoresUniq = _.uniqBy(scores.users, 'user.id');
        let currentUser = scores.currentUser;
        let scoresState = scoresUniq;
        if (
          currentUser &&
          currentUser.user &&
          scoresUniq.length &&
          scoresUniq.map(obj => obj.user.id).filter(id => id == currentUser.user.id).length === 0
        ) {
          scoresState = scoresUniq.concat(currentUser);
          this.props.dispatch(updateLeaderboardUser(scoresState));
        } else {
          this.props.dispatch(updateLeaderboardUser(scoresUniq));
        }
        this.pending = false;
        this.offsetUser = scores.users.length;
        this.isLastPage = scoresUniq.length >= scores.totalCount;
        this.setState({
          currentUser: currentUser,
          leaderboardList: scoresUniq,
          scores: scoresState,
          pending: false,
          noScores: !scoresUniq.length
        });
      })
      .catch(err => {
        console.error(`Error in LeaderboardContainerv2.updateFilter.getUserScores.func : ${err}`);
      });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.location && nextProps.location.search !== this.state.searchParam) {
      let groupIdFromURL = this.state.groupIdFromURL;
      if (nextProps.location.search === '') {
        groupIdFromURL = false;
        this.updateFilter('group_ids', []);
      } else {
        this.groupPreselect();
      }
      this.setState({ searchParam: nextProps.location.search, groupIdFromURL });
    }
  }
  groupPreselect = () => {
    this.pending = true;
    this.setState({ pending: true });
    let groupID = this.getUrlParams().group_id;
    if (!parseInt(groupID) && (this.props.currentUser && this.props.currentUser.defaultTeamId)) {
      groupID = this.props.currentUser.defaultTeamId;
    }
    this.setState({ groupIdFromURL: parseInt(groupID) });
    if (groupID && !(this.props.groups && this.props.groups[groupID])) {
      this.props
        .dispatch(getGroupDetail(groupID))
        .then(groupDetails => {
          this.setState({
            groups: _.uniqBy(this.state.groups.concat(groupDetails), 'id')
          });
          this.updateFilter('group_ids', [groupDetails]);
        })
        .catch(err => {
          console.error(`Error in LeaderboardContainerv2.getGroupDetail.func : ${err}`);
        });
    } else if (groupID) {
      this.setState({
        groups: _.uniqBy(
          this.state.groups.concat(this.props.groups && this.props.groups[groupID]),
          'id'
        )
      });
      this.updateFilter('group_ids', [this.props.groups && this.props.groups[groupID]]);
    }
  };

  getUrlParams = () => {
    let urlParams;
    (window.onpopstate = function() {
      var match,
        pl = /\+/g, // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function(s) {
          return decodeURIComponent(s.replace(pl, ' '));
        },
        query = window.location.search.substring(1);

      urlParams = {};
      while ((match = search.exec(query))) urlParams[decode(match[1])] = decode(match[2]);
    })();
    return urlParams;
  };

  render() {
    return (
      <div className={`lbv2 ${this.state.isNewProfileNavigation && 'lbv2_new'}`} id="leaderboardv2">
        <div className="row leaderboard-zero-padding">
          <div className="lbv2__title small-6 columns">
            {this.state.isNewProfileNavigation && (
              <div
                aria-label="back"
                className="breadcrumbBack"
                onClick={() => {
                  window.history.back();
                }}
              >
                <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
              </div>
            )}
            <span role="heading" aria-level="3">
              {tr('Leaderboard')}
            </span>
          </div>
          <div className="small-6 columns float-right">
            <input
              ref="queryInput"
              type="text"
              placeholder="Search for users"
              onChange={this.handleChangeQuery.bind(this)}
            />
          </div>
        </div>
        {Permissions['enabled'] !== undefined && Permissions.has('GET_LEADERBOARD_INFORMATION') && (
          <div className="lbv2__filter-row">
            <MultiSelect
              inputText={tr('Group')}
              itemName="group"
              items={this.state.groups}
              loadMoreData={this.showMoreGroups}
              isLastPage={this.state.isLastGroupPage}
              onChange={selectGroups => this.updateFilter('group_ids', selectGroups)}
              preSelectedItem={
                typeof this.state.groupIdFromURL !== 'boolean'
                  ? this.props.groups && this.props.groups[this.state.groupIdFromURL]
                  : this.state.groupIdFromURL
              }
            />
            <MultiSelect
              inputText={tr('User Type')}
              itemName="role"
              items={this.state.roles}
              loadMoreData={this.showMoreRoles}
              isLastPage={this.state.isLastRolePage}
              onChange={selectRoles => this.updateFilter('roles', selectRoles)}
            />
            <div className="lbv2__select-input-container">
              {this.state.isExpertiseOpen && (
                <div className="multiselect-overflow" onClick={this.toogleExpertise} />
              )}
              <div className="lbv2__select-input" onClick={this.toogleExpertise}>
                {this.state.genpactUI ? tr('Interests') : tr('Skills')}:{' '}
                {this.state.expertise.topic_label}
                {!!this.state.expertise.topic_label && (
                  <button
                    className="my-icon-button my-icon-button_small select-input__remove-value"
                    onClick={this.removeTopicHandler}
                    disabled={this.state.previewMode}
                  >
                    <NavigationClose color={colors.darkGray} />
                  </button>
                )}
                {this.state.skillLoader && (
                  <div className="my-icon-button my-icon-button_small select-input__remove-value">
                    <CircularProgress size={24} style={{ bottom: '3px', right: '5px' }} />
                  </div>
                )}
              </div>
              {this.state.isExpertiseOpen && (
                <div className="lbv2__select-dropdown auto-complete-as-input">
                  <span className="auto-complete-as-input__icon">
                    <ActionSearch color="#6f708b" />
                  </span>
                  <AutoComplete
                    style={{ fontWeight: '600' }}
                    menuStyle={this.styles.autoCompleteMenu}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSource={this.state.skillSource
                      .filter(el => el && el.domain)
                      .map(item => {
                        return {
                          text: item.label,
                          domain: {
                            id: item.domain.id,
                            label: item.domain.label,
                            name: item.domain.name
                          },
                          id: item.id,
                          label: item.label,
                          name: item.name,
                          value: (
                            <MenuItem
                              primaryText={item.label}
                              value={item.name}
                              key={item.id}
                              style={this.styles.autoCompleteItem}
                            />
                          )
                        };
                      })}
                    fullWidth={true}
                    onNewRequest={this.selectSkillInput}
                    searchText={this.state.searchText}
                    maxSearchResults={20}
                    hintText={tr('Input skill name')}
                    openOnFocus={true}
                    listStyle={{ maxHeight: '5.75rem' }}
                  />
                </div>
              )}
            </div>
            <div className="lbv2__select-input-container lbv2__select-input_datarange">
              {this.state.isDataRangeOpen && (
                <div className="multiselect-overflow" onClick={this.toogleDataRange} />
              )}
              <div id="1" className="lbv2__select-input" onClick={this.toogleDataRange}>
                <span>
                  {tr('Date Range')}: {tr(this.state.dates)}
                </span>
              </div>
              {this.state.isDataRangeOpen && (
                <div className="lbv2__select-dropdown lbv2__select-dropdown_datarange">
                  <DateRange
                    onChange={this.handleChangeDateRange.bind(this, 'rangePicker')}
                    startDate={this.state.startDateRange}
                    endDate={this.state.endDateRange}
                  />
                  <div className="text-center">
                    <button
                      className="my-button"
                      style={{ borderColor: colors.followColor }}
                      onClick={this.clearDateRange}
                    >
                      {tr('Clear date range')}
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
        <div className="lbv2__table-header">
          <div className="lbv2__table-cell lbv2__table-cell_1" role="heading" aria-level="4">
            <span>{tr('User')}</span>
          </div>
          <div className="lbv2__table-cell lbv2__table-cell_2">
            <span>{tr('Score')}</span>
          </div>
          <div className="lbv2__table-cell lbv2__table-cell_3">
            <span>{this.state.genpactUI ? tr('Interests') : tr('Skills')}</span>
          </div>
        </div>
        <div className="lbv2__table-body">
          {_.orderBy(
            this.state.scores.filter(member => member && !!member.user),
            'smartbitesScore',
            'desc'
          ).map((member, index) => {
            if (member.user) {
              return (
                <LeaderboardListItemv2
                  key={index}
                  index={index}
                  user={member.user}
                  member={member}
                  scoresLength={this.state.scores.length}
                />
              );
            }
          })}
        </div>
        {!this.state.pending && this.state.noScores && (
          <div className="empty-message data-not-available-msg">
            {tr('There are no users matching your filter')}
          </div>
        )}
        {this.state.pending && (
          <div className="text-center pending-cards">
            <Spinner />
          </div>
        )}
      </div>
    );
  }
}

LeaderboardContainerv2.propTypes = {
  currentUser: PropTypes.object,
  users: PropTypes.object,
  groups: PropTypes.object,
  location: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  users: state.users.toJS(),
  groups: state.groups.toJS()
}))(LeaderboardContainerv2);
