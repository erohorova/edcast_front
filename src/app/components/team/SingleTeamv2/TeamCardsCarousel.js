import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Carousel from '../../common/Carousel';
import Card from '../../cards/Card';
import DiscoverCard from '../../common/Card';
import CardModal from '../../modals/CardModal';
import { openTeamCardsModal, confirmation } from '../../../actions/modalActions';
import IconButton from 'material-ui/IconButton/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTeamCards, updateCardsModal, deleteSharedCard } from '../../../actions/groupsActionsV2';
import EditIcon from 'material-ui/svg-icons/image/edit';
import { Permissions } from '../../../utils/checkPermissions';

class TeamCardsCarousel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      modalOpen: false
    };
  }

  getCardProps = data => {
    this.setState({ cardData: data, modalOpen: true });
  };

  voidCardProps = () => {
    this.setState({ modalOpen: false, cardData: null });
  };

  handleViewAllClick = (e, isViewAll = null) => {
    e.preventDefault();
    let groupID = this.props.group.id;
    let shared = this.props.deleteSharedCard;
    let cardModalDetails = {
      groupID,
      desc: this.props.description,
      count: this.props.count,
      type: this.fetchData().type,
      name: this.fetchData().name
    };
    this.props.dispatch(updateCardsModal(cardModalDetails));
    this.props.dispatch(
      openTeamCardsModal(shared, this.props.items, cardModalDetails, this.props.group, isViewAll)
    );
  };

  fetchData = () => {
    let titleKeysObj = {};
    switch (this.props.description) {
      case 'Shared':
        titleKeysObj.name = 'sharedCards';
        titleKeysObj.type = 'shared';
        titleKeysObj.offset = 'sharedCardsOffset';
        break;
      case 'Assigned':
        titleKeysObj.name = 'teamAssignments';
        titleKeysObj.type = 'assigned';
        titleKeysObj.offset = 'teamAssignmentsOffset';
        break;
      default:
        break;
    }
    return titleKeysObj;
  };

  deleteSharedCardHandler = payload => {
    let groupID = this.props.group.id;
    this.props.dispatch(
      confirmation(
        'Remove from Group',
        `Group members will no longer be able to see this card in the group.`,
        () => {
          this.props.dispatch(deleteSharedCard(groupID, payload));
        }
      )
    );
  };

  showViewAll = isShowAll => {
    this.setState({ isShowAll });
  };

  render() {
    let allowMarkFeatured =
      (this.props.group && this.props.group.isTeamAdmin) || Permissions.has('ADMIN_ONLY');
    return (
      <div className={this.props.isTeamFeed ? 'team-feed-content' : ''}>
        <div className="row channel-title">
          <div className="small-6 columns">
            <h5 style={{ display: 'inline-block', verticalAlign: 'middle', marginRight: '5px' }}>
              {this.props.description} ({this.props.count})
            </h5>
            {allowMarkFeatured && (
              <IconButton
                onTouchTap={e => this.handleViewAllClick(e)}
                style={{
                  height: '15px',
                  width: '15px',
                  padding: 0,
                  display: 'inline-block',
                  verticalAlign: 'middle',
                  marginRight: '5px'
                }}
                iconStyle={{ width: '20px', height: '20px' }}
              >
                <EditIcon color="#6f708b" />
              </IconButton>
            )}
          </div>
          {this.state.isShowAll && (
            <div className="small-6 columns text-right">
              <a
                href="#"
                onClick={e => this.handleViewAllClick(e, true)}
                style={{ textDecoration: 'underline' }}
              >
                {tr('View All')}
              </a>
            </div>
          )}
        </div>

        <div className="row">
          <div className="small-12 columns channel-card-wrapper">
            <div
              className="channel-card-wrapper-inner"
              style={{ 'max-height': '420px', overflow: 'hidden' }}
            >
              <Carousel
                isGroupCarousel={true}
                isTeamFeedOn={this.props.isTeamFeed}
                windowInnerWidth={this.props.windowInnerWidth}
                showViewAll={this.showViewAll}
              >
                {this.props.items.length > 0 &&
                  this.props.items.map(card => {
                    return (
                      <div key={card.id}>
                        <Card
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={this.fetchData().name !== 'teamAssignments'}
                          tooltipPosition="top-center"
                          moreCards={false}
                          withoutCardModal={true}
                          getCardProps={this.getCardProps}
                          voidCardProps={this.voidCardProps}
                          deleteSharedCard={
                            this.props.deleteSharedCard
                              ? this.deleteSharedCardHandler.bind(this, { card_id: card.id })
                              : false
                          }
                        />
                      </div>
                    );
                  })}
              </Carousel>
              {this.state.modalOpen && (
                <CardModal
                  logoObj={this.state.cardData.logoObj}
                  defaultImage={this.state.cardData.defaultImage}
                  card={this.state.cardData.card}
                  isUpvoted={this.state.cardData.isUpvoted}
                  updateCommentCount={this.state.cardData.updateCommentCount}
                  commentsCount={this.state.cardData.commentsCount}
                  votesCount={this.state.cardData.votesCount}
                  closeModal={this.state.cardData.closeModal}
                  likeCard={this.state.cardData.likeCard}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TeamCardsCarousel.propTypes = {
  items: PropTypes.any,
  currentUser: PropTypes.object,
  description: PropTypes.string,
  count: PropTypes.number,
  isTeamFeed: PropTypes.bool,
  deleteSharedCard: PropTypes.func,
  windowInnerWidth: PropTypes.any,
  group: PropTypes.shape({
    id: PropTypes.number,
    isTeamAdmin: PropTypes.bool
  })
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(TeamCardsCarousel);
