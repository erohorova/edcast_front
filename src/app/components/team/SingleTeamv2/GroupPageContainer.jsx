import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import GroupDetails from './GroupDetails';
import {
  getGroupDetail,
  getTeamCards,
  getTeamChannels,
  deleteCard,
  fetchPinnedCardsv2,
  updateGroupCarouselsOrder
} from '../../../actions/groupsActionsV2';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import { isInternetExplorer } from '../../../utils/isInternetExplorer';
import find from 'lodash/find';
import each from 'lodash/each';
import throttle from 'lodash/throttle';
import GroupCarouselContainer from './GroupCarouselContainer';
import RightRail from '../../home/RightRail';
import IconButton from 'material-ui/IconButton/IconButton';
import ChatIcon from 'edc-web-sdk/components/icons/ChatIcon';
import { snackBarOpenClose } from '../../../actions/channelsActionsV2';
import * as actionTypes from '../../../constants/actionTypes';

class GroupPageContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0 51px'
      },
      withTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 340px)'
      },
      withNewTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 360px)'
      },
      rowTeemFeed: {
        padding: 0,
        margin: 0
      },
      padTeemFeed: {
        padding: 0
      },
      row: {
        maxWidth: '100%'
      },
      homeUI: {
        padding: 0
      },
      addLink: {
        textDecoration: 'underline',
        color: '#4990e2',
        marginRight: '0.5rem'
      },
      editIcon: {
        width: '21px',
        height: '21px',
        verticalAlign: 'bottom'
      },
      editChannelsIcon: {
        padding: '0',
        marginLeft: '0.3125rem',
        width: '1.3125rem',
        height: '1.3125rem'
      }
    };
    this.state = {
      hideTeamFeed: localStorage.getItem('hideTeamFeed') === 'true',
      isMenuOpen: false,
      groupID: null,
      groupDetail: null,
      offsetFeed: 0,
      footerOffset: 0,
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      topNavVersion: window.ldclient.variation('topnav-version', 'V2'),
      newFeedStyle: window.ldclient.variation('feed-style-v2', false),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      openAddChannelModal: false,
      windowWidth: 1200,
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar')
    };
    this.collapseWidth = 1100;
  }

  toggleBlock = () => {
    if (this.state.windowWidth <= this.collapseWidth || !this.state.newFeedStyle) {
      this.setState(prevState => ({
        isMenuOpen: !prevState.isMenuOpen
      }));
    }
    this.toggleTeamFeed();
  };

  toggleTeamFeed = () => {
    if (this.state.windowWidth <= this.collapseWidth) {
      this.toggleBlock();
    } else {
      localStorage.setItem('hideTeamFeed', `${!this.state.hideTeamFeed}`);
      this.setState(prevState => {
        return {
          hideTeamFeed: !prevState.hideTeamFeed
        };
      });
    }
  };

  updatePosition = () => {
    let windowInnerWidth = window.innerWidth;
    let windowWidth =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let position = calculateTeemFeedPosition();
    let fixedHeader = document.querySelector('#secondary-nav.fixed');
    let doc = document.documentElement || document.body;
    let navClass;
    let defaultSize;
    let isMenuOpen;
    if (this.state.topNavVersion === 'V2') {
      navClass = 'topnav-main-container-v2';
      defaultSize = 170;
    } else {
      navClass = 'topnav-main-container-v3';
      defaultSize = 124;
    }
    let offsetFeed = fixedHeader
      ? fixedHeader.offsetHeight
      : document.getElementsByClassName(navClass)[0]
      ? document.getElementsByClassName(navClass)[0].offsetHeight - doc.scrollTop
      : defaultSize - doc.scrollTop;
    offsetFeed = `${offsetFeed + 23}px`;
    if (windowWidth > this.collapseWidth) {
      isMenuOpen = false;
    }
    let footerOffset = position.footerOffset;

    if (
      offsetFeed !== this.state.offsetFeed ||
      windowInnerWidth !== this.state.windowInnerWidth ||
      windowWidth !== this.state.windowWidth ||
      isMenuOpen !== this.state.isMenuOpen ||
      footerOffset !== this.state.footerOffset
    ) {
      this.setState({
        offsetFeed,
        windowInnerWidth,
        windowWidth,
        isMenuOpen,
        footerOffset
      });
    }
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePosition);
    window.removeEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let groupDetails =
      this.state.groupID &&
      nextProps.groupsV2 &&
      nextProps.groupsV2[this.state.groupID] &&
      nextProps.groupsV2[this.state.groupID];
    if (groupDetails && groupDetails.carousels_order_change) {
      this.fetchGroupDetails();
      this.props.dispatch(updateGroupCarouselsOrder(this.state.groupID, false));
    }
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    window.addEventListener('resize', this.updatePosition);
    window.addEventListener('scroll', this.handleScroll);
    setTimeout(() => {
      this.handleScroll();
    }, 10);
    this.fetchGroupDetails();
  }

  fetchGroupDetails = () => {
    let slug = this.props.params && this.props.params.slug ? this.props.params.slug : '';
    this.props
      .dispatch(getGroupDetail(slug))
      .then(groupDetail => {
        this.updatePosition();
        let groupDetailData = groupDetail;
        let groupID = groupDetail.id;
        this.setState({
          groupID: groupID,
          groupDetail: groupDetailData
        });
        this._fetchPinnedCards(groupID);
      })
      .catch(err => {
        this.props.dispatch(
          snackBarOpenClose('You are not authorized to access this group.', 5000)
        );
        this.props.dispatch(push(`/org-groups`));
        console.error(`Error in GroupPageContainer.getGroupDetail.func : ${err}`);
      });
  };

  _fetchPinnedCards = teamId => {
    this.props
      .dispatch(
        fetchPinnedCardsv2({ pin: { pinnable_id: teamId, pinnable_type: 'Team' } }, 'featured')
      )
      .then(() => {})
      .catch(err => {
        this.props.dispatch({
          type: actionTypes.RECEIVE_PINNED_CARDS_GROUP,
          teamId,
          itemType: 'Team',
          cards: []
        });
        console.error(
          `Error in GroupPageContainer.componentDidMount.fetchPinnedCardsv2.func: ${err}`
        );
      });
  };

  componentWillUpdate(nextProps, nextState) {
    const propsValid = nextProps.cards && nextProps.groupsV2;
    const propsChanged =
      propsValid &&
      nextProps.cards !== this.props.cards &&
      nextProps.groupsV2 !== this.props.groupsV2;
    const stateValid = nextState.groupDetail && nextState.groupDetail.id;
    const stateChanged =
      stateValid &&
      (!this.state.groupDetail || // now state is valid and it wasn't valid before
        (nextState.groupDetail && nextState.groupDetail.id) !==
          (this.state.groupDetail && this.state.groupDetail.id));

    if (propsValid && stateValid && (propsChanged || stateChanged)) {
      let group = nextProps.groupsV2[nextState.groupDetail && nextState.groupDetail.id];
      each(nextProps.cards, (card, cardID) => {
        if (group && card.dismissed) {
          let assignmentCard = find(group.teamAssignments, { id: cardID });

          let sharedCard = find(group.sharedCards, { id: cardID });
          if (assignmentCard) {
            this.props.dispatch(deleteCard(cardID, 'teamAssignments'));
          }

          if (sharedCard) {
            this.props.dispatch(deleteCard(cardID, 'sharedCards'));
          }
        }
      });
    }
  }

  teamFeedTabEnable = obj => {
    if (obj) {
      let teamFeed = obj['web/leftRail/teamActivity'];
      return teamFeed && teamFeed.visible !== false;
    } else {
      return false;
    }
  };

  handleScroll = throttle(
    () => {
      setTimeout(() => {
        this.updatePosition();
      }, 1);
    },
    150,
    { leading: false }
  );

  render() {
    let groupID = this.state.groupID;
    let groupDetail = this.state.groupDetail;
    let group = groupID && this.state.groupDetail;
    let isRail =
      this.teamFeedTabEnable(this.props.team.OrgConfig.leftRail) && this.state.newTeamActivity;
    let isNewRail =
      this.teamFeedTabEnable(this.props.team.OrgConfig.leftRail) &&
      this.state.newTeamActivity &&
      this.state.newFeedStyle;

    let isIE = isInternetExplorer();

    return (
      <div className="channel-base">
        <div className="row" style={this.styles.row}>
          <div
            className={`medium-12 team-detail-page${groupID && isRail ? '_with-teem-feed' : ''} ${
              this.state.newFeedStyle ? 'team-detail-page_with-big-team-feed' : ''
            } ${
              this.state.isMenuOpen &&
              this.state.newFeedStyle &&
              this.state.windowWidth < this.collapseWidth
                ? 'team-detail-page_active-team-feed'
                : ''
            }
          ${
            this.state.hideTeamFeed &&
            this.state.newFeedStyle &&
            this.state.windowWidth >= this.collapseWidth
              ? 'team-detail-page_hide-team-feed'
              : ''
          }`}
            style={
              groupID && isNewRail
                ? this.styles.withNewTeemFeed
                : isRail
                ? this.styles.withTeemFeed
                : this.styles.rowPadding
            }
          >
            {groupID && <GroupDetails isTeamFeed={isRail} groupInfo={group} />}

            {groupDetail && (
              <GroupCarouselContainer
                groupDetail={groupDetail}
                isTeamFeed={isRail}
                windowInnerWidth={this.state.windowInnerWidth}
                filterByLanguage={this.state.multilingualFiltering}
              />
            )}
          </div>
          {groupID && isRail && (
            <div
              id="right-rail-container"
              className={`home-new-ui ${
                this.state.newFeedStyle ? 'feed-page_new-style' : 'feed-page_old-style'
              } ${
                this.state.hideTeamFeed &&
                this.state.newFeedStyle &&
                this.state.windowWidth >= this.collapseWidth
                  ? 'hide-team-feed'
                  : ''
              }`}
              style={this.styles.homeUI}
            >
              <div
                className={`group-page left-rail right-rail ${
                  this.state.isMenuOpen && this.state.windowWidth < this.collapseWidth
                    ? 'active-menu-container'
                    : ''
                }`}
                style={{
                  top: this.state.offsetFeed,
                  height: `calc(100% - ${this.state.offsetFeed} - ${this.state.footerOffset})`
                }}
              >
                {this.state.newFeedStyle &&
                  this.state.hideTeamFeed &&
                  this.state.homepageVersion === 'left-sidebar' && (
                    <div className={`right-rail__mobile-btn rail__mobile-btn`}>
                      <IconButton
                        onClick={this.toggleBlock}
                        className={`right-rail__mobile-btn rail__mobile-btn`}
                      >
                        <ChatIcon />
                      </IconButton>
                    </div>
                  )}
                {this.state.hideTeamFeed && this.state.homepageVersion === 'right-sidebar' && (
                  <button
                    onClick={this.toggleBlock}
                    className={`chat-button-group ${
                      isIE
                        ? 'chat-button-group-ie'
                        : this.state.newFeedStyle
                        ? 'chat-btn-group-other'
                        : ''
                    }`}
                  >
                    <img src="/i/images/chat-icon.svg" alt="Chat" />
                  </button>
                )}
                <RightRail
                  hideTeamFeed={this.state.hideTeamFeed}
                  newFeedStyle={this.state.newFeedStyle}
                  justTeamFeed={true}
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                  newTeamActivity={true}
                  currentUser={this.props.currentUser}
                  currentGroup={group}
                  toggleTeamFeed={this.toggleTeamFeed}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

GroupPageContainer.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  cards: PropTypes.object,
  groupsV2: PropTypes.object,
  params: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS(),
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupPageContainer);
