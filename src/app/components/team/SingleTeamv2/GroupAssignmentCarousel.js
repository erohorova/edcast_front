import React from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';
import CardLoader from '../../common/CardLoader';
import { getTeamCards } from '../../../actions/groupsActionsV2';
import TeamCardsCarousel from './TeamCardsCarousel';
import EmptyBlock from '../../discovery/EmptyBlock';

class GroupAssignmentCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0px 12px'
      },
      rowTeamFeed: {
        padding: 0,
        margin: 0
      }
    };
  }

  componentDidMount() {
    let groupDetail = this.props.groupDetail;
    let groupID = groupDetail.id;
    this.props.dispatch(
      getTeamCards(
        groupID,
        'teamAssignments',
        { limit: groupDetail.cardsLimit, offset: 0, type: 'assigned' },
        this.props.filterByLanguage
      )
    );
  }

  render() {
    let groupID = this.props.groupDetail.id;
    let group = groupID && this.props.groupsV2[groupID + ''];
    let teamAssignments = groupID && group.teamAssignments;
    let teamAssignmentsCount = groupID && group.teamAssignmentsCount;
    let teamAssignmentsLoaded = groupID && group.teamAssignmentsLoaded;

    return (
      <div>
        {!teamAssignmentsLoaded ? (
          <div className="small-12">
            <div className="small-12 group-title">
              <div>{tr('Assigned')}</div>
            </div>
            <CardLoader loadingCards={4} />
          </div>
        ) : teamAssignmentsCount ? (
          <TeamCardsCarousel
            group={group}
            isTeamFeed={this.props.isTeamFeed}
            description={'Assigned'}
            items={teamAssignments}
            count={teamAssignmentsCount}
            editable={false}
            windowInnerWidth={this.props.windowInnerWidth}
          />
        ) : (
          <div style={{ marginTop: '15px' }}>
            <div className="small-12 group-title">
              <div>{tr('Assigned')}</div>
            </div>
            <EmptyBlock
              className="content"
              title={tr('Cards assigned to the group')}
              style={{ width: '100%' }}
            />
          </div>
        )}
      </div>
    );
  }
}

GroupAssignmentCarousel.propTypes = {
  groupsV2: PropTypes.object,
  windowInnerWidth: PropTypes.any,
  groupDetail: PropTypes.object,
  isTeamFeed: PropTypes.any,
  filterByLanguage: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupAssignmentCarousel);
