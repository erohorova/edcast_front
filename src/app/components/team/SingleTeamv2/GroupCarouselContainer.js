import React from 'react';
import PropTypes from 'prop-types';
import GroupAssignmentCarousel from './GroupAssignmentCarousel';
import GroupSharedCarousel from './GroupSharedCarousel';
import GroupChannelCarousel from './GroupChannelCarousel';
import GroupFeaturedCarousel from './GroupFeaturedCarousel';
class GroupCarouselContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0px 12px'
      },
      rowTeamFeed: {
        padding: 0,
        margin: 0
      }
    };
  }

  addComponent = () => {
    this.props.groupDetail.carousels.forEach(item => {
      switch (item.default_label) {
        case 'Featured':
          item.component = GroupFeaturedCarousel;
          break;
        case 'Assigned':
          item.component = GroupAssignmentCarousel;
          break;
        case 'Shared':
          item.component = GroupSharedCarousel;
          break;
        case 'Channels':
          item.component = GroupChannelCarousel;
          break;
        default:
          break;
      }
    });
  };

  render() {
    if (this.props.groupDetail && this.props.groupDetail.carousels) {
      this.addComponent();
    }
    return (
      <div style={this.props.isTeamFeed ? this.styles.rowTeamFeed : this.styles.rowPadding}>
        {this.props.groupDetail &&
          this.props.groupDetail.carousels &&
          this.props.groupDetail.carousels
            .sort((a, b) => a.index - b.index)
            .map(item => {
              if (item.visible) {
                const Component = item.component;
                return <Component {...this.props} key={item.index} />;
              }
            })}
      </div>
    );
  }
}

GroupCarouselContainer.propTypes = {
  windowInnerWidth: PropTypes.any,
  groupDetail: PropTypes.object,
  isTeamFeed: PropTypes.any,
  filterByLanguage: PropTypes.bool
};

export default GroupCarouselContainer;
