import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { usersv2, search } from 'edc-web-sdk/requests/index';
import { Menu, MenuItem, AsyncTypeahead } from 'react-bootstrap-typeahead';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { addTeamUsers, getGroupDetail, deleteTeamUsers } from '../../../actions/groupsActionsV2';
import { tr } from 'edc-web-sdk/helpers/translations';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import find from 'lodash/find';
import without from 'lodash/without';

class AddUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      selectedUsers: props.userType === 'group-admin' ? props.subAdmins : props.leaders,
      selectedUserIds: [],
      newUsers: [],
      deleteUsers: [],
      saveBtn: 'Save'
    };

    this.styles = {
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chipLabel: {
        fontSize: '12px',
        color: '#9276b5'
      },
      customIcon: {
        position: 'absolute',
        top: '1px',
        maxHeight: '18px',
        right: '7px',
        padding: 0,
        maxWidth: '18px'
      },
      chip: {
        border: '1px solid #9a9a9a !important'
      }
    };
  }

  componentDidMount() {
    let users = this.state.newUsers.concat(this.state.selectedUsers);
    let selectedUserIds = users.map(userItem => {
      return userItem.id;
    });
    this.setState({ selectedUserIds });
  }

  handleRequestDelete = userId => {
    let { selectedUsers, newUsers, selectedUserIds, deleteUsers } = this.state;
    if (this.props.userType === 'group-leader' && selectedUsers.length === 1) {
      let message = tr(
        'Error. You can’t remove last Group Leader. Please assign a new Group Leader before removing yourself'
      );
      this.props.dispatch(openSnackBar(message, true));
      return;
    }
    let alreadyAdded = find(newUsers, user => {
      return user.id == userId;
    });
    selectedUserIds = without(selectedUserIds, userId);
    if (alreadyAdded) {
      newUsers = without(newUsers, alreadyAdded);
      this.setState({ newUsers, selectedUserIds });
    } else {
      let delUser = find(selectedUsers, user => {
        return user.id == userId;
      });
      selectedUsers = without(selectedUsers, delUser);
      deleteUsers.push(delUser);
      this.setState({ deleteUsers, selectedUsers, selectedUserIds });
    }
  };

  _handleSearch = query => {
    let that = this;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

    if (!query) {
      return;
    }
    usersv2
      .searchUsers({ q: query })
      .then(data => {
        let users = data.users.filter(user => {
          if (user.handle) {
            return {
              id: user.id,
              name: user.name,
              slug: user.handle,
              avatar: user.avatarimages.tiny || defaultUserImage,
              type: 'User',
              email: user.email,
              isFollowing: user.isFollowing,
              role: 'admin'
            };
          }
        });
        that.setState({ dataSource: users });
      })
      .catch(err => {
        console.error(`Error in AddUsers.searchUsers.func : ${err}`);
      });
  };

  handleUserClick = user => {
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let isGroupMember;
    usersv2
      .searchUsers({ q: user.handle, slug: groupID, team_ids: groupID })
      .then(data => {
        isGroupMember = data.total > 0;
        let isOwners = find(group.owners, owner => {
          return user.id == owner.id;
        });
        if (isGroupMember || (this.props.userType === 'group-admin' && isOwners)) {
          this.updateChipSet(user);
        } else {
          let message =
            this.props.userType === 'group-admin'
              ? tr('The user is not a member of the group. You can’t add it as Group Admin')
              : tr('The user is not a member of the group. You can’t add it as Group Leader');
          this.props.dispatch(openSnackBar(message, true));
          this.refs.userSearchTypeahead.getInstance().blur();
        }
      })
      .catch(err => {
        console.error(`Error in AddUsers.handleUserClick.searchUsers.func : ${err}`);
      });
  };

  updateChipSet = user => {
    if (!this.state.selectedUserIds.includes(user.id)) {
      let { deleteUsers, newUsers, selectedUsers } = this.state;
      let alreadyAdded = find(deleteUsers, userItem => {
        return userItem.id == user.id;
      });
      if (alreadyAdded) {
        deleteUsers = without(deleteUsers, alreadyAdded);
        selectedUsers.push(alreadyAdded);
      } else {
        newUsers.push(user);
      }
      let allUsers = newUsers.concat(selectedUsers);
      let selectedUserIds = allUsers.map(userItem => {
        return userItem.id;
      });
      this.setState({ newUsers, selectedUserIds, deleteUsers });
      this.refs.userSearchTypeahead.getInstance().blur();
    }
  };

  saveUsers = async () => {
    this.setState({ saveBtn: 'Saving...' });
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let groupSlug = group.slug;
    let { deleteUsers, newUsers } = this.state;
    let role = this.props.userType === 'group-admin' ? 'sub_admin' : 'admin';
    if (deleteUsers) {
      let emails = deleteUsers.map(a => a.email);
      let ids = deleteUsers.map(a => a.id);
      let payload = { ids, groupID, role, deleteUsers };
      if (emails.length > 0) {
        await this.props.dispatch(deleteTeamUsers(payload));
      }
    }
    if (newUsers) {
      let emails = newUsers.map(a => a.email);
      let ids = newUsers.map(a => a.id);
      let payload = { ids, groupID, role, newUsers };
      if (emails.length > 0) {
        await this.props.dispatch(addTeamUsers(payload));
      }
    }
    this.props.closeModal();
  };

  _renderMenu = (results, menuProps) => {
    const users = results.map(user => {
      return [
        <li key={user.id}>
          <a onClick={this.handleUserClick.bind(this, user)}>
            <img src={user.avatar} height="20" className="user-search-image" />
            <div className="user-search-name">{user.name}</div>
          </a>
        </li>
      ];
    });

    return <Menu {...menuProps}>{users}</Menu>;
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    let users = this.state.newUsers.concat(this.state.selectedUsers);
    return (
      <div className="channel-curators-wrapper">
        <div className="list-users">
          <div style={this.styles.wrapper}>
            {users &&
              users.map(user => {
                let isCurrentUser = this.props.currentUser.id + '' === user.id + '';
                return (
                  <Chip
                    key={user.id}
                    backgroundColor={'#fff'}
                    labelColor={'#9276b5'}
                    labelStyle={this.styles.chipLabel}
                    className={'user-chip'}
                    style={this.styles.chip}
                  >
                    <small style={this.styles.chipLabel}>{user.name}</small>
                    <IconButton
                      className={'cancel-icon delete'}
                      ref={'cancelIcon'}
                      tooltip={tr('Remove Selection')}
                      disableTouchRipple
                      tooltipPosition="top-center"
                      style={this.styles.customIcon}
                      onTouchTap={this.handleRequestDelete.bind(this, user.id)}
                      disabled={isCurrentUser}
                      aria-label="remove"
                    >
                      <Close color={isCurrentUser ? '#9a9a9a' : '#9276b5'} />
                    </IconButton>
                  </Chip>
                );
              })}
          </div>
        </div>

        <div className="channel-user-search-input user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={this.state.dataSource}
            placeholder={
              this.props.userType === 'group-admin' ? tr('Add Admin...') : tr('Add Leader...')
            }
            onSearch={this._handleSearch}
            submitFormOnEnter={false}
            ref="userSearchTypeahead"
          />

          <SecondaryButton
            disabled={this.state.saveBtn === 'Saving...'}
            label={tr(this.state.saveBtn)}
            className="save-curators"
            onTouchTap={this.saveUsers}
          />
        </div>
      </div>
    );
  }
}

AddUsers.propTypes = {
  groupsV2: PropTypes.object,
  currentUser: PropTypes.object,
  leaders: PropTypes.any,
  closeModal: PropTypes.func,
  userType: PropTypes.string,
  subAdmins: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(AddUsers);
