import React from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';
import CardLoader from '../../common/CardLoader';
import { getFeaturedCards } from '../../../actions/groupsActionsV2';
import TeamCardsCarousel from './TeamCardsCarousel';
import EmptyBlock from '../../discovery/EmptyBlock';

class GroupFeaturedCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0px 12px'
      },
      rowTeamFeed: {
        padding: 0,
        margin: 0
      }
    };
  }

  componentDidMount() {
    let groupDetail = this.props.groupDetail;
    let groupID = groupDetail.id;
    this.props.dispatch(
      getFeaturedCards({ pin: { pinnable_id: groupID, pinnable_type: 'Team' } }, 'featured')
    );
  }

  render() {
    let pinnedCards = this.props.teamProps.get('pinnedCards');
    let groupID = this.props.groupDetail.id;
    let group = groupID && this.props.groupsV2[groupID + ''];
    let featured = groupID && group.featured;
    let featuredCount = groupID && group.featuredCount;
    let pinnedCardsLength = pinnedCards && (pinnedCards.length || 0);

    return (
      <div>
        {pinnedCardsLength > 0 && (
          <TeamCardsCarousel
            group={group}
            isTeamFeed={this.props.isTeamFeed}
            description={'Featured'}
            items={pinnedCards}
            count={pinnedCardsLength || 0}
            editable={false}
            windowInnerWidth={this.props.windowInnerWidth}
          />
        )}
      </div>
    );
  }
}

GroupFeaturedCarousel.propTypes = {
  groupsV2: PropTypes.object,
  windowInnerWidth: PropTypes.any,
  groupDetail: PropTypes.object,
  isTeamFeed: PropTypes.any,
  filterByLanguage: PropTypes.bool,
  teamProps: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    teamProps: state.teamNewReducer
  };
}

export default connect(mapStoreStateToProps)(GroupFeaturedCarousel);
