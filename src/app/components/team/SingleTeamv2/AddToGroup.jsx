import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import { Menu, AsyncTypeahead } from 'react-bootstrap-typeahead';
import { tr } from 'edc-web-sdk/helpers/translations';
import Checkbox from 'material-ui/Checkbox';
import uniqBy from 'lodash/uniqBy';
import findIndex from 'lodash/findIndex';
import throttle from 'lodash/throttle';

class AddToGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orgUsers: [],
      dataSource: [],
      selectedUsers: this.props.curators,
      limit: 19,
      isLastPage: false,
      pending: false,
      selectedOrgUsers: [],
      currentHandle: null,
      setDefaultSearch: false,
      showAlert: false
    };
    this.currentHandle = null;
    this.styles = {
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      customIcon: {
        position: 'absolute',
        top: '1px',
        maxHeight: '18px',
        right: '7px',
        padding: 0,
        maxWidth: '18px'
      }
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreOrgUsers = this.showMoreOrgUsers.bind(this);
  }

  showMoreOrgUsers = () => {
    let payload = {
      limit: this.state.limit,
      offset: this.state.orgUsers.length
    };
    this.setState({ pending: true });
    this.usersUpdate(payload);
  };

  usersUpdate = payload => {
    usersSDK
      .getItems(payload)
      .then(users => {
        let orgUsers = users.items;
        let isLastPage = orgUsers.length < this.state.limit;
        this.setState({
          orgUsers: this.state.setDefaultSearch
            ? orgUsers
            : uniqBy(this.state.orgUsers.concat(orgUsers), 'id'),
          pending: false,
          isLastPage
        });
      })
      .catch(err => {
        console.error(`Error in AddToGroup.getUsers.func : ${err}`);
      });
  };

  _handleSearch = query => {
    let that = this;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    if (!query) {
      return;
    }
    usersSDK
      .getUsers({ q: query })
      .then(data => {
        let users = data.items.map(user => {
          return {
            id: user.id,
            name: user.name,
            slug: user.handle,
            avatar: user.avatarimages.tiny || defaultUserImage,
            type: 'User'
          };
        });
        let existingMembers = this.props.groupsV2[
          this.props.groupsV2.currentGroupID
        ].members.concat(this.props.groupsV2[this.props.groupsV2.currentGroupID].pending);
        existingMembers.map(existingMember => {
          let index = findIndex(users, user => {
            return user.id == existingMember.id;
          });
          if (index >= 0) {
            users.splice(index, 1);
          }
        });
        that.setState({ dataSource: users });
      })
      .catch(err => {
        console.error(`Error in AddToGroup.searchUsers.func : ${err}`);
      });
  };

  checkifAlreadyInvited = user => {
    let existingMembers = this.props.groupsV2[this.props.groupsV2.currentGroupID].members.concat(
      this.props.groupsV2[this.props.groupsV2.currentGroupID].pending
    );
    let index = findIndex(existingMembers, existingMember => {
      return (
        parseInt(existingMember.userId ? existingMember.userId : existingMember.id) ==
        parseInt(user.id)
      );
    });
    return index;
  };

  handleUserClick = user => {
    let handle = user.slug;
    let payload = {
      limit: this.state.limit,
      q: handle ? handle : ''
    };
    this.setState({ currentHandle: handle, setDefaultSearch: true });
    this.usersUpdate(payload);
    this.refs.userSearchTypeahead.getInstance().blur();
  };

  componentWillMount = () => {
    this.setState({ selectedOrgUsers: this.props.selectedOrgUsers });
  };

  componentDidMount = () => {
    let payload = {
      limit: this.state.limit,
      offset: this.state.orgUsers.length
    };
    this.setState({ pending: true });
    this.usersUpdate(payload);
    this._orgUsersContainer.addEventListener('scroll', this.handleScroll);
    window.addEventListener('keydown', this.closeModalOnESC);
  };
  closeModalOnESC = e => {
    if (e.keyCode == 27) {
      this.props.closeModal();
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ selectedOrgUsers: nextProps.selectedOrgUsers });
  }

  componentWillUnmount = () => {
    this._orgUsersContainer.removeEventListener('scroll', this.handleScroll);
  };

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      let container = this._orgUsersContainer;
      if (container.scrollTop === container.scrollHeight - container.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreOrgUsers();
        }
      }
    },
    150,
    { leading: false }
  );

  _renderMenu = (results, menuProps) => {
    if (menuProps.text === '' && this.state.setDefaultSearch) {
      this.setState({ setDefaultSearch: false, pending: true });
      let payload = {
        limit: this.state.limit
      };
      this.usersUpdate(payload);
    }
    const users = results.map(user => {
      return [
        <li key={user.id}>
          <a onClick={this.handleUserClick.bind(this, user)}>
            <div className="user-search-name" style={{ fontSize: '12px' }}>
              {user.name}
            </div>
            <span style={{ fontSize: '11px' }}> ({user.slug && user.slug})</span>
          </a>
        </li>
      ];
    });

    return <Menu {...menuProps}>{users}</Menu>;
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    return (
      <div className="group-user-wrapper">
        <div className="group-user-search-input user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={this.state.dataSource}
            placeholder={tr('Add Members...')}
            filterBy={(userData, property) => {
              return true;
            }}
            onSearch={this._handleSearch}
            submitFormOnEnter={false}
            ref="userSearchTypeahead"
            value={this.state.currentHandle}
          />
          <div>
            <div
              id="orgUsersContainer"
              style={{ height: '360px', overflow: 'scroll', marginTop: '10px' }}
              ref={node => {
                this._orgUsersContainer = node;
              }}
            >
              {this.state.orgUsers &&
                this.state.orgUsers.map((user, index) => {
                  let userAvatar =
                    (user.avatarimages && user.avatarimages.tiny) || defaultUserImage;
                  let alreadyInvited = this.checkifAlreadyInvited.bind(this, user)();
                  return (
                    <div key={user.id}>
                      <Checkbox
                        label={
                          <a
                            style={{ color: '#454560' }}
                            onClick={() => {
                              this.props.updateCheck(user);
                            }}
                          >
                            <img
                              src={userAvatar}
                              height="25"
                              className="user-search-image"
                              style={{ width: '25px', height: '25px' }}
                            />
                            <div
                              className="user-search-name"
                              style={{ fontSize: '13px', fontWeight: 500 }}
                            >
                              {user.name}
                            </div>
                            <span style={{ fontSize: '11px', verticalAlign: 'middle' }}>
                              {' '}
                              {user.handle && '(' + user.handle + ')'}
                            </span>
                          </a>
                        }
                        checked={!!~findIndex(this.state.selectedOrgUsers, { id: user.id })}
                        disabled={alreadyInvited >= 0}
                        onCheck={() => {
                          this.props.updateCheck(user);
                        }}
                        labelStyle={{ margin: '5px 0 5px 0', fontSize: '13px', fontWeight: 300 }}
                        iconStyle={{
                          width: '16px',
                          height: '16px',
                          marginRight: '10px',
                          marginTop: '6px'
                        }}
                      />
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AddToGroup.propTypes = {
  groupsV2: PropTypes.object,
  currentUser: PropTypes.object,
  selectedOrgUsers: PropTypes.array,
  curators: PropTypes.any,
  updateCheck: PropTypes.func,
  closeModal: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(AddToGroup);
