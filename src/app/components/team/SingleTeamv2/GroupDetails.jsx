import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';

import CloseIcon from 'material-ui/svg-icons/content/clear';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import EditIcon from 'material-ui/svg-icons/image/edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton/IconButton';

import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import CameraIcon from 'edc-web-sdk/components/icons/CameraIcon';

import {
  getGroupUsers,
  updateCardsModal,
  updateGroup,
  initializedGroupUser,
  leaveFromGroup,
  joinToGroup,
  declineInviteToGroup,
  acceptInviteToGroup
} from '../../../actions/groupsActionsV2';
import {
  openGroupCreationModal,
  openGroupMemberModal,
  openInviteV2UserModal,
  openShowChannelsModal,
  openTeamCardsModal,
  openGroupInviteModal,
  openUpdateGroupDescriptionModal,
  openMultiactionsModal,
  confirmationPrivateCardModal
} from '../../../actions/modalActions';
import { createDynamicSelectionGroup, deleteGroup } from 'edc-web-sdk/requests/groups.v2';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import { fileStackSources } from '../../../constants/fileStackSource';
import { Permissions } from '../../../utils/checkPermissions';
import { filestackClient } from '../../../utils/filestack';
import Dialog from 'material-ui/Dialog';
import ConfirmationModal from '../../modals/ConfirmationModal';
import { uploadPolicyAndSignature } from 'edc-web-sdk/requests/filestack';

class GroupDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowMargin: {
        margin: '0px'
      },
      lrPadding15: {
        padding: '0px 15px'
      },
      lPadding15: {
        padding: '0px 0px 0px 15px'
      },
      groupTitle: {
        fontSize: '22px',
        color: '#454560',
        wordWrap: 'break-word'
      },
      rowPadding: {
        padding: '0px 12px'
      },
      rowTeamFeed: {
        padding: 0,
        margin: 0
      },
      countLabel: {
        textDecoration: 'underline',
        color: '#4990e2'
      },
      deleteIcon: {
        width: '30px',
        height: '30px',
        verticalAlign: 'bottom'
      },
      confirmationModal: {
        zIndex: 1502
      },
      confirmationContent: {
        transform: 'none'
      },
      editIcon: {
        width: '21px',
        height: '21px',
        verticalAlign: 'bottom'
      },
      leaderEdit: {
        verticalAlign: 'middle',
        padding: '0px',
        marginRight: '5px',
        width: '21px',
        height: '21px',
        top: '-2px'
      },
      groupLeader: {
        color: '#6f708b'
      },
      cameraBg: {
        height: '57px',
        width: '58px',
        backgroundColor: '#000000',
        opacity: '0.6',
        borderRadius: '50%',
        margin: 'auto',
        position: 'absolute',
        top: '0px',
        bottom: '0px',
        left: '0px',
        right: '0px'
      },
      groupLeaderFont: {
        color: '#6f708b',
        fontSize: '12px'
      },
      groupLeaderTitle: {
        fontSize: '14px',
        fontWeight: '300',
        color: '#454560'
      },
      viewMore: {
        fontSize: '12px',
        textDecoration: 'underline',
        color: '#4990e2'
      },
      closeIcon: {
        position: 'absolute',
        top: '-2px',
        right: '0'
      },
      dropDownGroupLeader: {
        display: 'block',
        margin: '5px'
      },
      popoverTitle: {
        fontSize: '14px',
        color: '#454560',
        marginBottom: '5px',
        display: 'inline-block'
      },
      cameraIcon: {
        cursor: 'pointer',
        margin: 'auto',
        position: 'absolute',
        top: '0px',
        left: '0px',
        bottom: '0px',
        right: '0px',
        height: '22px',
        width: '25px'
      },
      popoverList: {
        fontSize: '14px',
        color: '#454560',
        lineHeight: '1.3',
        fontWeight: '100',
        overflow: 'scroll',
        maxHeight: '150px'
      },
      editTitle: {
        width: '21px',
        height: '31px',
        verticalAlign: 'bottom',
        padding: '0',
        paddingLeft: '5px'
      },
      deleteTitle: {
        width: '30px',
        height: '30px',
        verticalAlign: 'bottom',
        float: 'right',
        padding: '0'
      },
      fullDescription: {
        fontSize: '14px',
        color: '#454560',
        lineHeight: '1.3',
        fontWeight: '100',
        wordWrap: 'break-word'
      }
    };
    this.state = {
      openPopOverOwners: false,
      openPopOverSubAdmins: false,
      openDescriptionPopOver: false,
      showEditImage: false,
      teamAnalyticsEnabled: window.ldclient.variation('team-analytics', false),
      isMemberInitilized: false,
      isPendingMemberInitilized: false,
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      groupModalV2: window.ldclient.variation('group-create-modal-v2', false),
      groupPageV2: window.ldclient.variation('group-page-v2', false),
      dynamicSelection: window.ldclient.variation('dynamic-selections', false)
    };

    this.triggerUpdate = this.triggerUpdate.bind(this);
  }

  openDescriptionPopOver = event => {
    event.preventDefault();
    this.setState({
      openDescriptionPopOver: true,
      anchorDescEl: event.currentTarget
    });
  };

  openPopOver = (userType, event) => {
    event.preventDefault();
    this.setState({
      [`openPopOver${userType}`]: true,
      anchorEl: event.currentTarget
    });
  };

  closePopOver = userType => {
    this.setState({
      [`openPopOver${userType}`]: false
    });
    if (this.state.openDescriptionPopOver) {
      this.setState({ openDescriptionPopOver: false });
    }
  };

  triggerUpdate = () => {
    if (this.state.groupModalV2) {
      this.props.dispatch(openGroupCreationModal(null, this.props.groupsV2.currentGroupID));
    } else {
      this.props.dispatch(openUpdateGroupDescriptionModal());
    }
  };

  openTeamMemberModal = (e, userType) => {
    e.preventDefault();
    let payloadObject = { user_type: userType, limit: 15, offset: 0 };
    if (userType == 'members' && !this.state.isMemberInitilized) {
      this.setState({ isMemberInitilized: true });
      this.props.dispatch(initializedGroupUser(payloadObject));
      this.props.dispatch(openGroupMemberModal(userType, true));
      return;
    }
    if (userType == 'pending' && !this.state.isPendingMemberInitilized) {
      this.setState({ isPendingMemberInitilized: true });
      this.props.dispatch(initializedGroupUser(payloadObject));
      this.props.dispatch(openGroupMemberModal(userType, true));
      return;
    }
    this.props.dispatch(openGroupMemberModal(userType, false));
  };

  openSharedModal = e => {
    e.preventDefault();
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let shared = group.isTeamAdmin;

    let cardModalDetails = {
      groupID,
      desc: 'Shared',
      count: group.sharedCardsCount,
      type: 'shared',
      name: 'sharedCards'
    };
    this.props.dispatch(updateCardsModal(cardModalDetails));
    this.props.dispatch(openTeamCardsModal(shared, group.sharedCards, cardModalDetails, group));
  };

  openAssignedModal = e => {
    e.preventDefault();
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let shared = group.isTeamAdmin;

    let cardModalDetails = {
      groupID,
      desc: 'Assigned',
      count: group.teamAssignmentsCount,
      type: 'assigned',
      name: 'teamAssignments'
    };
    this.props.dispatch(updateCardsModal(cardModalDetails));
    this.props.dispatch(openTeamCardsModal(shared, group.teamAssignments, cardModalDetails, group));
  };

  openFeaturedModal = e => {
    e.preventDefault();
    let groupID = this.props.groupsV2.currentGroupID;
    let group = this.props.groupsV2[groupID];
    let shared = group.isTeamAdmin;

    let cardModalDetails = {
      groupID,
      desc: 'Featured',
      count: group.featuredCount,
      type: 'featured',
      name: 'teamAssignments'
    };
    this.props.dispatch(updateCardsModal(cardModalDetails));
    this.props.dispatch(openTeamCardsModal(shared, group.featured, cardModalDetails, group));
  };

  openChannelModal = e => {
    e.preventDefault();
    this.props.dispatch(openShowChannelsModal());
  };

  openGroupModal = userType => {
    if (this.state.groupModalV2) {
      this.props.dispatch(openGroupCreationModal(null, this.props.groupsV2.currentGroupID));
    } else {
      this.props.dispatch(openGroupInviteModal(userType));
    }
  };

  openInviteModal = () => {
    if (this.state.dynamicSelection) {
      this.props.dispatch(openMultiactionsModal({}, 'invite'));
    } else {
      this.props.dispatch(openInviteV2UserModal());
    }
  };

  fileStackHandler = (e, accept, max, callback) => {
    e.preventDefault();
    let fromSources = fileStackSources;

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: accept,
            maxFiles: max,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in GroupDetails.filestackClient.pick.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(
          `Error in GroupDetails.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  updateFileStackFile(fileStack) {
    let tempPath = fileStack.filesUploaded;
    let imageUrl = tempPath.length ? tempPath[0].url : undefined;
    if (imageUrl) {
      this.setState({ fileStack: tempPath });
      let group = this.props.groupInfo;
      this.props.dispatch(updateGroup(group.id, { image: imageUrl }));
    } else {
      this.props.dispatch(openSnackBar('Upload Image Error!', true));
    }
  }

  onHover = () => {
    this.setState({ showEditImage: true });
  };

  onHoverOut = () => {
    this.setState({ showEditImage: false });
  };

  openAnalytics = (e, slug) => {
    e.preventDefault();
    this.props.dispatch(push(`/teams/${slug}/analytics`));
  };

  openLeaderboard = e => {
    e.preventDefault();
    this.props.dispatch(
      push(
        this.state.isNewProfileNavigation
          ? `/leaderboard?group_id=${this.props.groupInfo.id}`
          : `/me/leaderboard?group_id=${this.props.groupInfo.id}`
      )
    );
  };

  joinToGroupHandel = group => {
    this.props.dispatch(joinToGroup(group.id));
  };

  groupLeaveConfirmation = group => {
    this.props.dispatch(leaveFromGroup(group.id));
    this.goBack();
  };

  leaveGroup = group => {
    if (group && group.isPrivate) {
      this.props.dispatch(
        confirmationPrivateCardModal(
          tr('Leave Group'),
          tr('You won’t be able to access this group, do you really want to leave ?'),
          false,
          this.groupLeaveConfirmation.bind(this, group)
        )
      );
    } else {
      this.groupLeaveConfirmation(group);
    }
  };

  acceptInvitationGroup = group => {
    this.props.dispatch(acceptInviteToGroup(group.id));
  };

  declineInvitationToGroup = group => {
    this.props.dispatch(declineInviteToGroup(group.id));
  };

  goBack() {
    if (window.history.length > 2) {
      window.history.back();
    } else {
      this.props.dispatch(push('/'));
    }
  }

  removeGroupClickHandler = () => {
    this.setState({ openConfirm: true });
  };

  confirmRemoveGroup = () => {
    this.setState({ openConfirm: false }, () => {
      let title = this.props.groupInfo.name;
      if (this.props.groupInfo.isDynamic) {
        let payload = {
          type: 'dynamic-selection-group',
          action: 'delete',
          payload: {
            groupId: this.props.groupInfo.id,
            groupTitle: title
          }
        };
        createDynamicSelectionGroup(payload)
          .then(() => {
            this.props.dispatch(openSnackBar(`${title} deleted. It will never show again.`, true));
            this.goBack();
          })
          .catch(err => {
            console.error(
              `Error in GroupDetails.confirmRemoveGroup.createDynamicSelectionGroup.func: ${err}`
            );
          });
      } else {
        deleteGroup(this.props.groupInfo.id)
          .then(() => {
            this.props.dispatch(openSnackBar(`${title} deleted. It will never show again.`, true));
            this.goBack();
          })
          .catch(err => {
            console.error(`Error in GroupDetails.deleteGroup.func : ${err}`);
          });
      }
    });
  };

  cancelRemoveGroup = () => {
    this.setState({ openConfirm: false });
  };

  render() {
    let groupDetails = this.props.groupInfo;
    let group = this.props.groupsV2[groupDetails.id];
    let groupName = group.name;
    let description = group && group.description;
    let showViewMore = group.description && group.description.length > 215;
    let coverImage = group && group.imageUrls.medium;
    let defaultUserImage = '/i/images/default_user_blue.png';
    let isInGroup = group.isMember || group.isTeamAdmin || group.isTeamSubAdmin;
    let displayLeaveButton = !group.isMandatory;
    let showIcon =
      groupDetails &&
      (groupDetails.isTeamAdmin ||
        groupDetails.isTeamSubAdmin ||
        Permissions.has('ADMIN_ONLY') ||
        (this.props.currentUser && this.props.currentUser.isSuperAdmin));
    let showIconGroupUpdate =
      showIcon &&
      (!groupDetails.isDynamic || (groupDetails.isDynamic && groupDetails.isDynamicSelected));
    let hiddenItems =
      groupDetails &&
      groupDetails.carousels &&
      groupDetails.carousels.filter(element => !element.visible).map(item => item.default_label);

    const { currentUser } = this.props;

    return (
      <div
        className="row"
        style={this.props.isTeamFeed ? this.styles.rowTeamFeed : this.styles.rowPadding}
      >
        {this.state.openConfirm && (
          <Dialog
            open={this.state.openConfirm}
            autoScrollBodyContent={false}
            style={this.styles.confirmationModal}
            contentStyle={this.styles.confirmationContent}
          >
            <ConfirmationModal
              title={null}
              noNeedClose={true}
              message={`Are you sure you want to remove ${groupName} group? This can't be undone.`}
              cancelClick={this.cancelRemoveGroup}
              callback={this.confirmRemoveGroup}
              confirmBtnTitle="Yes"
              cancelBtnTitle="No"
            />
          </Dialog>
        )}
        <div className="small-12 channel-details">
          <div className="row" style={this.styles.rowMargin}>
            <div
              className="large-3 small-12 cover-image-container"
              style={this.styles.lPadding15}
              onMouseEnter={this.onHover}
              onMouseLeave={this.onHoverOut}
            >
              <a
                href="#"
                onClick={
                  showIcon
                    ? e =>
                        this.fileStackHandler(
                          e,
                          ['image/*'],
                          1,
                          this.updateFileStackFile.bind(this)
                        )
                    : () => {}
                }
                className="cover-image anchorAlignment"
                style={{
                  backgroundImage: 'url("' + (coverImage ? coverImage : '') + '")',
                  position: 'relative',
                  backgroundColor: 'rgba(211,211,211,0)'
                }}
              >
                {showIcon && this.state.showEditImage && (
                  <div style={this.styles.cameraBg}>
                    <CameraIcon style={this.styles.cameraIcon} />
                  </div>
                )}
              </a>
            </div>
            <div className="large-9 small-12 details-container" style={this.styles.lrPadding15}>
              <div style={this.styles.groupTitle}>
                {groupName}
                {showIcon && (
                  <IconButton
                    aria-label={`edit group, ${groupName}`}
                    onTouchTap={this.triggerUpdate}
                    iconStyle={this.styles.editIcon}
                    style={this.styles.editTitle}
                  >
                    <EditIcon color="#6f708b" />
                  </IconButton>
                )}
                {showIconGroupUpdate && (
                  <IconButton
                    aria-label={`delete group, ${groupName}`}
                    onTouchTap={this.removeGroupClickHandler}
                    iconStyle={this.styles.deleteIcon}
                    style={this.styles.deleteTitle}
                  >
                    <DeleteIcon color="#6f708b" />
                  </IconButton>
                )}
              </div>
              <div className="channel-description">
                {description.slice(0, 215)}
                {showViewMore ? (
                  <span style={{ fontSize: '12px', fontWeight: '400' }}>
                    ...
                    <a style={this.styles.viewMore} onClick={this.openDescriptionPopOver}>
                      {tr('View More')}
                    </a>
                  </span>
                ) : (
                  ''
                )}
              </div>
              <div className="channel-content-list">
                {groupDetails && groupDetails.isTeamAdmin && this.state.teamAnalyticsEnabled && (
                  <span>
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={e => this.openAnalytics(e, this.props.groupInfo.slug)}>
                        {' '}
                        {tr('Analytics')}{' '}
                      </a>
                    </span>{' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                  </span>
                )}

                {Permissions['enabled'] !== undefined &&
                  Permissions.has('GET_LEADERBOARD_INFORMATION') &&
                  this.props.team &&
                  this.props.team.config &&
                  this.props.team.config.leaderboard && (
                    <span>
                      <span style={this.styles.countLabel}>
                        <a href="#" onClick={this.openLeaderboard}>
                          {' '}
                          {tr('Leaderboard')}{' '}
                        </a>
                      </span>{' '}
                      &nbsp; &nbsp; | &nbsp; &nbsp;
                    </span>
                  )}
                <span style={this.styles.countLabel}>
                  <a href="#" onClick={e => this.openTeamMemberModal(e, 'members')}>
                    {' '}
                    {tr('Members')} ({group.membersTotal}){' '}
                  </a>
                </span>
                {group.pendingTotal > 0 && (
                  <span>
                    {' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={e => this.openTeamMemberModal(e, 'pending')}>
                        {' '}
                        {tr('Pending Members')} ({group.pendingTotal}){' '}
                      </a>
                    </span>
                  </span>
                )}
                {hiddenItems && !~hiddenItems.indexOf('Featured') && (
                  <span>
                    {' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={this.openFeaturedModal}>
                        {' '}
                        {tr('Featured')} ({group.featuredCount}){' '}
                      </a>
                    </span>
                  </span>
                )}
                {hiddenItems && !~hiddenItems.indexOf('Assigned') && (
                  <span>
                    {' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={this.openAssignedModal}>
                        {' '}
                        {tr('Assigned')} ({group.teamAssignmentsCount}){' '}
                      </a>
                    </span>
                  </span>
                )}
                {hiddenItems && !~hiddenItems.indexOf('Shared') && (
                  <span>
                    {' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={this.openSharedModal}>
                        {' '}
                        {tr('Shared')} ({group.sharedCardsCount}){' '}
                      </a>
                    </span>
                  </span>
                )}
                {hiddenItems && !~hiddenItems.indexOf('Channels') && (
                  <span>
                    {' '}
                    &nbsp; &nbsp; | &nbsp; &nbsp;
                    <span style={this.styles.countLabel}>
                      <a href="#" onClick={this.openChannelModal}>
                        {' '}
                        {tr('Channels')} ({group.teamChannelsCount}){' '}
                      </a>
                    </span>
                  </span>
                )}
              </div>
              <div className="creator-curator">
                <span style={this.styles.labelBlock}>
                  <span style={this.styles.groupLeaderTitle}>{tr('Group Leader')}:</span>
                  {showIcon && (
                    <IconButton
                      aria-label="Group Leader edit"
                      onTouchTap={this.openGroupModal.bind(this, 'group-leader')}
                      iconStyle={this.styles.editIcon}
                      style={this.styles.leaderEdit}
                    >
                      <EditIcon color="#6f708b" />
                    </IconButton>
                  )}
                </span>
                {group &&
                  group.owners &&
                  group.owners.length > 0 &&
                  group.owners.slice(0, 2).map((owner, i) => {
                    let userAvatar =
                      (owner.avatarimages && owner.avatarimages.tiny) || defaultUserImage;
                    return (
                      <span
                        style={{ ...this.styles.groupLeaderFont, fontWeight: 400 }}
                        key={owner.id}
                      >
                        <a
                          href={
                            '/' +
                            (owner.handle.replace('@', '') === currentUser.handle
                              ? 'me'
                              : owner.handle)
                          }
                          style={this.styles.groupLeader}
                        >
                          <span style={{ marginRight: '2px' }}>
                            <img className="author-img" src={userAvatar} height="26" width="26" />
                            <span className="author-name">{owner.name}</span>
                          </span>
                        </a>
                        {i === 0 && group.owners.length > 2 ? (
                          <span style={{ margin: '0 2px 0 -8px' }}>, </span>
                        ) : (
                          ''
                        )}
                      </span>
                    );
                  })}

                <span className="float-right" style={{ marginTop: '-5px', marginBottom: '-3px' }}>
                  {groupDetails && groupDetails.isTeamAdmin && !groupDetails.isDynamic && (
                    <SecondaryButton
                      label={tr('INVITE')}
                      className="invite"
                      onTouchTap={this.openInviteModal}
                    />
                  )}
                </span>

                {this.state.groupPageV2 && (
                  <span className="float-right" style={{ marginTop: '-5px', marginBottom: '-3px' }}>
                    {!isInGroup && !group.isPrivate && !group.isPending && (
                      <SecondaryButton
                        label={tr('Join')}
                        onTouchTap={() => this.joinToGroupHandel(group)}
                        labelStyle={this.styles.actionBtnLabel}
                      />
                    )}
                    {group.isPending && (
                      <span>
                        <SecondaryButton
                          label={tr('Accept')}
                          onTouchTap={() => this.acceptInvitationGroup(group)}
                          labelStyle={this.styles.actionBtnLabel}
                        />
                        <SecondaryButton
                          label={tr('Decline')}
                          onTouchTap={() => this.declineInvitationToGroup(group)}
                          labelStyle={this.styles.actionBtnLabel}
                        />
                      </span>
                    )}
                    {isInGroup && !group.isEveryoneTeam && displayLeaveButton && (
                      <SecondaryButton
                        label={tr('Leave')}
                        onTouchTap={() => this.leaveGroup(group)}
                        labelStyle={this.styles.actionBtnLabel}
                      />
                    )}
                  </span>
                )}

                {group && group.owners && group.owners.length > 2 && (
                  <span style={{ ...this.styles.groupLeaderFont, marginLeft: '-8px' }}>
                    and
                    <a style={this.styles.viewMore} onClick={this.openPopOver.bind(this, 'Owners')}>
                      {group.owners.length - 2 + ' ' + tr('others')}
                    </a>
                  </span>
                )}

                <Popover
                  open={this.state.openDescriptionPopOver}
                  anchorEl={this.state.anchorDescEl}
                  anchorOrigin={{ horizontal: 'middle', vertical: 'bottom' }}
                  targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                  onRequestClose={this.closePopOver.bind(this, 'Owners')}
                  animation={PopoverAnimationVertical}
                  className="channel-desc-popover"
                >
                  <IconButton
                    onTouchTap={this.closePopOver}
                    iconStyle={{ width: '20px', height: '20px' }}
                    style={this.styles.closeIcon}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                  <h5 className="channel-popover-header">{tr('Description')}</h5>
                  <p style={this.styles.fullDescription}>{group.description}</p>
                </Popover>
                <Popover
                  open={this.state.openPopOverOwners}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  onRequestClose={this.closePopOver.bind(this, 'Owners')}
                  animation={PopoverAnimationVertical}
                  className="channel-desc-popover"
                >
                  <h5 style={this.styles.popoverTitle}>{tr('Group Leader:')}</h5>
                  <IconButton
                    aria-label="Group Leader close"
                    onTouchTap={this.closePopOver.bind(this, 'Owners')}
                    iconStyle={{ width: '20px', height: '20px' }}
                    style={this.styles.closeIcon}
                  >
                    <CloseIcon color="#acadc1" style={{ height: '45px' }} />
                  </IconButton>
                  <div style={this.styles.popoverList}>
                    {group &&
                      group.owners &&
                      group.owners.length > 0 &&
                      group.owners.slice(2, group.owners.length).map(owner => {
                        let userAvatar =
                          (owner.avatarimages && owner.avatarimages.tiny) || owner.avatar;
                        return (
                          <a
                            href={'/' + owner.handle}
                            style={this.styles.dropDownGroupLeader}
                            key={owner.id}
                          >
                            <span style={{ padding: '2px' }}>
                              <img className="author-img" src={userAvatar} height="26" width="26" />
                              <span
                                title={owner.name}
                                className="author-name"
                                style={{ color: 'rgb(111, 112, 139)' }}
                              >
                                {owner.name.length > 20
                                  ? owner.name.slice(0, 20) + '..'
                                  : owner.name}
                              </span>
                            </span>
                          </a>
                        );
                      })}
                  </div>
                </Popover>
              </div>
              <div className="creator-curator">
                <span style={this.styles.labelBlock}>
                  <span style={this.styles.groupLeaderTitle}>{tr('Group Admin')}:</span>
                  {showIcon && (
                    <IconButton
                      aria-label="Group admin edit"
                      onTouchTap={this.openGroupModal.bind(this, 'group-admin')}
                      iconStyle={this.styles.editIcon}
                      style={this.styles.leaderEdit}
                    >
                      <EditIcon color="#6f708b" />
                    </IconButton>
                  )}
                </span>
                {group &&
                  group.subAdmins &&
                  group.subAdmins.length > 0 &&
                  group.subAdmins.slice(0, 2).map((admin, i) => {
                    let userAvatar =
                      (admin.avatarimages && admin.avatarimages.tiny) || defaultUserImage;
                    return (
                      <span
                        style={{ ...this.styles.groupLeaderFont, fontWeight: 400 }}
                        key={admin.id}
                      >
                        <a
                          href={
                            '/' +
                            (admin.handle.replace('@', '') === currentUser.handle
                              ? 'me'
                              : admin.handle)
                          }
                          style={this.styles.groupLeader}
                        >
                          <span style={{ marginRight: '2px' }}>
                            <img className="author-img" src={userAvatar} height="26" width="26" />
                            <span className="author-name">{admin.name}</span>
                          </span>
                        </a>
                        {i === 0 && group.subAdmins.length > 2 ? (
                          <span style={{ margin: '0 2px 0 -8px' }}>, </span>
                        ) : (
                          ''
                        )}
                      </span>
                    );
                  })}

                {group && group.subAdmins && group.subAdmins.length > 2 && (
                  <span style={{ ...this.styles.groupLeaderFont, marginLeft: '-8px' }}>
                    and
                    <a
                      style={this.styles.viewMore}
                      onClick={this.openPopOver.bind(this, 'SubAdmins')}
                    >
                      {group.subAdmins.length - 2 + ' ' + tr('others')}
                    </a>
                  </span>
                )}
                <Popover
                  open={this.state.openPopOverSubAdmins}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  onRequestClose={this.closePopOver.bind(this, 'SubAdmins')}
                  animation={PopoverAnimationVertical}
                  className="channel-desc-popover"
                >
                  <h5 style={this.styles.popoverTitle}>{tr('Group Admin:')}</h5>
                  <IconButton
                    aria-label="Group Leader close"
                    onTouchTap={this.closePopOver.bind(this, 'SubAdmins')}
                    iconStyle={{ width: '20px', height: '20px' }}
                    style={this.styles.closeIcon}
                  >
                    <CloseIcon color="#acadc1" style={{ height: '45px' }} />
                  </IconButton>
                  <div style={this.styles.popoverList}>
                    {group &&
                      group.subAdmins &&
                      group.subAdmins.length > 0 &&
                      group.subAdmins.slice(2, group.subAdmins.length).map(admin => {
                        let userAvatar =
                          (admin.avatarimages && admin.avatarimages.tiny) || admin.avatar;
                        return (
                          <a
                            href={'/' + admin.handle}
                            style={this.styles.dropDownGroupLeader}
                            key={admin.id}
                          >
                            <span style={{ padding: '2px' }}>
                              <img className="author-img" src={userAvatar} height="26" width="26" />
                              <span
                                title={admin.name}
                                className="author-name"
                                style={{ color: 'rgb(111, 112, 139)' }}
                              >
                                {admin.name.length > 20
                                  ? `${admin.name.slice(0, 20)}..`
                                  : admin.name}
                              </span>
                            </span>
                          </a>
                        );
                      })}
                  </div>
                </Popover>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

GroupDetails.propTypes = {
  groupsV2: PropTypes.object,
  currentUser: PropTypes.object,
  groupInfo: PropTypes.object,
  isTeamFeed: PropTypes.bool,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(GroupDetails);
