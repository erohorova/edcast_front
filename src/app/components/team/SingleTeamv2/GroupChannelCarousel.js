import React from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';
import ChannelLoader from '../../common/ChannelLoader';
import { getTeamChannels } from '../../../actions/groupsActionsV2';
import IconButton from 'material-ui/IconButton/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';
import AddChannelToGroupModal from '../../modals/AddChannelToGroupModal';
import Carousel from '../../common/Carousel';
import { openShowChannelsModal } from '../../../actions/modalActions';
import Channel from '../../discovery/Channel.jsx';
import EmptyBlock from '../../discovery/EmptyBlock';

class GroupChannelCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0 51px'
      },
      withTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 340px)'
      },
      withNewTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 360px)'
      },
      rowTeemFeed: {
        padding: 0,
        margin: 0
      },
      padTeemFeed: {
        padding: 0
      },
      row: {
        maxWidth: '100%'
      },
      homeUI: {
        padding: 0
      },
      addLink: {
        textDecoration: 'underline',
        color: '#4990e2',
        marginRight: '0.5rem'
      },
      editIcon: {
        width: '21px',
        height: '21px',
        verticalAlign: 'bottom'
      },
      editChannelsIcon: {
        padding: '0',
        marginLeft: '0.3125rem',
        width: '1.3125rem',
        height: '1.3125rem'
      }
    };
    this.state = {
      openAddChannelModal: false
    };
  }

  componentDidMount() {
    let groupDetail = this.props.groupDetail;
    let groupID = groupDetail.id;
    this.props.dispatch(
      getTeamChannels(groupID, {
        limit: groupDetail.teamChannelsLimit,
        offset: 0,
        fields: 'id,allow_follow,image_url,is_following,is_private,slug,label,profile_image_url'
      })
    );
  }

  handleViewAllClick = (e, removable) => {
    e.preventDefault();
    this.props.dispatch(openShowChannelsModal(removable));
  };

  toggleAddingChannelModal = e => {
    e && e.preventDefault();
    this.setState(prevState => ({ openAddChannelModal: !prevState.openAddChannelModal }));
  };

  updateChannelsList = async () => {
    let groupDetail = this.props.groupDetail;
    let groupID = groupDetail.id;
    this.props.dispatch(
      getTeamChannels(groupID, { limit: this.props.groupsV2[groupID].teamChannelsLimit, offset: 0 })
    );
  };

  render() {
    let groupID = this.props.groupDetail.id;
    let group = groupID && this.props.groupsV2[groupID + ''];
    let teamChannels = groupID && group.teamChannels;
    let teamChannelsCount = groupID && group.teamChannelsCount;
    let teamChannelsLoaded = groupID && group.teamChannelsLoaded;

    return (
      <div>
        {!teamChannelsLoaded ? (
          <div className="small-12">
            <div className="small-12 group-title">
              <div>{tr('Channel')}</div>
            </div>
            <ChannelLoader loadingCards={5} />
          </div>
        ) : (
          <div className="small-12 columns">
            <div className="row group-title">
              <div className="small-6 columns">
                <div className="itemLabel">
                  {tr('Channels')} ({teamChannelsCount})
                  {(group.isTeamAdmin || group.isTeamSubAdmin) && (
                    <IconButton
                      aria-label="edit"
                      onTouchTap={e => this.handleViewAllClick(e, true)}
                      iconStyle={this.styles.editIcon}
                      style={this.styles.editChannelsIcon}
                    >
                      <EditIcon color="#6f708b" />
                    </IconButton>
                  )}
                </div>
              </div>
              <div className="small-6 columns text-right">
                {(group.isTeamAdmin || group.isTeamSubAdmin) && (
                  <a href="#" onClick={this.toggleAddingChannelModal} style={this.styles.addLink}>
                    {tr('+ Add Channel')}
                  </a>
                )}
                {teamChannelsCount > 5 && (
                  <a
                    href="#"
                    onClick={e => this.handleViewAllClick(e, false)}
                    style={{ textDecoration: 'underline' }}
                  >
                    {tr('View All')}
                  </a>
                )}
              </div>
            </div>
            {teamChannelsCount ? (
              <div className="row">
                <div className="small-12 columns channel-card-wrapper">
                  <div className="channel-card-wrapper-inner">
                    <Carousel slidesToShow={5}>
                      {teamChannels.map(channel => (
                        <div key={channel.id}>
                          <Channel
                            channel={channel}
                            id={channel.id}
                            isFollowing={channel.isFollowing}
                            title={channel.label}
                            imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
                            slug={channel.slug}
                            allowFollow={channel.allowFollow}
                          />
                        </div>
                      ))}
                    </Carousel>
                  </div>
                </div>
              </div>
            ) : (
              <EmptyBlock
                className="content"
                title={tr('No channels followed')}
                style={{ width: '100%' }}
              />
            )}
          </div>
        )}
        {this.state.openAddChannelModal && (
          <AddChannelToGroupModal
            title={'Add Channel to Group'}
            message={'Do you want to delete the comment?'}
            closeModal={this.toggleAddingChannelModal}
            groupID={groupID}
            updateChannelsList={this.updateChannelsList}
          />
        )}
      </div>
    );
  }
}

GroupChannelCarousel.propTypes = {
  groupsV2: PropTypes.object,
  windowInnerWidth: PropTypes.any,
  groupDetail: PropTypes.object,
  isTeamFeed: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupChannelCarousel);
