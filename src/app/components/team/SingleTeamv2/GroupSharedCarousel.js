import React from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';
import CardLoader from '../../common/CardLoader';
import { getTeamCards, deleteCard } from '../../../actions/groupsActionsV2';
import TeamCardsCarousel from './TeamCardsCarousel';
import EmptyBlock from '../../discovery/EmptyBlock';

class GroupSharedCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0px 12px'
      },
      rowTeamFeed: {
        padding: 0,
        margin: 0
      }
    };
  }

  componentDidMount() {
    let groupDetail = this.props.groupDetail;
    let groupID = groupDetail.id;
    this.props.dispatch(
      getTeamCards(
        groupID,
        'sharedCards',
        { limit: groupDetail.cardsLimit, offset: 0, type: 'shared' },
        this.props.filterByLanguage
      )
    );
  }

  render() {
    let groupID = this.props.groupDetail.id;
    let group = groupID && this.props.groupsV2[groupID + ''];
    let sharedCards = groupID && group.sharedCards;
    let sharedCardsCount = groupID && group.sharedCardsCount;
    let sharedCardsLoaded = groupID && group.sharedCardsLoaded;

    return (
      <div>
        {!sharedCardsLoaded ? (
          <div className="small-12">
            <div className="small-12 group-title">
              <div>{tr('Shared')}</div>
            </div>
            <CardLoader loadingCards={4} />
          </div>
        ) : sharedCardsCount ? (
          <TeamCardsCarousel
            group={group}
            isTeamFeed={this.props.isTeamFeed}
            description={'Shared'}
            items={sharedCards}
            deleteSharedCard={group.isTeamAdmin}
            count={sharedCardsCount}
            editable={false}
            windowInnerWidth={this.props.windowInnerWidth}
          />
        ) : (
          <div>
            <div className="small-12 group-title">
              <div>{tr('Shared')}</div>
            </div>
            <EmptyBlock
              className="content"
              title={tr('Share a card with the group')}
              style={{ width: '100%' }}
            />
          </div>
        )}
      </div>
    );
  }
}

GroupSharedCarousel.propTypes = {
  groupsV2: PropTypes.object,
  windowInnerWidth: PropTypes.any,
  groupDetail: PropTypes.object,
  isTeamFeed: PropTypes.any,
  filterByLanguage: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupSharedCarousel);
