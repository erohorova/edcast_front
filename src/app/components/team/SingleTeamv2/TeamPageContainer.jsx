import React from 'react';
import { connect } from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import { users, teams, groups } from 'edc-web-sdk/requests/index';
import { getItems } from 'edc-web-sdk/requests/users.v2';
import { getTeamUsers } from 'edc-web-sdk/requests/teams';
import { updateUser } from '../../../actions/usersActions';
import ListView from '../../common/ListView';
import UserListItem from '../../common/UserListItem';
import { push } from 'react-router-redux';
import _ from 'lodash';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import * as importSDK from 'edc-web-sdk/requests/importCSV';
import { uploadImage } from 'edc-web-sdk/requests/imageUpload';
import FlashAlert from '../../common/FlashAlert';
import Spinner from '../common/spinner';
import colors from 'edc-web-sdk/components/colors';
import { openInviteUserModal } from '../../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class TeamPageContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userIds: [],
      users: [],
      pending: true,
      limit: 10,
      filterType: 'all',
      uploadDisabled: false,
      bulkUploadButtonText: 'BULK UPLOAD',
      groupInfo: {}
    };

    this.filterOptions = [];

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreTeams = this.showMoreTeams.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  componentDidMount() {
    let slug = this.props.params && this.props.params.slug ? this.props.params.slug : '';
    groups.getTeamDetail(slug).then(group => {
      this.getUsers({ team_ids: group.id, slug: group.id });
      this.setState({ groupInfo: group });
    });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = _.throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreTeams();
        }
      }
    },
    150,
    { leading: false }
  );

  getUsers = payload => {
    this.setState({ pending: true });
    getTeamUsers(payload).then(users => {
      let userIds = users.items.map(user => user.id);
      let isLastPage = userIds.length < this.state.limit;
      this.props.dispatch(updateUser(users.items));
      this.setState({
        users: _.uniqBy([...this.state.users, ...users.items], 'id'),
        userIds: _.uniq(this.state.userIds.concat(userIds)),
        pending: false,
        isLastPage: isLastPage
      });
    });
  };

  showMoreTeams = () => {
    let payload = {
      team_ids: this.state.groupInfo.id,
      slug: this.state.groupInfo.id,
      offset: this.state.userIds.length,
      q: this.state.searchText
    };
    this.getUsers(payload);
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      let payload = {
        team_ids: this.state.groupInfo.id,
        slug: this.state.groupInfo.id,
        q: e.target.value
      };
      this.setState({ searchText: e.target.value, users: [] }, () => {
        this.getUsers(payload);
      });
    }
  };

  openInviteUserModal = () => {
    this.props.dispatch(openInviteUserModal(this.state.groupInfo));
  };

  bulkFileInputChangeHandler = () => {
    if (this._bulkFileInput.files.length) {
      this.setState({ isBulkFileUploading: true, pendingBulkFileUpload: true });
      let file = this._bulkFileInput.files[0];
      this.setState({
        uploadDisabled: true
      });
      return uploadImage(file).then(url => {
        return importSDK
          .getFileData(url)
          .then(data => {
            if (data.file_id) {
              return importSDK
                .uploadCSV(data.file_id, true, true, [])
                .then(data => {
                  this.setState(
                    {
                      bulkUploadButtonText: 'UPLOADED',
                      uploadDisabled: false
                    },
                    function() {
                      setTimeout(() => {
                        this.setState({
                          bulkUploadButtonText: 'BULK UPLOAD'
                        });
                      }, 5000);
                    }
                  );
                })
                .catch(error => {
                  this.showAlert('There was some error with your CSV.');
                  this.setState({
                    uploadDisabled: false
                  });
                });
            } else {
              this.showAlert('There was some error with your CSV.');
              this.setState({
                uploadDisabled: false
              });
            }
          })
          .catch(error => {
            this.showAlert('There was some error with your CSV.');
            this.setState({
              uploadDisabled: false
            });
          });
      });
    }
  };

  showAlert(message) {
    this.setState({ showAlert: true, alertMessage: message });
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  render() {
    let currentUserId = this.props.currentUser.get('id');
    return (
      <div>
        {this.state.showAlert && (
          <FlashAlert
            message={this.state.alertMessage}
            toShow={this.state.showAlert}
            removeAlert={this.removeAlert}
          />
        )}
        <div className="row">
          <div className="small-12 container-padding vertical-spacing-medium">
            {this.state.groupInfo.name !== undefined && this.state.groupInfo.name !== undefined && (
              <div>
                <h5>{this.state.groupInfo.name}</h5>
                <div>{this.state.groupInfo.description}</div>
              </div>
            )}
          </div>
          <div
            className="small-12 container-padding vertical-spacing-medium"
            style={{ marginBottom: '-80px' }}
          >
            {this.state.groupInfo.isTeamAdmin && (
              <SecondaryButton
                label={tr('INVITE NEW')}
                disabled={this.state.uploadDisabled}
                className="invite"
                onTouchTap={this.openInviteUserModal}
              />
            )}
          </div>
        </div>
        <div className="row">
          <div className="small-12 container-padding vertical-spacing-medium">
            <div className="vertical-spacing-large">
              <ListView
                isTable={false}
                showFilterSearch={true}
                handleFilterChange={this.handleFilterChange}
                handleFilterSearch={this.handleFilterSearch}
                pending={this.state.pending}
                filterValue={this.state.filterType}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr('There are no users available.')}
                  </div>
                }
                selectedState={this.state.filterType}
              >
                {this.state.users.map((realUser, index) => {
                  let user = this.props.users.getIn(['idMap', realUser.id]);
                  return (
                    <UserListItem
                      teamPage={true}
                      key={index}
                      user={user}
                      realUser={realUser}
                      team={this.state.groupInfo}
                      isCurrentUser={realUser.id === currentUserId}
                      isPendingUser={user.get('pending')}
                    />
                  );
                })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  currentUser: state.currentUser,
  users: state.users
}))(TeamPageContainer);
