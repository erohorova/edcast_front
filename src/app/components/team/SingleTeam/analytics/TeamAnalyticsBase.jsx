import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import colors from 'edc-web-sdk/components/colors/index';
// import TeamsLinearGraph from './TeamsLinearGraph';
import {
  getGraphData,
  getGroupDetail,
  topContent,
  topContributors
} from '../../../../actions/groupAnalyticActions';
import SmallGraphContainer from './SmallGraphContainer';
import TeamGraphStandalone from './TeamGraphStandalone';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import moment from 'moment';
import { push } from 'react-router-redux';
import Spinner from '../../../common/spinner';
import { CSVLink } from 'react-csv';
import Popover from 'material-ui/Popover';
import { List, ListItem } from 'material-ui/List';

const TeamsLinearGraph = Loadable({
  loader: () => import('./TeamsLinearGraph'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

class TeamAnalyticsBase extends Component {
  constructor(props, context) {
    super(props, context);
    let startDate = moment
      .utc()
      .subtract(1, 'year')
      .startOf('month')
      .format('MM/DD/YYYY');
    let finishDate = moment
      .utc()
      .add(1, 'month')
      .startOf('month')
      .format('MM/DD/YYYY');

    this.state = {
      clcEnabled: window.ldclient.variation('clc', false),
      startDate,
      finishDate,
      listOfScreens: ['top', 'graph', 'all'],
      currentScreen: 'all',
      period: 'year',
      graphLoading: true,
      currentTop: '',
      metricsForDownload: [],
      isTeamAnalyticsV2: window.ldclient.variation('group-analytics-v2', false)
    };

    this.styles = {
      list: {
        padding: 0
      }
    };

    this.topContent = [];
    this.topContributors = [];
  }

  componentDidMount = () => {
    let slug = this.props.routeParams.slug;

    this.props
      .dispatch(getGroupDetail(slug))
      .then(group => {
        this.setState({ groupId: group.id });
        let payload = {
          from_date: this.state.startDate,
          to_date: this.state.finishDate,
          team_id: group.id
        };

        let payload_for_top = {
          limit: 12,
          offset: 0
        };

        let teamId = this.state.groupId;
        this.props
          .dispatch(getGraphData(teamId, payload, this.state.isTeamAnalyticsV2))
          .then(graphData => {
            this.setState({ graphLoading: false, graphData });
          })
          .catch(err => {
            console.error(`Error in TeamAnalyticsBase.getGraphData.func : ${err}`);
          });
        //Sending limit and offsets
        this.props
          .dispatch(topContent(teamId, payload_for_top))
          .then(data => {
            this.topContent =
              (data &&
                data.topContent.map(item => {
                  delete item.id;
                  return item;
                })) ||
              [];
          })
          .catch(err => {
            console.error(`Error in TeamAnalyticsBase.topContent.func : ${err}`);
          });
        this.props
          .dispatch(topContributors(teamId, payload_for_top))
          .then(data => {
            this.topContributors =
              (data &&
                data.topContributors.map(item => {
                  delete item.id;
                  return item;
                })) ||
              [];
          })
          .catch(err => {
            console.error(`Error in TeamAnalyticsBase.topContributors.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in TeamAnalyticsBase.getGroupDetail.func : ${err}`);
      });
  };

  openAssignmentPerformance = () => {
    let slug = this.props.routeParams.slug;
    this.props.dispatch(push(`/teams/${slug}/analytics/assignment`));
  };

  onGraphClick = currentGraph => {
    this.setState({ currentScreen: 'graph', currentGraph });
  };

  onBackClick = () => {
    if (this.state.currentScreen === 'all') {
      window.history.back();
    } else {
      this.setState({ currentScreen: 'all' });
    }
  };

  showTopContent = () => {
    this.setState({ currentScreen: 'top', currentTop: 'content' });
  };

  showTopContributor = () => {
    this.setState({ currentScreen: 'top', currentTop: 'contributor' });
  };

  downloadMetrics = name => {
    this[name].children[0].click();
  };

  createDownloadSection = (dataForDownload, filename) => {
    return (
      <div className="download-button-container">
        <PrimaryButton
          className="pull-right download-button"
          label={tr(this.state.isDownloading ? 'DOWNLOADING' : 'DOWNLOAD')}
          onClick={this.downloadMetrics.bind(this, `_download_${filename}`)}
        />
        <div className="hide-checking" ref={input => (this[`_download_${filename}`] = input)}>
          <CSVLink data={dataForDownload} target="_self" filename={`${filename}.csv`}>
            download csv
          </CSVLink>
        </div>
      </div>
    );
  };

  render() {
    let groupAnalytics = this.props.groupAnalytics;
    return (
      <div className="team-analytics-base">
        <div className="row row-width">
          <div className="column small-6">
            <div aria-label="back" className="breadcrumbBack" onTouchTap={this.onBackClick}>
              <BackIcon style={{ width: '19px' }} color={'#454560'} />
            </div>
          </div>
          {!(this.state.currentScreen === 'assignment') && this.state.currentScreen === 'all' && (
            <div className="column small-6 clearfix">
              <PrimaryButton
                className="float-right"
                label={tr('Assignment Performance')}
                onTouchTap={this.openAssignmentPerformance}
              />
            </div>
          )}
        </div>

        {this.state.currentScreen === 'all' && (
          <div className="row row-width">
            <div className="column small-6 top-container" onClick={this.showTopContent}>
              {this.props.groupAnalytics.topContent && (
                <TeamsLinearGraph
                  type={'content'}
                  viewStyle={'group'}
                  data={this.props.groupAnalytics.topContent.slice(0, 4)}
                />
              )}
            </div>
            <div className="column small-6 top-container" onClick={this.showTopContributor}>
              {this.props.groupAnalytics.topContributors && (
                <TeamsLinearGraph
                  type={'contributors'}
                  viewStyle={'group'}
                  data={this.props.groupAnalytics.topContributors.slice(0, 4)}
                />
              )}
            </div>
          </div>
        )}

        {this.state.currentScreen === 'top' && this.state.currentTop === 'content' && (
          <div className="row row-width">
            <div className="column small-12">
              {this.createDownloadSection(this.topContent, 'Top_Content')}
              <TeamsLinearGraph
                type={'content'}
                charLimit={30}
                viewStyle={'standalone'}
                data={this.props.groupAnalytics.topContent}
              />
            </div>
          </div>
        )}

        {this.state.currentScreen === 'top' && this.state.currentTop === 'contributor' && (
          <div className="row row-width">
            <div className="column small-12">
              {this.createDownloadSection(this.topContributors, 'Top_Contributors')}
              <TeamsLinearGraph
                type={'contributors'}
                charLimit={30}
                viewStyle={'standalone'}
                data={this.props.groupAnalytics.topContributors}
              />
            </div>
          </div>
        )}

        {groupAnalytics && this.state.currentScreen == 'all' && (
          <div className="row row-width graphs-container">
            {this.state.graphLoading && (
              <div className="small-12 text-center">
                <Spinner />
              </div>
            )}
            {groupAnalytics.graphData &&
              !!groupAnalytics.graphData.length &&
              groupAnalytics.graphData.map((graph, i) => {
                let clcFlag = this.state.clcEnabled ? true : !(graph.name === 'clc');

                if (clcFlag) {
                  return (
                    <SmallGraphContainer
                      key={i}
                      onClick={this.onGraphClick.bind(this, graph)}
                      startDate={this.state.startDate}
                      finishDate={this.state.finishDate}
                      graphTitle={graph.title}
                      graphSubTitle={graph.subTitle}
                      graphValue={graph.graphValue}
                      period={this.state.period}
                    />
                  );
                }
              })}
          </div>
        )}
        {this.state.currentScreen === 'graph' && (
          <TeamGraphStandalone
            startDate={this.state.startDate}
            finishDate={this.state.finishDate}
            graphTitle={this.state.currentGraph.title}
            graphSubTitle={this.state.currentGraph.subTitle}
            graphValue={this.state.currentGraph.graphValue}
            period={this.state.period}
            createDownloadSection={this.createDownloadSection}
          />
        )}
      </div>
    );
  }
}

TeamAnalyticsBase.propTypes = {
  groupAnalytics: PropTypes.any,
  routeParams: PropTypes.any
};

export default connect(state => ({
  groupAnalytics: state.groupAnalytic.toJS()
}))(TeamAnalyticsBase);
