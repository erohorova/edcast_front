import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TeamAnalyticsGraph from './TeamAnalyticsGraph';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';

class SmallGraphContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      graphColor: '#e4e4eb'
    };
  }

  render() {
    return (
      <div
        className="small-graph-container graph-container columns small-4"
        onClick={this.props.onClick}
      >
        <Paper>
          <div className="container-padding">
            <div className="graph-title">{tr(this.props.graphTitle)}</div>
            <div className="graph-subtitle">{this.props.graphSubTitle}</div>
            <div className="graph">
              <TeamAnalyticsGraph
                strokeColor={this.state.graphColor}
                fillColor={this.state.graphColor}
                graphValue={this.props.graphValue}
                startDate={this.props.startDate}
                finishDate={this.props.finishDate}
                period={this.props.period}
              />
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

SmallGraphContainer.propTypes = {
  graphValue: PropTypes.any,
  graphTitle: PropTypes.string,
  graphSubTitle: PropTypes.string,
  period: PropTypes.string,
  startDate: PropTypes.string,
  finishDate: PropTypes.string,
  graphRecord: PropTypes.func,
  onClick: PropTypes.func
};

export default SmallGraphContainer;
