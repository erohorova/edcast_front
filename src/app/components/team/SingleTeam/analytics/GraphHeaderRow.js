import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

function GraphHeaderRow() {
  return (
    <div className="row graph-header-row">
      <div className="small-12 medium-3 text-center columns graph-header">
        <div>{tr('X Axis')}</div>
      </div>
      <div className="small-12 medium-3 text-center columns graph-header">
        <div>{tr('Y Axis')}</div>
      </div>
    </div>
  );
}

export default GraphHeaderRow;
