import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/CircularProgress';
import {
  AreaChart,
  Area,
  LineChart,
  Line,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from 'recharts';
import moment from 'moment';

class TeamAnalyticsGraph extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      period: this.props.period,
      startDate: this.props.startDate,
      finishDate: this.props.finishDate
    };

    this.keys = {
      day: [],
      month: [],
      year: []
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  createChartData = dv => {
    if (this.state.period === 'day') {
      this.setDateForGraph(dv.length);
    } else if (this.state.period === 'year' || this.state.period === 'month') {
      this.setMonthsForGraph(dv.length);
    }

    let key = this.keys[this.state.period];
    let newData = [];
    for (let i = 0; i < dv.length; i++) {
      let d = { name: key[i], Current: dv[i] };
      newData.push(d);
    }

    if (this.props.graphRecord) {
      this.props.graphRecord(newData);
    }
    return newData;
  };

  setDateForGraph = l => {
    let curStartDate = new Date(this.state.startDate);
    let curEndDate = moment()
      .add(l, 'days')
      .toDate();
    let rangeOfDates = [];
    while (curStartDate < curEndDate) {
      rangeOfDates.push(curStartDate.getDate());
      let newDate = curStartDate.setDate(curStartDate.getDate() + 1);
      curStartDate = new Date(newDate);
    }

    this.keys[this.state.period] = rangeOfDates;
  };

  //this is an additional method to plot years eg: 2016, 2017, 2018
  setYearForGraph = l => {
    let curStartDate = moment(this.state.startDate).year();
    let curEndDate = moment(this.state.finishDate).year();
    let rangeOfYears = [];
    while (curStartDate <= curEndDate) {
      rangeOfYears.push(curStartDate++);
    }
    this.keys[this.state.period] = rangeOfYears;
  };

  setMonthsForGraph = l => {
    let startDate = moment(this.state.startDate);
    let endDate = moment(this.state.finishDate);

    let rangeOfMonths = [];
    let currentDate = startDate.clone();

    while (currentDate.isSameOrBefore(endDate)) {
      let monthString = moment.monthsShort(currentDate.month()) + " '" + currentDate.format('YY');
      rangeOfMonths.push(monthString);
      currentDate.add(1, 'month');
    }
    this.keys[this.state.period] = rangeOfMonths;
  };

  render() {
    return (
      <ResponsiveContainer>
        <AreaChart data={this.createChartData(this.props.graphValue)}>
          <XAxis dataKey="name" />
          <YAxis stroke={this.props.strokeColor} />
          <Tooltip />
          <Area
            type="monotone"
            dataKey="Current"
            stroke={this.props.strokeColor}
            fill={this.props.fillColor}
            fillOpacity={1}
          />
          <Area
            type="monotone"
            dataKey="Previous"
            stroke={this.props.strokeColor}
            fill={this.props.fillColor}
            fillOpacity={1}
          />
        </AreaChart>
      </ResponsiveContainer>
    );
  }
}

TeamAnalyticsGraph.propTypes = {
  graphValue: PropTypes.any,
  strokeColor: PropTypes.string,
  fillColor: PropTypes.string,
  period: PropTypes.string,
  startDate: PropTypes.string,
  finishDate: PropTypes.string,
  graphRecord: PropTypes.func
};

export default TeamAnalyticsGraph;
