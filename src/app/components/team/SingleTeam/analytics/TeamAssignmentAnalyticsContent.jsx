import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import DateConverter from '../../../common/DateConverter';
import moment from 'moment';

function TeamAssignmentAnalyticsContent(props) {
  let totalAssignees =
    props.metric.assignedCount + props.metric.startedCount + props.metric.completedCount;
  let contentNameTooltip =
    (!!props.metric.contentName.trim() && props.metric.contentName) ||
    (!!props.metric.contentTitle.trim() && props.metric.contentTitle) ||
    (!!props.metric.contentMessage.trim() && props.metric.contentMessage);
  let teamNameTooltip = props.metric.team.name;
  let contentName =
    contentNameTooltip.length > 40
      ? contentNameTooltip.slice(0, 37).concat('...')
      : contentNameTooltip;
  let teamName =
    teamNameTooltip.length > 40 ? teamNameTooltip.slice(0, 37).concat('...') : teamNameTooltip;
  return (
    <div className="row card-list-row">
      <div className="small-12 medium-3 columns title-wrapper">
        <div
          title={contentNameTooltip}
          className="path-title assignment-header-row"
          onClick={props.redirectUrl.bind(this, props.metric)}
        >
          {contentName}
        </div>
        <div className="card-meta-data">
          <span className="meta-data-title">{tr('Assigner:')}</span>{' '}
          {props.metric.assignor.firstName} {props.metric.assignor.lastName}
          &nbsp;&nbsp;|&nbsp;&nbsp;<span className="meta-data-title">{tr('Type:')}</span>{' '}
          {tr(props.metric.cardType === 'pack' ? 'Pathway' : 'Card')}
        </div>
      </div>
      <div className="small-12 medium-1 columns cell-center-align">
        {props.metric.assignedAt &&
          moment
            .utc(props.metric.assignedAt)
            .local()
            .format('MM/DD/YYYY')}
        {!props.metric.assignedAt && <div>NA</div>}
      </div>
      <div className="small-12 medium-2 columns cell-center-align" style={{ fontWeight: '600' }}>
        {props.metric.assignor.firstName + ' ' + props.metric.assignor.lastName}
      </div>
      <div
        title={teamNameTooltip}
        className="small-12 medium-1 columns cell-center-align"
        style={{ fontWeight: '600' }}
      >
        {teamName}
      </div>
      <div className="small-12 medium-1 columns cell-center-align">
        <div>
          {props.metric.dueAt &&
            moment
              .utc(props.metric.dueAt)
              .local()
              .format('MM/DD/YYYY')}
          {!props.metric.dueAt && <div>NA</div>}
        </div>
      </div>
      <div
        className="small-12 medium-1 columns cell-center-align"
        onClick={e => {
          totalAssignees && props.openModal(props.metric, 'Total Assignees');
        }}
      >
        <a className="pointer count-link">{totalAssignees}</a>
      </div>
      <div
        className="small-12 medium-1 columns cell-center-align"
        onClick={e => {
          props.metric.assignedCount && props.openModal(props.metric, 'Not Started');
        }}
      >
        <a className="pointer count-link">{props.metric.assignedCount}</a>
      </div>
      <div
        className="small-12 medium-1 columns cell-center-align"
        onClick={e => {
          props.metric.startedCount && props.openModal(props.metric, 'Started');
        }}
      >
        <a className="pointer count-link">{props.metric.startedCount}</a>
      </div>
      <div
        className="small-12 medium-1 columns cell-center-align"
        onClick={e => {
          props.metric.completedCount && props.openModal(props.metric, 'Completed');
        }}
      >
        <a className="pointer count-link">{props.metric.completedCount}</a>
      </div>
    </div>
  );
}

TeamAssignmentAnalyticsContent.propTypes = {
  metric: PropTypes.any,
  redirectUrl: PropTypes.func,
  openInviteModal: PropTypes.func,
  openModal: PropTypes.any
};

export default TeamAssignmentAnalyticsContent;
