import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TeamAnalyticsGraph from './TeamAnalyticsGraph';
import Paper from 'edc-web-sdk/components/Paper';
import GraphHeaderRow from './GraphHeaderRow';
import Spinner from '../../../common/spinner';
import { tr } from 'edc-web-sdk/helpers/translations';

class TeamGraphStandalone extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      graphColor: '#dbd2ed',
      strokeColor: '#c5c5ca',
      loading: true
    };
  }

  render() {
    let THIS = this;
    return (
      <div
        className="standalone-graph-container graph-container row row-width column"
        onClick={this.props.onClick}
      >
        {this.props.createDownloadSection(
          [
            {
              title: this.props.graphTitle,
              subtitle: this.props.graphSubTitle,
              value: this.props.graphValue,
              startDate: this.props.startDate,
              finishDate: this.props.finishDate,
              period: this.props.period
            }
          ],
          this.props.graphTitle
        )}
        <Paper>
          <div className="container-padding">
            <div className="graph-title">{tr(this.props.graphTitle)}</div>
            <div className="graph-subtitle">{this.props.graphSubTitle}</div>
            <div className="graph">
              <TeamAnalyticsGraph
                strokeColor={this.state.strokeColor}
                fillColor={this.state.graphColor}
                graphValue={this.props.graphValue}
                startDate={this.props.startDate}
                finishDate={this.props.finishDate}
                period={this.props.period}
                graphRecord={graphRecord => {
                  if (graphRecord) {
                    if (THIS.state.loading) {
                      THIS.setState({ graphRecord, loading: false });
                    }
                  }
                }}
              />
            </div>

            <GraphHeaderRow />
            {this.state.loading && (
              <div className="small-12 medium-6 text-center">
                <Spinner />
              </div>
            )}
            {!this.state.loading &&
              this.state.graphRecord &&
              this.state.graphRecord.map((eachRecord, i) => {
                return (
                  <div key={i} className="row graph-body-row">
                    <div className="small-12 medium-3 text-center columns graph-body">
                      <div className="graph-record">{eachRecord.name}</div>
                    </div>
                    <div className="small-12 medium-3 text-center columns graph-body">
                      <div className="graph-record">{eachRecord.Current}</div>
                    </div>
                  </div>
                );
              })}
          </div>
        </Paper>
      </div>
    );
  }
}

TeamGraphStandalone.propTypes = {
  graphTitle: PropTypes.string,
  onClick: PropTypes.func,
  graphSubTitle: PropTypes.string,
  graphValue: PropTypes.string,
  startDate: PropTypes.string,
  finishDate: PropTypes.string,
  period: PropTypes.string,
  createDownloadSection: PropTypes.any
};

export default TeamGraphStandalone;
