import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import LinearProgress from 'material-ui/LinearProgress';
import Avatar from 'material-ui/Avatar';
import ListItem from 'material-ui/List/ListItem';
import { Axis } from 'react-axis';
import map from 'lodash/map';
import { tr } from 'edc-web-sdk/helpers/translations';

class TeamsLinearGraph extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: this.props.charLimit ? this.props.charLimit : 14
    };
    this.styles = {
      headerText: {
        fontSize: '16px',
        color: '#555',
        marginBottom: '6px'
      },
      content: {
        padding: '15px 30px 0px 10px',
        fontSize: '14px',
        fontWeight: 'normal',
        minHeight: '235px'
      },
      column1: {
        width: '20%',
        display: 'inline-block',
        wordWrap: 'break-word',
        fontSize: '12px',
        lineHeight: '15px',
        verticalAlign: 'middle',
        textAlign: 'right',
        color: '#797993',
        maxHeight: '30px',
        overflow: 'hidden'
      },
      column2: {
        width: '80%',
        display: 'inline-block',
        paddingLeft: '8px'
      },
      linearProgress: {
        backgroundColor: '#fff',
        height: '8px',
        borderRadius: 'unset !important'
      },
      scaleFixer: {
        width: '80%',
        display: 'inline-block',
        marginLeft: '0px',
        overflow: 'hidden'
      }
    };
  }

  render() {
    let type;
    switch (this.props.type) {
      case 'content':
        type = 'Top Content';
        break;
      case 'contributors':
        type = 'Top Contributors';
        break;
      default:
        break;
    }

    let viewStyle = this.props.viewStyle === 'group' ? 'group' : 'standalone';

    let graphStyles;
    if (this.props.viewStyle === 'group') {
      graphStyles = {
        width: 450,
        height: 30
      };
      this.styles.column2.width = 450;
    }
    if (this.props.viewStyle === 'standalone') {
      graphStyles = {
        width: 850,
        height: 30
      };
      this.styles.column2.width = 786;
    }

    let limitData = Math.max.apply(
      null,
      map(this.props.data, data => {
        return data.score;
      })
    );

    // To change the number to its nearest round off of 10,100,1000...
    let limitTester = Math.pow(10, (limitData + '').length);
    let limit =
      limitData % limitTester != 0
        ? limitTester - (limitData % limitTester) + limitData
        : limitData;

    return (
      <div>
        <div style={this.styles.headerText}>{tr(type)}</div>
        <Paper style={this.styles.content}>
          {this.props.data && this.props.data.length > 0 && (
            <div>
              <div style={this.styles.column1} />
              <div style={this.styles.scaleFixer}>
                {limit && (
                  <Axis
                    position="bottom"
                    tickSize={5}
                    max={limit}
                    min="0"
                    width={graphStyles.width}
                    height={graphStyles.height}
                    format="d"
                    standalone={true}
                    label=""
                    tickCount={10}
                  />
                )}
              </div>
              {this.props.data &&
                this.props.data.map(data => {
                  let titleData = data.title ? data.title : data.firstName + ' ' + data.lastName;
                  let title =
                    titleData.length >= this.state.limit
                      ? titleData.slice(0, this.state.limit) + ' ...'
                      : titleData;
                  let defaultImage = '/i/images/default_user_blue.png';
                  let image = data.photo ? data.photo : defaultImage;
                  return (
                    <div key={data.id} style={{ paddingBottom: '15px' }}>
                      <div style={this.styles.column1}>
                        <div style={{ display: 'inline-flex', width: '100%' }}>
                          {image && type === 'Top Contributors' && (
                            <div style={{ width: '40%', textAlign: 'right', marginRight: '15px' }}>
                              <Avatar src={image} size={30} />
                            </div>
                          )}
                          <div
                            style={{
                              width: '60%',
                              textAlign: 'left',
                              margin: 'auto',
                              overflow: 'hidden'
                            }}
                          >
                            <span title={titleData}>{title}</span>
                          </div>
                        </div>
                      </div>
                      <div style={this.styles.column2}>
                        <LinearProgress
                          color="#6f708b"
                          min={0}
                          max={limit}
                          style={this.styles.linearProgress}
                          value={data.score}
                          mode="determinate"
                        />
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
          {this.props.data && this.props.data.length == 0 && (
            <div style={{ textAlign: 'center' }}>
              <p>No content available</p>
            </div>
          )}
        </Paper>
      </div>
    );
  }
}

TeamsLinearGraph.propTypes = {
  data: PropTypes.object,
  charLimit: PropTypes.number,
  type: PropTypes.string,
  viewStyle: PropTypes.string
};

export default connect()(TeamsLinearGraph);
