import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import Workbook from 'react-excel-workbook';
import TeamAssignmentAnalyticsHeader from './TeamAssignmentAnalyticsHeader';
import TeamAssignmentAnalyticsContent from './TeamAssignmentAnalyticsContent';
import { push } from 'react-router-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import {
  getTeamAssignmentMetrics,
  getGroupDetail,
  getTeamAnalyticsUserList
} from '../../../../actions/groupAnalyticActions';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import {
  openGroupMemberModal,
  openInviteV2UserModal,
  openShowChannelsModal,
  openTeamCardsModal,
  openGroupInviteModal,
  openUpdateGroupDescriptionModal
} from '../../../../actions/modalActions';
import { CSVLink } from 'react-csv';
import Popover from 'material-ui/Popover';
import { List, ListItem } from 'material-ui/List';
import { groupsV2 } from 'edc-web-sdk/requests/index';

class TeamAssignmentAnalyticsContainer extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      container: {
        margin: '20px 40px'
      },
      paperStyle: {
        padding: '25px',
        minHeight: '400px'
      },
      titleStyle: {
        color: '#6f718a',
        marginBottom: '20px',
        fontSize: '25px',
        fontWeight: 600
      },
      noContentMessage: {
        fontSize: '13px',
        padding: '9em'
      },
      list: {
        padding: 0
      }
    };
    this.state = {
      loading: null,
      offset: 0,
      metricsForDownload: []
    };
  }

  componentDidMount = () => {
    let slug = this.props.routeParams.slug;

    this.props
      .dispatch(getGroupDetail(slug))
      .then(group => {
        let payload = {
          team_id: this.props.groupAnalytic.groupID,
          limit: this.props.groupAnalytic.assignmentLimit,
          offset: this.props.groupAnalytic.assignmentOffset
        };

        this.props
          .dispatch(getTeamAssignmentMetrics(payload))
          .then(() => {
            this.setState({ loading: false });
          })
          .catch(err => {
            console.error(
              `Error in TeamAssignmentAnalyticsContainer.getTeamAssignmentMetrics.func : ${err}`
            );
          });
      })
      .catch(err => {
        console.error(`Error in TeamAssignmentAnalyticsContainer.getGroupDetail.func : ${err}`);
      });
  };

  redirectUrl = metric => {
    if (metric.cardType === 'pack') {
      this.props.dispatch(push(`/pathways/${metric.cardId}`));
    } else if (metric.cardType === 'journey') {
      this.props.dispatch(push(`/journey/${metric.cardId}`));
    } else {
      this.props.dispatch(push(`/insights/${metric.cardId}`));
    }
  };

  onBackClick = () => {
    window.history.back();
  };

  viewMoreClickHandler = () => {
    this.setState({ loading: true });
    let offset =
      this.props.groupAnalytic.assignmentOffset + this.props.groupAnalytic.assignmentLimit;
    let payload = {
      team_id: this.props.groupAnalytic.groupID,
      limit: this.props.groupAnalytic.assignmentLimit,
      offset: offset
    };
    this.props
      .dispatch(getTeamAssignmentMetrics(payload))
      .then(() => {
        this.setState({ loading: false });
      })
      .catch(err => {
        console.error(
          `Error in TeamAssignmentAnalyticsContainer.viewMoreClickHandler.getTeamAssignmentMetrics.func : ${err}`
        );
      });
  };

  openModal = (metric, state) => {
    this.props.dispatch(getTeamAnalyticsUserList(metric, state));
  };

  openPopover = (isOpen, e) => {
    this.setState({
      isDownloadListOpen: isOpen,
      anchor: e.currentTarget
    });
  };

  downloadMetrics = async isExcel => {
    this.setState({
      isDownloading: true,
      isDownloadListOpen: false
    });
    let limit = 100;
    let offset = 0;
    let data = await groupsV2.getTeamAssessmentMetrics({
      limit,
      offset,
      team_id: this.props.groupAnalytic.groupID
    });
    let items = data && data.metrics && data.metrics.length ? data.metrics : [];
    let tmpDataItems = items;
    while (tmpDataItems && tmpDataItems.length) {
      offset = items.length;
      data = await groupsV2.getTeamAssessmentMetrics({
        limit,
        offset,
        team_id: this.props.groupAnalytic.groupID
      });
      tmpDataItems = data && data.metrics && data.metrics.length ? data.metrics : [];
      items = items.concat(tmpDataItems);
    }
    let metricsForDownload = items.map(item => ({
      'Content Name': item.contentName || item.contentTitle || item.contentMessage,
      'Assigned At': item.assignedAt || 'NA',
      'Assigned By': `${item.assignor.firstName} ${item.assignor.lastName}`,
      'Team Name': item.team.name,
      'Due At': item.dueAt || 'NA',
      'Total Assignees': item.assignedCount + item.startedCount + item.completedCount || '0',
      'Not Started': item.assignedCount || '0',
      Started: item.startedCount || '0',
      Completed: item.completedCount || '0'
    }));
    this.setState({ isDownloading: false, metricsForDownload }, () => {
      if (isExcel) {
        this._downloadBtnElement.click();
      } else {
        this._downloadCsvBtnElement.children[0].click();
      }
    });
  };

  render() {
    let assignment = this.props.groupAnalytic.assignmentMetrics;
    let showViewMore = assignment && assignment.length < this.props.groupAnalytic.assignmentTotal;
    let isEmpty = assignment && !!!assignment.length && typeof this.state.loading === 'boolean';
    return (
      <div className="team-analytics-base">
        <div className="row row-width">
          <div className="column small-6">
            <div aria-label="back" className="breadcrumbBack" onTouchTap={this.onBackClick}>
              <BackIcon style={{ width: '19px' }} color={'#454560'} />
            </div>
          </div>
        </div>
        <div className="assignment-analytics-container" style={this.styles.container}>
          <Paper style={this.styles.paperStyle}>
            <div style={this.styles.titleStyle}>
              {tr('Assignment Performance')}
              {this.props.groupAnalytic.assignmentMetrics &&
                !!this.props.groupAnalytic.assignmentMetrics.length && (
                  <PrimaryButton
                    className="pull-right download-button"
                    label={tr(this.state.isDownloading ? 'DOWNLOADING' : 'DOWNLOAD')}
                    onClick={this.openPopover.bind(this, true)}
                  />
                )}
              <Popover
                open={this.state.isDownloadListOpen}
                anchorEl={this.state.anchor}
                onRequestClose={this.openPopover.bind(this, false)}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                targetOrigin={{ horizontal: 'right', vertical: 'top' }}
              >
                <List style={this.styles.list}>
                  <ListItem
                    primaryText={<div>{tr('Excel Report')}</div>}
                    onClick={this.downloadMetrics.bind(this, true)}
                  />
                  <ListItem
                    primaryText={<div>{tr('CSV Report')}</div>}
                    onClick={this.downloadMetrics.bind(this, false)}
                  />
                </List>
              </Popover>
              <div className="hide-checking" ref={input => (this._downloadCsvBtnElement = input)}>
                <CSVLink
                  data={this.state.metricsForDownload}
                  target="_self"
                  filename={'Assignment_Performance.csv'}
                >
                  download csv
                </CSVLink>
              </div>
              <Workbook
                filename="Assignment_Performance.xlsx"
                element={
                  <button
                    className="download-hidden-btn"
                    ref={input => (this._downloadBtnElement = input)}
                  />
                }
              >
                <Workbook.Sheet data={this.state.metricsForDownload} name="Assignment_Performance">
                  {this.state.metricsForDownload.length &&
                    Object.keys(this.state.metricsForDownload[0]).map(key => (
                      <Workbook.Column key={key} label={key} value={key} />
                    ))}
                </Workbook.Sheet>
              </Workbook>
            </div>
            {assignment && !!assignment.length && <TeamAssignmentAnalyticsHeader />}
            <div style={{ marginTop: '20px' }}>
              {this.props.groupAnalytic.assignmentMetrics &&
                !!this.props.groupAnalytic.assignmentMetrics.length &&
                this.props.groupAnalytic.assignmentMetrics.map(metric => {
                  return (
                    <TeamAssignmentAnalyticsContent
                      key={metric.id}
                      redirectUrl={this.redirectUrl}
                      metric={metric}
                      openModal={this.openModal}
                    />
                  );
                })}
              {isEmpty && (
                <div
                  style={this.styles.noContentMessage}
                  className="container-padding row align-center"
                >
                  {tr('There are currently no assignments at this time.')}
                </div>
              )}
            </div>
            <div className="row">
              <div className="small-12 columns text-center">
                {showViewMore && (
                  <PrimaryButton
                    label={this.state.loading ? tr('Loading...') : tr('View More')}
                    className="view-more"
                    disabled={this.state.loading}
                    onTouchTap={this.viewMoreClickHandler.bind(this)}
                  />
                )}
              </div>
            </div>
            <br />
          </Paper>
        </div>
      </div>
    );
  }
}

TeamAssignmentAnalyticsContainer.propTypes = {
  groupAnalytic: PropTypes.any,
  routeParams: PropTypes.any
};

export default connect(state => ({
  groupAnalytic: state.groupAnalytic.toJS()
}))(TeamAssignmentAnalyticsContainer);
