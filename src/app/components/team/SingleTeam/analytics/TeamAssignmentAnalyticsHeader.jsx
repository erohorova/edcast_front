import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

function TeamAssignmentAnalyticsHeader() {
  return (
    <div className="row assignment-header-row">
      <div className="small-12 medium-3 columns assignment-header">
        <div>{tr('Content Name')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Assigned Date')}</div>
      </div>
      <div className="small-12 medium-2 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Assigned By')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Team Name')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Due At')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Total Assignees')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Not Started')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Started')}</div>
      </div>
      <div className="small-12 medium-1 columns assignment-header" style={{ textAlign: 'center' }}>
        <div>{tr('Completed')}</div>
      </div>
    </div>
  );
}

export default TeamAssignmentAnalyticsHeader;
