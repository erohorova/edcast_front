import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import GroupDetails from './GroupDetails';
import { tr } from 'edc-web-sdk/helpers/translations';
import TeamCardsCarousel from './TeamCardsCarousel';
import Carousel from '../../common/Carousel';
import EmptyBlock from '../../discovery/EmptyBlock.jsx';
import Channel from '../../discovery/Channel.jsx';
import {
  getGroupDetail,
  getTeamCards,
  getTeamChannels,
  deleteCard
} from '../../../actions/groupsActionsV2';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import { openShowChannelsModal } from '../../../actions/modalActions';
import RightRail from '../../home/RightRail';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import ChatIcon from 'edc-web-sdk/components/icons/ChatIcon';

import throttle from 'lodash/throttle';
import find from 'lodash/find';
import each from 'lodash/each';
import IconButton from 'material-ui/IconButton/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';
import AddChannelToGroupModal from '../../modals/AddChannelToGroupModal';
import { snackBarOpenClose } from '../../../actions/channelsActionsV2';

class GroupPageContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      rowPadding: {
        padding: '0 51px'
      },
      withTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 340px)'
      },
      withNewTeemFeed: {
        padding: '0 24px 0 38px',
        maxWidth: 'calc(100% - 360px)'
      },
      rowTeemFeed: {
        padding: 0,
        margin: 0
      },
      padTeemFeed: {
        padding: 0
      },
      row: {
        maxWidth: '100%'
      },
      homeUI: {
        padding: 0
      },
      addLink: {
        textDecoration: 'underline',
        color: '#4990e2',
        marginRight: '0.5rem'
      },
      editIcon: {
        width: '21px',
        height: '21px',
        verticalAlign: 'bottom'
      },
      editChannelsIcon: {
        padding: '0',
        marginLeft: '0.3125rem',
        width: '1.3125rem',
        height: '1.3125rem'
      }
    };
    this.state = {
      hideTeamFeed: localStorage.getItem('hideTeamFeed') === 'true',
      isMenuOpen: false,
      groupID: null,
      offsetFeed: 0,
      footerOffset: 0,
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      topNavVersion: window.ldclient.variation('topnav-version', 'V2'),
      newFeedStyle: window.ldclient.variation('feed-style-v2', false),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      openAddChannelModal: false,
      windowWidth: 1200
    };
    this.collapseWidth = 1100;
  }

  updatePosition = () => {
    let windowInnerWidth = window.innerWidth;
    let windowWidth =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let position = calculateTeemFeedPosition();
    let fixedHeader = document.querySelector('#secondary-nav.fixed');
    let doc = document.documentElement || document.body;
    let navClass;
    let defaultSize;
    let isMenuOpen;
    if (this.state.topNavVersion === 'V2') {
      navClass = 'topnav-main-container-v2';
      defaultSize = 170;
    } else {
      navClass = 'topnav-main-container-v3';
      defaultSize = 124;
    }
    let offsetFeed = fixedHeader
      ? fixedHeader.offsetHeight
      : document.getElementsByClassName(navClass)[0]
      ? document.getElementsByClassName(navClass)[0].offsetHeight - doc.scrollTop
      : defaultSize - doc.scrollTop;
    offsetFeed = `${offsetFeed + 23}px`;
    if (windowWidth > this.collapseWidth) {
      isMenuOpen = false;
    }
    this.setState({
      offsetFeed,
      windowInnerWidth,
      windowWidth,
      isMenuOpen,
      footerOffset: position.footerOffset
    });
  };

  handleScroll = throttle(
    () => {
      setTimeout(() => {
        this.updatePosition();
      }, 1);
    },
    150,
    { leading: false }
  );

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePosition);
    window.removeEventListener('scroll', this.handleScroll);
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);

    let slug = this.props.params && this.props.params.slug ? this.props.params.slug : '';
    let _this = this;
    this.props
      .dispatch(getGroupDetail(slug))
      .then(groupDetail => {
        this.updatePosition();
        let groupID = groupDetail.id;
        let group = this.props.groupsV2[groupID];
        _this.props.dispatch(
          getTeamCards(
            groupID,
            'teamAssignments',
            { limit: group.cardsLimit, offset: 0, type: 'assigned' },
            this.state.multilingualFiltering
          )
        );
        _this.props.dispatch(
          getTeamCards(
            groupID,
            'sharedCards',
            { limit: group.cardsLimit, offset: 0, type: 'shared' },
            this.state.multilingualFiltering
          )
        );
        _this.props.dispatch(
          getTeamChannels(groupID, { limit: group.teamChannelsLimit, offset: 0 })
        );
        _this.setState({ groupID });
      })
      .catch(err => {
        this.props.dispatch(
          snackBarOpenClose('You are not authorized to access this group.', 5000)
        );
        this.props.dispatch(push(`/org-groups`));
        console.error(`Error in GroupPageContainer.getGroupDetail.func : ${err}`);
      });
    window.addEventListener('resize', this.updatePosition);
    window.addEventListener('scroll', this.handleScroll);
    setTimeout(() => {
      this.handleScroll();
    }, 10);
  }

  componentWillReceiveProps(nextProps) {
    let group = nextProps.groupsV2[this.state.groupID];
    each(nextProps.cards, (card, cardID) => {
      if (group && card.dismissed) {
        let assignmentCard = find(group.teamAssignments, { id: cardID });

        let sharedCard = find(group.sharedCards, { id: cardID });
        if (assignmentCard) {
          this.props.dispatch(deleteCard(cardID, 'teamAssignments'));
        }

        if (sharedCard) {
          this.props.dispatch(deleteCard(cardID, 'sharedCards'));
        }
      }
    });
  }

  channelComp = channel => {
    return (
      <Channel
        channel={channel}
        id={channel.id}
        isFollowing={channel.isFollowing}
        title={channel.label}
        imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
        slug={channel.slug}
        allowFollow={channel.allowFollow}
      />
    );
  };

  handleViewAllClick = (e, removable) => {
    e.preventDefault();
    this.props.dispatch(openShowChannelsModal(removable));
  };

  teamFeedTabEnable = obj => {
    if (obj) {
      let teamFeed = obj['web/leftRail/teamActivity'];
      return teamFeed && teamFeed.visible !== false;
    } else {
      return false;
    }
  };

  toggleAddingChannelModal = e => {
    e && e.preventDefault();
    this.setState(prevState => ({ openAddChannelModal: !prevState.openAddChannelModal }));
  };

  updateChannelsList = async () => {
    this.props.dispatch(
      getTeamChannels(this.state.groupID, {
        limit: this.props.groupsV2[this.state.groupID].teamChannelsLimit,
        offset: 0
      })
    );
  };

  toggleBlock = () => {
    if (this.state.windowWidth <= this.collapseWidth || !this.state.newFeedStyle) {
      this.setState(prevState => ({
        isMenuOpen: !prevState.isMenuOpen
      }));
    } else {
      this.toggleTeamFeed();
    }
  };

  toggleTeamFeed = () => {
    if (this.state.windowWidth <= this.collapseWidth) {
      this.toggleBlock();
    } else {
      localStorage.setItem('hideTeamFeed', `${!this.state.hideTeamFeed}`);
      this.setState(prevState => {
        return {
          hideTeamFeed: !prevState.hideTeamFeed
        };
      });
    }
  };

  render() {
    let groupID = this.state.groupID;
    let group = groupID && this.props.groupsV2[groupID + ''];
    let sharedCards = groupID && group.sharedCards;
    let sharedCardsCount = groupID && group.sharedCardsCount;
    let teamAssignments = groupID && group.teamAssignments;
    let teamAssignmentsCount = groupID && group.teamAssignmentsCount;
    let teamChannels = groupID && group.teamChannels;
    let teamChannelsCount = groupID && group.teamChannelsCount;
    let isRail =
      this.teamFeedTabEnable(this.props.team.OrgConfig.leftRail) && this.state.newTeamActivity;
    let isNewRail =
      this.teamFeedTabEnable(this.props.team.OrgConfig.leftRail) &&
      this.state.newTeamActivity &&
      this.state.newFeedStyle;

    return (
      <div className="channel-base">
        <div className="row" style={this.styles.row}>
          <div
            className={`medium-12 team-detail-page${groupID && isRail ? '_with-teem-feed' : ''} ${
              this.state.newFeedStyle ? 'team-detail-page_with-big-team-feed' : ''
            } ${
              this.state.isMenuOpen &&
              this.state.newFeedStyle &&
              this.state.windowWidth < this.collapseWidth
                ? 'team-detail-page_active-team-feed'
                : ''
            }
          ${
            this.state.hideTeamFeed &&
            this.state.newFeedStyle &&
            this.state.windowWidth >= this.collapseWidth
              ? 'team-detail-page_hide-team-feed'
              : ''
          }`}
            style={
              groupID && isNewRail
                ? this.styles.withNewTeemFeed
                : isRail
                ? this.styles.withTeemFeed
                : this.styles.rowPadding
            }
          >
            {groupID && <GroupDetails isTeamFeed={isRail} groupInfo={group} />}

            {groupID && teamAssignmentsCount == 0 && (
              <div className="row" style={this.styles.rowTeemFeed}>
                <div
                  className="small-12 columns empty-state"
                  style={groupID && isRail ? this.styles.padTeemFeed : {}}
                >
                  <div className="small-12 columns group-title">
                    <div>{tr('Assigned')}</div>
                  </div>
                  <EmptyBlock className="content" title={tr('Cards assigned to the group')} />
                </div>
              </div>
            )}

            {groupID && teamAssignmentsCount > 0 && (
              <TeamCardsCarousel
                isTeamFeed={isRail}
                description={'Assigned'}
                items={teamAssignments}
                count={teamAssignmentsCount}
                editable={false}
                windowInnerWidth={this.state.windowInnerWidth}
              />
            )}
            {groupID && sharedCardsCount == 0 && (
              <div className="small-12 columns empty-state" style={this.styles.padTeemFeed}>
                <div className="small-12 columns group-title">
                  <div>{tr('Shared')}</div>
                </div>
                <EmptyBlock className="content" title={tr('Share a card with the group')} />
              </div>
            )}

            {groupID && sharedCardsCount > 0 && (
              <TeamCardsCarousel
                isTeamFeed={isRail}
                description={'Shared'}
                items={sharedCards}
                deleteSharedCard={group.isTeamAdmin}
                count={sharedCardsCount}
                editable={false}
                windowInnerWidth={this.state.windowInnerWidth}
              />
            )}

            {groupID && teamChannelsCount == 0 && (
              <div className="small-12 columns empty-state" style={this.styles.padTeemFeed}>
                <div className="row group-title">
                  <div className="small-6 columns">
                    <div>{tr('Channels')}</div>
                  </div>
                  <div className="small-6 columns text-right">
                    {(group.isTeamAdmin || group.isTeamSubAdmin) && (
                      <a
                        href="#"
                        onClick={this.toggleAddingChannelModal}
                        style={this.styles.addLink}
                      >
                        {tr('+ Add Channel')}
                      </a>
                    )}
                  </div>
                </div>
                <EmptyBlock className="content" title="No channels followed" />
              </div>
            )}
            {groupID && teamChannelsCount > 0 && (
              <div className="small-12 columns">
                <div className="row group-title">
                  <div className="small-6 columns">
                    <div className="itemLabel">
                      {tr('Channels')} ({teamChannelsCount})
                      {(group.isTeamAdmin || group.isTeamSubAdmin) && (
                        <IconButton
                          aria-label="edit"
                          onTouchTap={e => this.handleViewAllClick(e, true)}
                          iconStyle={this.styles.editIcon}
                          style={this.styles.editChannelsIcon}
                        >
                          <EditIcon color="#6f708b" />
                        </IconButton>
                      )}
                    </div>
                  </div>
                  <div className="small-6 columns text-right">
                    {(group.isTeamAdmin || group.isTeamSubAdmin) && (
                      <a
                        href="#"
                        onClick={this.toggleAddingChannelModal}
                        style={this.styles.addLink}
                      >
                        {tr('+ Add Channel')}
                      </a>
                    )}
                    <a
                      href="#"
                      onClick={e => this.handleViewAllClick(e, false)}
                      style={{ textDecoration: 'underline' }}
                    >
                      {tr('View All')}
                    </a>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 columns channel-card-wrapper">
                    <div className="channel-card-wrapper-inner">
                      <Carousel slidesToShow={5}>
                        {teamChannels.map(channel => {
                          return <div key={channel.id}>{this.channelComp(channel)}</div>;
                        })}
                      </Carousel>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
          {groupID && isRail && (
            <div
              id="right-rail-container"
              className={`home-new-ui ${
                this.state.newFeedStyle ? 'feed-page_new-style' : 'feed-page_old-style'
              } ${
                this.state.hideTeamFeed &&
                this.state.newFeedStyle &&
                this.state.windowWidth >= this.collapseWidth
                  ? 'hide-team-feed'
                  : ''
              }`}
              style={this.styles.homeUI}
            >
              <div
                className={`group-page left-rail right-rail ${
                  this.state.isMenuOpen && this.state.windowWidth < this.collapseWidth
                    ? 'active-menu-container'
                    : ''
                }`}
                style={{
                  top: this.state.offsetFeed,
                  height: `calc(100% - ${this.state.offsetFeed} - ${this.state.footerOffset})`
                }}
              >
                {this.state.newFeedStyle && this.state.hideTeamFeed && (
                  <div className={`right-rail__mobile-btn rail__mobile-btn`}>
                    <IconButton
                      onClick={this.toggleBlock}
                      className={`right-rail__mobile-btn rail__mobile-btn`}
                    >
                      <ChatIcon />
                    </IconButton>
                  </div>
                )}
                <RightRail
                  newFeedStyle={this.state.newFeedStyle}
                  justTeamFeed={true}
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                  newTeamActivity={true}
                  currentUser={this.props.currentUser}
                  currentGroup={group}
                  toggleTeamFeed={this.toggleTeamFeed}
                />
              </div>
            </div>
          )}
        </div>
        {this.state.openAddChannelModal && (
          <AddChannelToGroupModal
            title={'Add Channel to Group'}
            message={'Do you want to delete the comment?'}
            closeModal={this.toggleAddingChannelModal}
            groupID={groupID}
            updateChannelsList={this.updateChannelsList}
          />
        )}
      </div>
    );
  }
}

GroupPageContainer.propTypes = {
  groupsV2: PropTypes.object,
  cards: PropTypes.any,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  params: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    groupsV2: state.groupsV2.toJS(),
    cards: state.cards.toJS(),
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupPageContainer);
