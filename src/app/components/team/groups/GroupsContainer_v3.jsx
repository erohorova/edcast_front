import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import { replace, push } from 'react-router-redux';

import Checkbox from 'material-ui/Checkbox';
import ListItem from 'material-ui/List/ListItem';

import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import { getListV2, searchGroups } from 'edc-web-sdk/requests/groups.v2';
import Paper from 'edc-web-sdk/components/Paper';

import Spinner from '../../common/spinner';
import { updateChannels } from '../../../actions/channelsActions';
import { openGroupCreationModal } from '../../../actions/modalActions';
import resizeCards from '../../../utils/resizeCards';
import { Permissions } from '../../../utils/checkPermissions';
import GroupItem from './GroupItem';
import { getGroupsForOrg } from 'edc-web-sdk/requests/teams';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';

class GroupsContainer_v3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      limit: 20,
      filterType:
        props.routeParams && props.routeParams.filter
          ? props.routeParams.filter
          : props.isMeTab
          ? 'my'
          : 'all',
      filterVersion: 'v1',
      groups: [],
      isLastPage: false,
      offset: 0,
      isShowSuggestions: false,
      suggestions: [],
      membersExtended: 5,
      leadersExtended: 5,
      adminsExtended: 5
    };

    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.125rem',
        verticalAlign: 'middle',
        width: '1.2rem',
        height: '1.5rem',
        right: '0.4375rem',
        fill: colors.silverSand
      },
      checkboxInner: {
        width: '0.8125rem',
        height: '0.8125rem',
        marginRight: '0.25rem'
      },
      checkboxInnerLabel: {
        lineHeight: '2.17',
        fontSize: '0.75rem',
        fontWeight: '300',
        color: '#6f708b',
        marginTop: '1px'
      },
      checkboxOuter: {
        width: '1.1875rem',
        height: '1.1875rem',
        marginRight: '0.5rem',
        marginBottom: '0.25rem'
      },
      checkboxOuterLabel: {
        padding: '0.375rem 0',
        lineHeight: '1.4'
      },
      checkboxIconStyle: {
        width: '1.1875rem',
        height: '1.1875rem'
      },
      uncheckedIcon: {
        width: '1.3125rem',
        height: '1.3125rem'
      },
      uncheckedIcon_inner: {
        width: '0.8125rem',
        height: '0.8125rem'
      }
    };

    const commonFilters = [
      { value: 'my', text: 'My groups' },
      { value: 'pending', text: 'My pending requests' }
    ];

    const standaloneFiltersOnly = [{ value: 'all', text: 'All groups' }];
    this.filterOptions = props.isMeTab
      ? commonFilters
      : [...standaloneFiltersOnly, ...commonFilters];

    this.searchQuery = {
      limit: 20,
      offset: 0
    };
    this.isSearching = props.location.search && props.location.query;
    this.gettingGroupsCounter = 0;
  }

  async componentDidMount() {
    if (this.isSearching) {
      this.searchQuery.q = this.props.location.query.q === '*' ? '' : this.props.location.query.q;
      this._inputFilter.value = this.searchQuery.q;
      if (this.props.routeParams.filter === 'pending') {
        this.searchQuery.current_user_pending_teams = true;
        delete this.searchQuery.current_user_teams;
      }
      if (this.props.routeParams.filter === 'my') {
        this.searchQuery.current_user_teams = true;
        delete this.searchQuery.current_user_pending_teams;
      }
      if (this.props.routeParams.filter === 'search') {
        this.searchQuery.skip_aggs = false;
        delete this.searchQuery.current_user_teams;
        delete this.searchQuery.current_user_pending_teams;
      }
      this.groupsSearch();
    } else {
      this.getData();
    }
    let userInfoCallBack = await getSpecificUserInfo(
      ['followingChannels', 'rolesDefaultNames', 'roles', 'writableChannels'],
      this.props.currentUser.toJS()
    );
    this.props.dispatch(userInfoCallBack);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps &&
      nextProps.routeParams &&
      this.props.routeParams &&
      (nextProps.routeParams.filter !== this.props.routeParams.filter ||
        nextProps.location.pathname !== this.props.location.pathname) &&
      !(nextProps.location.search && nextProps.location.query && nextProps.location.query.q)
    ) {
      this.setState(
        {
          filterType: nextProps.routeParams.filter
            ? nextProps.routeParams.filter
            : this.props.isMeTab
            ? 'my'
            : 'all',
          groups: [],
          isLastPage: false,
          offset: 0,
          filterVersion: 'v1'
        },
        () => {
          this.getData();
        }
      );
    }
    this.isSearching = nextProps.location.search && nextProps.location.query;
    if (this.isSearching && nextProps.location.query.q !== this.props.location.query.q) {
      this.searchQuery.q = nextProps.location.query.q === '*' ? '' : nextProps.location.query.q;
      this._inputFilter.value = this.searchQuery.q;
      if (nextProps.routeParams.filter) {
        if (nextProps.routeParams.filter === 'pending') {
          this.searchQuery.current_user_pending_teams = true;
          delete this.searchQuery.current_user_teams;
        }
        if (nextProps.routeParams.filter === 'my') {
          this.searchQuery.current_user_teams = true;
          delete this.searchQuery.current_user_pending_teams;
        }
        if (nextProps.routeParams.filter === 'search') {
          this.searchQuery.skip_aggs = false;
          delete this.searchQuery.current_user_teams;
          delete this.searchQuery.current_user_pending_teams;
        }
      }
      this.groupsSearch();
    }
  }

  getData = isScroll => {
    this.setState({
      pending: true,
      groups: isScroll === true ? this.state.groups : []
    });

    let payload = {
      limit: this.state.limit,
      offset: isScroll ? this.state.offset + this.state.limit : 0
    };

    if (!this.props.routeParams.filter || this.props.routeParams.filter === 'all') {
      payload.all = true;
    }
    if (this.props.isMeTab || this.props.routeParams.filter === 'my') {
      delete payload.all;
      delete payload.pending;
    }
    if (this.props.routeParams.filter === 'pending') {
      payload.pending = true;
      delete payload.all;
    }
    if (this.props.routeParams.filter === 'search') {
      delete payload.pending;
      payload.skip_aggs = false;
      this.groupsSearch(isScroll);
    } else {
      getListV2(payload)
        .then(data => {
          if (data && data.teams) {
            let uniqGroups = _.uniqBy(
              isScroll ? this.state.groups.concat(data.teams) : data.teams,
              'id'
            );
            let isLastPage = data.total < payload.offset;
            this.setState({
              groups: uniqGroups,
              pending: false,
              isLastPage,
              offset: payload.offset
            });
          }
        })
        .catch(err => {
          console.error(`Error in GroupsContainerv2.getList.func : ${err}`);
        });
    }
  };

  groupsSearch = isScroll => {
    this.setState({
      pending: true,
      groups: isScroll === true ? this.state.groups : [],
      filterVersion: 'v2'
    });
    this.searchQuery.offset = isScroll ? this.searchQuery.offset + this.searchQuery.limit : 0;
    searchGroups(this.searchQuery)
      .then(data => {
        let uniqGroups = _.uniqBy(
          isScroll ? this.state.groups.concat(data.teams) : data.teams,
          'id'
        );
        let isLastPage = data.teams.length < this.searchQuery.limit;
        let offset = this.searchQuery.offset + data.teams.length;
        this.setState({
          groups: uniqGroups,
          pending: false,
          isLastPage,
          offset,
          admins:
            (data.aggs &&
              data.aggs.admins &&
              data.aggs.admins.filtered &&
              data.aggs.admins.filtered.values &&
              data.aggs.admins.filtered.values.buckets) ||
            [],
          leaders:
            (data.aggs &&
              data.aggs.leaders &&
              data.aggs.leaders.filtered &&
              data.aggs.leaders.filtered.values &&
              data.aggs.leaders.filtered.values.buckets) ||
            [],
          members:
            (data.aggs &&
              data.aggs.members &&
              data.aggs.members.filtered &&
              data.aggs.members.filtered.values &&
              data.aggs.members.filtered.values.buckets) ||
            []
        });
      })
      .catch(err => {
        console.error(`Error in GroupsContainerv2.searchGroups.func : ${err}`);
      });
  };

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = _.throttle(
    () => {
      if (this.state.pending || this.state.isLastPage) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          if (this.isSearching) {
            this.groupsSearch(true);
          } else {
            this.getData(true);
          }
        }
      }
    },
    150,
    { leading: false }
  );

  handleFilterChange = value => {
    if (this.state.filterType !== value) {
      this.props.dispatch(
        replace(this.props.isMeTab ? `/me/groups/${value}` : `/org-groups/${value}`)
      );
    }
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      this.filterSearch();
    }
  };

  filterSearch = () => {
    let val = this._inputFilter.value.trim();
    this.setState({ searchText: val, filterVersion: 'v2' });
    this.props.dispatch(replace(`/org-groups/search?q=${encodeURIComponent(val)}`));
  };

  handleChangeType = (e, isInputChecked, type, name) => {
    if (type === 'group_type') {
      this.searchQuery.is_private =
        (name === 'private' && isInputChecked) ||
        (name === 'open' && !isInputChecked && this._private.state.switched);
      if (
        (isInputChecked &&
          ((name === 'open' && this._private.state.switched) ||
            (name === 'private' && this._open.state.switched))) ||
        (!isInputChecked &&
          ((name === 'open' && !this._private.state.switched) ||
            (name === 'private' && !this._open.state.switched)))
      ) {
        delete this.searchQuery.is_private;
      }
    }
    this.groupsSearch();
  };

  handleChangePerson = (e, isInputChecked, role, name) => {
    let queryArray = this.searchQuery[`${role}_names[]`];
    if (queryArray && queryArray.length) {
      if (isInputChecked) {
        queryArray.push(name);
      } else {
        let index = queryArray.findIndex(item => item === name);
        queryArray.splice(index, 1);
      }
    } else {
      queryArray = [name];
    }
    this.searchQuery[`${role}_names[]`] = queryArray;
    if (!queryArray.length) {
      delete this.searchQuery[`${role}_names[]`];
    }
    this.groupsSearch();
  };

  handleOpenGroupCreation = () => {
    this.props.dispatch(
      openGroupCreationModal(group => {
        let groups = this.state.groups;
        groups.unshift(group);
        this.setState({ groups });
      })
    );
  };

  showAll = type => {
    this.setState({
      [`${type}Extended`]: this.state[`${type}Extended`] + 10
    });
  };

  removeGroupFromArray = index => {
    if (this.state.filterType !== 'all') {
      let groups = this.state.groups;
      groups.splice(index, 1);
      this.setState({ groups });
    }
  };

  toggleShowingSuggestions = (e, clearSuggestions) => {
    this.setState(
      prevState => {
        let newState = { isShowSuggestions: !prevState.isShowSuggestions };
        if (clearSuggestions) newState = { ...newState, ...{ suggestions: [] } };
        return newState;
      },
      () => {
        if (this.state.isShowSuggestions) {
          document.body.addEventListener('click', this.hideSuggsContainer);
        } else {
          document.body.removeEventListener('click', this.hideSuggsContainer);
        }
      }
    );
  };

  hideSuggsContainer = e => {
    if (
      e &&
      e.target &&
      e.target.className &&
      (e.target.className === 'suggestion-item' || e.target.className === 'search-channels-input')
    )
      return;
    this.toggleShowingSuggestions();
  };

  getSuggestions = _.debounce(
    () => {
      let requestNumber = ++this.gettingGroupsCounter;
      let currentSearchText = this._inputFilter.value && this._inputFilter.value.trim();
      if (!currentSearchText) {
        return this.setState({
          isShowSuggestions: false,
          suggestions: []
        });
      }
      searchGroups({ q: currentSearchText })
        .then(data => {
          if (data && data.teams && data.teams.length > 0) {
            this.setState({
              suggestions: data.teams,
              isShowSuggestions: !!data.teams.length
            });
          }
        })
        .catch(err => {
          console.log(`Error in GroupsContainer_v3.searchGroups.fun: ${err}`);
        });
    },
    100,
    { leading: false, trailing: true }
  );

  chooseSuggestion = group => {
    this._inputFilter.value = group && group.name;
    this.toggleShowingSuggestions(null, true);
    this.filterSearch();
  };

  goBack = () => {
    if (this.props.location.pathname === '/org-groups/search') {
      this.props.dispatch(replace('/org-groups'));
    } else {
      window.history.back();
    }
  };

  render() {
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let shouldShowGroup = labels && labels['web/labels/create_group'];

    let adminsKeys = (this.state.admins && this.state.admins.map(el => el.key)) || [];
    let filteredAdmins =
      (this.searchQuery[`admins_names[]`] &&
        this.searchQuery[`admins_names[]`].filter(el => !~adminsKeys.indexOf(el))) ||
      [];

    let leadersKeys = (this.state.leaders && this.state.leaders.map(el => el.key)) || [];
    let filteredLeaders =
      (this.searchQuery[`leaders_names[]`] &&
        this.searchQuery[`leaders_names[]`].filter(el => !~leadersKeys.indexOf(el))) ||
      [];

    let membersKeys = (this.state.members && this.state.members.map(el => el.key)) || [];
    let filteredMembers =
      (this.searchQuery[`members_names[]`] &&
        this.searchQuery[`members_names[]`].filter(el => !~membersKeys.indexOf(el))) ||
      [];

    const isMeTab = this.props.isMeTab || false;

    return (
      <div id="groups" className="channelsv2 home-new-ui lbv2_new">
        <div className="lbv2__title channelv2__title">
          <div>
            {!isMeTab && (
              <div
                aria-label="back"
                className="breadcrumbBack breadcrumbBack_v2"
                onClick={this.goBack}
              >
                <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
              </div>
            )}
            {!isMeTab && <span>{tr(this.isSearching ? 'Groups search' : 'Groups')}</span>}
          </div>
          {!isMeTab && (
            <div className="my-team__search-bar">
              <div className="my-team__search-input-block">
                <input
                  placeholder={tr('Search')}
                  onFocus={this.toggleShowingSuggestions}
                  className="search-channels-input"
                  onChange={this.getSuggestions}
                  onKeyDown={this.handleFilterSearch}
                  type="text"
                  ref={node => (this._inputFilter = node)}
                />
                <SearchIcon style={this.styles.searchIcon} onClick={this.filterSearch} />
                {this.state.isShowSuggestions &&
                  this.state.suggestions &&
                  !!this.state.suggestions.length && (
                    <div className="channels-suggestions-container">
                      <ul>
                        {this.state.suggestions.map(suggestion => (
                          <li
                            key={`suggestion-${suggestion.id}`}
                            className="suggestion-item"
                            onClick={this.chooseSuggestion.bind(this, suggestion)}
                          >
                            {suggestion.name}
                          </li>
                        ))}
                      </ul>
                    </div>
                  )}
              </div>
              {shouldShowGroup && shouldShowGroup.visible && Permissions.has('CREATE_GROUP') && (
                <PrimaryButton onClick={this.handleOpenGroupCreation} label={tr('Create Group')} />
              )}
            </div>
          )}
        </div>

        <div className="my-team__container">
          <div
            className={`my-team__filter-block ${
              this.state.filterVersion === 'v2' ? 'my-team__filter-block_wide' : ''
            }`}
          >
            {this.state.filterVersion === 'v1' ? (
              this.filterOptions.map((opt, index) => {
                return (
                  <div
                    className={`my-team__filter-item ${
                      this.state.filterType === opt.value ? 'my-team__filter-item_active' : ''
                    }`}
                    key={`filter_${index}`}
                    onClick={() => this.handleFilterChange(opt.value)}
                  >
                    {tr(opt.text)}
                  </div>
                );
              })
            ) : (
              <div>
                <div className="filter-block">
                  <div className="filter-title">{tr('Type')}</div>
                  <div className="filter-content">
                    <div className="checkbox-label-alignment">
                      <Checkbox
                        label={`${tr('Open')}`}
                        ref={node => (this._open = node)}
                        checkedIcon={<CheckOn1 color="#6f708b" />}
                        uncheckedIcon={
                          <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                        }
                        iconStyle={this.styles.checkboxOuter}
                        labelStyle={this.styles.checkboxOuterLabel}
                        onCheck={(event, isInputChecked) =>
                          this.handleChangeType(event, isInputChecked, 'group_type', 'open')
                        }
                        value={'open'}
                      />
                    </div>
                    <div className="checkbox-label-alignment">
                      <Checkbox
                        label={`${tr('Private')}`}
                        ref={node => (this._private = node)}
                        checkedIcon={<CheckOn1 color="#6f708b" />}
                        uncheckedIcon={
                          <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                        }
                        iconStyle={this.styles.checkboxOuter}
                        labelStyle={this.styles.checkboxOuterLabel}
                        onCheck={(event, isInputChecked) =>
                          this.handleChangeType(event, isInputChecked, 'group_type', 'private')
                        }
                        value={'private'}
                      />
                    </div>
                  </div>
                </div>
                {(!!filteredAdmins.length || (this.state.admins && !!this.state.admins.length)) && (
                  <div className="filter-block">
                    <div className="filter-title">{tr('Admin')}</div>
                    <div className="filter-content">
                      <div>
                        {this.state.admins
                          .slice(
                            0,
                            this.state.admins.length > this.state.adminsExtended
                              ? this.state.adminsExtended
                              : this.state.admins.length
                          )
                          .map(item => (
                            <div className="checkbox-label-alignment">
                              <Checkbox
                                label={`${item.key} (${item.doc_count})`}
                                checkedIcon={<CheckOn1 color="#6f708b" />}
                                uncheckedIcon={
                                  <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                }
                                iconStyle={this.styles.checkboxOuter}
                                labelStyle={this.styles.checkboxOuterLabel}
                                onCheck={(event, isInputChecked) =>
                                  this.handleChangePerson(event, isInputChecked, 'admins', item.key)
                                }
                                value={item.key}
                                checked={
                                  this.searchQuery[`admins_names[]`] &&
                                  ~this.searchQuery[`admins_names[]`].findIndex(
                                    name => name === item.key
                                  )
                                }
                              />
                            </div>
                          ))}
                        {filteredAdmins.map((admin, index) => (
                          <div className="checkbox-label-alignment">
                            <Checkbox
                              label={tr(`${admin}(0)`)}
                              checkedIcon={<CheckOn1 color="#6f708b" />}
                              uncheckedIcon={
                                <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                              }
                              iconStyle={this.styles.checkboxOuter}
                              labelStyle={this.styles.checkboxOuterLabel}
                              onCheck={(event, isInputChecked) =>
                                this.handleChangePerson(event, isInputChecked, 'admins', admin)
                              }
                              value={admin}
                              checked={true}
                            />
                          </div>
                        ))}
                      </div>
                      {this.state.admins.length > this.state.adminsExtended && (
                        <div className="view-more" onClick={this.showAll.bind(this, 'admins')}>
                          {tr('View more')}
                        </div>
                      )}
                    </div>
                  </div>
                )}
                {(!!filteredLeaders.length ||
                  (this.state.leaders && !!this.state.leaders.length)) && (
                  <div className="filter-block">
                    <div className="filter-title">{tr('Leader')}</div>
                    <div className="filter-content">
                      <div>
                        {this.state.leaders
                          .slice(
                            0,
                            this.state.leaders.length > this.state.leadersExtended
                              ? this.state.leadersExtended
                              : this.state.leaders.length
                          )
                          .map(item => (
                            <div className="checkbox-label-alignment">
                              <Checkbox
                                label={`${item.key} (${item.doc_count})`}
                                checkedIcon={<CheckOn1 color="#6f708b" />}
                                uncheckedIcon={
                                  <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                }
                                iconStyle={this.styles.checkboxOuter}
                                labelStyle={this.styles.checkboxOuterLabel}
                                onCheck={(event, isInputChecked) =>
                                  this.handleChangePerson(
                                    event,
                                    isInputChecked,
                                    'leaders',
                                    item.key
                                  )
                                }
                                value={item.key}
                                checked={
                                  this.searchQuery[`leaders_names[]`] &&
                                  ~this.searchQuery[`leaders_names[]`].findIndex(
                                    name => name === item.key
                                  )
                                }
                              />
                            </div>
                          ))}
                        {filteredLeaders.map((leader, index) => (
                          <div className="checkbox-label-alignment">
                            <Checkbox
                              label={tr(`${leader}(0)`)}
                              checkedIcon={<CheckOn1 color="#6f708b" />}
                              uncheckedIcon={
                                <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                              }
                              iconStyle={this.styles.checkboxOuter}
                              labelStyle={this.styles.checkboxOuterLabel}
                              onCheck={(event, isInputChecked) =>
                                this.handleChangePerson(event, isInputChecked, 'leaders', leader)
                              }
                              value={leader}
                              checked={true}
                            />
                          </div>
                        ))}
                      </div>
                      {this.state.leaders.length > this.state.leadersExtended && (
                        <div className="view-more" onClick={this.showAll.bind(this, 'leaders')}>
                          {tr('View more')}
                        </div>
                      )}
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
          <div
            className={
              this.state.filterVersion === 'v2' ? 'my-content-wrapper' : 'my-content-wrapper-v1'
            }
          >
            <div className="channel-container">
              {!!this.state.groups.length &&
                this.state.groups.map((group, index) => {
                  return (
                    <GroupItem
                      key={`group-item-${group.id}`}
                      index={index}
                      removeGroupFromArray={this.removeGroupFromArray}
                      group={group}
                    />
                  );
                })}
              {!this.state.pending && !this.state.groups.length && (
                <div className="row">
                  <Paper className="vertical-spacing-large container-padding">
                    {tr(`No groups found.`)}
                  </Paper>
                </div>
              )}
            </div>
            {this.state.pending && (
              <div className="text-center pending-cards">
                <Spinner />
              </div>
            )}

            {isMeTab && this.state.isLastPage && (
              <p className="explore-msg-channel-group-user text-center">
                <a
                  onClick={() => {
                    this.props.dispatch(push('/org-groups'));
                  }}
                >
                  <strong>Search</strong>
                </a>{' '}
                other group to explore and join
              </p>
            )}
          </div>
        </div>
      </div>
    );
  }
}

GroupsContainer_v3.propTypes = {
  team: PropTypes.object,
  location: PropTypes.object,
  routeParams: PropTypes.any,
  isMeTab: PropTypes.bool,
  currentUser: PropTypes.object
};

export default connect(state => ({
  team: state.team.toJS(),
  currentUser: state.currentUser
}))(GroupsContainer_v3);
