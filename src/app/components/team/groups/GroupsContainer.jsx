/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getList } from 'edc-web-sdk/requests/groups.v2';
import ListItem from 'material-ui/List/ListItem';
import Paper from 'edc-web-sdk/components/Paper';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Spinner from '../../common/spinner';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { Permissions } from '../../../utils/checkPermissions';
import { openGroupCreationModal } from '../../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class GroupsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      groups: [],
      pending: true,
      limit: 20,
      isLastPage: true
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreGroups = this.showMoreGroups.bind(this);
  }

  componentDidMount() {
    getList(this.state.limit, 0)
      .then(groups => {
        let isLastPage = groups.length < this.state.limit;
        this.setState({ groups, pending: false, isLastPage: isLastPage });
      })
      .catch(err => {
        console.error(`Error in GroupsContainer.getList.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreGroups();
        }
      }
    },
    150,
    { leading: false }
  );

  handleOpenGroupCreation = () => {
    this.props.dispatch(openGroupCreationModal());
  };

  showMoreGroups = () => {
    let offset = this.state.groups.length;
    this.setState({ pending: true });
    getList(this.state.limit, offset)
      .then(groups => {
        let isLastPage = groups.length < this.state.limit;
        this.setState({
          groups: uniq(this.state.groups.concat(groups), 'id'),
          pending: false,
          isLastPage
        });
      })
      .catch(err => {
        console.error(`Error in GroupsContainer.showMoreGroups.getList.func : ${err}`);
      });
  };

  render() {
    let labels = this.props.team.OrgConfig.labels;
    let shouldShowGroup = labels && labels['web/labels/create_group'];
    return (
      <div className="row content" id="groups">
        <div className="columns expand vertical-spacing-large">
          {shouldShowGroup &&
            shouldShowGroup.visible &&
            (Permissions.has('MANAGE_CARD') || Permissions.has('CREATE_GROUP')) && (
              <div className="text-right">
                <SecondaryButton
                  onTouchTap={this.handleOpenGroupCreation}
                  label={tr('CREATE GROUP')}
                />
              </div>
            )}
          {this.state.groups.map((group, index) => {
            return (
              <Paper key={index}>
                <ListItem
                  disabled
                  primaryText={
                    <a className="matte" href={`/teams/${group.slug}`}>
                      <strong>{group.name}</strong>
                    </a>
                  }
                  secondaryText={
                    <p>
                      <span>
                        <strong>{tr('members')}</strong> <small>{group.membersCount || 0}</small>
                      </span>
                      <br />
                      <span>{group.description}</span>
                    </p>
                  }
                  secondaryTextLines={2}
                  innerDivStyle={{ paddingLeft: '8.125rem' }}
                  leftIcon={
                    <a href={`/teams/${group.slug}`}>
                      <div
                        aria-label={tr(`Visit the group, ${group.name}`)}
                        className="banner"
                        style={{
                          width: '6.25rem',
                          height: '4.0625rem',
                          backgroundImage: `url(\'${group.imageUrls.medium}\')`
                        }}
                      />
                    </a>
                  }
                  rightIconButton={
                    <div className="role" style={{ top: '1.75rem', right: '1rem' }}>
                      <Chip>
                        {group.role && <Avatar>{group.role.substr(0, 1).toUpperCase()}</Avatar>}
                        {group.role}
                      </Chip>
                    </div>
                  }
                />
              </Paper>
            );
          })}
          {this.state.pending && (
            <div className="text-center" style={{ marginTop: '5rem' }}>
              <Spinner />
            </div>
          )}
        </div>
      </div>
    );
  }
}

GroupsContainer.propTypes = {
  team: PropTypes.object,
  routeParams: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupsContainer);
