import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import ListItem from 'material-ui/List/ListItem';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import {
  leaveFromGroup,
  joinToGroup,
  declineInviteToGroup,
  acceptInviteToGroup
} from 'edc-web-sdk/requests/groups.v2';
import { confirmationPrivateCardModal } from '../../../actions/modalActions';

class GroupItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      group: this.props.group
    };

    this.styles = {
      actionBtnLabel: {
        textTransform: 'none'
      },
      title: {
        fontSize: '1rem',
        fontWeight: 600,
        color: '#26273b',
        textTransform: 'capitalize',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden'
      },
      description: {
        marginTop: '0.625rem',
        height: 49,
        fontSize: '0.875rem',
        fontWeight: 300,
        color: '#26273b',
        whiteSpace: 'normal',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        width: 199
      },
      members: {
        fontSize: '0.75rem',
        color: '#acadc1',
        textTransform: 'uppercase',
        marginTop: '0.8125rem'
      },
      listItem: {
        padding: '0.6875rem 0.75rem 2.5625rem'
      },
      bodyText: {
        height: 'auto',
        margin: '0'
      },
      groupImgSvg: {
        zIndex: 2,
        position: 'relative'
      }
    };
  }

  componentDidMount() {}

  moveToGroup(slug, isInGroup) {
    if (
      this.state.group.isPrivate &&
      !isInGroup &&
      this.props.currentUser &&
      !this.props.currentUser.isAdmin
    ) {
      return;
    }
    this.props.dispatch(push(`/teams/${slug}`));
  }

  joinToGroup = e => {
    e.stopPropagation();
    joinToGroup(this.state.group.id)
      .then(data => {
        let group = this.state.group;
        group.isMember = true;
        this.setState({ group });
      })
      .catch(err => {
        console.error(`Error in GroupItem.groups.v2.joinToGroup : ${err}`);
      });
  };

  groupLeaveConfirmation = () => {
    leaveFromGroup(this.state.group.id)
      .then(data => {
        let group = this.state.group;
        group.isMember = false;
        group.isTeamAdmin = false;
        group.isTeamSubAdmin = false;
        this.setState({ group });
        this.props.removeGroupFromArray(this.props.index);
      })
      .catch(err => {
        console.error(`Error in GroupItem.groups.v2.groupLeaveConfirmation : ${err}`);
      });
  };

  leaveGroup = e => {
    e.stopPropagation();
    if (this.state.group && this.state.group.isPrivate) {
      this.props.dispatch(
        confirmationPrivateCardModal(
          tr('Leave Group'),
          tr('You won’t be able to access this group, do you really want to leave ?'),
          false,
          () => {
            this.groupLeaveConfirmation();
          }
        )
      );
    } else {
      this.groupLeaveConfirmation();
    }
  };

  acceptInvitationGroup = e => {
    e.stopPropagation();
    acceptInviteToGroup(this.state.group.id)
      .then(data => {
        this.props.removeGroupFromArray(this.props.index);
        this.props.dispatch(push(`/teams/${this.state.group.slug}`));
      })
      .catch(err => {
        console.error(`Error in GroupItem.groups.v2.acceptInviteToGroup : ${err}`);
      });
  };

  declineInvitationToGroup = e => {
    e.stopPropagation();
    declineInviteToGroup(this.state.group.id)
      .then(data => {
        if (data && data.id) {
          this.setState({ group: data });
        }
        this.props.removeGroupFromArray(this.props.index);
      })
      .catch(err => {
        console.error(`Error in GroupItem.groups.v2.declineInviteToGroup : ${err}`);
      });
  };

  render() {
    let svgStyle = { filter: `url(#blur-effect-card-${this.state.group.id})` };
    let isInGroup =
      this.state.group.isMember || this.state.group.isTeamAdmin || this.state.group.isTeamSubAdmin;
    let displayLeaveButton = !this.state.group.isMandatory;
    let groupHoverText =
      this.state.group && this.state.group.isPrivate
        ? 'This group is private. You should accept invitation to open group detail page.'
        : 'This group is open, you can access it to explore and join.';

    return (
      <div className="card-v2 channel-v2 group-v2 tooltip tooltip_bottom-center">
        <div className="channel-v2__body paper-card">
          <div
            className="card-img-container"
            onClick={this.moveToGroup.bind(this, this.props.group.slug, isInGroup)}
          >
            <div className="card-img button-icon card-blurred-background">
              <svg width="100%" height="100%">
                <image
                  style={svgStyle}
                  xlinkHref={this.state.group.imageUrls.medium}
                  x="-30%"
                  y="-30%"
                  width="160%"
                  height="160%"
                />
                <filter id={`blur-effect-card-${this.state.group.id}`}>
                  <feGaussianBlur stdDeviation="10" />
                </filter>
              </svg>
            </div>
            <svg width="100%" height="100%" style={this.styles.groupImgSvg}>
              <image xlinkHref={this.state.group.imageUrls.medium} width="100%" height="100%" />
            </svg>
          </div>
          <ListItem
            style={this.styles.listItem}
            disabled
            primaryText={
              <div
                onClick={this.moveToGroup.bind(this, this.props.group.slug, isInGroup)}
                className="matte group-name"
                style={this.styles.title}
              >
                {this.props.group.name}
              </div>
            }
            secondaryText={
              <div style={this.styles.bodyText}>
                <div onClick={this.moveToGroup.bind(this, this.props.group.slug, isInGroup)}>
                  <div style={this.styles.description}>{this.state.group.description}</div>
                  <div style={this.styles.members}>{`${tr('members')} ${this.state.group
                    .membersCount || 0}`}</div>
                </div>
                <div className="button-block text-right">
                  {!isInGroup && !this.state.group.isPrivate && !this.state.group.isPending && (
                    <PrimaryButton
                      label={tr('Join')}
                      onTouchTap={e => this.joinToGroup(e)}
                      labelStyle={this.styles.actionBtnLabel}
                    />
                  )}
                  {this.state.group.isPending && (
                    <span>
                      {<span className="tooltiptext">{tr(groupHoverText)}</span>}
                      <PrimaryButton
                        label={tr('Accept')}
                        onTouchTap={e => this.acceptInvitationGroup(e)}
                        labelStyle={this.styles.actionBtnLabel}
                      />
                      <PrimaryButton
                        label={tr('Decline')}
                        onTouchTap={e => this.declineInvitationToGroup(e)}
                        labelStyle={this.styles.actionBtnLabel}
                      />
                    </span>
                  )}
                  {isInGroup && !this.state.group.isEveryoneTeam && displayLeaveButton && (
                    <PrimaryButton
                      label={tr('Leave')}
                      onTouchTap={e => this.leaveGroup(e)}
                      labelStyle={this.styles.actionBtnLabel}
                    />
                  )}
                </div>
              </div>
            }
          />
        </div>
      </div>
    );
  }
}

GroupItem.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  group: PropTypes.object,
  index: PropTypes.number,
  removeGroupFromArray: PropTypes.func
};

export default connect(state => ({
  team: state.team.toJS(),
  currentUser: state.currentUser.toJS()
}))(GroupItem);
