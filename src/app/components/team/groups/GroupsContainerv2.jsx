/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import uniqBy from 'lodash/uniqBy';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

import { getList } from 'edc-web-sdk/requests/groups.v2';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Spinner from '../../common/spinner';
import ListItem from 'material-ui/List/ListItem';
//import CircularProgress from 'material-ui/CircularProgress';
import { openGroupCreationModal } from '../../../actions/modalActions';
import resizeCards from '../../../utils/resizeCards';
import { Permissions } from '../../../utils/checkPermissions';

class GroupsContainerv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      groups: [],
      pending: true,
      limit: 20,
      isLastPage: true,
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false)
    };
    this.styles = {
      bodyText: {
        height: 'auto',
        margin: '0'
      },
      listItem: {
        padding: '11px 12px 41px'
      },
      title: {
        fontSize: '16px',
        fontWeight: 600,
        color: '#26273b',
        textTransform: 'capitalize',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden'
      },
      description: {
        marginTop: '10px',
        height: 49,
        fontSize: '14px',
        fontWeight: 300,
        color: '#26273b',
        whiteSpace: 'normal',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        width: 199
      },
      members: {
        fontSize: '12px',
        color: '#acadc1',
        textTransform: 'uppercase',
        marginTop: '13px'
      },
      groupImgSvg: {
        zIndex: 2,
        position: 'relative'
      }
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreGroups = this.showMoreGroups.bind(this);
  }

  componentDidMount() {
    getList(this.state.limit, 0)
      .then(groups => {
        let uniqGroups = uniqBy(groups, 'id');
        let isLastPage = uniqGroups.length < this.state.limit;
        this.setState({ groups: uniqGroups, pending: false, isLastPage: isLastPage });
      })
      .catch(err => {
        console.error(`Error in GroupsContainerv2.getList.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    () => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreGroups();
        }
      }
    },
    150,
    { leading: false }
  );

  showMoreGroups = () => {
    let offset = this.state.groups.length;
    this.setState({ pending: true });
    getList(this.state.limit, offset)
      .then(groups => {
        let isLastPage = groups.length < this.state.limit;
        this.setState({
          groups: uniq(this.state.groups.concat(groups), 'id'),
          pending: false,
          isLastPage
        });
      })
      .catch(err => {
        console.error(`Error in GroupsContainerv2.showMoreGroups.getList.func : ${err}`);
      });
  };

  handleOpenGroupCreation = () => {
    this.props.dispatch(
      openGroupCreationModal(group => {
        let groups = this.state.groups;
        groups.unshift(group);
        this.setState({ groups });
      })
    );
  };

  moveToGroup = (e, slug) => {
    e.preventDefault();
    this.props.dispatch(push(`/teams/${slug}`));
  };

  render() {
    let labels = this.props.team.OrgConfig.labels;
    let shouldShowGroup = labels && labels['web/labels/create_group'];
    return (
      <div id="groups" className={`home-new-ui ${this.state.isNewProfileNavigation && 'lbv2_new'}`}>
        {shouldShowGroup && shouldShowGroup.visible && Permissions.has('CREATE_GROUP') && (
          <div className="text-right" style={{ marginBottom: '10px' }}>
            <SecondaryButton
              onTouchTap={this.handleOpenGroupCreation}
              label={tr('Create Group')}
              style={{ borderRadius: '3px' }}
            />
          </div>
        )}
        {this.state.isNewProfileNavigation && (
          <div className="lbv2__title">
            <div
              aria-label="back"
              role="heading"
              aria-level="2"
              className="breadcrumbBack"
              onClick={() => {
                window.history.back();
              }}
            >
              <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
            </div>
            {tr('My Groups')}
          </div>
        )}
        <div className="my-content-wrapper feed-without-rail">
          {!!this.state.groups.length && (
            <div className="custom-card-container">
              <div className="five-card-column">
                {this.state.groups.map((group, index) => {
                  let svgStyle = { filter: `url(#blur-effect-card-${group.id})` };
                  return (
                    <div key={index} className="card-v2 small-12 column more-cards">
                      <a
                        aria-label={`Visit the group, ${group.name}`}
                        href="#"
                        onClick={e => this.moveToGroup(e, group.slug)}
                      >
                        <Paper className="paper-card">
                          <div className="card-img-container">
                            <div className="card-img button-icon card-blurred-background">
                              <svg width="100%" height="100%">
                                <title>{`Visit the group, ${group.name}`}</title>
                                <image
                                  style={svgStyle}
                                  xlinkHref={group.imageUrls.medium}
                                  x="-30%"
                                  y="-30%"
                                  width="160%"
                                  height="160%"
                                />
                                <filter id={`blur-effect-card-${group.id}`}>
                                  <feGaussianBlur stdDeviation="10" />
                                </filter>
                              </svg>
                            </div>
                            <svg width="100%" height="100%" style={this.styles.groupImgSvg}>
                              <image
                                xlinkHref={group.imageUrls.medium}
                                width="100%"
                                height="100%"
                              />
                            </svg>
                          </div>
                          <ListItem
                            style={this.styles.listItem}
                            disabled
                            primaryText={
                              <div className="matte group-name" style={this.styles.title}>
                                {group.name}
                              </div>
                            }
                            secondaryText={
                              <div style={this.styles.bodyText}>
                                <div style={this.styles.description}>{group.description}</div>
                                <div style={this.styles.members}>{`${tr(
                                  'members'
                                )} ${group.membersCount || 0}`}</div>
                              </div>
                            }
                          />
                        </Paper>
                      </a>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
          {this.state.pending && (
            <div className="text-center pending-cards">
              <Spinner />
            </div>
          )}
        </div>
      </div>
    );
  }
}

GroupsContainerv2.propTypes = {
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupsContainerv2);
