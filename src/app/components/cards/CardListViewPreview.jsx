import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import find from 'lodash/find';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';

import LockIcon from 'edc-web-sdk/components/icons/Lock';
import colors from 'edc-web-sdk/components/colors/index';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import { CardHeader } from 'material-ui/Card';

import MarkdownRenderer from '../common/MarkdownRenderer';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});
import CreationDate from '../common/CreationDate';
import SvgImageResized from '../common/ImageResized';
import convertRichText from '../../utils/convertRichText';
import BlurImage from '../common/BlurImage';
import Poll from '../feed/Poll';
import * as logoType from '../../constants/logoTypes';
import getCardParams from '../../utils/getCardParams';

let LocaleCurrency = require('locale-currency');

const logoObj = logoType.LOGO;

class CardListViewPreview extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      card: props.card,
      defaultImage: '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg',
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false)
    };

    this.styles = {
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      userAvatar: {
        marginRight: 0,
        display: 'inline-block'
      },
      cardHeader: {
        padding: 0
      },
      svgImage: {
        zIndex: 2
      },
      titleStyle: {
        padding: 0
      },
      midLockIcon: {
        width: '40px',
        height: '40px'
      },
      lockedCard: {
        background: 'rgba(255, 255, 255, 0.5) no-repeat',
        backgroundSize: '100% 100%'
      },
      audio: {
        width: '100%'
      },
      avatarBox: {
        height: '1.625rem',
        width: '1.625rem',
        marginRight: '0.5625rem',
        position: 'relative'
      },
      privateLabel: {
        color: colors.primary
      }
    };
    this.lockPathwayCardFlag = window.ldclient.variation('lock-pathway-card', false);
  }

  componentWillReceiveProps(nextProp) {
    this.setState({
      card: nextProp.card
    });
  }

  getPricingPlans() {
    return this.state.edcastPlansForPricing &&
      this.props.card.cardMetadatum &&
      this.props.card.cardMetadatum.plan
      ? tr(startCase(toLower(this.props.card.cardMetadatum.plan)))
      : tr('Free');
  }

  render() {
    let card = this.props.card;
    let params = getCardParams(card, this.props, 'cardListViewPreview') || {};
    let svgStyle = {
      filter: `url(#blur-effect-preview-${card.id})`
    };
    let contentStyle = {
      width: params.isShowTopImage
        ? card.author
          ? 'calc(100% - 288px)'
          : 'calc(100% - 210px)'
        : 'calc(100% - 151px)'
    };
    let authorStyle = card.author ? {} : { minHeight: '72px', lineHeight: '50px' };
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = {};
    let skillcoin_image = '/i/images/skillcoin-new.png';

    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: 'SKILLCOIN' }) ||
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }
    if (priceData && priceData.currency == 'SKILLCOIN') {
      priceData.symbol = <img className="pricing_skill_coins_label_icon" src={skillcoin_image} />;
    }
    return (this.lockPathwayCardFlag &&
      (this.props.card.isLocked || this.props.card.showLocked) &&
      !this.props.isPathwayOwner) ||
      this.props.isPrivate ? (
      <div className="list-block-preview">
        <div className="list-main-card">
          <div className="locked-list-card">
            {this.props.isPrivate ? (
              <h6 style={this.styles.privateLabel}>
                {tr('You do not have permission to view this card')}
              </h6>
            ) : (
              <LockIcon style={this.styles.midLockIcon} color="#7c7d94" />
            )}
          </div>
          <div className="card-index">
            <span>{this.props.index}</span>
          </div>
        </div>
      </div>
    ) : (
      <div className="list-block-preview">
        <div className="list-main-card">
          {this.lockPathwayCardFlag &&
            (this.props.card.isLocked || this.props.card.showLocked) &&
            this.props.isPathwayOwner && (
              <div className="locked-list-card" style={this.styles.lockedCard}>
                <LockIcon style={this.styles.midLockIcon} color="#7c7d94" />
              </div>
            )}
          <div className="card-index">
            <span>{this.props.index}</span>
          </div>
          {params.isShowTopImage && (
            <div className="card-img-container">
              <div className="card-blurred-background">
                <svg width="100%" height="100%">
                  <title>{convertRichText(card.message)}</title>
                  <SvgImageResized
                    cardId={`${card.id}`}
                    style={svgStyle}
                    resizeOptions={'height:99'}
                    xlinkHref={params.cardSvgBackground}
                    x="-30%"
                    y="-30%"
                    width="160%"
                    height="160%"
                  />
                  <filter id={`blur-effect-preview-${card.id}`}>
                    <feGaussianBlur stdDeviation="10" />
                  </filter>
                </svg>
              </div>
              {params.videoFileStack ? (
                <div className="fp fp_video fp_video_card">
                  <video
                    src={card.filestack[0].url}
                    poster={params.poster}
                    preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                    controlsList={params.isDownloadContentDisabled ? 'nodownload' : ''}
                  />
                </div>
              ) : (
                <svg width="100%" height="100%" style={this.styles.mainSvg}>
                  <title>{convertRichText(card.message)}</title>
                  <SvgImageResized
                    cardId={`${card.id}`}
                    resizeOptions={'height:99'}
                    xlinkHref={params.poster || params.cardSvgBackground}
                    width="100%"
                    style={this.styles.svgImage}
                    height="100%"
                  />
                </svg>
              )}
              {(params.cardType === 'VIDEO' || card.cardSubtype === 'video') && (
                <div>
                  <PlayIcon className="center-play-icon" color={colors.white} />
                </div>
              )}
            </div>
          )}
          <div className="author-block" style={authorStyle}>
            {card.author && (
              <CardHeader
                className="card-header-preview"
                title={
                  <div className="author-info-preview">
                    {params.showCreator && (
                      <a
                        className="user-name"
                        onTouchTap={this.props.linkToPush.bind(null, null, card.author.handle)}
                      >
                        {card.author.fullName
                          ? card.author.fullName
                          : `${card.author.firstName ? card.author.firstName : ''} ${
                              card.author.lastName ? card.author.lastName : ''
                            }`}
                      </a>
                    )}
                    {params.showCreator && <br />}
                    <CreationDate
                      card={card}
                      standaloneLinkClickHandler={this.props.linkToPush.bind(
                        null,
                        this.props.card,
                        null
                      )}
                    />
                  </div>
                }
                subtitle={
                  card.publishedAt && (
                    <div className="header-secondary-text">
                      <span className="matte" />
                    </div>
                  )
                }
                avatar={
                  params.showCreator ? (
                    <a
                      style={this.styles.userAvatar}
                      onTouchTap={this.props.linkToPush.bind(null, null, card.author.handle)}
                    >
                      <BlurImage
                        style={this.styles.avatarBox}
                        id={card.id}
                        image={
                          card.author &&
                          (card.author.picture ||
                            (card.author.avatarimages && card.author.avatarimages.small) ||
                            params.defaultUserImage)
                        }
                      />
                    </a>
                  ) : null
                }
                style={this.styles.cardHeader}
                titleStyle={this.styles.titleStyle}
              />
            )}
            <div className="logo-type">
              {(card.eclSourceLogoUrl ||
                (card.eclSourceTypeName && logoObj[card.eclSourceTypeName] !== undefined)) && (
                <img
                  className="provider-logo"
                  src={card.eclSourceLogoUrl || logoObj[card.eclSourceTypeName]}
                />
              )}
              {params.iconFileSrc && <img className="provider-logo " src={params.iconFileSrc} />}
              <span className="label-text">
                {tr((card.readableCardType || params.cardType).toUpperCase())}
              </span>
              {this.state.edcastPricing && !params.isHideSpecialInfo && (
                <span className="card-pricing">
                  {tr('Price')} :{' '}
                  {this.props.card.isPaid && priceData && priceData.amount ? (
                    <span>
                      {priceData.symbol}
                      {priceData.amount}
                    </span>
                  ) : (
                    this.getPricingPlans()
                  )}
                </span>
              )}
            </div>
          </div>
          <div style={contentStyle}>
            {((params.cardType === 'POLL' || params.cardType === 'QUIZ') && (
              <div className="card-message poll-message">
                <div className="card-message-poll">{params.message}</div>
                <div className="poll-content">
                  <Poll
                    card={this.state.card}
                    pathwayModal={true}
                    cardUpdated={this.props.cardUpdated}
                  />
                </div>
              </div>
            )) ||
              (params.audioFileStack ? (
                <audio controls src={card.filestack[0].url} style={this.styles.audio} />
              ) : (
                <div className="card-message markdown-text">
                  <div className="markdown-container">
                    <RichTextReadOnly
                      text={convertRichText(
                        params.message.length > 160
                          ? params.message.substr(0, 160) + '...'
                          : params.message
                      )}
                    />
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

CardListViewPreview.propTypes = {
  card: PropTypes.object,
  cardUpdated: PropTypes.func,
  linkToPush: PropTypes.func,
  isPathwayOwner: PropTypes.bool,
  isPrivate: PropTypes.bool,
  index: PropTypes.number,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardListViewPreview);
