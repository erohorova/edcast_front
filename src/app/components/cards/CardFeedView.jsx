import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import find from 'lodash/find';
import moment from 'moment';

import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import LockIcon from 'edc-web-sdk/components/icons/Lock';

import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import CheckedIcon from 'material-ui/svg-icons/action/done';

import MarkdownRenderer from '../common/MarkdownRenderer';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});
import SmartBiteCardHeader from '../common/SmartBiteCardHeader';
import SmartBiteCardFooter from '../common/SmartBiteCardFooter';
import DateConverter from '../common/DateConverter';
import SvgImageResized from '../common/ImageResized';

import getDefaultImage from '../../utils/getDefaultCardImage';
import convertRichText from '../../utils/convertRichText';

import VideoStream from '../feed/VideoStream';
import Poll from '../feed/Poll';

import CardModal from '../modals/CardModal';
import CardClassesModal from '../modals/CardClassesModal';
import Spinner from '../common/spinner';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';

class CardFeedView extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      videoPlaying: false,
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      poster: this.props.params.poster,
      cardImage: this.props.params.cardImage,
      cardSvgBackground: this.props.params.cardSvgBackground,
      scormStateMsg: this.setScormStateMsg(this.props.card)
    };

    this.styles = {
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      checkedIcon: {
        width: 70,
        height: '100%'
      },
      videoPlaying: {
        display: 'none'
      },
      playIcon: {
        height: '3.125rem',
        width: '3.125rem'
      },
      bigLockIcon: {
        width: '4.375rem',
        height: '4.375rem'
      },
      lockedCard: {
        background: 'rgba(255, 255, 255, 0.5) no-repeat',
        backgroundSize: '100% 100%'
      },
      audio: {
        width: '100%'
      },
      privateLabel: {
        color: colors.primary
      },
      videoBox: {
        width: '100%',
        height: '100%'
      }
    };
    this.lockPathwayCardFlag = window.ldclient.variation('lock-pathway-card', false);
    this.isCsodCourse = props.card.course_id && props.card.source_type_name === 'csod';
  }

  componentDidMount() {
    setTimeout(() => {
      if (document.querySelector('.list-view-block .ml-video-cards')) {
        let videoEle = document.querySelectorAll('.list-view-block .ml-video-cards');
        this.bindElements(videoEle);
      }
    }, 200);
  }

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  bindElements = videoEle => {
    for (let i = 0; i < videoEle.length; i++) {
      videoEle[i].onplay = () => {
        this.props.reloadVideo(videoEle[i], videoEle);
        this.props.getAlliframeElements('feedView');
      };
      videoEle[i].onclick = () => {
        this.props.reloadVideo(videoEle[i], videoEle);
        this.props.getAlliframeElements('feedView');
      };
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.props.card.dismissed) {
      return null;
    }

    let { isPartOfPathway } = this.props;

    let card = this.props.card;
    let params = this.props.params;
    let scormCard = this.props.scormCard;
    let priceData = this.props.priceData;
    let isNameCut = this.props.isNameCut;
    let authorName = this.props.authorName;
    let disableTopics = this.props.disableTopics;
    let isCompleted = this.props.isCompleted;
    let svgStyle = this.props.svgStyle;
    let isShowLeap = this.props.isShowLeap;
    let currency = this.props.currency;
    let transcodedVideoStatus = this.props.transcodedVideoStatus;
    let hideActions = this.props.hideActions;
    let logoObj = this.props.logoObj;
    let averageRating = card.averageRating;
    let cardMsg = convertRichText(this.props.card.title || this.props.card.message);
    let isShowLeftFile = params.isShowTopImage || params.fileFileStack;
    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }
    let endDate =
      card.end_date &&
      moment
        .utc(card.end_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');
    let startDate =
      card.start_date &&
      moment
        .utc(card.start_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');

    return (this.lockPathwayCardFlag &&
      !!this.props.pathwayDetails &&
      (this.props.card.isLocked || this.props.card.showLocked) &&
      !params.isPathwayOwner &&
      !this.props.isShowLockedCardContent) ||
      this.props.isPrivate ? (
      <div className="list-view-block">
        <Paper
          className={`feed-card-view ${this.props.fullView ? 'full-view' : ''} ${
            this.props.pathwayView ? 'pathway-view' : ''
          }`}
        >
          <div className="locked-list-card">
            {this.props.isPrivate ? (
              <h4 style={this.styles.privateLabel}>
                {tr('You do not have permission to view this card')}
              </h4>
            ) : (
              <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
            )}
          </div>
          {this.props.showControls && (
            <div className="number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}
        </Paper>
      </div>
    ) : (
      <div
        className="list-view-block"
        onClick={() => {
          if (!this.isCsodCourse) return;
          this.props.deeplink();
        }}
      >
        <Paper
          className={`feed-card-view ${this.props.fullView ? 'full-view' : ''} ${
            this.props.pathwayView ? 'pathway-view' : ''
          }`}
        >
          <div
            onClick={this.props.handleCardClicked}
            className={
              this.props.pathwayChecking &&
              (!params.isShowTopImage || (!params.isFileAttached && params.audioFileStack)) &&
              params.cardType !== 'FILE' &&
              isCompleted
                ? 'checking-over'
                : 'hide-checking'
            }
          >
            <CheckedIcon color={colors.primary} style={this.styles.checkedIcon} />
          </div>
          <div className={scormCard && card.hidden ? 'scorm-not-uploaded-list' : 'hide-checking'}>
            <div className="scorm-not-uploaded-text">{tr('Course is getting uploaded')}</div>
          </div>
          {this.lockPathwayCardFlag &&
            !!this.props.pathwayDetails &&
            (card.isLocked || card.showLocked) &&
            params.isPathwayOwner && (
              <div className="locked-list-card" style={this.styles.lockedCard}>
                <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
              </div>
            )}
          {this.props.showControls && (
            <div className="number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}
          <div className="list-view-main-card">
            <div className="flex-block">
              {isShowLeftFile && (
                <div className="relative">
                  <div
                    onClick={this.props.handleCardClicked}
                    className={
                      this.props.pathwayChecking &&
                      (params.isShowTopImage ||
                        (params.isFileAttached && !params.audioFileStack)) &&
                      isCompleted
                        ? 'checking-over-image'
                        : 'hide-checking'
                    }
                  >
                    <CheckedIcon color={colors.primary} style={this.styles.checkedIcon} />
                  </div>
                  {card.cardType === 'pack' && (
                    <div className="pathway-label-block">
                      {card.state === 'draft' && (
                        <div className="pathway-draft-label">{tr('DRAFT')}</div>
                      )}
                      <div className="pathway-label">
                        {card.badging && card.badging.imageUrl && (
                          <img
                            className="pathway-badge-preview"
                            src={addSecurity(
                              card.badging.imageUrl,
                              expireAfter,
                              this.props.currentUser.id
                            )}
                          />
                        )}
                        <span>{tr('PATHWAY')}</span>
                      </div>
                    </div>
                  )}

                  {card.cardType === 'journey' && (
                    <div className="pathway-label-block">
                      {card.state === 'draft' && (
                        <div className="pathway-draft-label">{tr('DRAFT')}</div>
                      )}
                      <div className="pathway-label">
                        {card.badging && card.badging.imageUrl && (
                          <img
                            className="pathway-badge-preview"
                            src={addSecurity(
                              card.badging.imageUrl,
                              expireAfter,
                              this.props.currentUser.id
                            )}
                          />
                        )}
                        <span>{tr('JOURNEY')}</span>
                      </div>
                    </div>
                  )}
                  <a
                    className="card-file-container anchorAlignment"
                    onClick={e => {
                      this.props.handleCardClicked(e, true);
                    }}
                  >
                    {(params.cardType.toUpperCase() === 'VIDEO' || card.cardSubtype === 'video') &&
                      !params.videoFileStack && (
                        <div>
                          <VideoStream
                            hideTitle={true}
                            startPlay={false}
                            card={card}
                            allowAutoPlay={this.props.allowAutoplay}
                            getAllvideoElements={this.props.getAllvideoElements}
                            sourceType={`feedView`}
                          />
                          <div className={params.videoData.videoLabelClass}>
                            {tr(params.videoData.videoLabelText)}
                          </div>
                        </div>
                      )}
                    {params.videoFileStack ? (
                      <div style={this.styles.videoBox}>
                        {this.state.cardImage ? (
                          <svg width="100%" height="100%" style={this.styles.mainSvg}>
                            <title>
                              <RichTextReadOnly text={cardMsg} />
                            </title>
                            <SvgImageResized
                              resizeOptions={'height:300'}
                              xlinkHref={addSecurity(
                                this.state.poster || this.state.cardImage,
                                expireAfter,
                                this.props.currentUser.id
                              )}
                              width="100%"
                              style={this.styles.svgImage}
                              height="100%"
                              cardId={`${card.id}`}
                            />
                          </svg>
                        ) : (
                          <div className="fp fp_video fp_video_card" onClick={this.props.videoPlay}>
                            {transcodedVideoStatus &&
                              (transcodedVideoStatus === 'completed' ? (
                                <video
                                  id={`video-${card.filestack[0].handle}`}
                                  src={card.filestack[0].url}
                                  ref={`video`}
                                  onPlay={() => {
                                    this.refs.video && this.refs.video.setAttribute('controls', '');
                                    this.setState({ videoPlaying: true });
                                  }}
                                  onPause={() => {
                                    this.refs.video && this.refs.video.removeAttribute('controls');
                                    this.setState({ videoPlaying: false });
                                  }}
                                  poster={
                                    (card.filestack &&
                                      card.filestack[1] &&
                                      card.filestack[1].mimetype &&
                                      card.filestack[1].mimetype.includes('image/') &&
                                      card.filestack[1] &&
                                      addSecurity(
                                        card.filestack[1].url,
                                        expireAfter,
                                        this.props.currentUser.id
                                      )) ||
                                    addSecurity(
                                      this.state.poster,
                                      expireAfter,
                                      this.props.currentUser.id
                                    )
                                  }
                                  preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                                  controlsList={
                                    params.isDownloadContentDisabled ? 'nodownload' : ''
                                  }
                                  className="ml-video-cards"
                                />
                              ) : (
                                <img
                                  className="waiting-video"
                                  src="/i/images/video_processing_being_processed.jpg"
                                />
                              ))}
                          </div>
                        )}
                      </div>
                    ) : (
                      this.state.poster &&
                      params.cardType === 'FILE' && (
                        <svg width="100%" height="100%" style={this.styles.mainSvg}>
                          <title>
                            <RichTextReadOnly text={cardMsg} />
                          </title>
                          <SvgImageResized
                            cardId={`${card.id}`}
                            resizeOptions={'height:300'}
                            xlinkHref={
                              (card.filestack &&
                                card.filestack[1] &&
                                card.filestack[1].mimetype &&
                                card.filestack[1].mimetype.includes('image/') &&
                                card.filestack[1] &&
                                addSecurity(
                                  card.filestack[1].url,
                                  expireAfter,
                                  this.props.currentUser.id
                                )) ||
                              addSecurity(this.state.poster, expireAfter, this.props.currentUser.id)
                            }
                            width="100%"
                            style={this.styles.svgImage}
                            height="100%"
                          />
                        </svg>
                      )
                    )}
                    {(params.cardType === 'VIDEO' || card.cardSubtype === 'video') &&
                      params.videoFileStack && (
                        <div>
                          <PlayIcon
                            className="center-play-icon"
                            style={
                              this.state.videoPlaying
                                ? this.styles.videoPlaying
                                : this.styles.playIcon
                            }
                            onClick={this.props.videoPlay}
                            id="play"
                            color={colors.white}
                          />
                        </div>
                      )}
                    {params.cardType !== 'VIDEO' &&
                      card.cardSubtype !== 'video' &&
                      !params.fileFileStack && (
                        <svg width="100%" height="100%" style={this.styles.mainSvg}>
                          <title>
                            <RichTextReadOnly text={cardMsg} />
                          </title>
                          <SvgImageResized
                            cardId={`${card.id}`}
                            resizeOptions={'height:300'}
                            xlinkHref={
                              (card.filestack &&
                                card.filestack[0] &&
                                card.filestack[0].mimetype &&
                                card.filestack[0].mimetype.includes('application/') &&
                                card.filestack[1] &&
                                addSecurity(
                                  card.filestack[1].url,
                                  expireAfter,
                                  this.props.currentUser.id
                                )) ||
                              addSecurity(
                                this.state.poster,
                                expireAfter,
                                this.props.currentUser.id
                              ) ||
                              addSecurity(
                                this.state.cardSvgBackground,
                                expireAfter,
                                this.props.currentUser.id
                              )
                            }
                            width="100%"
                            style={this.styles.svgImage}
                            height="100%"
                          />
                        </svg>
                      )}
                    {params.isShowTopImage && (
                      <div className="card-img button-icon card-blurred-background">
                        <svg width="100%" height="100%">
                          <title>
                            <RichTextReadOnly text={cardMsg} />
                          </title>
                          <SvgImageResized
                            cardId={`${card.id}`}
                            resizeOptions={'height:300'}
                            style={svgStyle}
                            xlinkHref={addSecurity(
                              this.state.poster || this.state.cardSvgBackground,
                              expireAfter,
                              this.props.currentUser.id
                            )}
                            x="-30%"
                            y="-30%"
                            width="160%"
                            height="160%"
                          />
                          <filter id={`blur-effect-card-${card.id}`}>
                            <feGaussianBlur stdDeviation="10" />
                          </filter>
                        </svg>
                      </div>
                    )}
                    {!!scormCard && (
                      <svg width="100%" height="100%" style={this.styles.mainSvg}>
                        <title>
                          <RichTextReadOnly text={cardMsg} />
                        </title>
                        <SvgImageResized
                          cardId={`${card.id}`}
                          resizeOptions={'height:300'}
                          xlinkHref={addSecurity(
                            this.state.poster || this.state.cardSvgBackground,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          width="100%"
                          style={this.styles.svgImage}
                          height="100%"
                        />
                      </svg>
                    )}
                    {params.fileFileStack && !params.isShowTopImage && (
                      <div>
                        <div key={card.filestack[0].handle} className="fp fp_preview">
                          <span>
                            <iframe
                              className="filestack-preview-container"
                              height="300px"
                              width="100%"
                              src={pdfPreviewUrl(
                                card.filestack[0].url,
                                expireAfter,
                                this.props.currentUser.id
                              )}
                              onMouseEnter={() => {
                                this.setState({ [card.filestack[0].handle]: true });
                              }}
                              onMouseLeave={() => {
                                this.setState({ [card.filestack[0].handle]: false });
                              }}
                            />
                            {!params.isDownloadContentDisabled &&
                              this.props.downloadBlock(card.filestack[0])}
                          </span>
                        </div>
                      </div>
                    )}
                  </a>
                </div>
              )}
              <div
                className={`${
                  isShowLeftFile ? 'content-view-block' : 'no-attached'
                } card-block-with-information relative`}
              >
                <SmartBiteCardHeader
                  card={card}
                  params={params}
                  isNameCut={isNameCut}
                  pathwayEditor={this.props.pathwayEditor}
                  authorName={authorName}
                  cardClickHandle={this.state.cardClickHandle}
                  standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
                  showControls={this.props.showControls}
                  allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
                  newSkillLevel={this.props.newSkillLevel}
                  rateCard={this.props.rateCard}
                  logoObj={logoObj}
                  handleCardClicked={this.props.handleCardClicked}
                  cardLayoutType={this.props.type}
                  viewType={`feedView`}
                />
                <div className="card-details">
                  <div
                    className="cursor"
                    onClick={
                      this.props.previewMode || (scormCard && card.hidden)
                        ? null
                        : this.props.handleCardClicked
                    }
                  >
                    <div className="image-exists vertical-spacing-small">
                      <div
                        className={
                          params.cardType === 'POLL'
                            ? 'main-card-content-container poll-results-container'
                            : 'main-card-content-container'
                        }
                      >
                        {params.cardType === 'TEXT' ||
                        params.cardType === 'IMAGE' ||
                        params.cardType === 'PROJECT' ? (
                          <div className="vertical-spacing-large">
                            <div
                              className={
                                params.imageExists
                                  ? `openCardFromSearch button-icon title ${
                                      this.props.dueAt
                                        ? 'title-with-image-and-due'
                                        : 'title-with-image'
                                    }`
                                  : 'openCardFromSearch button-icon title text-title'
                              }
                            >
                              {card.title && (
                                <div
                                  className="card-title pointer card-details__title_bold"
                                  onClick={this.props.handleCardClicked}
                                >
                                  <MarkdownRenderer markdown={card.title} />
                                </div>
                              )}

                              {params.message && (
                                <div
                                  className="card-title markdown-text pointer"
                                  onClick={this.props.handleCardClicked}
                                >
                                  <RichTextReadOnly text={convertRichText(params.message)} />
                                </div>
                              )}
                            </div>
                          </div>
                        ) : params.cardType === 'AUDIO' ? (
                          <audio
                            controls
                            src={addSecurity(
                              card.filestack[0].url,
                              expireAfter,
                              this.props.currentUser.id
                            )}
                            style={this.styles.audio}
                          />
                        ) : (
                          <div
                            className={`${!(
                              params.cardType === 'POLL' || params.cardType === 'QUIZ'
                            ) && 'vertical-spacing-large'}`}
                          >
                            <div
                              className={`button-icon title ${(params.cardType === 'POLL' ||
                                params.cardType === 'QUIZ') &&
                                'quiz-title'} ${
                                card.author
                                  ? this.props.dueAt
                                    ? 'has-author-due'
                                    : 'has-author'
                                  : this.props.dueAt
                                  ? 'no-author-due'
                                  : 'no-author'
                              }`}
                            >
                              {!!card.resource &&
                              (!!card.resource.title || !!card.resource.description) ? (
                                card.resource &&
                                card.resource.url &&
                                !!~card.resource.url.indexOf('/api/scorm/') ? (
                                  <div>
                                    {params.message && card.message !== card.resource.title && (
                                      <div
                                        className="text-title pointer"
                                        onClick={
                                          scormCard && card.hidden
                                            ? null
                                            : this.props.handleCardClicked
                                        }
                                        dangerouslySetInnerHTML={{ __html: params.message }}
                                      />
                                    )}
                                    {card.resource &&
                                      card.resource.url &&
                                      card.state == 'published' && (
                                        <a
                                          title={tr('open in new tab')}
                                          className="link-to-original"
                                          href={card.resource.url}
                                          target="_blank"
                                        >
                                          {' '}
                                          {tr('Click here to access the course')}
                                        </a>
                                      )}
                                    {card.resource &&
                                      card.resource.url &&
                                      card.state !== 'published' && (
                                        <div className="scorm-processing">
                                          <Spinner />
                                          <p>{this.state.ScormStateMsg}...</p>
                                        </div>
                                      )}
                                    {card.resource.description &&
                                      card.resource.description !== params.message && (
                                        <div
                                          className="text-title pointer"
                                          onClick={this.props.handleCardClicked}
                                          dangerouslySetInnerHTML={{
                                            __html: card.resource.description
                                          }}
                                        />
                                      )}
                                  </div>
                                ) : (
                                  <div>
                                    {card.resource &&
                                      card.resource.title &&
                                      card.resource.title !== card.resource.url && (
                                        <div
                                          className="pointer card-details__title_bold"
                                          onClick={this.props.handleCardClicked}
                                          dangerouslySetInnerHTML={{ __html: card.resource.title }}
                                        />
                                      )}
                                    {card.resource &&
                                      card.resource.description &&
                                      card.resource.description !== params.message && (
                                        <div
                                          className="text-title pointer"
                                          onClick={this.props.handleCardClicked}
                                          dangerouslySetInnerHTML={{
                                            __html: card.resource.description
                                          }}
                                        />
                                      )}
                                    {params.message && card.message !== card.resource.title && (
                                      <div
                                        className="text-title pointer"
                                        onClick={
                                          scormCard && card.hidden
                                            ? null
                                            : this.props.handleCardClicked
                                        }
                                        dangerouslySetInnerHTML={{ __html: params.message }}
                                      />
                                    )}
                                  </div>
                                )
                              ) : (
                                <div>
                                  <div
                                    onClick={
                                      scormCard && card.hidden ? null : this.props.handleCardClicked
                                    }
                                    className="pointer"
                                    dangerouslySetInnerHTML={{ __html: params.message }}
                                  />
                                  {(card.cardType === 'pack' || card.cardType === 'journey') &&
                                    !!card.title && (
                                      <div
                                        className="text-title pointer"
                                        onClick={this.props.handleCardClicked}
                                        dangerouslySetInnerHTML={{ __html: card.message }}
                                      />
                                    )}
                                </div>
                              )}
                            </div>
                            {(params.cardType === 'POLL' || params.cardType === 'QUIZ') &&
                              !params.isShowTopImage && (
                                <div
                                  className="poll-content"
                                  onClick={this.props.handleCardClicked}
                                >
                                  <Poll card={card} cardUpdated={this.props.cardUpdated} />
                                </div>
                              )}
                          </div>
                        )}
                      </div>
                      {this.isCsodCourse && (
                        <div>
                          {card.status && (
                            <div className="csod-course__info">
                              {tr('Status: ')} {card.status}
                            </div>
                          )}
                          {startDate && (
                            <div className="csod-course__info">
                              {tr('Assigned Date: ')} {startDate}
                            </div>
                          )}
                          {endDate && (
                            <div className="csod-course__info">
                              {tr('Due Date: ')} {endDate}
                            </div>
                          )}
                          {card.duration && (
                            <div className="csod-course__info">
                              {tr('Duration: ')} {card.duration}
                            </div>
                          )}
                        </div>
                      )}
                      {!this.isCsodCourse && (
                        <div>
                          {this.props.startDate && (
                            <div className="csod-course__info">
                              {tr('Started Date: ')}
                              <DateConverter date={this.props.startDate} isMonthText="true" />
                            </div>
                          )}
                          {this.props.dueAt && (
                            <div className="csod-course__info">
                              {tr('Due Date: ')}
                              <DateConverter date={this.props.dueAt} isMonthText="true" />
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </div>
                  <SmartBiteCardFooter
                    isCsodCourse={this.isCsodCourse}
                    edcastPricing={this.state.edcastPricing}
                    edcastPlansForPricing={this.state.edcastPlansForPricing}
                    card={card}
                    priceData={priceData}
                    isShowLeap={isShowLeap}
                    params={params}
                    lockPathwayCardFlag={this.lockPathwayCardFlag}
                    toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                    previewMode={this.props.previewMode}
                    hideActions={hideActions}
                    disableTopics={disableTopics}
                    dismissible={this.props.dismissible}
                    providerCards={this.props.providerCards}
                    showTopicToggleClick={this.props.showTopicToggleClick}
                    isStandalone={this.props.isStandalone}
                    cardUpdated={this.props.cardUpdated}
                    removeCardFromList={this.props.removeCardFromList}
                    hideComplete={this.props.hideComplete}
                    isCompleted={isCompleted}
                    deleteSharedCard={this.props.deleteSharedCard}
                    channel={this.props.channel}
                    type={this.props.type}
                    averageRating={averageRating}
                    ratingCount={this.props.ratingCount}
                    handleCardClicked={this.props.handleCardClicked}
                    comments={this.props.comments}
                    isLiveStream={this.props.isLiveStream}
                    commentsCount={this.props.commentsCount}
                    isPartOfPathway={isPartOfPathway}
                    openReasonReportModal={this.props.openReasonReportModal}
                    clickOnComments={this.props.clickOnComments}
                    removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
                  />
                </div>
              </div>
            </div>
          </div>
        </Paper>
        {this.props.modalOpen && !this.props.withoutCardModal && (
          <CardModal
            logoObj={logoObj}
            defaultImage={this.state.defaultImage}
            card={card}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            likeCard={this.props.cardLikeHandler}
          />
        )}
        {this.props.modalClassesOpen && (
          <CardClassesModal
            classes={this.props.card.additional_metadata.classes}
            closeModal={this.props.closeClassesModal}
          />
        )}
      </div>
    );
  }
}

CardFeedView.propTypes = {
  comments: PropTypes.array,
  params: PropTypes.object,
  channel: PropTypes.object,
  providerCards: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  priceData: PropTypes.object,
  disableTopics: PropTypes.bool,
  pathway: PropTypes.object,
  arrToLeap: PropTypes.object,
  svgStyle: PropTypes.object,
  cardsList: PropTypes.object,
  isUpvoted: PropTypes.bool,
  showControls: PropTypes.bool,
  isOwner: PropTypes.bool,
  dismissible: PropTypes.bool,
  previewMode: PropTypes.bool,
  userInterest: PropTypes.bool,
  modalClassesOpen: PropTypes.bool,
  withoutCardModal: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  modalOpen: PropTypes.bool,
  isStandalone: PropTypes.bool,
  scormCard: PropTypes.any,
  moreCards: PropTypes.bool,
  isLiveStream: PropTypes.bool,
  isPrivate: PropTypes.bool,
  isCompleted: PropTypes.bool,
  pathwayEditor: PropTypes.bool,
  showIndex: PropTypes.bool,
  hideActions: PropTypes.bool,
  isShowLeap: PropTypes.any,
  journeyDetails: PropTypes.bool,
  isNameCut: PropTypes.bool,
  showTopic: PropTypes.bool,
  pathwayDetails: PropTypes.any,
  isLeapModalOpen: PropTypes.bool,
  pathwayChecking: PropTypes.bool,
  fullView: PropTypes.bool,
  pathwayView: PropTypes.bool,
  votesCount: PropTypes.number,
  commentsCount: PropTypes.number,
  ratingCount: PropTypes.number,
  index: PropTypes.number,
  cardLikeHandler: PropTypes.func,
  closeModal: PropTypes.func,
  updateCommentCount: PropTypes.func,
  toggleHandleLeapModal: PropTypes.func,
  closeClassesModal: PropTypes.func,
  hideComplete: PropTypes.func,
  downloadBlock: PropTypes.func,
  cardUpdated: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  removeCardFromList: PropTypes.func,
  handleCardClicked: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  videoPlay: PropTypes.func,
  deeplink: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  videoPlaying: PropTypes.any,
  controlsElement: PropTypes.any,
  rateCard: PropTypes.any,
  currency: PropTypes.any,
  type: PropTypes.string,
  dueAt: PropTypes.string,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  transcodedVideoStatus: PropTypes.string,
  allowAutoplay: PropTypes.bool,
  startDate: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  getAlliframeElements: PropTypes.func,
  reloadVideo: PropTypes.func,
  getAllvideoElements: PropTypes.func,
  clickOnComments: PropTypes.func,
  viewType: PropTypes.string,
  removeDismissAssessmentFromList: PropTypes.func,
  team: PropTypes.object
};

CardFeedView.defaultProps = {
  isPartOfPathway: false
};

function mapStoreStateToProps(state) {
  return {
    cards: state.cards.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardFeedView);
