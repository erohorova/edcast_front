import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import DateConverter from 'edc-web-sdk/components/common/DateConverter';
import moment from 'moment';

function CourseCard(props) {
  let additional_metadata = props.course.additional_metadata;
  let course = props.course;
  let courseType = props.courseType;
  let courseImg = '';
  if (courseType === 'csod') {
    courseImg = 'https://s3.amazonaws.com/edc-dev-web/csod_logo.png';
  } else {
    courseImg = 'https://s3.amazonaws.com/edc-dev-web/saba_logo.png';
  }
  let endDate = course.end_date
    ? moment
        .utc(course.end_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l')
    : null;
  let assignedDate = additional_metadata && additional_metadata.assigned_date;
  assignedDate = assignedDate
    ? moment
        .utc(assignedDate, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l')
    : null;
  let dueDate = additional_metadata && additional_metadata.due_date;
  dueDate =
    dueDate && course.status !== 'Completed'
      ? moment
          .utc(dueDate, 'YYYY-MM-DD HH:mm:ss')
          .local()
          .format('l')
      : null;
  let startDate = course.start_date
    ? moment
        .utc(course.start_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l')
    : null;
  let duration = course.duration;
  let status = course.status;
  return (
    <div className="course-cards-v2-container">
      <Paper
        className="course-cards-v2"
        onClick={() => {
          var win = window.open(course.deeplink_url, '_blank');
        }}
      >
        <div className="course-image-container">
          <img className="course-image" src={courseImg} />
        </div>
        <div className="course-card-type">
          <p>COURSE</p>
        </div>
        <div className="course-card-details">
          <p className="course-card-title">{course.course.name}</p>
        </div>
        <div className="course-card-other-details course-status" style={{ fontSize: '13px' }}>
          {status && (
            <div className="info">
              {tr('Status: ')} {status}
            </div>
          )}
          {assignedDate && (
            <div className="info">
              {tr('Approved Date: ')} {assignedDate}
            </div>
          )}
          {startDate && (
            <div className="info">
              {tr('Start Date: ')} {startDate}
            </div>
          )}
          {dueDate && (
            <div className="info">
              {tr('Due Date: ')} {dueDate}
            </div>
          )}
          {endDate && (
            <div className="info">
              {tr('Completion Date: ')} {endDate}
            </div>
          )}
          {duration && (
            <div className="info">
              {tr('Duration: ')} {duration}
            </div>
          )}
        </div>
      </Paper>
    </div>
  );
}

CourseCard.propTypes = {
  courseType: PropTypes.string,
  course: PropTypes.object
};

export default CourseCard;
