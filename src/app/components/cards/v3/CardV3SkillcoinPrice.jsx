import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';

let LocaleCurrency = require('locale-currency');

class CardV3SkillcoinPrice extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {};
  }

  getPricingPlans() {
    return this.props.edcastPlansForPricing &&
      this.props.card.cardMetadatum &&
      this.props.card.cardMetadatum.plan
      ? tr(startCase(toLower(this.props.card.cardMetadatum.plan)))
      : tr('Free');
  }

  render() {
    let { card, params } = this.props;
    let paidByUser = card && card.paidByUser;
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = {};
    let skillcoinPriceData = {};
    if (card.prices && card.prices.length > 0) {
      let localePrice = undefined;
      let USDPrice = undefined;
      let nonSkillcoinPrice = undefined;
      card.prices.forEach(price => {
        if (!nonSkillcoinPrice && price.currency !== 'SKILLCOIN') {
          nonSkillcoinPrice = price;
        }
        if (!localePrice && price.currency === currency) {
          localePrice = price;
        }
        if (!USDPrice && price.currency === 'USD') {
          USDPrice = price;
        }
        if (!skillcoinPriceData.currency && price.currency === 'SKILLCOIN') {
          skillcoinPriceData = price;
        }
      });
      priceData = localePrice || USDPrice || nonSkillcoinPrice;
    }

    let eclLogoObj = this.props.logoObj && this.props.logoObj[card.eclSourceTypeName];
    let isProvider =
      (!params.hideProvider && card.providerImage) ||
      (!this.isCsodCourse &&
        ((card.eclSourceLogoUrl && !card.providerImage) || eclLogoObj !== undefined));
    let isAuthor = card.author && params.showCreator;
    let isVideo = params.cardType.toUpperCase() === 'VIDEO' || card.cardSubtype === 'video';
    const isStandalone = this.props.cardType.toLowerCase() === 'standalone';
    const isTile = this.props.cardType && this.props.cardType.toLowerCase() === 'tile';
    let isShowPrice =
      params.cardType !== 'PATHWAY' &&
      params.cardType !== 'JOURNEY' &&
      card.cardType !== 'video_stream' &&
      card.cardType !== 'VideoStream';
    let edcastPricing = this.props.edcastPricing;
    let cpeCredits = card && card.eclSourceCpeCredits;
    let cpeSubject = card && card.eclSourceCpeSubArea;
    if (!isStandalone && cpeSubject && cpeSubject.length > 30) {
      cpeSubject = cpeSubject.substring(0, 30) + '...';
    }
    if (!isStandalone && cpeCredits && cpeCredits.length > 25) {
      cpeCredits = cpeCredits.substring(0, 25) + '..';
    }
    //todo: view Purchased if user has already buy it EP-14045 and EP-13752
    return (
      <div className={!isStandalone ? 'card-v3__skillcoin-container' : ''}>
        <div
          className={`card-${this.props.cardType ||
            'tile'}-v3__skillcoin-price card-v3__skillcoin-price`}
        >
          {isProvider && isAuthor && !isVideo && card.author && params.showCreator && (
            <span className="card-v3__reference-info card-v3__posted-by-name_container">
              {tr('Posted By')}:
              <span className="card-v3__posted-by-name">
                &nbsp;
                {card.author.fullName
                  ? card.author.fullName
                  : `${card.author.firstName ? card.author.firstName : ''} ${
                      card.author.lastName ? card.author.lastName : ''
                    }`}
              </span>
              <span className="dot" />
            </span>
          )}
          {edcastPricing &&
            !!skillcoinPriceData.amount &&
            (!paidByUser || isStandalone) &&
            isShowPrice && (
              <span className={`card-v3__pricing card-v3__reference-info`}>
                {'Skillcoins'}:
                <span className="card-v3__pricing-value">&nbsp;{skillcoinPriceData.amount}</span>
                {edcastPricing && priceData && (!paidByUser || isStandalone) && isShowPrice && (
                  <span className="dot" />
                )}
              </span>
            )}
          {edcastPricing && priceData && (!paidByUser || isStandalone) && isShowPrice && (
            <span className={`card-v3__pricing card-v3__reference-info`}>
              {tr('Price')}:
              {this.props.card.isPaid && priceData && priceData.amount ? (
                <span className="card-v3__pricing-value">
                  &nbsp;{priceData.symbol}
                  {priceData.amount}
                </span>
              ) : (
                this.getPricingPlans()
              )}
            </span>
          )}

          {paidByUser && !isStandalone && isShowPrice && (
            <span className="card-v3__pricing card-v3__reference-info">{tr('Purchased')}</span>
          )}
        </div>
        <div className={isStandalone ? 'card-v3__stadalone-CPE' : 'card-v3__tile-CPE'}>
          {(isTile || isStandalone) && cpeSubject && (
            <div className={`card-v3__CPE-credit`}>
              {tr('CPE Subject')}: <span> {cpeSubject}</span>
              <span className={isStandalone ? `dot` : ``} />
            </div>
          )}
          {(isTile || isStandalone) && cpeCredits && (
            <div className={`card-v3__CPE-credit`}>
              {tr('CPE Credits')}:<span className="card-v3__pricing-value">{cpeCredits}</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

CardV3SkillcoinPrice.propTypes = {
  card: PropTypes.object,
  params: PropTypes.object,
  logoObj: PropTypes.object,
  edcastPlansForPricing: PropTypes.any,
  currentUser: PropTypes.object,
  cardType: PropTypes.string,
  edcastPricing: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(CardV3SkillcoinPrice);
