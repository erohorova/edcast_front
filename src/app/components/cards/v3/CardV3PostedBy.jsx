import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class CardV3PostedBy extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {};
  }

  render() {
    let { card, params, cardType } = this.props;

    return (
      <div className={`card-${cardType || 'tile'}-v3__posted-by card-v3__posted-by`}>
        {card.author && params.showCreator && (
          <span className="card-v3__reference-info">
            {tr('Posted By:')}
            <span className="card-v3__posted-by-name">
              {' '}
              {card.author.fullName
                ? card.author.fullName
                : `${card.author.firstName ? card.author.firstName : ''} ${
                    card.author.lastName ? card.author.lastName : ''
                  }`}
            </span>
          </span>
        )}
      </div>
    );
  }
}

CardV3PostedBy.propTypes = {
  card: PropTypes.object,
  edcastPlansForPricing: PropTypes.any,
  currentUser: PropTypes.object,
  cardType: PropTypes.string,
  params: PropTypes.shape({
    showCreator: PropTypes.bool
  })
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(CardV3PostedBy);
