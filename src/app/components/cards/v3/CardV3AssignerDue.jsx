import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';

import DateConverter from '../../common/DateConverter';
import getFormattedDateTime from '../../../utils/getFormattedDateTime';

class CardV3SkillcoinPrice extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {};
  }

  render() {
    let { isStandalone, isCsodCourse, card, params } = this.props;

    let endDate = card.end_date && getFormattedDateTime(card.end_date, 'DD/MM/YYYY');

    return (
      <div className={`card-${this.props.cardType || 'tile'}-v3__assigner-due`}>
        {card.author && params.showCreator && (
          <span className="card-v3__reference-info">
            {tr('Posted By:')}
            <span>
              {' '}
              {card.author.fullName
                ? card.author.fullName
                : `${card.author.firstName ? card.author.firstName : ''} ${
                    card.author.lastName ? card.author.lastName : ''
                  }`}
            </span>
            <span className="dot" />
          </span>
        )}
        {isStandalone && false && (
          <span className="card-v3__reference-info">
            {tr('Assigner : info about assigner')}
            <span className="dot" />
          </span>
        )}
        {isCsodCourse ? (
          <div className="card-v3__reference-info">
            {tr('Due Date: ')} {endDate}
          </div>
        ) : (
          (card.dueAt || (card.assignment && card.assignment.dueAt)) && (
            <div className="card-v3__reference-info">
              {tr('Due Date: ')}
              <DateConverter
                isMonthText="true"
                date={card.dueAt || (card.assignment && card.assignment.dueAt)}
              />
            </div>
          )
        )}
      </div>
    );
  }
}

CardV3SkillcoinPrice.propTypes = {
  card: PropTypes.object,

  currentUser: PropTypes.object,
  cardType: PropTypes.string,

  isCsodCourse: PropTypes.bool,
  isStandalone: PropTypes.bool
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(CardV3SkillcoinPrice);
