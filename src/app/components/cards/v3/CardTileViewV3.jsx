import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';

import colors from 'edc-web-sdk/components/colors/index';
import LockIcon from 'edc-web-sdk/components/icons/LockV3';
import CheckedIcon from 'edc-web-sdk/components/icons/CheckV3';
import StreamIcon from 'edc-web-sdk/components/icons/StreamIcon';

import PlayVideo from 'edc-web-sdk/components/icons/PlayVideo';
import CardModal from '../../modals/CardModal';
import SlideOutCardModal from '../../modals/SlideOutCardModal';

import LeapModal from '../../modals/LeapModal';
const RichTextReadOnly = Loadable({
  loader: () => import('../../common/RichTextReadOnly'),
  loading: () => null
});

import MarkdownRenderer from '../../common/MarkdownRenderer';
import convertRichText from '../../../utils/convertRichText';

import { Permissions } from '../../../utils/checkPermissions';
import getDefaultImage from '../../../utils/getDefaultCardImage';
import addSecurity from '../../../utils/filestackSecurity';

const FileCard = Loadable({
  loader: () => import('./FileCard'),
  loading: () => null
});

const Poll = Loadable({
  loader: props => import('../../feed/Poll'),
  loading: () => null
});

import CardV3SkillcoinPrice from './CardV3SkillcoinPrice';
import CardV3Image from './CardV3Image';
import CardV3Footer from './CardV3Footer';
import CardV3SkillTypeEffort from './CardV3SkillTypeEffort';
import CardV3Header from './CardV3Header';
import CardV3Assignor from './CardV3Assignor';

class CardTileViewV3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      checkedIcon: {
        width: '2.8125rem',
        height: '1.9375rem',
        marginTop: '5.375rem'
      },
      cardImgContainerImg: {
        zIndex: 2
      },
      cardImgContainerSvg: {
        zIndex: 2,
        position: 'relative'
      },
      audio: {
        width: '100%'
      },
      privateLabel: {
        margin: '3.125rem 1rem 0',
        color: colors.primary
      },
      privateCard: {
        height: '0'
      },
      bigLockIcon: {
        width: '1.875rem',
        height: '2.3125rem',
        marginTop: '5.375rem'
      },
      streamIcon: {
        height: '0.625rem',
        width: '0.875rem',
        marginRight: '0.25rem'
      },
      playIcon: {
        position: 'absolute',
        left: '45%',
        top: '45%',
        zIndex: '10',
        height: '24px',
        width: '34px'
      }
    };
    this.state = {
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      lockPathwayCardFlag: window.ldclient.variation('lock-pathway-card', false),
      isNewTileCard: window.ldclient.variation('new-ui-tile-card', false)
    };

    this.isIE =
      /msie\s|trident\/|edge\//i.test(window.navigator.userAgent) &&
      !!(
        document.uniqueID ||
        document.documentMode ||
        window.ActiveXObject ||
        window.MSInputMethodContext
      );
  }

  setScormMsg = card => {
    if (card.state == 'processing') {
      return 'Course is getting uploaded';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  clickHandle = (e, flag, param) => {
    e.stopPropagation();
    this.props.handleCardClicked && this.props.handleCardClicked(e, flag, param);
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let {
      isPartOfPathway,
      params,
      card,
      isLocked,
      isChecked,
      isShowText,
      isWithFile,
      blurredImage,
      isLockedForOwner
    } = this.props;
    let scormMsg = this.setScormMsg(card);

    let isVideo = params.cardType.toUpperCase() === 'VIDEO' || card.cardSubtype === 'video';

    let eclLogoObj =
      (this.props.logoObj && this.props.logoObj[card.eclSourceTypeName]) || this.props.providerLogo;
    let isProvider =
      (!params.hideProvider && card.providerImage) ||
      (!this.isCsodCourse &&
        ((card.eclSourceLogoUrl && !card.providerImage) || eclLogoObj !== undefined));

    return (
      <div className={`card-v3 card-v3__tile-view`}>
        <CardV3Assignor assignor={card.assignor} sharedBy={card.sharedBy} />
        {this.props.isShowLeap && (params.isOwner || this.props.pathwayEditor) && (
          <LeapModal
            isOpen={this.props.isLeapModalOpen}
            card={card}
            toggleHandleLeapModal={this.props.toggleHandleLeapModal}
            pathwayEditor={this.props.pathwayEditor}
            cardsList={this.props.cardsList}
            addToLeap={this.props.addToLeap}
            arrToLeap={this.props.arrToLeap}
            pathway={this.props.pathway}
            cardUpdated={this.props.cardUpdated}
          />
        )}
        {(isLockedForOwner || (isLocked && (isWithFile || isVideo))) && (
          <div
            className="card-v3__locked-card pointer"
            style={this.styles.lockedCard}
            onClick={this.props.handleCardClicked}
          >
            <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
          </div>
        )}
        {isChecked && !isLocked && (
          <div
            className="card-v3__completed-card card-v3-cursor"
            onClick={this.props.handleCardClicked}
          >
            <CheckedIcon color="#38b6a0" style={this.styles.checkedIcon} />
          </div>
        )}
        <div
          className={
            this.props.scormCard && card.state !== 'published'
              ? 'scorm-not-uploaded'
              : 'hide-checking'
          }
        >
          <div className="scorm-not-uploaded-text">{tr(scormMsg)}</div>
        </div>
        <div>
          {this.props.showControls && this.props.controlsElement}
          {(this.props.showControls || this.props.showIndex) && (
            <div className="close-button pathway-card-btn number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}
        </div>
        <div
          className="card-v3__container card-tile-v3__container card-v3-cursor"
          onClick={this.props.handleCardClicked}
        >
          <div
            className={`card-v3__image-and-skill-container card-tile-v3__image-and-skill-container ${
              isWithFile || isVideo ? 'card-tile-v3__image-and-skill-container_with-image' : ''
            }`}
          >
            {isWithFile && !isVideo && (
              <div className="card-v3__image card-tile-v3__image">
                <span
                  className="card-v3__image_gradient"
                  style={{
                    backgroundImage: `${
                      this.isIE
                        ? 'linear-gradient(rgba(123, 123, 153, 0.05) 65%, ' +
                          this.props.cardImageShadowColor +
                          ')'
                        : 'linear-gradient(to bottom, rgba(123, 123, 153, 0.05), 65%, ' +
                          this.props.cardImageShadowColor +
                          ')'
                    }`,
                    pointerEvents: 'none'
                  }}
                />
                <CardV3Image card={card} img={blurredImage} currentUser={this.props.currentUser} />
              </div>
            )}
            {isVideo && !isLocked && (
              <div
                className="card-v3__image card-tile-v3__image"
                onClick={e => {
                  this.clickHandle(e, true, params.isYoutubeVideo);
                }}
              >
                <span
                  className="card-v3__image_gradient"
                  style={{
                    backgroundImage: `${
                      this.isIE
                        ? 'linear-gradient(rgba(123, 123, 153, 0.05) 65%, ' +
                          this.props.cardImageShadowColor +
                          ')'
                        : 'linear-gradient(to bottom, rgba(123, 123, 153, 0.05), 65%, ' +
                          this.props.cardImageShadowColor +
                          ')'
                    }`,
                    pointerEvents: 'none'
                  }}
                />
                <PlayVideo style={this.styles.playIcon} />
                {params.videoFileStack ? (
                  <div className="fp fp_video fp_video_card default-video-card">
                    {this.props.transcodedVideoStatus &&
                      (this.props.transcodedVideoStatus.trim() === 'completed' ? (
                        <video
                          preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                          src={!params.poster && this.props.transcodedVideoUrl}
                          poster={addSecurity(
                            params.poster,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          controlsList={params.isDownloadContentDisabled ? 'nodownload' : ''}
                        />
                      ) : (
                        <img
                          className="waiting-video"
                          src="/i/images/video_processing_being_processed.jpg"
                        />
                      ))}
                  </div>
                ) : (
                  <CardV3Image
                    card={card}
                    img={params.poster}
                    currentUser={this.props.currentUser}
                  />
                )}
                {(card.cardType === 'video_stream' || card.cardType === 'VideoStream') && (
                  <div className="card-v3__video_stream">
                    <PlayVideo style={this.styles.playIcon} />
                    <div className={params.videoData.videoLabelClass}>
                      <StreamIcon style={this.styles.streamIcon} />
                      {tr((params.videoData.videoLabelText || '').replace(' ', ''))}
                    </div>
                  </div>
                )}
              </div>
            )}
            {card.cardType !== 'video_stream' && card.cardType !== 'VideoStream' && (
              <CardV3SkillTypeEffort
                allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
                params={params}
                card={card}
                isOnDetailPage={this.props.pathwayChecking}
                rateCard={this.props.rateCard}
                isOnTheFile={isWithFile || params.cardType === 'VIDEO'}
                isLocked={isLocked}
              />
            )}
          </div>
          {isLocked ? (
            <div className={`card-v3__locked-img${isWithFile || isVideo ? '_with-image' : ''}`} />
          ) : (
            <CardV3Header
              card={card}
              params={params}
              handleCardClicked={this.props.handleCardClicked}
              isNameCut={this.props.isNameCut}
              authorName={this.props.authorName}
              eclLogoObj={eclLogoObj}
              isProvider={isProvider}
              isWithFile={
                isWithFile ||
                isVideo ||
                params.cardType === 'ARTICLE' ||
                params.cardType === 'COURSE'
              }
              callCompleteClickHandler={this.props.completeClickHandler}
              currentUser={this.props.currentUser}
            />
          )}
          {(isShowText ||
            isVideo ||
            params.cardType === 'FILE' ||
            params.cardType === 'QUIZ' ||
            params.cardType === 'POLL') &&
            !isLocked && (
              <div
                className={`card-tile-v3__text-container card-v3__text-container ${
                  params.cardType === 'QUIZ' || params.cardType === 'POLL'
                    ? 'card-v3__quiz-container card-tile-v3__quiz-container'
                    : ''
                } ${params.imageExists || isVideo ? 'card-tile-v3__text-container_with-image' : ''}
                  ${isVideo ? 'card-v3__video-text-container' : ''} ${
                  params.cardType === 'FILE' ? 'card-tile-v3__text-container_with-file' : ''
                }`}
              >
                {card.title && (
                  <div className="card-v3__text card-v3__title">
                    <MarkdownRenderer markdown={card.title} />
                  </div>
                )}
                {params.message && !card.title && (
                  <div
                    className={`card-v3__text ${
                      params.cardType === 'POLL' || params.cardType === 'QUIZ' ? 'poll-title' : ''
                    }`}
                  >
                    <RichTextReadOnly
                      text={convertRichText(
                        params.message.length > 70 &&
                          (params.cardType === 'POLL' || params.cardType === 'QUIZ')
                          ? params.message.substr(0, 70) + '...'
                          : params.message
                      )}
                    />
                  </div>
                )}
              </div>
            )}
          {params.cardType === '' && params.message && (
            <div className={`card-tile-v3__text-container card-v3__text-container`}>
              <div className="card-v3__text">
                <RichTextReadOnly
                  text={convertRichText(
                    this.state.truncateMessage
                      ? () => {
                          this.props.truncateMessageText(params.message);
                        }
                      : params.message
                  )}
                />
              </div>
            </div>
          )}
          {!isLocked && (params.cardType === 'POLL' || params.cardType === 'QUIZ') && (
            <div className="poll-content card-tile-v3__poll-content card-v3__poll-content card-content-v3__poll-content">
              <Poll card={card} cardUpdated={this.props.cardUpdated} isCardV3={true} />
            </div>
          )}
          {!isLocked && params.fileFileStack && params.cardType !== 'QUIZ' && !isWithFile && (
            <FileCard
              classFile="card-tile-v3__file"
              height="156.5px"
              params={params}
              scormCard={this.props.scormCard}
              card={card}
              downloadBlock={this.props.downloadBlock}
              currentUser={this.props.currentUser}
            />
          )}
          <div className="card-v3__bottom-block">
            {params.cardType !== 'PATHWAY' &&
              params.cardType !== 'JOURNEY' &&
              card.cardType !== 'video_stream' &&
              card.cardType !== 'VideoStream' &&
              params.cardType !== 'POLL' &&
              params.cardType !== 'QUIZ' &&
              !params.isHideSpecialInfo &&
              !isLocked && (
                <CardV3SkillcoinPrice
                  card={card}
                  params={params}
                  edcastPricing={this.state.edcastPricing}
                  edcastPlansForPricing={this.state.edcastPlansForPricing}
                  cardType="tile"
                  logoObj={this.props.logoObj}
                />
              )}
            {!isLocked && (
              <CardV3Footer
                isCsodCourse={this.isCsodCourse}
                card={card}
                isShowLeap={this.props.isShowLeap}
                params={params}
                lockPathwayCardFlag={this.state.lockPathwayCardFlag}
                toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                previewMode={this.props.previewMode}
                hideActions={this.props.hideActions}
                disableTopics={this.props.disableTopics}
                dismissible={this.props.dismissible}
                providerCards={this.props.providerCards}
                showTopicToggleClick={this.props.showTopicToggleClick}
                isStandalone={this.props.isStandalone}
                cardUpdated={this.props.cardUpdated}
                removeCardFromList={this.props.removeCardFromList}
                hideComplete={this.props.hideComplete}
                isCompleted={this.props.isCompleted}
                deleteSharedCard={this.props.deleteSharedCard}
                channel={this.props.channel}
                type={this.props.type}
                averageRating={card.averageRating}
                ratingCount={this.props.ratingCount}
                handleCardClicked={this.props.handleCardClicked}
                comments={this.props.comments}
                isLiveStream={this.props.isLiveStream}
                commentsCount={this.props.commentsCount}
                isPartOfPathway={isPartOfPathway}
                openReasonReportModal={this.props.openReasonReportModal}
                clickOnComments={this.props.clickOnComments}
                cardSectionName={this.props.cardSectionName}
                removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
              />
            )}
          </div>
          {this.props.hideEdit && (
            <div className="card-hide-edit tooltip">
              <span className="tooltiptext">{tr(this.props.hideEdit)}</span>
            </div>
          )}
        </div>

        {this.props.modalOpen && !this.props.withoutCardModal && (
          <CardModal
            logoObj={this.props.logoObj}
            defaultImage={this.props.defaultImage}
            card={card}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            likeCard={this.props.cardLikeHandler}
          />
        )}

        {this.props.slideOutCardModalOpen && (
          <SlideOutCardModal
            card={card}
            logoObj={this.props.logoObj}
            cardUpdated={this.props.cardUpdated}
            defaultImage={this.props.defaultImage}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            deleteSharedCard={this.props.deleteSharedCard}
            showComment={Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            isNameCut={this.props.isNameCut}
            pathwayEditor={this.props.pathwayEditor}
            authorName={this.props.authorName}
            cardClickHandle={this.state.cardClickHandle}
            standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
            showControls={this.props.showControls}
            allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
            newSkillLevel={this.props.newSkillLevel}
            rateCard={this.props.rateCard}
            handleCardClicked={this.props.handleCardClicked}
            avatarDiameter="2.5rem"
            cardLayoutType={this.props.type}
            closeSlideOut={this.props.closeSlideOut}
            averageRating={card.averageRating}
            ratingCount={this.props.ratingCount}
            likeCard={this.props.cardLikeHandler}
          />
        )}
      </div>
    );
  }
}

CardTileViewV3.propTypes = {
  defaultImage: PropTypes.string,
  startDate: PropTypes.string,
  comments: PropTypes.array,
  params: PropTypes.object,
  channel: PropTypes.object,
  providerCards: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  priceData: PropTypes.object,
  disableTopics: PropTypes.bool,
  pathway: PropTypes.object,
  arrToLeap: PropTypes.object,
  svgStyle: PropTypes.object,
  cardsList: PropTypes.object,
  isUpvoted: PropTypes.bool,
  showControls: PropTypes.bool,
  isOwner: PropTypes.bool,
  dismissible: PropTypes.bool,
  previewMode: PropTypes.bool,
  userInterest: PropTypes.bool,
  withoutCardModal: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  modalOpen: PropTypes.bool,
  isStandalone: PropTypes.bool,
  scormCard: PropTypes.any,
  moreCards: PropTypes.bool,
  isLiveStream: PropTypes.bool,
  isPrivate: PropTypes.bool,
  isCompleted: PropTypes.bool,
  pathwayEditor: PropTypes.bool,
  showIndex: PropTypes.bool,
  hideActions: PropTypes.bool,
  isShowLeap: PropTypes.any,
  journeyDetails: PropTypes.bool,
  isNameCut: PropTypes.bool,
  showTopic: PropTypes.bool,
  pathwayDetails: PropTypes.object,
  isLeapModalOpen: PropTypes.bool,
  pathwayChecking: PropTypes.bool,
  votesCount: PropTypes.number,
  commentsCount: PropTypes.number,
  ratingCount: PropTypes.number,
  index: PropTypes.number,
  cardLikeHandler: PropTypes.func,
  closeModal: PropTypes.func,
  toggleChannelModal: PropTypes.func,
  updateCommentCount: PropTypes.func,
  toggleHandleLeapModal: PropTypes.func,
  hideComplete: PropTypes.func,
  downloadBlock: PropTypes.func,
  cardUpdated: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  removeCardFromList: PropTypes.func,
  handleCardClicked: PropTypes.func,
  truncateMessageText: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  addToLeap: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  closeSlideOut: PropTypes.func,
  clickOnComments: PropTypes.func,
  controlsElement: PropTypes.any,
  rateCard: PropTypes.any,
  type: PropTypes.string,
  dueAt: PropTypes.string,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  transcodedVideoUrl: PropTypes.string,
  transcodedVideoStatus: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  hideEdit: PropTypes.string,
  slideOutCardModalOpen: PropTypes.bool,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  team: PropTypes.object,
  cardImageShadowColor: PropTypes.string,
  removeDismissAssessmentFromList: PropTypes.func,
  isLocked: PropTypes.bool,
  isLockedForOwner: PropTypes.bool,
  isChecked: PropTypes.bool,
  isShowText: PropTypes.bool,
  isWithFile: PropTypes.any,
  blurredImage: PropTypes.any,
  providerLogo: PropTypes.any,
  completeClickHandler: PropTypes.func
};

CardTileViewV3.defaultProps = {
  hideModal: false,
  channelSetting: false,
  groupSetting: false,
  isPartOfPathway: false
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardTileViewV3);
