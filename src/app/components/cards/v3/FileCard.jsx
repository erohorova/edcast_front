import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import pdfPreviewUrl from '../../../utils/previewPdf';
import addSecurity from '../../../utils/filestackSecurity';

class FileCard extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let params = this.props.params;
    let card = this.props.card;
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    return (
      <div
        className={`card-v3__image-and-skill-container card-v3__image-and-skill-container_file card-list-v3__image-and-skill-container ${
          this.props.classFile && this.props.classFile === 'card-list-v3__file'
            ? 'card-list-v3__audio'
            : ''
        }`}
      >
        {params.audioFileStack ? (
          <audio
            controls
            src={addSecurity(card.filestack[0].url, expireAfter, this.props.currentUser.id)}
            style={{ width: '100%' }}
          />
        ) : (
          !this.props.scormCard &&
          card.filestack.map(file => {
            return (
              <div
                key={file.handle}
                className={`fp fp_preview ${this.props.classFile} ${
                  params.cardType !== 'FILE' || params.isShowTopImage ? 'no-padding' : ''
                }`}
              >
                <span>
                  {params.cardType === 'FILE' && !params.poster ? (
                    <iframe
                      className="filestack-preview-container"
                      height={`${this.props.height}`}
                      width={`${this.props.width || '100%'}`}
                      src={pdfPreviewUrl(file.url, expireAfter, this.props.currentUser.id)}
                      onMouseEnter={() => {
                        this.setState({ [file.handle]: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ [file.handle]: false });
                      }}
                    />
                  ) : (
                    ''
                  )}
                  {!params.isDownloadContentDisabled && this.props.downloadBlock(file)}
                </span>
              </div>
            );
          })
        )}
      </div>
    );
  }
}
FileCard.propTypes = {
  params: PropTypes.object,
  card: PropTypes.object,
  scormCard: PropTypes.any,
  downloadBlock: PropTypes.func,
  classFile: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect()(FileCard);
