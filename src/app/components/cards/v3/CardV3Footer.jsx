import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactStars from 'react-stars';
import { connect } from 'react-redux';

import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import colors from 'edc-web-sdk/components/colors/index';
import CardAnalyticsIcon from 'edc-web-sdk/components/icons/CardAnalytics.v2';

import CheckedIcon from 'edc-web-sdk/components/icons/CheckV3';

import { Permissions } from '../../../utils/checkPermissions';
import abbreviateNumber from '../../../utils/abbreviateNumbers';
import addTabInCardRating from '../../../utils/addTabInCardRating';

import InsightDropDownActions from '../../../components/feed/InsightDropDownActions';

import SmartBiteInfoCommentsBlock from '../../common/SmartBiteInfoCommentsBlock';

class SmartBiteCardFooter extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      actionButtonStyle: {
        verticalAlign: 'middle',
        width: 'auto',
        height: '19px',
        padding: 0
      },
      tooltipActiveBts: {
        marginTop: -20
      },
      iconStyle: {
        height: 19,
        width: 19
      },
      lockLeapIcon: {
        color: '#fff',
        width: '30px',
        height: '30px'
      },
      leapIcon: {
        marginRight: '7px'
      },
      insightDropDownActions: {
        paddingRight: '5px',
        paddingLeft: 0,
        width: 'auto'
      },
      moreIconStyle: {
        height: '1rem',
        width: '1.5625rem',
        left: '-0.1875rem',
        position: 'relative'
      },
      checkedIcon: {
        verticalAlign: 'middle',
        height: '1rem',
        width: '1rem',
        marginRight: '7px',
        marginTop: '-2px',
        color: '#ffffff'
      }
    };
    this.homePagev1 = window.ldclient.variation('home-page-fix-v1', false);
    this.configureCompleteButton = window.ldclient.variation(
      'configurable-colors-for-complete-button',
      false
    );
    this.showMarkAsComplete = window.ldclient.variation('show-markAsComplete-on-visit', true);
  }
  componentDidMount = () => {
    if (this.props.type === 'StandAlone') {
      setTimeout(() => {
        addTabInCardRating(this.props.ratingChanged);
      }, 500);
    }
  };

  renderRatingBlock() {
    return (
      <div className="rating-container card-v3__rating-container">
        <ReactStars
          onChange={this.props.ratingChanged ? this.props.ratingChanged : () => {}}
          edit={this.props.type === 'StandAlone'}
          count={5}
          size={12}
          half={false}
          color1={'#d6d6e1'}
          color2={'#6f708b'}
          className={`relevancyRatingStars card-v3__relevancyRatingStars ${
            this.props.type === 'StandAlone' ? 'relevancyRatingStars_tab' : ''
          }`}
          value={this.props.averageRating ? parseInt(this.props.averageRating) : 0}
        />
        <span className="card-v3__comment-count">
          {this.props.ratingCount > 100
            ? '99+'
            : this.props.ratingCount === 0
            ? ''
            : +this.props.ratingCount}
        </span>
      </div>
    );
  }

  render() {
    let { isPartOfPathway, type, card, params } = this.props;

    let background =
      (this.configureCompleteButton &&
        ((!(card.completionState === 'COMPLETED' || this.props.isCompleted) &&
          colors.markAsCompleteBackground) ||
          ((card.completionState === 'COMPLETED' || this.props.isCompleted) &&
            colors.completedBackground))) ||
      null;

    let textColor =
      (this.configureCompleteButton &&
        ((!(card.completionState === 'COMPLETED' || this.props.isCompleted) &&
          colors.markAsCompleteText) ||
          ((card.completionState === 'COMPLETED' || this.props.isCompleted) &&
            colors.completedText))) ||
      '#ffffff';

    let markAsCompleteDisabledForLink =
      !this.showMarkAsComplete &&
      !this.props.markAsCompleteEnableForLink &&
      (this.props.card &&
        this.props.card.resource &&
        (this.props.card.resource.url ||
          this.props.card.resource.description ||
          this.props.card.resource.fileUrl)) &&
      this.props.card.resource.type !== 'Video' &&
      ((this.props.card.readableCardType &&
        this.props.card.readableCardType.toUpperCase() === 'ARTICLE') ||
        params.cardType === 'ARTICLE') &&
      card.completionState !== 'COMPLETED' &&
      !this.props.isCompleted;

    return (
      !this.props.isCsodCourse && (
        <div
          className={`card-v3__footer my-card__footer card-${
            type ? type.toLowerCase() : 'tile'
          }-v3__footer`}
          onClick={e => {
            e.stopPropagation();
          }}
        >
          <div className="actions-bar__bottom-row">
            {type === 'StandAlone' && Permissions.has('LIKE_CONTENT') && (
              <div className="like-container">
                <button
                  className={`my-icon-button my-icon-button_small tooltip tooltip_bottom-center ${
                    Permissions.has('LIKE_CONTENT') ? '' : 'my-icon-button_prevented_click'
                  }`}
                  onClick={Permissions.has('LIKE_CONTENT') && this.props.cardLikeHandler}
                >
                  {!card.isUpvoted && (
                    <span tabIndex={-1} className="hideOutline" aria-label="like">
                      <LikeIcon />
                    </span>
                  )}
                  {card.isUpvoted && (
                    <span tabIndex={-1} className="hideOutline" aria-label="liked">
                      <LikeIconSelected />
                    </span>
                  )}
                  <span className="tooltiptext">{tr('Like')}</span>
                </button>
                {card.votesCount > 0 && (
                  <small className="count">{abbreviateNumber(card.votesCount)}</small>
                )}
              </div>
            )}
            {!this.props.commentDisabled &&
              !this.props.providerCards &&
              !params.isHideSpecialInfo && (
                <SmartBiteInfoCommentsBlock
                  isCardv3
                  handleCardClicked={this.props.clickOnComments || this.props.handleCardClicked}
                  comments={this.props.comments}
                  isLiveStream={this.props.isLiveStream}
                  commentsCount={this.props.commentsCount}
                />
              )}
            <div className="right-block" style={{ display: 'inherit' }}>
              {type === 'StandAlone' &&
                ((Permissions.has('MARK_AS_COMPLETE') && card.completionState === 'COMPLETED') ||
                  !card.markFeatureDisabledForSource) &&
                (this.props.params.cardType !== 'QUIZ' &&
                  this.props.params.cardType !== 'POLL') && (
                  <button
                    className={`checked-button my-icon-button tooltip tooltip_bottom-center ${
                      card.completionState === 'COMPLETED' || this.props.isCompleted
                        ? 'my-icon-button_mark_incomplete'
                        : `my-icon-button_mark_complete ${
                            markAsCompleteDisabledForLink ? 'mark_as_complete_button_disabled' : ''
                          }`
                    }`}
                    disabled={markAsCompleteDisabledForLink}
                    onClick={this.props.callCompleteClickHandler}
                    style={{ background, color: textColor }}
                  >
                    <CheckedIcon style={this.styles.checkedIcon} color={textColor} />
                    {tr(
                      card.completionState === 'COMPLETED' || this.props.isCompleted
                        ? 'Completed'
                        : 'Mark as Complete'
                    )}
                    {markAsCompleteDisabledForLink && (
                      <span className="tooltiptext">{tr('Please visit link url')}</span>
                    )}
                  </button>
                )}
              {type === 'StandAlone' &&
                this.props.card &&
                this.props.card.author &&
                (params.isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                  <span>
                    <button
                      className="statistic-button my-icon-button my-icon-button_disabled my-icon-button_small tooltip tooltip_bottom-center"
                      onClick={this.props.handleCardAnalayticsModal}
                      aria-label="Card Statistics"
                    >
                      <span tabIndex={-1} className="hideOutline">
                        <CardAnalyticsIcon color={colors.darkGray} />
                      </span>
                      <span className="tooltiptext">{tr('Card Statistics')}</span>
                    </button>
                  </span>
                )}
              {Permissions.has('CAN_RATE') && !params.isHideSpecialInfo && this.renderRatingBlock()}
              {!this.props.hideActions && !this.props.previewMode && !params.isHideSpecialInfo && (
                <InsightDropDownActions
                  isCardV3={true}
                  isPartOfPathway={isPartOfPathway}
                  card={this.props.card}
                  author={this.props.card.author}
                  disableTopics={this.props.disableTopics}
                  dismissible={this.props.dismissible}
                  showTopicToggleClick={this.props.showTopicToggleClick}
                  style={this.styles.insightDropDownActions}
                  isStandalone={this.props.isStandalone}
                  cardUpdated={this.props.cardUpdated.bind(this)}
                  openChannelModal={this.props.toggleChannelModal}
                  openReasonReportModal={this.props.openReasonReportModal}
                  removeCardFromList={this.props.removeCardFromList}
                  iconStyle={this.styles.moreIconStyle}
                  hideComplete={this.props.hideComplete}
                  isCompleted={
                    this.props.card.completionState &&
                    this.props.card.completionState.toUpperCase() === 'COMPLETED'
                  }
                  deleteSharedCard={this.props.deleteSharedCard}
                  channel={this.props.channel}
                  type={type}
                  isShowLeap={
                    this.props.params.isOwner &&
                    this.props.lockPathwayCardFlag &&
                    this.props.isShowLeap
                  }
                  toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                  cardSectionName={this.props.cardSectionName}
                  removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                  callCompleteClickHandler={this.props.callCompleteClickHandler}
                  isStandalonePage={this.props.isStandalonePage}
                  removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
                  markAsCompleteDisabledForLink={markAsCompleteDisabledForLink}
                />
              )}
            </div>
          </div>
        </div>
      )
    );
  }
}

SmartBiteCardFooter.propTypes = {
  isCsodCourse: PropTypes.any,
  card: PropTypes.object,
  isShowLeap: PropTypes.any,
  lockPathwayCardFlag: PropTypes.any,
  toggleHandleLeapModal: PropTypes.any,
  previewMode: PropTypes.any,
  hideActions: PropTypes.any,
  ratingCount: PropTypes.any,
  disableTopics: PropTypes.any,
  isLiveStream: PropTypes.bool,
  averageRating: PropTypes.any,
  channel: PropTypes.any,
  comments: PropTypes.any,
  isCompleted: PropTypes.any,
  deleteSharedCard: PropTypes.any,
  providerCards: PropTypes.any,
  hideComplete: PropTypes.func,
  isStandalone: PropTypes.bool,
  dismissible: PropTypes.bool,
  commentDisabled: PropTypes.bool,
  commentsCount: PropTypes.number,
  handleCardClicked: PropTypes.func,
  cardUpdated: PropTypes.func,
  removeCardFromList: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  toggleChannelModal: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  clickOnComments: PropTypes.func,
  params: PropTypes.object,
  currentUser: PropTypes.object,
  type: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  completeClickHandler: PropTypes.func,
  ratingChanged: PropTypes.func,
  cardStatusButton: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func,
  cardLikeHandler: PropTypes.func,
  callCompleteClickHandler: PropTypes.func,
  author: PropTypes.any,
  markAsCompleteEnableForLink: PropTypes.bool,
  handleCardAnalayticsModal: PropTypes.func,
  isStandalonePage: PropTypes.bool
};

SmartBiteCardFooter.defaultProps = {
  isPartOfPathway: false,
  currentUser: PropTypes.object,
  tooltipPosition: 'bottom'
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(SmartBiteCardFooter);
