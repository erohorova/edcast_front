import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
const RichTextReadOnly = Loadable({
  loader: () => import('../../common/RichTextReadOnly'),
  loading: () => null
});
import { tr } from 'edc-web-sdk/helpers/translations';
import SvgImageResized from '../../common/ImageResized';
import addSecurity from '../../../utils/filestackSecurity';

class CardV3Image extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      }
    };
  }

  render() {
    let { img, card } = this.props;
    let svgStyle = {
      filter: `url(#blur-effect-overview-${card.id})`
    };
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    img = addSecurity(img, expireAfter, this.props.currentUser.id);
    return (
      <div className="main-image card-image-block">
        <div className="card-blurred-background">
          <svg width="100%" height="100%">
            <title>
              <RichTextReadOnly text={card.title || card.message} />
            </title>
            <SvgImageResized
              cardId={`${card.id}`}
              resizeOptions={'height:200'}
              style={svgStyle}
              xlinkHref={img}
              x="-30%"
              y="-30%"
              width="160%"
              height="160%"
            />
            <filter id={`blur-effect-overview-${card.id}`}>
              <feGaussianBlur stdDeviation="10" />
            </filter>
          </svg>
        </div>
        <svg width="100%" height="100%" style={this.styles.mainSvg}>
          <title>
            <RichTextReadOnly text={card.title || card.message} />
          </title>
          <SvgImageResized
            cardId={`${card.id}`}
            resizeOptions={'height:200'}
            xlinkHref={img}
            width="100%"
            style={this.styles.svgImage}
            height="100%"
          />
        </svg>
      </div>
    );
  }
}

CardV3Image.propTypes = {
  card: PropTypes.object,
  img: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect()(CardV3Image);
