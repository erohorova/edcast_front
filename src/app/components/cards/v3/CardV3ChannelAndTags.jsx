import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';

class CardV3ChannelAndTags extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      tooltipbigview: {
        width: '18.75rem',
        whiteSpace: 'pre-wrap',
        textAlign: 'left'
      }
    };
    this.state = {
      currentUserChannelIds: this.props.currentUser.followingChannels
        ? this.props.currentUser.followingChannels.map(c => c.id)
        : []
    };
  }

  visibleChannelList(cardChannels) {
    let currentUserChannels = this.state.currentUserChannelIds;
    let visibleChannels = [];
    cardChannels.map(channel => {
      if (channel.isPrivate) {
        currentUserChannels.indexOf(channel.id) !== -1 && visibleChannels.push(channel);
      } else {
        visibleChannels.push(channel);
      }
    });
    return visibleChannels;
  }

  render() {
    let { card } = this.props;
    let listOfChannelsToDisplay = this.visibleChannelList(card.channels);
    let tags = '',
      channels = '';
    return (
      <div className={`card-${this.props.cardType || 'tile'}-v3__channel-tags`}>
        {card.channels && listOfChannelsToDisplay.length > 0 && (
          <span className="card-v3__reference-info tooltip tooltip_bottom-center tooltiplargetext">
            {tr('Channel:')}
            {'\u00A0'}
            {listOfChannelsToDisplay.map((channel, index) => {
              channels += `${channel.label}${
                index !== listOfChannelsToDisplay.length - 1 ? ',' : ''
              }${'\u00A0'}`;
              if (index > 2) {
                return;
              } else if (index === 2) {
                return (
                  <span className="card-v3__channel-label" key={`channel-${index}`}>
                    ...
                  </span>
                );
              } else {
                return (
                  <span key={`channel-${index}`}>
                    <a
                      className="card-v3__channel-label"
                      title={channel.label}
                      onClick={() => {
                        this.props.dispatch(push(`/channel/${channel.id}`));
                      }}
                    >
                      {channel.label}
                      {index === 0 && index !== listOfChannelsToDisplay.length - 1 && (
                        <span>,</span>
                      )}
                    </a>
                    {'\u00A0'}
                  </span>
                );
              }
            })}
            {(card.tags && card.tags.length) > 0 && <span className="dot" />}
            <span
              className="tooltiptext"
              style={channels.length > 100 ? this.styles.tooltipbigview : {}}
            >
              {channels}
            </span>
          </span>
        )}

        {(card.tags && card.tags.length) > 0 && (
          <span className="card-v3__reference-info tooltip tooltip_bottom-center tooltiplargetext">
            {tr('Tags:')}
            {'\u00A0'}
            {card.tags.map((tag, index) => {
              tags += `${tag.name}${index !== card.tags.length - 1 ? ',' : ''}${'\u00A0'}`;
              if (index > 2) {
                return;
              } else if (index === 2) {
                return (
                  <span className="card-v3__tag-name" key={`tag-${tag.id}`}>
                    ...
                  </span>
                );
              } else {
                return (
                  <span key={`tag-${tag.id}`}>
                    <a className="card-v3__tag-name" title={tag.name}>
                      {tag.name}
                      {index === 0 && index !== card.tags.length - 1 && <span>,</span>}
                    </a>
                    {'\u00A0'}
                  </span>
                );
              }
            })}
            <span
              className="tooltiptext"
              style={tags.length > 100 ? this.styles.tooltipbigview : {}}
            >
              {tags}
            </span>
          </span>
        )}
      </div>
    );
  }
}

CardV3ChannelAndTags.propTypes = {
  card: PropTypes.object,
  cardType: PropTypes.string,
  currentUser: PropTypes.object
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(CardV3ChannelAndTags);
