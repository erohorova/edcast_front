import React from 'react';
import { tr } from 'edc-web-sdk/helpers/translations';

const sharedBy = tr('Shared by');
const assignedBy = tr('Assigned by');

export default class CardV3Assignor extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.assignor && !this.props.sharedBy) {
      return null;
    }

    const assigned = this.props.assignor ? 'assignor' : 'sharedBy';
    const name = this.props[assigned].name
      ? this.props[assigned].name
      : this.props[assigned].fullName;
    const output = assigned === 'assignor' ? assignedBy : sharedBy;
    return (
      <div className="card-v3__assignor">
        {output}: {name}
      </div>
    );
  }
}
