import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import { push } from 'react-router-redux';

import CheckedIcon from 'edc-web-sdk/components/icons/CheckV3';
import PrivateCard from 'edc-web-sdk/components/icons/PrivateCard';

import { Permissions } from '../../../utils/checkPermissions';
import addSecurity from '../../../utils/filestackSecurity';
import BlurImage from '../../common/BlurImage';
import TooltipLabel from '../../common/TooltipLabel';

class CardV3Header extends Component {
  constructor(props, context) {
    super(props, context);

    this.showPublishedDate = window.ldclient.variation('show-published-date', false);
    this.styles = {
      avatarBox: {
        height: '2.4375rem',
        width: '2.4375rem',
        marginRight: '0.75rem',
        position: 'relative'
      },
      checkedIcon: {
        height: '0.625rem'
      },
      privateIcon: {
        height: '0.875rem',
        top: '0',
        position: 'relative'
      }
    };
    this.state = {
      showNew: window.ldclient.variation('show-new-for-card', false),
      isShowUserJob: window.ldclient.variation('is-show-user-job', false),
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true)
    };
    this.showBIA = !!this.props.team.config.enabled_bia;
    this.isCsodCourse = props.card.course_id && props.card.source_type_name === 'csod';
    let uA = window.navigator.userAgent,
      isIE =
        /msie\s|trident\/|edge\//i.test(uA) &&
        !!(
          document.uniqueID ||
          document.documentMode ||
          window.ActiveXObject ||
          window.MSInputMethodContext
        ),
      isMozilla = /Firefox\//.test(uA);
    this.isIeOrMoz = isIE || isMozilla;
  }

  tooltipText = tooltipText => {
    return `<p className="tooltip-text">${tr(tooltipText)}</p>`;
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let { params, card, eclLogoObj, isProvider, isWithFile, type } = this.props;

    let pathname = this.props.pathname;

    let isAuthor = card.author && params.showCreator;

    let isNameCut =
      (card.badging && card.badging.imageUrl) ||
      (this.state.showNew && card.isNew) ||
      card.deletedAt;
    const isECL =
      (!this.isCsodCourse && card.eclSourceLogoUrl && !card.providerImage) ||
      (card.eclSourceTypeName && eclLogoObj !== undefined);

    let paidByUser = card && card.paidByUser;
    const isStandalone = type ? type.toLowerCase() === 'standalone' : '';

    const isCompleted = card.completionState === 'COMPLETED' || this.props.isCompleted;

    let isFeed =
      pathname.indexOf('curate') > -1 ||
      pathname.indexOf('my-assignments') > -1 ||
      pathname.indexOf('featured') > -1 ||
      pathname.indexOf('team-learning') > -1;
    let isPathway = card.cardType === 'pack' || card.cardType === 'journey';
    let jobTitle =
      card.author &&
      (card.author.jobTitle || (card.author.profile && card.author.profile.jobTitle));
    return (
      <div
        className={`card-v3__user-provider ${
          isWithFile ? 'card-v3__user-provider_under-img' : ''
        } card-${type ? type.toLowerCase() : 'tile'}-v3__user-provider
          ${
            isProvider
              ? `card-v3__user-provider_with${isECL ? '-ecl' : ''}-provider`
              : isAuthor
              ? 'card-v3__user-provider_with-user'
              : ''
          }`}
      >
        {isProvider ? (
          <div className={`my-card__header`} onClick={this.props.handleCardClicked}>
            {!params.hideProvider && card.providerImage && (
              <img
                className="my-card__provider-logo"
                src={addSecurity(card.providerImage, expireAfter, this.props.currentUser.id)}
                title={tr('Source of the content')}
              />
            )}
            {!params.hideProvider && isECL && (card.eclSourceLogoUrl || eclLogoObj) && (
              <img
                className="my-card__provider-logo eclSourceLogoUrl"
                onError={el => {
                  el.target.onError = null;
                  el.target.style.display = 'none';
                }}
                title={tr('Source of the content')}
                src={addSecurity(
                  card.eclSourceLogoUrl || eclLogoObj,
                  expireAfter,
                  this.props.currentUser.id
                )}
              />
            )}
          </div>
        ) : (
          <div
            className={`my-card__header
            ${isNameCut ? 'my-card__header_cut' : ''}`}
          >
            {card.author && params.showCreator && (
              <div className="my-card__user-avatar">
                <BlurImage
                  style={this.styles.avatarBox}
                  id={card.id}
                  image={
                    card.author &&
                    (card.author.picture ||
                      (card.author.avatarimages && card.author.avatarimages.small) ||
                      params.defaultUserImage)
                  }
                />
              </div>
            )}

            {card.author && params.showCreator && (
              <div
                className={`author-info-container ${
                  this.state.showNew && card.isNew ? 'author-info-container_short' : ''
                }`}
              >
                <TooltipLabel
                  text={this.tooltipText('Creator of the card')}
                  html={true}
                  position={'top-right'}
                >
                  <a className={`user-name ${this.props.isNameCut ? 'user-name_cutting' : ''}`}>
                    {card.author.fullName || this.props.authorName}
                  </a>
                  {jobTitle && this.state.isShowUserJob && (
                    <span className="user-v3__job-title">{jobTitle}</span>
                  )}
                </TooltipLabel>
                <div className="user-name-popover">
                  <div className="tooltip-snippet info-tooltip-custom">
                    <p className="info-tooltip-header text-center">
                      {card.author.fullName
                        ? card.author.fullName
                        : `${card.author.firstName ? card.author.firstName : ''} ${
                            card.author.lastName ? card.author.lastName : ''
                          }`}
                    </p>
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
        {card.deletedAt && <div className="card-new__deleted card-new__right">{tr('DELETED')}</div>}

        {paidByUser && isStandalone && <span className="card-v3__pricing">{tr('Purchased')}</span>}
        {this.state.showNew && card.isNew && (isFeed || (!isFeed && isPathway)) && (
          <div className="card-new">{tr('NEW')}</div>
        )}
        {!this.props.params.isHideSpecialInfo && (
          <div className={`right-block `}>
            {!card.isPublic && <PrivateCard style={this.styles.privateIcon} color={`#ACADC1`} />}
            {card.isPublic &&
              !isStandalone &&
              ((Permissions.has('MARK_AS_COMPLETE') && card.completionState === 'COMPLETED') ||
                !card.markFeatureDisabledForSource) &&
              (this.props.params.cardType !== 'QUIZ' && this.props.params.cardType !== 'POLL') &&
              (this.state.showMarkAsComplete || isCompleted) && (
                <TooltipLabel
                  text={this.tooltipText(isCompleted ? 'Completed' : 'Mark as Complete')}
                  html={true}
                  position={this.isIeOrMoz ? 'center' : 'bottom'}
                >
                  <button
                    className={`my-icon-button my-icon-button_small card-v3__complete-icon`}
                    onClick={e => {
                      e.stopPropagation();
                      this.props.callCompleteClickHandler(e);
                    }}
                  >
                    <CheckedIcon
                      style={this.styles.checkedIcon}
                      color={isCompleted ? `#38b6a0` : '#acadc1'}
                    />
                  </button>
                </TooltipLabel>
              )}
            {card.badging && card.badging.imageUrl && (
              <div className="card-v3__badge">
                <img
                  title={tr('Badge')}
                  className="my-card__provider-logo"
                  src={addSecurity(card.badging.imageUrl, expireAfter, this.props.currentUser.id)}
                  style={{ marginRight: '5px' }}
                  alt=" "
                />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

CardV3Header.propTypes = {
  showControls: PropTypes.any,
  pathwayEditor: PropTypes.any,
  isNameCut: PropTypes.any,
  allowConsumerModifyLevel: PropTypes.any,
  isCsodCourse: PropTypes.any,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  params: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  rateCard: PropTypes.any,
  standaloneLinkClickHandler: PropTypes.func,
  cardClickHandle: PropTypes.string,
  avatarDiameter: PropTypes.string,
  handleCardClicked: PropTypes.func,
  team: PropTypes.any,
  cardLayoutType: PropTypes.any,
  viewType: PropTypes.string,
  isSlideOutCard: PropTypes.bool,
  closeModal: PropTypes.func,
  callCompleteClickHandler: PropTypes.func,
  pathname: PropTypes.string,
  isWithFile: PropTypes.bool,
  eclLogoObj: PropTypes.string,
  type: PropTypes.string,
  isProvider: PropTypes.bool,
  isCompleted: PropTypes.bool,
  currentUser: PropTypes.object
};

function mapStateToProps(state) {
  return {
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStateToProps)(CardV3Header);
