import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import DurationIcon from 'edc-web-sdk/components/icons/DurationIcon';
import SmartBiteLevel from '../../common/SmartBiteLevel';

class CardV3SkillTypeEffort extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      durationIcon: {
        width: '.5625rem',
        height: '.5625rem',
        marginRight: '0.3125rem'
      }
    };
    this.state = {
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    this.showBIA = !!this.props.team.config.enabled_bia;
  }
  render() {
    let cardType = this.props.card.readableCardType || this.props.params.cardType.toLowerCase();
    if (cardType === 'pack') {
      cardType = 'pathway';
    }
    return (
      <div
        className={`card-v3__skill-type-effort ${
          this.props.isOnTheFile ? 'card-v3__skill-type-effort_on-the-file' : ''
        } ${this.props.isLocked ? 'card-v3__skill-type-effort_on-the-file_blur' : ''}`}
      >
        {this.showBIA && !this.props.params.isHideSpecialInfo && (
          <SmartBiteLevel
            isRevertColor={this.props.isOnTheFile}
            allowConsumerModifyLevel={this.props.allowConsumerModifyLevel}
            params={this.props.params}
            newSkillLevel={this.props.newSkillLevel}
            card={this.props.card}
            rateCard={this.props.rateCard}
            isV3
            type={this.props.type}
          />
        )}
        {!this.props.params.isHideSpecialInfo && (
          <span className="card-v3__card-type" title={tr(cardType)}>
            {tr(cardType)}
          </span>
        )}
        {this.props.card.eclDurationMetadata &&
          this.props.card.eclDurationMetadata.calculated_duration_display &&
          this.props.card.eclDurationMetadata.calculated_duration > 0 &&
          !this.props.params.isHideSpecialInfo && (
            <span className="card-v3__duration">
              <span className="dot" />
              <DurationIcon
                style={this.styles.durationIcon}
                color={this.props.isOnTheFile ? '#ffffff' : '#454560'}
              />
              {this.props.card.eclDurationMetadata.calculated_duration_display.replace(
                ' Months',
                'mon'
              )}
            </span>
          )}
      </div>
    );
  }
}

CardV3SkillTypeEffort.propTypes = {
  allowConsumerModifyLevel: PropTypes.any,
  newSkillLevel: PropTypes.string,
  params: PropTypes.object,
  card: PropTypes.object,
  rateCard: PropTypes.any,
  team: PropTypes.any,
  pathname: PropTypes.string,
  type: PropTypes.string,
  isOnTheFile: PropTypes.bool,
  isOnDetailPage: PropTypes.bool,
  isLocked: PropTypes.any
};

function mapStateToProps(state) {
  return {
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStateToProps)(CardV3SkillTypeEffort);
