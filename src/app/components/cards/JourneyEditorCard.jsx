import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from './Card';

class JourneyEditorCard extends Component {
  constructor(props, context) {
    super(props, context);

    this.isUpdateJourneyCards = true;
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.card !== this.props.card) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.keyIndex !== this.props.keyIndex) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.index !== this.props.index) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.dueAt !== this.props.dueAt) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.startDate !== this.props.startDate) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.author !== this.props.author) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.currentUser !== this.props.currentUser) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.cardsList !== this.props.cardsList) {
      this.isUpdateJourneyCards = true;
    } else if (nextProps.hideEdit !== this.props.hideEdit) {
      this.isUpdateJourneyCards = true;
    } else {
      this.isUpdateJourneyCards = false;
    }
    return this.isUpdateJourneyCards;
  }

  render() {
    return (
      <Card
        card={this.props.card}
        showControls={true}
        key={this.props.keyIndex}
        pathwayEditor={true}
        index={this.props.index}
        dueAt={this.props.dueAt}
        startDate={this.props.startDate}
        author={this.props.author}
        user={this.props.currentUser}
        removeCardFromPathway={this.props.removeCardFromPathway}
        moreCards={true}
        cardsList={this.props.cardsList}
        lockCard={this.props.lockCard}
        pathway={this.props.pathway}
        addToLeap={this.props.addToLeap}
        arrToLeap={this.props.arrToLeap}
        openCardEdit={this.props.openCardEdit}
        hideEdit={this.props.hideEdit}
      />
    );
  }
}

JourneyEditorCard.propTypes = {
  card: PropTypes.object,
  keyIndex: PropTypes.number,
  index: PropTypes.number,
  dueAt: PropTypes.string,
  startDate: PropTypes.string,
  hideEdit: PropTypes.string,
  currentUser: PropTypes.object,
  pathway: PropTypes.any,
  author: PropTypes.any,
  cardsList: PropTypes.object,
  removeCardFromPathway: PropTypes.func,
  lockCard: PropTypes.func,
  addToLeap: PropTypes.func,
  arrToLeap: PropTypes.func,
  openCardEdit: PropTypes.func
};

export default JourneyEditorCard;
