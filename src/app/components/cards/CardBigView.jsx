import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import moment from 'moment';

import colors from 'edc-web-sdk/components/colors/index';
import LockIcon from 'edc-web-sdk/components/icons/Lock';

import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import CheckedIcon from 'material-ui/svg-icons/action/done';

import RichTextEditor from 'react-rte';
import MarkdownRenderer from '../common/MarkdownRenderer';
import SmartBiteCardHeader from '../common/SmartBiteCardHeader';
import SmartBiteCardFooter from '../common/SmartBiteCardFooter';
import DateConverter from '../common/DateConverter';
import SvgImageResized from '../common/ImageResized';
import convertRichText from '../../utils/convertRichText';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import getDefaultImage from '../../utils/getDefaultCardImage';

import VideoStream from '../feed/VideoStream';
import Poll from '../feed/Poll';
import CardModal from '../modals/CardModal';
import CardClassesModal from '../modals/CardClassesModal';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';

class CardBigView extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      videoPlaying: false,
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      lockPathwayCardFlag: window.ldclient.variation('lock-pathway-card', false),
      isCsodCourse: props.card.course_id && props.card.source_type_name === 'csod',
      poster: this.props.params.poster,
      cardImage: this.props.params.cardImage,
      cardSvgBackground: this.props.params.cardSvgBackground
    };

    this.styles = {
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      checkedIcon: {
        width: 70,
        height: '100%'
      },
      videoPlaying: {
        display: 'none'
      },
      playIcon: {
        height: '3.125rem',
        width: '3.125rem'
      },
      bigLockIcon: {
        width: '4.375rem',
        height: '4.375rem'
      },
      lockedCard: {
        background: 'rgba(255, 255, 255, 0.5) no-repeat',
        backgroundSize: '100% 100%'
      },
      audio: {
        width: '100%'
      },
      privateLabel: {
        color: colors.primary
      },
      videoBox: {
        width: '100%',
        height: '100%'
      }
    };
  }

  componentDidMount() {
    setTimeout(() => {
      if (document.querySelector('.big-vew-block .ml-video-cards')) {
        let videoEle = document.querySelectorAll('.big-vew-block .ml-video-cards');
        this.bindElements(videoEle);
      }
    }, 200);
  }

  bindElements = videoEle => {
    for (let i = 0; i < videoEle.length; i++) {
      videoEle[i].onplay = () => {
        this.props.reloadVideo(videoEle[i], videoEle);
        this.props.getAlliframeElements('bigView');
      };
      videoEle[i].onclick = () => {
        this.props.reloadVideo(videoEle[i], videoEle);
        this.props.getAlliframeElements('bigView');
      };
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.props.card.dismissed) {
      return null;
    }

    let { isPartOfPathway } = this.props;

    let averageRating = this.props.card.averageRating;
    let isShowLeftFile = this.props.params.isShowTopImage || this.props.params.fileFileStack;
    let endDate =
      this.props.card.end_date &&
      moment
        .utc(this.props.card.end_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');
    let startDate =
      this.props.card.start_date &&
      moment
        .utc(this.props.card.start_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');
    let cardMsg = convertRichText(this.props.card.title || this.props.card.message);
    let isShowTopTitle =
      ((this.props.card.resource &&
        (this.props.card.resource.url ||
          this.props.card.resource.description ||
          this.props.card.resource.fileUrl)) ||
        this.state.isCsodCourse) &&
      ((this.props.card.message &&
        (this.props.params.cardType === 'ARTICLE' ||
          this.props.card.cardType === 'course' ||
          this.props.params.cardType === 'VIDEO') &&
        this.props.card.message !== this.props.card.resource.title) ||
        (this.state.isCsodCourse && this.props.card.name));

    return (this.state.lockPathwayCardFlag &&
      !!this.props.pathwayDetails &&
      (this.props.card.isLocked || this.props.card.showLocked) &&
      !this.props.params.isPathwayOwner &&
      !this.props.isShowLockedCardContent) ||
      this.props.isPrivate ? (
      <div className="big-vew-block list-view-block">
        <div className={`feed-card-view paper`}>
          <div className="locked-list-card">
            {this.props.isPrivate ? (
              <h4 style={this.styles.privateLabel}>
                {tr('You do not have permission to view this card')}
              </h4>
            ) : (
              <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
            )}
          </div>
          {this.props.showControls && (
            <div className="number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}
        </div>
      </div>
    ) : (
      <div className="big-vew-block paper">
        <div
          onClick={this.props.handleCardClicked}
          className={
            this.props.pathwayChecking && this.props.isCompleted
              ? this.props.params.isShowTopImage ||
                (this.props.params.isFileAttached && !this.props.params.audioFileStack)
                ? 'checking-over-image'
                : 'checking-over'
              : 'hide-checking'
          }
        >
          <CheckedIcon color={colors.primary} style={this.styles.checkedIcon} />
        </div>
        <div
          className={
            this.props.scormCard && this.props.card.hidden
              ? 'scorm-not-uploaded-list'
              : 'hide-checking'
          }
        >
          <div className="scorm-not-uploaded-text">{tr('Course is getting uploaded')}</div>
        </div>
        {this.state.lockPathwayCardFlag &&
          !!this.props.pathwayDetails &&
          (this.props.card.isLocked || this.props.card.showLocked) &&
          this.props.params.isPathwayOwner && (
            <div className="locked-list-card" style={this.styles.lockedCard}>
              <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
            </div>
          )}
        <div className="">
          <SmartBiteCardHeader
            card={this.props.card}
            params={this.props.params}
            isNameCut={this.props.isNameCut}
            pathwayEditor={this.props.pathwayEditor}
            authorName={this.props.authorName}
            cardClickHandle={this.state.cardClickHandle}
            standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
            showControls={this.props.showControls}
            allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
            newSkillLevel={this.props.newSkillLevel}
            rateCard={this.props.rateCard}
            logoObj={this.props.logoObj}
            handleCardClicked={this.props.handleCardClicked}
            cardLayoutType={this.props.type}
            viewType={`bigView`}
          />
          <div className="line_big-view" />
          {isShowLeftFile && (
            <div className="my-card__big-image relative">
              {this.props.card.cardType === 'pack' && (
                <div className="pathway-label-block">
                  {this.props.card.state === 'draft' && (
                    <div className="pathway-draft-label">{tr('DRAFT')}</div>
                  )}
                  <div className="pathway-label">
                    {this.props.card.badging && this.props.card.badging.imageUrl && (
                      <img
                        className="pathway-badge-preview"
                        src={addSecurity(
                          this.props.card.badging.imageUrl,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                      />
                    )}
                    <span>{tr('PATHWAY')}</span>
                  </div>
                </div>
              )}

              {this.props.card.cardType === 'journey' && (
                <div className="pathway-label-block">
                  {this.props.card.state === 'draft' && (
                    <div className="pathway-draft-label">{tr('DRAFT')}</div>
                  )}
                  <div className="pathway-label">
                    {this.props.card.badging && this.props.card.badging.imageUrl && (
                      <img
                        className="pathway-badge-preview"
                        src={addSecurity(
                          this.props.card.badging.imageUrl,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                      />
                    )}
                    <span>{tr('JOURNEY')}</span>
                  </div>
                </div>
              )}
              {isShowTopTitle && (
                <div className="card-title card-title_top">
                  <RichTextReadOnly
                    text={convertRichText(this.props.card.title || this.props.card.message)}
                  />
                  {this.state.isCsodCourse && this.props.card.name && (
                    <h5 className="resource-message">{this.props.card.name}</h5>
                  )}
                </div>
              )}
              <a
                className="card-file-container anchorAlignment"
                onClick={e => {
                  this.props.handleCardClicked(e, true, this.props.params.isYoutubeVideo);
                }}
              >
                {(this.props.params.cardType.toUpperCase() === 'VIDEO' ||
                  this.props.card.cardSubtype === 'video') &&
                  !this.props.params.videoFileStack && (
                    <div>
                      <VideoStream
                        hideTitle={true}
                        startPlay={false}
                        card={this.props.card}
                        allowAutoPlay={this.props.allowAutoplay}
                        getAllvideoElements={this.props.getAllvideoElements}
                        sourceType={`bigView`}
                      />
                      <div className={this.props.params.videoData.videoLabelClass}>
                        {tr(this.props.params.videoData.videoLabelText)}
                      </div>
                    </div>
                  )}
                {this.props.params.videoFileStack ? (
                  <div style={this.styles.videoBox}>
                    {this.state.cardImage ? (
                      <svg width="100%" height="100%" style={this.styles.mainSvg}>
                        <title>
                          <RichTextReadOnly text={cardMsg} />
                        </title>
                        <SvgImageResized
                          cardId={`${this.props.card.id}`}
                          resizeOptions={'height:300'}
                          xlinkHref={addSecurity(
                            this.props.params.poster || this.state.cardImage,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          width="100%"
                          style={this.styles.svgImage}
                          height="100%"
                        />
                      </svg>
                    ) : (
                      <div className="fp fp_video fp_video_card" onClick={this.props.videoPlay}>
                        {this.props.transcodedVideoStatus &&
                          (this.props.transcodedVideoStatus === 'completed' ? (
                            <video
                              id={`video-${this.props.card.filestack[0].handle}`}
                              src={this.props.card.filestack[0].url}
                              poster={addSecurity(
                                this.state.poster,
                                expireAfter,
                                this.props.currentUser.id
                              )}
                              ref={`video`}
                              onPlay={() => {
                                this.refs.video && this.refs.video.setAttribute('controls', '');
                                this.setState({ videoPlaying: true });
                              }}
                              onPause={() => {
                                this.refs.video && this.refs.video.removeAttribute('controls');
                                this.setState({ videoPlaying: false });
                              }}
                              preload={
                                this.props.card.filestack && this.props.card.filestack[1]
                                  ? 'none'
                                  : 'auto'
                              }
                              controlsList={
                                this.props.params.isDownloadContentDisabled ? 'nodownload' : ''
                              }
                              className="ml-video-cards"
                            />
                          ) : (
                            <img
                              className="waiting-video"
                              src="/i/images/video_processing_being_processed.jpg"
                            />
                          ))}
                      </div>
                    )}
                  </div>
                ) : (
                  this.state.poster &&
                  this.props.params.cardType === 'FILE' && (
                    <svg width="100%" height="100%" style={this.styles.mainSvg}>
                      <title>
                        <RichTextReadOnly text={cardMsg} />
                      </title>
                      <SvgImageResized
                        cardId={`${this.props.card.id}`}
                        resizeOptions={'height:300'}
                        xlinkHref={addSecurity(
                          this.state.poster,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                        width="100%"
                        style={this.styles.svgImage}
                        height="100%"
                      />
                    </svg>
                  )
                )}
                {(this.props.params.cardType === 'VIDEO' ||
                  this.props.card.cardSubtype === 'video') &&
                  this.props.params.videoFileStack && (
                    <div>
                      <PlayIcon
                        className="center-play-icon"
                        style={
                          this.state.videoPlaying ? this.styles.videoPlaying : this.styles.playIcon
                        }
                        onClick={this.props.videoPlay}
                        id="play"
                        color={colors.white}
                      />
                    </div>
                  )}
                {this.props.params.cardType !== 'VIDEO' &&
                  this.props.card.cardSubtype !== 'video' &&
                  !this.props.params.fileFileStack && (
                    <svg width="100%" height="100%" style={this.styles.mainSvg}>
                      <title>
                        <RichTextReadOnly text={cardMsg} />
                      </title>
                      <SvgImageResized
                        cardId={`${this.props.card.id}`}
                        resizeOptions={'height:300'}
                        xlinkHref={addSecurity(
                          this.state.cardSvgBackground,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                        width="100%"
                        style={this.styles.svgImage}
                        height="100%"
                      />
                    </svg>
                  )}
                {this.props.params.isShowTopImage && (
                  <div className="card-img button-icon card-blurred-background">
                    <svg width="100%" height="100%">
                      <title>
                        <RichTextReadOnly text={cardMsg} />
                      </title>
                      <SvgImageResized
                        cardId={`${this.props.card.id}`}
                        resizeOptions={'height:300'}
                        style={this.props.svgStyle}
                        xlinkHref={addSecurity(
                          this.state.poster || this.state.cardSvgBackground,
                          expireAfter,
                          this.props.currentUser.id
                        )}
                        x="-30%"
                        y="-30%"
                        width="160%"
                        height="160%"
                      />
                      <filter id={`blur-effect-card-${this.props.card.id}`}>
                        <feGaussianBlur stdDeviation="10" />
                      </filter>
                    </svg>
                  </div>
                )}
                {this.props.scormCard ? (
                  <svg width="100%" height="100%" style={this.styles.mainSvg}>
                    <title>
                      <RichTextReadOnly text={cardMsg} />
                    </title>
                    <SvgImageResized
                      cardId={`${this.props.card.id}`}
                      resizeOptions={'height:300'}
                      xlinkHref={addSecurity(
                        this.state.poster || this.state.cardSvgBackground,
                        expireAfter,
                        this.props.currentUser.id
                      )}
                      width="100%"
                      style={this.styles.svgImage}
                      height="100%"
                    />
                  </svg>
                ) : null}
                {this.props.params.fileFileStack && !this.props.params.isShowTopImage && (
                  <div>
                    <div key={this.props.card.filestack[0].handle} className="fp fp_preview">
                      <span>
                        <iframe
                          className="filestack-preview-container"
                          height="300px"
                          width="100%"
                          src={pdfPreviewUrl(
                            this.props.card.filestack[0].url,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          onMouseEnter={() => {
                            this.setState({ [this.props.card.filestack[0].handle]: true });
                          }}
                          onMouseLeave={() => {
                            this.setState({ [this.props.card.filestack[0].handle]: false });
                          }}
                        />
                        {!this.props.params.isDownloadContentDisabled &&
                          this.props.downloadBlock(this.props.card.filestack[0])}
                      </span>
                    </div>
                  </div>
                )}
              </a>
            </div>
          )}
          <div
            className={`my-card__text-info cursor ${
              isShowLeftFile ? 'my-card__text-info_bordered' : ''
            }`}
          >
            <div className="image-exists vertical-spacing-small">
              <div>
                {this.props.params.cardType === 'TEXT' || this.props.params.cardType === 'IMAGE' ? (
                  <div className="vertical-spacing-large">
                    <div
                      className={
                        this.props.params.imageExists
                          ? `openCardFromSearch button-icon title ${
                              this.props.dueAt ? 'title-with-image-and-due' : 'title-with-image'
                            }`
                          : 'openCardFromSearch button-icon title text-title'
                      }
                    >
                      {this.props.card.title && (
                        <div className="card-title pointer" onClick={this.props.handleCardClicked}>
                          <MarkdownRenderer markdown={this.props.card.title} />
                        </div>
                      )}

                      {this.props.params.message && (
                        <div
                          className="card-title markdown-text pointer"
                          onClick={this.props.handleCardClicked}
                        >
                          <RichTextEditor
                            readOnly={true}
                            value={RichTextEditor.createValueFromString(
                              convertRichText(this.props.card.message),
                              'markdown'
                            )}
                          />
                        </div>
                      )}
                    </div>
                  </div>
                ) : this.props.params.cardType === 'AUDIO' ? (
                  <audio
                    controls
                    src={addSecurity(
                      this.props.card.filestack[0].url,
                      expireAfter,
                      this.props.currentUser.id
                    )}
                    style={this.styles.audio}
                  />
                ) : (
                  <div
                    className={`${!(
                      this.props.params.cardType === 'POLL' || this.props.params.cardType === 'QUIZ'
                    ) && 'vertical-spacing-large'}`}
                  >
                    <div
                      className={`button-icon title ${(this.props.params.cardType === 'POLL' ||
                        this.props.params.cardType === 'QUIZ') &&
                        'quiz-title'} ${
                        this.props.card.author
                          ? this.props.dueAt
                            ? 'has-author-due'
                            : 'has-author'
                          : this.props.dueAt
                          ? 'no-author-due'
                          : 'no-author'
                      }`}
                    >
                      {!!this.props.card.resource &&
                      (!!this.props.card.resource.title ||
                        !!this.props.card.resource.description) ? (
                        <div>
                          {this.props.card.resource.title &&
                            this.props.card.resource.title !== this.props.card.resource.url &&
                            !isShowTopTitle && (
                              <div
                                className="pointer card-details__title_bold"
                                onClick={
                                  this.props.scormCard && this.props.card.hidden
                                    ? null
                                    : this.props.handleCardClicked
                                }
                                dangerouslySetInnerHTML={{ __html: this.props.card.resource.title }}
                              />
                            )}
                          {this.props.card.resource.description &&
                            this.props.card.resource.description !== this.props.params.message && (
                              <div
                                className="text-title pointer"
                                onClick={this.props.handleCardClicked}
                                dangerouslySetInnerHTML={{
                                  __html: this.props.card.resource.description
                                }}
                              />
                            )}
                          {this.props.params.message && this.props.card.title && (
                            <div
                              className="text-title pointer"
                              dangerouslySetInnerHTML={{ __html: this.props.params.message }}
                            />
                          )}
                        </div>
                      ) : (
                        <div>
                          <div
                            onClick={
                              this.props.scormCard && this.props.card.hidden
                                ? null
                                : this.props.handleCardClicked
                            }
                            className={`pointer ${
                              this.props.card.cardType === 'pack' ||
                              this.props.card.cardType === 'journey'
                                ? 'card-details__title_bold'
                                : ''
                            }`}
                            dangerouslySetInnerHTML={{ __html: this.props.params.message }}
                          />
                          {(this.props.card.cardType === 'pack' ||
                            this.props.card.cardType === 'journey') &&
                            !isShowTopTitle &&
                            !!this.props.card.title && (
                              <div
                                className="text-title pointer"
                                onClick={this.props.handleCardClicked}
                                dangerouslySetInnerHTML={{ __html: this.props.card.message }}
                              />
                            )}
                        </div>
                      )}
                    </div>
                    {(this.props.params.cardType === 'POLL' ||
                      this.props.params.cardType === 'QUIZ') &&
                      !this.props.params.isShowTopImage && (
                        <div
                          className="poll-content poll-results-container"
                          onClick={this.props.handleCardClicked}
                        >
                          <Poll card={this.props.card} cardUpdated={this.props.cardUpdated} />
                        </div>
                      )}
                  </div>
                )}
              </div>
              {this.state.isCsodCourse && (
                <div>
                  {this.props.card.status && (
                    <div className="csod-course__info">
                      {tr('Status: ')} {this.props.card.status}
                    </div>
                  )}
                  {startDate && (
                    <div className="csod-course__info">
                      {tr('Assigned Date: ')} {startDate}
                    </div>
                  )}
                  {endDate && (
                    <div className="csod-course__info">
                      {tr('Due Date: ')} {endDate}
                    </div>
                  )}
                  {this.props.card.duration && (
                    <div className="csod-course__info">
                      {tr('Duration: ')} {this.props.card.duration}
                    </div>
                  )}
                </div>
              )}
              {!this.state.isCsodCourse && (
                <div>
                  {this.props.startDate && (
                    <div className="csod-course__info">
                      {tr('Start Date: ')}
                      <DateConverter date={this.props.startDate} isMonthText="true" />
                    </div>
                  )}
                  {this.props.dueAt && (
                    <div className="csod-course__info">
                      {tr('Due Date: ')}
                      <DateConverter date={this.props.dueAt} isMonthText="true" />
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
          <SmartBiteCardFooter
            isCsodCourse={this.state.isCsodCourse}
            edcastPricing={this.state.edcastPricing}
            edcastPlansForPricing={this.state.edcastPlansForPricing}
            card={this.props.card}
            priceData={this.props.priceData}
            isShowLeap={this.props.isShowLeap}
            params={this.props.params}
            lockPathwayCardFlag={this.state.lockPathwayCardFlag}
            toggleHandleLeapModal={this.props.toggleHandleLeapModal}
            previewMode={this.props.previewMode}
            hideActions={this.props.hideActions}
            disableTopics={this.props.disableTopics}
            dismissible={this.props.dismissible}
            providerCards={this.props.providerCards}
            showTopicToggleClick={this.props.showTopicToggleClick}
            isStandalone={this.props.isStandalone}
            cardUpdated={this.props.cardUpdated}
            removeCardFromList={this.props.removeCardFromList}
            hideComplete={this.props.hideComplete}
            isCompleted={this.props.isCompleted}
            deleteSharedCard={this.props.deleteSharedCard}
            channel={this.props.channel}
            type={this.props.type}
            averageRating={averageRating}
            ratingCount={this.props.ratingCount}
            handleCardClicked={this.props.handleCardClicked}
            comments={this.props.comments}
            isLiveStream={this.props.isLiveStream}
            commentsCount={this.props.commentsCount}
            isPartOfPathway={isPartOfPathway}
            openReasonReportModal={this.props.openReasonReportModal}
            clickOnComments={this.props.clickOnComments}
            removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
          />
        </div>
        {this.props.modalOpen && !this.props.withoutCardModal && (
          <CardModal
            logoObj={this.props.logoObj}
            defaultImage={this.state.defaultImage}
            card={this.props.card}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            likeCard={this.props.cardLikeHandler}
          />
        )}
        {this.props.modalClassesOpen && (
          <CardClassesModal
            classes={this.props.card.additional_metadata.classes}
            closeModal={this.props.closeClassesModal}
          />
        )}
      </div>
    );
  }
}

CardBigView.propTypes = {
  comments: PropTypes.array,
  params: PropTypes.object,
  channel: PropTypes.object,
  providerCards: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  priceData: PropTypes.object,
  team: PropTypes.object,
  disableTopics: PropTypes.bool,
  pathway: PropTypes.object,
  arrToLeap: PropTypes.object,
  svgStyle: PropTypes.object,
  cardsList: PropTypes.object,
  isUpvoted: PropTypes.bool,
  showControls: PropTypes.bool,
  isOwner: PropTypes.bool,
  dismissible: PropTypes.bool,
  previewMode: PropTypes.bool,
  userInterest: PropTypes.bool,
  modalClassesOpen: PropTypes.bool,
  withoutCardModal: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  modalOpen: PropTypes.bool,
  isStandalone: PropTypes.bool,
  scormCard: PropTypes.bool,
  moreCards: PropTypes.bool,
  isLiveStream: PropTypes.bool,
  isPrivate: PropTypes.bool,
  isCompleted: PropTypes.bool,
  pathwayEditor: PropTypes.bool,
  showIndex: PropTypes.bool,
  hideActions: PropTypes.bool,
  isShowLeap: PropTypes.any,
  journeyDetails: PropTypes.bool,
  isNameCut: PropTypes.bool,
  showTopic: PropTypes.bool,
  pathwayDetails: PropTypes.bool,
  isLeapModalOpen: PropTypes.bool,
  pathwayChecking: PropTypes.bool,
  fullView: PropTypes.bool,
  pathwayView: PropTypes.bool,
  votesCount: PropTypes.number,
  commentsCount: PropTypes.number,
  ratingCount: PropTypes.number,
  index: PropTypes.number,
  cardLikeHandler: PropTypes.func,
  closeModal: PropTypes.func,
  toggleChannelModal: PropTypes.func,
  updateCommentCount: PropTypes.func,
  toggleHandleLeapModal: PropTypes.func,
  closeClassesModal: PropTypes.func,
  hideComplete: PropTypes.func,
  downloadBlock: PropTypes.func,
  cardUpdated: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  removeCardFromList: PropTypes.func,
  handleCardClicked: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  videoPlay: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  videoPlaying: PropTypes.any,
  controlsElement: PropTypes.any,
  rateCard: PropTypes.any,
  currency: PropTypes.any,
  type: PropTypes.string,
  dueAt: PropTypes.string,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  transcodedVideoStatus: PropTypes.string,
  allowAutoplay: PropTypes.bool,
  startDate: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  getAlliframeElements: PropTypes.func,
  reloadVideo: PropTypes.func,
  getAllvideoElements: PropTypes.func,
  clickOnComments: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func
};

CardBigView.defaultProps = {
  isPartOfPathway: false
};

function mapStoreStateToProps(state) {
  return {
    cards: state.cards.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardBigView);
