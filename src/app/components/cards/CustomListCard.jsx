import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import moment from 'moment';
import CardClassesModal from '../modals/CardClassesModal';
import { tr } from 'edc-web-sdk/helpers/translations';
import DateConverter from '../common/DateConverter';

class CustomListCard extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalOpen: false
    };
    this.deeplink = this.deeplink.bind(this);
  }

  deeplink() {
    let card = this.props.card;
    let classes =
      this.props.card.additional_metadata && this.props.card.additional_metadata.user_courses;
    let has_many_classes = classes && classes.length > 1;

    if (has_many_classes) {
      this.setState({ modalOpen: true });
    } else {
      let url = card.deeplink_url;
      if (classes && classes.length == 1) {
        url = classes[0].deeplink_url;
      }
      window.open(url, '_blank');
    }
  }

  closeModal = () => {
    this.setState({ modalOpen: false }, () => {
      document.body.style.overflow = '';
    });
  };

  render() {
    let card = this.props.card;
    let additional_metadata = card.additional_metadata;
    let status = card.status.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
    let endDate = card.end_date
      ? moment
          .utc(card.end_date, 'YYYY-MM-DD HH:mm:ss')
          .local()
          .format('l')
      : null;
    let assignedDate = additional_metadata && additional_metadata.assigned_date;
    assignedDate = assignedDate
      ? moment
          .utc(assignedDate, 'YYYY-MM-DD HH:mm:ss')
          .local()
          .format('l')
      : null;
    let dueDate = additional_metadata && additional_metadata.due_date;
    dueDate =
      dueDate && card.status !== 'Completed'
        ? moment
            .utc(dueDate, 'YYYY-MM-DD HH:mm:ss')
            .local()
            .format('l')
        : null;
    let startDate = card.start_date
      ? moment
          .utc(card.start_date, 'YYYY-MM-DD HH:mm:ss')
          .local()
          .format('l')
      : null;
    let duration = card.duration;
    return (
      <div className="two-card column">
        <a onClick={this.deeplink}>
          <Paper style={{ padding: '.75rem .75rem', minHeight: '10.5rem', position: 'relative' }}>
            <div className="row">
              <div className="small-3 medium-3 large-3 columns img-container">
                <div
                  className="card-img"
                  style={{
                    height: '167px',
                    backgroundImage:
                      'url("/i/images/courses/course' +
                      (Math.floor(Math.random() * 48) + 1) +
                      '.jpg")',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    margin: '0 auto 0 0',
                    width: '100%'
                  }}
                />
              </div>
              <div className="small-7 medium-7 large-7 columns course-title">
                <div className="title clamp-4">{card.name}</div>
                <div className="course-status" style={{ fontSize: '13px', padding: '36px 0 0 0' }}>
                  {status && (
                    <div className="info">
                      {tr('Status: ')} {status}
                    </div>
                  )}
                  {assignedDate && (
                    <div className="info">
                      {tr('Approved Date: ')} {assignedDate}
                    </div>
                  )}
                  {startDate && (
                    <div className="info">
                      {tr('Start Date: ')} {startDate}
                    </div>
                  )}
                  {dueDate && (
                    <div className="info">
                      {tr('Due Date: ')} {dueDate}
                    </div>
                  )}
                  {endDate && (
                    <div className="info">
                      {tr('Completion Date: ')} {endDate}
                    </div>
                  )}
                  {duration && (
                    <div className="info">
                      {tr('Duration: ')} {duration}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </Paper>
        </a>

        {this.state.modalOpen && (
          <CardClassesModal
            classes={this.props.card.additional_metadata.classes}
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}

CustomListCard.propTypes = {
  card: PropTypes.object
};

export default connect()(CustomListCard);
