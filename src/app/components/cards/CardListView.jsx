import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton/IconButton';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Avatar } from 'material-ui';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});
import SvgImageResized from '../common/ImageResized';
import convertRichText from '../../utils/convertRichText';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import MoveIcon from 'material-ui/svg-icons/maps/zoom-out-map';
import * as logoType from '../../constants/logoTypes';
import getCardParams from '../../utils/getCardParams';
import getCardType from '../../utils/getCardType';
import { Permissions } from '../../utils/checkPermissions';
import addSecurity from '../../utils/filestackSecurity';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

const logoObj = logoType.LOGO;

class CardListView extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      card: props.card,
      defaultImage: '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg'
    };

    this.styles = {
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      moveBtn: {
        height: '1.25rem',
        width: '1.25rem',
        background: '#fff',
        padding: 0,
        cursor: 'move',
        border: '0.0625rem solid'
      },
      moveIcon: {
        width: '1rem',
        height: '1rem',
        transform: 'rotate(45deg)'
      },
      removeBtn: {
        height: '1.25rem',
        width: '1.25rem',
        padding: 0,
        border: '0.0625rem solid'
      },
      removeIcon: {
        width: '1.25rem',
        height: '1.25rem'
      },
      avatar: {
        marginRight: '0.5625rem'
      },
      audio: {
        width: '100%'
      },
      privateLabel: {
        height: '2.875rem',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
      },
      privateIndex: {
        position: 'absolute',
        left: '0'
      },
      privateTitle: {
        color: colors.primary
      }
    };
    this.isDownloadContentDisabled =
      props.team &&
      props.team.OrgConfig &&
      props.team.OrgConfig.content &&
      props.team.OrgConfig.content['web/content/disableDownload'] &&
      props.team.OrgConfig.content['web/content/disableDownload'].value;
  }

  openCardEdit = e => {
    this.props.openCardEdit(e, this.props.card, this.props.index);
  };

  refreshAudioUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let audio = document.getElementsByClassName('audio-preview')[0];
          let seen = audio.currentTime;
          audio.setAttribute('src', newUrl);
          audio.currentTime = seen;
          if (seen !== 0) {
            audio.play();
          }
        })
        .catch(err => {
          console.error(`Error in CardListView.refreshAudioUrl.refreshFilestackUrl.func : ${err}`);
        });
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let card = this.props.card;
    let params = getCardParams(card, this.props, 'cardListView') || {};
    let svgStyle = {
      filter: `url(#blur-effect-list-${card.id})`
    };
    let cardType = getCardType(card) || '';
    return (
      <div className="list-block">
        <div className="list-icons move-card-btn">
          <IconButton style={this.styles.moveBtn} iconStyle={this.styles.moveIcon}>
            <MoveIcon color="#454560" />
          </IconButton>
        </div>
        <div className="list-icons">
          <IconButton
            onTouchTap={this.props.removeCardFromPathway}
            style={this.styles.removeBtn}
            iconStyle={this.styles.removeIcon}
            aria-label="close"
          >
            <CloseIcon viewBox="1 1 24 24" color="#454560" />
          </IconButton>
        </div>
        <div
          className={`${this.props.openCardEdit ? 'cursor-pointer' : ''} list-main-card`}
          onClick={this.openCardEdit}
        >
          {this.props.isPrivate ? (
            <div style={this.styles.privateLabel}>
              <div className="small-1 text-center" style={this.styles.privateIndex}>
                <div className="card-index">
                  <span>{this.props.index}</span>
                </div>
              </div>
              <p style={this.styles.privateTitle}>
                {tr('You do not have permission to view this card')}
              </p>
            </div>
          ) : (
            <div className="row">
              <div className="small-1 text-center">
                <div className="card-index">
                  <span>{this.props.index}</span>
                </div>
              </div>
              <div className="small-2 medium-1 text-center">
                {params.isShowTopImage && (
                  <div className="card-img-container">
                    <div className="card-img button-icon card-blurred-background">
                      <svg width="100%" height="100%">
                        <title>
                          <RichTextReadOnly text={convertRichText(card.title || card.message)} />
                        </title>
                        <SvgImageResized
                          style={svgStyle}
                          resizeOptions={'height:300'}
                          xlinkHref={addSecurity(
                            params.cardSvgBackground,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          x="-30%"
                          y="-30%"
                          width="160%"
                          height="160%"
                          cardId={`${card.id}`}
                        />
                        <filter id={`blur-effect-list-${card.id}`}>
                          <feGaussianBlur stdDeviation="10" />
                        </filter>
                      </svg>
                    </div>
                    {params.videoFileStack ? (
                      <div className="fp fp_video fp_video_card">
                        <video
                          src={card.filestack[0].url}
                          poster={addSecurity(
                            params.poster,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                          controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
                        />
                      </div>
                    ) : (
                      <svg width="100%" height="100%" style={this.styles.mainSvg}>
                        <title>
                          <RichTextReadOnly text={convertRichText(card.message)} />
                        </title>
                        <SvgImageResized
                          xlinkHref={addSecurity(
                            params.cardSvgBackground,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          resizeOptions={'height:300'}
                          width="100%"
                          style={this.styles.svgImage}
                          height="100%"
                          cardId={`${card.id}`}
                        />
                      </svg>
                    )}
                    {(params.cardType === 'VIDEO' || card.cardSubtype === 'video') && (
                      <div>
                        <PlayIcon className="center-play-icon" color={colors.white} />
                      </div>
                    )}
                  </div>
                )}
              </div>
              <div className="small-1 text-center">
                <Avatar
                  src={
                    card.author &&
                    (card.author.picture ||
                      (card.author.avatarimages && card.author.avatarimages.small) ||
                      params.defaultUserImage)
                  }
                  size={26}
                  style={this.styles.avatar}
                />
              </div>
              <div className="small-2 medium-1">
                {(card.eclSourceLogoUrl ||
                  (card.eclSourceTypeName && logoObj[card.eclSourceTypeName] !== undefined)) && (
                  <div className="inline-block">
                    <img
                      className="provider-logo"
                      src={addSecurity(
                        card.eclSourceLogoUrl || logoObj[card.eclSourceTypeName],
                        expireAfter,
                        this.props.currentUser.id
                      )}
                    />
                  </div>
                )}
                {params.iconFileSrc && (
                  <div className="inline-block">
                    <img
                      className="provider-logo "
                      src={addSecurity(params.iconFileSrc, expireAfter, this.props.currentUser.id)}
                    />
                  </div>
                )}
                <div className="inline-block">
                  <div className="label-text">
                    {tr((this.props.card.readableCardType || cardType).toUpperCase())}
                  </div>
                </div>
              </div>
              <div className="small-6 medium-8">
                {params.audioFileStack ? (
                  <audio
                    controls
                    className="audio-preview"
                    onError={() => {
                      this.refreshAudioUrl(card.filestack[0].handle);
                    }}
                    src={addSecurity(card.filestack[0].url, expireAfter, this.props.currentUser.id)}
                    style={this.styles.audio}
                  />
                ) : (
                  <div className="card-message markdown-text">
                    <div className="markdown-container">
                      <RichTextReadOnly
                        text={convertRichText(
                          params.message.length > 260
                            ? params.message.substr(0, 260) + '...'
                            : params.message
                        )}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          )}
          {this.props.hideEdit && (
            <div className="card-hide-edit tooltip">
              <span className="tooltiptext">{tr(this.props.hideEdit)}</span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

CardListView.propTypes = {
  card: PropTypes.object,
  removeCardFromPathway: PropTypes.func,
  openCardEdit: PropTypes.func,
  isPrivate: PropTypes.bool,
  pathwayEditor: PropTypes.bool,
  hideEdit: PropTypes.string,
  index: PropTypes.number,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardListView);
