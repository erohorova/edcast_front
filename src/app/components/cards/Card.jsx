import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import find from 'lodash/find';
import Loadable from 'react-loadable';

import Download from 'edc-web-sdk/components/icons/Download';
import { fetchCard, rateCard } from 'edc-web-sdk/requests/cards';
import { getUserById } from 'edc-web-sdk/requests/users';
import LockIcon from 'edc-web-sdk/components/icons/Lock';
import UnlockIcon from 'edc-web-sdk/components/icons/Unlock';
import LeapIcon from 'edc-web-sdk/components/icons/Leap';
import { recordVisit } from 'edc-web-sdk/requests/analytics';

import getTranscodedVideo from '../../utils/getTranscodedVideo';
import IconButton from 'material-ui/IconButton/IconButton';
import FlatButton from 'material-ui/FlatButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import MoveIcon from 'material-ui/svg-icons/maps/zoom-out-map';

import { toggleLikeCardAsync, updateCurrentCard, loadComments } from '../../actions/cardsActions';
import {
  openPathwayOverviewModal,
  openSmartBiteOverviewModal,
  openJourneyOverviewModal,
  openJourneyOverviewModalV2,
  openSlideOutCardModal
} from '../../actions/modalActions';
import {
  removeConsumptionPathway,
  saveConsumptionPathwayHistoryURL
} from '../../actions/pathwaysActions';
import {
  removeConsumptionJourney,
  saveConsumptionJourneyHistoryURL
} from '../../actions/journeyActions';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';

import { Permissions } from '../../utils/checkPermissions';
import checkResources from '../../utils/checkResources';
import getCardParams from '../../utils/getCardParams';
import getDefaultImage from '../../utils/getDefaultCardImage';
import linkPrefix from '../../utils/linkPrefix';
import cardMarkAsComplete from '../../utils/cardMarkAsComplete';

import * as logoType from '../../constants/logoTypes';
import * as upshotActions from '../../actions/upshotActions';

let LocaleCurrency = require('locale-currency');

const logoObj = logoType.LOGO;

const CardTile = Loadable({
  loader: () => import('./CardTileView'),
  loading: () => null
});

const CardTileV3 = Loadable({
  loader: () => import('./v3/CardTileViewV3'),
  loading: () => null
});

const CardList = Loadable({
  loader: () => import('./CardFeedView'),
  loading: () => null
});

const CardListV3 = Loadable({
  loader: () => import('./v3/CardListViewV3'),
  loading: () => null
});

const CardBig = Loadable({
  loader: () => import('./CardBigView'),
  loading: () => null
});

const CardBigV3 = Loadable({
  loader: () => import('./v3/CardBigViewV3'),
  loading: () => null
});

class Card extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      card: props.card,
      pendingLike: false,
      isUpvoted: props.card.isUpvoted,
      votesCount: props.card.votesCount,
      commentsCount: props.card.commentsCount,
      defaultImage: getDefaultImage(props.currentUser.id).url,
      modalOpen: false,
      isCompleted:
        props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED',
      showTopic: false,
      isShowAllTags: false,
      hideActions: this.props.hideActions,
      commentDisabled: this.props.commentDisabled,
      isLeapModalOpen: false,
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      isNewTileCard: window.ldclient.variation('new-ui-tile-card', false),
      isSlideOutCard: window.ldclient.variation('slide-out-card', false),
      bigCardView: window.ldclient.variation('big-card-view', false),
      isCardV3:
        props.isCardV3 === undefined ? window.ldclient.variation('card-v3', false) : props.isCardV3,
      journeyEnhancement: window.ldclient.variation('journey-enhancement', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false),
      ratingCount: props.card.allRatings || 0,
      comments: [],
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      isLiveStream: false
    };
    this.styles = {
      close: {
        height: '100%',
        width: '100%',
        background: '#000',
        padding: 0
      },
      move: {
        height: '100%',
        width: '100%',
        background: '#000',
        padding: 0,
        cursor: 'move'
      },
      moveIcon: {
        width: '1rem',
        height: '1rem',
        transform: 'rotate(45deg)'
      },
      iconClose: {
        width: '1.25rem',
        height: '1.25rem'
      },
      download: {
        width: '100%',
        height: '100%',
        padding: 0
      },
      tooltipStyles: {
        left: '-50%'
      },
      lockLeapIcon: {
        color: '#fff',
        width: this.state.isCardV3 ? '1.375rem' : '1.875rem',
        height: this.state.isCardV3 ? '1.375rem' : '1.875rem'
      },
      lockIcon: {
        marginLeft: '0.5625rem'
      },
      leapIcon: {
        marginRight: '0.4375rem'
      },
      lockBtnLabel: {
        paddingLeft: '0.0625rem'
      },
      leapBtnLabel: {
        right: this.state.isNewTileCard ? '-0.75rem' : '-0.375rem',
        paddingLeft: this.state.isNewTileCard ? '1rem' : '0.375rem',
        paddingRight: this.state.isNewTileCard ? '0.5rem' : '0'
      }
    };
    this.cardLikeHandler = this.cardLikeHandler.bind(this);
    this.lockPathwayCardFlag = window.ldclient.variation('lock-pathway-card', false);
    this.defaulCardImageShadowColor = '#454560';
  }

  componentDidMount() {
    let setStateObject = {};
    let ratingCount = this.allCountRating(this.state.card);
    if (ratingCount !== null) {
      setStateObject['ratingCount'] = ratingCount;
    }
    if (this.props.card) {
      if (this.props.card.cardType === 'video_stream') {
        let isLiveStream = false;
        if (this.props.card.videoStream !== undefined) {
          isLiveStream = this.props.card.videoStream.status === 'live';
          setStateObject['isLiveStream'] = isLiveStream;
        } else if (this.props.card.status) {
          isLiveStream = this.props.card.status === 'live';
          setStateObject['isLiveStream'] = isLiveStream;
        }
      }
      if (this.state.isCardV3 && this.state.commentsCount > 0) {
        this.fetchComments();
      }
    }

    // If we receive a user_id from Smartsearch, we need to update that into the card.
    if (!this.props.card.author && this.props.card.user_id) {
      let cardWithAuthor = this.state.card;
      getUserById(this.props.card.user_id)
        .then(data => {
          cardWithAuthor.author = data;
          this.setState({ card: cardWithAuthor });
        })
        .catch(err => {
          console.error(`Error in Card.ComponentDidMount.getUserById.func: ${err}`);
        });
    }

    if (!isEmpty(setStateObject)) {
      this.setState(setStateObject);
    }
  }

  fetchComments = () => {
    let cardId = this.state.card.cardId || this.state.card.id;
    let isECL = /^ECL-/.test(cardId);
    if (!isECL && this.state.commentsCount) {
      loadComments(
        cardId,
        this.state.commentsCount,
        this.state.card.cardType,
        cardId ? cardId : null
      )
        .then(
          data => {
            this.setState({ comments: data });
          },
          error => {
            console.warn(`error when loadComments for card ${cardId}. Error: ${error}`);
          }
        )
        .catch(err => {
          console.error(`Error in CardOverviewModal.loadComments.func: ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  };

  componentWillMount() {
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  setStatePending = (state, val) => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  componentWillReceiveProps(nextProps) {
    let setStateObject = {};
    let ratingCount = this.allCountRating(this.state.card);
    if (ratingCount !== null) {
      setStateObject['ratingCount'] = ratingCount;
    }
    let isCompleted =
      nextProps.card.completionState &&
      nextProps.card.completionState.toUpperCase() === 'COMPLETED';
    let updatedCard = nextProps.card;
    let cardId = this.state.card.id;
    if (isCompleted !== undefined && isCompleted !== this.state.isCompleted) {
      setStateObject['isCompleted'] = isCompleted;
    }

    for (let key in nextProps.cards) {
      if (nextProps.cards.hasOwnProperty(key) && nextProps.cards[key].eclId) {
        if (this.state.card.eclId == nextProps.cards[key].eclId) {
          if (/^ECL-/.test(this.state.card.id) && this.state.card.id != nextProps.cards[key].id) {
            cardId = nextProps.cards[key].id;
          } else {
            cardId = this.state.card.id;
          }
        }
      }
    }

    if (nextProps.cards[cardId]) {
      updatedCard = nextProps.cards[cardId];
      if (
        updatedCard.dismissed &&
        (this.props.pathwayDetails || this.props.type !== 'Tile') &&
        this.props.removeCardFromList
      ) {
        this.props.removeCardFromList(cardId);
      }
    }

    //code only in Card file so only for Tile view
    if (
      this.props.type === 'Tile' &&
      nextProps.pathways.updateCountCardsInPathways &&
      nextProps.pathways.updateCountCardsInPathways.length
    ) {
      let el = nextProps.pathways.updateCountCardsInPathways.find(x => x.id == nextProps.card.id);
      updatedCard = el ? el.pathway : updatedCard;
    }

    let setStateFlag = false;
    let propsChange =
      !isEqual(this.state.card, updatedCard) ||
      this.state.commentsCount !== nextProps.card.commentsCount ||
      this.state.isUpvoted !== nextProps.card.isUpvoted ||
      this.state.votesCount !== nextProps.card.votesCount;

    if (propsChange) {
      setStateFlag = true;
    } else if (!isEmpty(setStateObject)) {
      setStateFlag = true;
    }

    if (setStateFlag) {
      setStateObject['card'] = updatedCard;
      setStateObject['commentsCount'] = nextProps.card.commentsCount;
      setStateObject['isUpvoted'] = nextProps.card.isUpvoted;
      setStateObject['votesCount'] = nextProps.card.votesCount;
      this.setState(setStateObject);
    }
  }

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise((resolve, reject) => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in Card.asyncDispatch.func: ${err}`);
        });
    });
  };

  toggleHandleLeapModal = () => {
    this.setState(prevState => {
      return {
        isLeapModalOpen: !prevState.isLeapModalOpen
      };
    });
  };

  async cardLikeHandler() {
    if (this.state.pendingLike) {
      return;
    }
    await this.setStatePending('pendingLike', true);
    await this.asyncDispatch(
      toggleLikeCardAsync,
      this.state.card.id,
      this.state.card.cardType,
      !this.state.isUpvoted
    )
      .then(() => {
        const votesCount = Math.max(this.state.votesCount + (!this.state.isUpvoted ? 1 : -1), 0);
        let card = this.state.card;
        card.votesCount = votesCount;
        this.setState({ votesCount, isUpvoted: !this.state.isUpvoted, card });
        if (this.state.upshotEnabled) {
          let name = (card.resource && card.resource.title) || card.message;
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
            name: name,
            category: this.state.card.cardType,
            type: this.state.card.cardSubtype,
            description: this.state.card.resource.description,
            status: !!this.state.card.completionState
              ? this.state.card.completionState
              : 'Incomplete',
            rating: this.state.card.averageRating,
            like: this.state.isUpvoted ? 'Yes' : 'No',
            event: this.state.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
          });
        }
      })
      .catch(err => {
        console.error(`Error in Card.asyncDispatch.toggleLikeCardAsync: ${err}`);
      });
    await this.setStatePending('pendingLike', false);
  }

  updateCommentCount = count => {
    this.setState({ commentsCount: count });
  };

  isACurriculumCard = () => {
    let isPathwayStandAlone = document.location.pathname.indexOf('/pathways/') === 0;
    return isPathwayStandAlone && this.props.card.cardType == 'journey';
  };

  openCardInNewTab = () => {
    window.open('/' + `${linkPrefix(this.state.card.cardType)}/${this.props.card.slug}`, '_blank');
    return;
  };

  cardUpdated = () => {
    if (this.state.card && !this.props.isPathwayCard) {
      fetchCard(this.state.card.id)
        .then(data => {
          this.setState({
            card: data,
            isCompleted: data.completionState && data.completionState.toUpperCase() === 'COMPLETED',
            isUpvoted: data.isUpvoted,
            votesCount: data.votesCount,
            commentsCount: data.commentsCount
          });
          if (!this.props.isNotNeedUpdateCardReducer) {
            this.props.dispatch(updateCurrentCard(data));
          }
          this.props.closeCardModal && this.props.closeCardModal(this.state.card.id);
          this.props.cardUpdated && this.props.cardUpdated(data);
        })
        .catch(err => {
          console.error(`Error in Card.fetchCard: ${err}`);
        });
    } else if (this.state.card && this.props.isPathwayCard) {
      this.props.closeCardModal && this.props.closeCardModal(this.state.card.id);
    }
  };

  handleCardClicked = (e, isOpenResourcePage, isYoutubeVideo) => {
    /**
     * Request: Sagar Bhute, EP-11962
     */
    if (this.props.discoverUpshotEventSend !== undefined && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Card',
        name: this.state.card.resource.title,
        event: 'Card Clicked'
      });
    }

    if (this.props.sendLearningUpshotEvent !== undefined && this.state.upshotEnabled) {
      this.props.sendLearningUpshotEvent({
        name: this.state.card.resource.title,
        event: 'Assigned Learning Card Clicked'
      });
    }

    if (
      this.props.sendLearnPageUpshotEvent !== undefined &&
      this.props.feedKey !== undefined &&
      this.state.upshotEnabled
    ) {
      this.props.sendLearnPageUpshotEvent(this.props.feedKey, {
        screenName: 'Learn',
        category: 'Card',
        name: this.state.card.resource.title,
        event: 'Card Clicked'
      });
    }

    if (this.state.upshotEnabled) {
      let name =
        (this.state.card.resource && this.state.card.resource.title) || this.state.card.message;
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
        name: name,
        category: this.state.card.cardType,
        type: this.state.card.cardSubtype,
        description: this.state.card.resource.description,
        status: !!this.state.card.completionState ? this.state.card.completionState : 'Incomplete',
        rating: this.state.card.averageRating,
        like: this.state.isUpvoted ? 'Yes' : 'No',
        event: 'Card Clicked'
      });
    }
    if (isOpenResourcePage) {
      e && e.preventDefault();
    }
    e && e.stopPropagation();
    if (this.state.cardClickHandle === 'resource') {
      if (
        this.state.card &&
        this.state.card.resource &&
        this.state.card.resource.url &&
        !isYoutubeVideo &&
        isOpenResourcePage
      ) {
        return window.open(this.state.card.resource.url, '_blank');
      } else {
        return this.standaloneLinkClickHandler();
      }
    }
    /**
     * Request: Udit, EP-9088, 12/22/17
     * Notes: Originally, all cards would go to standalone, now pathway cards must open modal
     */
    let isPathwayStandAlone = document.location.pathname.indexOf('/pathways/') === 0;
    let isJourneyStandAlone = document.location.pathname.indexOf('/journey/') === 0;
    let type = isJourneyStandAlone || isPathwayStandAlone ? 'modal' : this.state.cardClickHandle;

    if (this.isACurriculumCard()) {
      this.openCardInNewTab();
      return;
    }

    if (this.props.pathwayEditor) {
      this.props.openCardEdit(e, this.state.card, this.props.index);
    }
    if (this.props.cardSplat) {
      this.openModal(e);
    } else {
      switch (type) {
        case 'standalone':
          window.history.pushState(null, null, window.location.pathname + window.location.search);
          this.standaloneLinkClickHandler();
          break;
        case 'modal':
        default:
          try {
            recordVisit(this.state.card.id);
            /*eslint no-shadow: "off"*/
          } catch (e) {}
          this.openModal(e);
      }
    }
  };

  clickOnComments = (e, isOpenResourcePage, isYoutubeVideo) => {
    /**
     * EP-10488: add slide out for tile card view by Add comment
     */
    if (this.state.isSlideOutCard) {
      let authorName = '';
      if (this.state.card.author) {
        authorName = `${this.state.card.author.firstName ? this.state.card.author.firstName : ''} ${
          this.state.card.author.lastName ? this.state.card.author.lastName : ''
        }`;
      }
      this.props.dispatch(
        openSlideOutCardModal(
          this.state.card,
          authorName,
          this.cardUpdated,
          this.state.defaultImage,
          logoObj,
          this.props.dismissible,
          this.state.ratingCount,
          this.handleCardClicked,
          this.standaloneLinkClickHandler,
          this.rateCard,
          this.props.providerLogos
        )
      );
    } else {
      this.handleCardClicked(e, isOpenResourcePage, isYoutubeVideo);
    }
  };

  closeSlideOut = () => {
    /**
     * EP-10488: add slide out for tile card view by Add comment
     */
    this.setState({ slideOutCardModalOpen: false });
  };

  openModal = e => {
    if (
      !this.props.pathwayEditor &&
      !this.props.hideModal &&
      !(e && e.target && e.target.name && e.target.name === 'marked-link')
    ) {
      if (this.props.pathwayDetails) {
        if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
          if (this.state.card.id === undefined) {
            this.state.card.id = 'private-card';
          }
          let linkPrefixValue = linkPrefix(this.props.pathwayDetails.cardType);
          this.props.dispatch(
            push(
              `/${linkPrefixValue}/${this.props.pathwayDetails.slug}/cards/${this.state.card.id}`
            )
          );
        } else {
          this.props.dispatch(
            openPathwayOverviewModal(
              this.props.pathwayDetails,
              logoObj,
              this.state.defaultImage,
              this.cardUpdated.bind(this),
              null,
              this.state.card.id,
              this.props.channelSetting,
              this.props.deleteSharedCard,
              this.props.groupSetting,
              this.props.isStandaloneModal,
              this.props.dueAt,
              this.props.startDate
            )
          );
        }
      } else if (this.state.card.cardType === 'pack') {
        if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
          let linkPrefixValue = linkPrefix(this.state.card.cardType);
          let pathwayBackUrl = window.location.pathname;
          if (this.props.pathways && this.props.pathways.consumptionPathway) {
            this.props.dispatch(removeConsumptionPathway(pathwayBackUrl));
          } else {
            this.props.dispatch(saveConsumptionPathwayHistoryURL(pathwayBackUrl));
          }
          this.props.dispatch(
            push(`/${linkPrefixValue}/${this.state.card.slug || this.state.card.id}`)
          );
        } else {
          this.props.dispatch(
            openPathwayOverviewModal(
              this.state.card,
              logoObj,
              this.state.defaultImage,
              this.cardUpdated.bind(this),
              null,
              null,
              this.props.channelSetting,
              this.props.deleteSharedCard,
              this.props.groupSetting,
              false,
              this.props.dueAt,
              this.props.startDate
            )
          );
        }
      } else if (this.props.journeyDetails || this.state.card.cardType === 'journey') {
        if (this.state.journeyEnhancement) {
          if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
            if (this.state.card.id === undefined) {
              this.state.card.id = 'private-card';
            }
            if (this.props.journeyDetails) {
              let linkPrefixValue = linkPrefix(this.props.journeyDetails.cardType);
              this.props.dispatch(
                push(
                  `/${linkPrefixValue}/${this.props.journeyDetails.slug}/cards/${
                    this.state.card.id
                  }`
                )
              );
            } else {
              let linkPrefixValue = linkPrefix(this.state.card.cardType);
              let journeyBackUrl = window.location.pathname;
              if (this.props.journeyDetails && this.props.journeyDetails.consumptionJourney) {
                this.props.dispatch(removeConsumptionJourney(journeyBackUrl));
              } else {
                this.props.dispatch(saveConsumptionJourneyHistoryURL(journeyBackUrl));
              }
              this.props.dispatch(
                push(`/${linkPrefixValue}/${this.state.card.slug || this.state.card.id}`)
              );
            }
          } else {
            this.props.dispatch(
              openJourneyOverviewModalV2(
                this.props.journeyDetails || this.state.card,
                logoObj,
                this.state.defaultImage,
                this.cardUpdated.bind(this),
                null,
                this.props.index,
                this.props.channelSetting,
                this.props.deleteSharedCard,
                this.props.groupSetting,
                this.props.currentSection,
                this.props.inStandalone,
                this.props.isStandaloneModal,
                this.props.dueAt,
                this.props.startDate
              )
            );
          }
        } else {
          this.props.dispatch(
            openJourneyOverviewModal(
              this.props.journeyDetails || this.state.card,
              logoObj,
              this.state.defaultImage,
              this.cardUpdated.bind(this),
              null,
              this.props.index,
              this.props.channelSetting,
              this.props.deleteSharedCard,
              this.props.groupSetting,
              this.props.currentSection,
              this.props.inStandalone,
              this.props.isStandaloneModal,
              this.props.dueAt,
              this.props.startDate
            )
          );
        }
      } else if (this.state.card.cardType !== 'pack') {
        this.props.dispatch(
          openSmartBiteOverviewModal(
            this.state.card,
            logoObj,
            this.state.defaultImage,
            this.cardUpdated.bind(this),
            null,
            this.props.channelSetting,
            this.props.deleteSharedCard,
            this.props.groupSetting,
            this.props.dueAt,
            this.props.startDate
          )
        );
      } else {
        this.setState({ modalOpen: true }, () => {
          document.body.style.overflow = 'hidden';
        });
        if (this.props.getCardProps) {
          let data = {
            logoObj: logoObj,
            defaultImage: this.state.defaultImage,
            card: this.state.card,
            isUpvoted: this.state.isUpvoted,
            updateCommentCount: this.updateCommentCount,
            commentsCount: this.state.commentsCount,
            votesCount: this.state.votesCount,
            closeModal: this.closeModal,
            modalOpen: this.state.modalOpen,
            likeCard: this.cardLikeHandler
          };
          this.props.getCardProps(data);
        }
      }
    }
  };

  closeModal = () => {
    this.setState({ modalOpen: false }, () => {
      document.body.style.overflow = '';
    });
    if (this.props.closeCardModal) {
      this.props.closeCardModal(this.state.card.id);
    }
    if (this.props.voidCardProps) {
      this.props.voidCardProps();
    }
  };

  standaloneLinkClickHandler = () => {
    if (!this.props.pathwayEditor) {
      let linkPrefixValue = linkPrefix(this.state.card.cardType);
      if (
        this.props.pathname === `/${linkPrefixValue}/${this.state.card.slug}` ||
        this.props.pathname === `/${linkPrefixValue}/${this.state.card.id}`
      ) {
        return;
      }
      let cardType = linkPrefixValue === 'video_streams' ? 'insights' : linkPrefixValue;
      let dataCard = {};
      if ((this.props.pathwayDetails || this.props.journeyDetails) && this.props.cardSplat) {
        dataCard = {
          fromType: this.props.journeyDetails ? 'journey' : 'pathways',
          cardSplat: this.props.cardSplat
        };
      }
      // If coming from Smarsearch, we need to add a slug
      // Mutating state not an issue
      if (!this.state.card.slug) {
        this.state.card.slug = this.state.card.id;
      }
      let audioElements = document.getElementsByTagName('audio');
      let videoElements = document.getElementsByTagName('video');
      let iframes = document.getElementsByTagName('iframe');
      if (audioElements && audioElements.length) {
        let audioList = Array.prototype.slice.call(audioElements);
        audioList.forEach(item => {
          item.pause();
        });
      }
      if (videoElements && videoElements.length) {
        let videoList = Array.prototype.slice.call(videoElements);
        videoList.forEach(item => {
          item.pause();
        });
      }
      if (iframes && iframes.length) {
        let iframesList = Array.prototype.slice.call(iframes);
        iframesList.map(item => {
          if (
            !!~item.src.indexOf('www.youtube.com') ||
            !!~item.src.indexOf('www.lynda.com') ||
            !!~item.src.indexOf('www.vimeo.com')
          ) {
            item.src = item.src;
          }
        });
      }
      let pathwayBackUrl = window.location.pathname;
      window.history.pushState(
        null,
        null,
        `/${linkPrefixValue}/${this.state.card.slug || this.state.card.id}`
      );
      this.props.dispatch(
        openStandaloneOverviewModal(
          this.state.card,
          cardType,
          dataCard,
          this.cardUpdated.bind(this)
        )
      );
      if (
        this.state.card.cardType === 'pack' &&
        this.state.isCardV3 &&
        this.state.pathwayConsumptionV2
      ) {
        if (this.props.pathways && this.props.pathways.consumptionPathway) {
          this.props.dispatch(removeConsumptionPathway(pathwayBackUrl));
        } else {
          this.props.dispatch(saveConsumptionPathwayHistoryURL(pathwayBackUrl));
        }
      }

      if (
        this.state.card.cardType === 'journey' &&
        this.state.isCardV3 &&
        this.state.journeyConsumptionV2
      ) {
        if (this.props.journeyDetails && this.props.journeyDetails.consumptionJourney) {
          this.props.dispatch(removeConsumptionJourney(pathwayBackUrl));
        } else {
          this.props.dispatch(saveConsumptionJourneyHistoryURL(pathwayBackUrl));
        }
      }
      if (this.props.toggleSearch) {
        this.props.toggleSearch();
      }
    }
  };

  showTopicToggleClick = () => {
    this.setState({ showTopic: !this.state.showTopic, isShowAllTags: false });
  };

  downloadBlock = file => {
    return (
      <span
        className="roll"
        onMouseEnter={() => {
          this.setState({ [file.handle]: true });
        }}
        style={{ display: this.state[file.handle] ? 'inherit' : 'none' }}
      >
        <a href={file.url} download={file.handle} target="_blank">
          <IconButton
            style={this.styles.download}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={tr('Download')}
          >
            <Download />
          </IconButton>
        </a>
      </span>
    );
  };

  truncateMessageText = message => {
    return message.substr(0, 260);
  };

  rateCard = level => {
    rateCard(this.state.card.id, { level: level })
      .then(() => {
        this.cardUpdated();
      })
      .catch(err => {
        console.error(`Error in Card.rateCard: ${err}`);
      });
  };

  addToLeap = (nextCorrectAnswer, nextIncorrectAnswer, cardId, leapId) => {
    this.props.addToLeap(nextCorrectAnswer, nextIncorrectAnswer, cardId, leapId);
  };

  allCountRating = card => {
    if (!card.allRatings) {
      return null;
    }

    if (typeof card.allRatings === 'number') {
      this.setState({ ratingCount: card.allRatings });
      return card.allRatings;
    }

    if (typeof card.allRatings === 'object') {
      let ratingCount = 0;
      for (let i = 1; i < 6; i++) {
        ratingCount += card.allRatings[i] ? card.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
      return ratingCount;
    }
  };

  videoPlay = e => {
    e.stopPropagation();
    let video = document.getElementById(`video-${this.state.card.filestack[0].handle}`);
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  };

  deeplink = () => {
    let card = this.props.card;
    let classes =
      this.props.card.additional_metadata && this.props.card.additional_metadata.user_courses;
    let has_many_classes = classes && classes.length > 1;

    if (has_many_classes) {
      this.setState({ modalClassesOpen: true });
    } else {
      let url = card.deeplink_url;
      if (classes && classes.length == 1) {
        url = classes[0].deeplink_url;
      }
      window.open(url, '_blank');
    }
  };

  closeClassesModal = () => {
    this.setState({ modalClassesOpen: false }, () => {
      document.body.style.overflow = '';
    });
  };

  isShowLeap = () => {
    let card = checkResources(this.state.card);
    let params = getCardParams(card, this.props, 'card') || {};
    let createLeap =
      params.cardType === 'QUIZ' &&
      Permissions['enabled'] !== undefined &&
      (!card.leaps && Permissions.has('CREATE_LEAP')) &&
      !(this.props.pathwayDetails || this.props.journeyDetails) &&
      'create';
    let updateLeap =
      params.cardType === 'QUIZ' &&
      Permissions['enabled'] !== undefined &&
      (card.leaps && card.leaps.standalone && Permissions.has('UPDATE_LEAP')) &&
      !(this.props.pathwayDetails || this.props.journeyDetails) &&
      'update';
    return createLeap || updateLeap;
  };

  /* ----- only one video should get played at a time if there are multiple videos in a listview ------ */
  getAllvideoElements = () => {
    let videoEle = document.getElementsByClassName('ml-video-cards');
    for (let i = 0; i < videoEle.length; i++) {
      this.reloadVideo(videoEle[i], videoEle);
    }
  };

  getAlliframeElements = sourceType => {
    let getAllVideoIds = this.getAllVideoCardIds();
    if (sourceType == 'bigView') {
      getAllVideoIds.map(ele => {
        document.querySelector(`.big-vew-block #ele_${ele} iframe`).src = document.querySelector(
          `.big-vew-block #ele_${ele} iframe`
        ).src;
      });
    }
    if (sourceType == 'feedView') {
      getAllVideoIds.map(ele => {
        document.querySelector(`.list-view-block #ele_${ele} iframe`).src = document.querySelector(
          `.list-view-block #ele_${ele} iframe`
        ).src;
      });
    }
  };

  getAllVideoCardIds = () => {
    let divElements = document.getElementsByClassName('link-card-video');
    let tempArr = [];
    let id;
    for (let i = 0; i < divElements.length; i++) {
      id = divElements[i].id.split('_')[1];
      tempArr.push(id);
    }
    return tempArr;
  };

  reloadVideo = (eleObj, allEleObj) => {
    for (let i = 0; i < allEleObj.length; i++) {
      if (allEleObj[i].id != eleObj.id) {
        allEleObj[i].load();
      }
    }
  };

  completeClickHandler = e => {
    e.stopPropagation();
    cardMarkAsComplete.call(
      this,
      this.state.card,
      this.state.isCompleted,
      this.cardUpdated,
      this.props.isPartOfPathway
    );
  };

  /*  ------------    -----------  */
  render() {
    if (this.state.card.dismissed) {
      return null;
    }
    let card = checkResources(this.state.card);
    let params = getCardParams(card, this.props, 'card') || {};
    let infoTooltipHeader = 'Matching channel(s):';

    // If we have an image at position 0 in fileResources, we need to display instead of FileStack
    if (
      card.fileResources &&
      card.fileResources.length &&
      card.fileResources[0].fileType === 'image' &&
      !params.audioFileStack
    ) {
      params.fileFileStack = false;
    }
    let isCompleted = this.state.isCompleted;
    let svgStyle = {
      filter: `url(#blur-effect-card-${card.id})`
    };
    let infoTooltipTextArr = card.channels || [];
    let authorName = '';
    let isNameCut = false;
    if (card.author) {
      authorName = `${card.author.firstName ? card.author.firstName : ''} ${
        card.author.lastName ? card.author.lastName : ''
      }`;
      if (
        authorName.length > 16 &&
        !this.state.isNewTileCard &&
        this.props.type !== 'List' &&
        this.props.type !== 'BigCard'
      ) {
        isNameCut = true;
        authorName = authorName.substr(0, 14) + '...';
      }
    }
    let isShowLeap = this.isShowLeap();
    let controlsElement = (
      <div>
        <div className="close close-button pathway-card-btn close-card-btn">
          <IconButton
            className="editor-image-btn-icon"
            onClick={this.props.removeCardFromPathway}
            style={this.styles.close}
            iconStyle={this.styles.iconClose}
            aria-label="close"
          >
            <span tabIndex={-1} className="hideOutline">
              <CloseIcon style={this.styles.iconClose} color="white" />
            </span>
          </IconButton>
        </div>
        <div className="close close-button pathway-card-btn move-card-btn">
          <IconButton
            className="editor-image-btn-icon"
            style={this.styles.move}
            iconStyle={this.styles.moveIcon}
          >
            <span tabIndex={-1} className="hideOutline">
              <MoveIcon style={this.styles.moveIcon} color="white" />
            </span>
          </IconButton>
        </div>
        {this.lockPathwayCardFlag && (
          <div className="lock-leap-panel">
            <FlatButton
              className="lock-btn"
              label={tr('LOCK')}
              labelStyle={this.styles.lockBtnLabel}
              icon={
                this.props.card.isLocked ? (
                  <LockIcon
                    style={{
                      ...this.styles.lockLeapIcon,
                      ...this.styles.lockIcon
                    }}
                    color="#fff"
                  />
                ) : (
                  <UnlockIcon
                    style={{
                      ...this.styles.lockLeapIcon,
                      ...this.styles.lockIcon
                    }}
                    color="#fff"
                  />
                )
              }
              onClick={this.props.lockCard}
            />
            {isShowLeap && (
              <FlatButton
                className="leap-btn"
                label={
                  isShowLeap === 'create'
                    ? this.state.isCardV3
                      ? tr('LEAP')
                      : tr('CREATE LEAP')
                    : tr('UPDATE LEAP')
                }
                onClick={this.toggleHandleLeapModal}
                labelPosition="before"
                labelStyle={this.styles.leapBtnLabel}
                icon={
                  <LeapIcon
                    style={{
                      ...this.styles.lockLeapIcon,
                      ...this.styles.leapIcon
                    }}
                    color="#fff"
                  />
                }
              />
            )}
          </div>
        )}
      </div>
    );

    // Pricing according to country code
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);

    let priceData = null;

    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }
    if (card.cardType == 'pack' && card.packCards) {
      /* this fix is to check if a pathway has atleast a single paid card*/
      for (let i = 0; i < card.packCards.length; i++) {
        if (card.packCards[i].prices && card.packCards[i].prices.length > 0) {
          priceData =
            find(card.packCards[i].prices, { currency: currency }) ||
            find(card.packCards[i].prices, { currency: 'USD' }) ||
            find(card.packCards[i].prices, function(price) {
              return price.currency != 'SKILLCOIN';
            });
          break;
        }
      }
    }

    let scormCard =
      card.filestack &&
      card.filestack.length &&
      card.filestack[0] &&
      card.filestack[0].scorm_course;

    let isLocked =
      this.lockPathwayCardFlag &&
      ((!!this.props.pathwayDetails && !params.isPathwayOwner) ||
        (this.props.journeyDetails && !params.isJourneyOwner)) &&
      (card.isLocked || card.showLocked) &&
      (!this.props.isShowLockedCardContent || this.props.isPrivate);

    let isLockedForOwner =
      this.lockPathwayCardFlag &&
      ((!!this.props.pathwayDetails && params.isPathwayOwner) ||
        (this.props.journeyDetails && params.isJourneyOwner)) &&
      (card.isLocked || card.showLocked);

    let isChecked = this.props.pathwayChecking && isCompleted;

    let isShowText =
      params.cardType === 'TEXT' ||
      params.cardType === 'PATHWAY' ||
      params.cardType === 'JOURNEY' ||
      params.cardType === 'IMAGE' ||
      params.cardType === 'ARTICLE' ||
      params.cardType === 'COURSE' ||
      params.cardType === 'PROJECT';
    let isWithFile =
      (params.imageExists && isShowText) || (params.poster && params.cardType === 'FILE');

    let mimetype = (card.filestack && card.filestack[0] && card.filestack[0].mimetype) || '';
    let videoAppMimeTypeCheck = params.appFileStack || params.videoFileStack;
    let filestackPresent = card.filestack && card.filestack.length;
    let lastResortImage = params.poster || params.cardSvgBackground || this.state.defaultImage;
    let blurredImage =
      (filestackPresent &&
        mimetype &&
        videoAppMimeTypeCheck &&
        card.filestack[1] &&
        card.filestack[1].url) ||
      (filestackPresent && !mimetype && card.filestack[0].url) ||
      lastResortImage;

    let cardImageShadowColor =
      (this.props.team.config && this.props.team.config['card_image_shadow_color']) ||
      this.defaulCardImageShadowColor;

    return this.props.type === 'List' ? (
      this.state.isCardV3 ? (
        <CardListV3
          {...this.props}
          isShowText={isShowText}
          isWithFile={isWithFile}
          blurredImage={blurredImage}
          cardImageShadowColor={cardImageShadowColor}
          card={card}
          params={params}
          infoTooltipHeader={infoTooltipHeader}
          scormCard={scormCard}
          priceData={priceData}
          isNameCut={isNameCut}
          authorName={authorName}
          disableTopics={false}
          isCompleted={isCompleted}
          svgStyle={svgStyle}
          infoTooltipTextArr={infoTooltipTextArr}
          countryCode={countryCode}
          isShowLeap={isShowLeap}
          currency={currency}
          logoObj={logoObj}
          transcodedVideoUrl={this.state.transcodedVideoUrl}
          ratingCount={this.state.ratingCount}
          transcodedVideoStatus={this.state.transcodedVideoStatus}
          showTopic={this.state.showTopic}
          comments={this.state.comments}
          isLiveStream={this.state.isLiveStream}
          commentsCount={this.state.commentsCount}
          handleCardClicked={this.handleCardClicked}
          allowAutoplay={false}
          toggleHandleLeapModal={this.toggleHandleLeapModal}
          getAlliframeElements={this.getAlliframeElements}
          reloadVideo={this.reloadVideo}
          getAllvideoElements={this.getAllvideoElements}
          clickOnComments={this.clickOnComments}
          closeClassesModal={this.closeClassesModal}
          deeplink={this.deeplink}
          videoPlay={this.videoPlay}
          rateCard={this.rateCard}
          downloadBlock={this.downloadBlock}
          showTopicToggleClick={this.showTopicToggleClick}
          standaloneLinkClickHandler={this.standaloneLinkClickHandler}
          closeModal={this.closeModal}
          cardUpdated={this.cardUpdated}
          updateCommentCount={this.updateCommentCount}
          cardLikeHandler={this.cardLikeHandler}
          completeClickHandler={this.completeClickHandler}
          removeCardFromCarousel={this.props.removeCardFromCarousel}
          removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        />
      ) : (
        <CardList
          {...this.props}
          isShowText={isShowText}
          isWithFile={isWithFile}
          blurredImage={blurredImage}
          cardImageShadowColor={cardImageShadowColor}
          card={card}
          params={params}
          infoTooltipHeader={infoTooltipHeader}
          scormCard={scormCard}
          priceData={priceData}
          isNameCut={isNameCut}
          authorName={authorName}
          disableTopics={false}
          isCompleted={isCompleted}
          svgStyle={svgStyle}
          infoTooltipTextArr={infoTooltipTextArr}
          countryCode={countryCode}
          isShowLeap={isShowLeap}
          currency={currency}
          logoObj={logoObj}
          transcodedVideoUrl={this.state.transcodedVideoUrl}
          ratingCount={this.state.ratingCount}
          transcodedVideoStatus={this.state.transcodedVideoStatus}
          showTopic={this.state.showTopic}
          comments={this.state.comments}
          isLiveStream={this.state.isLiveStream}
          commentsCount={this.state.commentsCount}
          handleCardClicked={this.handleCardClicked}
          allowAutoplay={false}
          toggleHandleLeapModal={this.toggleHandleLeapModal}
          getAlliframeElements={this.getAlliframeElements}
          reloadVideo={this.reloadVideo}
          getAllvideoElements={this.getAllvideoElements}
          clickOnComments={this.clickOnComments}
          closeClassesModal={this.closeClassesModal}
          deeplink={this.deeplink}
          videoPlay={this.videoPlay}
          rateCard={this.rateCard}
          downloadBlock={this.downloadBlock}
          showTopicToggleClick={this.showTopicToggleClick}
          standaloneLinkClickHandler={this.standaloneLinkClickHandler}
          closeModal={this.closeModal}
          cardUpdated={this.cardUpdated}
          updateCommentCount={this.updateCommentCount}
          cardLikeHandler={this.cardLikeHandler}
          completeClickHandler={this.completeClickHandler}
          removeCardFromCarousel={this.props.removeCardFromCarousel}
          removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        />
      )
    ) : this.props.type === 'BigCard' && this.state.bigCardView ? (
      this.state.isCardV3 ? (
        <CardBigV3
          {...this.props}
          isShowText={isShowText}
          isWithFile={isWithFile}
          blurredImage={blurredImage}
          cardImageShadowColor={cardImageShadowColor}
          card={card}
          params={params}
          infoTooltipHeader={infoTooltipHeader}
          scormCard={scormCard}
          priceData={priceData}
          isNameCut={isNameCut}
          authorName={authorName}
          disableTopics={false}
          isCompleted={isCompleted}
          svgStyle={svgStyle}
          infoTooltipTextArr={infoTooltipTextArr}
          countryCode={countryCode}
          isShowLeap={isShowLeap}
          currency={currency}
          logoObj={logoObj}
          transcodedVideoUrl={this.state.transcodedVideoUrl}
          ratingCount={this.state.ratingCount}
          transcodedVideoStatus={this.state.transcodedVideoStatus}
          showTopic={this.state.showTopic}
          comments={this.state.comments}
          isLiveStream={this.state.isLiveStream}
          commentsCount={this.state.commentsCount}
          handleCardClicked={this.handleCardClicked}
          allowAutoplay={false}
          toggleHandleLeapModal={this.toggleHandleLeapModal}
          getAlliframeElements={this.getAlliframeElements}
          reloadVideo={this.reloadVideo}
          getAllvideoElements={this.getAllvideoElements}
          clickOnComments={this.clickOnComments}
          closeClassesModal={this.closeClassesModal}
          videoPlay={this.videoPlay}
          rateCard={this.rateCard}
          downloadBlock={this.downloadBlock}
          showTopicToggleClick={this.showTopicToggleClick}
          standaloneLinkClickHandler={this.standaloneLinkClickHandler}
          closeModal={this.closeModal}
          cardUpdated={this.cardUpdated}
          updateCommentCount={this.updateCommentCount}
          cardLikeHandler={this.cardLikeHandler}
          completeClickHandler={this.completeClickHandler}
          removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        />
      ) : (
        <CardBig
          {...this.props}
          isShowText={isShowText}
          isWithFile={isWithFile}
          blurredImage={blurredImage}
          cardImageShadowColor={cardImageShadowColor}
          card={card}
          params={params}
          infoTooltipHeader={infoTooltipHeader}
          scormCard={scormCard}
          priceData={priceData}
          isNameCut={isNameCut}
          authorName={authorName}
          disableTopics={false}
          isCompleted={isCompleted}
          svgStyle={svgStyle}
          infoTooltipTextArr={infoTooltipTextArr}
          countryCode={countryCode}
          isShowLeap={isShowLeap}
          currency={currency}
          logoObj={logoObj}
          transcodedVideoUrl={this.state.transcodedVideoUrl}
          ratingCount={this.state.ratingCount}
          transcodedVideoStatus={this.state.transcodedVideoStatus}
          showTopic={this.state.showTopic}
          comments={this.state.comments}
          isLiveStream={this.state.isLiveStream}
          commentsCount={this.state.commentsCount}
          handleCardClicked={this.handleCardClicked}
          allowAutoplay={false}
          toggleHandleLeapModal={this.toggleHandleLeapModal}
          getAlliframeElements={this.getAlliframeElements}
          reloadVideo={this.reloadVideo}
          getAllvideoElements={this.getAllvideoElements}
          clickOnComments={this.clickOnComments}
          closeClassesModal={this.closeClassesModal}
          videoPlay={this.videoPlay}
          rateCard={this.rateCard}
          downloadBlock={this.downloadBlock}
          showTopicToggleClick={this.showTopicToggleClick}
          standaloneLinkClickHandler={this.standaloneLinkClickHandler}
          closeModal={this.closeModal}
          cardUpdated={this.cardUpdated}
          updateCommentCount={this.updateCommentCount}
          cardLikeHandler={this.cardLikeHandler}
          completeClickHandler={this.completeClickHandler}
          removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        />
      )
    ) : this.state.isCardV3 ? (
      <CardTileV3
        {...this.props}
        isLocked={isLocked}
        isLockedForOwner={isLockedForOwner}
        isChecked={isChecked}
        isShowText={isShowText}
        isWithFile={isWithFile}
        blurredImage={blurredImage}
        cardImageShadowColor={cardImageShadowColor}
        card={card}
        params={params}
        infoTooltipHeader={infoTooltipHeader}
        controlsElement={controlsElement}
        scormCard={scormCard}
        priceData={priceData}
        isNameCut={isNameCut}
        authorName={authorName}
        disableTopics={false}
        isCompleted={isCompleted}
        svgStyle={svgStyle}
        infoTooltipTextArr={infoTooltipTextArr}
        countryCode={countryCode}
        isShowLeap={isShowLeap}
        currency={currency}
        logoObj={logoObj}
        transcodedVideoUrl={this.state.transcodedVideoUrl}
        ratingCount={this.state.ratingCount}
        transcodedVideoStatus={this.state.transcodedVideoStatus}
        showTopic={this.state.showTopic}
        comments={this.state.comments}
        isLiveStream={this.state.isLiveStream}
        commentsCount={this.state.commentsCount}
        isLeapModalOpen={this.state.isLeapModalOpen}
        type="Tile"
        handleCardClicked={this.handleCardClicked}
        toggleHandleLeapModal={this.toggleHandleLeapModal}
        cardUpdated={this.cardUpdated}
        slideOutCardModalOpen={this.state.slideOutCardModalOpen}
        closeSlideOut={this.closeSlideOut}
        clickOnComments={this.clickOnComments}
        addToLeap={this.addToLeap}
        rateCard={this.rateCard}
        truncateMessageText={this.truncateMessageText}
        downloadBlock={this.downloadBlock}
        showTopicToggleClick={this.showTopicToggleClick}
        standaloneLinkClickHandler={this.standaloneLinkClickHandler}
        closeModal={this.closeModal}
        updateCommentCount={this.updateCommentCount}
        cardLikeHandler={this.cardLikeHandler}
        removeCardFromCarousel={this.props.removeCardFromCarousel}
        removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        completeClickHandler={this.completeClickHandler}
      />
    ) : (
      <CardTile
        {...this.props}
        isLocked={isLocked}
        isChecked={isChecked}
        isShowText={isShowText}
        isWithFile={isWithFile}
        blurredImage={blurredImage}
        cardImageShadowColor={cardImageShadowColor}
        card={card}
        params={params}
        infoTooltipHeader={infoTooltipHeader}
        controlsElement={controlsElement}
        scormCard={scormCard}
        priceData={priceData}
        isNameCut={isNameCut}
        authorName={authorName}
        disableTopics={false}
        isCompleted={isCompleted}
        svgStyle={svgStyle}
        infoTooltipTextArr={infoTooltipTextArr}
        countryCode={countryCode}
        isShowLeap={isShowLeap}
        currency={currency}
        logoObj={logoObj}
        transcodedVideoUrl={this.state.transcodedVideoUrl}
        ratingCount={this.state.ratingCount}
        transcodedVideoStatus={this.state.transcodedVideoStatus}
        showTopic={this.state.showTopic}
        comments={this.state.comments}
        isLiveStream={this.state.isLiveStream}
        commentsCount={this.state.commentsCount}
        isLeapModalOpen={this.state.isLeapModalOpen}
        type="Tile"
        handleCardClicked={this.handleCardClicked}
        toggleHandleLeapModal={this.toggleHandleLeapModal}
        cardUpdated={this.cardUpdated}
        slideOutCardModalOpen={this.state.slideOutCardModalOpen}
        closeSlideOut={this.closeSlideOut}
        clickOnComments={this.clickOnComments}
        addToLeap={this.addToLeap}
        rateCard={this.rateCard}
        truncateMessageText={this.truncateMessageText}
        downloadBlock={this.downloadBlock}
        showTopicToggleClick={this.showTopicToggleClick}
        standaloneLinkClickHandler={this.standaloneLinkClickHandler}
        closeModal={this.closeModal}
        updateCommentCount={this.updateCommentCount}
        cardLikeHandler={this.cardLikeHandler}
        removeCardFromCarousel={this.props.removeCardFromCarousel}
        removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
        completeClickHandler={this.completeClickHandler}
      />
    );
  }
}

Card.propTypes = {
  card: PropTypes.object,
  cards: PropTypes.object,
  providerLogos: PropTypes.object,
  hideEdit: PropTypes.string,
  userInterest: PropTypes.object,
  currentSection: PropTypes.object,
  currentUser: PropTypes.object,
  pathways: PropTypes.object,
  pathname: PropTypes.string,
  type: PropTypes.string,
  dueAt: PropTypes.string,
  cardSplat: PropTypes.any,
  pathwayEditor: PropTypes.bool,
  hideModal: PropTypes.bool,
  channelSetting: PropTypes.bool,
  providerCards: PropTypes.bool,
  journeyDetails: PropTypes.bool,
  isStandaloneModal: PropTypes.bool,
  inStandalone: PropTypes.bool,
  groupSetting: PropTypes.bool,
  hideActions: PropTypes.bool,
  pathwayDetails: PropTypes.object,
  commentDisabled: PropTypes.bool,
  dismissible: PropTypes.bool,
  removeCardFromList: PropTypes.func,
  removeCardFromPathway: PropTypes.func,
  getCardProps: PropTypes.func,
  index: PropTypes.number,
  closeCardModal: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  cardUpdated: PropTypes.func,
  voidCardProps: PropTypes.func,
  hideComplete: PropTypes.func,
  addToLeap: PropTypes.func,
  toggleSearch: PropTypes.func,
  lockCard: PropTypes.func,
  openCardEdit: PropTypes.func,
  startDate: PropTypes.string,
  isPathwayCard: PropTypes.bool,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  discoverUpshotEventSend: PropTypes.func,
  sendLearningUpshotEvent: PropTypes.func,
  feedKey: PropTypes.string,
  sendLearnPageUpshotEvent: PropTypes.func,
  removeCardFromCarousel: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func,
  isNotNeedUpdateCardReducer: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  isPrivate: PropTypes.bool
};

Card.defaultProps = {
  hideModal: false,
  channelSetting: false,
  groupSetting: false,
  isPartOfPathway: false
};

function mapStoreStateToProps(state) {
  return {
    cards: state.cards.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    pathways: state.pathways.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(Card);
