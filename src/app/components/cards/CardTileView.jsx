import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';

import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import LockIcon from 'edc-web-sdk/components/icons/Lock';

import CheckedIcon from 'material-ui/svg-icons/action/done';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';

import CardModal from '../modals/CardModal';
import SlideOutCardModal from '../modals/SlideOutCardModal';

import LeapModal from '../modals/LeapModal';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import Poll from '../feed/Poll';
import MarkdownRenderer from '../common/MarkdownRenderer';
import SmartBiteCardHeader from '../common/SmartBiteCardHeader';
import SmartBiteCardFooter from '../common/SmartBiteCardFooter';
import Spinner from '../common/spinner';
import SvgImageResized from '../common/ImageResized';
import convertRichText from '../../utils/convertRichText';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';
import { Permissions } from '../../utils/checkPermissions';
import getDefaultImage from '../../utils/getDefaultCardImage';
import pdfPreviewUrl from '../../utils/previewPdf';
import addSecurity from '../../utils/filestackSecurity';

const defaultVideoProcessingImage = '/i/images/video_processing_being_processed.jpg';

class CardTileView extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      checkedIcon: {
        width: 70,
        height: 70,
        marginTop: '3.125rem'
      },
      cardImgContainerImg: {
        zIndex: 2
      },
      cardImgContainerSvg: {
        zIndex: 2,
        position: 'relative'
      },
      lockedCard: {
        background: 'rgba(255, 255, 255, 0.5) no-repeat',
        backgroundSize: '100% 100%'
      },
      audio: {
        width: '100%'
      },
      privateLabel: {
        margin: '3.125rem 1rem 0',
        color: colors.primary
      },
      privateCard: {
        height: '0'
      }
    };
    this.state = {
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      lockPathwayCardFlag: window.ldclient.variation('lock-pathway-card', false),
      isNewTileCard: window.ldclient.variation('new-ui-tile-card', false)
    };

    this.avatarDiameter = this.state.isNewTileCard ? '2.5rem' : '1.625rem';
    this.imageHeight = this.state.isNewTileCard ? 'height:200' : 'height:160';
  }

  setScormMsg = card => {
    if (card.state == 'processing') {
      return 'Course is getting uploaded';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  refreshAudioUrl = handle => {
    if (!!handle) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let audio = document.getElementsByClassName('audio-preview')[0];
          let seen = audio.currentTime;
          audio.setAttribute('src', newUrl);
          audio.currentTime = seen;
          if (seen !== 0) {
            audio.play();
          }
        })
        .catch(err => {
          console.error(
            `Error in CardOverviewModal.refreshAudioUrl.refreshFilestackUrl.func : ${err}`
          );
        });
    }
  };

  render() {
    if (this.props.card.dismissed) {
      return null;
    }

    let { isPartOfPathway } = this.props;
    let card = this.props.card;
    let scormMsg = this.setScormMsg(this.props.card);
    let scormStateMsg = this.setScormStateMsg(this.props.card);
    let cardMsg = convertRichText(this.props.card.title || this.props.card.message);
    let mimetype =
      (this.props.card.filestack &&
        this.props.card.filestack[0] &&
        this.props.card.filestack[0].mimetype) ||
      '';
    let videoAppMimeTypeCheck = mimetype.includes('application/') || mimetype.includes('video/');
    let filestackPresent = this.props.card.filestack && this.props.card.filestack.length;
    let lastResortImage =
      this.props.params.poster || this.props.params.cardSvgBackground || this.state.defaultImage;
    let blurredImage =
      (filestackPresent &&
        mimetype &&
        videoAppMimeTypeCheck &&
        this.props.card.filestack[1] &&
        this.props.card.filestack[1].url) ||
      (filestackPresent && !mimetype && this.props.card.filestack[0].url) ||
      lastResortImage;
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let defaultImage = getDefaultImage(this.props.currentUser.id).url;
    blurredImage = addSecurity(blurredImage, expireAfter, this.props.currentUser.id);

    return (this.state.lockPathwayCardFlag &&
      ((!!this.props.pathwayDetails && !this.props.params.isPathwayOwner) ||
        (this.props.journeyDetails && !this.props.params.isJourneyOwner)) &&
      (this.props.card.isLocked || this.props.card.showLocked) &&
      !this.props.isShowLockedCardContent) ||
      this.props.isPrivate ? (
      <div
        className={`card-v2 ${this.props.moreCards ? 'more-cards' : ''} ${
          this.state.isNewTileCard ? 'card-v2__wide' : 'card-v2__simple-size'
        }`}
      >
        <Paper
          className={this.props.showControls ? 'paper-card paper-card-controls' : 'paper-card'}
          zDepth={this.props.showControls && 0}
        >
          <div
            className="locked-card"
            style={!this.props.isPrivate ? this.styles.lockedCard : this.styles.privateCard}
          >
            {this.props.isPrivate ? (
              <h4 style={this.styles.privateLabel}>
                {tr('You do not have permission to view this card')}
              </h4>
            ) : (
              <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
            )}
          </div>
          {(this.props.showControls || this.props.showIndex) && (
            <div className="close-button pathway-card-btn number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}
          {this.props.isPrivate && this.props.showControls && this.props.controlsElement}
        </Paper>
      </div>
    ) : (
      <div
        className={`card-v2 ${this.props.moreCards ? ' more-cards' : ''} ${
          this.state.isNewTileCard ? 'card-v2__wide' : 'card-v2__simple-size'
        }`}
      >
        <div
          className={
            this.props.showControls ? 'paper paper-card paper-card-controls' : 'paper paper-card'
          }
        >
          {this.props.isShowLeap && (this.props.params.isOwner || this.props.pathwayEditor) && (
            <LeapModal
              isOpen={this.props.isLeapModalOpen}
              card={this.props.card}
              toggleHandleLeapModal={this.props.toggleHandleLeapModal}
              pathwayEditor={this.props.pathwayEditor}
              cardsList={this.props.cardsList}
              addToLeap={this.props.addToLeap}
              arrToLeap={this.props.arrToLeap}
              pathway={this.props.pathway}
              cardUpdated={this.props.cardUpdated}
            />
          )}
          {this.state.lockPathwayCardFlag &&
            ((!!this.props.pathwayDetails && this.props.params.isPathwayOwner) ||
              (this.props.journeyDetails && this.props.params.isJourneyOwner)) &&
            (this.props.card.isLocked || this.props.card.showLocked) && (
              <div className="locked-card" style={this.styles.lockedCard}>
                <LockIcon style={this.styles.bigLockIcon} color="#7c7d94" />
              </div>
            )}
          <div
            onClick={this.props.handleCardClicked}
            className={
              this.props.pathwayChecking &&
              !this.props.params.isShowTopImage &&
              this.props.isCompleted
                ? 'checking-full-over'
                : 'hide-checking'
            }
          >
            <CheckedIcon color={colors.primary} style={this.styles.checkedIcon} />
          </div>
          <div
            className={
              this.props.scormCard && this.props.card.state !== 'published'
                ? 'scorm-not-uploaded'
                : 'hide-checking'
            }
          >
            <div className="scorm-not-uploaded-text">{scormMsg}</div>
          </div>
          {this.props.showControls && this.props.controlsElement}
          {(this.props.showControls || this.props.showIndex) && (
            <div className="close-button pathway-card-btn number-card-btn">
              <span>{this.props.index}</span>
            </div>
          )}

          {this.props.card.cardType === 'pack' && (
            <div className="pathway-label-block">
              {this.props.card.state === 'draft' && (
                <div className="pathway-draft-label">{tr('DRAFT')}</div>
              )}
              <div className="pathway-label">
                {this.props.card.badging && this.props.card.badging.imageUrl && (
                  <img
                    className="pathway-badge-preview"
                    src={addSecurity(
                      this.props.card.badging.imageUrl,
                      expireAfter,
                      this.props.currentUser.id
                    )}
                  />
                )}
                <span>{tr('PATHWAY')}</span>
              </div>
            </div>
          )}
          {this.props.card.cardType === 'journey' && (
            <div className="pathway-label-block">
              {this.props.card.state === 'draft' && (
                <div className="pathway-draft-label">{tr('DRAFT')}</div>
              )}
              <div className="pathway-label">
                {this.props.card.badging && this.props.card.badging.imageUrl && (
                  <img
                    className="pathway-badge-preview"
                    src={addSecurity(
                      this.props.card.badging.imageUrl,
                      expireAfter,
                      this.props.currentUser.id
                    )}
                  />
                )}
                <span>{tr('JOURNEY')}</span>
              </div>
            </div>
          )}
          {this.props.params.isShowTopImage && (
            <a
              href="#"
              className="card-img-container"
              onClick={e => {
                this.props.handleCardClicked(e, true, this.props.params.isYoutubeVideo);
              }}
            >
              <div
                className={
                  this.props.pathwayChecking && this.props.isCompleted
                    ? 'checking-over'
                    : 'hide-checking'
                }
              >
                <CheckedIcon color={colors.primary} style={this.styles.checkedIcon} />
              </div>
              {((this.props.params.showBlurryBackground &&
                this.props.params.cardType !== 'AUDIO') ||
                (this.props.params.cardType === 'AUDIO' && this.props.params.imageExists)) && (
                <div className="card-img button-icon card-blurred-background">
                  <svg width="100%" height="100%">
                    <title>
                      <RichTextReadOnly text={cardMsg} />{' '}
                    </title>
                    <SvgImageResized
                      cardId={`${this.props.card.id}`}
                      resizeOptions={this.imageHeight}
                      style={this.props.svgStyle}
                      xlinkHref={
                        (this.props.transcodedVideoStatus &&
                          this.props.transcodedVideoStatus.trim()) != 'completed'
                          ? addSecurity(
                              defaultVideoProcessingImage,
                              expireAfter,
                              this.props.currentUser.id
                            )
                          : blurredImage || defaultImage
                      }
                      x="-30%"
                      y="-30%"
                      width="160%"
                      height="160%"
                    />
                    <filter id={`blur-effect-card-${this.props.card.id}`}>
                      <feGaussianBlur stdDeviation="10" />
                    </filter>
                  </svg>
                </div>
              )}
              {this.props.params.videoFileStack ? (
                <div>
                  {this.props.params.cardImage ? (
                    <svg width="100%" height="100%" style={this.styles.cardImgContainerSvg}>
                      <title>
                        <RichTextReadOnly text={cardMsg} />
                      </title>
                      <SvgImageResized
                        cardId={`${this.props.card.id}`}
                        resizeOptions={this.imageHeight}
                        xlinkHref={
                          addSecurity(
                            this.props.params.poster,
                            expireAfter,
                            this.props.currentUser.id
                          ) ||
                          addSecurity(
                            this.props.params.cardImage,
                            expireAfter,
                            this.props.currentUser.id
                          ) ||
                          defaultImage
                        }
                        width="100%"
                        style={this.styles.cardImgContainerImg}
                        height="100%"
                      />
                    </svg>
                  ) : (
                    <div className="fp fp_video fp_video_card default-video-card">
                      {this.props.transcodedVideoStatus &&
                        ((this.props.transcodedVideoStatus || '').trim() === 'completed' ? (
                          <video
                            preload={
                              this.props.card.filestack && this.props.card.filestack[1]
                                ? 'none'
                                : 'auto'
                            }
                            src={!this.props.params.poster && this.props.transcodedVideoUrl}
                            poster={
                              (this.props.card.filestack &&
                                this.props.card.filestack[1] &&
                                this.props.card.filestack[1].mimetype &&
                                this.props.card.filestack[1].mimetype.includes('image/') &&
                                this.props.card.filestack[1] &&
                                addSecurity(
                                  this.props.card.filestack[1].url,
                                  expireAfter,
                                  this.props.currentUser.id
                                )) ||
                              addSecurity(
                                this.props.params.poster,
                                expireAfter,
                                this.props.currentUser.id
                              )
                            }
                            controlsList={
                              this.props.params.isDownloadContentDisabled ? 'nodownload' : ''
                            }
                          />
                        ) : (
                          <img
                            className="waiting-video"
                            src="/i/images/video_processing_being_processed.jpg"
                          />
                        ))}
                    </div>
                  )}
                </div>
              ) : (
                <svg width="100%" height="100%" style={this.styles.cardImgContainerSvg}>
                  <title>
                    <RichTextReadOnly text={cardMsg} />
                  </title>
                  <SvgImageResized
                    cardId={`${this.props.card.id}`}
                    resizeOptions={this.imageHeight}
                    xlinkHref={blurredImage || defaultImage}
                    width="100%"
                    style={this.styles.cardImgContainerImg}
                    height="100%"
                  />
                </svg>
              )}
              {(this.props.params.cardType === 'VIDEO' ||
                this.props.card.cardSubtype === 'video') && (
                <div>
                  <PlayIcon className="center-play-icon" color={colors.white} />
                  <div className={this.props.params.videoData.videoLabelClass}>
                    {tr(this.props.params.videoData.videoLabelText)}
                  </div>
                </div>
              )}
              {this.props.card.cardType === 'VideoStream' ||
                (this.props.card.cardType === 'video_stream' &&
                  this.props.params.videoData.videoStatus != 'pending' && (
                    <div
                      style={{ display: 'none' }}
                      className={`stream-label ${this.props.params.videoData.videoStatus}`}
                    >
                      {tr(this.props.params.videoData.channelCardLable)}
                    </div>
                  ))}
            </a>
          )}
          <div className="card-v2__text-information">
            {window.ldclient.variation('relevance-rating', false) && this.props.card.simWt ? (
              <div className="relevance-weight">
                {tr('Relevance')}: {parseInt(this.props.card.simWt * 100)}%
              </div>
            ) : (
              ''
            )}
            <SmartBiteCardHeader
              card={this.props.card}
              params={this.props.params}
              isNameCut={this.props.isNameCut}
              pathwayEditor={this.props.pathwayEditor}
              authorName={this.props.authorName}
              cardClickHandle={this.state.cardClickHandle}
              standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
              showControls={this.props.showControls}
              allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
              newSkillLevel={this.props.newSkillLevel}
              rateCard={this.props.rateCard}
              logoObj={this.props.logoObj}
              handleCardClicked={this.props.handleCardClicked}
              avatarDiameter={this.avatarDiameter}
              cardLayoutType={this.props.type}
            />

            <div
              className={`card-details ${
                this.props.params.cardType === 'POLL' ? 'poll-card-details' : ''
              }`}
              onClick={
                this.props.previewMode || (this.props.scormCard && this.props.card.hidden)
                  ? null
                  : this.props.handleCardClicked
              }
            >
              <div
                className={
                  this.props.previewMode || (this.props.scormCard && this.props.card.hidden)
                    ? ''
                    : 'cursor'
                }
              >
                <div
                  className={
                    this.props.params.cardType === 'POLL'
                      ? 'main-card-content-container poll-results-container'
                      : 'main-card-content-container'
                  }
                >
                  {this.props.params.cardType === 'TEXT' ? (
                    <div
                      className={
                        this.props.params.imageExists
                          ? `title ${
                              this.props.dueAt ? 'title-with-image-and-due' : 'title-with-image'
                            }`
                          : 'title text-title'
                      }
                    >
                      {this.props.card.title && (
                        <div className="card-title card-title-v2">
                          <MarkdownRenderer
                            markdown={
                              this.state.truncateTitle
                                ? this.props.card.title.substr(0, 260)
                                : this.props.card.title
                            }
                          />
                        </div>
                      )}
                      {this.props.params.message && (
                        <div className="card-title text-card-message card-title-v2">
                          <RichTextReadOnly
                            text={convertRichText(
                              this.state.truncateMessage
                                ? () => {
                                    this.props.truncateMessageText(this.props.params.message);
                                  }
                                : this.props.params.message
                            )}
                          />
                        </div>
                      )}
                    </div>
                  ) : (
                    <div
                      className={`${!(
                        this.props.params.cardType === 'POLL' ||
                        this.props.params.cardType === 'QUIZ'
                      ) && 'vertical-spacing-large'}`}
                    >
                      <div
                        className={`button-icon title ${(this.props.params.cardType === 'POLL' ||
                          this.props.params.cardType === 'QUIZ') &&
                          'quiz-title'}`}
                      >
                        {this.props.params.message && (
                          <RichTextReadOnly
                            text={convertRichText(
                              this.props.params.message.length > 60
                                ? this.props.params.message.substr(0, 60) + '...'
                                : this.props.params.message
                            )}
                          />
                        )}
                        {!!this.props.scormCard &&
                          this.props.card &&
                          this.props.card.state !== 'published' && (
                            <div className="scorm-processing">
                              <Spinner />
                              <p>{scormStateMsg}...</p>
                            </div>
                          )}
                      </div>
                      {(this.props.params.cardType === 'POLL' ||
                        this.props.params.cardType === 'QUIZ') &&
                        !this.props.params.isShowTopImage && (
                          <div className="poll-content">
                            <Poll card={this.props.card} cardUpdated={this.props.cardUpdated} />
                          </div>
                        )}
                    </div>
                  )}
                  {this.props.params.fileFileStack && this.props.params.cardType !== 'QUIZ' && (
                    <div>
                      {this.props.params.audioFileStack ? (
                        <audio
                          controls
                          className="audio-preview"
                          src={addSecurity(
                            this.props.card.filestack[0].url,
                            expireAfter,
                            this.props.currentUser.id
                          )}
                          style={this.styles.audio}
                          onError={() => {
                            this.refreshAudioUrl(this.props.card.filestack[0].handle);
                          }}
                        />
                      ) : (
                        !this.props.scormCard &&
                        this.props.card.filestack.map(file => {
                          return (
                            <div key={file.handle} className="fp fp_preview">
                              <span>
                                {this.props.params.cardType === 'FILE' &&
                                !this.props.params.poster ? (
                                  <iframe
                                    className="filestack-preview-container"
                                    height="480px"
                                    width="100%"
                                    src={pdfPreviewUrl(
                                      file.url,
                                      expireAfter,
                                      this.props.currentUser.id
                                    )}
                                    onMouseEnter={() => {
                                      this.setState({ [file.handle]: true });
                                    }}
                                    onMouseLeave={() => {
                                      this.setState({ [file.handle]: false });
                                    }}
                                  />
                                ) : (
                                  ''
                                )}
                                {!this.props.params.isDownloadContentDisabled &&
                                  this.props.downloadBlock(file)}
                              </span>
                            </div>
                          );
                        })
                      )}
                    </div>
                  )}
                </div>
              </div>
            </div>

            {!this.props.pathwayEditor && (
              <SmartBiteCardFooter
                isCsodCourse={this.isCsodCourse}
                edcastPricing={this.state.edcastPricing}
                edcastPlansForPricing={this.state.edcastPlansForPricing}
                card={this.props.card}
                priceData={this.props.priceData}
                isShowLeap={this.props.isShowLeap}
                params={this.props.params}
                lockPathwayCardFlag={this.state.lockPathwayCardFlag}
                toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                previewMode={this.props.previewMode}
                hideActions={this.props.hideActions}
                disableTopics={this.props.disableTopics}
                dismissible={this.props.dismissible}
                providerCards={this.props.providerCards}
                showTopicToggleClick={this.props.showTopicToggleClick}
                isStandalone={this.props.isStandalone}
                cardUpdated={this.props.cardUpdated}
                removeCardFromList={this.props.removeCardFromList}
                hideComplete={this.props.hideComplete}
                isCompleted={this.props.isCompleted}
                deleteSharedCard={this.props.deleteSharedCard}
                channel={this.props.channel}
                type={this.props.type}
                averageRating={this.props.card.averageRating}
                ratingCount={this.props.ratingCount}
                handleCardClicked={this.props.handleCardClicked}
                comments={this.props.comments}
                isLiveStream={this.props.isLiveStream}
                commentsCount={this.props.commentsCount}
                isPartOfPathway={isPartOfPathway}
                openReasonReportModal={this.props.openReasonReportModal}
                clickOnComments={this.props.clickOnComments}
                cardSectionName={this.props.cardSectionName}
                removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                removeCardFromCarousel={this.props.removeCardFromCarousel}
                removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
              />
            )}
          </div>
          {this.props.hideEdit && (
            <div className="card-hide-edit tooltip">
              <span className="tooltiptext">{tr(this.props.hideEdit)}</span>
            </div>
          )}
        </div>

        {this.props.modalOpen && !this.props.withoutCardModal && (
          <CardModal
            logoObj={this.props.logoObj}
            defaultImage={this.state.defaultImage}
            card={this.props.card}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            likeCard={this.props.cardLikeHandler}
          />
        )}

        {this.props.slideOutCardModalOpen && (
          <SlideOutCardModal
            card={this.props.card}
            logoObj={this.props.logoObj}
            cardUpdated={this.props.cardUpdated}
            defaultImage={this.props.defaultImage}
            dueAt={this.props.dueAt}
            startDate={this.props.startDate}
            deleteSharedCard={this.props.deleteSharedCard}
            showComment={Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')}
            isUpvoted={this.props.isUpvoted}
            updateCommentCount={this.props.updateCommentCount}
            commentsCount={this.props.commentsCount}
            votesCount={this.props.votesCount}
            closeModal={this.props.closeModal}
            isNameCut={this.props.isNameCut}
            pathwayEditor={this.props.pathwayEditor}
            authorName={this.props.authorName}
            cardClickHandle={this.state.cardClickHandle}
            standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
            showControls={this.props.showControls}
            allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
            newSkillLevel={this.props.newSkillLevel}
            rateCard={this.props.rateCard}
            handleCardClicked={this.props.handleCardClicked}
            avatarDiameter="2.5rem"
            cardLayoutType={this.props.type}
            closeSlideOut={this.props.closeSlideOut}
            averageRating={this.props.card.averageRating}
            ratingCount={this.props.ratingCount}
            likeCard={this.props.cardLikeHandler}
          />
        )}
      </div>
    );
  }
}

CardTileView.propTypes = {
  defaultImage: PropTypes.string,
  startDate: PropTypes.string,
  comments: PropTypes.array,
  params: PropTypes.object,
  channel: PropTypes.object,
  providerCards: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  priceData: PropTypes.object,
  disableTopics: PropTypes.bool,
  pathway: PropTypes.object,
  arrToLeap: PropTypes.object,
  svgStyle: PropTypes.object,
  cardsList: PropTypes.object,
  isUpvoted: PropTypes.bool,
  showControls: PropTypes.bool,
  isOwner: PropTypes.bool,
  dismissible: PropTypes.bool,
  previewMode: PropTypes.bool,
  userInterest: PropTypes.bool,
  withoutCardModal: PropTypes.bool,
  isShowLockedCardContent: PropTypes.bool,
  modalOpen: PropTypes.bool,
  isStandalone: PropTypes.bool,
  scormCard: PropTypes.any,
  moreCards: PropTypes.bool,
  isLiveStream: PropTypes.bool,
  isPrivate: PropTypes.bool,
  isCompleted: PropTypes.bool,
  pathwayEditor: PropTypes.bool,
  showIndex: PropTypes.bool,
  hideActions: PropTypes.bool,
  isShowLeap: PropTypes.any,
  journeyDetails: PropTypes.bool,
  isNameCut: PropTypes.bool,
  showTopic: PropTypes.bool,
  pathwayDetails: PropTypes.object,
  isLeapModalOpen: PropTypes.bool,
  pathwayChecking: PropTypes.bool,
  votesCount: PropTypes.number,
  commentsCount: PropTypes.number,
  ratingCount: PropTypes.number,
  index: PropTypes.number,
  cardLikeHandler: PropTypes.func,
  closeModal: PropTypes.func,
  updateCommentCount: PropTypes.func,
  toggleHandleLeapModal: PropTypes.func,
  hideComplete: PropTypes.func,
  downloadBlock: PropTypes.func,
  cardUpdated: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  removeCardFromList: PropTypes.func,
  handleCardClicked: PropTypes.func,
  truncateMessageText: PropTypes.func,
  deleteSharedCard: PropTypes.func,
  standaloneLinkClickHandler: PropTypes.func,
  addToLeap: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  closeSlideOut: PropTypes.func,
  clickOnComments: PropTypes.func,
  controlsElement: PropTypes.any,
  rateCard: PropTypes.any,
  type: PropTypes.string,
  dueAt: PropTypes.string,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  transcodedVideoUrl: PropTypes.string,
  transcodedVideoStatus: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  hideEdit: PropTypes.string,
  slideOutCardModalOpen: PropTypes.bool,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  removeCardFromCarousel: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func,
  team: PropTypes.object
};

CardTileView.defaultProps = {
  hideModal: false,
  channelSetting: false,
  groupSetting: false,
  isPartOfPathway: false
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardTileView);
