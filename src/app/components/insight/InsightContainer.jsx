import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { startAssignment } from '../../actions/cardsActions';
import { fetchCardForStandaloneLayout } from 'edc-web-sdk/requests/cards';
import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Permissions } from '../../utils/checkPermissions';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import Spinner from '../common/spinner';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { JWT } from 'edc-web-sdk/requests/csrfToken';

// Require Super Agent
import { _agent } from 'edc-web-sdk/helpers/superagent-use';

const Insight = Loadable({
  loader: () =>
    window.ldclient.variation('card-v3', false)
      ? import('../feed/InsightV3')
      : import('../feed/InsightV2'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  },
  delay: 400
});

class InsightContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      unavailable: false,
      card: {}
    };

    this.visibilityListener = false;
    this.isSumTotalCourse = false;
  }

  componentDidMount() {
    this.props
      .dispatch(
        getSpecificUserInfo(
          ['followingChannels', 'roles', 'writableChannels', 'first_name', 'last_name'],
          this.props.currentUser
        )
      )
      .then(() => {
        this.loadInsight();
      })
      .catch(err => {
        console.error(`Error in InsightContainer.componentDidMount.func: ${err}`);
      });
  }

  componentWillUnmount() {
    if (this.visibilityListener) {
      window.removeEventListener('visibilitychange', this.tabVisibilityListener);
    }
  }

  componentWillReceiveProps(nextProps) {
    // If we change the .splat, update the insight component
    if (this.props.routeParams.splat !== nextProps.routeParams.splat) {
      this.setState(
        {
          pending: true
        },
        () => {
          this.loadInsight();
        }
      );
    }
  }

  loadInsight() {
    // use .splat to get entire param
    fetchCardForStandaloneLayout(this.props.routeParams.splat, { is_standalone_page: true })
      .then(data => {
        // One off SumTotal requirement EP-21212
        if (
          data.cardType === 'course' &&
          data.eclSourceTypeName &&
          data.eclSourceTypeName.toLowerCase() === 'sumtotal'
        ) {
          this.isSumTotalCourse = true;
        }
        try {
          recordVisit(data.id);
        } catch (e) {}
        this.setState({ card: data, pending: false }, () => {
          if (
            this.state.card &&
            this.state.card.completionState === null &&
            this.state.card.isAssigned
          ) {
            startAssignment(this.state.card.id)
              .then(e => {})
              .catch(() => {
                this.setState({ pending: false });
              });
          }

          // Listen when loading course that is not completed
          if (
            !this.visibilityListener &&
            this.state.card &&
            this.state.card.completionState === null &&
            this.state.card.cardType === 'course'
          ) {
            this.visibilityListener = true;
            window.addEventListener('visibilitychange', this.tabVisibilityListener);
          }
        });
      })
      .catch(e => {
        console.error(e);
        this.setState({
          pending: false,
          unavailable: true
        });
      });
  }

  tabVisibilityListener = () => {
    if (document.visibilityState === 'visible') {
      if (this.isSumTotalCourse) {
        let userId = this.props.currentUser.id;
        let eclId = this.state.card.eclId;
        // Request the progress of the course from the ECL ID and User ID
        _agent
          .post(`${window.process.env['api.lms']}/api/v1/user_courses/progress_update`)
          .set({ 'X-API-TOKEN': JWT.token })
          .set({ 'X-Edcast-JWT': true })
          .send({
            user_id: userId,
            ecl_id: eclId
          })
          .end((err, response) => {
            // Completion rate should be updated on LRS side now, update card itself
            console.error(`Error in InsightContainer.tabVisibilityListener.func: ${err}`);
            this.fetchCards();
          });
      } else {
        this.fetchCards();
      }
    }
  };

  fetchCards = () => {
    fetchCardForStandaloneLayout(this.props.routeParams.splat, { is_standalone_page: true })
      .then(data => {
        // Let's update the card if some items have changed
        if (
          data &&
          this.state.card &&
          (data.completionState !== this.state.card.completionState ||
            data.completedPercentage !== this.state.card.completedPercentage)
        ) {
          this.setState({ card: data });
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  render() {
    if (this.state.pending) {
      return (
        <div className="text-center">
          <Spinner />
        </div>
      );
    }

    if (this.state.unavailable) {
      return (
        <div className="row container-padding">
          <Paper className="small-12">
            <div className="container-padding vertical-spacing-large text-center">
              <p>{tr('This content does not exist or has been deleted.')}</p>
            </div>
          </Paper>
        </div>
      );
    }

    let card = this.state.card;
    let author;
    if (card.author !== undefined && card.author.id !== undefined) {
      author = card.author;
    }
    return (
      <div className="stand-alone">
        <div className="row container-padding-insight-v2">
          <Insight
            card={card}
            author={author}
            showComment={Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')}
            isStandalone={true}
            isStandaloneModal={this.props.isStandaloneModal}
            dataCard={this.props.dataCard}
            cardUpdated={this.props.cardUpdated}
          />
        </div>
      </div>
    );
  }
}

InsightContainer.propTypes = {
  routeParams: PropTypes.object,
  dataCard: PropTypes.object,
  isStandaloneModal: PropTypes.bool,
  route: PropTypes.object,
  currentUser: PropTypes.object,
  cardUpdated: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS(),
    users: state.users.toJS()
  };
}

export default connect(mapStoreStateToProps)(InsightContainer);
