import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Sortable from 'sortablejs';
import { tr } from 'edc-web-sdk/helpers/translations';
import Select from 'react-select';
import debounce from 'lodash/debounce';
import { searchCardsInChannel } from 'edc-web-sdk/requests/channels.v2';
import HorizontalBarMenuIcon from '../../common/HorizontalBarMenuIcon';
import Checkbox from 'material-ui/Checkbox';
import EditIcon from 'material-ui/svg-icons/image/edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import Card from '../../cards/Card';
import { addItem, getCarouselItems, reorderItem } from 'edc-web-sdk/requests/carousels';

class CarouselListItem extends Component {
  constants = {
    addCardText: tr('Add Card'),
    loadingText: tr('Loading...'),
    noResultFoundText: tr('No results found'),
    selectCardPlaceholder: tr('Select Card'),
    closeLabel: tr('Close')
  };

  state = {
    editCarouselContent: false,
    options: [],
    carouselCardsIds: [],
    carouselCards: [],
    cards: [],
    searchChannelText: '',
    carouselId: +this.props.carousel.id,
    isVisible: this.props.carousel.visible
  };


  styles = {
    thumbStyle: {
      backgroundColor: '#009688'
    }
  };

  searchQuery = {
    limit: 8,
    offset: 0
  };

  componentDidMount() {
    if (this.props.carousel.custom_carousel) {
      this.fetchCarouselItems();
    }
  }

  fetchCarouselItems = () => {
    getCarouselItems(this.state.carouselId)
      .then(response => {
        if (response.structuredItems) {
          this.setState(() => ({
            carouselCards: response.structuredItems.map(obj => obj.entity),
            carouselCardsIds: response.structuredItems.map(obj => obj.entity.id)
          }));
        }
      })
      .catch(error => {
        console.error('Error in CarouselListItem.fetchCarouselItems', error);
      });
  };

  editCarousel = () => {
    this.setState(
      prevState => ({
        editCarouselContent: !prevState.editCarouselContent
      }),
      () => {
        if (this.state.editCarouselContent) {
          const list = document.getElementById(`custom-carousel-${this.props.index}`);
          Sortable.create(list, {
            handle: '.move-card-btn',
            animation: 150,
            forceFallback: true,
            onUpdate: this.changeCardOrder
          });
        }
      }
    );
  };

  // card reordering callback
  changeCardOrder = e => {
    const carouselCards = [...this.state.carouselCards];
    carouselCards[e.newIndex] = this.state.carouselCards[e.oldIndex];
    carouselCards[e.oldIndex] = this.state.carouselCards[e.newIndex];
    // e.newIndex index start from 0 and card oder start from 1 i.e. e.newIndex + 1
    this.updateCardPositionInCarousel(carouselCards[e.newIndex].id, e.newIndex + 1);
    this.setState(() => ({ carouselCards }));
  };

  handleInputChange = debounce(
    input => {
      const searchChannelText = input;
      if (searchChannelText !== this.state.searchChannelText) {
        this.searchQuery.offset = 0;
      }
      this.setState(
        () => ({
          isLoadingExternally: true,
          searchChannelText
        }),
        () => {
          this.getChannelCards();
        }
      );
    },
    800,
    { leading: false, trailing: true }
  );

  getChannelCards() {
    let payload = {
      limit: this.searchQuery.limit,
      offset: this.searchQuery.offset,
      q: this.state.searchChannelText,
      skip_aggs: true,
      is_cms: false
    };
    searchCardsInChannel(this.props.channel.id, payload)
      .then(data => {
        if (data.cards && !!data.cards.length) {
          this.setState(
            () => ({ cards: [...this.state.cards, ...data.cards] }),
            () => {
              this.removeAlreadyAdded(this.state.cards);
            }
          );
        } else {
          this.setState(() => ({ isLoadingExternally: false }));
        }
      })
      .catch(error => {
        this.setState(() => ({ isLoadingExternally: false }));
        console.error('error in CarouselListItem.handleInputChange', error);
      });
  }

  removeAlreadyAdded = items => {
    const suggestions = [];
    items.forEach(item => {
      if (this.state.carouselCardsIds.indexOf(item.id) === -1) {
        const label = `${item.title || item.message}`.slice(0, 50);
        suggestions.push({ value: item.id, label: label });
      }
    });
    this.setState(() => ({ options: suggestions, isLoadingExternally: false }));
  };

  handleChannelChange = card => {
    this.setState(() => ({
      selectedCard: card
    }));
  };

  scrollToBottom = () => {
    this.searchQuery.offset += this.searchQuery.limit;
    this.getChannelCards();
  };

  clearSearchOptions = () => {
    this.setState(() => ({
      offset: 0,
      options: [],
      searchChannelText: ''
    }));
  };

  addCardToCarousel = () => {
    const carouselCard = this.state.cards.find(obj => obj.id === this.state.selectedCard.value);
    const payload = {
      entity_id: carouselCard.id,
      entity_type: 'Card',
      structure_id: this.state.carouselId
    };
    addItem(payload, this.state.carouselId)
      .then(() => {
        this.setState(() => ({
          carouselCards: [...this.state.carouselCards, carouselCard],
          carouselCardsIds: [...this.state.carouselCards, carouselCard].map(obj => obj.id),
          selectedCard: {}
        }));
      })
      .catch(error => {
        console.error('Error in CarouselListItem.addCardToCarousel', error);
      });
  };

  removeCardFromCarousel = removeCard => {
    const carouselCards = this.state.carouselCards.filter(card => card.id !== removeCard.id);
    this.setState(() => ({ carouselCards }));
  };

  updateCardPositionInCarousel = (cardId, position) => {
    const payload = {
      entity_id: cardId,
      entity_type: 'Card',
      position
    };
    reorderItem(payload, this.state.carouselId)
      .then(() => {})
      .catch(error => {
        console.error(
          'Error in CarouselListItem.updateCardPositionInCarousel',
          error
        );
      });
  };

  toggleCarouselVisibility = () => {
    this.setState((prevState) => ({
      isVisible: !prevState.isVisible
    }), () => {
      this.props.toggleVisibility(this.props.carousel);
    })
  }

  deleteItem = () => {
    this.props.deleteCarousel(this.props.carousel);
  };

  render() {
    const { carousel } = this.props;
    return (
      <div className="list-container" key={this.props.key}>
        <div className="carousel-list">
          <div className="name">
            <div className="sort-handler">
              <HorizontalBarMenuIcon />
            </div>
            <p>{carousel.default_label}</p>
          </div>
          <div className="status">
            <div className="toggle-container">
              <Checkbox
                onCheck={this.toggleCarouselVisibility}
                checked={this.state.isVisible}
              />
            </div>
          </div>
          <div className="action">
            {carousel.custom_carousel && !this.state.editCarouselContent && (
              <div className="action-buttons">
                <div onClick={this.editCarousel} className="edit">
                  <EditIcon color={'#6f708b'} />
                </div>
                <div className="delete" onClick={this.deleteItem}>
                  <DeleteIcon color={'#d6d6e1'} />
                </div>
              </div>
            )}
            {carousel.custom_carousel && this.state.editCarouselContent && (
              <div className="action-buttons">
                <label className="close-label" onClick={this.editCarousel}>
                  {this.constants.closeLabel}
                </label>
              </div>
            )}
          </div>
        </div>
        {carousel.custom_carousel && this.state.editCarouselContent && (
          <div className="custom-carousel-setup">
            <div className="add-card-container">
              <Select
                cache={false}
                onChange={this.handleChannelChange}
                value={this.state.selectedCard}
                onMenuScrollToBottom={this.scrollToBottom}
                filterOption={() => true}
                placeholder={this.constants.selectCardPlaceholder}
                noResultsText={
                  this.state.isLoadingExternally
                    ? this.constants.loadingText
                    : this.constants.noResultFoundText
                }
                options={this.state.options}
                onBlur={this.clearSearchOptions}
                isLoading={this.state.isLoadingExternally}
                onInputChange={this.handleInputChange}
              />
              <button className="add-card-btn" onClick={this.addCardToCarousel}>
                {this.constants.addCardText}
              </button>
            </div>
            <div
              id={`custom-carousel-${this.props.index}`}
              className="custom-carousel-cards-container"
            >
              {!!this.state.carouselCards.length &&
                this.state.carouselCards.map((card, index) => (
                  <Card
                    key={card.id}
                    card={card}
                    fullView={true}
                    dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                    startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                    author={card.author}
                    user={this.currentUser}
                    isMarkAsCompleteDisabled={true}
                    withoutCardModal={true}
                    type={this.viewRegime}
                    removeCardFromList={this.removeCardFromList}
                    showControls
                    index={index + 1}
                    removeCardFromPathway={this.removeCardFromCarousel.bind(this, card)}
                  />
                ))}
            </div>
          </div>
        )}
      </div>
    );
  }
}

CarouselListItem.propTypes = {
  carouselsLength: PropTypes.number,
  carousel: PropTypes.object,
  index: PropTypes.number,
  channel: PropTypes.object,
  key: PropTypes.string,
  toggleVisibility: PropTypes.func,
  deleteCarousel: PropTypes.func
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(CarouselListItem);
