import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactTags from 'react-tag-autocomplete';
import _ from 'lodash';
import IconButton from 'material-ui/IconButton';
import ImagePhotoCamera from 'material-ui/svg-icons/image/photo-camera';
import Checkbox from 'material-ui/Checkbox';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import {
  getNonChannelCurators,
  getNonChannelCollaborators,
  getTrustedCollaborators
} from 'edc-web-sdk/requests/channels.v2';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import * as channelsSDK from 'edc-web-sdk/requests/channels';
import { saveHtmlWidget, getHtmlWidget, updateHtmlWidget } from 'edc-web-sdk/requests/htmlWidget';
import { createCarousel, putCarousel, deleteCarousel } from 'edc-web-sdk/requests/carousels';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import { fileStackSources } from '../../../constants/fileStackSource';
import { uploadPolicyAndSignature, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';
import FlatButton from 'material-ui/FlatButton';

import { filestackClient } from '../../../utils/filestack';
import { getChannelSources } from '../../../actions/eclActions';
import TextField from 'material-ui/TextField';
import debounce from 'lodash/debounce';
import { languageKeyVal } from '../../../constants/languages';
import { snackBarOpenClose } from '../../../actions/channelsActionsV2';
import Sortable from 'sortablejs';
import CarouselListItem from './CarouselListItem';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import CreateHtmlWidget from '../../common/CreateHtmlWidget';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';
const viewColor = '#454560';

export class ChannelSetting extends Component {
  constants = {
    uploadImageText: tr('Upload Channel Image'),
    imageHintText: tr('Recommended Size: 480px x 320px'),
    charactersRemaining: tr('Characters Remaining'),
    collaboratorHintText: tr('Add Collaborators, Collaborators can post to this channel'),
    trustedCollaboratorsHintText: tr(
      'Add Trusted Collaborators, Trusted Collaborators can post to this channel'
    ),
    addCuratorHintText: tr(
      'Add Curators, Curators can configure, curate, and post to this channel(unless role is modified by admin under RBAC settings)'
    ),
    contentModrationHintText: tr(
      `Content posted to this channel has to be moderated by Curator before making it visible in the channel`
    ),
    contentCuratedNote: tr(`The Content posted by the followers of this channel can be curated`),
    whoCanPost: tr('Who can post to this channel?'),
    ccfCheckboxLabel: tr('Curators, Collaborators, and Followers'),
    ccCheckboxLabel: tr('Curators and Collaborators only'),
    curateFollowerslabel: tr('Curate followers content'),
    curatebleChannelCheckboxLabel: tr('Curatable Channel'),
    advanceSettingText: tr('Advanced Settings'),
    titleLabel: tr('Title'),
    descriptionLable: tr('Description'),
    moreLable: tr('more'),
    changeImageText: tr('Change Image'),
    updateLable: tr('Update Channel'),
    cancelLabel: tr('Cancel'),
    carouselLabel: tr('Carousel'),
    nameLabel: tr('Name'),
    statusLabel: tr('Enable/Disable'),
    actionLabel: tr('Action'),
    addLabel: tr('Add'),
    carouselNamePlaceholder: tr('Enter Name'),
    customCarouselError: tr('Unable to add custom carousel, Please try again!'),
    htmlWidgetLabel: tr('HTML Widget'),
    htmlWidgetPlaceholder: tr('Write your announcements here and you can add link by simply adding an anchor tag like <a href="link to the source">Click here</a>'),
    saveLabel: tr('Save'),
    savingLabel: tr('Saving...'),
    htmlWidgetSavedMessage: tr('HTML widget saved successfully'),
    htmlWidgetUpdatedMessage: tr('HTML widget updated successfully'),
    htmlWidgetErrorMessage: tr('HTML widget unable to store, Please try again!'),
  };

  state = {
    securedUrl: '',
    collaboratorsMore: [],
    collaboratorsCurrent: [],
    allowTrustedCollaborators: false,
    trustedCollaboratorsMore: false,
    trustedCollaboratorsCurrent: [],
    curatorsMore: [],
    curatorsCurrent: [],
    showAdvancedSettings: false,
    collaboratorOptions: [],
    collaboratorsRemove: [],
    trustedCollaboratorOptions: [],
    trustedCollaboratorsRemove: [],
    curatorOptions: [],
    curatorsRemove: [],
    label: this.props.channel.label,
    image: this.props.channel.image,
    imageInit: this.props.channel.bannerImageUrl,
    profileImageUrl: this.props.channel.profileImageUrl,
    id: this.props.channel.id,
    curateOnly: this.props.channel.curateOnly,
    curateUgc: this.props.channel.curateUgc,
    eclEnabled: this.props.channel.eclEnabled,
    onlyAuthorsCanPost: this.props.channel.onlyAuthorsCanPost,
    description: this.props.channel.description,
    isPrivate: this.props.channel.isPrivate,
    isOpen: this.props.channel.isOpen, // we don't have this field now
    allowFollow: this.props.channel.allowFollow,
    publishLabel: this.constants.updateLable,
    carousels: this.props.channel.carousels,
    carouselName: '',
    widget: null,
    widgetId: null,
    updatingWidget: false
  };
  styles = {
    mainImage: {
      height: '2rem',
      width: '2rem',
      marginTop: '4.625rem',
      fill: darkPurp
    },
    checkboxLabelStyle: {
      fontSize: '0.75rem',
      lineHeight: 2,
      color: '#6f708b'
    },
    actionBtnLabel: {
      textTransform: 'none',
      fontFamily: 'OpenSans-Regular, Open Sans !important',
      fontSize: '0.75rem'
    },
    tagIconStyles: {
      height: '0.75rem',
      width: '1rem'
    },
    channelImg: {
      background: 'none'
    },
    checkOff: {
      width: '1.15rem',
      height: '1.15rem',
      border: '.0625rem solid #acadc1',
      borderRadius: '.125rem',
      boxSizing: 'border-box',
      margin: '0.2rem 0 0 0.19rem'
    },
    cancel: {
      color: '#6f708b',
      boxSizing: 'content-box',
      borderColor: lightPurp,
      borderStyle: 'solid',
      borderWidth: '0.062rem',
      textTransform: 'none',
      margin: '0.25rem 0.75rem 0.25rem 0.25rem',
      opacity: '0.6',
      lineHeight: '1rem',
      height: '1.875rem'
    }
  };

  componentDidMount() {
    this.getChannelCurators(this.props.channel.id);
    this.getChannelCollaborators(this.props.channel.id);
    this.getChannelTrustedCollaborators(this.props.channel.id);
    this.fetchHtmlWidget();
  }

  getChannelCurators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.curatorsTotal : this.state.limit,
      offset: this.state.curatorsOffset
    };
    channelsSDK
      .getCurators(channelId, payload, true)
      .then(data => {
        let curators = data.curators !== undefined ? data.curators : [];
        let curatorsOffset = this.state.curatorsOffset + curators.length;
        let curatorsList = _.uniqBy(this.state.curatorsCurrent.concat(curators), 'id');
        let isLastPage = data.total < curatorsList.length;
        let curatorsMore = data.total - curatorsList.length;
        if (data.total > curatorsList.length) {
          curatorsList.push({ id: 'vm', name: 'view more' });
        }
        if (loadMore) {
          this.setState({
            moreItems: curatorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            curatorsCurrent: curatorsList,
            curatorsTotal: data.total,
            curatorsMore,
            isLastPage,
            curatorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelSetting.channelsSDK.getCurators.func : ${err}`);
      });
  }

  getChannelCollaborators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.collaboratorsTotal : this.state.limit,
      offset: this.state.collaboratorsOffset
    };
    channelsSDK
      .getCollaborators(channelId, payload, true)
      .then(data => {
        let collaborators = data.authors;
        let collaboratorsOffset = this.state.collaboratorsOffset + collaborators.length;
        let collaboratorsList = _.uniqBy(
          this.state.collaboratorsCurrent.concat(collaborators),
          'id'
        );
        let isLastPage = data.total <= collaboratorsList.length;
        let collaboratorsMore = data.total - collaboratorsList.length;
        if (data.total > collaboratorsList.length) {
          collaboratorsList.push({ id: 'vm', name: 'view more' });
        }
        if (loadMore) {
          this.setState({
            moreItems: collaboratorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            collaboratorsCurrent: collaboratorsList,
            collaboratorsTotal: data.total,
            collaboratorsMore,
            isLastPage,
            collaboratorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelSetting.channelsSDK.getCollaborators.func : ${err}`);
      });
  }

  getChannelTrustedCollaborators(channelId, loadMore) {
    let payload = {
      limit: loadMore ? this.state.trustedCollaboratorsTotal : this.state.limit,
      offset: this.state.collaboratorsOffset
    };
    getTrustedCollaborators(channelId, payload, true)
      .then(data => {
        let trustedCollaborators = data.trustedCollaborators;
        let trustedCollaboratorsOffset =
          this.state.trustedCollaboratorsOffset + trustedCollaborators.length;
        let collaboratorsList = _.uniqBy(
          this.state.trustedCollaboratorsCurrent.concat(trustedCollaborators),
          'id'
        );
        let isLastPage = data.total <= collaboratorsList.length;
        let trustedCollaboratorsMore = data.total - collaboratorsList.length;
        if (data.total > collaboratorsList.length) {
          collaboratorsList.push({ id: 'vm', name: 'view more' });
        }
        if (loadMore) {
          this.setState({
            moreItems: collaboratorsList,
            isLastPage: true
          });
        } else {
          this.setState({
            trustedCollaboratorsCurrent: collaboratorsList,
            trustedCollaboratorsTotal: data.total,
            trustedCollaboratorsMore,
            isLastPage,
            trustedCollaboratorsOffset
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelSetting.channelsSDK.getCollaborators.func : ${err}`);
      });
  }

  handleAddition(itemsType, tag) {
    let itemType = itemsType;
    let itemsRemove = this.state[`${itemType}sRemove`];
    let isOld = _.remove(itemsRemove, function(item) {
      return item.id.toString() === tag.id.toString();
    });

    if (isOld.length === 0) {
      tag.isNew = true;
    }
    let current = this.state[`${itemType}sCurrent`];
    current.push(tag);

    this.setState({ [`${itemType}sCurrent`]: current, [`${itemType}sRemove`]: itemsRemove });
  }

  handleDelete = (itemsType, tag) => {
    let itemType = itemsType;
    const current = this.state[`${itemType}sCurrent`].slice(0);
    const itemsRemove = this.state[`${itemType}sRemove`];
    let index = _.findIndex(current, function(item) {
      return item.id === tag.id;
    });
    let remove = current.splice(index, 1);
    if (remove[0].id === 'vm') {
      return;
    }
    if (remove[0] && !remove[0].isNew) {
      let name = remove[0].firstName ? remove[0].firstName + ' ' : '';
      name += remove[0].lastName ? remove[0].lastName : '';
      name += `(${remove[0].email})`;
      itemsRemove.push({ id: remove[0].id, name: name });
    }
    this.setState({ [`${itemType}sCurrent`]: current, [`${itemType}sRemove`]: itemsRemove });
  };

  getCollaboratorUsers = (input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchUserText;
    this.setState(
      {
        isLoadingCollaborator: true,
        searchUserText: input,
        collaboratorOptions: [],
        userSuggestionsOffset:
          isScrollToBottom && !isChanged
            ? this.state.collaboratorOptions + this.state.collaboratorOptions
            : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCollaborators(this.state.channel.id, {
            search_term: input,
            limit: 10,
            offset: this.state.userSuggestionsOffset || 0
          })
            .then(data => {
              let currentOptions = this.state.collaboratorOptions;
              let collaboratorOptions = data.notAuthors
                .filter(
                  item =>
                    !~this.state.collaboratorsCurrent.findIndex(
                      collaborator => collaborator.id == item.id
                    )
                )
                .map(item => {
                  let label = item.firstName ? item.firstName + ' ' : '';
                  label += item.lastName ? item.lastName : '';
                  label += `( ${item.email} )`;
                  return { id: item.id, name: label, label: 'member' };
                });
              collaboratorOptions = isScrollToBottom
                ? currentOptions.concat(collaboratorOptions)
                : collaboratorOptions;

              this.setState({
                collaboratorOptions,
                isLoadingCollaborator: false
              });
            })
            .catch(err => {
              console.error(
                `Error in ChannelEditor.getNonChannelCollaborators.getItems.func : ${err}`
              );
            });
        } else {
          this.handleInputChange(input, 'collaborator');
        }
      }
    );
  };

  getTrustedCollaboratorUsers = (input, isScrollToBottom) => {
    let isChanged = input !== this.state.searchTrustedUserText;
    this.setState(
      {
        isLoadingTrustedCollaborator: true,
        searchTrustedUserText: input,
        trustedCollaboratorOptions: [],
        trustedUserSuggestionsOffset:
          isScrollToBottom && !isChanged
            ? this.state.trustedCollaboratorOptions + this.state.trustedCollaboratorOptions
            : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCollaborators(this.state.channel.id, {
            search_term: input,
            limit: 10,
            offset: this.state.trustedUserSuggestionsOffset || 0,
            trusted: true
          })
            .then(data => {
              let currentOptions = this.state.trustedCollaboratorOptions;
              let trustedCollaboratorOptions = data.notAuthors
                .filter(
                  item =>
                    !~this.state.trustedCollaboratorsCurrent.findIndex(
                      collaborator => collaborator.id == item.id
                    )
                )
                .map(item => {
                  let label = `${item.firstName ? `${item.firstName} ` : ''} ${item.lastName ||
                    ''}${item.email ? `( ${item.email || ''} )` : ''}`;
                  return { id: item.id, name: label, label: 'trustedCollaborator' };
                });
              trustedCollaboratorOptions = isScrollToBottom
                ? currentOptions.concat(trustedCollaboratorOptions)
                : trustedCollaboratorOptions;
              this.setState({
                trustedCollaboratorOptions,
                isLoadingTrustedCollaborator: false
              });
            })
            .catch(err => {
              console.error(
                `Error in ChannelEditor.getNonChannelCollaborators.getItems.func : ${err}`
              );
            });
        } else {
          this.handleInputChange(input, 'trustedCollaborator');
        }
      }
    );
  };

  handleInputChange(query, itemsType) {
    let payload = {
      limit: this.state[`${itemsType}SuggestionsLimit`] || 10,
      offset: this.state[`${itemsType}SuggestionsOffset`] || 0,
      q: query
    };
    usersSDK
      .getItems(payload)
      .then(members => {
        if (members && members.items) {
          let options = members.items
            .filter(
              item =>
                !~this.state[`${itemsType}sCurrent`].findIndex(curator => curator.id == item.id)
            )
            .map(item => {
              let label = item.firstName ? item.firstName + ' ' : '';
              label += item.lastName ? item.lastName : '';
              label += `( ${item.email} )`;
              return { id: item.id, name: label };
            });
          this.setState({
            [`${itemsType}Options`]: options
          });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelEditor.usersSDK.getItems.func : ${err}`);
      });
  }

  fileStackHandler = callback => {
    let fromSources = fileStackSources;
    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;
        filestackClient(policy, signature)
          .pick({
            accept: ['image/*'],
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [900, 400],
            imageMin: [200, 200],
            fromSources
          })
          .then(fileStack => {
            callback(fileStack);
          })
          .catch(err => {
            console.error(`Error in ChannelSetting.filestackClient.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(
          `Error in ChannelSetting.fileStackHandler.uploadPolicyAndSignature.func : ${err}`
        );
      });
  };

  handleProfileImageChange(image) {
    if (image.filesUploaded && image.filesUploaded.length) {
      let url = image.filesUploaded[0].url;
      let securedUrl;
      refreshFilestackUrl(url)
        .then(resp => {
          securedUrl = resp.signed_url;
          this.setState({ profileImageUrl: url, securedUrl: securedUrl });
        })
        .catch(err => {
          console.error(`Error in ChannelSetting.handleProfileImageChange.func : ${err}`);
        });
    }
  }

  getCuratorUser = input => {
    let isChanged = input !== this.state.searchCuratorText;
    this.setState(
      {
        searchCuratorText: input,
        curatorOptions: [],
        curatorSuggestionsOffset: !isChanged
          ? this.state.curatorSuggestionsOffset + this.state.limit
          : 0
      },
      () => {
        if (this.state.channel && this.state.channel.id) {
          getNonChannelCurators(this.state.channel.id, {
            search_term: input,
            limit: this.state.limit,
            offset: this.state.curatorSuggestionsOffset || 0
          })
            .then(data => {
              let curatorOptions = data.notCurators
                .filter(
                  item => !~this.state.curatorsCurrent.findIndex(curator => curator.id == item.id)
                )
                .map(item => {
                  let label = item.firstName ? item.firstName + ' ' : '';
                  label += item.lastName ? item.lastName : '';
                  label += `( ${item.email} )`;
                  return { id: item.id, name: label };
                });

              this.setState({
                curatorOptions
              });
            })
            .catch(err => {
              console.error(`Error in ChannelSetting.getNonChannelCurators.func : ${err}`);
            });
        } else {
          this.handleInputChange(input, 'curator');
        }
      }
    );
  };

  selectedRestrictedToTags(type, tag) {
    let moreCount = this.state[`${type}sMore`];
    return tag.tag.id === 'vm' ? (
      <a
        className="react-tags_link"
        key={`tag-${type}-${tag.tag.id}`}
        onClick={event => this.openViewMore(event, type)}
      >
        +{moreCount || 0} {this.constants.moreLable}
      </a>
    ) : (
      <span
        key={`tag-${type}-${tag.tag.id}`}
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={
          this.state.isOpenViewMore
            ? this.handleMoreDelete.bind(this, tag.tag)
            : this.handleDelete.bind(this, type, tag.tag)
        }
      >
        {(type === 'curator' || type === 'collaborator' || type === 'trustedCollaborator') && (
          <MemberBadgev2 viewBox="0 2 24 18" color="#555" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  }

  hoverBlock(onClick) {
    return (
      <span
        className="roll-image"
        onClick={onClick}
        onMouseEnter={() => {
          this.setState({ mainImage: true });
        }}
        style={{ display: this.state.mainImage ? 'inherit' : 'none' }}
      >
        <IconButton aria-label={this.constants.changeImageText} style={this.styles.imageBtn}>
          <ImagePhotoCamera color="white" />
        </IconButton>
        <div className="roll-text">{this.constants.changeImageText}</div>
      </span>
    );
  }

  toggleAdvancedSettings = e => {
    e.preventDefault();
    this.setState(
      prevState => {
        return {
          showAdvancedSettings: !prevState.showAdvancedSettings
        };
      },
      () => {
        const list = document.getElementById('carousels-list');
        Sortable.create(list, {
          handle: '.sort-handler',
          animation: 150,
          forceFallback: true,
          onUpdate: this.changeOrder
        });
      }
    );
  };

  changeOrder = e => {
    const carousels = this.state.carousels;
    const sortData = carousels[e.oldIndex];
    carousels.splice(e.oldIndex, 1);
    carousels.splice(e.newIndex, 0, sortData);
    carousels.forEach((c, i) => (c.index = i));
    const payload = { carousels };
    channelsSDK.editChannelCarousels(this.props.channel.id, payload);
  };

  saveChannel = () => {
    if (this.state.label.length === 0) {
      return;
    }
    this.setState({ showFollowModal: false });
    const channel = {};
    channel.label = this.state.label;
    channel.description = this.state.description;
    channel.profile_image_url =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.profileImageUrl =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.mobile_image =
      this.state.profileImageUrl ||
      'http://dp598loym07sk.cloudfront.net/assets/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'; // || default banner from fileStack
    channel.is_private = this.state.isPrivate;
    channel.allow_follow = this.state.allowFollow;
    channel.only_authors_can_post = this.state.onlyAuthorsCanPost;
    channel.curate_only = this.state.curateOnly;
    channel.curate_ugc = this.state.curateUgc;
    channel.ecl_enabled = this.state.eclEnabled;
    if (this.state.openChannel) {
      channel.is_open = this.state.isOpen;
    }
    const channelPayload = { ...this.props.channel, ...channel };
    this.setState({ publishLabel: 'Processing...', isCreating: true });
    channelsSDK
      .editChannel(channelPayload)
      .then(data => {
        if (data.message) {
          this.setState({
            errorMessage: data.message.replace('Label', 'Name'),
            isCreating: false,
            publishLabel: 'Update channel'
          });
        } else {
          this.updateCollaborators(channelPayload.id, false);
          this.updateCollaborators(channelPayload.id, true);
          this.updateCurators(channelPayload.id);
          this.setState({ publishLabel: 'Update channel', isCreating: false });
        }
      })
      .catch(err => {
        console.error(`Error in ChannelSetting.saveChannel : ${err}`);
      });
  };

  updateCollaborators = (channelId, isTrusted) => {
    let collaboratorIds = this.getNewTagsIds(isTrusted ? 'trustedCollaborator' : 'collaborator');
    if (collaboratorIds.length > 0) {
      channelsSDK.addCollaborators(collaboratorIds, [], channelId, isTrusted).catch(err => {
        console.error(`Error in ChannelSetting.channelsSDK.addCollaborators : ${err}`);
      });
    }
    let removeIds = this.getRemoveTagsIds(isTrusted ? 'trustedCollaborator' : 'collaborator');
    if (removeIds && removeIds.length) {
      channelsSDK.deleteCollaborators(channelId, removeIds, isTrusted).catch(err => {
        console.error(`Error in ChannelSetting.channelsSDK.deleteCollaborators : ${err}`);
      });
    }
  };

  updateCurators = channelId => {
    let curatorIds = this.getNewTagsIds('curator');
    if (curatorIds.length > 0) {
      let payLoad = {
        id: channelId,
        user_ids: curatorIds
      };
      channelsSDK.addCurators(payLoad).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.addCurators : ${err}`);
      });
    }
    let removeIds = this.getRemoveTagsIds('curator');
    if (removeIds.length > 0) {
      channelsSDK.removeCurator(channelId, removeIds).catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.removeCurator : ${err}`);
      });
    }
  };

  getNewTagsIds = itemsType => {
    return this.state[`${itemsType}sCurrent`]
      .filter(item => {
        return item.hasOwnProperty('isNew');
      })
      .map(item => item.id);
  };

  getRemoveTagsIds = itemsType => {
    return this.state[`${itemsType}sRemove`].map(item => item.id);
  };

  toggleUgcCuration = () => {
    this.setState(prevState => ({
      curateUgc: !prevState.curateUgc
    }));
  };

  toggleCuration = () => {
    let curateOnly = !this.state.curateOnly;
    let curateUgc = this.state.curateUgc;
    if (this.state.curateOnly) {
      curateUgc = false;
    }
    this.setState({
      curateOnly: curateOnly,
      curateUgc: curateUgc
    });
  };

  toggleOnlyAuthors = () => {
    this.setState(prevState => ({
      onlyAuthorsCanPost: !prevState.onlyAuthorsCanPost
    }));
  };

  updateCarouselName = e => {
    const carouselName = e.target.value;
    this.setState(() => ({ carouselName }));
  };

  addToCarousel = () => {
    const payload = {
      context: 'channel',
      display_name: this.state.carouselName,
      enabled: false,
      parent_id: this.props.channel.id,
      parent_type: 'Channel',
      slug: 'cards'
    };
    createCarousel(payload)
      .then(response => {
        if (response) {
          const newCarouselEntry = {
            default_label: response.displayName,
            index: this.state.carousels.length,
            visible: false,
            id: response.id,
            custom_carousel: true
          };
          const carousels = [...this.state.carousels, newCarouselEntry];
          const carouselPayload = { carousels };
          channelsSDK.editChannelCarousels(this.props.channel.id, carouselPayload);
          this.setState(() => ({ carousels, carouselName: '' }));
        }
      })
      .catch(error => {
        this.props.dispatch(openSnackBar(this.constants.customCarouselError, true));
        console.error(`Error in ChannelEditModal.addToCarousel.createCarousel : ${error}`);
      });
  };

  toggleVisibility = carousel => {
    const carousels = this.state.carousels.map(data => {
      if (data.default_label === carousel.default_label) {
        data.visible = !data.visible;
      }
      return data;
    });
    if (carousel.id) {
      const structurePayload = {
        enabled: carousel.visible,
        id: carousel.id
      };
      putCarousel(structurePayload);
    }
    const carouselPayload = { carousels };
    channelsSDK.editChannelCarousels(this.props.channel.id, carouselPayload);
    this.setState(() => ({ carousels }));
  };

  deleteCarousel = carousel => {
    const carousels = this.state.carousels.filter(data => {
      if (data.default_label !== carousel.default_label) {
        return data;
      }
    });
    if (carousel.id) {
      deleteCarousel(carousel);
    }
    const carouselPayload = { carousels };
    channelsSDK.editChannelCarousels(this.props.channel.id, carouselPayload);
    this.setState(() => ({ carousels }));
  };

  fetchHtmlWidget = () => {
    const payload = {
      widget: {
        parent_id: this.props.channel.id,
        parent_type: 'Channel',
        context: 'channel',
        enabled: true
      }
    }
    getHtmlWidget(payload)
      .then((response) => {
        if (response && !!response.widgets.length) {
          this.setState(() => ({ 
            widget: response.widgets[0]
          }))
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  saveUpdateWidget = (widget) => {
    const payload = {
      widget: {
        parent_id: this.props.channel.id,
        parent_type: 'Channel',
        context: 'channel',
        enabled: widget.isWidgetEnabled,
        code: widget.widgetContext,
      }
    }
    this.setState(() => ({ updatingWidget: true }));
    if (this.state.widget.id) {
      payload.id = this.state.widget.id;
      updateHtmlWidget(payload)
        .then(() => {
          this.setState(() => ({ updatingWidget: false }));
          this.props.dispatch(openSnackBar(this.constants.htmlWidgetUpdatedMessage, true))
        })
        .catch((error) => {
          this.setState(() => ({ updatingWidget: false }));
          this.props.dispatch(openSnackBar(this.constants.htmlWidgetErrorMessage, true))
          console.log('Error in ChannelSetting.saveUpdateWidget.updateHtmlWidget', error);
        });
    } else {
      saveHtmlWidget(payload)
        .then((response) => {
          this.setState(() => ({ updatingWidget: false }));
          this.props.dispatch(openSnackBar(this.constants.htmlWidgetSavedMessage, true));
        })
        .catch((error) => {
          this.setState(() => ({ updatingWidget: false }));
          this.props.dispatch(openSnackBar(this.constants.htmlWidgetErrorMessage, true));
          console.log('Error in ChannelSetting.saveUpdateWidget.saveHtmlWidget', error);
        });
    }
  }

  render() {
    const collaboratorSuggestions = _.uniqWith(
      [...this.state.collaboratorOptions, ...this.state.collaboratorsRemove],
      _.isEqual
    );
    const trustedCollaboratorSuggestions = _.uniqWith(
      [...this.state.trustedCollaboratorOptions, ...this.state.trustedCollaboratorsRemove],
      _.isEqual
    );
    const curatorSuggestions = _.uniqWith(
      [...this.state.curatorOptions, ...this.state.curatorsRemove],
      _.isEqual
    );
    return (
      <div className="common-block">
        <div className="row">
          <div className="small-12">
            <div className="edit-container">
              <div className="row">
                <div className="small-12 medium-3">
                  {!this.state.profileImageUrl && (
                    <div
                      className="item-banner"
                      style={this.styles.channelImg}
                      onClick={this.fileStackHandler.bind(
                        this,
                        this.handleProfileImageChange.bind(this)
                      )}
                    >
                      <div className="text-center">
                        <ImagePhotoCamera style={this.styles.mainImage} />
                        <div className="empty-image" style={{ marginTop: '-.5rem' }}>
                          {this.constants.uploadImageText}
                        </div>
                      </div>
                    </div>
                  )}
                  {this.state.profileImageUrl && (
                    <div className="item-banner preview-upload">
                      <div
                        className="card-img-container"
                        onMouseEnter={() => {
                          this.setState({ mainImage: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ mainImage: false });
                        }}
                      >
                        <div
                          className="card-img button-icon"
                          style={{
                            ...this.styles.pathwayImage,
                            ...{
                              backgroundImage: `url(\'${this.state.securedUrl ||
                                this.state.profileImageUrl}\')`
                            }
                          }}
                        >
                          {this.hoverBlock(
                            this.fileStackHandler.bind(
                              this,
                              this.handleProfileImageChange.bind(this)
                            )
                          )}
                        </div>
                      </div>
                    </div>
                  )}
                  <div className="hint-text mb-20">{this.constants.imageHintText}</div>
                </div>
                <div className="small-12 medium-9">
                  <div className="input-block" style={{ paddingBottom: '1.375rem' }}>
                    <input
                      className="my-input"
                      type="text"
                      maxLength={150}
                      value={this.state.label}
                      placeholder={this.constants.titleLabel}
                      ref={node => (this._label = node)}
                      onChange={e => {
                        this.setState({ label: e.target.value });
                      }}
                    />
                    <div className="hint-text">{`${150 - this.state.label.length}/150 ${
                      this.constants.charactersRemaining
                    }`}</div>
                  </div>
                  <div className="input-block">
                    <input
                      className="my-input"
                      type="text"
                      maxLength={2000}
                      value={this.state.description}
                      placeholder={this.constants.descriptionLable}
                      ref={node => (this._description = node)}
                      onChange={e => {
                        this.setState({ description: e.target.value });
                      }}
                    />
                    <div className="hint-text">{`${2000 - this.state.description.length}/2000 ${
                      this.constants.charactersRemaining
                    }`}</div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="small-12">
                  <div
                    className={`${
                      this.state.collaboratorsMore > 0 ? 'with-view-more' : ''
                    } input-block`}
                  >
                    <ReactTags
                      tags={this.state.collaboratorsCurrent}
                      suggestions={collaboratorSuggestions}
                      placeholder=""
                      tagComponent={this.selectedRestrictedToTags.bind(this, 'collaborator')}
                      handleAddition={this.handleAddition.bind(this, 'collaborator')}
                      handleDelete={this.handleDelete.bind(this, 'collaborator')}
                      handleInputChange={this.getCollaboratorUsers.bind(this)}
                    />
                    <div className="hint-text">{this.constants.collaboratorHintText}</div>
                  </div>
                </div>
              </div>
              {this.state.allowTrustedCollaborators && (
                <div className="row">
                  <div className="small-12">
                    <div
                      className={`${
                        this.state.trustedCollaboratorsMore > 0 ? 'with-view-more' : ''
                      } input-block`}
                    >
                      <ReactTags
                        tags={this.state.trustedCollaboratorsCurrent}
                        suggestions={trustedCollaboratorSuggestions}
                        placeholder=""
                        tagComponent={this.selectedRestrictedToTags.bind(
                          this,
                          'trustedCollaborator'
                        )}
                        handleAddition={this.handleAddition.bind(this, 'trustedCollaborator')}
                        handleDelete={this.handleDelete.bind(this, 'trustedCollaborator')}
                        handleInputChange={this.getTrustedCollaboratorUsers.bind(this)}
                      />
                      <div className="hint-text">{this.constants.trustedCollaboratorsHintText}</div>
                    </div>
                  </div>
                </div>
              )}
              <div className="row">
                <div className="small-12">
                  <div
                    className={`${this.state.curatorsMore > 0 ? 'with-view-more' : ''} input-block`}
                    style={{ paddingBottom: '1.43rem' }}
                  >
                    <ReactTags
                      tags={this.state.curatorsCurrent}
                      suggestions={curatorSuggestions}
                      placeholder=""
                      tagComponent={this.selectedRestrictedToTags.bind(this, 'curator')}
                      handleAddition={this.handleAddition.bind(this, 'curator')}
                      handleDelete={this.handleDelete.bind(this, 'curator')}
                      handleInputChange={this.getCuratorUser.bind(this)}
                    />
                    <div className="hint-text">{this.constants.addCuratorHintText}</div>
                  </div>
                </div>
              </div>
              <div>
                <div className="input-block" style={{ paddingBottom: '0.875rem' }}>
                  <div className="row">
                    <div className="small-12 medium-8 large-9">
                      <span className="advanced-toggle" onTouchTap={this.toggleAdvancedSettings}>
                        {this.constants.advanceSettingText}
                      </span>
                    </div>
                  </div>
                </div>
                {this.state.showAdvancedSettings && (
                  <div>
                    <div className="row">
                      <div className="small-12">
                        <div className="input-block" style={{ paddingBottom: '1rem' }}>
                          <Checkbox
                            style={this.styles.privateContent}
                            labelStyle={this.styles.checkboxLabelStyle}
                            checked={this.state.curateOnly}
                            label={this.constants.curatebleChannelCheckboxLabel}
                            onCheck={this.toggleCuration}
                            aria-label={this.constants.curatebleChannelCheckboxLabel}
                            uncheckedIcon={<div style={this.styles.checkOff} />}
                          />
                          <div className="hint-text label-spacing">
                            {this.constants.contentModrationHintText}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="small-12">
                        <div className="input-block">
                          <Checkbox
                            className="checkbox"
                            style={this.styles.privateContent}
                            labelStyle={this.styles.checkboxLabelStyle}
                            checked={this.state.curateUgc}
                            label={this.constants.curateFollowerslabel}
                            onCheck={this.toggleUgcCuration}
                            aria-label={this.constants.curateFollowerslabel}
                            uncheckedIcon={<div style={this.styles.checkOff} />}
                          />
                          <div className="hint-text label-spacing">
                            {this.constants.contentCuratedNote}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="small-3">
                        <span className="radio-channel">{this.constants.whoCanPost}</span>
                      </div>
                      <div className="small-9">
                        <div
                          className="input-block"
                          style={{ paddingLeft: '.5rem', paddingBottom: '1.375rem' }}
                        >
                          <Checkbox
                            className="checkbox"
                            style={{ paddingBottom: '.3rem' }}
                            labelStyle={this.styles.checkboxLabelStyle}
                            iconStyle={{ marginRight: '6px' }}
                            checked={!this.state.onlyAuthorsCanPost}
                            label={this.constants.ccfCheckboxLabel}
                            onCheck={this.toggleOnlyAuthors}
                            aria-label={this.constants.ccfCheckboxLabel}
                            uncheckedIcon={<div style={this.styles.checkOff} />}
                          />
                          <Checkbox
                            className="checkbox"
                            style={this.styles.privateContent}
                            labelStyle={this.styles.checkboxLabelStyle}
                            iconStyle={{ marginRight: '6px' }}
                            checked={this.state.onlyAuthorsCanPost}
                            label={this.constants.ccCheckboxLabel}
                            onCheck={this.toggleOnlyAuthors}
                            aria-label={this.constants.ccCheckboxLabel}
                            uncheckedIcon={<div style={this.styles.checkOff} />}
                          />
                        </div>
                      </div>
                    </div>
                    <div>
                      {!!this.state.carousels.length && (
                        <div>
                          <p className="carousel-section-title">{this.constants.carouselLabel}</p>
                          <div className="carousel-list-header">
                            <div className="name">
                              <label>{this.constants.nameLabel}</label>
                            </div>
                            <div className="status">
                              <label>{this.constants.statusLabel}</label>
                            </div>
                            <div className="action">
                              <label>{this.constants.actionLabel}</label>
                            </div>
                          </div>
                          <div id="carousels-list">
                            {this.state.carousels.map((carousel, index) => (
                              <CarouselListItem
                                index={index}
                                key={`carousel.default_label=${index}`}
                                carousel={carousel}
                                carouselsLength={this.state.carousels.length}
                                channel={this.props.channel}
                                toggleVisibility={this.toggleVisibility}
                                deleteCarousel={this.deleteCarousel}
                              />
                            ))}
                          </div>
                        </div>
                      )}
                      <div className="add-carousel-container mb-20">
                        <div className="carousel-name-input">
                          <input
                            value={this.state.carouselName}
                            onChange={this.updateCarouselName}
                            placeholder={this.constants.carouselNamePlaceholder}
                          />
                        </div>
                        <button className="add-carousel-btn" onClick={this.addToCarousel}>
                          {this.constants.addLabel}
                        </button>
                      </div>
                    </div>
                    <CreateHtmlWidget 
                      saveUpdateWidget={this.saveUpdateWidget}
                      widget={this.state.widget}
                      updatingWidget={this.state.updatingWidget}
                    />
                  </div>
                )}
              </div>
              <div className="action-buttons">
                <FlatButton
                  label={this.constants.cancelLabel}
                  disabled={this.state.isCreating}
                  style={this.styles.cancel}
                  labelStyle={this.styles.actionBtnLabel}
                  onTouchTap={this.props.goBack}
                />
                <PrimaryButton
                  label={tr(this.state.publishLabel)}
                  disabled={this.state.isCreating || this.state.label.trim().length === 0}
                  onTouchTap={this.saveChannel}
                  labelStyle={this.styles.actionBtnLabel}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChannelSetting.propTypes = {
  channel: PropTypes.object,
  goBack: PropTypes.func
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ChannelSetting);
