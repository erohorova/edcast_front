import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactTags from 'react-tag-autocomplete';
import _ from 'lodash';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Checkbox from 'material-ui/Checkbox';
import List from 'material-ui/List';
import ListItem from 'material-ui/List/ListItem';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import FlatButton from 'material-ui/FlatButton';
import {
  saveChannelProgramming,
  deleteElSources,
  deleteTopics
} from 'edc-web-sdk/requests/channels.v2';
import * as channelsSDK from 'edc-web-sdk/requests/channels';
import * as eclSDK from 'edc-web-sdk/requests/ecl';
import * as topicsSdk from 'edc-web-sdk/requests/topics';
import { getChannelSources } from '../../../actions/eclActions';
import { langs } from '../../../constants/languages';
import { snackBarOpenClose } from '../../../actions/channelsActionsV2';
import { close } from '../../../actions/modalActions';

const darkPurp = '#6f708b';
const lightPurp = '#acadc1';
const viewColor = '#454560';

export class ChannelTunning extends Component {
  constants = {
    contentTypes: [
      {
        name: 'video',
        id: 'video'
      },
      {
        name: 'poll',
        id: 'poll'
      },
      {
        name: 'insight',
        id: 'insight'
      },
      {
        name: 'pathway',
        id: 'pathway'
      },
      {
        name: 'article',
        id: 'article'
      },
      {
        name: 'course',
        id: 'course'
      },
      {
        name: 'Video Stream',
        id: 'video_stream'
      },
      {
        name: 'journey',
        id: 'journey'
      },
      {
        name: 'project',
        id: 'project'
      }
    ],
    sourceLoadingText: tr('Loading sources...'),
    sourcePlaceholder: tr('  Search and select sources'),
    loadingTopicText: tr('Loading topics...'),
    topicPlaceholder: tr('  Search and select topics'),
    contentPlaceholder: tr('  Search and select content types eg.video, poll'),
    languagePlaceholder: tr('  Search and select languages eg.English, Hindi'),
    openTopicAlert: tr('For open source at least one topic need to be selected'),
    cardLimitPlaceholder: tr('Card fecth limit 500'),
    maxCardLimitAlert: tr('Minimum 1 and Maximum 500 cards')
  };

  state = {
    openSourceId: '1d97649b-7969-4111-8e82-b770f942bcb3',
    eclSourcesCurrent: [],
    eclSourcesAll: [],
    channelSourcesIds: [],
    topicsCurrent: [],
    topicsAll: [],
    channelTopicsIds: [],
    contentTypesCurrent: [],
    contentTypesAll: [],
    languagesCurrent: [],
    languagesAll: [],
    sourcesLoading: true,
    loadingTopics: true,
    showRequiredTopic: false,
    cardFetchLimit: this.props.channel.cardFetchLimit,
    channelRedesign: window.ldclient.variation('channel-redesigning', false),
    cardV3: window.ldclient.variation('card-v3', false),
    showCardAlert: false
  };

  styles = {
    closeBtn: {
      paddingRight: 0,
      width: 'auto'
    },
    actionBtnLabel: {
      textTransform: 'none',
      fontFamily: 'OpenSans-Regular, Open Sans !important',
      fontSize: '0.75rem'
    },
    removeBtn: {
      padding: '0',
      marginTop: '.75rem'
    },
    cancel: {
      color: '#6f708b',
      boxSizing: 'content-box',
      borderColor: lightPurp,
      borderStyle: 'solid',
      borderWidth: '0.062rem',
      textTransform: 'none',
      margin: '0.25rem 0.75rem 0.25rem 0.25rem',
      opacity: '0.6',
      lineHeight: '1rem',
      height: '1.875rem'
    }
  };

  componentDidMount() {
    this.getChannelTopics(this.props.channel.id);
    this.getEclChannelSources(this.props.channel.id);
    this.getLanguage();
    this.getContentType();
  }

  getLanguage() {
    let languagesCurrent = [];
    let languagesAll = [];
    for (const key in langs) {
      if (langs.hasOwnProperty(key)) {
        const temp = {};
        temp.name = key;
        temp.id = langs[key];
        languagesAll.push(temp);
      }
    }

    this.props.channel.language.map(lang => {
      let index = languagesAll.findIndex(language => language.id === lang);
      if (index !== -1) {
        languagesCurrent.push(languagesAll[index]);
        languagesAll.splice(index, 1);
      }
    });
    this.setState({ languagesCurrent, languagesAll });
  }

  getContentType() {
    let contentTypesCurrent = [];
    let contentTypesAll = this.constants.contentTypes;

    this.props.channel.contentType.map(contentType => {
      let index = contentTypesAll.findIndex(content => content.id === contentType);
      if (index !== -1) {
        contentTypesCurrent.push(contentTypesAll[index]);
        contentTypesAll.splice(index, 1);
      }
    });
    this.setState({ contentTypesAll, contentTypesCurrent });
  }

  getEclChannelSources(channelId) {
    eclSDK
      .getChannelSources(channelId)
      .then(channelSources => {
        this.setState(
          {
            sourcesLoading: true
          },
          () => {
            let channelSourcesIds = [];
            let eclSourcesCurrent = [];
            channelSources.map(source => {
              if (source.eclSourceId === this.state.openSourceId) {
                source.displayName = 'Open Source';
              }
              channelSourcesIds.push(source.eclSourceId);
              eclSourcesCurrent.push({ id: source.eclSourceId, name: source.displayName });
            });
            eclSDK
              .getAllSources({ is_enabled: true })
              .then(data => {
                let suggs = [];
                let optionObj;
                data.map(item => {
                  if (channelSourcesIds.indexOf(item.id) === -1) {
                    optionObj = { id: item.id, name: item.display_name };
                    suggs.push(optionObj);
                  }
                });
                if (channelSourcesIds.indexOf(this.state.openSourceId) === -1) {
                  suggs.push({
                    name: 'Open Source',
                    id: this.state.openSourceId
                  });
                }
                this.setState({
                  channelSourcesIds,
                  channelSources,
                  eclSourcesCurrent,
                  eclSourcesAll: suggs,
                  sourcesLoading: false
                });
              })
              .catch(err => {
                this.setState({ sourcesLoading: false });
                console.error(`Error in ChannelEditModal.eclSDK.getAllSources.func : ${err}`);
              });
          }
        );
      })
      .catch(err => {
        console.error(
          `Error in ChannelEditModal.getEclChannelSources.getEclChannelSources.func : ${err}`
        );
      });
  }

  getChannelTopics(channelId) {
    channelsSDK
      .getTopics(channelId)
      .then(async data => {
        let channelTopics = (data && data.topics) || [];
        let channelTopicsIds = [];
        let topicsCurrent = [];
        let topicsAll = [];

        channelTopics.map(topic => {
          channelTopicsIds.push(topic.id);
          topicsCurrent.push({
            ...topic,
            id: topic.id,
            actualName: topic.name,
            name: (topic.name || '').replace(/\./g, ' > ')
          });
        });
        let queryData = await topicsSdk.getQueryTopics('');
        if (queryData.response === 'success') {
          queryData.topics.forEach(topic => {
            if (channelTopicsIds.indexOf(topic.id) === -1) {
              topicsAll.push({
                ...topic,
                id: topic.id,
                actualName: topic.name,
                name: (topic.name || '').replace(/\./g, ' > ')
              });
            }
          });
        } else {
          console.error(`Error in ChannelEditModal.topicsSdk.getQueryTopics.func`);
        }
        this.setState({
          channelTopicsIds,
          channelTopics,
          topicsCurrent,
          topicsAll,
          loadingTopics: false
        });
      })
      .catch(err => {
        console.error(`Error in ChannelEditModal.channelsSDK.getTopics.func : ${err}`);
      });
  }

  handleAddition(itemsType, tag) {
    let current = this.state[`${itemsType}sCurrent`];
    let allType = this.state[`${itemsType}sAll`].filter(content => content.id !== tag.id);

    current.push(tag);

    this.setState({ [`${itemsType}sCurrent`]: current, [`${itemsType}sAll`]: allType });
  }

  handleDeleteByIds = (itemsType, tagId) => {
    //Checking index of deletion
    if (tagId >= 0) {
      let current = this.state[`${itemsType}sCurrent`].slice(0);
      let tag = current[tagId];
      let allType = this.state[`${itemsType}sAll`];

      current.splice(tagId, 1);
      allType.push(tag);

      this.setState({ [`${itemsType}sCurrent`]: current, [`${itemsType}sAll`]: allType });
    }
  };

  deleteSourceCall = item => {
    let channelSources = this.state.channelSources;
    let sourceIndex = this.state.channelSources.findIndex(
      source => source.eclSourceId === item.eclSourceId
    );
    if (~sourceIndex) {
      let payload = { ecl_source: { ecl_source_id: item.eclSourceId } };
      eclSDK.deleteChannelSource(this.props.channel.id, payload);
      channelSources.splice(sourceIndex, 1);
      this.setState({ channelSources });
    }
  };

  deleteTopicCall = item => {
    let channelTopics = this.state.channelTopics;
    let topicIndex = this.state.channelTopics.findIndex(topic => topic.id === item.id);
    if (~topicIndex) {
      channelsSDK.removeTopic(this.props.channel.id, item.id);
      channelTopics.splice(topicIndex, 1);
      this.setState({ channelTopics });
    }
  };

  programChannel = () => {
    let openSourceIndex = this.state.eclSourcesCurrent.findIndex(obj => obj.name === 'Open Source');
    let openSourceTopic = openSourceIndex !== -1 ? this.state.topicsCurrent.length > 0 : true;
    if (openSourceTopic) {
      this.setState(() => ({ savingChannel: true }));
      //channel Id
      const channelId = this.props.channel.id;

      //Handling source addition and deletion
      const allSelectedSourcesIds = this.state.eclSourcesCurrent.map(source => source.id);
      //All channel sources  = channelSourcesIds
      const sourcesToAddIds = _.difference(allSelectedSourcesIds, this.state.channelSourcesIds);
      const sourcesToDeleteIds = _.difference(this.state.channelSourcesIds, allSelectedSourcesIds);
      const sourcesToAdd = this.state.eclSourcesCurrent.filter(source => {
        let index = sourcesToAddIds.findIndex(id => id === source.id);
        if (index !== -1) {
          return source;
        }
      });

      //Handling topic addition and deletion
      const allSelectedTopicIds = this.state.topicsCurrent.map(topic => topic.id);
      //All channel topic = channelTopicsIds
      const topicsToAddIds = _.difference(allSelectedTopicIds, this.state.channelTopicsIds);
      const topicsToDeleteIds = _.difference(this.state.channelTopicsIds, allSelectedTopicIds);
      const topicsToAdd = this.state.topicsCurrent.filter(topic => {
        let index = topicsToAddIds.findIndex(id => id === topic.id);
        if (index !== -1) {
          return topic;
        }
      });

      //Deletion Source
      if (sourcesToDeleteIds.length > 0) {
        deleteElSources(channelId, { ecl_source: { ecl_source_ids: sourcesToDeleteIds } })
          .then(() => {})
          .catch(err => {
            console.error(`Error in contentTuning.programChannel.sourceDelete.func : ${err}`);
          });
      }

      //Deleting Topic
      if (topicsToDeleteIds.length > 0) {
        deleteTopics(channelId, { topic: { topic_ids: topicsToDeleteIds } })
          .then(() => {})
          .catch(err => {
            console.error(`Error in contentTuning.programChannel.topicDelete.func : ${err}`);
          });
      }

      //programming channel with new additions
      const channelPayload = {
        channel: {
          language: this.state.languagesCurrent.map(obj => obj.id),
          content_type: this.state.contentTypesCurrent.map(obj => obj.id),
          card_fetch_limit: this.state.cardFetchLimit,
          ecl_sources: sourcesToAdd.map(source => {
            return { source_id: source.id, displayName: source.name };
          }),
          topics: topicsToAdd.map(topic => {
            return {
              id: topic.id,
              name: topic.actualName,
              label: topic.label,
              description: topic.description,
              image_url: topic.image_url,
              taxo_id: topic.taxo_id,
              domain: topic.domain
            };
          })
        }
      };
      saveChannelProgramming(channelId, channelPayload)
        .then(() => {
          //Go to channels page
          this.setState(() => ({ savingChannel: false }));
          this.gotoBack();
        })
        .catch(err => {
          const errMessage = JSON.parse(err.response.text).message;
          this.setState(
            () => ({ showMessage: true, statusMessage: errMessage }),
            () => {
              setTimeout(() => {
                this.setState(() => ({ showMessage: false }));
              }, 5000);
            }
          );
          console.error(`Error in contentTuning.programChannel.func : ${err}`);
        });
    } else {
      this.setState(
        () => ({ showRequiredTopic: true }),
        () => {
          setTimeout(() => {
            this.setState(() => ({ showRequiredTopic: false }));
          }, 5000);
        }
      );
    }
  };

  inputChange = event => {
    if ((event.target.value > 500 || event.target.value < 1) && event.target.value !== '') {
      this.setState(() => ({
        showCardAlert: true
      }));
    } else {
      this.setState({
        cardFetchLimit: event.target.value,
        showCardAlert: false
      });
    }
  };

  gotoBack = () => {
    if (this.state.channelRedesign && this.state.cardV3) {
      this.props.goBack();
    } else {
      this.props.dispatch(close());
    }
  };

  render() {
    return (
      <div className="common-block">
        <div className="row">
          <div className="small-12">
            <div className="edit-container">
              <div className="row">
                <div className="small-12 medium-12 large-12">
                  <div className="input-block">
                    <div>
                      <ReactTags
                        tags={this.state.eclSourcesCurrent}
                        suggestions={this.state.eclSourcesAll}
                        handleDelete={this.handleDeleteByIds.bind(this, 'eclSource')}
                        handleAddition={this.handleAddition.bind(this, 'eclSource')}
                        placeholder={
                          this.state.sourcesLoading ? this.constants.sourceLoadingText : ''
                        }
                      />
                      <div className="hint-text">{this.constants.sourcePlaceholder}</div>
                    </div>
                  </div>
                </div>
              </div>
              {false && this.state.eclSourcesCurrent && !!this.state.eclSourcesCurrent.length && (
                <div className="table-container">
                  <div className="row">
                    <div className="column small-10">
                      <strong className="list-text">Name</strong>
                    </div>
                    <div className="column small-2">
                      <strong className="list-text">Private</strong>
                    </div>
                  </div>
                  {this.state.eclSourcesCurrent.map((item, index) => (
                    <div className="row">
                      <div className="column small-10">
                        <span className="list-text">{item.name}</span>
                      </div>
                      {item.id != this.state.openSourceId && (
                        <div className="column small-2">
                          <Checkbox
                            onCheck={e => {
                              this.markSourcePrivate(e, index);
                            }}
                          />
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              )}
              {this.state.eclSourcesCurrent && !!this.state.eclSourcesCurrent.length && (
                <div>
                  <div className="row">
                    <div className="small-12 medium-12 large-12">
                      <div className="input-block">
                        <ReactTags
                          tags={this.state.topicsCurrent}
                          suggestions={this.state.topicsAll}
                          handleDelete={this.handleDeleteByIds.bind(this, 'topic')}
                          handleAddition={this.handleAddition.bind(this, 'topic')}
                          placeholder={
                            this.state.loadingTopics ? this.constants.loadingTopicText : ''
                          }
                          classNames={{
                            root: 'react-tags',
                            rootFocused: 'is-focused',
                            selected: 'react-tags__selected',
                            selectedTag: 'react-tags__selected-tag',
                            selectedTagName: 'react-tags__selected-tag-name',
                            search: 'react-tags__search',
                            searchInput: 'react-tags__search-input',
                            suggestions: 'react-tags__suggestions react-tags__suggestions-width',
                            suggestionActive: 'is-active',
                            suggestionDisabled: 'is-disabled'
                          }}
                        />
                        <div className="hint-text">{this.constants.topicPlaceholder}</div>
                        {this.state.showRequiredTopic && (
                          <p className="alert-text">{this.constants.openTopicAlert}</p>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="small-12 medium-12 large-12">
                      <div className="input-block">
                        <ReactTags
                          tags={this.state.contentTypesCurrent}
                          suggestions={this.state.contentTypesAll}
                          handleDelete={this.handleDeleteByIds.bind(this, 'contentType')}
                          handleAddition={this.handleAddition.bind(this, 'contentType')}
                          placeholder=""
                        />
                        <div className="hint-text">{this.constants.contentPlaceholder}</div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="small-12 medium-12 large-12">
                      <div className="input-block">
                        <ReactTags
                          tags={this.state.languagesCurrent}
                          suggestions={this.state.languagesAll}
                          handleDelete={this.handleDeleteByIds.bind(this, 'language')}
                          handleAddition={this.handleAddition.bind(this, 'language')}
                          placeholder=""
                        />
                        <div className="hint-text">{this.constants.languagePlaceholder}</div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="small-12 medium-12 large-12">
                      <div className="input-block">
                        <input
                          type="number"
                          className="my-input"
                          value={this.state.cardFetchLimit}
                          onChange={this.inputChange}
                          min={1}
                          max={500}
                        />
                        <div className="hint-text">{this.constants.cardLimitPlaceholder}</div>
                        {this.state.showCardAlert && (
                          <p className="alert-text">{this.constants.maxCardLimitAlert}</p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
              {
                //Below section is not needed for now
              }
              {false && (
                <div className="row">
                  <div className="small-12 medium-12 large-12">
                    {this.state.channelSources && !!this.state.channelSources.length && (
                      <div className="table-container">
                        <p className="channel-sources">
                          <strong>{tr('Previously Selected Sources')}</strong>
                        </p>
                        <List className="wide-item-list">
                          {this.state.channelSources.map(item => {
                            return (
                              <div
                                className="selected-source"
                                key={`channelSources-${item.eclSourceId}`}
                              >
                                <ListItem
                                  rightIcon={
                                    <IconButton
                                      style={{ ...this.styles.closeBtn, ...this.styles.removeBtn }}
                                      onTouchTap={this.deleteSourceCall.bind(this, item)}
                                    >
                                      <CloseIcon color="gray" />
                                    </IconButton>
                                  }
                                  innerDivStyle={this.styles.listItem}
                                  primaryText={
                                    <div className="row">
                                      <div className="column small-4">
                                        <p className="list-text">
                                          {item.eclSourceId == this.state.openSourceId
                                            ? 'Open Source'
                                            : item.displayName}
                                        </p>
                                      </div>
                                      <div className="column small-8">
                                        <p className="list-text">
                                          <strong>Restricted Groups: </strong>
                                          <span>
                                            {item.restrictedTeams
                                              .map(team => {
                                                return team.name;
                                              })
                                              .join()}
                                          </span>
                                        </p>
                                        <p className="list-text">
                                          <strong>Restricted Users: </strong>
                                          <span>
                                            {item.restrictedUsers
                                              .map(user => {
                                                return user.first_name + ' ' + user.last_name;
                                              })
                                              .join()}
                                          </span>
                                        </p>
                                      </div>
                                    </div>
                                  }
                                />
                              </div>
                            );
                          })}
                        </List>
                      </div>
                    )}
                    {
                      // Below section is not required for now
                    }
                    {false && this.state.channelTopics && !!this.state.channelTopics.length && (
                      <div className="table-container">
                        <p className="channel-sources">
                          <strong>{tr('Previously Selected Topics')}</strong>
                        </p>
                        <List className="wide-item-list">
                          {this.state.channelTopics.map(item => {
                            return (
                              <div className="selected-source" key={`channelTopics-${item.id}`}>
                                <ListItem
                                  rightIcon={
                                    <IconButton
                                      style={{ ...this.styles.closeBtn, ...this.styles.removeBtn }}
                                      onTouchTap={this.deleteTopicCall.bind(this, item)}
                                    >
                                      <CloseIcon color="gray" />
                                    </IconButton>
                                  }
                                  innerDivStyle={this.styles.listItem}
                                  primaryText={
                                    <div className="selected-source__text">{item.label || ' '}</div>
                                  }
                                />
                              </div>
                            );
                          })}
                        </List>
                      </div>
                    )}
                  </div>
                </div>
              )}
              <div className="action-buttons">
                <FlatButton
                  label={tr('Cancel')}
                  disabled={this.state.savingChannel}
                  style={this.styles.cancel}
                  labelStyle={this.styles.actionBtnLabel}
                  onTouchTap={this.gotoBack}
                />
                <PrimaryButton
                  label={this.state.savingChannel ? tr('Saving...') : tr('Save')}
                  disabled={this.state.savingChannel}
                  onTouchTap={this.programChannel}
                  labelStyle={this.styles.actionBtnLabel}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChannelTunning.propTypes = {
  channel: PropTypes.object,
  goBack: PropTypes.func
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps)(ChannelTunning);
