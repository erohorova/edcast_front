import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';
import ChannelSetting from './ChannelSetting';
import ChannelTunning from './ChannelTunning';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getChannel } from 'edc-web-sdk/requests/channels';
import { saveChannelDetails } from '../../../actions/channelsActionsV3';
import { duskColor } from 'edc-web-sdk/components/colors/colors';

class ChannelEdit extends Component {
  constants = {
    duskColor: duskColor,
    settingText: tr('Settings'),
    tunningText: tr('Tunning'),
    headerText: tr('Channel Settings')
  };

  state = {
    selectedTab: this.constants.settingText,
    channel: null
  };

  styles = {
    backIconStyle: {
      width: '1.5625rem'
    }
  };

  componentDidMount() {
    if (this.props.channel) {
      this.setState(() => ({ channel: this.props.channel }));
    } else {
      this.fetchChannelDetails();
    }
  }

  fetchChannelDetails = () => {
    getChannel(this.props.routeParams.slug)
      .then(data => {
        if (data) {
          this.props.dispatch(saveChannelDetails(data));
          this.setState(() => ({ channel: data, loadingDetails: false }));
        }
      })
      .catch(err => {
        console.error('Error in ChannelRedesignBase.fetchChannelDetails', err);
      });
  };

  changeTab = tab => {
    this.setState(() => ({ selectedTab: tab }));
  };

  goBack = () => {
    this.props.dispatch(goBack());
  };

  render() {
    return (
      <div className="channel-edit-modal channel-edit-standalone">
        {this.state.channel && (
          <div className="row position-relative">
            <div className="back-arrow-position" onClick={this.goBack}>
              <BackIcon style={this.styles.backIconStyle} color={this.constants.duskColor} />
            </div>
            <h4 className="header">{this.constants.headerText}</h4>
            <div className="tabs">
              <div className="tab" onClick={this.changeTab.bind(this, this.constants.settingText)}>
                <p
                  className={`${this.state.selectedTab === this.constants.settingText &&
                    'selected-teb-text'}`}
                >
                  {this.constants.settingText}
                </p>
              </div>
              <div className="tab" onClick={this.changeTab.bind(this, this.constants.tunningText)}>
                <p
                  className={`${this.state.selectedTab === this.constants.tunningText &&
                    'selected-teb-text'}`}
                >
                  {this.constants.tunningText}
                </p>
              </div>
            </div>
          </div>
        )}
        {this.state.selectedTab === this.constants.settingText && this.state.channel && (
          <ChannelSetting channel={this.state.channel} goBack={this.goBack} />
        )}
        {this.state.selectedTab === this.constants.tunningText && this.state.channel && (
          <ChannelTunning channel={this.state.channel} goBack={this.goBack} />
        )}
      </div>
    );
  }
}

ChannelEdit.propTypes = {
  channel: PropTypes.object,
  routeParams: PropTypes.object
};

const mapStateToProps = ({ channelReducerV3 }) => ({
  channel: channelReducerV3.get('channel')
});

export default connect(mapStateToProps)(ChannelEdit);
