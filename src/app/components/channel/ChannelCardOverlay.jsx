import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import PinIcon from 'edc-web-sdk/components/icons/Pin';
import UnpinIcon from 'edc-web-sdk/components/icons/Unpin';
import DeleteIcon from 'edc-web-sdk/components/icons/Delete';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { channelReorder } from 'edc-web-sdk/requests/channels.v2';
import { getTypeCardsAfterReordering } from '../../actions/channelsActionsV2';
import { Permissions } from '../../utils/checkPermissions';

class ChannelCardOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinned: (props.card && props.card.pinnedStatus) || false,
      disableButton: true,
      isReorderingActivated: window.ldclient.variation('reorder-content', false),
      rank: props.card && props.card.rank,
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      iconStyle: {
        width: '30px',
        height: '30px'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card && nextProps.card.pinnedStatus) {
      this.setState({ pinned: nextProps.card.pinnedStatus });
    }
  }

  pinUnpinCard = () => {
    if (this.props.pinnedCount == 6 && !this.state.pinned) {
      this.props.dispatch(openSnackBar('You can pin max 6 cards!', true));
      return;
    }
    this.props.pinUnpinCard(this.props.card, !this.state.pinned);
    this.setState({ pinned: !this.state.pinned });
  };

  removeCardLaunchModal = () => {
    this.props.removeCardLaunchModal(this.props.card);
  };

  reorderInputValidation = e => {
    let rank = e.target.value;
    if (rank >= 1 && rank <= this.props.allCardsCount) {
      this.setState({
        disableButton: false,
        rank
      });
    } else {
      this.setState({
        disableButton: true,
        rank
      });
    }
  };

  reorderClickHandler = (event, clickedOn) => {
    event.stopPropagation();
    this.props.reorderToggle(true);
    let cardType = [];
    switch (this.props.card.cardType) {
      case 'video_stream':
        cardType = ['video_stream'];
        break;
      case 'pack':
        cardType = ['pack'];
        break;
      case 'course':
        cardType = ['course'];
        break;
      case 'journey':
        cardType = ['journey'];
        break;
      default:
        cardType = ['media', 'poll'];
        break;
    }
    switch (clickedOn) {
      case 'top':
        channelReorder(this.props.channel.id, this.props.card.id, { rank: 1 })
          .then(() => {
            this.props
              .dispatch(getTypeCardsAfterReordering(this.props.channel.id, cardType, 12))
              .then(() => {
                this.props.setOffset();
                this.props.reorderToggle(false);
              })
              .catch(err => {
                console.error(
                  `Error in ChannelCardOverlay.getTypeCardsAfterReordering.func top : ${err}`
                );
              });
          })
          .catch(err => {
            console.error(`Error in ChannelCardOverlay.channelReorder.func top : ${err}`);
          });
        break;
      default:
        channelReorder(this.props.channel.id, this.props.card.id, { rank: this.state.rank })
          .then(() => {
            this.props
              .dispatch(getTypeCardsAfterReordering(this.props.channel.id, cardType, 12))
              .then(() => {
                this.props.setOffset();
                this.props.reorderToggle(false);
              })
              .catch(err => {
                console.error(
                  `Error in ChannelCardOverlay.getTypeCardsAfterReordering.func default : ${err}`
                );
              });
          })
          .catch(err => {
            console.error(`Error in ChannelCardOverlay.channelReorder.func default : ${err}`);
          });
        break;
    }
  };

  render() {
    let isSingleCard = this.props.allCardsCount === 1 ? true : false;
    return (
      <div className={`card-overlay-layer ${this.state.isCardV3 ? 'card-overlay-layer-v3' : ''}`}>
        {!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard && (
          <div className="row action-icons-row">
            <div className="small-6 columns">
              <div className="card-overlay-text">{tr('Enter Order')}</div>
            </div>
            <div className="small-6 columns input-block-style">
              <input
                value={this.state.rank}
                type="text"
                onChange={e => this.reorderInputValidation(e)}
                className="channel-rank-input"
                required
              />
              <button
                className="channel-rank-submit"
                onClick={e => this.reorderClickHandler(e, 'reorder')}
                disabled={this.state.disableButton}
              >
                {tr('OK')}
              </button>
            </div>
            <div className="small-12 columns">
              <div className="card-overlay-text">
                <span
                  name="top"
                  className="pointer"
                  onTouchTap={e => this.reorderClickHandler(e, 'top')}
                >
                  {tr('Top of List')}
                </span>
              </div>
            </div>
          </div>
        )}
        <div
          className={
            'row action-icons-row ' +
            (!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard
              ? 'not-custom-carousel'
              : 'no-reordering')
          }
        >
          <div className="small-12 columns icon-wrapper">
            {!this.state.pinned &&
              Permissions['enabled'] !== undefined &&
              Permissions.has('PIN_CONTENT') && (
                <div className="specific-icon-parent-wrapper">
                  <IconButton
                    className="specific-icon-wrapper"
                    aria-label="pin"
                    onTouchTap={this.pinUnpinCard}
                  >
                    <PinIcon customStyle={this.styles.iconStyle} />
                  </IconButton>
                  <div className="text-center icon-label">{tr('Pin to top')}</div>
                </div>
              )}
            {this.state.pinned &&
              Permissions['enabled'] !== undefined &&
              Permissions.has('PIN_CONTENT') && (
                <div className="specific-icon-parent-wrapper">
                  <IconButton
                    className="specific-icon-wrapper unpin"
                    aria-label="unpin"
                    onTouchTap={this.pinUnpinCard}
                  >
                    <UnpinIcon customStyle={this.styles.iconStyle} color={'#4de8ce'} />
                  </IconButton>
                  <div className="text-center icon-label">{tr('Un-pin')}</div>
                </div>
              )}
            <div className="specific-icon-parent-wrapper delete text-center">
              <IconButton
                className="specific-icon-wrapper delete-icon"
                aria-label="delete"
                onTouchTap={this.removeCardLaunchModal}
              >
                <DeleteIcon />
              </IconButton>
              <div className="text-center delete icon-label">{tr('Remove')}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChannelCardOverlay.propTypes = {
  card: PropTypes.object,
  allCardsCount: PropTypes.number,
  removeCardLaunchModal: PropTypes.func,
  pinUnpinCard: PropTypes.func,
  pinnedCount: PropTypes.number,
  channel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  reorderToggle: PropTypes.func,
  setOffset: PropTypes.func
};

export default connect()(ChannelCardOverlay);
