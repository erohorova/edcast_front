import React from 'react';
import PropTypes from 'prop-types';
import Avatar from 'material-ui/Avatar';

let defaultImage = '/i/images/default_user_blue.png';

export default class ContributorTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contributors: this.props.data
    };
  }

  render() {
    return (
      <ol className="contributor-table">
        {this.state.contributors.map((c, i) => {
          let image = c.photo ? c.photo : defaultImage;
          return (
            <li key={`contributor-${i}`} className="contributor-row">
              <Avatar src={image} size={30} />
              <span className="contributor-name">
                {c.firstName} {c.lastName}
              </span>
              <span className="contributor-score">{c.score}</span>
            </li>
          );
        })}
      </ol>
    );
  }
}

ContributorTable.propTypes = {
  data: PropTypes.array
};
