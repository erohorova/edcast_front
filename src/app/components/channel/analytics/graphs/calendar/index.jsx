// Calendar for displaying information
import React from 'react';
import PropTypes from 'prop-types';
import AreaGraph from '../area';

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

export default class Calendar extends React.Component {
  constructor(props) {
    super(props);
    let initDate = this.props.startDate;
    let lastDayOfMonth = new Date(initDate.getFullYear(), initDate.getMonth() + 1, 0);
    let lastDayOfPreviousMonth = new Date(initDate.getFullYear(), initDate.getMonth(), 0);
    let startCalendarOn = lastDayOfPreviousMonth.getDate() - lastDayOfPreviousMonth.getDay();

    let calendarInit = [
      ...[...new Array(lastDayOfPreviousMonth.getDate() + 1).keys()].slice(startCalendarOn),
      ...[...new Array(lastDayOfMonth.getDate() + 1).keys()].slice(1)
    ]; // Create array of 5 weeks

    // Add the remaining days if it's less than 5 weeks
    if (calendarInit.length < 35) {
      calendarInit = [
        ...calendarInit,
        ...[...new Array(8).keys()].slice(1, Math.abs(calendarInit.length - 35) + 1)
      ];
    }

    this.calendarInit = calendarInit;
  }

  render() {
    let startData = false;
    return (
      <div className="calendar-grid">
        <div className="calendar-day month-year">
          {MONTHS[this.props.startDate.getMonth()]} {this.props.startDate.getFullYear()}
        </div>
        {['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'].map((day, index) => {
          return (
            <div key={`weekday-${index}`} className="calendar-day weekday">
              {day}
            </div>
          );
        })}
        {this.calendarInit.map((day, index) => {
          if (day === 1) {
            startData = true;
          }

          let style = null;
          let hasData = null;
          if (startData) {
            let dateToCheck = `${this.props.startDate.getFullYear()}-${(
              '0' +
              (this.props.startDate.getMonth() + 1)
            ).slice(-2)}-${day}T00:00:00Z`;
            let eventData = this.props.channelEvents.filter(i => {
              return i.time === dateToCheck;
            });
            if (eventData && eventData.length && eventData[0].num_channel_follows > 0) {
              hasData = eventData[0].num_channel_follows;
              style = {
                backgroundColor: `rgba(69, 69, 96, ${eventData[0].num_channel_follows / 10})`,
                color: '#fff'
              };
            }
          }
          return (
            <div key={`day-${index}`} className="calendar-day" style={style}>
              {day}
              {hasData && <span className="calendar-data">{hasData} Users</span>}
            </div>
          );
        })}
      </div>
    );
  }
}

Calendar.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  channelEvents: PropTypes.any
};
