import React from 'react';
import PropTypes from 'prop-types';
import { PieChart, Pie, Cell, ResponsiveContainer, Legend } from 'recharts';
import AreaGraph from '../area';

const COLORS = ['#45455E', '#6F7089', '#ACADBF', '#D6D6E0'];

export default class SimplePieChart extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let map = {
      android: 0,
      extension: 0,
      ios: 0,
      web: 0
    };
    this.props.channelEvents.map(i => {
      map['android'] += i.num_monthly_active_users.android;
      map['extension'] += i.num_monthly_active_users.extension;
      map['ios'] += i.num_monthly_active_users.ios;
      map['web'] += i.num_monthly_active_users.web;
    });
    const data = [
      { name: 'Web', value: map.web },
      { name: 'iOS', value: map.ios },
      { name: 'Android', value: map.android },
      { name: 'Plugins', value: map.extension }
    ];
    return (
      <ResponsiveContainer height={300} width="100%">
        <PieChart width={300} height={300}>
          <Pie data={data} dataKey="value" labelLine={false} outerRadius={80} fill="#8884d8">
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
          </Pie>
          <Legend
            layout="vertical"
            align="right"
            wrapperStyle={{
              top: '0',
              bottom: '0',
              display: 'flex',
              alignItems: 'center',
              fontSize: '12px'
            }}
          />
        </PieChart>
      </ResponsiveContainer>
    );
  }
}

SimplePieChart.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  channelEvents: PropTypes.any
};
