import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import Calendar from './graphs/calendar';
import SimplePieChart from './graphs/pie';
import AreaGraph from './graphs/area';
import ContributorTable from './graphs/table';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import { getChannelMetrics } from 'edc-web-sdk/requests/analytics';
import { getChannel } from 'edc-web-sdk/requests/channels';
import { DateRangePicker } from 'react-date-range';
import Spinner from '../../common/spinner';

const TeamsLinearGraph = Loadable({
  loader: () => import('../../team/SingleTeam/analytics/TeamsLinearGraph'),
  loading() {
    return (
      <div style={{ marginLeft: '50%' }}>
        <Spinner />
      </div>
    );
  }
});

class ChannelAnalyticsContainer extends React.Component {
  constructor(props) {
    super(props);

    let startDate = new Date();
    startDate.setDate(startDate.getDate() - 7);
    startDate.setHours(0, 0, 0, 0);

    this.state = {
      startDate: startDate,
      endDate: new Date(),
      dateSelectorVisible: false,
      channelEvents: null,
      channelContributors: null,
      channelTopCards: null,
      channelLeaderboard: null,
      largeView: null
    };

    this.maxDate = new Date(); // Limit future date selection
    this.channelId = null;
    this.cardRender = {};
  }

  componentDidMount = async () => {
    let channelData = await getChannel(this.props.params.slug);
    this.channelId = channelData.id;
    if (channelData.isCurator === true || this.props.currentUser.isAdmin === true) {
      this.getChannelAnalyticsData();
    } else {
      this.setState({ unauthorized: true });
    }
  };

  componentWillUnmount() {
    document
      .querySelector('.channel-analytics-container')
      .removeEventListener('click', this.calendarClickOutsideEvent);
  }

  getChannelAnalyticsData = async () => {
    if (this.state.dateSelectorVisible) {
      this.setState({
        dateSelectorVisible: false,
        channelEvents: null
      });
    }
    let payload = {
      entity: 'channels',
      start_date: Math.floor(this.state.startDate.getTime() / 1000),
      end_date: Math.floor(this.state.endDate.getTime() / 1000)
    };
    let channelEvents = await getChannelMetrics(this.channelId, payload);
    this.setState({
      channelEvents: channelEvents[0].values,
      channelContributors: channelEvents[0].tags.top_contributors,
      channelTopCards: channelEvents[0].tags.top_cards,
      channelLeaderboard: channelEvents[0].tags.top_scoring_users
    });
  };

  handleDateRangeSelection = ranges => {
    this.setState({
      startDate: ranges.selection.startDate,
      endDate: ranges.selection.endDate
    });
  };

  handleObjectClick = (view, title) => {
    this.setState({
      largeView: view,
      largeViewTitle: title
    });
  };

  handleBackButtonClick = () => {
    if (this.state.largeView) {
      this.setState({ largeView: null });
    } else {
      window.history.back();
    }
  };

  generateCardRender = () => {
    this.cardRender['topCards'] = (
      <TeamsLinearGraph
        type={'content'}
        viewStyle={'group'}
        data={this.state.channelTopCards.slice(0, 4).map(o => {
          return {
            id: o.card_id,
            score: o.sort_score,
            title: o.title
          };
        })}
      />
    );
    this.cardRender['topCardsLarge'] = (
      <TeamsLinearGraph
        type={'content'}
        viewStyle={'group'}
        data={this.state.channelTopCards.map(o => {
          return {
            id: o.card_id,
            score: o.sort_score,
            title: o.title
          };
        })}
      />
    );
    this.cardRender['topContributors'] = (
      <TeamsLinearGraph
        type={'contributors'}
        viewStyle={'group'}
        data={this.state.channelContributors.slice(0, 4).map(o => {
          return {
            email: null,
            firstName: o.first_name,
            handle: null,
            id: o.user_id,
            lastName: o.last_name,
            photo: null,
            score: o.num_cards
          };
        })}
      />
    );
    this.cardRender['topContributorLarge'] = (
      <TeamsLinearGraph
        type={'contributors'}
        viewStyle={'group'}
        data={this.state.channelContributors.map(o => {
          return {
            email: null,
            firstName: o.first_name,
            handle: null,
            id: o.user_id,
            lastName: o.last_name,
            photo: null,
            score: o.num_cards
          };
        })}
      />
    );
    this.cardRender['calendar'] = <Calendar {...this.state} />;
    this.cardRender['platform'] = <SimplePieChart {...this.state} />;
    this.cardRender['leaderboard'] = (
      <ContributorTable
        data={this.state.channelLeaderboard.slice(0, 5).map(o => {
          return {
            email: null,
            firstName: o.first_name,
            handle: null,
            id: o.user_id,
            lastName: o.last_name,
            photo: null,
            score: o.total_user_score
          };
        })}
      />
    );
    this.cardRender['leaderboardLarge'] = (
      <ContributorTable
        data={this.state.channelLeaderboard.map(o => {
          return {
            email: null,
            firstName: o.first_name,
            handle: null,
            id: o.user_id,
            lastName: o.last_name,
            photo: null,
            score: o.total_user_score
          };
        })}
      />
    );
    this.cardRender['activeUsers'] = (
      <AreaGraph
        startDate={this.state.startDate.toLocaleDateString()}
        finishDate={this.state.endDate.toLocaleDateString()}
        strokeColor={null}
        strokeColorSecondary={null}
        fillColor="#D6D6E0"
        fillColorSecondary="#6F7089"
        modifyData={true}
        primaryDataKey="Current"
        graphValue={this.state.channelEvents.map(i => {
          return i.num_monthly_active_users.total;
        })}
      />
    );
    this.cardRender['channelTraffic'] = (
      <AreaGraph
        startDate={this.state.startDate.toLocaleDateString()}
        finishDate={this.state.endDate.toLocaleDateString()}
        strokeColor={null}
        strokeColorSecondary={null}
        fillColor="#D6D6E0"
        fillColorSecondary="#6F7089"
        modifyData={false}
        primaryDataKey="Visits"
        secondaryDataKey="Follows"
        yLabel="Follows/Visits"
        graphValue={this.state.channelEvents.map(i => {
          return {
            name: i.time.split('T')[0].substr(5),
            Visits: i.num_channel_visits,
            Follows: i.num_channel_follows
          };
        })}
      />
    );
    this.cardRender['cardContributions'] = (
      <AreaGraph
        startDate={this.state.startDate.toLocaleDateString()}
        finishDate={this.state.endDate.toLocaleDateString()}
        strokeColor={null}
        strokeColorSecondary={null}
        fillColor="#D6D6E0"
        fillColorSecondary="#6F7089"
        modifyData={false}
        primaryDataKey="Created"
        yLabel="Cards Count"
        graphValue={this.state.channelEvents.map(i => {
          return { name: i.time.split('T')[0].substr(5), Created: i.num_cards_added_to_channel };
        })}
      />
    );
    this.cardRender['cardCompletion'] = (
      <AreaGraph
        startDate={this.state.startDate.toLocaleDateString()}
        finishDate={this.state.endDate.toLocaleDateString()}
        strokeColor={null}
        strokeColorSecondary={null}
        fillColor="#D6D6E0"
        fillColorSecondary="#6F7089"
        modifyData={false}
        primaryDataKey="Completed"
        yLabel="Completed Count"
        graphValue={this.state.channelEvents.map(i => {
          return {
            name: i.time.split('T')[0].substr(5),
            Completed: i.num_cards_completed_in_channel
          };
        })}
      />
    );
    this.cardRender['cardsLiked'] = (
      <AreaGraph
        startDate={this.state.startDate.toLocaleDateString()}
        finishDate={this.state.endDate.toLocaleDateString()}
        strokeColor={null}
        strokeColorSecondary={null}
        fillColor="#D6D6E0"
        fillColorSecondary="#6F7089"
        modifyData={false}
        primaryDataKey="Liked"
        yLabel="Liked Count"
        graphValue={this.state.channelEvents.map(i => {
          return { name: i.time.split('T')[0].substr(5), Liked: i.num_cards_liked_in_channel };
        })}
      />
    );
  };

  breadCrumb = () => {
    const selectionRange = {
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      key: 'selection',
      color: '#45455C'
    };
    return (
      <div className="row">
        <div className="column small-6">
          <div aria-label="back" className="breadcrumbBack">
            <BackIcon
              onClick={this.handleBackButtonClick}
              style={{ width: '19px' }}
              color={'#454560'}
            />
          </div>
        </div>
        <div className="column small-6">
          <button
            className="date-selector"
            onClick={() => {
              this.setState({ dateSelectorVisible: true });
            }}
          >
            {this.state.startDate.toLocaleDateString()} - {this.state.endDate.toLocaleDateString()}{' '}
            ▾
          </button>
          {this.state.dateSelectorVisible && (
            <div
              className="date-selector-layer"
              onClick={() => {
                this.setState({ dateSelectorVisible: false });
              }}
            />
          )}
          <span className="date-selector-calendar">
            {this.state.dateSelectorVisible && (
              <div className="date-selector-wrapper">
                <DateRangePicker
                  ranges={[selectionRange]}
                  maxDate={this.maxDate}
                  onChange={this.handleDateRangeSelection}
                />
                <button className="date-selector-update" onClick={this.getChannelAnalyticsData}>
                  Update
                </button>
              </div>
            )}
          </span>
        </div>
      </div>
    );
  };

  render() {
    if (!this.state.channelEvents) {
      return (
        <div style={{ textAlign: 'center' }}>
          <Spinner />
        </div>
      );
    }
    if (Object.keys(this.cardRender).length === 0) {
      this.generateCardRender();
    }

    if (this.state.largeView) {
      return (
        <section className="channel-analytics-container">
          {this.breadCrumb()}
          <div className="row">
            <div className="column small-12">
              <div className="graph-card">
                <h3>{this.state.largeViewTitle}</h3>
                <h5 className="date">
                  {this.state.startDate.toLocaleDateString()} to{' '}
                  {this.state.endDate.toLocaleDateString()}
                </h5>
                {this.cardRender[this.state.largeView]}
              </div>
            </div>
          </div>
        </section>
      );
    }

    return (
      <section className="channel-analytics-container">
        {this.breadCrumb()}
        <div className="row">
          <div className="small-6 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'topCardsLarge', 'Top Cards')}
            >
              <h3>Top Cards</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['topCards']}
            </div>
          </div>
          <div className="small-6 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(
                this,
                'topContributorsLarge',
                'Top Contributors'
              )}
            >
              <h3>Top Contributors</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['topContributors']}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'calendar', 'New Users')}
            >
              <h3>New Users</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['calendar']}
            </div>
          </div>
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'platform', 'Active Users by Platform')}
            >
              <h3>Active Users by Platform</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['platform']}
            </div>
          </div>
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(
                this,
                'leaderboardLarge',
                'Leaderboard by Score'
              )}
            >
              <h3>Leaderboard by Score</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['leaderboard']}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'activeUsers', 'Active Unique Users')}
            >
              <h3>Active Unique Users</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['activeUsers']}
            </div>
          </div>
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'channelTraffic', 'Channel Traffic')}
            >
              <h3>Channel Traffic</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['channelTraffic']}
            </div>
          </div>
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'cardContributions', 'Card Contributions')}
            >
              <h3>Card Contributions</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['cardContributions']}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'cardCompletion', 'Card Completion Trend')}
            >
              <h3>Card Completion Trend</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['cardCompletion']}
            </div>
          </div>
          <div className="small-4 columns">
            <div
              className="graph-card"
              onClick={this.handleObjectClick.bind(this, 'cardsLiked', 'Cards Liked')}
            >
              <h3>Cards Liked</h3>
              <h5 className="date">
                {this.state.startDate.toLocaleDateString()} to{' '}
                {this.state.endDate.toLocaleDateString()}
              </h5>
              {this.cardRender['cardsLiked']}
            </div>
          </div>
          <div className="small-4 columns" />
        </div>
      </section>
    );
  }
}

ChannelAnalyticsContainer.propTypes = {
  params: PropTypes.any,
  slug: PropTypes.any,
  currentUser: PropTypes.object,
  isAdmin: PropTypes.any
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(ChannelAnalyticsContainer);
