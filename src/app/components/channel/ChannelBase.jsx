import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import uniqBy from 'lodash/uniqBy';

import { tr } from 'edc-web-sdk/helpers/translations';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import { getCollaborators } from 'edc-web-sdk/requests/channels';

import {
  toggleFollow,
  snackBarOpenClose,
  getSingleChannel,
  getTypeCards,
  fetchPinnedCards
} from '../../actions/channelsActionsV2';
import { followingChannelAction, getSpecificUserInfo } from '../../actions/currentUserActions';
import { getCarousels, getCarouselItems } from '../../actions/carouselsActions';
import * as upshotActions from '../../actions/upshotActions';

import ChannelDetails from './ChannelDetails';
import ChannelPinnedCardsCarousel from './ChannelPinnedCardsCarousel';
import CarouselsContainer from './CarouselsContainer';
import Card from '../cards/Card';
import Spinner from '../common/spinner';
import CardViewToggle from '../common/CardViewToggle';
import { cardFields } from '../../constants/cardFields';
import updatePageLastVisit from '../../utils/updatePageLastVisit';

class ChannelBase extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      editable: false,
      isCurate: false,
      carouselEditable: false,
      isLoading: true,
      limit: 12,
      offset: 0,
      clickFollowStatus: null,
      emptyStateImageLoaded: false,
      viewRegime:
        props.team.OrgConfig.cardView && props.team.OrgConfig.cardView['web/cardView/channel']
          ? props.team.OrgConfig.cardView['web/cardView/channel'].defaultValue
          : 'Tile',
      feedCardsStyle:
        window.ldclient.variation('feed-style-card-layout', false) &&
        window.ldclient.variation('channel-list-view', false),
      automatedPinnedCard: props.team.config['enable_automated_pinned_cards'] || false,
      showJourney: window.ldclient.variation('journey', false),
      openChannel: window.ldclient.variation('open-channel-enabled', false),
      customItems: [],
      isCardV3: window.ldclient.variation('card-v3', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };

    this.styles = {
      pinCards: {
        marginTop: '1rem'
      },
      middlePosition: {
        padding: '13px',
        maxWidth: '65.625rem',
        margin: 'auto'
      },
      anchorTagScroll: {
        display: 'block',
        height: '100px',
        marginTop: '-100px',
        visibility: 'hidden'
      },
      listViewContainer: {
        display: 'block',
        marginTop: '2rem',
        padding: '0 0.75rem'
      }
    };
    if (this.state.showNew) {
      updatePageLastVisit('channelLastVisit');
    }
  }

  componentWillUnmount() {
    this.state.showNew &&
      localStorage.setItem('channelLastVisit', Math.round(new Date().getTime() / 1000));
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);

    this.props
      .dispatch(getSingleChannel(this.props.routeParams.slug))
      .then(data => {
        if (
          Object.keys(this.props.channels).length === 0 &&
          data &&
          data.message === 'Unauthorized'
        ) {
          this.props.dispatch(
            snackBarOpenClose('You are not authorized to access this channel.', 5000)
          );
          this.props.dispatch(push('/me/learners-dashboard'));
        }
        if (this.state.channel) {
          recordVisit(this.state.channel.id, 'Channel');
          getCollaborators(this.state.channel.id, { search_term: this.props.currentUser.handle })
            .then(collaboratorUsers => {
              this.setState({ isCollaborator: !!collaboratorUsers.length });
            })
            .catch(err => {
              console.error(`Error in ChannelBase.getCollaborators.func: ${err}`);
            });
          this.props.dispatch(
            fetchPinnedCards(
              this.state.channel.id,
              'Channel',
              this.state.multilingualFiltering,
              localStorage.getItem('channelLastVisit'),
              cardFields
            )
          );
          let payload = { parent_id: this.state.channel.id, parent_type: 'Channel' };
          let customItems = this.state.customItems;
          this.props
            .dispatch(getCarousels({ structure: payload, only_enabled: true }))
            .then(dataParam => {
              if (dataParam) {
                dataParam.structures.forEach(structure => {
                  if (
                    this.state.channel.carousels.some(
                      item => item.visible && item.id && item.id === structure.id
                    )
                  ) {
                    this.props
                      .dispatch(getCarouselItems({ structure_id: structure.id }))
                      .then(structureData => {
                        customItems.push(
                          ...structureData.structuredItems
                            .filter(item => item.entity)
                            .map(item => item.entity)
                        );
                      })
                      .catch(err => {
                        console.error(`Error in ChannelBase.getCarouselItems.func: ${err}`);
                      });
                  }
                });
                this.setState({ customItems });
              }
            })
            .catch(err => {
              console.error(`Error in ChannelBase.getCarousels.func: ${err}`);
            });
          !this.state.feedCardsStyle || this.state.viewRegime === 'Tile'
            ? this.getTileViewCards()
            : this.getListCards();
        }
      })
      .catch(err => {
        console.error(`Error in ChannelBase.componentDidMount.getSingleChannel.func: ${err}`);
      });
    this.extractChannel(this.props.channels, this.props.routeParams.slug);
    this.checkIfEditable();
    this.checkIfCurate();
  }

  getTileViewCards = () => {
    this.props.dispatch(
      getTypeCards(
        this.state.channel.id,
        ['video_stream'],
        this.state.limit,
        0,
        false,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
    );
    this.props.dispatch(
      getTypeCards(
        this.state.channel.id,
        ['pack'],
        this.state.limit,
        0,
        false,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
    );
    this.props.dispatch(
      getTypeCards(
        this.state.channel.id,
        ['media', 'poll'],
        this.state.limit,
        0,
        false,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
    );
    this.props.dispatch(
      getTypeCards(
        this.state.channel.id,
        ['course'],
        this.state.limit,
        0,
        false,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
    );
    this.props.dispatch(
      getTypeCards(
        this.state.channel.id,
        ['journey'],
        this.state.limit,
        0,
        false,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
    );
    this.setState({ isLoading: false });
  };

  getListCards = () => {
    this.setState({ isLoading: true }, () => {
      let cardTypes = [];
      let displayingCarouselTypes = this.state.channel.carousels
        .filter(item => item.visible)
        .map(item => item.default_label);
      displayingCarouselTypes.forEach(item => {
        switch (item) {
          case 'Streams':
            cardTypes.push('video_stream');
            break;
          case 'Pathways':
            cardTypes.push('pack');
            break;
          case 'SmartCards':
            cardTypes.push('media', 'poll');
            break;
          case 'Journeys':
            cardTypes.push('journey');
            break;
          //added this case to support carousels mistakenly named with Russian letter 'С'
          case 'Сourses':
          case 'Courses':
            cardTypes.push('course');
            break;
          default:
            break;
        }
      });
      this.props
        .dispatch(
          getTypeCards(
            this.state.channel.id,
            cardTypes,
            this.state.limit,
            this.state.offset,
            true,
            this.state.multilingualFiltering,
            localStorage.getItem('channelLastVisit')
          )
        )
        .then(() => {
          let offset = this.state.offset + this.state.limit;
          this.setState({ isLoading: false, offset });
        })
        .catch(err => {
          console.error(`Error in ChannelBase.getTypeCards.func: ${err}`);
        });
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.routeParams.slug !== this.props.routeParams.slug) {
      this.props
        .dispatch(getSingleChannel(nextProps.routeParams.slug))
        .then(() => {
          this.props.dispatch(
            fetchPinnedCards(
              this.state.channel.id,
              'Channel',
              this.state.multilingualFiltering,
              localStorage.getItem('channelLastVisit')
            )
          );
          this.state.viewRegime === 'Tile' ? this.getTileViewCards() : this.getListCards();
        })
        .catch(err => {
          console.error(`Error in ChannelBase.getSingleChannel.func: ${err}`);
        });
    }
    this.extractChannel(nextProps.channels, nextProps.routeParams.slug);
    this.checkIfEditable();
    this.checkIfCurate();
  }

  checkIfEditable = () => {
    let channel = this.state.channel;
    let isEditable =
      channel &&
      ((channel.creator &&
        channel.isOpen &&
        (channel.creator.id == this.props.currentUser.id || channel.isFollowing)) ||
        (channel.curators && channel.curators.some(item => item.id == this.props.currentUser.id)));
    this.setState({
      editable: isEditable,
      carouselEditable: isEditable
    });
  };

  checkIfCurate = () => {
    let channel = this.state.channel;
    let isOpen = channel && (typeof channel.isOpen == 'boolean' && channel.isOpen);
    if (
      channel &&
      channel.curateOnly &&
      channel.isFollowing &&
      (this.state.openChannel ? isOpen : false)
    ) {
      this.setState({ isCurate: true });
    } else if (channel && channel.curateOnly && channel.curators && channel.curators.length) {
      for (let i = 0; i < channel.curators.length; i++) {
        if (channel.curators[i].id == this.props.currentUser.id) {
          this.setState({ isCurate: true });
        }
      }
    }
  };

  extractChannel = (channels, slug) => {
    let channel;
    Object.keys(channels).forEach(channelId => {
      if (channels[channelId] != undefined) {
        if (channels[channelId].slug === slug || channels[channelId].id == slug) {
          channel = channels[channelId];
        }
      }
    });
    this.setState({ channel });
  };

  followClickHandler = followStatus => {
    this.props
      .dispatch(toggleFollow(this.state.channel.id, !followStatus))
      .then(() => {
        if (this.state.upshotEnabled) {
          upshotActions.sendCustomEvent(
            window.UPSHOTEVENT[!this.state.isFollowing ? 'FOLLOW' : 'UNFOLLOW'],
            {
              eventAction: !this.state.following ? 'Follow' : 'Unfollow',
              event: 'Channel'
            }
          );
        }
        this.props.dispatch(followingChannelAction(this.state.channel, followStatus));
      })
      .catch(error => {
        this.props.dispatch(snackBarOpenClose(error.message, 1000));
        this.channelFollowStatus();
      });
  };

  onImageLoad = e => {
    this.state.emptyStateImageLoaded = true;
  };

  changeViewMode = viewRegime => {
    if (viewRegime !== this.state.viewRegime) {
      this.setState({ viewRegime, isLoading: true }, () => {
        viewRegime === 'Tile' ? this.getTileViewCards() : this.getListCards();
      });
    }
  };

  channelFollowStatus = () => {
    this.setState({ clickFollowStatus: false });
  };

  removeCardFromList = id => {
    let channel = this.state.channel;
    channel.all = channel.all.filter(card => {
      return card.id !== id;
    });
    this.setState({ channel });
  };

  render() {
    let count =
      (this.state.channel &&
        this.state.channel.pinnedCardsIds &&
        this.state.channel.pinnedCardsIds.length) ||
      0;
    let isAllow =
      this.state.channel &&
      (this.state.channel.allowFollow ||
        this.state.channel.isCurate ||
        this.state.isCollaborator ||
        this.state.channel.isOwner ||
        this.props.currentUser.isAdmin);
    return (
      <div className="channel-base">
        <div>
          <ChannelDetails
            channel={this.state.channel}
            followClickHandler={this.followClickHandler}
            editable={this.state.editable}
            isCurate={this.state.isCurate}
            isFollowPending={this.state.clickFollowStatus}
          />
        </div>
        {isAllow && this.state.editable && count <= 0 && (
          <div>
            <div className="row" style={{ position: 'relative' }}>
              <div className="small-12 columns">
                <div className="emptyStateWrapper">
                  <div
                    style={{
                      background: `${this.state.emptyStateImageLoaded} ? '#fff' : 'transparent`,
                      borderRadius: '2px'
                    }}
                  >
                    {this.state.emptyStateImageLoaded && (
                      <p className="emptyStateImageText">
                        {tr('Select a card to "Pin to Top" on edit mode!')}
                      </p>
                    )}
                    <img
                      className="emptyStateImage"
                      src="/i/images/empty_state_pinned_card.png"
                      onLoad={this.onImageLoad.bind(this)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {isAllow && this.state.feedCardsStyle && (
          <div className="row card-view-channel">
            <CardViewToggle
              viewRegime={this.state.viewRegime}
              changeViewMode={this.changeViewMode}
            />
          </div>
        )}
        {isAllow && count > 0 && (
          <div style={this.styles.pinCards}>
            <ChannelPinnedCardsCarousel
              items={this.state.channel.pinnedCards}
              channel={this.state.channel}
              editable={this.state.editable && !this.state.automatedPinnedCard}
            />
          </div>
        )}
        {isAllow && (
          <div>
            {!this.state.feedCardsStyle || this.state.viewRegime === 'Tile' ? (
              <div>
                {this.state.channel && (
                  <CarouselsContainer
                    channel={this.state.channel}
                    carouselEditable={this.state.carouselEditable}
                    automatedPinnedCard={this.state.automatedPinnedCard}
                  />
                )}
              </div>
            ) : (
              <div
                style={this.styles.listViewContainer}
                className={`row ${this.state.isCardV3 ? 'vertical-spacing-medium' : ''}`}
              >
                {this.state.channel &&
                  uniqBy(this.state.channel.all.concat(this.state.customItems), 'id').map(
                    (card, index) => {
                      return (
                        <Card
                          card={card}
                          index={index + 1}
                          fullView={true}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          author={card.author}
                          user={this.props.currentUser}
                          isMarkAsCompleteDisabled={true}
                          withoutCardModal={true}
                          key={index}
                          type="List"
                          removeCardFromList={this.removeCardFromList}
                        />
                      );
                    }
                  )}
                {!this.state.isLoading && (
                  <div className="text-center">
                    {this.state.channel.all &&
                      this.state.channel.all.length === this.state.offset && (
                        <SecondaryButton
                          label={tr('View More')}
                          className="view-more"
                          onTouchTap={this.getListCards}
                        />
                      )}
                  </div>
                )}
              </div>
            )}
            {this.state.isLoading && (
              <div className="text-center">
                <Spinner />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

ChannelBase.propTypes = {
  routeParams: PropTypes.object,
  carousel: PropTypes.object,
  channels: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

export default connect(state => ({
  channels: state.channelsV2.toJS(),
  team: state.team.toJS(),
  currentUser: state.currentUser.toJS()
}))(ChannelBase);
