import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Checkbox from 'material-ui/Checkbox';
import CheckOn from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';

export default class AggregationBox extends Component {
  state = {
    selectedOption: []
  };

  styles = {
    checkboxOuter: {
      width: '1rem',
      height: '1rem',
      marginRight: '0.3125rem'
    },
    checkboxOuterLabel: {
      fontSize: '0.75rem',
      padding: '0.375rem 0',
      lineHeight: '1.25rem'
    },
    uncheckedIcon: {
      width: '1rem',
      height: '1rem'
    },
    uncheckedIconInner: {
      width: '0.8125rem',
      height: '0.8125rem'
    }
  };

  checkBoxColor = '#6f708b';

  componentWillReceiveProps(nextProps) {
    if (nextProps.removedAggrigationOptn) {
      this.removeCheckStatus(nextProps.removedAggrigationOptn);
    }
  }

  removeCheckStatus = removedAggrigationOptn => {
    this.setState(() => ({
      selectedOption: this.state.selectedOption.filter(
        option => option !== removedAggrigationOptn.id
      )
    }));
  };

  handleClick = (e, isChecked) => {
    e.persist();
    const selectedOption = this.state.selectedOption;
    if (isChecked) {
      // checking valid integer string
      if (e.target.value.match(/^-{0,1}\d+$/)) {
        selectedOption.push(+e.target.value);
      } else {
        selectedOption.push(e.target.value);
      }
    } else {
      let index;
      if (e.target.value.match(/^-{0,1}\d+$/)) {
        index = selectedOption.indexOf(+e.target.value);
      } else {
        index = selectedOption.indexOf(e.target.value);
      }
      selectedOption.splice(index, 1);
    }
    this.setState(
      () => ({
        selectedOption
      }),
      () => {
        this.props.aggregationHandler(selectedOption, this.props.filterType);
      }
    );
  };

  render() {
    const { contentArray, aggsLable, showViewMore } = this.props;
    return (
      <div className="filter-block">
        <div className="filter-title">{aggsLable}</div>
        <div className="filter-content">
          {contentArray.map(content => (
            <div className="outer-checkbox" key={content.id}>
              <Checkbox
                label={`${tr(`${content.display_name || content.displayName}`)} ${
                  content.count ? `(${content.count})` : ''
                }`}
                checkedIcon={<CheckOn color={this.checkBoxColor} />}
                uncheckedIcon={
                  <CheckOff style={this.styles.uncheckedIcon} color={this.checkBoxColor} />
                }
                iconStyle={this.styles.checkboxOuter}
                labelStyle={this.styles.checkboxOuterLabel}
                onCheck={this.handleClick}
                value={content.id}
                checked={this.state.selectedOption.indexOf(content.id) !== -1}
              />
            </div>
          ))}
        </div>
        {this.props.showViewMore && (
          <div className="view-more-container" onClick={this.props.showMoreHandler}>
            <small>{tr('View All')}</small>
          </div>
        )}
      </div>
    );
  }
}

AggregationBox.propTypes = {
  contentArray: PropTypes.array,
  aggregationHandler: PropTypes.func,
  aggsLable: PropTypes.string,
  showViewMore: PropTypes.bool,
  showMoreHandler: PropTypes.func,
  filterType: PropTypes.string,
  removedAggrigationOptn: PropTypes.object
};
