import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';

import { tr } from 'edc-web-sdk/helpers/translations';
import { getChannleCarouselData } from 'edc-web-sdk/requests/channels.v2';
import { getCarouselItems } from 'edc-web-sdk/requests/carousels';
import Carousel from '../../common/Carousel';
import Card from './../../cards/Card';
import { openChannelCarouselCardsModal } from '../../../actions/modalActions';

export class ChannelCarousel extends Component {
  constatnts = {
    pencilColor: '#6f708b'
  };

  state = {
    carouselCards: [],
    totalCardCount: null
  };

  styles = {
    iconButtonStyle: {
      height: '1.562rem',
      width: '1.562rem',
      padding: 0,
      display: 'inline-block',
      verticalAlign: 'middle',
      marginLeft: '0.312rem',
      paddingTop: '0.125rem'
    },
    iconStyle: {
      width: '1.375rem',
      height: '1.375rem'
    }
  };

  componentDidMount() {
    if (this.props.data.custom_carousel) {
      this.fetchCustomCarouselData();
    } else {
      this.fectCarouselData();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.cardSortBy !== this.props.cardSortBy ||
      nextProps.cardOrderIn !== this.props.cardOrderIn ||
      nextProps.cardFilterByContent !== this.props.cardFilterByContent ||
      nextProps.cardFilterByDate !== this.props.cardFilterByDate ||
      nextProps.showAllFilterAndSortCards !== this.props.showAllFilterAndSortCards
    ) {
      this.setState(
        () => ({ loading: true }),
        () => {
          this.searchQuery.offset = 0;
          this.fectCarouselData();
          if (nextProps.showAllFilterAndSortCards && this.props.data.custom_carousel) {
            this.setState(() => ({ carouselCards: [] }));
          } else if (!nextProps.showAllFilterAndSortCards && this.props.data.custom_carousel) {
            this.fetchCustomCarouselData();
          }
        }
      );
    }
    if (
      nextProps.removedCardInfo &&
      nextProps.removedCardInfo.carouselTitle === this.props.data.default_label
    ) {
      this.upateCarouselCards(nextProps.removedCardInfo.cardId);
    }
    if (nextProps.deletedCardId && nextProps.deletedCardId !== this.props.deletedCardId) {
      if (this.state.carouselCards.findIndex(card => card.id === nextProps.deletedCardId) >= 0) {
        this.upateCarouselCards(nextProps.deletedCardId);
      }
    }
  }

  searchQuery = {
    limit: 10,
    offset: 0
  };

  carouselSettings = [
    { breakpoint: 2900, slidesNumber: 6, slidesToScroll: 3 },
    { breakpoint: 1920, slidesNumber: 6, slidesToScroll: 3 },
    { breakpoint: 1919, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1804, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1760, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1670, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1600, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1580, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1488, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1350, slidesNumber: 4, slidesToScroll: 3 },
    { breakpoint: 1280, slidesNumber: 4, slidesToScroll: 2 },
    { breakpoint: 1250, slidesNumber: 4, slidesToScroll: 2 }
  ];

  upateCarouselCards = cardId => {
    this.setState(() => ({
      carouselCards: this.state.carouselCards.filter(card => card.id !== cardId),
      totalCardCount: this.state.totalCardCount - 1
    }));
  };

  fectCarouselData = () => {
    const { data, channelId } = this.props;
    let cardTypes = [];
    let cardSubTypes = [];
    const smartCardSubTypes = ['link', 'audio', 'file', 'text', 'video', 'poll'];
    let getCards = false;
    switch (data.default_label) {
      case 'SmartCards':
        cardTypes = this.props.showAllFilterAndSortCards
          ? this.props.cardFilterByContent === 'poll'
            ? ['poll']
            : ['media']
          : ['media', 'poll'];
        if (this.props.cardFilterByContent && this.props.cardFilterByContent !== 'poll') {
          cardSubTypes.push(this.props.cardFilterByContent);
        }
        getCards =
          (this.props.data.default_label === 'SmartCards' &&
            smartCardSubTypes.indexOf(this.props.cardFilterByContent) !== -1 &&
            this.props.showAllFilterAndSortCards) ||
          !this.props.showAllFilterAndSortCards ||
          (!!this.props.cardFilterByDate.from_date.length && this.props.cardFilterByContent === '');
        break;
      case 'Pathways':
        cardTypes = ['pack'];
        getCards =
          (this.props.data.default_label === 'Pathways' &&
            this.props.cardFilterByContent === 'pack' &&
            this.props.showAllFilterAndSortCards) ||
          !this.props.showAllFilterAndSortCards ||
          (!!this.props.cardFilterByDate.from_date.length && this.props.cardFilterByContent === '');
        break;
      case 'Journeys':
        cardTypes = ['journey'];
        getCards =
          (this.props.data.default_label === 'Journeys' &&
            this.props.cardFilterByContent === 'journey' &&
            this.props.showAllFilterAndSortCards) ||
          !this.props.showAllFilterAndSortCards ||
          (!!this.props.cardFilterByDate.from_date.length && this.props.cardFilterByContent === '');
        break;
      case 'Streams':
        cardTypes = ['video_stream'];
        getCards =
          (this.props.data.default_label === 'Streams' &&
            this.props.cardFilterByContent === 'video_stream' &&
            this.props.showAllFilterAndSortCards) ||
          !this.props.showAllFilterAndSortCards ||
          (!!this.props.cardFilterByDate.from_date.length && this.props.cardFilterByContent === '');
        break;
      case 'Courses':
        cardTypes = ['course'];
        getCards =
          (this.props.data.default_label === 'Courses' &&
            this.props.cardFilterByContent === 'course' &&
            this.props.showAllFilterAndSortCards) ||
          !this.props.showAllFilterAndSortCards ||
          (!!this.props.cardFilterByDate.from_date.length && this.props.cardFilterByContent === '');
        break;
      default:
        break;
    }
    if (getCards) {
      const payload = {
        'card_subtype[]': cardSubTypes,
        'card_type[]': cardTypes,
        sort: this.props.cardSortBy,
        order: this.props.cardOrderIn,
        from_date: this.props.cardFilterByDate.from_date,
        to_date: this.props.cardFilterByDate.to_date,
        ...this.searchQuery
      };
      getChannleCarouselData(payload, channelId)
        .then(response => {
          this.setState(() => ({ carouselCards: response.cards, totalCardCount: response.total }));
        })
        .catch(error => {
          console.error('Error in ChannelCarousel.getChannleCarouselData', error);
        });
    } else {
      this.setState(() => ({ carouselCards: [] }));
    }
  };

  fetchCustomCarouselData = () => {
    getCarouselItems(this.props.data.id)
      .then(response => {
        if (response.structuredItems) {
          const carouselCards = response.structuredItems.map(card => card.entity);
          this.setState(() => ({
            carouselCards: carouselCards,
            totalCardCount: carouselCards.length
          }));
        }
      })
      .catch(error => {
        console.error('Error in ChannelCarousel.fetchCustomCarouselData', error);
      });
  };

  editCarouselCards = () => {
    const payload = {
      carouselCards: this.state.carouselCards,
      carouselData: this.props.data,
      totalCarouselCardsCount: this.state.totalCardCount,
      showChannelCardConfig: true
    };
    this.props.dispatch(openChannelCarouselCardsModal(payload));
  };

  showCarouselCards = () => {
    const payload = {
      carouselCards: this.state.carouselCards,
      carouselData: this.props.data,
      totalCarouselCardsCount: this.state.totalCardCount,
      showChannelCardConfig: false
    };
    this.props.dispatch(openChannelCarouselCardsModal(payload));
  };

  render() {
    const { carouselCards, totalCardCount } = this.state;

    return (
      <div>
        {!!carouselCards.length && (
          <div className="channel-carousel-container">
            <div className="carousel-header-conatiner">
              <div className="label-icon-container">
                <p className="carousel-label card-v3-padding-fix">
                  {this.props.data.default_label} ({totalCardCount})
                </p>
                {this.props.isEditable && (
                  <IconButton
                    aria-label="edit"
                    onTouchTap={this.editCarouselCards}
                    style={this.styles.iconButtonStyle}
                    iconStyle={this.styles.iconStyle}
                  >
                    <EditIcon color={this.constatnts.pencilColor} />
                  </IconButton>
                )}
              </div>
              {totalCardCount > this.searchQuery.limit && (
                <div className="view-all-container" onClick={this.showCarouselCards}>
                  <p className="view-all-text">{tr('View All')}</p>
                </div>
              )}
            </div>
            <Carousel slidesToShow={3} isCardV3={true} carouselSettings={this.carouselSettings}>
              {carouselCards.map(card => (
                <div key={card.id}>
                  <Card
                    author={card.author && card.author}
                    card={card}
                    dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                    startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                    tooltipPosition="top-center"
                    moreCards={false}
                    withoutCardModal={true}
                    type={'Tile'}
                    removeCardFromCarousel={this.removeCardFromCarousel}
                    channel={this.props.channel}
                  />
                </div>
              ))}
            </Carousel>
          </div>
        )}
      </div>
    );
  }
}

ChannelCarousel.propTypes = {
  props: PropTypes.object,
  data: PropTypes.object,
  channelId: PropTypes.number,
  cardSortBy: PropTypes.string,
  cardOrderIn: PropTypes.string,
  cardFilterByDate: PropTypes.object,
  cardFilterByContent: PropTypes.string,
  showAllFilterAndSortCards: PropTypes.bool,
  isEditable: PropTypes.bool,
  removedCardInfo: PropTypes.object,
  deletedCardId: PropTypes.string,
  channel: PropTypes.object
};

const mapStateToProps = state => ({
  removedCardInfo: state.channelReducerV3.get('removedCardInfo'),
  deletedCardId: state.channelReducerV3.get('deletedCardId'),
  channel: state.channelReducerV3.get('channel')
});

export default connect(mapStateToProps)(ChannelCarousel);
