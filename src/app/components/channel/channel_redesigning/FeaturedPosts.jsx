import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import IconButton from 'material-ui/IconButton';
import NextIcon from 'material-ui/svg-icons/image/navigate-next';
import PrevIcon from 'material-ui/svg-icons/image/navigate-before';

import FeaturedCard from './FeaturedCard';
import { tr } from 'edc-web-sdk/helpers/translations';
import { fetchPinnedCards } from 'edc-web-sdk/requests/channels.v2';
import { saveFeaturedCards } from '../../../actions/channelsActionsV3';

class FeaturedPosts extends Component {
  state = {
    allCards: []
  };

  componentDidMount() {
    this.fetchPinnedCards();
  }

  fetchPinnedCards = () => {
    fetchPinnedCards(this.props.channelId, 'Channel', true)
      .then(response => {
        if (response) {
          this.props.dispatch(saveFeaturedCards(response));
        }
      })
      .catch(error => {
        console.error('error in FeaturedPosts.fetchPinnedCards', error);
      });
  };

  render() {
    const allCards = this.props.featuredCards;
    let settings = {
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      arrows: allCards ? allCards.length >= 1 : 0,
      slidesToShow: 1,
      autoplaySpeed: false,
      responsive: [],
      infinite: false,
      slidesToScroll: 1,
      afterChange: this.reloadVideos
    };

    return (
      <div className="featured-carousel">
        {allCards && !!allCards.length && <p className="carousel-label">{tr('Featured Posts')}</p>}
        {allCards && !allCards.length && <div className="filter-sort-position-fix" />}
        <Slider {...settings}>
          {allCards &&
            allCards.map((obj, index) => (
              <div key={index}>
                <FeaturedCard data={obj} />
              </div>
            ))}
        </Slider>
      </div>
    );
  }
}

FeaturedCard.propTypes = {
  channelId: PropTypes.string,
  onClick: PropTypes.func
};

/*eslint react/prop-types: "off"*/
let NextArrow = props => (
  <div className="slick-next">
    <IconButton
      aria-label="next"
      className="nextArrow"
      onTouchTap={props.onClick}
      disabled={props.className.indexOf('disabled') !== -1}
    >
      <NextIcon />
    </IconButton>
  </div>
);
let PrevArrow = props => (
  <div className="slick-prev">
    <IconButton
      aria-label="previous"
      className="prevArrow"
      onTouchTap={props.onClick}
      disabled={props.className.indexOf('disabled') !== -1}
    >
      <PrevIcon />
    </IconButton>
  </div>
);

const mapStateToProps = state => ({
  featuredCards: state.channelReducerV3.get('featuredCards')
});

export default connect(mapStateToProps)(FeaturedPosts);
