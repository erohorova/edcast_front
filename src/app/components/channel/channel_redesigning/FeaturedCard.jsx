import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Loadable from 'react-loadable';
import uniqBy from 'lodash/uniqBy';

import FeaturedCardFooter from './FeaturedCardFooter';
import { tr } from 'edc-web-sdk/helpers/translations';
import convertRichText from '../../../utils/convertRichText';
import addSecurity from '../../../utils/filestackSecurity';
import { getComments } from 'edc-web-sdk/requests/cards';
import Poll from '../../feed/Poll';
import linkPrefix from '../../../utils/linkPrefix';
import getCardParams from '../../../utils/getCardParams';
import IconButton from 'material-ui/IconButton/IconButton';
import Downloadv2 from 'edc-web-sdk/components/icons/Downloadv2';

const RichTextReadOnly = Loadable({
  loader: () => import('../../common/RichTextReadOnly'),
  loading: () => null
});

const FileCard = Loadable({
  loader: () => import('../../cards/v3/FileCard'),
  loading: () => null
});

const AutherInfo = props => {
  const { author } = props;
  return (
    <a href={`/${author.handle || author.slug}`}>
      <div className="author-container">
        <div className="avatar">
          <img src={author.avatarimages.small} />
        </div>
        <div className="fullname">
          <p>{author.fullName}</p>
        </div>
      </div>
    </a>
  );
};

class FeaturedCard extends Component {
  state = {
    selected: null,
    commentedUser: [],
    defaulCardImageShadowColor:
      (this.props.team && this.props.team.get('config').card_image_shadow_color) || '#454560'
  };

  styles = {
    radioIcon: {
      width: '1rem',
      height: '1rem',
      borderColor: '#979797',
      color: '#00a1e1',
      marginRight: '0.5625rem'
    },
    radioLabel: {
      fontSize: '1.125rem',
      fontFamily: 'Open Sans, sans-serif',
      textAlign: 'left',
      color: '#6f708b',
      lineHeight: 1.47,
      width: '100%',
      overflow: 'hidden',
      textTransform: 'capitalize',
      top: '-0.1875rem'
    },
    backgroundGradient: {
      backgroundImage: `linear-gradient(to bottom, rgba(123, 123, 153, 0.05), 65%, ${
        this.state.defaulCardImageShadowColor
      })`
    },
    btnIcons: {
      verticalAlign: 'middle',
      width: '1rem',
      height: '0.875rem',
      padding: 0
    },
    tooltipStyles: {
      left: '-50%'
    },
    downloadButton: {
      width: '100%',
      height: '100%',
      padding: 0,
      background: '#454560'
    }
  };
  componentDidMount() {
    this.fetchCardComments();
  }

  fetchCardComments = () => {
    getComments(this.props.data.id, this.props.data.commentsCount)
      .then(response => {
        if (!!response.length) {
          this.setState(() => ({ commentedUser: uniqBy(response, 'user.id').splice(0, 3) }));
        }
      })
      .catch(error => {
        console.error('error in FeaturedCard.fetchCardComments', error);
      });
  };

  redirectClickHandler = () => {
    this.props.dispatch(push(`${linkPrefix(this.props.data.cardType)}/${this.props.data.slug}`));
  };

  downloadBlock = file => {
    return (
      <span className="roll">
        <a onClick={() => this.openUrl(file.url)} download={file.handle} target="_blank">
          <IconButton
            style={this.styles.downloadButton}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={tr('Download')}
            aria-label={tr('Download')}
            iconStyle={this.styles.btnIcons}
          >
            <Downloadv2 />
          </IconButton>
        </a>
      </span>
    );
  };

  render() {
    let expireAfter = window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let bgImage;
    const fileStackImageCards =
      this.props.data.cardType === 'pack' ||
      this.props.data.cardType === 'journey' ||
      (this.props.data.cardType === 'media' && this.props.data.cardSubtype === 'image');
    if (fileStackImageCards) {
      bgImage = addSecurity(
        this.props.data.filestack[0].url,
        expireAfter,
        this.props.currentUser.get('id')
      );
    }
    let params = getCardParams(this.props.data, this.props, 'card') || {};
    return (
      <div className="featured-card-layout card-v3-padding-fix" key={this.props.key}>
        {((this.props.data.cardType === 'media' && this.props.data.cardSubtype === 'link') ||
          (this.props.data.cardType === 'course' && this.props.data.cardSubtype === 'link')) && (
          <div
            className="card-bg-image"
            style={{ backgroundImage: `url(${this.props.data.resource.imageUrl})` }}
          >
            <div className="bg-gradient" style={this.styles.backgroundGradient}>
              <div className="card-padding">
                <div className="card-content" onClick={this.redirectClickHandler}>
                  <div className="decription">
                    <p>{this.props.data.resource.description}</p>
                  </div>
                </div>
                <div className="footer-conatiner">
                  <FeaturedCardFooter
                    card={this.props.data}
                    commentedUser={this.state.commentedUser}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
        {this.props.data.cardType === 'media' && this.props.data.cardSubtype === 'text' && (
          <div className="card-padding bg-white">
            <AutherInfo author={this.props.data.author} />
            <div className="text-card-content content-height" onClick={this.redirectClickHandler}>
              <RichTextReadOnly text={convertRichText(this.props.data.message)} />
            </div>
            <div className="footer-conatiner">
              <FeaturedCardFooter card={this.props.data} commentedUser={this.state.commentedUser} />
            </div>
          </div>
        )}
        {this.props.data.cardType === 'poll' && (
          <div className="card-padding bg-white">
            <AutherInfo author={this.props.data.author} />
            <div className="poll-card-content content-height" onClick={this.redirectClickHandler}>
              <p className="qstn">{this.props.data.message}</p>
              <Poll card={this.props.data} isCardV3={true} />
            </div>
            <div className="footer-conatiner">
              <FeaturedCardFooter card={this.props.data} commentedUser={this.state.commentedUser} />
            </div>
          </div>
        )}
        {this.props.data.cardType === 'media' && this.props.data.cardSubtype === 'video' && (
          <div
            className="card-bg-image"
            style={{ backgroundImage: `url(${this.props.data.resource.imageUrl})` }}
          >
            <div className="bg-gradient" style={this.styles.backgroundGradient}>
              <div className="card-padding">
                <div className="card-content" onClick={this.redirectClickHandler}>
                  <div className="decription">
                    <p>{this.props.data.resource.description}</p>
                  </div>
                </div>
                <div className="footer-conatiner">
                  <FeaturedCardFooter
                    card={this.props.data}
                    commentedUser={this.state.commentedUser}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
        {fileStackImageCards && (
          <div className="card-bg-image" style={{ backgroundImage: `url(${bgImage})` }}>
            <div className="bg-gradient" style={this.styles.backgroundGradient}>
              <div className="card-padding">
                <div className="pathway-author-container">
                  <AutherInfo author={this.props.data.author} />
                </div>
                <div className="card-content" onClick={this.redirectClickHandler}>
                  <div className="decription">
                    <p>
                      {this.props.data.message.length > 300
                        ? `${this.props.data.message.slice(0, 297)}...`
                        : this.props.data.message}
                    </p>
                  </div>
                </div>
                <div className="footer-conatiner">
                  <FeaturedCardFooter
                    card={this.props.data}
                    commentedUser={this.state.commentedUser}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
        {this.props.data.cardType === 'media' &&
          (this.props.data.cardSubtype === 'file' || this.props.data.cardSubtype === 'audio') && (
            <div className="card-padding bg-white">
              <AutherInfo author={this.props.data.author} />
              <div
                className="text-card-content content-height file-card-content"
                onClick={this.redirectClickHandler}
              >
                <p className="file-title">{this.props.data.message}</p>
                <FileCard
                  height="368px"
                  card={this.props.data}
                  currentUser={this.props.currentUser}
                  params={params}
                  downloadBlock={this.downloadBlock}
                  currentUser={this.props.currentUser.toJS()}
                />
              </div>
              <div className="footer-conatiner">
                <FeaturedCardFooter
                  card={this.props.data}
                  commentedUser={this.state.commentedUser}
                />
              </div>
            </div>
          )}
      </div>
    );
  }
}

FeaturedCard.propTypes = {
  data: PropTypes.object,
  key: PropTypes.any,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

AutherInfo.propTypes = {
  author: PropTypes.object
};

const mapStateToProps = state => ({
  team: state.team,
  currentUser: state.currentUser
});

export default connect(mapStateToProps)(FeaturedCard);
