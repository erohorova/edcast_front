import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getHtmlWidget } from 'edc-web-sdk/requests/htmlWidget';

export class HtmlWidget extends Component {
  
  state = {
    widget: null
  }

  componentDidMount() {
    this.fetchHtmlWidget()
  }

  fetchHtmlWidget = () => {
    const payload = {
      widget: {
        parent_id: this.props.channel.id,
        parent_type: 'Channel',
        context: 'channel',
        enabled: true
      }
    }
    getHtmlWidget(payload)
      .then((response) => {
        if (response && !!response.widgets.length) {
          this.setState(() => ({
            widget: response.widgets[0]
          }))
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  }

  render() {
    return (
      <div>
        {this.state.widget && (
          <div className="html-widget-container">
            <div dangerouslySetInnerHTML={{ __html: this.state.widget.code }} />
          </div>
        )}
      </div>
    )
  }
}

HtmlWidget.propTypes = {
  channel: PropTypes.object
};

const mapStateToProps = (state) => ({
  
});

export default connect(mapStateToProps)(HtmlWidget)
