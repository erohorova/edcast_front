import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';
import Checkbox from 'material-ui/Checkbox';
import AggregationBox from './AggregationBox';
import Card from '../../cards/Card';
import Spinner from '../../common/spinner';
import { tr } from 'edc-web-sdk/helpers/translations';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import moment from 'moment';
import { searchCardsInChannel } from 'edc-web-sdk/requests/channels.v2';
import findIndex from 'lodash/findIndex';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import uniqWith from 'lodash/uniqWith';
import isEqual from 'lodash/isEqual';

const styles = {
  checkboxInner: {
    width: '0.8125rem',
    height: '0.8125rem',
    marginRight: '0.25rem'
  },
  checkboxInnerLabel: {
    fontSize: '0.687rem',
    fontWeight: '300',
    color: '#6f708b',
    lineHeight: '1'
  },
  checkboxOuter: {
    width: '1rem',
    height: '1rem',
    marginRight: '0.3125rem'
  },
  checkboxOuterLabel: {
    fontSize: '0.75rem',
    padding: '0.375rem 0',
    lineHeight: '1.25rem'
  },
  checkboxIconStyle: {
    width: '1rem',
    height: '1rem'
  },
  uncheckedIcon: {
    width: '1rem',
    height: '1rem'
  },
  uncheckedIconInner: {
    width: '0.8125rem',
    height: '0.8125rem'
  }
};
class ChannelSearch extends Component {
  state = {
    suggesstedCards: [],
    isCardV3: window.ldclient.variation('card-v3', false),
    sources: [],
    contentTypes: [],
    postedBy: [],
    smartCardTypes: [],
    aggsArray: [],
    postedByEntries: [],
    sourcesEntries: [],
    smartCardsCount: null,
    smartCardChecked: false,
    pending: true,
    queryFromAggs: false,
    selectedContentTypes: [],
    showAllPostedBy: false,
    showAllSources: false,
    isRenderSearchResult: true,
    searchQueryString: this.props.searchQueryString || '',
    channelId: this.props.channelId,
    pagginationQuery: false,
    searchAggChips: [],
    searchContentChips: [],
    sourceChips: [],
    postByChips: [],
    dateRangeChips: [],
    removedAggrigationOptn: null
  };

  searchQuery = {
    limit: 20,
    offset: 0,
    q: ''
  };

  currentYear = moment().year();
  startOfTheYear = `01/01/${this.currentYear}`;
  endOfTheYear = `12/31/${this.currentYear}`;
  dateFilter = [
    {
      id: 'last-thirty-days',
      type: 'date_range',
      displayName: 'Last 30 days',
      value: {
        from_date: moment()
          .subtract(30, 'days')
          .format('DD/MM/YYYY'),
        till_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'last-ninety-days',
      type: 'date_range',
      displayName: 'Last 90 days',
      value: {
        from_date: moment()
          .subtract(90, 'days')
          .format('DD/MM/YYYY'),
        till_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'current-year',
      type: 'date_range',
      displayName: this.currentYear,
      value: {
        from_date: moment(this.startOfTheYear).format('DD/MM/YYYY'),
        till_date: moment(this.endOfTheYear).format('DD/MM/YYYY')
      }
    },
    {
      id: 'last-year',
      type: 'date_range',
      displayName: moment()
        .subtract(1, 'year')
        .year(),
      value: {
        from_date: moment(this.startOfTheYear)
          .subtract(1, 'year')
          .format('DD/MM/YYYY'),
        till_date: moment(this.endOfTheYear)
          .subtract(1, 'year')
          .format('DD/MM/YYYY')
      }
    },
    {
      id: 'before-two-year',
      type: 'date_range',
      displayName: moment()
        .subtract(2, 'year')
        .year(),
      value: {
        from_date: moment(this.startOfTheYear)
          .subtract(2, 'year')
          .format('DD/MM/YYYY'),
        till_date: moment(this.endOfTheYear)
          .subtract(2, 'year')
          .format('DD/MM/YYYY')
      }
    }
  ];

  componentDidMount() {
    this.buildQuery(this.props.searchQueryString);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.searchQueryString !== this.props.searchQueryString) {
      this.buildQuery(nextProps.searchQueryString);
    }
  }

  handleScroll = throttle(
    () => {
      if (
        !this.state.pending &&
        (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight &&
          !this.isLastPage)
      ) {
        // currentCardCount card count getting from letest api call
        // pending check for network request
        // suggesstedCards length more than and equal to limit then allow to calll pagination
        const condition =
          this.state.currentCardCount === this.searchQuery.limit &&
          this.state.suggesstedCards.length >= this.searchQuery.limit;
        if (condition) {
          this.searchQuery.offset += this.searchQuery.limit;
          this.setState(
            () => ({
              pagginationQuery: true,
              queryFromAggs: true,
              isRenderSearchResult: true
            }),
            () => {
              this.buildQuery();
            }
          );
        }
      } else return null;
    },
    150,
    { leading: false }
  );

  // building query
  buildQuery = value => {
    const payload = {};
    if (value !== undefined) {
      this.searchQuery.q = value.trim();
      payload['content_type[]'] = [];
      // reset filter
      this.resetFilter();
    } else {
      if (this.state.aggsArray) {
        // this manipulation required for query
        payload['content_type[]'] = this.state.selectedContentTypes.map(selectedType => {
          const index = findIndex(this.state.aggsArray, content => {
            return content.id === selectedType;
          });
          const tempObj = {};
          tempObj.card_type = this.state.aggsArray[index].card_type;
          tempObj.card_subtype = this.state.aggsArray[index].card_subtype;
          return tempObj;
        });
      } else {
        payload['content_type[]'] = [];
      }
    }
    payload.offset = this.state.queryFromAggs ? this.searchQuery.offset : 0;
    payload['author_id[]'] = this.state.queryFromAggs ? this.state.selectedAuthorId : [];
    payload['source_display_name[]'] = this.state.queryFromAggs
      ? this.state.selectedSourceDisplayName
      : [];
    payload['date_range[]'] = this.state.queryFromAggs ? this.state.selectedDateRange : [];
    payload.limit = this.searchQuery.limit;
    payload.q = this.searchQuery.q;
    this.setState(() => ({ pending: true }), () => this.getChannelCardsBasedOnQuery(payload));
  };

  // get suggested card for search query
  getChannelCardsBasedOnQuery = payload => {
    searchCardsInChannel(this.state.channelId, payload)
      .then(res => {
        if (res.cards) {
          this.setState(() => ({
            filterCards: this.state.pagginationQuery
              ? [...this.state.filterCards, ...res.cards]
              : res.cards,
            pending: false,
            pagginationQuery: false,
            currentCardCount: res.cards.length,
            resultQuantity: res.total
          }));
          if (res.aggs) {
            if (!this.state.queryFromAggs && this.state.isRenderSearchResult) {
              if (!res.cards.length && !res.aggs.length) {
                this.resetFilter();
              } else {
                this.generateAggregations(res);
              }
            } else if (this.state.queryFromAggs && this.state.isRenderSearchResult) {
              this.setState(() => ({
                suggesstedCards: this.state.filterCards,
                isRenderSearchResult: false
              }));
            }
          }
        } else {
          this.setState(() => ({
            pending: false
          }));
        }
      })
      .catch(err =>
        console.error('An error had occurred while running searchCardsInChannel function \n', err)
      );
  };

  resetFilter = fromClearAll => {
    this.setState(
      () => ({
        selectedContentTypes: [],
        selectedDateRange: [],
        selectedSourceDisplayName: [],
        selectedAuthorId: [],
        showAllPostedBy: false,
        showAllSources: false,
        suggesstedCards: [],
        postedBy: [],
        sources: [],
        smartCardTypes: [],
        contentTypes: [],
        isSearching: true,
        isRenderSearchResult: true,
        queryFromAggs: false,
        searchContentChips: [],
        postByChips: [],
        sourceChips: [],
        dateRangeChips: []
      }),
      () => {
        if (fromClearAll) {
          this.buildQuery();
        }
      }
    );
  };

  // generate aggrigations
  generateAggregations = response => {
    const aggsArray = response.aggs;
    const sources = [];
    const contentTypes = [];
    const smartCardTypes = [];
    const postedBy = [];
    // sort by type
    aggsArray.forEach(obj => {
      obj.display_name = obj.display_name.replace(/_/, ' ');
      switch (obj.type) {
        case 'source_display_name':
          sources.push(obj);
          break;
        case 'content_type':
          if (obj.card_type !== 'pack' && obj.card_type !== 'journey') {
            smartCardTypes.push(obj);
          } else {
            contentTypes.push(obj);
          }
          break;
        case 'author_id':
          postedBy.push(obj);
          break;
        default:
          break;
      }
    });
    const sourcesEntries = sources;
    const postedByEntries = postedBy;
    const smartCardsCount = smartCardTypes.reduce((total, obj) => total + obj.count, 0);
    this.setState(() => ({
      sources: this.state.showAllSources ? sources : sourcesEntries.slice(0, 5),
      contentTypes,
      postedBy: this.state.showAllPostedBy ? postedBy : postedByEntries.slice(0, 5),
      smartCardTypes,
      aggsArray,
      postedByEntries,
      sourcesEntries,
      smartCardsCount,
      suggesstedCards: this.state.filterCards,
      isSearching: true,
      isRenderSearchResult: false,
      searchQueryString: this.searchQuery.q
    }));
  };

  // add remove chips
  addRemoveChip = (value, isChecked) => {
    const searchContentChips = this.state.searchContentChips;
    if (isChecked) {
      const contentArray = [...this.state.smartCardTypes, ...this.state.contentTypes];
      const objIndex = contentArray.findIndex(obj => obj.id === value);
      searchContentChips.push(contentArray[objIndex]);
    } else {
      const index = searchContentChips.findIndex(obj => obj.id === value);
      searchContentChips.splice(index, 1);
    }
    this.setState({ searchContentChips: uniqWith(searchContentChips, isEqual) });
  };

  // checkbox handler for content type filter
  handleSearchContentType = (event, isChecked) => {
    event.persist();
    const selectedContentTypes = this.state.selectedContentTypes;
    this.addRemoveChip(event.target.value, isChecked);
    if (isChecked) {
      selectedContentTypes.push(event.target.value);
    } else {
      const index = selectedContentTypes.indexOf(event.target.value);
      selectedContentTypes.splice(index, 1);
    }
    const smartCheck = this.state.smartCardTypes.some(
      c => selectedContentTypes.indexOf(c.id) === -1
    );
    this.searchQuery.offset = 0;
    this.setState(
      () => ({
        selectedContentTypes,
        smartCardChecked: !smartCheck,
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCards: []
      }),
      () => {
        this.buildQuery();
      }
    );
  };

  // handle aggrigation cchek
  aggregationCheckBoxhandler = (value, filtertype) => {
    this.searchQuery.offset = 0;
    switch (filtertype) {
      case 'postedBy':
        this.setState(() => ({ selectedAuthorId: value }));
        const postByChips = this.state.postedBy.filter(obj => {
          const index = value.findIndex(val => obj.id === val);
          if (index !== -1) {
            return obj;
          }
        });
        this.setState(() => ({ postByChips }));
        break;
      case 'sources':
        this.setState(() => ({ selectedSourceDisplayName: value }));
        const sourceChips = this.state.sources.filter(obj => {
          const index = value.findIndex(val => obj.id === val);
          if (index !== -1) {
            return obj;
          }
        });
        this.setState(() => ({ sourceChips }));
        break;
      case 'year':
        const selectedDateRange = value.map(date => {
          const index = this.dateFilter.findIndex(content => content.id === date);
          return this.dateFilter[index].value;
        });
        const dateRangeChips = this.dateFilter.filter(obj => {
          const index = value.findIndex(val => obj.id === val);
          if (index !== -1) {
            return obj;
          }
        });
        this.setState(() => ({ selectedDateRange, dateRangeChips }));
        break;
      default:
        break;
    }
    this.setState(
      () => ({
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCards: []
      }),
      () => {
        this.buildQuery();
      }
    );
  };

  // handle smart card checkbox click
  handleSmartCardCheckClicked = (cards, isChecked) => {
    const selectedContentTypes = isChecked
      ? uniq(cards.map(c => c.id).concat(this.state.selectedContentTypes))
      : [];
    let searchContentChips = [...this.state.searchContentChips];
    if (isChecked) {
      searchContentChips = searchContentChips.concat(cards);
    } else {
      searchContentChips = searchContentChips.filter(chip => {
        const index = cards.findIndex(cardObj => cardObj.id === chip.id);
        if (index === -1) {
          return chip;
        }
      });
    }
    this.searchQuery.offset = 0;
    this.setState(
      () => ({
        selectedContentTypes,
        smartCardChecked: !this.state.smartCardChecked,
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCards: [],
        searchContentChips: uniqWith(searchContentChips, isEqual)
      }),
      () => {
        this.buildQuery();
      }
    );
  };

  // // view more handler for sources
  sourceViewMorehandler = () => {
    this.setState({
      sources: this.state.sourcesEntries,
      showAllSources: !this.state.showAllSources
    });
  };

  // view more handler for sources
  postedViewMorehandler = () => {
    this.setState({
      postedBy: this.state.postedByEntries,
      showAllPostedBy: !this.state.showAllPostedBy
    });
  };

  removeFilterChip = obj => {
    switch (obj.type) {
      case 'content_type':
        const selectedContentTypes = this.state.selectedContentTypes.filter(
          content => content !== obj.id
        );
        const searchContentChips = this.state.searchContentChips.filter(
          content => content.id !== obj.id
        );
        const smartCardChecked = !this.state.smartCardTypes.some(
          cardType => cardType.id === obj.id
        );
        this.setState(() => ({ searchContentChips, selectedContentTypes, smartCardChecked }));
        break;
      case 'author_id':
        const postByChips = this.state.postByChips.filter(content => content.id !== obj.id);
        const selectedAuthorId = this.state.selectedAuthorId.filter(content => content !== obj.id);
        this.setState(() => ({ postByChips, selectedAuthorId }));
        break;
      case 'source_display_name':
        const sourceChips = this.state.sourceChips.filter(content => content.id !== obj.id);
        const selectedSourceDisplayName = this.state.selectedSourceDisplayName.filter(
          content => content !== obj.id
        );
        this.setState(() => ({ sourceChips, selectedSourceDisplayName }));
        break;
      case 'date_range':
        const dateRangeChips = this.state.dateRangeChips.filter(content => content.id !== obj.id);
        const selectedDateRange = this.state.selectedDateRange.filter(
          content => content !== obj.value
        );
        this.setState(() => ({ dateRangeChips, selectedDateRange }));
        break;
      default:
        break;
    }
    this.setState(
      () => ({
        removedAggrigationOptn: obj,
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCards: []
      }),
      () => {
        this.buildQuery();
        this.setState(() => ({ removedAggrigationOptn: null }));
      }
    );
  };

  // todo
  removeCardFromList = cardId => {
    if (this.state.isSearching) {
      // const cardIndex = this.state.sugge
    } else {
    }
  };
  render() {
    const checkBoxColor = '#6f708b';
    const searchChips = [
      ...this.state.searchContentChips,
      ...this.state.postByChips,
      ...this.state.sourceChips,
      ...this.state.dateRangeChips
    ];
    return (
      <div className="channel-search-container">
        <div className="search-info-box">
          <div className="aggregation-box-column">
            {(!!this.state.suggesstedCards.length || this.state.queryFromAggs) && (
              <p className="text">{tr('Refine your search')}</p>
            )}
          </div>
          <div className="searched-card-column">
            {((!this.state.pending && !!this.state.suggesstedCards.length) ||
              this.state.queryFromAggs) && (
              <p className="text">
                {`${tr('About')} ${this.state.resultQuantity} ${tr('results for')} `}
                <span className="query-text">
                  {this.state.searchQueryString
                    ? this.state.searchQueryString.length > 50
                      ? `"${this.state.searchQueryString.slice(0, 50)}..."`
                      : `"${this.state.searchQueryString}"`
                    : tr('all')}
                </span>
              </p>
            )}
            <div className="selected-categories">
              {!!searchChips.length &&
                searchChips.map(obj => (
                  <div className="badge" key={obj.id}>
                    <p>{obj.display_name || obj.displayName}</p>
                    <div className="close-btn" onClick={this.removeFilterChip.bind(this, obj)}>
                      <CloseIcon style={{ height: '0.75rem', width: '0.75rem' }} />
                    </div>
                  </div>
                ))}
              {!!searchChips.length && (
                <span className="clear-all" onClick={this.resetFilter.bind(this, true)}>
                  {tr('clear all')}
                </span>
              )}
            </div>
          </div>
        </div>
        <div className="channel-search-layout">
          <div className="aggregation-box-column">
            <div className="aggregation-box-container">
              {(this.state.contentTypes.length > 0 || this.state.smartCardTypes.length > 0) && (
                <div className="filter-block">
                  <div className="filter-title">{tr('Content Type')}</div>
                  <div className="filter-content">
                    {this.state.smartCardTypes.length > 0 && (
                      <div className="outer-checkbox">
                        <Checkbox
                          label={`SmartCard (${this.state.smartCardsCount})`}
                          checkedIcon={<CheckOn1 color={checkBoxColor} />}
                          uncheckedIcon={
                            <CheckOff style={styles.uncheckedIcon} color={checkBoxColor} />
                          }
                          iconStyle={styles.checkboxOuter}
                          labelStyle={styles.checkboxOuterLabel}
                          onCheck={(e, isChecked) =>
                            this.handleSmartCardCheckClicked(this.state.smartCardTypes, isChecked)
                          }
                          value={this.smartCardTypes}
                          checked={this.state.smartCardChecked}
                        />
                      </div>
                    )}
                    {this.state.smartCardTypes.length > 0 &&
                      this.state.smartCardTypes.map(content => (
                        <div className="inner-checkbox" key={content.id}>
                          <Checkbox
                            label={`${content.display_name} (${content.count})`}
                            checkedIcon={<CheckOn1 color={checkBoxColor} />}
                            uncheckedIcon={
                              <CheckOff style={styles.uncheckedIconInner} color={checkBoxColor} />
                            }
                            iconStyle={styles.checkboxInner}
                            labelStyle={styles.checkboxInnerLabel}
                            onCheck={this.handleSearchContentType}
                            value={content.id}
                            checked={this.state.selectedContentTypes.indexOf(content.id) !== -1}
                          />
                        </div>
                      ))}
                    {this.state.contentTypes.length > 0 &&
                      this.state.contentTypes.map(content => (
                        <div className="outer-checkbox" key={content.id}>
                          <Checkbox
                            label={`${content.display_name} (${content.count})`}
                            checkedIcon={<CheckOn1 color={checkBoxColor} />}
                            uncheckedIcon={
                              <CheckOff style={styles.uncheckedIcon} color={checkBoxColor} />
                            }
                            iconStyle={styles.checkboxOuter}
                            labelStyle={styles.checkboxOuterLabel}
                            onCheck={this.handleSearchContentType}
                            value={content.id}
                            checked={this.state.selectedContentTypes.indexOf(content.id) !== -1}
                          />
                        </div>
                      ))}
                  </div>
                </div>
              )}
              {this.state.postedBy.length > 0 && (
                <AggregationBox
                  contentArray={this.state.postedBy}
                  aggregationHandler={this.aggregationCheckBoxhandler}
                  aggsLable={tr('Posted By')}
                  showViewMore={
                    this.state.postedByEntries.length > 5 && !this.state.showAllPostedBy
                  }
                  showMoreHandler={this.postedViewMorehandler}
                  filterType={'postedBy'}
                  removedAggrigationOptn={this.state.removedAggrigationOptn}
                />
              )}
              {this.state.sources.length > 0 && (
                <AggregationBox
                  contentArray={this.state.sources}
                  aggregationHandler={this.aggregationCheckBoxhandler}
                  aggsLable={tr('Sources')}
                  showViewMore={this.state.sourcesEntries.length > 5 && !this.state.showAllSources}
                  showMoreHandler={this.sourceViewMorehandler}
                  filterType={'sources'}
                  removedAggrigationOptn={this.state.removedAggrigationOptn}
                />
              )}
              {(this.state.suggesstedCards.length !== 0 || this.state.queryFromAggs) && (
                <AggregationBox
                  contentArray={this.dateFilter}
                  aggregationHandler={this.aggregationCheckBoxhandler}
                  aggsLable={tr('Year')}
                  showViewMore={false}
                  filterType={'year'}
                  removedAggrigationOptn={this.state.removedAggrigationOptn}
                />
              )}
            </div>
          </div>
          <div className="searched-card-column">
            {this.state.suggesstedCards &&
              this.state.suggesstedCards.length > 0 &&
              this.state.suggesstedCards.map((card, index) => (
                <Card
                  key={card.id}
                  card={card}
                  index={index + 1}
                  fullView={true}
                  dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                  startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                  author={card.author}
                  user={this.currentUser}
                  isMarkAsCompleteDisabled={true}
                  withoutCardModal={true}
                  key={index}
                  type={this.viewRegime}
                  removeCardFromList={this.removeCardFromList}
                />
              ))}
            {this.state.suggesstedCards.length === 0 &&
              !this.state.pending &&
              this.state.queryFromAggs && (
                <div className="note-container">
                  <small>{tr('No data related to search and filter.')}</small>
                </div>
              )}
            {this.state.pending && (
              <div className="make-center">
                <Spinner />
              </div>
            )}
          </div>
        </div>
        {this.state.suggesstedCards.length === 0 &&
          !this.state.pending &&
          !this.state.queryFromAggs && (
            <div className="note-container">
              <small>{tr('No data related to search and filter.')}</small>
            </div>
          )}
      </div>
    );
  }
}

ChannelSearch.propTypes = {
  searchQueryString: PropTypes.string,
  channelId: PropTypes.number
};

export default connect(state => ({
  currentUser: state.currentUser
}))(ChannelSearch);
