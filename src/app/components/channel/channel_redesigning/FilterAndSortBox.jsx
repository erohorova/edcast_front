import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover';
import moment from 'moment';

import Filter from 'edc-web-sdk/components/icons/Filter';
import { tr } from 'edc-web-sdk/helpers/translations';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import CloseIcon from 'material-ui/svg-icons/content/clear';

export default class FilterAndSortBox extends Component {
  state = {
    isSortDropDrown: false,
    isFilterVisible: false,
    selectedDate: '',
    selectedContent: '',
    createdAtAsc: false,
    likedByAsc: false
  };

  styles = {
    filterIconStyle: {
      width: '22px',
      fill: '#6f708b',
      height: '20px'
    }
  };

  showFilters = () => {
    this.setState(() => ({ isFilterVisible: !this.state.isFilterVisible }));
  };

  requestCloseHandler = () => {
    this.setState(() => ({ isSortDropDrown: false }));
  };

  currentYear = moment().year();
  dateFilter = [
    {
      id: 'today',
      type: 'date_range',
      displayName: 'Today',
      value: {
        from_date: moment().format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'this-week',
      type: 'date_range',
      displayName: 'This Week',
      value: {
        from_date: moment()
          .subtract(7, 'days')
          .format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'this-month',
      type: 'date_range',
      displayName: 'This Month',
      value: {
        from_date: moment()
          .subtract(30, 'days')
          .format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'last-three-month',
      type: 'date_range',
      displayName: 'Last 3 Months',
      value: {
        from_date: moment()
          .subtract(90, 'days')
          .format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'last-six-month',
      type: 'date_range',
      displayName: 'Last 6 Months',
      value: {
        from_date: moment()
          .subtract(180, 'days')
          .format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'last-nine-month',
      type: 'date_range',
      displayName: 'Last 9 Months',
      value: {
        from_date: moment()
          .subtract(270, 'days')
          .format('DD/MM/YYYY'),
        to_date: moment().format('DD/MM/YYYY')
      }
    },
    {
      id: 'current-year',
      type: 'date_range',
      displayName: 'This Year',
      value: {
        from_date: `01/01/${this.currentYear}`,
        to_date: moment().format('DD/MM/YYYY')
      }
    }
  ];

  contentTypeFilter = [
    {
      id: 'link',
      label: 'Article'
    },
    {
      id: 'video',
      label: 'Video'
    },
    {
      id: 'audio',
      label: 'Audio'
    },
    {
      id: 'file',
      label: 'File (PDF, Word etc)'
    },
    {
      id: 'text',
      label: 'Text'
    },
    {
      id: 'course',
      label: 'Course'
    },
    {
      id: 'Video_stream',
      label: 'Livestream'
    },
    {
      id: 'journey',
      label: 'Journey'
    },
    {
      id: 'pack',
      label: 'Pathway'
    },
    {
      id: 'poll',
      label: 'Poll'
    }
  ];
  filterByDate = dateObj => {
    this.setState(() => ({ selectedDate: dateObj.id }));
    this.props.filterCall(dateObj.value, 'date');
  };

  filterByContentType = contentObj => {
    this.setState(() => ({ selectedContent: contentObj.id }));
    this.props.filterCall(contentObj.id, 'content');
  };

  sortContent = sortType => {
    switch (sortType) {
      case 'date':
        this.props.sortCall({
          sort: 'created_at',
          order: this.state.createdAtAsc ? 'ASC' : 'DESC'
        });
        this.setState(() => ({ createdAtAsc: !this.state.createdAtAsc }));
        break;
      case 'like':
        this.props.sortCall({
          sort: 'likes_count',
          order: this.state.likedByAsc ? 'DESC' : 'ASC'
        });
        this.setState(() => ({ likedByAsc: !this.state.likedByAsc }));
        break;
      case 'most_viewed':
        this.props.sortCall({
          sort: 'views_count',
          order: 'DESC'
        });
        break;
      case 'least_viewed':
        this.props.sortCall({
          sort: 'views_count',
          order: 'ASC'
        });
        break;
      default:
        break;
    }
    this.requestCloseHandler();
  };

  removeContentFilter = () => {
    this.setState(() => ({ selectedContent: '' }));
    this.props.filterCall('', 'content');
  };

  removeDateFilter = () => {
    this.setState(() => ({ selectedDate: '' }));
    this.props.filterCall({ from_date: '', to_date: '' }, 'date');
  };

  render() {
    const arrowColor = '#6f708b';
    return (
      <div className="filter-sort-layout">
        <div className="fliter-sort-box">
          {this.props.isFilterVisible && (
            <div className="filter-box" onClick={this.showFilters}>
              <Filter style={this.styles.filterIconStyle} />
              <p>{tr('Filter')}</p>
            </div>
          )}
          {this.props.isSortVisible && (
            <div
              className="sort-box"
              ref={instance => (this.sortBox = instance)}
              onClick={() => this.setState(() => ({ isSortDropDrown: true }))}
            >
              <div>
                <p>{tr('Sort by Default')}</p>
              </div>
              <div className="down-arrow" />
            </div>
          )}
          <Popover
            open={this.state.isSortDropDrown}
            anchorEl={this.sortBox}
            onRequestClose={this.requestCloseHandler}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            targetOrigin={{ horizontal: 'right', vertical: 'top' }}
          >
            <div className="sort-popover-container">
              <div className="popover-text-container" onClick={this.sortContent.bind(this, 'date')}>
                <p className="popover-text">{tr('Sort by date')}</p>
                {this.state.createdAtAsc ? (
                  <div className="up-arrow">
                    <BackIcon style={{ height: '12px' }} color={arrowColor} />
                  </div>
                ) : (
                  <div className="down-arrow">
                    <BackIcon style={{ height: '12px' }} color={arrowColor} />
                  </div>
                )}
              </div>
              <div className="popover-text-container" onClick={this.sortContent.bind(this, 'like')}>
                <p className="popover-text">{tr('Likes')}</p>
                {this.state.likedByAsc ? (
                  <div className="down-arrow">
                    <BackIcon style={{ height: '12px' }} color={arrowColor} />
                  </div>
                ) : (
                  <div className="up-arrow">
                    <BackIcon style={{ height: '12px' }} color={arrowColor} />
                  </div>
                )}
              </div>
              <div
                className="popover-text-container"
                onClick={this.sortContent.bind(this, 'most_viewed')}
              >
                <p className="popover-text">{tr('Most Viewed')}</p>
              </div>
              <div
                className="popover-text-container"
                onClick={this.sortContent.bind(this, 'least_viewed')}
              >
                <p className="popover-text">{tr('Least Viewed')}</p>
              </div>
            </div>
          </Popover>
        </div>
        <div
          className={`channel-filter-container ${
            this.state.isFilterVisible ? 'show-filter-content' : 'hide-filter-content'
          }`}
        >
          <div className="filter-header-container thin-border">
            <div className="filter-column">
              <p className="filter-header">{tr('Upload Date')}</p>
            </div>
            <div className="filter-column">
              <p className="filter-header">{tr('Type')}</p>
            </div>
          </div>
          <div className="filter-content-conatiner">
            <div className="filter-column">
              {this.dateFilter.map(dateObj => (
                <div className="filter-option" key={dateObj.id}>
                  <p
                    key={dateObj.displayName}
                    className="filter-label"
                    onClick={this.filterByDate.bind(this, dateObj)}
                  >
                    {tr(dateObj.displayName)}
                  </p>
                  {this.state.selectedDate === dateObj.id && (
                    <div className="close-icon" onClick={this.removeDateFilter.bind(this, dateObj)}>
                      <CloseIcon color="black" style={{ width: '14px' }} />
                    </div>
                  )}
                </div>
              ))}
            </div>
            <div className="filter-column">
              {this.contentTypeFilter.map(contentObj => (
                <div className="filter-option" key={contentObj.id}>
                  <p
                    key={contentObj.label}
                    className="filter-label"
                    onClick={this.filterByContentType.bind(this, contentObj)}
                  >
                    {tr(contentObj.label)}
                  </p>
                  {this.state.selectedContent === contentObj.id && (
                    <div
                      className="close-icon"
                      onClick={this.removeContentFilter.bind(this, contentObj)}
                    >
                      <CloseIcon color="black" style={{ width: '14px' }} />
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FilterAndSortBox.propTypes = {
  filterCall: PropTypes.func,
  sortCall: PropTypes.func,
  isFilterVisible: PropTypes.bool,
  isSortVisible: PropTypes.bool
};
