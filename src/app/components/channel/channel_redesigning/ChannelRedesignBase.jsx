import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { goBack } from 'react-router-redux';

import ChannelBanner from './ChannelBanner';
import { getChannel } from 'edc-web-sdk/requests/channels';
import { searchCardsInChannel } from 'edc-web-sdk/requests/channels.v2';
import Spinner from '../../common/spinner';
import ChannelCarousel from './ChannelCarousel';
import SearchWithSuggestions from './SearchWithSuggestions';
import ChannelSearch from './ChannelSearch';
import CarouselTabs from './CarouselTabs';
import TabStandAloneView from './TabStandAloneView';
import FeaturedPosts from './FeaturedPosts';
import { saveChannelDetails, removeFeaturedCards } from '../../../actions/channelsActionsV3';
import FilterAndSortBox from './FilterAndSortBox';
import HtmlWidget from './HtmlWidget';

class ChannelRedesignBase extends Component {
  INITIAL_STATE = {
    loadingDetails: true,
    channel: null,
    channelSuggestions: [],
    currentSearchValue: '',
    isSearching: false,
    searchQueryString: null,
    isShowAllCarousels: true,
    isShowStandAloneTabView: false,
    selectedTabData: null,
    selectedTab: 'All',
    showFeaturedCards: true,
    cardFilterByContent: '',
    cardSortBy: '',
    cardOrderIn: '',
    cardFilterByDate: {
      from_date: '',
      to_date: ''
    },
    showAllFilterAndSortCards: false,
    isCurator: false,
    isEditable: false
  };

  state = { ...this.INITIAL_STATE };

  componentDidMount() {
    this.fetchChannelDetails(this.props.routeParams.slug);
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.routeParams.slug !== this.props.routeParams.slug) {
      this.fetchChannelDetails(nextProps.routeParams.slug);
    }
    return true;
  }

  // get channel details from slug
  fetchChannelDetails = slug => {
    this.props.dispatch(removeFeaturedCards());
    this.setState(() => this.INITIAL_STATE);
    getChannel(slug)
      .then(data => {
        if (data) {
          this.props.dispatch(saveChannelDetails(data));
          this.setState(() => ({ channel: data, loadingDetails: false }));
          this.props.dispatch(saveChannelDetails(data));
          this.checkIfEditableAndCuratable();
        }
      })
      .catch(err => {
        console.error('Error in ChannelRedesignBase.fetchChannelDetails', err);
      });
  };

  // list for channel search
  getSuggestedList = value => {
    searchCardsInChannel(this.state.channel.id, { q: value })
      .then(response => {
        if (response.cards && !!response.cards.length) {
          this.setState(() => ({
            channelSuggestions: response.cards.map(item => {
              return { id: item.id, text: item.message };
            })
          }));
        }
      })
      .catch(error => {
        console.error('Error in ChannelRedesignBase.getSuggestedList', error);
      });
  };

  // showing query result
  showFilterResult = value => {
    this.setState(() => ({
      searchQueryString: value,
      isSearching: true,
      showFeaturedCards: false,
      isShowStandAloneTabView: false
    }));
  };

  showStandAloneTabView = tabObj => {
    this.setState(() => ({
      isShowAllCarousels: false,
      isShowStandAloneTabView: true,
      selectedTabData: tabObj,
      isSearching: false,
      selectedTab: tabObj.default_label,
      showFeaturedCards: false
    }));
  };

  showAllCarousel = () => {
    this.setState(() => ({
      isShowAllCarousels: true,
      selectedTab: 'All',
      showFeaturedCards: true,
      isSearching: false,
      showAllFilterAndSortCards: false,
      isShowStandAloneTabView: false
    }));
  };

  goBackFromChannel = () => {
    this.props.dispatch(goBack());
  };

  // set sort by values in state for FilterAndSortCardView component
  getSortCards = value => {
    this.setState(() => ({
      cardSortBy: value.sort ? value.sort : this.state.cardSortBy,
      cardOrderIn: value.order ? value.order : this.state.cardOrderIn
    }));
  };

  getFilteredCards = (value, filterType) => {
    switch (filterType) {
      case 'date':
        this.setState(
          () => ({
            cardFilterByDate: value,
            showAllFilterAndSortCards: true
          }),
          () => {
            this.resetFIlter();
          }
        );
        break;
      case 'content':
        this.setState(
          () => ({
            cardFilterByContent: value,
            showAllFilterAndSortCards: true
          }),
          () => {
            this.resetFIlter();
          }
        );
        break;
      default:
        break;
    }
  };

  resetFIlter = () => {
    if (!this.state.cardFilterByContent.length && !this.state.cardFilterByDate.from_date.length) {
      this.setState(() => ({ showAllFilterAndSortCards: false }));
    }
  };

  checkIfEditableAndCuratable = () => {
    const state = {
      isEditable: false,
      isCurator: false
    };
    let channel = this.state.channel;
    let currentUserId = this.props.currentUser.get('id');

    // check channel editable permission
    state.isEditable =
      channel && //Admin can edit any channel
      (this.props.currentUser.get('isAdmin') ||
        //Creator can edit channel
        (channel.creator && channel.creator.id == currentUserId) ||
        //Followers of open channel
        (channel.isOpen && channel.isFollowing) ||
        //Curators can edit channel
        (channel.curators && channel.curators.some(item => item.id == currentUserId)));

    // check channel curatable permission
    let isOpen = channel && (typeof channel.isOpen == 'boolean' && channel.isOpen);
    state.isCurator =
      channel &&
      channel.curateOnly &&
      //open channel and user is following and channel is curateable
      ((channel.isFollowing && isOpen) ||
        //current user is curator of channel
        (channel.curators && channel.curators.some(item => item.id == currentUserId)));
    this.setState(state);
  };

  render() {
    return (
      <div>
        {this.state.loadingDetails && (
          <div className="make-loader-center">
            <Spinner />
          </div>
        )}
        {!this.state.loadingDetails && (
          <div className="channel-redesign-base">
            <ChannelBanner
              channel={this.state.channel}
              goBackFromChannel={this.goBackFromChannel}
              isEditable={this.state.isEditable}
              isCurator={this.state.isCurator}
            />
            <div className="content-padding card-v3-padding-fix">
              <HtmlWidget channel={this.state.channel} />
              <div className="tab-search-container">
                <CarouselTabs
                  carousels={this.state.channel.carousels}
                  showAllCarousel={this.showAllCarousel}
                  showStandAloneTabView={this.showStandAloneTabView}
                  selectedTab={this.state.selectedTab}
                />
                <div className="search-container">
                  <SearchWithSuggestions
                    suggestionList={this.state.channelSuggestions}
                    getSuggestList={this.getSuggestedList}
                    currentSearchValue={this.state.currentSearchValue}
                    showFilterResult={this.showFilterResult}
                  />
                </div>
              </div>
              {!this.state.isShowStandAloneTabView && !this.state.isSearching && (
                <FilterAndSortBox
                  isFilterVisible
                  isSortVisible
                  sortCall={this.getSortCards}
                  filterCall={this.getFilteredCards}
                />
              )}
              {this.state.showFeaturedCards &&
                this.state.cardFilterByDate.from_date === '' &&
                this.state.cardFilterByContent === '' && (
                  <FeaturedPosts channelId={this.state.channel.id} />
                )}
              {this.state.channel.carousels &&
                !!this.state.channel.carousels.length &&
                !this.state.isSearching &&
                this.state.isShowAllCarousels && (
                  <div className="all-carousel">
                    {this.state.channel.carousels.map(carousel => (
                      <div key={carousel.default_label}>
                        {carousel.visible && (
                          <ChannelCarousel
                            data={carousel}
                            channelId={this.state.channel.id}
                            cardSortBy={this.state.cardSortBy}
                            cardOrderIn={this.state.cardOrderIn}
                            cardFilterByDate={this.state.cardFilterByDate}
                            cardFilterByContent={this.state.cardFilterByContent}
                            channelId={this.state.channel.id}
                            defaultCardTypes={this.state.defaultCardTypes}
                            showAllFilterAndSortCards={this.state.showAllFilterAndSortCards}
                            isEditable={this.state.isEditable}
                          />
                        )}
                      </div>
                    ))}
                  </div>
                )}
              {this.state.isSearching && (
                <ChannelSearch
                  searchQueryString={this.state.searchQueryString}
                  channelId={this.state.channel.id}
                />
              )}
              {this.state.isShowStandAloneTabView &&
                !this.state.isShowAllCarousels &&
                !this.state.isSearching && (
                  <TabStandAloneView
                    data={this.state.selectedTabData}
                    channelId={this.state.channel.id}
                    showAllCarousel={this.showAllCarousel}
                  />
                )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelRedesignBase.propTypes = {
  routeParams: PropTypes.object,
  channel: PropTypes.object,
  currentUser: PropTypes.object
};

const mapStateToProps = state => ({
  channel: state.channelReducerV3.get('channel'),
  currentUser: state.currentUser
});

export default connect(mapStateToProps)(ChannelRedesignBase);
