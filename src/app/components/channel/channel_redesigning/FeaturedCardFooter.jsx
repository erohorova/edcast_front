import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactStars from 'react-stars';
import InsightDropDownActions from '../../../components/feed/InsightDropDownActions';
import TooltipLabel from '../../common/TooltipLabel';
import { tr } from 'edc-web-sdk/helpers/translations';

class FeaturedCardFooter extends Component {
  render() {
    const cardSubtype = this.props.card.cardSubtype;
    const whiteCardTypes = ['text', 'poll', 'file'];
    const { commentsCount } = this.props.card;
    const averageRating = Math.trunc(this.props.card.averageRating);
    return (
      <div className="footer">
        <div className="left-conatiner">
          <div className="comment-conatiner">
            <span className="comment-avatar-container">
              {!!commentsCount &&
                !!this.props.commentedUser.length &&
                this.props.commentedUser.map((val, index) => (
                  <TooltipLabel
                    text={val.user.fullName}
                    html={true}
                    position={'top right'}
                    key={index}
                  >
                    <a href={`/${val.user.handle || val.user.slug}`}>
                      <div
                        key={index}
                        className="comment-avatar"
                        style={{ right: `${index * 0.5}rem` }}
                      >
                        <img src={val.user.pictureUrl} />
                      </div>
                    </a>
                  </TooltipLabel>
                ))}
            </span>
            {!!commentsCount && (
              <p
                className={
                  whiteCardTypes.indexOf(cardSubtype) !== -1
                    ? 'count-text dark-text-comment'
                    : 'count-text'
                }
                style={{ right: `${(this.props.commentedUser.length - 1) * 0.5}rem` }}
              >
                {commentsCount > 99 ? '99+' : commentsCount}
                {commentsCount === 1 ? tr(' comment') : tr(' comments')}
              </p>
            )}
          </div>
        </div>
        <div className="right-conatiner">
          <ReactStars
            count={5}
            color1={whiteCardTypes.indexOf(cardSubtype) !== -1 ? '#f0f0f5' : '#6f708b'}
            color2={whiteCardTypes.indexOf(cardSubtype) !== -1 ? '#6f708b' : '#f0f0f5'}
            value={averageRating}
            edit={false}
            size={13}
          />
          <span
            className={
              whiteCardTypes.indexOf(cardSubtype) !== -1
                ? 'count-text dark-text-count'
                : 'count-text'
            }
          >
            {averageRating ? averageRating : 0}
          </span>
          <div className="insight-dropdown">
            <InsightDropDownActions
              card={this.props.card}
              author={this.props.card.author}
              isStandalone={false}
              assignable={false}
              isCardV3
              showVerticalMenu
              verticalMenuColor={whiteCardTypes.indexOf(cardSubtype) !== -1 && '#6f708b'}
              type={'featuredCard'}
              disableTopics={this.props.disableTopics}
              channel={this.props.channel}
            />
          </div>
        </div>
      </div>
    );
  }
}

FeaturedCardFooter.propTypes = {
  card: PropTypes.object,
  commentedUser: PropTypes.array,
  disableTopics: PropTypes.bool,
  channel: PropTypes.object
};

const mapStateToProps = state => ({
  channel: state.channelReducerV3.get('channel')
});

export default connect(mapStateToProps)(FeaturedCardFooter);
