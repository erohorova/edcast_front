import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import NextIcon from 'material-ui/svg-icons/image/navigate-next';
import PrevIcon from 'material-ui/svg-icons/image/navigate-before';

export default class CarouselTabs extends Component {
  state = {
    showArrows: false,
    showLeftArrow: false,
    selectedTab: '',
    carousels: []
  };

  componentDidMount() {
    if (this.props.carousels && !!this.props.carousels.length) {
      const carousels = this.props.carousels.filter(carousel => carousel.visible);
      const selectedTab = this.props.carousels[0].default_label
      carousels.unshift({
        default_label: 'All'
      });
      this.setState(() => ({ carousels, selectedTab }));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedTab !== this.state.selectedTab) {
      this.setState(() => ({ selectedTab: nextProps.selectedTab }));
    }
  }

  next = () => {
    this.slider.slickNext();
  };

  prev = () => {
    this.slider.slickPrev();
  };

  onTabPress = (tabLabel) => {
    if (tabLabel === 'All') {
      this.props.showAllCarousel();
    } else {
      this.setState(() => ({ selectedTab: tabLabel }));
      const tabObj = this.state.carousels.filter(carousel => carousel.default_label === tabLabel)[0];
      this.props.showStandAloneTabView(tabObj);
    }
  };

  render() {
    const menu = Menu(this.state.carousels, this.state.selectedTab);
    return (
      <div className="tab-container">
        <ScrollMenu
          ref={el => (this.menu = el)}
          data={menu}
          arrowLeft={ArrowLeft}
          arrowRight={ArrowRight}
          selected={this.state.selectedTab}
          onSelect={this.onTabPress}
          hideArrows
          hideSingleArrow
          scrollToSelected
          translate={0}
          alignCenter={false}
        />
      </div>
    );
  }
}

CarouselTabs.propTypes = {
  carousels: PropTypes.array,
  showAllCarousel: PropTypes.func,
  showStandAloneTabView: PropTypes.func,
  selectedTab: PropTypes.string
};


// One item component
// selected prop will be passed
const MenuItem = (({ text, selected }) => <div className={`menu-item ${selected ? 'active' : ''}`}>{text}</div>);
MenuItem.propTypes = {
  text: PropTypes.string,
  selected: PropTypes.bool
}

// All items component
// Important! add unique key
export const Menu = (list, selected) => list.map((el) =>  <MenuItem text={el.default_label} key={el.default_label} selected={selected} />);

// Menu Arrow
const Arrow = (({ Icon, className }) => <div className={className}>{Icon}</div>);
Arrow.propTypes = {
  Icon: PropTypes.func,
  className: PropTypes.string
}


const ArrowLeft = Arrow({ Icon: <PrevIcon />, className: 'arrow-prev' });
const ArrowRight = Arrow({ Icon: <NextIcon />, className: 'arrow-next' });


