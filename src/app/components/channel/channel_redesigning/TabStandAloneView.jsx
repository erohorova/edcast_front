import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import throttle from 'lodash/throttle';

import Spinner from '../../common/spinner';
import { getChannleCarouselData } from 'edc-web-sdk/requests/channels.v2';
import { getCarouselItems } from 'edc-web-sdk/requests/carousels';
import Card from './../../cards/Card';
import FilterAndSortBox from './FilterAndSortBox';
import { tr } from 'edc-web-sdk/helpers/translations';

export default class TabStandAloneView extends Component {
  constant = {
    duskColor: '#454560'
  };

  state = {
    cards: [],
    totalCardCount: null,
    loading: true,
    tabData: this.props,
    paginationQuery: false,
    paginationLoader: false,
    currentCardCount: null
  };

  componentDidMount() {
    if (this.state.tabData.data.custom_carousel) {
      this.fetchCustomCarouselData();
    } else {
      this.fetchData(this.state.tabData, this.searchQuery.offset);
    }
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.data.default_label !== this.props.data.default_label) {
      this.searchQuery.offset = 0;
      this.searchQuery.limit = 10;
      this.setState(
        () => ({
          cards: [],
          totalCardCount: null,
          loading: true,
          tabData: nextProps
        }),
        () => {
          if (!nextProps.data.custom_carousel) {
            this.fetchData(this.state.tabData);
          } else {
            this.fetchCustomCarouselData();
          }
        }
      );
    }
  }

  searchQuery = {
    limit: 10,
    offset: 0,
    sort: '',
    order: ''
  };

  handleScroll = throttle(
    () => {
      if (
        !this.state.loading &&
        window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight
      ) {
        if (this.state.currentCardCount === this.searchQuery.limit) {
          this.searchQuery.offset = this.searchQuery.offset + this.searchQuery.limit;
          this.setState(
            () => ({
              paginationLoader: true,
              paginationQuery: true
            }),
            () => {
              this.fetchData(this.state.tabData);
            }
          );
        }
      } else return null;
    },
    150,
    { leading: false }
  );

  fetchData = tabData => {
    const { data, channelId } = tabData;
    let cardTypes = [];
    switch (data.default_label) {
      case 'SmartCards':
        cardTypes = ['media', 'poll'];
        break;
      case 'Pathways':
        cardTypes = ['pack'];
        break;
      case 'Journeys':
        cardTypes = ['journey'];
        break;
      case 'Streams':
        cardTypes = ['video_stream'];
        break;
      case 'Courses':
        cardTypes = ['course'];
        break;
      default:
        break;
    }

    const payload = {
      'card_type[]': cardTypes,
      ...this.searchQuery
    };

    getChannleCarouselData(payload, channelId)
      .then(response => {
        if (response.cards) {
          this.setState(() => ({
            cards: this.state.paginationQuery
              ? [...this.state.cards, ...response.cards]
              : response.cards,
            totalCardCount: response.total,
            loading: false,
            paginationLoader: false,
            currentCardCount: response.cards.length
          }));
        }
      })
      .catch(error => {
        this.setState(() => ({ loading: false, paginationLoader: false }));
        console.error('Error in TabStandAloneView.getChannleCarouselData', error);
      });
  };

  fetchCustomCarouselData = () => {
    getCarouselItems(this.state.tabData.data.id)
      .then(response => {
        if (response.structuredItems) {
          const carouselCards = response.structuredItems.map(card => card.entity);
          this.setState(() => ({
            cards: carouselCards,
            totalCardCount: carouselCards.length,
            loading: false,
            paginationLoader: false
          }));
        }
      })
      .catch(error => {
        this.setState(() => ({ loading: false, paginationLoader: false }));
        console.error('Error in ChannelCarousel.fetchCustomCarouselData', error);
      });
  };

  removeCardFromCarousel = () => {};

  sortCall = sortQuery => {
    this.searchQuery = {
      sort: sortQuery.sort,
      order: sortQuery.order || this.searchQuery.order,
      offset: 0,
      limit: 10
    };
    this.setState(
      () => ({ loading: true }),
      () => {
        this.fetchData(this.state.tabData);
      }
    );
  };

  styles = {
    backIconStyle: {
      width: '19px'
    }
  };

  render() {
    return (
      <div className="standalone-tab-container">
        {!this.state.loading && (
          <div>
            {!this.props.data.custom_carousel && (
              <FilterAndSortBox sortCall={this.sortCall} isFilterVisible={false} isSortVisible />
            )}
            <div className="tab-label-conatiner">
              <div className="back-arrow" onClick={this.props.showAllCarousel}>
                <BackIcon style={this.styles.backIconStyle} color={this.constant.duskColor} />
              </div>
              <p className="tab-label">
                {this.props.data.default_label}{' '}
                {!!this.state.totalCardCount && `(${this.state.totalCardCount})`}
              </p>
            </div>
            <div className="cards-container">
              {!!this.state.cards.length &&
                this.state.cards.map(card => (
                  <div className="tab-card" key={card.id}>
                    <Card
                      author={card.author && card.author}
                      card={card}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      tooltipPosition="top-center"
                      moreCards={false}
                      withoutCardModal={true}
                      type={'Tile'}
                      removeCardFromCarousel={this.removeCardFromCarousel}
                    />
                  </div>
                ))}
              {this.state.paginationLoader && (
                <div className="make-loader-center">
                  <Spinner />
                </div>
              )}
              {!!!this.state.cards.length && (
                <div className="make-loader-center">
                  <p>{tr('There is no content to show')}</p>
                </div>
              )}
            </div>
          </div>
        )}
        {this.state.loading && (
          <div className="make-loader-center">
            <Spinner />
          </div>
        )}
      </div>
    );
  }
}

TabStandAloneView.propTypes = {
  selectedTabData: PropTypes.object,
  data: PropTypes.object,
  channelId: PropTypes.number,
  showAllCarousel: PropTypes.func
};
