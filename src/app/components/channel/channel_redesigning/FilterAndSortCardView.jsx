import React, { Component } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';

import Spinner from '../../common/spinner';
import { getChannleCarouselData } from 'edc-web-sdk/requests/channels.v2';
import Card from './../../cards/Card';

export default class FilterAndSortCardView extends Component {
  state = {
    defaultCardSubTypes: ['course', 'video_stream', 'journey', 'pack', 'media', 'poll'],
    filterSortcards: [],
    paginationQuery: false,
    loading: true,
    currentCardCount: null
  };

  componentDidMount() {
    this.fetchCards();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.cardSortBy !== this.props.cardSortBy ||
      nextProps.cardOrderIn !== this.props.cardOrderIn ||
      nextProps.cardFilterByContent !== this.props.cardFilterByContent ||
      nextProps.cardFilterByDate !== this.props.cardFilterByDate
    ) {
      this.searchQuery.offset = 0;
      this.setState(
        () => ({ loading: true }),
        () => {
          this.fetchCards();
        }
      );
    }
  }

  searchQuery = {
    limit: 10,
    offset: 0
  };

  handleScroll = throttle(
    () => {
      if (
        !this.state.paginationQuery &&
        window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight
      ) {
        if (this.state.currentCardCount === this.searchQuery.limit) {
          this.searchQuery.offset = this.searchQuery.offset + this.searchQuery.limit;
          this.setState(
            () => ({
              paginationLoader: true,
              paginationQuery: true
            }),
            () => {
              this.fetchCards();
            }
          );
        }
      } else return null;
    },
    150,
    { leading: false }
  );

  fetchCards = () => {
    let cardTypes = [];
    let cardSubTypes = [];
    switch (this.props.cardFilterByContent) {
      case 'link':
      case 'audio':
      case 'file':
      case 'text':
      case 'video':
        cardTypes.push('media');
        cardSubTypes.push(this.props.cardFilterByContent);
        break;
      case 'Course':
        cardTypes.push('Course');
        break;
      case 'poll':
        cardTypes.push('poll');
        break;
      case 'Video_stream':
        cardTypes.push('Video_stream');
        break;
      default:
        break;
    }
    const payload = {
      channelId: this.props.channelId,
      'card_subtype[]': cardSubTypes,
      'card_type[]': cardTypes,
      sort: this.props.cardSortBy,
      order: this.props.cardOrderIn,
      from_date: this.props.cardFilterByDate.from_date,
      to_date: this.props.cardFilterByDate.to_date,
      ...this.searchQuery
    };
    getChannleCarouselData(payload, this.props.channelId)
      .then(response => {
        if (response.cards) {
          this.setState(() => ({
            filterSortcards: this.state.paginationQuery
              ? [...this.state.filterSortcards, ...response.cards]
              : response.cards,
            totalCardCount: response.total,
            loading: false,
            paginationLoader: false,
            currentCardCount: response.cards.length,
            paginationQuery: false
          }));
        }
      })
      .catch(error => {
        this.setState(() => ({ loading: false, paginationLoader: false }));
        console.error('Error in FilterAndSortCardView.fetchCards', error);
      });
  };
  render() {
    return (
      <div className="standalone-tab-container">
        <div className="cards-container">
          {!this.state.loading &&
            !!this.state.filterSortcards.length &&
            this.state.filterSortcards.map(card => (
              <div className="tab-card" key={card.id}>
                <Card
                  author={card.author && card.author}
                  card={card}
                  dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                  startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                  tooltipPosition="top-center"
                  moreCards={false}
                  withoutCardModal={true}
                  type={'Tile'}
                />
              </div>
            ))}
          {(this.state.loading || this.state.paginationLoader) && (
            <div className="make-loader-center">
              <Spinner />
            </div>
          )}
        </div>
      </div>
    );
  }
}

FilterAndSortCardView.propTypes = {
  cardSortBy: PropTypes.string,
  cardOrderIn: PropTypes.string,
  cardFilterByDate: PropTypes.object,
  cardFilterByContent: PropTypes.array,
  channelId: PropTypes.number
};
