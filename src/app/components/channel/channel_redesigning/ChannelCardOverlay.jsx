import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import PinIcon from 'edc-web-sdk/components/icons/Pin';
import UnpinIcon from 'edc-web-sdk/components/icons/Unpin';
import DeleteIcon from 'edc-web-sdk/components/icons/Delete';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { channelReorder, pinCard, unpinCard } from 'edc-web-sdk/requests/channels.v2';
import { removeChannelCardv2 } from '../../../actions/channelsActionsV2';
import {
  addFeaturedCardInChannel,
  removeFeaturedCardInChannel,
  removeCardFromChannelCarousel
} from '../../../actions/channelsActionsV3';
import { Permissions } from '../../../utils/checkPermissions';

class ChannelCardOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinned: props.pinnedStatus || false,
      disableButton: true,
      isReorderingActivated: window.ldclient.variation('reorder-content', false),
      confirmation: false,
      deletingCardStatus: false,
      rank: props.card && props.card.rank,
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      iconStyle: {
        width: '1.875rem',
        height: '1.875rem'
      }
    };
    this.constants = {
      maxFeaturedCardNote: tr('You can pin max 10 cards!'),
      enterOrderText: tr('Enter Order'),
      okText: tr('OK'),
      topOfListText: tr('Top of List'),
      markFeaturedText: tr('Featured'),
      markUnfeaturedText: tr('Un-featured'),
      removeText: tr('Remove'),
      deletingText: tr('DELETING...'),
      deleteConfirmationNoteFirstPart: tr(`Do you really want to remove selected card from`),
      deleteConfirmationNoteSecondPart: tr('channel?'),
      cancelText: tr('CANCEL')
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card && nextProps.card.pinnedStatus !== this.props.card.pinnedStatus) {
      this.setState({ pinned: nextProps.card.pinnedStatus });
    }
  }

  pinUnpinCard = () => {
    if (this.props.pinnedCount == 10 && !this.state.pinned) {
      this.props.dispatch(openSnackBar(this.constants.maxFeaturedCardNote, true));
      return;
    }
    let payload = {
      pinnable_id: this.props.channel.id,
      pinnable_type: 'Channel',
      object_id: this.props.card.id,
      object_type: 'Card'
    };
    if (!this.state.pinned) {
      pinCard(payload)
        .then(response => {
          this.props.dispatch(addFeaturedCardInChannel(this.props.card));
        })
        .catch(error => {
          console.error(`error in ChannelCardsOverlay.pinUnpinCard ${error}`);
        });
    } else {
      unpinCard(payload)
        .then(response => {
          this.props.dispatch(removeFeaturedCardInChannel(this.props.card));
        })
        .catch(error => {
          console.error(`error in ChannelCardsOverlay.pinUnpinCard ${error}`);
        });
    }
    this.setState({ pinned: !this.state.pinned });
  };

  removeCardOrCancelHandler = () => {
    this.setState({ confirmation: !this.state.confirmation });
  };

  reorderInputValidation = e => {
    let rank = e.target.value;
    if (rank >= 1 && rank <= this.props.allCardsCount) {
      this.setState(() => ({ disableButton: false, rank }));
    } else {
      this.setState(() => ({ disableButton: true, rank }));
    }
  };

  reorderByRank = () => {
    channelReorder(this.props.channel.id, this.props.card.id, { rank: this.state.rank })
      .then(() => {
        this.props.channelReOrderCallback();
      })
      .catch(err => {
        console.error(`Error in ChannelCardOverlay.channelReorder.func default : ${err}`);
      });
  };

  reorderToTop = () => {
    channelReorder(this.props.channel.id, this.props.card.id, { rank: 1 })
      .then(() => {
        this.props.channelReOrderCallback();
      })
      .catch(err => {
        console.error(`Error in ChannelCardOverlay.channelReorder.func top : ${err}`);
      });
  };

  deleteCard = () => {
    const payload = {
      cardId: this.props.card.id,
      carouselTitle: this.props.carouselCardsInfo.carouselTitle
    };
    let card = this.props.card;
    let channel = this.props.channel;
    this.setState(() => ({ deletingCardStatus: true }));
    this.props
      .dispatch(removeChannelCardv2(channel.id, card, this.state.pinned))
      .then(() => {
        this.props.dispatch(removeCardFromChannelCarousel(payload));
        this.props.deleteCard(card.id);
        this.removeCardOrCancelHandler();
        this.setState(() => ({ deletingCardStatus: false }));
      })
      .catch(err => {
        console.error(`Error in ChannelCardOverlay.deleteCard.func: ${err}`);
      });
  };

  render() {
    let isSingleCard = this.props.allCardsCount === 1 ? true : false;
    return (
      <div className="card-overlay-layer card-overlay-layer-v2">
        {!this.state.confirmation ? (
          <div>
            {!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard && (
              <div className="row action-icons-row">
                <div className="small-6 columns">
                  <div className="card-overlay-text">{this.constants.enterOrderText}</div>
                </div>
                <div className="small-6 columns input-block-style">
                  <input
                    value={this.state.rank}
                    type="text"
                    onChange={e => this.reorderInputValidation(e)}
                    className="channel-rank-input"
                    required
                  />
                  <button
                    className="channel-rank-submit"
                    onClick={this.reorderByRank}
                    disabled={this.state.disableButton}
                  >
                    {this.constants.okText}
                  </button>
                </div>
                <div className="small-12 columns">
                  <div className="card-overlay-text">
                    <span name="top" className="pointer" onTouchTap={this.reorderToTop}>
                      {this.constants.topOfListText}
                    </span>
                  </div>
                </div>
              </div>
            )}
            <div
              className={
                'row action-icons-row ' +
                (!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard
                  ? 'not-custom-carousel'
                  : 'no-reordering')
              }
            >
              <div className="small-12 columns icon-wrapper">
                {!this.state.pinned && !this.props.autoPinCards && (
                  <div className="icon-overlay-wrapper-v3">
                    <IconButton className="specific-icon-wrapper" onTouchTap={this.pinUnpinCard}>
                      <PinIcon customStyle={this.styles.iconStyle} />
                    </IconButton>
                    <div className="text-center icon-label">{this.constants.markFeaturedText}</div>
                  </div>
                )}
                {this.state.pinned && !this.props.autoPinCards && (
                  <div className="icon-overlay-wrapper-v3">
                    <IconButton
                      className="specific-icon-wrapper unpin"
                      onTouchTap={this.pinUnpinCard}
                    >
                      <UnpinIcon customStyle={this.styles.iconStyle} />
                    </IconButton>
                    <div className="text-center icon-label">
                      {this.constants.markUnfeaturedText}
                    </div>
                  </div>
                )}
                <div className="icon-overlay-wrapper-v3 delete text-center">
                  <IconButton
                    className="specific-icon-wrapper delete-icon"
                    onTouchTap={this.removeCardOrCancelHandler}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <div className="text-center delete icon-label">{this.constants.removeText}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div>
            <div className="card-overlay-confirmation">
              <p className="card-overlay-text text-center">
                {`${this.constants.deleteConfirmationNoteFirstPart} ${this.props.channel.label} ${
                  this.constants.deleteConfirmationNoteSecondPart
                }`}
              </p>
              <div className="confirmation-buttons-container">
                <button
                  className="confirmation-buttons primary"
                  onClick={this.removeCardOrCancelHandler}
                >
                  {this.constants.cancelText}
                </button>
                <button className="confirmation-buttons secondary" onClick={this.deleteCard}>
                  {this.state.deletingCardStatus
                    ? this.constants.deletingText
                    : this.constants.okText}
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelCardOverlay.propTypes = {
  card: PropTypes.object,
  allCardsCount: PropTypes.number,
  removeCardLaunchModal: PropTypes.func,
  pinUnpinCard: PropTypes.func,
  pinnedCount: PropTypes.number,
  channel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  pinnedStatus: PropTypes.bool,
  channelReOrderCallback: PropTypes.func,
  reorderToggle: PropTypes.func,
  setOffset: PropTypes.func,
  deleteCard: PropTypes.func,
  autoPinCards: PropTypes.bool,
  carouselCardsInfo: PropTypes.object
};

export default connect()(ChannelCardOverlay);
