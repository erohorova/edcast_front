import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { tr } from 'edc-web-sdk/helpers/translations';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import { openChannelEditModal, openChannelCurateCardModal } from '../../../actions/modalActions';
import { getNonCurateCards } from '../../../actions/channelsActionsV2';
import { follow, unfollow } from 'edc-web-sdk/requests/channels.v2';
import TooltipLabel from '../../common/TooltipLabel';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import LockIcon from 'edc-web-sdk/components/icons/LockIcon';

const CuratorBox = props => {
  const currentUserHandle = props.currentUserHandle;

  return (
    <a
      href={`/${
        (props.curator.handle || props.curator.slug).replace('@', '') === currentUserHandle
          ? 'me'
          : props.curator.handle || props.curator.slug
      }`}
      key={props.curator.id}
    >
      <div className="curator-row">
        <div className="curator-dp">
          <img src={props.curator.avatarimages.tiny} />
        </div>
        <div className="curator-name">
          <p>
            {props.curator.firstName} {props.curator.lastName}
          </p>
        </div>
      </div>
    </a>
  );
};

CuratorBox.propTypes = {
  curator: PropTypes.object,
  currentUserHandle: PropTypes.string
};

class ChannelBanner extends Component {
  constant = {
    whiteColor: '#FFFFFF',
    unfolloingLabel: tr('UNFOLLOWING...'),
    followingLable: tr('FOLLOWING'),
    followingProgressLabel: tr('FOLLOWING...'),
    followLabel: tr('FOLLOW'),
    unfollowLabel: tr('UNFOLLOW'),
    viewMore: tr('view more'),
    privateChannel: tr('Private Channel')
  };

  state = {
    isCuratorDropDown: false,
    isSetting: false,
    followingLable: this.constant.followingLable,
    followLabel: this.constant.followLabel,
    isFollowing: (this.props.channel && this.props.channel.isFollowing) || false,
    isCurate: false,
    openChannel: window.ldclient.variation('open-channel-enabled', false),
    isShowFullDescription: false
  };

  styles = {
    backIconStyle: {
      width: '1.187rem'
    },
    anchorOriginStyle: {
      horizontal: 'right',
      vertical: 'bottom'
    },
    targetOriginStyle: {
      horizontal: 'right',
      vertical: 'top'
    },
    bgBlack: {
      backgroundColor: '#000000'
    },
    closeIcon: {
      width: '1.25rem',
      height: '1.25rem'
    },
    closeIconPosition: {
      position: 'absolute',
      top: '-0.125rem',
      right: '0'
    },
    tooltipStyle: {
      top: '1.25rem',
      zIndex: '1000'
    },
    lockIconStyle: {
      width: '1.125rem',
      height: '1.125rem'
    }
  };

  componentDidMount() {}

  openCuratorDropDown = () => {
    this.setState(() => ({ isCuratorDropDown: true }));
  };

  requestCloseHandler = () => {
    if (this.state.isSetting) {
      this.setState(() => ({ isSetting: false }));
    } else if (this.state.isShowFullDescription) {
      this.setState(() => ({ isShowFullDescription: false }));
    } else {
      this.setState(() => ({ isCuratorDropDown: false }));
    }
  };

  openChannelEditModal = () => {
    this.setState(
      () => ({ isSetting: false }),
      () => {
        this.props.dispatch(push(`/channel/edit/${this.props.channel.slug}`));
      }
    );
  };

  openCurateModal = () => {
    this.props.dispatch(openChannelCurateCardModal(this.props.channel.id));
  };

  followChannel = () => {
    this.setState(() => ({ followLabel: this.constant.followingProgressLabel }));
    follow(this.props.channel.id)
      .then(() => {
        this.setState(() => ({ isFollowing: true, followLabel: this.constant.followLabel }));
      })
      .catch(error => {
        console.error('error in ChannelBanner.followChannel', error);
      });
  };

  unFollowChannel = () => {
    this.setState(() => ({ followingLable: this.constant.unfolloingLabel }));
    unfollow(this.props.channel.id)
      .then(() => {
        this.setState(() => ({ isFollowing: false, followingLable: this.constant.followingLable }));
      })
      .catch(error => {
        console.error('error in ChannelBanner.followChannel', error);
      });
  };

  render() {
    const {
      curators,
      description,
      label,
      isPrivate,
      bannerImageUrl,
      isOwner,
      allowFollow,
      profileImageUrl,
      provider,
      providerImage
    } = this.props.channel;
    return (
      <div className="channel-banner">
        <div className="banner-container">
          <div className="cb-right-block">
            <div className="arrow-container" onClick={this.props.goBackFromChannel}>
              <BackIcon style={this.styles.backIconStyle} color={this.constant.whiteColor} />
            </div>
            <div className="channel-info-container">
              <div className="channel-label">
                <div className="tooltip">
                  <p>{label.length > 42 ? `${label.slice(0, 39)}...` : label}</p>
                  <span className="tooltiptext">{label}</span>
                </div>
                {isPrivate && this.props.teamShowLockIcon && (
                <div className="lock-container">
                  <div className="tooltip">
                    <LockIcon style={this.styles.lockIconStyle} />
                    <span className="tooltiptext">{this.constant.privateChannel}</span>
                  </div>
                </div>
                )}
              </div>
              <div className="channel-description">
                <p>
                  {description.length > 146 ? `${description.slice(0, 136)}...` : description}
                  {description.length > 146 && (
                    <span
                      onClick={() => this.setState(() => ({ isShowFullDescription: true }))}
                      ref={instance => (this.descriptionTarget = instance)}
                    >
                      {this.constant.viewMore}
                    </span>
                  )}
                </p>
              </div>
              <div className="curate-button-provider-container">
                {!this.props.isCurator && !this.state.isFollowing && allowFollow && (
                  <button className="curate-btn" onClick={this.followChannel}>
                    {tr(this.state.followLabel)}
                  </button>
                )}
                {!allowFollow && <button className="curate-btn">{tr('Coming Soon')}</button>}
                {!this.props.isCurator && this.state.isFollowing && (
                  <button
                    className="curate-btn"
                    onMouseEnter={() =>
                      this.setState(() => ({ followingLable: this.constant.unfollowLabel }))
                    }
                    onMouseLeave={() =>
                      this.setState(() => ({
                        followingLable:
                          this.state.followingLable !== this.constant.unfolloingLabel
                            ? this.constant.followingLable
                            : this.constant.unfolloingLabel
                      }))
                    }
                    onClick={this.unFollowChannel}
                  >
                    {tr(this.state.followingLable)}
                  </button>
                )}
                {this.props.isCurator && allowFollow && (
                  <button className="curate-btn" onClick={this.openCurateModal}>
                    {tr('CURATE')}
                  </button>
                )}
                <div className="provider-container">
                  {((providerImage && !!providerImage.length) ||
                    (provider && !!provider.length)) && <label>{tr('Provider')} : </label>}
                  {providerImage && !!providerImage.length && <img src={providerImage} />}
                  {provider && !!provider.length && (
                    <div className="tooltip">
                      <p>{provider.length > 52 ? `${provider.slice(0, 49)}...` : provider}</p>
                      <span className="tooltiptext">{provider}</span>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="cb-left-block">
            <div
              className="banner-image-block"
              style={{
                backgroundImage: `url(${profileImageUrl ? profileImageUrl : bannerImageUrl})`
              }}
            >
              <div className="image-gradient" />
            </div>
            <div className="channel-info-box">
              {(this.props.isCurator || isOwner) && (
                <div
                  className="vertical-menu"
                  ref={instance => (this.settingtarget = instance)}
                  onClick={() => this.setState(() => ({ isSetting: true }))}
                >
                  <div className="dot" />
                  <div className="dot" />
                  <div className="dot" />
                </div>
              )}
              {curators && !!curators.length && (
                <div className="curator-box">
                  <p className="curator-label">{tr('Curator :')}</p>
                  {curators.slice(0, 2).map(curator => (
                    <CuratorBox
                      curator={curator}
                      key={curator.id}
                      currentUserHandle={this.props.currentUserHandle}
                    />
                  ))}
                  {curators.length > 2 && (
                    <p className="and-other-text">
                      {tr('and ')}
                      <span
                        ref={instance => (this.curatorPopoverTarget = instance)}
                        onClick={this.openCuratorDropDown}
                      >
                        {curators.length - 2} {tr(`${curators.length === 3 ? 'other' : 'others'}`)}
                      </span>
                    </p>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>
        <Popover
          open={this.state.isCuratorDropDown}
          anchorEl={this.curatorPopoverTarget}
          onRequestClose={this.requestCloseHandler}
          anchorOrigin={this.styles.anchorOriginStyle}
          targetOrigin={this.styles.targetOriginStyle}
          style={this.styles.bgBlack}
        >
          <div className="popover-curator-list">
            {curators.slice(2, curators.length).map(curator => (
              <CuratorBox
                curator={curator}
                key={curator.id}
                currentUserHandle={this.props.currentUserHandle}
              />
            ))}
          </div>
        </Popover>
        <Popover
          open={this.state.isSetting}
          anchorEl={this.settingtarget}
          onRequestClose={this.requestCloseHandler}
          anchorOrigin={this.styles.anchorOriginStyle}
          targetOrigin={this.styles.targetOriginStyle}
        >
          <div className="setting-edit-dropdown">
            <p onClick={this.openChannelEditModal}>{tr('Edit')}</p>
            <a href={`${document.location.href}/analytics`}>{tr('Analytics')}</a>
          </div>
        </Popover>
        <Popover
          open={this.state.isShowFullDescription}
          anchorEl={this.descriptionTarget}
          onRequestClose={this.requestCloseHandler}
          animation={PopoverAnimationVertical}
        >
          <div className="channel-description-popover">
            <IconButton
              onTouchTap={this.requestCloseHandler}
              iconStyle={this.styles.closeIcon}
              style={this.styles.closeIconPosition}
            >
              <CloseIcon color="#acadc1" />
            </IconButton>
            <h5 className="channel-popover-header">{tr('Description')}</h5>
            <p style={this.styles.fullDescription}>{description}</p>
          </div>
        </Popover>
      </div>
    );
  }
}

ChannelBanner.propTypes = {
  channel: PropTypes.object,
  goBackFromChannel: PropTypes.func,
  currentUserId: PropTypes.string,
  teamShowLockIcon: PropTypes.bool,
  isCurator: PropTypes.bool,
  currentUserHandle: PropTypes.string
};

// todo...
const mapStateToProps = ({ currentUser, team }) => {
  return {
    currentUserId: currentUser.get('id'),
    currentUserHandle: currentUser.get('handle'),
    teamShowLockIcon: team.get('config').show_lock_icon
  };
};

export default connect(mapStateToProps)(ChannelBanner);
