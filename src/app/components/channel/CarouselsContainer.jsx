import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChannelCarousel from './ChannelCarousel';
import { tr } from 'edc-web-sdk/helpers/translations';

class CarouselsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showJourney: window.ldclient.variation('journey', false),
      channelCustomCarousels: window.ldclient.variation('channel-custom-carousels', false)
    };
  }

  listChannel = obj => {
    let ChannelController = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        ChannelController.push(listObj);
      });
      ChannelController.sort((a, b) => a.index - b.index);
    }
    return ChannelController;
  };

  render() {
    let automatedPinnedCard = this.props.channel.autoPinCards;
    if (
      this.state.channelCustomCarousels &&
      this.props.channel.carousels &&
      this.props.channel.carousels.length
    ) {
      let ChannelController = this.listChannel(this.props.channel.carousels);
      return (
        <div>
          {ChannelController.filter(item => item.visible).map(item => {
            if (item.custom_carousel) {
              let customCarousel =
                this.props.carousel &&
                this.props.carousel.carousels &&
                this.props.carousel.carousels.find(carouselItem => {
                  return carouselItem.id === item.id;
                });
              if (customCarousel && customCarousel.items && customCarousel.items.length) {
                return (
                  <div
                    key={`carousal-${customCarousel.id}`}
                    id={`custom-carousal-${customCarousel.id}`}
                  >
                    <ChannelCarousel
                      isChannelInfoCarousel="true"
                      description={tr(customCarousel.displayName)}
                      items={customCarousel.items}
                      count={customCarousel.items.length}
                      channel={this.props.channel}
                      editable={this.props.carouselEditable}
                      type="custom_carousel"
                    />
                  </div>
                );
              }
            } else {
              switch (item.default_label) {
                case 'Pathways':
                  if (
                    this.props.channel &&
                    this.props.channel.pack &&
                    !!this.props.channel.pack.length
                  ) {
                    return (
                      <div key={`carousal-${item.default_label}`} id="pathway-carousal">
                        <ChannelCarousel
                          isChannelInfoCarousel="true"
                          description={tr('Pathways')}
                          items={this.props.channel.pack}
                          count={this.props.channel.publishedPathwaysCount}
                          channel={this.props.channel}
                          editable={this.props.carouselEditable && !automatedPinnedCard}
                          type="pack"
                        />
                      </div>
                    );
                  }
                  break;
                case 'Journeys':
                  if (
                    this.props.channel &&
                    this.props.channel.journey &&
                    !!this.props.channel.journey.length &&
                    this.state.showJourney
                  ) {
                    return (
                      <div key={`carousal-${item.default_label}`} id="journey-carousal">
                        <ChannelCarousel
                          isChannelInfoCarousel="true"
                          description={tr('Journeys')}
                          items={this.props.channel.journey}
                          count={this.props.channel.publishedJourneysCount}
                          channel={this.props.channel}
                          editable={this.props.carouselEditable && !automatedPinnedCard}
                          type="journey"
                        />
                      </div>
                    );
                  }
                  break;
                case 'Streams':
                  if (
                    this.props.channel &&
                    this.props.channel.video_stream &&
                    !!this.props.channel.video_stream.length
                  ) {
                    return (
                      <div key={`carousal-${item.default_label}`} id="stream-carousal">
                        <ChannelCarousel
                          isChannelInfoCarousel="true"
                          description={tr('Streams')}
                          items={this.props.channel.video_stream}
                          count={this.props.channel.videoStreamsCount}
                          channel={this.props.channel}
                          editable={this.props.carouselEditable && !automatedPinnedCard}
                          type="video_stream"
                        />
                      </div>
                    );
                  }
                  break;
                //added this case to support carousels mistakenly named with Russian letter 'С'
                case 'Сourses':
                case 'Courses':
                  if (
                    this.props.channel &&
                    this.props.channel.course &&
                    !!this.props.channel.course.length
                  ) {
                    return (
                      <div key={`carousal-${item.default_label}`} id="courses-carousal">
                        <ChannelCarousel
                          isChannelInfoCarousel="true"
                          description={tr('Сourses')}
                          items={this.props.channel.course}
                          count={this.props.channel.coursesCount}
                          channel={this.props.channel}
                          editable={this.props.carouselEditable && !automatedPinnedCard}
                          type="course"
                        />
                      </div>
                    );
                  }
                  break;
                case 'SmartCards':
                  if (
                    this.props.channel &&
                    this.props.channel.smartbite &&
                    !!this.props.channel.smartbite.length
                  ) {
                    return (
                      <div key={`carousal-${item.default_label}`} id="smartbites-carousal">
                        <ChannelCarousel
                          isChannelInfoCarousel="true"
                          description={tr('SmartCards')}
                          items={this.props.channel.smartbite}
                          count={this.props.channel.smartbitesCount}
                          channel={this.props.channel}
                          editable={this.props.carouselEditable && !automatedPinnedCard}
                          type="smartbite"
                        />
                      </div>
                    );
                  }
                  break;
                default:
                  break;
              }
            }
          })}
        </div>
      );
    } else {
      return (
        <div>
          {this.props.channel &&
            this.props.channel.video_stream &&
            !!this.props.channel.video_stream.length && (
              <div id="stream-carousal">
                <ChannelCarousel
                  isChannelInfoCarousel="true"
                  description={tr('Streams')}
                  items={this.props.channel.video_stream}
                  count={this.props.channel.videoStreamsCount}
                  channel={this.props.channel}
                  editable={this.props.carouselEditable && !automatedPinnedCard}
                  type="video_stream"
                />
              </div>
            )}
          {this.props.channel && this.props.channel.pack && !!this.props.channel.pack.length && (
            <div id="pathway-carousal">
              <ChannelCarousel
                isChannelInfoCarousel="true"
                description={tr('Pathways')}
                items={this.props.channel.pack}
                count={this.props.channel.publishedPathwaysCount}
                channel={this.props.channel}
                editable={this.props.carouselEditable && !automatedPinnedCard}
                type="pack"
              />
            </div>
          )}
          {this.props.channel &&
            this.props.channel.smartbite &&
            !!this.props.channel.smartbite.length && (
              <div id="smartbites-carousal">
                <ChannelCarousel
                  isChannelInfoCarousel="true"
                  description={tr('SmartCards')}
                  items={this.props.channel.smartbite}
                  count={this.props.channel.smartbitesCount}
                  channel={this.props.channel}
                  editable={this.props.carouselEditable && !automatedPinnedCard}
                  type="smartbite"
                />
              </div>
            )}
          {this.props.channel && this.props.channel.course && !!this.props.channel.course.length && (
            <div id="courses-carousal">
              <ChannelCarousel
                isChannelInfoCarousel="true"
                description={tr('Сourses')}
                items={this.props.channel.course}
                count={this.props.channel.coursesCount}
                channel={this.props.channel}
                editable={this.props.carouselEditable && !automatedPinnedCard}
                type="course"
              />
            </div>
          )}
          {this.props.channel &&
            this.props.channel.journey &&
            !!this.props.channel.journey.length &&
            this.state.showJourney && (
              <div id="journey-carousal">
                <ChannelCarousel
                  isChannelInfoCarousel="true"
                  description={tr('Journeys')}
                  items={this.props.channel.journey}
                  count={this.props.channel.publishedJourneysCount}
                  channel={this.props.channel}
                  editable={this.props.carouselEditable && !automatedPinnedCard}
                  type="journey"
                />
              </div>
            )}
        </div>
      );
    }
  }
}

CarouselsContainer.propTypes = {
  channel: PropTypes.object,
  carouselEditable: PropTypes.bool,
  carousel: PropTypes.object
};

export default connect(state => ({
  carousel: state.carousel.toJS()
}))(CarouselsContainer);
