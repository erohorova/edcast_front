import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChannelDetails from './ChannelDetails';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import {
  getSingleChannel,
  getSingleChannelv2,
  getTypeCardsv2,
  fetchPinnedCardsv2,
  emptyPinnedCardCarousel,
  snackBarOpenClose
} from '../../../actions/channelsActionsV2';
import ChannelPinnedCardsCarousel from './ChannelPinnedCardsCarousel';
import { tr } from 'edc-web-sdk/helpers/translations';
import CardViewToggle from '../../common/CardViewToggle';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import { getCollaborators } from 'edc-web-sdk/requests/channels';
import CarouselsContainer from './CarouselsContainer';
import Spinner from '../../common/spinner';
import Card from '../../cards/Card';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { push } from 'react-router-redux';
import { cardFields } from '../../../constants/cardFields';
// chanel search feature dependencies
import colors from 'edc-web-sdk/components/colors/index';
import SearchWithSuggestions from '../../common/SearchWithSuggestions';
import { searchCardsInChannel } from 'edc-web-sdk/requests/channels.v2';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';
import Checkbox from 'material-ui/Checkbox';
import moment from 'moment';
import findIndex from 'lodash/findIndex';
import throttle from 'lodash/throttle';
import AggregationBox from './common/AggregationBox';
import some from 'lodash/some';
import { isInternetExplorer } from '../../../utils/isInternetExplorer';
import { isEdgeBrowser } from '../../../utils/IsEdgeBrowser';
import updatePageLastVisit from '../../../utils/updatePageLastVisit';

class ChannelBase extends Component {
  constructor(props, context) {
    super(props, context);
    const currentYear = moment().year();
    const startOfTheYear = `01/01/${currentYear}`;
    const endOfTheYear = `12/31/${currentYear}`;
    this.INITIAL_STATE = {
      channel: null,
      pinnedCards: [],
      editable: false,
      isCurate: false,
      emptyStateImageLoaded: false,
      viewRegime:
        props.team.OrgConfig.cardView && props.team.OrgConfig.cardView['web/cardView/channel']
          ? props.team.OrgConfig.cardView['web/cardView/channel'].defaultValue
          : 'Tile',
      feedCardsStyle:
        window.ldclient.variation('feed-style-card-layout', false) &&
        window.ldclient.variation('channel-list-view', false),
      automatedPinnedCard: props.team.config['enable_automated_pinned_cards'] || false,
      showJourney: window.ldclient.variation('journey', false),
      openChannel: window.ldclient.variation('open-channel-enabled', false),
      channelSearch: window.ldclient.variation('channel-standalone-search', false),
      customItems: [],
      listCards: [],
      isCardV3: window.ldclient.variation('card-v3', false),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      currentSearchValue: [],
      contentTypes: [],
      postedBy: [],
      sources: [],
      dateFilter: [
        {
          id: 'last-thirty-days',
          displayName: 'Last 30 days',
          value: {
            from_date: moment()
              .subtract(30, 'days')
              .format('DD/MM/YYYY'),
            till_date: moment().format('DD/MM/YYYY')
          }
        },
        {
          id: 'last-ninety-days',
          displayName: 'Last 90 days',
          value: {
            from_date: moment()
              .subtract(90, 'days')
              .format('DD/MM/YYYY'),
            till_date: moment().format('DD/MM/YYYY')
          }
        },
        {
          id: 'current-year',
          displayName: currentYear,
          value: {
            from_date: moment(startOfTheYear).format('DD/MM/YYYY'),
            till_date: moment(endOfTheYear).format('DD/MM/YYYY')
          }
        },
        {
          id: 'last-year',
          displayName: moment()
            .subtract(1, 'year')
            .year(),
          value: {
            from_date: moment(startOfTheYear)
              .subtract(1, 'year')
              .format('DD/MM/YYYY'),
            till_date: moment(endOfTheYear)
              .subtract(1, 'year')
              .format('DD/MM/YYYY')
          }
        },
        {
          id: 'before-two-year',
          displayName: moment()
            .subtract(2, 'year')
            .year(),
          value: {
            from_date: moment(startOfTheYear)
              .subtract(2, 'year')
              .format('DD/MM/YYYY'),
            till_date: moment(endOfTheYear)
              .subtract(2, 'year')
              .format('DD/MM/YYYY')
          }
        }
      ],
      selectedSourceDisplayName: [],
      selectedContentTypes: [],
      selectedAuthorId: [],
      selectedDateRange: [],
      smartCardChecked: false,
      aggsArray: null,
      pending: false,
      pagginationQuery: false,
      currentCardCount: 20,
      smartCardTypes: [],
      showAllSources: false,
      showAllPostedBy: false,
      queryFromAggs: false,
      isLoading: false,
      suggesstedCard: [],
      showChannelBase: false,
      isRenderSearchResult: false,
      filterCards: [],
      searchQueryString: '',
      isSearching: false,
      newSearchQuery: false,
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    this.state = this.INITIAL_STATE;

    this.styles = {
      pinCards: {
        marginTop: '1rem'
      },
      middlePosition: {
        padding: '0.8125rem',
        maxWidth: '65.625rem',
        margin: 'auto'
      },
      anchorTagScroll: {
        display: 'block',
        height: '100px',
        marginTop: '-100px',
        visibility: 'hidden'
      },
      listViewContainer: {
        display: 'block',
        marginTop: '2rem',
        padding: '0 0.75rem'
      },
      searchIcon: {
        position: 'absolute',
        bottom: '0.125rem',
        verticalAlign: 'middle',
        width: '1.2rem',
        height: '1.5rem',
        right: '0.4375rem',
        fill: colors.silverSand
      },
      checkboxInner: {
        width: '0.8125rem',
        height: '0.8125rem',
        marginRight: '0.25rem'
      },
      checkboxInnerLabel: {
        fontSize: '0.75rem',
        fontWeight: '300',
        color: '#6f708b',
        lineHeight: '1'
      },
      checkboxOuter: {
        width: '1rem',
        height: '1rem',
        marginRight: '0.3125rem'
      },
      checkboxOuterLabel: {
        padding: '0.375rem 0',
        lineHeight: '1.3'
      },
      checkboxIconStyle: {
        width: '1rem',
        height: '1rem'
      },
      uncheckedIcon: {
        width: '1rem',
        height: '1rem'
      },
      uncheckedIconInner: {
        width: '0.8125rem',
        height: '0.8125rem'
      }
    };
    this.searchQuery = {
      limit: 20,
      offset: 0,
      q: ''
    };

    if (this.state.showNew) {
      updatePageLastVisit('channelLastVisit');
    }
  }

  componentDidMount() {
    this.initilizeComponent();
  }

  componentWillUnmount() {
    this.props.dispatch(emptyPinnedCardCarousel());
    if (this.state.channelSearch) {
      window.removeEventListener('scroll', this.handleScroll);
    }
    this.state.showNew &&
      localStorage.setItem('channelLastVisit', Math.round(new Date().getTime() / 1000));
  }

  initilizeComponent = () => {
    this.setState(
      () => this.INITIAL_STATE,
      async () => {
        let userInfoCallBack = await getSpecificUserInfo(
          [
            'followingChannels',
            'roles',
            'rolesDefaultNames',
            'writableChannels',
            'first_name',
            'last_name'
          ],
          this.props.currentUser
        );
        this.props.dispatch(userInfoCallBack);
        this.props
          .dispatch(getSingleChannelv2(this.props.routeParams.slug))
          .then(channelData => {
            if (channelData && channelData.status === 401) {
              this.props.dispatch(
                snackBarOpenClose('You are not authorized to access this channel.', 5000)
              );
              this.props.dispatch(push('/me/learners-dashboard'));
            }
            let setStateObj = {};
            let channel = channelData;

            setStateObj['channel'] = channel;

            this.props.dispatch({
              type: 'receive_carousels',
              carousels:
                channel &&
                channel.carousels &&
                channel.carousels.filter(carousel => carousel.custom_carousel)
            });

            let checkIfEditable = this.checkIfEditable(channel);
            setStateObj = { ...setStateObj, ...checkIfEditable };

            let checkIfCurate = this.checkIfCurate(channel);
            setStateObj = { ...setStateObj, ...checkIfCurate };

            if (Object.keys(setStateObj).length) {
              this.setState(setStateObj);
            }

            this._getCollaborators(channel.id);
            this._fetchPinnedCards(channel.id);
            recordVisit(channel.id, 'Channel');
            if (this.state.viewRegime !== 'Tile') {
              this.getListCards();
            }
            this.setState(() => ({ showChannelBase: true }));
          })
          .catch(err => {
            console.error(`Error in ChannelBase.componentDidMount.getSingleChannelv2.func: ${err}`);
          });
        if (this.state.channelSearch) {
          window.addEventListener('scroll', this.handleScroll);
        }
      }
    );
  };

  handleScroll = throttle(
    () => {
      if (
        !this.state.pending &&
        (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight &&
          !this.isLastPage)
      ) {
        // currentCardCount card count getting from letest api call
        // pending check for network request
        // suggesstedCard length more than and equal to limit then allow to calll pagination
        const condition =
          this.state.currentCardCount === this.searchQuery.limit &&
          this.state.suggesstedCard.length >= this.searchQuery.limit;
        if (condition) {
          this.searchQuery.offset += this.searchQuery.limit;
          this.setState(
            () => ({
              pagginationQuery: true,
              queryFromAggs: true,
              isRenderSearchResult: true
            }),
            () => {
              this.getSuggestedList();
            }
          );
        }
      } else return null;
    },
    150,
    { leading: false }
  );

  componentWillReceiveProps(nextProps) {
    if (nextProps.routeParams.slug !== this.props.routeParams.slug) {
      this.props.dispatch(emptyPinnedCardCarousel());
      this.initilizeComponent();
    }
    let updatedChannel = Object.values(nextProps.channels).find(
      channel =>
        channel &&
        (channel.slug === nextProps.routeParams.slug || channel.id == nextProps.routeParams.slug)
    );
    this.extractChannel(nextProps.channels, nextProps.routeParams.slug);
  }

  checkIfCurate = channel => {
    let isOpen = !!(channel && channel.isOpen);
    let isCurate = false;
    if (
      channel &&
      channel.curateOnly &&
      channel.isFollowing &&
      (this.state.openChannel ? isOpen : false)
    ) {
      isCurate = true;
    } else if (channel && channel.curateOnly && (channel.curators && channel.curators.length)) {
      for (let i = 0; i < (channel && channel.curators && channel.curators.length); i++) {
        if (channel.curators[i].id == this.props.currentUser.id) {
          isCurate = true;
        }
      }
    }
    return { isCurate };
  };

  extractChannel = (channels, slug) => {
    let channel;
    Object.keys(channels).forEach(channelId => {
      if (channels[channelId] != undefined) {
        if (channels[channelId].slug === slug || channels[channelId].id == slug) {
          channel = channels[channelId];
        }
      }
    });
    this.setState({ channel });
  };

  checkIfEditable = channel => {
    let editable = false;
    let carouselEditable = false;
    if (channel && channel.creator) {
      if (
        channel.creator.id == this.props.currentUser.id ||
        (channel.isFollowing && channel.isOpen)
      ) {
        editable = true;
        carouselEditable = true;
      }
    }
    if (channel && channel.curators && channel.curators.length) {
      for (let i = 0; i < (channel && channel.curators && channel.curators.length); i++) {
        if (channel.curators[i].id == this.props.currentUser.id) {
          editable = true;
          carouselEditable = true;
          break;
        }
      }
    }
    return { editable, carouselEditable };
  };

  changeViewMode = viewRegime => {
    if (viewRegime !== this.state.viewRegime) {
      this.setState({ viewRegime, isLoading: true }, () => {
        if (viewRegime !== 'Tile') {
          this.getListCards();
        } else {
          this.setState({ isLoading: false });
        }
      });
    }
  };

  _getCollaborators = channelId => {
    getCollaborators(channelId, { search_term: this.props.currentUser.handle })
      .then(collaboratorUsers => {
        this.setState({
          isCollaborator: !!collaboratorUsers && collaboratorUsers.length
        });
      })
      .catch(err => {
        console.error(`Error in ChannelBase.getCollaborators.func: ${err}`);
      });
  };

  _fetchPinnedCards = channelId => {
    this.props
      .dispatch(
        fetchPinnedCardsv2(
          channelId,
          'Channel',
          this.state.multilingualFiltering,
          localStorage.getItem('channelLastVisit'),
          cardFields
        )
      )
      .then(() => {})
      .catch(err => {
        this.props.dispatch({
          type: 'recieve_pinned_cards',
          channelId,
          itemType: 'Channel',
          cards: []
        });
        console.error(`Error in ChannelBase.componentDidMount.fetchPinnedCardsv2.func: ${err}`);
      });
  };

  getListCards = () => {
    this.setState({ isLoading: true }, () => {
      let cardTypes = [];
      let displayingCarouselTypes =
        this.state.channel &&
        this.state.channel.carousels.filter(item => item.visible).map(item => item.default_label);
      displayingCarouselTypes.forEach(item => {
        switch (item) {
          case 'Streams':
            cardTypes.push('video_stream');
            break;
          case 'Pathways':
            cardTypes.push('pack');
            break;
          case 'SmartCards':
            cardTypes.push('media', 'poll');
            break;
          case 'Journeys':
            cardTypes.push('journey');
            break;
          case 'Courses':
            cardTypes.push('course');
            break;
          default:
            break;
        }
      });
      getTypeCardsv2(
        this.state.channel.id,
        cardTypes,
        this.state.limit,
        this.state.offset,
        this.state.multilingualFiltering,
        localStorage.getItem('channelLastVisit'),
        cardFields
      )
        .then(data => {
          let listCards = [...this.state.listCards, ...data];
          let offset = listCards.length;
          this.setState({ isLoading: false, offset, listCards });
        })
        .catch(err => {
          console.error(`Error in ChannelBase.getListCards.getTypeCardsv2.func: ${err}`);
        });
    });
  };

  // get suggested card for search query
  getSuggestedList = value => {
    const payload = {};
    if (value !== undefined) {
      this.searchQuery.q = value.trim();
      payload['content_type[]'] = [];
      // reset filter
      this.resetFilter();
    } else {
      if (this.state.aggsArray) {
        // this manipulation required for query
        payload['content_type[]'] = this.state.selectedContentTypes.map(selectedType => {
          const index = findIndex(this.state.aggsArray, content => {
            return content.id === selectedType;
          });
          const tempObj = {};
          tempObj.card_type = this.state.aggsArray[index].card_type;
          tempObj.card_subtype = this.state.aggsArray[index].card_subtype;
          return tempObj;
        });
      } else {
        payload['content_type[]'] = [];
      }
    }
    payload.offset = this.state.queryFromAggs ? this.searchQuery.offset : 0;
    payload['author_id[]'] = this.state.queryFromAggs ? this.state.selectedAuthorId : [];
    payload['source_display_name[]'] = this.state.queryFromAggs
      ? this.state.selectedSourceDisplayName
      : [];
    payload['date_range[]'] = this.state.queryFromAggs ? this.state.selectedDateRange : [];
    payload.limit = this.searchQuery.limit;
    payload.q = this.searchQuery.q;
    this.setState(
      () => ({
        pending: true
      }),
      () => {
        searchCardsInChannel(this.state.channel.id, payload)
          .then(res => {
            if (res.cards) {
              this.setState(() => ({
                suggestions: res.cards.map(item => {
                  return { id: item.id, text: item.message };
                }),
                filterCards: this.state.pagginationQuery
                  ? [...this.state.filterCards, ...res.cards]
                  : res.cards,
                immutableSuggestedCard: res.cards,
                pending: false,
                pagginationQuery: false,
                currentCardCount: res.cards.length,
                resultQuantity: res.total
              }));
              if (res.aggs) {
                if (!this.state.queryFromAggs && this.state.isRenderSearchResult) {
                  if (res.cards.length === 0 && res.aggs.length === 0) {
                    // reset filter result
                    this.setState(() => ({
                      suggesstedCard: [],
                      postedBy: [],
                      sources: [],
                      smartCardTypes: [],
                      contentTypes: [],
                      isSearching: true
                    }));
                  } else {
                    this.generateAggregations(res);
                  }
                } else if (this.state.queryFromAggs && this.state.isRenderSearchResult) {
                  this.setState(() => ({
                    suggesstedCard: this.state.filterCards,
                    isRenderSearchResult: false
                  }));
                }
              }
            } else {
              this.setState(() => ({
                pending: false
              }));
            }
          })
          .catch(err =>
            console.error(
              'An error had occurred while running searchCardsInChannel function \n',
              err
            )
          );
      }
    );
  };

  resetFilter = () => {
    this.setState(() => ({
      selectedContentTypes: [],
      selectedDateRange: [],
      selectedSourceDisplayName: [],
      selectedAuthorId: [],
      showAllPostedBy: false,
      showAllSources: false,
      smartCardChecked: false
    }));
  };

  // generate aggrigations
  generateAggregations = response => {
    const aggsArray = response.aggs;
    const sources = [];
    const contentTypes = [];
    const smartCardTypes = [];
    const postedBy = [];
    // sort by type
    aggsArray.forEach(obj => {
      obj.display_name = obj.display_name.replace(/_/, ' ');
      switch (obj.type) {
        case 'source_display_name':
          sources.push(obj);
          break;
        case 'content_type':
          if (obj.card_type !== 'pack' && obj.card_type !== 'journey') {
            smartCardTypes.push(obj);
          } else {
            contentTypes.push(obj);
          }
          break;
        case 'author_id':
          postedBy.push(obj);
          break;
        default:
          break;
      }
    });
    const sourcesEntries = sources;
    const postedByEntries = postedBy;
    const smartCardsCont = smartCardTypes.reduce((total, obj) => total + obj.count, 0);
    this.setState(() => ({
      sources: this.state.showAllSources ? sources : sourcesEntries.slice(0, 5),
      contentTypes,
      postedBy: this.state.showAllPostedBy ? postedBy : postedByEntries.slice(0, 5),
      smartCardTypes,
      aggsArray,
      postedByEntries,
      sourcesEntries,
      smartCardsCont,
      suggesstedCard: this.state.filterCards,
      isSearching: true,
      isRenderSearchResult: false,
      resultQuantity: response.total,
      searchQueryString: this.searchQuery.q,
      newSearchQuery: false
    }));
  };

  // checkbox handler for content type filter
  handleSearchContentType = (event, isChecked) => {
    event.persist();
    const selectedContentTypes = this.state.selectedContentTypes;
    if (isChecked) {
      selectedContentTypes.push(event.target.value);
    } else {
      const index = selectedContentTypes.indexOf(event.target.value);
      selectedContentTypes.splice(index, 1);
    }
    const smartCheck = this.state.smartCardTypes.some(
      c => selectedContentTypes.indexOf(c.id) === -1
    );
    this.searchQuery.offset = 0;
    this.setState(
      () => ({
        selectedContentTypes,
        smartCardChecked: !smartCheck,
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCard: []
      }),
      () => {
        this.getSuggestedList();
      }
    );
  };

  // handle aggrigation cchek
  aggregationCheckBoxhandler = (value, filtertype) => {
    this.searchQuery.offset = 0;
    switch (filtertype) {
      case 'postedBy':
        this.setState(() => ({ selectedAuthorId: value }));
        break;
      case 'sources':
        this.setState(() => ({ selectedSourceDisplayName: value }));
        break;
      case 'year':
        const selectedDateRange = value.map(date => {
          const index = this.state.dateFilter.findIndex(content => content.id === date);
          return this.state.dateFilter[index].value;
        });
        this.setState(() => ({ selectedDateRange }));
        break;
      default:
        break;
    }
    this.setState(
      () => ({
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCard: []
      }),
      () => {
        this.getSuggestedList();
      }
    );
  };

  // handle smart card checkbox click
  handleSmartCardCheckClicked = (cards, isChecked) => {
    const selectedContentTypes = isChecked ? cards.map(c => c.id) : [];
    this.searchQuery.offset = 0;
    this.setState(
      () => ({
        selectedContentTypes,
        smartCardChecked: !this.state.smartCardChecked,
        queryFromAggs: true,
        isRenderSearchResult: true,
        suggesstedCard: []
      }),
      () => {
        this.getSuggestedList();
      }
    );
  };

  // showing query result
  showFilterResult = value => {
    this.searchQuery.offset = 0;
    this.resetFilter();
    this.setState(
      () => ({
        isRenderSearchResult: true,
        queryFromAggs: false,
        searchQueryString: this.searchQuery.q,
        newSearchQuery: true
      }),
      () => {
        this.getSuggestedList(value);
      }
    );
  };

  // hide search
  backToCarouselView = () => {
    this.setState({ isSearching: false, searchQueryString: '' });
  };

  // auto scroll
  autoScroll = (e, id) => {
    e.preventDefault();
    this.setState({ isSearching: false, searchQueryString: '' }, () => {
      if (document.getElementById(id)) {
        if (window.pageYOffset < 110) {
          window.scrollBy({
            top: 40,
            left: 0,
            behavior: 'smooth'
          });
        }
        setTimeout(() => {
          let scrollTop = document.getElementById(id).getBoundingClientRect().top;
          let scrollToValue = scrollTop > 100 ? scrollTop - 100 : scrollTop;
          if (isInternetExplorer() || isEdgeBrowser()) {
            window.scrollTo(0, scrollToValue - 30);
          } else {
            window.scrollBy({
              top: scrollToValue, // could be negative value
              left: 0,
              behavior: 'smooth'
            });
          }
        }, 115);
      }
    });
  };

  render() {
    let isAllow =
      this.state.channel &&
      (this.state.channel.allowFollow ||
        this.state.channel.isCurate ||
        this.state.isCollaborator ||
        this.state.channel.isOwner ||
        this.props.currentUser.isAdmin);
    const checkBoxColor = '#6f708b';
    return (
      <div>
        {!this.state.showChannelBase && (
          <div className="channel-base">
            <div className="text-center">
              <Spinner />
            </div>
          </div>
        )}
        {this.state.showChannelBase && (
          <div
            className={`channel-base ${this.state.isSearching ? 'channel-base__card-search' : ''}`}
          >
            <ChannelDetails
              channel={this.state.channel}
              editable={this.state.editable}
              isCurate={this.state.isCurate}
              isChannelSearching={this.state.isSearching}
              backToCarouselView={this.backToCarouselView}
              autoScroll={this.autoScroll}
            />
            {isAllow && (
              <div className="card-view-channel card-view-channel__with-search">
                <div className="card-view-channel-search-info-row">
                  <div className="row container-spacing">
                    <div className="small-4 columns padding-left-zero">
                      {this.state.isSearching &&
                        (this.state.suggesstedCard.length > 0 || this.state.queryFromAggs) && (
                          <div className="refine-search">{tr('Refine Search')}</div>
                        )}
                    </div>
                    <div className="small-4 columns">
                      {!this.state.isLoading &&
                        this.state.isSearching &&
                        this.state.suggesstedCard.length > 0 && (
                          <div className="search-results-info">
                            {tr('About ')}
                            <b>{this.state.resultQuantity}</b>
                            {tr(' results for')} "
                            <b className="italic">
                              {(this.state.searchQueryString &&
                                (this.state.searchQueryString.length > 50
                                  ? `${this.state.searchQueryString.slice(0, 50)}...`
                                  : this.state.searchQueryString)) ||
                                tr('all')}
                            </b>
                            "
                          </div>
                        )}
                    </div>
                    <div className="small-4 columns padding-right-zero">
                      <div className="make-filter-align">
                        {this.state.channelSearch && (
                          <SearchWithSuggestions
                            suggestionList={this.state.suggestions}
                            getSuggestList={this.getSuggestedList}
                            currentSearchValue={(this.searchQuery && this.searchQuery.q) || ''}
                            showFilterResult={this.showFilterResult}
                          />
                        )}
                        {this.state.feedCardsStyle && (
                          <CardViewToggle
                            viewRegime={this.state.viewRegime}
                            changeViewMode={this.changeViewMode}
                          />
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {isAllow && !this.state.isSearching && (
              <ChannelPinnedCardsCarousel
                channel={this.state.channel}
                editable={this.state.editable && !this.state.automatedPinnedCard}
              />
            )}
            {isAllow && !this.state.isSearching && (
              <div>
                {!this.state.feedCardsStyle || this.state.viewRegime === 'Tile' ? (
                  <div>
                    {this.state.channel && (
                      <CarouselsContainer
                        channel={this.state.channel}
                        carouselEditable={this.state.carouselEditable}
                        automatedPinnedCard={this.state.automatedPinnedCard}
                      />
                    )}
                  </div>
                ) : (
                  <div
                    style={this.styles.listViewContainer}
                    className={`row ${this.state.isCardV3 ? 'vertical-spacing-medium' : ''}`}
                  >
                    {this.state.channel &&
                      this.state.listCards.map((card, index) => {
                        return (
                          <Card
                            card={card}
                            index={index + 1}
                            fullView={true}
                            dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                            startDate={
                              card.startDate || (card.assignment && card.assignment.startDate)
                            }
                            author={card.author}
                            user={this.props.currentUser}
                            isMarkAsCompleteDisabled={true}
                            withoutCardModal={true}
                            key={index}
                            type="List"
                            removeCardFromList={this.removeCardFromList}
                          />
                        );
                      })}
                    <div className="text-center">
                      {this.state.isLoading ? (
                        <Spinner />
                      ) : (
                        <SecondaryButton
                          label={tr('View More')}
                          className="view-more"
                          onTouchTap={this.getListCards}
                        />
                      )}
                    </div>
                  </div>
                )}
              </div>
            )}
            {this.state.isSearching && !this.state.newSearchQuery && (
              <div className="row container-spacing">
                <div className="small-4 columns">
                  <div className="row">
                    <div className="my-team-filter-block">
                      {(this.state.contentTypes.length > 0 ||
                        this.state.smartCardTypes.length > 0) && (
                        <div className="filter-block">
                          <div className="filter-title">{tr('Content Type')}</div>
                          <div className="filter-content">
                            {this.state.smartCardTypes.length > 0 && (
                              <div className="outer-checkbox">
                                <Checkbox
                                  label={`SmartCard (${this.state.smartCardsCont})`}
                                  checkedIcon={<CheckOn1 color={checkBoxColor} />}
                                  uncheckedIcon={
                                    <CheckOff
                                      style={this.styles.uncheckedIcon}
                                      color={checkBoxColor}
                                    />
                                  }
                                  iconStyle={this.styles.checkboxOuter}
                                  labelStyle={this.styles.checkboxOuterLabel}
                                  onCheck={(e, isChecked) =>
                                    this.handleSmartCardCheckClicked(
                                      this.state.smartCardTypes,
                                      isChecked
                                    )
                                  }
                                  value={this.state.smartCardTypes}
                                  checked={this.state.smartCardChecked}
                                />
                              </div>
                            )}
                            {this.state.smartCardTypes.length > 0 &&
                              this.state.smartCardTypes.map(content => (
                                <div className="inner-checkbox" key={content.id}>
                                  <Checkbox
                                    label={`${content.display_name} (${content.count})`}
                                    checkedIcon={<CheckOn1 color={checkBoxColor} />}
                                    uncheckedIcon={
                                      <CheckOff
                                        style={this.styles.uncheckedIconInner}
                                        color={checkBoxColor}
                                      />
                                    }
                                    iconStyle={this.styles.checkboxInner}
                                    labelStyle={this.styles.checkboxInnerLabel}
                                    onCheck={this.handleSearchContentType}
                                    value={content.id}
                                    checked={
                                      this.state.selectedContentTypes.indexOf(content.id) !== -1
                                    }
                                  />
                                </div>
                              ))}
                            {this.state.contentTypes.map(content => (
                              <div className="outer-checkbox" key={content.id}>
                                <Checkbox
                                  label={`${content.display_name} (${content.count})`}
                                  checkedIcon={<CheckOn1 color={checkBoxColor} />}
                                  uncheckedIcon={
                                    <CheckOff
                                      style={this.styles.uncheckedIcon}
                                      color={checkBoxColor}
                                    />
                                  }
                                  iconStyle={this.styles.checkboxOuter}
                                  labelStyle={this.styles.checkboxOuterLabel}
                                  onCheck={this.handleSearchContentType}
                                  value={content.id}
                                  checked={
                                    this.state.selectedContentTypes.indexOf(content.id) !== -1
                                  }
                                />
                              </div>
                            ))}
                          </div>
                        </div>
                      )}
                      {this.state.postedBy.length > 0 && (
                        <AggregationBox
                          contentArray={this.state.postedBy}
                          aggregationHandler={this.aggregationCheckBoxhandler}
                          aggsLable={tr('Posted By')}
                          showViewMore={
                            this.state.postedByEntries.length > 5 && !this.state.showAllPostedBy
                          }
                          showMoreHandler={() =>
                            this.setState({
                              postedBy: this.state.postedByEntries,
                              showAllPostedBy: !this.state.showAllPostedBy
                            })
                          }
                          filterType={'postedBy'}
                        />
                      )}
                      {this.state.sources.length > 0 && (
                        <AggregationBox
                          contentArray={this.state.sources}
                          aggregationHandler={this.aggregationCheckBoxhandler}
                          aggsLable={tr('Sources')}
                          showViewMore={
                            this.state.sourcesEntries.length > 5 && !this.state.showAllSources
                          }
                          showMoreHandler={() =>
                            this.setState({
                              sources: this.state.sourcesEntries,
                              showAllSources: !this.state.showAllSources
                            })
                          }
                          filterType={'sources'}
                        />
                      )}
                      {(this.state.suggesstedCard.length !== 0 || this.state.queryFromAggs) && (
                        <AggregationBox
                          contentArray={this.state.dateFilter}
                          aggregationHandler={this.aggregationCheckBoxhandler}
                          aggsLable={tr('Year')}
                          showViewMore={false}
                          filterType={'year'}
                        />
                      )}
                    </div>
                  </div>
                </div>
                <div className="small-8 columns padding-right-zero">
                  <div className="custom-card-container">
                    <div className="two-card-column">
                      {this.state.suggesstedCard &&
                        this.state.suggesstedCard.length > 0 &&
                        this.state.suggesstedCard.map((card, index) => (
                          <div
                            key={card.id}
                            className={
                              this.state.isCardV3
                                ? this.state.viewRegime === 'Tile'
                                  ? 'card-v3-tile-space'
                                  : 'card-v3-list-space'
                                : this.state.viewRegime === 'Tile'
                                ? 'card-tile-space'
                                : 'card-list-space'
                            }
                          >
                            <Card
                              card={card}
                              index={index + 1}
                              fullView={true}
                              dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                              startDate={
                                card.startDate || (card.assignment && card.assignment.startDate)
                              }
                              author={card.author}
                              user={this.props.currentUser}
                              isMarkAsCompleteDisabled={true}
                              withoutCardModal={true}
                              key={index}
                              type={this.state.viewRegime}
                              removeCardFromList={this.removeCardFromList}
                            />
                          </div>
                        ))}
                    </div>
                  </div>
                  {this.state.suggesstedCard.length === 0 &&
                    !this.state.pending &&
                    this.state.queryFromAggs && (
                      <div className="note-container make-center">
                        <small>{tr('No data related to search and filter.')}</small>
                      </div>
                    )}
                  {this.state.pending && this.state.pagginationQuery && (
                    <div className="make-center">
                      <Spinner />
                    </div>
                  )}
                </div>
              </div>
            )}
            {this.state.pending && !this.state.pagginationQuery && (
              <div className="make-center">
                <Spinner />
              </div>
            )}
            <div className="small-12 columns">
              {this.state.channelSearch &&
                this.state.isSearching &&
                this.state.suggesstedCard.length === 0 &&
                !this.state.pending &&
                !this.state.queryFromAggs && (
                  <div className="note-container make-center">
                    <small>{tr('No data related to search and filter.')}</small>
                  </div>
                )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelBase.propTypes = {
  routeParams: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  channels: PropTypes.object,
  location: PropTypes.object
};

export default connect(state => ({
  channels: state.channelsV2.toJS(),
  team: state.team.toJS(),
  currentUser: state.currentUser.toJS()
}))(ChannelBase);
