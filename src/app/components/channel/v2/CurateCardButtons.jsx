import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import RejectIcon from 'material-ui/svg-icons/content/clear';
import PublishIcon from 'material-ui/svg-icons/action/done';

import { curateItem, skipItem } from '../../actions/channelsActionsV2';
import { openCardRemovalConfirmation } from '../../actions/modalActions';

class CurateCardButtons extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      publishColor: {
        color: '#4de8ce'
      },
      rejectColor: {
        color: '#f1736a'
      }
    };
  }

  curateClickHandler = (e, isReject, rejectCallback) => {
    e.preventDefault();
    if (isReject) {
      // If Reject Prompt config is true, we need to show rejection modal
      if (this.props.configs.reject_prompt_for_curators) {
        this.props.dispatch(
          openCardRemovalConfirmation(
            this.props.card,
            this.props.channel,
            this.props.card.cardType,
            isReject,
            rejectCallback
          )
        );
      } else {
        this.props
          .dispatch(skipItem(this.props.card.id, this.props.channel.id, this.props.channel.label))
          .then(() => {
            this.props.rejectCardFromList(this.props.card.id);
          })
          .catch(err => {
            console.error(`Error in CurateCardButtons.curateClickHandler.func: ${err}`);
          });
      }
    } else {
      this.props.rejectPublishCallback(this.props.card.id, this.props.channel.id);
      this.props.dispatch(
        curateItem(this.props.card.id, this.props.channel.id, this.props.channel.label)
      );
    }
  };

  render() {
    return (
      <div className="curate-card-button" style={{ margin: '5px 10px' }}>
        <div className="row">
          <div className="small-6 column">
            <a
              href="#"
              onClick={e => {
                this.curateClickHandler(e, false);
              }}
              className="row publishButton"
            >
              <div className="small-5 text-right">
                <PublishIcon style={this.styles.publishColor} />
              </div>
              <div className="small-7" style={this.styles.publishColor}>
                {tr('Publish')}
              </div>
            </a>
          </div>
          <div className="small-6 column">
            <a
              href="#"
              onClick={e => {
                this.curateClickHandler(e, true, this.props.cardList);
              }}
              className="row rejectButton"
            >
              <div className="small-5 text-right">
                <RejectIcon style={this.styles.rejectColor} />
              </div>
              <div className="small-7" style={this.styles.rejectColor}>
                {tr('Reject')}
              </div>
            </a>
          </div>
        </div>
      </div>
    );
  }
}

CurateCardButtons.propTypes = {
  card: PropTypes.object,
  cardList: PropTypes.object,
  channel: PropTypes.object,
  rejectPublishCallback: PropTypes.func
};

const mapStateToProps = state => {
  return {
    configs: state.team.get('config')
  };
};

export default connect(mapStateToProps)(CurateCardButtons);
