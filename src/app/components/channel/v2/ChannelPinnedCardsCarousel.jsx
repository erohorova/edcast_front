import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Carousel from '../../common/Carousel';
import CardOverviewModal from '../../modals/CardOverviewModal';
import { pinUnpinCardv2, removeCardFromCarousel } from '../../../actions/channelsActionsV2';
import { deleteComment } from 'edc-web-sdk/requests/cards';
import { connect } from 'react-redux';
import ConfirmationModal from '../../modals/ConfirmationCommentModal';
import { Permissions } from '../../../utils/checkPermissions';
import { tr } from 'edc-web-sdk/helpers/translations';

class ChannelPinnedCardsCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openConfirm: false,
      cardId: null,
      commentId: null,
      updateStateFunc: function() {},
      postThisCard: {}
    };
  }

  unpinClickHandler = card => {
    let payload = {
      pinnable_id: this.props.channel.id,
      pinnable_type: 'Channel',
      object_id: card.id,
      object_type: 'Card'
    };
    this.props.dispatch(pinUnpinCardv2(payload, false, null, card));
  };

  showConfirmationModal = (cardId, commentId, updateStateFunc) => {
    this.setState({
      openConfirm: !this.state.openConfirm,
      cardId: cardId,
      commentId: commentId,
      updateStateFunc: updateStateFunc
    });
  };

  toggleModal = () => {
    this.setState({ openConfirm: !this.state.openConfirm });
  };

  confirmDelete = () => {
    this.setState({ openConfirm: false });
    deleteComment(this.state.cardId, this.state.commentId)
      .then(() => {
        this.state.updateStateFunc();
      })
      .catch(err => {
        console.error(`Error in ChannelPinnedCardsCarousel.deleteComment.func : ${err}`);
      });
  };

  onImageLoad = e => {
    this.state.emptyStateImageLoaded = true;
  };

  removeCardFromCarousel = card => {
    this.props.dispatch(removeCardFromCarousel(card));
  };

  render() {
    let defaultImage = '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg';
    let pinnedCards = this.props.channelProps.get('pinnedCards');
    return (
      <div>
        {!pinnedCards ? (
          <div className="row" style={{ position: 'relative' }}>
            <div className="small-12 columns">
              <div className="emptyStateWrapper">
                <div
                  style={{
                    background: `${this.state.emptyStateImageLoaded} ? '#fff' : 'transparent`,
                    borderRadius: '2px'
                  }}
                >
                  {this.state.emptyStateImageLoaded && (
                    <p className="emptyStateImageText">
                      {tr('Select a card to "Pin to Top" on edit mode!')}
                    </p>
                  )}
                  <img
                    className="emptyStateImage"
                    src="/i/images/empty_state_pinned_card.png"
                    onLoad={this.onImageLoad.bind(this)}
                  />
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="row">
            <div className="small-12 columns">
              <div className="channel-carousel larger-arrow">
                {Array.isArray(pinnedCards) && !!pinnedCards.length && (
                  <Carousel slidesToShow={1} channelPinnedCards={true}>
                    {pinnedCards.map(card => {
                      return (
                        <div key={card.id}>
                          <CardOverviewModal
                            isPinned={true}
                            card={card}
                            defaultImage={this.props.defaultImage}
                            cardUpdated={function() {}}
                            showUnPinOption={this.props.editable}
                            unpinClickHandler={this.unpinClickHandler}
                            hideCommentModal={true}
                            showComment={
                              Permissions['enabled'] !== undefined &&
                              Permissions.has('CREATE_COMMENT')
                            }
                            showConfirmationModal={this.showConfirmationModal}
                            parentSource="fromChannel"
                            isPinnedInChannel={true}
                            removeCardFromCarousel={this.removeCardFromCarousel}
                          />
                        </div>
                      );
                    })}
                  </Carousel>
                )}
                {this.state.openConfirm && (
                  <ConfirmationModal
                    title={'Confirm'}
                    message={'Do you want to delete the comment?'}
                    closeModal={this.toggleModal}
                    callback={this.confirmDelete}
                  />
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelPinnedCardsCarousel.defaultProps = {
  editable: false
};

ChannelPinnedCardsCarousel.propTypes = {
  defaultImage: PropTypes.string,
  editable: PropTypes.bool,
  channel: PropTypes.object,
  channelProps: PropTypes.object
};

export default connect(state => ({
  channelProps: state.channelNewReducer
}))(ChannelPinnedCardsCarousel);
