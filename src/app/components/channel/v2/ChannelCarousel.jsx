import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Carousel from '../../common/Carousel';
import Card from './../../cards/Card';
import CardModal from './../../modals/CardModal';
import { openChannelCardsModal } from './../../../actions/modalActions';
import IconButton from 'material-ui/IconButton/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';
import { tr } from 'edc-web-sdk/helpers/translations';
import ChannelContnetLoader from './ChannelContnetLoader';
import { getTypeCardsv2 } from '../../../actions/channelsActionsV2';
import { getCarouselItems } from '../../../actions/carouselsActions';

class ChannelCarousel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      newCardView: window.ldclient.variation('new-card', false),
      newUITileCard:
        window.ldclient.variation('new-ui-tile-card', false) ||
        window.ldclient.variation('card-v3', false),
      multilingualFiltering: window.ldclient.variation('multilingual-content', false),
      modalOpen: false,
      postThisCard: {},
      channelCardsToRemove: [],
      loading: true,
      cardData: {}
    };
  }

  componentDidMount() {
    this.fetchCarouselCards();
  }

  fetchCarouselCards = (viewAll, editMode) => {
    let channel = this.props.channel || {};
    let carousel = this.props.carousel || {};
    let cards = [];
    let cardModalDetails = {
      isCustomCarousel: carousel.custom_carousel,
      autoPinCards: this.props.autoPinCards
    };
    if (!carousel.custom_carousel) {
      let payload = this._fetchCarouselCardPayload(carousel.default_label);
      getTypeCardsv2(channel.id, payload, 12, 0, this.state.multilingualFiltering)
        .then(data => {
          cards = data;
          this.setState({
            cards,
            loading: false,
            carouselCardCount: this.props.count
          });
        })
        .then(() => {
          if (viewAll) {
            this.props.dispatch(
              openChannelCardsModal(
                this.props.description,
                this.props.count,
                this.state.cards,
                this.props.channel,
                editMode,
                cardModalDetails
              )
            );
          }
        })
        .catch(e => {
          console.error(e);
        });
    } else if (carousel.custom_carousel) {
      this.props
        .dispatch(getCarouselItems({ structure_id: carousel.id }))
        .then(structureData => {
          cards = structureData.structuredItems.map(card => card.entity);
          this.setState({
            cards,
            loading: false,
            carouselCardCount: cards && cards.length
          });
        })
        .then(() => {
          if (viewAll) {
            this.props.dispatch(
              openChannelCardsModal(
                this.props.description,
                this.state.carouselCardCount,
                this.state.cards,
                this.props.channel,
                editMode,
                cardModalDetails
              )
            );
          }
        })
        .catch(e => {
          console.error(e);
        });
    }
  };

  _fetchCarouselCardPayload(cardType) {
    let payload = [];
    switch (cardType) {
      case 'SmartCards':
        payload = ['media', 'poll'];
        break;
      case 'Pathways':
        payload = ['pack'];
        break;
      case 'Journeys':
        payload = ['journey'];
        break;
      case 'Streams':
        payload = ['video_stream'];
        break;
      case 'Courses':
        payload = ['course'];
        break;
      default:
        break;
    }
    return payload;
  }

  getCardProps = data => {
    this.setState({ cardData: data, modalOpen: true });
  };

  voidCardProps = () => {
    this.setState({ modalOpen: false, cardData: null });
  };

  handleViewAllClick = (editMode, e) => {
    e && e.preventDefault();
    this.fetchCarouselCards(true, editMode);
  };

  updateChannelCards = (cardId, channelIds) => {
    if (!~channelIds.indexOf(this.props.channel.id)) {
      let channelCardsToRemove = this.state.channelCardsToRemove;
      channelCardsToRemove.push(cardId);
      this.setState({ channelCardsToRemove }, this.props.changeCount);
    }
  };

  removeCardFromCarousel = card => {
    const cards = [...this.state.cards];
    const cardIndex = cards.indexOf(card);
    cards.splice(cardIndex, 1);
    this.setState({ cards, carouselCardCount: this.state.carouselCardCount - 1 });
  };

  render() {
    let items = this.state.cards || [];
    let cardsInCarousel = this.state.newUITileCard ? 3 : 4;
    let carouselCardCount = this.state.carouselCardCount;

    return (
      <div>
        <div className="row channel-title">
          {!this.state.loading && (items && items.length > 0) && (
            <div className="small-6 columns">
              <h5 style={{ display: 'inline-block', verticalAlign: 'middle', marginRight: '5px' }}>
                {this.props.carousel && this.props.carousel.default_label} ({carouselCardCount})
              </h5>
              {this.props.editable && (
                <IconButton
                  onTouchTap={() => this.handleViewAllClick(true)}
                  style={{
                    height: '15px',
                    width: '15px',
                    padding: 0,
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    marginRight: '5px'
                  }}
                  iconStyle={{ width: '20px', height: '20px' }}
                >
                  <EditIcon color="#6f708b" />
                </IconButton>
              )}
            </div>
          )}
          {items.length > cardsInCarousel && (
            <div className="small-6 columns text-right">
              <a
                href="#"
                onClick={e => this.handleViewAllClick(false, e)}
                style={{ textDecoration: 'underline', color: 'rgb(73, 144, 226)' }}
              >
                {tr('View All')}
              </a>
            </div>
          )}

          {this.state.loading && <ChannelContnetLoader loadingCards={cardsInCarousel} />}
        </div>

        {!this.state.loading && (
          <div>
            <div className="row">
              <div className="small-12 columns channel-card-wrapper">
                <div className="channel-card-wrapper-inner">
                  <Carousel
                    slidesToShow={cardsInCarousel}
                    isChannelInfoCarousel={this.props.isChannelInfoCarousel}
                  >
                    {items &&
                      items.length > 0 &&
                      items.map(card => {
                        return (
                          <div key={card.id}>
                            <Card
                              toggleSearch={function() {}}
                              author={card.author && card.author}
                              card={card}
                              dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                              startDate={
                                card.startDate || (card.assignment && card.assignment.startDate)
                              }
                              user={this.props.currentUser}
                              tooltipPosition="top-center"
                              moreCards={false}
                              withoutCardModal={true}
                              getCardProps={this.getCardProps}
                              voidCardProps={this.voidCardProps}
                              channel={this.props.channel}
                              type={this.props.type}
                              removeCardFromCarousel={this.removeCardFromCarousel}
                            />
                          </div>
                        );
                      })}
                  </Carousel>
                  {this.state.modalOpen && (
                    <CardModal
                      logoObj={this.state.cardData.logoObj}
                      defaultImage={this.state.cardData.defaultImage}
                      card={this.state.cardData.card}
                      isUpvoted={this.state.cardData.isUpvoted}
                      updateCommentCount={this.state.cardData.updateCommentCount}
                      commentsCount={this.state.cardData.commentsCount}
                      votesCount={this.state.cardData.votesCount}
                      closeModal={this.state.cardData.closeModal}
                      likeCard={this.state.cardData.likeCard}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelCarousel.propTypes = {
  changeCount: PropTypes.any,
  currentUser: PropTypes.object,
  channel: PropTypes.object,
  items: PropTypes.array,
  editable: PropTypes.bool,
  count: PropTypes.number,
  description: PropTypes.string,
  type: PropTypes.string,
  isChannelInfoCarousel: PropTypes.string,
  carousel: PropTypes.object,
  autoPinCards: PropTypes.bool
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(ChannelCarousel);
