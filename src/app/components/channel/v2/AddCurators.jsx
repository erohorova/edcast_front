import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { usersv2 } from 'edc-web-sdk/requests/index';
import { Menu, MenuItem, AsyncTypeahead } from 'react-bootstrap-typeahead';
import Chip from 'material-ui/Chip';
import IconButton from 'material-ui/IconButton/IconButton';
import Close from 'edc-web-sdk/components/icons/Close';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { saveCurators } from '../../actions/channelsActionsV2';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';

class AddCurators extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      currentUsers: this.props.curators.map(item => ({ ...item })),
      selectedUsers: this.props.curators,
      selectedUserIds: []
    };

    this.styles = {
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chipLabel: {
        fontSize: '12px',
        color: '#9276b5'
      },
      customIcon: {
        position: 'absolute',
        top: '1px',
        maxHeight: '18px',
        right: '7px',
        padding: 0,
        maxWidth: '18px'
      },
      chip: {}
    };
  }

  handleRequestDelete = userId => {
    let existSelectedUsers = this.state.selectedUsers;
    let selectedUsers = [];
    let selectedUserIds = [];
    existSelectedUsers.forEach(user => {
      if (user.id !== userId) {
        selectedUsers.push(user);
        selectedUserIds.push(user.id);
      }
    });
    this.setState({ selectedUsers: selectedUsers, selectedUserIds: selectedUserIds });
  };

  _handleSearch = query => {
    let that = this;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

    if (!query) {
      return;
    }
    usersv2
      .searchUsers({ q: query })
      .then(data => {
        let users = data.users.map(user => {
          return {
            id: user.id,
            name: user.name,
            slug: user.handle,
            avatar: user.avatarimages.tiny || defaultUserImage,
            type: 'User'
          };
        });
        that.setState({
          dataSource: users.filter(
            item =>
              findIndex(this.state.selectedUsers, selectedUser => selectedUser.id == item.id) === -1
          )
        });
      })
      .catch(err => {
        console.error(`Error in AddCurators.searchUsers.func: ${err}`);
      });
  };

  handleUserClick = user => {
    if (!this.state.selectedUserIds.includes(user.id)) {
      let users = this.state.selectedUsers;
      users.push(user);
      let userIds = users.map(userItem => {
        return userItem.id;
      });
      this.setState({ selectedUsers: users, selectedUserIds: userIds });
      this.refs.userSearchTypeahead.getInstance().blur();
    }
  };

  saveCurators = () => {
    this.props.dispatch(
      saveCurators(this.props.channelId, this.state.selectedUsers, this.state.currentUsers)
    );
    this.props.closeModal();
  };

  _renderMenu = (results, menuProps) => {
    const users = results.map(user => {
      return [
        <li key={user.id}>
          <a onClick={this.handleUserClick.bind(this, user)}>
            <img src={user.avatar} height="20" className="user-search-image" />
            <div className="user-search-name">{user.name}</div>
          </a>
        </li>
      ];
    });

    return <Menu {...menuProps}>{users}</Menu>;
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    return (
      <div className="channel-curators-wrapper">
        <div className="list-users">
          <div style={this.styles.wrapper}>
            {this.state.selectedUsers.map(user => {
              return (
                <Chip
                  key={user.id}
                  style={this.styles.chip}
                  backgroundColor={'#fff'}
                  labelColor={'#9276b5'}
                  labelStyle={this.styles.chipLabel}
                  className={'user-chip'}
                >
                  <small style={this.styles.chipLabel}>{user.name}</small>
                  <IconButton
                    className={'cancel-icon delete'}
                    ref={'cancelIcon'}
                    tooltip={tr('Remove Selection')}
                    disableTouchRipple
                    tooltipPosition="top-center"
                    style={this.styles.customIcon}
                    onTouchTap={this.handleRequestDelete.bind(this, user.id)}
                  >
                    <Close color="#9276b5" />
                  </IconButton>
                </Chip>
              );
            })}
          </div>
        </div>

        <div className="channel-user-search-input user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={this.state.dataSource}
            placeholder={tr('Add Curator...')}
            onSearch={this._handleSearch}
            submitFormOnEnter={false}
            ref="userSearchTypeahead"
            filterBy={userData => this.state.dataSource.some(item => item.id == userData.id)}
          />

          <SecondaryButton
            label={tr('Save')}
            className="save-curators"
            onTouchTap={this.saveCurators}
          />
        </div>
      </div>
    );
  }
}

AddCurators.propTypes = {
  curators: PropTypes.any,
  renderMenu: PropTypes.any,
  closeModal: PropTypes.func,
  channelId: PropTypes.any
};

export default connect()(AddCurators);
