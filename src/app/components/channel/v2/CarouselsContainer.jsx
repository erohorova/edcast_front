import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ChannelCarousel from './ChannelCarousel';
import { tr } from 'edc-web-sdk/helpers/translations';
import sortBy from 'lodash/sortBy';

class CarouselsContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showJourney: window.ldclient.variation('journey', false),
      channelCustomCarousels: window.ldclient.variation('channel-custom-carousels', false)
    };
  }

  listChannel = obj => {
    let ChannelController = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        ChannelController.push(listObj);
      });
      ChannelController.sort((a, b) => a.index - b.index);
    }
    return ChannelController;
  };

  getCarouselCardCount = carouselType => {
    let count = 0;
    switch (carouselType) {
      case 'SmartCards':
        count = this.props.channel.smartbitesCount;
        break;
      case 'Pathways':
        count = this.props.channel.publishedPathwaysCount;
        break;
      case 'Journeys':
        count = this.props.channel.publishedJourneysCount;
        break;
      case 'Streams':
        count = this.props.channel.videoStreamsCount;
        break;
      case 'Courses':
        count = this.props.channel.coursesCount;
        break;
      default:
        break;
    }
    return count;
  };

  render() {
    let channelCarrousels = ((this.props.channel && this.props.channel.carousels) || []).filter(
      carousel => carousel.visible
    );
    channelCarrousels = sortBy(channelCarrousels, 'index');

    return (
      <div>
        {channelCarrousels.map((carousel, index) => {
          let count = carousel.custom_carousel
            ? carousel.items && carousel.items.length
            : this.getCarouselCardCount(carousel.default_label);
          let carouselID = '';
          switch (carousel.default_label) {
            case 'SmartCards':
              carouselID = 'smartbites-carousal';
              break;
            case 'Pathways':
              carouselID = 'pathway-carousal';
              break;
            case 'Journeys':
              carouselID = 'journey-carousal';
              break;
            case 'Streams':
              carouselID = 'stream-carousal';
              break;
            case 'Courses':
            case 'Сourses':
              carouselID = 'courses-carousal';
              break;
            default:
              carouselID = `custom-carousal-${carousel.id}`;
              break;
          }
          return (
            <div key={index} id={carouselID}>
              <ChannelCarousel
                isChannelInfoCarousel="true"
                description={tr(carousel.default_label)}
                count={count}
                channel={this.props.channel}
                editable={this.props.carouselEditable}
                type={carousel.default_label.toLowerCase()}
                carousel={carousel}
                autoPinCards={this.props.channel.autoPinCards}
              />
            </div>
          );
        })}
      </div>
    );
  }
}

CarouselsContainer.propTypes = {
  channel: PropTypes.object,
  carouselEditable: PropTypes.bool,
  carousel: PropTypes.object
};

export default connect(state => ({
  carousel: state.carousel.toJS()
}))(CarouselsContainer);
