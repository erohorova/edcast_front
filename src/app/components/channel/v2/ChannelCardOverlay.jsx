import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import PinIcon from 'edc-web-sdk/components/icons/Pin';
import UnpinIcon from 'edc-web-sdk/components/icons/Unpin';
import DeleteIcon from 'edc-web-sdk/components/icons/Delete';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { channelReorder } from 'edc-web-sdk/requests/channels.v2';
import { removeChannelCardv2 } from '../../../actions/channelsActionsV2';
import { Permissions } from '../../../utils/checkPermissions';

class ChannelCardOverlay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pinned: props.pinnedStatus || false,
      disableButton: true,
      isReorderingActivated: window.ldclient.variation('reorder-content', false),
      confirmation: false,
      deletingCardStatus: false,
      rank: props.card && props.card.rank,
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      iconStyle: {
        width: '30px',
        height: '30px'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card && nextProps.card.pinnedStatus) {
      this.setState({ pinned: nextProps.card.pinnedStatus });
    }
  }

  pinUnpinCard = () => {
    if (this.props.pinnedCount == 10 && !this.state.pinned) {
      this.props.dispatch(openSnackBar('You can pin max 10 cards!', true));
      return;
    }
    this.props.pinUnpinCard(this.props.card, !this.state.pinned);
    this.setState({ pinned: !this.state.pinned });
  };

  removeCardOrCancelHandler = () => {
    let confirmation = this.state.confirmation;
    this.setState({ confirmation: !confirmation });
  };

  reorderInputValidation = e => {
    let rank = e.target.value;
    if (rank >= 1 && rank <= this.props.allCardsCount) {
      this.setState({
        disableButton: false,
        rank
      });
    } else {
      this.setState({
        disableButton: true,
        rank
      });
    }
  };

  reorderClickHandler = (event, clickedOn) => {
    event.stopPropagation();
    this.props.reorderToggle(true);
    let cardType = [];
    switch (this.props.card.cardType) {
      case 'video_stream':
        cardType = ['video_stream'];
        break;
      case 'pack':
        cardType = ['pack'];
        break;
      case 'course':
        cardType = ['course'];
        break;
      case 'journey':
        cardType = ['journey'];
        break;
      default:
        cardType = ['media', 'poll'];
        break;
    }
    switch (clickedOn) {
      case 'top':
        channelReorder(this.props.channel.id, this.props.card.id, { rank: 1 })
          .then(() => {
            this.props.channelReOrderCallback();
          })
          .catch(err => {
            console.error(`Error in ChannelCardOverlay.channelReorder.func top : ${err}`);
          });
        break;
      default:
        channelReorder(this.props.channel.id, this.props.card.id, { rank: this.state.rank })
          .then(() => {
            this.props.channelReOrderCallback();
          })
          .catch(err => {
            console.error(`Error in ChannelCardOverlay.channelReorder.func default : ${err}`);
          });
        this.refs.orderNumber.value = '';
        break;
    }
  };

  deleteCard = () => {
    let card = this.props.card;
    let channel = this.props.channel;
    this.setState({
      deletingCardStatus: true
    });
    this.props
      .dispatch(removeChannelCardv2(channel.id, card, this.state.pinned))
      .then(() => {
        this.props.deleteCard(card.id);
        this.removeCardOrCancelHandler();
        this.setState({
          deletingCardStatus: false
        });
      })
      .catch(err => {
        console.error(`Error in ChannelCardOverlay.deleteCard.func: ${err}`);
      });
  };

  render() {
    let isSingleCard = this.props.allCardsCount === 1 ? true : false;
    return (
      <div className={`card-overlay-layer ${this.state.isCardV3 ? 'card-overlay-layer-v3' : ''}`}>
        {!this.state.confirmation ? (
          <div>
            {!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard && (
              <div className="row action-icons-row">
                <div className="small-6 columns">
                  <div className="card-overlay-text">{tr('Enter Order')}</div>
                </div>
                <div className="small-6 columns input-block-style">
                  <input
                    value={this.state.rank}
                    type="text"
                    onChange={e => this.reorderInputValidation(e)}
                    className="channel-rank-input"
                    required
                  />
                  <button
                    className="channel-rank-submit"
                    onClick={e => this.reorderClickHandler(e, 'reorder')}
                    disabled={this.state.disableButton}
                  >
                    {tr('OK')}
                  </button>
                </div>
                <div className="small-12 columns">
                  <div className="card-overlay-text">
                    <span
                      name="top"
                      className="pointer"
                      onTouchTap={e => this.reorderClickHandler(e, 'top')}
                    >
                      {tr('Top of List')}
                    </span>
                  </div>
                </div>
              </div>
            )}
            <div
              className={
                'row action-icons-row ' +
                (!this.props.isCustomCarousel && this.state.isReorderingActivated && !isSingleCard
                  ? 'not-custom-carousel'
                  : 'no-reordering')
              }
            >
              <div className="small-12 columns icon-wrapper">
                {!this.state.pinned && !this.props.autoPinCards && (
                  <div className="specific-icon-parent-wrapper">
                    <IconButton className="specific-icon-wrapper" onTouchTap={this.pinUnpinCard}>
                      <PinIcon customStyle={this.styles.iconStyle} />
                    </IconButton>
                    <div className="text-center icon-label">{tr('Pin to top')}</div>
                  </div>
                )}
                {this.state.pinned && !this.props.autoPinCards && (
                  <div className="specific-icon-parent-wrapper">
                    <IconButton
                      className="specific-icon-wrapper unpin"
                      onTouchTap={this.pinUnpinCard}
                    >
                      <UnpinIcon customStyle={this.styles.iconStyle} color={'#4de8ce'} />
                    </IconButton>
                    <div className="text-center icon-label">{tr('Un-pin')}</div>
                  </div>
                )}
                <div className="specific-icon-parent-wrapper delete text-center">
                  <IconButton
                    className="specific-icon-wrapper delete-icon"
                    onTouchTap={this.removeCardOrCancelHandler}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <div className="text-center delete icon-label">{tr('Remove')}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div>
            <div className="card-overlay-confirmation">
              <p className="card-overlay-text text-center">
                Do you really want to remove selected card from {this.props.channel.label} channel?
              </p>
              <div className="confirmation-buttons-container">
                <button
                  className="confirmation-buttons primary"
                  onClick={this.removeCardOrCancelHandler}
                >
                  CANCEL
                </button>
                <button
                  className="confirmation-buttons secondary"
                  onClick={() => {
                    this.deleteCard();
                  }}
                >
                  {this.state.deletingCardStatus ? 'DELETING...' : 'OK'}
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelCardOverlay.propTypes = {
  card: PropTypes.object,
  allCardsCount: PropTypes.number,
  removeCardLaunchModal: PropTypes.func,
  pinUnpinCard: PropTypes.func,
  pinnedCount: PropTypes.number,
  channel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  pinnedStatus: PropTypes.bool,
  channelReOrderCallback: PropTypes.func,
  reorderToggle: PropTypes.func,
  setOffset: PropTypes.func,
  deleteCard: PropTypes.func,
  autoPinCards: PropTypes.bool
};

export default connect()(ChannelCardOverlay);
