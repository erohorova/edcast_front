import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import FollowButton from 'edc-web-sdk/components/FollowButton';
import colors from 'edc-web-sdk/components/colors/index';
import { PrimaryButton } from 'edc-web-sdk/components/index';
import CameraIcon from 'edc-web-sdk/components/icons/CameraIcon';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import LockIcon from 'edc-web-sdk/components/icons/Lock';

import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import EditIcon from 'material-ui/svg-icons/image/edit';

import {
  openAddCuratorModal,
  openUploadImageModal,
  openUpdateChannelDescriptionModal,
  openChannelEditModal
} from '../../../actions/modalActions';
import {
  updateChannelDetails,
  getNonCurateCards,
  toggleFollowV2,
  snackBarOpenClose
} from '../../../actions/channelsActionsV2';
import { followingChannelAction } from '../../../actions/currentUserActions';
import BreadcrumbV2 from '../../common/BreadcrumpV2';
import * as filestack from '../../../utils/filestack';

class ChannelDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      description: '',
      descriptionLimit: 200,
      channel: null,
      isFollowing: false,
      openPopOver: false,
      openCuratorsPopOver: false,
      showEditImage: false,
      showJourney: window.ldclient.variation('journey', false),
      openChannel: window.ldclient.variation('open-channel-enabled', false),
      showLockIcon: this.props.team.config && this.props.team.config.show_lock_icon
    };
    let blue = '#4990e2';
    this.styles = {
      channelDetails: {
        backgroundColor: '#ffffff',
        height: '186px',
        color: '#454560 !important'
      },
      lrPadding15: {
        padding: '0px 15px'
      },
      lPadding15: {
        padding: '0px 0px 0px 15px'
      },
      coverImage: {
        backgroundColor: '#F2F2F2',
        backgroundImage:
          'url("http://www.advblog.net/wp-content/uploads/2015/06/marketing-protiv-brandinga-223x155.jpg")',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        width: '223px',
        height: '155px'
      },
      channelTitle: {
        fontSize: '22px',
        color: '#454560'
      },
      closeIcon: {
        position: 'absolute',
        top: '-2px',
        right: '0'
      },
      viewMore: {
        fontSize: '14px',
        textDecoration: 'underline',
        color: blue
      },
      rowPadding: {
        padding: '0rem 0.75rem',
        position: 'relative'
      },
      rowMargin: {
        margin: '0px'
      },
      editIcon: {
        width: '21px',
        height: '21px',
        verticalAlign: 'bottom'
      },
      creatorCurator: {
        color: '#6f708b'
      },
      curatorListFont: {
        color: '#6f708b',
        fontSize: '12px'
      },
      curatorEdit: {
        verticalAlign: 'middle',
        padding: '0px',
        marginRight: '5px',
        width: '21px',
        height: '21px',
        top: '-2px'
      },
      labelBlock: {
        marginTop: '3px',
        display: 'inline-block',
        marginRight: '18px'
      },
      cameraIcon: {
        cursor: 'pointer',
        margin: 'auto',
        position: 'absolute',
        top: '0px',
        left: '0px',
        bottom: '0px',
        right: '0px',
        height: '22px',
        width: '25px'
      },
      cameraBg: {
        height: '57px',
        width: '58px',
        backgroundColor: '#000000',
        opacity: '0.6',
        borderRadius: '50%',
        margin: 'auto',
        position: 'absolute',
        top: '0px',
        bottom: '0px',
        left: '0px',
        right: '0px'
      },
      countLabel: {
        textDecoration: 'underline',
        color: blue
      },
      fullDescription: {
        fontSize: '14px',
        color: '#454560',
        lineHeight: '1.3',
        fontWeight: '100',
        wordWrap: 'break-word'
      }
    };

    this.triggerUpdate = this.triggerUpdate.bind(this);
  }

  componentDidMount() {
    let channel = this.props.channel;
    this.setState({
      channel: channel,
      isFollowing: channel && channel.isFollowing,
      description: channel && channel.description
    });
  }

  triggerUpdate = () => {
    this.props.dispatch(openChannelEditModal(this.state.channel));
  };

  componentWillReceiveProps(nextProps) {
    let channel = nextProps.channel;
    this.setState({
      channel: channel,
      isFollowing: channel && channel.isFollowing,
      description: channel && channel.description
    });
  }

  openPopOver = event => {
    event.preventDefault();
    this.setState({
      openPopOver: true,
      anchorEl: event.currentTarget
    });
  };

  closePopOver = () => {
    this.setState({
      openPopOver: false
    });
  };

  openCuratorsPopOver = event => {
    event.preventDefault();
    this.setState({
      openCuratorsPopOver: true,
      anchorCuratorsEl: event.currentTarget
    });
  };

  closeCuratorsPopOver = () => {
    this.setState({
      openCuratorsPopOver: false
    });
  };

  addCurator = () => {
    this.props.dispatch(
      openAddCuratorModal(
        this.state.channel.id,
        this.state.channel.curators,
        this.props.currentUser.id
      )
    );
  };

  curate = () => {
    this.props.dispatch(getNonCurateCards(false, this.props.channel, 0, true));
  };

  editImage = () => {
    let sizes = {
      maxWidth: 223,
      maxHeight: 155
    };
    let imageType = 'channelImage';
    let channel = this.state.channel;
    this.props.dispatch(
      openUploadImageModal(imageType, sizes, newUrl => {
        channel.mobile_image = newUrl;
        channel.profileImageUrl = newUrl;
        this.setState({ channel: channel });
        this.props.dispatch(updateChannelDetails(channel, false));
      })
    );
  };

  onHover = () => {
    this.setState({ showEditImage: true });
  };

  onHoverOut = () => {
    this.setState({ showEditImage: false });
  };

  channelCustomCarousels = carousel => {
    return (
      <span key={carousel.id}>
        {(this.state.channel.videoStreamsCount > 0 ||
          this.state.channel.publishedPathwaysCount > 0 ||
          this.state.channel.smartbitesCount > 0 ||
          this.state.channel.publishedJourneysCount > 0 ||
          this.state.channel.coursesCount > 0) && <span>&nbsp; &nbsp; | &nbsp; &nbsp;</span>}
        <a
          style={this.styles.countLabel}
          onClick={e => this.props.autoScroll(e, `custom-carousal-${carousel.id}`)}
        >
          {tr(carousel.default_label)}({carousel.items && carousel.items.length})
        </a>
      </span>
    );
  };

  getCustomCarousels = () => {
    let customCarousels =
      this.props.channel &&
      this.props.channel.carousels &&
      this.props.channel.carousels.filter(carousel => carousel.custom_carousel);
    let carouselsArray = [];
    this.props.carousel &&
      this.props.carousel.carousels &&
      this.props.carousel.carousels.forEach(item => {
        customCarousels &&
          customCarousels.forEach(carousel => {
            if (item.id == carousel.id) {
              carouselsArray.push(item);
            }
          });
      });
    return carouselsArray;
  };

  followClickHandler = followStatus => {
    let channel = this.state.channel;
    channel.followPending = true;
    this.setState(
      {
        channel
      },
      () => {
        this.props
          .dispatch(toggleFollowV2(this.state.channel.id, !followStatus))
          .then(followData => {
            this.props.dispatch(followingChannelAction(this.state.channel, followStatus));
            let channelData = this.state.channel;
            channelData.isFollowing = followData;
            channel.followPending = false;
            this.setState({ channel: channelData });
          })
          .catch(error => {
            this.props.dispatch(snackBarOpenClose(error.message, 1000));
            let channelData = this.state.channel;
            channel.followPending = false;
            this.setState({ channel: channelData });
          });
      }
    );
  };

  checkCarouselVisibility = (name, id) => {
    let carousel =
      this.state.channel &&
      this.state.channel.carousels &&
      this.state.channel.carousels.find(item => {
        if (id) {
          return item.id == id;
        } else {
          if (name === 'Courses') {
            //added this to support carousels mistakenly named with Russian letter 'С'
            return item.default_label === name || item.default_label === 'Сourses';
          }
          return item.default_label === name;
        }
      });
    return carousel ? carousel.visible : true;
  };

  render() {
    let channel = this.state.channel;
    let description =
      channel &&
      this.state.description &&
      this.state.description.slice(0, this.state.descriptionLimit);
    let showViewMore =
      this.state.description && this.state.description.length > this.state.descriptionLimit;
    let coverImage = channel && channel.profileImageUrl;
    let smallImageUrl =
      coverImage &&
      (~coverImage.indexOf('default_banner_image')
        ? coverImage
        : filestack.getResizedUrl(coverImage, 'height:150'));
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let config = this.props.team.config;
    let showCurator =
      config &&
      ((typeof config.show_curator == 'undefined' && !config.show_curator) ||
        !!config.show_curator);
    let isOpen = channel && !(typeof channel.isOpen == 'boolean' && channel.isOpen);
    let customCarousels = this.getCustomCarousels();

    const currentUserHandle = this.props.currentUser.handle;

    return (
      <div className="row" style={this.styles.rowPadding}>
        <BreadcrumbV2
          isChannelSearching={this.props.isChannelSearching}
          backToCarouselView={this.props.backToCarouselView}
        />
        {channel && (
          <div className="small-12 channel-details">
            <div className="row" style={this.styles.rowMargin}>
              <div
                className="large-3 small-12 cover-image-container"
                style={this.styles.lPadding15}
                onMouseEnter={this.onHover}
                onMouseLeave={this.onHoverOut}
              >
                <div
                  className="cover-image"
                  style={{
                    backgroundImage: 'url("' + (smallImageUrl ? smallImageUrl : '') + '")',
                    position: 'relative'
                  }}
                >
                  {this.props.editable && this.state.showEditImage && (
                    <div
                      style={this.styles.cameraBg}
                      onClick={this.editImage.bind(this, this.state.channel.profileImageUrl)}
                    >
                      <CameraIcon style={this.styles.cameraIcon} />
                    </div>
                  )}
                </div>
              </div>

              <div className="large-9 small-12 details-container" style={this.styles.lrPadding15}>
                <div style={this.styles.channelTitle}>
                  {' '}
                  {channel.label}
                  {this.props.editable && (
                    <span>
                      <IconButton
                        className="edit"
                        onTouchTap={this.triggerUpdate}
                        style={{
                          width: '21px',
                          height: '31px',
                          verticalAlign: 'bottom',
                          padding: '0',
                          paddingLeft: '5px'
                        }}
                        iconStyle={this.styles.editIcon}
                      >
                        <EditIcon color="#6f708b" />
                      </IconButton>
                    </span>
                  )}
                  {channel.isPrivate && this.state.showLockIcon && (
                    <IconButton
                      tooltip="Private Channel"
                      disableTouchRipple={true}
                      tooltipStyles={{ top: '20px', zIndex: '1000' }}
                      tooltipPosition="bottom-center"
                      style={{
                        width: '21px',
                        height: '31px',
                        verticalAlign: 'bottom',
                        padding: '0',
                        paddingLeft: '10px'
                      }}
                    >
                      <LockIcon />
                    </IconButton>
                  )}
                </div>
                {showViewMore}
                <div className="channel-description">
                  {description}
                  {showViewMore ? (
                    <span style={{ fontSize: '12px', fontWeight: '400' }}>
                      ...
                      <a style={this.styles.viewMore} onClick={this.openPopOver}>
                        {tr('View More')}
                      </a>
                    </span>
                  ) : (
                    ''
                  )}
                </div>
                <Popover
                  open={this.state.openPopOver}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  onRequestClose={this.closePopOver}
                  animation={PopoverAnimationVertical}
                  className="channel-desc-popover"
                >
                  <IconButton
                    onTouchTap={this.closePopOver}
                    iconStyle={{ width: '20px', height: '20px' }}
                    style={this.styles.closeIcon}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                  <h5 className="channel-popover-header">{tr('Description')}</h5>
                  <p style={this.styles.fullDescription}>{this.state.description}</p>
                </Popover>
                <div className="channel-content-list">
                  {channel.videoStreamsCount > 0 && this.checkCarouselVisibility('Streams') && (
                    <span>
                      <a
                        href="#"
                        style={this.styles.countLabel}
                        onClick={e => this.props.autoScroll(e, 'stream-carousal')}
                      >
                        {tr('Streams')}({channel.videoStreamsCount})
                      </a>
                    </span>
                  )}

                  {channel.publishedPathwaysCount > 0 && this.checkCarouselVisibility('Pathways') && (
                    <span>
                      {channel.videoStreamsCount > 0 && (
                        <span className="channel-left-boundary">&nbsp; &nbsp; | &nbsp; &nbsp;</span>
                      )}
                      <a
                        href="#"
                        style={this.styles.countLabel}
                        onClick={e => this.props.autoScroll(e, 'pathway-carousal')}
                      >
                        {tr('Pathways')}({channel.publishedPathwaysCount})
                      </a>
                    </span>
                  )}

                  {channel.smartbitesCount > 0 && this.checkCarouselVisibility('SmartCards') && (
                    <span>
                      {(channel.videoStreamsCount > 0 || channel.publishedPathwaysCount > 0) && (
                        <span className="channel-left-boundary">&nbsp; &nbsp; | &nbsp; &nbsp;</span>
                      )}
                      <a
                        href="#"
                        style={this.styles.countLabel}
                        onClick={e => this.props.autoScroll(e, 'smartbites-carousal')}
                      >
                        {tr('SmartCards')}({channel.smartbitesCount})
                      </a>
                    </span>
                  )}

                  {channel.publishedJourneysCount > 0 &&
                    this.state.showJourney &&
                    this.checkCarouselVisibility('Journeys') && (
                      <span>
                        {(channel.videoStreamsCount > 0 ||
                          channel.publishedPathwaysCount > 0 ||
                          channel.smartbitesCount > 0) && (
                          <span className="channel-left-boundary">
                            &nbsp; &nbsp; | &nbsp; &nbsp;
                          </span>
                        )}
                        <a
                          href="#"
                          style={this.styles.countLabel}
                          onClick={e => this.props.autoScroll(e, 'journey-carousal')}
                        >
                          {tr('Journeys')}({channel.publishedJourneysCount})
                        </a>
                      </span>
                    )}

                  {channel.coursesCount > 0 && this.checkCarouselVisibility('Courses') && (
                    <span>
                      {(channel.videoStreamsCount > 0 ||
                        channel.publishedPathwaysCount > 0 ||
                        channel.smartbitesCount > 0 ||
                        channel.publishedJourneysCount > 0) && (
                        <span className="channel-left-boundary">&nbsp; &nbsp; | &nbsp; &nbsp;</span>
                      )}
                      <a
                        href="#"
                        style={this.styles.countLabel}
                        onClick={e => this.props.autoScroll(e, 'courses-carousal')}
                      >
                        {tr('Courses')}({channel.coursesCount})
                      </a>
                    </span>
                  )}
                  {customCarousels.length > 0 &&
                    customCarousels.map(carousel => {
                      if (
                        carousel.items &&
                        carousel.items.length &&
                        this.checkCarouselVisibility(null, carousel.id)
                      ) {
                        return this.channelCustomCarousels(carousel);
                      }
                    })}
                  {window.ldclient.variation('channel-analytics', false) &&
                    (this.props.channel.isCurator || this.props.currentUser.isAdmin) && (
                      <span>
                        <span className="channel-left-boundary">&nbsp; &nbsp; | &nbsp; &nbsp;</span>
                        <a
                          style={this.styles.countLabel}
                          href={`${document.location.href}/analytics`}
                        >
                          {tr('Analytics')}
                        </a>
                      </span>
                    )}
                </div>
                <div className="creator-curator">
                  {showCurator && (this.state.openChannel ? isOpen : true) && (
                    <span style={this.styles.labelBlock}>
                      <span className="author-title">{tr('Curator:')}</span>
                      {this.props.editable && (
                        <IconButton
                          onTouchTap={this.addCurator}
                          iconStyle={this.styles.editIcon}
                          style={this.styles.curatorEdit}
                        >
                          <EditIcon color="#6f708b" />
                        </IconButton>
                      )}
                      {(channel.curators && channel.curators.length) == 0 && (
                        <span style={{ fontSize: '12px' }}>{tr('Not added yet.')}</span>
                      )}
                      {(channel.curators && channel.curators.length) > 0 &&
                        channel.curators.slice(0, 2).map((curator, i) => {
                          let userAvatar =
                            (curator.avatarimages && curator.avatarimages.tiny) || curator.avatar;
                          return (
                            <span
                              style={{ ...this.styles.curatorListFont, fontWeight: 400 }}
                              key={i}
                            >
                              <a
                                href={`/${
                                  (curator.handle || curator.slug).replace('@', '') ===
                                  currentUserHandle
                                    ? 'me'
                                    : curator.handle || curator.slug
                                }`}
                                style={this.styles.creatorCurator}
                              >
                                <span style={{ marginRight: '2px' }} key={curator.id}>
                                  <img
                                    className="author-img"
                                    src={userAvatar || defaultUserImage}
                                    height="26"
                                    width="26"
                                  />
                                  <span className="author-name">{curator.name}</span>
                                </span>
                              </a>
                              {i == 0 && channel.curators.length > 2 ? (
                                <span style={{ margin: '0 2px 0 -8px' }}>, </span>
                              ) : (
                                ''
                              )}
                            </span>
                          );
                        })}
                      {(channel.curators && channel.curators.length) > 2 && (
                        <span style={{ ...this.styles.curatorListFont, marginLeft: '-8px' }}>
                          and{' '}
                          <a
                            style={{ ...this.styles.viewMore, fontSize: '12px' }}
                            onClick={this.openCuratorsPopOver}
                          >
                            {channel.curators.length - 2 + ' ' + tr('others')}
                          </a>
                        </span>
                      )}
                      <Popover
                        open={this.state.openCuratorsPopOver}
                        anchorEl={this.state.anchorCuratorsEl}
                        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        onRequestClose={this.closeCuratorsPopOver}
                        animation={PopoverAnimationVertical}
                        className="channel-desc-popover"
                      >
                        <IconButton
                          onTouchTap={this.closeCuratorsPopOver}
                          iconStyle={{ width: '20px', height: '20px' }}
                          style={this.styles.closeIcon}
                        >
                          <CloseIcon color="#acadc1" />
                        </IconButton>
                        <h5 className="channel-popover-header">{tr('Curator:')}</h5>
                        <div className="curator-list">
                          {(channel.curators && channel.curators.length) > 0 &&
                            (channel.curators || [])
                              .slice(2, channel.curators.length)
                              .map((curator, i) => {
                                let userAvatar =
                                  (curator.avatarimages && curator.avatarimages.tiny) ||
                                  curator.avatar;
                                return (
                                  <a
                                    href={
                                      '/' + (curator.handle || curator.slug).replace('@', '') ===
                                      currentUserHandle
                                        ? 'me'
                                        : curator.handle || curator.slug
                                    }
                                    style={this.styles.creatorCurator}
                                    key={i}
                                  >
                                    <div style={{ padding: '5px' }} key={curator.id}>
                                      <img
                                        className="author-img"
                                        src={userAvatar || defaultUserImage}
                                        height="26"
                                        width="26"
                                      />
                                      <span className="author-name">{curator.name}</span>
                                    </div>
                                  </a>
                                );
                              })}
                        </div>
                      </Popover>
                    </span>
                  )}
                  {(channel.provider || channel.providerImage) && (
                    <span style={this.styles.labelBlock}>
                      <span className="author-title">{tr('Provider')}:</span>
                      {channel.provider && <span className="author-name">{channel.provider}</span>}
                      {channel.providerImage && (
                        <img className="provider-img" src={channel.providerImage} />
                      )}
                    </span>
                  )}
                  <span className="float-right" style={{ marginTop: '-4px', marginRight: '-3px' }}>
                    {!this.props.isCurate ? (
                      channel.allowFollow ? (
                        <FollowButton
                          following={channel.isFollowing}
                          label={tr(channel.isFollowing ? 'Following' : 'Follow')}
                          hoverLabel={tr(channel.isFollowing ? 'Unfollow' : '')}
                          pendingLabel={tr(channel.isFollowing ? 'Unfollowing...' : 'Following...')}
                          className="follow"
                          onTouchTap={this.followClickHandler.bind(this, channel.isFollowing)}
                          pending={channel.followPending}
                          theme={colors.followColor}
                        />
                      ) : (
                        <SecondaryButton label={tr('Coming Soon')} disabled={true} />
                      )
                    ) : (
                      <PrimaryButton
                        style={{ width: '7rem' }}
                        label={tr('Curate')}
                        onTouchTap={this.curate}
                        theme={colors.followColor}
                      />
                    )}
                  </span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelDetails.propTypes = {
  editable: PropTypes.bool,
  isCurate: PropTypes.bool,
  channel: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  carousel: PropTypes.object,
  isChannelSearching: PropTypes.bool,
  backToCarouselView: PropTypes.func
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS(),
  carousel: state.carousel.toJS()
}))(ChannelDetails);
