import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Carousel from '../common/Carousel';
import Card from './../cards/Card';
import DiscoverCard from '../common/Card';
import CardModal from '../modals/CardModal';
import { openChannelCardsModal } from '../../actions/modalActions';
import IconButton from 'material-ui/IconButton/IconButton';
import EditIcon from 'material-ui/svg-icons/image/edit';
import { tr } from 'edc-web-sdk/helpers/translations';

class ChannelCarousel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      modalOpen: false,
      channelCardsToRemove: []
    };
  }

  getCardProps = data => {
    this.setState({ cardData: data, modalOpen: true });
  };

  voidCardProps = () => {
    this.setState({ modalOpen: false, cardData: null });
  };

  handleViewAllClick = (editMode, e) => {
    e && e.preventDefault();
    this.props.dispatch(
      openChannelCardsModal(
        this.props.description,
        this.props.count,
        this.props.items,
        this.props.channel,
        editMode
      )
    );
  };

  updateChannelCards = (cardId, channelIds) => {
    if (!~channelIds.indexOf(this.props.channel.id)) {
      let channelCardsToRemove = this.state.channelCardsToRemove;
      channelCardsToRemove.push(cardId);
      this.setState({ channelCardsToRemove }, this.props.changeCount);
    }
  };

  render() {
    let items = this.props.items;
    let count = this.props.count;
    if (this.state.channelCardsToRemove.length) {
      items = items.filter(item => !~this.state.channelCardsToRemove.indexOf(item.id));
      count = this.props.count - (this.props.items.length - items.length);
    }
    return (
      <div className="">
        <div className="row channel-title">
          <div className="small-6 columns">
            <h5 style={{ display: 'inline-block', verticalAlign: 'middle', marginRight: '5px' }}>
              {this.props.description} ({count})
            </h5>
            {this.props.editable && (
              <IconButton
                aria-label="edit"
                onTouchTap={() => this.handleViewAllClick(true)}
                style={{
                  height: '15px',
                  width: '15px',
                  padding: 0,
                  display: 'inline-block',
                  verticalAlign: 'middle',
                  marginRight: '5px'
                }}
                iconStyle={{ width: '20px', height: '20px' }}
              >
                <EditIcon color="#6f708b" />
              </IconButton>
            )}
          </div>
          {items.length > 4 && (
            <div className="small-6 columns text-right">
              <a
                href="#"
                onClick={e => this.handleViewAllClick(false, e)}
                style={{ textDecoration: 'underline', color: 'rgb(73, 144, 226)' }}
              >
                {tr('View All')}
              </a>
            </div>
          )}
        </div>
        <div className="row">
          <div className="small-12 columns channel-card-wrapper">
            <div className="channel-card-wrapper-inner">
              <Carousel isChannelInfoCarousel={this.props.isChannelInfoCarousel}>
                {items.length > 0 &&
                  items.map(card => {
                    return (
                      <div key={card.id}>
                        <Card
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          tooltipPosition="top-center"
                          moreCards={false}
                          withoutCardModal={true}
                          getCardProps={this.getCardProps}
                          voidCardProps={this.voidCardProps}
                          channel={this.props.channel}
                          type={this.props.type}
                        />
                      </div>
                    );
                  })}
              </Carousel>
              {this.state.modalOpen && (
                <CardModal
                  logoObj={this.state.cardData.logoObj}
                  defaultImage={this.state.cardData.defaultImage}
                  card={this.state.cardData.card}
                  isUpvoted={this.state.cardData.isUpvoted}
                  updateCommentCount={this.state.cardData.updateCommentCount}
                  commentsCount={this.state.cardData.commentsCount}
                  votesCount={this.state.cardData.votesCount}
                  closeModal={this.state.cardData.closeModal}
                  likeCard={this.state.cardData.likeCard}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChannelCarousel.propTypes = {
  changeCount: PropTypes.any,
  currentUser: PropTypes.object,
  channel: PropTypes.object,
  items: PropTypes.array,
  editable: PropTypes.bool,
  count: PropTypes.number,
  description: PropTypes.string,
  type: PropTypes.string,
  isChannelInfoCarousel: PropTypes.string
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(ChannelCarousel);
