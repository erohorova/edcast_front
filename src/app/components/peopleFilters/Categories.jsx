import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';
import UserSearchComponent from '../search/UserSearchComponent.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';

class CategoriesFilterContainer extends Component {
  constructor(props) {
    super(props);

    this.styles = {
      checkboxOuter: {
        width: '0.875rem',
        height: '0.875rem',
        marginRight: '0.5rem',
        marginBottom: '1rem'
      },
      checkboxOuterLabel: {
        lineHeight: '1',
        textTransform: 'uppercase'
      },
      uncheckedIcon: {
        width: '0.875rem',
        height: '0.875rem'
      }
    };

    this.filterOptions = props.options;
  }

  handleChange = (name, type, e) => {
    this.props.handleFilterChange(e.target.checked, name, type);
  };

  render() {
    return (
      <div className="new-search__paper-container">
        <Paper className="new-search__paper">
          <div className="filter-title">{tr('CATEGORIES')}</div>
          {this.filterOptions.map((item, index) => {
            return (
              <div className="outer-checkbox" key={index}>
                <Checkbox
                  label={tr(item.text)}
                  iconStyle={this.styles.checkboxOuter}
                  labelStyle={this.styles.checkboxOuterLabel}
                  checkedIcon={<CheckOn1 color="#6f708b" />}
                  uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                  onCheck={this.handleChange.bind(this, item.value, 'category')}
                  value={item.value}
                  checked={
                    ~['followers', 'following'].indexOf(item.value)
                      ? this.props.query[item.value]
                      : this.props.query['role[]'] &&
                        ~this.props.query['role[]'].indexOf(item.value)
                  }
                />
              </div>
            );
          })}
          <div className="new-search__input-container">
            <UserSearchComponent
              handleFilterChange={this.props.handleFilterChange}
              type="categories"
              placeholder="By name"
            />
          </div>
          {this.props.hideFilters && <div className="translucent-layer" />}
        </Paper>
      </div>
    );
  }
}

CategoriesFilterContainer.propTypes = {
  options: PropTypes.array,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  checkedItem: PropTypes.str,
  query: PropTypes.object
};

export default CategoriesFilterContainer;
