import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import UserSearchComponent from '../search/UserSearchComponent.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';

class CustomBlockFilterComponent extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      checkboxOuter: {
        width: '0.875rem',
        height: '0.875rem',
        marginRight: '0.5rem',
        marginBottom: '1rem',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        lineHeight: '1',
        fontSize: '0.875rem',
        color: '#6f708b'
      },
      uncheckedIcon: {
        width: '0.875rem',
        height: '0.875rem'
      }
    };
    this.state = {
      options: props.options || []
    };
    this.type = props.type;
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.options) {
      this.setState({ options: nextProps.options });
    }
  }

  handleChange = (name, type, e) => {
    this.props.handleFilterChange(e.target.checked, name, type, this.props.type);
  };

  render() {
    let parsedQuery = this.props.query['custom_fields'];
    if (parsedQuery) {
      try {
        parsedQuery = JSON.parse(this.props.query['custom_fields']);
      } catch (e) {
        console.error('JSON parse error in CusomBlock');
      }
    }
    return (
      <div className="new-search__paper-container">
        <Paper className="new-search__paper new-search__paper_custom-block">
          <div className="filter-title">{tr(this.props.type.replace('_', ' '))}</div>
          <div>
            {this.state.options.map((item, index) => {
              return (
                <div className="outer-checkbox" key={index}>
                  <Checkbox
                    label={`${item.key} (${item.doc_count})`}
                    iconStyle={this.styles.checkboxOuter}
                    labelStyle={this.styles.checkboxOuterLabel}
                    checkedIcon={<CheckOn1 color="#6f708b" />}
                    uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                    onCheck={this.handleChange.bind(this, item.key, 'customField')}
                    value={item.key}
                    checked={
                      parsedQuery &&
                      ~findIndex(
                        parsedQuery,
                        customField => customField.name === this.props.type
                      ) &&
                      ~findIndex(parsedQuery, customField => ~customField.values.indexOf(item.key))
                    }
                  />
                </div>
              );
            })}
          </div>
          <div className="new-search__input-container">
            <UserSearchComponent
              handleFilterChange={this.props.handleFilterChange}
              type={this.type}
              placeholder="By name"
            />
          </div>
          {this.props.hideFilters && <div className="translucent-layer" />}
        </Paper>
      </div>
    );
  }
}

CustomBlockFilterComponent.propTypes = {
  hideFilters: PropTypes.bool,
  type: PropTypes.string,
  options: PropTypes.array,
  handleFilterChange: PropTypes.func,
  query: PropTypes.object
};

export default CustomBlockFilterComponent;
