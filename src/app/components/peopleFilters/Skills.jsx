import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import Checkbox from 'material-ui/Checkbox';
import { tr } from 'edc-web-sdk/helpers/translations';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1';
import CheckOff from 'edc-web-sdk/components/icons/CheckOff';

class SkillsFilterContainer extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      checkboxOuter: {
        width: '0.875rem',
        height: '0.875rem',
        marginRight: '0.5rem',
        marginBottom: '1rem',
        fill: '#6f708b'
      },
      checkboxOuterLabel: {
        lineHeight: '1',
        fontSize: '0.875rem',
        color: '#6f708b'
      },
      uncheckedIcon: {
        width: '0.875rem',
        height: '0.875rem'
      }
    };

    this.state = {
      showViewMoreSkills: true,
      filters: props.skills || []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.skills) {
      this.setState({ filters: nextProps.skills });
    }
  }

  handleChange = (name, type, e) => {
    this.props.handleFilterChange(e.target.checked, name, type);
  };

  showAll() {
    this.setState({
      showViewMoreSkills: !this.state.showViewMoreSkills
    });
  }

  render() {
    let hideSkills = this.state.showViewMoreSkills && this.state.filters.length > 5;
    let displayedFilters;
    if (hideSkills) {
      displayedFilters = this.state.filters.slice(0, 5);
    } else {
      displayedFilters = this.state.filters;
    }
    return (
      <div className="new-search__paper-container">
        <Paper className="new-search__paper">
          <div className="filter-title">
            {tr('Skills')} ({this.state.filters.length})
          </div>
          {displayedFilters && (
            <div>
              {displayedFilters.map((skillFilter, index) => {
                return (
                  <div className="outer-checkbox" key={index}>
                    <Checkbox
                      label={`${skillFilter.key} (${skillFilter.doc_count})`}
                      iconStyle={this.styles.checkboxOuter}
                      labelStyle={this.styles.checkboxOuterLabel}
                      checkedIcon={<CheckOn1 color="#6f708b" />}
                      uncheckedIcon={<CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />}
                      onCheck={this.handleChange.bind(this, skillFilter.key, 'skill')}
                      value={`${skillFilter.key}`}
                      checked={
                        this.props.query['expertises_names[]'] &&
                        ~this.props.query['expertises_names[]'].indexOf(skillFilter.key)
                      }
                    />
                  </div>
                );
              })}
              {hideSkills && (
                <div className="view-more" onClick={this.showAll.bind(this)}>
                  {tr('View all')}
                </div>
              )}
            </div>
          )}
          {this.props.hideFilters && <div className="translucent-layer" />}
        </Paper>
      </div>
    );
  }
}

SkillsFilterContainer.propTypes = {
  isNewStyle: PropTypes.bool,
  hideFilters: PropTypes.bool,
  filters: PropTypes.object,
  handleFilterChange: PropTypes.func,
  skills: PropTypes.array,
  query: PropTypes.object
};

export default SkillsFilterContainer;
