import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import moment from 'moment';
import Calendar from 'material-ui/DatePicker/Calendar';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

class DateJoinedFilterContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: moment(),
      endDate: moment(),
      displayStartCalendar: false,
      displayEndCalendar: false,
      startInputValue: '',
      endInputValue: ''
    };

    this.filterOptions = props.options;
    this.type = props.type;
    this.styles = {
      close: {
        height: '0.75rem',
        width: '0.75rem',
        padding: 0,
        right: '0.75rem'
      },
      iconClose: {
        width: '100%',
        height: '100%'
      }
    };
  }

  openCalendar(target) {
    if (target === 'start') {
      this.setState({
        displayStartCalendar: !this.state.displayStartCalendar,
        displayEndCalendar: false
      });
    } else if (target === 'end') {
      this.setState({
        displayEndCalendar: !this.state.displayEndCalendar,
        displayStartCalendar: false
      });
    }
  }

  chooseDate(value, target) {
    this.setState(
      {
        [target === 'start' ? 'startInputValue' : 'endInputValue']: moment(value).format(
          'YYYY-MM-DD'
        )
      },
      () => {
        this.openCalendar(target);
        this.props.handleFilterChange(
          this.state[target === 'start' ? 'startInputValue' : 'endInputValue'],
          target === 'start' ? 'from_date' : 'to_date',
          'dates'
        );
      }
    );
  }

  handleChange = e => {
    this.props.handleFilterChange(e.target.value, false, this.type);
  };

  clearDate = type => {
    this.setState(
      {
        [type === 'start' ? 'startInputValue' : 'endInputValue']: ''
      },
      () => {
        this.props.handleFilterChange(
          this.state[type === 'start' ? 'startInputValue' : 'endInputValue'],
          type === 'start' ? 'from_date' : 'to_date',
          'dates'
        );
      }
    );
  };

  render() {
    return (
      <div className="new-search__paper-container">
        <Paper className="new-search__paper">
          <div className="filter-title">{tr('Date Joined')}</div>
          <span>{tr('From')}:</span>
          <div className="date-field-container">
            <input
              className="date-input-field"
              value={this.state.startInputValue}
              onClick={this.openCalendar.bind(this, 'start')}
              readOnly
            />
            {this.state.startInputValue && (
              <IconButton
                aria-label="clear date"
                onClick={this.clearDate.bind(this, 'start')}
                style={this.styles.close}
                iconStyle={this.styles.iconClose}
              >
                <CloseIcon color="black" />
              </IconButton>
            )}
          </div>
          {this.state.displayStartCalendar && (
            <div className="calendar-container">
              <Calendar
                autoOk={false}
                cancelLabel={false}
                firstDayOfWeek={1}
                onTouchTapDay={(event, date) => this.chooseDate(date, 'start')}
                mode={'portrait'}
                open={true}
                date={this.state.endDate}
                maxDate={new Date(this.state.endInputValue)}
              />
            </div>
          )}
          <span>{tr('to')}:</span>
          <div className="date-field-container">
            <input
              className="date-input-field date-input-field__last"
              value={this.state.endInputValue}
              onClick={this.openCalendar.bind(this, 'end')}
              readOnly
            />
            {this.state.endInputValue && (
              <IconButton
                aria-label="clear date"
                onClick={this.clearDate.bind(this, 'end')}
                style={this.styles.close}
                iconStyle={this.styles.iconClose}
              >
                <CloseIcon color="black" />
              </IconButton>
            )}
          </div>
          {this.state.displayEndCalendar && (
            <div className="calendar-container">
              <Calendar
                autoOk={false}
                cancelLabel={false}
                firstDayOfWeek={1}
                onTouchTapDay={(event, date) => this.chooseDate(date, 'end')}
                mode={'portrait'}
                open={true}
                date={this.state.endDate}
                minDate={new Date(this.state.startInputValue)}
              />
            </div>
          )}
          {this.props.hideFilters && <div className="translucent-layer" />}
        </Paper>
      </div>
    );
  }
}

DateJoinedFilterContainer.propTypes = {
  options: PropTypes.object,
  hideFilters: PropTypes.bool,
  handleFilterChange: PropTypes.func,
  type: PropTypes.str
};

export default DateJoinedFilterContainer;
