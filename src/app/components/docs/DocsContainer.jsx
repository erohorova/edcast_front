import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class DocsContainer extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {}

  render() {
    return (
      <div id="docs-container">
        <div className="row">
          <div className="small-12 columns">
            <div className="docs">
              <div className="docs-subsection">
                <br />
                <h3 className="text-center">{tr('Getting started with REST API')}</h3>
              </div>

              <div className="docs-subsection">
                <div className="text-center resource-heading">{tr('Data Formats')}</div>
                <div className="content">
                  <div className="resource-sub-heading">{tr('Requesting data from the APIs')}</div>
                  <div className="resource-paragraph">
                    {tr(
                      "Once necessary parameters are passed in, all of EdCast's APIs will return the information that you request in the JSON data format."
                    )}
                    '
                  </div>
                  <div className="api-url-container text-center">
                    <b className="api-name"> Search cards </b>
                    <div className="api-url">
                      <div className="api-request">
                        <pre className="text-left">
                          <b>GET</b>{' '}
                          https://www.edcast.com/api/developer/content_items?q=`YOUR-SEARCH-KEYWORD`
                          <br />
                          <b>X-API-TOKEN:</b> YOUR-GENERATED-TOKEN
                          <br />
                          <b>X-Edcast-JWT:</b> 1
                          <br />
                          <b>Content-Type:</b> application/json
                          <br />
                          <b>Parameters: </b>
                          {JSON.stringify({ limit: 20, offset: 0 }, null, 2)}
                        </pre>
                      </div>
                    </div>
                  </div>

                  <div className="json-response">
                    <div className="response-header text-center">{tr('Sample JSON response')}</div>
                    <div className="response-content">
                      <pre>
                        {JSON.stringify(
                          {
                            results: [
                              {
                                _id: '63808',
                                _index: 'cards_staging_20160714184318895',
                                _score: 0.92799544,
                                _type: 'card',
                                card_template: 'text',
                                card_type: 'course',
                                channel_ids: [],
                                content: {
                                  link_description:
                                    'Learn and practice the basic principles of running...',
                                  link_image_url: null,
                                  link_site: null,
                                  link_title: 'Fundamentals of Rehearsing Music Ensembles',
                                  link_url: 'https://www.coursera.org/learn/music-ensembles',
                                  link_video_url: null,
                                  message:
                                    'Learn and practice the basic principles of running an effective...',
                                  title: 'Fundamentals of Rehearsing Music Ensembles'
                                },
                                ecl: {
                                  coherence: 0,
                                  origin: 'COURSERA'
                                },
                                id: 63808,
                                title: 'Fundamentals of Rehearsing Music Ensembles',
                                message:
                                  'Learn and practice the basic principles of running an effective...',
                                slug: 'fundamentals-of-rehearsing-music-ensembles',
                                organization_id: 15,
                                resource_id: 61214,
                                state: 'published',
                                tags: [],
                                created_at: '2016-09-08T09:33:32.000Z',
                                updated_at: '2016-09-08T09:33:32.000Z',
                                published_at: '2016-09-08T09:33:32.000Z'
                              }
                            ],
                            facets: {
                              card_type: {
                                doc_count: 92,
                                doc_count_error_upper_bound: 0,
                                sum_other_doc_count: 0,
                                buckets: [
                                  {
                                    key: 'media',
                                    doc_count: 88
                                  },
                                  {
                                    key: 'course',
                                    doc_count: 4
                                  }
                                ]
                              },
                              'ecl.origin': {
                                doc_count: 92,
                                doc_count_error_upper_bound: 0,
                                sum_other_doc_count: 0,
                                buckets: [
                                  {
                                    key: 'ARTICLE',
                                    doc_count: 23
                                  },
                                  {
                                    key: 'Coursera',
                                    doc_count: 3
                                  },
                                  {
                                    key: 'COURSERA',
                                    doc_count: 1
                                  },
                                  {
                                    key: 'YOUTUBE',
                                    doc_count: 1
                                  }
                                ]
                              },
                              card_template: {
                                doc_count: 92,
                                doc_count_error_upper_bound: 0,
                                sum_other_doc_count: 0,
                                buckets: [
                                  {
                                    key: 'link',
                                    doc_count: 86
                                  },
                                  {
                                    key: 'text',
                                    doc_count: 4
                                  },
                                  {
                                    key: 'video',
                                    doc_count: 2
                                  }
                                ]
                              }
                            }
                          },
                          null,
                          2
                        )}
                      </pre>
                    </div>
                  </div>

                  <br />
                  <br />
                  <br />
                  <br />
                  <div className="api-url-container text-center">
                    <b className="api-name">{tr('Card push')}</b>
                    <div className="api-url">
                      <div className="api-request">
                        <pre className="text-left">
                          <b>POST</b> https://www.edcast.com/api/developer/content_items
                          <br />
                          <b>X-API-TOKEN: </b> YOUR-GENERATED-TOKEN
                          <br />
                          <b>X-Edcast-JWT: </b> 1
                          <br />
                          <b>Content-Type: </b> application/json
                          <br />
                          <b>Parameters: </b>
                          {JSON.stringify(
                            {
                              name: 'Selenium',
                              description: 'Selenium with 3 Ailments',
                              url: 'https://www.youtube.com/watch?v=mMPi2-FX8qM',
                              tags: ['Automation', 'Selenium'],
                              content_source: {
                                source_type: 'youtube'
                              },
                              channel_ids: [659]
                            },
                            null,
                            2
                          )}
                        </pre>
                      </div>
                    </div>
                  </div>

                  <div className="json-response">
                    <div className="response-header text-center">{tr('Sample JSON response')}</div>
                    <div className="response-content">
                      <pre>
                        {JSON.stringify(
                          {
                            id: 118620
                          },
                          null,
                          2
                        )}
                      </pre>
                    </div>
                  </div>
                </div>
                <br />
                <br />
              </div>

              <div className="docs-subsection">
                <div className="text-center resource-heading">
                  {tr('Sending parameters to the APIs')}
                </div>
                <div className="content">
                  <div className="resource-sub-heading">
                    {tr('Understanding parameters being sent to APIs')}
                  </div>
                  <div className="resource-paragraph">
                    {tr(
                      'Search Cards API support more parameters to get narrowed resultset. There are several card types being supported. Each card is pulled from different sources.'
                    )}
                    {tr('Following is the structure that explains this very well.')}
                  </div>
                  <div className="resource-paragraph">
                    <table className="api-parameters">
                      <th>
                        <td>origins[]</td>
                        <td>{tr('Types of card returned')}</td>
                      </th>
                      <tr>
                        <td>
                          <span>COURSERA</span>
                          <span>UDACITY</span>
                          <span>CROSS KNOWLEDGE</span>
                          <span>UDEMY API</span>
                          <span>EDX</span>
                          <span>KHANACADEMY</span>
                          <span>LYNDA</span>
                          <span>TUTSPLUS</span>
                          <span>SKILLSHARE</span>
                          <span>PLURALSIGHT</span>
                        </td>
                        <td>
                          <span>{tr('Courses')}</span>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <span>{tr('ARTICLE')}</span>
                          <span>{tr('BOOK')}</span>
                        </td>
                        <td>
                          <span>{tr('Articles (from RSS) and Books (from misc sources)')}</span>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <span>
                            <i>{tr('not provided')}</i>
                          </span>
                        </td>
                        <td>
                          <span>{tr('Pathways')}</span>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <span>{tr('GET_ABSTRACT')}</span>
                        </td>
                        <td>
                          <span>{tr('getAbstract Content')}</span>
                        </td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DocsContainer.propTypes = {};

export default connect()(DocsContainer);
