import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import Paper from 'edc-web-sdk/components/Paper';
import { getCuratedChannels, getCurateCards } from 'edc-web-sdk/requests/curate';
import colors from 'edc-web-sdk/components/colors/index';

import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import Insight from '../Insight';
import InsightCurate from './InsightCurate';
import ListView from '../../common/ListView';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';

const ugcFilter = ['All', 'User Generated Cards'];
class CurateContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channels: [],
      curatedChannels: [],
      curatedCards: [],
      pending: true,
      totalCards: 0,
      isLastPage: false,
      limit: 15,
      ugcSelectedValue: 'All',
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
    this.backToFeedClickHandler = this.backToFeedClickHandler.bind(this);
  }

  componentDidMount() {
    calculateTeemFeedPosition();
    getCuratedChannels()
      .then(data => {
        if (data.length > 0) {
          let curatedChannels = data;

          this.setState({
            curatedChannels: curatedChannels
          });
          let channelId = !!curatedChannels.length && curatedChannels[0].id;
          this.getChannelCounts(curatedChannels);
          if (channelId) {
            getCurateCards(channelId, {
              limit: this.state.limit + 1,
              filter_by_language: this.state.multilingualContent,
              channel_card_state: 'new'
            })
              .then(channelCards => {
                let cards = channelCards.cards;
                this.setState(
                  {
                    curatedChannels: curatedChannels,
                    isLastPage: cards.length < this.state.limit + 1,
                    curatedCards: cards.slice(0, this.state.limit),
                    currentChannel: curatedChannels[0].id,
                    totalCards: channelCards.total,
                    pending: false
                  },
                  calculateTeemFeedPosition
                );
                this.getStream();
                setActivityListHeight(this.state.homepageVersion, 'curate');
              })
              .catch(() => {
                this.setState({
                  pending: false,
                  isLastPage: true
                });
              });
          } else {
            this.setState({
              pending: false,
              isLastPage: true
            });
          }
        } else {
          this.setState({
            pending: false,
            isLastPage: true
          });
        }
      })
      .catch(err => {
        console.error(`Error in CurateContainer.getCuratedChannels.func : ${err}`);
      });
  }

  getChannelCounts = channels => {
    let hash = {};
    channels.forEach(channel => {
      getCurateCards(channel.id, {
        filter_by_language: this.state.multilingualContent,
        channel_card_state: 'new'
      })
        .then(cards => {
          hash[channel.id] = { total: cards.total, label: channel.label };
          setActivityListHeight(this.state.homepageVersion, 'curate');
        })
        .then(() => {
          this.setState({ channelsHash: hash });
        })
        .catch(err => {
          console.error(`Error in CurateContainer.getChannelCounts.getCurateCards.func : ${err}`);
        });
    });
  };
  backToFeedClickHandler() {
    this.props.dispatch(push('/'));
  }

  filterChange = (e, index, value) => {
    this.setState({ pending: true }, () => {
      this.filterActionCall(e, value);
    });
  };
  filterForUgcCards = (e, index, value) => {
    this.setState({ pending: true }, () => {
      this.filterActionCall(e, this.state.currentChannel, true, value);
    });
  };
  filterActionCall = (e, value, calledFromUgcFilter, ugcVal) => {
    let payload;
    let ugcSelectedValue = calledFromUgcFilter ? ugcVal : this.state.ugcSelectedValue;
    ugcSelectedValue == 'User Generated Cards'
      ? (payload = { limit: this.state.limit + 1, ugc: true })
      : (payload = { limit: this.state.limit + 1, ugc: false });
    payload.filter_by_language = this.state.multilingualContent;
    payload.channel_card_state = 'new';
    getCurateCards(value, payload)
      .then(channelCards => {
        let cards = channelCards.cards;
        this.setState(
          {
            curatedCards: cards.slice(0, this.state.limit),
            isLastPage: cards.length <= this.state.limit,
            channelName: e.target.innerHTML,
            currentChannel: value,
            totalCards: channelCards.total,
            pending: false,
            ugcSelectedValue
          },
          calculateTeemFeedPosition
        );
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'curate');
      })
      .catch(err => {
        console.error(`Error in CurateContainer.filterActionCall.getCurateCards.func : ${err}`);
      });
  };
  viewMoreClickHandler = () => {
    let isUgc;
    this.state.ugcSelectedValue == 'User Generated Cards' ? (isUgc = true) : (isUgc = false);
    getCurateCards(this.state.currentChannel, {
      offset: this.state.curatedCards.length,
      limit: this.state.limit + 1,
      ugc: isUgc,
      filter_by_language: this.state.multilingualContent,
      channel_card_state: 'new'
    })
      .then(channelCards => {
        let cards = channelCards.cards;
        this.setState(
          {
            isLastPage: cards.length < this.state.limit + 1,
            curatedCards: this.state.curatedCards.concat(cards.slice(0, this.state.limit)),
            pending: false,
            totalCards: channelCards.total
          },
          calculateTeemFeedPosition
        );
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'curate');
      })
      .catch(err => {
        console.error(`Error in CurateContainer.viewMoreClickHandler.getCurateCards.func : ${err}`);
      });
  };

  removeFromList = (cardId, channelId) => {
    let newCuratedCards = this.state.curatedCards.filter(card => card.id !== cardId);
    let newChannelsHash = Object.assign({}, this.state.channelsHash);
    newChannelsHash[channelId]['total']--;
    this.setState(
      {
        curatedCards: newCuratedCards,
        totalCards: this.state.totalCards - 1,
        channelsHash: newChannelsHash
      },
      () => {
        calculateTeemFeedPosition();
        if (newCuratedCards.length === 0) {
          this.setState({ pending: true });
          this.viewMoreClickHandler();
        }
        setActivityListHeight(this.state.homepageVersion, 'curate');
      }
    );
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(175, true, 'curate'));
      }
      setActivityListHeight(this.state.homepageVersion, 'curate');
    }
  }

  componentDidUpdate() {
    setActivityListHeight(this.state.homepageVersion, 'curate');
  }

  render() {
    return (
      <div>
        {this.state.curatedChannels.length > 0 && (
          <div className="curate-channel-filter container-padding">
            <div className="horizontal-spacing-large">
              <span className="select-bar-title">{tr('Curate content for channel')}</span>
              <SelectField
                style={{ verticalAlign: 'middle' }}
                iconStyle={{
                  width: '1.87rem',
                  height: '1.87rem',
                  top: '0.437rem',
                  fill: colors.silverSand
                }}
                maxHeight={200}
                onChange={this.filterChange}
                value={this.state.currentChannel}
              >
                {this.state.curatedChannels.map(channel => {
                  return (
                    <MenuItem
                      value={channel.id}
                      primaryText={
                        channel.label +
                        (this.state.channelsHash &&
                        this.state.channelsHash[channel.id] !== undefined &&
                        this.state.channelsHash[channel.id]['total'] !== undefined
                          ? ` (${this.state.channelsHash[channel.id]['total']})`
                          : '')
                      }
                      key={channel.id}
                    />
                  );
                })}
              </SelectField>
              {/* EP-18345 : Removed unnecessary count
                {this.state.totalCards !== 0 && <div className="total-curate-count" style={{color: colors.primary300}}>{this.state.totalCards === 1 ? (this.state.totalCards + ' Item') : (this.state.totalCards + ' Items')} </div>}
              */}
              {this.state.currentChannel && (
                <div className="total-curate-count">
                  <span className="select-bar-title">{tr('Curate')}</span>
                  <SelectField
                    style={{ verticalAlign: 'middle' }}
                    iconStyle={{
                      width: '1.87rem',
                      height: '1.87rem',
                      top: '0.437rem',
                      fill: colors.silverSand
                    }}
                    maxHeight={200}
                    onChange={this.filterForUgcCards}
                    value={this.state.ugcSelectedValue}
                  >
                    {ugcFilter.map((type, index) => {
                      return <MenuItem value={tr(type)} primaryText={tr(type)} key={index} />;
                    })}
                  </SelectField>
                </div>
              )}
            </div>
          </div>
        )}
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          emptyMessage={
            <div className="data-not-available-msg">
              {this.state.curatedChannels.length == 0 &&
                tr(`Enjoy your day and check back later! There is no content to display.`)}
            </div>
          }
          isLastPage={this.state.isLastPage}
          onViewMore={this.viewMoreClickHandler}
        >
          {!this.state.pending &&
            this.state.curatedCards.map(card => {
              return (
                <Paper key={card.id}>
                  <Insight card={card} isActionsDisabled={true} />
                  <Divider />
                  <InsightCurate
                    cardId={card.id}
                    card={card}
                    channelId={this.state.currentChannel}
                    channelName={this.state.channelsHash[this.state.currentChannel]['label']}
                    skipPublishCallback={this.removeFromList}
                  />
                </Paper>
              );
            })}
        </ListView>
      </div>
    );
    // FIXME: Unreachable code
    /*eslint no-unreachable: "off"*/
    setActivityListHeight(this.state.homepageVersion, 'curate');
  }
}

CurateContainer.propTypes = {
  users: PropTypes.object,
  cards: PropTypes.object,
  curate: PropTypes.object
};

function mapStoreStateToProps(state) {
  return { curate: state.curate.toJS(), cards: state.cards.toJS(), users: state.users.toJS() };
}

export default connect(mapStoreStateToProps)(CurateContainer);
