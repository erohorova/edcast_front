import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';

//Icons
import PublishIcon from 'material-ui/svg-icons/editor/publish';
import ArchiveIcon from 'material-ui/svg-icons/content/archive';
//actions
import { curateItem, skipItem } from '../../../actions/curateActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class InsightCurate extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      skip: false,
      curate: false
    };
  }

  curateClickHandler = state => {
    this.setState({ [state]: true });
    let func = {
      skip: skipItem,
      curate: curateItem
    };
    this.props
      .dispatch(func[state](this.props.cardId, this.props.channelId, this.props.channelName))
      .then(() => {
        this.setState({ [state]: false }, () => {
          this.props.skipPublishCallback(this.props.cardId, this.props.channelId);
        });
      })
      .catch(err => {
        console.error(`Error in InsightCurate.curateClickHandler.func : ${err}`);
      });
  };

  render() {
    return (
      <div className="container-padding clearfix">
        <PrimaryButton
          className="float-right"
          label={tr('Publish')}
          icon={<PublishIcon />}
          pending={this.state.curate}
          pendingLabel={tr('Publishing...')}
          onTouchTap={() => {
            this.curateClickHandler('curate');
          }}
        />
        <SecondaryButton
          className="float-right"
          label={tr('Skip')}
          icon={<ArchiveIcon />}
          pending={this.state.skip}
          pendingLabel={tr('Skipping...')}
          onTouchTap={() => {
            this.curateClickHandler('skip');
          }}
        />
      </div>
    );
  }
}

InsightCurate.propTypes = {
  cardId: PropTypes.string,
  channelId: PropTypes.string,
  card: PropTypes.object,
  channelName: PropTypes.string,
  skipPublishCallback: PropTypes.func
};

export default connect()(InsightCurate);
