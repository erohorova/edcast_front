import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { getInsights } from '../../actions/feedActions';
import Channel from '../discovery/Channel.jsx';
import Influencer from '../discovery/Influencer.jsx';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';

class EmptyFeedContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channels: this.props.channels || [],
      channelsOffset: 4,
      users: this.props.users || [],
      usersOffset: 4
    };
  }

  removeItem = (type, id) => {
    let newArray = this.state[type].filter(obj => obj.id !== id);
    this.setState({ [type]: newArray });
  };

  render() {
    let moreUsers = this.state.users.length > this.state.usersOffset;
    let moreChannels = this.state.channels.length > this.state.channelsOffset;
    let defaultUserImage = '/i/images/default_user_blue.png';
    return (
      <div>
        <Paper className="empty-feed-container">
          <div className="container-padding vertical-spacing-medium">
            <div className="text-center title-block">
              <h6>{tr('Follow Subject Matter Experts (SME) and Channels')}</h6>

              {this.props.currentUser.followingUsers &&
                this.props.currentUser.followingChannels &&
                (this.props.currentUser.followingUsers.length == 0 ||
                  this.props.currentUser.followingChannels == 0) &&
                (this.props.channels.length == 0 && this.props.users.length == 0) && (
                  <div>
                    {tr(
                      'Your Team is not setup completely. Please ask your team administrator to add some Channels and invite a few Subject Matter Experts (SME).'
                    )}
                  </div>
                )}
              {this.props.currentUser.followingUsers &&
                this.props.currentUser.followingChannels &&
                (this.props.currentUser.followingUsers.length == 0 ||
                  this.props.currentUser.followingChannels == 0) &&
                (this.props.channels.length > 0 || this.props.users.length > 0) && (
                  <div>
                    {this.props.currentUser.name ? this.props.currentUser.name + ', ' : ''}{' '}
                    {tr(
                      'Your feed is currently empty. Please follow a few Subject Matter Experts (SME) and Channels.'
                    )}
                  </div>
                )}
            </div>
            <div className="container-padding vertical-spacing-large channel-user">
              <div className="user-container vertical-spacing-large">
                <div className="row">
                  {this.state.users.slice(0, this.state.usersOffset).map(user => {
                    let userAvatar =
                      user.pictureUrl ||
                      (user.avatarimages && user.avatarimages.small) ||
                      defaultUserImage;
                    return (
                      <div className="custom-grid columns" key={user.id}>
                        <Influencer
                          id={user.id}
                          name={user.name}
                          handle={user.handle}
                          imageUrl={userAvatar}
                          interests={user.interests}
                          role={user.role}
                          rolesDefaultNames={user.rolesDefaultNames}
                        />
                      </div>
                    );
                  })}
                </div>
                {moreUsers && this.state.users.length > 4 && (
                  <div className="text-center viewMore">
                    <a
                      href="#"
                      tabIndex={0}
                      onClick={e => {
                        e.preventDefault();
                        this.setState({ usersOffset: this.state.usersOffset + 4 });
                      }}
                    >
                      {tr('View More')}
                    </a>
                  </div>
                )}
              </div>
              <div className="vertical-spacing-large">
                <div className="row">
                  {this.state.channels.slice(0, this.state.channelsOffset).map(channel => {
                    return (
                      <div className="custom-grid columns" key={channel.id}>
                        <Channel
                          channel={channel}
                          id={channel.id}
                          title={channel.label}
                          imageUrl={
                            channel.imageUrl || channel.profileImageUrl || channel.bannerImageUrl
                          }
                          slug={channel.slug}
                          allowFollow={channel.allowFollow}
                        />
                      </div>
                    );
                  })}
                </div>
                {moreChannels && this.state.channels.length > 4 && (
                  <div className="text-center viewMore">
                    <a
                      href="#"
                      tabIndex={0}
                      onClick={e => {
                        e.preventDefault();
                        this.setState({ channelsOffset: this.state.channelsOffset + 4 });
                      }}
                    >
                      {tr('View More')}
                    </a>
                  </div>
                )}
              </div>
            </div>
            {(this.props.channels.length > 1 || this.props.users.length > 0) && (
              <div className="text-center">
                <PrimaryButton
                  tabIndex={0}
                  label={tr('Done')}
                  className="viewMore"
                  onTouchTap={() => {
                    this.props.getCards();
                  }}
                />
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

EmptyFeedContainer.propTypes = {
  users: PropTypes.array,
  getCards: PropTypes.func,
  currentUser: PropTypes.object,
  channels: PropTypes.array
};

export default EmptyFeedContainer;
