import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CommentItem from './CommentItem.v2';
import CommentInput from './CommentInput.v2';
import { loadComments } from '../../actions/cardsActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import { Permissions } from '../../utils/checkPermissions';

class CommentList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showAll: false,
      comments: props.comments || [],
      inModal: props.inModal
    };
  }

  componentDidMount() {
    if (!this.props.overViewModal) {
      let nIntervId;
      nIntervId = setInterval(this.checkHeight, 1000);
      setTimeout(function() {
        clearInterval(nIntervId);
      }, 10000);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pathwayPreview && nextProps.cardId != this.props.cardId) {
      this.setState({
        comments: nextProps.comments || [],
        inModal: nextProps.inModal
      });
    }
  }

  checkHeight = () => {
    var cardContainerEle = document.getElementsByClassName('card-modal-left-column');
    var headerEle = document.getElementsByClassName('modal-header');
    if (cardContainerEle.length > 0) {
      var height = cardContainerEle[0].offsetHeight;
      var commentsList = document.getElementsByClassName('comment-list-container inside-modal');
      var finalHeight = height - 165;
      if (commentsList.length > 0) {
        commentsList[0].setAttribute(
          'style',
          `height: ${finalHeight}px; max-height: ${finalHeight}px; opacity: 1;`
        );
      }

      if (commentsList.length > 0 && (headerEle.length == 0 || finalHeight > 382)) {
        commentsList[0].style.paddingBottom = '55px';
      }
    }
  };

  viewMoreClickHandler = () => {
    loadComments(
      this.props.cardId,
      this.props.numOfComments,
      this.props.cardType,
      this.props.videoId
    )
      .then(data => {
        this.setState({ comments: data, showAll: true });
      })
      .catch(err => {
        console.error(`Error in CommentList.v2.loadComments.func : ${err}`);
      });
  };

  updateCommentList = newComments => {
    this.setState({ comments: newComments, showAll: true }, () => {
      this.props.updateCommentCount();
      let element = document.getElementById('commentsContainer2');
      element.scrollTop = element.scrollHeight;
    });
  };

  render() {
    let comments = this.state.comments.sort((a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });
    if (this.state.showAll === false) {
      comments = comments.slice(-3);
    }
    return (
      <div className="comment-list">
        <div
          id="commentsContainer2"
          className="inside-modal comment-list-container vertical-spacing-large "
        >
          {this.props.numOfComments > 3 && !this.state.showAll && (
            <div>
              <small>
                <a className="view-more-comments" onTouchTap={this.viewMoreClickHandler}>
                  {tr('View')} {this.props.numOfComments - 3}{' '}
                  {this.props.numOfComments - 3 > 1 ? tr('more comments') : tr('comment')}
                </a>
              </small>
            </div>
          )}
          {this.props.pending && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
          {comments.map((comment, index) => {
            return (
              <CommentItem
                key={comment.id}
                cardId={this.props.cardId}
                comment={comment}
                user={comment.user}
                isOwn={
                  comment.user.id === this.props.currentUser.id ||
                  this.props.cardOwner == this.props.currentUser.id ||
                  this.props.currentUser.isAdmin
                }
                canReport={
                  comment.user.id != this.props.currentUser.id || this.props.currentUser.isAdmin
                }
                commentPostCallback={this.props.updateCommentCount}
                hideCommentModal={this.props.hideCommentModal}
                showConfirmationModal={this.props.showConfirmationModal}
                isCanLike={Permissions.has('LIKE_CONTENT')}
              />
            );
          })}
        </div>
        <CommentInput
          isSlideout={this.props.isSlideout}
          isPinned={this.props.isPinned}
          cardId={this.props.cardId}
          cardType={this.props.cardType}
          autoFocus={false}
          numOfComments={this.state.comments.length}
          commentPostCallback={this.updateCommentList}
          slideOutAvatarBoxStyle={this.props.slideOutAvatarBoxStyle}
          isCardV3={this.props.isCardV3}
        />
      </div>
    );
  }
}

CommentList.propTypes = {
  currentUser: PropTypes.object,
  cardId: PropTypes.string,
  cardType: PropTypes.string,
  cardOwner: PropTypes.any,
  videoId: PropTypes.string,
  numOfComments: PropTypes.number,
  pending: PropTypes.bool,
  isPinned: PropTypes.bool,
  pathwayPreview: PropTypes.bool,
  users: PropTypes.object,
  comments: PropTypes.array,
  inModal: PropTypes.bool,
  overViewModal: PropTypes.bool,
  hideCommentModal: PropTypes.bool,
  updateCommentCount: PropTypes.func,
  showConfirmationModal: PropTypes.func,
  slideOutAvatarBoxStyle: PropTypes.func,
  isSlideout: PropTypes.bool,
  isCardV3: PropTypes.bool
};

CommentList.defaultProps = {
  hideCommentModal: false,
  showConfirmationModal: function() {}
};

function mapStoreStateToProps(state) {
  return {
    users: state.users.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CommentList);
