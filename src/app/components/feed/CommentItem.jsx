import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListItem from 'material-ui/List/ListItem';
import { tr } from 'edc-web-sdk/helpers/translations';
import Avatar from 'edc-web-sdk/components/Avatar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import colors from 'edc-web-sdk/components/colors/index';
import extractMentions from '../../utils/extractMentions';
import TimeAgo from 'react-timeago';
import translateTA from '../../utils/translateTimeAgo';
import UserProfileHover from '../common/UserProfileHover';

// Material UI Icons
import KeyboardArrowDownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';

//actions
import { deleteComment } from 'edc-web-sdk/requests/cards';
import { toggleLikeComment } from '../../actions/cardsActions';
import * as modalActions from '../../actions/modalActions';
import Linkify from 'react-linkify';

class CommentItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isEditing: false,
      upVoted: this.props.comment.isUpvoted,
      votesCount: this.props.comment.votesCount || 0,
      visible: true
    };
    this.likeClickHandler = this.likeClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.styles = {
      listItem: {
        padding: '0 56px 0 72px',
        margin: '0 -1rem'
      }
    };
  }

  likeClickHandler = e => {
    e.preventDefault();
    toggleLikeComment(this.props.cardId, this.props.comment.id, !this.state.upVoted)
      .then(data => {
        this.setState({
          upVoted: !this.state.upVoted,
          votesCount: data.upvoted ? this.state.votesCount + 1 : this.state.votesCount - 1
        });
      })
      .catch(err => {
        console.error(`Error in CommentItem.toggleLikeComment.func : ${err}`);
      });
  };

  deleteClickHandler = () => {
    this.props.dispatch(
      modalActions.confirmation('Confirm', 'Do you want to delete the comment?', () => {
        deleteComment(this.props.cardId, this.props.comment.id)
          .then(() => {
            this.setState({ visible: false }, () => {
              this.props.commentPostCallback(-1);
            });
          })
          .catch(err => {
            console.error(`Error in CommentItem.deleteComment.func : ${err}`);
          });
      })
    );
  };

  render() {
    if (!this.state.visible) {
      return null;
    }
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);

    let user = this.props.user;
    let message = extractMentions(this.props.comment.message);
    let deleteDropdown = null;
    if (this.props.isOwn) {
      deleteDropdown = (
        <IconMenu
          iconButtonElement={
            <IconButton style={{ marginTop: '-12px' }}>
              <KeyboardArrowDownIcon color={colors.silverSand} />
            </IconButton>
          }
        >
          <MenuItem className="delete" onTouchTap={this.deleteClickHandler}>
            <small>Delete</small>
          </MenuItem>
        </IconMenu>
      );
    }
    return (
      <div>
        <ListItem
          disabled
          style={this.styles.listItem}
          leftAvatar={<Avatar user={user} />}
          primaryText={
            <div className="break-comment">
              <small>
                <strong>
                  <a className="matte user" href={'/' + user.handle}>{`${user.firstName} ${
                    user.lastName
                  }`}</a>
                </strong>
                <span> &#x25cf; </span>
                <TimeAgo
                  date={new Date(this.props.comment.createdAt)}
                  live={false}
                  formatter={formatter}
                />
              </small>
            </div>
          }
          rightIconButton={deleteDropdown}
        />
        <ListItem
          disabled
          insetChildren={true}
          style={this.styles.listItem}
          primaryText={
            <small>
              {message.map((part, index) => {
                if (part.type === 'text') {
                  return (
                    <span key={index}>
                      <Linkify properties={{ target: '_blank' }}>{part.text}</Linkify>
                    </span>
                  );
                }
                if (part.type === 'handle') {
                  let commenter = this.props.comment.mentions.filter(
                    mention => mention.handle == part.handle
                  );
                  if (commenter.length) {
                    return (
                      <UserProfileHover user={commenter[0]} userProfileURL={`/${part.handle}`} />
                    );
                  } else {
                    return <span key={index}>{part.handle}</span>;
                  }
                }
              })}
            </small>
          }
        />
        {this.props.comment.id && (
          <ListItem
            disabled
            insetChildren={true}
            style={this.styles.listItem}
            primaryText={
              <div>
                <small>
                  <a href="#" className="like" onClick={this.likeClickHandler}>
                    {this.state.upVoted ? tr('Liked') : tr('Like')}
                  </a>
                </small>
                <small> {this.state.votesCount || ''}</small>
              </div>
            }
          />
        )}
      </div>
    );
  }
}

CommentItem.propTypes = {
  cardId: PropTypes.string,
  users: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  commentPostCallback: PropTypes.func,
  isOwn: PropTypes.bool,
  comment: PropTypes.shape({
    message: PropTypes.string,
    userId: PropTypes.string,
    mentions: PropTypes.any,
    isUpvoted: PropTypes.bool,
    votesCount: PropTypes.number,
    createdAt: PropTypes.string,
    id: PropTypes.string
  })
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CommentItem);
