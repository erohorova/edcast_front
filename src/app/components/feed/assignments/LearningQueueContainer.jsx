import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getLearningQueueItems } from 'edc-web-sdk/requests/feed';
import ListView from '../../common/ListView';
import { push } from 'react-router-redux';
import find from 'lodash/find';
import without from 'lodash/without';
import throttle from 'lodash/throttle';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../../cards/Card';

class LearningQueueContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cards: [],
      isLastPage: false,
      limit: 20,
      filterType: 'all'
    };

    this.filterOptions = [
      { value: 'all', text: 'All', source: 'all' },
      { value: 'active', text: 'Active Assignments', source: 'assignments', state: 'active' },
      { value: 'completed', text: 'Completed Items', source: 'assignments', state: 'completed' },
      { value: 'bookmarks', text: 'Bookmarks', source: 'bookmarks' }
    ];

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreCards = this.showMoreCards.bind(this);
  }

  componentDidMount() {
    this.handleRouteParams(this.props.routeParams.filter);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreCards();
        }
      }
    },
    150,
    { leading: false }
  );

  componentWillReceiveProps(nextProps) {
    if (nextProps.routeParams.filter !== this.props.routeParams.filter) {
      this.handleRouteParams(nextProps.routeParams.filter);
    }
    this.checkCardList();
  }

  checkCardList() {
    let cards = this.state.cards.filter(card => card && !card.featuredHidden);
    this.setState({ cards });
  }

  handleRouteParams = param => {
    let currentFilterData = this.filterOptions.filter(option => option.value === param)[0];
    let filter = currentFilterData ? param : 'all';
    filter = filter == 'all' ? undefined : filter; // Need to reset to undefined
    let source = currentFilterData ? currentFilterData['source'] : filter;
    let states = currentFilterData ? [currentFilterData['state']] : [];
    this.setState({ pending: true, filterType: filter, cardIds: [] }, () => {
      getLearningQueueItems({ limit: this.state.limit, 'source[]': [source], 'state[]': states })
        .then(data => {
          let cards = data.learningQueueItems.map(card => card.queueable);
          let isLastPage = cards.length < this.state.limit;
          if (!isLastPage) {
            document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
              'calc(100vh - 11.875rem)';
          }
          this.setState({ pending: false, isLastPage, cards });
        })
        .catch(err => {
          console.error(`Error in LearningQueueContainer.getLearningQueueItems.func : ${err}`);
        });
    });
  };

  showMoreCards() {
    let currentFilterData = this.filterOptions.filter(
      option => option.value === this.state.filterType
    )[0];
    let filter = this.state.filterType == 'all' ? undefined : filter; // Need to reset to undefined
    let source = currentFilterData ? currentFilterData['source'] : filter;
    let states = currentFilterData ? [currentFilterData['state']] : [];
    this.setState({ pending: true }, () => {
      getLearningQueueItems({
        limit: this.state.limit,
        offset: this.state.cards.length,
        'state[]': states,
        'source[]': [source]
      })
        .then(data => {
          let cards = data.learningQueueItems.map(card => card.queueable);
          let isLastPage = cards.length < this.state.limit;
          this.setState({
            pending: false,
            isLastPage: isLastPage,
            cards: [...this.state.cards, ...cards]
          });
        })
        .catch(err => {
          console.error(
            `Error in LearningQueueContainer.showMoreCards.getLearningQueueItems.func : ${err}`
          );
        });
    });
  }

  changeLearningQueueState = filterValue => {
    this.props.dispatch(push(`/me/learning/${filterValue}`));
  };

  removeCardFromList = id => {
    let findCard = find(this.state.cards, item => item && item.id == id && item.isBookmarked);
    if (findCard) {
      let newCardsList = without(this.state.cards, findCard);
      this.setState({ cards: newCardsList });
    }
  };

  cardUpdated = card => {
    let newCardsList = this.state.cards;
    let findCard = find(newCardsList, item => item && item.id == card.id);
    let indexUpdatedCard = newCardsList.indexOf(findCard);
    newCardsList[indexUpdatedCard] = card;
    this.setState({ cards: newCardsList });
  };

  render() {
    return (
      <div id="feed" className="row">
        <div className="vertical-spacing-large">
          <ListView
            pending={this.state.pending}
            filterValue={this.state.filterType || 'all'}
            emptyMessage={
              <div className="data-not-available-msg">
                {this.props.currentUser.name},{' '}
                {tr('there are currently no pending learning items at this time.')}
              </div>
            }
            filterOptions={this.filterOptions}
            handleFilterChange={this.changeLearningQueueState}
          >
            {this.state.cards.length > 0 && (
              <div className="custom-card-container">
                <div
                  className={`${
                    !!this.state.itemMargin ? '' : 'four-card-column'
                  } vertical-spacing-medium`}
                >
                  {this.state.cards.length > 0 &&
                    this.state.cards.map(filteredCard => {
                      if (
                        !filteredCard ||
                        (filteredCard.cardType != undefined &&
                          filteredCard.cardType == 'pack_draft')
                      ) {
                        return;
                      }
                      return (
                        <Card
                          key={filteredCard.id}
                          card={filteredCard}
                          author={filteredCard.author}
                          providerLogos={this.state.providerLogos}
                          dismissible={false}
                          dueAt={
                            filteredCard.dueAt ||
                            (filteredCard.assignment && filteredCard.assignment.dueAt)
                          }
                          startDate={
                            filteredCard.startDate ||
                            (filteredCard.assignment && filteredCard.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          removeCardFromList={this.removeCardFromList}
                          cardUpdated={this.cardUpdated}
                          moreCards={true}
                        />
                      );
                    })}
                </div>
              </div>
            )}
          </ListView>
        </div>
      </div>
    );
  }
}

LearningQueueContainer.propTypes = {
  routeParams: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(LearningQueueContainer);
