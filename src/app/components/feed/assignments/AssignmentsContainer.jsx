import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';

import { getAssignments } from 'edc-web-sdk/requests/assignments';
import { fetchCard } from 'edc-web-sdk/requests/cards';

import ListView from '../../common/ListView';
import Card from '../../cards/Card';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import { removeDissmissCard } from '../../../actions/mylearningplanV5Actions';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import getCardView from '../../../utils/getCardView';
import { Permissions } from '../../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import updatePageLastVisit from '../../../utils/updatePageLastVisit';
import * as upshotActions from '../../../actions/upshotActions';

const limit = 9;

class AssignmentsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleScroll = this.handleScroll.bind(this);
    this.state = {
      assignments: [],
      offset: 0,
      pending: true,
      isLastPage: false,
      completedCards: [],
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };

    if (this.state.showNew) {
      updatePageLastVisit('assignmentsLastVisit');
    }
    this.cardFields =
      'id,state,completed_at,due_at,start_date,started_at,' +
      'card(id,completed_percentage,card_subtype,all_ratings,author,average_rating,card_metadatum,comments_count,completion_state,filestack,hidden,is_assigned,' +
      'is_bookmarked,is_clone,is_official,is_paid,is_public,is_upvoted,mark_feature_disabled_for_source,prices,provider,provider_image,share_url,' +
      'resource,state,votes_count,card_type,slug,badging,skill_level,title,message,ecl_duration_metadata,readable_card_type,is_new,quiz,video_stream,published_at,ecl_metadata,is_reported,teams_permitted,users_permitted),' +
      'assignor,assignable,created_at';
  }

  async componentWillMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    calculateTeemFeedPosition();
    getAssignments(
      limit,
      undefined,
      ['started', 'assigned'],
      this.state.multilingualContent,
      localStorage.getItem('assignmentsLastVisit'),
      this.cardFields
    )
      .then(data => {
        let isLastPage = data.assignments.length < limit;
        if (!isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
        this.setState(
          { assignments: data.assignments, pending: false, isLastPage },
          calculateTeemFeedPosition
        );
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in AssignmentsContainer.getAssignments.func : ${err}`);
      });
    setActivityListHeight(this.state.homepageVersion, 'card');
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';
    if (newView !== this.state.viewRegime) {
      this.setState({ assignments: [], pending: true });
      getAssignments(
        limit,
        undefined,
        ['started', 'assigned'],
        this.state.multilingualContent,
        localStorage.getItem('assignmentsLastVisit'),
        this.cardFields
      )
        .then(data => {
          let isLastPage = data.assignments.length < limit;
          if (!isLastPage) {
            document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
              'calc(100vh - 11.875rem)';
          }
          this.setState({
            assignments: data.assignments,
            pending: false,
            isLastPage,
            viewRegime: newView
          });
          this.getStream();
        })
        .catch(err => {
          console.error(`Error in AssignmentsContainer.getAssignments.func : ${err}`);
        });
    }
  }

  componentWillUnmount() {
    this.state.showNew &&
      localStorage.setItem('assignmentsLastVisit', Math.round(new Date().getTime() / 1000));
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    () => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadMore();
          this.getStream();
          setActivityListHeight(this.state.homepageVersion, 'card');
        }
      }
    },
    150,
    { leading: false }
  );

  loadMore() {
    if (!this.state.isLastPage) {
      this.setState({ pending: true }, () => {
        calculateTeemFeedPosition();
        getAssignments(
          limit + 1,
          this.state.assignments.length,
          ['started', 'assigned'],
          this.state.multilingualContent,
          localStorage.getItem('assignmentsLastVisit'),
          this.cardFields
        )
          .then(data => {
            if (data.assignments.length === 0) {
              window.removeEventListener('scroll', this.handleScroll);
            }
            this.setState(
              {
                assignments: this.state.assignments.concat(data.assignments),
                pending: false,
                isLastPage: data.assignments.length === 0
              },
              calculateTeemFeedPosition
            );
            this.getStream();
            setActivityListHeight(this.state.homepageVersion, 'card');
          })
          .catch(err => {
            console.error(`Error in AssignmentsContainer.loadMore.getAssignments.func : ${err}`);
          });
      });
    }
  }

  hideComplete(cardId) {
    let completedCards = this.state.completedCards;
    completedCards.push(cardId);
    this.setState({
      completedCards
    });
    if (this.state.completedCards.length == this.state.assignments.length) {
      getAssignments(limit, undefined, ['started', 'assigned'], this.state.multilingualContent)
        .then(data => {
          this.setState(
            {
              assignments: data.assignments,
              pending: false,
              isLastPage: data.assignments.length === 0
            },
            calculateTeemFeedPosition
          );
          this.getStream();
          setActivityListHeight(this.state.homepageVersion, 'card');
        })
        .catch(err => {
          console.error(`Error in AssignmentsContainer.hideComplete.getAssignments.func : ${err}`);
        });
    }
  }

  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let assignments = this.state.assignments;
        let findCardIndex = findIndex(assignments, item => {
          return item.assignableId == id;
        });
        assignments[findCardIndex].assignable = data;
        this.setState({ assignments }, calculateTeemFeedPosition);
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in AssignmentsContainer.fetchCard.func : ${err}`);
      });
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
      }
    }
  }

  removeCardFromList = id => {
    let assignedList = this.state.assignments.filter(card => card.assignable.id !== id);
    this.setState({ assignments: assignedList }, calculateTeemFeedPosition);
  };

  sendLearningUpshotEvent = payload => {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['ASSIGNED_LEARNING'], payload);
  };

  // remove card from list after dissmissed from card action
  removeDismissAssessmentFromList = card => {
    const remainingCards = this.state.assignments.filter(obj => obj.assignable.id !== card.id);
    this.setState(() => ({
      assignments: remainingCards
    }));
    this.props.dispatch(removeDissmissCard(card));
  };

  render() {
    let assignedList = this.state.assignments.filter(
      card =>
        !(
          (card.state && card.state == 'COMPLETED') ||
          !!~this.state.completedCards.indexOf(card.id)
        )
    );
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);

    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');

    return (
      <div id="feed">
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          isLastPage={this.state.isLastPage}
          emptyMessage={
            <div className="empty-message data-not-available-msg">
              {this.props.currentUser.name},{' '}
              {tr('there are currently no pending learning items at this time.')}
            </div>
          }
        >
          {this.state.assignments.length > 0 && (
            <div
              className={
                viewRegime == 'Tile'
                  ? 'custom-card-container align-tileview-feed'
                  : 'custom-card-container'
              }
            >
              <div className="five-card-column vertical-spacing-medium">
                {assignedList.map(card => {
                  card.assignable.assignor = card.assignor; // Map key into card assignable item
                  return (
                    <Card
                      key={card.id}
                      card={card.assignable}
                      author={card.assignable.author}
                      hideComplete={this.hideComplete.bind(this, card.id)}
                      moreCards={!isRail}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      user={this.props.currentUser}
                      type={viewRegime}
                      closeCardModal={this.findCheckedCardAtList}
                      removeCardFromList={this.removeCardFromList}
                      sendLearningUpshotEvent={this.sendLearningUpshotEvent}
                      removeDismissAssessmentFromList={this.removeDismissAssessmentFromList}
                    />
                  );
                })}
              </div>
            </div>
          )}
        </ListView>
      </div>
    );
  }
}

AssignmentsContainer.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  route: PropTypes.object,
  pathname: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(AssignmentsContainer);
