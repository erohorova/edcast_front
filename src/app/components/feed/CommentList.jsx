import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CommentItemV3 from './CommentItem.v3';
import CommentInputV3 from './CommentInput.v3';
import { loadComments } from '../../actions/cardsActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';

class CommentList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showAll: false,
      comments: props.comments || [],
      inModal: props.inModal
    };
  }

  viewMoreClickHandler = () => {
    loadComments(
      this.props.cardId,
      this.props.numOfComments,
      this.props.cardType,
      this.props.videoId
    )
      .then(data => {
        this.setState({ comments: data, showAll: true });
      })
      .catch(err => {
        console.error(`Error in CommentList.loadComments.func : ${err}`);
      });
  };

  updateCommentList = newComments => {
    this.setState({ comments: newComments, showAll: true }, () => {
      this.props.updateCommentCount();
      let element = document.getElementById('commentsContainer');
      element.scrollTop = element.scrollHeight;
    });
  };

  render() {
    let comments = this.state.comments.sort((a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });
    if (this.state.showAll === false) {
      comments = comments.slice(-3);
    }
    return (
      <div>
        <div id="commentsContainer" className="comment-list-container vertical-spacing-large">
          {this.props.pending && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
          {comments.map((comment, index) => {
            return (
              <div key={comment.id}>
                <CommentItemV3
                  key={comment.id}
                  cardId={this.props.cardId}
                  comment={comment}
                  user={comment.user}
                  isCardV3={this.props.isCardV3}
                  isOwn={
                    comment.user.id === this.props.currentUser.id ||
                    this.props.cardOwner == this.props.currentUser.id ||
                    this.props.currentUser.isAdmin
                  }
                  commentPostCallback={this.props.updateCommentCount}
                  canReport={
                    comment.user.id != this.props.currentUser.id || this.props.currentUser.isAdmin
                  }
                />
              </div>
            );
          })}
          {this.props.numOfComments > 3 && !this.state.showAll && (
            <div>
              <small>
                <a onTouchTap={this.viewMoreClickHandler}>
                  {tr('View')} {this.props.numOfComments - 3}{' '}
                  {this.props.numOfComments - 3 > 1 ? tr('more comments') : tr('comment')}
                </a>
              </small>
            </div>
          )}
          <CommentInputV3
            cardId={this.props.cardId}
            cardType={this.props.cardType}
            autoFocus={false}
            isCardV3={this.props.isCardV3}
            numOfComments={this.state.comments.length}
            commentPostCallback={this.updateCommentList}
          />
        </div>
      </div>
    );
  }
}

CommentList.propTypes = {
  currentUser: PropTypes.object,
  cardId: PropTypes.string,
  videoId: PropTypes.string,
  cardOwner: PropTypes.any,
  numOfComments: PropTypes.number,
  updateCommentCount: PropTypes.func,
  cardType: PropTypes.string,
  pending: PropTypes.bool,
  users: PropTypes.object,
  comments: PropTypes.array,
  inModal: PropTypes.bool,
  isCardV3: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    users: state.users.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CommentList);
