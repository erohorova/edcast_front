import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'edc-web-sdk/components/Avatar';
import extractMentions from '../../utils/extractMentions';
import TimeAgo from 'react-timeago';
import { tr } from 'edc-web-sdk/helpers/translations';
import ConfirmationModal from '../modals/ConfirmationCommentModal';
import translateTA from '../../utils/translateTimeAgo';
import colors from 'edc-web-sdk/components/colors/index';
import More from 'edc-web-sdk/components/icons/More';
import Linkify from 'react-linkify';

//actions
import { deleteComment } from 'edc-web-sdk/requests/cards';
import { toggleLikeComment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { openCommentReasonModal } from '../../actions/modalActions';
import UserProfileHover from '../common/UserProfileHover';
import IconButton from 'material-ui/IconButton';

class CommentItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isEditing: false,
      openConfirm: false,
      upVoted: this.props.comment.isUpvoted,
      votesCount: this.props.comment.votesCount || 0,
      visible: true,
      truncatedText: this.props.comment.message.length > 145,
      reportingInappropriateComment: window.ldclient.variation(
        'reporting-inappropriate-comment',
        false
      ),
      cardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      listItem: {
        padding: '0 36px 0 64px',
        margin: '0 -1rem'
      },
      primaryColor: {
        color: '#4a90e2'
      },
      primaryColorV3: {
        color: '#4a90e2',
        display: 'inline-flex',
        alignItems: 'center'
      },
      more: {
        color: '#4a90e2',
        fontWeight: '600',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem',
        fontSize: '0.75rem'
      },
      dot: {
        marginRight: '0.5rem',
        marginLeft: '0'
      },
      timeStyle: {
        fontSize: '0.75rem',
        color: '#26273b',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      },
      votes: {
        marginLeft: '4px',
        fontSize: '0.75rem',
        color: '#4a90e2',
        fontWeight: '600',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      },
      commentListDetails: {
        marginBottom: '0'
      },
      avatar: {
        top: '0',
        width: '32px',
        height: '32px'
      },
      commentDetails: {
        width: '92%',
        display: 'inline-block'
      },
      alignViewOption: {
        width: 'inherit',
        height: '33px',
        padding: 'inherit'
      }
    };
  }

  likeClickHandler = e => {
    e.preventDefault();
    toggleLikeComment(this.props.cardId, this.props.comment.id, !this.state.upVoted)
      .then(data => {
        this.setState({
          upVoted: !this.state.upVoted,
          votesCount: data.upvoted ? this.state.votesCount + 1 : this.state.votesCount - 1
        });
      })
      .catch(err => {
        console.error(`Error in CommentItem.v2.toggleLikeComment.func : ${err}`);
      });
  };

  updateState = () => {
    this.setState({ visible: false }, () => {
      this.props.commentPostCallback(-1);
    });
  };

  openReasonReportModal = () => {
    let comment = this.props.comment;
    let isAlreadyReported =
      this.props.users && this.props.users.reported_comment_ids
        ? this.props.users.reported_comment_ids.indexOf(this.props.comment.id) >= 0
        : false;
    if (comment.isReported || isAlreadyReported) {
      this.props.dispatch(openSnackBar('You have already reported this comment', true));
    } else {
      this.props.dispatch(openCommentReasonModal(this.props.comment));
    }
    return;
  };

  confirmDelete = () => {
    this.setState({ openConfirm: false });
    deleteComment(this.props.cardId, this.props.comment.id)
      .then(() => {
        this.updateState();
      })
      .catch(err => {
        console.error(`Error in CommentItem.v2.deleteComment.func : ${err}`);
      });
  };

  toggleModal = () => {
    if (this.props.hideCommentModal) {
      this.props.showConfirmationModal(this.props.cardId, this.props.comment.id, this.updateState);
    } else {
      this.setState({ openConfirm: !this.state.openConfirm });
    }
  };

  truncateMessageText = message => {
    return message.substr(0, 140);
  };

  fullMessage = () => {
    this.setState({ truncatedText: false });
  };

  isReportable = () => {
    return this.state.reportingInappropriateComment && this.props.canReport;
  };

  isDeletable = () => {
    return this.props.isOwn;
  };

  render() {
    if (!this.state.visible) {
      return null;
    }
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);
    let user = this.props.user;
    let message = extractMentions(this.props.comment.message);
    const { currentUser } = this.props;

    return (
      <div className="comment-list-item" style={this.styles.commentListDetails}>
        <div className="commentDetails" style={this.styles.commentDetails}>
          <ListItem
            disabled
            style={this.styles.listItem}
            leftAvatar={
              <Avatar
                className="pointer"
                user={user}
                style={this.styles.avatar}
                onClick={() => {
                  this.props.dispatch(
                    push(
                      `/${
                        (user.handle && user.handle.replace('@', '')) === currentUser.handle
                          ? 'me'
                          : user.handle
                      }`
                    )
                  );
                }}
              />
            }
            primaryText={
              <div className="break-comment">
                <small>
                  <strong>
                    <a
                      className="matte user comment-list-user"
                      href={`/${
                        (user.handle && user.handle.replace('@', '')) === currentUser.handle
                          ? 'me'
                          : user.handle
                      }`}
                    >
                      {user.firstName} {user.lastName}
                    </a>
                  </strong>
                </small>
                <small className="comment-text">
                  {message.map((part, index) => {
                    if (part.type === 'text') {
                      if (this.state.truncatedText) {
                        return (
                          <span key={index}>
                            <Linkify properties={{ target: '_blank' }}>
                              {this.truncateMessageText(part.text)}
                            </Linkify>
                          </span>
                        );
                      } else {
                        return (
                          <span key={index}>
                            <Linkify properties={{ target: '_blank' }}>{part.text}</Linkify>
                          </span>
                        );
                      }
                    }
                    if (part.type === 'handle') {
                      let commenter = this.props.comment.mentions.filter(
                        mention => mention.handle == part.handle
                      );
                      if (commenter.length) {
                        return (
                          <UserProfileHover
                            user={commenter[0]}
                            userProfileURL={`/${
                              (part.handle && part.handle.replace('@', '')) === currentUser.handle
                                ? 'me'
                                : part.handle
                            }`}
                          />
                        );
                      } else {
                        return <span key={index}>{part.handle}</span>;
                      }
                    }
                  })}
                  {this.state.truncatedText && (
                    <span
                      style={this.styles.more}
                      className="more-text-comment"
                      onClick={this.fullMessage}
                    >
                      {tr('more')}
                    </span>
                  )}
                </small>
              </div>
            }
          />
          {this.props.comment.id && (
            <ListItem
              disabled
              insetChildren={true}
              style={this.styles.listItem}
              primaryText={
                <div>
                  <div
                    className="comment-actions horizontal-spacing-medium"
                    style={
                      this.state.cardV3 ? this.styles.primaryColorV3 : this.styles.primaryColor
                    }
                  >
                    {this.props.isCanLike && (
                      <div className="likes">
                        <a href="#" onClick={this.likeClickHandler}>
                          <small>
                            <a style={this.styles.more}>
                              {this.state.upVoted ? tr('Liked') : tr('Like')}
                            </a>
                          </small>
                        </a>
                        <div>
                          {this.state.votesCount > 0 && (
                            <small style={this.styles.votes}> {this.state.votesCount}</small>
                          )}
                        </div>
                      </div>
                    )}
                    {this.state.cardV3 && this.props.isCanLike && (
                      <div className="dot" style={this.styles.dot} />
                    )}
                    {!this.state.cardV3 && this.props.isCanLike && (
                      <div className="pipe">
                        <small>|</small>
                      </div>
                    )}
                    <TimeAgo
                      date={new Date(this.props.comment.createdAt)}
                      live={false}
                      style={this.styles.timeStyle}
                      formatter={formatter}
                    />
                  </div>
                </div>
              }
            />
          )}
        </div>
        {(this.isReportable() || this.isDeletable()) && (
          <IconMenu
            className="insight-dropdown vertical-insight-dropdown"
            iconButtonElement={
              <IconButton style={this.styles.alignViewOption}>
                <More color={colors.gray} />
              </IconButton>
            }
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            targetOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            listStyle={this.styles.iconMenu}
            menuStyle={this.styles.menuItem}
            animated={false}
          >
            {this.isReportable() && (
              <MenuItem
                style={this.styles.menuItem}
                primaryText={tr('Report it')}
                onTouchTap={this.openReasonReportModal}
              />
            )}
            {this.isDeletable() && (
              <MenuItem
                style={this.styles.menuItem}
                primaryText={tr('Delete')}
                onTouchTap={this.toggleModal}
              />
            )}
          </IconMenu>
        )}

        {this.state.openConfirm && !this.props.hideCommentModal && (
          <ConfirmationModal
            title={'Confirm'}
            message={'Do you want to delete the comment?'}
            closeModal={this.toggleModal}
            callback={this.confirmDelete}
          />
        )}
      </div>
    );
  }
}

CommentItem.propTypes = {
  cardId: PropTypes.string,
  users: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  isOwn: PropTypes.bool,
  canReport: PropTypes.bool,
  isCanLike: PropTypes.bool,
  comment: PropTypes.shape({
    message: PropTypes.string,
    isUpvoted: PropTypes.bool,
    votesCount: PropTypes.number,
    mentions: PropTypes.any,
    userId: PropTypes.string,
    createdAt: PropTypes.string,
    id: PropTypes.string
  }),
  hideCommentModal: PropTypes.bool,
  commentPostCallback: PropTypes.func,
  showConfirmationModal: PropTypes.func
};

CommentItem.defaultProps = {
  hideCommentModal: false,
  showConfirmationModal: function() {}
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    users: state.users.toJS()
  };
}

export default connect(mapStoreStateToProps)(CommentItem);
