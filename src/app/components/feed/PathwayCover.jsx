import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CardMedia, CardTitle } from 'material-ui/Card';
import colors from 'edc-web-sdk/components/colors';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import addSecurity from '../../utils/filestackSecurity';

class PathwayCover extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      playIcon: { width: '50px', height: '50px' },
      packCountMargin: { marginTop: '15px' }
    };
    this.showPathway = this.showPathway.bind(this);
  }

  showPathway(e, slug) {
    e.preventDefault();
    this.props.dispatch(
      push(`/${this.props.card.cardType === 'journey' ? 'journey' : 'pathways'}/${slug}`)
    );
  }

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let isFileAttached = this.props.card.filestack && !!this.props.card.filestack.length;
    let imageFileStack = !!(
      isFileAttached &&
      this.props.card.filestack[0].mimetype &&
      ~this.props.card.filestack[0].mimetype.indexOf('image/')
    );
    let image = imageFileStack
      ? this.props.card.filestack[0].url
      : (this.props.card.fileResources &&
          this.props.card.fileResources[0] &&
          this.props.card.fileResources[0].fileUrl) ||
        this.props.defaultImage;
    if (!image && !this.props.modal) {
      image =
        '//dp598loym07sk.cloudfront.net/assets/card_default_image_min-38538421e73fde77e005d645a1ee28ae.jpg';
    }
    let title = this.props.card.title || this.props.card.message;
    return (
      <div className="pathway-cover">
        <a
          href="#"
          aria-label={tr(`Visit the pathway, ${this.props.card.title}`)}
          className="matte-hover"
          onClick={e => this.showPathway(e, this.props.card.slug)}
        >
          <h5>{title}</h5>
          {image && (
            <div className="text-center">
              <div className="pathway-img-cont">
                <img
                  src={addSecurity(image, expireAfter, this.props.currentUser.id)}
                  alt={tr(`Visit the pathway, ${this.props.card.title}`)}
                />
              </div>
            </div>
          )}
        </a>
        <div style={this.styles.packCountMargin}>
          <small>
            {this.props.card.packCount}
            {this.props.card.packCount > 1 ? tr(' Posts') : tr(' Post')}
          </small>
        </div>
      </div>
    );
  }
}

PathwayCover.propTypes = {
  card: PropTypes.object,
  modal: PropTypes.bool,
  defaultImage: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect()(PathwayCover);
