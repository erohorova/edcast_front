import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import Sidebar from 'react-sidebar';
import moment from 'moment';
import find from 'lodash/find';
import PDF from 'react-pdf-js';

import IconButton from 'material-ui/IconButton/IconButton';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import colors from 'edc-web-sdk/components/colors/index';
import Downloadv2 from 'edc-web-sdk/components/icons/Downloadv2';
import FullTimeAccess from 'edc-web-sdk/components/icons/FullTimeAccess';
import Support from 'edc-web-sdk/components/icons/Support';
import WebandMobile from 'edc-web-sdk/components/icons/WebandMobile';
import Certificate from 'edc-web-sdk/components/icons/Certificate';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';

import {
  markAsComplete,
  markAsUncomplete,
  initiateCardPurchase,
  completedCardPurchase,
  cancelCardPurchase
} from 'edc-web-sdk/requests/cards.v2';
import {
  fetchCardForStandaloneLayout,
  rateCard,
  submitProjectCard
} from 'edc-web-sdk/requests/cards';

import VideoStream from './VideoStream';
import CommentList from './CommentList';
import LiveCommentList from './LiveCommentList';
import PathwayCover from './PathwayCover';
import InlineVideo from './InlineVideo.jsx';
import Poll from './Poll';

import {
  loadComments,
  toggleLikeCardAsync,
  toggleBookmarkCardAsync
} from '../../actions/cardsActions';
import {
  openCardStatsModal,
  openStatusModal,
  openCardWalletPaymentModal,
  openPayPalSuccessModal,
  confirmation
} from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { submitRatings, updateRatedQueueAfterRating } from '../../actions/relevancyRatings';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';

import TooltipLabel from '../common/TooltipLabel';
import MarkdownRenderer from '../common/MarkdownRenderer';
import CreationDate from '../common/CreationDate';
import BreadcrumbV2 from '../common/BreadcrumpV2';
import RecommendedBlock from '../common/RecommendedBlock';
import Spinner from '../common/spinner';
import SvgImageResized from '../common/ImageResized';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import abbreviateNumber from '../../utils/abbreviateNumbers';
import checkResources from '../../utils/checkResources';
import getDefaultImage from '../../utils/getDefaultCardImage';
import getCardParams from '../../utils/getCardParams';
import convertRichText from '../../utils/convertRichText';
import getTranscodedVideo from '../../utils/getTranscodedVideo';
import linkPrefix from '../../utils/linkPrefix';
import checkResourceURL from '../../utils/checkResourceURL';
import thumbnailImage from '../../utils/thumbnailImage';
import getFormattedDateTime from '../../utils/getFormattedDateTime';

import CardClassesModal from '../modals/CardClassesModal';

import * as logoType from '../../constants/logoTypes';
import 'url-search-params-polyfill';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import VideoIcon from 'edc-web-sdk/components/icons/Video';
import { filestackClient } from '../../utils/filestack';
import {
  videoTranscode,
  uploadPolicyAndSignature,
  refreshFilestackUrl
} from 'edc-web-sdk/requests/filestack';
import TextFieldCustom from '../common/SmartBiteInput';
import { fetchSumissionList } from 'edc-web-sdk/requests/submission';

import CardV3SkillTypeEffort from '../cards/v3/CardV3SkillTypeEffort';
import CardV3Header from '../cards/v3/CardV3Header';
import CardV3SkillcoinPrice from '../cards/v3/CardV3SkillcoinPrice';
import CardV3Footer from '../cards/v3/CardV3Footer';
import CardV3PostedBy from '../cards/v3/CardV3PostedBy';
import CardV3ChannelAndTags from '../cards/v3/CardV3ChannelAndTags';
import { parseUrl } from '../../utils/urlParser';
import pubnubAdapter from '../../utils/pubnubAdapter';
import pdfPreviewUrl from '../../utils/previewPdf';
import CardV3Assignor from '../cards/v3/CardV3Assignor';

let LocaleCurrency = require('locale-currency');
let GetVideoId = require('get-video-id');

const logoObj = logoType.LOGO;
const lightPurp = '#acadc1';
const videoType = ['video/mp4', 'video/ogg', 'video/webm', 'video/quicktime', 'video/ogv'];

class InsightV3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.state = {
      showComment: false,
      comments: [],
      pendingLike: false,
      pendingBookmark: false,
      truncateTitle: !props.isStandalone
        ? props.card.title && props.card.title.length > 260
        : false,
      truncateMessage: !props.isStandalone
        ? props.card.message && props.card.message.length > 260
        : false,
      dismissed: false,
      commentsCount: props.card.commentsCount,
      averageRating: props.card.averageRating,
      isCompleted:
        props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED',
      isLiveStream: false,
      liveCommentsCount: 0,
      ratingCount: 0,
      sidebarOpen: false,
      courseDefaultImage: getDefaultImage(this.props.currentUser.id).url,
      card: this.props.card,
      wallet: this.props.team.config.wallet || false,
      modalClassesOpen: false,
      customConfig:
        (this.props.team.config.lxpCustomCSS &&
          this.props.team.config.lxpCustomCSS.configs &&
          this.props.team.config.lxpCustomCSS.configs.header) ||
        {},
      isOpenBlock: { cards: false, users: false },
      prevPath: localStorage.getItem('prevPath'),
      recommendedCards: [],
      recommendedUsers: [],
      fileStack: [],
      createLabel: 'Submit',
      description: '',
      disableSubmit: true,
      optionsError: '',
      submissionList: [],
      cardPurchased: false,
      showLoader: false,
      showProjectCard: window.ldclient.variation('project-card', false),
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      configurableHeader: window.ldclient.variation('new-configurable-header', false),
      showRecommendedContent: window.ldclient.variation('winterstorm', false),
      standaloneCardFilePreview: window.ldclient.variation('standalone-card-file-preview', false),
      recommendBlocksConfig: window.ldclient
        .variation('recommendation-block-config', 'Default')
        .split('-'),
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      markAsCompleteEnableForLink: false,
      liveUsers: [],
      videoSubmitted: false
    };
    this.styles = {
      btnIcons: {
        verticalAlign: 'middle',
        width: '1rem',
        height: '0.875rem',
        padding: 0
      },
      downloadButton: {
        width: '100%',
        height: '100%',
        padding: 0,
        background: '#454560',
        cursor: 'pointer'
      },
      tooltipStyles: {
        left: '-50%'
      },
      imgStyle: {
        marginRight: '0'
      },
      arrowRightIcon: {
        color: '#6f708b',
        cursor: 'pointer',
        height: '1.25rem'
      },
      arrowLeftIcon: {
        color: '#6f708b',
        cursor: 'pointer',
        height: '1.25rem'
      },
      svgImage: {
        zIndex: 2,
        position: 'relative'
      },
      svgStyle: {
        filter: 'url(#blur-effect-1)'
      },
      audio: {
        width: '100%'
      },
      buyWithSkillCoins: {
        width: '11.875rem',
        margin: '0.9375rem 0',
        fontSize: '0.8125rem',
        fontFamily: 'Helvetica',
        height: '2.1875rem',
        borderRadius: '0.3125rem'
      },
      buyNow: {
        width: '11.875rem',
        fontSize: '0.8125rem',
        fontFamily: 'Helvetica',
        height: '2.1875rem',
        borderRadius: '0.3125rem',
        backgroundColor: '#6f708b',
        cursor: 'pointer'
      },
      videoIcon: {
        width: '2.125rem',
        height: '2.125rem',
        marginTop: '-0.3125rem'
      },
      submissionList: {
        fontSize: '0.7em',
        color: lightPurp
      },
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      btnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      }
    };

    this.stylesSidebar = {
      content: {
        overflow: 'hidden'
      },
      sidebar: {
        backgroundColor: '#f0f0f5',
        width: '11.0625rem',
        overflowX: 'hidden'
      }
    };

    this.isCsodCourse = props.card.course_id && props.card.source_type_name === 'csod';

    this.rateCard = this.rateCard.bind(this);
    this.cardLikeHandler = this.cardLikeHandler.bind(this);
    this.handleCardAnalayticsModal = this.handleCardAnalayticsModal.bind(this);
    this.completeClickHandler = this.completeClickHandler.bind(this);
    this.truncateMessageText = this.truncateMessageText.bind(this);
    this.linkExtract = this.linkExtract.bind(this);
    this._metricRecorder = this._metricRecorder.bind(this);
    this.checkVisible = this.checkVisible.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    this.onMessageReceiveJoinLeave = this.onMessageReceiveJoinLeave.bind(this);
    this.componentCleanup = this.componentCleanup.bind(this);
    this.openCardWalletModal = this.openCardWalletModal.bind(this);
    this.openPayPalPayment = this.openPayPalPayment.bind(this);
    this.cardPurchased = this.cardPurchased.bind(this);
  }

  async componentWillMount() {
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  componentDidMount() {
    this.props
      .dispatch(getSpecificUserInfo(['handle'], this.props.currentUser))
      .then(userData => {
        let urlParamsExtractor = new URLSearchParams(window.location.search);
        // should get called only for project card
        if (this.state.showProjectCard && this.props.card && this.props.card.projectId) {
          this.fetchSubmissionList();
        }

        if (urlParamsExtractor.getAll('paymentId').length > 0) {
          let payPalPayLoad = {
            payment_id: urlParamsExtractor.getAll('paymentId')[0],
            payer_id: urlParamsExtractor.getAll('PayerID')[0],
            token: urlParamsExtractor.getAll('token')[0]
          };
          let payload = {
            paypal_data: payPalPayLoad,
            token: urlParamsExtractor.getAll('app_token')[0],
            gateway: 'paypal'
          };
          completedCardPurchase(payload)
            .then(data => {
              let paypalData = {
                card: this.props.card,
                redirectUrl: data.transactionDetails.redirectUrl,
                cardPurchased: true
              };
              this.props.dispatch(openPayPalSuccessModal(paypalData));
            })
            .catch(error => {
              this.props.dispatch(openSnackBar('Something went wrong! Please try again', true));
              window.open(window.location.origin + window.location.pathname, '_self');
            });
        } else if (
          urlParamsExtractor.getAll('paymentId').length <= 0 &&
          urlParamsExtractor.getAll('token').length > 0 &&
          urlParamsExtractor.getAll('cancelled') == 'true'
        ) {
          let payload = {
            token: urlParamsExtractor.getAll('app_token')[0]
          };
          cancelCardPurchase(payload)
            .then(data => {
              this.props.dispatch(openSnackBar('The payment has been cancelled', true));
            })
            .catch(err1 => {
              console.error('Error in canceling payment');
            });
        }

        window.addEventListener('beforeunload', this.componentCleanup);
        if (this.isCsodCourse) return;
        let cardType;

        if (this.props.showComment) {
          let cardId = this.state.card.cardId || this.state.card.id;
          let isECL = /^ECL-/.test(cardId);
          if (!isECL && this.state.card.commentsCount) {
            loadComments(
              cardId,
              this.state.card.commentsCount,
              this.state.card.cardType,
              this.state.card.cardId ? this.state.card.id : null
            )
              .then(data => {
                this.setState({ comments: data, showComment: true });
              })
              .catch(err => {
                console.error(`Error in InsightV3.loadComments.func : ${err}`);
              });
          } else {
            this.setState({ comments: [], showComment: true });
          }
        }
        switch (this.state.card.cardType) {
          case 'VideoStream' || 'video_stream':
            cardType = 'video_stream';
            break;
          case 'pack':
            cardType = 'collection';
            break;
          default:
            cardType = 'card';
        }
        if (this.state.card.cardType === 'video_stream') {
          let isLiveStream = false;
          if (this.state.card && this.state.card.videoStream !== undefined) {
            isLiveStream = this.state.card.videoStream.status === 'live';
          } else if (this.state.card && this.state.card.status) {
            isLiveStream = this.state.card.status === 'live';
          }
          this.setState({ isLiveStream });
          if (isLiveStream) {
            this.postActionOnPubNub('join', userData);
          }
        }
        this.allCountRating(this.state.card);
        this._metricRecorder(cardType);
        window.addEventListener('scroll', this._metricRecorder);
      })
      .catch(err => {
        console.error(`Error in InsightV3.getSpecificUserInfo.func: ${err}`);
      });
  }

  componentWillUnmount() {
    this.postActionOnPubNub('leave');
    window.removeEventListener('scroll', this._metricRecorder);
    this.videoTranscodeInterval && clearInterval(this.videoTranscodeInterval);
    this.videoTranscodeEditInterval && clearInterval(this.videoTranscodeEditInterval);
    this.componentCleanup();
    window.removeEventListener('beforeunload', this.componentCleanup);
  }
  componentCleanup() {
    // whatever you want to do when the component is unmounted or page refreshes
    this.postActionOnPubNub('leave');
  }

  fetchSubmissionList = () => {
    fetchSumissionList({ project_id: this.props.card.projectId, filter: 'all' })
      .then(subList => {
        if (subList) this.setState({ submissionList: subList.projectSubmissions });
      })
      .catch(err => {
        console.error(`Error in InsightV3.fetchSubmissionList.func : ${err}`);
      });
  };

  postActionOnPubNub = (action, currentUserObj = null) => {
    //prepare payload
    if (this.state.card.videoStream !== undefined) {
      let pubnubPayload = {};
      pubnubPayload['uid'] = this.props.currentUser.id || '';
      pubnubPayload['p'] = this.props.currentUser.picture || '';
      pubnubPayload['fn'] = this.props.currentUser.name.split(' ')[0] || '';
      pubnubPayload['ln'] = this.props.currentUser.name.split(' ')[1] || '';
      pubnubPayload['h'] =
        this.props.currentUser.handle || (currentUserObj && currentUserObj.handle) || '';
      pubnubPayload['a'] = action;
      pubnubPayload['type'] = 'videoStream';
      //publish to pubnub
      this.pubnub.publish({
        channel: this.getCardUuid(),
        message: pubnubPayload
      });
    }
  };

  onMessageReceiveJoinLeave = m => {
    let liveUsers = this.state.liveUsers;
    let removeIndex = liveUsers.indexOf(m.uid);
    if (m.a == 'join' && removeIndex === -1) {
      liveUsers.push(m.uid);
    } else if (m.a == 'leave' && removeIndex !== -1) {
      liveUsers.splice(removeIndex, 1);
    }
    this.setState({ liveUsers });
  };

  _metricRecorder = cardType => {
    if (this._insight && this.checkVisible(this._insight)) {
      window.removeEventListener('scroll', this._metricRecorder);
    }
  };

  rateCard(level) {
    rateCard(this.state.card.id, { level: level })
      .then(data_rating => {
        this.cardUpdated();
      })
      .catch(err => {
        console.error(`Error in InsightV3.rateCard.func : ${err}`);
      });
  }

  checkVisible = elm => {
    let rect = elm.getBoundingClientRect();
    let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
  };

  cardUpdated = card => {
    if (!this.state.card) {
      return;
    }

    let cardobj;
    if (this.props.pathname.indexOf('/channel/') !== -1) {
      let routeParam = this.props.pathname.split('/');
      let slug = routeParam[routeParam.length - 1];
      let findChannel = find(this.props.channels, el => {
        return el.slug == slug;
      });
      if (findChannel) {
        let existChannel = find(card.channel_ids, el => {
          return el == findChannel.id;
        });
        if (!existChannel) {
          this.hideInsight();
        }
      }
    }
    if (card) {
      cardobj = card.card;
    } else {
      cardobj = this.state.card;
    }

    if (
      cardobj &&
      cardobj.filestack &&
      cardobj.filestack.length &&
      cardobj.filestack[0].mimetype &&
      cardobj.filestack[0].mimetype.includes('video')
    ) {
      getTranscodedVideo.call(
        this,
        this.state.card,
        cardobj,
        this.props.currentUser.id,
        true,
        true
      );
    } else {
      fetchCardForStandaloneLayout(this.state.card.id, { is_standalone_page: true })
        .then(data => {
          this.setState({
            card: data
          });
        })
        .catch(err => {
          console.error(`Error in InsightV3.fetchCard.func : ${err}`);
        });
    }
  };

  handleCardAnalayticsModal() {
    this.props.dispatch(openCardStatsModal(this.state.card));
  }

  cardLikeHandler = () => {
    if (this.state.pendingLike) {
      return;
    }
    this.setState({ pendingLike: true }, () => {
      toggleLikeCardAsync(
        this.state.card.cardId || this.state.card.id,
        this.state.card.cardType,
        !this.state.card.isUpvoted
      )
        .then(() => {
          if (!this.state.card.isUpvoted && this.state.isLiveStream) {
            this.postActionOnPubNub('like');
          }

          let card = this.state.card;
          card.isUpvoted = !card.isUpvoted;
          card.votesCount = Math.max(card.votesCount + (card.isUpvoted ? 1 : -1), 0);
          this.setState({ card, pendingLike: false });
        })
        .catch(err => {
          console.error(`Error in InsightV3.toggleLikeCardAsync.func : ${err}`);
        });
    });
  };

  hideInsight = () => {
    let card = this.state.card;
    card.featuredHidden = true;
    this.setState({ card, dismissed: true });
  };

  completeClickHandler() {
    let cardType;
    switch (this.state.card.cardType) {
      case 'VideoStream':
        cardType = 'video_stream';
        break;
      default:
        cardType = 'card';
        break;
    }

    let markFeatureDisabledForSource = this.state.card.markFeatureDisabledForSource;
    let isPack = this.state.card.cardType === 'pack' || this.state.card.cardType === 'journey';
    if (
      (!isPack && !this.state.isCompleted) ||
      (isPack &&
        this.state.card.state === 'published' &&
        +this.state.card.completedPercentage === 98)
    ) {
      markAsComplete(this.state.card.id, { state: 'complete' })
        .then(() => {
          this.setState({ isCompleted: true }, () => {
            if (this.state.newModalAndToast) {
              let message = `You have completed this ${
                this.state.card.cardType === 'pack'
                  ? 'Pathway'
                  : this.state.card.cardType === 'journey'
                  ? 'Journey'
                  : 'SmartCard'
              } and can track it in completion history under Me tab`;
              this.props.dispatch(openSnackBar(message, true));
            } else {
              let compoundMessage = [
                'You have completed this task. You can view it under',
                '/me/content/completed',
                'Me',
                'Tab'
              ];
              this.props.dispatch(
                openStatusModal(
                  '',
                  () => {
                    // getting if this is a standlone insight page & redirecting to the previous link.
                  },
                  compoundMessage
                )
              );
            }
          });
          this.state.card.featuredHidden = true;
          let card = this.state.card;
          card.featuredHidden = true;
          card.completionState = 'COMPLETED';
          this.setState({ card });
          if (this.props.hideComplete !== undefined) {
            this.props.hideComplete(this.state.card.id);
          }
        })
        .catch(err => {
          console.error(`Error in InsightV3.markAsComplete.func : ${err}`);
        });
    } else if (
      (!isPack && this.state.isCompleted) ||
      (isPack && +this.state.card.completedPercentage === 100)
    ) {
      if (!markFeatureDisabledForSource) {
        this.props.dispatch(
          confirmation(
            'Please note',
            'Marking the card as incomplete will not change the Continuous Learning hour or the score associated with it.',
            () => {
              markAsUncomplete(this.state.card.id)
                .then(() => {
                  this.cardUpdated();
                  this.setState({ isCompleted: false, markAsCompleteEnableForLink: false }, () => {
                    setTimeout(() => {
                      if (this.state.newModalAndToast) {
                        this.props.dispatch(
                          openSnackBar('You have marked this SmartCard as incomplete', true)
                        );
                      } else {
                        this.props.dispatch(
                          openStatusModal('You have marked this task as incomplete')
                        );
                      }
                    }, 1500);
                  });
                })
                .catch(err => {
                  console.error(`Error in InsightV3.markAsUncomplete.func : ${err}`);
                });
            }
          )
        );
      }
    } else {
      let msg =
        this.state.card.state === 'published'
          ? `Please complete all pending cards before marking the ${
              this.state.card.cardType === 'pack' ? 'Pathway' : 'Journey'
            } as complete.`
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  }

  standaloneLinkClickHandler = () => {
    let linkPrefixValue = linkPrefix(this.state.card.cardType);
    // If coming from modal search, remove overflow.
    // document.getElementsByTagName('body')[0].style.overflow = '';
    let cardType = linkPrefixValue === 'video_streams' ? 'insights' : linkPrefixValue;
    window.history.pushState(null, null, `/${linkPrefixValue}/${this.state.card.slug}`);
    this.props.dispatch(openStandaloneOverviewModal(this.state.card, cardType));
    if (this.props.toggleSearch) {
      this.props.toggleSearch();
    }
  };

  viewMoreTitleHandler = () => {
    this.setState({ truncateTitle: false });
  };

  viewMoreMessageHandler = () => {
    this.setState({ truncateMessage: false });
  };

  updateCommentCount = (count = 1) => {
    let commentsCount = this.state.commentsCount + count;
    loadComments(
      this.state.card.id,
      commentsCount,
      this.state.card.cardType,
      this.state.card.cardId ? this.state.card.id : null
    )
      .then(data => {
        this.setState({ comments: data, showComment: true, commentsCount: commentsCount });
      })
      .catch(err => {
        console.error(`Error in InsightV3.loadComments.func : ${err}`);
      });
  };

  cardStatusButton = (e, card, isCompleted) => {
    e.preventDefault();
    !this.isUncompleteEnabled &&
    ((card.assignment && card.assignment.state === 'COMPLETED') || isCompleted)
      ? () => {
          return null;
        }
      : this.completeClickHandler();
  };

  truncateMessageText(message) {
    return message.substr(0, 260);
  }

  openUrl = url => {
    refreshFilestackUrl(url)
      .then(resp => {
        window.open(resp.signed_url, '_blank');
      })
      .catch(err => {
        console.error(`Error in InsightV3.openUrl.refreshFilestackUrl.func : ${err}`);
      });
  };

  downloadBlock(file) {
    return (
      <span className="roll">
        <a onClick={() => this.openUrl(file.url)} download={file.handle} target="_blank">
          <IconButton
            style={this.styles.downloadButton}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={tr('Download')}
            aria-label={tr('Download')}
            iconStyle={this.styles.btnIcons}
          >
            <Downloadv2 />
          </IconButton>
        </a>
      </span>
    );
  }

  cardViewProperty() {
    let cardType = this.state.card.cardType;
    let cardSubtype = this.state.card.cardSubtype;

    if (
      ['video_stream', 'pack', 'pack_draft'].indexOf(cardType) > -1 ||
      (cardType === 'media' && ['link', 'video'].indexOf(cardSubtype) > -1)
    ) {
      return this.props.isStandalone ? 'full' : 'summary';
    } else {
      return 'full';
    }
  }

  liveCommentsCount = commentCount => {
    this.setState({ liveCommentsCount: commentCount });
  };

  getCardStatus() {
    if (this.state.card && this.state.card.videoStream !== undefined) {
      return this.state.card.videoStream.status;
    } else if (this.state.card && this.state.card.status) {
      return this.state.card.status;
    } else {
      return '';
    }
  }

  getCardUuid() {
    if (this.state.card.videoStream !== undefined) {
      return this.state.card.videoStream.uuid;
    } else if (this.state.card.uuid !== undefined) {
      return this.state.card.uuid;
    } else {
      return '';
    }
  }

  linkExtract() {
    let url =
      typeof this.state.card.resource.url !== 'undefined'
        ? this.state.card.id
          ? `/insights/${encodeURIComponent(this.state.card.id)}/visit`
          : this.state.card.resource.url
        : `/insights/${this.state.card.id}`;
    return url;
  }

  ratingChanged = userRating => {
    this.setState({
      averageRating: userRating
    });
    let payload = {
      rating: userRating
    };

    submitRatings(this.state.card.id, payload)
      .then(response => {
        this.setState({
          averageRating: response.averageRating
        });
        this.allCountRating(response);
      }, this)
      .catch(err => {
        console.error(`Error in InsightV3.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.state.card));
  };

  allCountRating = card => {
    if (!card.allRatings) {
      return null;
    }

    if (typeof card.allRatings === 'number') {
      this.setState({ ratingCount: card.allRatings });
      return card.allRatings;
    }

    if (typeof card.allRatings === 'object') {
      let ratingCount = 0;
      for (let i = 1; i < 6; i++) {
        ratingCount += card.allRatings[i] ? card.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
      return ratingCount;
    }
  };

  onDocumentComplete = pages => {
    this.setState({ page: 1, pages });
  };

  onPageComplete = page => {
    this.setState({ page });
  };

  handlePrevious = () => {
    if (this.state.page > 1) {
      this.setState({ page: this.state.page - 1 });
    }
  };

  handleNext = () => {
    if (this.state.page < this.state.pages) {
      this.setState({ page: this.state.page + 1 });
    }
  };

  viewPagesPDF = () => {
    let page = [];
    for (let i = 1; i <= this.state.pages; i++) {
      page[i] = i;
    }
    return page;
  };

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }

  toggleSidbare() {
    this.setState({ sidebarOpen: !this.state.sidebarOpen });
  }

  checkComplete = (card, isAnswer) => {
    if (isAnswer) {
      markAsComplete(card.id, { state: 'complete' });
      this.setState({ isCompleted: true });
    }
  };

  handleResourceClicked = e => {
    e.preventDefault();
    if (this.state.card && this.state.card.resource && this.state.card.resource.url) {
      window.open(
        this.state.card.resource.url.includes('/api/scorm/')
          ? this.state.card.resource.url
          : this.linkExtract(),
        '_blank'
      );
      this.setState({
        markAsCompleteEnableForLink: true
      });
    }
  };

  handleResourceUrlClicked = (e, url) => {
    e.preventDefault();
    if (url) {
      window.open(url, '_blank');
      this.setState({
        markAsCompleteEnableForLink: true
      });
    }
  };

  imageContainer = (img, key, isArticle) => {
    let opts = {
      className: `anchorAlignment image-container ${
        isArticle ? 'anchorAlignment image-container__article' : ''
      } `,
      onClick: e => this.handleResourceClicked(e),
      href: '#'
    };
    if (key) {
      opts['key'] = `img-container-${key}`;
    }
    return (
      <a {...opts}>
        <div
          style={this.styles.imgStyle}
          className="card-standalone-v3__image pathway-image card-blurred-background fp fp_img"
        >
          <svg id="svg-image-blur" width="100%" height="100%">
            <title>
              {' '}
              <RichTextReadOnly text={convertRichText(this.props.card.message)} />
            </title>
            <SvgImageResized
              cardId={`${this.props.card.id}`}
              resizeOptions={'height:505'}
              id="svg-image"
              style={this.styles.svgStyle}
              xlinkHref={img}
              x="-30%"
              y="-30%"
              width="160%"
              height="160%"
            />
            <filter id="blur-effect-1">
              <feGaussianBlur stdDeviation="10" />
            </filter>
          </svg>
        </div>
        <svg width="100%" height="100%" style={this.styles.svgImage}>
          <title>
            {' '}
            <RichTextReadOnly text={convertRichText(this.props.card.message)} />
          </title>
          <SvgImageResized
            cardId={`${this.props.card.id}`}
            resizeOptions={'height:505'}
            xlinkHref={img}
            width="100%"
            style={this.styles.svgImage}
            height="100%"
          />
        </svg>
      </a>
    );
  };

  embedUrl(url) {
    let sources = ['vimeo', 'youtube'];
    let embed_url = url;
    let json_data = GetVideoId(url);

    if (json_data && json_data.id && !!~sources.indexOf(json_data.service)) {
      if (json_data.service == 'vimeo') {
        embed_url = 'https://player.vimeo.com/video/' + json_data.id;
      } else if (json_data.service == 'youtube') {
        embed_url = '//www.youtube.com/embed/' + json_data.id;
      }
    }

    return embed_url;
  }

  truncatedTitle(title, url) {
    if (!!url && url.substr(0, 50) === title.substr(0, 50)) {
      return title.substr(0, 35) + '...';
    } else {
      return title;
    }
  }

  deeplink() {
    let card = this.props.card;
    let classes = card.additional_metadata && card.additional_metadata.user_courses;
    let has_many_classes = classes && classes.length > 1;

    if (has_many_classes) {
      this.setState({ modalClassesOpen: true });
    } else {
      let url = card.deeplink_url;
      if (classes && classes.length == 1) {
        url = classes[0].deeplink_url;
      }
      window.open(url, '_blank');
    }
  }

  closeClassesModal = () => {
    this.setState({ modalClassesOpen: false }, () => {
      document.body.style.overflow = '';
    });
  };

  openCardWalletModal(data) {
    let priceData = {
      card: this.props.card,
      priceData: data,
      cardPurchased: this.cardPurchased
    };

    this.props.dispatch(openCardWalletPaymentModal(priceData));
  }

  openPayPalPayment(priceData) {
    this.setState({
      showLoader: true
    });
    let payload = {
      orderable_type: 'card',
      price_id: priceData.id,
      orderable_id: this.props.card.id,
      gateway: 'paypal'
    };
    initiateCardPurchase(payload)
      .then(data => {
        window.open(data.transactionDetails.url, '_self');
        this.setState({
          showLoader: false
        });
      })
      .catch(error => {
        this.setState({
          showLoader: false
        });
        this.props.dispatch(openSnackBar('Something went wrong! Please try again', true));
      });
  }

  cardPurchased() {
    this.setState({
      cardPurchased: true
    });
  }

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  fileStackHandler = (accept, callback) => {
    let fromSources = [
      'local_file_system',
      'googledrive',
      'dropbox',
      'box',
      'github',
      'gmail',
      'onedrive',
      'webcam'
    ];

    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: videoType,
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources: fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in InsightV3.fileStackHandler.filestackClient.func : ${err}`);
          });
        this.setState({ uploadErrorText: '' });
      })
      .catch(err => {
        console.error(`Error in InsightV3.fileStackHandler.uploadPolicyAndSignature.func : ${err}`);
      });
  };

  addFileStackFiles(fileStack) {
    let videoPreTranscodeUrl;
    refreshFilestackUrl(fileStack.filesUploaded[0].url)
      .then(resp => {
        if (
          fileStack.filesUploaded[0].mimetype &&
          ~fileStack.filesUploaded[0].mimetype.indexOf('video')
        ) {
          let securedVideoUrl = resp.signed_url;
          let securedVideoPath = parseUrl(securedVideoUrl).pathname;
          videoTranscode(securedVideoPath)
            .then(data => {
              this.setState({
                transcodedVideoUrl: data.status === 'completed' ? data.data.url : null,
                transcodedThumbnail: data.status === 'completed' ? data.data.thumbnail : null,
                transcodedVideoStatus: data.status
              });
            })
            .catch(err => {
              console.error(
                `Error in InsightV3.addFileStackFiles.refreshFilestackUrl.videoTranscode.func : ${err}`
              );
            });
          videoPreTranscodeUrl = securedVideoUrl || fileStack.filesUploaded[0].url;
        }
        this.setState({
          fileStack: [...this.state.fileStack, ...fileStack.filesUploaded],
          videoPreTranscodeUrl
        });
      })
      .catch(err => {
        console.error(`Error in InsightV3.addFileStackFiles.refreshFilestackUrl.func : ${err}`);
      });
  }

  videoSourcePreview = (item, isDownloadContentDisabled) => {
    return (
      <div className="project-preview-top-spacing">
        <span>
          <div>
            <video
              preload="auto"
              poster={this.state.fileStack[1] && this.state.fileStack[1].url}
              className="preview-upload-video"
              controls
              src={this.state.videoPreTranscodeUrl || item.url}
              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
            />
            <div />
          </div>
        </span>
      </div>
    );
  };

  submitClickHandler = cardData => {
    if (!this.state.fileStack[0]) {
      this.setState({ uploadErrorText: 'Please upload a video', disableSubmit: false });
      return;
    }
    if (!this.state.description) {
      this.setState({ disableSubmit: false, optionsError: 'This is required' });
      return;
    }
    let payload = {
      project_submission: {
        description: this.state.description,
        project_id: cardData.projectId,
        filestack: this.state.fileStack
      }
    };
    this.setState({ createLabel: 'Submitting..' });

    submitProjectCard(payload)
      .then(data => {
        this.setState({
          videoSubmitted: true,
          createLabel: 'Submit',
          description: '',
          fileStack: [],
          optionsError: '',
          disableSubmit: true
        });
        this.props.dispatch(openSnackBar('Submission Added', true));

        // should get called only for project card
        if (this.state.showProjectCard && this.props.card && this.props.card.projectId) {
          this.fetchSubmissionList();
        }
      })
      .catch(err => {
        console.error(`Error in InsightV3.submitProjectCard.func : ${err}`);
      });
  };

  descriptionCardChange = event => {
    this.setState({ description: event.target.value, optionsError: '' });
  };

  refreshMediaUrl = (media, handle) => {
    if (!!handle && (media == 'audio' || media == 'video')) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let mediaObj = document.getElementsByTagName(media)[0];
          let seen = mediaObj.currentTime;
          mediaObj.setAttribute('src', newUrl);
          mediaObj.currentTime = seen;
          mediaObj.play();
        })
        .catch(err => {
          console.error(`Error in insightV2.refreshFilestackUrl.func : ${err}`);
        });
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.state.dismissed) {
      return null;
    }
    let card = checkResources(this.state.card);
    let params = getCardParams(card, this.props, 'insightV2') || {};
    let resourceImage;
    let scormCard = card.filestack && card.filestack[0] && card.filestack[0].scorm_course;
    let formattedDate = this.props.card.publishedAt
      ? getFormattedDateTime(this.props.card.publishedAt, 'DD MMM YYYY')
      : null;
    if (
      !scormCard &&
      ((card.resource && card.resource.imageUrl) ||
        (card.filestack && card.filestack[0] && card.filestack[0].url))
    ) {
      resourceImage = card.resource.imageUrl || card.filestack[0].url;
    } else if (scormCard) {
      resourceImage = (card.filestack[1] && card.filestack[1].url) || this.state.courseDefaultImage;
    } else {
      resourceImage = this.state.courseDefaultImage;
    }
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;

    if (card.card_type) {
      // Reformat to fit correct insight requirement
      //TODO: assignment, commentsPending, isUpvoted
      card.timestampWithZone = card.updated_at;
      card.isOffical = card.is_official;
      card.cardType = card.card_type;
      card.commentsCount = card.comments_count;
      card.publishedAt = card.published_at;
      //TODO: feed_v1 is not having config with card, need to fix
      // card.isShareable = card.config.shareable;
    }

    // card resource fix
    if (typeof card.resource === 'undefined') {
      if (card.video) {
        card.resource = {
          description: card.video.description,
          embedHtml: card.video.embedHtml,
          site: card.video.site,
          title: card.video.title,
          slug: `/${linkPrefix(card.cardType)}/${card.slug}`
        };
      }
    }

    // Fix for SQL -> Markdown issues
    let viewMore = this.state.truncateTitle ? (
      <span>
        ...
        <a className="viewMore" onClick={this.viewMoreTitleHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let viewMoreMessage = this.state.truncateMessage ? (
      <span>
        ...
        <a className="viewMore" target="_blank" onClick={this.viewMoreMessageHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    // clicking for standalone page or URL
    let htmlRender =
      (!!card.resource && (!!card.resource.title || !!card.resource.description)) ||
      card.cardType === 'project';
    let resourceUrl = this.linkExtract();

    const isPptFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/vnd.ms-powerpoint')
    );
    const isPptxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      )
    );
    const isDocFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/msword')
    );
    const isDocxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      )
    );
    const isPdfFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/pdf')
    );

    // Pricing according to geodata
    let countryCode =
      (this.props.currentUser.geoData && this.props.currentUser.geoData.country_code) || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = null;
    let priceSkillCoinData = null;

    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
      priceSkillCoinData = find(card.prices, { currency: 'SKILLCOIN' });
    }

    // Recommended Block sections
    // Defined here to map through later on
    let recommendBlocks = {
      CC: <RecommendedBlock type="cards" card_id={card.eclId || card.id} />,
      CU: <RecommendedBlock type="users" tag="CU" card_id={card.eclId || card.id} />,
      CUC: <RecommendedBlock type="cards" tag="CUC" card_id={card.eclId || card.id} />,
      CT: <RecommendedBlock type="topics" tag="CT" card_id={card.eclId || card.id} />
    };
    // Get the order based on LD flag
    let cantBuyWithSkillCoins =
      this.props.currentUser.walletBalance < (priceSkillCoinData && priceSkillCoinData.amount);
    let skillcoin_image = '/i/images/skillcoin-new.png';

    let isShowText =
      params.cardType === 'TEXT' ||
      params.cardType === 'PATHWAY' ||
      params.cardType === 'JOURNEY' ||
      params.cardType === 'IMAGE' ||
      params.cardType === 'ARTICLE' ||
      params.cardType === 'COURSE';
    let isWithFile = params.imageExists && isShowText;
    let isVideo = params.cardType.toUpperCase() === 'VIDEO' || card.cardSubtype === 'video';

    let eclLogoObj = this.props.logoObj && this.props.logoObj[card.eclSourceTypeName];
    let isProvider =
      (!params.hideProvider && card.providerImage) ||
      (!this.isCsodCourse &&
        ((card.eclSourceLogoUrl && !card.providerImage) || eclLogoObj !== undefined));
    let isAuthor = card.author && params.showCreator;

    let isShowDescriptionBeforeFile =
      params.cardType === 'VIDEO' &&
      card.resource &&
      card.resource.description &&
      (!card.message || card.message === card.resource.title) &&
      !card.name;
    let isShowTextBeforeFile =
      isShowDescriptionBeforeFile ||
      ((isPptFile ||
        isPptxFile ||
        isDocFile ||
        isDocxFile ||
        isPdfFile ||
        card.cardType === 'VideoStream' ||
        card.cardType === 'video_stream') &&
        params.message) ||
      (card.message &&
        (params.cardType === 'ARTICLE' ||
          card.cardType === 'course' ||
          params.cardType === 'VIDEO') &&
        card.message !== card.resource.title);

    return (
      <div className="flex">
        <div
          className="insightV2"
          ref={node => (this._insight = node)}
          onClick={() => {
            if (!this.isCsodCourse) return;
            this.deeplink();
          }}
        >
          <BreadcrumbV2
            isStandaloneModal={this.props.isStandaloneModal}
            dataCard={this.props.dataCard}
            cardUpdated={this.props.cardUpdated}
          />
          <div className="insight-v3">
            <CardV3Assignor assignor={card.assigner} sharedBy={card.sharedBy} />
            {!this.isCsodCourse && (
              <div className="insight-v3__header">
                <CardV3Header
                  card={card}
                  params={params}
                  handleCardClicked={this.props.handleCardClicked}
                  isNameCut={this.props.isNameCut}
                  authorName={this.props.authorName}
                  eclLogoObj={eclLogoObj}
                  isProvider={isProvider}
                  currentUser={this.props.currentUser}
                  type="standalone"
                  isWithFile={
                    isWithFile ||
                    isVideo ||
                    params.cardType === 'ARTICLE' ||
                    params.cardType === 'COURSE'
                  }
                />
                {formattedDate && (
                  <span className="card-v3__card-type card-v3__published-date">
                    <TooltipLabel text={'Published Date'} html={true} position={'bottom'}>
                      {formattedDate}
                    </TooltipLabel>
                  </span>
                )}
                {card.cardType !== 'video_stream' && card.cardType !== 'VideoStream' && (
                  <CardV3SkillTypeEffort
                    allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
                    params={params}
                    card={card}
                    rateCard={this.rateCard}
                    isOnTheFile={false}
                    type="standalone"
                  />
                )}
              </div>
            )}

            {(isShowTextBeforeFile ||
              (!params.isFileAttached && params.cardType === 'TEXT') ||
              (isPptFile || isPptxFile || isDocFile || isDocxFile || isPdfFile) ||
              (card.cardType === 'VideoStream' ||
                card.cardType === 'video_stream' ||
                card.cardType === 'poll' ||
                card.cardType === 'quiz') ||
              (card.title &&
                (card.title !== card.resource.title &&
                  card.message !== card.resource.title &&
                  params.cardType !== 'VIDEO' &&
                  card.title !== card.resource.url))) && <div className="line" />}

            <div
              className={`insight-v3__container ${
                isShowTextBeforeFile
                  ? 'insight-v3__container_small-top-padding'
                  : (params.isFileAttached && params.cardType === 'TEXT') ||
                    card.cardType === 'poll' ||
                    card.cardType === 'quiz'
                  ? 'insight-v3__container_10-top-padding'
                  : ''
              }`}
            >
              <div className="resource-card">
                {(card.cardType === 'media' ||
                  card.cardType === 'poll' ||
                  card.cardType === 'course' ||
                  card.cardType === 'project' ||
                  this.isCsodCourse) &&
                  (!!scormCard ? card.state && card.state === 'published' : true) && (
                    <div className="">
                      {!scormCard && (
                        <div onClick={this.standaloneLinkClickHandler.bind(this)}>
                          {card.title &&
                            (card.title !== card.resource.title &&
                              params.cardType !== 'VIDEO' &&
                              card.title !== card.resource.url) && (
                              <div className="card-v3__text card-standalone-v3__text-container_above-image">
                                {htmlRender &&
                                  ((
                                    <div
                                      dangerouslySetInnerHTML={{
                                        __html: (this.state.truncateTitle
                                          ? card.title.substr(0, 260)
                                          : card.title
                                        ).replace(/\n/gi, '\n\n')
                                      }}
                                    />
                                  ) || (
                                    <MarkdownRenderer
                                      markdown={(this.state.truncateTitle
                                        ? card.title.substr(0, 260)
                                        : card.title
                                      ).replace(/\n/gi, '\n\n')}
                                    />
                                  ))}
                                {viewMore}
                              </div>
                            )}
                          {(isPptFile || isPptxFile || isDocFile || isDocxFile || isPdfFile) &&
                            params.message && (
                              <div className="card-v3__text card-standalone-v3__text-container_above-image">
                                <RichTextReadOnly
                                  text={convertRichText(
                                    this.state.truncateMessage
                                      ? this.truncateMessageText(params.message)
                                      : params.message
                                  )}
                                />
                                {viewMore}
                              </div>
                            )}
                          {!params.isFileAttached &&
                            params.imageFileResources &&
                            (!card.resource || card.resource.type !== 'Video') &&
                            this.imageContainer(card.fileResources[0].fileUrl)}
                          {params.isFileAttached && (
                            <div>
                              {!(
                                params.cardType === 'POLL' &&
                                card.resource &&
                                card.resource.type &&
                                card.resource.type.toLowerCase() === 'video'
                              ) &&
                                (card.filestack[0].mimetype &&
                                  card.filestack[0].mimetype.indexOf('video/') > -1) && (
                                  <div
                                    key={`card-handle-${card.filestack[0].handle}`}
                                    className="fp fp_video"
                                  >
                                    <span>
                                      <video
                                        onMouseEnter={() => {
                                          this.setState({ [card.filestack[0].handle]: true });
                                        }}
                                        onMouseLeave={() => {
                                          this.setState({ [card.filestack[0].handle]: false });
                                        }}
                                        controls
                                        src={card.filestack[0].url}
                                        poster={
                                          card.filestack &&
                                          card.filestack[1] &&
                                          card.filestack[1].url
                                        }
                                        preload={
                                          card.filestack && card.filestack[1] ? 'none' : 'auto'
                                        }
                                        autoPlay={this.props.team.config['enable-video-auto-play']}
                                        controlsList={
                                          params.isDownloadContentDisabled ? 'nodownload' : ''
                                        }
                                        onError={() => {
                                          this.refreshMediaUrl('video', card.filestack[0].handle);
                                        }}
                                      />
                                      {!params.isDownloadContentDisabled &&
                                        this.downloadBlock(card.filestack[0])}
                                    </span>
                                  </div>
                                )}
                              {!(
                                params.cardType === 'POLL' &&
                                card.resource &&
                                card.resource.type &&
                                card.resource.type.toLowerCase() === 'video'
                              ) &&
                                (card.filestack[0].mimetype &&
                                  card.filestack[0].mimetype.indexOf('video/') === -1) &&
                                card.filestack.map(file => {
                                  if (
                                    (isPptFile ||
                                      isPptxFile ||
                                      isDocFile ||
                                      isDocxFile ||
                                      isPdfFile) &&
                                    (card &&
                                      card.filestack &&
                                      card.filestack[1] &&
                                      card.filestack[1].mimetype &&
                                      card.filestack[1].mimetype.includes('image/')) &&
                                    file.mimetype.includes('image/')
                                  ) {
                                    return;
                                  }
                                  if (
                                    file.mimetype &&
                                    ~file.mimetype.indexOf('image/') &&
                                    (card.fileResources &&
                                      card.fileResources.length === 0 &&
                                      !card.filestack.scorm_course) &&
                                    (!card.resource || card.resource.type !== 'Video')
                                  ) {
                                    return this.imageContainer(file.url, file.handle);
                                  } else if (
                                    file.mimetype &&
                                    ~file.mimetype.indexOf('image/') &&
                                    (card.fileResources &&
                                      card.fileResources.length > 0 &&
                                      card.fileResources[0].filestack &&
                                      !card.fileResources[0].filestack.scorm_course) &&
                                    (!card.resource || card.resource.type !== 'Video')
                                  ) {
                                    return this.imageContainer(file.url, file.handle);
                                  } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                                    return (
                                      <div key={`handle-${file.handle}`} className="fp fp_video">
                                        <span>
                                          {this.state.transcodedVideoStatus &&
                                            (this.state.transcodedVideoStatus === 'completed' ? (
                                              <video
                                                onMouseEnter={() => {
                                                  this.setState({ [file.handle]: true });
                                                }}
                                                onMouseLeave={() => {
                                                  this.setState({ [file.handle]: false });
                                                }}
                                                controls
                                                src={this.state.transcodedVideoUrl}
                                                controlsList={
                                                  params.isDownloadContentDisabled
                                                    ? 'nodownload'
                                                    : ''
                                                }
                                                autoPlay={
                                                  this.props.team.config['enable-video-auto-play']
                                                }
                                              />
                                            ) : (
                                              <img
                                                className="waiting-video"
                                                src="/i/images/video_processing_being_processed.jpg"
                                              />
                                            ))}
                                          {!params.isDownloadContentDisabled &&
                                            this.downloadBlock(file)}
                                        </span>
                                      </div>
                                    );
                                  } else if (
                                    this.state.standaloneCardFilePreview &&
                                    ((file.mimetype && ~file.mimetype.indexOf('application/pdf')) ||
                                      isPptFile ||
                                      isPptxFile ||
                                      isDocFile ||
                                      isDocxFile ||
                                      isPdfFile)
                                  ) {
                                    return (
                                      <div key={`handle-${file.handle}`} className="">
                                        <div className="row">
                                          <div className="medium-6 large-6 columns">
                                            <div
                                              className="sidebar-toggle-button"
                                              onClick={this.toggleSidbare.bind(this)}
                                            />
                                          </div>
                                          <div className="medium-6 large-6 columns text-right navigation-pdf-pages">
                                            <div className="arrow-left-icon">
                                              <ArrowLeft
                                                onClick={this.handlePrevious}
                                                style={this.styles.arrowLeftIcon}
                                              />
                                            </div>
                                            <div className="current-page-pdf">
                                              {this.state.page}/{this.state.pages}
                                            </div>
                                            <div className="arrow-right-icon">
                                              <ArrowRight
                                                onClick={this.handleNext}
                                                style={this.styles.arrowRightIcon}
                                              />
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className={`sidebar-container${
                                            isPptFile || isPptxFile
                                              ? ' sidebar-container-for-ppt'
                                              : ''
                                          }`}
                                        >
                                          <Sidebar
                                            styles={this.stylesSidebar}
                                            shadow={false}
                                            sidebar={this.viewPagesPDF().map(num => {
                                              return (
                                                <div key={`pagesPDF-${num}`} className="row">
                                                  <div
                                                    onClick={() => {
                                                      this.setState({ page: num });
                                                    }}
                                                    className="mini-pdf"
                                                  >
                                                    <PDF
                                                      scale={0.2}
                                                      file={
                                                        isPptFile ||
                                                        isPptxFile ||
                                                        isDocFile ||
                                                        isDocxFile
                                                          ? `https://process.filestackapi.com/output=f:pdf/${
                                                              file.url
                                                            }`
                                                          : file.url
                                                      }
                                                      page={num}
                                                      loading={<div>{tr('Loading file...')}</div>}
                                                    />
                                                    <div className="num-page">{num}</div>
                                                  </div>
                                                </div>
                                              );
                                            })}
                                            open={this.state.sidebarOpen}
                                            docked={this.state.sidebarOpen}
                                            onSetOpen={this.onSetSidebarOpen}
                                          >
                                            <div className="main-pdf">
                                              <PDF
                                                file={
                                                  isPptFile || isPptxFile || isDocFile || isDocxFile
                                                    ? `https://process.filestackapi.com/output=f:pdf/${
                                                        file.url
                                                      }`
                                                    : file.url
                                                }
                                                onDocumentComplete={this.onDocumentComplete}
                                                onPageComplete={this.onPageComplete}
                                                page={this.state.page}
                                                loading={
                                                  <div className="data-not-available-msg">
                                                    {tr('Loading file...')}
                                                  </div>
                                                }
                                              />
                                              <div className="current-main-page">
                                                {this.state.page}
                                              </div>
                                              <div
                                                className={
                                                  this.state.sidebarOpen
                                                    ? 'line-bottom line-bottom-sidebar'
                                                    : 'line-bottom'
                                                }
                                              />
                                            </div>
                                          </Sidebar>
                                        </div>
                                      </div>
                                    );
                                  } else {
                                    return file.mimetype && ~file.mimetype.indexOf('audio/') ? (
                                      <audio
                                        controls
                                        src={file.url}
                                        style={this.styles.audio}
                                        onError={() => {
                                          this.refreshMediaUrl('audio', file.handle);
                                        }}
                                      />
                                    ) : (
                                      <span>
                                        {!scormCard && (
                                          <div
                                            key={`card-file-handle-${file.handle}`}
                                            className="fp fp_preview"
                                          >
                                            <span>
                                              {params.cardType === 'FILE' ? (
                                                <iframe
                                                  height="500px"
                                                  width="100%"
                                                  src={pdfPreviewUrl(
                                                    file.url,
                                                    expireAfter,
                                                    this.props.currentUser.id
                                                  )}
                                                />
                                              ) : (
                                                <a
                                                  className="fp inline-box"
                                                  href={resourceUrl}
                                                  target="_blank"
                                                />
                                              )}
                                              {!params.isDownloadContentDisabled &&
                                                this.downloadBlock(file)}
                                            </span>
                                          </div>
                                        )}
                                      </span>
                                    );
                                  }
                                })}
                            </div>
                          )}
                          {params.message &&
                            !card.resource.title &&
                            !scormCard &&
                            card.cardType !== 'course' &&
                            !isPptFile &&
                            !isPptxFile &&
                            !isDocFile &&
                            !isDocxFile &&
                            !isPdfFile && (
                              <div className="card-v3__text">
                                <RichTextReadOnly
                                  text={convertRichText(
                                    this.state.truncateMessage
                                      ? this.truncateMessageText(params.message)
                                      : params.message
                                  )}
                                />
                                {viewMoreMessage}
                              </div>
                            )}
                        </div>
                      )}
                      {card.cardType == 'project' && card.filestack && this.state.showProjectCard && (
                        <div className="openCardFromSearch clearfix">
                          <div className="common-margin">
                            <Divider />
                          </div>
                          {((!this.state.videoSubmitted &&
                            (this.state.submissionList &&
                              this.state.submissionList.length > 0 &&
                              this.state.submissionList[0].projectSubmissionReviews &&
                              this.state.submissionList[0].projectSubmissionReviews.length > 0 &&
                              (this.state.submissionList[0].projectSubmissionReviews[0].status !==
                                'approved' ||
                                this.state.submissionList[0].projectSubmissionReviews[0].status ===
                                  'rejected'))) ||
                            (this.state.submissionList &&
                              this.state.submissionList.length == 0)) && (
                            <div>
                              <span>{tr('ADD YOUR SUBMISSION')}</span>{' '}
                              <span style={this.styles.submissionList}>
                                {tr('You can submit only one file per project for approval.')}
                              </span>
                              <FlatButton
                                className="image-upload overview-tags-container"
                                icon={<VideoIcon style={this.styles.videoIcon} color="#bababa" />}
                                onClick={this.fileStackHandler.bind(
                                  this,
                                  videoType,
                                  this.addFileStackFiles.bind(this)
                                )}
                              />
                              {this.state.fileStack &&
                                this.state.fileStack[0] &&
                                this.state.fileStack[0].mimetype &&
                                this.state.fileStack[0].mimetype.indexOf('video/') > -1 && (
                                  <div className="text-center">
                                    <div>
                                      <div className="small-12 relative">
                                        {this.videoSourcePreview(
                                          this.state.fileStack[0],
                                          isDownloadContentDisabled
                                        )}
                                      </div>
                                      <div className="small-12 file-title">
                                        <span className="item-name">
                                          {this.state.fileStack[0].filename ||
                                            this.state.fileStack[0].handle}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                )}
                              <div className="error-text">{this.state.uploadErrorText}</div>
                              <div className="overview-tags-container">
                                <TextFieldCustom
                                  hintText="Description"
                                  value={this.state.description}
                                  inputChangeHandler={this.descriptionCardChange.bind(this)}
                                  rowsMax={4}
                                  rows={4}
                                  multiLine={true}
                                  fullWidth={true}
                                />
                                {this.state.optionsError && (
                                  <div className="error-text">{tr(this.state.optionsError)}</div>
                                )}
                              </div>
                              <div className="action-buttons overview-tags-container">
                                <FlatButton
                                  label={tr(this.state.createLabel)}
                                  className={this.styles.create}
                                  onTouchTap={this.submitClickHandler.bind(this, card)}
                                  labelStyle={this.styles.btnLabel}
                                  style={
                                    !this.state.disableSubmit
                                      ? this.styles.disabled
                                      : this.styles.create
                                  }
                                />
                              </div>
                            </div>
                          )}
                          <div className="overview-tags-container">
                            {this.state.submissionList && this.state.submissionList.length > 0 && (
                              <div>{tr('Previous Submissions')}</div>
                            )}
                            {this.state.submissionList &&
                              this.state.submissionList.map((list, index) => {
                                return (
                                  <div key={index} style={this.styles.submissionList}>
                                    {list.filestack[0].filename}
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}
                      {((card.resource &&
                        (card.resource.url ||
                          card.resource.description ||
                          card.resource.fileUrl)) ||
                        this.isCsodCourse) && (
                        <div>
                          {card.message &&
                            (params.cardType === 'ARTICLE' ||
                              card.cardType === 'course' ||
                              params.cardType === 'VIDEO') &&
                            card.message !== card.resource.title && (
                              <h5
                                className="card-v3__text resource-message"
                                dangerouslySetInnerHTML={{ __html: card.message }}
                              />
                            )}

                          {this.isCsodCourse && card.name && (
                            <h5 className="resource-message">{card.name}</h5>
                          )}
                          {isShowDescriptionBeforeFile && (
                            <div className="description description_top">
                              {card.resource.description}
                              {!params.isYoutubeVideo && !params.isVimeoVideo && (
                                <a
                                  title={tr('open in new tab')}
                                  className="link-to-original"
                                  onClick={e => this.handleResourceUrlClicked(e, resourceUrl)}
                                  href="#"
                                >
                                  {' '}
                                  {tr('View More')}
                                </a>
                              )}
                            </div>
                          )}
                          <div className={`resource `}>
                            {params.cardType === 'VIDEO' &&
                              card.resource.description &&
                              (!card.message ||
                                card.message !== card.resource.title ||
                                card.name) && (
                                <div className="card-v3__text">
                                  {card.resource.description}
                                  {!params.isYoutubeVideo && !params.isVimeoVideo && (
                                    <a
                                      title={tr('open in new tab')}
                                      className="link-to-original"
                                      onClick={e => this.handleResourceUrlClicked(e, resourceUrl)}
                                      href="#"
                                    >
                                      {' '}
                                      {tr('View More')}
                                    </a>
                                  )}
                                </div>
                              )}
                            {(card.resource.embedHtml ||
                              card.resource.videoUrl ||
                              card.resource.url) &&
                              card.resource.type === 'Video' && (
                                <InlineVideo
                                  cardId={this.props.card.id}
                                  embedHtml={
                                    card.resource.embedHtml ||
                                    (checkResourceURL(
                                      card.resource.videoUrl || this.embedUrl(card.resource.url)
                                    )
                                      ? `<iframe width="1" src="${card.resource.videoUrl ||
                                          this.embedUrl(card.resource.url)}"></iframe>`
                                      : thumbnailImage(
                                          this.state.card.cardId || this.state.card.id,
                                          this.state.card.resource.imageUrl,
                                          this.props.currentUser.id
                                        ))
                                  }
                                  videoUrl={card.resource.videoUrl}
                                />
                              )}
                            {card.resource.fileUrl && <img src={card.resource.fileUrl} />}
                            <div className="clearfix">
                              {card.resource.title &&
                                params.cardType !== 'VIDEO' &&
                                card.readableCardType !== 'video' &&
                                card.resource.title !== card.resource.url && (
                                  <div
                                    className={`${
                                      params.cardType === 'ARTICLE' || card.cardType === 'course'
                                        ? 'resource-title-block__article'
                                        : ''
                                    }`}
                                  >
                                    {card.resource &&
                                    card.resource.url &&
                                    card.resource.url.includes('/api/scorm/') ? (
                                      ' '
                                    ) : (
                                      <h5
                                        className={`resource-title ${
                                          params.cardType === 'ARTICLE'
                                            ? 'resource-title__article'
                                            : ''
                                        }`}
                                        dangerouslySetInnerHTML={{
                                          __html: this.truncatedTitle(
                                            params.cardType === 'ARTICLE'
                                              ? card.resource.title || card.message || card.title
                                              : card.resource.title,
                                            card.resource.url
                                          )
                                        }}
                                      />
                                    )}
                                    {params.cardType === 'ARTICLE' ||
                                      (card.cardType === 'course' && (
                                        <div className="align-self-middle">
                                          {card.eclSourceTypeName &&
                                            logoObj[card.eclSourceTypeName.toLowerCase()] !==
                                              undefined &&
                                            this.props.author && (
                                              <span className="insight-img-logo">
                                                <img
                                                  src={
                                                    logoObj[card.eclSourceTypeName.toLowerCase()]
                                                  }
                                                />
                                              </span>
                                            )}
                                        </div>
                                      ))}
                                  </div>
                                )}
                              {(params.cardType === 'ARTICLE' ||
                                card.cardType === 'course' ||
                                this.isCsodCourse) && (
                                <div className="article-resource">
                                  <div className="description">
                                    {this.imageContainer(resourceImage, null, 'article')}
                                    <div
                                      style={{ display: 'inline' }}
                                      dangerouslySetInnerHTML={{
                                        __html: card.resource.description
                                      }}
                                    />
                                    {!this.isCsodCourse && card.state === 'published' ? (
                                      <a
                                        title={tr('open in new tab')}
                                        className={
                                          this.props.card.paymentEnabled &&
                                          !this.props.card.paidByUser
                                            ? 'link-to-original hide'
                                            : 'link-to-original'
                                        }
                                        onClick={e => this.handleResourceClicked(e)}
                                        href="#"
                                      >
                                        {' '}
                                        {card.resource &&
                                        card.resource.url &&
                                        card.resource.url.includes('/api/scorm/')
                                          ? tr('Click here to access the course')
                                          : tr('View More')}
                                      </a>
                                    ) : (
                                      <div className="scorm-processing">
                                        <Spinner />
                                        <p>{this.setScormStateMsg(card)}</p>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              )}
                              {card.resource.imageUrl &&
                                !card.resource.embedHtml &&
                                card.resource.type !== 'Video' &&
                                params.cardType !== 'ARTICLE' &&
                                card.cardType !== 'course' &&
                                !params.isYoutubeVideo && (
                                  <div className="image-container">
                                    {this.imageContainer(card.resource.imageUrl)}
                                  </div>
                                )}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                {scormCard &&
                  !(!!card.resource && !!card.resource.url) &&
                  card.state &&
                  card.state !== 'published' && (
                    <div className="article-resource">
                      <div className="description">
                        {this.imageContainer(resourceImage)}
                        <div className="scorm-processing">
                          <Spinner />
                          <p>{this.setScormStateMsg(card)}</p>
                        </div>
                      </div>
                    </div>
                  )}
                {(card.cardType === 'pack' || card.cardType === 'pack_draft') && (
                  <PathwayCover card={card} currentUser={this.props.currentUser} />
                )}
                {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
                  <VideoStream
                    card={card}
                    standaloneLinkClickHandler={this.standaloneLinkClickHandler}
                  />
                )}
              </div>
              {card.cardType === 'poll' && (
                <Poll
                  cardOverview={true}
                  cardUpdated={this.cardUpdated.bind(this)}
                  card={card}
                  checkComplete={this.checkComplete}
                  currentUserId={this.props.currentUser.id}
                />
              )}

              <div id="content-topics-ref">
                {tr('SmartCard Topics')}: <span id="content-topics" />
              </div>

              {((card.tags && card.tags.length > 0) ||
                (card.channels && card.channels.length > 0)) &&
                card.cardType !== 'poll' &&
                card.cardType !== 'quiz' && (
                  <CardV3ChannelAndTags card={card} cardType="standalone" />
                )}

              {params.cardType !== 'PATHWAY' &&
                params.cardType !== 'JOURNEY' &&
                card.cardType !== 'video_stream' &&
                card.cardType !== 'VideoStream' &&
                card.cardType !== 'poll' &&
                card.cardType !== 'quiz' &&
                !params.isHideSpecialInfo && (
                  <CardV3SkillcoinPrice
                    card={card}
                    edcastPricing={this.state.edcastPricing}
                    edcastPlansForPricing={this.state.edcastPlansForPricing}
                    cardType="standalone"
                    params={params}
                  />
                )}

              <CardV3Footer
                isCsodCourse={this.isCsodCourse}
                card={card}
                isShowLeap={this.props.isShowLeap}
                params={params}
                lockPathwayCardFlag={this.state.lockPathwayCardFlag}
                tooltipPosition={this.props.tooltipPosition}
                toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                previewMode={this.props.previewMode}
                hideActions={this.props.hideActions}
                disableTopics={false}
                dismissible={this.props.dismissible}
                providerCards={this.props.providerCards}
                showTopicToggleClick={this.props.showTopicToggleClick}
                isStandalone={this.props.isStandalone}
                cardUpdated={this.cardUpdated}
                removeCardFromList={this.props.removeCardFromList}
                hideComplete={this.props.hideComplete}
                isCompleted={this.state.isCompleted}
                deleteSharedCard={this.props.deleteSharedCard}
                channel={this.props.channel}
                type="StandAlone"
                averageRating={this.state.averageRating}
                ratingCount={this.state.ratingCount}
                handleCardClicked={this.props.handleCardClicked}
                comments={this.state.comments}
                isLiveStream={this.props.isLiveStream}
                commentsCount={this.state.commentsCount}
                openReasonReportModal={this.props.openReasonReportModal}
                clickOnComments={this.props.clickOnComments}
                cardSectionName={this.props.cardSectionName}
                removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                cardLikeHandler={this.cardLikeHandler}
                callCompleteClickHandler={this.completeClickHandler}
                cardStatusButton={this.cardStatusButton}
                handleCardAnalayticsModal={this.handleCardAnalayticsModal}
                ratingChanged={this.ratingChanged}
                markAsCompleteEnableForLink={this.state.markAsCompleteEnableForLink}
                isStandalonePage={true}
              />
            </div>

            {this.state.showComment && !this.isCsodCourse && (
              <div className="comments-block">
                {!this.state.isLiveStream && (
                  <CommentList
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.props.author ? this.props.author.id : ''}
                    cardType={card.cardType}
                    comments={this.state.comments}
                    pending={card.commentPending}
                    numOfComments={this.state.commentsCount}
                    videoId={card.cardId ? card.id : null}
                    updateCommentCount={this.updateCommentCount}
                    isCardV3={true}
                  />
                )}
                {this.state.isLiveStream && (
                  <LiveCommentList
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.props.author ? this.props.author.id : ''}
                    cardType={card.cardType}
                    pending={card.commentPending}
                    videoId={card.cardId ? card.id : null}
                    updateCommentCount={this.updateCommentCount}
                    cardStatus={this.getCardStatus()}
                    cardUuid={this.getCardUuid()}
                    liveCommentsCount={this.liveCommentsCount}
                    onMessageReceiveJoinLeave={this.onMessageReceiveJoinLeave}
                    isCardV3={true}
                  />
                )}
              </div>
            )}

            {this.state.showRecommendedContent && (
              <div className="recommended-block">
                {this.state.recommendBlocksConfig.map(block => {
                  return <div key={`recommend-block-${block}`}>{recommendBlocks[block]}</div>;
                })}
              </div>
            )}
          </div>

          {this.state.modalClassesOpen && (
            <CardClassesModal
              classes={this.props.card.additional_metadata.classes}
              closeModal={this.closeClassesModal}
            />
          )}
        </div>

        {this.state.edcastPricing &&
          this.state.wallet &&
          card.isPaid &&
          (priceSkillCoinData || priceData) &&
          this.props.card.paymentEnabled && (
            <div className="insights-pricing-info">
              {priceSkillCoinData && (
                <div className="useSkillCoins">
                  <p className="numberOfCoins">
                    <img className="pricing_skill_coins_tile_icon" src={skillcoin_image} />
                    {parseInt(priceSkillCoinData.amount)}
                  </p>
                  <PrimaryButton
                    disabled={
                      cantBuyWithSkillCoins ||
                      this.props.card.paidByUser ||
                      this.state.cardPurchased
                    }
                    onClick={this.openCardWalletModal.bind(this, priceSkillCoinData)}
                    label={tr('BUY WITH SKILLCOINS')}
                    className="review-btn"
                    style={this.styles.buyWithSkillCoins}
                  />
                  <div className="seperator" />
                </div>
              )}
              {priceData &&
                this.props.team.paymentGateways &&
                this.props.team.paymentGateways.braintree && (
                  <div>
                    <div className="otherOptions">
                      <p className="numberOfCoins">
                        Price: {priceData.symbol}
                        {parseInt(priceData.amount)}
                      </p>
                      <div>
                        <PrimaryButton
                          disabled={this.props.card.paidByUser || this.state.cardPurchased}
                          label={tr('BUY NOW')}
                          onClick={this.openCardWalletModal.bind(this, priceData)}
                          className="review-btn"
                          style={this.styles.buyNow}
                        />
                      </div>
                    </div>
                    <div className="seperator" />
                  </div>
                )}
              {priceData &&
                this.props.team.paymentGateways &&
                this.props.team.paymentGateways.paypal && (
                  <div>
                    <div className="otherOptions">
                      <p className="numberOfCoins">
                        Price: {priceData.symbol}
                        {parseInt(priceData.amount)}
                      </p>
                      <div>
                        <PrimaryButton
                          disabled={this.props.card.paidByUser || this.state.cardPurchased}
                          label={tr('BUY NOW')}
                          onClick={this.openPayPalPayment.bind(this, priceData)}
                          className="review-btn"
                          style={this.styles.buyNow}
                        />
                      </div>
                    </div>
                    <div className="seperator" />
                  </div>
                )}
              <div className="card-features">
                <p>
                  <span>
                    <FullTimeAccess />
                  </span>
                  <span>{tr('Full time access')}</span>
                </p>
                <p>
                  <span>
                    <Support />
                  </span>
                  <span>24/7 {tr('Support')}</span>
                </p>
                <p>
                  <span>
                    <WebandMobile />
                  </span>
                  <span>{tr('Access on Web and Mobile')}</span>
                </p>
                <p>
                  <span>
                    <Certificate />
                  </span>
                  <span>{tr('Certificate of Completion')}</span>
                </p>
              </div>
            </div>
          )}
      </div>
    );
  }
}

InsightV3.defaultProps = {
  isMarkAsCompleteDisabled: false,
  toggleSearch: null,
  isStandalone: false
};

InsightV3.propTypes = {
  card: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  dataCard: PropTypes.object,
  team: PropTypes.object,
  author: PropTypes.object,
  channels: PropTypes.object,
  pathname: PropTypes.string,
  cardId: PropTypes.string,
  dismissible: PropTypes.bool,
  isStandaloneModal: PropTypes.bool,
  isActionsDisabled: PropTypes.bool,
  showComment: PropTypes.bool,
  feed: PropTypes.any,
  isMarkAsCompleteDisabled: PropTypes.bool,
  removeCardFromList: PropTypes.func,
  hideComplete: PropTypes.func,
  toggleSearch: PropTypes.func,
  isStandalone: PropTypes.bool,
  cardUpdated: PropTypes.func,
  params: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    feed: state.feed.toJS(),
    currentUser: state.currentUser.toJS(),
    channels: state.channels.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(InsightV3);
