import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getMentionSuggest } from '../../actions/usersActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import ReactDOM from 'react-dom';
import BlurImage from '../common/BlurImage';

//UI components
import Popover from 'react-simple-popover';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import pubnubAdapter from '../../utils/pubnubAdapter';

//actions
import { tr } from 'edc-web-sdk/helpers/translations';

class LiveCommentInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.state = {
      open: false,
      inputValue: '',
      mentionQuery: '',
      sendingComment: false,
      userTagging: window.ldclient.variation('user-tagging', true)
    };
    this.styles = {
      selectContainer: {
        width: '100%',
        zIndex: '10',
        maxHeight: '112px',
        overflow: 'auto',
        boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.3)',
        background: 'white'
      },
      selectContent: {
        width: '100%',
        marginTop: '-5px',
        padding: 0
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '-0.2rem',
        left: '1rem'
      }
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.inputBlurHandler = this.inputBlurHandler.bind(this);
    this.submitInputHandler = this.submitInputHandler.bind(this);
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      ['first_name', 'last_name'],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);

    if (this.props.autoFocus) {
      this.refs.input.focus();
    }
  }

  autoSize = () => {
    const id = this.props.isPinned
      ? `commentField-pin-${this.props.cardId}`
      : `commentField-${this.props.cardId}`;
    let el = document.getElementById(id);
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
  };

  postCommentOnPubNub = comment => {
    //prepare payload
    var timeStamp = Math.floor(Date.now());
    var pubnubPayload = {};
    pubnubPayload['uid'] = this.props.currentUser.id;
    pubnubPayload['p'] = this.props.currentUser.picture;
    pubnubPayload['fn'] = this.props.currentUser.first_name;
    pubnubPayload['ln'] = this.props.currentUser.last_name;
    pubnubPayload['h'] = this.props.currentUser.handle;
    pubnubPayload['a'] = 'comment';
    pubnubPayload['m'] = comment;
    pubnubPayload['type'] = 'videoStream';
    pubnubPayload['msgId'] = this.props.currentUser.id + '_' + timeStamp;
    //publish to pubnub
    this.pubnub.publish({
      channel: this.props.cardUuid,
      message: pubnubPayload,
      callback: m => {}
    });
  };

  inputChangeHandler(event) {
    let string = event.target.value;
    let parts = string.split(' ');
    this.autoSize();
    if (parts.length && parts[parts.length - 1].charAt(0) === '@') {
      let mentionQuery = parts[parts.length - 1].slice(1).toLowerCase();

      let { userTagging } = this.state;

      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }

        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        inputValue: string,
        open: true,
        anchorEl: ReactDOM.findDOMNode(this.refs.input),
        mentionQuery
      });
    } else if (parts.length > 1 && parts[parts.length - 2].charAt(0) === '@') {
      let mentionQuery =
        parts[parts.length - 2].slice(1).toLowerCase() +
        ' ' +
        parts[parts.length - 1].toLowerCase();

      let { userTagging } = this.state;

      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }

        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        inputValue: string,
        open: true,
        anchorEl: ReactDOM.findDOMNode(this.refs.input),
        mentionQuery
      });
    } else {
      this.setState({ inputValue: string, open: false, mentionQuery: '' });
    }
  }

  inputBlurHandler(event) {
    this.setState({ open: false });
  }

  mentionClickHandler(value) {
    let inputValue = this.state.inputValue;
    let mentionQuery = '@' + this.state.mentionQuery;
    var n = inputValue.toLowerCase().lastIndexOf(mentionQuery.toLowerCase());
    inputValue =
      inputValue.slice(0, n) + inputValue.slice(n).replace(new RegExp(mentionQuery, 'i'), value);
    this.setState({ inputValue: inputValue + ' ' });
    setTimeout(() => {
      this.refs.input.focus();
    }, 20);
  }

  submitInputHandler = e => {
    if (!!this.state.inputValue.trim()) {
      if (e.keyCode === 13) {
        this.setState({ sendingComment: true }, () => {
          this.postCommentOnPubNub(this.state.inputValue);
          this.setState({
            inputValue: '',
            sendingComment: false
          });
        });
      }
    }
  };

  handleClose(e) {
    this.setState({ open: false });
  }

  render() {
    let usersMap = this.props.users.get('idMap');
    let usersArr = usersMap.filter(user => {
      let userName = user.get('fullName') || user.get('name');
      let userHandle = user.get('handle');
      return (
        (userName && userName.toLowerCase().indexOf(this.state.mentionQuery) !== -1) ||
        (userHandle && userHandle.toLowerCase().indexOf(this.state.mentionQuery) !== -1)
      );
    });
    return (
      <div className="comment-input-modal">
        <textarea
          rows="1"
          id={
            this.props.isPinned
              ? `commentField-pin-${this.props.cardId}`
              : `commentField-${this.props.cardId}`
          }
          className="comment create"
          ref="input"
          placeholder={
            this.props.isCardV3
              ? tr('Leave a comment here...')
              : tr('Leave a comment / Tag peers...')
          }
          onChange={this.inputChangeHandler}
          onBlur={this.inputBlurHandler}
          value={this.state.inputValue}
          onKeyDown={this.submitInputHandler}
          autoFocus={this.props.autoFocus}
          disabled={this.state.sendingComment}
        />

        <Popover
          style={this.styles.selectContent}
          containerStyle={this.styles.selectContainer}
          placement="bottom"
          container={this}
          target={this.refs.input}
          show={this.state.open}
          onHide={this.handleClose.bind(this)}
        >
          <div>
            {usersArr.map((user, index) => {
              let userNameOrHandle =
                user.get('name') == null ? user.get('handle').replace('@', '') : user.get('name');
              return (
                <ListItem
                  key={`comment_user_live_${index}`}
                  leftIcon={
                    <BlurImage
                      style={this.styles.avatarBox}
                      id={user.id}
                      image={user.get('avatarimages').get('small')}
                    />
                  }
                  primaryText={<span>{userNameOrHandle}</span>}
                  onMouseDown={this.mentionClickHandler.bind(this, user.get('handle'))}
                />
              );
            })}
          </div>
        </Popover>
      </div>
    );
  }
}

LiveCommentInput.propTypes = {
  cardId: PropTypes.string.isRequired,
  cardType: PropTypes.string,
  cardUuid: PropTypes.string,
  users: PropTypes.object,
  currentUser: PropTypes.object,
  autoFocus: PropTypes.bool,
  isPinned: PropTypes.bool,
  numOfComments: PropTypes.number,
  isCardV3: PropTypes.bool
};

export default connect(state => ({
  users: state.users,
  currentUser: state.currentUser.toJS()
}))(LiveCommentInput);
