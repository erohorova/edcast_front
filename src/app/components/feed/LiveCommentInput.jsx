import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getMentionSuggest } from '../../actions/usersActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
//UI components
import TextField from 'material-ui/TextField';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import BlurImage from '../common/BlurImage';
import pubnubAdapter from '../../utils/pubnubAdapter';

//actions
import { tr } from 'edc-web-sdk/helpers/translations';

class LiveCommentInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.state = {
      open: false,
      inputValue: '',
      mentionQuery: '',
      sendingComment: false,
      userTagging: window.ldclient.variation('user-tagging', true)
    };
    this.styles = {
      userListPopover: {
        height: '112px',
        overflow: 'scroll'
      },
      anchorOrigin: {
        horizontal: 'left',
        vertical: 'bottom'
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'top'
      },
      commentInputhint: {
        marginTop: '12px',
        bottom: 'inherit'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '-0.2rem',
        left: '1rem'
      }
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.inputBlurHandler = this.inputBlurHandler.bind(this);
    this.submitInputHandler = this.submitInputHandler.bind(this);
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      ['first_name', 'last_name'],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);

    if (this.props.autoFocus) {
      this.refs.input.focus();
    }
  }

  postCommentOnPubNub = comment => {
    //prepare payload
    var pubnubPayload = {};
    pubnubPayload['uid'] = this.props.currentUser.id;
    pubnubPayload['p'] = this.props.currentUser.picture;
    pubnubPayload['fn'] = this.props.currentUser.first_name;
    pubnubPayload['ln'] = this.props.currentUser.last_name;
    pubnubPayload['h'] = this.props.currentUser.handle;
    pubnubPayload['a'] = 'comment';
    pubnubPayload['m'] = comment;
    pubnubPayload['type'] = 'videoStream';
    //publish to pubnub
    this.pubnub.publish({
      channel: this.props.cardUuid,
      message: pubnubPayload,
      callback: m => {}
    });
  };

  inputChangeHandler = event => {
    let commentText = event.target.value;
    let splitCommentText = commentText.split(' ');
    if (
      splitCommentText.length &&
      splitCommentText[splitCommentText.length - 1].charAt(0) === '@'
    ) {
      let mentionQuery = splitCommentText[splitCommentText.length - 1].slice(1).toLowerCase();

      let { userTagging } = this.state;
      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }
        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        inputValue: commentText,
        open: true,
        anchorEl: event.currentTarget,
        mentionQuery
      });
    } else {
      this.setState({ inputValue: commentText, open: false, mentionQuery: '' });
    }
  };

  inputBlurHandler = event => {
    this.setState({ open: false });
  };

  mentionClickHandler = (e, handle) => {
    e.preventDefault();
    let restOfHandle = handle.substr(
      handle.indexOf('@' + this.state.mentionQuery) + this.state.mentionQuery.length + 1
    );
    setTimeout(() => {
      this.setState({ open: false, inputValue: this.state.inputValue + restOfHandle + ' ' });
      this.refs.input.focus();
    }, 300);
  };

  submitInputHandler = e => {
    if (!!this.state.inputValue.trim()) {
      if (e.keyCode === 13) {
        this.setState({ sendingComment: true }, () => {
          this.postCommentOnPubNub(this.state.inputValue);
          this.setState({
            inputValue: '',
            sendingComment: false
          });
        });
      }
    }
  };

  render() {
    let usersMap = this.props.users.get('idMap');
    let usersArr = usersMap.filter(user => {
      let userName = user.get('fullName') || user.get('name');
      let userHandle = user.get('handle');
      return (
        (userName && userName.toLowerCase().indexOf(this.state.mentionQuery) !== -1) ||
        (userHandle && userHandle.toLowerCase().indexOf(this.state.mentionQuery) !== -1)
      );
    });

    return (
      <div className={this.props.inModal ? 'comment-input-modal' : ''}>
        <TextField
          ref="input"
          hintText={tr('Leave a comment / Tag peers...')}
          hintStyle={this.styles.commentInputhint}
          autoComplete="off"
          className="comment create"
          fullWidth={true}
          multiLine={true}
          onChange={this.inputChangeHandler}
          onBlur={this.inputBlurHandler}
          value={this.state.inputValue}
          onKeyDown={this.submitInputHandler}
          autoFocus={true}
          disabled={this.state.sendingComment}
          aria-label={tr('Leave a comment / Tag peers...')}
        />

        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={this.styles.anchorOrigin}
          targetOrigin={this.styles.targetOrigin}
          useLayerForClickAway={false}
          style={this.styles.userListPopover}
          canAutoPosition={true}
          animated={false}
        >
          {usersArr.map((user, index) => {
            let userNameOrHandle =
              user.get('name') == null ? user.get('handle').replace('@', '') : user.get('name');
            return (
              <ListItem
                key={index}
                leftIcon={
                  <BlurImage
                    style={this.styles.avatarBox}
                    id={user.id}
                    image={user.get('avatarimages').get('small')}
                  />
                }
                primaryText={<span>{userNameOrHandle}</span>}
                onMouseDown={e => {
                  this.mentionClickHandler(e, user.get('handle'));
                }}
              />
            );
          })}
        </Popover>
      </div>
    );
  }
}

LiveCommentInput.propTypes = {
  cardId: PropTypes.string.isRequired,
  users: PropTypes.object,
  cardUuid: PropTypes.string,
  inModal: PropTypes.bool,
  currentUser: PropTypes.object,
  autoFocus: PropTypes.bool
};

export default connect(state => ({ users: state.users, currentUser: state.currentUser.toJS() }))(
  LiveCommentInput
);
