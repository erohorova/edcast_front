import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import LiveStream from './LiveStream.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';
import InlineVideo from './InlineVideo.jsx';
import addSecurity from '../../utils/filestackSecurity';

class VideoStream extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      startPlay: false,
      isCardV3: window.ldclient.variation('card-v3', false)
    };
    this.styles = {
      playIcon: {
        height: '50px',
        width: '50px',
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginLeft: this.state.isCardV3 ? '0px' : '-25px',
        marginTop: '-25px'
      },
      mainSvg: {
        zIndex: 2,
        position: 'relative'
      },
      svgImage: {
        zIndex: 2
      },
      video: {
        height: '85%'
      }
    };
  }

  componentDidMount() {
    let isPast =
      this.props.card.status == 'past' ||
      (this.props.card.videoStream && this.props.card.videoStream.status == 'past');
    let videoSrc = '';
    if (isPast) {
      videoSrc =
        (this.props.card.recording && this.props.card.recording.mp4Location) ||
        (this.props.card.videoStream &&
          this.props.card.videoStream.recording &&
          this.props.card.videoStream.recording.mp4Location);
    }
    if (isPast && videoSrc) {
      let allowAutoplaying = this.props.allowAutoPlay;
      if (this.props.team.config['enable-video-auto-play'] && allowAutoplaying) {
        this.setState({ startPlay: true });
      }
    }
  }

  videoClickHandler = () => {
    if (this._video.paused) {
      this._video.play();
    } else {
      this._video.pause();
    }
    this.setState({ startPlay: true });
  };

  openCard() {
    if (this.props.standaloneLinkClickHandler) {
      this.props.standaloneLinkClickHandler();
    }
  }

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    const defaultImage = '/i/images/video_processing_being_processed.jpg';
    let isUpcoming =
      this.props.card.status == 'upcoming' ||
      (this.props.card.videoStream && this.props.card.videoStream.status == 'upcoming');
    let isPast =
      this.props.card.status == 'past' ||
      (this.props.card.videoStream && this.props.card.videoStream.status == 'past');
    let isLive =
      this.props.card.state === 'live' ||
      this.props.card.status == 'live' ||
      (this.props.card.videoStream && this.props.card.videoStream.status == 'live');
    let isLinkVideo =
      this.props.card.resource &&
      (this.props.card.resource.embedHtml || this.props.card.resource.videoUrl);
    let startDateTime, startDate, startTime;
    let videoSrc = '';
    let resourceUrl = '';
    let imageUrlBanner = '';
    let card = this.props.card;
    if (isPast) {
      videoSrc =
        card.videoStream &&
        ((card.videoStream.recording && card.videoStream.recording.mp4Location) ||
          card.videoStream.embedPlaybackUrl);
    }
    if (this.props.card.resource) {
      resourceUrl = this.props.card.resource.url;
      imageUrlBanner = this.props.card.resource.imageUrl;
    }

    let imageUrl =
      this.props.card.videoStream && this.props.card.videoStream.imageUrl
        ? this.props.card.videoStream.imageUrl
        : 'https://dp598loym07sk.cloudfront.net/assets/card_default_image_min-38538421e73fde77e005d645a1ee28ae.jpg';
    if (this.props.card.videoStream && this.props.card.videoStream.startTime != undefined) {
      startDateTime = new Date(this.props.card.videoStream.startTime);
      startDate = startDateTime.toDateString();
      startTime = startDateTime.toTimeString();
    }
    let svgStyle = {
      filter: 'url(#blur-effect-overview-video)'
    };
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    let allowAutoplaying = this.props.allowAutoPlay;
    let currentUserId = this.props.currentUser.get('id');
    return (
      <div className="video-block">
        {!this.props.hideTitle && (
          <div
            className={this.props.standaloneLinkClickHandler && 'openCardFromSearch'}
            onTouchTap={this.openCard.bind(this)}
          >
            {this.props.card.name && <h5>{this.props.card.name}</h5>}
            {this.props.card.title && <h5>{this.props.card.title}</h5>}
            {(this.props.card.cardType === 'video_stream' ||
              this.props.card.cardType === 'VideoStream') &&
              this.props.card.message && <p>{this.props.card.message}</p>}
          </div>
        )}
        {isPast && !videoSrc && !isLive && (
          <div>
            <img
              className="waiting-video"
              onError={e => {
                e.target.src = defaultImage;
              }}
              src="/i/images/video_processing_being_processed.jpg"
            />
          </div>
        )}

        {isUpcoming && !videoSrc && (
          <div
            className="upcoming-video"
            style={{
              background:
                'url(' +
                addSecurity(imageUrl, expireAfter, currentUserId) +
                ')  center center / contain no-repeat rgb(0, 0, 1)'
            }}
          >
            <div className="upcoming-video-container">
              <div className="upcoming-video-overlay">
                <div className="upcoming-video-content row">
                  <div className="calender-icon columns small-4 medium-4 large-4">
                    <img src="https://d2rdbjk9w0dffy.cloudfront.net/assets/scheduled-livestream-calender-icon.png" />
                  </div>
                  <div className="upcoming-video-text columns small-7 medium-7 large-7">
                    {tr('This LiveStream is scheduled for')} <br />
                    <div className="strong-text">
                      {tr(`%{date}, at %{time}`, {date: startDate, time: startTime})}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {isPast && videoSrc && (
          <div className="video-stream playVideo">
            <video
              ref={node => {
                this._video = node;
              }}
              style={this.styles.video}
              poster={
                this.props.card.imageUrl ||
                (this.props.card.videoStream && this.props.card.videoStream.imageUrl)
              }
              controls={this.state.startPlay}
              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
              autoPlay={this.props.team.config['enable-video-auto-play'] && allowAutoplaying}
            >
              <source src={addSecurity(videoSrc, expireAfter, currentUserId)} type="video/mp4" />
            </video>
            {!(
              this.state.startPlay ||
              (this.props.team.config['enable-video-auto-play'] && allowAutoplaying)
            ) && (
              <PlayIcon
                style={this.styles.playIcon}
                className="center-play-icon"
                color={colors.white}
                onTouchTap={this.videoClickHandler}
              />
            )}
          </div>
        )}

        {isLive && <LiveStream card={this.props.card} />}
        {isLinkVideo &&
          (this.props.card.resource.siteName === 'YouTube' &&
          this.props.card.resource.embedHtml === null ? (
            <a href={this.props.card.resource.url} target="_blank">
              <img
                style={{ width: '100%' }}
                src={addSecurity(this.props.card.resource.imageUrl, expireAfter, currentUserId)}
              />
            </a>
          ) : (
            <InlineVideo
              cardId={this.props.card.cardId || this.props.card.id}
              isCardV3={this.state.isCardV3}
              allowAutoplay={allowAutoplaying}
              embedHtml={
                this.props.card.resource.embedHtml ||
                `<iframe height=${this.props.heightV3 || '30rem'} width="1" src="${addSecurity(
                  this.props.card.resource.videoUrl,
                  expireAfter,
                  currentUserId
                )}" />`
              }
              videoUrl={this.props.card.resource.videoUrl}
              getAllvideoElements={this.props.getAllvideoElements}
              sourceType={this.props.sourceType}
            />
          ))}
        {!this.props.card.videoStream && !isLinkVideo && !isLive && !videoSrc && (
          <div className="card-img-container">
            <div className="card-blurred-background">
              <svg width="100%" height="100%">
                <image
                  style={svgStyle}
                  xlinkHref={addSecurity(imageUrlBanner, expireAfter, currentUserId)}
                  x="-30%"
                  y="-30%"
                  width="160%"
                  height="160%"
                />
                <filter id="blur-effect-overview-video">
                  <feGaussianBlur stdDeviation="10" />
                </filter>
              </svg>
            </div>
            <a
              title={tr('open in new tab')}
              onClick={e => e.stopPropagation()}
              className="matte-hover"
              href={resourceUrl}
              target="_blank"
            >
              <svg width="100%" height="100%" style={this.styles.mainSvg}>
                <image
                  xlinkHref={addSecurity(imageUrlBanner, expireAfter, currentUserId)}
                  width="100%"
                  style={this.styles.svgImage}
                  height="100%"
                />
              </svg>
            </a>
          </div>
        )}
      </div>
    );
  }
}
VideoStream.propTypes = {
  standaloneLinkClickHandler: PropTypes.func,
  team: PropTypes.object,
  hideTitle: PropTypes.bool,
  card: PropTypes.object,
  allowAutoPlay: PropTypes.bool,
  getAllvideoElements: PropTypes.func,
  sourceType: PropTypes.string,
  widthV3: PropTypes.string,
  heightV3: PropTypes.string,
  isCardV3: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser
  };
}

export default connect(mapStoreStateToProps)(VideoStream);
