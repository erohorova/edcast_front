import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import Divider from 'material-ui/Divider';

import colors from 'edc-web-sdk/components/colors/index';
import More from 'edc-web-sdk/components/icons/More';
import MoreV2 from 'edc-web-sdk/components/icons/MoreV2';
import VerticalMenuIcon from 'edc-web-sdk/components/icons/VerticalMenuIcon';
import {
  markAsComplete,
  markAsUncomplete,
  dismissContent,
  getMDPCard
} from 'edc-web-sdk/requests/cards.v2';
import { editContentV2 } from 'edc-web-sdk/requests/contents.v2';

import { open as openSnackBar } from '../../actions/snackBarActions';
import { markAssignmentCountToBeUpdated } from '../../actions/assignmentsActions';
import {
  getLearningQueueContent,
  getLearningContent,
  removeBookmarkFromLearningQueue
} from '../../actions/learningQueueActions';
import {
  dismissCard,
  deleteCard,
  toggleShowTopic,
  toggleBookmarkCardAsync
} from '../../actions/cardsActions';
import {
  confirmation,
  confirmationPrivateCardModal,
  openAssignModal,
  openAddToPathway,
  openAddToJourney,
  openSmartBiteCreationModal,
  openJourneyCreationModal,
  openPathwayCreationModal,
  openCardStatsModal,
  openCardActionsModal,
  openShareModal,
  openPostToChannelModal,
  openStatusModal,
  openShareContentModal,
  openMultiactionsModal,
  openReasonModal,
  openChangeAuthorModal,
  openMDPModal
} from '../../actions/modalActions';
import { removeChannelCard } from '../../actions/channelsActionsV2';

import { Permissions } from '../../utils/checkPermissions';
import completeClickHandler from '../../utils/cardMarkAsComplete';
import getCardType from '../../utils/getCardType';

class InsightDropDownActions extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showTopic: false,
      isOfficial: this.props.card.isOfficial,
      isQuizEnable: window.ldclient.variation('quiz-smart-card', false),
      enableJourneys: window.ldclient.variation('journey', false),
      isBookmarked: this.props.card.isBookmarked,
      dismissed: false,
      isAssigned: this.props.card.isAssigned,
      changeAuthor: false
    };
    this.dismissClickHandler = this.dismissClickHandler.bind(this);
    this.handleOpenAssignModal = this.handleOpenAssignModal.bind(this);
    this.addToPathwayClickHandler = this.addToPathwayClickHandler.bind(this);
    this.deleteClickHandler = this.deleteClickHandler.bind(this);
    this.showTopicsClickHandler = this.showTopicsClickHandler.bind(this);
    // this.completeClickHandler = this.completeClickHandler.bind(this);
    this.bookmarkClickHandler = this.bookmarkClickHandler.bind(this);
    this.showViewsClickHandler = this.showViewsClickHandler.bind(this);
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
    this.shareContent = window.ldclient.variation('share-content', false);
    this.assignModalV2 = window.ldclient.variation('assign-v-2', false);
    this.dynamicSelection = window.ldclient.variation('dynamic-selections', false);
    this.showMarkAsComplete = window.ldclient.variation('show-markAsComplete-on-visit', true);

    this.styles = {
      iconMenu: {
        backgroundColor: '#ffffff'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#26273b',
        minHeight: '26px',
        backgroundColor: '#ffffff'
      }
    };
  }

  hideInsight = () => {
    this.props.card.featuredHidden = true;
    this.setState({ dismissed: true });
  };

  dismissClickHandler(card) {
    document.body.style.overflow = '';
    this.props.dispatch(
      confirmation(
        'Dismiss SmartCard',
        "This SmartCard will be removed from your feed and you can't find it anymore. Do you want to remove this from your feed?",
        () => {
          this.hideInsight();
          this.props.dispatch(dismissCard(card.id, card.cardType));
        }
      )
    );
  }

  dismissKeyDownHandler = (e, card) => {
    if (e.keyCode === 13) {
      this.dismissClickHandler(card);
    }
  };

  handleOpenAssignModal(selfAssign) {
    if (this.assignModalV2 && !selfAssign) {
      this.props.dispatch(openMultiactionsModal(this.props.card, 'assign', selfAssign));
    } else {
      this.props.dispatch(openAssignModal(this.props.card, selfAssign, this.assignedStateChange));
    }
  }

  handleChangeAuthorModal() {
    this.props.dispatch(
      openChangeAuthorModal(
        this.props.card,
        this.props.removeCardFromList,
        this.changeAuthorOptionRemoval,
        this.props.removeCardFromCardContainer,
        this.props.cardSectionName
      )
    );
  }

  handleOpenAssignModalKeyDown = (e, selfAssign) => {
    if (e.keyCode === 13) {
      this.handleOpenAssignModal(selfAssign);
    }
  };

  addToPathwayClickHandler(isAddingToJourney) {
    if (this.props.card.cardType === 'pack') {
      this.props.dispatch(openAddToJourney(this.props.card.id));
      return;
    }
    let type = this.props.card.cardType === 'VideoStream' ? 'VideoStream' : 'Card';
    this.props.dispatch(
      openAddToPathway(this.props.card.id, this.props.card, type, isAddingToJourney)
    );
  }
  addToPathwaykeyDown = (e, isAddingToJourney) => {
    if (e.keyCode == 13) {
      this.addToPathwayClickHandler(isAddingToJourney);
    }
  };

  deleteClickHandler() {
    let type, snackBarType;
    if (this.props.card.cardType === 'journey') {
      type = 'journey';
      snackBarType = 'Journey';
    } else if (this.props.card.cardType === 'pack' || this.props.card.cardType === 'pack_draft') {
      type = 'pathway';
      snackBarType = 'Pathway';
    } else {
      type = 'insight';
      snackBarType = 'SmartCard Insight';
    }
    let adminMsg =
      this.props.author && this.props.author.id != this.props.currentUser.id
        ? `You are deleting ${this.props.author.name || this.props.author.fullName}'s ${type}. `
        : '';
    document.body.style.overflow = '';
    this.props.dispatch(
      confirmation(
        'Confirm',
        `${adminMsg}All associated statistics and comments will be permanently removed.`,
        () => {
          if (!this.props.removeCardFromCarousel) {
            if (this.props.channel) {
              this.props
                .dispatch(
                  removeChannelCard(this.props.channel.id, this.props.card, this.props.type)
                )
                .then(() => {
                  this.props.dispatch(
                    deleteCard(this.props.card.id, this.props.card.cardType, this.props.channel)
                  );
                })
                .catch(err => {
                  console.error(`Error in InsightDropDownActions.removeChannelCard.func : ${err}`);
                });
            } else {
              this.props
                .dispatch(
                  deleteCard(this.props.card.id, this.props.card.cardType, this.props.channel)
                )
                .then(cardId => {
                  this.props.removeCardFromList && this.props.removeCardFromList(cardId);
                  this.props.removeCardFromCardContainer &&
                    this.props.removeCardFromCardContainer(this.props.cardSectionName);
                })
                .catch(err => {
                  console.error(`Error in InsightDropDownActions.deleteCard.func : ${err}`);
                });
            }
          } else {
            this.props
              .dispatch(
                deleteCard(this.props.card.id, this.props.card.cardType, this.props.channel)
              )
              .then(cardId => {
                this.props.removeCardFromCarousel &&
                  this.props.removeCardFromCarousel(this.props.card);
              })
              .catch(err => {
                console.error(`Error in InsightDropDownActions.deleteCard.func : ${err}`);
              });
          }
          if (this.props.isStandalone) {
            this.props.dispatch(push('/'));
          } else {
            this.props.dispatch(
              openSnackBar(`${snackBarType} deleted. It will never show again.`, true)
            );
            !this.props.channel && this.hideInsight();
          }
        }
      )
    );
  }

  deleteKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.deleteClickHandler();
    }
  };

  editClickHandler = () => {
    if (this.props.card.cardType === 'pack' || this.props.card.cardType === 'pack_draft') {
      this.props.dispatch(openPathwayCreationModal(this.props.card, this.props.cardUpdated));
    } else if (this.props.card.cardType === 'journey') {
      this.props.dispatch(openJourneyCreationModal(this.props.card, this.props.cardUpdated));
    } else {
      this.props.dispatch(openSmartBiteCreationModal(this.props.card, this.props.cardUpdated));
    }
  };

  unpinClickHandler = () => {
    this.props.unpinClickHandler(this.props.card);
  };
  unpinKeyHandler = e => {
    if (e.keyCode === 13) {
      this.unpinClickHandler();
    }
  };
  //Topics are Tags. We show the word 'Tags' to user.
  showTopicsClickHandler() {
    this.setState({ showTopic: !this.state.showTopic });
    this.props.showTopicToggleClick();
  }

  showTopicskeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.showTopicsClickHandler();
    }
  };

  openChannelModal = () => {
    this.openPrivateWarning('channel');
  };
  openChannelModalKeyModal = e => {
    if (e.keyCode === 13) {
      this.openChannelModal();
    }
  };
  openPrivateWarning = postTo => {
    if (this.props.card.isPublic) {
      postTo === 'channel'
        ? this.props.dispatch(openPostToChannelModal(this.props.card))
        : this.shareContent
        ? this.props.dispatch(openMultiactionsModal(this.props.card, 'share'))
        : this.props.dispatch(openShareModal(this.props.card));
    } else {
      this.props.dispatch(
        confirmationPrivateCardModal(
          'Share a private content',
          `You are about to share a private content, this content once shared, will be accessible to all the users of the ${postTo}. Do you want to continue?`,
          true,
          () => {
            postTo === 'channel'
              ? this.props.dispatch(openPostToChannelModal(this.props.card))
              : this.shareContent
              ? this.props.dispatch(openMultiactionsModal(this.props.card, 'share'))
              : this.props.dispatch(openShareModal(this.props.card));
          }
        )
      );
    }
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise((resolve, reject) => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in InsightDropDownActions.asyncDispatch.func : ${err}`);
        });
    });
  };

  setStatePending = (state, val) => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  async bookmarkClickHandler() {
    if (this.state.pendingBookmark) {
      return;
    }
    await this.setStatePending('pendingBookmark', true);
    await this.asyncDispatch(
      toggleBookmarkCardAsync,
      this.props.card.id,
      this.props.card.cardType,
      !this.state.isBookmarked
    );

    this.props.dispatch(getLearningQueueContent());
    this.props.dispatch(
      getLearningContent({
        'state[]': ['active'],
        'source[]': ['bookmarks']
      })
    );

    await this.setStatePending('isBookmarked', !this.state.isBookmarked);
    await this.setStatePending('pendingBookmark', false);
    let cardType;
    switch (this.props.card.cardType) {
      case 'pack':
        cardType = 'Pathway';
        break;
      case 'journey':
        cardType = 'Journey';
        break;
      default:
        cardType = 'SmartCard';
        break;
    }

    await this.props.dispatch(
      openSnackBar(
        this.state.isBookmarked
          ? `Done! This ${cardType} has been Bookmarked and can now be found in your bookmarked content.`
          : `This ${cardType} has been removed from your Bookmarks.`,
        true
      )
    );
  }

  bookmarkKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.bookmarkClickHandler();
    }
  };

  promoteClickHandler = () => {
    let actionName = this.state.isOfficial ? 'unpromoted' : 'promoted';
    editContentV2(this.props.card.id, actionName.slice(0, -1))
      .then(() => {
        this.setState({ isOfficial: !this.state.isOfficial });
        this.props.dispatch(openSnackBar(`The card was successfully ${actionName}!`, true));
      })
      .catch(err => {
        console.error(`Error in InsightDropDownActions.editContentV2.func : ${err}`);
      });
  };

  promoteKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.promoteClickHandler();
    }
  };

  completeClickHandler = () => {
    if (this.props.isStandalonePage && this.props.callCompleteClickHandler) {
      this.props.callCompleteClickHandler();
    } else {
      completeClickHandler.call(
        this,
        this.props.card,
        this.props.isCompleted,
        this.props.cardUpdated,
        this.props.isPartOfPathway
      );
    }
  };

  completeKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.completeClickHandler();
    }
  };

  showViewsClickHandler = () => {
    this.props.dispatch(openCardActionsModal(this.props.card, 'view'));
  };

  showLikesClickHandler = () => {
    this.props.dispatch(openCardActionsModal(this.props.card, 'like'));
  };
  showLikesKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.showLikesClickHandler();
    }
  };

  shareClickHandler = () => {
    this.openPrivateWarning('group');
  };
  shareKeyDownHandler = e => {
    if (e.keyCode === 13) {
      this.shareClickHandler();
    }
  };

  handleCardAnalayticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.props.card));
  };
  handleCardAnalayticsModalkeyHandler = e => {
    if (e.keyCode === 13) {
      this.handleCardAnalayticsModal();
    }
  };

  openReasonReportModal = () => {
    let isAlreadyReported =
      this.props.users && this.props.users.reported_card_ids
        ? this.props.users.reported_card_ids.indexOf(this.props.card.id) >= 0
        : false;
    if (this.props.card.isReported || isAlreadyReported) {
      this.props.dispatch(openSnackBar('You have already reported this card', true));
    } else {
      this.props.dispatch(openReasonModal(this.props.card));
    }
  };
  openReasonReportModalKeyHandler = e => {
    if (e.keyCode === 13) {
      this.openReasonReportModal();
    }
  };

  openMDPModal = () => {
    getMDPCard(this.props.card.id)
      .then(isAlreadAdded => {
        if (isAlreadAdded) {
          this.props.dispatch(openSnackBar('You have already added this card to MDP', true));
        } else {
          this.props.dispatch(openMDPModal(this.props.card));
        }
      })
      .catch(err => {
        console.error(`Error in InsightDropDownActions._openMDPModal.func : ${err}`);
      });
  };

  openMDPModalKeyHandler = e => {
    if (e.keyCode === 13) {
      this.openMDPModal();
    }
  };

  isCardPublished = card => {
    if (card.state === 'published' || (card && card.id && card.id.toString().indexOf('ECL') == 0)) {
      return true;
    } else {
      return false;
    }
  };

  _openShareContentModal = () => {
    this.openPrivateWarning('group');
  };
  _openShareContentModalKeyDown = e => {
    if (e.keyCode === 13) {
      this._openShareContentModal();
    }
  };
  assignedStateChange = () => {
    this.setState({ isAssigned: true });
  };

  changeAuthorOptionRemoval = () => {
    this.setState({ changeAuthor: true });
  };

  _dismissAssignmentClickHandler = card => {
    this.props.dispatch(
      confirmation(
        'Dismiss Assignment',
        'Are you sure that you want to remove this from your assignment list?',
        () => {
          dismissContent({ content_id: card.id })
            .then(() => {
              this.setState({ isAssigned: false });
              if (window.location.pathname && window.location.pathname.indexOf('/feed') >= 0) {
                this.props.removeCardFromList && this.props.removeCardFromList(card.id);
              } else {
                this.props.removeDismissAssessmentFromList &&
                  this.props.removeDismissAssessmentFromList(card);
              }
              this.props.dispatch(
                openSnackBar(`This content has been removed from your assignment list.`, true)
              );
            })
            .catch(err => {
              console.error(
                `Error in InsightDropDownActions._dismissAssignmentClickHandler.func : ${err}`
              );
            });
        }
      )
    );
  };
  _dismissAssignmentKeyDownHandler = (e, card) => {
    if (e.keyCode === 13) {
      this._dismissAssignmentClickHandler(card);
    }
  };
  editKeyHandler = e => {
    if (e.keyCode === 13) {
      this.props.editPathway || this.editClickHandler();
    }
  };
  deleteSharedCardKeyDown = e => {
    if (e.keyCode === 13) {
      this.props.deleteSharedCard();
    }
  };
  render() {
    if (this.state.dismissed) {
      return null;
    }
    let { isPartOfPathway } = this.props;
    let isOwned = false,
      showUnPinOption = false,
      showEdit = false,
      showAddToPathway = false,
      showAddToJourney = false,
      showDismiss = false,
      showAssignToMe = false,
      showAssign = false,
      showChangeAuthor = false,
      showDelete = false,
      showTopics = true,
      showBookmark = false,
      showMarkComplete = false,
      showMarkActive = false,
      showFeatured = false,
      isAuthor = false,
      showLikes = false,
      showShare = false,
      isPathwayCardProject = this.props.isPathwayCardProject;
    if (
      this.props.author &&
      this.props.author.id + '' === this.props.currentUser.id &&
      Permissions.has('MANAGE_CARD')
    ) {
      isOwned = true;
      showDelete = true;
    }

    let isPathwayCheck =
      this.props.card.cardType === 'pack' || this.props.card.cardType === 'pack_draft';
    let isJourneyCheck = this.props.card.cardType === 'journey';
    let isPaidCheck = !this.props.card.isPaid || isJourneyCheck || isPathwayCheck;
    let userIsAuthor =
      isPaidCheck &&
      Permissions.has('CHANGE_AUTHOR') &&
      (this.props.author &&
        this.props.author.id &&
        this.props.author.id + '' === this.props.currentUser.id);
    let userIsAdmin =
      isPaidCheck &&
      Permissions.has('CHANGE_AUTHOR') &&
      this.props.author &&
      this.props.author.id &&
      this.props.currentUser.isAdmin;

    if (!this.state.changeAuthor && (userIsAuthor || userIsAdmin)) {
      showChangeAuthor = true;
    }
    if (this.props.author && this.props.author.id + '' === this.props.currentUser.id) {
      isAuthor = true;
    }
    if (this.props.hasOwnProperty('disableTopics') && !Boolean(this.props.disableTopics)) {
      showTopics = false;
    }
    if (this.props.currentUser.isAdmin && Permissions.has('MANAGE_CARD')) {
      if (
        !!this.props.card.id &&
        !(
          this.props.card &&
          this.props.card.id &&
          this.props.card.id.toString().startsWith('ECL-S-')
        )
      ) {
        showDelete = true;
        showFeatured = true;
      }
    }
    showLikes = Permissions['enabled'] !== undefined && Permissions.has('LIKE_CONTENT') && isAuthor;
    if (
      (isOwned || this.props.currentUser.isAdmin) &&
      this.props.card.cardType !== 'VideoStream' &&
      this.props.card.cardType !== 'video_stream' &&
      Permissions.has('MANAGE_CARD')
    ) {
      showEdit = true;
    }
    if (this.props.showUnPinOption) {
      showUnPinOption = true;
    }
    let pollPermission = this.props.card.cardType === 'poll' && Permissions.has('MANAGE_CARD');
    let textCardPermission =
      Permissions['enabled'] !== undefined && Permissions.has('CREATE_TEXT_CARD');
    let cardType = getCardType(this.props.card) || '';

    if (
      (pollPermission && !!this.props.card.attemptCount) ||
      (pollPermission &&
        this.props.card.isAssessment &&
        !this.state.isQuizEnable &&
        !isPartOfPathway) ||
      (cardType === 'TEXT' && !textCardPermission)
    ) {
      showEdit = false;
    }
    if (
      this.props.card.cardType !== 'pack' &&
      this.props.card.cardType !== 'pack_draft' &&
      Permissions.has('ADD_TO_PATHWAY')
    ) {
      showAddToPathway = true;
    }
    if (
      this.props.card.cardType !== 'journey' &&
      Permissions.has('CREATE_JOURNEY') &&
      this.state.enableJourneys
    ) {
      showAddToJourney = true;
    }
    if (this.props.dismissible && Permissions.has('DISMISS_CONTENT')) {
      showDismiss = true;
    }

    let shouldShowAssign =
      this.props.currentUser.isGroupLeader &&
      Permissions.has('ASSIGN_CONTENT') &&
      this.isCardPublished(this.props.card);

    if (shouldShowAssign) {
      showAssign = true;
    }
    if (!this.state.isAssigned && this.isCardPublished(this.props.card)) {
      showAssignToMe = true;
    }
    let editLink;
    if (this.props.card.cardType === 'pack' || this.props.card.cardType === 'pack_draft') {
      editLink = `/pathways/${this.props.card.slug}/edit`;
    } else {
      editLink = `/manage/insights/${this.props.card.slug}/edit`;
    }

    if (Permissions.has('BOOKMARK_CONTENT')) {
      showBookmark = true;
    }
    if (Permissions.has('SHARE') && !this.props.card.hidden) {
      showShare = true;
    }
    if (
      Permissions.has('MARK_AS_COMPLETE') &&
      this.props.card.cardType !== 'poll' &&
      this.props.card.cardType !== 'project' &&
      !isPathwayCardProject
    ) {
      if (!this.props.isCompleted) {
        showMarkComplete = true;
      } else {
        if (
          this.props.card.cardType == 'pack' ||
          (isPartOfPathway &&
            (this.props.card.isPathwayCompleted || this.props.card.completionState == 'COMPLETED'))
        ) {
          showMarkActive = this.props.isCompleted;
        } else {
          if (
            this.props.modal.card &&
            this.props.modal.card.cardType == 'pack' &&
            this.props.modal.card.completionState == 'COMPLETED'
          ) {
            showMarkActive = false;
          } else {
            showMarkActive = true;
          }
        }
      }
    }
    let hasWritableChannels =
      !!this.props.currentUser.writableChannels && !!this.props.currentUser.writableChannels.length;
    let showChannelPost = hasWritableChannels;
    if (this.props.card.hidden) {
      showAddToPathway = false;
      showAddToJourney = false;
      showDismiss = false;
      showAssignToMe = false;
      showAssign = false;
      showBookmark = false;
      showChannelPost = false;
      showFeatured = false;
      showShare = false;
    }

    if (this.props.card.cardType === 'journey') {
      showAddToPathway = false;
      showAddToJourney = false;
    }

    if (!this.props.card.isPublic && isAuthor && !this.props.card.hidden) {
      showShare = true;
    }

    // hiding Delete button from UI for NON-UGC cards - short term fix
    showDelete = this.props.author && this.props.author.id ? showDelete : false;

    // if(this.props.card.cardType && this.props.card.cardType === 'course'){
    //   showEdit = false;
    // }

    if (
      this.props.card.cardType &&
      (this.props.card.cardType === 'journey' || this.props.card.cardType === 'pack')
    ) {
      if (!this.props.card.isPublic) {
        showAddToPathway = false;
      }
      if (
        this.props.card.completionState === null &&
        this.props.reviewStatusLabel &&
        this.props.reviewStatusLabel !== 'Mark as Complete'
      ) {
        showMarkComplete = false;
      }
    }

    if (this.props.card.state === 'draft') {
      showAddToPathway = false;
      showAddToJourney = false;
      showAssignToMe = false;
      showAssign = false;
      showChannelPost = false;
      showShare = false;
    }
    if (this.props.card.state === 'draft' && isAuthor) {
      showEdit = true;
      showDelete = true;
      showDismiss = false;
      showBookmark = false;
      showMarkComplete = false;
      showMarkActive = false;
      showFeatured = false;
    }

    if (this.props.card.isClone) {
      showEdit = false;
    }

    if (
      !this.props.card.isPublic &&
      this.props.currentUser &&
      this.props.currentUser.isAdmin &&
      !Permissions.has('ADMIN_ONLY') &&
      !this.props.currentUser.isSuperAdmin &&
      !isAuthor
    ) {
      showEdit = false;
      showDelete = false;
    }

    // Admin, author and users card is restricted to will be able perform share and post to channel on private card
    let shareability =
      isAuthor ||
      (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) ||
      (this.props.card.teamsPermitted && this.props.card.teamsPermitted.length > 0) ||
      (this.props.card.usersPermitted && this.props.card.usersPermitted.length > 0);

    let sharePrivateCard = shareability && !this.props.card.hidden;
    let assignPrivateCard =
      shouldShowAssign &&
      (isAuthor || (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) &&
      !this.props.card.hidden;

    if (!this.props.card.isPublic) {
      showShare = sharePrivateCard;
      showChannelPost = sharePrivateCard && hasWritableChannels;
      showAssign = assignPrivateCard;
    }

    showShare = Permissions.has('SHARE') && showShare;
    let showActions =
      showEdit || showAddToPathway || showDismiss || showAssign || showDelete || showUnPinOption;

    let showReportOption =
      (!(['pack', 'journey'].indexOf(this.props.card.cardType) >= 0) ||
        !!~window.location.pathname.indexOf('/featured')) &&
      this.props.team &&
      this.props.team.config &&
      this.props.team.config.reporting_content;

    let showMDP =
      (!(['pack', 'journey'].indexOf(this.props.card.cardType) >= 0) &&
        (this.props.team && this.props.team.config && this.props.team.config.addToMDP)) ||
      false;
    let parentType = this.props.type;
    let showMarkAsComplete =
      (parentType !== 'BigCard' &&
        parentType !== 'Tile' &&
        parentType !== 'List' &&
        parentType !== 'SlideOut' &&
        parentType !== 'featuredCard') ||
      this.showMarkAsComplete;

    let showDivider =
      (showAddToPathway ||
        showAddToJourney ||
        showShare ||
        showChannelPost ||
        showTopics ||
        (showLikes && !!this.props.card.votesCount) ||
        showAssignToMe ||
        showAssign ||
        showChangeAuthor ||
        showFeatured ||
        (showMarkAsComplete &&
          !this.props.markAsCompleteDisabledForLink &&
          !this.props.card.markFeatureDisabledForSource &&
          showMarkComplete) ||
        (!this.props.card.markFeatureDisabledForSource &&
          this.isUncompleteEnabled &&
          showMarkActive) ||
        showBookmark ||
        showMDP) &&
      ((Permissions.has('DISMISS_ASSIGNMENT') && this.state.isAssigned) ||
        showEdit ||
        showDismiss ||
        showDelete ||
        this.props.deleteSharedCard ||
        this.props.showUnPinOption ||
        showReportOption ||
        ((isOwned || Permissions.has('VIEW_CARD_ANALYTICS')) &&
          !!this.props.card.id &&
          !this.props.card.id.toString().startsWith('ECL-') &&
          this.props.card.cardType !== 'QUIZ' &&
          !this.props.providerCards));

    const MenuIcon = this.props.isCardV3 ? (
      this.props.showVerticalMenu ? (
        <VerticalMenuIcon color={this.props.verticalMenuColor} />
      ) : (
        <MoreV2 viewBox={this.props.isCardV3Pathway ? '0 0 4 25' : '-4 1 4 20'} />
      )
    ) : (
      <More color={colors.gray} />
    );
    return (
      <IconMenu
        className="insight-dropdown"
        iconButtonElement={
          <IconButton aria-label="view options" disabled={this.props.card.deletedAt}>
            {MenuIcon}
          </IconButton>
        }
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        listStyle={this.styles.iconMenu}
        menuStyle={this.styles.menuItem}
        iconStyle={this.props.iconStyle}
        animated={false}
        useLayerForClickAway={parentType !== 'SlideOut'}
      >
        {showAddToPathway && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Add to Pathway')}
            className="addToPathway iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.addToPathwayClickHandler.bind(this, false)}
            onKeyDown={e => this.addToPathwaykeyDown(e, false)}
          />
        )}
        {showAddToJourney && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Add to Journey')}
            className="addToPathway iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.addToPathwayClickHandler.bind(this, true)}
            onKeyDown={e => this.addToPathwaykeyDown(e, true)}
          />
        )}
        {showShare && !this.shareContent && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Share to Groups')}
            onClick={this.shareClickHandler}
            onKeyDown={this.shareKeyDownHandler}
            className="iconmenu-style-ontab show-outline-iconmenu"
          />
        )}
        {showChannelPost && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Post to Channel')}
            className="channel iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.openChannelModal}
            onKeyDown={this.openChannelModalKeyModal}
          />
        )}
        {showTopics && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={!this.state.showTopic ? tr('Show Tags') : tr('Hide Tags')}
            className="toggleTopics iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.showTopicsClickHandler}
            onKeyDown={this.showTopicskeyDownHandler}
          />
        )}
        {showLikes && !!this.props.card.votesCount && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={`${tr('Show Likes')} (${this.props.card.votesCount})`}
            className="toggleTopics iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.showLikesClickHandler}
            onKeyDown={this.showLikesKeyDownHandler}
          />
        )}

        {showAssignToMe && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Assign to Me')}
            className="assign iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.handleOpenAssignModal.bind(this, true)}
            onKeyDown={e => this.handleOpenAssignModalKeyDown(e, true)}
          />
        )}
        {showAssign && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Assign')}
            className="assign iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.handleOpenAssignModal.bind(this, false)}
            onKeyDown={e => this.handleOpenAssignModalKeyDown(e, false)}
          />
        )}
        {showChangeAuthor && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Change Author')}
            className="assign iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.handleChangeAuthorModal.bind(this, true)}
          />
        )}
        {showFeatured && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={!this.state.isOfficial ? tr('Promote') : tr('Unpromote')}
            className="promote iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.promoteClickHandler}
            onKeyDown={this.promoteKeyDownHandler}
          />
        )}
        {showMarkAsComplete &&
          !this.props.markAsCompleteDisabledForLink &&
          !this.props.card.markFeatureDisabledForSource &&
          showMarkComplete && (
            <MenuItem
              style={this.styles.menuItem}
              primaryText={tr('Mark as Complete')}
              className="complete iconmenu-style-ontab show-outline-iconmenu"
              onClick={this.completeClickHandler}
              onKeyDown={this.completeKeyDownHandler}
            />
          )}
        {!this.props.card.markFeatureDisabledForSource &&
          this.isUncompleteEnabled &&
          showMarkActive && (
            <MenuItem
              style={this.styles.menuItem}
              primaryText={tr('Mark as Incomplete')}
              className="complete iconmenu-style-ontab show-outline-iconmenu"
              onClick={this.completeClickHandler}
              onKeyDown={this.completeKeyDownHandler}
            />
          )}
        {showBookmark && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={this.state.isBookmarked ? tr('Unbookmark') : tr('Bookmark')}
            className="bookmark iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.bookmarkClickHandler}
            onKeyDown={this.bookmarkKeyDownHandler}
          />
        )}
        {showShare && this.shareContent && (
          <MenuItem
            style={this.styles.menuItem}
            onClick={() => {
              this._openShareContentModal();
            }}
            onKeyDown={this._openShareContentModalKeyDown}
            className="share iconmenu-style-ontab show-outline-iconmenu"
            primaryText={tr('Share')}
          />
        )}
        {showMDP && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Add to MDP')}
            className="unpin iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.openMDPModal.bind(this)}
            onKeyDown={this.openMDPModalKeyHandler}
          />
        )}
        {showDivider && (
          <Divider
            style={{
              backgroundColor: '#d6d6e1',
              marginRight: '15px',
              marginLeft: '15px'
            }}
          />
        )}
        {Permissions.has('DISMISS_ASSIGNMENT') && this.state.isAssigned && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Dismiss from Assignments')}
            onClick={() => {
              this._dismissAssignmentClickHandler(this.props.card);
            }}
            onKeyDown={this._dismissAssignmentKeyDownHandler}
            className="iconmenu-style-ontab show-outline-iconmenu"
          />
        )}
        {showEdit && (
          <MenuItem
            style={this.styles.menuItem}
            onClick={this.props.editPathway || this.editClickHandler}
            onKeyDown={this.editKeyHandler}
            className="edit iconmenu-style-ontab show-outline-iconmenu"
            primaryText={tr('Edit')}
          />
        )}
        {showDismiss && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Dismiss')}
            className="dismiss iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.dismissClickHandler.bind(this, this.props.card)}
            onKeyDown={e => this.dismissKeyDownHandler(e, this.props.card)}
          />
        )}
        {showDelete && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Delete')}
            className="delete iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.deleteClickHandler}
            onKeyDown={this.deleteKeyDownHandler}
          />
        )}
        {this.props.deleteSharedCard && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Remove from Group')}
            className="delete iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.props.deleteSharedCard}
            onKeyDown={this.deleteSharedCardKeyDown}
          />
        )}
        {this.props.showUnPinOption && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Unpin')}
            className="unpin iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.unpinClickHandler}
            onKeyDown={this.unpinKeyHandler}
          />
        )}
        {showReportOption && (
          <MenuItem
            style={this.styles.menuItem}
            primaryText={tr('Report it')}
            className="unpin iconmenu-style-ontab show-outline-iconmenu"
            onClick={this.openReasonReportModal.bind(this)}
            onKeyDown={this.openReasonReportModalKeyHandler}
          />
        )}
        {(isOwned || Permissions.has('VIEW_CARD_ANALYTICS')) &&
          !!this.props.card.id &&
          !this.props.card.id.toString().startsWith('ECL-') &&
          this.props.card.cardType !== 'QUIZ' &&
          !this.props.providerCards && (
            <MenuItem
              style={this.styles.menuItem}
              primaryText={tr('Card Statistics')}
              className="cardStats iconmenu-style-ontab show-outline-iconmenu"
              onClick={this.handleCardAnalayticsModal}
              onKeyDown={this.handleCardAnalayticsModalkeyHandler}
            />
          )}
      </IconMenu>
    );
  }
}

InsightDropDownActions.defaultProps = {
  showUnPinOption: false,
  isPartOfPathway: false
};

InsightDropDownActions.propTypes = {
  isPartOfPathway: PropTypes.bool,
  card: PropTypes.object,
  iconStyle: PropTypes.object,
  channel: PropTypes.object,
  style: PropTypes.object,
  currentUser: PropTypes.object,
  dismissible: PropTypes.bool,
  author: PropTypes.object,
  isStandalone: PropTypes.bool,
  isCompleted: PropTypes.bool,
  disableTopics: PropTypes.bool,
  isOverviewModal: PropTypes.bool,
  editPathway: PropTypes.func,
  providerCards: PropTypes.bool,
  isPathwayCardProject: PropTypes.bool,
  deleteSharedCard: PropTypes.func,
  removeCardFromList: PropTypes.func,
  cardUpdated: PropTypes.func,
  unpinClickHandler: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  markAsComplete: PropTypes.func,
  hideComplete: PropTypes.func,
  type: PropTypes.string,
  pathname: PropTypes.string,
  cardSectionName: PropTypes.string,
  showUnPinOption: PropTypes.bool,
  isCardV3: PropTypes.bool,
  isCardV3Pathway: PropTypes.bool,
  toggleHandleLeapModal: PropTypes.func,
  isShowLeap: PropTypes.any,
  callCompleteClickHandler: PropTypes.func,
  modal: PropTypes.object,
  users: PropTypes.object,
  team: PropTypes.object,
  removeCardFromCardContainer: PropTypes.func,
  reviewStatusLabel: PropTypes.string,
  completeStatus: PropTypes.string,
  removeCardFromCarousel: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func,
  showVerticalMenu: PropTypes.bool,
  verticalMenuColor: PropTypes.string,
  markAsCompleteDisabledForLink: PropTypes.bool,
  isStandalonePage: PropTypes.bool
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  pathname: state.routing.locationBeforeTransitions.pathname,
  modal: state.modal.toJS(),
  users: state.users.toJS(),
  team: state.team.toJS()
}))(InsightDropDownActions);
