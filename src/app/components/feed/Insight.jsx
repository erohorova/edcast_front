import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton/IconButton';
import colors from 'edc-web-sdk/components/colors/index';
import VideoStream from './VideoStream';
import InsightDropDownActions from './InsightDropDownActions';
import CommentList from './CommentList';
import LiveCommentList from './LiveCommentList';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import CompletedAssignment from 'edc-web-sdk/components/icons/CompletedAssignmentGrey';
import ChatBubbleOutlineAsset from 'edc-web-sdk/components/icons/Comment';
import BookmarkAsset from 'edc-web-sdk/components/icons/Bookmark';
import InfoIcon from 'edc-web-sdk/components/icons/InfoIcon';
import FeaturedIcon from 'material-ui/svg-icons/toggle/star';
const SelectedIconStar = FeaturedIcon;
import {
  loadComments,
  toggleLikeCardAsync,
  toggleBookmarkCardAsync,
  setRateCardAsync
} from '../../actions/cardsActions';
import {
  openAddToPathway,
  openStatusModal,
  openAssignModal,
  openCardStatsModal
} from '../../actions/modalActions';
import Resource from './Resource';
import PathwayCover from './PathwayCover';
import Poll from './Poll';
import { push } from 'react-router-redux';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { markAsComplete, markAsUncomplete } from 'edc-web-sdk/requests/cards.v2';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import MarkdownRenderer from '../common/MarkdownRenderer';
import CreationDate from '../common/CreationDate';
import Popover from 'material-ui/Popover';
import IconStar from 'material-ui/svg-icons/toggle/star-border';
import Rating from 'react-rating';
import find from 'lodash/find';
import map from 'lodash/map';
import includes from 'lodash/includes';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import linkPrefix from '../../utils/linkPrefix';
import Download from 'edc-web-sdk/components/icons/Download';
import * as logoType from '../../constants/logoTypes';
import checkResources from '../../utils/checkResources';
import BlurImage from '../common/BlurImage';
import * as upshotActions from '../../actions/upshotActions';
import pdfPreviewUrl from '../../utils/previewPdf';

const logoObj = logoType.LOGO;

class Insight extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showComment: false,
      comments: [],
      ratingPercent: {},
      providerLogos: {},
      pendingLike: false,
      showRating: false,
      statusRating: 'Rate this content',
      pendingRate: false,
      pendingBookmark: false,
      ratingOpen: false,
      fullRatingOpen: false,
      truncateTitle: !props.isStandalone
        ? props.card.title && props.card.title.length > 260
        : false,
      truncateMessage: !props.isStandalone
        ? props.card.message && props.card.message.length > 260
        : false,
      channelTooltip: false,
      dismissed: false,
      commentsCount: this.props.card.commentsCount,
      card: props.card,
      averageRating: props.card.averageRating,
      showTopic: false,
      isCompleted:
        props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED',
      isLiveStream: false,
      liveCommentsCount: 0,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };
    this.styles = {
      menuItem: {
        fontSize: '14px'
      },
      cardHeader: {
        paddingRight: '56px',
        paddingBottom: 0
      },
      labelStyle: {
        border: '1px #ccc solid',
        boxShadow: 'none',
        borderRadius: 0,
        height: '30px',
        lineHeight: '30px',
        display: 'inline-block',
        padding: '0px 12px',
        fontWeight: '500'
      },
      btnIcons: {
        verticalAlign: 'middle',
        width: '24px',
        height: '24px',
        padding: 0
      },
      tooltipActiveBts: {
        marginTop: -32
      },
      commentsCount: {
        verticalAlign: 'middle',
        display: 'inline-block'
      },
      audio: {
        width: '100%'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.5rem',
        width: '2.5rem',
        marginRight: '1rem',
        position: 'relative'
      },
      anchorColor: {
        color: '#555555',
        display: 'inline-block'
      }
    };
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
    this.commentButtonClickHandler = this.commentButtonClickHandler.bind(this);
    this.cardLikeHandler = this.cardLikeHandler.bind(this);
    this.bookmarkClickHandler = this.bookmarkClickHandler.bind(this);
    this.handleOpenAssignModal = this.handleOpenAssignModal.bind(this);
    this.handleCardAnalayticsModal = this.handleCardAnalayticsModal.bind(this);
    this.addToPathwayClickHandler = this.addToPathwayClickHandler.bind(this);
    this.completeClickHandler = this.completeClickHandler.bind(this);
    this.cardRatingHandler = this.cardRatingHandler.bind(this);
    this.handleShowRating = this.handleShowRating.bind(this);
    this.handleFullShowRating = this.handleFullShowRating.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.clickRatingHandler = this.clickRatingHandler.bind(this);
    this.truncateMessageText = this.truncateMessageText.bind(this);
  }

  componentWillMount() {
    if (this.props.showComment) {
      this.loadCommentsHandle();
    }
  }

  cardUpdated(card) {
    if (this.props.card) {
      if (this.props.pathname.indexOf('/channel/') !== -1) {
        let routeParam = this.props.pathname.split('/');
        let slug = routeParam[routeParam.length - 1];
        let findChannel = find(this.props.channels, el => {
          return el.slug == slug;
        });
        if (findChannel) {
          let existChannel = find(card.channel_ids, el => {
            return el == findChannel.id;
          });
          if (!existChannel) {
            this.hideInsight();
          }
        }
      }
      fetchCard(this.props.card.id)
        .then(data => {
          this.setState({
            card: data
          });
        })
        .catch(err => {
          console.error(`Error in Insight.fetchCard.func : ${err}`);
        });
    }
  }

  async cardRatingHandler(newRating) {
    if (newRating) {
      this.setRatingStatus(newRating);
    } else {
      this.setRatingStatus(this.props.card.userRating);
    }
  }
  async clickRatingHandler(newRating) {
    if (this.state.pendingRate) {
      return;
    }
    this.props.card.userRating = newRating;
    await this.setStatePending('pendingRate', true);
    await this.asyncDispatch(
      setRateCardAsync,
      this.props.card.cardId || this.props.card.id,
      this.props.card.cardType,
      newRating
    )
      .then(result => {
        this.allCountRating(result);
        this.setState({ averageRating: result.averageRating });
      })
      .catch(err => {
        console.error(`Error in Insight.setRateCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingRate', false);
  }
  async clickRatingHandler(newRating) {
    if (this.state.pendingRate) {
      return;
    }
    this.props.card.userRating = newRating;
    await this.setStatePending('pendingRate', true);
    await this.asyncDispatch(
      setRateCardAsync,
      this.props.card.cardId || this.props.card.id,
      this.props.card.cardType,
      newRating
    )
      .then(result => {
        this.allCountRating(result);
        this.setState({ averageRating: result.averageRating });
      })
      .catch(err => {
        console.error(`Error in Insight.setRateCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingRate', false);
  }

  setRatingStatus(rating) {
    if (rating >= 4) {
      this.setState({ statusRating: 'Very Good' });
    } else if (rating >= 2 && rating < 4) {
      this.setState({ statusRating: 'Good' });
    } else if (rating && rating < 2) {
      this.setState({ statusRating: 'Bad' });
    } else {
      this.setState({ statusRating: 'Rate this content' });
    }
  }

  allCountRating(card) {
    let ratingCount = 0;
    let ratingCountText = '';
    let ratingPercent = {};
    for (let i = 1; i < 6; i++) {
      ratingCount += card.allRatings[i] ? card.allRatings[i] : 0;
    }
    for (let i = 1; i < 6; i++) {
      ratingPercent[i] = card.allRatings[i]
        ? ((card.allRatings[i] / ratingCount) * 100).toFixed(0) + '%'
        : '0%';
    }
    ratingCount == 1
      ? (ratingCountText = ratingCount + ' Rating')
      : (ratingCountText = ratingCount + ' Ratings');
    this.setState({
      ratingCount: ratingCount,
      ratingCountText: ratingCountText,
      ratingPercent: ratingPercent
    });
  }

  handleShowRating = event => {
    event.preventDefault();
    this.setState({
      ratingOpen: true,
      anchorEl: event.currentTarget
    });
  };

  handleFullShowRating = event => {
    event.preventDefault();
    this.setState({
      fullRatingOpen: !this.state.fullRatingOpen,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState({
      ratingOpen: false,
      fullRatingOpen: false
    });
  };

  componentDidMount() {
    let cardType;
    switch (this.props.card.cardType) {
      case 'VideoStream' || 'video_stream':
        cardType = 'video_stream';
        break;
      case 'pack':
        cardType = 'collection';
        break;
      default:
        cardType = 'card';
    }

    if (this.props.card.cardType == 'video_stream') {
      let isLiveStream = false;
      if (this.props.card && this.props.card.videoStream !== undefined) {
        isLiveStream = this.props.card.videoStream.status == 'live';
      } else if (this.props.card && this.props.card.status) {
        isLiveStream = this.props.card.status == 'live';
      }
      this.setState({ isLiveStream: isLiveStream });
    }

    if (this.props.card.allRatings) {
      this.allCountRating(this.props.card);
    }

    if (this.props.card.userRating) {
      this.setRatingStatus(this.props.card.userRating);
    }

    function checkVisible(elm) {
      let rect = elm.getBoundingClientRect();
      let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
      return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
    }

    this._metricRecorder = () => {
      if (checkVisible(this._insight)) {
        window.removeEventListener('scroll', this._metricRecorder);
      }
    };
    window.addEventListener('scroll', this._metricRecorder);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this._metricRecorder);
  }

  loadCommentsHandle = () => {
    let cardId = this.props.card.cardId || this.props.card.id;
    let isECL = /^ECL-/.test(cardId);
    if (!isECL && this.props.card.commentsCount) {
      loadComments(
        cardId,
        this.props.card.commentsCount,
        this.props.card.cardType,
        this.props.card.cardId ? this.props.card.id : null
      )
        .then(data => {
          this.setState({ comments: data, showComment: true });
        })
        .catch(err => {
          console.error(`Error in Insight.loadComments.func : ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true });
    }
  };

  commentButtonClickHandler() {
    if (!this.state.showComment) {
      this.loadCommentsHandle();
    } else {
      this.setState({ showComment: false });
    }
  }

  handleCardAnalayticsModal() {
    this.props.dispatch(openCardStatsModal(this.props.card));
  }
  setStatePending = (state, val) => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise((resolve, reject) => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in Insight.asyncDispatch.func : ${err}`);
        });
    });
  };

  async cardLikeHandler() {
    if (this.state.pendingLike) {
      return;
    }
    await this.setStatePending('pendingLike', true);
    await this.asyncDispatch(
      toggleLikeCardAsync,
      this.props.card.cardId || this.props.card.id,
      this.props.card.cardType,
      !this.props.card.isUpvoted
    )
      .then(() => {
        this.props.card.isUpvoted = !this.props.card.isUpvoted;
        if (this.state.upshotEnabled) {
          var name;
          if (this.props.card.resource && this.props.card.resource.title) {
            name = this.props.card.resource.title;
          } else {
            name = this.props.card.message;
          }
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
            name: name,
            category: this.props.card.cardType,
            type: this.props.card.cardSubtype,
            description: this.props.card.resource.description,
            status: !!this.props.card.completionState
              ? this.props.card.completionState
              : 'Incomplete',
            rating: this.props.card.averageRating,
            like: this.props.card.isUpvoted ? 'Yes' : 'No',
            event: this.props.card.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
          });
        }
        this.props.card.votesCount = Math.max(
          this.props.card.votesCount + (this.props.card.isUpvoted ? 1 : -1),
          0
        );
      })
      .catch(err => {
        console.error(`Error in Insight.toggleLikeCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingLike', false);
  }

  async bookmarkClickHandler() {
    let bookmarkSnackMessage = 'Done! This SmartCard has been moved to your My Learning Plan.';
    let unbookmarkSnackMessage = 'Removed from your My Learning Plan';
    let snackMessage = this.props.card.isBookmarked ? unbookmarkSnackMessage : bookmarkSnackMessage;
    if (this.state.pendingBookmark) {
      return;
    }
    await this.setStatePending('pendingBookmark', true);
    await this.asyncDispatch(
      toggleBookmarkCardAsync,
      this.props.card.id,
      this.props.card.cardType,
      !this.props.card.isBookmarked
    )
      .then(() => {
        this.props.card.isBookmarked = !this.props.card.isBookmarked;
      })
      .catch(err => {
        console.error(`Error in Insight.toggleBookmarkCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingBookmark', false);
    await this.props.dispatch(openSnackBar(snackMessage, true));
  }

  hideInsight = () => {
    this.props.card.featuredHidden = true;
    this.setState({ dismissed: true });
  };

  handleOpenAssignModal() {
    this.props.dispatch(openAssignModal(this.props.card));
  }

  addToPathwayClickHandler() {
    let type = this.props.card.cardType === 'VideoStream' ? 'VideoStream' : 'Card';
    this.props.dispatch(openAddToPathway(this.props.card.id, this.props.card, type));
  }

  completeClickHandler() {
    let cardType;
    switch (this.props.card.cardType) {
      case 'VideoStream':
        cardType = 'video_stream';
        break;
      default:
        cardType = 'card';
        break;
    }
    let isPack = this.props.card.cardType === 'pack' || this.props.card.cardType === 'journey';
    if (
      (!isPack && !this.state.isCompleted) ||
      (isPack &&
        this.props.card.state === 'published' &&
        +this.props.card.completedPercentage === 98)
    ) {
      markAsComplete(this.props.card.id, { state: 'complete' })
        .then(() => {
          let snackMessage, snackLink;
          if (['/required', '/'].indexOf(this.props.pathname) > -1) {
            snackMessage = 'Great job! This SmartCard has been moved to your';
            snackLink = (
              <a
                className="meLearningQueue"
                onTouchTap={() => {
                  this.props.dispatch(push('/me/learning'));
                }}
              >
                My Learning Plan.
              </a>
            );
          } else {
            snackMessage = '';
            snackLink = [
              'You have completed this task. You can view it under',
              '/me/content/completed',
              'Me',
              'Tab'
            ];
          }
          // getting if this is a standlone insight page & redirecting to the previous link.
          this.props.card.featuredHidden = true;
          this.setState({ isCompleted: true }, () => {
            if (this.state.newModalAndToast) {
              this.props.dispatch(
                openSnackBar(
                  snackMessage + 'You have completed this task. You can view it under Me tab.'
                )
              );
            } else {
              this.props.dispatch(openStatusModal(snackMessage, null, snackLink));
            }
          });
          if (this.props.hideComplete !== undefined) {
            this.props.hideComplete(this.props.card.id);
          }
          if (this.state.upshotEnabled) {
            var name;
            if (this.props.card.resource && this.props.card.resource.title) {
              name = this.props.card.resource.title;
            } else {
              name = this.props.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.props.card.cardType,
              type: this.props.card.cardSubtype,
              description: this.props.card.resource.description,
              status: 'COMPLETED',
              rating: this.props.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: 'Mark Completed'
            });
          }
        })
        .catch(err => {
          console.error(`Error in Insight.markAsComplete.func : ${err}`);
        });
    } else if (
      (!isPack && this.state.isCompleted) ||
      (isPack && +this.props.card.completedPercentage === 100)
    ) {
      markAsUncomplete(this.props.card.id)
        .then(() => {
          this.setState({ isCompleted: false }, () => {
            setTimeout(() => {
              if (this.state.newModalAndToast) {
                this.props.dispatch(openSnackBar('You have marked this task as incomplete'));
              } else {
                this.props.dispatch(openStatusModal('You have marked this task as incomplete'));
              }
            }, 1500);
          });
          if (this.state.upshotEnabled) {
            var name;
            if (this.props.card.resource && this.props.card.resource.title) {
              name = this.props.card.resource.title;
            } else {
              name = this.props.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.props.card.cardType,
              type: this.props.card.cardSubtype,
              description: this.props.card.resource.description,
              status: !!this.props.card.completionState
                ? this.props.card.completionState
                : 'Incomplete',
              rating: this.props.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: 'Mark Incomplete'
            });
          }
        })
        .catch(err => {
          console.error(`Error in Insight.markAsUncomplete.func : ${err}`);
        });
    } else {
      let msg =
        this.props.card.state === 'published'
          ? `Please complete all pending cards before marking the ${
              this.state.card.cardType === 'pack' ? 'Pathway' : 'Journey'
            } as complete.`
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  }

  standaloneLinkClickHandler = () => {
    let linkPrefixValue = linkPrefix(this.props.card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${this.props.card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${this.props.card.id}`
    ) {
      return;
    }
    // If coming from modal search, remove overflow.
    // document.getElementsByTagName('body')[0].style.overflow = '';
    this.props.dispatch(push(`/${linkPrefixValue}/${this.props.card.slug}`));
    if (this.props.toggleSearch) {
      this.props.toggleSearch();
    }
  };

  viewMoreTitleHandler = () => {
    this.setState({ truncateTitle: false });
  };

  viewMoreMessageHandler = () => {
    this.setState({ truncateMessage: false });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count });
  };

  showTopicToggleClick = () => {
    this.setState({ showTopic: !this.state.showTopic });
  };

  channelMessage = (channel, index, card) => {
    var message = '';
    if (card.channels.length <= 2 || (card.channels.length > 2 && index == 0)) {
      message = card.channels[index].label;
    }
    message = message + (index == 0 && card.channels.length == 2 ? ' and ' : '');
    return message;
  };

  checkHighlight = tag => {
    let interestList = this.props.userInterest.map(item => item.topic_label);
    return includes(interestList, tag);
  };

  truncateMessageText(message) {
    return message.substr(0, 260);
  }

  downloadBlock(file) {
    return (
      <span
        className="roll"
        onMouseEnter={() => {
          this.setState({ [file.handle]: true });
        }}
        style={{ display: this.state[file.handle] ? 'inherit' : 'none' }}
      >
        <a href={file.url} download={file.handle} target="_blank">
          <IconButton
            aria-label={tr('Download')}
            style={{ width: '100%', height: '100%', padding: 0 }}
            tooltipPosition="bottom-center"
            tooltipStyles={{ left: '-50%' }}
            tooltip={tr('Download')}
          >
            <Download />
          </IconButton>
        </a>
      </span>
    );
  }

  cardViewProperty() {
    let cardType = this.props.card.cardType;
    let cardSubtype = this.props.card.cardSubtype;

    if (
      ['video_stream', 'pack', 'pack_draft', 'journey'].indexOf(cardType) > -1 ||
      (cardType == 'media' && ['link', 'video'].indexOf(cardSubtype) > -1)
    ) {
      return this.props.isStandalone ? 'full' : 'summary';
    } else {
      return 'full';
    }
  }

  liveCommentsCount = commentCount => {
    this.setState({ liveCommentsCount: commentCount });
  };

  getCardStatus() {
    if (this.props.card && this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.status;
    } else if (this.props.card && this.props.card.status) {
      return this.props.card.status;
    } else {
      return '';
    }
  }

  getCardUuid() {
    if (this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.uuid;
    } else if (this.props.card.uuid !== undefined) {
      return this.props.card.uuid;
    } else {
      return '';
    }
  }

  checkComplete = (card, isAnswer) => {
    if (isAnswer) {
      markAsComplete(card.id, { state: 'complete' });
      this.setState({ isCompleted: true });
    }
  };
  openStandalone = e => {
    e.preventDefault();
    this.standaloneLinkClickHandler();
  };

  render() {
    if (this.state.dismissed) {
      return null;
    }
    let isCompleted = this.state.isCompleted;
    let card = checkResources(this.state.card || this.props.card);
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let dateObj;
    let isOwner = this.props.author && this.props.author.id === this.props.currentUser.id;
    let disableTopics = card.tags && card.tags.length > 0;

    if (card.card_type) {
      // Reformat to fit correct insight requirement
      //TODO: rationale, assignment, commentsPending, isUpvoted
      card.timestampWithZone = card.updated_at;
      card.isOffical = card.is_official;
      card.cardType = card.card_type;
      card.commentsCount = card.comments_count;
      card.publishedAt = card.published_at;
      //TODO: feed_v1 is not having config with card, need to fix
      // card.isShareable = card.config.shareable;
    }

    // card resource fix
    if (typeof card.resource === 'undefined') {
      if (card.video) {
        card.resource = {
          description: card.video.description,
          embedHtml: card.video.embedHtml,
          site: card.video.site,
          title: card.video.title,
          slug: `/${linkPrefix(card.cardType)}/${card.slug}`
        };
      }
    }

    // Fix for SQL -> Markdown issues
    let title = card.title ? card.title.replace(/\\\*/g, '*') : '';
    let message = card.message;

    let viewMore = this.state.truncateTitle ? (
      <span>
        ...
        <a className="viewMore" onTouchTap={this.viewMoreTitleHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let viewMoreMessage = this.state.truncateMessage ? (
      <span>
        ...
        <a className="viewMore" target="_blank" onTouchTap={this.viewMoreMessageHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    // clicking for standalone page or URL
    let moreThanTwoChannels = card.channels && card.channels.length > 2;
    let htmlRender = !!card.resource && (!!card.resource.title || !!card.resource.description);
    let commentsCountForAbbreviate = this.state.isLiveStream
      ? this.state.liveCommentsCount
      : this.state.commentsCount;

    if (card.filestack && !!card.filestack.length && card.filestack[0].handle === message) {
      message = '';
    }
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    return (
      <div className="insight" ref={node => (this._insight = node)}>
        <CardHeader
          title={
            <div className="channels-block">
              {this.props.author && (
                <div className="author-float">
                  <a
                    className="matte user"
                    onTouchTap={() => {
                      this.props.dispatch(push('/' + this.props.author.handle));
                    }}
                  >
                    <strong>
                      {`${this.props.author.firstName ? this.props.author.firstName : ''} ${
                        this.props.author.lastName ? this.props.author.lastName : ''
                      }`}
                    </strong>
                  </a>
                  <br />
                  {this.state.cardClickHandle === 'modal' && (
                    <CreationDate
                      card={card}
                      standaloneLinkClickHandler={this.standaloneLinkClickHandler}
                    />
                  )}
                </div>
              )}

              {(card.channels && card.channels.length) > 0 && (
                <div
                  style={
                    !this.props.author ? { borderLeft: 'none', margin: '0', paddingLeft: '0' } : {}
                  }
                  className={
                    this.props.pathname.indexOf('/me/') == -1
                      ? 'header-secondary-text matte insight-channel-post author-float channels-limit'
                      : 'header-secondary-text matte insight-channel-post author-float channels-limit full-channels'
                  }
                >
                  <span>{` Posted in${'\u00A0'}`}</span>
                  {card.channels.slice(0, 2).map((channel, index) => {
                    return (
                      <a
                        className="matte channel-label channel"
                        title={channel.label}
                        key={index}
                        onTouchTap={() => {
                          this.props.dispatch(push('/channel/' + channel.id));
                        }}
                      >
                        {this.channelMessage(channel, index, card)}
                      </a>
                    );
                  })}
                  {moreThanTwoChannels && (
                    <a
                      className="matte insight-other-channels"
                      title={card.channels
                        .slice(1)
                        .map(item => {
                          return item.label;
                        })
                        .join(', ')}
                      style={!this.props.author ? { margin: '0' } : {}}
                      onMouseEnter={() => this.setState({ channelTooltip: true })}
                      onMouseLeave={() => this.setState({ channelTooltip: false })}
                    >
                      {tr(`and %{count} other ${card.channels.length > 2 ? 'channels' : 'channel'}`, {count: card.channels.length - 1})}
                    </a>
                  )}
                </div>
              )}
            </div>
          }
          subtitle={
            card.publishedAt && (
              <div className="header-secondary-text">
                <span className="matte" />
              </div>
            )
          }
          avatar={
            card.author && (
              <BlurImage
                style={this.styles.avatarBox}
                id={card.id}
                image={
                  card.author.picture ||
                  (card.author.avatarimages && card.author.avatarimages.small) ||
                  defaultUserImage
                }
              />
            )
          }
          style={this.styles.cardHeader}
        >
          <div className="card-header-icons horizontal-spacing-xlarge">
            {card.isOffical && (
              <span>
                <IconButton
                  tooltip={tr('Featured')}
                  aria-label={tr('Featured')}
                  disableTouchRipple
                  tooltipPosition="top-center"
                >
                  <FeaturedIcon color={colors.arylide} />
                </IconButton>
              </span>
            )}
            {card.rationale && (
              <span>
                <IconButton
                  tooltip={card.rationale}
                  aria-label={card.rationale}
                  disableTouchRipple
                  tooltipPosition="top-center"
                >
                  <InfoIcon />
                </IconButton>
              </span>
            )}
            {!this.props.isActionsDisabled && (
              <InsightDropDownActions
                card={card}
                author={this.props.author}
                hideInsight={this.hideInsight}
                dismissible={this.props.dismissible}
                disableTopics={disableTopics}
                isStandalone={this.props.isStandalone}
                showTopicToggleClick={this.showTopicToggleClick}
                cardUpdated={this.cardUpdated.bind(this)}
                removeCardFromList={this.props.removeCardFromList}
              />
            )}
          </div>
        </CardHeader>
        {card.assignment && card.assignment.dueAt && (
          <small style={{ paddingLeft: '16px' }} className="due-date">
            Due Date:{' '}
            <strong>{new Date(card.assignment.dueAt).toLocaleString().split(',')[0]}</strong>
          </small>
        )}
        <div className={this.state.showTopic ? 'topic-block' : 'without-topics-block'}>
          <div className={this.state.showTopic ? 'insight-topics' : 'without-topics'}>
            {this.props.userInterest && (
              <div className="topics-list">
                {(card.tags.length &&
                  card.tags.map((tag, index) => {
                    return (
                      <span
                        style={this.checkHighlight(tag.name) ? { color: colors.primary } : {}}
                        key={tag.id}
                      >
                        {tag.name}
                        {index != card.tags.length - 1 && <span>, </span>}
                      </span>
                    );
                  })) ||
                  tr('Sorry, no tags!')}
              </div>
            )}
            {!this.props.userInterest && (
              <div className="topics-list">
                {map(card.tags, 'name').join(', ') || tr('Sorry, no tags!')}
              </div>
            )}
          </div>
        </div>
        <div className="container-padding vertical-spacing-medium insight-container">
          <div>
            {(card.cardType === 'media' || card.cardType === 'poll') && (
              <div className="vertical-spacing-large">
                <a
                  href="#"
                  style={this.styles.anchorColor}
                  className="openCardFromSearch"
                  onClick={this.openStandalone}
                >
                  <div style={{ marginBottom: '5px' }} />
                  {message && (
                    <div style={{ marginBottom: '10px' }}>
                      {(htmlRender && (
                        <div
                          dangerouslySetInnerHTML={{
                            __html: this.state.truncateMessage
                              ? this.truncateMessageText(message)
                              : message
                          }}
                        />
                      )) || (
                        <MarkdownRenderer
                          markdown={
                            this.state.truncateMessage ? this.truncateMessageText(message) : message
                          }
                        />
                      )}
                      {viewMoreMessage}
                    </div>
                  )}
                </a>
                {card.filestack && !!card.filestack.length && (
                  <div>
                    {card.filestack[0].mimetype &&
                      card.filestack[0].mimetype.indexOf('video/') > -1 && (
                        <div key={card.filestack[0].handle} className="fp fp_video">
                          <span>
                            <video
                              onMouseEnter={() => {
                                this.setState({ [card.filestack[0].handle]: true });
                              }}
                              onMouseLeave={() => {
                                this.setState({ [card.filestack[0].handle]: false });
                              }}
                              controls
                              src={card.filestack[0].url}
                              poster={card.filestack && card.filestack[1] && card.filestack[1].url}
                              preload={card.filestack && card.filestack[1] ? 'none' : 'auto'}
                              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                            />
                            {!isDownloadContentDisabled && this.downloadBlock(card.filestack[0])}
                          </span>
                        </div>
                      )}
                    {card.filestack[0].mimetype &&
                      card.filestack[0].mimetype.indexOf('video/') === -1 &&
                      card.filestack.map(file => {
                        if (file.mimetype && ~file.mimetype.indexOf('image/')) {
                          return (
                            <div key={file.handle} className="fp fp_img">
                              <span>
                                <img
                                  onMouseEnter={() => {
                                    this.setState({ [file.handle]: true });
                                  }}
                                  onMouseLeave={() => {
                                    this.setState({ [file.handle]: false });
                                  }}
                                  src={file.url}
                                />
                                {!isDownloadContentDisabled && this.downloadBlock(file)}
                              </span>
                            </div>
                          );
                        } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                          return (
                            <div key={file.handle} className="fp fp_video">
                              <span>
                                <video
                                  onMouseEnter={() => {
                                    this.setState({ [file.handle]: true });
                                  }}
                                  onMouseLeave={() => {
                                    this.setState({ [file.handle]: false });
                                  }}
                                  controls
                                  src={file.url}
                                  controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                                />
                                {!isDownloadContentDisabled && this.downloadBlock(file)}
                              </span>
                            </div>
                          );
                        } else {
                          return file.mimetype && ~file.mimetype.indexOf('audio/') ? (
                            <audio controls src={file.url} style={this.styles.audio} />
                          ) : (
                            <div key={file.handle} className="fp fp_preview">
                              <span>
                                <iframe
                                  height="480px"
                                  width="100%"
                                  src={pdfPreviewUrl(
                                    file.url,
                                    expireAfter,
                                    this.props.currentUser.id
                                  )}
                                  onMouseEnter={() => {
                                    this.setState({ [file.handle]: true });
                                  }}
                                  onMouseLeave={() => {
                                    this.setState({ [file.handle]: false });
                                  }}
                                />
                                {!isDownloadContentDisabled && this.downloadBlock(file)}
                              </span>
                            </div>
                          );
                        }
                      })}
                  </div>
                )}
                {card.resource && (card.filestack && !card.filestack.length) && (
                  <div>
                    <Resource
                      resource={card.resource}
                      cardId={card.id}
                      providerLogos={this.props.providerLogos}
                      currentUser={this.props.currentUser}
                    />
                  </div>
                )}
              </div>
            )}
            {card.cardType === 'course' && (
              <div className="vertical-spacing-large">
                <div>{card.message && <MarkdownRenderer markdown={card.message} />}</div>
                {card.resource && (
                  <div>
                    <Resource
                      resource={card.resource}
                      cardId={card.id}
                      providerLogos={this.props.providerLogos}
                      currentUser={this.props.currentUser}
                    />
                  </div>
                )}
              </div>
            )}
            {(card.cardType === 'pack' ||
              card.cardType === 'pack_draft' ||
              card.cardType === 'journey') && (
              <PathwayCover card={card} currentUser={this.props.currentUser} />
            )}
            {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
              <VideoStream
                card={card}
                standaloneLinkClickHandler={this.standaloneLinkClickHandler}
              />
            )}
            {card.ecl && card.ecl.origin && this.props.providerLogos[card.ecl.origin] != undefined && (
              <span>
                <img className="origin-logos" src={this.props.providerLogos[card.ecl.origin]} />
              </span>
            )}
          </div>
          {card.cardType === 'poll' && (
            <Poll
              cardOverview={true}
              card={card}
              cardUpdated={this.cardUpdated.bind(this)}
              checkComplete={this.checkComplete}
              currentUserId={this.props.currentUser.id}
            />
          )}

          <div className="card-info">
            <div className="row">
              <div className="medium-6 large-6 columns align-self-middle">
                {(card.eclSourceLogoUrl ||
                  (card.eclSourceTypeName && logoObj[card.eclSourceTypeName] !== undefined)) && (
                  <span className="insight-img-logo">
                    <img
                      src={card.eclSourceLogoUrl || logoObj[card.eclSourceTypeName.toLowerCase()]}
                    />
                  </span>
                )}
                {card.eclSourceTypeName && logoObj[card.eclSourceTypeName] === undefined && (
                  <div className="card-type">{card.eclSourceDisplayName}</div>
                )}
              </div>
              <div className="medium-6 large-6 columns text-right align-self-middle">
                <span className="card-type">
                  {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
                    <div style={this.styles.labelStyle}>{tr('Video')}</div>
                  )}
                  {card.cardType === 'media' && card.cardSubtype === 'link' && (
                    <div style={this.styles.labelStyle}>{tr('Article')}</div>
                  )}
                  {card.cardType === 'media' && card.cardSubtype === 'video' && (
                    <div style={this.styles.labelStyle}>{tr('Video')}</div>
                  )}
                  {card.cardType === 'media' && card.cardSubtype === 'Image' && (
                    <div style={this.styles.labelStyle}>{tr('Image')}</div>
                  )}
                  {card.cardType === 'course' && (
                    <div style={this.styles.labelStyle}>{tr('COURSE')}</div>
                  )}
                </span>

                {card.viewCount !== undefined && (
                  <span className="card-views">{card.viewCount}</span>
                )}

                {card.cardDuration !== undefined && (
                  <span className="duration">{card.cardDuration}</span>
                )}
              </div>
            </div>
          </div>
          {card.eclDurationMetadata &&
            !!card.eclDurationMetadata.calculated_duration &&
            card.eclDurationMetadata.calculated_duration_display &&
            this.props.card.eclDurationMetadata.calculated_duration > 0 && (
              <div className="read-time-ago">
                {tr('Duration')}: {card.eclDurationMetadata.calculated_duration_display}
              </div>
            )}
          {!this.props.isActionsDisabled && (
            <div className="clearfix assignment-info">
              {card.assignment &&
                Object.keys(card.assignment).length > 0 &&
                card.assignment.groups != undefined &&
                card.assignment.groups.length > 0 && (
                  <div className="assignment-details" style={{ display: 'inline-block' }}>
                    {card.assignment.teams[0].name != null && (
                      <strong>
                        {tr('GROUP')} -
                        {card.assignment.groups[0].name +
                          (card.assignment.teams.length > 1
                            ? ` ${tr('and')} ${card.assignment.teams.length - 1} ${tr('others')}`
                            : '')}{' '}
                        {card.assignment.due_at
                          ? `, ${tr('DUE')}: ${new Date(
                              card.assignment.due_at
                            ).toLocaleDateString()}`
                          : ''}
                      </strong>
                    )}

                    <div>{`${tr('Assigned By')} ${card.assignment.groups[0].assignor.first_name +
                      ' ' +
                      card.assignment.groups[0].assignor.last_name}`}</div>
                  </div>
                )}
            </div>
          )}
          {!this.props.isActionsDisabled && (
            <div className="insight-action-bar">
              <div className="float-left horizontal-spacing-xlarge">
                {!this.state.showRating && Permissions.has('LIKE_CONTENT') && (
                  <div className="like-container">
                    <IconButton
                      tooltip={!this.state.isUpvoted ? tr('Like') : tr('Liked')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.btnIcons}
                      className="like"
                      onTouchTap={this.cardLikeHandler}
                    >
                      {!this.state.card.isUpvoted && <LikeIcon />}
                      {this.state.card.isUpvoted && <LikeIconSelected />}
                    </IconButton>
                    <small
                      style={{
                        verticalAlign: 'middle',
                        display: 'inline-block',
                        marginTop: '11px'
                      }}
                    >
                      {card.votesCount ? abbreviateNumber(card.votesCount) : ''}
                    </small>
                  </div>
                )}
                {this.state.showRating && (
                  <span>
                    <IconButton
                      tooltip={tr('Rating')}
                      aria-label={tr('Rating')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.btnIcons}
                      className="rating"
                      onTouchTap={this.handleShowRating}
                    >
                      {(!this.state.ratingOpen && (
                        <IconStar
                          color={
                            this.state.averageRating || card.userRating
                              ? colors.primary
                              : colors.darkGray
                          }
                        />
                      )) || <SelectedIconStar color={colors.primary} />}
                    </IconButton>
                    {this.state.averageRating && (
                      <small>
                        <strong>{Math.ceil(parseFloat(this.state.averageRating) * 2) / 2}</strong>
                        <span onMouseMove={this.handleFullShowRating}>
                          {' '}
                          ({tr(this.state.ratingCountText)})
                        </span>
                      </small>
                    )}
                  </span>
                )}
                {Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT') && (
                  <span>
                    <IconButton
                      tooltip={tr('Comment')}
                      aria-label={tr('Comment')}
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.btnIcons}
                      className="comment"
                      onTouchTap={this.commentButtonClickHandler}
                    >
                      <ChatBubbleOutlineAsset
                        color={this.state.showComment ? colors.primary : colors.darkGray}
                      />
                    </IconButton>
                    {!this.state.isLiveStream && (
                      <small
                        className="comments-count"
                        style={{
                          verticalAlign: 'middle',
                          display: 'inline-block',
                          marginTop: '11px'
                        }}
                      >
                        {this.state.commentsCount ? abbreviateNumber(this.state.commentsCount) : ''}
                      </small>
                    )}
                    {this.state.isLiveStream && (
                      <small
                        className="comments-count"
                        style={{
                          verticalAlign: 'middle',
                          display: 'inline-block',
                          marginTop: '11px'
                        }}
                      >
                        {this.state.liveCommentsCount
                          ? abbreviateNumber(this.state.liveCommentsCount)
                          : ''}
                      </small>
                    )}
                  </span>
                )}
                {this.props.author && (isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                  <span>
                    <IconButton
                      tooltip={tr('Card Statistics')}
                      className="statistics"
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.btnIcons}
                      onTouchTap={this.handleCardAnalayticsModal}
                    >
                      <CardAnalyticsV2 />
                    </IconButton>
                  </span>
                )}
              </div>
              <div className="float-right horizontal-spacing-xlarge">
                {!this.props.isMarkAsCompleteDisabled && Permissions.has('MARK_AS_COMPLETE') && (
                  <span>
                    <IconButton
                      tooltip={
                        !card.markFeatureDisabledForSource &&
                        tr(
                          isCompleted
                            ? this.isUncompleteEnabled
                              ? 'Mark as Incomplete'
                              : 'Completed'
                            : 'Mark as Complete'
                        )
                      }
                      aria-label={
                        !card.markFeatureDisabledForSource &&
                        tr(
                          isCompleted
                            ? this.isUncompleteEnabled
                              ? 'Mark as Incomplete'
                              : 'Completed'
                            : 'Mark as Complete'
                        )
                      }
                      tooltipPosition="bottom-center"
                      tooltipStyles={this.styles.tooltipActiveBts}
                      style={this.styles.btnIcons}
                      className="completed"
                      onTouchTap={
                        !card.markFeatureDisabledForSource &&
                        (this.isUncompleteEnabled || !isCompleted)
                          ? this.completeClickHandler
                          : () => {
                              return null;
                            }
                      }
                    >
                      <CompletedAssignment color={isCompleted ? colors.primary : colors.darkGray} />
                    </IconButton>
                  </span>
                )}
                {card.cardType !== 'VideoStream' &&
                  Permissions['enabled'] !== undefined &&
                  Permissions.has('BOOKMARK_CONTENT') && (
                    <span>
                      <IconButton
                        tooltip={tr(card.isBookmarked ? 'Remove bookmark' : 'Bookmark')}
                        aria-label={tr(card.isBookmarked ? 'Remove bookmark' : 'Bookmark')}
                        tooltipPosition="bottom-center"
                        tooltipStyles={this.styles.tooltipActiveBts}
                        style={this.styles.btnIcons}
                        className="bookmark"
                        onTouchTap={this.bookmarkClickHandler}
                      >
                        <BookmarkAsset
                          color={card.isBookmarked ? colors.primary : colors.darkGray}
                        />
                      </IconButton>
                    </span>
                  )}
              </div>
            </div>
          )}
          {this.state.showComment && !this.state.isLiveStream && (
            <CommentList
              cardId={card.cardId ? card.cardId + '' : card.id}
              cardOwner={this.props.author ? this.props.author.id : ''}
              cardType={card.cardType}
              comments={this.state.comments}
              pending={card.commentPending}
              numOfComments={this.state.commentsCount}
              videoId={card.cardId ? card.id : null}
              updateCommentCount={this.updateCommentCount}
            />
          )}
          {this.state.showComment && this.state.isLiveStream && (
            <LiveCommentList
              cardId={card.cardId ? card.cardId + '' : card.id}
              cardOwner={this.props.author ? this.props.author.id : ''}
              cardType={card.cardType}
              pending={card.commentPending}
              videoId={card.cardId ? card.id : null}
              updateCommentCount={this.updateCommentCount}
              cardStatus={this.getCardStatus()}
              cardUuid={this.getCardUuid()}
              liveCommentsCount={this.liveCommentsCount}
            />
          )}
        </div>

        <Popover
          className="rating-popover"
          style={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
          open={this.state.ratingOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
          targetOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          onRequestClose={this.handleRequestClose}
        >
          <div className="rating-stars">
            <div className="rating-text">{tr(this.state.statusRating)}</div>
            <Rating
              initialRate={card.userRating || 0}
              empty={<IconStar color={'white'} />}
              full={<SelectedIconStar viewBox="1 1 22 22" color={colors.primary} />}
              onRate={this.cardRatingHandler}
              onClick={this.clickRatingHandler}
            />
          </div>
        </Popover>
        <Popover
          className="full-rating-popover"
          open={this.state.fullRatingOpen}
          style={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
          targetOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          onRequestClose={this.handleRequestClose}
        >
          <div className="rating-stars" onMouseLeave={this.handleRequestClose}>
            <div className="rating-text">
              {' '}
              <strong> {tr(this.state.ratingCountText)}</strong>
            </div>
            <div>
              <div className="row align-stars">
                <div className="col-10">
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                </div>
                <div className="col-2 rating-text">{this.state.ratingPercent[5]}</div>
              </div>
              <div className="row align-stars">
                <div className="col-10">
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                </div>
                <div className="col-2 rating-text">{this.state.ratingPercent[4]}</div>
              </div>
              <div className="row align-stars">
                <div className="col-10">
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                </div>
                <div className="col-2 rating-text">{this.state.ratingPercent[3]}</div>
              </div>
              <div className="row align-stars">
                <div className="col-10">
                  <SelectedIconStar color={colors.primary} />
                  <SelectedIconStar color={colors.primary} />
                </div>
                <div className="col-2 rating-text">{this.state.ratingPercent[2]}</div>
              </div>
              <div className="row align-stars">
                <div className="col-10">
                  <SelectedIconStar color={colors.primary} />
                </div>
                <div className="col-2 rating-text">{this.state.ratingPercent[1]}</div>
              </div>
            </div>
          </div>
        </Popover>
      </div>
    );
  }
}

Insight.defaultProps = {
  isMarkAsCompleteDisabled: false,
  providerLogos: {},
  toggleSearch: null,
  isStandalone: false
};

Insight.propTypes = {
  card: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  userInterest: PropTypes.object,
  author: PropTypes.object,
  channels: PropTypes.object,
  team: PropTypes.object,
  showComment: PropTypes.bool,
  dismissible: PropTypes.bool,
  isActionsDisabled: PropTypes.bool,
  isMarkAsCompleteDisabled: PropTypes.bool,
  providerLogos: PropTypes.object,
  pathname: PropTypes.string,
  toggleSearch: PropTypes.func,
  hideComplete: PropTypes.func,
  removeCardFromList: PropTypes.func,
  isStandalone: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS(),
    channels: state.channels.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(Insight);
