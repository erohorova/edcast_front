import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BlurImage from '../common/BlurImage';
import ReactDOM from 'react-dom';
//UI components
import Popover from 'react-simple-popover';
import ListItem from 'material-ui/List/ListItem';
//actions
import { postComment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';

import { getMentionSuggest } from 'edc-web-sdk/requests/users';

class CommentInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
      inputValue: '',
      mentionQuery: '',
      sendingComment: false,
      userTagging: window.ldclient.variation('user-tagging', true),
      userList: []
    };
    this.styles = {
      selectContainer: {
        width: '100%',
        zIndex: '10',
        maxHeight: '95px',
        padding: 0,
        overflow: 'auto',
        boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.3)',
        background: 'white'
      },
      selectContent: {
        width: '100%',
        padding: 0
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '0.2rem',
        left: '1rem'
      }
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.inputBlurHandler = this.inputBlurHandler.bind(this);
    this.submitInputHandler = this.submitInputHandler.bind(this);
  }

  componentDidMount() {
    if (this.props.autoFocus) {
      this.refs.input.focus();
    }
  }

  autoSize = () => {
    const id = this.props.isPinned
      ? `commentField-pin-${this.props.cardId}`
      : `commentField-${this.props.cardId}`;
    let el = document.getElementById(id);
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
  };

  inputChangeHandler(event) {
    let string = event.target.value;
    let parts = string.split(' ');
    this.autoSize();
    if (parts.length && parts[parts.length - 1].charAt(0) === '@') {
      let mentionQuery = parts[parts.length - 1].slice(1).toLowerCase();
      this.setState({ open: false }, () => {
        this.callgetMentionSuggest(string, mentionQuery);
      });
    } else if (parts.length > 1 && parts[parts.length - 2].charAt(0) === '@') {
      let mentionQuery =
        parts[parts.length - 2].slice(1).toLowerCase() +
        ' ' +
        parts[parts.length - 1].toLowerCase();
      this.setState({ open: false }, () => {
        this.callgetMentionSuggest(string, mentionQuery);
      });
    } else {
      this.setState({ inputValue: string, open: false, mentionQuery: '' });
    }
  }
  callgetMentionSuggest = (inputValue, mentionQuery) => {
    let { userTagging } = this.state;
    if (userTagging) {
      if (this.fetchUserTimeout) {
        clearTimeout(this.fetchUserTimeout);
      }
      this.fetchUserTimeout = setTimeout(() => {
        getMentionSuggest(mentionQuery)
          .then(usersParam => {
            this.setState({ userList: usersParam, open: true });
          })
          .catch(err => {
            console.error(`error in getting users : ${err}`);
          });
      }, 300);
    }

    this.setState({
      inputValue,
      anchorEl: ReactDOM.findDOMNode(this.refs.input),
      mentionQuery
    });
  };
  inputBlurHandler(event) {
    this.setState({ open: false });
  }

  mentionClickHandler(e, value) {
    e && e.preventDefault();
    let tempHandle = this.state.inputValue;
    let tempHandleArr = this.state.inputValue.split(' ');
    if (tempHandleArr[tempHandleArr.length - 1].charAt(0) == '@') {
      tempHandle = tempHandleArr.pop(tempHandleArr[tempHandleArr.length - 1]);
      tempHandle = tempHandleArr.join(' ');
    } else if (tempHandleArr[tempHandleArr.length - 2].charAt(0) == '@') {
      tempHandleArr.splice(tempHandleArr.length - 2, tempHandleArr.length);
      tempHandle = tempHandleArr.join(' ');
    }
    tempHandle
      ? this.setState({ inputValue: tempHandle + ' ' + value + ' ' })
      : this.setState({ inputValue: value + ' ' });
    setTimeout(() => {
      this.refs.input.focus();
    }, 20);
  }

  mentionKeyHandler = (e, handle) => {
    if (e.keyCode == 13) {
      this.mentionClickHandler('', handle);
    }
  };

  submitInputHandler(e) {
    if (this.state.inputValue.trim() !== '') {
      if (e.keyCode === 13) {
        this.setState({ sendingComment: true }, () => {
          postComment(
            this.props.cardId,
            this.state.inputValue,
            this.props.numOfComments + 1,
            this.props.cardType,
            this.props.currentUser.get('id')
          )
            .then(data => {
              if (data && data.embargoErr) {
                this.setState({ sendingComment: false });
                this.props.dispatch(
                  openSnackBar(
                    'Sorry, the content you are trying to post is from unapproved website or words.',
                    true
                  )
                );
              } else {
                this.setState({
                  inputValue: '',
                  sendingComment: false
                });
                this.props.commentPostCallback(data);
                const id = this.props.isPinned
                  ? `commentField-pin-${this.props.cardId}`
                  : `commentField-${this.props.cardId}`;
                let el = document.getElementById(id);
                el.style.cssText = 'height: 42px';
              }
            })
            .catch(err => {
              console.error(`Error in CommentInput.v2.postComment.func : ${err}`);
            });
        });
      }
    }
  }

  handleClose(e) {
    this.setState({ open: false });
  }

  render() {
    let usersArr = [];
    if (this.state.userList && this.state.userList.length > 0) {
      let usersMap = this.state.userList;
      usersArr = usersMap.filter(user => {
        let userName = user.fullName || user.name;
        let userHandle = user.handle;
        return (
          (userName && userName.toLowerCase().indexOf(this.state.mentionQuery) !== -1) ||
          (userHandle && userHandle.toLowerCase().indexOf(this.state.mentionQuery) !== -1)
        );
      });
      if (!!usersArr && usersArr.length === 0) {
        usersArr = usersMap.filter(user => {
          return !!(user.fullName || user.name) || !!user.handle;
        });
      }
    }

    return (
      <div className="comment-input-modal">
        <textarea
          rows="1"
          id={
            this.props.isPinned
              ? `commentField-pin-${this.props.cardId}`
              : `commentField-${this.props.cardId}`
          }
          className="comment create"
          ref="input"
          placeholder={
            this.props.isCardV3
              ? tr('Leave a comment here...')
              : tr('Leave a comment / Tag peers...')
          }
          onChange={this.inputChangeHandler}
          value={this.state.inputValue}
          onKeyDown={this.submitInputHandler}
          autoFocus={this.props.autoFocus}
          disabled={this.state.sendingComment}
        />

        <Popover
          style={this.styles.selectContent}
          containerStyle={this.styles.selectContainer}
          placement={this.props.isSlideout ? 'top' : 'bottom'}
          container={this}
          target={this.refs.input}
          show={!!usersArr && usersArr.length === 0 ? false : this.state.open}
          onHide={this.handleClose.bind(this)}
        >
          <div className="cardTypePopupIsActive">
            {!!usersArr &&
              usersArr.length !== 0 &&
              usersArr.map((user, index) => {
                let userNameOrHandle = user.name == null ? user.handle.replace('@', '') : user.name;
                return (
                  <ListItem
                    key={`comment_user_V2_${index}`}
                    leftIcon={
                      <BlurImage
                        commentInput={true}
                        style={
                          this.props.slideOutAvatarBoxStyle == null
                            ? this.styles.avatarBox
                            : this.props.slideOutAvatarBoxStyle
                        }
                        id={user.id}
                        image={user.avatarimages.small}
                      />
                    }
                    primaryText={<span>{userNameOrHandle}</span>}
                    onMouseDown={e => this.mentionClickHandler(e, user.handle)}
                    onKeyDown={e => this.mentionKeyHandler(e, user.handle)}
                  />
                );
              })}
          </div>
        </Popover>
      </div>
    );
  }
}

CommentInput.propTypes = {
  cardId: PropTypes.string.isRequired,
  cardType: PropTypes.string,
  users: PropTypes.object,
  currentUser: PropTypes.object,
  comment: PropTypes.object,
  autoFocus: PropTypes.bool,
  isPinned: PropTypes.bool,
  commentPostCallback: PropTypes.func,
  numOfComments: PropTypes.number,
  slideOutAvatarBoxStyle: PropTypes.object,
  isSlideout: PropTypes.bool,
  isCardV3: PropTypes.bool
};

export default connect(state => ({ users: state.users, currentUser: state.currentUser }))(
  CommentInput
);
