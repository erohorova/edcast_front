import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import colors from 'edc-web-sdk/components/colors/index';
import {
  fetchSumissionList,
  fetchProjectFilterList,
  reviewSubmission
} from 'edc-web-sdk/requests/submission';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Paper from 'edc-web-sdk/components/Paper';
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import ListView from '../../common/ListView';

import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import { open as openSnackBar } from '../../../actions/snackBarActions';
import { confirmation } from '../../../actions/modalActions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import addSecurity from '../../../utils/filestackSecurity';

class Submission extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentProjects: [],
      pending: true,
      isLastPage: false,
      pendingProjectList: false,
      limit: 10,
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      pendingRequestApproval: [],
      pendingRequestReject: false
    };
    this.styles = {
      statusMessage: {
        color: 'green',
        fontWeight: 'bold',
        fontSize: '9px',
        padding: '1.3rem'
      }
    };
    this.reviewClickHandler = this.reviewClickHandler.bind(this);
    this.isDownloadContentDisabled =
      props.team &&
      props.team.OrgConfig &&
      props.team.OrgConfig.content &&
      props.team.OrgConfig.content['web/content/disableDownload'] &&
      props.team.OrgConfig.content['web/content/disableDownload'].value;
  }

  componentDidMount() {
    calculateTeemFeedPosition();
    fetchProjectFilterList()
      .then(data => {
        if (data.projects.length) {
          this.setState(
            { currentProjects: data.projects, pendingProjectList: true },
            calculateTeemFeedPosition
          );
          this.filterChange(null, null, data.projects[0].projectId);
        } else {
          this.setState({ pending: false, isLastPage: false }, calculateTeemFeedPosition);
        }
      })
      .catch(err => {
        console.error(`Error in Submission.fetchProjectFilterList.func : ${err}`);
      });
    this.expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
  }

  filterChange = (e, index, value) => {
    let payload = {
      project_id: value,
      limit: this.state.limit,
      filter: 'reviewer'
    };
    fetchSumissionList(payload)
      .then(subCards => {
        if (subCards) {
          let submissions = subCards.projectSubmissions;
          this.setState(
            {
              projectCards: submissions,
              currentProject: value,
              pending: false
            },
            () => {
              calculateTeemFeedPosition();
              this.setState({
                isLastPage: subCards.projectSubmissionsTotalCount > this.state.projectCards.length
              });
            }
          );
        } else {
          this.setState(
            {
              pending: false,
              projectCards: null,
              currentProject: value,
              isLastPage: false
            },
            calculateTeemFeedPosition
          );
        }
      })
      .catch(err => {
        console.error(`Error in Submission.fetchSumissionList.func : ${err}`);
      });
  };

  videoSourcePreview = item => {
    return (
      <div>
        <span>
          <div>
            <video
              preload="auto"
              className="preview-upload-video"
              controls
              src={addSecurity(item.url, this.expireAfter, this.props.currentUser.id)}
              controlsList={this.isDownloadContentDisabled ? 'nodownload' : ''}
            />
            <div />
          </div>
        </span>
      </div>
    );
  };

  viewMoreClickHandler = () => {
    fetchSumissionList({
      project_id: this.state.currentProject,
      filter: 'reviewer',
      offset: this.state.projectCards.length,
      limit: this.state.limit
    })
      .then(subCards => {
        let cards = subCards.projectSubmissions;
        this.setState(
          {
            projectCards: this.state.projectCards.concat(cards),
            pending: false
          },
          () => {
            calculateTeemFeedPosition();
            this.setState({
              isLastPage: subCards.projectSubmissionsTotalCount < this.state.projectCards.length
            });
          }
        );
      })
      .catch(err => {
        console.error(`Error in Submission.viewMoreClickHandler.fetchSumissionList.func : ${err}`);
      });
  };

  reviewClickHandler = (id, reviewStatus) => {
    let pendingRequestApproval = [];
    pendingRequestApproval = this.state.pendingRequestApproval;
    if (reviewStatus == 'approved') {
      pendingRequestApproval[id] = true;
      this.setState({ pendingRequestApproval });
      this.callReviewSubmission(id, reviewStatus);
    } else {
      this.props.dispatch(
        confirmation('Confirm', 'Do you want to reject this submission ?', () => {
          this.callReviewSubmission(id, reviewStatus);
        })
      );
    }
  };

  callReviewSubmission = (id, reviewStatus) => {
    reviewSubmission({ description: '', status: reviewStatus }, id)
      .then(data => {
        if (reviewStatus == 'approved') {
          this.props.dispatch(openSnackBar('Submission approved', true));
          this.filterChange(null, null, this.state.currentProject);
        } else {
          this.props.dispatch(openSnackBar('Submission rejected', true));
          this.filterChange(null, null, this.state.currentProject);
        }
      })
      .catch(err => {
        console.error(`Error in Submission.reviewSubmission.func : ${err}`);
      });
  };

  render() {
    let card = this.state.projectCards;
    return (
      <div>
        {this.state.pendingProjectList && (
          <div className="curate-channel-filter container-padding">
            <div className="horizontal-spacing-large">
              <span className="select-bar-title">{tr('Select project')}</span>
              <SelectField
                style={{ verticalAlign: 'middle' }}
                iconStyle={{
                  width: '1.87rem',
                  height: '1.87rem',
                  top: '0.437rem',
                  fill: colors.silverSand
                }}
                maxHeight={200}
                onChange={this.filterChange}
                value={this.state.currentProject}
              >
                {this.state.currentProjects.map(project => {
                  return (
                    <MenuItem
                      value={project.projectId}
                      primaryText={project.cardTitle}
                      key={project.projectId}
                    />
                  );
                })}
              </SelectField>
            </div>
          </div>
        )}
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          emptyMessage={
            <div className="data-not-available-msg">
              {card && card.length > 0
                ? ''
                : tr(`Enjoy your day and check back later! There are no submissions to display.`)}
            </div>
          }
          isLastPage={!this.state.isLastPage}
          onViewMore={this.viewMoreClickHandler}
        >
          {!this.state.pending &&
            this.state.projectCards &&
            this.state.projectCards.map(cardItem => {
              return (
                <Paper key={cardItem.id}>
                  <div className="container-padding vertical-spacing-medium insight-container">
                    {cardItem.user.firstName}
                  </div>
                  {cardItem.filestack &&
                    cardItem.filestack[0] &&
                    cardItem.filestack[0].mimetype &&
                    cardItem.filestack[0].mimetype.indexOf('video/') > -1 && (
                      <div className="text-center">
                        <div className="row">
                          <div className="small-12 relative">
                            {this.videoSourcePreview(cardItem.filestack[0])}
                          </div>
                          <div className="small-12 file-title">
                            <span className="item-name">
                              {cardItem.filestack[0].filename || cardItem.filestack[0].handle}
                            </span>
                          </div>
                        </div>
                      </div>
                    )}
                  <div className="container-padding vertical-spacing-medium insight-container">
                    {cardItem.description}
                  </div>
                  <Divider />
                  {!cardItem.projectSubmissionReviews.length && (
                    <div className="container-padding clearfix">
                      <PrimaryButton
                        className="float-left"
                        label={tr('Approve')}
                        pending={this.state.pendingRequestApproval[cardItem.id]}
                        pendingLabel={tr('Approving...')}
                        onTouchTap={() => {
                          this.reviewClickHandler(cardItem.id, 'approved');
                        }}
                      />
                      <SecondaryButton
                        className="float-left"
                        label={tr('Reject')}
                        pending={this.state.pendingRequestReject}
                        pendingLabel={tr('Rejecting...')}
                        onTouchTap={() => {
                          this.reviewClickHandler(cardItem.id, 'rejected');
                        }}
                      />
                    </div>
                  )}
                </Paper>
              );
            })}
          {!this.state.projectCards && (
            <div className="container-padding vertical-spacing-medium">
              <div className="text-center vertical-spacing-large">
                <div className="empty-message">{tr('No submissions available')}</div>
              </div>
            </div>
          )}
        </ListView>
      </div>
    );
  }
}
Submission.propTypes = {
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    curate: state.curate.toJS(),
    cards: state.cards.toJS(),
    users: state.users.toJS(),
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(Submission);
