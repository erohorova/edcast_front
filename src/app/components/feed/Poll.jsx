import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { cards } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import _ from 'lodash';

class Poll extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      attemptPending: false,
      attemptCount: this.props.card.attemptCount,
      quizQuestionOptions:
        typeof this.props.card.options !== 'undefined'
          ? this.props.card.options || []
          : this.props.card.quizQuestionOptions || [],
      hasAttempted: this.props.card.hasAttempted,
      isAssessment: this.props.card.isAssessment,
      result: null,
      correctAnswer: null,
      voteCurrent: {},
      reanswerMode: false,
      errorMessage: '',
      label: tr(props.card.isAssessment ? 'Answer' : 'Vote'),
      isCardV3: window.ldclient.variation('card-v3', false),
      viewResults: false
    };

    this.voteClickHandler = this.voteClickHandler.bind(this);
    this.styles = {
      radioLabel: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '0.8125rem',
        lineHeight: 1.47,
        textAlign: 'left',
        color: '#6f708b',
        width: '100%'
      },
      pollStars: {
        marginBottom: '0.8125rem',
        minHeight: '1.25rem',
        padding: '0 0.5rem 0 0.6875rem',
        border: '1px #d6d6e1 solid',
        borderRadius: '0'
      },
      radioLabelV3: {
        fontSize: '0.875rem',
        fontFamily: 'Open Sans, sans-serif',
        textAlign: 'left',
        color: '#6f708b',
        lineHeight: 1.47,
        width: '100%',
        overflow: 'hidden'
      },
      disabledRadioLabel: {
        color: '#bababa'
      },
      disabledRadioLabelV3: {
        color: '#454560'
      },
      correctAnswer: {
        color: '#2fb7a0',
        fontSize: '0.875rem'
      },
      incorrectAnswer: {
        color: '#e64360',
        fontSize: '0.875rem'
      },
      radioIcon: {
        width: '16px',
        height: '16px',
        borderColor: '#979797',
        color: '#00a1e1',
        marginRight: '9px'
      },
      voteButton: {
        fontSize: '14px',
        color: '#00a1e1',
        border: '1px solid #00a1e1',
        verticalAlign: 'middle',
        margin: 0,
        minWidth: '53px',
        padding: 0,
        width: '51px',
        height: '22px',
        marginLeft: '0'
      },
      voteButtonLabel: {
        margin: 0,
        padding: 0,
        lineHeight: '22px'
      },
      vote: {
        fontSize: '11px',
        verticalAlign: 'text-top'
      },
      totalVotes: {
        fontSize: '13px',
        marginTop: '5px'
      },
      totalVotesV3: {
        fontSize: '0.6875rem',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: '#454560'
      },
      optionsText: {
        fontSize: '0.875rem'
      },
      optionsTextV3: {
        fontSize: '0.875rem',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        lineHeight: 'normal',
        letterSpacing: 'normal',
        color: '#454560',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        width: 'calc(100% - 6.8rem)',
        bottom: '0'
      },
      viewResultsLink: {
        padding: '0',
        verticalAlign: 'middle',
        letterSpacing: '0',
        textTransform: 'none',
        fontWeight: '500',
        fontSize: '0.875rem',
        lineHeight: '1.375rem',
        position: 'relative'
      },
      viewResultsLinkPosition: {
        marginLeft: '1.25rem'
      }
    };
  }

  preventClickHandler(e) {
    if (!this || (this && this.state.selected === undefined)) {
      e.preventDefault();
      e.stopPropagation();
    }
  }
  voteClickHandler(e) {
    e.stopPropagation();
    this.setState({ attemptPending: true });
    let optionId = this.state.selected;
    if (optionId) {
      this.setState({ label: tr(this.state.isAssessment ? 'Answering...' : 'Voting...') });
    }
    optionId
      ? cards
          .attemptPoll(this.props.card.id, optionId)
          .then(() => {
            let newQuizQuestionOptions = this.state.quizQuestionOptions.reduce((acc, curr) => {
              this.setState({ errorMessage: '' });
              this.setState({ label: tr(this.state.isAssessment ? 'Answer' : 'Vote') });
              if (curr.id === optionId) {
                let newOption = Object.assign({}, curr);
                newOption.count++;
                acc.push(newOption);
              } else {
                acc.push(curr);
              }
              return acc;
            }, []);
            let voteCurrent = {
              selected: this.state.selected,
              id: this.props.card.id,
              attemptCount: this.state.attemptCount + 1,
              newQuizQuestionOptions: newQuizQuestionOptions
            };
            this.setState({
              attemptPending: false,
              voteCurrent,
              attemptCount: this.state.attemptCount + 1,
              quizQuestionOptions: newQuizQuestionOptions,
              hasAttempted: true,
              reanswerMode: false,
              result: this.state.correctAnswer && optionId == this.state.correctAnswer.id
            });
            if (this.props.cardUpdated) this.props.cardUpdated();
            if (this.props.checkComplete) {
              this.props.checkComplete(this.props.card, true);
            }
          })
          .catch(err => {
            console.error(`Error in Poll.attemptPoll.func : ${err}`);
          })
      : this.setState({ errorMessage: 'Please select an option' });
  }

  componentDidMount() {
    if (this.props.cards !== undefined) {
      let correctAnswer = this.props.card.quizQuestionOptions.find(obj => {
        return obj.isCorrect === true;
      });
      if (correctAnswer) {
        this.setState({ correctAnswer });
      }
      if (this.state.isAssessment && this.state.hasAttempted) {
        let selected =
          this.props.card && this.props.card.attemptedOption && this.props.card.attemptedOption.id
            ? this.props.card.attemptedOption.id
            : this.props.card && this.props.card.attemptedOption;
        let result = correctAnswer && selected == correctAnswer.id;
        this.setState({ selected, result });
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cards !== undefined) {
      let state = {
        quizQuestionOptions:
          typeof nextProps.card.options !== 'undefined'
            ? nextProps.card.options
            : nextProps.card.quizQuestionOptions,
        result: null || this.state.result,
        correctAnswer: null || this.state.selected,
        isAssessment: nextProps.card.isAssessment,
        hasAttempted: nextProps.card.hasAttempted,
        attemptCount: nextProps.card.attemptCount,
        selected: this.state.selected || undefined
      };
      let correctAnswer =
        nextProps.card.quizQuestionOptions &&
        nextProps.card.quizQuestionOptions.find(obj => {
          return obj.isCorrect === true;
        });
      if (correctAnswer) {
        state['correctAnswer'] = correctAnswer;
      }
      if (nextProps.card.isAssessment && nextProps.card.hasAttempted) {
        // Made below changes as show api contain attemptedOption hash containing { id, image_url, is_correct, label } and in cards api we only get attemptedOption id
        let selected =
          nextProps.card && nextProps.card.attemptedOption && nextProps.card.attemptedOption.id
            ? nextProps.card.attemptedOption.id
            : nextProps.card && nextProps.card.attemptedOption;
        let result = correctAnswer && selected == correctAnswer.id;
        state['selected'] = selected;
        state['result'] = result;
      }
      if (nextProps.card && nextProps.card.attemptedOption) {
        state['selected'] =
          nextProps.card && nextProps.card.attemptedOption && nextProps.card.attemptedOption.id
            ? nextProps.card.attemptedOption.id
            : nextProps.card && nextProps.card.attemptedOption;
        state['hasAttempted'] = true;
        state['quizQuestionOptions'] =
          typeof nextProps.card.options !== 'undefined'
            ? nextProps.card.options
            : nextProps.card.quizQuestionOptions;
        state['attemptCount'] = nextProps.card.attemptCount;
      }
      if (this.state.voteCurrent.id && this.state.voteCurrent.id === nextProps.card.id) {
        state['selected'] = this.state.voteCurrent.selected;
        state['result'] = correctAnswer && state['selected'] == correctAnswer.id;
        state['attemptCount'] = this.state.voteCurrent.attemptCount;
        state['hasAttempted'] = true;
        state['quizQuestionOptions'] = this.state.voteCurrent.newQuizQuestionOptions;
      }
      this.setState(state);
    }
  }

  voteBtnRender = () => {
    let isOwner =
      (this.props.card.author && +this.props.card.author.id) ===
      (this.props.currentUserId && +this.props.currentUserId);
    let isPoll = !this.state.isAssessment;
    if (this.props.cardOverview) {
      return (
        <div onClick={this.preventClickHandler}>
          <SecondaryButton
            key="vote-button"
            label={this.state.label}
            disabled={
              (this.state.selected === undefined ||
                this.state.hasAttempted ||
                this.state.attemptPending) &&
              !this.props.card.canBeReanswered
            }
            onClick={this.voteClickHandler}
            className={`vote ${this.state.isCardV3 ? 'vote-v3' : ''} ${
              this.state.isCardV3 && this.state.isAssessment ? 'answer-v3' : ''
            } ${
              (this.state.selected === undefined ||
                this.state.hasAttempted ||
                this.state.attemptPending) &&
              !this.props.card.canBeReanswered
                ? 'disabled-v3'
                : ''
            }`}
            labelStyle={this.styles.voteButtonLabel}
          />
          {isOwner && isPoll && (
            <span>
              <a
                href="#"
                title="View results"
                onClick={e => {
                  this.viewResultsHandler(e, 'show');
                }}
                style={{ ...this.styles.viewResultsLink, ...this.styles.viewResultsLinkPosition }}
              >
                View results
              </a>
            </span>
          )}
        </div>
      );
    } else return null;
  };

  viewResultsHandler(e, viewType) {
    e.stopPropagation();
    e.preventDefault();
    let isOwner =
      (this.props.card.author && +this.props.card.author.id) ===
      (this.props.currentUserId && +this.props.currentUserId);
    if (isOwner && viewType === 'show') {
      this.setState({ viewResults: true });
    } else if (isOwner && viewType === 'hide') {
      this.setState({ viewResults: false });
    }
  }
  answeredBtnRender = () => {
    if (this.props.cardOverview && !this.state.hasAttempted) {
      return (
        <SecondaryButton
          key="vote-button"
          label={tr('Answered')}
          disabled={true}
          className="vote"
        />
      );
    } else return null;
  };

  adjustRoundedOffPercentages = (pollPercentageArray, targetPercentage) => {
    var percentageHash = new Object();

    var offset =
      targetPercentage -
      _.reduce(
        pollPercentageArray,
        function(acc, x) {
          return acc + Math.round(x.percentage);
        },
        0
      );
    _.chain(pollPercentageArray)
      .sortBy(function(x) {
        return Math.round(x.percentage) - x.percentage;
      })
      .map(function(x, i) {
        percentageHash[x.label] =
          Math.round(x.percentage) + (offset > i) - (i >= pollPercentageArray.length + offset);
      })
      .value();
    return percentageHash;
  };

  render() {
    let isOwner =
      (this.props.card.author && +this.props.card.author.id) ===
      (this.props.currentUserId && +this.props.currentUserId);
    let checkLabelLength = this.state.quizQuestionOptions.filter(item => {
      return item.label.length > 30;
    });
    let additionalStyle = this.props.pathwayModal
      ? checkLabelLength.length
        ? { width: '100%' }
        : { width: 'auto' }
      : { width: '100%' };
    let isPoll =
      (this.state.hasAttempted &&
        this.state.quizQuestionOptions &&
        !this.state.isAssessment &&
        !!this.state.quizQuestionOptions.length) ||
      (this.state.quizQuestionOptions &&
        !this.state.isAssessment &&
        !!this.state.quizQuestionOptions.length &&
        this.state.viewResults);
    let pollPercentageArray = [];
    this.state.quizQuestionOptions.map((option, index) => {
      let percentage = (option.count / this.state.attemptCount) * 100;
      pollPercentageArray.push({ label: option.label, percentage: percentage });
    });
    let PollPercentageHash = this.adjustRoundedOffPercentages(pollPercentageArray, 100);

    return (
      <div className="vertical-spacing-medium">
        {!this.state.hasAttempted &&
          !this.state.viewResults && [
            <RadioButtonGroup
              key="radio-button-group"
              name="poll-radio-button-group"
              className="vertical-spacing-medium"
              valueSelected={this.state.selected}
              onChange={(e, value) => {
                this.setState({ selected: value });
              }}
            >
              {this.state.quizQuestionOptions &&
                this.state.quizQuestionOptions.map((option, index) => {
                  return (
                    <RadioButton
                      iconStyle={this.styles.radioIcon}
                      labelStyle={
                        this.state.isCardV3 ? this.styles.radioLabelV3 : this.styles.radioLabel
                      }
                      key={index}
                      value={option.id}
                      disabled={!this.props.cardOverview}
                      style={additionalStyle}
                      className={`poll-radio-box ${
                        this.state.isCardV3 ? 'poll-radio-v3' : 'poll-radio'
                      }`}
                      label={option.label}
                      aria-label={option.label}
                      onClick={e => {
                        e.stopPropagation();
                      }}
                    />
                  );
                })}
            </RadioButtonGroup>,
            this.voteBtnRender(),
            <span style={this.styles.incorrectAnswer}> {this.state.errorMessage}</span>
          ]}
        {isPoll &&
          this.state.quizQuestionOptions.map((option, index) => {
            let percentage = PollPercentageHash[option.label] + '%';
            let voteLabel = option.count > 1 ? 'votes' : 'vote';
            return (
              <div
                key={index}
                className="poll-stats-bar"
                style={this.state.isCardV3 ? this.styles.pollStars : {}}
              >
                <span
                  className={`bar ${this.state.isCardV3 ? 'bar-v3' : ''}`}
                  style={{ width: percentage }}
                />
                <span
                  className={!!this.state.attemptCount ? 'name' : 'name name-no-result'}
                  style={this.state.isCardV3 ? this.styles.optionsTextV3 : this.styles.optionsText}
                >
                  {option.label}
                </span>
                {this.state.attemptCount && (
                  <div>
                    <span
                      className={`votes ${this.state.isCardV3 ? 'votes-v3' : ''}`}
                      style={this.styles.vote}
                    >{`(${option.count} ${tr(voteLabel)}) `}</span>
                    <span className={`percentage ${this.state.isCardV3 ? 'percentage-v3' : ''}`}>
                      {percentage}
                    </span>
                  </div>
                )}
              </div>
            );
          })}
        {isPoll && (
          <div style={this.state.isCardV3 ? this.styles.totalVotesV3 : this.styles.totalVotes}>
            {' '}
            {tr('Total Votes Polled')}: {!!this.state.attemptCount ? this.state.attemptCount : 0}{' '}
          </div>
        )}
        {isPoll && isOwner && !this.state.hasAttempted && (
          <a
            href="#"
            title="Go back"
            onClick={e => {
              this.viewResultsHandler(e, 'hide');
            }}
            style={this.styles.viewResultsLink}
          >
            Go back
          </a>
        )}
        {this.state.isAssessment &&
          this.state.hasAttempted && [
            <RadioButtonGroup
              key="radio-button-group"
              name="poll-radio-button-group"
              className="vertical-spacing-medium"
              valueSelected={this.state.selected}
              disabled={true}
              onChange={(e, value) => {
                this.setState({ reanswerMode: this.props.card.canBeReanswered, selected: value });
              }}
            >
              {this.state.quizQuestionOptions &&
                this.state.quizQuestionOptions.map((option, index) => {
                  let iconStyle = this.styles.radioIcon;
                  let labelStyle = {
                    ...(this.state.isCardV3 ? this.styles.radioLabelV3 : this.styles.radioLabel),
                    ...(this.state.isCardV3
                      ? this.styles.disabledRadioLabelV3
                      : this.styles.disabledRadioLabel)
                  };
                  let selectedItem = this.state.selected && this.state.selected === option.id;
                  if (selectedItem && !this.state.reanswerMode) {
                    iconStyle = {
                      ...iconStyle,
                      ...this.styles[
                        this.state.result === true ? 'correctAnswer' : 'incorrectAnswer'
                      ]
                    };
                    labelStyle = {
                      ...labelStyle,
                      ...this.styles[
                        this.state.result === true ? 'correctAnswer' : 'incorrectAnswer'
                      ]
                    };
                  }
                  return (
                    <RadioButton
                      iconStyle={iconStyle}
                      labelStyle={labelStyle}
                      key={index}
                      disabled={!(this.props.card.canBeReanswered && this.props.cardOverview)}
                      className={`${
                        this.state.isCardV3 ? 'poll-radio-v3' : 'poll-radio'
                      } poll-radio-box ${selectedItem &&
                        !this.state.reanswerMode &&
                        (this.state.result === true
                          ? this.state.isCardV3
                            ? 'correct-poll-radio-v3'
                            : 'correct-poll-radio'
                          : this.state.isCardV3
                          ? 'incorrect-poll-radio-v3'
                          : 'incorrect-poll-radio')}`}
                      value={option.id}
                      label={
                        this.state.isCardV3 ? (
                          option.label
                        ) : (
                          <div>
                            <span>{option.label}</span>
                            {selectedItem &&
                              this.state.result === true &&
                              !this.state.reanswerMode && <span>{tr(' - Correct answer')}</span>}
                            {selectedItem &&
                              this.state.result === false &&
                              !this.state.reanswerMode && <span>{tr(' - Incorrect answer')}</span>}
                          </div>
                        )
                      }
                      aria-label={option.id}
                      onTouchTap={e => {
                        e.stopPropagation();
                      }}
                    />
                  );
                })}
            </RadioButtonGroup>,
            this.voteBtnRender(),
            <span style={this.styles.incorrectAnswer}> {this.state.errorMessage}</span>
          ]}
      </div>
    );
  }
}

Poll.propTypes = {
  cardUpdated: PropTypes.func,
  checkComplete: PropTypes.func,
  cardOverview: PropTypes.bool,
  pathwayModal: PropTypes.bool,
  cards: PropTypes.object,
  card: PropTypes.object,
  isCardV3: PropTypes.bool,
  currentUserId: PropTypes.any
};

export default connect(state => ({
  cards: state.cards.toJS()
}))(Poll);
