import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import pubnubAdapter from '../../utils/pubnubAdapter';
import { liveStreamJoied, liveStreamLeft } from 'edc-web-sdk/requests/liveStream';
import { getSpecificUserInfo } from '../../actions/currentUserActions';

class LiveStream extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.onMessageReceive = this.onMessageReceive.bind(this);
    this.getPubNubHistory = this.getPubNubHistory.bind(this);
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(['handle'], this.props.currentUser);
    this.props
      .dispatch(userInfoCallBack)
      .then(userData => {
        let uuid = this.getUuid();
        this.getPubNubHistory(uuid);
        this.pubnub.subscribe({
          channel: uuid,
          message: function(message) {
            this.onMessageReceive(message);
          }.bind(this),
          /*eslint handle-callback-err: "off"*/
          error: function(err) {}
        });
        this.postActionOnPubNub('join', userData);
      })
      .catch(err => {
        console.error(`Error in LiveStream.getSpecificUserInfo.func: ${err}`);
      });
    this.props.card.videoStream &&
      this.props.card.videoStream.id &&
      liveStreamJoied(this.props.card.videoStream.id);
  }

  getUuid() {
    if (this.props.card.videoStream !== undefined) {
      return this.props.card.videoStream.uuid;
    } else {
      return this.props.card.uuid;
    }
  }

  componentWillUnmount() {
    this.postActionOnPubNub('leave');
    this.pubnub.unsubscribe({
      channel: this.props.card.uuid
    });
    this.props.card.videoStream &&
      this.props.card.videoStream.id &&
      liveStreamLeft(this.props.card.videoStream.id);
  }

  onMessageReceive(message) {
    if (message.a == 'stop') {
      ReactDOM.render(
        <div className="end-of-stream-message">
          The user has ended this stream. Thank you for watching.
        </div>,
        document.getElementById('video-stream-' + this.props.card.id)
      );
    }
  }

  getPubNubHistory(channel) {
    this.pubnub.history({
      channel: channel,
      callback: function(message) {
        for (var i = 0; i < message[0].length; i++) {
          this.onMessageReceive(message[0][i]);
        }
      }.bind(this)
    });
  }

  postActionOnPubNub = (action, userData = null) => {
    //prepare payload
    var pubnubPayload = {};
    pubnubPayload['uid'] = this.props.currentUser.id || '';
    pubnubPayload['p'] = this.props.currentUser.picture || '';
    pubnubPayload['fn'] = this.props.currentUser.name.split(' ')[0] || '';
    pubnubPayload['ln'] = this.props.currentUser.name.split(' ')[1] || '';
    pubnubPayload['h'] = this.props.currentUser.handle || (userData && userData.handle) || '';
    pubnubPayload['a'] = action;
    pubnubPayload['type'] = 'videoStream';
    //publish to pubnub
    this.pubnub.publish({
      channel: this.getUuid(),
      message: pubnubPayload,
      callback: m => {}
    });
  };

  render() {
    return (
      <div id={`video-stream-${this.props.card.id}`} className="video-stream__live">
        <iframe
          frameBorder="0"
          style={{ width: '100%', height: '30rem' }}
          src={
            this.props.card.embedPlaybackUrl ||
            (this.props.card.videoStream !== undefined &&
              this.props.card.videoStream.embedPlaybackUrl)
          }
          allowFullScreen
        />
      </div>
    );
  }
}

LiveStream.propTypes = {
  currentUser: PropTypes.object,
  card: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LiveStream);
