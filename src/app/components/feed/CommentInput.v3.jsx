import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getMentionSuggest } from '../../actions/usersActions';
import BlurImage from '../common/BlurImage';
import ReactDOM from 'react-dom';

//UI components
import Popover from 'react-simple-popover';
import ListItem from 'material-ui/List/ListItem';
//actions
import { postComment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';

import { tr } from 'edc-web-sdk/helpers/translations';

class CommentInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
      inputValue: '',
      mentionQuery: '',
      sendingComment: false,
      userTagging: window.ldclient.variation('user-tagging', true)
    };
    this.styles = {
      userListPopover: {
        height: '112px',
        overflow: 'scroll'
      },
      anchorOrigin: {
        horizontal: 'left',
        vertical: 'bottom'
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'top'
      },
      commentInputhint: {
        marginTop: '12px',
        bottom: 'inherit'
      },
      selectContainer: {
        width: '100%',
        maxHeight: '112px',
        overflow: 'auto',
        boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.3)',
        background: 'white',
        zIndex: 11
      },
      selectContent: {
        width: '100%',
        marginTop: '-5px',
        padding: 0
      },
      selectItem: {
        minHeight: '56px'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '-0.2rem',
        left: '1rem'
      }
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.submitInputHandler = this.submitInputHandler.bind(this);
    this.clickInputHandler = this.clickInputHandler.bind(this);
  }

  componentDidMount() {
    if (this.props.autoFocus) {
      this.refs.input.focus();
    }
  }

  inputChangeHandler(event) {
    let inputValue = event.target.value;
    this.resizeTextarea();
    let parts = inputValue.split(' ');
    if (parts.length && parts[parts.length - 1].charAt(0) === '@') {
      let mentionQuery = parts[parts.length - 1].slice(1).toLowerCase();
      this.callgetMentionSuggest(inputValue, mentionQuery);
    } else if (parts.length > 1 && parts[parts.length - 2].charAt(0) === '@') {
      let mentionQuery =
        parts[parts.length - 2].slice(1).toLowerCase() +
        ' ' +
        parts[parts.length - 1].toLowerCase();
      this.callgetMentionSuggest(inputValue, mentionQuery);
    } else {
      this.setState({ inputValue, open: false, mentionQuery: '' });
    }
  }

  resizeTextarea() {
    this.refs.input.style.height = 0;
    this.refs.input.style.height = (this.refs.input.scrollHeight + 2) / 16 + 'rem';
  }

  callgetMentionSuggest = (inputValue, mentionQuery) => {
    let { userTagging } = this.state;
    if (userTagging) {
      if (this.fetchUserTimeout) {
        clearTimeout(this.fetchUserTimeout);
      }
      this.fetchUserTimeout = setTimeout(() => {
        this.props.dispatch(getMentionSuggest(mentionQuery));
      }, 300);
    }

    this.setState({
      inputValue,
      open: true,
      anchorEl: ReactDOM.findDOMNode(this.refs.input),
      mentionQuery
    });
  };

  mentionClickHandler(e, handle) {
    e && e.preventDefault();
    let tempHandle = this.state.inputValue;
    let tempHandleArr = this.state.inputValue.split(' ');
    if (tempHandleArr[tempHandleArr.length - 1].charAt(0) == '@') {
      tempHandle = tempHandleArr.pop(tempHandleArr[tempHandleArr.length - 1]);
      tempHandle = tempHandleArr.join(' ');
    } else if (tempHandleArr[tempHandleArr.length - 2].charAt(0) == '@') {
      tempHandleArr.splice(tempHandleArr.length - 2, tempHandleArr.length);
      tempHandle = tempHandleArr.join(' ');
    }
    tempHandle
      ? this.setState({ inputValue: tempHandle + ' ' + handle + ' ' })
      : this.setState({ inputValue: handle + ' ' });
    setTimeout(() => {
      this.setState({ open: false });
      this.refs.input.focus();
    }, 300);
  }

  mentionKeyHandler = (e, handle) => {
    if (e.keyCode == 13) {
      this.mentionClickHandler('', handle);
    }
  };

  submitInputHandler(e) {
    if (e.keyCode === 13) {
      this.clickInputHandler();
    }
  }

  clickInputHandler() {
    if (this.state.inputValue.trim() !== '') {
      this.setState({ sendingComment: true }, () => {
        postComment(
          this.props.cardId,
          this.state.inputValue,
          this.props.numOfComments + 1,
          this.props.cardType,
          this.props.currentUser.get('id')
        )
          .then(data => {
            if (data && data.embargoErr) {
              this.setState({
                sendingComment: false
              });
              this.props.dispatch(
                openSnackBar(
                  'Sorry, the content you are trying to post is from unapproved website or words.',
                  true
                )
              );
            } else {
              this.props.commentPostCallback(data);
              this.setState(
                {
                  inputValue: '',
                  sendingComment: false
                },
                () => {
                  this.resizeTextarea();
                }
              );
            }
          })
          .catch(err => {
            console.error(`Error in CommentInput.v3.postComment.func : ${err}`);
          });
      });
    }
  }

  sendComment = e => {
    e.preventDefault();
    this.clickInputHandler();
  };

  handleClose(e) {
    this.setState({ open: false });
  }

  render() {
    let usersMap = this.props.users.get('idMap');
    let usersArr = usersMap.filter(user => {
      let userName = user.get('fullName') || user.get('name');
      let userHandle = user.get('handle');
      return (
        (userName && userName.toLowerCase().indexOf(this.state.mentionQuery) !== -1) ||
        (userHandle && userHandle.toLowerCase().indexOf(this.state.mentionQuery) !== -1)
      );
    });
    if (!!usersArr && usersArr.size === 0) {
      usersArr = usersMap.filter(user => {
        return !!(user.get('fullName') || user.get('name')) || !!user.get('handle');
      });
    }
    return (
      <div className="comment-container-v3">
        <div className="comment-create">
          <textarea
            className="comment-input"
            ref="input"
            placeholder={
              this.props.isCardV3
                ? tr('Leave a comment here...')
                : tr('Leave a comment / Tag peers...')
            }
            value={this.state.inputValue}
            onChange={this.inputChangeHandler}
            onKeyDown={this.submitInputHandler}
            disabled={this.state.sendingComment}
          />
          <div className="comment-button">
            <span type="submit">@</span>
            <a onClick={this.sendComment}>
              <input
                type="button"
                disabled={this.state.sendingComment}
                value={tr('Send')}
                style={{ color: '#4a90e2' }}
              />
            </a>
          </div>
        </div>

        <Popover
          style={this.styles.selectContent}
          containerStyle={this.styles.selectContainer}
          placement="bottom"
          container={this}
          target={this.refs.input}
          show={usersArr.size === 0 ? false : this.state.open}
          onHide={this.handleClose.bind(this)}
        >
          <div className="cardTypePopupIsActive">
            {usersArr.map((user, index) => {
              let userNameOrHandle =
                user.get('name') == null ? user.get('handle').replace('@', '') : user.get('name');
              return (
                <ListItem
                  key={`comment_user_V2_${index}`}
                  leftIcon={
                    <BlurImage
                      commentInput={true}
                      style={this.styles.avatarBox}
                      id={user.id}
                      image={user.get('avatarimages').get('small')}
                    />
                  }
                  primaryText={<span>{userNameOrHandle}</span>}
                  onMouseDown={e => {
                    this.mentionClickHandler(e, user.get('handle'));
                  }}
                  onKeyDown={e => this.mentionKeyHandler(e, user.get('handle'))}
                />
              );
            })}
          </div>
        </Popover>
      </div>
    );
  }
}

CommentInput.propTypes = {
  cardId: PropTypes.string.isRequired,
  cardType: PropTypes.string,
  users: PropTypes.object,
  currentUser: PropTypes.object,
  autoFocus: PropTypes.bool,
  commentPostCallback: PropTypes.func,
  numOfComments: PropTypes.number,
  isCardV3: PropTypes.bool
};

export default connect(state => ({ users: state.users, currentUser: state.currentUser }))(
  CommentInput
);
