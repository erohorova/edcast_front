import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import addParamStart from '../../utils/addParamStartForYoutubeLink';

class InlineVideo extends Component {
  constructor(props, context) {
    super(props, context);
    this.onMouseOnVideo = this.onMouseOnVideo.bind(this);
    this.onMouseOffVideo = this.onMouseOffVideo.bind(this);

    this.state = {
      hoverId: null
    };
    this.generateEmbedHTML(props);
  }

  componentDidMount() {
    let _this = this;
    window.addEventListener('blur', function() {
      if (_this.state.hoverId) {
        _this.setState({
          hoverId: null
        });
      }
    });
    setTimeout(() => {
      let cardIDS = this.getAllVideoCardIds();
      let cardId = this.props.cardId;
      let elem;
      if (this.props.sourceType == 'cardOverviewModal') {
        elem = document.querySelector(`.card-overview-content #ele_${cardId} iframe`);
      } else if (this.props.sourceType == 'feedView') {
        elem = document.querySelector(`.list-view-block #ele_${cardId} iframe`);
      } else if (this.props.sourceType == 'bigView') {
        elem = document.querySelector(`.big-vew-block #ele_${cardId} iframe`);
      }
      let iframeClickedId = '';
      if (elem) {
        elem.addEventListener('mouseover', function() {
          iframeClickedId = cardId;
        });
        elem.addEventListener('mouseout', function() {
          iframeClickedId = '';
          window.focus();
        });

        window.addEventListener('blur', () => {
          this.refreshIframe(cardIDS, iframeClickedId, this.props.sourceType);
        });
      }
    }, 200);
  }

  componentWillReceiveProps(nextProps) {
    this.generateEmbedHTML(nextProps);
  }

  refreshIframe = (cardIDS, iframeClickedId, sourceType) => {
    cardIDS.map(ele => {
      if (iframeClickedId && ele != iframeClickedId) {
        if (sourceType == 'cardOverviewModal') {
          document.querySelector(
            `.card-overview-content #ele_${ele} iframe`
          ).src = document.querySelector(`.card-overview-content #ele_${ele} iframe`).src;
        } else if (sourceType == 'feedView') {
          document.querySelector(
            `.list-view-block #ele_${ele} iframe`
          ).src = document.querySelector(`.list-view-block #ele_${ele} iframe`).src;
        } else if (sourceType == 'bigView') {
          document.querySelector(`.big-vew-block #ele_${ele} iframe`).src = document.querySelector(
            `.big-vew-block #ele_${ele} iframe`
          ).src;
        }
      }
      if (ele == iframeClickedId) {
        if (sourceType == 'cardOverviewModal') {
          if (document.querySelector(`.list-view-block #ele_${ele} iframe`)) {
            document.querySelector(
              `.list-view-block #ele_${ele} iframe`
            ).src = document.querySelector(`.list-view-block #ele_${ele} iframe`).src;
          }
          if (document.querySelector(`.big-vew-block #ele_${ele} iframe`)) {
            document.querySelector(
              `.big-vew-block #ele_${ele} iframe`
            ).src = document.querySelector(`.big-vew-block #ele_${ele} iframe`).src;
          }
        } else if (sourceType == 'feedView') {
          if (document.querySelector(`.card-overview-content #ele_${ele} iframe`)) {
            document.querySelector(
              `.card-overview-content #ele_${ele} iframe`
            ).src = document.querySelector(`.card-overview-content #ele_${ele} iframe`).src;
          }
          if (document.querySelector(`.big-vew-block #ele_${ele} iframe`)) {
            document.querySelector(
              `.big-vew-block #ele_${ele} iframe`
            ).src = document.querySelector(`.big-vew-block #ele_${ele} iframe`).src;
          }
        } else if (sourceType == 'bigView') {
          if (document.querySelector(`.list-view-block #ele_${ele} iframe`)) {
            document.querySelector(
              `.list-view-block #ele_${ele} iframe`
            ).src = document.querySelector(`.list-view-block #ele_${ele} iframe`).src;
          }
          if (document.querySelector(`.card-overview-content #ele_${ele} iframe`)) {
            document.querySelector(
              `.card-overview-content #ele_${ele} iframe`
            ).src = document.querySelector(`.card-overview-content #ele_${ele} iframe`).src;
          }
        }
      }
    });
    if (this.props.getAllvideoElements) {
      this.props.getAllvideoElements();
    }
  };

  generateEmbedHTML = compProps => {
    let play =
      !!(compProps.allowAutoplay || compProps.team.config['enable-video-auto-play']) || false;
    let embedHtml = this.embedHtml;
    let videoCardsCount = document.getElementsByClassName('link-card-video').length;
    let isChannelPage = document.getElementsByClassName('channel-base').length;
    let stanalone =
      document.getElementsByClassName('stand-alone').length ||
      document.getElementsByClassName('slide-out-card-modal-container').length;
    if (videoCardsCount) {
      if (isChannelPage) {
        this.embedHtml = {
          __html: compProps.embedHtml
            .replace(/'/g, '"')
            .replace(/width=\"[0-9]*\"/g, 'width="100%"')
            .replace(/height=\"[0-9]*\"/g, 'height="294"')
            .replace(/autoplay=[a-z0-9-]*/g, 'autoplay=0')
            .replace('feature=oembed', 'feature=oembed&rel=0&showinfo=0')
            .replace(/allow=\"autoplay; encrypted-media\"/g, '')
        };
      } else if (this.props.sourceType != 'cardOverviewModal') {
        if (videoCardsCount == 1) {
          this.embedHtml = {
            __html: compProps.embedHtml
              .replace(/'/g, '"')
              .replace(/width=\"[0-9]*\"/g, 'width="100%"')
              .replace(/height=\"[0-9]*\"/g, 'height="294"')
              .replace(/autoplay=[a-z0-9-]*/g, 'autoplay=' + play)
              .replace('feature=oembed', 'feature=oembed&rel=0&showinfo=0')
          };
        } else {
          this.embedHtml = {
            __html: compProps.embedHtml
              .replace(/'/g, '"')
              .replace(/width=\"[0-9]*\"/g, 'width="100%"')
              .replace(/height=\"[0-9]*\"/g, 'height="294"')
              .replace(/autoplay=[a-z0-9-]*/g, 'autoplay=0')
              .replace('feature=oembed', 'feature=oembed&rel=0&showinfo=0')
              .replace(/allow=\"autoplay; encrypted-media\"/g, '')
          };
        }
      } else {
        this.embedHtml = {
          __html: compProps.embedHtml
            .replace(/'/g, '"')
            .replace(/width=\"[0-9]*\"/g, 'width="100%"')
            .replace(/height=\"[0-9]*\"/g, 'height="294"')
            .replace(/autoplay=[a-z0-9-]*/g, 'autoplay=' + play)
            .replace('feature=oembed', 'feature=oembed&rel=0&showinfo=0')
        };
      }
    } else if (
      stanalone ||
      this.props.sourceType === 'cardOverviewModal' ||
      this.props.sourceType === 'slideOut'
    ) {
      this.embedHtml = {
        __html: compProps.embedHtml
          .replace(/'/g, '"')
          .replace(/width=\"[0-9]*\"/g, 'width="100%"')
          .replace(/height=\"[0-9]*\"/g, 'height="294"')
          .replace(/autoplay=[a-z0-9-]*/g, 'autoplay=' + play)
          .replace('feature=oembed', 'feature=oembed&rel=0&showinfo=0')
      };
    }

    if (compProps.videoUrl) {
      embedHtml = addParamStart(this.embedHtml, compProps.videoUrl);
    }
  };

  getAllVideoCardIds = () => {
    let divElements = document.getElementsByClassName('link-card-video');
    let tempArr = [];
    let id;
    for (let i = 0; i < divElements.length; i++) {
      id = divElements[i].id.split('_')[1];
      tempArr.push(id);
    }
    if (divElements.length > 1) {
      this.generateEmbedHTML(this.props);
    }
    return tempArr;
  };

  onMouseOnVideo() {
    this.setState({
      hoverId: this.props.cardId
    });
  }

  onMouseOffVideo() {
    this.setState({
      hoverId: null
    });
    window.focus();
  }

  render() {
    return (
      <div>
        <div
          id={`ele_${this.props.cardId}`}
          onMouseEnter={this.onMouseOnVideo}
          onMouseLeave={this.onMouseOffVideo}
          className={`${this.props.isCardV3 ? 'inline-video-v3' : 'inline-video'} link-card-video`}
          dangerouslySetInnerHTML={this.embedHtml}
        />
      </div>
    );
  }
}

InlineVideo.propTypes = {
  cardId: PropTypes.string,
  team: PropTypes.object,
  embedHtml: PropTypes.string.isRequired,
  allowAutoplay: PropTypes.bool,
  videoUrl: PropTypes.string,
  getAllvideoElements: PropTypes.func,
  sourceType: PropTypes.string,
  isCardV3: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(InlineVideo);
