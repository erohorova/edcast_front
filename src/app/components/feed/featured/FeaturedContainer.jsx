import React, { Component } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';

import { getCards, fetchCard } from 'edc-web-sdk/requests/cards';
import { getProviderLogos } from 'edc-web-sdk/requests/ecl';

import ListView from '../../common/ListView';
import Card from '../../cards/Card';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import getCardView from '../../../utils/getCardView';
import { Permissions } from '../../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import updatePageLastVisit from '../../../utils/updatePageLastVisit';
import { cardFields } from '../../../constants/cardFields';
//Icons

const limit = 9;

class FeaturedContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleScroll = this.handleScroll.bind(this);

    this.state = {
      providerLogos: {},
      cards: [],
      filteredCard: [],
      isLastPage: false,
      pending: true,
      loaded: false,
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };

    if (this.state.showNew) {
      updatePageLastVisit('featuredLastVisit');
    }
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';

    if (newView !== this.state.viewRegime) {
      this.setState({ cards: [], pending: true });
      getCards({
        limit,
        is_official: true,
        sort: 'promoted_first',
        filter_by_language: this.state.multilingualContent,
        last_access_at: localStorage.getItem('featuredLastVisit'),
        fields: cardFields
      })
        .then(data => {
          let isLastPage = data.length === 0;
          this.setState({
            cards: data,
            isLastPage,
            pending: false,
            loaded: true,
            viewRegime: newView
          });
          if (!isLastPage) {
            document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
              'calc(100vh - 11.875rem)';
          }
        })
        .catch(err => {
          console.error(`Error in FeaturedContainer.getCards.func : ${err}`);
        });
    } else {
      if (this.state.loaded) {
        this.checkCardList();
      }
    }
  }

  checkCardList() {
    let cards = this.state.cards.filter(card => !card.featuredHidden);
    this.setState({ cards }, calculateTeemFeedPosition);
  }

  async componentWillMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    calculateTeemFeedPosition();
    getCards({
      limit,
      is_official: true,
      sort: 'promoted_first',
      filter_by_language: this.state.multilingualContent,
      last_access_at: localStorage.getItem('featuredLastVisit'),
      fields: cardFields
    })
      .then(data => {
        let isLastPage = data.length === 0;
        this.setState(
          {
            cards: [...this.state.cards, ...data],
            isLastPage,
            pending: false,
            loaded: true
          },
          calculateTeemFeedPosition
        );
        if (!isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in FeaturedContainer.getCards.func : ${err}`);
      });
    getProviderLogos()
      .then(providerLogos => {
        this.setState({
          providerLogos: providerLogos
        });
      })
      .catch(err => {
        console.error(`Error in FeaturedContainer.getProviderLogos.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadMore();
          this.getStream();
        }
      }
    },
    150,
    { leading: false }
  );

  loadMore() {
    if (!this.state.isLastPage) {
      this.setState({ pending: true }, () => {
        getCards({
          offset: this.state.cards.length,
          limit,
          is_official: true,
          sort: 'promoted_first',
          filter_by_language: this.state.multilingualContent,
          last_access_at: localStorage.getItem('featuredLastVisit'),
          fields: cardFields
        })
          .then(data => {
            this.setState(
              {
                cards: [...this.state.cards, ...data],
                isLastPage: data.length === 0,
                pending: false
              },
              calculateTeemFeedPosition
            );
            setActivityListHeight(this.state.homepageVersion, 'card');
          })
          .catch(err => {
            console.error(`Error in FeaturedContainer.loadMore.getCards.func : ${err}`);
          });
      });
    }
  }

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => card.id !== id);
    this.setState({ cards: newCardsList }, calculateTeemFeedPosition);
    setActivityListHeight(this.state.homepageVersion, 'card');
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
      }
    }
  }

  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let cards = this.state.cards;
        let findCardIndex = findIndex(cards, item => {
          return item.id == id;
        });
        cards[findCardIndex] = data;
        this.setState({ cards }, calculateTeemFeedPosition);
      })
      .catch(err => {
        console.error(`Error in FeaturedContainer.fetchCard.func : ${err}`);
      });
  };

  render() {
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);
    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');
    return (
      <div id="feed">
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          loadingMessage={tr('Loading...')}
        >
          {this.state.cards.length > 0 && (
            <div
              className={
                viewRegime == 'Tile'
                  ? 'custom-card-container align-tileview-feed'
                  : 'custom-card-container'
              }
            >
              <div className="five-card-column vertical-spacing-medium">
                {this.state.cards.map(card => {
                  return (
                    <Card
                      card={card}
                      key={card.id}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      removeCardFromList={this.removeCardFromList}
                      moreCards={!isRail}
                      type={viewRegime}
                      closeCardModal={this.findCheckedCardAtList}
                      author={card.author}
                      isStandalone={false}
                      showComment={isShowComment}
                    />
                  );
                })}
              </div>
            </div>
          )}
          {!this.state.pending && this.state.cards.length == 0 && (
            <div className="container-padding vertical-spacing-medium custom-result">
              <div className="text-center vertical-spacing-large">
                <div className="empty-message data-not-available-msg">
                  {tr('There are no available cards.')}
                </div>
              </div>
            </div>
          )}
        </ListView>
      </div>
    );
  }
}

FeaturedContainer.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  route: PropTypes.object,
  cards: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(FeaturedContainer);
