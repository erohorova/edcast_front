import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import LiveCommentItem from './LiveCommentItem';
import LiveCommentInput from './LiveCommentInput.v2';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import pubnubAdapter from '../../utils/pubnubAdapter';
import { Permissions } from '../../utils/checkPermissions';

class LiveCommentList extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.state = {
      showAll: false,
      comments: [],
      commentCount: 0,
      deletedComments: [],
      inModal: props.inModal || false
    };
    this.onMessageReceive = this.onMessageReceive.bind(this);
    this.onMessageReceiveSubscribe = this.onMessageReceiveSubscribe.bind(this);
    this.getPubNubHistory = this.getPubNubHistory.bind(this);
    this.deleteComment = this.deleteComment.bind(this);
    this.subscribeToPubNub = this.subscribeToPubNub.bind(this);
  }

  componentDidMount() {
    this.postActionOnPubNub('join');
    let uuid = this.getUuid();
    this.getPubNubHistory(uuid);
    this.pubnub.init(this);
    this.subscribeToPubNub(uuid);
    this.pubnub.getMessage(uuid, msg => {
      this.onMessageReceive(!!msg.message && msg.message);
    });
  }

  subscribeToPubNub(channel) {
    this.pubnub.subscribe({
      channels: [channel],
      message: function(message) {
        if (message.a == 'deletedComment') {
          this.onMessageReceiveSubscribe(message);
        } else if (
          (message.a == 'join' || message.a == 'leave') &&
          this.props.onMessageReceiveJoinLeave
        ) {
          this.props.onMessageReceiveJoinLeave(message);
        } else {
          this.onMessageReceive(message);
        }
      },
      /*eslint handle-callback-err: "off"*/
      error: function(err) {
        console.error(`Error in LiveCommentList.subscribeToPubnub.func : ${err}`);
      }
    });
  }

  getUuid() {
    return this.props.cardUuid;
  }

  getStatus() {
    return this.props.cardStatus;
  }

  componentWillUnmount() {
    this.postActionOnPubNub('leave');
    this.pubnub.unsubscribe({
      channel: this.getUuid()
    });
  }

  postActionOnPubNub = action => {
    //prepare payload
    var pubnubPayload = {};
    pubnubPayload['uid'] = this.props.currentUser.id || '';
    pubnubPayload['p'] = this.props.currentUser.picture || '';
    pubnubPayload['fn'] = this.props.currentUser.first_name || '';
    pubnubPayload['ln'] = this.props.currentUser.last_name || '';
    pubnubPayload['h'] = this.props.currentUser.handle || '';
    pubnubPayload['a'] = action;
    pubnubPayload['type'] = 'videoStream';
    //publish to pubnub
    this.pubnub.publish({
      channel: this.getUuid(),
      message: pubnubPayload,
      callback: m => {}
    });
  };

  onMessageReceiveSubscribe(message) {
    if (message.a == 'deletedComment') {
      this.setState({ deletedComments: message.dc });

      const removeIndex = this.state.comments.findIndex(
        item => item.msgId === message.dc[message.dc.length - 1]
      );
      let commentsAfterDelete = this.state.comments.slice();
      if (removeIndex !== -1) commentsAfterDelete.splice(removeIndex, 1);

      this.setState({
        comments: commentsAfterDelete,
        commentCount: commentsAfterDelete.length
      });
      this.props.liveCommentsCount(this.state.commentCount);
    }
  }

  onMessageReceive(message, dateFromHistory = new Date().toString()) {
    if (message.a == 'deletedComment') {
      this.setState({ deletedComments: message.dc });

      const removeIndex = this.state.comments.findIndex(
        item => item.msgId === message.dc[message.dc.length - 1]
      );
      let commentsAfterDelete = this.state.comments.slice();
      if (removeIndex !== -1) commentsAfterDelete.splice(removeIndex, 1);

      this.setState({
        comments: commentsAfterDelete,
        commentCount: commentsAfterDelete.length
      });
    }
    if (message.a == 'stop') {
      ReactDOM.render(
        <div className="end-of-stream-message">
          The user has ended this stream. Thank you for watching.
        </div>,
        document.getElementById('video-stream-' + this.props.cardId)
      );
    }

    if ((message.a == 'join' || message.a == 'leave') && this.props.onMessageReceiveJoinLeave) {
      this.props.onMessageReceiveJoinLeave(message);
    }

    let comments = this.state.comments;
    if (
      message.a == 'comment' &&
      this.state.deletedComments.indexOf(message.msgId) === -1 &&
      this.state.comments.indexOf(message.msgId) === -1
    ) {
      let comment = {
        id: message.uid,
        msgId: message.msgId,
        message: message.m,
        createdAt: dateFromHistory,
        user: {
          id: message.uid,
          handle: message.h,
          firstName: message.fn,
          lastName: message.ln,
          pictureUrl: message.p,
          name: message.fn + ' ' + message.ln
        }
      };
      comments.push(comment);
      this.setState({ comments, commentCount: comments.length });
      this.props.liveCommentsCount(this.state.commentCount);
    }
  }

  getPubNubHistory(channel) {
    let componentThisRef = this;
    this.pubnub.history(
      {
        channel: channel,
        count: 100,
        stringifiedTimeToken: true
      },
      function(status, message) {
        let lastDeletedPubnubmsg = message.messages
          .map(function(item) {
            return item.a;
          })
          .lastIndexOf('deletedComment');
        if (lastDeletedPubnubmsg !== -1)
          this.setState({
            deletedComments: message['messages'][lastDeletedPubnubmsg].dc
          });

        for (let i = 0; i < message.messages.length; i++) {
          if (message.messages[i].entry.a === 'comment') {
            componentThisRef.onMessageReceive(
              message.messages[i].entry,
              new Date((+message.messages[i].timetoken / 10000000) * 1000).toString()
            );
          }
        }
      }
    );
  }

  viewMoreClickHandler = () => {
    let allComments = this.state.comments;
    this.setState({ comments: allComments, showAll: true });
  };

  updateCommentList = newComments => {
    this.setState({ comments: newComments, showAll: true }, () => {
      this.props.updateCommentCount();
      let element = document.getElementById('commentsContainer');
      element.scrollTop = element.scrollHeight;
    });
  };

  deleteComment = commentMsgId => {
    let deletedComments = this.state.deletedComments;
    deletedComments.push(commentMsgId);
    //push deleted comments list on pubnub
    var pubnubPayload = {};
    pubnubPayload['dc'] = deletedComments;
    pubnubPayload['a'] = 'deletedComment';
    //publish to pubnub
    this.pubnub.publish({
      channel: this.getUuid(),
      message: pubnubPayload,
      callback: m => {}
    });
    this.setState({ deletedComments });
  };

  render() {
    let comments = this.state.comments.sort((a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });
    if (this.state.showAll === false) {
      comments = comments.slice(-3);
    }
    let outerDivClasses = this.state.inModal ? 'comment-list' : '';
    let innerDivClasses = this.state.inModal
      ? 'inside-modal comment-list-container vertical-spacing-large'
      : 'comment-list-container vertical-spacing-large';
    return (
      <div className={outerDivClasses}>
        <div id="commentsContainer" className={innerDivClasses}>
          {this.state.comments.length > 3 && !this.state.showAll && (
            <div>
              <small>
                <a className="view-more-comments" onTouchTap={this.viewMoreClickHandler}>
                  {tr(`View %{count} more comments`, {count: this.state.comments.length - 3})}
                </a>
              </small>
            </div>
          )}
          {this.props.pending && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
          {comments.map((comment, index) => {
            return (
              <LiveCommentItem
                key={index}
                cardId={this.props.cardId}
                comment={comment}
                user={comment.user}
                isOwn={
                  comment.user.id === this.props.currentUser.id ||
                  this.props.cardOwner == this.props.currentUser.id ||
                  this.props.currentUser.isAdmin
                }
                commentPostCallback={this.props.updateCommentCount}
                deleteComment={this.deleteComment}
                isCanLike={Permissions.has('LIKE_CONTENT')}
              />
            );
          })}
          <LiveCommentInput
            cardId={this.props.cardId}
            cardType={this.props.cardType}
            autoFocus={false}
            numOfComments={this.state.comments.length}
            commentPostCallback={this.updateCommentList}
            status={this.getStatus()}
            cardUuid={this.getUuid()}
            isCardV3={this.props.isCardV3}
          />
        </div>
      </div>
    );
  }
}

LiveCommentList.propTypes = {
  currentUser: PropTypes.object,
  cardId: PropTypes.string,
  cardOwner: PropTypes.any,
  cardType: PropTypes.string,
  cardUuid: PropTypes.string,
  pending: PropTypes.bool,
  users: PropTypes.object,
  comments: PropTypes.array,
  inModal: PropTypes.bool,
  hideCommentModal: PropTypes.bool,
  updateCommentCount: PropTypes.func,
  liveCommentsCount: PropTypes.func,
  cardStatus: PropTypes.func,
  showConfirmationModal: PropTypes.func,
  onMessageReceiveJoinLeave: PropTypes.func,
  isCardV3: PropTypes.bool
};

LiveCommentList.defaultProps = {
  hideCommentModal: false,
  showConfirmationModal: function() {}
};

function mapStoreStateToProps(state) {
  return {
    users: state.users.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LiveCommentList);
