import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BlankStateInterest from './BlankStateInterest';
import { fetchUserLearningInterest } from '../../../actions/todayLearningActions';
import { newSuggestedTopics } from '../../../actions/onboardingActions';
import { feed } from 'edc-web-sdk/requests/index';
import uniqBy from 'lodash/uniqBy';
import ListView from '../../common/ListView';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../../cards/Card';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import getCardView from '../../../utils/getCardView';
import { Permissions } from '../../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';

let count = 0;

class TodayLearningContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.fetchCards = this.fetchCards.bind(this);
    this.updateTopicsClickHandler = this.updateTopicsClickHandler.bind(this);
    this.state = {
      cards: [],
      filteredCard: [],
      limit: 9,
      pending: true,
      topics: [],
      expertiseTopics: [],
      isLastLearningTopicPage: false,
      isLastExpertiseTopicPage: false,
      loaded: false,
      skills: {},
      expertiseSkills: {},
      error: false,
      learningCardView: window.ldclient.variation('learning-card', false),
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
    calculateTeemFeedPosition();
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';

    if (newView !== this.state.viewRegime) {
      this.props
        .dispatch(fetchUserLearningInterest(this.props.currentUser.id))
        .then(() => {
          this.setState(
            { userInterest: this.props.todayLearning.userInterest, viewRegime: newView },
            () => {
              if (this.props.todayLearning.userInterest) {
                this.checkAvailability();
              } else {
                if (this.props.todayLearning.expertiseTopics.length == 0) {
                  this.props.dispatch(newSuggestedTopics());
                }
                this.setState({ pending: false, isLastPage: true });
              }
            }
          );
        })
        .catch(err => {
          console.error(`Error in TodayLearningContainer.fetchUserLearningInterest.func : ${err}`);
        });
    }

    this.setState({
      topics: nextProps.todayLearning.topics,
      skills: nextProps.todayLearning.skills ? nextProps.todayLearning.skills : {},
      isLastLearningTopicPage: nextProps.isLastLearningTopicPage
    });
    if (this.state.loaded) {
      calculateTeemFeedPosition();
      this.checkCardList();
    }
  }

  checkCardList() {
    let cardList = this.state.cards.filter(card => !card.featuredHidden);
    this.setState({
      filteredCard: cardList,
      pending: false,
      loaded: true
    });
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      ['followingChannels', 'rolesDefaultNames', 'roles', 'first_name', 'last_name'],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    this.props
      .dispatch(fetchUserLearningInterest(this.props.currentUser.id))
      .then(() => {
        this.setState({ userInterest: this.props.todayLearning.userInterest }, () => {
          calculateTeemFeedPosition();
          if (this.props.todayLearning.userInterest) {
            this.checkAvailability();
          } else {
            if (this.props.todayLearning.expertiseTopics.length == 0) {
              this.props.dispatch(newSuggestedTopics());
            }
            this.setState({ pending: false, isLastPage: true });
          }
        });
      })
      .catch(err => {
        console.error(`Error in TodayLearningContainer.fetchUserLearningInterest.func : ${err}`);
      });
  }

  checkAvailability = () => {
    feed
      .getTodayLearningItemsAvailible()
      .then(response => {
        if (response.state !== 'completed') {
          count++;
          if (count > 5) {
            this.setState({ error: true, pending: false });
          } else {
            setTimeout(() => {
              this.checkAvailability();
            }, 5000);
          }
        } else {
          this.fetchCards();
        }
      })
      .catch(error => {
        this.setState({ error: true });
      });
  };

  fetchCards = () => {
    this.setState({ loaded: false }, calculateTeemFeedPosition);
    feed
      .getTodayLearningItems({
        limit: this.state.limit,
        offset: this.state.cards.length,
        filter_by_language: this.state.multilingualContent
      })
      .then(data => {
        this.setState(
          { cards: uniqBy(data.cards, 'id') },
          () => {
            this.checkCardList();
          },
          calculateTeemFeedPosition
        );
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(error => {
        this.setState({ error: true }, calculateTeemFeedPosition);
      });
  };

  updateTopicsClickHandler(topics) {
    this.setState({ pending: true, userInterest: topics }, calculateTeemFeedPosition);
    this.checkAvailability();
  }

  removeCardFromList = id => {
    let newCardsList = this.state.filteredCard.filter(card => {
      return card.id !== id;
    });
    this.setState({ filteredCard: newCardsList });
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
        setActivityListHeight(this.state.homepageVersion, 'card');
      }
    }
  }

  render() {
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);
    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');
    return (
      <div id="todayLearning">
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          loadingMessage={'Loading...please wait. '}
        >
          <div
            className={
              viewRegime == 'Tile'
                ? 'custom-card-container align-tileview-feed'
                : 'custom-card-container'
            }
          >
            <div className="five-card-column vertical-spacing-medium">
              {this.state.userInterest &&
                this.state.filteredCard.map(card => {
                  return (
                    <Card
                      key={card.id}
                      card={card}
                      author={card.author}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      userInterest={this.state.userInterest}
                      dismissible={true}
                      removeCardFromList={this.removeCardFromList}
                      type={viewRegime}
                      providerLogos={this.state.providerLogos}
                      moreCards={viewRegime === 'List' ? false : !isRail}
                      user={this.props.currentUser}
                      fullView={true}
                      showComment={isShowComment}
                      isStandalone={false}
                    />
                  );
                })}
            </div>
          </div>
          {!this.state.pending && this.state.userInterest && this.state.filteredCard.length == 0 && (
            <div className="container-padding vertical-spacing-medium">
              <div className="text-center vertical-spacing-large">
                <div className="empty-message data-not-available-msg">
                  {this.state.error
                    ? tr('We will be back after 24 hours')
                    : tr('We will be back after 24 hours')}
                </div>
              </div>
            </div>
          )}
          {!this.state.pending && !this.state.userInterest && (
            <BlankStateInterest
              user={this.props.currentUser}
              suggestedTopics={this.state.topics}
              skills={this.state.skills}
              isLastTopicPage={this.state.isLastLearningTopicPage}
              updateTopicsClickHandler={this.updateTopicsClickHandler}
            />
          )}
        </ListView>
      </div>
    );
  }
}

TodayLearningContainer.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  todayLearning: PropTypes.object,
  route: PropTypes.object,
  isLastLearningTopicPage: PropTypes.bool,
  cards: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS(),
    team: state.team.toJS(),
    todayLearning: state.todayLearning.toJS()
  };
}

export default connect(mapStoreStateToProps)(TodayLearningContainer);
