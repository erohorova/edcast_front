import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BlankStateInterest from './BlankStateInterest';
import { newSuggestedTopics } from '../../../actions/onboardingActions';
import { feed } from 'edc-web-sdk/requests/index';
import _ from 'lodash';
import ListView from '../../common/ListView';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../../cards/Card';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import getCardView from '../../../utils/getCardView';
import { Permissions } from '../../../utils/checkPermissions';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';

export class TodayLearningContainerSearch extends Component {
  constructor(props, context) {
    super(props, context);
    this.fetchCards = this.fetchCards.bind(this);
    this.updateTopicsClickHandler = this.updateTopicsClickHandler.bind(this);
    this.state = {
      cards: [],
      filteredCard: [],
      limit: 9,
      pending: true,
      topics: [],
      expertiseTopics: [],
      isLastLearningTopicPage: false,
      isLastExpertiseTopicPage: false,
      loaded: false,
      skills: {},
      expertiseSkills: {},
      error: false,
      learningCardView: window.ldclient.variation('learning-card', false),
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';

    if (newView !== this.state.viewRegime) {
      let setStateObject = { viewRegime: newView };
      if (this.props.currentUser.profile && this.props.currentUser.profile.learningTopics) {
        this.checkAvailability();
      } else {
        if (this.props.todayLearning.expertiseTopics.length == 0) {
          this.props.dispatch(newSuggestedTopics());
        }
        setStateObject['pending'] = false;
        setStateObject['isLastPage'] = true;
      }
      this.setState(setStateObject);
    }

    if (
      this.props.todayLearning.topics != nextProps.topics ||
      this.props.todayLearning.skills != nextProps.todayLearning.skills ||
      this.props.isLastLearningTopicPage != nextProps.isLastLearningTopicPage
    ) {
      this.setState(
        {
          topics: nextProps.todayLearning.topics,
          skills: nextProps.todayLearning.skills ? nextProps.todayLearning.skills : {},
          isLastLearningTopicPage: nextProps.isLastLearningTopicPage
        },
        calculateTeemFeedPosition
      );
      if (this.state.loaded) {
        this.checkCardList();
      }
    }
  }

  checkCardList() {
    let cardList = this.state.cards.filter(card => !card.featuredHidden);
    this.setState(
      {
        filteredCard: cardList,
        pending: false,
        loaded: true
      },
      calculateTeemFeedPosition
    );
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    calculateTeemFeedPosition();
    let setStateObject = {
      userInterest:
        (this.props.currentUser.profile && this.props.currentUser.profile.learningTopics) || []
    };
    if (this.props.currentUser.profile && this.props.currentUser.profile.learningTopics) {
      this.checkAvailability();
    } else {
      if (this.props.todayLearning.expertiseTopics.length == 0) {
        this.props.dispatch(newSuggestedTopics());
      }
      setStateObject['pending'] = false;
      setStateObject['isLastPage'] = true;
    }
    this.setState(setStateObject);
    calculateTeemFeedPosition();
    setActivityListHeight(this.state.homepageVersion, 'card');
  }

  checkAvailability = () => {
    this.fetchCards();
  };

  fetchCards = () => {
    this.setState({ loaded: false });
    feed
      .getSearchTodayLearningItems({
        limit: this.state.limit,
        offset: this.state.cards.length,
        filter_by_language: this.state.multilingualContent
      })
      .then(data => {
        this.setState({ cards: _.uniqBy(data.cards, 'id') }, () => {
          this.checkCardList();
        });
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(error => {
        this.setState({ error: true });
      });
  };

  updateTopicsClickHandler(topics) {
    this.setState({ pending: true, userInterest: topics }, calculateTeemFeedPosition);
    this.checkAvailability();
    this.getStream();
    setActivityListHeight(this.state.homepageVersion, 'card');
  }

  removeCardFromList = id => {
    let newCardsList = this.state.filteredCard.filter(card => {
      return card.id !== id;
    });
    this.setState({ filteredCard: newCardsList }, calculateTeemFeedPosition);
  };

  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let filteredCard = this.state.filteredCard;
        let findCardIndex = _.findIndex(filteredCard, item => {
          return item.id == id;
        });
        filteredCard[findCardIndex] = data;
        this.setState({ filteredCard }, calculateTeemFeedPosition);
      })
      .catch(err => {
        console.error(
          `Error in TodayLearningContainerSearch.findCheckedCardAtList.fetchCard.func : ${err}`
        );
      });
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(25, true, 'card'));
      }
    }
  }

  render() {
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);
    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');
    return (
      <div id="todayLearning">
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          loadingMessage={'Loading...please wait. '}
        >
          <div
            className={
              viewRegime == 'Tile'
                ? 'custom-card-container align-tileview-feed'
                : 'custom-card-container'
            }
          >
            <div className="five-card-column vertical-spacing-medium">
              {this.state.userInterest &&
                this.state.filteredCard.map(card => {
                  return (
                    <Card
                      key={card.id}
                      card={card}
                      author={card.author}
                      userInterest={this.state.userInterest}
                      dismissible={true}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      removeCardFromList={this.removeCardFromList}
                      closeCardModal={this.findCheckedCardAtList}
                      type={viewRegime}
                      providerLogos={this.state.providerLogos}
                      moreCards={viewRegime === 'List' ? false : !isRail}
                      user={this.props.currentUser}
                      fullView={true}
                      showComment={isShowComment}
                      isStandalone={false}
                    />
                  );
                })}
            </div>
          </div>

          {!this.state.pending && this.state.userInterest && this.state.filteredCard.length == 0 && (
            <div className="container-padding vertical-spacing-medium">
              <div className="text-center vertical-spacing-large">
                <div className="empty-message data-not-available-msg">
                  {this.state.error
                    ? tr('We will be back after 24 hours')
                    : tr('We will be back after 24 hours')}
                </div>
              </div>
            </div>
          )}
          {!this.state.pending && !this.state.userInterest && (
            <BlankStateInterest
              user={this.props.currentUser}
              suggestedTopics={this.state.topics}
              skills={this.state.skills}
              isLastTopicPage={this.state.isLastLearningTopicPage}
              updateTopicsClickHandler={this.updateTopicsClickHandler}
            />
          )}
        </ListView>
      </div>
    );
  }
}

TodayLearningContainerSearch.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  route: PropTypes.object,
  todayLearning: PropTypes.object,
  isLastLearningTopicPage: PropTypes.bool,
  cards: PropTypes.object,
  topics: PropTypes.array
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS(),
    team: state.team.toJS(),
    todayLearning: state.todayLearning.toJS()
  };
}

export default connect(mapStoreStateToProps)(TodayLearningContainerSearch);
