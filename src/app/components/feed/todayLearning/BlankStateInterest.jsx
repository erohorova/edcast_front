import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { postLearningDomainTopics } from '../../../actions/onboardingActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import uniqBy from 'lodash/uniqBy';
import FlashAlert from '../../common/FlashAlert';
import AutoComplete from 'material-ui/AutoComplete';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import { topics } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';

class BlankStateInterest extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      arrayList: props.suggestedTopics
        ? this.divideTopics(uniqBy(props.suggestedTopics, 'id'))
        : [],
      topics: uniqBy(props.suggestedTopics, 'id'),
      skills: props.skills ? props.skills : [],
      page: 1,
      isLastTopicPage: props.isLastTopicPage,
      learningDomainTopics: [],
      showAlert: false,
      activeTopicId: '',
      skillSource: [],
      learningTopics: [],
      searchText: '',
      errorMessage: ''
    };

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };

    this.divideTopics = this.divideTopics.bind(this);
    this.doneClickHandler = this.doneClickHandler.bind(this);
    this.selectTopicSkill = this.selectTopicSkill.bind(this);
    this.findTopicLabel = this.findTopicLabel.bind(this);
    this.updateActiveTopicId = this.updateActiveTopicId.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      skills: nextProps.skills,
      arrayList: nextProps.suggestedTopics
        ? this.divideTopics(uniqBy(nextProps.suggestedTopics, 'id'))
        : [],
      topics: uniqBy(nextProps.suggestedTopics, 'id'),
      isLastTopicPage: nextProps.isLastTopicPage
    });
  }

  updateActiveTopicId(topicId) {
    this.setState({ activeTopicId: topicId });
  }

  findTopicLabel(topicId, paramRequired) {
    let topicsArray = this.state.arrayList;
    let label = '';
    for (let i = 0; i < topicsArray.length; i++) {
      topicsArray[i].map(topic => {
        if (topic.id === topicId) {
          label = paramRequired == 'name' ? topic.name : topic.label;
        }
      });
    }
    return label;
  }

  selectTopicSkill(skillId, skillName, skillLabel, topicId, action) {
    let selectedLearningDomainTopics = this.state.learningDomainTopics;
    if (action == 'store') {
      selectedLearningDomainTopics.push({
        topic_id: skillId,
        topic_name: skillName,
        topic_label: skillLabel,
        domain_id: topicId,
        domain_name: this.findTopicLabel(topicId, 'name'),
        domain_label: this.findTopicLabel(topicId, 'label')
      });
    } else {
      for (let i = 0; i < selectedLearningDomainTopics.length; i++) {
        if (selectedLearningDomainTopics[i]['topic_id'] == skillId) {
          selectedLearningDomainTopics.splice(i, 1);
        }
      }
    }
    if (selectedLearningDomainTopics.length > 3) {
      this.setState({ showAlert: true });
    } else {
      this.setState({ showAlert: false });
    }

    this.setState({ learningDomainTopics: selectedLearningDomainTopics });
  }

  doneClickHandler() {
    if (this.state.learningTopics && this.state.learningTopics.length) {
      this.props.dispatch(
        postLearningDomainTopics(
          this.state.learningTopics,
          this.props.user.id,
          this.props.user.profile ? this.props.user.profile.id : null
        )
      );
      this.props.updateTopicsClickHandler(this.state.learningTopics);
    }
  }

  isInt = n => {
    return n % 1 === 0;
  };

  divideTopics = topicsParam => {
    let length = topicsParam.length;
    let countItem = 0;
    let finalTopics = [];
    let finalArrayLength = this.isInt(length / 4)
      ? Math.floor(length / 4)
      : Math.floor(length / 4) + 1;

    for (let i = 0; i < finalArrayLength; i++) {
      finalTopics[i] = topicsParam.splice(0, 4);
    }

    return finalTopics;
  };

  handleSkillInput = (value = '') => {
    this.setState({
      searchText: value
    });
    topics
      .queryTopics(value)
      .then(topicsParam => {
        this.setState({
          skillSource: topicsParam.topics
        });
      })
      .catch(err => {
        console.error(`Error in BlankStateInterest.queryTopics.func : ${err}`);
      });
  };

  selectSkillInput = skill => {
    let learningTopics = this.state.learningTopics;
    let filteredTopics = learningTopics.filter(function(obj) {
      return skill.id == obj.topic_id;
    });
    if (learningTopics.length > 2) {
      this.showAlert(
        'You can select a maximum of three topics. Don’t worry you can always change them from your profile page.'
      );
      this.setState({
        searchText: ''
      });
      return;
    }
    if (filteredTopics.length > 0) {
      this.showAlert('The topic is already added!');
      this.setState(
        {
          searchText: ''
        },
        function() {
          this.handleSkillInput();
        }
      );
      return;
    }
    let learningTopic = {
      domain_id: skill.domain.id,
      domain_label: skill.domain.label,
      domain_name: skill.domain.name,
      topic_id: skill.id,
      topic_label: skill.label,
      topic_name: skill.name
    };
    learningTopics.push(learningTopic);
    this.setState(
      {
        learningTopics,
        searchText: ''
      },
      function() {
        this.handleSkillInput();
      }
    );
  };

  removeTopicHandler(id) {
    let learningTopics = this.state.learningTopics;
    learningTopics = learningTopics.filter(function(obj) {
      return obj.topic_id !== id;
    });
    this.setState({
      learningTopics
    });
  }

  showAlert(message) {
    this.setState({ showAlert: true, errorMessage: message });
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  render() {
    let _this = this;
    return (
      <div id="blankState" className="vertical-spacing-large onboarding-page-style">
        {this.state.showAlert && (
          <FlashAlert
            message={tr(this.state.errorMessage)}
            toShow={this.state.showAlert}
            removeAlert={this.removeAlert}
          />
        )}
        <div className="blank-container">
          <div className="text-center">
            <h5 className="blank-title">{tr('Choose Topics')}</h5>
            <p className="blank-sub-title">
              {this.props.user.name},{' '}
              {tr('Choose up to three topics you are interested in and get a personalized feed.')}
            </p>
          </div>
          <div>
            <div>
              <div className="row skill-input-row">
                <div className="small-12 columns text-center">
                  <div style={this.styles.chipsWrapper}>
                    {this.state.learningTopics.map((skill, idx) => {
                      return (
                        <Chip
                          key={idx}
                          style={this.styles.chip}
                          backgroundColor={colors.primary}
                          onRequestDelete={this.removeTopicHandler.bind(_this, skill.topic_id)}
                        >
                          <small style={{ color: colors.white }} />{' '}
                          <small style={{ color: colors.white }}>{skill.topic_label}</small>
                        </Chip>
                      );
                    })}
                  </div>
                </div>
              </div>
              <div className="row skill-input-row">
                <div className="small-12 columns text-center">
                  <div>
                    <AutoComplete
                      popoverProps={{
                        style: {
                          bottom: '0.7rem',
                          overflowY: 'auto',
                          background: 'transparent',
                          boxShadow: 'none'
                        },
                        className: 'auto-complete-list'
                      }}
                      menuStyle={{ background: 'white' }}
                      hintText={tr('What would you like to learn?')}
                      filter={AutoComplete.caseInsensitiveFilter}
                      dataSource={this.state.skillSource}
                      onUpdateInput={this.handleSkillInput}
                      fullWidth={true}
                      dataSourceConfig={{ text: 'label', value: 'name' }}
                      onNewRequest={this.selectSkillInput}
                      searchText={this.state.searchText}
                      maxSearchResults={20}
                    />
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="small-12 text-center">
                  {/*<div className="view-more-link" onClick={this.ViewMoreTopics}><span>View More</span></div>*/}
                  {this.state.showAlert && <div>{tr('Please select a max of 3 topics')}</div>}
                  <PrimaryButton
                    label={tr('Done')}
                    disabled={!this.state.learningTopics.length}
                    className="done-button create"
                    onTouchTap={this.doneClickHandler}
                    style={{ margin: '8px 0', padding: '0.1rem 0.3rem' }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BlankStateInterest.propTypes = {
  skills: PropTypes.object,
  suggestedTopics: PropTypes.object,
  isLastTopicPage: PropTypes.bool,
  user: PropTypes.object,
  updateTopicsClickHandler: PropTypes.func
};

export default connect()(BlankStateInterest);
