import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListItem from 'material-ui/List/ListItem';
import { Avatar } from 'material-ui';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import colors from 'edc-web-sdk/components/colors/index';
import extractMentions from '../../utils/extractMentions';
import TimeAgo from 'react-timeago';
import translateTA from '../../utils/translateTimeAgo';
import ConfirmationModal from '../modals/ConfirmationCommentModal';

// Material UI Icons
import KeyboardArrowDownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';

//actions
import { toggleLikeComment } from '../../actions/cardsActions';
import Linkify from 'react-linkify';
import * as modalActions from '../../actions/modalActions';

class LiveCommentItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isEditing: false,
      openConfirm: false,
      upVoted: this.props.comment.isUpvoted,
      votesCount: this.props.comment.votesCount || 0,
      visible: true
    };
    this.likeClickHandler = this.likeClickHandler.bind(this);
    this.styles = {
      listItem: {
        padding: '0 56px 0 72px',
        margin: '0 -1rem'
      },
      commentText: {
        fontSize: '0.75rem',
        color: '#26273b',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      },
      linkText: {
        fontSize: '0.75rem',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      }
    };
  }

  likeClickHandler() {
    toggleLikeComment(this.props.cardId, this.props.comment.id, !this.state.upVoted)
      .then(data => {
        this.setState({
          upVoted: !this.state.upVoted,
          votesCount: data.upvoted ? this.state.votesCount + 1 : this.state.votesCount - 1
        });
      })
      .catch(err => {
        console.error(`Error in LiveCommentItem.toggleLikeComment.func : ${err}`);
      });
  }

  updateState = () => {
    this.setState({ visible: false }, () => {
      this.props.commentPostCallback(-1);
    });
  };

  confirmDelete = () => {
    this.setState({ openConfirm: false });
    if (this.props.comment.msgId !== undefined) {
      this.props.deleteComment(this.props.comment.msgId);
    }
  };

  toggleModal = () => {
    this.setState({ openConfirm: !this.state.openConfirm });
  };

  render() {
    if (!this.state.visible) {
      return null;
    }
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);
    let user = this.props.user;
    let message = extractMentions(this.props.comment.message);
    let deleteDropdown = null;
    if (this.props.isOwn) {
      deleteDropdown = (
        <IconMenu
          iconButtonElement={
            <IconButton style={{ marginTop: '-12px' }}>
              <KeyboardArrowDownIcon color={colors.silverSand} />
            </IconButton>
          }
        >
          <MenuItem className="delete" onTouchTap={this.toggleModal}>
            <small>Delete</small>
          </MenuItem>
        </IconMenu>
      );
    }
    return (
      <div>
        <ListItem
          disabled
          style={this.styles.listItem}
          leftAvatar={<Avatar src={user.pictureUrl} />}
          primaryText={
            <div>
              <small>
                <strong>
                  <a className="matte user comment-list-user" href={'/' + user.handle}>{`${
                    user.firstName
                  } ${user.lastName}`}</a>
                </strong>
                <span> &#x25cf; </span>
                <TimeAgo
                  date={new Date(this.props.comment.createdAt)}
                  style={this.styles.commentText}
                  live={false}
                  formatter={formatter}
                />
              </small>
            </div>
          }
          rightIconButton={deleteDropdown}
        />
        <ListItem
          disabled
          insetChildren={true}
          style={this.styles.listItem}
          primaryText={
            <small>
              {message.map((part, index) => {
                if (part.type === 'text') {
                  return (
                    <span key={index} style={this.styles.commentText}>
                      <Linkify properties={{ target: '_blank' }}>{part.text}</Linkify>
                    </span>
                  );
                }
                if (part.type === 'handle') {
                  return (
                    <span key={index}>
                      <a style={this.styles.commentText} href={`/${part.handle}`}>
                        {part.handle}
                      </a>
                    </span>
                  );
                }
              })}
            </small>
          }
        />
        {this.props.comment.id && this.props.isCanLike && (
          <ListItem
            disabled
            insetChildren={true}
            style={this.styles.listItem}
            primaryText={
              <div>
                <small>
                  <a
                    className="like"
                    style={this.styles.linkText}
                    onTouchTap={this.likeClickHandler}
                  >
                    {this.state.upVoted ? 'Unlike' : 'Like'}
                  </a>
                </small>
                <small> {this.state.votesCount || ''}</small>
              </div>
            }
          />
        )}
        {this.state.openConfirm && (
          <ConfirmationModal
            title={'Confirm'}
            message={'Do you want to delete the comment?'}
            closeModal={this.toggleModal}
            callback={this.confirmDelete}
          />
        )}
      </div>
    );
  }
}

LiveCommentItem.propTypes = {
  cardId: PropTypes.string,
  users: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  isOwn: PropTypes.bool,
  isCanLike: PropTypes.bool,
  commentPostCallback: PropTypes.func,
  deleteComment: PropTypes.func,
  comment: PropTypes.shape({
    message: PropTypes.string,
    votesCount: PropTypes.string,
    isUpvoted: PropTypes.bool,
    userId: PropTypes.string,
    createdAt: PropTypes.string,
    id: PropTypes.string,
    msgId: PropTypes.string
  })
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LiveCommentItem);
