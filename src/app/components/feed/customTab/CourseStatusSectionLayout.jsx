import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import CourseCard from '../../cards/CourseCard';

function CourseStatusSectionLayout(props) {
  let cardCountainerClass = '';

  switch (props.numberOfCards) {
    case 3:
      cardCountainerClass = 'three-course-card';
      break;
    case 4:
      cardCountainerClass = 'four-course-card';
      break;
    case 5:
      cardCountainerClass = 'five-course-card';
      break;
    default:
      cardCountainerClass = 'five-course-card';
      break;
  }

  return (
    <div className="row new-course-card-layout-container">
      <div className="course-status-section">
        {props.courseStatus.id}
        {props.courses.length >= props.numberOfCards && (
          <div
            onClick={e => {
              props.launchCourseModal(props.courseStatus.id);
            }}
            className="view-all"
          >
            {tr('View all')}
          </div>
        )}
      </div>
      <div className={'course-status-section-container ' + cardCountainerClass}>
        {props.courses.map((course, i) => {
          return <CourseCard key={i} courseType={course.source_type_name} course={course} />;
        })}
      </div>
    </div>
  );
}

CourseStatusSectionLayout.propTypes = {
  courseStatus: PropTypes.object,
  courses: PropTypes.any,
  launchCourseModal: PropTypes.function,
  numberOfCards: PropTypes.integer
};

export default CourseStatusSectionLayout;
