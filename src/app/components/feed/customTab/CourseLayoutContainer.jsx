import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CourseCard from '../../cards/CourseCard';
import { getUserSabaCourses } from 'edc-web-sdk/requests/users.v2';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CourseStatusSectionLayout from './CourseStatusSectionLayout';
import CircularProgress from 'material-ui/CircularProgress';
import colors from 'edc-web-sdk/components/colors/index';
import { openCourseModal } from '../../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class CourseLayoutContainer extends Component {
  constructor(props, context) {
    super(props, context);
    let leftRailObj = props.team && props.team.OrgConfig && props.team.OrgConfig.leftRail;

    let isLeftRailVisible = !!(
      (leftRailObj['web/leftRail/announcements'] &&
        leftRailObj['web/leftRail/announcements'].visible) ||
      (leftRailObj['web/leftRail/myLearningQueue'] &&
        leftRailObj['web/leftRail/myLearningQueue'].visible) ||
      (leftRailObj['web/leftRail/teamActivity'] && leftRailObj['web/leftRail/teamActivity'].visible)
    );
    this.state = {
      courses: [],
      aggregations: [],
      selectedOption: 'all',
      loading: true,
      totalCourses: 0,
      isLeftRailVisible: !!(
        isLeftRailVisible && window.ldclient.variation('homepage-version', 'right-sidebar')
      ),
      isLeftMLPVisible: window.ldclient.variation('left-user-panel', false)
    };

    if (this.state.isLeftRailVisible && this.state.isLeftMLPVisible) {
      this.state['numberOfCards'] = 3;
    } else {
      if (this.state.isLeftRailVisible ^ this.state.isLeftMLPVisible) {
        this.state['numberOfCards'] = 4;
      } else {
        this.state['numberOfCards'] = 5;
      }
    }

    this.styles = {
      langDropDown: {
        textAlign: 'center',
        width: '128px',
        height: '30px',
        verticalAlign: 'top',
        backgroundColor: '#d6d6e1',
        boxShadow: '1px 2px 4px 0 rgba(0, 0, 0, 0.24)'
      },
      labelStyleSelect: {
        fontSize: '12px',
        lineHeight: '38px',
        paddingRight: '18px'
      },
      iconSelect: {
        height: '30px',
        width: '30px',
        padding: 0,
        fill: '#6f708b'
      },
      menuItemStyle: {
        fontSize: '12px'
      }
    };
  }

  componentDidMount() {
    let payload = {
      course_type: 'all',
      per_page: this.state.numberOfCards
    };
    let page = 1;
    this.fetchCourses(page, payload);
  }

  getUserCourses = (page, payload) => {
    return new Promise(resolve => {
      getUserSabaCourses(this.props.currentUser.id, page, payload)
        .then(result => {
          this.setState({
            aggregations: result.aggregations,
            totalCourses: result.total_items
          });
          resolve(result.data);
        })
        .catch(err => {
          console.error(`Error in CourseLayoutContainer.getUserSabaCourses.func : ${err}`);
        });
    });
  };

  async fetchCourses(page, payload) {
    this.setState({ loading: true });
    let courses = {};
    courses['Started'] = await this.getUserCourses(page, { ...payload, ...{ status: 'Started' } });
    for (let el of this.state.aggregations) {
      if (!courses[el.id] && el.type == 'status') {
        courses[el.id] = await this.getUserCourses(page, { ...payload, ...{ status: el.id } });
      }
    }
    this.setState({
      courses,
      loading: false
    });
  }

  courseTypeSelectHandler = courseType => {
    this.setState({
      selectedOption: courseType
    });
    this.fetchCourses(1, { course_type: courseType, per_page: this.state.numberOfCards });
  };

  launchCourseModal = courseStatus => {
    let courseType = this.state.selectedOption;
    this.props.dispatch(openCourseModal(courseStatus, courseType));
  };

  render() {
    let sectionCounter = 0;
    return (
      <div className="new-course-layout-container">
        {this.state.loading ? (
          <div className="text-center">
            <CircularProgress
              size={80}
              color={colors.primary}
              thickness={5}
              className="course-loader"
            />
            <div>Loading...</div>
          </div>
        ) : (
          <div>
            {!!this.state.aggregations.length && (
              <div className="drop-down-container">
                <div style={{ display: 'inline-block', margin: '2px 10px 0px 0px' }}>
                  Course Type :{' '}
                </div>
                <SelectField
                  value={this.state.selectedOption}
                  style={this.styles.langDropDown}
                  maxHeight={250}
                  underlineStyle={{ display: 'none' }}
                  labelStyle={this.styles.labelStyleSelect}
                  iconStyle={this.styles.iconSelect}
                  menuItemStyle={this.styles.menuItemStyle}
                >
                  <MenuItem
                    key={-1}
                    value={'all'}
                    innerDivStyle={{ padding: '0 8px' }}
                    primaryText={'All'}
                    onTouchTap={this.courseTypeSelectHandler.bind(this, 'all')}
                  />
                  {this.state.aggregations.map((courseType, index) => {
                    if (courseType.type == 'course_type') {
                      return (
                        <MenuItem
                          key={index}
                          value={courseType.id}
                          innerDivStyle={{ padding: '0 8px' }}
                          primaryText={courseType.id}
                          onTouchTap={this.courseTypeSelectHandler.bind(this, courseType.id)}
                        />
                      );
                    }
                  })}
                </SelectField>
              </div>
            )}
            {this.state.totalCourses ? (
              <div>
                {this.state.aggregations.map((courseStatus, index) => {
                  if (courseStatus.type == 'status') {
                    if (
                      this.state.courses &&
                      this.state.courses[courseStatus.id] &&
                      this.state.courses[courseStatus.id].length > 0
                    ) {
                      sectionCounter++;
                      return (
                        <CourseStatusSectionLayout
                          key={index}
                          courseStatus={courseStatus}
                          courses={this.state.courses[courseStatus.id]}
                          launchCourseModal={this.launchCourseModal}
                          numberOfCards={this.state.numberOfCards}
                        />
                      );
                    }
                  }
                })}
                {sectionCounter == 0 && (
                  <div>
                    <div className="text-center empty-course">
                      <p>{tr('There are no available cards.')}</p>
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <div className="text-center empty-course">
                <p>{tr('There are no available cards.')}</p>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

CourseLayoutContainer.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS()
}))(CourseLayoutContainer);
