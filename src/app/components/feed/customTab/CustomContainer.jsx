import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Card from '../../cards/Card';
import CustomListCard from '../../cards/CustomListCard';
import ListView from '../../common/ListView';
import { getProviderLogos } from 'edc-web-sdk/requests/ecl';
import { fetchCard, getCards } from 'edc-web-sdk/requests/cards';
import { getUserSabaCourses, getCustomPath } from 'edc-web-sdk/requests/users.v2';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../../actions/teamActivityAction';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';
import getCardView from '../../../utils/getCardView';
import { Permissions } from '../../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../../utils/calculateTeemFeedPosition';
import updatePageLastVisit from '../../../utils/updatePageLastVisit';
import CourseLayoutContainer from './CourseLayoutContainer';
import { tr } from 'edc-web-sdk/helpers/translations';
import { cardFields } from '../../../constants/cardFields';

class CustomContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: 9,
      columns: this.props.route.columns,
      url: this.props.route.url,
      cards: [],
      coursesAggregations: [],
      filterType: 'all',
      courseType: 'all',
      filterOptions: [],
      secondFilterOptions: [],
      pending: true,
      sabaCoursesPage: 0,
      isLastSabaPage: false,
      isLastPage: false,
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      showLmsCourseTypes: window.ldclient.variation('show-lms-course-types', false),
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      courseLayoutV2: window.ldclient.variation('course-layout-v2', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };

    this.handleScroll = this.handleScroll.bind(this);

    if (this.state.showNew) {
      updatePageLastVisit('customLastVisit');
    }
  }

  async componentWillMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    calculateTeemFeedPosition();
    getProviderLogos()
      .then(providerLogos => {
        this.setState({
          providerLogos: providerLogos
        });
      })
      .catch(err => {
        console.error(`Error in CustomContainer.getProviderLogos.func : ${err}`);
      });
    this.initData();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    this.state.showNew &&
      localStorage.setItem('customLastVisit', Math.round(new Date().getTime() / 1000));
  }

  fetchCourses() {
    let query = ~this.state.url.indexOf('?') ? this.state.url.split('?')[1].split('&') : false;
    let params = query
      ? query.reduce((p, h) => {
          let [key, val] = h.split('=');
          return Object.assign(p, { [key]: decodeURIComponent(val) });
        }, {})
      : {};

    let page = this.state.sabaCoursesPage + 1;
    if (!this.state.isLastSabaPage) {
      params['status'] = this.state.filterType;
      params['course_type'] = this.state.courseType;
      params['per_page'] = this.state.limit;
      getUserSabaCourses(this.props.currentUser.id, page, params)
        .then(result => {
          // let cards = this.state.cards.concat([...data]);
          this.setCourseStatuses(result);
          this.setState(
            {
              cards: [...this.state.cards, ...result.data],
              pending: false,
              isLastSabaPage: result.data.length < this.state.limit ? true : false,
              sabaCoursesPage: page
            },
            calculateTeemFeedPosition
          );
          setActivityListHeight(this.state.homepageVersion, 'card');
        })
        .catch(err => {
          console.error(`Error in CustomContainer.getUserSabaCourses.func : ${err}`);
        });
    } else {
      this.setState({ pending: false });
    }
  }

  capitalize(str) {
    return str.replace(/\b\w/g, w => {
      return w.toUpperCase();
    });
  }

  setCourseStatuses(result) {
    let courseStatuses = [{ value: 'all', text: `All Course Status (${result.total_items})` }];
    let courseTypes = [{ value: 'all', text: `All Training Types` }];

    result &&
      result.aggregations &&
      result.aggregations.forEach(record => {
        if (record.type === 'status') {
          courseStatuses.push({
            value: record.id,
            text: `${this.capitalize(record.display_name)} (${record.count})`
          });
        }
        if (record.type === 'course_type') {
          courseTypes.push({
            value: record.id,
            text: `${this.capitalize(record.display_name)} (${record.count})`
          });
        }
      });

    this.setState({
      filterOptions: courseStatuses,
      secondFilterOptions: this.state.showLmsCourseTypes ? courseTypes : []
    });
  }

  fetchDefault() {
    getCards({
      limit: this.state.limit,
      is_official: true,
      offset: this.state.cards.length,
      filter_by_language: this.state.multilingualContent,
      last_access_at: localStorage.getItem('customLastVisit'),
      fields: cardFields
    })
      .then(data => {
        let isLastPage = data.length < this.state.limit;
        this.setState(
          {
            cards: [...this.state.cards, ...data],
            isLastPage,
            pending: false
          },
          calculateTeemFeedPosition
        );
        if (!isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in CustomContainer.getCards.func : ${err}`);
      });
  }

  fetchCustom() {
    getCustomPath(this.state.url, { limit: this.state.limit, offset: this.state.cards.length })
      .then(data => {
        let isLastPage = data.length < this.state.limit;
        if (data && data.cards.length) {
          this.setState(
            {
              cards: [...this.state.cards, ...data.cards],
              isLastPage,
              pending: false
            },
            calculateTeemFeedPosition
          );
        } else {
          this.setState({
            pending: false,
            isLastPage: true
          });
        }
        setActivityListHeight(this.state.homepageVersion, 'card');
        /*eslint handle-callback-err: "off"*/
      })
      .catch(err => {
        this.setState({
          pending: false,
          isLastPage: true
        });
        if (!this.state.isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
      });
  }

  initData() {
    if (/Courses/.test(this.state.url) && !this.state.courseLayoutV2) {
      this.fetchCourses();
    } else if (this.state.url.startsWith('/')) {
      this.fetchCustom();
    } else {
      this.fetchDefault();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.route.path != this.props.route.path) {
      this.setState(
        {
          cards: [],
          columns: nextProps.route.columns,
          url: nextProps.route.url,
          pending: true,
          sabaCoursesPage: 0,
          isLastSabaPage: false,
          isLastPage: false
        },
        () => {
          calculateTeemFeedPosition();
          this.initData();
        }
      );
    }
  }

  handleScroll = throttle(
    event => {
      if (
        !(this.props.route.url !== 'Courses' || !this.state.courseLayoutV2) ||
        this.state.pending
      ) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage && !this.state.isLastSabaPage) {
          this.loadMore();
          this.getStream();
        }
      }
    },
    150,
    { leading: false }
  );

  loadMore() {
    if (!this.state.isLastPage && !this.state.isLastSabaPage) {
      this.setState(
        {
          pending: true
        },
        () => {
          if (/Courses/.test(this.state.url)) {
            this.fetchCourses();
          } else {
            this.initData();
          }
        }
      );
    }
  }

  changeCourseStatus = filterValue => {
    this.setState(
      {
        cards: [],
        filterType: filterValue,
        courseType: 'all',
        sabaCoursesPage: 0,
        isLastSabaPage: false,
        pending: true
      },
      () => {
        calculateTeemFeedPosition();
        this.fetchCourses();
      }
    );
  };

  changeCourseType = courseType => {
    this.setState(
      {
        cards: [],
        courseType: courseType,
        sabaCoursesPage: 0,
        isLastSabaPage: false,
        pending: true
      },
      () => {
        calculateTeemFeedPosition();
        this.fetchCourses();
      }
    );
  };

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => card.id !== id);
    this.setState({ cards: newCardsList }, calculateTeemFeedPosition);
    setActivityListHeight(this.state.homepageVersion, 'card');
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
      }
    }
  }

  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let cards = this.state.cards;
        let findCardIndex = findIndex(cards, item => {
          return item.id == id;
        });
        cards[findCardIndex] = data;
        this.setState({ cards }, calculateTeemFeedPosition);
      })
      .catch(err => {
        console.error(`Error in CustomContainer.findCheckedCardAtList.fetchCard.func : ${err}`);
      });
  };

  render() {
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);
    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');
    return (
      <div>
        {this.props.route.url !== 'Courses' || !this.state.courseLayoutV2 ? (
          <ListView
            paginationRef="referForPagination"
            pending={this.state.pending}
            filterValue={this.state.filterType}
            secondFilterValue={this.state.courseType}
            filterOptions={/Courses/.test(this.state.url) ? this.state.filterOptions : []}
            secondFilterOptions={
              /Courses/.test(this.state.url) ? this.state.secondFilterOptions : []
            }
            loadingMessage="Loading..."
            handleFilterChange={this.changeCourseStatus}
            handleSecondFilterChange={this.changeCourseType}
          >
            <div
              className={
                viewRegime == 'Tile'
                  ? 'custom-card-container align-tileview-feed'
                  : 'custom-card-container'
              }
            >
              <div className="five-card-column vertical-spacing-medium">
                {this.state.columns === 3 &&
                  this.state.cards.map(card => {
                    return (
                      <Card
                        card={card}
                        author={card.author}
                        key={card.id}
                        dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                        startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                        removeCardFromList={this.removeCardFromList}
                        closeCardModal={this.findCheckedCardAtList}
                        type={viewRegime}
                        providerLogos={this.state.providerLogos}
                        dismissible={false}
                        moreCards={viewRegime === 'List' ? false : !isRail}
                        user={this.props.currentUser}
                        fullView={true}
                        showComment={isShowComment}
                        isStandalone={false}
                      />
                    );
                  })}
                {this.state.columns === 2 &&
                  this.state.cards.map(card => {
                    return <CustomListCard card={card} key={card.id} />;
                  })}
              </div>
            </div>
            {!this.state.pending && this.state.cards.length == 0 && (
              <div className="container-padding vertical-spacing-medium custom-result">
                <div className="text-center vertical-spacing-large">
                  <div className="empty-message">
                    {tr('There is no content available based on the criteria you selected.')}
                  </div>
                </div>
              </div>
            )}
          </ListView>
        ) : (
          <CourseLayoutContainer />
        )}
      </div>
    );
  }
}

CustomContainer.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  route: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS()
}))(CustomContainer);
