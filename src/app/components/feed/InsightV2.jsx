import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
import ReactStars from 'react-stars';
import Sidebar from 'react-sidebar';
import moment from 'moment';
import _ from 'lodash';
import PDF from 'react-pdf-js';

import { CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton/IconButton';
import FeaturedIcon from 'material-ui/svg-icons/toggle/star';
import ArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import CheckedIcon from 'material-ui/svg-icons/action/done';
import colors from 'edc-web-sdk/components/colors/index';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import CompletedAssignment from 'edc-web-sdk/components/icons/CompletedAssignmentGrey';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import Downloadv2 from 'edc-web-sdk/components/icons/Downloadv2';
import FullTimeAccess from 'edc-web-sdk/components/icons/FullTimeAccess';
import Support from 'edc-web-sdk/components/icons/Support';
import WebandMobile from 'edc-web-sdk/components/icons/WebandMobile';
import Certificate from 'edc-web-sdk/components/icons/Certificate';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';

import {
  markAsComplete,
  markAsUncomplete,
  initiateCardPurchase,
  completedCardPurchase,
  cancelCardPurchase
} from 'edc-web-sdk/requests/cards.v2';
import {
  fetchCardForStandaloneLayout,
  rateCard,
  submitProjectCard
} from 'edc-web-sdk/requests/cards';

import VideoStream from './VideoStream';
import InsightDropDownActions from './InsightDropDownActions';
import CommentList from './CommentList';
import LiveCommentList from './LiveCommentList';
import PathwayCover from './PathwayCover';
import InlineVideo from './InlineVideo.jsx';
import Poll from './Poll';

import {
  loadComments,
  toggleLikeCardAsync,
  toggleBookmarkCardAsync
} from '../../actions/cardsActions';
import {
  openCardStatsModal,
  openStatusModal,
  openCardWalletPaymentModal,
  openPayPalSuccessModal,
  confirmation
} from '../../actions/modalActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { submitRatings, updateRatedQueueAfterRating } from '../../actions/relevancyRatings';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';

import addTabInCardRating from '../../utils/addTabInCardRating';

import MarkdownRenderer from '../common/MarkdownRenderer';
import CreationDate from '../common/CreationDate';
import BreadcrumbV2 from '../common/BreadcrumpV2';
import BlurImage from '../common/BlurImage';
import SmartBiteInfoCommentsBlock from '../common/SmartBiteInfoCommentsBlock';
import DateConverter from '../common/DateConverter';
import RecommendedBlock from '../common/RecommendedBlock';
import Spinner from '../common/spinner';
import SvgImageResized from '../common/ImageResized';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import checkResources from '../../utils/checkResources';
import getDefaultImage from '../../utils/getDefaultCardImage';
import getCardParams from '../../utils/getCardParams';
import convertRichText from '../../utils/convertRichText';
import getTranscodedVideo from '../../utils/getTranscodedVideo';
import linkPrefix from '../../utils/linkPrefix';
import checkResourceURL from '../../utils/checkResourceURL';
import thumbnailImage from '../../utils/thumbnailImage';

import CardClassesModal from '../modals/CardClassesModal';

import * as logoType from '../../constants/logoTypes';
import 'url-search-params-polyfill';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import VideoIcon from 'edc-web-sdk/components/icons/Video';
import { filestackClient } from '../../utils/filestack';
import {
  videoTranscode,
  refreshFilestackUrl,
  uploadPolicyAndSignature
} from 'edc-web-sdk/requests/filestack';
import TextFieldCustom from '../common/SmartBiteInput';
import { fetchSumissionList } from 'edc-web-sdk/requests/submission';
import Avatar from 'material-ui/Avatar';
import * as upshotActions from '../../actions/upshotActions';
import SmartBiteLevel from '../common/SmartBiteLevel';
import { parseUrl } from '../../utils/urlParser';
import pubnubAdapter from '../../utils/pubnubAdapter';
import pdfPreviewUrl from '../../utils/previewPdf';

let LocaleCurrency = require('locale-currency');
let GetVideoId = require('get-video-id');

const logoObj = logoType.LOGO;
const lightPurp = '#acadc1';
const videoType = ['video/mp4', 'video/ogg', 'video/webm', 'video/quicktime', 'video/ogv'];

class InsightV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.pubnub = pubnubAdapter;

    this.state = {
      showComment: false,
      comments: [],
      providerLogos: {},
      pendingLike: false,
      pendingBookmark: false,
      truncateTitle: !props.isStandalone
        ? props.card.title && props.card.title.length > 260
        : false,
      truncateMessage: !props.isStandalone
        ? props.card.message && props.card.message.length > 260
        : false,
      channelTooltip: false,
      dismissed: false,
      commentsCount: props.card.commentsCount,
      averageRating: props.card.averageRating,
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      configureCompleteButton: window.ldclient.variation(
        'configurable-colors-for-complete-button',
        false
      ),
      isCompleted:
        props.card.completionState && props.card.completionState.toUpperCase() === 'COMPLETED',
      isLiveStream: false,
      viewsCount: 0,
      liveUsers: [],
      viewersCount: 0,
      liveCommentsCount: 0,
      relevancyLevel: props.card.skillLevel,
      ratingCount: 0,
      sidebarOpen: false,
      courseDefaultImage: getDefaultImage(this.props.currentUser.id).url,
      showChannelsTooltip: false,
      showTagsTooltip: false,
      card: this.props.card,
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      showNewDesignForCompleteButton: window.ldclient.variation('complete-button-v-2', false),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      wallet: this.props.team.config.wallet || false,
      cardClickHandle: window.ldclient.variation('card-click-handle', 'modal'),
      modalClassesOpen: false,
      customConfig:
        (this.props.team.config.lxpCustomCSS &&
          this.props.team.config.lxpCustomCSS.configs &&
          this.props.team.config.lxpCustomCSS.configs.header) ||
        {},
      configurableHeader: window.ldclient.variation('new-configurable-header', false),
      showRecommendedContent: window.ldclient.variation('winterstorm', false),
      isOpenBlock: { cards: false, users: false },
      prevPath: localStorage.getItem('prevPath'),
      recommendedCards: [],
      recommendedUsers: [],
      showProjectCard: window.ldclient.variation('project-card', false),
      fileStack: [],
      createLabel: 'Submit',
      description: '',
      disableSubmit: true,
      optionsError: '',
      submissionList: [],
      cardPurchased: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      showLoader: false,
      isV3: window.ldclient.variation('card-v3', false),
      currentUserChannelIds: this.props.currentUser.followingChannels
        ? this.props.currentUser.followingChannels.map(c => c.id)
        : [],
      disableCompleteBtn: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true),
      markAsCompleteEnableForLink: false,
      videoSubmitted: false
    };
    this.styles = {
      menuItem: {
        fontSize: '14px'
      },
      priceAndBia: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'baseline'
      },
      cardHeader: {
        padding: '2rem',
        paddingBottom: 0
      },
      labelStyle: {
        border: '1px #ccc solid',
        boxShadow: 'none',
        borderRadius: 0,
        height: '30px',
        lineHeight: '30px',
        display: 'inline-block',
        padding: '0px 12px',
        fontWeight: '500'
      },
      btnIcons: {
        verticalAlign: 'middle',
        width: '1rem',
        height: '0.875rem',
        padding: 0
      },
      likeButton: {
        height: '25px',
        width: '25px'
      },
      commentButton: {
        height: '27px',
        width: '25px'
      },
      completedButton: {
        height: '23px',
        width: '23px'
      },
      statisticsButton: {
        height: '18px',
        width: '23px'
      },
      tooltipActiveBts: {
        marginTop: -32
      },
      commentsCount: {
        verticalAlign: 'middle',
        display: 'inline-block'
      },
      authorStyle: {
        borderLeft: 'none',
        margin: '0',
        paddingLeft: '0'
      },
      downloadButton: {
        width: '100%',
        height: '100%',
        padding: 0,
        background: '#454560',
        cursor: 'pointer'
      },
      tooltipStyles: {
        left: '-50%'
      },
      ratingBlock: {
        display: 'inline-flex',
        paddingLeft: '1rem'
      },
      separator: {
        borderLeft: '2px solid #b3b3b3'
      },
      imgStyle: {
        float: 'left',
        marginRight: '20px'
      },
      resourceImg: {
        width: '400px',
        float: 'left',
        marginTop: '1rem',
        marginRight: '1rem'
      },
      arrowRightIcon: {
        color: '#6f708b',
        cursor: 'pointer',
        height: '20px'
      },
      arrowLeftIcon: {
        color: '#6f708b',
        cursor: 'pointer',
        height: '20px'
      },
      svgImage: {
        zIndex: 2,
        position: 'relative'
      },
      svgStyle: {
        filter: 'url(#blur-effect-1)'
      },
      labelInfoStyle: {
        fontSize: '14px',
        color: '#26273b',
        fontWeight: 'normal'
      },
      audio: {
        width: '100%'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.5rem',
        width: '2.5rem',
        marginRight: '1rem',
        position: 'relative'
      },
      cardStatisticsBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginRight: '1rem'
      },
      commentatorAvatars: {
        marginLeft: '-0.3125rem',
        width: '1.25rem',
        height: '1.25rem',
        border: '0.0625rem solid #ffffff',
        borderRadius: '50%'
      },
      accordionTitleIcon: {
        fill: 'inherit',
        height: '2.125rem',
        width: '2.125rem'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      },
      buyWithSkillCoins: {
        width: '190px',
        margin: '15px 0px',
        fontSize: '13px',
        fontFamily: 'Helvetica',
        height: '35px',
        borderRadius: '5px'
      },
      buyNow: {
        width: '190px',
        fontSize: '13px',
        fontFamily: 'Helvetica',
        height: '35px',
        borderRadius: '5px',
        backgroundColor: '#6f708b',
        cursor: 'pointer'
      },
      addToCart: {
        width: '190px',
        fontSize: '13px',
        fontFamily: 'Helvetica',
        height: '35px',
        borderRadius: '5px',
        border: 'solid 2px #6f708b'
      },
      cardMarkBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginRight: '0'
      },
      paymentLoader: {
        margin: '0 auto'
      },
      videoIcon: {
        width: '34px',
        height: '34px',
        marginTop: '-5px'
      },
      submissionList: {
        fontSize: '0.7em',
        color: lightPurp
      },
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      btnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      }
    };

    this.stylesSidebar = {
      content: {
        overflow: 'hidden'
      },
      sidebar: {
        backgroundColor: '#f0f0f5',
        width: '177px',
        overflowX: 'hidden'
      }
    };

    this.standaloneCardFilePreview = window.ldclient.variation(
      'standalone-card-file-preview',
      false
    );
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
    this.isCsodCourse = props.card.course_id && props.card.source_type_name === 'csod';
    this.defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    this.showBIA =
      this.props.team && this.props.team.config && !!this.props.team.config.enabled_bia;
    this.rateCard = this.rateCard.bind(this);

    this.cardLikeHandler = this.cardLikeHandler.bind(this);
    this.bookmarkClickHandler = this.bookmarkClickHandler.bind(this);
    this.handleCardAnalayticsModal = this.handleCardAnalayticsModal.bind(this);
    this.completeClickHandler = this.completeClickHandler.bind(this);
    this.truncateMessageText = this.truncateMessageText.bind(this);
    this.linkExtract = this.linkExtract.bind(this);
    this._metricRecorder = this._metricRecorder.bind(this);
    this.checkVisible = this.checkVisible.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    this.onMessageReceiveJoinLeave = this.onMessageReceiveJoinLeave.bind(this);
    this.componentCleanup = this.componentCleanup.bind(this);
    //this.getPubNubHistory = this.getPubNubHistory.bind(this);
    this.openCardWalletModal = this.openCardWalletModal.bind(this);
    this.openPayPalPayment = this.openPayPalPayment.bind(this);
    this.cardPurchased = this.cardPurchased.bind(this);
    this.visibleChannelList = this.visibleChannelList.bind(this);
  }

  async componentWillMount() {
    getTranscodedVideo.call(
      this,
      this.state.card,
      this.state.card,
      this.props.currentUser.id,
      false
    );
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(['handle'], this.props.currentUser);
    this.props
      .dispatch(userInfoCallBack)
      .then(userData => {
        let urlParamsExtractor = new URLSearchParams(window.location.search);
        // should get called only for project card
        if (this.state.showProjectCard && this.props.card && this.props.card.projectId) {
          this.fetchSubmissionList();
        }

        if (urlParamsExtractor.getAll('paymentId').length > 0) {
          let payPalPayLoad = {
            payment_id: urlParamsExtractor.getAll('paymentId')[0],
            payer_id: urlParamsExtractor.getAll('PayerID')[0],
            token: urlParamsExtractor.getAll('token')[0]
          };
          let payload = {
            paypal_data: payPalPayLoad,
            token: urlParamsExtractor.getAll('app_token')[0],
            gateway: 'paypal'
          };
          completedCardPurchase(payload)
            .then(data => {
              let paypalData = {
                card: this.props.card,
                redirectUrl: data.transactionDetails.redirectUrl,
                cardPurchased: true
              };
              this.props.dispatch(openPayPalSuccessModal(paypalData));
            })
            .catch(error => {
              this.props.dispatch(openSnackBar('Something went wrong! Please try again', true));
              window.open(window.location.origin + window.location.pathname, '_self');
            });
        } else if (
          urlParamsExtractor.getAll('paymentId').length <= 0 &&
          urlParamsExtractor.getAll('token').length > 0 &&
          urlParamsExtractor.getAll('cancelled') == 'true'
        ) {
          let payload = {
            token: urlParamsExtractor.getAll('app_token')[0]
          };
          cancelCardPurchase(payload)
            .then(data => {
              this.props.dispatch(openSnackBar('The payment has been cancelled', true));
            })
            .catch(err1 => {
              console.error('Error in canceling payment');
            });
        }

        window.addEventListener('beforeunload', this.componentCleanup);
        if (this.isCsodCourse) return;
        let cardType;

        if (this.props.showComment) {
          let cardId = this.state.card.cardId || this.state.card.id;
          let isECL = /^ECL-/.test(cardId);
          if (!isECL && this.state.card.commentsCount) {
            loadComments(
              cardId,
              this.state.card.commentsCount,
              this.state.card.cardType,
              this.state.card.cardId ? this.state.card.id : null
            )
              .then(data => {
                this.setState({ comments: data, showComment: true });
              })
              .catch(err => {
                console.error(`Error in InsightV2.loadComments.func : ${err}`);
              });
          } else {
            this.setState({ comments: [], showComment: true });
          }
        }
        switch (this.state.card.cardType) {
          case 'VideoStream' || 'video_stream':
            cardType = 'video_stream';
            break;
          case 'pack':
            cardType = 'collection';
            break;
          default:
            cardType = 'card';
        }
        if (this.state.card.cardType === 'video_stream') {
          let isLiveStream = false;
          if (this.state.card && this.state.card.videoStream !== undefined) {
            isLiveStream = this.state.card.videoStream.status === 'live';
          } else if (this.state.card && this.state.card.status) {
            isLiveStream = this.state.card.status === 'live';
          }
          this.setState({ isLiveStream });
          if (isLiveStream) {
            this.postActionOnPubNub('join', userData);
          } else if (
            this.state.card &&
            this.state.card.videoStream !== undefined &&
            this.state.card.videoStream.status === 'past'
          ) {
            this.setState({ viewersCount: this.state.card.videoStream.watchers });
          }
        }
        this.allCountRating(this.state.card);
        this._metricRecorder(cardType);
        window.addEventListener('scroll', this._metricRecorder);
      })
      .catch(err => {
        console.error(`Error in InsightV2.getSpecificUserInfo.func: ${err}`);
      });
    let that = this;
    setTimeout(() => {
      addTabInCardRating(that.ratingChanged);
    }, 500);
  }

  componentWillUnmount() {
    this.postActionOnPubNub('leave');
    window.removeEventListener('scroll', this._metricRecorder);
    this.videoTranscodeInterval && clearInterval(this.videoTranscodeInterval);
    this.videoTranscodeEditInterval && clearInterval(this.videoTranscodeEditInterval);
    this.componentCleanup();
    window.removeEventListener('beforeunload', this.componentCleanup);
  }
  componentCleanup() {
    // whatever you want to do when the component is unmounted or page refreshes
    this.postActionOnPubNub('leave');
  }

  fetchSubmissionList = () => {
    fetchSumissionList({ project_id: this.props.card.projectId, filter: 'all' })
      .then(subList => {
        if (subList) this.setState({ submissionList: subList.projectSubmissions });
      })
      .catch(err => {
        console.error(`Error in InsightV2.fetchSubmissionList.func : ${err}`);
      });
  };

  postActionOnPubNub = (action, currentUserObj = null) => {
    //prepare payload
    if (this.state.card.videoStream !== undefined) {
      let pubnubPayload = {};
      pubnubPayload['uid'] = this.props.currentUser.id || '';
      pubnubPayload['p'] = this.props.currentUser.picture || '';
      pubnubPayload['fn'] = this.props.currentUser.name.split(' ')[0] || '';
      pubnubPayload['ln'] = this.props.currentUser.name.split(' ')[1] || '';
      pubnubPayload['h'] =
        this.props.currentUser.handle || (currentUserObj && currentUserObj.handle) || '';
      pubnubPayload['a'] = action;
      pubnubPayload['type'] = 'videoStream';
      //publish to pubnub
      this.pubnub.publish({
        channel: this.getCardUuid(),
        message: pubnubPayload
      });
    }
  };

  onMessageReceiveJoinLeave = m => {
    let liveUsers = this.state.liveUsers;
    let removeIndex = liveUsers.indexOf(m.uid);
    if (m.a == 'join' && removeIndex === -1) {
      liveUsers.push(m.uid);
    } else if (m.a == 'leave' && removeIndex !== -1) {
      liveUsers.splice(removeIndex, 1);
    }
    this.setState({ liveUsers });
    this.setState({ viewsCount: liveUsers.length });
  };

  _metricRecorder = cardType => {
    if (this._insight && this.checkVisible(this._insight)) {
      window.removeEventListener('scroll', this._metricRecorder);
    }
  };

  rateCard(level) {
    rateCard(this.state.card.id, { level: level })
      .then(data_rating => {
        this.cardUpdated();
      })
      .catch(err => {
        console.error(`Error in InsightV2.rateCard.func : ${err}`);
      });
  }

  checkVisible = elm => {
    let rect = elm.getBoundingClientRect();
    let viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
  };

  cardUpdated(card) {
    if (!this.state.card) {
      return;
    }

    let cardobj;
    if (this.props.pathname.indexOf('/channel/') !== -1) {
      let routeParam = this.props.pathname.split('/');
      let slug = routeParam[routeParam.length - 1];
      let findChannel = _.find(this.props.channels, el => {
        return el.slug == slug;
      });
      if (findChannel) {
        let existChannel = _.find(card.channel_ids, el => {
          return el == findChannel.id;
        });
        if (!existChannel) {
          this.hideInsight();
        }
      }
    }
    if (card) {
      cardobj = card.card;
    } else {
      cardobj = this.state.card;
    }

    if (
      cardobj &&
      cardobj.filestack &&
      cardobj.filestack.length &&
      cardobj.filestack[0].mimetype &&
      cardobj.filestack[0].mimetype.includes('video')
    ) {
      getTranscodedVideo.call(this, this.state.card, cardobj, this.props.currentUser.id, true);
    } else {
      fetchCardForStandaloneLayout(this.state.card.id)
        .then(data => {
          this.setState({
            card: data
          });
        })
        .catch(err => {
          console.error(`Error in InsightV2.fetchCard.func : ${err}`);
        });
    }
  }

  handleCardAnalayticsModal() {
    this.props.dispatch(openCardStatsModal(this.state.card));
  }

  visibleChannelList(cardChannels) {
    let currentUserChannels = this.state.currentUserChannelIds;
    let visibleChannels = [];
    cardChannels.map(channel => {
      if (channel.isPrivate) {
        currentUserChannels.indexOf(channel.id) !== -1 && visibleChannels.push(channel);
      } else {
        visibleChannels.push(channel);
      }
    });
    return visibleChannels;
  }

  setStatePending = (state, val) => {
    return new Promise((resolve, reject) => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise((resolve, reject) => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in InsightV2.asyncDispatch.func : ${err}`);
        });
    });
  };

  async cardLikeHandler() {
    if (this.state.pendingLike) {
      return;
    }
    await this.setStatePending('pendingLike', true);
    await this.asyncDispatch(
      toggleLikeCardAsync,
      this.state.card.cardId || this.state.card.id,
      this.state.card.cardType,
      !this.state.card.isUpvoted
    )
      .then(() => {
        if (this.state.card.isUpvoted) {
          _.remove(this.state.card.voters, voter => {
            return voter.id == this.props.currentUser.id;
          });
        } else {
          let newLike = {
            id: this.props.currentUser.id,
            name: this.props.currentUser.name,
            handle: this.props.currentUser.handle,
            avatar: this.props.currentUser.avatar
          };
          if (this.state.isLiveStream) {
            this.postActionOnPubNub('like');
          }
          this.state.card.voters.push(newLike);
        }
        this.state.card.isUpvoted = !this.state.card.isUpvoted;
        if (this.state.upshotEnabled) {
          var name;
          if (this.state.card.resource && this.state.card.resource.title) {
            name = this.state.card.resource.title;
          } else {
            name = this.state.card.message;
          }
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
            name: name,
            category: this.state.card.cardType,
            type: this.state.card.cardSubtype,
            description: this.state.card.resource.description,
            status: !!this.state.card.completionState
              ? this.state.card.completionState
              : 'Incomplete',
            rating: this.state.card.averageRating,
            like: this.state.card.isUpvoted ? 'Yes' : 'No',
            event: this.state.card.isUpvoted ? 'Clicked Like' : 'Clicked Unlike'
          });
        }
        this.state.card.votesCount = Math.max(
          this.state.card.votesCount + (this.state.card.isUpvoted ? 1 : -1),
          0
        );
      })
      .catch(err => {
        console.error(`Error in InsightV2.toggleLikeCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingLike', false);
  }

  async bookmarkClickHandler() {
    let bookmarkSnackMessage = 'Done! This SmartCard has been moved to your My Learning Plan.';
    let unbookmarkSnackMessage = 'Removed from your My Learning Plan';
    let snackMessage = this.state.card.isBookmarked ? unbookmarkSnackMessage : bookmarkSnackMessage;
    if (this.state.pendingBookmark) {
      return;
    }
    await this.setStatePending('pendingBookmark', true);
    await this.asyncDispatch(
      toggleBookmarkCardAsync,
      this.state.card.id,
      this.state.card.cardType,
      !this.state.card.isBookmarked
    )
      .then(() => {
        this.state.card.isBookmarked = !this.state.card.isBookmarked;
      })
      .catch(err => {
        console.error(`Error in InsightV2.toggleBookmarkCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingBookmark', false);
    await this.props.dispatch(openSnackBar(snackMessage, true));
  }

  hideInsight = () => {
    this.state.card.featuredHidden = true;
    this.setState({ dismissed: true });
  };

  completeClickHandler() {
    let cardType;
    switch (this.state.card.cardType) {
      case 'VideoStream':
        cardType = 'video_stream';
        break;
      default:
        cardType = 'card';
        break;
    }

    let markFeatureDisabledForSource = this.state.card.markFeatureDisabledForSource;
    let isPack = this.state.card.cardType === 'pack' || this.state.card.cardType === 'journey';
    if (
      (!isPack && !this.state.isCompleted) ||
      (isPack &&
        this.state.card.state === 'published' &&
        +this.state.card.completedPercentage === 98)
    ) {
      this.setState({ disableCompleteBtn: true });
      markAsComplete(this.state.card.id, { state: 'complete' })
        .then(() => {
          this.setState({ isCompleted: true, disableCompleteBtn: false }, () => {
            if (this.state.newModalAndToast) {
              let message = `You have completed this ${
                this.state.card.cardType === 'pack'
                  ? 'Pathway'
                  : this.state.card.cardType === 'journey'
                  ? 'Journey'
                  : 'SmartCard'
              } and can track it in completion history under Me tab`;
              this.props.dispatch(openSnackBar(message, true));
            } else {
              let compoundMessage = [
                'You have completed this task. You can view it under',
                '/me/content/completed',
                'Me',
                'Tab'
              ];
              this.props.dispatch(
                openStatusModal(
                  '',
                  () => {
                    // getting if this is a standlone insight page & redirecting to the previous link.
                  },
                  compoundMessage
                )
              );
            }
          });
          this.state.card.featuredHidden = true;
          if (this.props.hideComplete !== undefined) {
            this.props.hideComplete(this.state.card.id);
          }
          if (this.state.upshotEnabled) {
            var name;
            if (this.state.card.resource && this.state.card.resource.title) {
              name = this.state.card.resource.title;
            } else {
              name = this.state.card.message;
            }
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: this.state.card.cardType,
              type: this.state.card.cardSubtype,
              description: this.state.card.resource.description,
              status: 'COMPLETED',
              rating: this.state.card.averageRating,
              like: this.state.isUpvoted ? 'Yes' : 'No',
              event: 'Mark Completed'
            });
          }
        })
        .catch(err => {
          console.error(`Error in InsightV2.markAsComplete.func : ${err}`);
        });
    } else if (
      (!isPack && this.state.isCompleted) ||
      (isPack && +this.state.card.completedPercentage === 100)
    ) {
      if (!markFeatureDisabledForSource) {
        this.props.dispatch(
          confirmation(
            'Please note',
            'Marking the card as incomplete will not change the Continuous Learning hour or the score associated with it.',
            () => {
              this.setState({ disableCompleteBtn: true });
              markAsUncomplete(this.state.card.id)
                .then(() => {
                  this.cardUpdated();
                  this.setState(
                    {
                      isCompleted: false,
                      disableCompleteBtn: false,
                      markAsCompleteEnableForLink: false
                    },
                    () => {
                      setTimeout(() => {
                        if (this.state.newModalAndToast) {
                          this.props.dispatch(
                            openSnackBar('You have marked this SmartCard as incomplete', true)
                          );
                        } else {
                          this.props.dispatch(
                            openStatusModal('You have marked this task as incomplete')
                          );
                        }
                      }, 1500);
                    }
                  );
                  if (this.state.upshotEnabled) {
                    var name;
                    if (this.state.card.resource && this.state.card.resource.title) {
                      name = this.state.card.resource.title;
                    } else {
                      name = this.state.card.message;
                    }
                    upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
                      name: name,
                      category: this.state.card.cardType,
                      type: this.state.card.cardSubtype,
                      description: this.state.card.resource.description,
                      status: !!this.state.card.completionState
                        ? this.state.card.completionState
                        : 'Incomplete',
                      rating: this.state.card.averageRating,
                      like: this.state.isUpvoted ? 'Yes' : 'No',
                      event: 'Mark Incomplete'
                    });
                  }
                })
                .catch(err => {
                  console.error(`Error in InsightV2.markAsUncomplete.func : ${err}`);
                });
            }
          )
        );
      }
    } else {
      let msg =
        this.state.card.state === 'published'
          ? `Please complete all pending cards before marking the ${
              this.state.card.cardType === 'pack' ? 'Pathway' : 'Journey'
            } as complete.`
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  }

  standaloneLinkClickHandler = () => {
    let linkPrefixValue = linkPrefix(this.state.card.cardType);
    // If coming from modal search, remove overflow.
    // document.getElementsByTagName('body')[0].style.overflow = '';
    let cardType = linkPrefixValue === 'video_streams' ? 'insights' : linkPrefixValue;
    window.history.pushState(null, null, `/${linkPrefixValue}/${this.state.card.slug}`);
    this.props.dispatch(openStandaloneOverviewModal(this.state.card, cardType));
    if (this.props.toggleSearch) {
      this.props.toggleSearch();
    }
  };

  viewMoreTitleHandler = () => {
    this.setState({ truncateTitle: false });
  };

  viewMoreMessageHandler = () => {
    this.setState({ truncateMessage: false });
  };

  updateCommentCount = (count = 1) => {
    this.setState({ commentsCount: this.state.commentsCount + count });
  };

  truncateMessageText(message) {
    return message.substr(0, 260);
  }

  openUrl = url => {
    refreshFilestackUrl(url)
      .then(resp => {
        window.open(resp.signed_url, '_blank');
      })
      .catch(err => {
        console.error(`Error in InsightV2.openUrl.refreshFilestackUrl.func : ${err}`);
      });
  };

  downloadBlock(file) {
    return (
      <span className="roll">
        <a onClick={() => this.openUrl(file.url)} download={file.handle} target="_blank">
          <IconButton
            style={this.styles.downloadButton}
            tooltipPosition="bottom-center"
            tooltipStyles={this.styles.tooltipStyles}
            tooltip={tr('Download')}
            aria-label={tr('Download')}
            iconStyle={this.styles.btnIcons}
          >
            <Downloadv2 />
          </IconButton>
        </a>
      </span>
    );
  }

  cardViewProperty() {
    let cardType = this.state.card.cardType;
    let cardSubtype = this.state.card.cardSubtype;

    if (
      ['video_stream', 'pack', 'pack_draft'].indexOf(cardType) > -1 ||
      (cardType === 'media' && ['link', 'video'].indexOf(cardSubtype) > -1)
    ) {
      return this.props.isStandalone ? 'full' : 'summary';
    } else {
      return 'full';
    }
  }

  liveCommentsCount = commentCount => {
    this.setState({ liveCommentsCount: commentCount });
  };

  getCardStatus() {
    if (this.state.card && this.state.card.videoStream !== undefined) {
      return this.state.card.videoStream.status;
    } else if (this.state.card && this.state.card.status) {
      return this.state.card.status;
    } else {
      return '';
    }
  }

  getCardUuid() {
    if (this.state.card.videoStream !== undefined) {
      return this.state.card.videoStream.uuid;
    } else if (this.state.card.uuid !== undefined) {
      return this.state.card.uuid;
    } else {
      return '';
    }
  }

  linkExtract() {
    let url =
      typeof this.state.card.resource.url !== 'undefined'
        ? this.state.card.id
          ? `/insights/${encodeURIComponent(this.state.card.id)}/visit`
          : this.state.card.resource.url
        : `/insights/${this.state.card.id}`;
    return url;
  }

  ratingChanged = userRating => {
    this.setState({
      averageRating: userRating
    });
    let payload = {
      rating: userRating
    };

    submitRatings(this.state.card.id, payload)
      .then(response => {
        let card = this.state.card;
        card.averageRating = response.averageRating;
        this.setState({
          averageRating: response.averageRating,
          card
        });
        if (this.state.upshotEnabled) {
          var name;
          if (this.state.card.resource && this.state.card.resource.title) {
            name = this.state.card.resource.title;
          } else {
            name = this.state.card.message;
          }
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
            name: name,
            category: this.state.card.cardType,
            type: this.state.card.cardSubtype,
            description: this.state.card.resource.description,
            status: !!this.state.card.completionState
              ? this.state.card.completionState
              : 'Incomplete',
            rating: this.state.card.averageRating,
            userRating: userRating,
            like: this.state.isUpvoted ? 'Yes' : 'No',
            event: 'User Rated Card'
          });
        }
        this.allCountRating(response);
      }, this)
      .catch(err => {
        console.error(`Error in InsightV2.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.state.card));
  };

  allCountRating(card) {
    if (!card.allRatings) {
      return;
    }

    if (typeof card.allRatings === 'number') {
      this.setState({ ratingCount: card.allRatings });
    }

    if (typeof card.allRatings === 'object') {
      let ratingCount = 0;
      for (let i = 1; i < 6; i++) {
        ratingCount += card.allRatings[i] ? card.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
    }
  }

  onDocumentComplete = pages => {
    this.setState({ page: 1, pages });
  };

  onPageComplete = page => {
    this.setState({ page });
  };

  handlePrevious = () => {
    if (this.state.page > 1) {
      this.setState({ page: this.state.page - 1 });
    }
  };

  handleNext = () => {
    if (this.state.page < this.state.pages) {
      this.setState({ page: this.state.page + 1 });
    }
  };

  viewPagesPDF = () => {
    let page = [];
    for (let i = 1; i <= this.state.pages; i++) {
      page[i] = i;
    }
    return page;
  };

  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }

  toggleSidbare() {
    this.setState({ sidebarOpen: !this.state.sidebarOpen });
  }

  showTooltip = name => {
    this.setState({
      [name]: true
    });
  };

  hideTooltip = name => {
    this.setState({
      [name]: false
    });
  };

  checkComplete = (card, isAnswer) => {
    if (isAnswer) {
      markAsComplete(card.id, { state: 'complete' });
      this.setState({ isCompleted: true });
    }
  };

  handleResourceClicked = e => {
    e.preventDefault();
    if (this.state.card && this.state.card.resource && this.state.card.resource.url) {
      window.open(
        this.state.card.resource.url.includes('/api/scorm/')
          ? this.state.card.resource.url
          : this.linkExtract(),
        '_blank'
      );
      this.setState({
        markAsCompleteEnableForLink: true
      });
    }
  };

  handleResourceUrlClicked = (e, url) => {
    e.preventDefault();
    if (url) {
      window.open(url, '_blank');
      this.setState({
        markAsCompleteEnableForLink: true
      });
    }
  };

  imageContainer = (img, key, isArticle) => {
    let opts = {
      className: `anchorAlignment image-container ${
        isArticle ? 'anchorAlignment image-container__article' : ''
      }`,
      onClick: e => this.handleResourceClicked(e),
      href: '#'
    };
    if (key) {
      opts['key'] = `img-container-${key}`;
    }
    return (
      <a {...opts}>
        <div
          style={this.styles.imgStyle}
          className="pathway-image card-blurred-background fp fp_img"
        >
          <svg id="svg-image-blur" width="100%" height="100%">
            <title>
              <RichTextReadOnly text={convertRichText(this.props.card.message)} />
            </title>
            <SvgImageResized
              cardId={`${this.props.card.id}`}
              resizeOptions={'height:505'}
              id="svg-image"
              style={this.styles.svgStyle}
              xlinkHref={img}
              x="-30%"
              y="-30%"
              width="160%"
              height="160%"
            />
            <filter id="blur-effect-1">
              <feGaussianBlur stdDeviation="10" />
            </filter>
          </svg>
        </div>
        <svg width="100%" height="100%" style={this.styles.svgImage}>
          <title>
            {' '}
            <RichTextReadOnly text={convertRichText(this.props.card.message)} />
          </title>
          <SvgImageResized
            cardId={`${this.props.card.id}`}
            resizeOptions={'height:505'}
            xlinkHref={img}
            width="100%"
            style={this.styles.svgImage}
            height="100%"
          />
        </svg>
      </a>
    );
  };

  commentatorAvatars = () => {
    let commentList = this.state.comments;
    let unique;
    let unicalTrio;
    if (commentList) {
      unique = _.uniqBy(commentList, 'user.id');
      unicalTrio = unique.splice(0, 3);
    }
    if (unicalTrio.length === 0) {
      return;
    } else {
      return unicalTrio.map(item => {
        return (
          <img
            key={`unicalTrio-${item.user.id}`}
            src={item.user.avatarimages.tiny}
            style={this.styles.commentatorAvatars}
          />
        );
      });
    }
  };

  commentsNumber = isLive => {
    let count = isLive ? this.state.liveCommentsCount : this.state.commentsCount;
    return (
      <small className="count count_comments">
        {count ? abbreviateNumber(count) : ''}&nbsp;
        {count > 1 ? tr('Comments') : count == 1 ? tr('Comment') : ''}
      </small>
    );
  };

  embedUrl(url) {
    let sources = ['vimeo', 'youtube'];
    let embed_url = url;
    let json_data = GetVideoId(url);

    if (json_data && json_data.id && !!~sources.indexOf(json_data.service)) {
      if (json_data.service == 'vimeo') {
        embed_url = 'https://player.vimeo.com/video/' + json_data.id;
      } else if (json_data.service == 'youtube') {
        embed_url = '//www.youtube.com/embed/' + json_data.id;
      }
    }

    return embed_url;
  }

  truncatedTitle(title, url) {
    if (!!url && url.substr(0, 50) === title.substr(0, 50)) {
      return title.substr(0, 35) + '...';
    } else {
      return title;
    }
  }

  deeplink() {
    let card = this.props.card;
    let classes =
      this.props.card.additional_metadata && this.props.card.additional_metadata.user_courses;
    let has_many_classes = classes && classes.length > 1;

    if (has_many_classes) {
      this.setState({ modalClassesOpen: true });
    } else {
      let url = card.deeplink_url;
      if (classes && classes.length == 1) {
        url = classes[0].deeplink_url;
      }
      window.open(url, '_blank');
    }
  }

  closeClassesModal = () => {
    this.setState({ modalClassesOpen: false }, () => {
      document.body.style.overflow = '';
    });
  };

  openCardWalletModal(data) {
    let priceData = {
      card: this.props.card,
      priceData: data,
      cardPurchased: this.cardPurchased
    };
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['BUY'], {
        courseName: this.props.card.resource.title,
        priceData: data,
        status: this.cardPurchased,
        event: 'Buy Now Button Clicked'
      });
    }
    this.props.dispatch(openCardWalletPaymentModal(priceData));
  }

  openPayPalPayment(priceData) {
    this.setState({
      showLoader: true
    });
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['BUY'], {
        courseName: this.props.card.resource.title,
        priceData: priceData,
        status: this.cardPurchased,
        event: 'Buy Now Button Clicked'
      });
    }
    let payload = {
      orderable_type: 'card',
      price_id: priceData.id,
      orderable_id: this.props.card.id,
      gateway: 'paypal'
    };
    initiateCardPurchase(payload)
      .then(data => {
        window.open(data.transactionDetails.url, '_self');
        this.setState({
          showLoader: false
        });
      })
      .catch(error => {
        this.setState({
          showLoader: false
        });
        this.props.dispatch(openSnackBar('Something went wrong! Please try again', true));
      });
  }

  getPricingPlans(card) {
    return this.state.edcastPlansForPricing && card.cardMetadatum && card.cardMetadatum.plan
      ? tr(_.startCase(_.toLower(card.cardMetadatum.plan)))
      : tr('Free');
  }

  cardPurchased() {
    this.setState({
      cardPurchased: true
    });
  }

  setScormStateMsg = card => {
    if (card.state == 'processing') {
      return 'processing';
    } else if (card.state == 'error') {
      return 'Error while uploading';
    } else {
      return null;
    }
  };

  fileStackHandler = (accept, callback) => {
    let fromSources = [
      'local_file_system',
      'googledrive',
      'dropbox',
      'box',
      'github',
      'gmail',
      'onedrive',
      'webcam'
    ];
    uploadPolicyAndSignature()
      .then(data => {
        let policy = data.policy;
        let signature = data.signature;

        filestackClient(policy, signature)
          .pick({
            accept: videoType,
            maxFiles: 1,
            storeTo: {
              location: 's3'
            },
            imageMax: [930, 505],
            imageMin: [200, 200],
            fromSources: fromSources
          })
          .then(callback)
          .catch(err => {
            console.error(`Error in insightV2.fileStackHandler.filestackClient.func : ${err}`);
          });
        this.setState({ uploadErrorText: '' });
      })
      .catch(err => {
        console.error(`Error in insightV2.fileStackHandler.uploadPolicyAndSignature.func : ${err}`);
      });
  };

  addFileStackFiles(fileStack) {
    let videoPreTranscodeUrl;
    refreshFilestackUrl(fileStack.filesUploaded[0].url)
      .then(resp => {
        if (
          fileStack.filesUploaded[0].mimetype &&
          ~fileStack.filesUploaded[0].mimetype.indexOf('video')
        ) {
          let securedVideoUrl = resp.signed_url;
          let securedVideoPath = parseUrl(securedVideoUrl).pathname;
          videoTranscode(securedVideoPath)
            .then(data => {
              this.setState({
                transcodedVideoUrl: data.status === 'completed' ? data.data.url : null,
                transcodedThumbnail: data.status === 'completed' ? data.data.thumbnail : null,
                transcodedVideoStatus: data.status
              });
            })
            .catch(err => {
              console.error(
                `Error in InsightV2.addFileStackFiles.refreshFilestackUrl.videoTranscode.func : ${err}`
              );
            });
          videoPreTranscodeUrl = securedVideoUrl || fileStack.filesUploaded[0].url;
        }
        this.setState({
          fileStack: [...this.state.fileStack, ...fileStack.filesUploaded],
          videoPreTranscodeUrl
        });
      })
      .catch(err => {
        console.error(`Error in InsightV2.addFileStackFiles.refreshFilestackUrl.func : ${err}`);
      });
  }

  videoSourcePreview = (item, isDownloadContentDisabled) => {
    return (
      <div className="project-preview-top-spacing">
        <span>
          <div>
            <video
              preload="auto"
              poster={this.state.fileStack[1] && this.state.fileStack[1].url}
              className="preview-upload-video"
              controls
              src={this.state.videoPreTranscodeUrl || item.url}
              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
            />
            <div />
          </div>
        </span>
      </div>
    );
  };

  submitClickHandler = cardData => {
    if (!this.state.fileStack[0]) {
      this.setState({ uploadErrorText: 'Please upload a video', disableSubmit: false });
      return;
    }
    if (!this.state.description) {
      this.setState({ disableSubmit: false, optionsError: 'This is required' });
      return;
    }
    let payload = {
      project_submission: {
        description: this.state.description,
        project_id: cardData.projectId,
        filestack: this.state.fileStack
      }
    };
    this.setState({ createLabel: 'Submitting..' });

    submitProjectCard(payload)
      .then(data => {
        this.setState({
          videoSubmitted: true,
          createLabel: 'Submit',
          description: '',
          fileStack: [],
          optionsError: '',
          disableSubmit: true
        });
        this.props.dispatch(openSnackBar('Submission Added', true));

        // should get called only for project card
        if (this.state.showProjectCard && this.props.card && this.props.card.projectId) {
          this.fetchSubmissionList();
        }
      })
      .catch(err => {
        console.error(`Error in insightV2.submitProjectCard.func : ${err}`);
      });
  };

  descriptionCardChange = event => {
    this.setState({ description: event.target.value, optionsError: '' });
  };

  cardStatusButton = (e, card, isCompleted) => {
    if (
      !this.state.disableCompleteBtn &&
      !(
        !this.isUncompleteEnabled &&
        ((card.assignment && card.assignment.state === 'COMPLETED') || isCompleted)
      )
    ) {
      this.completeClickHandler();
    }
  };

  onUserHandle = handle => {
    const currentUserHandle = this.props.currentUser.handle;
    this.props.dispatch(push(`/${handle.replace('@', '') === currentUserHandle ? 'me' : handle}`));
  };

  refreshMediaUrl = (media, handle) => {
    if (!!handle && (media == 'audio' || media == 'video')) {
      refreshFilestackUrl(`http://cdn.filestackcontent.com/${handle}`)
        .then(data => {
          let newUrl = data.signed_url;
          let mediaObj = document.getElementsByTagName(media)[0];
          let seen = mediaObj.currentTime;
          mediaObj.setAttribute('src', newUrl);
          mediaObj.currentTime = seen;
          mediaObj.play();
        })
        .catch(err => {
          console.error(`Error in insightV2.refreshFilestackUrl.func : ${err}`);
        });
    }
  };

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (this.state.dismissed) {
      return null;
    }
    let card = checkResources(this.state.card);
    let params = getCardParams(card, this.props, 'insightV2') || {};
    let isCompleted = this.state.isCompleted;
    let resourceImage;
    let scormCard = card.filestack && card.filestack[0] && card.filestack[0].scorm_course;

    if (
      !scormCard &&
      ((card.resource && card.resource.imageUrl) ||
        (card.filestack && card.filestack[0] && card.filestack[0].url))
    ) {
      resourceImage = card.resource.imageUrl || card.filestack[0].url;
    } else if (scormCard) {
      resourceImage = (card.filestack[1] && card.filestack[1].url) || this.state.courseDefaultImage;
    } else {
      resourceImage = this.state.courseDefaultImage;
    }
    let isTodaysLearningFirstFeedTab =
      this.props.feed &&
      this.props.feed.firstFeedTab &&
      this.props.feed.firstFeedTab === 'feed/todaysLearning';

    let isInitialCurrentRoute = this.props.pathname === '/' || this.props.pathname === '/feed';
    let isInitialPrevPath = this.state.prevPath === '/' || this.state.prevPath === '/feed';
    let standAloneModalOnTodaysLearning =
      this.props.isStandaloneModal &&
      (this.props.pathname.indexOf('todays-learning') !== -1 ||
        (isTodaysLearningFirstFeedTab && isInitialCurrentRoute));
    let standAloneFromTodaysLearning =
      !this.props.isStandaloneModal &&
      this.state.prevPath &&
      this.state.prevPath.indexOf('todays-learning') !== -1;
    let isTodaysLearning =
      standAloneModalOnTodaysLearning ||
      standAloneFromTodaysLearning ||
      (isTodaysLearningFirstFeedTab && isInitialPrevPath && !this.props.isStandaloneModal);

    let isRecommendedFirstFeedTab =
      this.props.feed &&
      this.props.feed.firstFeedTab &&
      this.props.feed.firstFeedTab === 'feed/recommended';
    let standAloneModalOnRecommended =
      this.props.isStandaloneModal &&
      (this.props.pathname.indexOf('recommended') !== -1 ||
        (isRecommendedFirstFeedTab && isInitialCurrentRoute));
    let standAloneFromRecommended =
      !this.props.isStandaloneModal &&
      this.state.prevPath &&
      this.state.prevPath.indexOf('recommended') !== -1;
    let isRecommended =
      standAloneModalOnRecommended ||
      standAloneFromRecommended ||
      (isRecommendedFirstFeedTab && isInitialPrevPath && !this.props.isStandaloneModal);
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;

    if (card.card_type) {
      // Reformat to fit correct insight requirement
      //TODO: assignment, commentsPending, isUpvoted
      card.timestampWithZone = card.updated_at;
      card.isOffical = card.is_official;
      card.cardType = card.card_type;
      card.commentsCount = card.comments_count;
      card.publishedAt = card.published_at;
      //TODO: feed_v1 is not having config with card, need to fix
      // card.isShareable = card.config.shareable;
    }

    // card resource fix
    if (typeof card.resource === 'undefined') {
      if (card.video) {
        card.resource = {
          description: card.video.description,
          embedHtml: card.video.embedHtml,
          site: card.video.site,
          title: card.video.title,
          slug: `/${linkPrefix(card.cardType)}/${card.slug}`
        };
      }
    }

    // Fix for SQL -> Markdown issues
    let viewMore = this.state.truncateTitle ? (
      <span>
        ...
        <a className="viewMore" onTouchTap={this.viewMoreTitleHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    let viewMoreMessage = this.state.truncateMessage ? (
      <span>
        ...
        <a className="viewMore" target="_blank" onTouchTap={this.viewMoreMessageHandler}>
          {tr('View More')}
        </a>
      </span>
    ) : null;
    // clicking for standalone page or URL
    let htmlRender = !!card.resource && (!!card.resource.title || !!card.resource.description);
    let resourceUrl = this.linkExtract();

    let headerTitle = (
      <div className="channels-block">
        {this.props.author ? (
          <div className="author-float">
            <a
              className="user"
              onTouchTap={() => {
                this.props.dispatch(
                  push(
                    `/${
                      this.props.author.handle.replace('@', '') === this.props.currentUser.handle
                        ? 'me'
                        : this.props.author.handle
                    }`
                  )
                );
              }}
            >
              <span>{this.props.card.author.fullName ? this.props.card.author.fullName : ''}</span>
            </a>
          </div>
        ) : (
          card.eclSourceTypeName &&
          logoObj[card.eclSourceTypeName.toLowerCase()] !== undefined && (
            <div className="align-self-middle">
              <span className="insight-img-logo">
                <img src={logoObj[card.eclSourceTypeName.toLowerCase()]} />
              </span>
            </div>
          )
        )}
      </div>
    );
    let headerSubtitle = card.publishedAt && (
      <div className="header-secondary-text">
        <span className="matte">
          {this.state.cardClickHandle === 'modal' && (
            <CreationDate
              card={card}
              standaloneLinkClickHandler={this.standaloneLinkClickHandler}
            />
          )}
        </span>
      </div>
    );
    const isInsightInfoBar =
      (card.channels && card.channels.length > 0) || (card.tags && card.tags.length > 0);
    const isPptFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/vnd.ms-powerpoint')
    );
    const isPptxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      )
    );
    const isDocFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/msword')
    );
    const isDocxFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf(
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      )
    );
    const isPdfFile = !!(
      params.isFileAttached &&
      card.filestack[0].mimetype &&
      ~card.filestack[0].mimetype.indexOf('application/pdf')
    );
    let displayCardType, fileIcon;
    if (params.isFileAttached) {
      switch (card.filestack[0].mimetype) {
        case 'application/vnd.ms-powerpoint':
          displayCardType = 'PPT';
          fileIcon = '/i/images/ppt_icon_min.png';
          break;
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
          displayCardType = 'PPTX';
          fileIcon = '/i/images/ppt_icon_min.png';
          break;
        case 'application/msword':
          displayCardType = 'DOC';
          fileIcon = '/i/images/doc_icon.png';
          break;
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
          displayCardType = 'DOCX';
          fileIcon = '/i/images/doc_icon.png';
          break;
        case 'application/pdf':
          displayCardType = 'PDF';
          fileIcon = '/i/images/pdf-icon.png';
          break;
        default:
          displayCardType = params.cardType;
          break;
      }
    } else {
      displayCardType = params.cardType;
    }

    // Pricing according to country code
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = null;
    let priceSkillCoinData = null;
    if (card.prices && card.prices.length > 0) {
      priceData =
        _.find(card.prices, { currency: currency }) ||
        _.find(card.prices, { currency: 'USD' }) ||
        _.find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
      priceSkillCoinData = _.find(card.prices, { currency: 'SKILLCOIN' });
    }

    let eclDuration = null;

    if (card.eclDurationMetadata && card.eclDurationMetadata.calculated_duration_display) {
      eclDuration = card.eclDurationMetadata.calculated_duration_display;
      eclDuration = eclDuration.charAt(0).toUpperCase() + eclDuration.slice(1);
    }

    let endDate =
      card.end_date &&
      moment
        .utc(card.end_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');
    let startDate =
      card.start_date &&
      moment
        .utc(card.start_date, 'YYYY-MM-DD HH:mm:ss')
        .local()
        .format('l');

    let customStyle = this.state.configurableHeader ? this.state.customConfig : {};
    let markAsCompleteColor = colors.primary;
    if (this.state.configurableHeader) {
      markAsCompleteColor =
        (customStyle.utilities && customStyle.utilities.markAsCompleteColor) || colors.primary;
    }

    // Recommended Block sections
    // Defined here to map through later on
    let recommendBlocks = {
      CC: <RecommendedBlock type="cards" card_id={card.eclId || card.id} />,
      CU: <RecommendedBlock type="users" tag="CU" card_id={card.eclId || card.id} />,
      CUC: <RecommendedBlock type="cards" tag="CUC" card_id={card.eclId || card.id} />,
      CT: <RecommendedBlock type="topics" tag="CT" card_id={card.eclId || card.id} />
    };
    // Get the order based on LD flag
    let recommendBlocksConfig = window.ldclient
      .variation('recommendation-block-config', 'Default')
      .split('-'); // Convert to array
    let cantBuyWithSkillCoins =
      this.props.currentUser.walletBalance < (priceSkillCoinData && priceSkillCoinData.amount);
    let skillcoin_image = '/i/images/skillcoin-new.png';
    let maxNum = 9;
    let listOfChannelsToDisplay = this.visibleChannelList(card.channels);

    let completedAssignment = card.assignment && card.assignment.state === 'COMPLETED';
    let background =
      (this.state.configureCompleteButton &&
        ((!(completedAssignment || isCompleted) && colors.markAsCompleteBackground) ||
          ((completedAssignment || isCompleted) && colors.completedBackground))) ||
      null;

    let textColor =
      (this.state.configureCompleteButton &&
        ((!(completedAssignment || isCompleted) && colors.markAsCompleteText) ||
          ((completedAssignment || isCompleted) && colors.completedText))) ||
      '#ffffff';
    let markAsCompleteDisabledForLink =
      !this.state.showMarkAsComplete &&
      !this.state.markAsCompleteEnableForLink &&
      (this.state.card &&
        this.state.card.resource &&
        (this.state.card.resource.url ||
          this.state.card.resource.description ||
          this.state.card.resource.fileUrl)) &&
      this.state.card.resource.type !== 'Video' &&
      ((this.state.card.readableCardType &&
        this.state.card.readableCardType.toUpperCase() === 'ARTICLE') ||
        params.cardType === 'ARTICLE') &&
      !((card.assignment && card.assignment.state == 'COMPLETED') || isCompleted);
    const { currentUser } = this.props.currentUser;

    return (
      <div className="flex">
        <div
          className="insightV2"
          ref={node => (this._insight = node)}
          onClick={() => {
            if (!this.isCsodCourse) return;
            this.deeplink();
          }}
        >
          <BreadcrumbV2
            isStandaloneModal={this.props.isStandaloneModal}
            dataCard={this.props.dataCard}
            cardUpdated={this.props.cardUpdated}
          />
          <div>
            <div style={this.styles.priceAndBia}>
              {this.state.edcastPricing && card.readableCardType != 'jobs' && (
                <div>
                  {card.isPaid && !priceSkillCoinData && (
                    <div className="pricing">
                      <div className="pricing-label-black">
                        <span>
                          {tr('Price')}: {priceData.symbol}
                          {parseInt(priceData.amount)}
                        </span>
                      </div>
                    </div>
                  )}
                  {card.isPaid && priceSkillCoinData && (
                    <div className="pricing">
                      <div className="pricing-label-black">
                        <span>
                          {tr('Price')}:{' '}
                          <img className="pricing_skill_coins_label_icon" src={skillcoin_image} />
                          {parseInt(priceSkillCoinData.amount)}
                        </span>
                      </div>
                    </div>
                  )}
                  {!card.isPaid && (
                    <div className="pricing">
                      <div className="pricing-label-black">
                        <span>
                          {tr('Price')}: {this.getPricingPlans(card)}
                        </span>
                      </div>
                    </div>
                  )}
                </div>
              )}
              {this.showBIA && (
                <div style={this.state.isV3 ? {} : { margin: '10px 31px 0 auto' }}>
                  <SmartBiteLevel
                    allowConsumerModifyLevel={this.state.allowConsumerModifyLevel}
                    params={params}
                    card={card}
                    rateCard={this.rateCard}
                    isV3={this.state.isV3}
                  />
                </div>
              )}
            </div>
            {!this.isCsodCourse && (
              <CardHeader
                className="card-header__container"
                title={params.showCreator ? headerTitle : null}
                subtitle={headerSubtitle}
                avatar={
                  card.author && params.showCreator ? (
                    <BlurImage
                      style={this.styles.avatarBox}
                      id={card.id}
                      image={
                        card.author.picture ||
                        (card.author.avatarimages && card.author.avatarimages.small) ||
                        params.defaultUserImage
                      }
                    />
                  ) : null
                }
                style={this.styles.cardHeader}
              >
                <div className="card-header-icons horizontal-spacing-xlarge">
                  {card.isOffical && (
                    <span>
                      <IconButton
                        tooltip={tr('Featured')}
                        aria-label={tr('Featured')}
                        disableTouchRipple
                        tooltipPosition="top-center"
                      >
                        <FeaturedIcon color={colors.arylide} />
                      </IconButton>
                    </span>
                  )}
                  <div className="card-info">
                    {params.cardType !== 'ARTICLE' && card.cardType !== 'course' && (
                      <div className="align-self-middle">
                        {(card.eclSourceLogoUrl ||
                          (card.eclSourceTypeName &&
                            logoObj[card.eclSourceTypeName.toLowerCase()] !== undefined)) && (
                          <span>
                            <span className="insight-img-logo">
                              <img
                                src={
                                  card.eclSourceLogoUrl ||
                                  logoObj[card.eclSourceTypeName.toLowerCase()]
                                }
                              />
                            </span>
                            <span className="file-type-devider">|</span>
                          </span>
                        )}
                      </div>
                    )}
                    {(params.cardType === 'ARTICLE' || card.cardType === 'course') &&
                      (card.resource.siteName ||
                        (card.eclSourceDisplayName && card.eclSourceDisplayName !== 'UGC')) && (
                        <div className="align-self-middle">
                          <span className="card-type-display-name">
                            {card.resource.siteName || card.eclSourceDisplayName}
                          </span>
                          <span className="file-type-devider">|</span>
                        </div>
                      )}
                    <div className="align-self-middle">
                      <span className="card-type">
                        <div>
                          {(isPptFile || isPptxFile || isDocFile || isDocxFile || isPdfFile) && (
                            <div className="file-icon-devider-container">
                              <img className="file-icon" src={fileIcon} />
                              <span className="file-type-devider">|</span>
                            </div>
                          )}
                          <span>
                            {tr((card.readableCardType || displayCardType).toUpperCase())}
                          </span>
                        </div>
                      </span>
                      {card.viewCount !== undefined && (
                        <span className="card-views">{card.viewCount}</span>
                      )}
                    </div>
                  </div>
                </div>
              </CardHeader>
            )}

            <div className="line" />

            <div className="insight-container">
              <div className="resource-card">
                {(card.cardType === 'media' ||
                  card.cardType === 'poll' ||
                  card.cardType === 'course' ||
                  card.cardType === 'project' ||
                  this.isCsodCourse) &&
                  (!!scormCard ? card.state && card.state === 'published' : true) && (
                    <div className="vertical-spacing-large">
                      {!scormCard && (
                        <div
                          className="openCardFromSearch clearfix"
                          onClick={this.standaloneLinkClickHandler.bind(this)}
                        >
                          {card.title &&
                            card.resource &&
                            (card.title !== card.resource.title &&
                              params.cardType !== 'VIDEO' &&
                              card.title !== card.resource.url) && (
                              <div className={!card.cardType === 'project' ? 'card-title' : ''}>
                                {(htmlRender || card.cardType === 'project') &&
                                  ((
                                    <div
                                      dangerouslySetInnerHTML={{
                                        __html: (this.state.truncateTitle
                                          ? card.title.substr(0, 260)
                                          : card.title
                                        ).replace(/\n/gi, '\n\n')
                                      }}
                                    />
                                  ) || (
                                    <MarkdownRenderer
                                      markdown={(this.state.truncateTitle
                                        ? card.title.substr(0, 260)
                                        : card.title
                                      ).replace(/\n/gi, '\n\n')}
                                    />
                                  ))}
                                {viewMore}
                              </div>
                            )}
                          {!params.isFileAttached &&
                            params.imageFileResources &&
                            (!card.resource || card.resource.type !== 'Video') &&
                            this.imageContainer(card.fileResources[0].fileUrl)}
                          {params.isFileAttached && (
                            <div>
                              {!(
                                params.cardType === 'POLL' &&
                                card.resource &&
                                card.resource.type &&
                                card.resource.type.toLowerCase() === 'video'
                              ) &&
                                (card.filestack[0].mimetype &&
                                  card.filestack[0].mimetype.indexOf('video/') > -1) && (
                                  <div
                                    key={`card-handle-${card.filestack[0].handle}`}
                                    className="fp fp_video"
                                  >
                                    <span>
                                      <video
                                        onMouseEnter={() => {
                                          this.setState({ [card.filestack[0].handle]: true });
                                        }}
                                        onMouseLeave={() => {
                                          this.setState({ [card.filestack[0].handle]: false });
                                        }}
                                        controls
                                        src={card.filestack[0].url}
                                        poster={
                                          card.filestack &&
                                          card.filestack[1] &&
                                          card.filestack[1].url
                                        }
                                        preload={
                                          card.filestack && card.filestack[1] ? 'none' : 'auto'
                                        }
                                        autoPlay={this.props.team.config['enable-video-auto-play']}
                                        controlsList={
                                          params.isDownloadContentDisabled ? 'nodownload' : ''
                                        }
                                        onError={() => {
                                          this.refreshMediaUrl('video', card.filestack[0].handle);
                                        }}
                                      />
                                      {!params.isDownloadContentDisabled &&
                                        this.downloadBlock(card.filestack[0])}
                                    </span>
                                  </div>
                                )}
                              {!(
                                params.cardType === 'POLL' &&
                                card.resource &&
                                card.resource.type &&
                                card.resource.type.toLowerCase() === 'video'
                              ) &&
                                (card.filestack[0].mimetype &&
                                  card.filestack[0].mimetype.indexOf('video/') === -1) &&
                                card.filestack.map(file => {
                                  if (
                                    (isPptFile ||
                                      isPptxFile ||
                                      isDocFile ||
                                      isDocxFile ||
                                      isPdfFile) &&
                                    (card &&
                                      card.filestack &&
                                      card.filestack[1] &&
                                      card.filestack[1].mimetype &&
                                      card.filestack[1].mimetype.includes('image/')) &&
                                    file.mimetype.includes('image/')
                                  ) {
                                    return;
                                  }
                                  if (
                                    file.mimetype &&
                                    ~file.mimetype.indexOf('image/') &&
                                    (card.fileResources &&
                                      card.fileResources.length === 0 &&
                                      !card.filestack.scorm_course) &&
                                    (!card.resource || card.resource.type !== 'Video')
                                  ) {
                                    return this.imageContainer(file.url, file.handle);
                                  } else if (
                                    file.mimetype &&
                                    ~file.mimetype.indexOf('image/') &&
                                    (card.fileResources &&
                                      card.fileResources.length > 0 &&
                                      card.fileResources[0].filestack &&
                                      !card.fileResources[0].filestack.scorm_course) &&
                                    (!card.resource || card.resource.type !== 'Video')
                                  ) {
                                    return this.imageContainer(file.url, file.handle);
                                  } else if (file.mimetype && ~file.mimetype.indexOf('video/')) {
                                    return (
                                      <div key={`handle-${file.handle}`} className="fp fp_video">
                                        <span>
                                          {this.state.transcodedVideoStatus &&
                                            (this.state.transcodedVideoStatus === 'completed' ? (
                                              <video
                                                onMouseEnter={() => {
                                                  this.setState({ [file.handle]: true });
                                                }}
                                                onMouseLeave={() => {
                                                  this.setState({ [file.handle]: false });
                                                }}
                                                controls
                                                src={this.state.transcodedVideoUrl}
                                                controlsList={
                                                  params.isDownloadContentDisabled
                                                    ? 'nodownload'
                                                    : ''
                                                }
                                                autoPlay={
                                                  this.props.team.config['enable-video-auto-play']
                                                }
                                              />
                                            ) : (
                                              <img
                                                className="waiting-video"
                                                src="/i/images/video_processing_being_processed.jpg"
                                              />
                                            ))}
                                          {!params.isDownloadContentDisabled &&
                                            this.downloadBlock(file)}
                                        </span>
                                      </div>
                                    );
                                  } else if (
                                    this.standaloneCardFilePreview &&
                                    ((file.mimetype && ~file.mimetype.indexOf('application/pdf')) ||
                                      isPptFile ||
                                      isPptxFile ||
                                      isDocFile ||
                                      isDocxFile ||
                                      isPdfFile)
                                  ) {
                                    return (
                                      <div key={`handle-${file.handle}`} className="">
                                        <div className="row">
                                          <div className="medium-6 large-6 columns">
                                            <div
                                              className="sidebar-toggle-button"
                                              onClick={this.toggleSidbare.bind(this)}
                                            />
                                          </div>
                                          <div className="medium-6 large-6 columns text-right navigation-pdf-pages">
                                            <div className="arrow-left-icon">
                                              <ArrowLeft
                                                onClick={this.handlePrevious}
                                                style={this.styles.arrowLeftIcon}
                                              />
                                            </div>
                                            <div className="current-page-pdf">
                                              {this.state.page}/{this.state.pages}
                                            </div>
                                            <div className="arrow-right-icon">
                                              <ArrowRight
                                                onClick={this.handleNext}
                                                style={this.styles.arrowRightIcon}
                                              />
                                            </div>
                                          </div>
                                        </div>
                                        <div
                                          className={`sidebar-container${
                                            isPptFile || isPptxFile
                                              ? ' sidebar-container-for-ppt'
                                              : ''
                                          }`}
                                        >
                                          <Sidebar
                                            styles={this.stylesSidebar}
                                            shadow={false}
                                            sidebar={this.viewPagesPDF().map(num => {
                                              return (
                                                <div key={`pagesPDF-${num}`} className="row">
                                                  <div
                                                    onTouchTap={() => {
                                                      this.setState({ page: num });
                                                    }}
                                                    className="mini-pdf"
                                                  >
                                                    <PDF
                                                      scale={0.2}
                                                      file={
                                                        isPptFile ||
                                                        isPptxFile ||
                                                        isDocFile ||
                                                        isDocxFile
                                                          ? `https://process.filestackapi.com/output=f:pdf/${
                                                              file.url
                                                            }`
                                                          : file.url
                                                      }
                                                      page={num}
                                                      loading={<div>{tr('Loading file...')}</div>}
                                                    />
                                                    <div className="num-page">{num}</div>
                                                  </div>
                                                </div>
                                              );
                                            })}
                                            open={this.state.sidebarOpen}
                                            docked={this.state.sidebarOpen}
                                            onSetOpen={this.onSetSidebarOpen}
                                          >
                                            <div className="main-pdf">
                                              <PDF
                                                file={
                                                  isPptFile || isPptxFile || isDocFile || isDocxFile
                                                    ? `https://process.filestackapi.com/output=f:pdf/${
                                                        file.url
                                                      }`
                                                    : file.url
                                                }
                                                onDocumentComplete={this.onDocumentComplete}
                                                onPageComplete={this.onPageComplete}
                                                page={this.state.page}
                                                loading={
                                                  <div className="data-not-available-msg">
                                                    {tr('Loading file...')}
                                                  </div>
                                                }
                                              />
                                              <div className="current-main-page">
                                                {this.state.page}
                                              </div>
                                              <div
                                                className={
                                                  this.state.sidebarOpen
                                                    ? 'line-bottom line-bottom-sidebar'
                                                    : 'line-bottom'
                                                }
                                              />
                                            </div>
                                          </Sidebar>
                                        </div>
                                      </div>
                                    );
                                  } else {
                                    return file.mimetype && ~file.mimetype.indexOf('audio/') ? (
                                      <audio
                                        controls
                                        src={file.url}
                                        style={this.styles.audio}
                                        onError={() => {
                                          this.refreshMediaUrl('audio', file.handle);
                                        }}
                                      />
                                    ) : (
                                      <span>
                                        {!scormCard && (
                                          <div
                                            key={`card-file-handle-${file.handle}`}
                                            className="fp fp_preview"
                                          >
                                            <span>
                                              {params.cardType === 'FILE' ? (
                                                <iframe
                                                  height="500px"
                                                  width="100%"
                                                  src={pdfPreviewUrl(
                                                    file.url,
                                                    expireAfter,
                                                    this.props.currentUser.id
                                                  )}
                                                />
                                              ) : (
                                                <a
                                                  className="fp inline-box"
                                                  href={resourceUrl}
                                                  target="_blank"
                                                />
                                              )}
                                              {!params.isDownloadContentDisabled &&
                                                this.downloadBlock(file)}
                                            </span>
                                          </div>
                                        )}
                                      </span>
                                    );
                                  }
                                })}
                            </div>
                          )}
                          {params.message &&
                            !card.resource.title &&
                            !scormCard &&
                            card.cardType !== 'course' && (
                              <div className="message">
                                {(htmlRender && (
                                  <div
                                    dangerouslySetInnerHTML={{
                                      __html: (this.state.truncateMessage
                                        ? this.truncateMessageText(params.message)
                                        : params.message
                                      ).replace(/\n|↵/gi, '\n')
                                    }}
                                  />
                                )) || (
                                  <RichTextReadOnly
                                    text={convertRichText(
                                      this.state.truncateMessage
                                        ? this.truncateMessageText(params.message)
                                        : params.message
                                    )}
                                  />
                                )}
                                {viewMoreMessage}
                              </div>
                            )}
                        </div>
                      )}
                      {card.cardType == 'project' && card.filestack && this.state.showProjectCard && (
                        <div className="openCardFromSearch clearfix">
                          <div className="common-margin">
                            <Divider />
                          </div>
                          {((!this.state.videoSubmitted &&
                            (this.state.submissionList &&
                              this.state.submissionList.length > 0 &&
                              this.state.submissionList[0].projectSubmissionReviews &&
                              this.state.submissionList[0].projectSubmissionReviews.length > 0 &&
                              (this.state.submissionList[0].projectSubmissionReviews[0].status !==
                                'approved' ||
                                this.state.submissionList[0].projectSubmissionReviews[0].status ===
                                  'rejected'))) ||
                            (this.state.submissionList &&
                              this.state.submissionList.length == 0)) && (
                            <div>
                              <span>{tr('ADD YOUR SUBMISSION')}</span>{' '}
                              <span style={this.styles.submissionList}>
                                {tr('You can submit only one file per project for approval.')}
                              </span>
                              <FlatButton
                                className="image-upload overview-tags-container"
                                icon={<VideoIcon style={this.styles.videoIcon} color="#bababa" />}
                                onClick={this.fileStackHandler.bind(
                                  this,
                                  videoType,
                                  this.addFileStackFiles.bind(this)
                                )}
                              />
                              {this.state.fileStack &&
                                this.state.fileStack[0] &&
                                this.state.fileStack[0].mimetype &&
                                this.state.fileStack[0].mimetype.indexOf('video/') > -1 && (
                                  <div className="text-center">
                                    <div>
                                      <div className="small-12 relative">
                                        {this.videoSourcePreview(
                                          this.state.fileStack[0],
                                          isDownloadContentDisabled
                                        )}
                                      </div>
                                      <div className="small-12 file-title">
                                        <span className="item-name">
                                          {this.state.fileStack[0].filename ||
                                            this.state.fileStack[0].handle}
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                )}
                              <div className="error-text">{this.state.uploadErrorText}</div>
                              <div className="overview-tags-container">
                                <TextFieldCustom
                                  hintText="Description"
                                  value={this.state.description}
                                  inputChangeHandler={this.descriptionCardChange.bind(this)}
                                  rowsMax={4}
                                  rows={4}
                                  multiLine={true}
                                  fullWidth={true}
                                />
                                {this.state.optionsError && (
                                  <div className="error-text">{tr(this.state.optionsError)}</div>
                                )}
                              </div>
                              <div className="action-buttons overview-tags-container">
                                <FlatButton
                                  label={tr(this.state.createLabel)}
                                  className={this.styles.create}
                                  onTouchTap={this.submitClickHandler.bind(this, card)}
                                  labelStyle={this.styles.btnLabel}
                                  style={
                                    !this.state.disableSubmit
                                      ? this.styles.disabled
                                      : this.styles.create
                                  }
                                />
                              </div>
                            </div>
                          )}
                          <div className="overview-tags-container">
                            {this.state.submissionList && this.state.submissionList.length > 0 && (
                              <div>{tr('Previous Submissions')}</div>
                            )}
                            {this.state.submissionList &&
                              this.state.submissionList.map(list => {
                                return (
                                  <div style={this.styles.submissionList}>
                                    {list.filestack[0].filename}
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}
                      {((card.resource &&
                        (card.resource.url ||
                          card.resource.description ||
                          card.resource.fileUrl)) ||
                        this.isCsodCourse) && (
                        <div>
                          {card.message &&
                            card.resource.title &&
                            (params.cardType === 'ARTICLE' ||
                              card.cardType === 'course' ||
                              params.cardType === 'VIDEO') &&
                            card.message !== card.resource.title && (
                              <h5
                                className="resource-message"
                                dangerouslySetInnerHTML={{ __html: card.message }}
                              />
                            )}
                          {this.isCsodCourse && card.name && (
                            <h5 className="resource-message">{card.name}</h5>
                          )}
                          <div className="resource">
                            {(card.resource.embedHtml ||
                              card.resource.videoUrl ||
                              card.resource.url) &&
                              card.resource.type === 'Video' && (
                                <InlineVideo
                                  cardId={this.props.cardId}
                                  embedHtml={
                                    card.resource.embedHtml ||
                                    (checkResourceURL(
                                      card.resource.videoUrl || this.embedUrl(card.resource.url)
                                    )
                                      ? `<iframe width="1" src="${card.resource.videoUrl ||
                                          this.embedUrl(card.resource.url)}"></iframe>`
                                      : thumbnailImage(
                                          this.state.card.cardId || this.state.card.id,
                                          this.state.card.resource.imageUrl,
                                          this.props.currentUser.id
                                        ))
                                  }
                                  videoUrl={card.resource.videoUrl}
                                />
                              )}
                            {card.resource.fileUrl && <img src={card.resource.fileUrl} />}
                            <div className="clearfix">
                              {card.resource.title &&
                                params.cardType !== 'VIDEO' &&
                                card.readableCardType !== 'video' &&
                                card.resource.title !== card.resource.url && (
                                  <div
                                    className={`${
                                      params.cardType === 'ARTICLE' || card.cardType === 'course'
                                        ? 'resource-title-block__article'
                                        : ''
                                    }`}
                                  >
                                    {card.resource &&
                                    card.resource.url &&
                                    card.resource.url.includes('/api/scorm/') ? (
                                      ' '
                                    ) : (
                                      <h5
                                        className={`resource-title ${
                                          params.cardType === 'ARTICLE'
                                            ? 'resource-title__article'
                                            : ''
                                        }`}
                                        dangerouslySetInnerHTML={{
                                          __html: this.truncatedTitle(
                                            params.cardType === 'ARTICLE'
                                              ? card.resource.title || card.message || card.title
                                              : card.resource.title,
                                            card.resource.url
                                          )
                                        }}
                                      />
                                    )}
                                    {params.cardType === 'ARTICLE' ||
                                      (card.cardType === 'course' && (
                                        <div className="align-self-middle">
                                          {card.eclSourceTypeName &&
                                            logoObj[card.eclSourceTypeName.toLowerCase()] !==
                                              undefined &&
                                            this.props.author && (
                                              <span className="insight-img-logo">
                                                <img
                                                  src={
                                                    logoObj[card.eclSourceTypeName.toLowerCase()]
                                                  }
                                                />
                                              </span>
                                            )}
                                        </div>
                                      ))}
                                  </div>
                                )}
                              {(params.cardType === 'ARTICLE' ||
                                card.cardType === 'course' ||
                                this.isCsodCourse) && (
                                <div className="article-resource">
                                  <div className="description">
                                    {this.imageContainer(resourceImage, null, 'article')}
                                    <div
                                      style={{ display: 'inline' }}
                                      dangerouslySetInnerHTML={{
                                        __html: card.resource.description
                                      }}
                                    />
                                    {!this.isCsodCourse && card.state === 'published' ? (
                                      <a
                                        title={tr('open in new tab')}
                                        className={
                                          this.props.card.paymentEnabled &&
                                          !this.props.card.paidByUser
                                            ? 'link-to-original hide'
                                            : 'link-to-original'
                                        }
                                        onClick={e => this.handleResourceClicked(e)}
                                        href="#"
                                      >
                                        {' '}
                                        {card.resource &&
                                        card.resource.url &&
                                        card.resource.url.includes('/api/scorm/')
                                          ? tr('Click here to access the course')
                                          : tr('View More')}
                                      </a>
                                    ) : (
                                      <div className="scorm-processing">
                                        <Spinner />
                                        <p>{this.setScormStateMsg(card)}</p>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              )}
                              {card.resource.imageUrl &&
                                !card.resource.embedHtml &&
                                card.resource.type !== 'Video' &&
                                params.cardType !== 'ARTICLE' &&
                                card.cardType !== 'course' &&
                                !params.isYoutubeVideo && (
                                  <div className="image-container">
                                    {this.imageContainer(card.resource.imageUrl)}
                                  </div>
                                )}
                              {params.cardType !== 'ARTICLE' && card.cardType !== 'course' && (
                                <div className="description">
                                  {card.resource.description}
                                  {!params.isYoutubeVideo && !params.isVimeoVideo && (
                                    <a
                                      title={tr('open in new tab')}
                                      className="link-to-original"
                                      onClick={e => this.handleResourceUrlClicked(e, resourceUrl)}
                                      href="#"
                                    >
                                      {' '}
                                      {tr('View More')}
                                    </a>
                                  )}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                {scormCard &&
                  !(!!card.resource && !!card.resource.url) &&
                  card.state &&
                  card.state !== 'published' && (
                    <div className="article-resource">
                      <div className="description">
                        {this.imageContainer(resourceImage)}
                        <div className="scorm-processing">
                          <Spinner />
                          <p>{this.setScormStateMsg(card)}</p>
                        </div>
                      </div>
                    </div>
                  )}
                {(card.cardType === 'pack' || card.cardType === 'pack_draft') && (
                  <PathwayCover card={card} currentUser={this.props.currentUser} />
                )}
                {(card.cardType === 'VideoStream' || card.cardType === 'video_stream') && (
                  <VideoStream
                    card={card}
                    standaloneLinkClickHandler={this.standaloneLinkClickHandler}
                  />
                )}
                {card.ecl &&
                  card.ecl.origin &&
                  this.props.providerLogos[card.ecl.origin] != undefined && (
                    <span>
                      <img
                        className="origin-logos"
                        src={this.props.providerLogos[card.ecl.origin]}
                      />
                    </span>
                  )}
              </div>
              {this.isCsodCourse && (
                <div className="csod-course_big">
                  {card.status && (
                    <div className="csod-course__info">
                      {tr('Status: ')} {card.status}
                    </div>
                  )}
                  {startDate && (
                    <div className="csod-course__info">
                      {tr('Assigned Date: ')} {startDate}
                    </div>
                  )}
                  {endDate && (
                    <div className="csod-course__info">
                      {tr('Due Date: ')} {endDate}
                    </div>
                  )}
                  {card.duration && (
                    <div className="csod-course__info">
                      {tr('Duration: ')} {card.duration}
                    </div>
                  )}
                </div>
              )}
              {card.cardType === 'poll' && (
                <Poll
                  cardOverview={true}
                  cardUpdated={this.cardUpdated.bind(this)}
                  card={card}
                  checkComplete={this.checkComplete}
                  currentUserId={this.props.currentUser.id}
                />
              )}
              {(card.startDate || (card.assignment && card.assignment.startDate)) && (
                <small className="due-date">
                  {tr('Start Date: ')}
                  <DateConverter
                    isMonthText="true"
                    date={card.startDate || (card.assignment && card.assignment.startDate)}
                  />
                </small>
              )}
              {(card.dueAt || (card.assignment && card.assignment.dueAt)) && (
                <small className="due-date">
                  {tr('Due Date: ')}
                  <DateConverter
                    isMonthText="true"
                    date={card.dueAt || (card.assignment && card.assignment.dueAt)}
                  />
                </small>
              )}

              <div className={`${isInsightInfoBar ? 'insight-info-bar' : ''} clearfix row`}>
                {card.channels && card.channels.length > 0 && this.state.showChannelsTooltip && (
                  <div className="tooltip">
                    {listOfChannelsToDisplay.map((channel, index) => {
                      return (
                        <span key={`tooltip-channels-${index}`}>
                          {channel.label}
                          {index !== card.channels.length - 1 && <span>,</span>}
                          {'\u00A0'}
                        </span>
                      );
                    })}
                  </div>
                )}
                {(card.channels && card.channels.length) > 0 && (
                  <div
                    onMouseEnter={this.showTooltip.bind(this, 'showChannelsTooltip')}
                    onMouseLeave={this.hideTooltip.bind(this, 'showChannelsTooltip')}
                    className="channels medium-4 large-4 columns"
                  >
                    {listOfChannelsToDisplay.length > 0 && (
                      <span className="info-bar-name" style={this.styles.labelInfoStyle}>
                        {tr('Channel:')}
                        {'\u00A0'}
                      </span>
                    )}
                    {listOfChannelsToDisplay.map((channel, index) => {
                      if (index > 2) {
                        return;
                      } else if (index === 2) {
                        return (
                          <span className="channel-label" key={`channel-${index}`}>
                            ...
                          </span>
                        );
                      } else {
                        return (
                          <span key={`channel-${index}`}>
                            <a
                              className="channel-label"
                              title={channel.label}
                              onTouchTap={() => {
                                this.props.dispatch(push(`/channel/${channel.id}`));
                              }}
                            >
                              {channel.label}
                              {index === 0 && index !== listOfChannelsToDisplay.length - 1 && (
                                <span>,</span>
                              )}
                            </a>
                            {'\u00A0'}
                          </span>
                        );
                      }
                    })}
                  </div>
                )}
                <div className="medium-8 large-8 tags columns tags-container">
                  {card.tags && card.tags.length > 0 && this.state.showTagsTooltip && (
                    <div className="tooltip">
                      {card.tags.map((tag, index) => {
                        return (
                          <span key={`tooltip-${index}`}>
                            {tag.name}
                            {index !== card.tags.length - 1 && <span>,</span>}
                            {'\u00A0'}
                          </span>
                        );
                      })}
                    </div>
                  )}
                  {(card.tags && card.tags.length) > 0 && (
                    <div
                      onMouseEnter={this.showTooltip.bind(this, 'showTagsTooltip')}
                      onMouseLeave={this.hideTooltip.bind(this, 'showTagsTooltip')}
                    >
                      <span className="info-bar-name" style={this.styles.labelInfoStyle}>
                        {tr('Tags:')}
                        {'\u00A0'}
                      </span>
                      {card.tags.map((tag, index) => {
                        if (index > 2) {
                          return;
                        } else if (index === 2) {
                          return (
                            <span className="tag-name" key={`tag-${tag.id}`}>
                              ...
                            </span>
                          );
                        } else {
                          return (
                            <span key={`tag-${tag.id}`}>
                              <a className="tag-name" title={tag.name}>
                                {tag.name}
                                {index === 0 && index !== card.tags.length - 1 && <span>,</span>}
                              </a>
                              {'\u00A0'}
                            </span>
                          );
                        }
                      })}
                    </div>
                  )}
                </div>
              </div>
              <div id="content-topics-ref">
                {tr('SmartCard Topics')}: <span id="content-topics" />
              </div>
              {!params.hideProvider && (card.provider || card.providerImage) && (
                <div className="provider-info-bar">
                  <span className="info-bar-name" style={this.styles.labelInfoStyle}>
                    {tr('Provider')}: {'\u00A0'}
                  </span>
                  {card.providerImage && (
                    <img className="provider-image" src={card.providerImage} />
                  )}
                  {card.provider && (
                    <span className="provider-name left-border">{card.provider}</span>
                  )}
                </div>
              )}
              <div
                className={`insight-action-bar views-rates-bar ${
                  !isInsightInfoBar ? 'insight-action-bar-margin' : ''
                }`}
              >
                <div className="float-left horizontal-spacing-xlarge">
                  <div className="inline-box">
                    {card.eclDurationMetadata &&
                      !!card.eclDurationMetadata.calculated_duration &&
                      card.eclDurationMetadata.calculated_duration_display &&
                      this.props.card.eclDurationMetadata.calculated_duration > 0 && (
                        <div className="read-time-ago read-time-ago__margin">
                          {tr('Duration')}: {eclDuration}
                        </div>
                      )}

                    {this.state.card.cardType === 'video_stream' ? (
                      this.state.isLiveStream ? (
                        <div className="views">
                          {this.state.viewsCount} {tr('Viewers')}
                        </div>
                      ) : (
                        <div className="views">
                          {this.state.viewersCount} {tr('Views')}
                        </div>
                      )
                    ) : (
                      <div className="views">
                        {card.viewsCount} {tr('Views')}
                      </div>
                    )}
                  </div>
                </div>
                {Permissions.has('CAN_RATE') && (
                  <div className="float-right horizontal-spacing-xlarge">
                    <div className="inline-box">
                      <div style={this.styles.ratingBlock}>
                        <ReactStars
                          onChange={this.ratingChanged}
                          count={5}
                          size={16}
                          half={false}
                          color1={'#d6d6e1'}
                          color2={'#6f708b'}
                          className="relevancyRatingStars relevancyRatingStars_tab"
                          value={this.state.averageRating ? parseInt(this.state.averageRating) : 0}
                        />
                        <div className="relevancy">({this.state.ratingCount})</div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              {!this.props.isActionsDisabled && !this.isCsodCourse && (
                <div className="insight-action-bar main-actions-container">
                  <div className="horizontal-spacing-xlarge">
                    {!this.state.showRating && Permissions.has('LIKE_CONTENT') && (
                      <div className="like-container">
                        <button
                          className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                          onClick={this.cardLikeHandler}
                        >
                          {!this.state.card.isUpvoted && (
                            <span tabIndex={-1} className="hideOutline" aria-label="like">
                              <LikeIcon />
                            </span>
                          )}
                          {this.state.card.isUpvoted && (
                            <span tabIndex={-1} className="hideOutline" aria-label="liked">
                              <LikeIconSelected />
                            </span>
                          )}
                          <span className="tooltiptext">{tr('Like')}</span>
                        </button>
                        <small className="count">
                          {card.votesCount ? abbreviateNumber(card.votesCount) : ''}
                        </small>
                        {!_.isEmpty(this.state.card.voters) && (
                          <span style={{ paddingLeft: '20px' }}>
                            {this.state.card.voters.slice(0, maxNum).map(voter => {
                              return (
                                <span style={{ padding: '0 4px' }}>
                                  <button
                                    aria-label={`Visit ${voter.name}'s handle`}
                                    className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                                    onClick={this.onUserHandle.bind(this, voter && voter.handle)}
                                  >
                                    <span tabIndex={-1} className="hideOutline">
                                      <Avatar
                                        size={24}
                                        src={
                                          (voter.avatarimages && voter.avatarimages['tiny']) ||
                                          voter.avatar ||
                                          ''
                                        }
                                      />
                                    </span>
                                    <span className="tooltiptext">{voter.name}</span>
                                  </button>
                                </span>
                              );
                            })}
                            {this.state.card.voters.length > maxNum && (
                              <span style={{ color: '#4a90e2' }}>{maxNum}+</span>
                            )}
                          </span>
                        )}
                      </div>
                    )}
                    <SmartBiteInfoCommentsBlock
                      isStandalone={true}
                      comments={this.state.comments}
                      commentsCount={this.state.commentsCount}
                      liveCommentsCount={this.state.liveCommentsCount}
                      isLiveStream={this.state.isLiveStream}
                    />
                  </div>
                  <div className="horizontal-spacing-xlarge dots-btn_container">
                    {!this.props.isMarkAsCompleteDisabled &&
                      Permissions.has('MARK_AS_COMPLETE') &&
                      card.cardType !== 'quiz' &&
                      card.cardType !== 'poll' &&
                      card.cardType !== 'project' &&
                      ((card.assignment && card.assignment.state == 'COMPLETED') ||
                        !card.markFeatureDisabledForSource ||
                        isCompleted) &&
                      !this.state.showNewDesignForCompleteButton && (
                        <span style={this.styles.cardStatisticsBox}>
                          <button
                            className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                            onClick={
                              !this.isUncompleteEnabled && (completedAssignment || isCompleted)
                                ? () => {
                                    return null;
                                  }
                                : this.completeClickHandler
                            }
                          >
                            <CompletedAssignment
                              color={
                                (card.assignment && card.assignment.state == 'COMPLETED') ||
                                isCompleted
                                  ? markAsCompleteColor
                                  : colors.darkGray
                              }
                            />
                            {!card.markFeatureDisabledForSource && (
                              <span className="tooltiptext">
                                {tr(
                                  (card.assignment && card.assignment.state == 'COMPLETED') ||
                                    isCompleted
                                    ? this.isUncompleteEnabled
                                      ? 'Mark as Incomplete'
                                      : 'Completed'
                                    : 'Mark as Complete'
                                )}
                              </span>
                            )}
                          </button>
                        </span>
                      )}
                    {this.props.author &&
                      (params.isOwner || Permissions.has('VIEW_CARD_ANALYTICS')) && (
                        <span style={this.styles.cardStatisticsBox}>
                          <button
                            className="my-icon-button my-icon-button_disabled my-icon-button_small tooltip tooltip_bottom-center"
                            onClick={this.handleCardAnalayticsModal}
                            aria-label="Card Statistics"
                          >
                            <span tabIndex={-1} className="hideOutline">
                              <CardAnalyticsV2 color={colors.darkGray} />
                            </span>
                            <span className="tooltiptext">{tr('Card Statistics')}</span>
                          </button>
                        </span>
                      )}
                    {!this.props.isMarkAsCompleteDisabled &&
                      Permissions.has('MARK_AS_COMPLETE') &&
                      card.cardType !== 'quiz' &&
                      card.cardType !== 'poll' &&
                      card.cardType !== 'project' &&
                      ((card.assignment && card.assignment.state == 'COMPLETED') ||
                        !card.markFeatureDisabledForSource ||
                        isCompleted) &&
                      this.state.showNewDesignForCompleteButton && (
                        <span style={this.styles.cardMarkBox}>
                          <button
                            className={`my-icon-button tooltip tooltip_bottom-center ${
                              !this.state.configureCompleteButton ? 'action-btn-color' : ''
                            } ${
                              (card.assignment && card.assignment.state == 'COMPLETED') ||
                              isCompleted
                                ? 'my-icon-button_mark_incomplete'
                                : `my-icon-button_mark_complete ${
                                    markAsCompleteDisabledForLink
                                      ? 'mark_as_complete_button_disabled'
                                      : ''
                                  }`
                            }`}
                            onClick={e => this.cardStatusButton(e, card, isCompleted)}
                            disabled={
                              this.state.disableCompleteBtn || markAsCompleteDisabledForLink
                            }
                            style={{
                              background: background,
                              color: textColor
                            }}
                          >
                            <CheckedIcon
                              aria-label={tr(
                                (card.assignment && card.assignment.state == 'COMPLETED') ||
                                  isCompleted
                                  ? 'Completed'
                                  : 'Mark as Complete'
                              )}
                              color={textColor}
                              className="mark-complete-incomplete-button"
                            />
                            {tr(
                              (card.assignment && card.assignment.state == 'COMPLETED') ||
                                isCompleted
                                ? 'Completed'
                                : 'Mark as Complete'
                            )}
                            {!card.markFeatureDisabledForSource && (
                              <span className="tooltiptext">
                                {tr(
                                  markAsCompleteDisabledForLink
                                    ? 'Please visit link url'
                                    : (card.assignment && card.assignment.state == 'COMPLETED') ||
                                      isCompleted
                                    ? this.isUncompleteEnabled
                                      ? 'Mark as Incomplete'
                                      : 'Completed'
                                    : 'Mark as Complete'
                                )}
                              </span>
                            )}
                          </button>
                        </span>
                      )}
                    {!this.props.isActionsDisabled && (
                      <InsightDropDownActions
                        card={card}
                        author={this.props.author}
                        dismissible={this.props.dismissible}
                        disableTopics={false}
                        isStandalone={this.props.isStandalone}
                        cardUpdated={this.cardUpdated.bind(this)}
                        removeCardFromList={this.props.removeCardFromList}
                        isCompleted={this.state.isCompleted}
                        callCompleteClickHandler={this.completeClickHandler}
                        type={'StandAlone'}
                        isStandalonePage={true}
                        markAsCompleteDisabledForLink={markAsCompleteDisabledForLink}
                      />
                    )}
                  </div>
                </div>
              )}
            </div>

            {this.state.showComment && !this.isCsodCourse && (
              <div className="comments-block">
                {!this.state.isLiveStream && (
                  <CommentList
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.props.author ? this.props.author.id : ''}
                    cardType={card.cardType}
                    comments={this.state.comments}
                    pending={card.commentPending}
                    numOfComments={this.state.commentsCount}
                    videoId={card.cardId ? card.id : null}
                    updateCommentCount={this.updateCommentCount}
                  />
                )}
                {this.state.isLiveStream && (
                  <LiveCommentList
                    cardId={card.cardId ? card.cardId + '' : card.id}
                    cardOwner={this.props.author ? this.props.author.id : ''}
                    cardType={card.cardType}
                    pending={card.commentPending}
                    videoId={card.cardId ? card.id : null}
                    updateCommentCount={this.updateCommentCount}
                    cardStatus={this.getCardStatus()}
                    cardUuid={this.getCardUuid()}
                    liveCommentsCount={this.liveCommentsCount}
                    onMessageReceiveJoinLeave={this.onMessageReceiveJoinLeave}
                  />
                )}
              </div>
            )}

            {this.state.showRecommendedContent && (
              <div className="recommended-block">
                {recommendBlocksConfig.map(block => {
                  return <div key={`recommend-block-${block}`}>{recommendBlocks[block]}</div>;
                })}
              </div>
            )}
          </div>

          {this.state.modalClassesOpen && (
            <CardClassesModal
              classes={this.props.card.additional_metadata.classes}
              closeModal={this.closeClassesModal}
            />
          )}
        </div>

        {this.state.edcastPricing &&
          this.state.wallet &&
          card.isPaid &&
          (priceSkillCoinData || priceData) &&
          this.props.card.paymentEnabled && (
            <div className="insights-pricing-info">
              {priceSkillCoinData && (
                <div className="useSkillCoins">
                  <p className="numberOfCoins">
                    <img className="pricing_skill_coins_tile_icon" src={skillcoin_image} />
                    {parseInt(priceSkillCoinData.amount)}
                  </p>
                  <PrimaryButton
                    disabled={
                      cantBuyWithSkillCoins ||
                      this.props.card.paidByUser ||
                      this.state.cardPurchased
                    }
                    onClick={this.openCardWalletModal.bind(this, priceSkillCoinData)}
                    label={tr('BUY WITH SKILLCOINS')}
                    className="review-btn"
                    style={this.styles.buyWithSkillCoins}
                  />
                  <div className="seperator" />
                </div>
              )}
              {priceData &&
                this.props.team.paymentGateways &&
                this.props.team.paymentGateways.braintree && (
                  <div>
                    <div className="otherOptions">
                      <p className="numberOfCoins">
                        Price: {priceData.symbol}
                        {parseInt(priceData.amount)}
                      </p>
                      <div>
                        <PrimaryButton
                          disabled={this.props.card.paidByUser || this.state.cardPurchased}
                          label={tr('BUY NOW')}
                          onClick={this.openCardWalletModal.bind(this, priceData)}
                          className="review-btn"
                          style={this.styles.buyNow}
                        />
                      </div>
                    </div>
                    <div className="seperator" />
                  </div>
                )}
              {priceData &&
                this.props.team.paymentGateways &&
                this.props.team.paymentGateways.paypal && (
                  <div>
                    <div className="otherOptions">
                      <p className="numberOfCoins">
                        Price: {priceData.symbol}
                        {parseInt(priceData.amount)}
                      </p>
                      <div>
                        <PrimaryButton
                          disabled={this.props.card.paidByUser || this.state.cardPurchased}
                          label={tr('BUY NOW')}
                          onClick={this.openPayPalPayment.bind(this, priceData)}
                          className="review-btn"
                          style={this.styles.buyNow}
                        />
                      </div>
                    </div>
                    <div className="seperator" />
                  </div>
                )}
              <div className="card-features">
                <p>
                  <span>
                    <FullTimeAccess />
                  </span>
                  <span>{tr('Full time access')}</span>
                </p>
                <p>
                  <span>
                    <Support />
                  </span>
                  <span>24/7 {tr('Support')}</span>
                </p>
                <p>
                  <span>
                    <WebandMobile />
                  </span>
                  <span>{tr('Access on Web and Mobile')}</span>
                </p>
                <p>
                  <span>
                    <Certificate />
                  </span>
                  <span>{tr('Certificate of Completion')}</span>
                </p>
              </div>
            </div>
          )}
      </div>
    );
  }
}

InsightV2.defaultProps = {
  isMarkAsCompleteDisabled: false,
  providerLogos: {},
  toggleSearch: null,
  isStandalone: false
};

InsightV2.propTypes = {
  card: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  dataCard: PropTypes.object,
  team: PropTypes.object,
  author: PropTypes.object,
  channels: PropTypes.object,
  pathname: PropTypes.string,
  cardId: PropTypes.string,
  dismissible: PropTypes.bool,
  isStandaloneModal: PropTypes.bool,
  isActionsDisabled: PropTypes.bool,
  showComment: PropTypes.bool,
  feed: PropTypes.any,
  isMarkAsCompleteDisabled: PropTypes.bool,
  removeCardFromList: PropTypes.func,
  hideComplete: PropTypes.func,
  providerLogos: PropTypes.object,
  toggleSearch: PropTypes.func,
  isStandalone: PropTypes.bool,
  cardUpdated: PropTypes.func,
  params: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname,
    feed: state.feed.toJS(),
    currentUser: state.currentUser.toJS(),
    channels: state.channels.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(InsightV2);
