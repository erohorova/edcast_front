import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqBy from 'lodash/uniqBy';
import concat from 'lodash/concat';
import findIndex from 'lodash/findIndex';

import { fetchCard, getCards } from 'edc-web-sdk/requests/cards';

import ListView from '../common/ListView';
import Card from '../cards/Card';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../actions/teamActivityAction';
import getCardView from '../../utils/getCardView';
import { Permissions } from '../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../utils/calculateTeemFeedPosition';
import updatePageLastVisit from '../../utils/updatePageLastVisit';
import { cardFields } from '../../constants/cardFields';
//Icons

class RecommendedContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      providerLogos: {},
      cards: [],
      isLastPage: false,
      pending: true,
      completedCards: [],
      channels: [],
      users: [],
      learningCardView: window.ldclient.variation('learning-card', false),
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      itemMargin: 0,
      itemsCount: 0,
      limit: 10,
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    this.handleScroll = this.handleScroll.bind(this);

    if (this.state.showNew) {
      updatePageLastVisit('recommendedLastVisit');
    }
  }

  componentDidMount() {
    calculateTeemFeedPosition();
    getCards({
      limit: this.state.limit,
      is_official: true,
      offset: this.state.cards.length,
      filter_by_language: this.state.multilingualContent,
      last_access_at: localStorage.getItem('recommendedLastVisit'),
      fields: cardFields
    })
      .then(data => {
        let isLastPage = data.length < this.state.limit;
        if (!isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
        this.setState(
          {
            cards: data,
            isLastPage,
            pending: false
          },
          calculateTeemFeedPosition
        );
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in RecommendedContainer.getCards.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';
    if (newView !== this.state.viewRegime) {
      this.setState({ cards: [], pending: true });
      getCards({
        limit: this.state.limit,
        is_official: true,
        offset: this.state.cards.length,
        filter_by_language: this.state.multilingualContent,
        last_access_at: localStorage.getItem('recommendedLastVisit'),
        fields: cardFields
      })
        .then(data => {
          let isLastPage = data.length < this.state.limit;
          if (!isLastPage) {
            document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
              'calc(100vh - 11.875rem)';
          }
          this.setState({
            cards: data,
            isLastPage,
            pending: false,
            viewRegime: newView
          });
        })
        .catch(err => {
          console.error(`Error in RecommendedContainer.getCards.func : ${err}`);
        });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    if (this.state.pending) {
      return;
    }
    if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
      if (!this.state.isLastPage) {
        this.loadMore();
        this.getStream();
        setActivityListHeight(this.state.homepageVersion, 'card');
      }
    }
  }

  loadMore() {
    if (!this.state.isLastPage) {
      this.setState({ pending: true }, () => {
        getCards({
          limit: this.state.limit,
          is_official: true,
          offset: this.state.cards.length,
          filter_by_language: this.state.multilingualContent,
          last_access_at: localStorage.getItem('recommendedLastVisit'),
          fields: cardFields
        })
          .then(data => {
            let uniqCards = uniqBy(concat(this.state.cards.concat, data), 'id');
            this.setState(
              {
                cards: uniqCards,
                isLastPage: data.length < this.state.limit,
                pending: false
              },
              calculateTeemFeedPosition
            );
            setActivityListHeight(this.state.homepageVersion, 'card');
          })
          .catch(err => {
            console.error(`Error in RecommendedContainer.loadMore.getCards.func : ${err}`);
          });
      });
    }
  }

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => card.id != id);
    this.setState({ cards: newCardsList }, calculateTeemFeedPosition);
    setActivityListHeight(this.state.homepageVersion, 'card');
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
      }
    }
  }

  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let cards = this.state.cards;
        let findCardIndex = findIndex(cards, item => {
          return item.id == id;
        });
        cards[findCardIndex] = data;
        this.setState({ cards }, calculateTeemFeedPosition);
      })
      .catch(err => {
        console.error(`Error in RecommendedContainer.fetchCard.func : ${err}`);
      });
  };

  render() {
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);

    return (
      <div id="feed">
        <ListView
          paginationRef="referForPagination"
          pending={this.state.pending}
          isLastPage={this.state.isLastPage}
          emptyMessage={
            <div className="empty-message data-not-available-msg">{tr('No recommended cards')}</div>
          }
        >
          {this.state.cards.length > 0 ? (
            <div
              className={
                viewRegime == 'Tile'
                  ? 'custom-card-container align-tileview-feed'
                  : 'custom-card-container'
              }
            >
              <div className="five-card-column vertical-spacing-medium">
                {this.state.cards.map(card => {
                  return (
                    <Card
                      key={card.id}
                      card={card}
                      author={card.author}
                      moreCards={!isRail}
                      dueAt={card.dueAt}
                      startDate={card.startDate}
                      user={this.props.currentUser}
                      type={viewRegime}
                      removeCardFromList={this.removeCardFromList}
                      closeCardModal={this.findCheckedCardAtList}
                    />
                  );
                })}
              </div>
            </div>
          ) : (
            ''
          )}
        </ListView>
      </div>
    );
  }
}

RecommendedContainer.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  route: PropTypes.object,
  cards: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS(),
  cards: state.cards.toJS()
}))(RecommendedContainer);
