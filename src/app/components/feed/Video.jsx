import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class Video extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      videoUrl: props.videoUrl
    };
  }

  render() {
    return (
      <div className="resource">
        {this.state.videoUrl && (
          <video width="100%" height="240" controls controlsList={this.props.controlsList}>
            <source src={this.state.videoUrl} />
            {tr('Your browser does not support the video tag.')}
          </video>
        )}
      </div>
    );
  }
}

Video.propTypes = {
  controlsList: PropTypes.string,
  videoUrl: PropTypes.string
};

export default connect()(Video);
