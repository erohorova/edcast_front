import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import TimeAgo from 'react-timeago';
import Linkify from 'react-linkify';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import ListItem from 'material-ui/List/ListItem';
import IconButton from 'material-ui/IconButton';

import Avatar from 'edc-web-sdk/components/Avatar';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors/index';
import More from 'edc-web-sdk/components/icons/More';
import { deleteComment } from 'edc-web-sdk/requests/cards';

import extractMentions from '../../utils/extractMentions';
import translateTA from '../../utils/translateTimeAgo';
import { Permissions } from '../../utils/checkPermissions';

import ConfirmationModal from '../modals/ConfirmationCommentModal';

import UserProfileHover from '../common/UserProfileHover';

//actions
import { toggleLikeComment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { openCommentReasonModal } from '../../actions/modalActions';

class CommentItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      openConfirm: false,
      upVoted: this.props.comment.isUpvoted,
      votesCount: this.props.comment.votesCount || 0,
      visible: true,
      truncatedText: this.props.comment.message.length > 145,
      reportingInappropriateComment: window.ldclient.variation(
        'reporting-inappropriate-comment',
        false
      )
    };
    this.styles = {
      listItem: {
        padding: '0 36px 0 64px',
        margin: '0 -1rem'
      },
      more: {
        color: '#4a90e2',
        marginLeft: '0.5rem'
      },
      likes: {
        display: 'inline-block',
        color: '#4a90e2'
      },
      timeStyleV3: {
        fontSize: '0.75rem',
        color: '#26273b',
        paddingLeft: '0.625rem',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      },
      timeStyle: {
        fontSize: '0.75rem',
        color: '#26273b',
        paddingLeft: '0.625rem',
        borderLeft: '0.0625rem solid #6f708b',
        fontWeight: 'normal',
        fontStyle: 'normal',
        fontStretch: 'normal',
        letterSpacing: '0.00625rem'
      },
      votes: {
        marginLeft: '4px'
      },
      avatar: {
        top: '0',
        width: '32px',
        height: '32px'
      },
      primaryColor: {
        color: '#4a90e2'
      },
      primaryColorV3: {
        color: '#4a90e2',
        display: 'inline-flex',
        alignItems: 'center'
      },
      primaryBorderColor: {
        borderColor: '#6f708b'
      },
      displayInlineBlock: {
        display: 'inline-block'
      },
      commentDetails: {
        width: '97%',
        display: 'inline-block'
      },
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '26px',
        backgroundColor: '#000'
      },
      alignViewOption: {
        width: 'inherit',
        height: '33px',
        padding: 'inherit'
      },
      allowVoteCursor: {
        cursor: Permissions.has('LIKE_CONTENT') ? 'pointer' : 'not-allowed'
      }
    };
    this.allowVote = Permissions.has('LIKE_CONTENT');
  }

  likeClickHandler = e => {
    e.preventDefault();
    if (!this.allowVote) {
      return;
    }
    toggleLikeComment(this.props.cardId, this.props.comment.id, !this.state.upVoted)
      .then(data => {
        this.setState({
          upVoted: !this.state.upVoted,
          votesCount: data.upvoted ? this.state.votesCount + 1 : this.state.votesCount - 1
        });
      })
      .catch(err => {
        console.error(`Error in CommentItem.v3.toggleLikeComment.func : ${err}`);
      });
  };

  updateState = () => {
    this.setState({ visible: false }, () => {
      this.props.commentPostCallback(-1);
    });
  };

  openReasonReportModal = () => {
    let comment = this.props.comment;
    let isAlreadyReported =
      this.props.users && this.props.users.reported_comment_ids
        ? this.props.users.reported_comment_ids.indexOf(this.props.comment.id) >= 0
        : false;
    if (comment.isReported || isAlreadyReported) {
      this.props.dispatch(openSnackBar('You have already reported this comment', true));
    } else {
      this.props.dispatch(openCommentReasonModal(this.props.comment));
    }
  };

  confirmDelete = () => {
    this.setState({ openConfirm: false });
    deleteComment(this.props.cardId, this.props.comment.id)
      .then(() => {
        this.updateState();
      })
      .catch(err => {
        console.error(`Error in CommentItem.v3.deleteComment.func : ${err}`);
      });
  };

  toggleModal = () => {
    if (this.props.hideCommentModal) {
      this.props.showConfirmationModal(this.props.cardId, this.props.comment.id, this.updateState);
    } else {
      this.setState({ openConfirm: !this.state.openConfirm });
    }
  };

  truncateMessageText = message => {
    return message.substr(0, 140);
  };

  fullMessage = () => {
    this.setState({ truncatedText: false });
  };

  isReportable = () => {
    return this.state.reportingInappropriateComment && this.props.canReport;
  };

  isDeletable = () => {
    return this.props.isOwn;
  };

  render() {
    if (!this.state.visible) {
      return null;
    }
    let user = this.props.user;
    let message = extractMentions(this.props.comment.message);
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);

    const { currentUser } = this.props;
    return (
      <div className="comment-list-item">
        <div className="comment-details" style={this.styles.commentDetails}>
          <ListItem
            disabled
            style={this.styles.listItem}
            leftAvatar={
              <Avatar
                className="pointer"
                user={user}
                style={this.styles.avatar}
                onClick={() => {
                  this.props.dispatch(
                    push(
                      `/${
                        (user.handle && user.handle.replace('@', '')) === currentUser.handle
                          ? 'me'
                          : user.handle
                      }`
                    )
                  );
                }}
              />
            }
            primaryText={
              <div className="break-comment">
                <span>
                  <span>
                    <a
                      className="comment-user"
                      href={`/${
                        (user.handle && user.handle.replace('@', '')) === currentUser.handle
                          ? 'me'
                          : user.handle
                      }`}
                    >
                      {user.firstName} {user.lastName}
                    </a>
                  </span>
                </span>
                <span className="comment-text">
                  {message.map((part, index) => {
                    if (part.type === 'text') {
                      if (this.state.truncatedText) {
                        return (
                          <span key={index}>
                            <Linkify properties={{ target: '_blank' }}>
                              {this.truncateMessageText(part.text)}
                            </Linkify>
                          </span>
                        );
                      } else {
                        return (
                          <span key={index}>
                            <Linkify properties={{ target: '_blank' }}>{part.text}</Linkify>
                          </span>
                        );
                      }
                    }
                    if (part.type === 'handle') {
                      let commenter = this.props.comment.mentions.filter(
                        mention => mention.handle === part.handle
                      );
                      if (commenter.length) {
                        return (
                          <UserProfileHover
                            user={commenter[0]}
                            userProfileURL={`/${
                              (part.handle && part.handle.replace('@', '')) === currentUser.handle
                                ? 'me'
                                : part.handle
                            }`}
                          />
                        );
                      } else {
                        return <span key={index}>{part.handle}</span>;
                      }
                    }
                  })}
                  {this.state.truncatedText && (
                    <span
                      style={this.styles.more}
                      className="more-text-comment"
                      onClick={this.fullMessage}
                    >
                      {tr('more')}
                    </span>
                  )}
                </span>
              </div>
            }
          />
          {this.props.comment.id && (
            <ListItem
              disabled
              insetChildren={true}
              style={this.styles.listItem}
              primaryText={
                <div>
                  <div
                    className="comment-actions"
                    style={
                      this.props.isCardV3 ? this.styles.primaryColorV3 : this.styles.primaryColor
                    }
                  >
                    <div className="likes">
                      {this.allowVote && (
                        <a
                          href="#"
                          onClick={this.likeClickHandler}
                          style={this.styles.allowVoteCursor}
                        >
                          <span>
                            <div style={this.styles.likes}>
                              {this.state.upVoted ? tr('Liked') : tr('Like')}
                            </div>
                          </span>
                          <div style={this.styles.displayInlineBlock}>
                            {this.state.votesCount > 0 && (
                              <span style={this.styles.votes}> {this.state.votesCount}</span>
                            )}
                          </div>
                        </a>
                      )}
                    </div>
                    {this.props.isCardV3 && this.allowVote && <div className="dot" />}
                    <div className="comment-time-ago">
                      <TimeAgo
                        date={new Date(this.props.comment.createdAt)}
                        live={false}
                        style={
                          this.props.isCardV3 ? this.styles.timeStyleV3 : this.styles.timeStyle
                        }
                        formatter={formatter}
                      />
                    </div>
                  </div>
                </div>
              }
            />
          )}
        </div>
        {(this.isReportable() || this.isDeletable()) && (
          <IconMenu
            className="insight-dropdown vertical-insight-dropdown"
            iconButtonElement={
              <IconButton style={this.styles.alignViewOption}>
                <More color={colors.gray} />
              </IconButton>
            }
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            targetOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            listStyle={this.styles.iconMenu}
            menuStyle={this.styles.menuItem}
            animated={false}
          >
            {this.isReportable() && (
              <MenuItem
                style={this.styles.menuItem}
                primaryText={tr('Report it')}
                onTouchTap={this.openReasonReportModal}
              />
            )}
            {this.isDeletable() && (
              <MenuItem
                style={this.styles.menuItem}
                primaryText={tr('Delete')}
                onTouchTap={this.toggleModal}
              />
            )}
          </IconMenu>
        )}
        {this.state.openConfirm && !this.props.hideCommentModal && (
          <ConfirmationModal
            title={'Confirm'}
            message={'Do you want to delete the comment?'}
            closeModal={this.toggleModal}
            callback={this.confirmDelete}
          />
        )}
      </div>
    );
  }
}

CommentItem.propTypes = {
  cardId: PropTypes.string,
  users: PropTypes.object,
  user: PropTypes.object,
  currentUser: PropTypes.object,
  comment: PropTypes.shape({
    message: PropTypes.string,
    isUpvoted: PropTypes.bool,
    votesCount: PropTypes.number,
    mentions: PropTypes.any,
    userId: PropTypes.string,
    createdAt: PropTypes.string,
    id: PropTypes.string
  }),
  hideCommentModal: PropTypes.bool,
  isOwn: PropTypes.bool,
  canReport: PropTypes.bool,
  commentPostCallback: PropTypes.func,
  showConfirmationModal: PropTypes.func,
  isCardV3: PropTypes.bool
};

CommentItem.defaultProps = {
  hideCommentModal: false,
  showConfirmationModal: function() {}
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    users: state.users.toJS()
  };
}

export default connect(mapStoreStateToProps)(CommentItem);
