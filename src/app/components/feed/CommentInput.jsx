import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import BlurImage from '../common/BlurImage';
//UI components
import TextField from 'material-ui/TextField';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';
import ListItem from 'material-ui/List/ListItem';
//actions
import { postComment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class CommentInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
      inputValue: '',
      mentionQuery: '',
      sendingComment: false,
      userTagging: window.ldclient.variation('user-tagging', true)
    };
    this.styles = {
      userListPopover: {
        height: '112px',
        overflow: 'scroll'
      },
      anchorOrigin: {
        horizontal: 'left',
        vertical: 'bottom'
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'top'
      },
      commentInputhint: {
        marginTop: '12px',
        bottom: 'inherit'
      },
      avatarBox: {
        userSelect: 'none',
        height: '2.3rem',
        width: '2.3rem',
        position: 'absolute',
        top: '0.2rem',
        left: '1rem'
      }
    };
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.inputBlurHandler = this.inputBlurHandler.bind(this);
    this.submitInputHandler = this.submitInputHandler.bind(this);
  }

  componentDidMount() {
    if (this.props.autoFocus) {
      this.refs.input.focus();
    }
  }

  inputChangeHandler(event) {
    let string = event.target.value;
    let parts = string.split(' ');
    if (parts.length && parts[parts.length - 1].charAt(0) === '@') {
      let mentionQuery = parts[parts.length - 1].slice(1).toLowerCase();

      let { userTagging } = this.state;
      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }

        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        inputValue: string,
        open: true,
        anchorEl: event.currentTarget,
        mentionQuery
      });
    } else {
      this.setState({ inputValue: string, open: false, mentionQuery: '' });
    }
  }

  inputBlurHandler(event) {
    this.setState({ open: false });
  }

  mentionClickHandler(e, handle) {
    e.preventDefault();
    let tempHandle = this.state.inputValue;
    let tempHandleArr = this.state.inputValue.split(' ');
    if (tempHandleArr[tempHandleArr.length - 1].charAt(0) == '@') {
      tempHandle = tempHandleArr.pop(tempHandleArr[tempHandleArr.length - 1]);
      tempHandle = tempHandleArr.join(' ');
    } else if (tempHandleArr[tempHandleArr.length - 2].charAt(0) == '@') {
      tempHandleArr.splice(tempHandleArr.length - 2, tempHandleArr.length);
      tempHandle = tempHandleArr.join(' ');
    }
    tempHandle
      ? this.setState({ inputValue: tempHandle + ' ' + handle + ' ' })
      : this.setState({ inputValue: handle + ' ' });
    setTimeout(() => {
      this.setState({ open: false });
      this.refs.input.focus();
    }, 300);
  }

  submitInputHandler(e) {
    if (this.state.inputValue.trim() !== '') {
      if (e.keyCode === 13) {
        this.setState({ sendingComment: true }, () => {
          postComment(
            this.props.cardId,
            this.state.inputValue,
            this.props.numOfComments + 1,
            this.props.cardType,
            this.props.currentUser.get('id')
          ).then(data => {
            if (data && data.embargoErr) {
              this.setState({ sendingComment: false });
              this.props.dispatch(
                openSnackBar(
                  'Sorry, the content you are trying to post is from unapproved website or words.',
                  true
                )
              );
            } else {
              this.setState({
                inputValue: '',
                sendingComment: false
              });
              this.props.commentPostCallback(data);
            }
          });
        });
      }
    }
  }

  render() {
    let usersMap = this.props.users.get('idMap');
    let usersArr = usersMap.filter(user => {
      let userName = user.get('fullName') || user.get('name');
      let userHandle = user.get('handle');
      return (
        (userName && userName.toLowerCase().indexOf(this.state.mentionQuery) !== -1) ||
        (userHandle && userHandle.toLowerCase().indexOf(this.state.mentionQuery) !== -1)
      );
    });
    return (
      <div className={this.props.inModal ? 'comment-input-modal' : ''}>
        <TextField
          ref="input"
          hintText={tr('Leave a comment / Tag peers...')}
          hintStyle={this.styles.commentInputhint}
          autoComplete="off"
          className="comment create"
          fullWidth={true}
          multiLine={true}
          onChange={this.inputChangeHandler}
          onBlur={this.inputBlurHandler}
          value={this.state.inputValue}
          onKeyDown={this.submitInputHandler}
          autoFocus={true}
          disabled={this.state.sendingComment}
          aria-label={tr('Leave a comment / Tag peers...')}
        />

        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={this.styles.anchorOrigin}
          targetOrigin={this.styles.targetOrigin}
          useLayerForClickAway={false}
          style={this.styles.userListPopover}
          canAutoPosition={true}
          animated={false}
        >
          {usersArr.map((user, index) => {
            let userNameOrHandle =
              user.get('name') == null ? user.get('handle').replace('@', '') : user.get('name');
            return (
              <ListItem
                key={index}
                leftAvatar={
                  <BlurImage
                    style={this.styles.avatarBox}
                    id={user.id}
                    image={user.get('avatarimages').get('small')}
                  />
                }
                primaryText={<span>{userNameOrHandle}</span>}
                onMouseDown={e => {
                  this.mentionClickHandler(e, user.get('handle'));
                }}
              />
            );
          })}
        </Popover>
      </div>
    );
  }
}

CommentInput.propTypes = {
  cardId: PropTypes.string.isRequired,
  cardType: PropTypes.string,
  users: PropTypes.object,
  currentUser: PropTypes.object,
  comment: PropTypes.object,
  commentPostCallback: PropTypes.func,
  autoFocus: PropTypes.bool,
  inModal: PropTypes.bool,
  numOfComments: PropTypes.number
};

export default connect(state => ({ users: state.users, currentUser: state.currentUser }))(
  CommentInput
);
