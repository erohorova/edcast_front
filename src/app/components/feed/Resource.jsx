import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import InlineVideo from './InlineVideo.jsx';
import Lightbox from 'react-images';
import { tr } from 'edc-web-sdk/helpers/translations';
import getDefaultImage from '../../utils/getDefaultCardImage';
import addSecurity from '../../utils/filestackSecurity';

class Resource extends Component {
  constructor(props, context) {
    super(props, context);
    this.linkExtract = this.linkExtract.bind(this);
    this.closeLightbox = this.closeLightbox.bind(this);

    this.state = {
      isOpen: false
    };
  }

  linkExtract() {
    let url =
      typeof this.props.resource.url !== 'undefined'
        ? this.props.cardId
          ? '/insights/' + encodeURIComponent(this.props.cardId) + '/visit'
          : this.props.resource.url
        : '/insights/' + this.props.cardId;
    return url;
  }

  closeLightbox() {
    this.setState({ isOpen: false });
  }

  render() {
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let resourceClassName = '';
    let resource = this.props.resource;
    if (resource.title || resource.url) {
      resourceClassName = 'media-container container-padding';
    }
    let description = resource.description || '';
    let resourceDescription =
      description.length > 300 ? description.substring(0, 300) + '...' : description;
    let resourceUrl = this.linkExtract();
    return (
      <div className="resource">
        {!resource.embedHtml &&
          resource.imageUrl != undefined &&
          resource.imageUrl &&
          !resource.url && (
            <a href={resourceUrl} target="_blank">
              <img className="openFullImage" src={resource.imageUrl} />
            </a>
          )}
        <a title={tr('open in new tab')} className="matte-hover" href={resourceUrl} target="_blank">
          {resource != undefined && (resource.url || resource.description || resource.fileUrl) && (
            <div className={resourceClassName}>
              {resource.imageUrl &&
                !resource.embedHtml &&
                resource.type !== 'Video' &&
                !resource.siteName !== 'YouTube' && (
                  <img
                    src={addSecurity(resource.imageUrl, expireAfter, this.props.currentUser.id)}
                  />
                )}
              {!resource.imageUrl &&
                !resource.embedHtml &&
                resource.type !== 'Video' &&
                !resource.siteName !== 'YouTube' && (
                  <img src={getDefaultImage(this.props.currentUser.id).url} />
                )}
              {resource.embedHtml && (resource.videoUrl && resource.type === 'Video') && (
                <InlineVideo
                  cardId={this.props.cardId}
                  embedHtml={
                    resource.embedHtml ||
                    `<iframe width="1" src="${addSecurity(
                      resource.videoUrl,
                      expireAfter,
                      this.props.currentUser.id
                    )}"></iframe>`
                  }
                  videoUrl={resource.videoUrl}
                />
              )}
              {!resource.embedHtml && resource.siteName === 'YouTube' && resource.imageUrl && (
                <a href={resourceUrl} target="_blank">
                  <img style={{ maxWidth: '100%' }} src={resource.imageUrl} />
                </a>
              )}
              {resource.fileUrl && (
                <img src={addSecurity(resource.fileUrl, expireAfter, this.props.currentUser.id)} />
              )}
              <div>
                {(resource.title || resource.url) && (
                  <h5
                    className="resource-title"
                    dangerouslySetInnerHTML={{ __html: resource.title || resource.url }}
                  />
                )}
                <div dangerouslySetInnerHTML={{ __html: resourceDescription }} />
              </div>
            </div>
          )}
        </a>
        {resource.imageUrl && !resource.url && (
          <Lightbox
            images={[
              { src: addSecurity(resource.imageUrl, expireAfter, this.props.currentUser.id) }
            ]}
            isOpen={this.state.isOpen}
            onClose={this.closeLightbox}
            showImageCount={false}
          />
        )}
      </div>
    );
  }
}

Resource.propTypes = {
  providerLogos: PropTypes.object,
  cardId: PropTypes.string,
  resource: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

export default connect()(Resource);
