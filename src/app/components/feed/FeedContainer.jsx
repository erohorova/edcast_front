import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getInsights } from '../../actions/feedActions';
import EmptyFeedContainer from './EmptyFeedContainer.jsx';
import { getProviderLogos } from 'edc-web-sdk/requests/ecl';
import { feed } from 'edc-web-sdk/requests/index';
import { getChannels } from 'edc-web-sdk/requests/channels.v2';
import { getSuggestedUsers } from 'edc-web-sdk/requests/users';
import Card from '../cards/Card';
import {
  getStream,
  setActivityListHeight,
  recieveActivityFeed
} from '../../actions/teamActivityAction';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';
import { fetchCard } from 'edc-web-sdk/requests/cards';
import getCardView from '../../utils/getCardView';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { Permissions } from '../../utils/checkPermissions';
import calculateTeemFeedPosition from '../../utils/calculateTeemFeedPosition';
import updatePageLastVisit from '../../utils/updatePageLastVisit';
import Spinner from '../common/spinner';
import * as upshotActions from '../../actions/upshotActions';
import { cardFields } from '../../constants/cardFields';
//Icons

const limit = 9;

class FeedContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      providerLogos: {},
      cards: [],
      isLastPage: false,
      pending: true,
      completedCards: [],
      channels: [],
      users: [],
      learningCardView: window.ldclient.variation('learning-card', false),
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      viewRegime:
        props.team.Feed && props.team.Feed[props.route.feedKey]
          ? props.team.Feed[props.route.feedKey].defaultCardView
          : 'Tile',
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      itemMargin: 0,
      itemsCount: 0,
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
    this.handleScroll = this.handleScroll.bind(this);

    if (this.state.showNew) {
      updatePageLastVisit('teamLearningLastVisit');
    }
  }
  async componentWillMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
  }

  componentDidMount() {
    calculateTeemFeedPosition();
    window.addEventListener('scroll', this.handleScroll);
    this.refreshFeed();
  }

  componentWillUnmount() {
    localStorage.setItem('teamLearningLastVisit', Math.round(new Date().getTime() / 1000));
    window.removeEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let newView =
      nextProps.team.Feed && nextProps.team.Feed[nextProps.route.feedKey]
        ? nextProps.team.Feed[nextProps.route.feedKey].defaultCardView
        : 'Tile';

    if (newView !== this.state.viewRegime) {
      this.refreshFeed(newView);
    }
    if (nextProps.cards !== undefined && nextProps.cards.completedCard !== undefined) {
      let completedCards = this.state.completedCards;
      completedCards.push(nextProps.cards.completedCard);
      this.setState({
        completedCards: completedCards
      });
      setActivityListHeight(this.state.homepageVersion, 'card');
    }
    if (!!nextProps.cards) {
      this.checkCardList();
    }
  }

  checkCardList() {
    let cards = this.state.cards.filter(card => !card.featuredHidden);
    this.setState({ cards }, calculateTeemFeedPosition);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage && !this.state.isLastSabaPage) {
          this.loadMore();
          this.getStream();
        }
      }
    },
    150,
    { leading: false }
  );

  refreshFeed = (newView = null) => {
    this.setState({ cards: [], pending: true });
    feed
      .getFeed(
        limit,
        0,
        this.state.multilingualContent,
        localStorage.getItem('teamLearningLastVisit'),
        cardFields
      )
      .then(data => {
        let isLastPage = data.cards.length === 0;
        if (data.cards.length === 0) {
          let p1 = getChannels();
          let p2 = getSuggestedUsers();
          Promise.all([p1, p2])
            .then(values => {
              let filteredChannels = values[0]
                ? values[0].filter(channel => !channel.isFollowing)
                : [];
              let newStates = {
                channels: filteredChannels,
                users: values[1],
                isLastPage,
                pending: false
              };
              if (newView !== null) {
                newStates['viewRegime'] = newView;
              }
              this.setState(newStates);
            })
            .catch(err => {
              console.error(`Error in FeedContainer.Promise.all.func : ${err}`);
            });
        } else {
          this.setState(
            { cards: data.cards, isLastPage, pending: false },
            calculateTeemFeedPosition
          );
        }
        if (!isLastPage) {
          document.getElementsByClassName('footer-wrapped-content')[0].style.minHeight =
            'calc(100vh - 11.875rem)';
        }
        setActivityListHeight(this.state.homepageVersion, 'card');
      })
      .catch(err => {
        console.error(`Error in FeedContainer.getFeed.func : ${err}`);
      });
    getProviderLogos()
      .then(providerLogos => {
        this.setState({
          providerLogos: providerLogos
        });
      })
      .catch(err => {
        console.error(`Error in FeedContainer.getProviderLogos.func : ${err}`);
      });
  };

  loadMore() {
    if (!this.state.isLastPage && !this.state.isLastSabaPage) {
      this.setState(
        {
          pending: true
        },
        () => {
          calculateTeemFeedPosition();
          feed
            .getFeed(
              limit,
              this.state.cards.length,
              this.state.multilingualContent,
              localStorage.getItem('teamLearningLastVisit'),
              cardFields
            )
            .then(data => {
              let isLastPage = data.cards.length === 0;
              if (isLastPage) {
                window.removeEventListener('scroll', this.handleScroll);
              }
              this.setState(
                { cards: this.state.cards.concat(data.cards), isLastPage, pending: false },
                calculateTeemFeedPosition
              );
              setActivityListHeight(this.state.homepageVersion, 'card');
            })
            .catch(err => {
              console.error(`Error in FeedContainer.loadMore.getFeed.func : ${err}`);
            });
        }
      );
    }
  }
  findCheckedCardAtList = id => {
    fetchCard(id)
      .then(data => {
        let cards = this.state.cards;
        let findCardIndex = findIndex(cards, item => {
          return item.id == id;
        });
        cards[findCardIndex] = data;
        this.setState({ cards }, calculateTeemFeedPosition);
      })
      .catch(err => {
        console.error(`Error in FeedContainer.findCheckedCardAtList.fetchCard.func : ${err}`);
      });
  };

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => card.id !== id);
    this.setState({ cards: newCardsList }, calculateTeemFeedPosition);
    setActivityListHeight(this.state.homepageVersion, 'card');
  };

  getStream() {
    if (this.state.newTeamActivity) {
      this.props.dispatch(recieveActivityFeed(true));
    } else {
      if (this.state.homepageVersion === 'right-sidebar') {
        this.props.dispatch(getStream(150, true, 'card'));
      }
    }
  }
  sendLearnPageUpshotEvent(feedKey, payload) {
    if (feedKey === 'feed/teamLearning') {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
    }
  }

  render() {
    let cards = this.state.cards
      .filter(
        card =>
          (!card.completionState ||
            (!!card.completionState && card.completionState.toUpperCase() !== 'COMPLETED')) &&
          !~this.state.completedCards.indexOf(card.id)
      )
      .filter(card => !(card.cardType && card.cardType === 'pack_draft'));
    let isRail = false;
    let leftRail = this.props.team.OrgConfig.leftRail;
    if (
      !!leftRail &&
      !!leftRail['web/leftRail/myLearningQueue'] &&
      !!leftRail['web/leftRail/teamActivity']
    ) {
      isRail =
        this.state.homepageVersion === 'left-sidebar'
          ? leftRail['web/leftRail/myLearningQueue'].visible ||
            leftRail['web/leftRail/teamActivity'].visible
          : leftRail['web/leftRail/teamActivity'].visible;
    }
    let viewRegime = getCardView(this.props.team.Feed, this.props.route.feedKey);
    let isShowComment = Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT');
    return (
      <section id="feed">
        <div className="vertical-spacing-large referForPagination">
          {!this.state.pending && this.state.cards.length === 0 && (
            <EmptyFeedContainer
              users={this.state.users}
              channels={this.state.channels}
              currentUser={this.props.currentUser}
              getCards={this.refreshFeed}
            />
          )}

          <div
            className={
              viewRegime == 'Tile'
                ? 'custom-card-container align-tileview-feed'
                : 'custom-card-container'
            }
          >
            <div className="five-card-column vertical-spacing-medium">
              {cards.map(filteredCard => {
                return (
                  <Card
                    key={filteredCard.id}
                    card={filteredCard}
                    author={filteredCard.author}
                    providerLogos={this.state.providerLogos}
                    dismissible={false}
                    dueAt={
                      filteredCard.dueAt ||
                      (filteredCard.assignment && filteredCard.assignment.dueAt)
                    }
                    startDate={
                      filteredCard.startDate ||
                      (filteredCard.assignment && filteredCard.assignment.startDate)
                    }
                    moreCards={viewRegime === 'List' ? false : !isRail}
                    user={this.props.currentUser}
                    removeCardFromList={this.removeCardFromList}
                    closeCardModal={this.findCheckedCardAtList}
                    type={viewRegime}
                    fullView={true}
                    showComment={isShowComment}
                    isStandalone={false}
                    sendLearnPageUpshotEvent={this.sendLearnPageUpshotEvent}
                    feedKey={this.props.route.feedKey}
                  />
                );
              })}
            </div>
          </div>

          {this.state.pending && (
            <div className="progress">
              <Spinner />
            </div>
          )}
        </div>
      </section>
    );
  }
}

FeedContainer.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  route: PropTypes.object,
  cards: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS(),
  cards: state.cards.toJS()
}))(FeedContainer);
