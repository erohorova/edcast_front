import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import EdcastLogoPlus from 'edc-web-sdk/components/icons/EdcastLogoPlus';
import { tr } from 'edc-web-sdk/helpers/translations';

class MobileBanner extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      edLogoStyle: {
        margin: 'auto auto',
        height: '1.6875rem',
        width: '50px'
      }
    };
  }

  render() {
    let ifAndroid = /Android/i.test(navigator.userAgent);
    let promotions = this.props.team.config.footer_options.promotions;
    return (
      <div id="mobile-banner">
        <div className="">
          <div className="logos">
            <EdcastLogoPlus style={this.styles.edLogoStyle} />
            <img src={this.props.team && this.props.team.coBrandingLogo} />
          </div>
        </div>
        <div className="app-message">
          {tr('Get our free App for Apple or Android')}
          <br /> {tr('and learn anywhere!')}
        </div>
        <div className="app-store-logos">
          <img src="https://s3.amazonaws.com/edc-dev-web/assets/android-mono.png" />
          <img src="https://s3.amazonaws.com/edc-dev-web/assets/apple-mono.png" />
        </div>
        {promotions && (
          <div className="download-btn-container">
            <a
              href={
                ifAndroid
                  ? 'https://play.google.com/store/apps/details?id=com.edcast'
                  : 'https://itunes.apple.com/app/apple-store/id974833832?pt=100220803&ct=KKWebsiteLink&mt=8'
              }
              className="download-btn"
            >
              {tr('DOWNLOAD APP')}
            </a>
          </div>
        )}
      </div>
    );
  }
}

MobileBanner.propTypes = {
  team: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(MobileBanner);
