import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Material UI Components
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';

import Channel from '../discovery/Channel.jsx';
import Influencer from '../discovery/Influencer.jsx';

import { postOnboardingStep } from '../../actions/onboardingActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class UserRecommendations extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      actionBar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#eee',
        width: '100%',
        marginLeft: '-24px',
        padding: '8px'
      },
      infoIcon: {
        height: '20px',
        width: '20px'
      }
    };

    this.state = {
      minChannelsFollowed: true
    };

    this.handleNextScreen = this.handleNextScreen.bind(this);
  }

  handleNextScreen() {
    let channelsFollowingCount = 0;
    let minChannelsFollowingCount = 0;

    if (this.props.channelIds.length <= 1) {
      minChannelsFollowingCount = this.props.channelIds.length;
    } else {
      minChannelsFollowingCount = 1;
    }

    this.props.channelIds.forEach(id => {
      if (this.props.channels[id].following) {
        channelsFollowingCount = channelsFollowingCount + 1;
      }
    });

    if (channelsFollowingCount < minChannelsFollowingCount) {
      this.setState({ minChannelsFollowed: false });
    } else {
      this.setState({ minChannelsFollowed: true });
    }

    // Since state changes are async, the timeout is required to make sure
    // that the state was updated.
    setTimeout(() => {
      if (this.state.minChannelsFollowed) {
        let user = {
          id: this.props.user.id
        };
        this.props.dispatch(postOnboardingStep(user, 3));
        this.props.handleNext();
      }
    }, 300);
  }

  render() {
    let channels = this.props.channelIds.map(id => {
      return this.props.channels[id];
    });
    return (
      <div style={{ marginBottom: '80px' }}>
        <h6>
          {tr(
            'Follow some channels and subject matter experts so that we can create a personalized learning feed for you.'
          )}
          {tr('You can always add or change these later.')}
        </h6>
        <div>
          <div className="row">
            <div className="small-12 columns">
              <div className="clearfix">
                <div className="float-left">
                  {this.state.minChannelsFollowed && (
                    <p style={{ marginTop: '11px' }}>{tr('Follow 1 or more channels')}</p>
                  )}
                  {!this.state.minChannelsFollowed && (
                    <p style={{ marginTop: '11px', color: 'red' }}>
                      {tr('Follow 1 or more channels')}
                    </p>
                  )}
                </div>
                <div className="float-left">
                  <IconButton
                    iconStyle={this.styles.infoIcon}
                    tooltipPosition="top-center"
                    tooltipStyles={{ lineHeight: '12px', padding: '4px' }}
                    tooltip={
                      <span>
                        {tr('Channels contain content organized by topics. Users can follow one')}{' '}
                        <br />
                        {tr('or more channels to receive content daily in their feed.')}
                      </span>
                    }
                  >
                    <InfoIcon />
                  </IconButton>
                </div>
              </div>
            </div>
          </div>

          <div className="row discovery-contents">
            {channels.map((channel, idx) => {
              return (
                <div key={idx} className="small-12 medium-4 large-expand columns">
                  <Channel
                    channel={channel}
                    id={channel.id}
                    isFollowing={channel.following}
                    followPending={channel.followPending}
                    title={channel.label}
                    imageUrl={channel.bannerUrl}
                    slug={channel.slug}
                    disableLink={true}
                    allowFollow={channel.allowFollow}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div style={this.styles.actionBar} className="clearfix">
          <PrimaryButton
            label="Next"
            className="float-right nextOnboarding"
            onTouchTap={this.handleNextScreen}
          />
          <SecondaryButton
            label="Back"
            className="float-left prevOnboarding"
            onTouchTap={this.props.handlePrev}
          />
        </div>
      </div>
    );
  }
}

UserRecommendations.propTypes = {
  influencerIds: PropTypes.array,
  channelIds: PropTypes.array,
  users: PropTypes.object
};

export default connect(state => ({ users: state.users.toJS(), channels: state.channels.toJS() }))(
  UserRecommendations
);
