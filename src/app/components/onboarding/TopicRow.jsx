import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { fetchTopicSkills } from '../../actions/onboardingActions';

import SkillRow from './SkillRow';

class TopicRow extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      skills: props.skills ? props.skills : {},
      selectedDomainTopics: [],
      activeTopicId: props.activeTopicId
    };
    this.fetchSkills = this.fetchSkills.bind(this);
    this.ViewMoreSkills = this.ViewMoreSkills.bind(this);
    this.storeSelectedTopicsState = this.storeSelectedTopicsState.bind(this);
    this.topicBlockClass = this.topicBlockClass.bind(this);
    this.updateActiveTopicId = this.updateActiveTopicId.bind(this);
    this.skillCount = this.skillCount.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      skills: nextProps.skills,
      activeTopicId: nextProps.activeTopicId
    });
  }

  skillCount(topicId) {
    let a = this.state.selectedDomainTopics;
    if (a.hasOwnProperty(`topic-${topicId}`)) {
      return a[`topic-${topicId}`].length;
    }
    return 0;
  }

  storeSelectedTopicsState(selectedDomainTopics, action) {
    let key = '';
    for (var m in selectedDomainTopics) {
      key = m;
    }
    let a = this.state.selectedDomainTopics;
    a[key] = selectedDomainTopics[key];
    if (a[key] && a[key].length == 0) {
      delete a[key];
    }
    this.setState({ selectedDomainTopics: a });
  }

  fetchSkills = (topicId, rowIndex) => {
    this.setState({ [`topic-${topicId}-open`]: true });
    this.props.updateActiveTopicId(topicId);
    if (
      !this.state[`topic-${topicId}-open`] &&
      this.props.section == 'expertiseSection' &&
      (this.props.todayLearning['expertiseSkills'][`skills-${rowIndex}`] === undefined ||
        this.props.todayLearning['expertiseSkills'][`skills-${rowIndex}`][`skill-${topicId}`] ===
          undefined)
    ) {
      this.props.dispatch(fetchTopicSkills(topicId, rowIndex, 1, 'expertiseSection'));
    } else {
      if (
        (!this.state[`topic-${topicId}-open`] &&
          this.props.todayLearning['skills'][`skills-${rowIndex}`] === undefined) ||
        this.props.todayLearning['skills'][`skills-${rowIndex}`][`skill-${topicId}`] === undefined
      ) {
        this.props.dispatch(fetchTopicSkills(topicId, rowIndex, 1, 'topics'));
      }
    }
  };

  ViewMoreSkills(topicId, page) {
    this.props.updateActiveTopicId(topicId);
    this.props.dispatch(
      fetchTopicSkills(
        topicId,
        this.props.rowIndex,
        page,
        this.props.section == 'expertiseSection' ? 'expertiseSection' : 'topics'
      )
    );
  }

  updateActiveTopicId(topicId) {
    this.props.updateActiveTopicId(topicId);
  }

  topicBlockClass(topicId) {
    let topicClass = 'topic-block';
    if (this.state.activeTopicId == topicId) {
      topicClass = topicClass + ' active';
    }

    if (this.state.selectedDomainTopics.hasOwnProperty(`topic-${topicId}`)) {
      topicClass = topicClass + ' selected';
    }

    return topicClass;
  }

  render() {
    let rowIndex = this.props.rowIndex;
    return (
      <div className={'topic-row-' + rowIndex}>
        <div className="row">
          {this.props.row.map((eachTopic, i) => {
            let classname = this.topicBlockClass(eachTopic.id);
            let selectedSkillCount = this.skillCount(eachTopic.id);
            return (
              <div
                key={eachTopic.id}
                className={'small-3 columns text-center topic-block' + rowIndex}
              >
                <div
                  className={classname}
                  onClick={this.fetchSkills.bind(this, eachTopic.id, rowIndex)}
                >
                  {selectedSkillCount > 0 && (
                    <div className="skill-count">{selectedSkillCount}</div>
                  )}
                  <span className="line-clamp-3">{eachTopic.label}</span>
                </div>
              </div>
            );
          })}
        </div>
        <br />
        <div className="row skills-row-transition">
          <div className={'small-12 columns skills-block-' + rowIndex}>
            {this.state.skills &&
              Object.keys(this.state.skills).map((skillRowKey, index) => {
                return (
                  <SkillRow
                    skillRow={this.state.skills[skillRowKey]}
                    activeTopicId={this.state.activeTopicId}
                    topicId={skillRowKey.replace('skill-', '')}
                    key={skillRowKey}
                    selectTopicSkill={this.props.selectTopicSkill}
                    ViewMoreSkills={this.ViewMoreSkills}
                    updateActiveTopicId={this.updateActiveTopicId}
                    storeSelectedTopicsState={this.storeSelectedTopicsState}
                    skillsCount={this.props.skillsCount}
                    maxLimit={this.props.maxLimit}
                    showAlert={this.props.showAlert}
                  />
                );
              })}
          </div>
        </div>
        <br />
      </div>
    );
  }
}

TopicRow.defaultProps = {
  showAlert: function() {}
};

TopicRow.propTypes = {
  todayLearning: PropTypes.object,
  row: PropTypes.object,
  skillsCount: PropTypes.number,
  activeTopicId: PropTypes.number,
  rowIndex: PropTypes.number,
  maxLimit: PropTypes.number,
  showAlert: PropTypes.func,
  selectTopicSkill: PropTypes.func,
  updateActiveTopicId: PropTypes.func,
  section: PropTypes.string,
  skills: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    todayLearning: state.todayLearning.toJS()
  };
}

export default connect(mapStoreStateToProps)(TopicRow);
