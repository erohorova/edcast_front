import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';

// Material UI Components
import Chip from 'material-ui/Chip';
import TextField from 'material-ui/TextField';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import InfoIcon from 'material-ui/svg-icons/action/info-outline';
import AutoComplete from 'material-ui/AutoComplete';
import TopicsInput from '../common/TopicsInput';

import { postUserPrefs, suggestedTopics, suggestedSources } from '../../actions/onboardingActions';
import { topics } from 'edc-web-sdk/requests/index';

class UserPrefs extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      question: {
        padding: '13px 0 8px 0',
        fontSize: '14px',
        fontWeight: '600'
      },
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      helpText: {
        fontSize: '14px',
        padding: '8px 0'
      },
      chip: {
        margin: '4px'
      },
      suggestedChip: {
        margin: '4px',
        border: '1px solid #00A1E1'
      },
      defaultFontSize: {
        fontSize: '14px'
      },
      infoIcon: {
        height: '20px',
        width: '20px'
      },
      actionBar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#eee',
        width: '100%',
        marginLeft: '-24px',
        padding: '8px'
      },
      errorText: {
        color: 'red',
        fontSize: '12px'
      }
    };

    this.state = {
      userTopics: [],
      userSources: [],
      suggestedTopics: this.props.suggestedTopics,
      suggestedSources: this.props.suggestedSources,
      topicSearch: [],
      topicSearchText: '',
      sourceSearch: [],
      sourceSearchText: '',
      inputText: '',
      errors: {
        topics: '',
        sources: ''
      }
    };

    this.deleteTopicPref = this.deleteTopicPref.bind(this);
    this.selectTopicPref = this.selectTopicPref.bind(this);
    this.deleteSourcePref = this.deleteSourcePref.bind(this);
    this.selectSourcePref = this.selectSourcePref.bind(this);
    this.handleNextScreen = this.handleNextScreen.bind(this);
    this.handleTopicInput = this.handleTopicInput.bind(this);
    this.addTopicHandler = this.addTopicHandler.bind(this);
    this.handleSourceInput = this.handleSourceInput.bind(this);
    this.addSourceHandler = this.addSourceHandler.bind(this);
  }

  componentDidMount() {
    this.setState({
      suggestedTopics: this.props.suggestedTopics
    });
  }

  selectTopicPref(topic) {
    if (this.state.userTopics.length < 3) {
      let updatedUserTopics = _.concat(this.state.userTopics, topic);
      let updatedSuggestedTopics = _.without(this.state.suggestedTopics, topic);
      this.setState({
        userTopics: updatedUserTopics,
        suggestedTopics: updatedSuggestedTopics,
        errors: {
          topics: ''
        }
      });
    } else {
      this.setState({ errors: { topics: 'You can only select 3 at this time' } });
    }
  }

  deleteTopicPref(topic) {
    let updatedUserTopics = _.without(this.state.userTopics, topic);
    let updatedSuggestedTopics = _.concat(this.state.suggestedTopics, topic);
    this.setState({
      userTopics: updatedUserTopics,
      suggestedTopics: updatedSuggestedTopics
    });
  }

  selectSourcePref(source) {
    let updatedUserSources = _.concat(this.state.userSources, source);
    let updatedSuggestedSources = _.without(this.state.suggestedSources, source);
    this.setState({
      userSources: updatedUserSources,
      suggestedSources: updatedSuggestedSources,
      errors: {
        sources: ''
      }
    });
  }

  deleteSourcePref(source) {
    let updatedUserSources = _.without(this.state.userSources, source);
    let updatedSuggestedSources = _.concat(this.state.suggestedSources, source);
    this.setState({
      userSources: updatedUserSources,
      suggestedSources: updatedSuggestedSources
    });
  }

  topicsChangeHandler = topics => {
    this.setState({ userTopics: topics, errors: { topics: '' }, inputText: '' });
  };

  handleNextScreen() {
    let updatedUser = {};
    updatedUser.id = this.props.user.id;
    updatedUser.user_interests =
      this.state.inputText.trim() !== ''
        ? this.state.userTopics.concat(this.state.inputText)
        : this.state.userTopics;
    if (updatedUser.user_interests.length >= 1) {
      this.props.dispatch(postUserPrefs(updatedUser, 2)).then(() => {
        this.setState({ userTopics: [], inputText: '' }, () => {
          this.props.handleNext();
        });
      });
    } else {
      if (updatedUser.user_interests.length == 0) {
        this.setState({ errors: { topics: 'Please add at least one topic' } });
      }
    }
  }

  extractDomain(url) {
    var domain;
    if (url.indexOf('://') > -1) {
      domain = url.split('/')[2];
    } else {
      domain = url.split('/')[0];
    }

    domain = domain.split(':')[0];
    return domain;
  }

  handleTopicInput(text) {
    this.setState({ topicSearchText: text });
    if (text.length >= 3) {
      topics.getSuggestedTopicsForUser(text).then(suggestedTopics => {
        this.setState({
          topicSearch: suggestedTopics
        });
      });
    }
  }

  addTopicHandler(text, index) {
    this.selectTopicPref(text);
    setTimeout(() => {
      this.refs.interestsField.setState({ searchText: '' });
      this.refs.interestsField.focus();
    }, 300);
  }

  handleSourceInput(text) {
    this.setState({ sourceSearchText: text });
    if (text.length >= 3) {
      this.setState({ sourceSearch: this.state.suggestedSources });
    } else {
      this.setState({ sourceSearch: [] });
    }
  }

  addSourceHandler(text, index) {
    this.selectSourcePref(text);
    setTimeout(() => {
      this.refs.searchField.setState({ searchText: '' });
      this.refs.searchField.focus();
    }, 300);
  }

  updateUserPrefs = searchText => {
    this.setState({ inputText: searchText });
  };

  render() {
    return (
      <div style={{ marginBottom: '80px' }}>
        <h6>Tell us a little about your goals so we can recommend learning for you</h6>
        <div>
          <div className="row">
            <div className="small-12 columns">
              <div className="clearfix">
                <div className="float-left" style={this.styles.question}>
                  {tr(
                    'What are you interested in learning? (Add up to three topics using comma as a separator)'
                  )}
                </div>
                <div className="float-left">
                  <IconButton
                    iconStyle={this.styles.infoIcon}
                    tooltipPosition="top-center"
                    tooltipStyles={{ lineHeight: '12px', padding: '4px' }}
                    tooltip={
                      <span>
                        Adding one or more goals will allow <br />
                        the EdCast platform to curate the content feed just for <br />
                        you based on the selected topics.
                      </span>
                    }
                  >
                    <InfoIcon />
                  </IconButton>
                </div>
              </div>
              <TopicsInput
                updateUserPrefs={this.updateUserPrefs}
                onChange={this.topicsChangeHandler}
                defaultValue={this.state.userTopics}
              />
              <label style={this.styles.errorText}>{this.state.errors.topics}</label>
            </div>
          </div>
          <br />
        </div>
        <div style={this.styles.actionBar} className="clearfix">
          <PrimaryButton
            label="Next"
            className="float-right nextOnboarding"
            onTouchTap={this.handleNextScreen}
          />
          <SecondaryButton
            label="Back"
            className="float-left prevOnboarding"
            onTouchTap={this.props.handlePrev}
          />
        </div>
      </div>
    );
  }
}

export default connect()(UserPrefs);
