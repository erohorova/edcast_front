import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class OnboardingCustomStepper extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let numberOfSteps = this.props.numberOfSteps;
    let activeStep = this.props.activeStep;
    let containerWidth = this.props.containerWidth;
    let stepWidth = 100 / numberOfSteps;
    let steps = [];
    for (let i = 0; i < numberOfSteps; i++) {
      let stepClass =
        i < activeStep ? 'step complete-border' : i == activeStep ? 'step active' : 'step';
      steps.push(
        <div className={stepClass} style={{ width: `${stepWidth}%` }} key={i}>
          {i + 1}
          <div className="onboard-dot" />
        </div>
      );
    }
    return (
      <div className="onboarding-custom-stepper">
        <div style={{ width: '520px', margin: '0 auto' }}>{steps}</div>
      </div>
    );
  }
}

OnboardingCustomStepper.propTypes = {
  numberOfSteps: PropTypes.number,
  activeStep: PropTypes.number,
  containerWidth: PropTypes.number
};

export default connect()(OnboardingCustomStepper);
