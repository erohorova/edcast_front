import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import invert from 'lodash/invert';

// Material UI components
import Dialog from 'material-ui/Dialog';
import OnboardingCustomStepper from './OnboardingCustomStepper';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

// Components for each step
import TopicsSkills from './TopicsSkills';
import Expertise from './Expertise';

import {
  initOnboardingState,
  postUserInfo,
  updateOnboardingStep,
  getOrgInfo
} from '../../actions/onboardingActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import Loadable from 'react-loadable';
const UserInfo = Loadable({
  loader: () => import('./UserInfo'),
  loading: () => null
});

class OnboardingContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: true,
      finished: props.onboarding_completed,
      step: props.step,
      isLoaded: props.isLoaded,
      firstName: props.user.first_name ? props.user.first_name : ''
    };

    this.style = {
      bgk: {
        backgroundImage:
          'url("https://d2rdbjk9w0dffy.cloudfront.net/On-boarding_color_banner_bw.jpg")',
        minHeight: window.innerHeight
      },
      coloredBgk: {
        backgroundImage: 'url("https://d2rdbjk9w0dffy.cloudfront.net/On-boarding_color_banner.jpg")'
      },
      fullHeight: {
        minHeight: window.innerHeight
      },
      overlayStyle: {
        backgroundColor: 'rgba(0,0,0,0)',
        position: 'absolute',
        height: window.innerHeight
      },
      customContentStyle: {
        width: '70%',
        maxWidth: 'none',
        backgroundColor: 'none'
      },
      loadingtext: {
        fontSize: '38px',
        color: 'white',
        position: 'absolute',
        top: '55%',
        left: 'calc(50% - 275px)',
        paddingBottom: '3px',
        borderBottom: '1px solid #fff',
        zIndex: '1',
        fontWeight: '300'
      },
      initialLoadingtext: {
        fontSize: '38px',
        color: 'white',
        position: 'absolute',
        top: '55%',
        left: 'calc(50% - 80px)',
        paddingBottom: '3px',
        borderBottom: '1px solid #fff',
        zIndex: '1',
        fontWeight: '300'
      },
      modalWrapper: {
        overflow: 'auto'
      }
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.updateFirstNameState = this.updateFirstNameState.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      finished: nextProps.onboarding_completed,
      isLoaded: nextProps.isLoaded,
      step: nextProps.step
    });
  }

  fetchCurrentStep() {
    let invertedOnboardingSteps = this.invertedOnboardingSteps();
    let step = Object.keys(invertedOnboardingSteps).indexOf(this.state.step.toString());

    return step;
  }

  invertedOnboardingSteps() {
    return invert(this.props.user.onboarding_options);
  }

  handleClose() {
    this.setState({ open: false });
  }

  handleNext() {
    const { step } = this.state;
    let currentStep = this.fetchCurrentStep();
    let nextStep = Object.keys(this.invertedOnboardingSteps())[currentStep + 1];
    this.props.dispatch(updateOnboardingStep(parseInt(nextStep, 10)));
  }

  handlePrev() {
    const { step } = this.state;
    let prevStep = this.fetchCurrentStep();
    this.props.dispatch(updateOnboardingStep(prevStep));
  }

  updateFirstNameState(first_name) {
    this.setState({ firstName: first_name });
  }

  render() {
    let coBrandingLogo = this.props.coBrandingLogo;
    let defaultTopics = this.props.defaultTopics ? this.props.defaultTopics : [];
    let invertedOnboardingSteps = this.invertedOnboardingSteps();
    let onboardingSteps = this.props.user.onboarding_options
      ? Object.keys(this.props.user.onboarding_options)
      : ['profile_setup'];
    let onboardingStepsCount = this.props.user.onboarding_options ? onboardingSteps.length : 1;
    let shouldShowModal = this.state.step
      ? Object.values(this.props.user.onboarding_options).includes(this.state.step)
      : false;
    let lastStep = onboardingSteps[onboardingStepsCount - 1];
    let currentStepper = Object.keys(invertedOnboardingSteps).indexOf(this.state.step.toString());

    return (
      <div>
        {shouldShowModal && (
          <ReactCSSTransitionGroup
            transitionName="onboarding"
            transitionAppear={true}
            transitionAppearTimeout={300}
            transitionEnterTimeout={300}
            transitionLeave={true}
            transitionLeaveTimeout={300}
          >
            <div
              style={this.style.bgk}
              className={
                !this.state.isLoaded
                  ? 'onboarding-loading-overlay bg-image'
                  : 'onboarding-loading-no-overlay bg-image'
              }
            >
              {!this.state.isLoaded && (
                <div className="data-not-available-msg" style={this.style.initialLoadingtext}>
                  {tr('Loading...')}
                </div>
              )}
            </div>
          </ReactCSSTransitionGroup>
        )}

        <ReactCSSTransitionGroup
          transitionName="onboarding"
          transitionAppear={true}
          transitionAppearTimeout={300}
          transitionEnterTimeout={300}
          transitionLeave={true}
          transitionLeaveTimeout={300}
        >
          <div>
            <div
              style={Object.assign(
                {},
                this.style.coloredBgk,
                !shouldShowModal && this.style.fullHeight
              )}
              className="onboarding-loading-overlay bg-image"
            >
              {!shouldShowModal && (
                <div className="data-not-available-msg" style={this.style.loadingtext}>
                  {tr('Loading your EdCast Experience...')}
                </div>
              )}
            </div>
          </div>
        </ReactCSSTransitionGroup>

        <div className="">
          {this.state.isLoaded && shouldShowModal && (
            <Dialog
              modal={true}
              open={this.state.open}
              autoScrollBodyContent={false}
              onRequestClose={this.handleClose}
              contentStyle={this.style.customContentStyle}
              overlayStyle={this.style.overlayStyle}
              contentClassName="onboarding-modal-body"
              style={this.style.modalWrapper}
            >
              <div className="row">
                <div className="small-12 medium-12 columns">
                  <OnboardingCustomStepper
                    activeStep={currentStepper}
                    numberOfSteps={onboardingStepsCount}
                  />
                </div>
                <div style={{ height: '30px' }} />
                {!this.state.finished && (
                  <div className="small-12 columns">
                    <div
                      className={
                        this.state.step == 1
                          ? 'onboarding-step activeStep'
                          : 'onboarding-step hideStep'
                      }
                    >
                      <UserInfo
                        user={this.props.user}
                        handleNext={this.handleNext}
                        orgName={this.props.teamName}
                        updateFirstNameState={this.updateFirstNameState}
                        coBrandingLogo={coBrandingLogo}
                        isLastStep={lastStep === 'profile_setup'}
                      />
                    </div>
                    <div
                      className={
                        this.state.step == 2 &&
                        invertedOnboardingSteps[this.state.step] == 'add_interests'
                          ? 'onboarding-step activeStep'
                          : 'onboarding-step hideStep'
                      }
                    >
                      <TopicsSkills
                        user={this.props.user}
                        handleNext={this.handleNext}
                        handlePrev={this.handlePrev}
                        firstName={this.state.firstName}
                        coBrandingLogo={coBrandingLogo}
                        defaultTopics={defaultTopics}
                        isLastStep={lastStep === 'add_interests'}
                      />
                    </div>
                    <div
                      className={
                        [2, 3].includes(this.state.step) &&
                        invertedOnboardingSteps[this.state.step] == 'add_expertise'
                          ? 'onboarding-step activeStep'
                          : 'onboarding-step hideStep'
                      }
                    >
                      <Expertise
                        user={this.props.user}
                        firstName={this.state.firstName}
                        handleNext={this.handleNext}
                        handlePrev={this.handlePrev}
                        coBrandingLogo={coBrandingLogo}
                        currentStep={this.state.step}
                        isLastStep={lastStep === 'add_expertise'}
                      />
                    </div>
                  </div>
                )}
              </div>
            </Dialog>
          )}
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return Object.assign({}, state.onboarding.toJS());
}

OnboardingContainer.propTypes = {
  user: PropTypes.object,
  defaultTopics: PropTypes.object,
  teamName: PropTypes.string,
  coBrandingLogo: PropTypes.string,
  onboarding_completed: PropTypes.bool,
  isLoaded: PropTypes.bool,
  step: PropTypes.number
};

export default connect(mapStoreStateToProps)(OnboardingContainer);
