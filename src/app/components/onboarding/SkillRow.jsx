import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { tr } from 'edc-web-sdk/helpers/translations';

class SkillRow extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      skills: props.skillRow ? props.skillRow : [],
      page: 1,
      selectedDomainTopics: {}
    };
    this.selectTopicSkill = this.selectTopicSkill.bind(this);
    this.skillBlockClass = this.skillBlockClass.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      skills: nextProps.skillRow
    });
  }

  selectTopicSkill(skillId, skillName, skillLabel, topicId) {
    let selectedState = this.state.selectedDomainTopics;
    let updated = false;
    if (this.props.maxLimit && this.props.skillsCount == 3) {
      if (
        !selectedState.hasOwnProperty(`topic-${topicId}`) ||
        !selectedState[`topic-${topicId}`].includes(skillId)
      ) {
        this.props.showAlert();
        return;
      }
    }

    if (!selectedState.hasOwnProperty(`topic-${topicId}`)) {
      selectedState[`topic-${topicId}`] = [skillId];
      updated = true;
    } else if (!selectedState[`topic-${topicId}`].includes(skillId)) {
      selectedState[`topic-${topicId}`] = selectedState[`topic-${topicId}`].concat([skillId]);
      updated = true;
    }

    if (updated) {
      this.props.updateActiveTopicId(topicId);
      this.setState({ selectedDomainTopics: selectedState });
      this.props.storeSelectedTopicsState(selectedState, 'store');
      this.props.selectTopicSkill(skillId, skillName, skillLabel, topicId, 'store');
    } else {
      // If goes in else means user is unselecting skill
      if (
        selectedState[`topic-${topicId}`] &&
        selectedState[`topic-${topicId}`].includes(skillId)
      ) {
        var index = selectedState[`topic-${topicId}`].indexOf(skillId);
        if (index > -1) {
          selectedState[`topic-${topicId}`].splice(index, 1);
          if (selectedState[`topic-${topicId}`].length == 0) {
            delete selectedState[`topic-${topicId}`];
          }
        }
      }

      this.setState({ selectedDomainTopics: selectedState });
      this.props.storeSelectedTopicsState(selectedState, 'remove');
      this.props.selectTopicSkill(skillId, skillName, skillLabel, topicId, 'remove');
    }
  }

  ViewMoreSkills() {
    this.setState({ page: this.state.page + 1 });
    this.props.ViewMoreSkills(this.props.topicId, this.state.page + 1);
  }

  skillBlockClass(skillId) {
    let classname = 'skill-block';
    if (this.props.activeTopicId == this.props.topicId) {
      classname = classname + ' active';
    }
    if (
      this.state.selectedDomainTopics[`topic-${this.props.topicId}`] &&
      this.state.selectedDomainTopics[`topic-${this.props.topicId}`].includes(skillId)
    ) {
      classname = classname + ' selected';
    }

    return classname;
  }

  render() {
    return (
      <div className="skill-row-container">
        {this.props.activeTopicId == this.props.topicId && (
          <div className="row">
            {this.props.skillRow &&
              this.props.skillRow.data.map((skill, index) => {
                let classname = this.skillBlockClass(skill.id);
                return (
                  <ReactCSSTransitionGroup
                    key={index}
                    transitionName="skill"
                    transitionAppear={true}
                    transitionAppearTimeout={300}
                    transitionEnter={false}
                    transitionLeave={false}
                    className="small-3 columns"
                  >
                    <div
                      className={classname}
                      onClick={this.selectTopicSkill.bind(
                        this,
                        skill.id,
                        skill.name,
                        skill.label,
                        this.props.topicId
                      )}
                    >
                      <span className="line-clamp-3 text-center">{skill.label}</span>
                      <img className="check-mark" src="/i/images/check_icon.png" width="24" />
                    </div>
                    <br />
                  </ReactCSSTransitionGroup>
                );
              })}
            {!this.props.skillRow.isLastSkillPage && (
              <div className="small-3 columns text-center">
                <div
                  className="view-more viewMore"
                  style={{ marginTop: '13px' }}
                  onClick={this.ViewMoreSkills.bind(this)}
                >
                  {tr('View More')}
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

SkillRow.propTypes = {
  skillRow: PropTypes.object,
  topicId: PropTypes.number,
  activeTopicId: PropTypes.number,
  ViewMoreSkills: PropTypes.func,
  selectTopicSkill: PropTypes.func,
  storeSelectedTopicsState: PropTypes.func,
  updateActiveTopicId: PropTypes.func,
  showAlert: PropTypes.func,
  maxLimit: PropTypes.number,
  skillsCount: PropTypes.number
};

export default connect()(SkillRow);
