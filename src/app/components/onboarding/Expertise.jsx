import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Material UI Components
import Chip from 'material-ui/Chip';
import FlashAlert from '../common/FlashAlert';
import EdcFullLogo from 'edc-web-sdk/components/icons/EdcFullLogo';
import RaisedButton from 'material-ui/RaisedButton';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Select from 'react-select';
import IconButton from 'material-ui/IconButton/IconButton';
import Tick from 'edc-web-sdk/components/icons/Tick';
import Close from 'edc-web-sdk/components/icons/Close';
import Add from 'edc-web-sdk/components/icons/Add';

import { postExpertiseDomainTopics, postOnboardingStep } from '../../actions/onboardingActions';
import { topics } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';

class Expertise extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      topicSource: [],
      learningTopics: [],
      searchText: '',
      errorMessage: ''
    };

    this.styles = {
      chip: {
        margin: '4px 8px 4px 0px',
        border: '1px solid #9476C9',
        width: '100%',
        cursor: 'pointer'
      },
      selectedLabel: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        maxWidth: '100%',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        color: '#9476C9'
      },
      chipLabel: {
        width: '100%'
      },
      nextButton: {
        width: '155px',
        fontSize: '13px'
      },
      backButton: {
        marginRight: '15px',
        width: '155px',
        fontSize: '13px'
      },
      customIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px'
      }
    };

    this.handleNextScreen = this.handleNextScreen.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  componentDidMount() {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.handleSkillInput();
  }

  handleNextScreen() {
    let user = {
      id: this.props.user.id
    };
    this.props.dispatch(postOnboardingStep(user, 3, this.props.isLastStep));
    this.props.dispatch(postExpertiseDomainTopics(this.state.learningTopics, this.props.user.id));
    this.props.handleNext();
  }

  handleSkillInput = (value = '') => {
    this.setState({
      searchText: value
    });
    if (value) {
      topics
        .queryTopics(value)
        .then(topicsData => {
          this.setState({
            topicSource: topicsData.topics.slice(0, 20)
          });
        })
        .catch(err => {
          console.error(`Error in Expertise.queryTopics.func : ${err}`);
        });
    } else {
      this.setState({
        topicSource: []
      });
    }
  };

  showAlert(message) {
    this.setState({ showAlert: true, errorMessage: message });
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  selectSkillInput = skill => {
    let skillParam = skill.skillData || skill;

    let learningTopics = this.state.learningTopics;
    this.setState({
      topicSource: []
    });
    let filteredTopics = learningTopics.filter(function(obj) {
      return skillParam.id == obj.topic_id;
    });
    let expertiseLimit =
      (this.props.team.config.limit_options &&
        this.props.team.config.limit_options.expertise_limit) ||
      null;
    if (expertiseLimit && learningTopics.length > expertiseLimit - 1) {
      let expertiseLimitMsg =
        expertiseLimit === 1 ? tr('one topic') : `${expertiseLimit} ${tr('topics')}`;
      this.showAlert(
        `${tr('You can select a maximum of %{count}. Don’t worry you can always change them from your profile page.', {count: expertiseLimitMsg})}`
      );
      this.setState({
        searchText: ''
      });
      return;
    }
    if (filteredTopics.length > 0) {
      this.showAlert(tr('The topic is already added!'));
      this.setState(
        {
          searchText: ''
        },
        function() {
          this.handleSkillInput();
        }
      );
      return;
    }
    let learningTopic = {
      domain_id: skillParam.domain.id,
      domain_label: skillParam.domain.label,
      domain_name: skillParam.domain.name,
      topic_id: skillParam.id,
      topic_label: skillParam.label,
      topic_name: skillParam.name
    };
    learningTopics.push(learningTopic);
    this.setState(
      {
        learningTopics,
        searchText: ''
      },
      function() {
        this.handleSkillInput();
      }
    );
  };

  removeTopicHandler(id) {
    let learningTopics = this.state.learningTopics;
    learningTopics = learningTopics.filter(function(obj) {
      return obj.topic_id !== id;
    });
    this.setState({
      learningTopics
    });
  }

  removeStateData = () => {
    this.setState({
      topicSource: [],
      searchText: ''
    });
  };

  getSources(input, callback) {
    this.setState({
      searchText: input
    });
    if (!input) {
      callback(null, {
        options: [],
        complete: true
      });
      return;
    }
    return topics
      .queryTopics(input)
      .then(topicsData => {
        let data = topicsData.topics.slice(0, 20);
        let suggs = [];
        let optionObj;

        if (data.length == 0) {
          this.showAlert(tr('The goal you are trying to add does not exist.'));
        } else {
          this.setState({ showAlert: false });
        }

        data.map(item => {
          optionObj = { value: item.id, label: item.label, skillData: item };
          suggs.push(optionObj);
        });

        callback(null, {
          options: suggs,
          complete: true
        });
      })
      .catch(err => {
        console.error(`Error in Expertise.getSources.queryTopics.func : ${err}`);
      });
  }

  render() {
    let fNameDisplay = this.props.firstName ? this.props.firstName + ', tell' : 'Tell';
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let skillLabel = tr('ADD SKILLS');
    if (labels) {
      let skillsLabelFlag =
        labels['web/labels/interests'] && labels['web/labels/expertise'].label.length > 0;
      skillLabel = `${tr('ADD')} ${
        skillsLabelFlag ? tr(labels['web/labels/expertise'].label) : tr('SKILLS')
      }`;
    }
    let _this = this;
    return (
      <div style={{ marginBottom: '80px' }}>
        <div className="row">
          <div className="small-12 medium-10 medium-offset-1 columns">
            {this.state.showAlert && (
              <FlashAlert
                message={this.state.errorMessage}
                toShow={this.state.showAlert}
                removeAlert={this.removeAlert}
              />
            )}
            <div style={{ textAlign: 'center' }}>
              {this.props.coBrandingLogo && (
                <img style={{ maxHeight: '72px', width: 'auto' }} src={this.props.coBrandingLogo} />
              )}
              {!this.props.coBrandingLogo && (
                <div style={{ transform: 'translate(5%, 13%)' }}>
                  <EdcFullLogo style={{ maxHeight: '72px', width: 'auto' }} />
                </div>
              )}
            </div>
            <div style={{ marginBottom: '10px' }} />
            <h4 className="step-title onBoard-page-title text-center">
              {skillLabel.toLocaleUpperCase()}
            </h4>
            <h6 className="step-desc text-center">
              {fNameDisplay}{' '}
              {tr(
                "others what you are good at. This will appear on your profile and help us recommend you as a 'Subject Matter Expert'"
              )}
            </h6>
            <br />
            <div className="row skill-input-row">
              <div className="small-12 medium-10 columns medium-offset-1">
                <div className="row">
                  <div className="small-12 columns">
                    {this.state.learningTopics.length > 0 && (
                      <ReactCSSTransitionGroup
                        transitionName="skill"
                        transitionAppear={true}
                        transitionAppearTimeout={400}
                        transitionEnterTimeout={400}
                        transitionLeave={true}
                        transitionLeaveTimeout={400}
                      >
                        <div className="row">
                          <div className="small-12 columns">
                            <p className="onboarding-label">{tr("I'm good at...")}</p>
                          </div>
                        </div>
                      </ReactCSSTransitionGroup>
                    )}
                    <div className="row">
                      <div className="small-12 columns">
                        <div className="row">
                          {this.state.learningTopics.map((skill, idx) => {
                            return (
                              <div className="small-3 columns">
                                <ReactCSSTransitionGroup
                                  transitionName="skill"
                                  transitionAppear={true}
                                  transitionAppearTimeout={400}
                                  transitionEnterTimeout={400}
                                  transitionLeave={true}
                                  transitionLeaveTimeout={400}
                                >
                                  <Chip
                                    key={idx}
                                    className="selected-label"
                                    style={this.styles.chip}
                                    labelStyle={this.styles.chipLabel}
                                    backgroundColor={'#fff'}
                                  >
                                    <small style={this.styles.selectedLabel}>
                                      {skill.topic_label}
                                    </small>
                                    <IconButton
                                      className={'tick-icon'}
                                      ref={'tickIcon'}
                                      tooltip={tr('Selected')}
                                      aria-label={`Selected, ${skill.topic_label}`}
                                      disableTouchRipple
                                      tooltipPosition="top-center"
                                      style={this.styles.customIcon}
                                    >
                                      <Tick color="#9476C9" />
                                    </IconButton>
                                    <IconButton
                                      className={'cancel-icon delete'}
                                      ref={'cancelIcon'}
                                      tooltip={tr('Remove Item')}
                                      aria-label={`remove, ${skill.topic_label}`}
                                      disableTouchRipple
                                      tooltipPosition="top-center"
                                      style={this.styles.customIcon}
                                      onTouchTap={this.removeTopicHandler.bind(
                                        _this,
                                        skill.topic_id
                                      )}
                                    >
                                      <Close color="#9476C9" />
                                    </IconButton>
                                  </Chip>
                                </ReactCSSTransitionGroup>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <br />
                <div className="row skill-input-row">
                  <div className="small-12 medium-6 medium-offset-3 columns text-center">
                    <div className="text-left">
                      <p className="onboarding-label text-left">{tr('What are you good at?')}</p>
                      <Select.Async
                        name="skill-select"
                        loadOptions={this.getSources.bind(this)}
                        onChange={this.selectSkillInput}
                        className={
                          this.state.searchText == ''
                            ? 'onboarding-autocomplete hide-options'
                            : 'onboarding-autocomplete'
                        }
                        onBlur={this.removeStateData}
                        placeholder={tr('Start typing here...')}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="small-12 text-center">
                <RaisedButton
                  style={this.styles.backButton}
                  label={tr('Back')}
                  className="onboarding-button prevOnboarding"
                  onClick={this.props.handlePrev}
                  labelColor="white"
                  backgroundColor="#9476C9"
                />
                <RaisedButton
                  style={this.styles.nextButton}
                  label={this.state.learningTopics.length ? tr('Next Step') : tr('Skip')}
                  className="onboarding-button nextOnboarding"
                  onClick={this.handleNextScreen}
                  labelColor="white"
                  backgroundColor="#9476C9"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Expertise.propTypes = {
  user: PropTypes.object,
  team: PropTypes.object,
  isLastStep: PropTypes.bool,
  handleNext: PropTypes.func,
  handlePrev: PropTypes.func,
  firstName: PropTypes.string,
  coBrandingLogo: PropTypes.string
};

export default connect(state => {
  return { team: state.team.toJS() };
})(Expertise);
