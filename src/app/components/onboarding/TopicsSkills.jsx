import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Material UI Components
import FlashAlert from '../common/FlashAlert';
import Chip from 'material-ui/Chip';
import EdcFullLogo from 'edc-web-sdk/components/icons/EdcFullLogo';
import RaisedButton from 'material-ui/RaisedButton';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Select from 'react-select';
import IconButton from 'material-ui/IconButton/IconButton';
import Tick from 'edc-web-sdk/components/icons/Tick';
import Close from 'edc-web-sdk/components/icons/Close';
import Add from 'edc-web-sdk/components/icons/Add';

import { postLearningDomainTopics, postOnboardingStep } from '../../actions/onboardingActions';
import { topics } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import * as upshotActions from '../../actions/upshotActions';

class TopicsSkills extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showAlert: false,
      activeTopicId: '',
      skillsCount: 0,
      skillSource: [],
      learningTopics: [],
      searchText: '',
      errorMessage: '',
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      selectedSkillsIdsCollection: []
    };

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px 8px 4px 0px',
        border: '1px solid #9476C9',
        width: '100%',
        cursor: 'pointer'
      },
      suggestionChip: {
        border: '1px solid #747588',
        margin: '4px 8px 4px 0px',
        width: '100%',
        cursor: 'pointer'
      },
      selectedSuggestionChip: {
        border: '1px solid #9476C9',
        margin: '4px 8px 4px 0px',
        width: '100%',
        cursor: 'pointer'
      },
      chipLabel: {
        width: '100%'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      },
      autoCompleteInput: {
        fontSize: '13px',
        color: '#ACADC1'
      },
      nextButton: {
        width: '155px',
        fontSize: '13px'
      },
      backButton: {
        marginRight: '15px',
        width: '155px',
        fontSize: '13px'
      },
      customPlusIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px'
      },
      customTickIcon: {
        position: 'absolute',
        top: '2px',
        maxHeight: '18px',
        right: '12px',
        padding: 0,
        maxWidth: '18px'
      },
      suggestionLabel: {
        color: '#747588',
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        maxWidth: '90%',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle'
      },
      selectedLabel: {
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        maxWidth: '100%',
        display: 'inline-block',
        lineHeight: '22px',
        verticalAlign: 'middle',
        color: '#9476C9'
      }
    };

    this.handleNextScreen = this.handleNextScreen.bind(this);
    this.removeAlert = this.removeAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  componentDidMount() {
    this.handleSkillInput();
  }

  showAlert(message) {
    this.setState({ showAlert: true, errorMessage: message });
  }

  removeAlert() {
    this.setState({ showAlert: false });
  }

  handleNextScreen() {
    let user = {
      id: this.props.user.id
    };
    this.props.dispatch(postOnboardingStep(user, 2, this.props.isLastStep));
    this.props.dispatch(postLearningDomainTopics(this.state.learningTopics, this.props.user.id));
    this.props.handleNext();
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['LEARNING_GOALS'], {
        learningTopics: this.state.learningTopics,
        event: 'Onboarding Learning Topics'
      });
    }
  }

  handleSkillInput = (value = '') => {
    this.setState({
      searchText: value
    });
    if (value) {
      topics
        .queryTopics(value)
        .then(topicsData => {
          this.setState({
            skillSource: topicsData.topics.slice(0, 20)
          });
        })
        .catch(err => {
          console.error(`Error in TopicsSkills.queryTopics.func : ${err}`);
        });
    } else {
      this.setState({
        skillSource: []
      });
    }
  };

  selectSkillInput = skill => {
    let skillParam = skill.skillData || skill;

    let learningTopics = this.state.learningTopics;
    let selectedSkillsIdsCollection = this.state.selectedSkillsIdsCollection;
    this.setState({
      skillSource: []
    });
    let filteredTopics = learningTopics.filter(function(obj) {
      return skillParam.id ? skillParam.id == obj.topic_id : skillParam.topic_id == obj.topic_id;
    });
    let interestsLimit =
      (this.props.team.config.limit_options &&
        this.props.team.config.limit_options.interests_limit) ||
      3;
    if (learningTopics.length > interestsLimit - 1) {
      let interestsLimitMsg = interestsLimit === 1 ? 'one topic' : `${interestsLimit} topics`;
      this.showAlert(
        `You can select a maximum of ${interestsLimitMsg}. Don’t worry you can always change them from your profile page.`
      );
      return;
    }
    if (filteredTopics.length > 0) {
      this.showAlert('The topic is already added!');
      this.handleSkillInput();
      return;
    }
    let learningTopic = {
      domain_id: skillParam.domain ? skillParam.domain.id : skillParam.domain_id,
      domain_label: skillParam.domain ? skillParam.domain.label : skillParam.domain_label,
      domain_name: skillParam.domain ? skillParam.domain.name : skillParam.domain_name,
      topic_id: skillParam.id ? skillParam.id : skillParam.topic_id,
      topic_label: skillParam.label ? skillParam.label : skillParam.topic_label,
      topic_name: skillParam.name ? skillParam.name : skillParam.topic_name
    };
    learningTopics.push(learningTopic);
    selectedSkillsIdsCollection.push(skillParam.id ? skillParam.id : skillParam.topic_id);
    this.setState(
      {
        learningTopics,
        selectedSkillsIdsCollection: selectedSkillsIdsCollection
      },
      function() {
        this.handleSkillInput();
      }
    );
  };

  removeTopicHandler(id) {
    let learningTopics = this.state.learningTopics;
    let skillCollection = this.state.selectedSkillsIdsCollection;
    learningTopics = learningTopics.filter(function(obj) {
      return obj.topic_id !== id;
    });

    var index = skillCollection.indexOf(id);
    if (index > -1) {
      skillCollection.splice(index, 1);
    }
    this.setState({
      learningTopics,
      selectedSkillsIdsCollection: skillCollection
    });
  }

  removeStateData = () => {
    this.setState({
      skillSource: [],
      searchText: ''
    });
  };

  getSources(input, callback) {
    this.setState({
      searchText: input
    });
    if (!input) {
      callback(null, {
        options: [],
        complete: true
      });
      return;
    }
    return topics
      .queryTopics(input)
      .then(topicsData => {
        let data = topicsData.topics.slice(0, 20);
        let suggs = [];
        let optionObj;

        if (data.length == 0) {
          this.showAlert('The goal you are trying to add does not exist.');
        } else {
          this.setState({ showAlert: false });
        }

        data.map(item => {
          optionObj = { value: item.id, label: item.label, skillData: item };
          suggs.push(optionObj);
        });

        callback(null, {
          options: suggs,
          complete: true
        });
      })
      .catch(err => {
        console.error(`Error in TopicsSkills.getSources.queryTopics.func : ${err}`);
      });
  }

  render() {
    let fNameDisplay = this.props.firstName ? this.props.firstName + ', let' : 'Let';
    let interestCount =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.limit_options &&
        this.props.team.config.limit_options.interests_limit) ||
      3;
    let _this = this;
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let interestLabel = tr('ADD LEARNING GOALS');
    let suggestedInterestLabel = tr('Suggested Learning Goals');
    if (labels) {
      let interestsLabelFlag =
        labels['web/labels/interests'] && labels['web/labels/interests'].label.length > 0;
      interestLabel = `${tr('ADD')} ${
        interestsLabelFlag ? tr(labels['web/labels/interests'].label) : tr('Learning Goals')
      }`;
      suggestedInterestLabel = `${tr('Suggested')} ${
        interestsLabelFlag ? tr(labels['web/labels/interests'].label) : tr('Learning Goals')
      }`;
    }
    return (
      <div style={{ marginBottom: '80px' }}>
        <div className="row">
          <div className="small-12 medium-10 medium-offset-1 columns">
            {this.state.showAlert && (
              <FlashAlert
                message={this.state.errorMessage}
                toShow={this.state.showAlert}
                removeAlert={this.removeAlert}
              />
            )}
            <div style={{ textAlign: 'center' }}>
              {this.props.coBrandingLogo && (
                <img style={{ maxHeight: '72px', width: 'auto' }} src={this.props.coBrandingLogo} />
              )}
              {!this.props.coBrandingLogo && (
                <div style={{ transform: 'translate(5%, 13%)' }}>
                  <EdcFullLogo style={{ maxHeight: '72px', width: 'auto' }} />
                </div>
              )}
            </div>
            <div style={{ marginBottom: '10px' }} />

            <h4 className="step-title onBoard-page-title text-center">
              {interestLabel.toLocaleUpperCase()}
            </h4>
            <h6 className="step-desc text-center">
              {fNameDisplay}{' '}
              {tr('us personalize the content based on your learning goals. Add up to')}{' '}
              {interestCount} {tr('goals.')}
            </h6>
            <br />

            <div className="row">
              <div className="small-12 columns">
                {this.state.learningTopics.length > 0 && (
                  <ReactCSSTransitionGroup
                    transitionName="skill"
                    transitionAppear={true}
                    transitionAppearTimeout={400}
                    transitionEnterTimeout={400}
                    transitionLeave={true}
                    transitionLeaveTimeout={400}
                  >
                    <div className="row" key={'title'}>
                      <div className="small-12 columns">
                        <p className="onboarding-label">{tr('I am interested in...')}</p>
                      </div>
                    </div>
                  </ReactCSSTransitionGroup>
                )}
                <div className="row">
                  <div className="small-12 columns">
                    <div className="row">
                      {this.state.learningTopics.map((skill, idx) => {
                        return (
                          <div className="small-3 columns">
                            <ReactCSSTransitionGroup
                              transitionName="skill"
                              transitionAppear={true}
                              transitionAppearTimeout={400}
                              transitionEnterTimeout={400}
                            >
                              <Chip
                                key={idx}
                                className="selected-label"
                                style={this.styles.chip}
                                labelStyle={this.styles.chipLabel}
                                backgroundColor={'#fff'}
                              >
                                <small style={this.styles.selectedLabel}>{skill.topic_label}</small>
                                <IconButton
                                  className={'tick-icon'}
                                  ref={'tickIcon'}
                                  tooltip={tr('Selected')}
                                  disableTouchRipple
                                  tooltipPosition="top-center"
                                  style={this.styles.customPlusIcon}
                                  aria-label={`selected, ${skill.topic_label}`}
                                >
                                  <Tick color="#9476C9" />
                                </IconButton>
                                <IconButton
                                  className={'cancel-icon delete'}
                                  aria-label={`remove, ${skill.topic_label}`}
                                  ref={'cancelIcon'}
                                  tooltip={tr('Remove Item')}
                                  disableTouchRipple
                                  tooltipPosition="top-center"
                                  style={this.styles.customPlusIcon}
                                  onTouchTap={this.removeTopicHandler.bind(_this, skill.topic_id)}
                                >
                                  <Close color="#9476C9" />
                                </IconButton>
                              </Chip>
                            </ReactCSSTransitionGroup>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <br />
            <div className="row skill-input-row">
              <div className="small-12 medium-5  columns text-left">
                <div>
                  <p className="onboarding-label">{tr('What are you interested in learning?')}</p>
                  <Select.Async
                    name="skill-select"
                    loadOptions={this.getSources.bind(this)}
                    onChange={this.selectSkillInput}
                    className={
                      this.state.searchText == ''
                        ? 'onboarding-autocomplete hide-options'
                        : 'onboarding-autocomplete'
                    }
                    onBlur={this.removeStateData}
                    placeholder={tr('Start typing here...')}
                  />
                </div>
              </div>
            </div>
            <br />
            <div className="row">
              <div className="small-12 columns">
                {this.props.defaultTopics.length > 0 && (
                  <div className="row">
                    <div className="small-12 columns">
                      <p className="onboarding-label">{suggestedInterestLabel}</p>
                    </div>
                  </div>
                )}
                <div className="row">
                  <div className="small-12 columns">
                    <div className="row">
                      {this.props.defaultTopics &&
                        this.props.defaultTopics.map((skill, idx) => {
                          let selected = this.state.selectedSkillsIdsCollection.includes(
                            skill.topic_id
                          );
                          return (
                            <div className="small-3 columns">
                              <Chip
                                className="suggestion-chip"
                                key={idx}
                                style={
                                  selected
                                    ? this.styles.selectedSuggestionChip
                                    : this.styles.suggestionChip
                                }
                                labelStyle={this.styles.chipLabel}
                                backgroundColor={selected ? '#DAD3E2' : '#fff'}
                                onClick={this.selectSkillInput.bind(_this, skill)}
                              >
                                <small
                                  style={
                                    selected
                                      ? this.styles.selectedLabel
                                      : this.styles.suggestionLabel
                                  }
                                >
                                  {skill.topic_label}
                                </small>
                                {!selected && (
                                  <IconButton
                                    className="add-icon create"
                                    tooltip={tr('Add Item')}
                                    aria-label={tr('Add Item')}
                                    disableTouchRipple
                                    tooltipPosition="top-center"
                                    style={this.styles.customPlusIcon}
                                  >
                                    <Add color="#747588" />
                                  </IconButton>
                                )}
                              </Chip>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <div className="row buttons-wrapper">
              <div className="small-12 text-center">
                <RaisedButton
                  style={this.styles.backButton}
                  label={tr('Back')}
                  className="onboarding-button prevOnboarding"
                  onClick={this.props.handlePrev}
                  labelColor="white"
                  backgroundColor="#9476C9"
                />
                <RaisedButton
                  style={this.styles.nextButton}
                  label={tr('Next Step')}
                  disabled={this.state.learningTopics.length == 0}
                  className="onboarding-button nextOnboarding"
                  onClick={this.handleNextScreen}
                  labelColor="white"
                  backgroundColor="#9476C9"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TopicsSkills.propTypes = {
  user: PropTypes.object,
  team: PropTypes.object,
  defaultTopics: PropTypes.object,
  isLastStep: PropTypes.bool,
  handleNext: PropTypes.func,
  firstName: PropTypes.string,
  handlePrev: PropTypes.func,
  coBrandingLogo: PropTypes.string
};

export default connect(state => {
  return { team: state.team.toJS() };
})(TopicsSkills);
