import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import trim from 'lodash/trim';
import ReactDOM from 'react-dom';

// Material UI Components
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import Spinner from '../common/spinner';
import EdcFullLogo from 'edc-web-sdk/components/icons/EdcFullLogo';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min';

import { postUserInfo } from '../../actions/onboardingActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import { openUploadImageModal } from '../../actions/modalActions';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';

class UserInfo extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      uploadProgress: {
        verticalAlign: 'middle'
      }
    };

    this.state = {
      first_name: this.props.user.first_name ? this.props.user.first_name : '',
      last_name: this.props.user.last_name ? this.props.user.last_name : '',
      first_name_field_disable: !!this.props.user.first_name,
      last_name_field_disable: !!this.props.user.last_name,
      isImageUploading: false,
      imageUpdated: false,
      imageUrl: this.props.user.avatar ? this.props.user.avatar : '/i/images/camera.png',
      promptChecked: false,
      errors: {
        first_name: '',
        last_name: '',
        password: ''
      },
      showPopover: false,
      encryption_payload:
        this.props.team && this.props.team.config && this.props.team.config.encryption_payload
    };

    this.getUserInfo = this.getUserInfo.bind(this);
    this.handleNextScreen = this.handleNextScreen.bind(this);
    this.uploadImageClickHandler = this.uploadImageClickHandler.bind(this);
    this.handleFirstNameUpdate = this.handleFirstNameUpdate.bind(this);
    this.handleLastNameUpdate = this.handleLastNameUpdate.bind(this);
    this.allLetter = this.allLetter.bind(this);

    this.showPopover = this.showPopover.bind(this);
    this.hidePopover = this.hidePopover.bind(this);
  }

  componentDidMount() {
    this.setState({
      infoEle: ReactDOM.findDOMNode(this.refs.password)
    });
  }

  showPopover() {
    this.setState({ showPopover: true });
  }

  hidePopover() {
    this.setState({ showPopover: false });
  }

  getUserInfo() {
    let updatedUser = this.props.user;
    updatedUser.first_name = this.refs.first_name.value;
    updatedUser.last_name = this.refs.last_name.value;

    if (this.props.user.password_required) {
      updatedUser.password = this.refs.password.value;
    }

    if (this.state.imageUpdated) {
      updatedUser.avatar = this.state.imageUrl;
    } else {
      delete updatedUser.avatar;
    }

    return updatedUser;
  }

  handleNextScreen() {
    let updatedUser = this.getUserInfo();

    if (this.state.promptChecked) {
      updatedUser.profile_attributes = {};
      updatedUser.profile_attributes.tac_accepted = this.state.promptChecked;
    }

    if (this.isValid(updatedUser)) {
      // Start Encryption of password in payload
      if (
        this.state.encryption_payload &&
        updatedUser.password &&
        JSEncrypt &&
        window.process.env.JsPublic
      ) {
        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(window.process.env.JsPublic);
        updatedUser.password = encrypt.encrypt(updatedUser.password);
      }
      // End encryption of password in payload
      this.props.dispatch(postUserInfo(updatedUser, 1, this.props.isLastStep));
      this.props.handleNext();
    }
  }

  isValid(user) {
    let errors = this.state.errors;
    let isValid = true;
    let fieldNameReg = /^[a-zA-Z0-9-_' ]+$/;

    let firstName = trim(user.first_name);
    if (!firstName) {
      errors.first_name = tr('First name cannot be blank');
      isValid = false;
    }

    let validFirstName = firstName.search(fieldNameReg) > -1;
    if (!validFirstName) {
      errors.first_name = tr(
        'First Name cannot contain the following characters: + . ( ) [ ] ; ! @  / : * ? " # % < > | ~ &'
      );
      isValid = false;
    } else {
      errors.first_name = '';
    }

    let lastName = trim(user.last_name);
    if (!lastName) {
      errors.last_name = tr('Last name cannot be blank');
      isValid = false;
    }

    let validLastName = lastName.search(fieldNameReg) > -1;
    if (!validLastName) {
      errors.last_name = tr(
        'Last Name cannot contain the following characters: + . ( ) [ ] ; ! @  / : * ? " # % < > | ~ &'
      );
      isValid = false;
    } else {
      errors.last_name = '';
    }

    if (this.props.user.password_required) {
      if (!trim(user.password)) {
        errors.password = tr('Required');
        isValid = false;
      } else {
        var passwordReg = new RegExp(
          '(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])(?=.{8,})'
        );
        if (!passwordReg.test(user.password)) {
          this.setState({
            showPopover: true
          });
          errors.password = tr('Password is invalid');
          isValid = false;
        } else {
          errors.password = '';
        }
      }
    }

    this.setState({
      errors: errors
    });

    return isValid;
  }

  uploadImageClickHandler() {
    let sizes = {
      maxWidth: 120,
      maxHeight: 120
    };
    this.props.dispatch(
      openUploadImageModal('avatar', sizes, url => {
        this.setState({
          imageUrl: url,
          isImageUploading: false,
          imageUpdated: true
        });
      })
    );
  }

  allLetter(inputtxt) {
    var letters = /^[^ $<>&%#@*0-9()!]+$/;
    return inputtxt.match(letters) ? true : false;
  }

  handleFirstNameUpdate() {
    this.setState({
      first_name: this.refs.first_name.value
    });

    setTimeout(() => {
      this.props.updateFirstNameState(this.state.first_name);
    }, 20);
  }

  handleLastNameUpdate() {
    this.setState({
      last_name: this.refs.last_name.value
    });
  }

  handlePromptChange = e => {
    let newValue = e.currentTarget.checked;
    this.setState({
      promptChecked: newValue
    });
  };
  render() {
    let acceptancePromptEnabled =
      this.props.acceptancePromptEnabled !== undefined ? this.props.acceptancePromptEnabled : null;
    let acceptancePromptMessage =
      this.props.acceptancePromptMessage !== undefined ? this.props.acceptancePromptMessage : null;
    let acceptancePromptLink =
      this.props.acceptancePromptLink !== undefined ? this.props.acceptancePromptLink : null;

    return (
      <div style={{ paddingBottom: '80px' }}>
        <Popover
          open={this.state.showPopover}
          anchorEl={this.state.infoEle}
          anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          useLayerForClickAway={false}
          canAutoPosition={false}
          animated={false}
          className="custom-popover"
        >
          <div className="popover-title">
            <ul>
              <li>Use at least 8 characters.</li>
              <li>At least one letter should be capital.</li>
              <li>Include at least a number and symbol (#?!@$%^&*-).</li>
              <li>Password is case sensitive.</li>
            </ul>
          </div>
        </Popover>

        <div style={{ textAlign: 'center' }}>
          {this.props.coBrandingLogo && (
            <img style={{ maxHeight: '72px', width: 'auto' }} src={this.props.coBrandingLogo} />
          )}
          {!this.props.coBrandingLogo && (
            <div style={{ transform: 'translate(5%, 13%)' }}>
              <EdcFullLogo style={{ maxHeight: '72px', width: 'auto' }} />
            </div>
          )}
        </div>
        <div style={{ marginBottom: '20px' }} />

        <h4 className="step-title onBoard-page-title text-center">
          {tr('Welcome to Team %{orgName} on Edcast!', {orgName: this.props.orgName})}
        </h4>
        <div className="row">
          <div className="small-12 columns text-center">
            <div style={{ marginBottom: '10px' }}>
              <h6 className="step-desc">{tr('Add your name and profile image')}</h6>
              <br />
              {!this.state.isImageUploading && (
                <Avatar
                  style={{ cursor: 'pointer' }}
                  src={this.state.imageUrl}
                  size={80}
                  className="uploadImage forIE"
                  onTouchTap={this.uploadImageClickHandler}
                />
              )}
              {this.state.isImageUploading && <Spinner />}
              <span style={{ display: 'block', fontSize: '12px', color: '#898989' }}>
                (120x120)
              </span>
              <br />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="small-12 medium-4 medium-offset-4 columns text-left">
            <label className="onboarding-label">{tr('First Name')}</label>
            <input
              className="onboarding-input-box"
              type="text"
              name="first_name"
              ref="first_name"
              value={this.state.first_name}
              disabled={this.state.first_name_field_disable && !this.props.user.password_changeable}
              onChange={this.handleFirstNameUpdate}
              required="true"
            />
            {this.state.errors.first_name != '' && (
              <ReactCSSTransitionGroup
                transitionName="error"
                transitionAppear={true}
                transitionAppearTimeout={400}
                transitionEnter={false}
                transitionLeave={false}
              >
                <span className="onboarding-error" id="pass_constrain">
                  {tr(this.state.errors.first_name)}
                </span>
              </ReactCSSTransitionGroup>
            )}
          </div>
        </div>
        <div className="row">
          <div className="small-12 medium-4 medium-offset-4 columns text-left">
            <label className="onboarding-label">Last Name</label>
            <input
              className="onboarding-input-box"
              type="text"
              name="last_name"
              ref="last_name"
              value={this.state.last_name}
              disabled={this.state.last_name_field_disable && !this.props.user.password_changeable}
              onChange={this.handleLastNameUpdate}
              required="true"
            />
            {this.state.errors.last_name != '' && (
              <ReactCSSTransitionGroup
                transitionName="error"
                transitionAppear={true}
                transitionAppearTimeout={400}
                transitionEnter={false}
                transitionLeave={false}
              >
                <span className="onboarding-error" id="pass_constrain">
                  {tr(this.state.errors.last_name)}
                </span>
              </ReactCSSTransitionGroup>
            )}
          </div>
        </div>

        {this.props.user.password_required && (
          <div className="row">
            <div className="small-12 medium-4 medium-offset-4 columns text-left">
              <label className="onboarding-label">{tr('Choose Password')} </label>
              <input
                className="onboarding-input-box"
                type="password"
                name="password"
                ref="password"
                onChange={this.handleLastNameUpdate}
                required="true"
                onFocus={this.showPopover}
                onBlur={this.hidePopover}
              />
              {this.state.errors.password != '' && (
                <ReactCSSTransitionGroup
                  transitionName="error"
                  transitionAppear={true}
                  transitionAppearTimeout={400}
                  transitionEnter={false}
                  transitionLeave={false}
                >
                  <span className="onboarding-error" id="pass_constrain">
                    {tr('Password is invalid')}
                  </span>
                </ReactCSSTransitionGroup>
              )}
            </div>
          </div>
        )}

        {acceptancePromptEnabled !== null && acceptancePromptEnabled == 'true' && (
          <div className="row">
            <div
              className="small-12 medium-4 medium-offset-4 columns text-left"
              style={{
                fontSize: '0.75rem',
                maxHeight: '200px',
                overflowY: 'auto'
              }}
            >
              <span dangerouslySetInnerHTML={{ __html: acceptancePromptMessage }} />
              &nbsp;
              <input type="checkbox" onChange={this.handlePromptChange} />
            </div>
          </div>
        )}

        <br />
        <div className="row">
          <div className="small-12 medium-4 medium-offset-4 columns text-center">
            <RaisedButton
              disabled={
                acceptancePromptEnabled !== null &&
                acceptancePromptEnabled !== 'false' &&
                this.state.promptChecked == false
              }
              label={tr('Next Step')}
              fullWidth={true}
              className="onboarding-button nextOnboarding"
              onTouchTap={this.handleNextScreen}
              labelColor="white"
              backgroundColor="#9476C9"
            />
          </div>
        </div>
      </div>
    );
  }
}

UserInfo.propTypes = {
  onboarding: PropTypes.object,
  user: PropTypes.object,
  isLastStep: PropTypes.bool,
  orgName: PropTypes.string,
  coBrandingLogo: PropTypes.string,
  acceptancePromptLink: PropTypes.string,
  acceptancePromptEnabled: PropTypes.string,
  acceptancePromptMessage: PropTypes.string,
  handleNext: PropTypes.func,
  updateFirstNameState: PropTypes.func,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return Object.assign({}, state.onboarding.toJS(), {
    team: state.team.toJS()
  });
}

export default connect(mapStoreStateToProps)(UserInfo);
