import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import colors from 'edc-web-sdk/components/colors/index';

import { postOnboardingStep } from '../../actions/onboardingActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class Promos extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      actionBar: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#eee',
        width: '100%',
        marginLeft: '-24px',
        padding: '8px'
      }
    };

    this.handleNextScreen = this.handleNextScreen.bind(this);
    this.showChromeExtensionPromo = this.showChromeExtensionPromo.bind(this);
  }

  handleNextScreen() {
    let user = {
      id: this.props.user.id
    };
    this.props.dispatch(postOnboardingStep(user, 4));
  }

  showChromeExtensionPromo() {
    let nq = /(.*)Chrome(.*)/.test(navigator.userAgent);
    if (nq) {
      return (
        <div className="row">
          <div className="small-12 medium-7 columns">
            <br />
            <br />
            {tr(
              'You might also be interested in our Chrome Extension which allows you to share great resources with others with one quick click.'
            )}
          </div>
          <div className="small-12 medium-4 medium-offset-1 columns">
            <img src="https://s3-us-west-2.amazonaws.com/www.skillupindia.org/chrome-extension-promo.png" />
            <br />
            <a
              href="https://chrome.google.com/webstore/detail/edcast-guideme/acildjkogadjdnolbaiepkefilhglcdm"
              target="_blank"
            >
               
              <PrimaryButton
                label={tr('&nbsp;&nbsp;Get your chrome plugin&nbsp;')}
                style={{ backgroundColor: colors.secondary, borderColor: colors.secondary }}
              />
            </a>
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <div style={{ marginBottom: '80px' }}>
        <h6>{tr('Thanks for taking the time to setup your profile.')}</h6>
        <br />
        <div>
          <div className="row">
            <div className="small-12 medium-7 columns">
              {tr(
                'Finding the learning you need has never been easier or more fun. Download and install the EdCast Mobile App for sharing knowledge anywhere anytime!'
              )}
            </div>
            <div className="small-12 medium-4 medium-offset-1 columns">
              <a
                href="https://itunes.apple.com/app/apple-store/id974833832?pt=100220803&ct=KKWebsiteLink&mt=8"
                target="_blank"
              >
                <img
                  src="/i/images/app_store.png"
                  style={{ height: '44px', margin: '6px 16px 0 0' }}
                />
              </a>
              <a href="https://play.google.com/store/apps/details?id=com.edcast" target="_blank">
                <img
                  src="/i/images/play_store.png"
                  style={{ height: '44px', margin: '6px 16px 0 0' }}
                />
              </a>
              <br />
              <br />
            </div>
          </div>
          {this.showChromeExtensionPromo()}
        </div>
        <div style={this.styles.actionBar} className="clearfix">
          <PrimaryButton
            label={tr('Show my Feed')}
            className="float-right prevOnboarding"
            onTouchTap={this.handleNextScreen}
          />
          <SecondaryButton
            label={tr('Back')}
            className="float-left nextOnboarding"
            onTouchTap={this.props.handlePrev}
          />
        </div>
      </div>
    );
  }
}

export default connect()(Promos);
