import React, { Component } from 'react';
import PropTypes from 'prop-types';

class EmptyContainer extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return <div />;
  }
}

export default EmptyContainer;
