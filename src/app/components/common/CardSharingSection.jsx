import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactTags from 'react-tag-autocomplete';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import debounce from 'lodash/debounce';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';

import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import ChannelIconActive from 'edc-web-sdk/components/icons/ChannelIconActive';

import { getItems } from 'edc-web-sdk/requests/users.v2';
import { getChannelsMinimal } from 'edc-web-sdk/requests/channels.v2';
import { getListV2 } from 'edc-web-sdk/requests/groups.v2';
import Checkbox from 'material-ui/Checkbox/index';
import ActionInfo from 'react-material-icons/icons/action/info';
import CheckOffSlim from 'edc-web-sdk/components/icons/CheckOffSlim';
import { Scrollbars } from 'react-custom-scrollbars';

export default class CardSharingSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showFromList: false,
      activeType: 'channel',
      membersSuggestions: [],
      channelsSuggestions: [],
      groupsSuggestions: [],
      pending: false
    };
    this.styles = {
      tagIconStyles: {
        height: '0.875rem',
        width: '1.5rem',
        marginBottom: '.0625rem'
      },
      tagGroupIconStyles: {
        height: '0.875rem',
        width: '2rem',
        marginBottom: '.125rem'
      },
      searchIcon: {
        position: 'absolute',
        left: '0.25rem',
        verticalAlign: 'middle',
        width: '1.2rem',
        height: '1.2rem',
        fill: '#acadc1',
        top: 0
      },
      checkbox: {
        fontSize: '.75rem',
        marginBottom: '.3125rem'
      },
      fromList: {
        textAlign: 'right',
        marginTop: '.1875rem'
      },
      checkOff: {
        width: '1rem',
        height: '1rem',
        border: '.0625rem solid #acadc1',
        borderRadius: '.125rem',
        boxSizing: 'border-box',
        margin: '.125rem 0 0 .125rem'
      }
    };
    this.membersSuggestionsOffset = 0;
    this.channelsSuggestionsOffset = 0;
    this.groupsSuggestionsOffset = 0;
    this.membersAll = false;
    this.channelsAll = false;
    this.groupsAll = false;
  }

  getSuggestions = isScrolledToBottom => {
    let payload = {
      limit: 15,
      offset: this[`${this.state.activeType}sSuggestionsOffset`],
      [`${this.state.activeType === 'group' ? 'search_term' : 'q'}`]:
        (this._searchInput && this._searchInput.value) || ''
    };
    this.setState({
      pending: true
    });
    switch (this.state.activeType) {
      case 'member':
        payload['fields'] = 'id,full_name';
        getItems(payload)
          .then(members => {
            let membersSuggestions =
              (members.items &&
                members.items.map(member => ({
                  name: `${member.fullName}`,
                  id: member.id,
                  label: 'member'
                }))) ||
              [];
            this.setState(
              {
                membersSuggestions: isScrolledToBottom
                  ? this.state.membersSuggestions.concat(membersSuggestions)
                  : membersSuggestions,
                pending: false
              },
              () => {
                this.membersAll = this.state.membersSuggestions.length === members.totalCount;
              }
            );
          })
          .catch(err => {
            console.error(`Error in CardSharingSection.getSuggestions.getItems.func : ${err}`);
          });
        break;
      case 'group':
        payload['writables'] = true;
        payload['fields'] = 'id,name';
        getListV2(payload)
          .then(groups => {
            let groupsSuggestions =
              (groups.teams &&
                groups.teams.map(group => ({
                  name: `${group.name}`,
                  id: group.id,
                  label: 'group'
                }))) ||
              [];
            this.setState(
              {
                groupsSuggestions: isScrolledToBottom
                  ? this.state.groupsSuggestions.concat(groupsSuggestions)
                  : groupsSuggestions,
                pending: false
              },
              () => {
                this.groupsAll = this.state.groupsSuggestions.length === groups.total;
              }
            );
          })
          .catch(err => {
            console.error(`Error in CardSharingSection.getSuggestions.getListV2.func : ${err}`);
          });
        break;
      case 'channel':
        payload['writables'] = true;
        payload['fields'] = 'id,label';
        getChannelsMinimal(payload)
          .then(data => {
            let channelsSuggestions = data.channels.map(channel => ({
              name: `${channel.label}`,
              id: channel.id,
              label: 'channel'
            }));
            this.setState(
              {
                channelsSuggestions: isScrolledToBottom
                  ? this.state.channelsSuggestions.concat(channelsSuggestions)
                  : channelsSuggestions,
                pending: false
              },
              () => {
                this.channelsAll = this.state.channelsSuggestions.length === data.total;
              }
            );
          })
          .catch(err => {
            console.error(
              `Error in CardSharingSection.getSuggestions.getChannelsMinimal.func : ${err}`
            );
          });
        break;
      default:
        break;
    }
  };

  toggleFromList = e => {
    e.preventDefault();
    this.setState(
      prevState => {
        return {
          showFromList: !prevState.showFromList,
          pending: !prevState.showFromList
        };
      },
      () => {
        if (this.state.showFromList) {
          this.getSuggestions();
        }
      }
    );
  };

  changeActiveTab = (e, activeType) => {
    this[`${activeType}sSuggestionsOffset`] = 0;
    e.preventDefault();
    this.setState({ activeType, pending: !this[`${activeType}sAll`] }, () => {
      !this[`${activeType}sAll`] && this.getSuggestions();
    });
  };

  handleSearch = debounce(
    () => {
      this[`${this.state.activeType}sAll`] = false;
      this[`${this.state.activeType}sSuggestionsOffset`] = 0;
      this.getSuggestions();
    },
    300,
    { leading: false, trailing: true }
  );

  selectedTags = tag => {
    return (
      <span
        className="react-tags__selected react-tags__selected-tag react-tags__selected-tag-name"
        onClick={() => this.props.handleDelete(tag.tag)}
      >
        {tag && tag.tag && tag.tag.label === 'group' && (
          <GroupActiveIcon color="#7a7b96" viewBox="0 0 45 30" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'channel' && (
          <ChannelIconActive color="#7a7b96" style={this.styles.tagIconStyles} />
        )}
        {tag && tag.tag && tag.tag.label === 'member' && (
          <MemberBadgev2 color="#7a7b96" viewBox="0 2 24 18" style={this.styles.tagIconStyles} />
        )}
        {tag.tag.name}
      </span>
    );
  };

  handleScrollFrame = throttle(
    values => {
      const { scrollTop, clientHeight, scrollHeight } = values;
      if (this.state.pending || this[`${this.state.activeType}sAll`]) {
        return;
      }
      if (scrollTop + clientHeight >= scrollHeight) {
        this[`${this.state.activeType}sSuggestionsOffset`] += 15;
        this.getSuggestions(true);
      }
    },
    150,
    { leading: false, trailing: true }
  );

  render() {
    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    return (
      <div style={{ position: 'relative' }}>
        {useFormLabels && (
          <label htmlFor={tr('Share with users, groups, and channels')}>
            {tr('Share with users, groups, and channels')}
          </label>
        )}

        <ReactTags
          tags={this.props.tags}
          suggestions={this.props.tagSuggestions}
          handleAddition={this.props.handleAddition}
          handleDelete={this.props.handleDelete}
          placeholder={
            useFormLabels
              ? ''
              : this.props.tags.length
              ? ''
              : tr('Share with users, groups, and channels')
          }
          tagComponent={this.selectedTags}
          handleInputChange={this.props.handleInputChange}
          autofocus={false}
        />
        <p style={this.styles.fromList}>
          <a className="advanced-label" onClick={this.toggleFromList}>
            {tr('From list')}
          </a>
        </p>
        {this.state.showFromList && (
          <div className="from-list-container">
            <div className="sharing-tabs-container">
              <a
                className={`sharing-tab-name ${
                  this.state.activeType === 'channel' ? 'sharing-tab-name__active' : ''
                }`}
                style={{ marginLeft: '-.3125rem' }}
                onClick={e => this.changeActiveTab(e, 'channel')}
              >
                <ChannelIconActive color="#7a7b96" style={this.styles.tagIconStyles} />
                <span style={{ paddingLeft: '.0625rem' }}>{tr('Channel')}</span>
              </a>
              <a
                className={`sharing-tab-name ${
                  this.state.activeType === 'group' ? 'sharing-tab-name__active' : ''
                }`}
                onClick={e => this.changeActiveTab(e, 'group')}
              >
                <GroupActiveIcon
                  color="#7a7b96"
                  viewBox="0 0 45 30"
                  style={this.styles.tagGroupIconStyles}
                />
                <span style={{ paddingLeft: '.125rem' }}>{tr('Group')}</span>
              </a>
              <a
                className={`sharing-tab-name ${
                  this.state.activeType === 'member' ? 'sharing-tab-name__active' : ''
                }`}
                onClick={e => this.changeActiveTab(e, 'member')}
              >
                <MemberBadgev2
                  color="#7a7b96"
                  viewBox="0 2 24 18"
                  style={this.styles.tagIconStyles}
                />
                <span style={{ paddingLeft: '.0625rem' }}>{tr('User')}</span>
              </a>
            </div>
            <div className="sharing-search-block">
              <input
                placeholder={tr('Search')}
                onChange={this.handleSearch}
                type="text"
                ref={node => (this._searchInput = node)}
              />
              <SearchIcon
                style={this.styles.searchIcon}
                onClick={this.handleSearch}
                viewBox="0 -3 24 24"
              />
            </div>
            {this.state[`${this.state.activeType}sSuggestions`].length ? (
              <Scrollbars
                style={{ height: 138 }}
                onScrollFrame={this.handleScrollFrame}
                renderThumbVertical={props => <div {...props} className="track-vertical" />}
                thumbMinSize={36}
              >
                {this.state[`${this.state.activeType}sSuggestions`].map(item => {
                  return (
                    <Checkbox
                      key={`${this.state.activeType}-${item.id}`}
                      style={this.styles.checkbox}
                      className="checkbox-to-share"
                      checked={
                        findIndex(
                          this.props.tags,
                          entity =>
                            +entity.id === +item.id && this.state.activeType === entity.label
                        ) > -1
                      }
                      onCheck={e => this.props.checkToShare(e, item)}
                      uncheckedIcon={<div style={this.styles.checkOff} />}
                      iconStyle={{ fill: '#4d4f6b', width: '1.25rem', height: '1.25rem' }}
                      label={item.name}
                      labelStyle={{ color: '#6f708b' }}
                    />
                  );
                })}
                {this.state.pending && (
                  <div className="spinner-container">
                    <Spinner />
                  </div>
                )}
              </Scrollbars>
            ) : this.state.pending ? (
              <div className="spinner-container">
                <Spinner />
              </div>
            ) : (
              <p className="no-items-found">{tr(`No ${this.state.activeType}s found`)}</p>
            )}
          </div>
        )}
        {!this.props.pathwayPart && (
          <div>
            <div style={this.props.styles.privateChannelStyle}>
              {this.props.showPrivateChannelMsg && (
                <div style={{ display: 'flex' }}>
                  <ActionInfo style={this.props.styles.privateChannelIconStyle} />
                  <p
                    style={{
                      verticalAlign: 'middle',
                      margin: '0 0 0 5px'
                    }}
                  >
                    {tr(this.props.privateChannelMsg)}
                  </p>
                </div>
              )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

CardSharingSection.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.any),
  tagSuggestions: PropTypes.arrayOf(PropTypes.any),
  handleAddition: PropTypes.any,
  handleDelete: PropTypes.any,
  selectedTags: PropTypes.any,
  handleInputChange: PropTypes.any,
  toggleFromList: PropTypes.func,
  checkToShare: PropTypes.func,
  pathwayPart: PropTypes.bool,
  styles: PropTypes.any,
  showPrivateChannelMsg: PropTypes.bool,
  privateChannelMsg: PropTypes.any
};
