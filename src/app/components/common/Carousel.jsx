import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

import IconButton from 'material-ui/IconButton';
import NextIcon from 'material-ui/svg-icons/image/navigate-next';
import PrevIcon from 'material-ui/svg-icons/image/navigate-before';

class Carousel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isNewTileCard:
        window.ldclient.variation('new-ui-tile-card', false) ||
        window.ldclient.variation('card-v3', false)
    };
    this.currentIndex = 0;
    this.styles = {
      arrowButton: {
        position: 'absolute'
      }
    };

    let slidesToScroll = props.slidesToShow ? +props.slidesToShow : 1;
    let isDiscovery = props.isDiscovery;
    let isDiscovery_card = props.isDiscovery_card;
    let isTeamFeedOn = props.isTeamFeedOn;
    let isProfileCardCarousel = props.isProfileCardCarousel;
    let isChannelInfoCarousel = props.isChannelInfoCarousel;
    let isGroupCarousel = props.isGroupCarousel;
    let isPeopleCarouselV3 = props.isPeopleCarouselV3;

    this.newTileCardSettings = [
      {
        breakpoint: 100000,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 5
            : 6
          : isDiscovery_card
          ? 6
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 9
            : 7
          : isProfileCardCarousel
          ? 6
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 2100,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 5
            : 6
          : isDiscovery_card
          ? 6
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 9
            : 7
          : isProfileCardCarousel
          ? 5
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1967,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 4
            : 5
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 9
            : 7
          : isProfileCardCarousel
          ? 5
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1804,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 4
            : 5
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 9
            : 7
          : isProfileCardCarousel
          ? 4
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1760,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 4
            : 5
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 8
            : 6
          : isProfileCardCarousel
          ? 4
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1670,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 8
            : 6
          : isProfileCardCarousel
          ? 4
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1600,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 7
            : 5
          : isProfileCardCarousel
          ? 4
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1488,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 6
            : 4
          : isProfileCardCarousel
          ? 3
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1350,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 2
            : 3
          : isDiscovery_card
          ? 3
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 6
            : 4
          : isProfileCardCarousel
          ? 3
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1240,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 2
            : 3
          : isDiscovery_card
          ? 3
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : isProfileCardCarousel
          ? 3
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1150,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 2
            : 3
          : isDiscovery_card
          ? 2
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : isProfileCardCarousel
          ? 2
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1024,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 1
            : 2
          : isDiscovery_card
          ? 2
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards
          ? 1
          : isProfileCardCarousel
          ? 2
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow === 5
          ? 4
          : 3,
        slidesToScroll: slidesToScroll > 4 ? 4 : slidesToScroll
      },
      {
        breakpoint: 920,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 1
            : 2
          : isDiscovery_card
          ? 2
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards || isProfileCardCarousel
          ? 1
          : isChannelInfoCarousel
          ? 2
          : 3,
        slidesToScroll: slidesToScroll > 3 ? 3 : slidesToScroll
      },
      {
        breakpoint: 785,
        slidesNumber:
          isGroupCarousel || isDiscovery_card
            ? 1
            : this.props.slidesToShow
            ? this.props.slidesToShow
            : this.props.channelPinnedCards || isProfileCardCarousel
            ? 1
            : isChannelInfoCarousel
            ? 1
            : 2,
        slidesToScroll: slidesToScroll > 2 ? 2 : slidesToScroll
      },
      {
        breakpoint: 540,
        slidesNumber: 1,
        slidesToScroll: 1
      }
    ];

    this.cardSettings = [
      {
        breakpoint: 100000,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 6
            : 7
          : isDiscovery_card
          ? 7
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 9
            : 7
          : isProfileCardCarousel
          ? 6
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1760,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 6
            : 7
          : isDiscovery_card
          ? 7
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 8
            : 6
          : isProfileCardCarousel
          ? 6
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1600,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 5
            : 6
          : isDiscovery_card
          ? 6
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 7
            : 5
          : isProfileCardCarousel
          ? 6
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1550,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 5
            : 6
          : isDiscovery_card
          ? 6
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 7
            : 5
          : isProfileCardCarousel
          ? 6
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1520,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 4
            : 5
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 7
            : 5
          : isProfileCardCarousel
          ? 5
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1440,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 4
            : 5
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 6
            : 4
          : isProfileCardCarousel
          ? 5
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1340,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 5
          : isDiscovery
          ? this.props.slidesToShow === 5
            ? 6
            : 4
          : isProfileCardCarousel || isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1240,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : isProfileCardCarousel || isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1150,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : isProfileCardCarousel || isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1080,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 4
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : isProfileCardCarousel
          ? 3
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1024,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 3
            : 4
          : isDiscovery_card
          ? 3
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards
          ? 1
          : isProfileCardCarousel
          ? 3
          : isChannelInfoCarousel
          ? 4
          : this.props.slidesToShow === 5
          ? 4
          : 3,
        slidesToScroll: slidesToScroll > 4 ? 4 : slidesToScroll
      },
      {
        breakpoint: 885,
        slidesNumber: isGroupCarousel
          ? isTeamFeedOn
            ? 2
            : 3
          : isDiscovery_card
          ? 3
          : isProfileCardCarousel
          ? 2
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards
          ? 1
          : 3,
        slidesToScroll: slidesToScroll > 3 ? 3 : slidesToScroll
      },
      {
        breakpoint: 785,
        slidesNumber: isGroupCarousel
          ? 2
          : isDiscovery_card
          ? 2
          : isProfileCardCarousel
          ? 2
          : isChannelInfoCarousel
          ? 3
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards
          ? 1
          : 2,
        slidesToScroll: slidesToScroll > 2 ? 2 : slidesToScroll
      },
      {
        breakpoint: 655,
        slidesNumber: isGroupCarousel
          ? 2
          : isDiscovery_card
          ? 2
          : isProfileCardCarousel
          ? 1
          : isChannelInfoCarousel
          ? 2
          : this.props.slidesToShow
          ? this.props.slidesToShow
          : this.props.channelPinnedCards
          ? 1
          : 2,
        slidesToScroll: slidesToScroll > 2 ? 2 : slidesToScroll
      },
      {
        breakpoint: 540,
        slidesNumber: 1,
        slidesToScroll: 1
      }
    ];

    this.newPeopleCardSettings = [
      {
        breakpoint: 100000,
        slidesNumber: 10,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 2100,
        slidesNumber: 8,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1967,
        slidesNumber: 7,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1804,
        slidesNumber: 6,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1760,
        slidesNumber: 6,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1670,
        slidesNumber: 6,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1600,
        slidesNumber: 6,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1488,
        slidesNumber: 5,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1350,
        slidesNumber: 4,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1240,
        slidesNumber: 4,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1150,
        slidesNumber: 3,
        slidesToScroll:
          slidesToScroll > this.props.slidesToShow ? this.props.slidesToShow : slidesToScroll
      },
      {
        breakpoint: 1024,
        slidesNumber: 4,
        slidesToScroll: slidesToScroll > 4 ? 4 : slidesToScroll
      },
      { breakpoint: 920, slidesNumber: 3, slidesToScroll: slidesToScroll > 3 ? 3 : slidesToScroll },
      { breakpoint: 785, slidesNumber: 2, slidesToScroll: slidesToScroll > 2 ? 2 : slidesToScroll },
      { breakpoint: 540, slidesNumber: 1, slidesToScroll: 1 }
    ];
    if (this.props.carouselSettings && this.props.carouselSettings.length) {
      this.carouselSettings = this.props.carouselSettings;
    } else {
      this.carouselSettings = isPeopleCarouselV3
        ? this.newPeopleCardSettings
        : this.state.isNewTileCard
        ? this.newTileCardSettings
        : this.cardSettings;
    }
  }

  componentDidMount() {
    this.checkCarouselCardsFit(this.props);
  }
  componentWillReceiveProps(nextProps) {
    this.checkCarouselCardsFit(nextProps);
  }

  checkCarouselCardsFit = props => {
    if (props.windowInnerWidth) {
      let slidesNumber;
      for (let i = this.carouselSettings.length - 1; i >= 0; i--) {
        if (props.windowInnerWidth < this.carouselSettings[i].breakpoint) {
          slidesNumber = this.carouselSettings[i].slidesNumber;
          break;
        }
      }
      if (!this.slidesNumber || this.slidesNumber !== slidesNumber) {
        this.slidesNumber = slidesNumber;
        this.props.showViewAll(this.slidesNumber < this.props.children.length);
      }
    }
  };

  reloadVideos = e => {
    let allIframeVideo = document.querySelectorAll(
      '.slick-slide .card-overview-content .link-card-video iframe'
    );
    let allVideos = document.querySelectorAll(
      '.slick-slide .card-overview-content .fp_video_card video'
    );
    for (let i = 0; i < allIframeVideo.length; i++) {
      allIframeVideo[i].src = allIframeVideo[i].src;
    }
    for (let i = 0; i < allVideos.length; i++) {
      allVideos[i].pause();
    }
  };

  render() {
    let childrenCount =
      this.props.children &&
      this.props.children.filter(item => {
        return item && item.key && item.key != 'empty';
      }).length;
    let slidesToScroll = this.props.slidesToShow ? +this.props.slidesToShow : 1;
    let autoplay = this.props.autoplay ? this.props.autoplay : false;
    let infinite = this.props.infinite ? this.props.infinite : false;
    let autoplaySpeed = this.props.autoplaySpeed ? this.props.autoplaySpeed : 0;

    let settings = {
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      arrows: childrenCount >= this.props.slidesToShow,
      slidesToShow: this.props.slidesToShow,
      autoplay: autoplay,
      autoplaySpeed: autoplaySpeed,
      responsive: [],
      infinite: infinite,
      slidesToScroll: slidesToScroll,
      afterChange: this.reloadVideos
    };

    this.carouselSettings.map((element, index) => {
      settings.responsive[index] = {
        breakpoint: element.breakpoint,
        settings: {
          slidesToShow: element.slidesNumber,
          arrows: childrenCount >= element.slidesNumber,
          slidesToScroll: element.slidesToScroll,
          afterChange: this.reloadVideos
        }
      };
    });

    return (
      <div className="carousel">
        <Slider {...settings}>{this.props.children}</Slider>
      </div>
    );
  }
}

Carousel.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  autoplay: PropTypes.bool,
  infinite: PropTypes.bool,
  isUserProfile: PropTypes.bool,
  isDiscovery: PropTypes.bool,
  isProfileCardCarousel: PropTypes.string,
  onClick: PropTypes.func,
  channelPinnedCards: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  slidesToShow: PropTypes.number,
  isPeopleCarouselV3: PropTypes.bool,
  carouselSettings: PropTypes.array
};
/*eslint react/prop-types: "off"*/

let NextArrow = props => (
  <div className="slick-next">
    <IconButton
      aria-label="next"
      className="nextArrow"
      onTouchTap={props.onClick}
      disabled={props.className.indexOf('disabled') !== -1}
    >
      <NextIcon />
    </IconButton>
  </div>
);
let PrevArrow = props => (
  <div className="slick-prev">
    <IconButton
      aria-label="previous"
      className="prevArrow"
      onTouchTap={props.onClick}
      disabled={props.className.indexOf('disabled') !== -1}
    >
      <PrevIcon />
    </IconButton>
  </div>
);

export default Carousel;
