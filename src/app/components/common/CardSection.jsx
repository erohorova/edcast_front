import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from 'material-ui/CircularProgress';
import { eclSearch } from 'edc-web-sdk/requests/ecl';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../cards/Card';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import Spinner from '../common/spinner';

class CardSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: 4,
      offset: 0,
      pending: true,
      cards: [],
      isLastPage: false,
      isNewTileCard: window.ldclient.variation('new-ui-tile-card', false),
      zeroCardsTotal: true,
      totalCardsCountReceived: 0
    };
  }

  componentDidMount() {
    let queryFromTopic = '';
    if (this.props.topic['topic_name']) {
      queryFromTopic = this.props.topic['topic_name'].split('.').pop();
      queryFromTopic = queryFromTopic.replace(/_/g, ' ');
    }
    eclSearch({
      q: queryFromTopic,
      'source_id[]': this.props.sourceId,
      limit: 24,
      offset: this.state.offset,
      sort_attr: this.props.sortInfo.sort_attr,
      sort_order: this.props.sortInfo.sort_order
    })
      .then(data => {
        let source_id_cards_count =
          data.aggregations &&
          data.aggregations.filter(obj => {
            return obj.type == 'source_id' || obj.type == 'source_type_name';
          })[0].count;
        this.setState({
          cards: data.cards.slice(0, data.cards.length),
          isLastPage: data.cards.length > 4,
          pending: false,
          offset: data.cards.slice(0, 24).length,
          zeroCardsTotal: data.cards.length === 0,
          totalCardsCountReceived: source_id_cards_count
        });
      })
      .catch(() => {
        this.setState({ pending: false, cards: [], zeroCardsTotal: true });
      });
  }

  loadMore = () => {
    this.setState({ pending: true });
    eclSearch({
      q: '',
      'topic[]': [this.props.topic['topic_name']],
      'source_id[]': this.props.sourceId,
      limit: 24,
      offset: this.state.offset,
      sort_attr: this.props.sortInfo.sort_attr,
      sort_order: this.props.sortInfo.sort_order
    })
      .then(data => {
        let allCards = this.state.cards.concat(data.cards.slice(0, data.cards.length));
        this.setState({
          cards: allCards,
          isLastPage: data.cards.length >= 4,
          pending: false,
          offset: allCards.length
        });
      })
      .catch(() => {
        this.setState({ pending: false });
      });
  };

  hideCard = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({ cards: newCardsList });
  };

  render() {
    if (this.state.zeroCardsTotal === true) {
      return null;
    } else {
      return (
        <div className="container-padding vertical-spacing-large">
          {this.state.cards.length > 0 && this.props.topic && (
            <div>
              <strong>{`${tr('Recommended in')} ${this.props.topic['topic_label']}`}</strong>
            </div>
          )}
          <div className="custom-card-container">
            <div
              className={`${
                this.state.isNewTileCard ? 'three-card-column' : 'four-card-column'
              } vertical-spacing-medium`}
            >
              {this.state.cards.map(card => {
                return (
                  <Card
                    key={card.id}
                    card={card}
                    author={card.author}
                    providerLogos={this.props.providerLogos}
                    moreCards={true}
                    dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                    startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                    providerCards={false}
                    user={this.props.currentUser}
                    removeCardFromList={this.hideCard}
                    hideComplete={this.hideCard}
                  />
                );
              })}
            </div>
            {!this.state.pending && this.state.cards.length < this.state.totalCardsCountReceived && (
              <div style={{ textAlign: 'center' }}>
                <SecondaryButton
                  label={tr('Show More')}
                  className="viewMore"
                  onTouchTap={this.loadMore}
                />
              </div>
            )}
            {this.state.cards.length > 0 && this.state.pending && (
              <div className="progress text-center">
                <Spinner />
              </div>
            )}
          </div>
        </div>
      );
    }
  }
}

CardSection.propTypes = {
  sourceId: PropTypes.string,
  sortInfo: PropTypes.object,
  currentUser: PropTypes.object,
  providerLogos: PropTypes.object,
  topic: PropTypes.object
};

export default CardSection;
