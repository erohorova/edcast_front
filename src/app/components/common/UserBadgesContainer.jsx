import React, { Component } from 'react';
import PropTypes from 'prop-types';
import colors from 'edc-web-sdk/components/colors/index';
import { GroupLeaderBadge } from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqWith from 'lodash/uniqWith';
import concat from 'lodash/concat';

class UserBadgesContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      iconColor: '#454560'
    };

    this.styles = {
      tooltipStyles: {
        marginTop: '-17px',
        zIndex: 1
      },
      badgeStyles: {
        padding: '0px',
        height: '30px',
        width: '30px',
        marginLeft: '5px',
        verticalAlign: 'top'
      },
      iconStyle: {
        borderRadius: '20px',
        color: colors.white
      }
    };
  }

  getRoleBadges = (roles, rolesDefaultNames = []) => {
    let allRoles = uniqWith(
      concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    let badgeIcons = [];
    allRoles.map((role, index) => {
      let data,
        roleBadgesStyle = 'role-badges-style';
      let roleParam = role ? role.toLowerCase() : '';
      if (roleParam === 'member') {
        data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
        roleBadgesStyle = 'role-badges-style role-badge-profile';
      } else if (roleParam === 'admin') {
        data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'groupleader') {
        data = [<GroupLeaderBadge />, this.state.iconColor];
      } else if (roleParam === 'influencer') {
        data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'sme') {
        data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
      }
      if (data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;
        badgeIcons.push(
          <IconButton
            className={roleBadgesStyle}
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={this.styles.tooltipStyles}
            disableTouchRipple
            style={Object.assign({}, this.styles.badgeStyles, this.props.badgeStylesComponent)}
            iconStyle={Object.assign(
              {},
              this.styles.iconStyle,
              { backgroundColor: data[1] },
              this.props.iconStyleComponent
            )}
          >
            {data[0]}
          </IconButton>
        );
      }
    });
    return badgeIcons;
  };

  render() {
    let roles = this.props.roles || [];
    let rolesDefaultNames = this.props.rolesDefaultNames;
    return <span className="badge-line">{this.getRoleBadges(roles, rolesDefaultNames)}</span>;
  }
}

UserBadgesContainer.propTypes = {
  roles: PropTypes.any,
  rolesDefaultNames: PropTypes.any,
  badgeStylesComponent: PropTypes.any,
  iconStyleComponent: PropTypes.any
};

export default UserBadgesContainer;
