import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import customAutoCorrectedDatePipe from './CustomAutoCorrectedDatePipe';

class SmartBiteLevel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      inputValue: this.props.value || ''
    };

    this.styles = {
      inputs: {
        border: '1px solid #d6d6e1',
        padding: '5px 10px',
        fontSize: '14px',
        lineHeight: '14px',
        color: '#999aad',
        borderRadius: '2px'
      },
      hint: {
        padding: '0 10px',
        fontSize: '12px',
        lineHeight: '31px',
        textAlign: 'left',
        color: '#acadc1',
        top: 0,
        bottom: 0,
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
      },
      mainInputStyle: {
        fontSize: '14px',
        lineHeight: '14px',
        height: 'auto'
      },
      error: {
        padding: '0.5rem 1rem 0'
      },
      searchIcon: {
        position: 'absolute',
        cursor: 'pointer',
        right: '10px',
        top: '10px'
      }
    };
    this.autoCorrectedDatePipe = customAutoCorrectedDatePipe(
      this.props.pipe ? this.props.pipe : ''
    );
  }

  rateCardHandler(level, e) {
    e.preventDefault();
    this.props.rateCard(level);
  }

  render() {
    let {
      allowConsumerModifyLevel,
      params,
      newSkillLevel,
      card,
      isV3,
      isRevertColor,
      type
    } = this.props;

    let dynamicStyles = {};

    let beginnerDynamicProps = {};
    let intermediateDynamicProps = {};
    let advancedDynamicProps = {};

    if (!(allowConsumerModifyLevel || params.isOwner)) {
      dynamicStyles['style'] = { cursor: 'default' };
    } else {
      dynamicStyles['style'] = { cursor: 'pointer' };
      if (!(isV3 && type !== 'standalone')) {
        beginnerDynamicProps['onClick'] = this.rateCardHandler.bind(this, 'beginner');
        intermediateDynamicProps['onClick'] = this.rateCardHandler.bind(this, 'intermediate');
        advancedDynamicProps['onClick'] = this.rateCardHandler.bind(this, 'advanced');
      }
    }

    return (
      <div
        className={`card-bia ${isV3 ? 'new-card-bia' : ''} ${
          isRevertColor ? 'new-card-bia_revert-colors' : ''
        }`}
      >
        <div className={`${newSkillLevel || card.skillLevel} level-dots`}>
          <a tabIndex={0} {...beginnerDynamicProps} {...dynamicStyles}>
            <span className="first">
              <div className="level-tip">{tr('Beginner')}</div>
            </span>
          </a>
          <a tabIndex={0} {...intermediateDynamicProps} {...dynamicStyles}>
            <span className="second">
              <div className="level-tip">{tr('Intermediate')}</div>
            </span>
          </a>
          <a tabIndex={0} {...advancedDynamicProps} {...dynamicStyles}>
            <span className="third">
              <div className="level-tip">{tr('Advanced')}</div>
            </span>
          </a>
        </div>
        <div className="level">{newSkillLevel || card.skillLevel}</div>
      </div>
    );
  }
}

SmartBiteLevel.propTypes = {
  allowConsumerModifyLevel: PropTypes.any,
  params: PropTypes.object,
  newSkillLevel: PropTypes.any,
  card: PropTypes.object,
  pipe: PropTypes.string,
  value: PropTypes.string,
  rateCard: PropTypes.any,
  isRevertColor: PropTypes.bool,
  isV3: PropTypes.bool
};

export default SmartBiteLevel;
