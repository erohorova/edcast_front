import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import NavigationArrowDropDown from 'react-material-icons/icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'react-material-icons/icons/navigation/arrow-drop-up';

import { getRecommendedUsers } from 'edc-web-sdk/requests/users';
import { getChannels } from 'edc-web-sdk/requests/channels.v2';

import { getRecommendedCards } from '../../actions/cardsActions';
import { getRecommendedItems } from '../../actions/recommendedActions';
import {
  openUpdateInterestsModal,
  openUpdateInterestsOnboardV2Modal
} from '../../actions/modalActions';

import Card from '../cards/Card';
import Influencer from '../discovery/Influencer.jsx';
import Channel from '../discovery/Channel.jsx';

class RecommendedBlock extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isOpenBlock: true,
      recommendedItems: []
    };
    this.styles = {
      accordionTitleIcon: {
        fill: 'inherit',
        height: '2.125rem',
        width: '2.125rem'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      }
    };
    this.label = '';
    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
  }
  componentDidMount() {
    let recommendedItems = [];
    let payload = {
      content_id: this.props.card_id
    };
    if (this.props.tag) {
      payload.tag = this.props.tag;
    }
    getRecommendedItems(payload)
      .then(items => {
        if (items && items[this.props.type]) {
          switch (this.props.type) {
            case 'cards':
              this.label = tr('Related SmartCards');
              if (this.props.tag === 'CUC') {
                this.label = tr(
                  'Members who find this SmartCard useful also found following SmartCards useful'
                );
              }
              recommendedItems = items.cards
                ? items.cards
                    .filter(
                      item => item.eclId !== this.props.card_id && item.id !== this.props.card_id
                    )
                    .slice(0, 3)
                : [];
              this.setState({ recommendedItems });
              break;
            case 'users':
              this.label = tr('Members who find this SmartCard interesting');
              recommendedItems = items.users ? items.users.slice(0, 4) : [];
              this.setState({ recommendedItems });
              break;
            case 'channels':
              this.label = tr('Related channels to this topic');
              recommendedItems = items.channels ? items.channels.slice(0, 4) : [];
              this.setState({ recommendedItems });
              break;
            case 'topics':
              this.label = tr('Topics you might also be interested in'); // Label is coded in InsightV2.jsx component
              recommendedItems = items.topics ? items.topics.slice(0, 4) : [];
              // We need to inject this into the main card view (InsightV2.jsx);
              let topicRef = document.querySelector('#content-topics-ref');
              if (topicRef && recommendedItems.length > 0) {
                topicRef.style.display = 'block';
                let topicsEl = document.querySelector('#content-topics');
                if (topicsEl) {
                  topicsEl.innerHTML = recommendedItems.map(item => item.label).join(', ');
                }
              }
              // this.setState({recommendedItems});
              break;
            default:
              // FIXME: implement default case
              // NOTE @rmahmoodi: There will be no default case. Do not display if no data.
              break;
          }
        } else {
          // this.getDefaultValues();
        }
      })
      .catch(err => {
        console.error(`Error in RecommendedBlock.getRecommendedItems.func : ${err}`);
      });
  }

  // Not deleting, may implement later
  // getDefaultValues = () => {
  //   switch (this.props.type) {
  //     case 'cards':
  //       this.label = tr('Related SmartCards');
  //       if (this.props.tag === 'CUC') {
  //         this.label = tr('Members who find this SmartCard useful also found following SmartCards useful');
  //       }
  //       getRecommendedCards().then((data) => {
  //         this.setState({recommendedItems: data});
  //       });
  //       break;
  //     case 'users':
  //       this.label = tr('Members who find this SmartCard interesting');
  //       getRecommendedUsers({limit: 4}).then((users) => {
  //         let recommendedItems = users.slice(0, 4);
  //         this.setState({recommendedItems});
  //       });
  //       break;
  //     case 'channels':
  //       this.label = tr('Related channels to this topic');
  //       getChannels(4).then((data) => {
  //         this.setState({recommendedItems: data});
  //       });
  //       break;
  //     default:
  //       // FIXME: implement default case
  //       break;
  //   }
  // };

  openBlock = () => {
    this.setState(prevState => {
      return {
        isOpenBlock: !prevState.isOpenBlock
      };
    });
  };

  triggerUpdateInterests = () => {
    if (this.onboardingVersion === 'v2') {
      this.props.dispatch(openUpdateInterestsOnboardV2Modal(this.props.currentUser.id));
    } else {
      this.props.dispatch(openUpdateInterestsModal(this.props.currentUser.id));
    }
  };

  render() {
    if (this.state.recommendedItems.length === 0) {
      return null;
    }
    return (
      <div className="journey-part__container" key={`recommented-card-block_${this.props.type}`}>
        <div
          className={`${
            this.state.isOpenBlock ? 'journey-section-header_open' : 'journey-section-header_close'
          } journey-section-header`}
        >
          <input
            type="text"
            className="journey-section-header__input"
            value={tr(this.label)}
            disabled="disabled"
          />
          <span>|</span>
          <button
            className="my-icon-button journey__accordion-title"
            onClick={() => {
              this.openBlock();
            }}
          >
            {this.state.isOpenBlock ? (
              <NavigationArrowDropDown
                style={{
                  ...this.styles.accordionTitleIcon,
                  ...this.styles.accordionTitleIcon_open
                }}
              />
            ) : (
              <NavigationArrowDropUp
                style={{
                  ...this.styles.accordionTitleIcon,
                  ...this.styles.accordionTitleIcon_close
                }}
              />
            )}
          </button>
        </div>
        {this.state.isOpenBlock && (
          <div>
            {this.props.type === 'cards' && (
              <div className="smartbites-block">
                <div className="custom-card-container">
                  <div className="five-card-column vertical-spacing-medium container__space-between recommend-cards flex card-wrapper-flex">
                    {this.state.recommendedItems.map((recommendedCard, index) => {
                      return recommendedCard.locked ? (
                        <div
                          className="card-v2 small-12 column"
                          key={`recommended-card_${recommendedCard.id}`}
                        >
                          <div className="paper paper-card" zDepth={0}>
                            <div className="locked-card" style={this.styles.lockedCard}>
                              <LockedIcon style={this.styles.bigLockIcon} color="#7c7d94" />
                            </div>
                            <div className="close close-button pathway-card-btn number-card-btn">
                              <span>{index + 1}</span>
                            </div>
                          </div>
                        </div>
                      ) : (
                        <Card
                          key={`recommended-card_${recommendedCard.id}`}
                          dueAt={
                            recommendedCard.dueAt ||
                            (recommendedCard.assignment && recommendedCard.assignment.dueAt)
                          }
                          startDate={
                            recommendedCard.startDate ||
                            (recommendedCard.assignment && recommendedCard.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          type={'Tile'}
                          inStandalone={true}
                          card={recommendedCard}
                          index={index + 1}
                          author={recommendedCard.author}
                          moreCards={false}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
            {this.props.type === 'users' && (
              <div className="smartbites-block">
                {this.state.recommendedItems.map((recommendedUser, index) => {
                  return (
                    <Influencer
                      key={`recommended-user_${recommendedUser.id}`}
                      id={recommendedUser.id}
                      name={recommendedUser.name}
                      handle={recommendedUser.handle}
                      imageUrl={
                        (recommendedUser.avatarimages && recommendedUser.avatarimages.medium) ||
                        this.defaultUserImage
                      }
                      expertSkills={recommendedUser.expertSkills}
                      roles={recommendedUser.roles}
                      rolesDefaultNames={recommendedUser.rolesDefaultNames}
                      position={'top'}
                      following={recommendedUser.isFollowing}
                    />
                  );
                })}
              </div>
            )}
            {this.props.type === 'channels' && (
              <div className="smartbites-block">
                <div className="custom-card-container">
                  <div className="five-card-column vertical-spacing-medium container__space-between">
                    {this.state.recommendedItems.map((recommendedChannel, index) => {
                      return (
                        <div
                          className="card-v2 search-people"
                          key={`recommended-channel_${recommendedChannel.id}`}
                        >
                          <div className="paper paper-card">
                            <Channel
                              channel={recommendedChannel}
                              id={recommendedChannel.id}
                              isFollowing={recommendedChannel.isFollowing}
                              title={recommendedChannel.label}
                              imageUrl={
                                recommendedChannel.profileImageUrl ||
                                recommendedChannel.bannerImageUrl
                              }
                              slug={recommendedChannel.slug}
                              allowFollow={recommendedChannel.allowFollow}
                              isPrivate={recommendedChannel.isPrivate}
                            />
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            )}
            {this.props.type === 'topics' && (
              <div className="smartbites-block">
                <div className="custom-card-container">
                  <div className="five-card-column vertical-spacing-medium container__space-between">
                    <button onClick={this.triggerUpdateInterests}>Update Interests</button>
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

RecommendedBlock.propTypes = {
  type: PropTypes.string,
  currentUser: PropTypes.object,
  isRecommended: PropTypes.bool,
  card_id: PropTypes.string,
  tag: PropTypes.string
};
function mapStoreStateToProps(state) {
  return { currentUser: state.currentUser.toJS() };
}

export default connect(mapStoreStateToProps)(RecommendedBlock);
