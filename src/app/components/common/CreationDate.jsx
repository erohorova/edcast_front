import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TimeAgo from 'react-timeago';
import { connect } from 'react-redux';
import translateTA from '../../utils/translateTimeAgo';

class CreationDate extends Component {
  constructor(props, context) {
    super(props, context);
  }

  openStandalone = e => {
    e.preventDefault();
    this.props.card.id.startsWith('ECL') ? null : this.props.standaloneLinkClickHandler();
  };

  render() {
    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);

    let card = this.props.card;
    if (card.publishedAt !== undefined && card.publishedAt !== null) {
      let dateObj = new Date(card.publishedAt.replace(' ', 'T') + '.000Z');
      if (!isNaN(dateObj.getMonth())) {
        card.timeAgo = dateObj;
      } else if (card.publishedAt != null) {
        card.timeAgo = new Date(card.publishedAt);
      }
    }
    if (card.timeAgo === undefined) return null;
    else {
      return (
        <a href="#" className="time-ago" onClick={this.openStandalone}>
          <TimeAgo date={card.timeAgo} live={false} formatter={formatter} />
        </a>
      );
    }
  }
}

CreationDate.propTypes = {
  standaloneLinkClickHandler: PropTypes.func.isRequired,
  currentUser: PropTypes.object,
  card: PropTypes.object.isRequired
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CreationDate);
