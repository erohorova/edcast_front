import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

class TooltipLabel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      tooltipEnabled: false
    };
  }

  componentWillReceiveProps(nextProps) {
    this.props.trigger === 'click' &&
      this.setState({
        tooltipEnabled: nextProps.isVisible && this.props.isVisible && nextProps.isShowTooltip
      });
  }

  render() {
    let customClass = `tooltip-snippet ${this.props.position}`;
    let sourceText =
      this.props.text.length > 81 ? this.props.text.substring(0, 80) + ' ...' : this.props.text;
    if (sourceText != '') {
      return (
        <div
          className="tooltip-parent"
          style={{ marginLeft: 'auto' }}
          onMouseEnter={() => {
            this.props.trigger !== 'click' && this.setState({ tooltipEnabled: true });
          }}
          onMouseLeave={() => {
            this.props.trigger !== 'click' && this.setState({ tooltipEnabled: false });
          }}
        >
          <div className="rail-text-title">
            {this.props.children}
            {this.props.html && this.state.tooltipEnabled && (
              <ReactCSSTransitionGroup
                transitionName="tooltip"
                transitionAppear={true}
                transitionAppearTimeout={300}
                transitionEnter={false}
                transitionLeave={true}
                transitionLeaveTimeout={300}
              >
                {this.props.trigger === 'click' ? (
                  <div className={customClass}>
                    <p className="info-tooltip-header">{this.props.header}</p>
                    {this.props.content.map(item => (
                      <p className="info-tooltip-line">{item}</p>
                    ))}
                  </div>
                ) : (
                  <div
                    className={customClass}
                    dangerouslySetInnerHTML={{ __html: this.props.text }}
                  />
                )}
              </ReactCSSTransitionGroup>
            )}
          </div>
          {!this.props.html && this.state.tooltipEnabled && (
            <div className="tooltip-snippet">{sourceText}</div>
          )}
        </div>
      );
    } else {
      return <div>{this.props.children}</div>;
    }
  }
}

TooltipLabel.deaultProps = {
  html: false,
  position: 'top'
};

TooltipLabel.propTypes = {
  text: PropTypes.string,
  header: PropTypes.string,
  trigger: PropTypes.string,
  isVisible: PropTypes.bool,
  isShowTooltip: PropTypes.bool,
  html: PropTypes.bool,
  content: PropTypes.object,
  children: PropTypes.any,
  position: PropTypes.string
};

export default TooltipLabel;
