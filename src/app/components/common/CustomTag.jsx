import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import SocialPerson from 'react-material-icons/icons/social/person';
import TeamActiveIcon from 'edc-web-sdk/components/icons/TeamActiveIcon';

class CustomTag extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      icon: {
        marginRight: '0.25rem',
        width: '1rem',
        height: '1rem'
      },
      tagIconStyles: {
        height: '0.8rem',
        paddingTop: '0.2rem',
        width: '1.3rem'
      }
    };
  }

  render() {
    return (
      <button
        type="button"
        className={this.props.classNames.selectedTag}
        onClick={this.props.onDelete}
        title={tr('Click to remove tag')}
      >
        {this.props.tag.type === 'channel' ? (
          <TeamActiveIcon viewBox="0 2 50 28" style={this.styles.tagIconStyles} />
        ) : (
          <SocialPerson style={this.styles.icon} />
        )}

        <span className={this.props.classNames.selectedTagName}>{this.props.tag.name}</span>
      </button>
    );
  }
}

CustomTag.propTypes = {
  classNames: PropTypes.object,
  tag: PropTypes.object,
  onDelete: PropTypes.func
};

export default connect()(CustomTag);
