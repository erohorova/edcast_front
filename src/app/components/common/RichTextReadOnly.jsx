import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RichTextEditor from 'react-rte';

class RichTextReadOnly extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {};
  }

  render() {
    let textValue = RichTextEditor.createValueFromString(this.props.text, 'markdown')
      .toString('html')
      .replace(/href="https:([/]*)/g, 'href="')
      .replace(/href="http:([/]*)/g, 'href="')
      .replace(/href="/g, 'href="//');
    let config = this.props.team && this.props.team.config && this.props.team.config.link_blank;
    let truncate = !!this.props.truncate;
    return (
      <div
        className={truncate ? 'rich-text-read-only truncate-text-250' : 'rich-text-read-only'}
        dangerouslySetInnerHTML={{
          __html: config ? textValue.replace(/<a /g, '<a target="_blank" ') : textValue
        }}
      />
    );
  }
}

RichTextReadOnly.propTypes = {
  text: PropTypes.string,
  team: PropTypes.object,
  truncate: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(RichTextReadOnly);
