import React from 'react';

const HorizontalBarMenuIcon = () => (
  <div className="bars-container">
    <div className="bar" />
    <div className="bar" />
    <div className="bar" />
    <div className="bar no-margin" />
  </div>
);

export default HorizontalBarMenuIcon;
