import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqWith from 'lodash/uniqWith';
import concat from 'lodash/concat';

import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';

import FollowButtonv2 from 'edc-web-sdk/components/FollowButtonv2';
import colors from 'edc-web-sdk/components/colors/index';
import { GroupLeaderBadge } from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';

import formatNumbers from '../../utils/formatNumbers';
import { toggleFollow } from '../../actions/usersActions';
import { Permissions } from '../../utils/checkPermissions';

class LeaderBoard extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      iconColor: '#454560',
      currentUser: this.props.currentUser
    };

    this.styles = {
      badgeStyles: {
        padding: '0 8px 0 0',
        width: 28,
        height: 20
      },
      iconStyle: {
        background: 'rgba(0, 0, 0, 0.6)',
        borderRadius: '50%',
        border: 'solid 0.5px #f0f0f5',
        color: '#ffffff',
        width: 20,
        height: 20
      },
      tooltipStyles: {
        top: '1rem',
        pointerEvents: 'none'
      }
    };

    this.userClickHandler = this.userClickHandler.bind(this);
    this.followClickHandler = this.followClickHandler.bind(this);
  }

  userClickHandler = () => {
    this.props.dispatch(push(`/${this.props.notification.user.handle}`));
  };

  followClickHandler(user) {
    this.props.dispatch(toggleFollow(user.id, !user.following));
  }

  getRoleBadges = (roles, rolesDefaultNames = []) => {
    let allRoles = uniqWith(
      concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    let badgeIcons = [];
    allRoles.map((role, index) => {
      let data;
      let roleParam = role ? role.toLowerCase() : '';

      switch (roleParam) {
        case 'member':
          data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'admin':
          data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'groupleader':
          data = [<GroupLeaderBadge />, this.state.iconColor];
          break;
        case 'influencer':
          data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'sme':
          data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        default:
          // FIXME: implement default case
          break;
      }
      if (data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;
        badgeIcons.push(
          <IconButton
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={this.styles.tooltipStyles}
            disableTouchRipple
            style={this.styles.badgeStyles}
            iconStyle={Object.assign({}, this.styles.iconStyle, { backgroundColor: data[1] })}
          >
            {data[0]}
          </IconButton>
        );
      }
    });
    return badgeIcons;
  };

  render() {
    let user = this.props.user;
    let member = this.props.member;
    let users = this.props.users;
    if (user && users['idMap'][user.id]['following'] === true) {
      user.following = true;
    } else if (user && users['idMap'][user.id]['following'] === false) {
      user.following = false;
    }
    let isCurrentUser = this.state.currentUser.id == user.id;
    return (
      <div
        className={`lbv2__table-row ${isCurrentUser ? 'lbv2__table-row_current-user' : ''}`}
        style={{ borderLeftColor: colors.followColor }}
      >
        <div className="lbv2__table-cell lbv2__table-cell_1 lbv2__table-cell_with-border lbv2__table-cell_flex">
          <span className="lbv2-table__index">
            {member.smartbitesScore === 0
              ? 'NA'
              : isCurrentUser && user.orderByScore > this.props.index + 2
              ? user.orderByScore
              : this.props.index + 1}
          </span>
          <div className="lbv2-table__user-info">
            <Avatar size={64} src={user.picture} />
            <div className="lbv2-table__user-info-text">
              <a className="matte lbv2-table__user-name" href={`/${user.handle}`}>
                {isCurrentUser ? tr('YOU') : user.name}
              </a>
              <small className="lbv2-table__user-name">{user.bio}</small>
              {user.roles && (
                <div className="user-roles">
                  {this.getRoleBadges(user.roles, user.rolesDefaultNames)}
                </div>
              )}
            </div>
          </div>

          <div className="lbv2-table__follow">
            {!isCurrentUser && !Permissions.has('DISABLE_USER_FOLLOW') && (
              <FollowButtonv2
                following={user.following}
                label={tr(user.following ? 'Following' : 'Follow')}
                hoverLabel={tr(user.following ? 'Unfollow' : '')}
                pendingLabel={tr(user.following ? 'Unfollowing...' : 'Following...')}
                pending={user.followPending}
                className="follow"
                onTouchTap={this.followClickHandler.bind(this, user)}
              />
            )}
            {!!user.followersCount && (
              <p className="lbv2-table__follow-text">
                <small>
                  <span style={{ color: colors.followColor }}>{user.followersCount}</span>{' '}
                  {tr('Followers')}
                </small>
              </p>
            )}
          </div>
        </div>
        <div className="lbv2__table-cell lbv2__table-cell_2 lbv2__table-cell_with-border">
          <span className="lbv2-table__score">
            {member.smartbitesScore === 0 ? 'NA' : formatNumbers(member.smartbitesScore)}
          </span>
        </div>
        <div className="lbv2__table-cell lbv2__table-cell_3">
          <span className="lbv2-table__expertise">
            {user.expertTopics &&
              user.expertTopics.map((topic, index) => {
                let result = topic.topic_label;
                if (index + 1 < user.expertTopics.length) result += ', ';
                return result;
              })}
          </span>
        </div>
      </div>
    );
  }
}

LeaderBoard.propTypes = {
  user: PropTypes.object,
  users: PropTypes.object,
  notification: PropTypes.object,
  index: PropTypes.number,
  currentUser: PropTypes.object,
  member: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  users: state.users.toJS()
}))(LeaderBoard);
