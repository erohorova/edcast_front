import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import translateTA from '../../utils/translateTimeAgo';
import getDefaultImage from '../../utils/getDefaultCardImage';
import find from 'lodash/find';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import unescape from 'lodash/unescape';
import { saveConsumptionPathwayHistoryURL } from '../../actions/pathwaysActions';

class Pathway extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false)
    };
    this.styles = {
      imageAlignment: {
        margin: '0.2rem',
        display: 'block'
      },
      titleAlignment: {
        display: 'inline-block',
        margin: '0.2rem'
      }
    };
  }

  showPathway = (e, slug) => {
    e.preventDefault();
    window.history.pushState(null, null, `/pathways/${slug}`);
    let pathwayBackUrl = window.location.pathname;
    if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
      this.props.dispatch(saveConsumptionPathwayHistoryURL(pathwayBackUrl));
    }
    this.props.dispatch(openStandaloneOverviewModal(this.props.pathway, 'pathways', {}));
  };

  render() {
    let languageAbbreviation =
      this.props.currentUser.profile && this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);
    let pathway = this.props.pathway;
    let imageUrl;
    let isFileAttached = pathway.filestack && !!pathway.filestack.length;
    let imageFileStack = !!(
      isFileAttached &&
      pathway.filestack[0].mimetype &&
      ~pathway.filestack[0].mimetype.indexOf('image/')
    );
    if (pathway.imageUrl) imageUrl = pathway.imageUrl;
    else {
      if (imageFileStack) {
        imageUrl = pathway.filestack[0].url;
      } else if (pathway.fileResources) {
        let findImage = find(pathway.fileResources, function(el) {
          return el.fileType == 'image';
        });
        if (findImage) imageUrl = findImage.fileUrl;
      }
    }
    return (
      <TableRow className="table-row" displayBorder={false}>
        <TableRowColumn className="table-row-column">
          <a
            style={this.styles.imageAlignment}
            aria-label={`${tr('Visit the featured pathway')},  ${unescape(
              pathway.title || pathway.message
            )}`}
            href="#"
            className="collections"
            onClick={e => this.showPathway(e, pathway.slug)}
          >
            <div className="pathway-image">
              <div
                className="container-padding image banner"
                style={{
                  height: '8rem',
                  backgroundImage: `url(\'${imageUrl || this.state.defaultImage}\')`,
                  backgroundRepeat: 'no-repeat',
                  backgroundPosition: 'center',
                  backgroundSize: 'cover',
                  margin: '1rem',
                  position: 'relative'
                }}
              />
            </div>
          </a>
        </TableRowColumn>
        <TableRowColumn
          className="container-padding"
          style={{ verticalAlign: 'top', position: 'relative' }}
        >
          <div>
            <div className="text-overflow vertical-spacing-small">
              <div>
                <a
                  style={this.styles.titleAlignment}
                  href="#"
                  className="matte collections"
                  onClick={e => this.showPathway(e, pathway.slug)}
                >
                  <div className="text-overflow">
                    <b>{unescape(pathway.title || pathway.message)}</b> - {pathway.packCardsCount}{' '}
                    SmartCards
                  </div>
                </a>
              </div>
              <div className="text-overflow">
                {pathway.createdAt && (
                  <a className="matte time-tag">
                    <TimeAgo date={pathway.createdAt} live={false} formatter={formatter} />
                  </a>
                )}
              </div>
            </div>
            <div
              className="like-comment-container horizontal-spacing-xlarge"
              style={{ position: 'absolute', bottom: '14px' }}
            >
              {/*<span className="horizontal-spacing-medium">
              <LikeIcon color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{pathway.votesCount || '0'}</small>
            </span>
            <span className="horizontal-spacing-medium">
              <ChatBubbleOutlineAsset color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{pathway.commentsCount || '0'}</small>
            </span>*/}
            </div>
          </div>
        </TableRowColumn>
      </TableRow>
    );
  }
}

Pathway.propTypes = {
  currentUser: PropTypes.object,
  pathway: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(Pathway);
