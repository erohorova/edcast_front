import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as filestack from '../../utils/filestack';
import getDefaultImage from '../../utils/getDefaultCardImage';
import checkImage from '../../utils/checkImages';
import addSecurity from '../../utils/filestackSecurity';

class SvgImageResized extends Component {
  constructor(props, context) {
    super(props, context);
    let { resizeOptions, cardId, ...newProps } = props;
    this.cardId = cardId;
    this.resizeOptions = resizeOptions;
    this.newProps = newProps;
    this.state = {
      xlinkHref: ''
    };
  }

  componentDidMount() {
    //always send resize params
    let originalImage = this.props.xlinkHref;
    if (this.resizeOptions && originalImage) {
      // first hit resize api, for images which are correct, resized images will be shown
      // if it gives error, then hit original image, if fetching original image fails too,
      // then show default image
      let xlinkHref = filestack.getResizedUrl(originalImage, this.resizeOptions);
      checkImage(xlinkHref)
        .then(resp => {
          if (resp == 'success') {
            this.setState({ xlinkHref });
          } else {
            return checkImage(originalImage).then(response => {
              if (response == 'success') {
                this.setState({ xlinkHref: originalImage });
              } else {
                this.setState({ xlinkHref: this.setDefaultImage() });
              }
            });
          }
        })
        .catch(err => {
          this.setState({ xlinkHref: this.setDefaultImage() });
          console.error(`Error in ImageResized.checkImage.func : ${err}`);
        });
    }
  }

  setDefaultImage = () => {
    let cardId = this.cardId;
    let cardImages = localStorage.getItem('cardImages');
    cardImages = cardImages ? JSON.parse(cardImages) : {};
    if (cardImages[cardId]) {
      return addSecurity(cardImages[cardId], 3600, this.props.currentUser.get('id'));
    } else {
      let img = getDefaultImage(this.props.currentUser.get('id')).url;
      cardImages[cardId] = img;
      localStorage.setItem('cardImages', JSON.stringify(cardImages));
      return img;
    }
  };

  checkImageHandle = link => {
    let xlinkHref = link;
    if (this.state.xlinkHref !== xlinkHref) {
      this.setState({ xlinkHref });
    }
  };

  //required on CardOverviewModal
  componentWillReceiveProps(nextProps) {
    if (this.props.xlinkHref !== nextProps.xlinkHref) {
      let xlinkHref = this.resizeOptions
        ? filestack.getResizedUrl(nextProps.xlinkHref, this.resizeOptions)
        : nextProps.xlinkHref;
      this.checkImageHandle(xlinkHref);
    }
  }

  render() {
    return (
      <image
        {...this.newProps}
        xlinkHref={addSecurity(this.state.xlinkHref, 3600, this.props.currentUser.get('id'))}
      />
    );
  }
}

SvgImageResized.propTypes = {
  resizeOptions: PropTypes.string,
  xlinkHref: PropTypes.string,
  cardId: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser
}))(SvgImageResized);
