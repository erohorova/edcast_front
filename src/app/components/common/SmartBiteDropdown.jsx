import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import colors from 'edc-web-sdk/components/colors/index';
import find from 'lodash/find';
import uniq from 'lodash/uniq';
import { connect } from 'react-redux';
import Chip from 'material-ui/Chip';
import CloseIcon from 'material-ui/svg-icons/content/clear';

class SmartBiteDropdown extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedChannels: props.currentChannels != undefined ? props.currentChannels : [],
      pathname: props.pathname.split('/'),
      currentUserChannelIds: this.props.currentUser.followingChannels
        ? this.props.currentUser.followingChannels.map(c => c.id)
        : []
    };

    this.styles = {
      mainStyle: {
        borderColor: '#d6d6e1',
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: 0,
        fontSize: '14px',
        lineHeight: '14px',
        height: '31px',
        width: '100%',
        maxWidth: '256px',
        verticalAlign: 'top',
        borderRadius: '2px'
      },
      hint: {
        padding: '0 10px',
        fontSize: '12px',
        color: '#acadc1',
        bottom: '5px'
      },
      iconStyle: {
        top: '2px',
        padding: 0,
        width: '24px',
        height: '24px'
      },
      selected: {
        lineHeight: '36px',
        top: 0,
        padding: '0 0.5rem',
        color: '#999aad'
      },
      chipsWrapper:
        this.props.cardType == 'pathway'
          ? {
              display: 'block',
              margin: '0 0 -0.25rem',
              paddingBottom: '10px'
            }
          : {
              display: 'flex',
              flexWrap: 'wrap',
              maxWidth: '100%',
              margin: '0 -0.25rem',
              paddingBottom: '10px'
            },
      chip: {
        margin: this.props.cardType == 'pathway' ? '.25rem' : 0,
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        padding: '0 37px 0 8px',
        width: this.props.cardType == 'pathway' ? 'auto' : '100%'
      },
      chipClose: {
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        borderRadius: '50%',
        color: colors.primary,
        verticalAlign: 'middle',
        marginLeft: '1rem',
        marginBottom: '2px',
        width: '22px',
        height: '22px',
        cursor: 'pointer',
        position: 'absolute',
        right: '8px',
        top: '5px'
      }
    };
  }

  componentDidMount() {
    if (
      this.props.currentUser.writableChannels &&
      this.state.pathname[1] &&
      this.state.pathname[1] == 'channel' &&
      this.state.pathname[2]
    ) {
      let channelIdentify = this.state.pathname[this.state.pathname.length - 1];
      let findChannel = find(this.props.currentUser.writableChannels, el => {
        return el.slug == channelIdentify || el.id == channelIdentify;
      });
      if (findChannel && this.props.cardType && this.props.cardType !== 'pathway') {
        this.handleChannelSelect(findChannel);
      }
    }
  }

  handleChannelDelete = removeId => {
    let newSelectedChannelIds = this.state.selectedChannels.filter(id => {
      return id !== removeId;
    });
    this.props.onChange();
    this.setState({
      selectedChannels: newSelectedChannelIds
    });
    if (this.props.onChange) {
      this.props.onChange(newSelectedChannelIds.concat(this.props.nonCuratedChannelIds));
    }
  };

  handleChannelSelect = channel => {
    let newSelectedChannelIds = uniq(this.state.selectedChannels.concat(channel.id));
    this.setState({ selectedChannels: newSelectedChannelIds });
    if (this.props.onChange) {
      this.props.onChange(
        newSelectedChannelIds.concat(this.props.nonCuratedChannelIds),
        channel,
        this.state.pathname
      );
    }
  };
  chipRender = (channelLabel, idx, id) => {
    return (
      <Chip
        style={this.styles.chip}
        className="channel-item-insight"
        backgroundColor={colors.white}
        labelColor={colors.primary}
      >
        {channelLabel}{' '}
        <CloseIcon
          onClick={this.handleChannelDelete.bind(this, id)}
          style={this.styles.chipClose}
        />
      </Chip>
    );
  };

  render() {
    let writableChannels = [];
    let allChannels =
      (this.props.cardChannel &&
        this.props.currentUser.writableChannels &&
        this.props.cardChannel.concat(this.props.currentUser.writableChannels)) ||
      [];
    if (this.props.currentUser.writableChannels) {
      writableChannels = this.props.currentUser.writableChannels
        .filter(channel => {
          return this.state.selectedChannels.indexOf(channel.id) === -1;
        })
        .sort((a, b) => a.label.localeCompare(b.label));
    }
    return (
      <div style={{ verticalAlign: 'bottom' }} className="text-block">
        {!!this.state.selectedChannels.length && (
          <div style={this.styles.chipsWrapper}>
            {this.state.selectedChannels.map((id, idx) => {
              let channelLabel = '';
              let channel_name = true;
              allChannels.some(channel => {
                if (channel.isPrivate === true) {
                  channel_name = this.state.currentUserChannelIds.indexOf(channel.id) !== -1;
                }
                if (channel.id === id) {
                  channelLabel = channel.label;
                  return true;
                }
                return false;
              });
              return (
                <div
                  key={id}
                  className={this.props.cardType == 'smartbite' ? 'chip-channel-pathway-item' : ''}
                  style={channel_name ? { display: 'block' } : { display: 'none' }}
                >
                  {this.chipRender(channelLabel, idx, id)}
                </div>
              );
            })}
          </div>
        )}
        <SelectField
          hintText={tr(this.props.hintText)}
          value={this.state.value}
          hintStyle={this.styles.hint}
          style={this.styles.mainStyle}
          fullWidth={this.props.fullWidth}
          underlineShow={false}
          iconStyle={this.styles.iconStyle}
          labelStyle={this.styles.selected}
          onChange={this.handleChange}
        >
          {!this.props.filterType &&
            writableChannels.map(channel => {
              return (
                <MenuItem
                  key={channel.id}
                  value={channel}
                  onTouchTap={this.handleChannelSelect.bind(this, channel)}
                  className="menu-item-custom postToChannel"
                  style={{ width: '256px' }}
                  primaryText={channel.label}
                />
              );
            })}
          {!this.props.filterType && !writableChannels.length && (
            <MenuItem
              key="empty"
              value=""
              disabled={true}
              style={{ width: '256px' }}
              className="menu-item-empty"
              primaryText={tr(
                'You are not currently following any Channels that allow you to add content.'
              )}
            />
          )}
          {this.props.filterType && (
            <MenuItem
              key="filter"
              value=""
              disabled={true}
              style={{ width: '256px' }}
              className="menu-item-filter"
              primaryText={tr('Here will be filter soon')}
            />
          )}
        </SelectField>
      </div>
    );
  }
}

SmartBiteDropdown.propTypes = {
  hintText: PropTypes.string,
  pathname: PropTypes.string,
  onChange: PropTypes.func,
  cardType: PropTypes.string,
  currentUser: PropTypes.object,
  filterType: PropTypes.bool,
  currentChannels: PropTypes.any,
  nonCuratedChannelIds: PropTypes.any,
  fullWidth: PropTypes.bool,
  cardChannel: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}
export default connect(mapStoreStateToProps)(SmartBiteDropdown);
