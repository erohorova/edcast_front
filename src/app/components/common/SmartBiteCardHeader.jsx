import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import { push } from 'react-router-redux';

import SmartBiteLevel from './SmartBiteLevel';
import BlurImage from './BlurImage';
import CreationDate from './CreationDate';
import TooltipLabel from '../common/TooltipLabel';
import getFormattedDateTime from '../../utils/getFormattedDateTime';
import addSecurity from '../../utils/filestackSecurity';

class SmartBiteCardHeader extends Component {
  constructor(props, context) {
    super(props, context);

    let avatarDiameter = this.props.avatarDiameter || '1.625rem';
    this.showPublishedDate = window.ldclient.variation('show-published-date', false);
    this.styles = {
      avatarBox: {
        height: avatarDiameter,
        width: avatarDiameter,
        marginRight: '0.5625rem',
        position: 'relative'
      }
    };
    this.state = {
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    this.showBIA = !!this.props.team.config.enabled_bia;
  }

  tooltipText = tooltipText => {
    return `<p className="tooltip-text">${tr(tooltipText)}</p>`;
  };
  goToHandle = e => {
    e.preventDefault();
    const currentUserHandle = this.props.currentUser.handle;
    this.props.closeModal && this.props.closeModal();
    !this.props.pathwayEditor
      ? this.props.dispatch(
          push(
            `/${
              this.props.card.author.handle.replace('@', '') === currentUserHandle
                ? 'me'
                : this.props.card.author.handle
            }`
          )
        )
      : null;
  };

  render() {
    let { card, params } = this.props;
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    let showCreationTime = window.ldclient.variation('card-click-handle', 'modal');
    let showProviderInCard = !(
      this.props &&
      this.props.team &&
      this.props.team.config &&
      this.props.team.config.hide_provider
    );
    let formattedDate = this.props.card.publishedAt
      ? getFormattedDateTime(this.props.card.publishedAt, 'DD MMM YYYY')
      : null;
    let pathname = this.props.pathname;

    let isFeed =
      pathname.indexOf('curate') > -1 ||
      pathname.indexOf('my-assignments') > -1 ||
      pathname.indexOf('featured') > -1 ||
      pathname.indexOf('team-learning') > -1;
    let isPathway = this.props.card.cardType === 'pack';
    return (
      <div className={this.props.isSlideOutCard ? 'slide-out__header' : ''}>
        <div className="my-card__header">
          {this.props.card.author && this.props.params.showCreator && (
            <div className="my-card__user-avatar" onClick={this.goToHandle}>
              <BlurImage
                style={this.styles.avatarBox}
                id={this.props.card.id}
                image={
                  this.props.card.author &&
                  (this.props.card.author.picture ||
                    (this.props.card.author.avatarimages &&
                      this.props.card.author.avatarimages.small) ||
                    this.props.params.defaultUserImage)
                }
              />
            </div>
          )}

          {this.props.card.author && this.props.params.showCreator && (
            <div
              className={`author-info-container ${
                this.state.showNew && this.props.card.isNew ? 'author-info-container_short' : ''
              }`}
            >
              <TooltipLabel
                text={this.tooltipText('Creator of the card')}
                html={true}
                position={'bottom'}
              >
                <a
                  href="#"
                  className={`user-name ${this.props.isNameCut ? 'user-name_cutting' : ''}`}
                  onClick={this.goToHandle}
                >
                  {this.props.card.author.fullName || this.props.authorName}
                </a>
              </TooltipLabel>
              <div className="user-name-popover">
                <div className="tooltip-snippet info-tooltip-custom">
                  <p className="info-tooltip-header text-center">
                    {this.props.card.author.fullName
                      ? this.props.card.author.fullName
                      : `${
                          this.props.card.author.firstName ? this.props.card.author.firstName : ''
                        } ${
                          this.props.card.author.lastName ? this.props.card.author.lastName : ''
                        }`}
                  </p>
                </div>
              </div>
              {showCreationTime == 'modal' && (
                <CreationDate
                  card={this.props.card}
                  standaloneLinkClickHandler={this.props.standaloneLinkClickHandler}
                />
              )}
            </div>
          )}
          {this.state.showNew && this.props.card.isNew && (isFeed || (!isFeed && isPathway)) && (
            <div className="card-new">{tr('NEW')}</div>
          )}

          {this.showBIA && (
            <SmartBiteLevel
              allowConsumerModifyLevel={this.props.allowConsumerModifyLevel}
              params={this.props.params}
              newSkillLevel={this.props.newSkillLevel}
              card={this.props.card}
              rateCard={this.props.rateCard}
            />
          )}
        </div>
        <div
          className="my-card__provider-container my-card__header-second-row"
          onClick={this.props.handleCardClicked}
        >
          {!this.props.params.hideProvider &&
            (this.props.card.provider || this.props.card.providerImage) && (
              <div className="my-card__provider">
                {this.props.card.providerImage && (
                  <span className="inline-block">
                    <img
                      className="my-card__provider-logo eclSourceLogoUrl"
                      src={addSecurity(
                        this.props.card.providerImage,
                        expireAfter,
                        this.props.currentUser.id
                      )}
                      title={tr('Source of the content')}
                    />
                  </span>
                )}
                {this.props.cardLayoutType === 'Tile' &&
                  this.props.card.provider &&
                  !this.props.card.providerImage && (
                    <span className="my-card__provider-name" title={this.props.card.provider}>
                      {this.props.card.provider}
                    </span>
                  )}
                {this.props.cardLayoutType === 'List' && this.props.card.provider && (
                  <span
                    className="my-card__provider-name"
                    style={{ marginLeft: '5px' }}
                    title={this.props.card.provider}
                  >
                    {this.props.card.provider}
                  </span>
                )}
              </div>
            )}
          {!this.props.isCsodCourse && (
            <div className="my-card__provider">
              {((!this.props.params.hideProvider &&
                this.props.card.eclSourceLogoUrl &&
                !this.props.card.providerImage) ||
                (this.props.card.eclSourceTypeName &&
                  this.props.logoObj[this.props.card.eclSourceTypeName] !== undefined)) && (
                <span className="inline-block">
                  <img
                    className="my-card__provider-logo eclSourceLogoUrl"
                    title={tr('Source of the content')}
                    src={addSecurity(
                      this.props.card.eclSourceLogoUrl ||
                        this.props.logoObj[this.props.card.eclSourceTypeName],
                      expireAfter,
                      this.props.currentUser.id
                    )}
                  />
                  <span className="my-card__small-vertical-line">|</span>
                </span>
              )}
              {this.props.params.iconFileSrc && (
                <span className="inline-block">
                  <img
                    className="my-card__provider-logo iconFileSrc"
                    src={
                      params.isFileAttached &&
                      card.filestack[0].mimetype &&
                      card.filestack[0].mimetype.indexOf('application/pdf') === 0
                        ? '/i/images/pdf-icon-min.png'
                        : addSecurity(
                            this.props.params.iconFileSrc,
                            expireAfter,
                            this.props.currentUser.id
                          )
                    }
                    title={tr('Source of the content')}
                  />
                  <span className="my-card__small-vertical-line">|</span>
                </span>
              )}
              <span
                className={
                  this.props.viewType === 'bigView' || this.props.viewType === 'feedView'
                    ? 'my-card__provider-name nonTileView-ellipsis'
                    : 'my-card__provider-name tileView-ellipsis'
                }
                title={tr(
                  this.props.card.readableCardType || this.props.params.cardType.toLowerCase()
                )}
              >
                {tr(this.props.card.readableCardType || this.props.params.cardType.toLowerCase())}
              </span>
              {this.props.card.deletedAt && (
                <div className="card-new__deleted">
                  <span className="my-card__small-vertical-line">|</span>
                  {tr('DELETED')}
                </div>
              )}
            </div>
          )}
          {this.props.card.badging && (
            <div className="my-card__provider">
              {this.props.card.badging.imageUrl && (
                <span className="inline-block">
                  <img
                    title={tr('Badge')}
                    className="my-card__provider-logo"
                    src={addSecurity(
                      this.props.card.badging.imageUrl,
                      expireAfter,
                      this.props.currentUser.id
                    )}
                    style={{ marginRight: '5px' }}
                  />
                </span>
              )}
            </div>
          )}
          {this.showPublishedDate && formattedDate && (
            <span className={'published-date-tile published-at'}>
              <TooltipLabel text={'Published Date'} html={true} position={'bottom'}>
                {formattedDate}
              </TooltipLabel>
            </span>
          )}
        </div>
      </div>
    );
  }
}

SmartBiteCardHeader.propTypes = {
  showControls: PropTypes.any,
  pathwayEditor: PropTypes.any,
  isNameCut: PropTypes.any,
  allowConsumerModifyLevel: PropTypes.any,
  isCsodCourse: PropTypes.any,
  authorName: PropTypes.string,
  newSkillLevel: PropTypes.string,
  params: PropTypes.object,
  logoObj: PropTypes.object,
  card: PropTypes.object,
  rateCard: PropTypes.any,
  standaloneLinkClickHandler: PropTypes.func,
  cardClickHandle: PropTypes.string,
  avatarDiameter: PropTypes.string,
  handleCardClicked: PropTypes.func,
  team: PropTypes.any,
  cardLayoutType: PropTypes.any,
  viewType: PropTypes.string,
  isSlideOutCard: PropTypes.bool,
  closeModal: PropTypes.func,
  pathname: PropTypes.string
};

function mapStateToProps(state) {
  return {
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStateToProps)(SmartBiteCardHeader);
