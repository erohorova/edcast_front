import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import forEach from 'lodash/forEach';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import * as languages from '../../constants/languages';

const langs = { ...languages.langs, ...{ Unspecified: 'unspecified' } };

class MultilingualContent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectableLanguage: props.language
    };
    this.styles = {
      priceDropDown: {
        textAlign: 'left',
        height: '1.875rem',
        verticalAlign: 'top',
        border: '1px solid #d6d6e2',
        fontSize: '0.75rem',
        paddingLeft: '0.3125rem',
        width: '100%',
        marginTop: '0.5rem',
        lineHeight: '0.3125rem'
      },
      iconSelect: {
        height: '1.875rem',
        width: '1.875rem',
        padding: 0
      },
      menuItemStyle: {
        fontSize: '0.75rem'
      },
      labelStyle: {
        fontSize: '0.75rem',
        lineHeight: '2.375rem',
        paddingRight: '1.125rem'
      }
    };
  }

  languageChange = data => {
    this.setState({ selectableLanguage: data });
    this.props.handleLanguageChange(data);
  };

  renderLangs = () => {
    let langsItems = [];
    forEach(langs, (val, key) => {
      let item = (
        <MenuItem
          key={val}
          value={val}
          innerDivStyle={{ padding: '0 0.5rem' }}
          primaryText={key}
          onTouchTap={this.languageChange.bind(this, val)}
        />
      );
      langsItems.push(item);
    });
    return langsItems.sort((a, b) => {
      if (a.key > b.key) return 1;
      if (a.key < b.key) return -1;
    });
  };

  render() {
    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    return (
      <div className="row">
        {!useFormLabels && (
          <div
            className={this.props.labelContainerClasses || 'small-12 medium-2 large-3 align-label'}
          >
            <div className={this.props.labelClasses || 'smartbite-label'}>{tr('Language')}</div>
          </div>
        )}
        <div className={this.props.inputContainerClasses || 'small-12 medium-8 large-8'}>
          {useFormLabels && <label htmlFor="form-language">{tr('Language')}</label>}
          <SelectField
            id="form-language"
            style={
              this.props.commonStyle
                ? {
                    ...this.props.commonStyle,
                    marginTop: useFormLabels ? 0 : this.props.commonStyle.marginTop
                  }
                : {
                    ...this.styles.priceDropDown,
                    marginTop: useFormLabels ? 0 : this.styles.priceDropDown.marginTop
                  }
            }
            labelStyle={this.props.labelStyle || this.styles.labelStyle}
            iconStyle={this.props.iconStyle || this.styles.iconSelect}
            menuItemStyle={this.props.menuItemStyle || this.styles.menuItemStyle}
            underlineStyle={{ display: 'none' }}
            value={this.state.selectableLanguage}
          >
            {this.renderLangs()}
          </SelectField>
        </div>
      </div>
    );
  }
}

MultilingualContent.propTypes = {
  commonStyle: PropTypes.object,
  iconSelect: PropTypes.object,
  labelStyle: PropTypes.object,
  menuItemStyle: PropTypes.object,
  handleLanguageChange: PropTypes.func,
  language: PropTypes.string,
  labelContainerClasses: PropTypes.string,
  labelClasses: PropTypes.string,
  inputContainerClasses: PropTypes.string,
  iconStyle: PropTypes.any
};

export default connect()(MultilingualContent);
