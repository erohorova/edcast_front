import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { follow, unfollow } from 'edc-web-sdk/requests/users.v2';
import TooltipLabel from '../common/TooltipLabel';
import UserBadgesContainer from '../../components/common/UserBadgesContainer';
import { tr } from 'edc-web-sdk/helpers/translations';
import { toggleTeamAdmin } from 'edc-web-sdk/requests/teams';
import IconButton from 'material-ui/IconButton/IconButton';
import MoreIcon from 'material-ui/svg-icons/navigation/more-horiz';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import colors from 'edc-web-sdk/components/colors/index';
import { Permissions } from '../../utils/checkPermissions';
import * as actionTypes from '../../constants/actionTypes';
import {
  deleteUserFromTeam,
  updatePendingMember,
  deletePendingUser
} from '../../actions/groupsActionsV2';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { push } from 'react-router-redux';

class User extends Component {
  constructor(props, context) {
    super(props, context);
    this.followClickHandler = this.followClickHandler.bind(this);
    this.getLink = this.getLink.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.state = {
      pending: false,
      following: this.props.following,
      role: this.props.roles && this.props.roles.length && this.props.roles[0]
    };
    this.styles = {
      badge: {
        position: 'absolute',
        left: 0,
        top: 0,
        padding: '10px'
      },
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '26px'
      },
      userDropdown: {
        position: 'absolute',
        top: '18px',
        left: '130px'
      }
    };
  }

  followClickHandler() {
    let action = this.state.following ? unfollow : follow;
    this.setState({ pending: true });
    action(this.props.id)
      .then(() => {
        this.props.dispatch(updatePendingMember(this.props.id, !this.state.following));
        this.setState(prevState => {
          return { pending: false, following: !prevState.following };
        });
      })
      .catch(err => {
        console.error(`Error in User.followClickHandler.func : ${err}`);
      });
  }

  getLink() {
    if (this.props.disableLink) {
      return 'javascript:;';
    } else {
      const currentUserHandle = this.props.currentUserHandle;
      this.props.dispatch(
        push(
          `/${this.props.handle.replace('@', '') === currentUserHandle ? 'me' : this.props.handle}`
        )
      );
    }
  }

  getStyle() {
    if (this.props.disableLink) {
      return { cursor: 'default' };
    } else {
      return {};
    }
  }

  formatSkills(skills) {
    let skillsString = '';
    for (var i = 0; i < skills.length; i++) {
      skillsString = skillsString + skills[i].topic_label;
      if (i !== skills.length - 1) {
        skillsString = skillsString + ', ';
      }
    }
    return skillsString;
  }

  tooltipHtml(name, formattedSkills) {
    return `<p class="text-center tooltip-title">${name}"s Skills</p><p class="line-clamp-4">${formattedSkills}</p>`;
  }

  handleToggleGroupLeader = (teamID, UserID) => {
    let _this = this;
    toggleTeamAdmin(teamID, UserID)
      .then(data => {
        this.setState({ role: data.user.role === 'admin' ? 'groupLeader' : 'groupMember' });
        _this.props.dispatch({
          type: actionTypes.UPDATE_MEMBER,
          member: data.user
        });
        _this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: data.user.role === 'admin' ? 'Promoted to Group Leader' : 'Demoted to Member',
          autoClose: true
        });
      })
      /*eslint handle-callback-err: "off"*/
      .catch(err => {
        _this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
      });
  };

  removeUser = e => {
    e.preventDefault();
    if (this.props.displayRemoveButton) {
      if (this.props.leadersCounting() && this.props.roles[0] === 'groupLeader') {
        this.props.dispatch(
          openSnackBar(
            'Error. You can’t remove last Group Leader. Please assign a new Group Leader before removing yourself',
            true
          )
        );
      } else {
        this.props.dispatch(deleteUserFromTeam(this.props));
      }
    } else {
      this.props.dispatch(deletePendingUser(this.props.id, this.props.teamID));
    }
  };

  render() {
    let skillsCount = this.props.expertSkills ? this.props.expertSkills.length : 0;
    let formattedSkills = skillsCount > 0 ? this.formatSkills(this.props.expertSkills) : '';
    let tooltipHtml = skillsCount > 0 ? this.tooltipHtml(this.props.name, formattedSkills) : '';
    return (
      <div className="user">
        <Paper style={{ width: '178px', height: '215px' }}>
          <div className="user-wrapper">
            <div style={{ paddingTop: '12px' }}>
              {this.props.roles && (
                <div style={this.styles.badge}>
                  <UserBadgesContainer
                    rolesDefaultNames={this.props.rolesDefaultNames}
                    roles={this.props.roles}
                  />
                </div>
              )}
              {this.props.isTeamAdmin &&
                (this.props.displayRemoveButton || this.props.displayRemoveButtonPending) && (
                  <a
                    aria-label="remove"
                    role="button"
                    className="remove-button"
                    onClick={this.removeUser}
                  >
                    {tr('Remove')}
                  </a>
                )}
              {this.props.teamID &&
                !this.props.isCurrentUser &&
                this.props.isTeamAdmin &&
                !this.props.pending && (
                  <IconMenu
                    style={this.styles.userDropdown}
                    className="user-more-menu"
                    iconButtonElement={
                      <IconButton aria-label="more" style={this.props.moreIconStyle || {}}>
                        <MoreIcon color={colors.gray} />
                      </IconButton>
                    }
                    anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
                    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                    listStyle={this.styles.iconMenu}
                  >
                    <MenuItem
                      style={this.styles.menuItem}
                      primaryText={tr(
                        this.state.role === 'groupLeader'
                          ? 'Demote to Member'
                          : 'Promote to Group Leader'
                      )}
                      onTouchTap={this.handleToggleGroupLeader.bind(
                        this,
                        this.props.teamID,
                        this.props.id
                      )}
                    />
                  </IconMenu>
                )}
              <a className="matte" onClick={this.getLink.bind(this)} style={this.getStyle()}>
                <div
                  className="user-avatar"
                  style={{ backgroundImage: 'url(' + this.props.imageUrl + ')', marginTop: '5px' }}
                />
                <div className="user-info text-overflow">
                  <div className="user-name">{this.props.name}</div>
                  {skillsCount === 0 && <small className="user-sub-name">&nbsp;</small>}
                  {skillsCount > 0 && (
                    <TooltipLabel text={tooltipHtml} html={true} position={this.props.position}>
                      <small className="user-sub-name">{formattedSkills}</small>
                    </TooltipLabel>
                  )}
                </div>
              </a>
              {!this.props.isCurrentUser && !Permissions.has('DISABLE_USER_FOLLOW') && (
                <FollowButton
                  onTouchTap={this.followClickHandler}
                  className="follow"
                  label={tr(this.state.following ? 'Following' : 'Follow')}
                  hoverLabel={tr(this.state.following ? 'Unfollow' : '')}
                  pendingLabel={tr(this.state.following ? 'Unfollowing...' : 'Following...')}
                  pending={this.state.pending}
                  following={this.state.following}
                  disabled={!this.props.handle}
                  theme="purple"
                />
              )}
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

User.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  handle: PropTypes.string,
  imageUrl: PropTypes.string,
  teamID: PropTypes.string,
  disableLink: PropTypes.bool,
  isCurrentUser: PropTypes.bool,
  isTeamAdmin: PropTypes.bool,
  pending: PropTypes.bool,
  following: PropTypes.bool,
  leadersCounting: PropTypes.func,
  roles: PropTypes.array,
  rolesDefaultNames: PropTypes.array,
  expertSkills: PropTypes.array,
  position: PropTypes.string,
  moreIconStyle: PropTypes.object,
  displayRemoveButton: PropTypes.bool,
  displayRemoveButtonPending: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    currentUserHandle: state.currentUser.get('handle')
  };
}

export default connect(mapStoreStateToProps)(User);
