import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { closeStandaloneOverviewModal } from '../../actions/modalStandAloneActions';

class Breadcrumb extends Component {
  constructor(props, context) {
    super(props, context);
  }

  goBack() {
    let card = this.props.card;
    if (this.props.isStandaloneModal) {
      window.history.back();
      this.props.dispatch(closeStandaloneOverviewModal(this.props.dataCard));
    } else if (card && card.attemptedOption && card.leaps && card.leaps.standalone) {
      let correctAnswer = card.quizQuestionOptions.find(obj => {
        return obj.isCorrect === true;
      });
      let selected = card.attemptedOption && card.attemptedOption.id;
      let result = correctAnswer && selected === correctAnswer.id;
      let nextCardId = result ? card.leaps.standalone.correctId : card.leaps.standalone.wrongId;
      this.props.dispatch(push(`/insights/${nextCardId}`));
    } else if (this.props.backPress) {
      this.props.dispatch(push(this.props.levels[0].url));
    } else {
      if (window.history.length > 2) {
        window.history.back();
      } else {
        this.props.dispatch(push(this.props.levels[0].url));
      }
    }
  }
  render() {
    return (
      <div className="breadcrumb-container">
        <div className="breadcrumb-padding row">
          <div className="small-12">
            <div>
              <div className="breadcrumb first">
                <button className="level breadcrumbBack" onClick={this.goBack.bind(this)}>
                  <a tabIndex={-1} className="back-link hideOutline">
                    &#60;&#60; {tr('Back')}
                  </a>
                </button>
              </div>
            </div>
            {/* <div>{this.props.levels.map((path, index) => {
              if(index === this.props.levels.length - 1){
                return <div className="last breadcrumb" key={index}><div className="level">&nbsp;{path.name}</div></div>
              }
              return <div className={ (index === 0) ? "breadcrumb first" : "breadcrumb"} key={index}><div className="level" onTouchTap={() => {this.props.dispatch(push(path.url))}}>&nbsp;{path.name}</div>{' >'}</div>;
            })}</div>*/}
          </div>
        </div>
      </div>
    );
  }
}

Breadcrumb.propTypes = {
  isStandaloneModal: PropTypes.bool,
  dataCard: PropTypes.object,
  card: PropTypes.object,
  levels: PropTypes.array,
  backPress: PropTypes.bool
};

export default connect()(Breadcrumb);
