import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import { closeStandaloneOverviewModal } from '../../actions/modalStandAloneActions';

class Breadcrumb extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  goBack() {
    if (this.props.isStandaloneModal) {
      window.history.back();
      this.props.cardUpdated && this.props.cardUpdated();
      this.props.dispatch(closeStandaloneOverviewModal(this.props.dataCard));
    } else {
      if (window.history.length > 2) {
        if (this.props.isChannelSearching) {
          this.props.backToCarouselView();
        } else if (this.props.isPathwayConsumption) {
          this.props.dispatch(push(`/pathways/${this.props.isPathwayConsumption.slug}`));
        } else if (this.props.isConsumptionPage) {
          this.props.dispatch(push(this.props.isConsumptionPage));
        } else if (this.props.isJourneyConsumption) {
          this.props.dispatch(push(`/journey/${this.props.isJourneyConsumption.slug}`));
        } else {
          window.history.back();
        }
      } else {
        this.props.dispatch(
          push(this.props.levels && !!this.props.levels.length ? this.props.levels[0].url : '/')
        );
      }
    }
  }
  render() {
    return (
      <button className="breadcrumbBack" onClick={this.goBack.bind(this)}>
        <a aria-label="back" tabIndex={-1} className="back-link hideOutline">
          <BackIcon
            style={{ width: '19px' }}
            color={
              this.props.isPathwayConsumption || this.props.isJourneyConsumption
                ? '#000000'
                : '#454560'
            }
          />
        </a>
      </button>
    );
  }
}

Breadcrumb.propTypes = {
  isStandaloneModal: PropTypes.bool,
  dataCard: PropTypes.object,
  card: PropTypes.object,
  levels: PropTypes.array,
  cardUpdated: PropTypes.func,
  backToCarouselView: PropTypes.func,
  isChannelSearching: PropTypes.bool,
  isPathwayConsumption: PropTypes.object,
  isConsumptionPage: PropTypes.string,
  isJourneyConsumption: PropTypes.object
};

export default connect()(Breadcrumb);
