import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';

// import CircularProgress from "material-ui/CircularProgress";
import Spinner from '../common/spinner';

import { tr } from 'edc-web-sdk/helpers/translations';
import some from 'lodash/some';
import findIndex from 'lodash/findIndex';
import uniqBy from 'lodash/uniqBy';

class MultiSelect extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      inputText: this.props.inputText,
      items: this.props.items,
      itemName: this.props.itemName,
      selectedItems: [],
      isSelectOpen: false,
      isAll: true
    };
  }

  toogleSelect = () => {
    this.setState(prevState => ({
      isSelectOpen: !prevState.isSelectOpen
    }));
  };

  handleChangeItem = item => {
    if (!item) {
      if (this.state.isAll) {
        return;
      } else {
        this.setState({
          selectedItems: this.props.items,
          isAll: true
        });
        this.props.onChange([]);
      }

      return;
    }
    let selectedItems = this.state.selectedItems;
    if (this.state.isAll) {
      selectedItems = [];
    }
    let index = findIndex(selectedItems, item);
    if (some(selectedItems, item)) {
      selectedItems.splice(index, 1);
    } else {
      selectedItems = uniqBy(selectedItems.concat(item), 'id');
    }
    this.setState({
      selectedItems,
      isAll: false
    });
    this.props.onChange(selectedItems);
  };

  handleScroll = node => {
    if (this.state.pending) {
      return;
    }
    let el = ReactDOM.findDOMNode(node);
    if (el.scrollTop === el.scrollHeight - el.clientHeight) {
      if (!this.props.isLastPage) {
        this.showMoreItems();
      }
    }
  };

  mountScroll = node => {
    if (node) {
      node.addEventListener('scroll', () => this.handleScroll(node));
    }
  };

  showMoreItems = () => {
    this.setState({ pending: true });
    this.props.loadMoreData(() => {
      this.setState({ pending: false });
    });
  };

  componentDidMount() {
    this.props.preSelectedItem &&
      this.setState({
        selectedItems: uniqBy(this.state.selectedItems.concat(this.props.preSelectedItem), 'id'),
        isAll: false
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hasOwnProperty('preSelectedItem') && nextProps.preSelectedItem === false) {
      this.setState({ selectedItems: [] });
    }
    if (
      nextProps.preSelectedItem &&
      nextProps.preSelectedItem !== true &&
      ((nextProps.preSelectedItem.id && !this.props.preSelectedItem) ||
        (this.props.preSelectedItem &&
          this.props.preSelectedItem !== true &&
          nextProps.preSelectedItem.id != this.props.preSelectedItem.id))
    ) {
      this.setState({
        selectedItems: uniqBy(this.state.selectedItems.concat(nextProps.preSelectedItem), 'id'),
        isAll: false
      });
    }
  }

  render() {
    return (
      <div className="lbv2__select-input-container">
        {this.state.isSelectOpen && (
          <div className="multiselect-overflow" onClick={this.toogleSelect} />
        )}
        <div className="lbv2__select-input" onClick={this.toogleSelect}>
          {this.state.inputText && tr(this.state.inputText) + ':'}{' '}
          {this.state.selectedItems.length === this.props.items.length
            ? tr('All')
            : this.state.selectedItems.map((selectedGroup, index) => {
                let result = selectedGroup.name;
                if (index + 1 < this.state.selectedItems.length) result += ', ';
                return result;
              })}
        </div>
        {this.state.isSelectOpen && (
          <div
            id={`${this.state.itemName}-select-body`}
            ref={this.mountScroll}
            className="lbv2__select-dropdown multiselect"
          >
            <div className="multiselect-checkbox" key={`${this.state.itemName}_block_00`}>
              <input
                type="checkbox"
                id={`${this.state.itemName}_00`}
                checked={this.state.isAll}
                onChange={this.handleChangeItem.bind(this, null)}
              />
              <label htmlFor={`${this.state.itemName}_00`} className="multiselect-checkbox__text">
                {tr('All')}
              </label>
            </div>
            {this.props.items.map((item, index) => {
              return (
                <div className="multiselect-checkbox" key={`${this.state.itemName}_${index}`}>
                  <input
                    type="checkbox"
                    id={`${this.state.itemName}_${index}`}
                    checked={some(this.state.selectedItems, item) && !this.state.isAll}
                    onChange={this.handleChangeItem.bind(this, item)}
                  />
                  <label
                    htmlFor={`${this.state.itemName}_${index}`}
                    className="multiselect-checkbox__text"
                  >
                    {item.name}
                  </label>
                </div>
              );
            })}
            {this.state.pending && (
              <div className="text-center pending-cards">
                <Spinner />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

MultiSelect.propTypes = {
  inputText: PropTypes.string,
  items: PropTypes.array,
  itemName: PropTypes.string,
  loadMoreData: PropTypes.any,
  onChange: PropTypes.any,
  preSelectedItem: PropTypes.any,
  isLastPage: PropTypes.bool
};

export default connect()(MultiSelect);
