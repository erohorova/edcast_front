import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import LockIcon from 'edc-web-sdk/components/icons/Lock';
import FollowButtonv2 from 'edc-web-sdk/components/FollowButtonv2';

import { TableRow, TableRowColumn } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton/IconButton';

import { toggleFollow } from '../../actions/channelsActions';
import { initUserData } from '../../actions/currentUserActions';

import TooltipLabel from '../common/TooltipLabel';
import * as upshotActions from '../../actions/upshotActions';

class Channelv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.toggleFollowClickHandler = this.toggleFollowClickHandler.bind(this);
    this.state = {
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      currentFollowersCount: props.channel.followersCount
    };

    this.styles = {
      followBtn: {
        height: '1.25rem',
        width: '3.75rem',
        minWidth: '3.75rem',
        lineHeight: 1,
        padding: '0',
        margin: '0',
        fontSize: '0.625rem'
      },
      followLabelStyle: {
        fontSize: '0.625rem',
        textTransform: 'capitalize'
      },
      channelBanner: {
        backgroundImage: `url(\'${props.channel.profileImageUrl ||
          props.channel.bannerImageUrl}\')`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: 'cover'
      }
    };
  }

  toggleFollowClickHandler(isFollow) {
    let followerCountUpdated = isFollow
      ? this.state.currentFollowersCount - 1
      : this.state.currentFollowersCount + 1;
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(
        window.UPSHOTEVENT[!this.state.isFollowing ? 'FOLLOW' : 'UNFOLLOW'],
        {
          eventAction: !this.state.following ? 'Follow' : 'Unfollow',
          event: 'Channel'
        }
      );
    }
    this.props
      .dispatch(toggleFollow(this.props.channel.id, !isFollow))
      .then(() => {
        this.setState({ currentFollowersCount: followerCountUpdated });
        this.props.dispatch(initUserData());
      })
      .catch(err => {
        console.error(`Error in ChannelListItem.toggleFollow.func : ${err}`);
      });
  }

  goToChannel = e => {
    e.preventDefault();
    this.props.channel.allowFollow &&
      this.props.dispatch(push(`/channel/${this.props.channel.slug}`));
  };

  render() {
    let showLockIcon = !!(this.props.team.config && this.props.team.config.show_lock_icon);
    return (
      <div className="channel-v2">
        <div className="channel-v2__body">
          <a
            aria-label={`${tr('Visit channel')}, ${this.props.channel.label}`}
            href="#"
            className="channel-bannerv2"
            onClick={this.goToChannel}
            style={this.styles.channelBanner}
          >
            <div className="channel-followers">
              {tr('FOLLOWERS')} {this.state.currentFollowersCount}
            </div>
            {this.props.channel.isPrivate && showLockIcon && (
              <div className="icon-inline-block my-icon-button-block">
                <button className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center">
                  <LockIcon color="#ffffff" />
                  <span className="tooltiptext">{tr('Private Channel')}</span>
                </button>
              </div>
            )}
          </a>
          <div className="channel-info">
            <TooltipLabel
              text={this.props.channel.label.length > 21 ? this.props.channel.label : ''}
              html={true}
              position={'top-left'}
            >
              <div className="channelv2-title text-overflow">
                <a href="#" className="matte" onClick={this.goToChannel}>
                  {this.props.channel.label}
                </a>
              </div>
            </TooltipLabel>

            {this.props.channel.allowFollow ? (
              <FollowButtonv2
                following={this.props.channel.isFollowing}
                className="follow"
                style={this.styles.followBtn}
                labelStyle={this.styles.followLabelStyle}
                label={tr(this.props.channel.isFollowing ? 'Following' : 'Follow')}
                hoverLabel={tr(this.props.channel.isFollowing ? 'Unfollow' : '')}
                pendingLabel={tr(
                  this.props.channel.isFollowing ? 'Unfollowing...' : 'Following...'
                )}
                onTouchTap={this.toggleFollowClickHandler.bind(
                  this,
                  this.props.channel.isFollowing
                )}
                pending={this.props.channel.followPending}
              />
            ) : (
              <SecondaryButton
                className="channel-coming-soon"
                label={tr('Coming Soon')}
                disabled={true}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

Channelv2.propTypes = {
  channel: PropTypes.object,
  openForEdit: PropTypes.func,
  team: PropTypes.object
};

export default connect(state => ({
  team: state.team.toJS()
}))(Channelv2);
