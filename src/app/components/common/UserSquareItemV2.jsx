import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import FollowButtonv3 from 'edc-web-sdk/components/FollowButtonv3';
import colors from 'edc-web-sdk/components/colors/index';

import { toggleFollow } from '../../actions/usersActions';
import { updateFollowingUsersCount } from '../../actions/currentUserActions';

import { Permissions } from '../../utils/checkPermissions';

import BlurImage from '../common/BlurImage';
import UserBadgesContainer from '../common/UserBadgesContainer';

class UserSquareItemV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentFollowersCount: props.user['followersCount'],
      currentFollowingCount: props.user['followingCount'],
      roles: props.user['roles'] ? props.user['roles'] : [],
      isFollowing: null,
      user: props.realUser,
      showCompany: window.ldclient.variation('show-company', false)
    };
    this.styles = {
      badgeIcons: {
        padding: '0',
        height: '1rem',
        width: '1rem',
        marginRight: '0.5rem',
        marginLeft: '0',
        marginBottom: '0.625rem',
        verticalAlign: 'top'
      },
      iconStyle: {
        height: '1rem',
        width: '1rem',
        borderRadius: '50%',
        color: colors.white
      },
      avatarBox: {
        userSelect: 'none',
        height: '3.9375rem',
        width: '3.9375rem',
        position: 'relative',
        border: '2px solid #ffffff'
      },
      followBtn: {
        flex: 'none'
      }
    };
    this.rolesDefaultNames = this.props.user['rolesDefaultNames']
      ? this.props.user['rolesDefaultNames']
      : [];
  }

  followClickHandler = user => {
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user['isFollowing'] || this.props.user['isFollowed'];
    let followerCountUpdated =
      isFollowing && this.state.currentFollowersCount
        ? this.state.currentFollowersCount - 1
        : this.state.currentFollowersCount + 1;
    this.props
      .dispatch(toggleFollow(user['id'], !isFollowing))
      .then(() => {
        this.setState(
          { currentFollowersCount: followerCountUpdated, isFollowing: !isFollowing },
          () => {
            this.props.dispatch(updateFollowingUsersCount(!isFollowing));
          }
        );
      })
      .catch(err => {
        console.error(`Error in UserSquareItem.toggleFollow.func : ${err}`);
      });
  };

  userClickHandler = () => {
    const { currentUserHandle } = this.props;
    this.props.dispatch(
      push(
        `/${
          this.props.user.handle.replace('@', '') === currentUserHandle
            ? 'me'
            : this.props.user.handle
        }`
      )
    );
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.user && this.props.user) {
      let isFollowersCountChanged =
        nextProps.user['followersCount'] !== this.props.user['followersCount'];
      let isFollowingCountChanged =
        nextProps.user['followingCount'] !== this.props.user['followingCount'];
      if (isFollowersCountChanged || isFollowingCountChanged) {
        this.setState({
          currentFollowersCount: nextProps.user['followersCount'],
          currentFollowingCount: nextProps.user['followingCount']
        });
      }
    }
  }

  render() {
    let user = this.props.user;
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user['isFollowing'] || this.props.user['isFollowed'];
    let isCompleted = user['isComplete'];
    let followersCount = this.props.isCurrentUser
      ? this.props.currentUser.get('followersCount')
      : this.state.currentFollowersCount !== undefined
      ? this.state.currentFollowersCount
      : '';
    let followingCount = this.props.isCurrentUser
      ? this.props.currentUser.get('followingCount')
      : this.state.currentFollowingCount !== undefined
      ? this.state.currentFollowingCount
      : '';

    const defaultUserImage = '/i/images/default_user_blue.png';
    const defaultBanner = '/i/images/default_banner_image.png';
    let imageUrl =
      user['avatar'] ||
      user['picture'] ||
      (user['avatarimages'] && user['avatarimages']['small']) ||
      defaultUserImage;
    let userBanner = user.coverimages ? user.coverimages.banner_url : defaultBanner;
    let expertTopics = (user.profile && user.profile.expertTopics) || user.expertSkills || [];

    return (
      <div
        className={
          !!this.props.source && this.props.source === 'discoveryCarousel'
            ? 'user-square-item user-square-item-v2 discover-user-square-item-v2'
            : 'user-square-item user-square-item-v2'
        }
      >
        <div
          className="user-square-item-v2__banner"
          style={{
            backgroundImage: `linear-gradient(to bottom, rgba(238, 238, 238, 0), #6f708b),url("${userBanner}")`
          }}
        >
          <div className="avatar-row">
            <div className="avatar-container">
              <BlurImage style={this.styles.avatarBox} image={imageUrl} />
            </div>
            <div className="badges-container">
              <UserBadgesContainer
                roles={this.state.roles}
                rolesDefaultNames={this.rolesDefaultNames}
                badgeStylesComponent={this.styles.badgeIcons}
                iconStyleComponent={this.styles.iconStyle}
              />
            </div>
          </div>
        </div>
        <div className="user-square-item__container">
          <div className="user-information-row">
            <div className="info-container">
              <div className="user-name-row">
                <a className="user-name" onTouchTap={this.userClickHandler}>
                  {user['name']}
                </a>
                {!this.props.isCurrentUser &&
                  !this.props.isPendingUser &&
                  !Permissions.has('DISABLE_USER_FOLLOW') && (
                    <FollowButtonv3
                      style={this.styles.followBtn}
                      following={isFollowing && isCompleted}
                      label={tr(isFollowing && isCompleted ? 'Following' : 'Follow')}
                      hoverLabel={tr(isFollowing ? 'Unfollow' : '')}
                      pendingLabel={tr(isFollowing ? 'Unfollowing...' : 'Following...')}
                      pending={user['followPending']}
                      className="follow"
                      onTouchTap={this.followClickHandler.bind(this, user)}
                      disabled={!isCompleted}
                    />
                  )}
              </div>
              <div className="user-info__title">
                {(user.profile && user.profile.jobTitle) || user.jobTitle}
                <span>
                  {this.state.showCompany &&
                    user.profile &&
                    user.profile.jobTitle &&
                    user.company &&
                    ' at '}
                </span>
                <span className="font-italic">{this.state.showCompany && user.company}</span>
              </div>
            </div>
          </div>
          {expertTopics.length ? (
            <div className="user-square-item-v2__topics">
              <span className="user-square-item-v2__expert-topic">
                {tr(expertTopics[0].topic_label)}
              </span>
              {expertTopics[1] && (
                <span className="user-square-item-v2__expert-topic">
                  <span className="dot" /> {tr(expertTopics[1].topic_label)}
                </span>
              )}
              <br />
              {expertTopics[2] && (
                <span className="user-square-item-v2__expert-topic">
                  {tr(expertTopics[2].topic_label)}
                </span>
              )}
              {expertTopics[3] && (
                <span className="user-square-item-v2__expert-topic">
                  <span className="dot" /> {tr(expertTopics[3].topic_label)}
                </span>
              )}
            </div>
          ) : null}

          {!this.props.isPendingUser && (
            <div className="user-follow">
              <div className="user-followers">
                <span>{followersCount}</span> {tr('Followers')}
              </div>
              <div className="user-following">
                <span>{followingCount}</span> {tr('Following')}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

UserSquareItemV2.propTypes = {
  isPendingUser: PropTypes.bool,
  isCurrentUser: PropTypes.bool,
  currentUser: PropTypes.object,
  user: PropTypes.object,
  realUser: PropTypes.object,
  index: PropTypes.number,
  source: PropTypes.string,
  currentUserHandle: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUserHandle: state.currentUser.get('handle')
  };
}

export default connect(mapStoreStateToProps)(UserSquareItemV2);
