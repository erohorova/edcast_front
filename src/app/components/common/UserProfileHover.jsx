import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ListItem from 'material-ui/List/ListItem';
import BlurImage from '../common/BlurImage';
import Popover from 'material-ui/Popover';

class UserProfileHover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      anchorOrigin: {
        horizontal: 'right',
        vertical: 'top'
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'bottom'
      }
    };

    this.styles = {
      avatarBox: {
        userSelect: 'none',
        height: '2.5rem',
        width: '2.5rem',
        position: 'absolute',
        top: '0.5rem',
        left: '0.5rem',
        margin: '0px'
      },
      userDetails: {
        fontSize: '15px',
        display: 'inline-block',
        wordWrap: 'break-word',
        width: '150px',
        paddingTop: '15px'
      },
      imageDetails: {
        display: 'inline-block',
        width: '70px'
      }
    };
    this.open = false;
    this.anchorEl = null;
  }

  mouse_enter = e => {
    this.setState({
      anchorEl: e.currentTarget
    });
  };

  mouse_leave = e => {
    this.setState({
      anchorEl: null
    });
  };

  visitProfile = e => {
    this.setState({
      anchorEl: null
    });
  };

  handleRequestClose = () => {
    this.setState({
      anchorEl: null
    });
  };

  render() {
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    let user = this.props.user;
    let userNameOrHandle = user.name == null ? user.handle : user.name;

    return (
      <span className="text-comment-at">
        <a
          href={this.props.userProfileURL}
          onMouseEnter={this.mouse_enter}
          onMouseLeave={this.mouse_leave}
          onClick={this.visitProfile}
        >
          {' '}
          {user.name}
          <Popover
            id="mouse-over-popover"
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={this.state.anchorOrigin}
            targetOrigin={this.state.targetOrigin}
            onRequestClose={this.handleRequestClose}
            useLayerForClickAway={false}
            canAutoPosition={true}
            animated={false}
          >
            <div style={{ width: '220px', minHeight: '57px' }}>
              <div style={this.styles.imageDetails}>
                <BlurImage
                  style={this.styles.avatarBox}
                  id={user.id}
                  image={user.avatarimages.small}
                />
              </div>
              <div style={this.styles.userDetails}>
                <b>{userNameOrHandle}</b>
              </div>
            </div>
          </Popover>
        </a>
      </span>
    );
  }
}

UserProfileHover.propTypes = {
  user: PropTypes.object,
  userProfileURL: PropTypes.any
};

export default UserProfileHover;
