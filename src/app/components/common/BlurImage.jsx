import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import addSecurity from '../../utils/filestackSecurity';

class BlurImage extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      homePagev1: window.ldclient.variation('home-page-fix-v1', false)
    };
    this.styles = {
      userPanelClass: {
        border: 'solid 4px #ffffff',
        width: '6.25rem',
        height: '6.25rem',
        position: 'relative'
      }
    };
  }
  imageEnterClick = e => {
    e.preventDefault();
    let keyCode = e.which || e.keyCode;
    keyCode === 13 ? this.imageClick(e) : null;
  };
  imageClick = e => {
    e.preventDefault();
    this.props.imageClickHandler
      ? this.props.imageClickHandler()
      : () => {
          return null;
        };
  };

  render() {
    let svgStyle = {
      filter: `url(#blur-effect-image-${this.props.id})`
    };
    let expireAfter =
      (this.props.team &&
        this.props.team.config &&
        this.props.team.config.filestack_url_expire_after_seconds) ||
      window.process.env.FILESTACK_DEFAULT_EXPIRY;
    if (!this.props.commentInput) {
      return (
        <a
          tabIndex={0}
          aria-label={
            this.props.titleText
              ? this.props.titleText.indexOf(this.props.currentUser.name) >= 0
                ? `your profile`
                : this.props.titleText
              : `Profile picture`
          }
          onClick={this.imageClick}
          onKeyPress={this.imageEnterClick}
          className={`img-blurred-container ${this.state.homePagev1 ? 'fix-v1' : ''} ${
            this.props.isChannel ? '' : 'avatar-box'
          }`}
          style={
            this.props.userPanel
              ? { ...this.props.style, ...this.styles.userPanelClass }
              : this.props.style
          }
        >
          <div className="img-blurred-background">
            <svg width="100%" height="100%">
              <image
                style={svgStyle}
                xlinkHref={addSecurity(this.props.image, expireAfter, this.props.currentUser.id)}
                x="-30%"
                y="-30%"
                width="160%"
                height="160%"
              />
              <filter id={`blur-effect-image-${this.props.id}`}>
                <feGaussianBlur stdDeviation="10" />
              </filter>
            </svg>
          </div>
          <img
            className="img-with-blurred"
            src={addSecurity(this.props.image, expireAfter, this.props.currentUser.id)}
            style={this.props.styleImage ? this.props.styleImage : {}}
            alt=" "
          />
        </a>
      );
    } else {
      return (
        <div
          tabIndex={0}
          aria-label={
            this.props.titleText
              ? this.props.titleText.indexOf(this.props.currentUser.name) >= 0
                ? `your profile`
                : this.props.titleText
              : `Profile picture`
          }
          onClick={
            this.props.imageClickHandler
              ? this.props.imageClickHandler
              : () => {
                  return null;
                }
          }
          onKeyPress={this.imageEnterClick}
          className={`img-blurred-container ${this.state.homePagev1 ? 'fix-v1' : ''} ${
            this.props.isChannel ? '' : 'avatar-box'
          }`}
          style={
            this.props.userPanel
              ? { ...this.props.style, ...this.styles.userPanelClass }
              : this.props.style
          }
        >
          <div className="img-blurred-background">
            <svg width="100%" height="100%">
              <image
                style={svgStyle}
                xlinkHref={addSecurity(this.props.image, expireAfter, this.props.currentUser.id)}
                x="-30%"
                y="-30%"
                width="160%"
                height="160%"
              />
              <filter id={`blur-effect-image-${this.props.id}`}>
                <feGaussianBlur stdDeviation="10" />
              </filter>
            </svg>
          </div>
          <img
            className="img-with-blurred"
            src={addSecurity(this.props.image, expireAfter, this.props.currentUser.id)}
            style={this.props.styleImage ? this.props.styleImage : {}}
            alt=" "
          />
        </div>
      );
    }
  }
}

BlurImage.propTypes = {
  image: PropTypes.string,
  id: PropTypes.any,
  isChannel: PropTypes.bool,
  userPanel: PropTypes.bool,
  styleImage: PropTypes.object,
  style: PropTypes.object,
  imageClickHandler: PropTypes.func,
  titleText: PropTypes.string,
  currentUser: PropTypes.object,
  commentInput: PropTypes.bool
};

export default connect(state => ({
  currentUser: state.currentUser.toJS()
}))(BlurImage);
