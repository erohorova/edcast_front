import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import LockIcon from 'edc-web-sdk/components/icons/Lock';
import FollowButton from 'edc-web-sdk/components/FollowButton';

import { TableRow, TableRowColumn } from 'material-ui/Table';
import IconButton from 'material-ui/IconButton/IconButton';

import { toggleFollow } from '../../actions/channelsActions';
import { initUserData } from '../../actions/currentUserActions';

class Channel extends Component {
  constructor(props, context) {
    super(props, context);
    this.toggleFollowClickHandler = this.toggleFollowClickHandler.bind(this);
    this.state = {
      currentFollowersCount: this.props.channel.followersCount
    };
  }

  toggleFollowClickHandler(isFollow) {
    let followerCountUpdated = isFollow
      ? this.state.currentFollowersCount - 1
      : this.state.currentFollowersCount + 1;
    this.props
      .dispatch(toggleFollow(this.props.channel.id, !isFollow))
      .then(() => {
        this.setState({ currentFollowersCount: followerCountUpdated });
        this.props.dispatch(initUserData());
      })
      .catch(err => {
        console.error(`Error in ChannelListItem.toggleFollow.func : ${err}`);
      });
  }

  goToChannel = e => {
    e.preventDefault();
    this.props.channel.allowFollow &&
      this.props.dispatch(push(`/channel/${this.props.channel.slug}`));
  };

  render() {
    let showLockIcon = !!(this.props.team.config && this.props.team.config.show_lock_icon);
    return (
      <TableRow key={1} className="table-row" displayBorder={false}>
        <TableRowColumn className="private-lock__container" style={{ width: '10.5rem' }}>
          <a
            aria-label={`${tr('Visit the channel')}, ${this.props.channel.label}`}
            href="#"
            onClick={this.goToChannel}
          >
            {!!this.props.isPrivate && showLockIcon && (
              <IconButton
                aria-label={tr('Private Channel')}
                className="my-icon-button private-lock"
                tooltip={tr('Private Channel')}
                tooltipPosition="bottom-center"
              >
                <LockIcon />
              </IconButton>
            )}
            <div
              className="container-padding banner"
              style={{
                height: '6rem',
                backgroundImage: `url(\'${this.props.channel.profileImageUrl ||
                  this.props.channel.bannerImageUrl}\')`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                margin: '1rem'
              }}
            />
          </a>
        </TableRowColumn>
        <TableRowColumn className="container-padding" style={{ verticalAlign: 'top' }}>
          <div className="text-overflow">
            <a
              className="matte"
              onTouchTap={() => {
                this.props.channel.allowFollow &&
                  this.props.dispatch(push(`/channel/${this.props.channel.slug}`));
              }}
            >
              <strong>{this.props.channel.label}</strong>
            </a>
            <div className="text-overflow">
              <small>{this.props.channel.description}</small>
            </div>
          </div>
        </TableRowColumn>
        <TableRowColumn
          className="container-padding"
          style={{ width: '7rem', paddingRight: '1rem' }}
        >
          {this.props.channel.followersCount > 0 && (
            <div className="text-right">
              <small>
                {this.state.currentFollowersCount} {tr('Followers')}
              </small>
            </div>
          )}
        </TableRowColumn>
        <TableRowColumn style={{ width: '9.5rem' }}>
          <div className="container-padding">
            {this.props.channel.allowFollow ? (
              <FollowButton
                following={this.props.channel.isFollowing}
                className="follow"
                label={tr(this.props.channel.isFollowing ? 'Following' : 'Follow')}
                hoverLabel={tr(this.props.channel.isFollowing ? 'Unfollow' : '')}
                pendingLabel={tr(
                  this.props.channel.isFollowing ? 'Unfollowing...' : 'Following...'
                )}
                onTouchTap={this.toggleFollowClickHandler.bind(
                  this,
                  this.props.channel.isFollowing
                )}
                pending={this.props.channel.followPending}
              />
            ) : (
              <SecondaryButton label={tr('Coming Soon')} disabled={true} />
            )}
          </div>
        </TableRowColumn>
      </TableRow>
    );
  }
}

Channel.propTypes = {
  channel: PropTypes.object,
  isPrivate: PropTypes.any,
  team: PropTypes.object
};

export default connect(state => ({
  team: state.team.toJS()
}))(Channel);
