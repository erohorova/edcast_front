import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TimeAgo from 'react-timeago';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import translateTA from '../../utils/translateTimeAgo';
import getDefaultImage from '../../utils/getDefaultCardImage';
import find from 'lodash/find';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { push } from 'react-router-redux';
import unescape from 'lodash/unescape';
import { saveConsumptionJourneyHistoryURL } from '../../actions/journeyActions';

class Journey extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      isCardV3: window.ldclient.variation('card-v3', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };
  }

  showJourney(slug) {
    let journeyBackUrl = window.location.pathname;
    if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
      this.props.dispatch(saveConsumptionJourneyHistoryURL(journeyBackUrl));
    }
    window.history.pushState(null, null, `/journey/${slug}`);
    this.props.dispatch(openStandaloneOverviewModal(this.props.journey, 'journey', {}));
  }

  render() {
    let languageAbbreviation =
      this.props.currentUser.profile && this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);
    let journey = this.props.journey;
    let imageUrl;
    let isFileAttached = journey.filestack && !!journey.filestack.length;
    let imageFileStack = !!(
      isFileAttached &&
      journey.filestack[0].mimetype &&
      ~journey.filestack[0].mimetype.indexOf('image/')
    );
    if (journey.imageUrl) imageUrl = journey.imageUrl;
    else {
      if (imageFileStack) {
        imageUrl = journey.filestack[0].url;
      } else if (journey.fileResources) {
        let findImage = find(journey.fileResources, function(el) {
          return el.fileType == 'image';
        });
        if (findImage) imageUrl = findImage.fileUrl;
      }
    }
    return (
      <TableRow className="table-row" displayBorder={false}>
        <TableRowColumn className="table-row-column">
          <a className="collections" onTouchTap={this.showJourney.bind(this, journey.slug)}>
            <div className="journey-image">
              <div
                className="container-padding image banner"
                style={{
                  height: '8rem',
                  backgroundImage: `url(\'${imageUrl || this.state.defaultImage}\')`,
                  backgroundRepeat: 'no-repeat',
                  backgroundPosition: 'center',
                  backgroundSize: 'cover',
                  margin: '1rem',
                  position: 'relative'
                }}
              />
            </div>
          </a>
        </TableRowColumn>
        <TableRowColumn
          className="container-padding"
          style={{ verticalAlign: 'top', position: 'relative' }}
        >
          <div>
            <div className="text-overflow vertical-spacing-small">
              <div>
                <a
                  className="matte collections"
                  onTouchTap={this.showJourney.bind(this, journey.slug)}
                >
                  <div className="text-overflow">
                    <b>{unescape(journey.title || journey.message)}</b> -{' '}
                    {journey.journeyPacksCount} Pathways
                  </div>
                </a>
              </div>
              <div className="text-overflow">
                {journey.createdAt && (
                  <a className="matte time-tag">
                    <TimeAgo date={journey.createdAt} live={false} formatter={formatter} />
                  </a>
                )}
              </div>
            </div>
            <div
              className="like-comment-container horizontal-spacing-xlarge"
              style={{ position: 'absolute', bottom: '14px' }}
            >
              {/*<span className="horizontal-spacing-medium">
              <LikeIcon color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{journey.votesCount || '0'}</small>
            </span>
            <span className="horizontal-spacing-medium">
              <ChatBubbleOutlineAsset color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{journey.commentsCount || '0'}</small>
            </span>*/}
            </div>
          </div>
        </TableRowColumn>
      </TableRow>
    );
  }
}

Journey.propTypes = {
  currentUser: PropTypes.object,
  journey: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(Journey);
