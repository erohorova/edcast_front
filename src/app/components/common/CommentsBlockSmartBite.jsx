import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactStars from 'react-stars';
import CardAnalyticsV2 from 'edc-web-sdk/components/icons/CardAnalytics.v2';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';

import { openCardStatsModal } from '../../actions/modalActions';
import { toggleLikeCardAsync, loadComments } from '../../actions/cardsActions';
import { submitRatings, updateRatedQueueAfterRating } from '../../actions/relevancyRatings';

import Avatar from 'material-ui/Avatar';
import InsightDropDownActions from '../feed/InsightDropDownActions';
import CommentList from '../feed/CommentList';
import { push } from 'react-router-redux';
import isEmpty from 'lodash/isEmpty';
import CheckedIcon from 'material-ui/svg-icons/action/done';
import colors from 'edc-web-sdk/components/colors/index';

class CommentsBlockSmartBite extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      checkedCard: props.checkedCard,
      isUpvoted: props.isUpvoted,
      votesCount: props.votesCount,
      commentsCount: props.smartBite.commentsCount,
      averageRating: props.smartBite.averageRating,
      open: false,
      commentsLoaded: false,
      voters: props.smartBite.voters,
      isCardV3: window.ldclient.variation('card-v3', false),
      configureCompleteButton: window.ldclient.variation(
        'configurable-colors-for-complete-button',
        false
      )
    };

    this.styles = {
      actionIcon: {
        paddingLeft: 0,
        paddingRight: 0,
        width: 'auto'
      },
      completeBtn: { height: '1.75rem', padding: '0.4375rem 0.765rem', margin: '0 1.875rem' }
    };

    this.cardLikeHandler = this.cardLikeHandler.bind(this);
  }

  componentDidMount() {
    let isECL = /^ECL-/.test(this.props.smartBite.id);
    if (!isECL && this.state.commentsCount) {
      loadComments(this.props.smartBite.id, this.state.commentsCount, this.props.smartBite.cardType)
        .then(data => {
          this.setState({ comments: data, showComment: true, commentsLoaded: true });
        })
        .catch(err => {
          console.error(`Error in CommentsBlockSmartBite.loadComments.func : ${err}`);
        });
    } else {
      this.setState({ comments: [], showComment: true, commentsLoaded: true });
    }
    this.allCountRating(this.props.smartBite);
  }

  asyncDispatch = (func, id, cardType, up) => {
    return new Promise(resolve => {
      func(id, cardType, up, this.props.dispatch)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          console.error(`Error in CommentsBlockSmartBite.asyncDispatch.func : ${err}`);
        });
    });
  };

  async cardLikeHandler() {
    let _this = this;
    if (this.state.pendingLike || this.state.checkedCard.id != this.props.smartBite.id) {
      return;
    }
    await this.setStatePending('pendingLike', true);
    await this.asyncDispatch(
      toggleLikeCardAsync,
      this.props.smartBite.id,
      this.props.smartBite.cardType,
      !this.state.isUpvoted
    )
      .then(() => {
        this.setState(prevState => {
          let voters = !prevState.isUpvoted
            ? [_this.props.currentUser].concat(_this.state.voters)
            : _this.state.voters.filter(user => _this.props.currentUser.id != user.id);
          return {
            votesCount: Math.max(prevState.votesCount + (!prevState.isUpvoted ? 1 : -1), 0),
            isUpvoted: !prevState.isUpvoted,
            voters
          };
        });
      })
      .catch(err => {
        console.error(`Error in CommentsBlockSmartBite.toggleLikeCardAsync.func : ${err}`);
      });
    await this.setStatePending('pendingLike', false);
  }

  setStatePending = (state, val) => {
    return new Promise(resolve => {
      this.setState(
        {
          [state]: val
        },
        () => {
          resolve(true);
        }
      );
    });
  };

  handleCardAnalyticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.props.smartBite));
  };

  updateCommentCount = count => {
    if (this.state.checkedCard.id == this.props.smartBite.id) {
      this.setState({ commentsCount: count });
    }
  };

  // work with rating
  ratingChanged = userRating => {
    this.setState({
      averageRating: userRating
    });
    let payload = {
      rating: userRating
    };
    submitRatings(this.props.smartBite.id, payload)
      .then(response => {
        this.setState({
          averageRating: response.averageRating
        });
        this.allCountRating(response);
      }, this)
      .catch(err => {
        console.error(`Error in CommentsBlockSmartBite.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.props.smartBite));
  };

  allCountRating = smartBite => {
    if (smartBite.allRatings) {
      let ratingCount = 0;
      for (let i = 1; i <= 5; i++) {
        ratingCount += smartBite.allRatings[i] ? smartBite.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
    }
  };

  handleCardAnalayticsModal = () => {
    this.props.dispatch(openCardStatsModal(this.state.card));
  };

  onUserHandle = handle => {
    this.props.dispatch(push(`/${handle}`));
  };

  render() {
    let maxNum = 9;

    let background =
      (this.state.configureCompleteButton &&
        ((+this.props.completeStatus === 98 && colors.markAsCompleteBackground) ||
          (+this.props.completeStatus === 100 && colors.completedBackground))) ||
      null;

    let textColor =
      (this.state.configureCompleteButton &&
        ((+this.props.completeStatus === 98 && colors.markAsCompleteText) ||
          (+this.props.completeStatus === 100 && colors.completedText))) ||
      '#ffffff';

    const { currentUser } = this.props;
    return (
      <div>
        {this.state.commentsLoaded && (
          <div>
            <div className={`pathway-details ${this.state.isCardV3 ? 'pathway-details-v3' : ''}`}>
              <div className="pathway-actions">
                {Permissions['enabled'] !== undefined && Permissions.has('LIKE_CONTENT') && (
                  <div>
                    <div className="icon-inline-block">
                      <button
                        className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                        onClick={this.cardLikeHandler}
                        disabled={this.props.previewMode}
                      >
                        {!this.state.isUpvoted && (
                          <span tabIndex={-1} className="hideOutline" aria-label="like">
                            <LikeIcon />
                          </span>
                        )}
                        {this.state.isUpvoted && (
                          <span tabIndex={-1} className="hideOutline" aria-label="liked">
                            <LikeIconSelected />
                          </span>
                        )}
                        <span className="tooltiptext">{tr('Like')}</span>
                      </button>
                      <small className="actions-counter">
                        {this.state.votesCount ? abbreviateNumber(this.state.votesCount) : '0'}
                      </small>
                      {!isEmpty(this.state.voters) && (
                        <span style={{ paddingLeft: '20px' }}>
                          {this.state.voters.slice(0, maxNum).map(voter => {
                            return (
                              <span style={{ padding: '0 4px' }}>
                                <button
                                  aria-label={`Visit ${voter.name}'s handle`}
                                  className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                                  onClick={this.onUserHandle.bind(
                                    this,
                                    voter.handle.replace('@', '') === currentUser.handle
                                      ? 'me'
                                      : voter.handle
                                  )}
                                >
                                  <span tabIndex={-1} className="hideOutline">
                                    <Avatar
                                      size={24}
                                      src={
                                        (voter.avatarimages && voter.avatarimages.tiny) ||
                                        voter.avatar ||
                                        ''
                                      }
                                    />
                                  </span>
                                  <span className="tooltiptext">{voter.name}</span>
                                </button>
                              </span>
                            );
                          })}
                          {this.state.voters.length > maxNum && (
                            <span style={{ color: '#4a90e2' }}>{maxNum}+</span>
                          )}
                        </span>
                      )}
                    </div>
                  </div>
                )}
              </div>

              <div className="pathway-actions pathway-actions_to-right">
                {!this.state.isCardV3 && (
                  <div className="action">{this.props.smartBite.viewsCount + ' Views'}</div>
                )}
                <div className="action">
                  {this.state.isCardV3 &&
                    Permissions.has('MARK_AS_COMPLETE') &&
                    !this.props.smartBite.markFeatureDisabledForSource && (
                      <span>
                        <button
                          onClick={this.props.completeClickHandler.bind(this)}
                          style={{
                            ...this.styles.completeBtn,
                            ...{ background, color: textColor }
                          }}
                          className={`my-icon-button tooltip tooltip_bottom-center ${
                            !this.state.configureCompleteButton ? 'action-btn-color' : ''
                          } my-icon-button_mark_complete ${
                            this.state.isCardV3 ? 'button_mark_complete_v3' : ''
                          } ${
                            +this.props.completeStatus === 100
                              ? 'button_mark_complete_disabled_on_complete'
                              : +this.props.completeStatus !== 98
                              ? 'button_mark_complete_disabled'
                              : ''
                          }`}
                          disabled={+this.props.completeStatus !== 98}
                        >
                          <CheckedIcon
                            aria-label={tr('Mark as Complete')}
                            color={textColor}
                            className="mark-complete-incomplete-button"
                          />
                          {+this.props.completeStatus === 100
                            ? tr('Completed')
                            : tr('Mark as Complete')}
                          <span className="tooltiptext">
                            {+this.props.completeStatus === 98
                              ? tr('Mark as Complete')
                              : +this.props.completeStatus === 100
                              ? tr('Completed')
                              : tr('All the SmartCards needs to be completed first')}
                          </span>
                        </button>
                      </span>
                    )}
                  {this.props.isOwner ||
                    (Permissions.has('VIEW_CARD_ANALYTICS') && (
                      <button
                        className="my-icon-button my-icon-button_disabled my-icon-button_small tooltip tooltip_bottom-center"
                        onClick={this.handleCardAnalyticsModal}
                        disabled={this.props.previewMode}
                        aria-label="Card Statistics"
                      >
                        <CardAnalyticsV2 />
                        <span className="tooltiptext">{tr('Card Statistics')}</span>
                      </button>
                    ))}
                </div>

                {!this.props.previewMode && (
                  <div
                    className="action"
                    style={{ transform: `${this.state.isCardV3 ? 'rotate(90deg)' : 'none'}` }}
                  >
                    <InsightDropDownActions
                      card={this.props.smartBite}
                      author={this.props.smartBite.author}
                      dismissible={false}
                      disableTopics={false}
                      editPathway={this.editPathway}
                      isCompleted={this.props.isCompleted}
                      cardUpdated={this.props.cardUpdated}
                      isPathwayCardProject={this.props.isPathwayCardProject}
                      type={this.props.type}
                      reviewStatusLabel={this.props.reviewStatusLabel}
                      completeStatus={this.props.completeStatus}
                      isCardV3={this.state.isCardV3}
                      isCardV3Pathway={this.state.isCardV3}
                    />
                  </div>
                )}
              </div>
            </div>

            <div className="comments-block">
              {this.props.smartBite &&
                this.state.commentsLoaded &&
                this.state.showComment &&
                !this.state.isLiveStream && (
                  <CommentList
                    cardId={this.props.smartBite.id}
                    cardOwner={this.props.smartBite.author}
                    cardType={this.props.smartBite.cardType}
                    comments={this.state.comments}
                    pending={this.props.smartBite.commentPending}
                    numOfComments={this.state.commentsCount}
                    videoId={this.props.smartBite.cardId ? this.props.smartBite.id : null}
                    isCardV3={this.state.isCardV3}
                    updateCommentCount={this.updateCommentCount}
                  />
                )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

CommentsBlockSmartBite.propTypes = {
  previewMode: PropTypes.bool,
  votesCount: PropTypes.number,
  isOwner: PropTypes.bool,
  isUpvoted: PropTypes.bool,
  currentUser: PropTypes.object,
  checkedCard: PropTypes.object,
  smartBite: PropTypes.object,
  cardUpdated: PropTypes.func,
  isCompleted: PropTypes.bool,
  team: PropTypes.object,
  isPathwayCardProject: PropTypes.bool,
  type: PropTypes.string,
  isCardV3: PropTypes.bool,
  completeClickHandler: PropTypes.func,
  completeStatus: PropTypes.any,
  reviewStatusLabel: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(CommentsBlockSmartBite);
