import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ReactStars from 'react-stars';
import slice from 'lodash/slice';
import find from 'lodash/find';
import unescape from 'lodash/unescape';

import { tr } from 'edc-web-sdk/helpers/translations';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import colors from 'edc-web-sdk/components/colors/index';
import { rateCard } from 'edc-web-sdk/requests/cards';

import { Avatar } from 'material-ui';
import LinearProgress from 'material-ui/LinearProgress';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';

import SvgImageResized from './ImageResized';
import BlurImage from './BlurImage';
import Bia from './Bia';
import DateConverter from './DateConverter';

import getDefaultImage from '../../utils/getDefaultCardImage';
import { submitRatings, updateRatedQueueAfterRating } from '../../actions/relevancyRatings';
import TooltipLabel from '../common/TooltipLabel';
import CardV3SkillTypeEffort from './../cards/v3/CardV3SkillTypeEffort';
import { Permissions } from '../../utils/checkPermissions';

class MainInfoSmartBite extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      smartBite: props.smartBite,
      open: false,
      truncateMessage: props.smartBite.message && props.smartBite.message.length > 140,
      openMoreChannels: false,
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      averageRating: props.smartBite.averageRating,
      ratingCount: 0,
      isPathwayPaid: !!this.props.isPathwayPaid,
      newSkillLevel: this.props.smartBite.skillLevel,
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      configureCompleteButton: window.ldclient.variation(
        'configurable-colors-for-complete-button',
        false
      ),
      allowConsumerModifyLevel: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_allow_consumer_modify_level
      ),
      isCardV3: props.isCardV3 && !props.isOverviewModal
    };

    this.styles = {
      completeV3: { bottom: '2.4rem', width: '9.375rem' },
      popoverClose: {
        padding: 0,
        width: 'auto',
        height: 'auto',
        float: 'right'
      },
      avatarBox: {
        height: '1.625rem',
        width: '1.625rem',
        position: 'relative',
        marginLeft: '0.3125rem'
      },
      avatarBoxV3: {
        height: '1.25rem',
        width: '1.25rem',
        position: 'relative',
        marginLeft: '0.3125rem'
      },
      providerImage: {
        maxHeight: '1.625rem',
        maxWidth: '6rem'
      },
      svgImage: {
        zIndex: 2,
        position: 'relative'
      },
      svgImagev3: {
        height: '13.438rem',
        zIndex: 2,
        position: 'relative'
      },
      completeBar: {
        backgroundColor: '#f0f0f5',
        borderRadius: '0.125rem',
        height: '0.25rem'
      },
      paddingLeftSmall: {
        paddingLeft: '0.3125rem'
      },
      paddingLeftSmallV3: {
        paddingLeft: '0.3125rem',
        marginTop: '-0.3125rem'
      },
      primary: {
        minWidth: '7rem',
        height: '2rem'
      },
      labelStyle: {
        fontSize: '0.8rem'
      }
    };
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
    this.showBIA = !!this.props.team.config.enabled_bia;
    this.isIE =
      /msie\s|trident\/|edge\//i.test(window.navigator.userAgent) &&
      !!(
        document.uniqueID ||
        document.documentMode ||
        window.ActiveXObject ||
        window.MSInputMethodContext
      );
  }

  componentDidMount = () => {
    this.allCountRating(this.state.smartBite);
    let that = this;
    setTimeout(() => {
      let starList = document.querySelectorAll('.inline-flex span[data-forhalf]');
      let starLength = document.querySelectorAll('.inline-flex span[data-forhalf]').length;
      for (let i = 0; i < starLength; i++) {
        starList[i].setAttribute('tabIndex', '0');
        starList[i].setAttribute('aria-label', `rating, ${i + 1}`);
        starList[i].addEventListener('keypress', e => {
          starList[i].classList.remove('removeBlueOutline');
          if (e.keyCode == 13) {
            that.ratingChanged(Number(e.currentTarget.dataset.index) + 1);
          }
        });
        starList[i].addEventListener('mouseover', e => {
          starList[i].classList.add('removeBlueOutline');
        });
      }
    }, 500);
    if (this.state.isCardV3) {
      this.setTitleClipped();
    }
    window.addEventListener('resize', this.setTitleClipped);
  };

  setTitleClipped = () => {
    let el = document.getElementById('pathway-title');
    if (el.offsetWidth < el.scrollWidth) {
      el.classList.add('pathway-title-v3_clipped');
    } else {
      el.classList.remove('pathway-title-v3_clipped');
    }

    if (this.state.isCardV3) {
      let el2 = document.getElementById('pathway-text-creator-v3__container');
      if (el2.offsetWidth < el2.scrollWidth) {
        el2.classList.add('pathway-text-creator-v3__container_clipped');
        this.refs.pathway_cropping_dots.style.left = el2.offsetWidth - 4 + 'px';
      } else {
        el2.classList.remove('pathway-text-creator-v3__container_clipped');
      }
    }
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.setTitleClipped);
  }

  componentWillReceiveProps(nextProp) {
    this.setState(
      {
        smartBite: nextProp.smartBite,
        open: false,
        truncateMessage: nextProp.smartBite.message && nextProp.smartBite.message.length > 140,
        isPathwayPaid: nextProp.isPathwayPaid
      },
      () => {
        if (this.state.isCardV3) {
          this.setTitleClipped();
        }
      }
    );
  }

  truncateMessageText = message => {
    return message.substr(0, 140);
  };

  allCountRating = card => {
    if (!card.allRatings) {
      return;
    }

    if (typeof card.allRatings === 'number') {
      this.setState({ ratingCount: card.allRatings });
    }

    if (typeof card.allRatings === 'object') {
      let ratingCount = 0;
      for (let i = 1; i < 6; i++) {
        ratingCount += card.allRatings[i] ? card.allRatings[i] : 0;
      }
      this.setState({ ratingCount });
    }
  };

  handleTouchTap = event => {
    event.preventDefault();
    this.setState({
      open: true,
      descriptionShow: false,
      anchorEl: document.getElementById('description')
    });
  };

  handleMoreChannelsTap = event => {
    event.preventDefault();
    this.setState({
      openMoreChannels: true,
      anchorEl: document.getElementById('pathwayChannels')
    });
  };

  handleRequestChannelsClose = () => {
    this.setState({
      openMoreChannels: false
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  linkToPush = newURL => {
    this.props.dispatch(push(`/${newURL}`));
  };

  regimeStatusClick = () => {
    this.props.regimeStatusClick();
  };

  handleCardAnalyticsModal = () => {
    this.props.handleCardAnalyticsModal();
  };

  dateConversion_ISOtoLOCALDate = date => {
    let localTimeZone = new Date().toString().match(/([-\+][0-9]+)\s/)[1];
    let gmtStringOfDate = new Date(date).toGMTString();
    let formatedDate = new Date(
      Date.parse(`${gmtStringOfDate} ${localTimeZone}`)
    ).toLocaleDateString(); //I use toLocaleDateString for correct view in IE
    return formatedDate;
  };

  rateCard = level => {
    if (this.props.smartBite.skillValue) {
      return;
    }
    rateCard(this.state.smartBite.id, { level: level })
      .then(data => {
        this.setState({ newSkillLevel: data.skillLevel });
      })
      .catch(err => {
        console.error(`Error in Bia.rateCard.func : ${err}`);
      });
  };

  ratingChanged = userRating => {
    this.setState({
      averageRating: userRating
    });
    let payload = {
      rating: userRating
    };
    submitRatings(this.state.smartBite.id, payload)
      .then(response => {
        let smartBite = this.state.smartBite;
        smartBite.averageRating = response.averageRating;
        this.setState({
          averageRating: response.averageRating,
          smartBite
        });
        this.allCountRating(response);
      }, this)
      .catch(err => {
        console.error(`Error in MainInfoSmartBite.submitRatings.func : ${err}`);
      });
    this.props.dispatch(updateRatedQueueAfterRating(this.state.smartBite));
  };

  render() {
    let { isOwner } = this.props;
    let { allowConsumerModifyLevel } = this.state;
    let channelLabel =
      this.state.smartBite.channels && this.state.smartBite.channels.length > 1
        ? 'Channels: '
        : 'Channel: ';
    let assignerLabel =
      this.state.smartBite.assignments && this.state.smartBite.assignments.length > 1
        ? 'Assigner: '
        : 'Assigners: ';
    let tagLabel =
      this.state.smartBite.tags && this.state.smartBite.tags.length > 1 ? 'Tags:' : 'Tag:';
    let tags =
      this.state.smartBite.tags && this.state.smartBite.tags.map(tag => tag.name).join(', ');
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let title = this.state.smartBite.title || this.state.smartBite.message;
    if (title) {
      title = title.replace(/amp;/gi, '');
    }
    let config = this.props.team.config;
    let hideProvider = !(config && !config.hide_provider);
    let providerVisible =
      !hideProvider && (!!this.state.smartBite.provider || !!this.state.smartBite.providerImage);
    let showCreator =
      config &&
      ((typeof config.show_creator_in_card === 'undefined' && !config.show_creator_in_card) ||
        !!config.show_creator_in_card);
    let truncatedChannels = slice(this.state.smartBite.channels, 0, 10);
    let remainingChannels = [];
    if (this.state.smartBite.channels) {
      remainingChannels = slice(
        this.state.smartBite.channels,
        10,
        this.state.smartBite.channels.length
      );
    }

    let eclDuration = null;
    if (
      this.state.smartBite &&
      this.state.smartBite.eclDurationMetadata &&
      this.state.smartBite.eclDurationMetadata.calculated_duration_display
    ) {
      eclDuration = this.state.smartBite.eclDurationMetadata.calculated_duration_display;
      eclDuration = eclDuration.charAt(0).toUpperCase() + eclDuration.slice(1);
    }
    let badgeVisible = this.state.smartBite && this.state.smartBite.badging;
    let imageUrl = getDefaultImage(this.props.currentUser.id).url;
    let isFileAttached = this.state.smartBite.filestack && !!this.state.smartBite.filestack.length;
    let imageFileStack = !!(
      isFileAttached &&
      this.state.smartBite.filestack[0].mimetype &&
      ~this.state.smartBite.filestack[0].mimetype.indexOf('image/')
    );
    let assignmentsVisible =
      this.state.smartBite.assignments && !!this.state.smartBite.assignments.length;
    let assignmentVisible = this.state.isCardV3
      ? this.state.smartBite.assigner && !!this.state.smartBite.assigner.id
      : this.state.smartBite.assigner && !!this.state.smartBite.assigner.length;
    let startDateVisible =
      this.state.smartBite.startDate ||
      (this.state.smartBite.assignment && this.state.smartBite.assignment.startDate) ||
      this.props.startDate;
    let dueDateVisible =
      this.state.smartBite.dueAt ||
      (this.state.smartBite.assignment && this.state.smartBite.assignment.dueAt) ||
      this.props.dueAt;
    if (this.state.smartBite.imageUrl) imageUrl = this.state.smartBite.imageUrl;
    else {
      if (
        imageFileStack ||
        (isFileAttached &&
          this.state.smartBite.filestack[0] &&
          !this.state.smartBite.filestack[0].mimetype)
      ) {
        imageUrl = this.state.smartBite.filestack[0].url;
      } else if (this.state.smartBite.fileResources) {
        let findImage = find(this.state.smartBite.fileResources, function(el) {
          return el.fileType == 'image';
        });
        if (findImage) imageUrl = findImage.fileUrl;
      }
    }
    let svgStyle = {
      filter: 'url(#blur-effect-1)'
    };

    let cardPlan, planTitle;
    if (this.state.isPathwayPaid) {
      cardPlan = 'PAID';
    } else {
      cardPlan =
        this.state.smartBite &&
        this.state.smartBite.cardMetadatum &&
        this.state.smartBite.cardMetadatum.plan
          ? this.state.smartBite.cardMetadatum.plan
          : 'FREE';
    }
    if (cardPlan == 'free/paid') {
      planTitle = 'The pathway contains one or more paid content';
    }

    let cardImageShadowColor =
      (this.props.team.config && this.props.team.config['card_image_shadow_color']) || '#454560';

    let background =
      (this.state.configureCompleteButton &&
        ((this.props.reviewStatusLabel &&
          this.props.reviewStatusLabel.toLowerCase() === 'mark as complete' &&
          colors.markAsCompleteBackground) ||
          (this.props.reviewStatusLabel &&
            this.props.reviewStatusLabel.toLowerCase() === 'completed' &&
            colors.completedBackground))) ||
      colors.primary;

    let textColor =
      (this.state.configureCompleteButton &&
        ((this.props.reviewStatusLabel &&
          this.props.reviewStatusLabel.toLowerCase() === 'mark as complete' &&
          colors.markAsCompleteText) ||
          (this.props.reviewStatusLabel &&
            this.props.reviewStatusLabel.toLowerCase() === 'completed' &&
            colors.completedText))) ||
      '#ffffff';

    const { currentUser } = this.props;

    return (
      <div
        className={`${!this.props.isOverviewModal ? 'container-padding paper' : ''} ${
          this.state.isCardV3 ? 'container-padding-v3 paper' : ''
        }`}
      >
        <div className="information-row" style={{ height: '100%' }}>
          {!this.props.isOverviewModal && (
            <div
              className={`${
                this.state.isCardV3
                  ? 'information-row__image-block-v3'
                  : 'information-row__image-block'
              }`}
            >
              {this.props.isLoaded && (
                <div className={`img-container ${this.state.isCardV3 ? 'card-v3__image' : ''}`}>
                  {this.state.isCardV3 && (
                    <span
                      className="card-v3__image_gradient"
                      style={{
                        backgroundImage: `${
                          this.isIE
                            ? 'linear-gradient(rgba(123, 123, 153, 0.05) 65%, ' +
                              cardImageShadowColor +
                              ')'
                            : 'linear-gradient(to bottom, rgba(123, 123, 153, 0.05), 65%, ' +
                              cardImageShadowColor +
                              ')'
                        }`
                      }}
                    />
                  )}
                  {!this.state.isCardV3 && (
                    <div className="pathway-label-block">
                      {this.state.smartBite.state === 'draft' && (
                        <div className="pathway-draft-label pathway-header-label">
                          {tr('DRAFT')}
                        </div>
                      )}
                      <div className="pathway-label pathway-header-label">
                        {this.state.smartBite.badging && this.state.smartBite.badging.imageUrl && (
                          <img
                            className="pathway-badge-preview"
                            src={this.state.smartBite.badging.imageUrl}
                          />
                        )}
                        <span>{tr(this.props.type.toUpperCase())}</span>
                      </div>
                      {this.state.edcastPricing && (
                        <TooltipLabel text={tr(planTitle) || ''} html={true} position={'bottom'}>
                          <div className="pathway-label pathway-header-label">
                            <span className="pathway-paid-label-uppercase">
                              <span>PRICE : </span>
                              {cardPlan == 'PAID' ? tr('PAID') : tr(cardPlan)}
                            </span>
                          </div>
                        </TooltipLabel>
                      )}
                    </div>
                  )}
                  {this.state.isCardV3 && (
                    <CardV3SkillTypeEffort
                      allowConsumerModifyLevel
                      params={this.state.smartBite}
                      card={this.state.smartBite}
                      rateCard={this.rateCard.bind(this)}
                      isOnTheFile={true}
                      type="standalone"
                      newSkillLevel={this.state.newSkillLevel}
                    />
                  )}
                  <div className={`pathway-image card-blurred-background`}>
                    <svg id="svg-image-blur" width="100%" height="100%">
                      <title>{unescape(title)}</title>
                      <SvgImageResized
                        cardId={`${this.state.smartBite.id}`}
                        resizeOptions={'height:150'}
                        id="svg-image"
                        xlinkHref={imageUrl}
                        x="-30%"
                        y="-30%"
                        style={svgStyle}
                        width="160%"
                        height="160%"
                      />
                      <filter id="blur-effect-1">
                        <feGaussianBlur stdDeviation="10" />
                      </filter>
                    </svg>
                  </div>
                  <svg
                    width="100%"
                    height="100%"
                    style={this.state.isCardV3 ? this.styles.svgImagev3 : this.styles.svgImage}
                  >
                    <title>{unescape(title)}</title>
                    <SvgImageResized
                      cardId={`${this.state.smartBite.id}`}
                      resizeOptions={'height:150'}
                      id="svg-image"
                      xlinkHref={imageUrl}
                      x="0%"
                      y="0%"
                      width="100%"
                      height="100%"
                    />
                  </svg>
                </div>
              )}
            </div>
          )}
          <div
            className={`information-row__main-info ${
              this.state.isCardV3 ? 'information-row__main-info-v3' : ''
            }`}
          >
            {(this.state.smartBite.id || this.props.previewMode) && (
              <div
                className={`pathway-main-info ${this.state.isCardV3 ? 'pathway-main-info-v3' : ''}`}
              >
                <div
                  id="pathway-title"
                  className={`pathway-title ${
                    this.props.previewMode ? 'pathway-title-preview' : ''
                  } ${this.state.isCardV3 ? 'pathway-title-v3' : ''}`}
                >
                  {unescape(title)}
                  {this.state.isCardV3 && this.props.previewMode && (
                    <div>
                      <div className="preview-mode" style={{ textAlign: 'right' }}>
                        {tr('preview mode')}
                      </div>
                    </div>
                  )}
                  {this.props.type === 'package' &&
                    this.state.smartBite.packCards &&
                    !!this.state.smartBite.packCards.length && (
                      <span>
                        {' '}
                        ({this.state.smartBite.packCards && this.state.smartBite.packCards.length})
                      </span>
                    )}
                </div>
                <span className="pathway-title-v3__cropping-dots pathway-v3__cropping-dots">
                  ..
                </span>
                {this.state.isCardV3 && (
                  <div className="pathway-title-v3__tooltip pathway-v3__tooltip">{title}</div>
                )}
                <div
                  className={`pathway-text pathway-text__margin ${
                    this.state.isCardV3 ? 'pathway-text-v3' : ''
                  }`}
                  id="description"
                >
                  {(this.state.truncateMessage && (
                    <span>
                      {this.truncateMessageText(unescape(this.state.smartBite.message))}{' '}
                      <span
                        className="more-description"
                        onMouseOver={() => {
                          this.setState({ descriptionShow: true });
                        }}
                        onMouseOut={e => {
                          this.setState({ descriptionShow: false });
                        }}
                        onClick={this.handleTouchTap}
                      >
                        {this.state.isCardV3 ? '..' : tr('more')}
                      </span>
                    </span>
                  )) || (
                    <span
                      dangerouslySetInnerHTML={{
                        __html: this.state.smartBite.message
                      }}
                    />
                  )}
                </div>
                {this.state.isCardV3 && this.state.descriptionShow && (
                  <div className="pathway-description-v3__tooltip pathway-v3__tooltip">
                    {this.state.smartBite.message}
                  </div>
                )}
                <Popover
                  open={this.state.open}
                  anchorEl={this.state.anchorEl}
                  anchorOrigin={{ horizontal: 'middle', vertical: 'top' }}
                  targetOrigin={{ horizontal: 'middle', vertical: 'top' }}
                  onRequestClose={this.handleRequestClose}
                  autoCloseWhenOffScreen={false}
                  className={`pathway-full-description ${
                    this.state.isCardV3 ? 'pathway-full-details-v3' : 'pathway-full-details-v2'
                  }`}
                  animation={PopoverAnimationVertical}
                >
                  <div className="relative">
                    <div className="popover-title">
                      <span className="pathway-description-title">{tr('Description')}</span>
                      <IconButton
                        style={this.styles.popoverClose}
                        onTouchTap={this.handleRequestClose}
                        aria-label="close"
                      >
                        <CloseIcon color="#acadc1" />
                      </IconButton>
                    </div>
                    <div className="pathway-text">{this.state.smartBite.message}</div>
                  </div>
                </Popover>
                {!this.state.isCardV3 && !!truncatedChannels && !!truncatedChannels.length && (
                  <div className="pathway-text info-block" id="pathwayChannels">
                    <span>{tr(channelLabel)}</span>
                    <span className="pathway-labels">
                      {truncatedChannels.map((channel, index) => {
                        return (
                          <span
                            role="button"
                            tabIndex={0}
                            className="pathway-labels channel-link"
                            onTouchTap={this.linkToPush.bind(
                              null,
                              `channel/${channel.slug || channel.id}`
                            )}
                            onKeyDown={e => {
                              if (e.keyCode === 13) {
                                this.linkToPush(`channel/${channel.slug || channel.id}`);
                              }
                            }}
                            key={`channel_${index}`}
                          >
                            {channel.label}
                            {index < truncatedChannels.length - 1 && <span>,</span>}
                          </span>
                        );
                      })}
                      <Popover
                        open={this.state.openMoreChannels}
                        anchorEl={this.state.anchorEl}
                        anchorOrigin={{
                          horizontal: 'middle',
                          vertical: 'bottom'
                        }}
                        targetOrigin={{
                          horizontal: 'middle',
                          vertical: 'top'
                        }}
                        onRequestClose={this.handleRequestChannelsClose}
                        autoCloseWhenOffScreen={false}
                        className="pathway-full-description"
                        animation={PopoverAnimationVertical}
                      >
                        <div className="relative">
                          <div className="popover-title">
                            <span className="pathway-description-title">{tr('Channels')}</span>
                            <IconButton
                              style={this.styles.popoverClose}
                              onTouchTap={this.handleRequestChannelsClose}
                              aria-label="close"
                            >
                              <CloseIcon color="#acadc1" />
                            </IconButton>
                          </div>
                          <div className="pathway-text">
                            {remainingChannels.map((channel, index) => {
                              return (
                                <span
                                  className="more-channels-link"
                                  onTouchTap={this.linkToPush.bind(
                                    null,
                                    `channel/${channel.slug || channel.id}`
                                  )}
                                  key={`channel_${index}`}
                                >
                                  {channel.label}
                                  {index < remainingChannels.length - 1 && <span>,</span>}
                                </span>
                              );
                            })}
                          </div>
                        </div>
                      </Popover>
                      {remainingChannels.length > 0 && (
                        <span>
                          <span className="more-description" onClick={this.handleMoreChannelsTap}>
                            {' '}
                            {this.state.isCardV3 ? '..' : tr('more')}
                          </span>
                        </span>
                      )}
                    </span>
                  </div>
                )}

                {!this.state.isCardV3 &&
                  this.state.smartBite.tags &&
                  !!this.state.smartBite.tags.length && (
                    <div className="pathway-text info-block">
                      <span>{tr(tagLabel)} </span>
                      <span className="pathway-labels">{tags}</span>
                    </div>
                  )}

                <div
                  className={`stick-to-bottom ${this.state.isCardV3 ? 'stick-to-bottom_v3' : ''}`}
                >
                  <div
                    className={`pathway-text inline-flex ${
                      this.state.isCardV3 ? 'pathway-text-stars-v3' : ''
                    }`}
                  >
                    {!this.state.isCardV3 &&
                      this.state.smartBite.eclDurationMetadata &&
                      this.state.smartBite.eclDurationMetadata.calculated_duration_display &&
                      eclDuration && (
                        <div style={{ display: 'flex', margin: 'auto auto' }}>
                          <span style={{ marginRight: '0.3125rem' }}>{tr('Duration: ')}</span>
                          <span key={'duration'}>
                            <span>{eclDuration}</span>
                          </span>
                        </div>
                      )}
                    {!this.state.isCardV3 &&
                      this.state.smartBite.eclDurationMetadata &&
                      this.state.smartBite.eclDurationMetadata.calculated_duration_display &&
                      eclDuration &&
                      Permissions.has('CAN_RATE') && (
                        <span style={{ color: '#d6d6e1' }}>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                      )}
                    {badgeVisible && this.state.isCardV3 && (
                      <div
                        className={`pathway-text inline-flex align-inline-box margin-l5 ${
                          this.state.isCardV3 ? 'align-inline-box-v3' : ''
                        }`}
                      >
                        <span>{tr('Credential:')}</span>
                        <span
                          style={
                            this.state.isCardV3
                              ? this.styles.paddingLeftSmallV3
                              : this.styles.paddingLeftSmall
                          }
                          className="pathway-labels"
                        >
                          <Avatar
                            src={this.state.smartBite.badging.imageUrl}
                            size={25}
                            backgroundColor={'#fff'}
                          />
                        </span>
                        <span
                          style={this.styles.paddingLeftSmall}
                          className="pathway-labels"
                          title={this.state.smartBite.badging.title}
                        >
                          {this.state.smartBite.badging.title}
                        </span>
                        <span className="dot" style={{ margin: '0.625rem' }} />
                      </div>
                    )}
                    {Permissions.has('CAN_RATE') && (
                      <div>
                        <ReactStars
                          onChange={this.ratingChanged}
                          count={5}
                          size={16}
                          half={false}
                          color1={'#d6d6e1'}
                          color2={'#6f708b'}
                          className={`relevancyRatingStars ${
                            this.state.isCardV3 ? 'relevancyRatingStars-v3' : ''
                          }`}
                          value={this.state.averageRating ? this.state.averageRating : 0}
                        />
                        <span
                          style={{
                            fontWeight: 600,
                            paddingLeft: '2px',
                            fontSize: '0.85rem'
                          }}
                        >
                          {!this.state.isCardV3 ? '(' : ''}
                          {this.state.ratingCount > 100
                            ? '99+'
                            : this.state.isCardV3 && +this.state.ratingCount === 0
                            ? ''
                            : +this.state.ratingCount}
                          {!this.state.isCardV3 ? ')' : ''}
                        </span>
                      </div>
                    )}
                  </div>
                  {!this.state.isCardV3 &&
                    assignmentsVisible &&
                    startDateVisible &&
                    dueDateVisible && (
                      <div className="pathway-text info-block assigners__container">
                        {assignmentsVisible && (
                          <div className="assigners__block">
                            <span>{tr(assignerLabel)}</span>
                            <span className="pathway-labels">
                              {this.state.smartBite.assignments.map((assignment, index) => {
                                return (
                                  <span
                                    onTouchTap={this.linkToPush.bind(
                                      null,
                                      this.state.smartBite.assignments_type === 'people'
                                        ? assignment.handle
                                        : 'teams/' + assignment.slug
                                    )}
                                    className="pathway-labels"
                                    key={`assignment_${index}`}
                                  >
                                    {assignment.name}
                                    {index < this.state.smartBite.assignments.length && (
                                      <span>,</span>
                                    )}
                                  </span>
                                );
                              })}
                            </span>
                          </div>
                        )}
                        {startDateVisible && (
                          <small className="due-date">
                            {tr('Start Date')}:
                            <DateConverter isMonthText="true" date={startDateVisible} />
                          </small>
                        )}
                        {dueDateVisible && (
                          <small className="due-date">
                            {tr('Due Date')}:
                            <DateConverter isMonthText="true" date={dueDateVisible} />
                          </small>
                        )}
                      </div>
                    )}
                  <div className="pathway-text__bottom-row">
                    <div
                      id={`${
                        this.state.isCardV3
                          ? 'pathway-text-creator-v3__container'
                          : 'pathway-text-creator'
                      }`}
                      className={`${
                        this.state.isCardV3 ? 'pathway-text-creator-v3__container' : ''
                      }`}
                    >
                      {!this.state.isCardV3 && providerVisible && (
                        <div
                          className="pathway-text inline-flex align-inline-box margin-l5"
                          style={{ marginRight: '0.625rem' }}
                        >
                          <span>{tr('Provider:')}</span>
                          {this.state.smartBite.providerImage && (
                            <span
                              style={this.styles.paddingLeftSmall}
                              className="pathway-labels"
                              key={'provider'}
                            >
                              <img
                                src={this.state.smartBite.providerImage}
                                style={this.styles.providerImage}
                              />
                            </span>
                          )}
                          {this.state.smartBite.provider && (
                            <span
                              style={this.styles.paddingLeftSmall}
                              className="pathway-labels"
                              title={this.state.smartBite.provider}
                            >
                              {this.state.smartBite.provider}
                            </span>
                          )}
                        </div>
                      )}

                      {badgeVisible && !this.state.isCardV3 && (
                        <div
                          className="pathway-text inline-flex align-inline-box margin-l5"
                          style={{ marginRight: '0.625rem' }}
                        >
                          <span>{tr('Badge:')}</span>
                          <span style={this.styles.paddingLeftSmall} className="pathway-labels">
                            <Avatar
                              src={this.state.smartBite.badging.imageUrl}
                              size={25}
                              backgroundColor={'#fff'}
                            />
                          </span>
                          <span
                            style={this.styles.paddingLeftSmall}
                            className="pathway-labels"
                            title={this.state.smartBite.badging.title}
                          >
                            {this.state.smartBite.badging.title}
                          </span>
                        </div>
                      )}
                      {(badgeVisible || providerVisible) && !this.state.isCardV3 && (
                        <span
                          style={{
                            borderLeft: '1px solid #d6d6e1',
                            paddingRight: '10px',
                            height: '14px'
                          }}
                        />
                      )}
                      <span className="posted-assigner__container">
                        {showCreator && (
                          <div
                            className={`pathway-text inline-flex align-inline-box margin-l5 ${
                              this.state.isCardV3 ? 'pathway-text-creator-v3' : ''
                            }`}
                          >
                            <span>{this.state.isCardV3 ? tr('Posted by:') : tr('Creator:')}</span>
                            <BlurImage
                              style={
                                !this.state.isCardV3
                                  ? this.styles.avatarBox
                                  : this.styles.avatarBoxV3
                              }
                              id={this.state.smartBite.id}
                              image={
                                this.state.smartBite.author &&
                                (this.state.smartBite.author.picture ||
                                  (this.state.smartBite.author.avatarimages &&
                                    this.state.smartBite.author.avatarimages.small) ||
                                  defaultUserImage)
                              }
                            />
                            <span
                              style={this.styles.paddingLeftSmall}
                              className="pathway-labels cursor-pointer"
                              key={'author'}
                              onTouchTap={
                                this.state.smartBite.author && this.state.smartBite.author.handle
                                  ? this.linkToPush.bind(
                                      null,
                                      this.state.smartBite.author.handle.replace('@', '') ===
                                        currentUser.handle
                                        ? 'me'
                                        : this.state.smartBite.author.handle
                                    )
                                  : null
                              }
                            >
                              <span
                                title={
                                  this.state.smartBite.author
                                    ? this.state.smartBite.author.name
                                      ? this.state.smartBite.author.name
                                      : this.state.smartBite.author.fullName
                                    : ''
                                }
                              >
                                {this.state.smartBite.author
                                  ? this.state.smartBite.author.name
                                    ? this.state.smartBite.author.name
                                    : this.state.smartBite.author.fullName
                                  : ''}
                              </span>
                            </span>
                            {this.state.isCardV3 && (dueDateVisible || assignmentVisible) && (
                              <span className="dot" style={{ margin: '0.625rem' }} />
                            )}
                          </div>
                        )}

                        {this.state.isCardV3 && assignmentVisible && (
                          <div
                            className={`pathway-text inline-flex align-inline-box margin-l5 ${
                              this.state.isCardV3 ? 'pathway-text-assigner-v3' : ''
                            }`}
                          >
                            <span>{tr('Assigner:')}</span>
                            <BlurImage
                              style={this.styles.avatarBoxV3}
                              id={this.state.smartBite.id}
                              image={
                                this.state.smartBite.assigner &&
                                ((this.state.smartBite.assigner.avatarimages &&
                                  this.state.smartBite.assigner.avatarimages.small) ||
                                  defaultUserImage)
                              }
                            />
                            <span
                              style={this.styles.paddingLeftSmall}
                              className="pathway-labels cursor-pointer"
                              key={'author'}
                              onTouchTap={
                                this.state.smartBite.assigner &&
                                this.state.smartBite.assigner.handle
                                  ? this.linkToPush.bind(
                                      null,
                                      this.state.smartBite.assigner.handle.replace('@', '') ===
                                        currentUser.handle
                                        ? 'me'
                                        : this.state.smartBite.assigner.handle
                                    )
                                  : null
                              }
                            >
                              <span
                                style={{ fontWeight: '600' }}
                                title={
                                  this.state.smartBite.assigner
                                    ? this.state.smartBite.assigner.name
                                      ? this.state.smartBite.assigner.name
                                      : this.state.smartBite.assigner.fullName
                                    : ''
                                }
                              >
                                {this.state.smartBite.assigner
                                  ? this.state.smartBite.assigner.name
                                    ? this.state.smartBite.assigner.name
                                    : this.state.smartBite.assigner.fullName
                                  : ''}
                              </span>
                            </span>
                            {this.state.isCardV3 && dueDateVisible && (
                              <span className="dot" style={{ margin: '0.625rem' }} />
                            )}
                          </div>
                        )}
                      </span>
                      {this.state.isCardV3 && dueDateVisible && (
                        <div
                          className={`pathway-text inline-flex align-inline-box margin-l5 ${
                            this.state.isCardV3 ? 'pathway-text-date-v3' : ''
                          }`}
                        >
                          {tr('Due Date')}:&#8194;
                          <DateConverter date={dueDateVisible} />
                        </div>
                      )}
                    </div>
                    <span
                      ref="pathway_cropping_dots"
                      className="pathway-text-creator-v3__cropping-dots pathway-v3__cropping-dots"
                    >
                      ..
                    </span>
                    {this.state.isCardV3 && dueDateVisible && (
                      <div className="pathway-text-date-v3__tooltip pathway-v3__tooltip">
                        {tr('Due Date')}:&#8194;
                        <DateConverter date={dueDateVisible} />
                      </div>
                    )}

                    {!this.props.isOverviewModal && this.state.isCardV3 && (
                      <div
                        className="information-row__btn-block pathway-buttons-block  pathway-buttons-block_v3"
                        style={{ marginLeft: 'auto' }}
                      >
                        {!this.props.previewMode &&
                          this.props.reviewStatusLabel &&
                          !!this.props.reviewStatusLabel.length && (
                            <div
                              className="complete-bar"
                              style={
                                this.props.reviewStatusLabel !== 'Start'
                                  ? this.styles.completeV3
                                  : { bottom: '1.9rem' }
                              }
                            >
                              <div>
                                {(this.props.reviewStatusLabel === 'Continue' ||
                                  this.props.reviewStatusLabel === 'Mark as Complete') && (
                                  <div>
                                    <div className="complete-text complete-text-v3">
                                      {this.props.completeStatus}% {tr('Complete')}
                                    </div>
                                    <LinearProgress
                                      min={0}
                                      style={this.styles.completeBar}
                                      max={100}
                                      color={'#38b6a0'}
                                      mode="determinate"
                                      value={this.props.completeStatus}
                                    />
                                  </div>
                                )}
                              </div>
                              {this.props.reviewStatusLabel === 'Start' && (
                                <div className="action-button start-action-button">
                                  <PrimaryButton
                                    label={tr(this.props.reviewStatusLabel)}
                                    style={this.styles.primary}
                                    labelStyle={this.styles.labelStyle}
                                    onTouchTap={this.regimeStatusClick}
                                    className="review-btn review-btn-v3"
                                  />
                                </div>
                              )}
                            </div>
                          )}
                        {!this.state.isCardV3 && this.props.previewMode && (
                          <div>
                            <div className="preview-mode">{tr('preview mode')}</div>
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            )}
          </div>
          {!this.props.isOverviewModal && !this.state.isCardV3 && (
            <div className="information-row__btn-block pathway-buttons-block">
              {this.showBIA && !this.state.isCardV3 && (
                <Bia
                  card={this.state.smartBite}
                  isClickable={isOwner || allowConsumerModifyLevel}
                />
              )}
              {!this.props.previewMode &&
                this.props.reviewStatusLabel &&
                !!this.props.reviewStatusLabel.length && (
                  <div className="complete-bar">
                    <div>
                      {(this.props.reviewStatusLabel === 'Continue' ||
                        this.props.reviewStatusLabel === 'Mark as Complete') && (
                        <div>
                          <div className="complete-text">
                            {this.props.completeStatus}% {tr('Complete')}
                          </div>
                          <LinearProgress
                            min={0}
                            style={this.styles.completeBar}
                            max={100}
                            mode="determinate"
                            value={this.props.completeStatus}
                          />
                        </div>
                      )}
                    </div>
                    <div className="action-button">
                      <PrimaryButton
                        label={tr(this.props.reviewStatusLabel)}
                        style={{
                          ...this.styles.primary,
                          ...{
                            backgroundColor: background,
                            color: textColor
                          }
                        }}
                        labelStyle={this.styles.labelStyle}
                        onTouchTap={this.regimeStatusClick}
                        className={`review-btn ${this.state.isCardV3 ? 'review-btn-v3' : ''} ${
                          this.props.reviewStatusLabel.toLowerCase() === 'completed'
                            ? this.state.configureCompleteButton
                              ? 'review-btn-completed'
                              : 'review-btn-completed review-btn-completed__color'
                            : ''
                        }`}
                      />
                    </div>
                  </div>
                )}
              {this.props.previewMode && (
                <div>
                  <div className="preview-mode">{tr('preview mode')}</div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

MainInfoSmartBite.propTypes = {
  currentUser: PropTypes.object,
  smartBite: PropTypes.object,
  team: PropTypes.object,
  reviewStatusLabel: PropTypes.string,
  completeStatus: PropTypes.any,
  type: PropTypes.string,
  previewMode: PropTypes.bool,
  isLoaded: PropTypes.bool,
  isOverviewModal: PropTypes.bool,
  isOwner: PropTypes.bool,
  regimeStatusClick: PropTypes.func,
  handleCardAnalyticsModal: PropTypes.func,
  startDate: PropTypes.string,
  dueAt: PropTypes.string,
  isPathwayPaid: PropTypes.bool,
  isCardV3: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(MainInfoSmartBite);
