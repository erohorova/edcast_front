import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import FollowButtonv2 from 'edc-web-sdk/components/FollowButtonv2';
import { toggleFollow } from '../../actions/usersActions';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { GroupLeaderBadge } from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';
import IconButton from 'material-ui/IconButton/IconButton';
import colors from 'edc-web-sdk/components/colors/index';
import { Permissions } from '../../utils/checkPermissions';
import { updateFollowingUsersCount } from '../../actions/currentUserActions';
import BlurImage from '../common/BlurImage';
import uniqWith from 'lodash/uniqWith';
import concat from 'lodash/concat';

class UserSquareItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentFollowersCount: props.user['followersCount'],
      currentFollowingCount: props.user['followingCount'],
      currentBio: props.user['bio'] || '',
      roles: props.user['roles'] ? props.user['roles'] : [],
      isFollowing: null,
      user: props.realUser,
      iconColor: '#454560'
    };
    this.styles = {
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '26px'
      },
      badgeIcons: {
        padding: '0px',
        height: '20px',
        width: '20px',
        marginRight: '9px',
        verticalAlign: 'top'
      },
      tooltipStyles: {
        top: '12px'
      },
      iconStyle: {
        height: '19px',
        width: '19px',
        borderRadius: '20px',
        color: colors.white
      },
      avatarBox: {
        userSelect: 'none',
        height: '4rem',
        width: '4rem',
        position: 'relative'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user && this.props.user) {
      let isFollowersCountChanged =
        nextProps.user['followersCount'] !== this.props.user['followersCount'];
      let isFollowingCountChanged =
        nextProps.user['followingCount'] !== this.props.user['followingCount'];
      if (isFollowersCountChanged || isFollowingCountChanged) {
        this.setState({
          currentFollowersCount: nextProps.user['followersCount'],
          currentFollowingCount: nextProps.user['followingCount']
        });
      }
    }
  }

  followClickHandler = user => {
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user['isFollowing'] || this.props.user['isFollowed'];
    let followerCountUpdated =
      isFollowing && this.state.currentFollowersCount
        ? this.state.currentFollowersCount - 1
        : this.state.currentFollowersCount + 1;
    this.props
      .dispatch(toggleFollow(user['id'], !isFollowing))
      .then(() => {
        this.setState(
          { currentFollowersCount: followerCountUpdated, isFollowing: !isFollowing },
          () => {
            this.props.dispatch(updateFollowingUsersCount(!isFollowing));
          }
        );
      })
      .catch(err => {
        console.error(`Error in UserSquareItem.toggleFollow.func : ${err}`);
      });
  };

  userClickHandler = () => {
    const { currentUserHandle } = this.props;
    this.props.dispatch(
      push(
        `/${
          this.props.user.handle.replace('@', '') === currentUserHandle
            ? 'me'
            : this.props.user.handle
        }`
      )
    );
  };

  getRoleBadges = () => {
    let badgeIcons = [];
    let rolesDefaultNames = this.props.user['rolesDefaultNames']
      ? this.props.user['rolesDefaultNames']
      : [];
    let roles = this.state.roles;
    let allRoles = uniqWith(
      concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    allRoles.map((role, index) => {
      let data;
      let roleParam = role ? role.toLowerCase() : '';
      if (roleParam === 'member') {
        data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'admin') {
        data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'groupleader') {
        data = [<GroupLeaderBadge />, this.state.iconColor];
      } else if (roleParam === 'influencer') {
        data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'sme') {
        data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
      }

      if (!!data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;
        badgeIcons.push(
          <IconButton
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={this.styles.tooltipStyles}
            disableTouchRipple
            style={this.styles.badgeIcons}
            iconStyle={Object.assign({}, this.styles.iconStyle, { backgroundColor: data[1] })}
          >
            {data[0]}
          </IconButton>
        );
      }
    });

    return badgeIcons;
  };

  render() {
    let user = this.props.user;
    let index = this.props.index;
    let roles = typeof user['roles'] === 'object' ? user['roles'] : ['member'];
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user['isFollowing'] || this.props.user['isFollowed'];
    let truncateMessageText =
      this.state.currentBio.length > 56
        ? this.state.currentBio.substr(0, 56) + '...'
        : this.state.currentBio;
    let isCompleted = user['isComplete'];
    let followersCount = this.props.isCurrentUser
      ? this.props.currentUser.get('followersCount')
      : this.state.currentFollowersCount !== undefined
      ? this.state.currentFollowersCount
      : '';
    let followingCount = this.props.isCurrentUser
      ? this.props.currentUser.get('followingCount')
      : this.state.currentFollowingCount !== undefined
      ? this.state.currentFollowingCount
      : '';
    let defaultUserImage = '/i/images/default_user_blue.png';
    let imageUrl =
      user['avatar'] ||
      user['picture'] ||
      (user['avatarimages'] && user['avatarimages']['small']) ||
      defaultUserImage;
    return (
      <div className="user-square-item">
        <div className="user-square-item__container">
          <div className="avatar-row">
            <div className="avatar-container">
              <BlurImage style={this.styles.avatarBox} image={imageUrl} />
            </div>
            {!this.props.isCurrentUser &&
              !this.props.isPendingUser &&
              !Permissions.has('DISABLE_USER_FOLLOW') && (
                <div className="btn-block">
                  <FollowButtonv2
                    following={isFollowing && isCompleted}
                    label={tr(isFollowing && isCompleted ? 'Following' : 'Follow')}
                    hoverLabel={tr(isFollowing ? 'Unfollow' : '')}
                    pendingLabel={tr(isFollowing ? 'Unfollowing...' : 'Following...')}
                    pending={user['followPending']}
                    className="follow"
                    onTouchTap={this.followClickHandler.bind(this, user)}
                    disabled={!isCompleted}
                  />
                </div>
              )}
          </div>
          <div className="user-name-row">
            <a className="user-name" onTouchTap={this.userClickHandler}>
              {user['name']}
            </a>
          </div>
          {user && (
            <div className="badges-container">
              <span>{this.getRoleBadges([user.role])}</span>
            </div>
          )}
          <div>
            {/*<div className="user-info__title">*/}
            {/*User Experience Designer*/}
            {/*</div>*/}
            <div className="user-info__text">{truncateMessageText}</div>
          </div>
          {!this.props.isPendingUser && (
            <div className="user-follow">
              <div className="user-followers">
                <span>{followersCount}</span> {tr('Followers')}
              </div>
              <div className="user-following">
                <span>{followingCount}</span> {tr('Following')}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

UserSquareItem.propTypes = {
  isPendingUser: PropTypes.bool,
  isCurrentUser: PropTypes.bool,
  currentUser: PropTypes.object,
  user: PropTypes.object,
  realUser: PropTypes.object,
  index: PropTypes.number,
  currentUserHandle: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUserHandle: state.currentUser.get('handle')
  };
}

export default connect(mapStoreStateToProps)(UserSquareItem);
