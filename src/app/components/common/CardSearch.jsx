import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors/index';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import TextFieldCustom from '../common/SmartBiteInput';
import Spinner from './spinner';
import { eclSearch } from 'edc-web-sdk/requests/ecl';
import { List, ListItem } from 'material-ui/List';
import _ from 'lodash';
import getCardType from '../../utils/getCardType';
import convertRichText from '../../utils/convertRichText';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});

class CardSearch extends Component {
  constructor(props, context) {
    super(props, context);
    let searchText = '';
    let disableSubmit = false;

    this.state = {
      currentUser: props.currentUser,
      searchText,
      searchCards: [],
      searchError: '',
      disableSubmit,
      limit: 10,
      offset: 0
    };
  }

  checkCard = (item, index) => {
    let searchCards = this.state.searchCards;
    if (this.props.correctLeap) {
      searchCards.forEach(card => {
        if (card.hasOwnProperty('nextCorrectAnswer') && !searchCards[index].nextCorrectAnswer) {
          delete card['nextCorrectAnswer'];
        }
      });
      searchCards[index].nextCorrectAnswer = !this.state.searchCards[index].nextCorrectAnswer;
      this.setState({ searchCards, searchError: '' }, this.props.checkCard(searchCards[index]));
    } else if (this.props.incorrectLeap) {
      searchCards.forEach(card => {
        if (card.hasOwnProperty('nextIncorrectAnswer') && !searchCards[index].nextIncorrectAnswer) {
          delete card['nextIncorrectAnswer'];
        }
      });
      searchCards[index].nextIncorrectAnswer = !this.state.searchCards[index].nextIncorrectAnswer;
      this.setState({ searchCards, searchError: '' }, this.props.checkCard(searchCards[index]));
    } else {
      searchCards[index].checked = !this.state.searchCards[index].checked;
      this.setState({ searchCards, searchError: '' }, this.props.checkCard(searchCards));
    }
  };

  searchTextChange = event => {
    this.setState({ searchText: event.target.value }, () => {
      this.setState({ searchCards: [], searchError: '' });
      !this.state.searchPending && this.getSearchCards(this.state.limit, 0);
    });
  };

  getSearchCards = (limit, offset) => {
    if (!this.state.searchText.trim()) {
      this.setState({
        searchPending: false,
        searchCards: [],
        searchError: '',
        disableSubmit: true
      });
    } else {
      this.setState({ searchPending: true });
      let params = { q: this.state.searchText, limit, offset };
      params['content_type[]'] = [
        'article',
        'poll',
        'video',
        'insight',
        'course',
        'media',
        'video_stream'
      ];
      params['load_hidden'] = 'true';
      if (this.props.isLeap) {
        params['content_type[]'].push('pathway');
      }
      if (this.props.activeType === 'bookmark') {
        params['bookmarked_by'] = { user_ids: `${this.state.currentUser.id}` };
      }
      eclSearch(params)
        .then(items => {
          if (params.q !== this.state.searchText) {
            this.getSearchCards(limit, offset);
          } else {
            if (items && items.cards && items.cards.length) {
              let searchList = _.uniqBy(
                _.differenceBy(items.cards, this.props.smartBites, 'id'),
                'id'
              );
              if (!this.props.isLeap) {
                searchList = _.filter(searchList, el => {
                  return el.cardType !== 'journey' && el.cardType !== 'pack';
                });
              }
              if (!searchList.length) {
                let newOffset = offset + limit;
                this.setState({
                  offset: newOffset
                });
                this.getSearchCards(limit, newOffset);
              } else {
                this.setState({
                  searchCards: _.compact(
                    offset ? _.uniqBy(this.state.searchCards.concat(searchList), 'id') : searchList
                  ),
                  pending: false,
                  disableSubmit: false,
                  searchPending: false,
                  isLastPage: items.cards.length < this.state.limit,
                  offset
                });
              }
            } else {
              this.setState({
                searchError: this.state.searchCards.length ? '' : 'No search results found.',
                searchCards: this.state.searchCards,
                pending: false,
                disableSubmit: !!this.state.searchCards.length,
                searchPending: false,
                isLastPage: true,
                offset: 0
              });
            }
          }
        })
        .catch(err => {
          console.error(`Error in CardSearch.eclSearch.func : ${err}`);
        });
    }
  };

  viewMoreClickHandler = () => {
    let offset = this.state.offset + this.state.limit;
    this.getSearchCards(this.state.limit, offset);
  };

  render() {
    let countResultsText = `${tr('About')} <strong>${this.state.searchCards.length}</strong> ${
      this.state.searchCards.length > 1 ? tr('results') : tr('result')
    } ${tr('for')} <strong>${this.state.searchText}</strong>`;
    return (
      <div className="smart-bite-cards">
        <div className="inputs-block">
          <div className="relative card-search">
            <TextFieldCustom
              hintText="Search"
              style={{ verticalAlign: 'top' }}
              value={this.state.searchText}
              searchIcon={true}
              inputChangeHandler={this.searchTextChange}
              clearDown={false}
              fullWidth={true}
            />
            <div className="error-text">{tr(this.state.searchError)}</div>
          </div>
          {!!this.state.searchCards.length && (
            <div className="search-smartbite-count">
              <span dangerouslySetInnerHTML={{ __html: countResultsText }} />
            </div>
          )}
        </div>
        <div className="cards-search-list">
          {!!this.state.searchCards.length && (
            <div>
              <List>
                {this.state.searchCards.map((item, index) => {
                  let image = `/i/images/courses/course${Math.min(29, Math.max(1, index + 1))}.jpg`;
                  if (item.resource && item.resource.imageUrl) {
                    image = item.resource.imageUrl;
                  }
                  let isFileAttached = item.filestack && !!item.filestack.length;
                  let videoFileStack = !!(
                    isFileAttached &&
                    item.filestack[0].mimetype &&
                    ~item.filestack[0].mimetype.indexOf('video/')
                  );
                  let imageFileStack = !!(
                    isFileAttached &&
                    item.filestack[0].mimetype &&
                    ~item.filestack[0].mimetype.indexOf('image/')
                  );
                  let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
                  let type = getCardType(item, videoFileStack, fileFileStack);
                  let message = convertRichText(item.message);
                  return (
                    <ListItem
                      key={index}
                      onClick={this.checkCard.bind(this, item, index)}
                      innerDivStyle={{
                        padding: '5px 10px',
                        background:
                          item.nextIncorrectAnswer ||
                          item.nextCorrectAnswer ||
                          this.state.searchCards[index].checked
                            ? 'rgba(172, 173, 193, 0.5)'
                            : 'white'
                      }}
                      primaryText={
                        <div className="search-block">
                          <div className="row searched-row">
                            <div className="small-2">
                              <div
                                className="search-image"
                                style={{
                                  backgroundImage: `url(\'${image}\')`
                                }}
                              />
                            </div>
                            <div className="small-7">
                              <div className="search-text">
                                <RichTextReadOnly truncate={true} text={message} />
                              </div>
                            </div>
                            <div className="small-3">
                              <div className="search-type">{tr(type)}</div>
                            </div>
                          </div>
                        </div>
                      }
                    />
                  );
                })}
              </List>
            </div>
          )}
          {this.state.searchPending && (
            <div className="text-center search-pending">
              <Spinner />
            </div>
          )}
          {!!this.state.searchCards.length && !this.state.isLastPage && !this.state.pending && (
            <div className="text-center">
              <SecondaryButton
                label={tr('View More')}
                className="viewMore"
                onTouchTap={this.viewMoreClickHandler}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

CardSearch.propTypes = {
  pathname: PropTypes.string,
  activeType: PropTypes.string,
  currentUser: PropTypes.object,
  smartBites: PropTypes.object,
  correctLeap: PropTypes.bool,
  incorrectLeap: PropTypes.bool,
  isLeap: PropTypes.bool,
  checkCard: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(CardSearch);
