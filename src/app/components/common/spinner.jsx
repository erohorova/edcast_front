import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Spinner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <img src="/i/images/loading.gif" className="loading-spinner" />;
  }
}
