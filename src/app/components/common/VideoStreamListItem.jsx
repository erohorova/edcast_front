import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import PlayIcon from 'edc-web-sdk/components/icons/PlayIcon';
import TimeAgo from 'react-timeago';
import translateTA from '../../utils/translateTimeAgo';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { push } from 'react-router-redux';
import unescape from 'lodash/unescape';

class VideoStreamList extends Component {
  constructor(props, context) {
    super(props, context);
  }

  showVideoStream(slug) {
    window.history.pushState(null, null, `/video_streams/${slug}`);
    this.props.dispatch(openStandaloneOverviewModal(this.props.stream, 'video_stream', {}));
  }

  render() {
    let languageAbbreviation =
      this.props.currentUser.profile && this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);

    return (
      <TableRow id="video-stream-list-item" className="table-row" displayBorder={false}>
        <TableRowColumn style={{ width: '10.5rem' }}>
          <a
            className="videostream"
            onTouchTap={this.showVideoStream.bind(this, this.props.stream.slug)}
          >
            <div
              className="video-stream-image container-padding banner"
              style={{
                backgroundImage: `url(\'${(this.props.stream.videoStream &&
                  this.props.stream.videoStream.imageUrl) ||
                  (this.props.stream.resource && this.props.stream.resource.imageUrl) ||
                  '/assets/card_default_image_min.jpg'}\')`,
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
                backgroundSize: 'cover'
              }}
            >
              <PlayIcon className="center-play-icon" />
            </div>
          </a>
        </TableRowColumn>
        <TableRowColumn className="stream-info container-padding" style={{ background: 'white' }}>
          <div className="info-container text-overflow vertical-spacing-small">
            <div>
              <a
                className="matte videostream"
                onTouchTap={this.showVideoStream.bind(this, this.props.stream.slug)}
              >
                <strong className="videostream-title">
                  {unescape(this.props.stream.message || this.props.stream.title)}
                </strong>
              </a>
            </div>
            <div className="text-overflow">
              {this.props.stream.createdAt && (
                <a
                  className="matte time-tag"
                  onTouchTap={this.showVideoStream.bind(this, this.props.stream.slug)}
                >
                  <TimeAgo date={this.props.stream.createdAt} live={false} formatter={formatter} />
                </a>
              )}
            </div>
          </div>
          <div className="like-comment-container horizontal-spacing-xlarge">
            {/*<span className="horizontal-spacing-medium">
              <LikeIcon color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{this.props.stream.votesCount || '0'}</small>
            </span>
            <span className="horizontal-spacing-medium">
              <ChatBubbleOutlineAsset color={colors.darkGray} style={{verticalAlign: 'middle'}}/>
              <small>{this.props.stream.commentsCount || '0'}</small>
            </span>*/}
          </div>
        </TableRowColumn>
      </TableRow>
    );
  }
}

VideoStreamList.propTypes = {
  currentUser: PropTypes.object,
  stream: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(VideoStreamList);
