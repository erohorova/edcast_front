import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Checkbox from 'material-ui/Checkbox';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';

export class CreateHtmlWidget extends Component {
  constants = {
    htmlWidgetLabel: tr('HTML Widget'),
    htmlWidgetPlaceholder: tr('Write your announcements here and you can add link by simply adding an anchor tag like <a href="link to the source">Click here</a>'),
    saveLabel: tr('Save'),
    savingLabel: tr('Saving...'),
    widgetHintText: tr('Min 5 characters'),
    statusLabel: tr('Enable/Disable'),
    scriptTagError: tr('Widget do not allow script!')
  }

  state = {
    widgetContext: this.props.widget && this.props.widget.code || '',
    isWidgetEnabled: this.props.widget && this.props.widget.enabled || false,
    isValidWidget: (this.props.widget && this.props.widget.code.length > 5) ||  false,
    showScriptTagError: false
  }

  styles = {
    checkOff: {
      width: '1.15rem',
      height: '1.15rem',
      border: '.0625rem solid #acadc1',
      borderRadius: '.125rem',
      boxSizing: 'border-box',
      margin: '0.2rem 0 0 0.19rem'
    },
  }

  updateHtmlWidgetText = (e) => {
    const widgetContext = e.target.value;
    this.setState(() => ({ widgetContext }), () => {
      const scripTagRegExp = /<[^>]*script/;
      const isScriptPresent = scripTagRegExp.test(this.state.widgetContext);
      if (isScriptPresent) {
        this.setState(() => ({
          isValidWidget: false,
          showScriptTagError: true
        }))
      } else {
        this.setState(() => ({
          isValidWidget: true,
          showScriptTagError: false
        }));
      }
    });
  }

  toggleWidgetStatus = () => {
    this.setState((prevState) => ({
      isWidgetEnabled: !prevState.isWidgetEnabled
    }))
  }

  saveWidget = () => {
    const widget = {
      isWidgetEnabled: this.state.isWidgetEnabled,
      widgetContext: this.state.widgetContext
    }
    this.props.saveUpdateWidget(widget);
  }

  render() {
    return (
      <div className="widget-container">
        <div className="widget-header-container">
          <p className="carousel-section-title">{this.constants.htmlWidgetLabel}</p>
          <div className="status-container">
            <span className="status-label">{this.constants.statusLabel}</span>
            <Checkbox
              className="checkbox"
              checked={this.state.isWidgetEnabled}
              onCheck={this.toggleWidgetStatus}
              uncheckedIcon={<div style={this.styles.checkOff} />}
            />
          </div>
          <PrimaryButton
            label={this.props.updatingWidget ? this.constants.savingLabel : this.constants.saveLabel}
            disabled={(!this.state.isValidWidget 
                      || this.state.widgetContext.length < 5
                      || this.props.updatingWidget)}
            onTouchTap={this.saveWidget}
            labelStyle={this.styles.actionBtnLabel}
          />
        </div>
        <textarea
          minLength={5}
          onChange={this.updateHtmlWidgetText.bind(this)}
          value={this.state.widgetContext}
          className="widget-layout"
          placeholder={this.constants.htmlWidgetPlaceholder}
        />
        {
          !this.state.showScriptTagError && (
            <div className="hint-text">{this.constants.widgetHintText}</div>
          )
        }
        {
          this.state.showScriptTagError && (
            <div className="hint-text error-text">{this.constants.scriptTagError}</div>
          )
        }
      </div>
    )
  }
}

CreateHtmlWidget.propTypes = {
  saveUpdateWidget: PropTypes.func,
  widget: PropTypes.object,
  updatingWidget: PropTypes.bool
}

const mapStateToProps = (state) => ({
  
})

export default connect(mapStateToProps)(CreateHtmlWidget)
