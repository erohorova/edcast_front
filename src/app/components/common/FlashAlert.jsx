import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { tr } from 'edc-web-sdk/helpers/translations';

class FlashAlert extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      toShow: props.toShow ? props.toShow : false
    };
    this.cancelClickHandler = this.cancelClickHandler.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.cancelClickHandler();
    }, this.props.displayTimeOut);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ toShow: false });
  }

  cancelClickHandler() {
    this.setState({ toShow: false });
    this.props.removeAlert();
  }

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionName="alert"
        transitionAppear={true}
        transitionAppearTimeout={100}
        transitionEnter={false}
        transitionLeave={false}
      >
        {this.state.toShow && (
          <div className="flash-alert">
            <p>{tr(this.props.message)}</p>
            <span className="close" onClick={this.cancelClickHandler}>
              &times;
            </span>
          </div>
        )}
      </ReactCSSTransitionGroup>
    );
  }
}

FlashAlert.defaultProps = {
  displayTimeOut: 6000
};

FlashAlert.propTypes = {
  message: PropTypes.string,
  toShow: PropTypes.bool,
  removeAlert: PropTypes.func,
  displayTimeOut: PropTypes.number
};

export default connect()(FlashAlert);
