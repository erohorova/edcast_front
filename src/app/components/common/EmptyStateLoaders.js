import React from 'react';
import PropTypes from 'prop-types';

export function TrendingPathwaysAndJourneys(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card trending-pathway-loader animated-background">
          <div className="background-masker card-block channel-block">
            <div className="animated-background" style={{ height: '100%' }}>
              <div className="channel-name-loader">
                <div className="channel-name-loader-right" />
              </div>
              <div className="background-masker channel-line" />
              <div className="channel-button-loader">
                <div className="channel-button-loader-right" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

export function CourseLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card course-loader animated-background">
          <div className="background-masker card-block channel-block">
            <div className="animated-background" style={{ height: '100%' }}>
              <div className="channel-name-loader" />
              <div className="background-masker channel-line" />
              <div className="channel-button-loader" />
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

export function VideoLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card video-loader animated-background">
          <div className="background-masker card-block video-title-block">
            <div className="animated-background" style={{ height: '100%' }}>
              <div className="channel-name-loader" />
              <div className="background-masker channel-line" />
              <div className="channel-button-loader" />
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

export function ProviderLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card provider-loader animated-background" />
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

export function UserLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card user-loader animated-background">
          <div className="background-masker card-block">
            <div className="animated-background">
              <div className="background-masker avatar-wrapper">
                <div className="avatar" />
              </div>
              <div className="background-masker line-0" />
              <div className="background-masker line-1" />
              <div className="background-masker line-2" />
              <div className="background-masker line-3" />
              <div className="background-masker line-4" />
              <div className="background-masker line-5" />
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

UserLoader.propTypes = {
  loadingCards: PropTypes.any
};

ProviderLoader.propTypes = {
  loadingCards: PropTypes.any
};

VideoLoader.propTypes = {
  loadingCards: PropTypes.any
};

CourseLoader.propTypes = {
  loadingCards: PropTypes.any
};

TrendingPathwaysAndJourneys.propTypes = {
  loadingCards: PropTypes.any
};
