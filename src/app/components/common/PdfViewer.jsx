import React, { Component } from 'react';
import PropTypes from 'prop-types';
import pdfPreviewUrl from '../../utils/previewPdf';

class PdfViewer extends Component {
  constructor(props, context) {
    super(props, context);
  }

  shouldComponentUpdate(nextProps) {
    return this.props.src !== nextProps.src;
  }

  render() {
    return (
      <iframe
        height={this.props.height}
        width={this.props.width}
        src={pdfPreviewUrl(this.props.src, this.props.expireAfter, this.props.currentUser.id)}
        onMouseEnter={() => this.props.onMouseEnterEvent()}
        onMouseLeave={() => this.props.onMouseLeaveEvent()}
      />
    );
  }
}

PdfViewer.propTypes = {
  isEdit: PropTypes.bool,
  height: PropTypes.string,
  width: PropTypes.string,
  src: PropTypes.string,
  expireAfter: PropTypes.number,
  isDownloadContentDisabled: PropTypes.bool,
  onMouseEnterEvent: PropTypes.func,
  onMouseLeaveEvent: PropTypes.func,
  currentUser: PropTypes.object
};

export default PdfViewer;
