import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import { tr } from 'edc-web-sdk/helpers/translations';
import SearchIcon from 'material-ui/svg-icons/action/search';
import InputMask from 'react-text-mask';
import unescape from 'lodash/unescape';
import customAutoCorrectedDatePipe from './CustomAutoCorrectedDatePipe';

class SmartBiteInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      inputValue: this.props.value || '',
      errorStyle: this.props.errorStyle
    };

    this.styles = {
      inputs: {
        border: '1px solid #d6d6e1',
        padding: '5px 10px',
        fontSize: '14px',
        lineHeight: '14px',
        color: '#999aad',
        borderRadius: '2px'
      },
      hint: {
        padding: '0 10px',
        fontSize: '12px',
        lineHeight: '31px',
        textAlign: 'left',
        color: '#acadc1',
        top: 0,
        bottom: 0,
        width: '100%',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis'
      },
      mainInputStyle: {
        fontSize: '14px',
        lineHeight: '14px',
        height: 'auto'
      },
      error: {
        padding: '0.5rem 1rem 0'
      },
      searchIcon: {
        position: 'absolute',
        cursor: 'pointer',
        right: '10px',
        top: '10px'
      }
    };
    this.autoCorrectedDatePipe = customAutoCorrectedDatePipe(
      this.props.pipe ? this.props.pipe : ''
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        inputValue: nextProps.value,
        errorStyle: nextProps.errorStyle
      });
    }
  }

  onKeyDownClick = e => {
    if (e.keyCode === 13 && this.props.onKeyDown) {
      if (this.props.clearDown) {
        this.setState({ inputValue: '' });
      }
      this.props.onKeyDown(e);
    }
  };

  onChangeClick = e => {
    this.setState({ inputValue: e.target.value });
    if (this.props.inputChangeHandler) this.props.inputChangeHandler(e);
  };

  render() {
    let notPresent = !this.props.pipe;

    const useFormLabels = window.ldclient.variation('use-form-labels', false);
    return (
      <div className="text-block">
        {useFormLabels && (
          <label required={this.props.required} htmlFor={this.props.hintText}>
            {tr(this.props.hintText)}
          </label>
        )}
        <TextField
          id={this.props.hintText}
          hintText={!useFormLabels && tr(this.props.hintText)}
          hintStyle={this.styles.hint}
          value={unescape(this.state.inputValue)}
          type="text"
          onChange={this.onChangeClick}
          style={this.styles.mainInputStyle}
          fullWidth={this.props.fullWidth}
          errorText={tr(this.props.errorText)}
          errorStyle={
            this.props.errorStyle
              ? { ...this.state.errorStyle, ...this.styles.error }
              : this.styles.error
          }
          rowsMax={this.props.rowsMax}
          onKeyDown={this.onKeyDownClick}
          underlineShow={false}
          inputStyle={this.styles.inputs}
          disabled={this.props.disabled}
          className={this.props.disabled ? 'smartbite-input disabled' : 'smartbite-input'}
          aria-label={tr(this.props.hintText)}
        >
          {this.props.mask && (
            <InputMask
              mask={this.props.mask}
              pipe={notPresent ? false : this.autoCorrectedDatePipe}
              defaultValue={this.props.value}
            />
          )}
        </TextField>
        {this.props.searchIcon && <SearchIcon style={this.styles.searchIcon} />}
      </div>
    );
  }
}

SmartBiteInput.propTypes = {
  hintText: PropTypes.string,
  value: PropTypes.string,
  errorText: PropTypes.string,
  errorStyle: PropTypes.object,
  mask: PropTypes.string,
  searchIcon: PropTypes.object,
  rowsMax: PropTypes.number,
  disabled: PropTypes.bool,
  clearDown: PropTypes.bool,
  pipe: PropTypes.string,
  inputChangeHandler: PropTypes.func,
  onKeyDown: PropTypes.func,
  fullWidth: PropTypes.bool,
  required: PropTypes.bool
};

export default SmartBiteInput;
