import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import TileIcon from 'edc-web-sdk/components/icons/TileView';
import ListIcon from 'edc-web-sdk/components/icons/ListView';

import ActionViewAgenda from 'react-material-icons/icons/action/view-agenda';

const unSelectedColor = '#acadc1';
const selectedColor = '#454560';

class CardViewToggle extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isCardV3: window.ldclient.variation('card-v3', false),
      homePagev1: window.ldclient.variation('home-page-fix-v1', false)
    };
    this.styles = {
      tileView: {
        border: 0,
        padding: 0,
        cursor: 'pointer',
        width: 'auto',
        height: 'auto'
      },
      icons: {
        width: '1rem',
        height: '1rem'
      },
      iconAlignment: {
        marginLeft: '10px'
      },
      hideOutline: {
        outline: 'none',
        display: 'block'
      }
    };
  }

  render() {
    return (
      !(this.state.isCardV3 && this.props.isPathway) && (
        <div id="cardViewToggle">
          <div className={`tile-block ${this.state.homePagev1 ? 'tile-block-v1' : ''}`}>
            {!this.props.curateTab && (
              <div className="tile-block-icons">
                <button
                  style={this.styles.iconAlignment}
                  onClick={this.props.changeViewMode.bind(this, 'Tile')}
                  title={tr('Tile View')}
                >
                  <span tabIndex={-1} style={this.styles.hideOutline}>
                    <TileIcon
                      focusable="false"
                      viewBox="0 0 18 18"
                      style={this.styles.icons}
                      color={this.props.viewRegime === 'Tile' ? selectedColor : unSelectedColor}
                    />
                  </span>
                </button>
                <button
                  style={this.styles.iconAlignment}
                  onClick={this.props.changeViewMode.bind(this, 'List')}
                  title={tr('List View')}
                >
                  <span tabIndex={-1} style={this.styles.hideOutline}>
                    <ListIcon
                      focusable="false"
                      viewBox="0 0 18 18"
                      style={this.styles.icons}
                      color={this.props.viewRegime === 'List' ? selectedColor : unSelectedColor}
                    />
                  </span>
                </button>
                {this.props.bigCardView && (
                  <button
                    style={this.styles.iconAlignment}
                    aria-label={tr('Big view')}
                    onClick={this.props.changeViewMode.bind(this, 'BigCard')}
                    title={tr('Big Card View')}
                  >
                    <span tabIndex={-1} style={this.styles.hideOutline}>
                      <ActionViewAgenda
                        focusable="false"
                        viewBox="0 4 18 18"
                        style={this.styles.icons}
                        color={
                          this.props.viewRegime === 'BigCard' ? selectedColor : unSelectedColor
                        }
                      />
                    </span>
                  </button>
                )}
              </div>
            )}
          </div>
        </div>
      )
    );
  }
}

CardViewToggle.propTypes = {
  viewRegime: PropTypes.string,
  bigCardView: PropTypes.bool,
  curateTab: PropTypes.bool,
  changeViewMode: PropTypes.func,
  isPathway: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardViewToggle);
