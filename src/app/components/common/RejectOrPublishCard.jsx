import React from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

import RejectIcon from 'material-ui/svg-icons/content/clear';
import PublishIcon from 'material-ui/svg-icons/action/done';

const styles = {
  publishColor: {
    color: '#4de8ce'
  },
  rejectColor: {
    color: '#f1736a'
  }
};

const RejectOrPublishCard = props => (
  <div className="curate-card-button" style={{ margin: '5px 10px' }}>
    <div className="row">
      <div className="small-6 column">
        <a onClick={props.publishCard.bind(this, props.card)} className="row">
          <div className="small-5 text-right">
            <PublishIcon style={styles.publishColor} />
          </div>
          <div className="small-7" style={styles.publishColor}>
            {tr('Publish')}
          </div>
        </a>
      </div>
      <div className="small-6 column">
        <a onClick={props.rejectCard.bind(this, props.card)} className="row">
          <div className="small-5 text-right">
            <RejectIcon style={styles.rejectColor} />
          </div>
          <div className="small-7" style={styles.rejectColor}>
            {tr('Reject')}
          </div>
        </a>
      </div>
    </div>
  </div>
);

RejectOrPublishCard.propTypes = {
  rejectCard: PropTypes.func,
  publishCard: PropTypes.func,
  card: PropTypes.object
};

export default RejectOrPublishCard;
