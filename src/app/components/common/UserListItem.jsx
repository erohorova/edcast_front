import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListItem from 'material-ui/List/ListItem';
import UserAvatar from 'edc-web-sdk/components/Avatar';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { toggleFollow } from '../../actions/usersActions';
import { toggleTeamAdmin } from 'edc-web-sdk/requests/teams';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqWith from 'lodash/uniqWith';
import concat from 'lodash/concat';
// import UserBadgesContainer from '../../components/common/UserBadgesContainer';
import { GroupLeaderBadge } from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import MoreIcon from 'material-ui/svg-icons/navigation/more-horiz';
import colors from 'edc-web-sdk/components/colors/index';
import { Permissions } from '../../utils/checkPermissions';

class UserListItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentFollowersCount: this.props.user.get('followersCount'),
      isFollowing: null,
      user: this.props.realUser,
      iconColor: '#454560'
    };

    this.styles = {
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '26px'
      }
    };
  }

  followClickHandler = user => {
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user.get('isFollowing') || this.props.user.get('isFollowed');
    let followerCountUpdated =
      isFollowing && this.state.currentFollowersCount
        ? this.state.currentFollowersCount - 1
        : this.state.currentFollowersCount + 1;
    this.props
      .dispatch(toggleFollow(user.get('id'), !isFollowing))
      .then(() => {
        this.setState({ currentFollowersCount: followerCountUpdated, isFollowing: !isFollowing });
      })
      .catch(err => {
        console.error(`Error in UserListItem.toggleFollow.func : ${err}`);
      });
  };

  userClickHandler = () => {
    this.props.dispatch(push('/' + this.props.user.get('handle')));
  };

  getRoleBadges = (roles, rolesDefaultNames = []) => {
    let allRoles = uniqWith(
      concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    let badgeIcons = [];

    allRoles.map((role, index) => {
      let data;
      let roleParam = role ? role.toLowerCase() : '';
      if (roleParam === 'member') {
        data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'admin') {
        data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'groupleader') {
        data = [<GroupLeaderBadge />, this.state.iconColor];
      } else if (roleParam === 'influencer') {
        data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
      } else if (roleParam === 'sme') {
        data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
      }
      if (data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;

        badgeIcons.push(
          <IconButton
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={{ top: '12px' }}
            disableTouchRipple
            style={{
              padding: '0px',
              height: '20px',
              width: '20px',
              marginLeft: '5px',
              verticalAlign: 'top'
            }}
            iconStyle={Object.assign(
              {},
              { borderRadius: '20px', color: colors.white },
              { backgroundColor: data[1] }
            )}
          >
            {data[0]}
          </IconButton>
        );
      }
    });

    return badgeIcons;
  };

  handleToggleTeamAdmin = id => {
    toggleTeamAdmin(this.props.team.id, id)
      .then(data => {
        let user = this.state.user;
        user.role = data.user.role;
        user.rolesDefaultNames = data.user.rolesDefaultNames;
        this.setState({ user });
      })
      .catch(err => {
        console.error(err);
      });
  };

  render() {
    let user = this.props.user;
    let roles = typeof user.get('roles') === 'object' ? user.get('roles') : ['member'];
    let isFollowing =
      this.state.isFollowing !== null
        ? this.state.isFollowing
        : this.props.user.get('isFollowing') || this.props.user.get('isFollowed');
    return (
      <div className="user-list-item">
        <div className="user-list-item-cell">
          <ListItem
            style={{ marginTop: '10px' }}
            disabled
            leftAvatar={
              <a className="user" onTouchTap={this.userClickHandler}>
                <UserAvatar user={user.toJS()} />
              </a>
            }
            primaryText={
              <div>
                <a className="matte user" onTouchTap={this.userClickHandler}>
                  {user.get('name')}
                </a>
                {/*<UserBadgesContainer roles={roles}/>*/}
                {this.state.user && (
                  <span>
                    {this.getRoleBadges([this.state.user.role, this.state.user.rolesDefaultNames])}
                  </span>
                )}
              </div>
            }
          />
        </div>

        <div className="container-padding user-list-item-cell right">
          {!this.props.isCurrentUser &&
            !this.props.isPendingUser &&
            !Permissions.has('DISABLE_USER_FOLLOW') && (
              <FollowButton
                following={isFollowing}
                label={tr(isFollowing ? 'Following' : 'Follow')}
                hoverLabel={tr(isFollowing ? 'Unfollow' : '')}
                pendingLabel={tr(isFollowing ? 'Unfollowing...' : 'Following...')}
                pending={user.get('followPending')}
                className="follow"
                onTouchTap={this.followClickHandler.bind(this, user)}
              />
            )}
        </div>

        {!this.props.isPendingUser && (
          <div className="container-padding user-list-item-cell right" style={{ marginTop: '7px' }}>
            <div className="text-right">
              <small>
                {this.state.currentFollowersCount !== undefined
                  ? this.state.currentFollowersCount
                  : ''}{' '}
                {tr('Followers')}
              </small>
            </div>
          </div>
        )}
        {this.props.isPendingUser && this.props.isPendingUser == true && (
          <div className="container-padding user-list-item-cell right" style={{ marginTop: '7px' }}>
            <div className="text-right">{tr('Pending Invitation')}</div>
          </div>
        )}

        {this.props.teamPage &&
          !this.props.isCurrentUser &&
          this.props.team.isTeamAdmin &&
          !this.props.isPendingUser && (
            <IconMenu
              className="insight-dropdown"
              iconButtonElement={
                <IconButton aria-label={tr('more')} style={this.props.style || {}}>
                  <MoreIcon color={colors.gray} />
                </IconButton>
              }
              anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
              targetOrigin={{ horizontal: 'right', vertical: 'top' }}
              listStyle={this.styles.iconMenu}
              menuStyle={this.styles.menuItem}
            >
              <MenuItem
                style={this.styles.menuItem}
                primaryText={tr(
                  this.state.user.role == 'admin' ? 'Demote to Member' : 'Promote to Group Leader'
                )}
                className=""
                onTouchTap={this.handleToggleTeamAdmin.bind(this, this.state.user.id)}
              />
            </IconMenu>
          )}
      </div>
    );
  }
}

UserListItem.propTypes = {
  isPendingUser: PropTypes.bool,
  isCurrentUser: PropTypes.bool,
  teamPage: PropTypes.bool,
  team: PropTypes.object,
  user: PropTypes.object,
  style: PropTypes.object,
  realUser: PropTypes.object
};

export default connect()(UserListItem);
