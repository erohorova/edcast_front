import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqBy from 'lodash/uniqBy';

import { Permissions } from '../../utils/checkPermissions';
import abbreviateNumber from '../../utils/abbreviateNumbers';

class SmartBiteInfoCommentsBlock extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      commentatorAvatars: {
        marginLeft: '-0.5rem',
        width: '1.5rem',
        height: '1.5rem',
        marginTop: '0.0625rem',
        border: '0.0625rem solid #ffffff',
        borderRadius: '50%'
      },
      comment: {
        fontSize: '0.625rem',
        color: '#acadc1',
        cursor: 'pointer',
        display: 'inline-block'
      }
    };

    this.state = {
      isNewTileCard: window.ldclient.variation('new-ui-tile-card', false)
    };
  }

  commentatorAvatars = () => {
    let commentList = this.props.comments;
    let unique;
    let unicalTrio;
    if (commentList) {
      unique = uniqBy(commentList, 'user.id');
      unicalTrio = unique.splice(0, 3);
    }
    if (unicalTrio.length === 0) {
      return;
    } else {
      return unicalTrio.map(item => {
        return (
          <img
            key={item.user.id}
            src={item.user.avatarimages.tiny}
            style={this.styles.commentatorAvatars}
          />
        );
      });
    }
  };

  commentsNumber = isLive => {
    let count = isLive ? this.props.liveCommentsCount : this.props.commentsCount;
    return (
      <small className={`count count_comments ${this.props.isCardv3 ? 'card-v3__comment' : ''}`}>
        {this.props.isCardv3 && (
          <span className="card-v3__commentators-avatar">{this.commentatorAvatars()}</span>
        )}
        {count ? abbreviateNumber(count) : ''}&nbsp;
        {count > 1 ? tr('Comments') : count == 1 ? tr('Comment') : ''}
      </small>
    );
  };

  clickHandle = e => {
    e.stopPropagation();
    if (e.keyCode === 13 || e.type === 'click') {
      this.props.handleCardClicked && this.props.handleCardClicked();
    }
  };

  render() {
    return Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT') ? (
      (this.props.isLiveStream ? (
        this.props.liveCommentsCount > 0
      ) : (
        this.props.commentsCount > 0
      )) ? (
        <span
          tabIndex="0"
          className="commentators-avatar"
          onKeyUp={this.clickHandle}
          onClick={this.clickHandle}
        >
          {this.commentsNumber(this.props.isLiveStream)}
        </span>
      ) : (
        <span className="add-comment">
          {!this.props.isStandalone && (
            <button
              style={this.props.isCardv3 ? {} : this.styles.comment}
              className={`inline-block ${this.props.isCardv3 ? 'card-v3__comment' : ''}`}
              tabIndex="0"
              onKeyUp={this.clickHandle}
              onClick={this.clickHandle}
            >
              <span tabIndex={-1} className="hideOutline">
                {tr('Add comment')}
              </span>
            </button>
          )}
        </span>
      )
    ) : (
      <span />
    );
  }
}

SmartBiteInfoCommentsBlock.propTypes = {
  liveCommentsCount: PropTypes.number,
  handleCardClicked: PropTypes.func,
  isLiveStream: PropTypes.bool,
  commentsCount: PropTypes.number,
  comments: PropTypes.array,
  isStandalone: PropTypes.bool,
  isCardv3: PropTypes.bool
};

export default SmartBiteInfoCommentsBlock;
