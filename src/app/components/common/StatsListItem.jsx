import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { fetchCardStatsUsers } from 'edc-web-sdk/requests/cards';
import Avatar from 'edc-web-sdk/components/Avatar';
import { push } from 'react-router-redux';

class StatsListItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      stats: this.props.statsObject,
      statsValue: this.props.value,
      statsUsers: [],
      showUsers: false,
      showUserBlock: 'none',
      offset: 0,
      customStyle: '',
      limit: 10,
      moreUsers: true
    };
    this.showUsers = this.showUsers.bind(this);
    this.fetchMore = this.fetchMore.bind(this);
  }

  componentWillMount() {
    let stat = this.props.statsObject
      .replace('s_count', '')
      .replace('complete', 'mark_as_complete');
    fetchCardStatsUsers(this.props.cardId, stat, 0)
      .then(statsUsers => {
        this.setState({
          statsUsers: statsUsers
        });
      })
      .catch(err => {
        console.error(`Error in StatsListItem.fetchCardStatsUsers.func : ${err}`);
      });
  }

  showUsers() {
    if (this.state.statsUsers.length != 0) {
      if (this.state.showUserBlock == 'none') {
        this.setState({
          showUsers: true,
          showUserBlock: 'block',
          customStyle: '#c0b1de'
        });
      } else if (this.state.showUserBlock == 'block') {
        this.setState({
          showUsers: false,
          showUserBlock: 'none',
          customStyle: ''
        });
      }
    }
  }

  fetchMore() {
    let stat = this.props.statsObject
      .replace('s_count', '')
      .replace('complete', 'mark_as_complete');
    this.setState(
      {
        offset: this.state.offset + 10
      },
      () => {
        if (this.state.moreUsers) {
          fetchCardStatsUsers(this.props.cardId, stat, this.state.offset)
            .then(statsUsers => {
              this.setState({
                moreUsers: statsUsers.length != 0,
                statsUsers: this.state.statsUsers.concat(statsUsers)
              });
            })
            .catch(err => {
              console.error(`Error in StatsListItem.fetchMore.fetchCardStatsUsers.func : ${err}`);
            });
        }
      }
    );
  }

  render() {
    let statsType =
      this.props.statsObject
        .replace('_count', '')
        .charAt(0)
        .toUpperCase() + this.props.statsObject.replace('_count', '').slice(1);
    let isShowItem = this.props.isViewsModal ? this.props.statsObject === 'views_count' : true;
    let currentValue =
      this.state.statsUsers.length < 10 && statsType != 'Views'
        ? this.state.statsUsers.length
        : this.props.value;
    return (
      <div
        className={this.props.statsObject}
        style={{ display: 'block', maxHeight: '200px', overflow: 'auto' }}
        onScroll={this.fetchMore}
      >
        {isShowItem && (
          <table style={{ border: 'none', width: '100%' }}>
            <tbody>
              <tr>
                <th style={{ width: '150px', height: '30px' }}>
                  <div
                    className={this.state.statsUsers.length != 0 && statsType != 'Views' && 'stats'}
                    onClick={statsType != 'Views' ? this.showUsers : ''}
                    style={{ color: this.state.customStyle, fontWeight: '400', maxWidth: '25%' }}
                  >
                    <div style={{ display: 'inline-block', float: 'left' }}>
                      {tr(statsType.replace('_', ' '))}
                    </div>
                    <div style={{ display: 'inline-block', float: 'right' }}>{currentValue}</div>
                  </div>
                </th>
              </tr>
              {this.state.showUsers &&
                this.state.statsUsers.map((user, index) => (
                  <tr style={{ borderBottom: 'solid 1px #f0f0f5' }} key={index}>
                    <td style={{ display: this.state.showUserBlock }}>
                      <div
                        className="user"
                        onTouchTap={() => {
                          const { currentUserHandle } = this.props;
                          this.props.dispatch(
                            push(
                              `/${
                                user.handle.replace('@', '') === currentUserHandle
                                  ? 'me'
                                  : user.handle
                              }`
                            )
                          );
                        }}
                      >
                        <table>
                          <tbody>
                            <tr>
                              <td>
                                <Avatar
                                  size={30}
                                  user={user}
                                  style={{ display: 'inline-block', marginRight: '10px' }}
                                />
                              </td>
                              <td>
                                <div style={{ display: 'inline-block', marginBottom: '10px' }}>
                                  <small>
                                    {user.firstName} {user.lastName}
                                  </small>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}
StatsListItem.propTypes = {
  statsObject: PropTypes.string,
  value: PropTypes.number,
  isViewsModal: PropTypes.bool,
  cardId: PropTypes.string,
  currentUserHandle: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUserHandle: state.currentUser.get('handle')
  };
}

export default connect(mapStoreStateToProps)(StatsListItem);
