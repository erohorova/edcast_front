import React from 'react';
import PropTypes from 'prop-types';

function ChannelLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card channel-loader animated-background">
          <div className="background-masker card-block channel-block">
            <div className="animated-background" style={{ height: '100%' }}>
              <div className="channel-name-loader">
                <div className="channel-name-loader-right" />
                <div className="channel-name-loader-left" />
              </div>
              <div className="background-masker channel-line" />
              <div className="channel-button-loader">
                <div className="channel-button-loader-right" />
                <div className="channel-button-loader-left" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

ChannelLoader.propTypes = {
  loadingCards: PropTypes.any
};

export default ChannelLoader;
