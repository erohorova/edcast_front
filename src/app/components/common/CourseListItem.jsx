import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import { tr } from 'edc-web-sdk/helpers/translations';

class Course extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let description = this.props.course.description || '';
    description = description.replace(/<\/?[^>]+(>|$)/g, '');
    return (
      <TableRow key={1} className="table-row" displayBorder={false}>
        <TableRowColumn style={{ width: '10.5rem' }}>
          <div
            className="container-padding banner"
            style={{
              height: '6rem',
              backgroundImage: `url(\'${this.props.course.logoUrl}\')`,
              backgroundRepeat: 'no-repeat',
              backgroundPosition: 'center',
              backgroundSize: 'cover',
              margin: '1rem'
            }}
          />
        </TableRowColumn>
        <TableRowColumn
          className="container-padding"
          style={{ verticalAlign: 'top', whiteSpace: 'normal' }}
        >
          <div className="text-overflow">
            <a className="matte" href={`${this.props.course.url}`} target="_blank">
              <strong>{this.props.course.name}</strong>
            </a>
            {this.props.course.isOwner && (
              <div className="owner-label text-center">
                <small className="label-text">{tr('Owner')}</small>
              </div>
            )}
            <div className="text-overflow">
              <small dangerouslySetInnerHTML={{ __html: description }} />
            </div>
          </div>
        </TableRowColumn>
        <TableRowColumn
          className="container-padding"
          style={{ width: '7rem', paddingRight: '1rem' }}
        >
          <div className="text-right">
            <small className="enrolled-count">
              {`${this.props.course.studentsCount || 0}`} {tr('Enrolled')}
            </small>
          </div>
        </TableRowColumn>
      </TableRow>
    );
  }
}

Course.propTypes = {
  course: PropTypes.object
};

export default connect()(Course);
