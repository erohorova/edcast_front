import React from 'react';
import PropTypes from 'prop-types';

function CardLoader(props) {
  let loadingCards = [];
  for (let i = 0; i < props.loadingCards; i++) {
    loadingCards.push(
      <div key={i} style={{ padding: '3px', width: '100%' }}>
        <div className="search-loader-card animated-background">
          <div className="background-masker card-block">
            <div className="animated-background" style={{ height: '100%' }}>
              <div className="background-masker avatar-wrapper">
                <div className="avatar" />
              </div>
              <div className="background-masker after-name" />
              <div className="background-masker card-line-1" />
              <div className="background-masker card-time" />
              <div className="background-masker card-line-2" />
              <div className="background-masker card-line-3" />
              <div className="background-masker card-line-4" />
              <div className="background-masker card-line-5" />
              <div className="background-masker card-line-6" />
              <div className="background-masker card-line-7" />
              <div className="background-masker card-line-8" />
              <div className="background-masker after-card-icon-1" />
              <div className="background-masker after-card-icon-2" />
            </div>
          </div>
        </div>
      </div>
    );
  }

  return <div style={{ display: 'flex', width: '100%' }}>{loadingCards}</div>;
}

CardLoader.propTypes = {
  loadingCards: PropTypes.any
};

export default CardLoader;
