import React, { Component } from 'react';
import PropTypes from 'prop-types';
import colors from 'edc-web-sdk/components/colors/index';
import Chip from 'material-ui/Chip';
import MenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux';
import SelectField from 'material-ui/SelectField';
import find from 'lodash/find';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

class ChannelPostComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      selectedChannels: props.currentChannels != undefined ? props.currentChannels : [],
      pathname: props.pathname.split('/')
    };
    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        margin: '0 -4px'
      },
      chip: {
        margin: '1rem .25rem .25rem .25rem'
      },
      channelSelect: {
        verticalAlign: 'bottom'
      }
    };
  }

  componentDidMount() {
    if (
      this.props.currentUser.writableChannels &&
      this.state.pathname[1] &&
      this.state.pathname[1] == 'channel' &&
      this.state.pathname[2]
    ) {
      let channelIdentify = this.state.pathname[this.state.pathname.length - 1];
      let findChannel = find(this.props.currentUser.writableChannels, el => {
        return el.slug == channelIdentify || el.id == channelIdentify;
      });
      if (findChannel) {
        this.handleChannelSelect(findChannel.id);
      }
    }
  }

  handleChannelDelete = removeId => {
    let newSelectedChannelIds = this.state.selectedChannels.filter(id => {
      return id !== removeId;
    });
    this.props.onChange();
    this.setState({
      selectedChannels: newSelectedChannelIds
    });
    if (this.props.onChange) {
      this.props.onChange(newSelectedChannelIds);
    }
  };

  handleChannelSelect = id => {
    let newSelectedChannelIds = uniq(this.state.selectedChannels.concat(id));
    this.setState({ selectedChannels: newSelectedChannelIds });
    if (this.props.onChange) {
      this.props.onChange(newSelectedChannelIds);
    }
  };

  render() {
    let writableChannels = [];
    if (this.props.currentUser.writableChannels) {
      writableChannels = this.props.currentUser.writableChannels.filter(channel => {
        return this.state.selectedChannels.indexOf(channel.id) === -1;
      });
    }
    return (
      <span style={this.styles.channelSelect}>
        <div style={this.styles.chipsWrapper}>
          {this.state.selectedChannels.map((id, idx) => {
            let channelLabel = '';
            this.props.currentUser.writableChannels.some(channel => {
              if (channel.id === id) {
                channelLabel = channel.label;
                return true;
              }
              return false;
            });
            return (
              <Chip
                key={idx}
                style={this.styles.chip}
                className="channel-item-insight"
                backgroundColor={colors.primary}
                labelColor={colors.white}
                labelStyle={{ width: 'calc(100% - 20px)' }}
                onRequestDelete={this.handleChannelDelete.bind(this, id)}
              >
                {channelLabel}
              </Chip>
            );
          })}
        </div>
        <SelectField
          style={{ verticalAlign: 'top' }}
          hintText={tr('Post to channel')}
          aria-label={tr('Post to channel')}
        >
          {writableChannels.map(channel => {
            return (
              <MenuItem
                key={channel.id}
                value={channel}
                onTouchTap={this.handleChannelSelect.bind(this, channel.id)}
                className="menu-item-custom postToChannel"
                style={{ width: '256px' }}
                primaryText={channel.label}
              />
            );
          })}
          {!writableChannels.length && (
            <MenuItem
              key="empty"
              value=""
              disabled={true}
              className="menu-item-empty"
              primaryText={tr(
                'You are not currently following any Channels that allow you to add content.'
              )}
            />
          )}
        </SelectField>
      </span>
    );
  }
}

ChannelPostComponent.propTypes = {
  currentUser: PropTypes.object,
  currentChannels: PropTypes.object,
  pathname: PropTypes.string,
  onChange: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(ChannelPostComponent);
