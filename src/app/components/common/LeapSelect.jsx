import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

export default class LeapSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.initValue
    };
  }

  render() {
    return (
      <select
        className="card-number-dropdown small-8"
        onChange={e => {
          this.setState({ value: e.target.value });
          this.props.onSelectChange(e.target.value);
        }}
        value={this.state.value}
      >
        <option className="placeholder" selected disabled>
          {tr('Choose the card for leap')}
        </option>
        {this.props.selectOptions.map((obj, index) => {
          if (!obj.isHidden)
            return (
              <option
                className="leap-option"
                key={index}
                value={obj[this.props.optionValue]}
              >{`#${index + 1}`}</option>
            );
        })}
      </select>
    );
  }
}

LeapSelect.propTypes = {
  onSelectChange: PropTypes.func,
  selectOptions: PropTypes.array,
  optionValue: PropTypes.string,
  initValue: PropTypes.any
};

LeapSelect.defaultProps = {
  optionValue: 'id'
};
