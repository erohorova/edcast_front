import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import debounce from 'lodash/debounce';

import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';

class SearchWithSuggestions extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isShowSuggestions: false,
      focusedItem: '',
      nextItem: '',
      chosenIndex: null
    };
    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.25rem',
        verticalAlign: 'middle',
        width: '1.2rem',
        height: '1.5rem',
        fill: colors.silverSand
      },
      searchBox: {
        width: '17.5rem'
      }
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = e => {
    if (this.searchBoxList.contains(e.target)) {
      return;
    } else {
      this.hideSuggestion();
    }
  };

  hideSuggestion = () => {
    this.setState(() => ({ isShowSuggestions: false }));
  };

  hideSuggestionsContainer = e => {
    this.hideSuggestion();
  };

  chooseSuggestion = item => {
    this._inputFilter.value = item && item.text;
    if (this.props.suggestionList.length > 0) {
      this.hideSuggestion();
      this.props.showFilterResult(this._inputFilter.value);
    }
  };

  getSuggestions = debounce(
    () => {
      this.props.getSuggestList(this._inputFilter.value);
    },
    800,
    { leading: false, trailing: true }
  );

  handleFilterSearch = e => {
    e.persist();
    let displaySuggestions =
      this.state.isShowSuggestions &&
      this.props.suggestionList &&
      !!this.props.suggestionList.length;
    let chosenIndex;
    const isSuggestedList = this.props.suggestionList && this.props.suggestionList.length > 0;
    if (e.keyCode === 13) {
      this._inputFilter.value =
        (this.state.chosenIndex !== null &&
          this.props.suggestionList[this.state.chosenIndex].text) ||
        this._inputFilter.value;
      this.hideSuggestion();
      this.setState(() => ({
        chosenIndex: null,
        focusedItem: ''
      }));
      this.props.showFilterResult(this._inputFilter.value);
    } else if (isSuggestedList && e.keyCode === 40 && displaySuggestions) {
      if (!this.state.focusedItem) {
        chosenIndex = 0;
      } else {
        chosenIndex =
          this.props.suggestionList.length - 1 > this.state.chosenIndex
            ? this.state.chosenIndex + 1
            : 0;
      }
      this.setState({
        focusedItem: `suggestion-${chosenIndex}`,
        chosenIndex
      });
    } else if (isSuggestedList && e.keyCode === 38 && displaySuggestions) {
      if (!this.state.focusedItem) {
        chosenIndex = this.props.suggestionList.length - 1;
      } else {
        chosenIndex =
          this.state.chosenIndex > 0
            ? this.state.chosenIndex - 1
            : this.props.suggestionList.length - 1;
      }
      this.setState({
        focusedItem: `suggestion-${chosenIndex}`,
        chosenIndex
      });
    } else {
      this.setState({
        isShowSuggestions: true
      });
    }
  };

  render() {
    return (
      <div
        className="my-team__search-input-block search-with-suggestions"
        style={this.styles.searchBox}
        ref={instance => (this.searchBoxList = instance)}
      >
        <input
          placeholder={tr('Search')}
          onFocus={() => this.setState({ isShowSuggestions: true })}
          className="search-channels-input"
          onChange={this.getSuggestions}
          type="text"
          ref={node => (this._inputFilter = node)}
          defaultValue={this.props.currentSearchValue}
          onKeyDown={this.handleFilterSearch}
        />
        <SearchIcon style={this.styles.searchIcon} onClick={this.getSuggestions} />
        {this.state.isShowSuggestions &&
          this.props.suggestionList &&
          !!this.props.suggestionList.length && (
            <div className="channels-suggestions-container">
              <ul>
                {this.props.suggestionList &&
                  this.props.suggestionList.slice(0, 10).map((suggestion, index) => {
                    let background =
                      this.state.focusedItem === `suggestion-${index}` ? '#eeeeee' : '';
                    return (
                      <li
                        key={suggestion.id}
                        id={`suggestion-${index}`}
                        className="suggestion-item suggestion-list-item"
                        onClick={this.chooseSuggestion.bind(this, suggestion)}
                        style={{ background: background }}
                        onMouseEnter={() => {
                          this.setState({ focusedItem: '', nextItem: '' });
                        }}
                      >
                        {suggestion.text.length > 50
                          ? `${suggestion.text.slice(0, 50)}...`
                          : suggestion.text}
                      </li>
                    );
                  })}
              </ul>
            </div>
          )}
      </div>
    );
  }
}

SearchWithSuggestions.propTypes = {
  suggestionList: PropTypes.any,
  getSuggestList: PropTypes.func,
  currentSearchValue: PropTypes.any,
  showFilterResult: PropTypes.func
};

export default connect()(SearchWithSuggestions);
