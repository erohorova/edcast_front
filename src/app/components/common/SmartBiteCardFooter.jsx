import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactStars from 'react-stars';
import InsightDropDownActions from '../../components/feed/InsightDropDownActions';
import SmartBiteInfoCommentsBlock from './SmartBiteInfoCommentsBlock';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import find from 'lodash/find';
import { connect } from 'react-redux';
let LocaleCurrency = require('locale-currency');
import TooltipLabel from '../common/TooltipLabel';
import { Permissions } from '../../utils/checkPermissions';

class SmartBiteCardFooter extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      actionButtonStyle: {
        verticalAlign: 'middle',
        width: 'auto',
        height: '19px',
        padding: 0
      },
      tooltipActiveBts: {
        marginTop: -20
      },
      iconStyle: {
        height: 19,
        width: 19
      },
      lockLeapIcon: {
        color: '#fff',
        width: '30px',
        height: '30px'
      },
      leapIcon: {
        marginRight: '7px'
      },
      insightDropDownActions: {
        paddingRight: '5px',
        paddingLeft: 0,
        width: 'auto'
      },
      moreIconStyle: {
        height: '0.875rem',
        top: '0.25rem',
        position: 'relative'
      }
    };
  }

  renderRatingBlock() {
    return (
      <div className="rating-container">
        <ReactStars
          edit={false}
          count={5}
          size={11}
          half={false}
          color1={'#d6d6e1'}
          color2={'#6f708b'}
          className="relevancyRatingStars"
          value={this.props.averageRating ? parseInt(this.props.averageRating) : 0}
        />
        <span>({this.props.ratingCount > 100 ? '99+' : +this.props.ratingCount})</span>
      </div>
    );
  }

  getPricingPlans() {
    return this.props.edcastPlansForPricing &&
      this.props.card.cardMetadatum &&
      this.props.card.cardMetadatum.plan
      ? tr(startCase(toLower(this.props.card.cardMetadatum.plan)))
      : tr('Free');
  }

  getPricingPlansForPathwayAndJourney() {
    return this.props.card.cardMetadatum && this.props.card.cardMetadatum.plan
      ? this.props.card.cardMetadatum.plan
      : 'Free';
  }

  render() {
    let { isPartOfPathway } = this.props;
    let card = this.props.card;
    let countryCode = this.props.currentUser.countryCode || 'us';
    let currency = LocaleCurrency.getCurrency(countryCode);
    let priceData = {};
    let skillcoin_image = '/i/images/skillcoin-new.png';
    if (card.prices && card.prices.length > 0) {
      priceData =
        find(card.prices, { currency: 'SKILLCOIN' }) ||
        find(card.prices, { currency: currency }) ||
        find(card.prices, { currency: 'USD' }) ||
        find(card.prices, function(price) {
          return price.currency != 'SKILLCOIN';
        });
    }

    if (priceData && priceData.currency == 'SKILLCOIN') {
      priceData.symbol = <img className="pricing_skill_coins_label_icon" src={skillcoin_image} />;
    }

    let pricingPlanForPathwayAndJourney = this.getPricingPlansForPathwayAndJourney();
    let tooltipText = '';
    if (pricingPlanForPathwayAndJourney == 'free/paid') {
      tooltipText = 'The pathway contains one or </br> more paid content';
    }

    return (
      !this.props.isCsodCourse && (
        <div className="my-card__footer">
          <div className="my-card__footer-first-row">
            {this.props.card.eclDurationMetadata &&
              this.props.card.eclDurationMetadata.calculated_duration_display &&
              this.props.card.eclDurationMetadata.calculated_duration > 0 && (
                <span className="read-time-ago__component">
                  <span className="read-time-ago">
                    {tr('Duration')}:{' '}
                    {this.props.card.eclDurationMetadata.calculated_duration_display}
                  </span>
                  {this.props.type !== 'Tile' && (
                    <span className="my-card__small-vertical-line">|</span>
                  )}
                </span>
              )}
            {this.props.edcastPricing &&
              this.props.card.readableCardType != 'jobs' &&
              this.props.card.cardType != 'pack' &&
              this.props.card.cardType != 'journey' && (
                <span className={`${this.props.type === 'Tile' && 'pricing_tile'} pricing`}>
                  {tr('Price')} :{' '}
                  {this.props.card.isPaid && priceData && priceData.amount ? (
                    <span>
                      {priceData.symbol}
                      {priceData.amount}
                    </span>
                  ) : (
                    this.getPricingPlans()
                  )}
                </span>
              )}
            {this.props.edcastPricing &&
              this.props.card.readableCardType != 'jobs' &&
              (this.props.card.cardType == 'pack' || this.props.card.cardType == 'journey') &&
              tooltipText && (
                <TooltipLabel text={tr(tooltipText)} html={true} position={'bottom'}>
                  <span className={`${this.props.type === 'Tile' && 'pricing_tile'} pricing `}>
                    {tr('Price')} :{' '}
                    <span className="pathway-paid-label">
                      {tr(pricingPlanForPathwayAndJourney)}
                    </span>
                  </span>
                </TooltipLabel>
              )}
            {this.props.edcastPricing &&
              this.props.card.readableCardType != 'jobs' &&
              (this.props.card.cardType == 'pack' || this.props.card.cardType == 'journey') &&
              !tooltipText && (
                <span className={`${this.props.type === 'Tile' && 'pricing_tile'} pricing `}>
                  {tr('Price')} :{' '}
                  <span className="pathway-paid-label">{tr(pricingPlanForPathwayAndJourney)}</span>
                </span>
              )}
            {Permissions.has('CAN_RATE') && this.props.type !== 'Tile' && this.renderRatingBlock()}
          </div>
          <div
            className="actions-bar__bottom-row"
            style={
              !(Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT'))
                ? { float: 'right' }
                : {}
            }
          >
            {!this.props.commentDisabled && !this.props.providerCards && (
              <SmartBiteInfoCommentsBlock
                handleCardClicked={this.props.clickOnComments || this.props.handleCardClicked}
                comments={this.props.comments}
                isLiveStream={this.props.isLiveStream}
                commentsCount={this.props.commentsCount}
              />
            )}
            <div style={{ display: 'inherit' }}>
              {Permissions.has('CAN_RATE') &&
                this.props.type === 'Tile' &&
                this.renderRatingBlock()}
              {!this.props.hideActions && !this.props.previewMode && (
                <InsightDropDownActions
                  isPartOfPathway={isPartOfPathway}
                  card={this.props.card}
                  author={this.props.card.author}
                  disableTopics={this.props.disableTopics}
                  dismissible={this.props.dismissible}
                  showTopicToggleClick={this.props.showTopicToggleClick}
                  style={this.styles.insightDropDownActions}
                  isStandalone={this.props.isStandalone}
                  cardUpdated={this.props.cardUpdated.bind(this)}
                  openReasonReportModal={this.props.openReasonReportModal}
                  removeCardFromList={this.props.removeCardFromList}
                  iconStyle={this.styles.moreIconStyle}
                  hideComplete={this.props.hideComplete}
                  isCompleted={
                    this.props.card.completionState &&
                    this.props.card.completionState.toUpperCase() === 'COMPLETED'
                  }
                  deleteSharedCard={this.props.deleteSharedCard}
                  channel={this.props.channel}
                  type={this.props.type}
                  isShowLeap={
                    this.props.params.isOwner &&
                    this.props.lockPathwayCardFlag &&
                    this.props.isShowLeap
                  }
                  toggleHandleLeapModal={this.props.toggleHandleLeapModal}
                  cardSectionName={this.props.cardSectionName}
                  removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                  removeCardFromCarousel={this.props.removeCardFromCarousel}
                  removeDismissAssessmentFromList={this.props.removeDismissAssessmentFromList}
                />
              )}
            </div>
          </div>
        </div>
      )
    );
  }
}

SmartBiteCardFooter.propTypes = {
  edcastPricing: PropTypes.any,
  edcastPlansForPricing: PropTypes.any,
  isCsodCourse: PropTypes.any,
  card: PropTypes.object,
  priceData: PropTypes.any,
  isShowLeap: PropTypes.any,
  lockPathwayCardFlag: PropTypes.any,
  toggleHandleLeapModal: PropTypes.any,
  previewMode: PropTypes.any,
  hideActions: PropTypes.any,
  ratingCount: PropTypes.any,
  disableTopics: PropTypes.any,
  isLiveStream: PropTypes.bool,
  averageRating: PropTypes.any,
  channel: PropTypes.any,
  comments: PropTypes.any,
  isCompleted: PropTypes.any,
  deleteSharedCard: PropTypes.any,
  providerCards: PropTypes.any,
  hideComplete: PropTypes.func,
  isStandalone: PropTypes.bool,
  dismissible: PropTypes.bool,
  commentDisabled: PropTypes.bool,
  commentsCount: PropTypes.number,
  handleCardClicked: PropTypes.func,
  cardUpdated: PropTypes.func,
  removeCardFromList: PropTypes.func,
  showTopicToggleClick: PropTypes.func,
  openReasonReportModal: PropTypes.func,
  clickOnComments: PropTypes.func,
  params: PropTypes.object,
  currentUser: PropTypes.object,
  type: PropTypes.string,
  isPartOfPathway: PropTypes.bool,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  removeCardFromCarousel: PropTypes.func,
  removeDismissAssessmentFromList: PropTypes.func
};

SmartBiteCardFooter.defaultProps = {
  isPartOfPathway: false,
  currentUser: PropTypes.object,
  tooltipPosition: 'bottom'
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(SmartBiteCardFooter);
