import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListItem from 'material-ui/List/ListItem';
import { push } from 'react-router-redux';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import Avatar from 'material-ui/Avatar';
import formatNumbers from '../../utils/formatNumbers';
import { toggleFollow } from '../../actions/usersActions';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import { Permissions } from '../../utils/checkPermissions';
import { tr } from 'edc-web-sdk/helpers/translations';

class LeaderBoard extends Component {
  constructor(props, context) {
    super(props, context);

    this.userClickHandler = this.userClickHandler.bind(this);
    this.followClickHandler = this.followClickHandler.bind(this);
  }

  userClickHandler = () => {
    this.props.dispatch(push('/' + this.props.notification.user.handle));
  };

  followClickHandler(user) {
    this.props.dispatch(toggleFollow(user.id, !user.following));
  }

  render() {
    let user = this.props.user;
    let member = this.props.member;
    let users = this.props.users;
    if (user && users['idMap'][user.id]['following'] == true) {
      user.following = true;
    } else if (user && users['idMap'][user.id]['following'] == false) {
      user.following = false;
    }
    return (
      <TableRow className="table-row" displayBorder={false}>
        <TableRowColumn style={{ width: '5rem' }}>
          <ListItem disabled primaryText={this.props.index + 1 + '.'} />
        </TableRowColumn>
        <TableRowColumn className="container-padding">
          <ListItem
            disabled
            primaryText={
              <div className="text-overflow">
                <a className="matte" href={`/${user && user.handle}`}>
                  {user && user.name}
                </a>
              </div>
            }
            secondaryText={user && user.handle}
            leftAvatar={<Avatar src={user && user.picture} />}
          />
        </TableRowColumn>
        <TableRowColumn style={{ width: '7rem' }} className="container-padding">
          <div>
            {tr('Score')}
            <small> {formatNumbers(member.smartbitesScore)}</small>
          </div>
        </TableRowColumn>
        <TableRowColumn style={{ width: '9rem' }} className="container-padding">
          {user &&
            this.props.currentUser.id != user.id &&
            !Permissions.has('DISABLE_USER_FOLLOW') && (
              <FollowButton
                following={user && user.following}
                pending={user && user.followPending}
                label={tr(user && user.following ? 'Following' : 'Follow')}
                hoverLabel={tr(user && user.following ? 'Unfollow' : '')}
                pendingLabel={tr(user && user.following ? 'Unfollowing...' : 'Following...')}
                className="follow"
                onTouchTap={this.followClickHandler.bind(this, user)}
              />
            )}
        </TableRowColumn>
      </TableRow>
    );
  }
}

LeaderBoard.propTypes = {
  user: PropTypes.object,
  users: PropTypes.object,
  notification: PropTypes.object,
  index: PropTypes.number,
  currentUser: PropTypes.object,
  member: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  users: state.users.toJS()
}))(LeaderBoard);
