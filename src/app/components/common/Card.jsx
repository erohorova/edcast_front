import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import LikeIcon from 'edc-web-sdk/components/icons/Like';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';
import BookmarkAsset from 'edc-web-sdk/components/icons/Bookmark';
import CompletedAssignment from 'edc-web-sdk/components/icons/CompletedAssignmentGrey';
import { RadioButton } from 'material-ui/RadioButton';
import { Permissions } from '../../utils/checkPermissions';
import { cards } from 'edc-web-sdk/requests/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import * as logoType from '../../constants/logoTypes';
import * as upshotActions from '../../actions/upshotActions';

const logoObj = logoType.LOGO;

class DiscoverCard extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      isUpvoted: props.card.isUpvoted,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      custom: props.custom
    };
    this.styles = {
      cardWrapper: {
        height: '280px',
        padding: '8px',
        position: 'relative'
      },
      likeIcon: {
        verticalAlign: 'middle',
        paddingLeft: 0,
        padding: 0,
        marginTop: '5px',
        width: '24px',
        height: '24px'
      },
      bookmarkIcon: {
        padding: 0,
        marginTop: '5px',
        width: '24px',
        height: '24px'
      },
      markAsCompleteIcon: {
        padding: 0,
        marginTop: '5px',
        height: '24px',
        width: '24px',
        marginLeft: '10px'
      },
      svgStyle: {
        filter: 'url(#blur-effect-1)'
      },
      cardImgContainerSvg: {
        zIndex: 2,
        position: 'relative'
      },
      cardImgContainerImg: {
        zIndex: 2
      }
    };
  }

  cardLikeHandler = () => {
    let card = this.props.card;
    if (this.state.isUpvoted) {
      this.setState({ isUpvoted: false });
      cards
        .dislikeCard(card.id, card.card_type)
        .then(() => {
          if (this.state.upshotEnabled) {
            let name = (card.resource && card.resource.title) || card.message;
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: card.resource.title,
              category: card.cardType,
              type: card.cardSubtype,
              description: card.resource.description,
              status: !!card.completionState ? card.completionState : 'Incomplete',
              rating: card.averageRating,
              like: 'No',
              event: 'Clicked Unlike'
            });
          }
        })
        .catch(err => {
          console.error(`Error in Card.dislikeCard.func : ${err}`);
        });
    } else {
      this.setState({ isUpvoted: true });
      cards
        .likeCard(card.id, card.card_type)
        .then(() => {
          if (this.state.upshotEnabled) {
            let name = (card.resource && card.resource.title) || card.message;
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
              name: name,
              category: card.cardType,
              type: card.cardSubtype,
              description: card.resource.description,
              status: !!card.completionState ? card.completionState : 'Incomplete',
              rating: card.averageRating,
              like: 'Yes',
              event: 'Clicked Like'
            });
          }
        })
        .catch(err => {
          console.error(`Error in Card.likeCard.func : ${err}`);
        });
    }
  };

  bookmarkClickHandler = () => {
    cards
      .bookmark(this.props.card.id)
      .then(() => {
        this.props.removeCardFromList(this.props.card.id);
      })
      .catch(err => {
        console.error(`Error in Card.bookmark.func : ${err}`);
      });
  };

  completeClickHandler = () => {
    cards
      .markCardAsComplete(this.props.card.id, undefined, 'Card')
      .then(() => {
        this.props.removeCardFromList(this.props.card.id);
        if (this.state.upshotEnabled) {
          let name =
            (this.props.card.resource && this.props.card.resource.title) || this.props.card.message;
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['SMARTCARD'], {
            name: name,
            category: this.props.card.cardType,
            type: this.props.card.cardSubtype,
            description: this.props.card.resource.description,
            status: 'COMPLETED',
            rating: this.props.card.averageRating,
            like: this.state.isUpvoted ? 'Yes' : 'No',
            event: 'Mark Completed'
          });
        }
      })
      .catch(err => {
        console.error(`Error in Card.markCardAsComplete.func : ${err}`);
      });
  };

  getCardTitle = () => {
    let card = this.props.card;
    if (this.props.custom && (card.cardType == 'media' || card.cardType == 'poll')) {
      return card.title;
    }

    if (this.props.carouselCard) {
      return card.message;
    }
    if (
      (card.cardType == 'media' &&
        card.cardSubtype == 'link' &&
        card.title == null &&
        card.resource) ||
      (card.cardType == 'poll' && card.cardSubtype == 'link' && card.resource) ||
      (card.cardType == 'media' && card.cardSubtype == 'video' && card.resource)
    ) {
      return card.resource.title;
    } else if (
      card.cardType == 'poll' &&
      (card.cardSubtype == 'text' || card.cardSubtype == 'video' || card.cardSubtype == 'image')
    ) {
      return card.message; // Pass message in place of title for better UI
    } else {
      return card.title;
    }
  };

  getCardMessage = () => {
    let card = this.props.card;
    if (this.props.carouselCard) {
      return card.message;
    } else if (
      (card.cardType == 'media' && card.cardSubtype == 'link' && card.resource) ||
      (card.cardType == 'poll' && card.cardSubtype == 'link' && card.resource) ||
      (card.cardType == 'media' && card.cardSubtype == 'video' && card.resource)
    ) {
      return card.resource.description;
    } else if (
      card.cardType == 'poll' &&
      (card.cardSubtype == 'text' || card.cardSubtype == 'video' || card.cardSubtype == 'image')
    ) {
      return ''; // Pass message in place of title for better UI
    } else {
      return card.message;
    }
  };

  getCardImageUrl = () => {
    let card = this.props.card;
    if (this.props.custom) {
      if (
        !!card.filestack.length &&
        card.filestack[0].mimetype &&
        card.filestack[0].mimetype.indexOf('image/') !== -1
      ) {
        return card.filestack[0].url;
      } else if (card.resource) {
        return card.resource.imageUrl;
      } else if (card.videoStream) {
        return card.videoStream.imageUrl;
      } else if (
        card.cardType == 'pack' &&
        card.fileResources != undefined &&
        card.fileResources.length > 0
      ) {
        return card.fileResources[0].fileUrls.medium_url;
      }
    } else {
      if (this.props.carouselCard) {
        return (
          card.fileResources != undefined &&
          card.fileResources.length > 0 &&
          card.fileResources[0].fileUrls.medium_url
        );
      } else if (
        (card.cardType == 'media' && card.cardSubtype == 'link' && card.resource) ||
        (card.cardType == 'course' && card.resource) ||
        (card.cardType == 'media' && card.cardSubtype == 'video' && card.resource) ||
        (card.cardType == 'poll' && card.cardSubtype == 'video' && card.resource)
      ) {
        return card.resource.imageUrl;
      } else if (
        (card.cardType == 'media' || card.cardType == 'poll') &&
        card.cardSubtype == 'image' &&
        card.fileResources != undefined &&
        card.fileResources.length > 0
      ) {
        return card.fileResources[0].fileUrls.medium_url;
      } else {
        return null;
      }
    }
  };

  getCardUrl = () => {
    let card = this.props.card;
    if (this.props.carouselCard) {
      return card.resource && card.resource.url;
    } else {
      return `/insights/${this.props.card.slug}`;
    }
  };

  render() {
    let title = this.getCardTitle();
    let message = this.getCardMessage();
    let imageUrl = this.getCardImageUrl();
    let cardUrl = this.getCardUrl();
    let lineClamp =
      (this.props.card.cardType == 'poll' && this.props.card.cardSubtype == 'link') ||
      this.props.card.cardType == 'course'
        ? 'line-clamp-3'
        : 'line-clamp-6';
    return (
      <div className="common-card">
        <Paper style={this.styles.cardWrapper}>
          <div>
            <a href={cardUrl} target={this.props.carouselCard ? '_blank' : ''}>
              {imageUrl && (
                <div className="card-img-container">
                  <div className="card-image card-blurred-background">
                    <svg id="svg-image-blur" width="100%" height="100%">
                      <title>{title}</title>
                      <image
                        id="svg-image"
                        style={this.styles.svgStyle}
                        xlinkHref={imageUrl}
                        x="-30%"
                        y="-30%"
                        width="160%"
                        height="160%"
                      />
                      <filter id="blur-effect-1">
                        <feGaussianBlur stdDeviation="10" />
                      </filter>
                    </svg>
                  </div>
                  <svg width="100%" height="100%" style={this.styles.cardImgContainerSvg}>
                    <title>{title}</title>
                    <image
                      xlinkHref={imageUrl}
                      width="100%"
                      style={this.styles.cardImgContainerImg}
                      height="100%"
                    />
                  </svg>
                </div>
              )}
              <div className="info">
                {title && <div className={imageUrl ? 'title with-image' : 'title'}>{title}</div>}
                {!imageUrl && message && <div className={'message ' + lineClamp}>{message}</div>}

                {(this.props.card.eclSourceLogoUrl ||
                  (this.props.card.eclSourceTypeName &&
                    logoObj[this.props.card.eclSourceTypeName] !== undefined)) && (
                  <div className="text-right premium-pro-logo">
                    <img
                      src={
                        this.props.card.eclSourceLogoUrl ||
                        logoObj[this.props.card.eclSourceTypeName.toLowerCase()]
                      }
                    />
                  </div>
                )}
              </div>
            </a>
            {!this.props.carouselCard && (
              <div className="action-bar">
                <div className="float-left">
                  {Permissions['enabled'] !== undefined && Permissions.has('LIKE_CONTENT') && (
                    <IconButton
                      tooltip={tr(this.state.isUpvoted ? 'Unlike' : 'Like')}
                      tooltipPosition={'top-center'}
                      className="like"
                      style={this.styles.likeIcon}
                      onTouchTap={this.cardLikeHandler}
                    >
                      {!this.state.isUpvoted && <LikeIcon />}
                      {this.state.isUpvoted && <LikeIconSelected />}
                    </IconButton>
                  )}
                </div>
                <div className="float-right">
                  {Permissions['enabled'] !== undefined && Permissions.has('BOOKMARK_CONTENT') && (
                    <IconButton
                      tooltip={tr('Bookmark')}
                      tooltipPosition={'top-center'}
                      className="bookmark"
                      style={this.styles.bookmarkIcon}
                      onTouchTap={this.bookmarkClickHandler}
                    >
                      <BookmarkAsset color={colors.darkGray} />
                    </IconButton>
                  )}
                  <div style={{ width: '5px', display: 'inline-block' }} />
                  {Permissions['enabled'] !== undefined && Permissions.has('MARK_AS_COMPLETE') && (
                    <IconButton
                      tooltip={tr('Mark As Complete')}
                      tooltipPosition={'top-left'}
                      className="completed"
                      style={this.styles.markAsCompleteIcon}
                      onTouchTap={
                        this.state.isCompleted
                          ? () => {
                              return;
                            }
                          : this.completeClickHandler
                      }
                    >
                      <CompletedAssignment
                        color={this.props.card.isCompleted ? colors.primary : colors.darkGray}
                      />
                    </IconButton>
                  )}
                </div>
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

DiscoverCard.propTypes = {
  card: PropTypes.object,
  carouselCard: PropTypes.bool,
  custom: PropTypes.bool,
  removeCardFromList: PropTypes.func
};

export default DiscoverCard;
