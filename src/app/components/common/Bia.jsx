import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import { rateCard } from 'edc-web-sdk/requests/cards';

class Bia extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      allowTooltipShow: typeof props.allowTooltip !== 'undefined' ? props.allowTooltip : true,
      card: props.card
    };
    this.styles = {
      biaLine: {
        position: 'absolute',
        top: props.skillValue ? '3.125rem' : '0.125rem',
        right: props.skillValue ? '1.4375rem' : '0.5rem',
        fontSize: '0.625rem',
        textTransform: 'uppercase'
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.skillValue) {
      this.setState({
        card: nextProps.card
      });
    }
  }

  rateCard(level, e) {
    e.preventDefault();
    if (this.props.skillValue) {
      return;
    }
    rateCard(this.state.card.id, { level: level })
      .then(new_card => {
        this.cardUpdated(new_card);
      })
      .catch(err => {
        console.error(`Error in Bia.rateCard.func : ${err}`);
      });
  }

  stopAutoScroll(e) {
    e.preventDefault();
    return;
  }

  cardUpdated(new_card) {
    this.setState({ card: new_card });
  }

  render() {
    let { isClickable } = this.props;
    let dynamicStyles = {};

    let beginnerDynamicProps = {};
    let intermediateDynamicProps = {};
    let advancedDynamicProps = {};

    if (isClickable) {
      dynamicStyles['style'] = { cursor: 'pointer' };
      beginnerDynamicProps['onClick'] = this.rateCard.bind(this, 'beginner');
      intermediateDynamicProps['onClick'] = this.rateCard.bind(this, 'intermediate');
      advancedDynamicProps['onClick'] = this.rateCard.bind(this, 'advanced');
    } else {
      dynamicStyles['style'] = { cursor: 'default' };
      beginnerDynamicProps['onClick'] = this.stopAutoScroll.bind(this);
      intermediateDynamicProps['onClick'] = this.stopAutoScroll.bind(this);
      advancedDynamicProps['onClick'] = this.stopAutoScroll.bind(this);
    }

    return (
      <div>
        <div className="bia" style={this.props.isNotAbsolutePosition ? {} : this.styles.biaLine}>
          <div
            className={`${this.state.card.newSkillLevel || this.state.card.skillLevel} level-dots`}
          >
            <a href="#" tabIndex={0} {...beginnerDynamicProps} {...dynamicStyles}>
              <span className="first">
                {this.state.allowTooltipShow && <div className="level-tip">{tr('Beginner')}</div>}
              </span>
            </a>
            <a href="#" tabIndex={0} {...intermediateDynamicProps} {...dynamicStyles}>
              <span className="second">
                {this.state.allowTooltipShow && (
                  <div className="level-tip">{tr('Intermediate')}</div>
                )}
              </span>
            </a>
            <a href="#" tabIndex={0} {...advancedDynamicProps} {...dynamicStyles}>
              <span className="third">
                {this.state.allowTooltipShow && <div className="level-tip">{tr('Advanced')}</div>}
              </span>
            </a>
          </div>
          {this.state.card.skillLevel && (
            <div className="level">{tr(this.state.card.skillLevel)}</div>
          )}
        </div>
      </div>
    );
  }
}

Bia.propTypes = {
  card: PropTypes.object,
  skillValue: PropTypes.bool,
  isNotAbsolutePosition: PropTypes.bool,
  allowTooltip: PropTypes.bool,
  isClickable: PropTypes.bool
};

export default connect()(Bia);
