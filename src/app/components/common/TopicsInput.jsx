import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSuggestedTopicsForUser } from 'edc-web-sdk/requests/topics';
import AutoComplete from 'material-ui/AutoComplete';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors';
import EnterIcon from 'material-ui/svg-icons/hardware/keyboard-return';
import { tr } from 'edc-web-sdk/helpers/translations';

class TopicsInput extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      searchText: '',
      topicsSuggest: [],
      selectedTopics: props.defaultValue || []
    };
    this.styles = {
      placeholder: {
        width: '256px'
      }
    };
  }

  componentDidMount() {
    getSuggestedTopicsForUser().then(topicsSuggest => {
      this.setState({ topicsSuggest });
    });
  }

  inputChangeHandler = searchText => {
    this.props.updateUserPrefs(searchText);
    if (searchText[searchText.length - 1] === ',') {
      this.addTopicHandler(searchText.slice(0, searchText.length - 1));
      this.setState({ searchText: '' });
    } else {
      this.setState({ searchText, errorText: '' });
      if (this._searchTimer) {
        clearTimeout(this._searchTimer);
        this._searchTimer = null;
      } else {
        this._searchTimer = setTimeout(() => {
          getSuggestedTopicsForUser(searchText).then(topicsSuggest => {
            this.setState({ topicsSuggest });
          });
        }, 300);
      }
    }
  };

  addTopicHandler = selectedTopic => {
    let isEmpty = selectedTopic.trim() === '';
    if (isEmpty) {
      this.setState({ errorText: "Can't be blank." });
      return;
    }
    let isExist = this.state.selectedTopics.some(topic => {
      return topic.toLowerCase() === selectedTopic.toLowerCase();
    });
    if (isExist) {
      this.setState({ errorText: 'Topic already added.', searchText: '' });
      return;
    }
    let newTopics = this.state.selectedTopics.concat(selectedTopic);
    this.setState({ searchText: '', errorText: '', selectedTopics: newTopics });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newTopics);
    }
  };

  removeTopicHandler = removeIndex => {
    let newTopics = this.state.selectedTopics.filter((topic, index) => index !== removeIndex);
    this.setState({
      errorText: '',
      selectedTopics: newTopics
    });
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(newTopics);
    }
  };

  getTopics = () => {
    return this.state.selectedTopics;
  };

  render() {
    return (
      <div>
        <div className="topic-chips">
          {this.state.selectedTopics.map((topic, index) => {
            return (
              <Chip
                key={index}
                onRequestDelete={this.removeTopicHandler.bind(this, index)}
                style={{ margin: '4px 8px 4px 0' }}
                backgroundColor={colors.primary}
                labelColor={colors.white}
              >
                {topic}
              </Chip>
            );
          })}
        </div>
        <AutoComplete
          hintText={
            <div style={this.styles.placeholder}>{tr('e.g. leadership, finance, sales')}</div>
          }
          searchText={this.state.searchText}
          dataSource={this.state.topicsSuggest}
          onUpdateInput={this.inputChangeHandler}
          onNewRequest={this.addTopicHandler}
          errorText={tr(this.state.errorText)}
        />
      </div>
    );
  }
}

TopicsInput.propTypes = {
  onChange: PropTypes.func,
  defaultValue: PropTypes.array
};

export default connect()(TopicsInput);
