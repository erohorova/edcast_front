import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import { connect } from 'react-redux';

class UserRow extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isChecked: false
    };
  }

  componentDidMount() {
    this._checkboxSetup(this.props);
  }

  _checkboxSetup = compProps => {
    let card = compProps.card || {};
  };

  _toggleCheckbox = e => {
    let user = this.props.user || {};
    let selectedUser = this.props.selectedUser || null;

    let isChecked = e.target.checked;
    if (user.id) {
      isChecked
        ? this.props.selectUser(user.id, this.props.card, true)
        : this.props.selectUser(user.id, this.props.card, false);
    }
    this.setState({ isChecked });
  };

  render() {
    let user = this.props.user || {};
    let selectedUser = this.props.selectedUser || null;
    let checkboxLabel = userLabel(user);

    return (
      <div>
        <Checkbox
          label={checkboxLabel}
          checked={this.state.isChecked && user.id === selectedUser}
          onCheck={e => {
            this._toggleCheckbox(e);
          }}
          labelStyle={{ margin: '5px 0 5px 0', fontSize: '13px', fontWeight: 300 }}
          iconStyle={{ width: '16px', height: '16px', marginRight: '10px', marginTop: '6px' }}
        />
      </div>
    );
  }
}

let userLabel = user => {
  let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

  if (user.id) {
    return (
      <a style={{ color: '#454560' }} onClick={() => {}} className="checkbox-label">
        {user.avatarimages && user.avatarimages.tiny && (
          <img
            src={(user.avatarimages && user.avatarimages.tiny) || defaultUserImage}
            className="user-image"
          />
        )}
        <div className="user-name">{user.name || user.fullName || ''}</div>
        {user.handle && <span className="user-handle">({user.handle})</span>}
      </a>
    );
  }
};

UserRow.propTypes = {
  user: PropTypes.any,
  card: PropTypes.object,
  selectedUser: PropTypes.any,
  selectUser: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(UserRow);
