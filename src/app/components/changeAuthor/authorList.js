import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import UserRow from '../../components/changeAuthor/UserRow';
import Spinner from '../common/spinner';
import throttle from 'lodash/throttle';

class AuthorList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loadMore: true,
      card: {}
    };
    this.pending = false;
  }

  componentDidMount() {
    document.getElementById('sc-individual-list-content') &&
      document
        .getElementById('sc-individual-list-content')
        .addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document.getElementById('sc-individual-list-content') &&
      document
        .getElementById('sc-individual-list-content')
        .removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    e => {
      if (this.pending) return;

      let users =
        this.props.users || (this.props.shareContent && this.props.shareContent.users) || [];
      let totalUsers = this.props.totalUsers;
      let payload = { limit: 15, offset: users.length };

      if (
        totalUsers > users.length &&
        e.target.clientHeight + Math.ceil(e.target.scrollTop) >= e.target.scrollHeight
      ) {
        this.pending = true;
        this.setState({ loadMore: true });
        this.props.applyFilterForIndividual(users.length);
        this.setState({ loadMore: false });
        this.pending = false;
      } else {
        if (this.state.loadMore !== false) {
          this.setState({ loadMore: false });
        }
        this.pending = false;
      }
    },
    500,
    { leading: false }
  );

  render() {
    let users = this.props.users;
    let filterEnabled = this.props.filterEnabled;
    let selectedUser = this.props.selectedUser;
    let errorMsg = '';
    let loadMore = this.props.totalUsers > users.length ? this.state.loadMore : false;
    if (filterEnabled === true && !(users && users.length)) {
      errorMsg = 'No results found for your search';
    }
    return (
      <div>
        <div id="sc-individual-list-content">
          {!!users &&
            !!users.length &&
            users.map((user, index) => {
              if (this.props.currentUser.id != user.id) {
                return (
                  <UserRow
                    user={user}
                    selectedUser={selectedUser}
                    selectUser={this.props.selectUser}
                    key={user.id + '-' + index}
                    {...this.props}
                  />
                );
              }
            })}

          {((!users && !users.length) || loadMore || this.props.totalUsers > users.length) && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
          <p>{errorMsg ? errorMsg : ''}</p>
        </div>
      </div>
    );
  }
}

AuthorList.propTypes = {
  currentUser: PropTypes.any,
  shareContent: PropTypes.any,
  card: PropTypes.any,
  isAllUsers: PropTypes.bool,
  modal: PropTypes.object,
  applyFilterForIndividual: PropTypes.func,
  users: PropTypes.any,
  selectUser: PropTypes.func,
  selectedUser: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(AuthorList);
