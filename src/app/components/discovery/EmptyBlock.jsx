import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class EmptyBlock extends Component {
  constructor(props, context) {
    super(props, context);
  }

  linkClickHandler = () => {
    this.props.dispatch(push(this.props.link.url));
  };

  render() {
    return (
      <div className={`empty-block text-center ${this.props.className}`}>
        <div>
          {this.props.title !== undefined && (
            <a
              href="#"
              className="data-not-available-msg"
              onClick={e => {
                e.preventDefault();
              }}
            >
              {tr(this.props.title)}
            </a>
          )}
          {this.props.link !== undefined && (
            <div>
              {this.props.link.isPush && (
                <a onTouchTap={this.linkClickHandler}>{tr(this.props.link.text)}</a>
              )}
              {!this.props.link.isPush && (
                <a href={this.props.link.url}>{tr(this.props.link.text)}</a>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

EmptyBlock.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  link: PropTypes.shape({
    text: PropTypes.string,
    url: PropTypes.string,
    isPush: PropTypes.bool
  })
};

export default connect()(EmptyBlock);
