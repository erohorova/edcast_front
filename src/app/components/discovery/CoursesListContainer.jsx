import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { recommendedCourses } from '../../actions/coursesActions';
import CourseListItem from '../common/CourseListItem';
import ListView from '../common/ListView';
import Breadcrumb from '../common/Breadcrumb';
import uniq from 'lodash/uniq';
import filter from 'lodash/filter';
import throttle from 'lodash/throttle';
import { tr } from 'edc-web-sdk/helpers/translations';

class CoursesListContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      courses: [],
      filteredList: [],
      pending: false,
      isEnrolled: true,
      filterType: 'All Courses',
      inputValue: '',
      offset: 0,
      limit: 10,
      filteredText: '',
      isLastPage: false
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    this.loadCourses(this.state.limit, this.state.offset, this.state.isEnrolled);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  loadCourses(limit, offset, isEnrolled) {
    if (!this.state.pending) {
      this.setState({ pending: true });
      this.props
        .dispatch(recommendedCourses(limit, offset, isEnrolled))
        .then(courses => {
          this.setState({ offset: offset + 1, isLastPage: courses.length < limit });
          this.afterLoad(courses);
        })
        .catch(err => {
          console.error(`Error in CoursesListContainer.recommendedCourses.func : ${err}`);
        });
    }
  }

  afterLoad(courses) {
    if (this.state.filterType == 'All Courses') {
      this.setState({
        courses: uniq(this.state.courses.concat(courses), 'id'),
        filteredList: uniq(this.state.courses.concat(courses), 'id'),
        pending: false
      });
    } else {
      this.setState({ courses: uniq(this.state.courses.concat(courses), 'id'), pending: false });
      this.sortCourses(this.state.courses);
    }
  }

  sortCourses(courses) {
    let sorted;
    if (this.state.filterType === 'Sort by Name') {
      sorted = courses.sort((a, b) => a.name.localeCompare(b.name));
    } else if (this.state.filterType === 'Show only Completed') {
      sorted = filter(courses, function(el) {
        return el.competitionState && el.competitionState == 'COMPLETED';
      });
    } else if (this.state.filterType === 'Show only Not Completed') {
      sorted = filter(courses, function(el) {
        return !el.competitionState || el.competitionState != 'COMPLETED';
      });
    } else if (this.state.filterType === 'All Courses') {
      sorted = courses.sort((a, b) => {
        return b.id - a.id;
      });
    }
    this.setState({ filteredList: sorted });
  }

  handleScroll = throttle(
    event => {
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadCourses(this.state.limit, this.state.offset, this.state.isEnrolled);
        }
      }
    },
    150,
    { leading: false }
  );

  handleFilterChange = e => {
    this.setState({ filterType: e });
    this.sortCourses(this.state.courses);
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      this.setState({
        filteredList: [],
        courses: [],
        filteredText: e.target.value,
        offset: 0,
        pending: true
      });
      if (!e.target.value) {
        this.loadCourses(this.state.limit, this.state.offset, this.state.isEnrolled);
      } else {
        this.props
          .dispatch(recommendedCourses(null, null, null, e.target.value))
          .then(courses => {
            this.setState({ isLastPage: true });
            this.afterLoad(courses);
          })
          .catch(err => {
            console.error(
              `Error in CoursesListContainer.handleFilterSearch.recommendedCourses.func : ${err}`
            );
          });
      }
    }
  };

  render() {
    return (
      <div>
        <Breadcrumb
          levels={[
            { name: 'Discover', url: '/discover' },
            { name: 'Courses', url: '/discover/courses' }
          ]}
        />
        <div className="container-padding row content" id="courses">
          <div className="columns expand vertical-spacing-large">
            <div className="vertical-spacing-medium">
              <ListView
                isTable={true}
                pending={this.state.pending}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr('There are no courses available.')}
                  </div>
                }
              >
                {this.state.filteredList.length > 0 &&
                  this.state.filteredList.map((course, index) => {
                    return <CourseListItem key={index} course={course} />;
                  })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CoursesListContainer.propTypes = {};

export default connect(state => ({ courses: state.courses.toJS() }))(CoursesListContainer);
