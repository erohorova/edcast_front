import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getJourneys } from '../../actions/journeyActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import JourneyListItem from '../common/JourneyListItem';
import ListView from '../common/ListView';
import Breadcrumb from '../common/Breadcrumb';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

class JourneysListContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      journeys: [],
      filteredList: [],
      pending: false,
      offset: 0,
      limit: 20,
      filteredText: '',
      isLastPage: false,
      filterType: 'All Journeys',
      enableBtnSearch: true,
      isCardV3: window.ldclient.variation('card-v3', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount() {
    let userInfoCallBack = getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props
      .dispatch(userInfoCallBack)
      .then(() => {
        this.loadJourneys(this.state.limit, this.state.offset);
        window.addEventListener('scroll', this.handleScroll);
      })
      .catch(err => {
        console.error(`Error in JourneysListContainer.componentWillMount.func: ${err}`);
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadJourneys(this.state.limit, this.state.offset);
        }
      }
    },
    150,
    { leading: false }
  );

  loadJourneys(limit, offset) {
    if (!this.state.pending) {
      this.setState({ pending: true });
      let payload = {
        limit: limit,
        offset: offset,
        fields: 'id,filestack,title,message,slug,journey_packs_count,views_count,created_at'
      };
      this.props
        .dispatch(getJourneys(payload))
        .then(journeys => {
          this.setState({ offset: offset + limit, isLastPage: journeys.length < limit });
          this.afterLoad(journeys);
        })
        .catch(err => {
          console.error(`Error in JourneysListContainer.getJourneys.func : ${err}`);
        });
    }
  }

  afterLoad(journeys) {
    if (this.state.filterType == 'All Journeys') {
      this.setState({
        journeys: uniq(this.state.journeys.concat(journeys), 'id'),
        filteredList: uniq(this.state.journeys.concat(journeys), 'id'),
        pending: false
      });
    } else {
      this.setState({ journeys: uniq(this.state.journeys.concat(journeys), 'id'), pending: false });
      this.sortJourney(this.state.journeys);
    }
  }

  sortJourney(journeys) {
    let sorted;
    if (this.state.filterType === 'Sort by Alphabetical') {
      journeys.map(function(journey, index) {
        if (!journey.title) {
          journey.title = journey.message;
        }
      });

      sorted = journeys.sort((a, b) => a.title.localeCompare(b.title));
    } else if (this.state.filterType === 'Sort by Views') {
      sorted = journeys.sort((a, b) => {
        return parseInt(b.viewsCount) - parseInt(a.viewsCount);
      });
    } else if (this.state.filterType === 'Sort by Date') {
      sorted = journeys.sort((a, b) => {
        return new Date(b.createdAt) - new Date(a.createdAt);
      });
    } else if (this.state.filterType === 'All Journeys') {
      sorted = this.state.journeys;
    }
    this.setState({ filteredList: sorted });
  }

  handleFilterChange = e => {
    this.setState({ filterType: e });
    this.sortJourney(this.state.filteredList);
  };
  getCardsWrapper(queryParam) {
    this.setState({ pending: true, journeys: [], filteredList: [], filteredText: queryParam });
    let payload = {
      limit: null,
      offset: 0,
      fields: 'id,filestack,title,message,slug,journey_packs_count',
      q: queryParam
    };
    this.props
      .dispatch(getJourneys(payload))
      .then(journeys => {
        this.setState({ isLastPage: true });
        this.afterLoad(journeys);
      })
      .catch(err => {
        console.error(`Error in JourneysListContainer.getCardsWrapper.func : ${err}`);
      });
  }
  handleFilterSearch = (e, inputValue = null) => {
    if (!!inputValue) {
      this.getCardsWrapper(inputValue);
    } else if (e.keyCode === 13 || !e.target.value) {
      this.setState({ journeys: [], filteredList: [], filteredText: e.target.value });
      if (!e.target.value) {
        this.loadJourneys(20, 0);
      } else {
        this.getCardsWrapper(e.target.value);
      }
    }
  };

  render() {
    return (
      <div>
        <Breadcrumb
          levels={[
            { name: 'Discover', url: '/discover' },
            { name: 'Journeys', url: '/discover/journeys' }
          ]}
          backPress={this.state.isCardV3 && this.state.journeyConsumptionV2}
        />
        <div className="container-padding row content" id="journeys">
          <div className="columns expand vertical-spacing-large">
            <div className="vertical-spacing-medium">
              <ListView
                isTable={true}
                filterOptions={[
                  'All Journeys',
                  'Sort by Alphabetical',
                  'Sort by Views',
                  'Sort by Date'
                ]}
                handleFilterChange={this.handleFilterChange}
                handleFilterSearch={this.handleFilterSearch}
                enableBtnSearch={this.state.enableBtnSearch}
                pending={this.state.pending}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr('There are no journeys available.')}
                  </div>
                }
              >
                {this.state.filteredList.map((journey, index) => {
                  return <JourneyListItem key={index} journey={journey} />;
                })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

JourneysListContainer.propTypes = {
  currentUser: PropTypes.object
};

export default connect(state => ({
  journeys: state.journey.toJS(),
  currentUser: state.currentUser.toJS()
}))(JourneysListContainer);
