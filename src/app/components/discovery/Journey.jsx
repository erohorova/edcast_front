import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { openJourneyOverviewModal, openJourneyOverviewModalV2 } from '../../actions/modalActions';
import * as logoType from '../../constants/logoTypes';
import * as filestack from '../../utils/filestack';
import linkPrefix from '../../utils/linkPrefix';
import SvgImageResized from '../common/ImageResized';
import unescape from 'lodash/unescape';
import { saveConsumptionJourneyHistoryURL } from '../../actions/journeyActions';

class Journey extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {};

    this.state = {
      card: this.props.card,
      isStandalonePage: window.ldclient.variation('card-click-handle', 'modal'),
      journeyEnhancement: window.ldclient.variation('journey-enhancement', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      isCardV3: window.ldclient.variation('card-v3', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };
  }

  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}`
    ) {
      return;
    }
    let journeyBackUrl = window.location.pathname;
    if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
      this.props.dispatch(saveConsumptionJourneyHistoryURL(journeyBackUrl));
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };
  showJourney(slug) {
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Journey',
        name: slug,
        event: 'Journey Clicked'
      });
    }
    const logoObj = logoType.LOGO;
    const defaultImage = `(/i/images/courses/course${Math.floor(Math.random() * 48) + 1}.jpg)`;
    if (this.state.isStandalonePage.toLowerCase().indexOf('standalone') >= 0) {
      this.standaloneLinkClickHandler(this.props.card);
    } else {
      if (this.state.journeyEnhancement) {
        this.props.dispatch(
          openJourneyOverviewModalV2(this.state.card, logoObj, defaultImage, null, null, null, null)
        );
      } else {
        this.props.dispatch(
          openJourneyOverviewModal(this.state.card, logoObj, defaultImage, null, null, null, null)
        );
      }
    }
  }

  render() {
    return (
      <div className="journey">
        <a onTouchTap={this.showJourney.bind(this, this.props.slug)}>
          <Paper>
            <div className="stream-image">
              <div className="image">
                <svg width="100%" height="100%">
                  <SvgImageResized
                    cardId={`${this.props.card.id}`}
                    height="100%"
                    width="100%"
                    resizeOptions={'height:178'}
                    xlinkHref={this.props.imageUrl}
                  />
                </svg>
              </div>
            </div>
            <div className="info">
              <div className="title">{unescape(this.props.title)}</div>
              <div className="clearfix">
                <small className="card-count">
                  {this.props.cardsCount} {tr('Items')}
                </small>
              </div>
            </div>
          </Paper>
        </a>
      </div>
    );
  }
}

Journey.propTypes = {
  card: PropTypes.object,
  id: PropTypes.number,
  title: PropTypes.string,
  imageUrl: PropTypes.string,
  pathname: PropTypes.string,
  slug: PropTypes.string,
  cardsCount: PropTypes.number,
  discoverUpshotEventSend: PropTypes.func
};

export default connect()(Journey);
