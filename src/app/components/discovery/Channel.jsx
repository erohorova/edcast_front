import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { push } from 'react-router-redux';
import { unfollow, follow } from 'edc-web-sdk/requests/channels.v2';
import { followingChannelAction } from '../../actions/currentUserActions';
import * as filestack from '../../utils/filestack';
import BlurImage from '../common/BlurImage';
//actions
import { tr } from 'edc-web-sdk/helpers/translations';
import { snackBarOpenClose } from '../../actions/channelsActionsV2';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import LockIcon from 'edc-web-sdk/components/icons/Lock';
import IconButton from 'material-ui/IconButton/IconButton';
import DeleteIcon from 'edc-web-sdk/components/icons/Delete';
import Dialog from 'material-ui/Dialog';
import { groupsUnfollowChannel } from 'edc-web-sdk/requests/channels';
import { getTeamChannels } from '../../actions/groupsActionsV2';
import TooltipLabel from '../common/TooltipLabel';
import * as upshotActions from '../../actions/upshotActions';
import unescape from 'lodash/unescape';

class Channel extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: false,
      isFollowing:
        this.props.isFollowing ||
        Boolean(
          this.props.currentUser.followingChannels &&
            this.props.currentUser.followingChannels.filter(c => {
              return c.id == this.props.id;
            })[0]
        ),
      openConfirm: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      buttonPending: false
    };
    this.followClickHandler = this.followClickHandler.bind(this);
    this.getLink = this.getLink.bind(this);
    this.styles = {
      link: {
        cursor: this.props.disableLink ? 'default' : 'pointer',
        position: 'relative'
      },
      channelCourse: {
        margin: '0 auto'
      },
      imageBox: {
        height: '6.688rem'
      },
      paperStyle: {
        position: 'relative'
      }
    };
  }

  followClickHandler(e) {
    let action = this.state.isFollowing ? unfollow : follow;
    let _this = this;
    this.setState({ pending: true });
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        buttonName: !this.state.following ? 'Follow' : 'Unfollow',
        category: 'Channel',
        name: this.props.title,
        event: !this.state.following ? 'Follow Clicked' : 'Unfollow Clicked'
      });
    }
    action(this.props.id)
      .then(response => {
        if (this.props.updateDiscoverStore) {
          let updatedChannel = this.props.channel;
          updatedChannel['isFollowing'] = !this.state.isFollowing;
          this.props.updateDiscoverStore(updatedChannel);
        }
        if (
          this.state.isFollowing &&
          this.props.addPrivateIds !== undefined &&
          this.props.isPrivate
        ) {
          this.props.addPrivateIds(this.props.id);
        }
        _this.props.dispatch(followingChannelAction(_this.props.channel, _this.state.isFollowing));
        if (this.state.upshotEnabled) {
          upshotActions.sendCustomEvent(
            window.UPSHOTEVENT[!this.state.isFollowing ? 'FOLLOW' : 'UNFOLLOW'],
            {
              eventAction: !this.state.following ? 'Follow' : 'Unfollow',
              event: 'Channel'
            }
          );
        }
        this.setState({ pending: false, isFollowing: !this.state.isFollowing });
      })
      .catch(error => {
        this.props.dispatch(snackBarOpenClose(error.message, 5000));
        this.setState({ pending: false });
      });
  }

  getLink() {
    if (this.props.disableLink) {
      return 'javascript:;';
    } else {
      return '/channel/' + this.props.slug;
    }
  }

  handleConfirmation = () => {
    this.setState(prevState => ({
      openConfirm: !prevState.openConfirm
    }));
  };

  confirmClickHandler = () => {
    this.setState({ buttonPending: true }, () => {
      groupsUnfollowChannel(this.props.id, { 'ids[]': [this.props.group.id] })
        .then(() => {
          this.setState(
            {
              openConfirm: !this.state.openConfirm,
              buttonPending: false
            },
            () => {
              this.props.dispatch(
                getTeamChannels(
                  this.props.group.id,
                  { limit: this.props.teamChannelsLimit, offset: 0 },
                  true
                )
              );
            }
          );
        })
        .catch(err => {
          console.error(`Error in Channel.groupsUnfollowChannel.func : ${err}`);
        });
    });
  };

  goToDetailChannelPage = e => {
    e.preventDefault();
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Channel',
        name: this.props.title,
        event: 'Channel Open'
      });
    }
    this.props.allowFollow && this.props.dispatch(push(this.getLink()));
  };

  render() {
    let imageUrl = this.props.imageUrl;
    let smallImageUrl =
      imageUrl &&
      (~imageUrl.indexOf('default_banner_image')
        ? imageUrl
        : filestack.getResizedUrl(imageUrl, 'height:107'));
    let showLockIcon = !!(this.props.team.config && this.props.team.config.show_lock_icon);

    return (
      <div
        className="channel-course"
        style={
          this.props.style
            ? { ...this.styles.channelCourse, ...this.props.style }
            : this.styles.channelCourse
        }
      >
        <Paper {...this.props.borderRadius} style={this.styles.paperStyle}>
          <a
            aria-label={tr(`Visit %{channel_title}'s channel`, {channel_title: `${unescape(this.props.title)}`})}
            href="#"
            className="channel"
            onClick={this.goToDetailChannelPage}
            style={this.styles.link}
          >
            {!!this.props.isPrivate && showLockIcon && (
              <IconButton
                className="my-icon-button private-lock"
                aria-label={tr('Private Channel')}
                tooltip={tr('Private Channel')}
                tooltipPosition="bottom-center"
              >
                <LockIcon />
              </IconButton>
            )}
            <BlurImage
              style={this.styles.imageBox}
              id={this.props.id}
              image={smallImageUrl}
              isChannel={true}
            />
          </a>

          <div className="info text-center">
            <TooltipLabel
              text={this.props.channel.label.length > 21 ? unescape(this.props.channel.label) : ''}
              html={true}
              position={'top-left'}
            >
              <div className="channelv2-title">
                <a href="#" className="title channel" onClick={this.goToDetailChannelPage}>
                  {unescape(this.props.title)}
                </a>
              </div>
            </TooltipLabel>
            <div>
              {this.props.allowFollow ? (
                <FollowButton
                  onTouchTap={this.followClickHandler.bind(this)}
                  label={tr(this.state.isFollowing ? 'Following' : 'Follow')}
                  hoverLabel={tr(this.state.isFollowing ? 'Unfollow' : '')}
                  pendingLabel={tr(this.state.isFollowing ? 'Unfollowing...' : 'Following...')}
                  className="follow"
                  pending={this.state.pending}
                  following={this.state.isFollowing}
                />
              ) : (
                <SecondaryButton label={tr('Coming Soon')} disabled={true} />
              )}
            </div>
          </div>
          {this.props.removable && (
            <div className="channel-overlay-container">
              <div className="delete-container">
                <IconButton
                  className="specific-icon-wrapper delete-icon"
                  aria-label={tr('delete')}
                  onTouchTap={this.handleConfirmation}
                >
                  <DeleteIcon />
                </IconButton>
                <div className="text-center delete icon-label">{tr('Remove')}</div>
              </div>
            </div>
          )}
        </Paper>
        {this.state.openConfirm && (
          <Dialog
            open={this.state.openConfirm}
            onRequestClose={this.handleConfirmation}
            autoScrollBodyContent={false}
          >
            <div className="vertical-spacing-large">
              {tr(`Do you really want to remove "%{channel_title}" channel from the group?`, {channel_title: `${this.props.title}`})}
            </div>
            <div className="modal-actions text-right">
              <SecondaryButton
                label={tr('Cancel')}
                className="close"
                onTouchTap={this.handleConfirmation}
              />
              <PrimaryButton
                label={tr('Confirm')}
                pending={this.state.buttonPending}
                pendingLabel={tr('Deleting...')}
                className="confirm"
                onTouchTap={this.confirmClickHandler}
              />
            </div>
          </Dialog>
        )}
      </div>
    );
  }
}

Channel.propTypes = {
  id: PropTypes.number,
  channel: PropTypes.object,
  title: PropTypes.string,
  imageUrl: PropTypes.string,
  slug: PropTypes.string,
  borderRadius: PropTypes.object,
  currentUser: PropTypes.object,
  style: PropTypes.object,
  allowFollow: PropTypes.bool,
  titleMouseEnter: PropTypes.func,
  titleMouseLeave: PropTypes.func,
  disableLink: PropTypes.bool,
  isPrivate: PropTypes.bool,
  addPrivateIds: PropTypes.func,
  group: PropTypes.object,
  teamChannelsLimit: PropTypes.any,
  removable: PropTypes.bool,
  discoverUpshotEventSend: PropTypes.func,
  isFollowing: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(Channel);
