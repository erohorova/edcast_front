import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchChannelsAction } from '../../actions/channelsActions';
import ChannelListItem from '../common/ChannelListItem';
import ListView from '../common/ListView';
import Breadcrumb from '../common/Breadcrumb';
import uniq from 'lodash/uniq';
import throttle from 'lodash/throttle';
import { tr } from 'edc-web-sdk/helpers/translations';

class ChannelsListContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channels: [],
      pending: true,
      isLastPage: false,
      limit: 20,
      offset: 0,
      filterType: 'All Channels',
      inputValue: '',
      enableBtnSearch: true
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreChannels = this.showMoreChannels.bind(this);
    this.fields =
      'id,slug,label,description,followers_count,allow_follow,is_following,banner_image_urls,profile_image_url,is_private';
  }

  componentDidMount() {
    let { limit, offset } = this.state;
    let payload = {
      limit,
      offset,
      fields: this.fields
    };

    this.props
      .dispatch(fetchChannelsAction(payload))
      .then(channels => {
        let isLastPage = channels.length < this.state.limit;
        this.setState({ channels, pending: false, isLastPage });
      })
      .catch(err => {
        console.error(`Error in ChannelsListContainer.fetchChannelsAction.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  updateState = ({ newChannels = [], isPaginating }) => {
    let { limit, channels } = this.state;
    channels = isPaginating ? channels : [];
    let isLastPage = newChannels.length < limit;

    this.setState({
      channels: uniq(channels.concat(newChannels), 'id'),
      pending: false,
      isLastPage
    });
  };

  getChannelsByType = ({ isPaginating = false }) => {
    let { limit, offset, inputValue } = this.state;
    let newOffset = offset + limit;
    let commonPayload = {
      limit,
      offset: newOffset,
      fields: this.fields,
      q: inputValue
    };
    let fetchChannelsPayload = commonPayload;
    let followingChannelsPayload = {
      is_following: true,
      ...commonPayload
    };
    let suggestedChannelsPayload = {
      is_following: false,
      ...commonPayload
    };
    this.setState({ pending: true, offset: newOffset });

    if (this.state.filterType === 'All Channels') {
      this.props
        .dispatch(fetchChannelsAction(fetchChannelsPayload))
        .then(newChannels => {
          this.updateState({ newChannels, isPaginating });
        })
        .catch(err => {
          console.error(
            `Error in ChannelsListContainer.fetchChannelsAction.func All Channels: ${err}`
          );
        });
    } else if (this.state.filterType === 'Following') {
      this.props
        .dispatch(fetchChannelsAction(followingChannelsPayload))
        .then(newChannels => {
          this.updateState({ newChannels, isPaginating });
        })
        .catch(err => {
          console.error(
            `Error in ChannelsListContainer.fetchChannelsAction.func Following: ${err}`
          );
        });
    } else if (this.state.filterType === 'Not Following') {
      this.props
        .dispatch(fetchChannelsAction(suggestedChannelsPayload))
        .then(newChannels => {
          this.updateState({ newChannels, isPaginating });
        })
        .catch(err => {
          console.error(
            `Error in ChannelsListContainer.fetchChannelsAction.func Not Following: ${err}`
          );
        });
    }
  };

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreChannels();
        }
      }
    },
    150,
    { leading: false }
  );

  showMoreChannels = () => {
    this.getChannelsByType({ isPaginating: true });
  };

  handleFilterChange = e => {
    // Setting state for offset, to make newOffset 0.
    this.setState({ filterType: e, offset: -1 * this.state.limit }, () =>
      this.getChannelsByType({ isPaginating: false })
    );
  };

  handleFilterSearch = (e, inputValue = null) => {
    let q = (!!e.target.value && e.target.value.trim()) || !!inputValue;
    if (e.keyCode === 13 && q) {
      this.setState({ inputValue: q, offset: -1 * this.state.limit }, () =>
        this.getChannelsByType({ isPaginating: false })
      );
    }
  };

  render() {
    return (
      <div>
        <Breadcrumb
          levels={[
            { name: 'Discover', url: '/discover' },
            { name: 'Channels', url: '/discover/channels' }
          ]}
        />
        <div className="container-padding row content" id="channels">
          <div className="columns expand vertical-spacing-large">
            <div className="vertical-spacing-medium">
              <ListView
                isTable={true}
                filterOptions={['All Channels', 'Following', 'Not Following']}
                handleFilterChange={this.handleFilterChange}
                handleFilterSearch={this.handleFilterSearch}
                enableBtnSearch={this.state.enableBtnSearch}
                searchPlaceholderText="Search All Channels"
                pending={this.state.pending}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr(`Sorry! We couldn't find the channel you're looking for.`)}
                  </div>
                }
              >
                {this.state.channels.length > 0 &&
                  this.state.channels.map((channelId, index) => {
                    let channel = this.props.channels[channelId];
                    return (
                      <ChannelListItem
                        key={index}
                        channel={channel}
                        isPrivate={channel.isPrivate}
                      />
                    );
                  })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChannelsListContainer.propTypes = {
  channels: PropTypes.object
};

export default connect(state => ({ channels: state.channels.toJS() }))(ChannelsListContainer);
