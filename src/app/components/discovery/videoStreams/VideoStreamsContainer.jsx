import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Breadcrumb from '../../common/Breadcrumb';
import ListView from '../../common/ListView';
import { getCards, getTrendingVideoCards } from 'edc-web-sdk/requests/cards';
import VideoStreamListItem from '../../common/VideoStreamListItem';
import * as insightType from '../../../constants/insightTypes';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

class VideoStreamsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      videoStreams: [],
      limit: 5,
      offset: 0,
      isLastPage: false,
      filteredList: [],
      filteredText: '',
      filterType: 'All Videostreams',
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      enableBtnSearch: true
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    this.loadVideos(this.state.limit, this.state.offset);
    window.addEventListener('scroll', this.handleScroll);
  }

  loadVideos(limit, offset, q) {
    if (!this.state.pending) {
      this.setState({ pending: true });
      getTrendingVideoCards({
        limit: limit,
        offset: offset,
        q: q,
        filter_by_language: this.state.multilingualContent
      })
        .then(data => {
          let isLastPage = data.length < this.state.limit;
          this.setState({ offset: offset + limit, pending: false, isLastPage });
          this.afterLoad(data);
        })
        .catch(err => {
          console.error(`Error in VideoStreamsContainer.getTrendingVideoCards.func : ${err}`);
        });
    }
  }

  afterLoad(videoStreams) {
    if (this.state.filterType == 'All Videostreams') {
      this.setState({
        videoStreams: uniq(this.state.videoStreams.concat(videoStreams), 'id'),
        filteredList: uniq(this.state.videoStreams.concat(videoStreams), 'id'),
        pending: false
      });
    } else {
      this.setState({
        videoStreams: uniq(this.state.videoStreams.concat(videoStreams), 'id'),
        pending: false
      });
      this.sortVideos(this.state.videoStreams);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadVideos(this.state.limit, this.state.offset, this.state.filteredText);
        }
      }
    },
    150,
    { leading: false }
  );

  getCardsWrapper(queryParam) {
    this.setState({
      pending: true,
      filteredList: [],
      videoStreams: [],
      filteredText: queryParam,
      offset: 0
    });
    getTrendingVideoCards({
      limit: this.state.limit,
      'card_type[]': insightType.VIDEO_STREAM,
      q: queryParam,
      filter_by_language: this.state.multilingualContent
    })
      .then(data => {
        this.setState({ isLastPage: data.length < this.state.limit });
        this.afterLoad(data);
      })
      .catch(err => {
        console.error(`Error in VideoStreamsContainer.getCardsWrapper.func : ${err}`);
      });
  }

  handleFilterSearch = (e, inputValue = null) => {
    if (!!inputValue) {
      this.getCardsWrapper(inputValue);
    } else if (e.keyCode === 13 || !e.target.value) {
      this.setState({
        filteredList: [],
        videoStreams: [],
        filteredText: e.target.value,
        offset: 0,
        pending: true
      });
      if (!e.target.value) {
        this.loadVideos(this.state.limit, 0);
      } else {
        this.getCardsWrapper(e.target.value);
      }
    }
  };

  sortVideos(videos) {
    let sorted;
    if (this.state.filterType === 'Sort by Name') {
      sorted = videos.sort((a, b) => a.message.localeCompare(b.message));
    } else if (this.state.filterType === 'Sort by Views') {
      sorted = videos.sort((a, b) => {
        return parseInt(b.viewsCount) - parseInt(a.viewsCount);
      });
    } else if (this.state.filterType === 'Sort by Date') {
      sorted = videos.sort((a, b) => {
        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
      });
    } else if (this.state.filterType === 'All Videostreams') {
      sorted = videos.sort((a, b) => {
        return b.id - a.id;
      });
    }
    this.setState({ filteredList: sorted });
  }

  handleFilterChange = e => {
    this.setState({ filterType: e });
    this.sortVideos(this.state.videoStreams);
  };

  render() {
    return (
      <div>
        <Breadcrumb levels={[{ name: 'Discover', url: '/discover' }, { name: 'Live Streams' }]} />
        <div className="row container-padding">
          <div className="vertical-spacing-large column small-12">
            <ListView
              pending={this.state.pending}
              isTable={true}
              filterOptions={['All Videostreams', 'Sort by Name', 'Sort by Date']}
              handleFilterChange={this.handleFilterChange}
              handleFilterSearch={this.handleFilterSearch}
              enableBtnSearch={this.state.enableBtnSearch}
              emptyMessage={
                <div className="data-not-available-msg">
                  {tr('There are no Live Streams available.')}
                </div>
              }
            >
              {this.state.filteredList.map(videoStream => {
                return <VideoStreamListItem stream={videoStream} key={videoStream.id} />;
              })}
            </ListView>
          </div>
        </div>
      </div>
    );
  }
}

VideoStreamsContainer.propTypes = {};

export default connect()(VideoStreamsContainer);
