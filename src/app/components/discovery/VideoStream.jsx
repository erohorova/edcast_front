import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import Paper from 'edc-web-sdk/components/Paper';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import { tr } from 'edc-web-sdk/helpers/translations';

class VideoStream extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="videostream">
        <Paper>
          <a href={`/video_streams/${this.props.cardId}`}>
            <div className="stream-image">
              <div
                className="image"
                style={{ backgroundImage: 'url(' + this.props.imageUrl + ')' }}
              >
                <PlayIcon
                  className="play-icon"
                  style={{
                    fill: 'white',
                    top: '50%',
                    left: '50%',
                    width: '50px',
                    height: '50px',
                    position: 'absolute',
                    transform: 'translate(-50%, -50%)'
                  }}
                />
                {this.props.status.toLowerCase() === 'live' && (
                  <small className="live-label">{tr(this.props.status)}</small>
                )}
              </div>
            </div>
          </a>
          <div className={!this.props.authorImage ? 'stream-from-cms' : ''}>
            <ListItem
              disabled
              leftAvatar={
                this.props.authorImage ? (
                  <a href={`/${this.props.handle}`}>
                    <Avatar size={40} src={this.props.authorImage} />
                  </a>
                ) : (
                  undefined
                )
              }
              primaryText={<div className="avatar-name">{this.props.title}</div>}
            />
          </div>
        </Paper>
      </div>
    );
  }
}

VideoStream.propTypes = {
  slug: PropTypes.string,
  status: PropTypes.string,
  handle: PropTypes.string,
  authorImage: PropTypes.string,
  imageUrl: PropTypes.string,
  cardId: PropTypes.string
};

export default VideoStream;
