import React, { PorpTypes, Component } from 'react';

class SampleBlock extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return <div style={{ backgroundColor: 'green', height: 200 }} />;
  }
}

SampleBlock.propTypes = {};

export default SampleBlock;
