import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTrendingPathways } from 'edc-web-sdk/requests/pathways';
import { getTrendingJourneys } from 'edc-web-sdk/requests/journeys';
import getDefaultImage from '../../../utils/getDefaultCardImage';
import Pathway from '../Pathway.jsx';
import Journey from '../Journey.jsx';
import Carousel from '../../common/Carousel';
import EmptyBlock from '../EmptyBlock.jsx';
import { TrendingPathwaysAndJourneys } from '../../common/EmptyStateLoaders';
import _ from 'lodash';
import * as upshotActions from '../../../actions/upshotActions';

class JourneyPathwayCarousels extends Component {
  constructor(props, context) {
    super(props, context);
    this.multilingualContent = window.ldclient.variation('multilingual-content', false);
    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let cards = null;

    cards =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      cards: cards || null
    };

    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };

    this.isShowCustomLabels =
      this.props.team.get('config') && this.props.team.get('config').custom_labels;
  }

  viewMorePathways = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  componentDidMount() {
    let cards = this.state.cards;

    if (!this.props.isCustomCarousel && !(Array.isArray(cards) && cards.length)) {
      let carouselKey = this.props.carouselKey;
      switch (carouselKey) {
        case 'discover/pathways':
          getTrendingPathways({ filter_by_language: this.multilingualContent })
            .then(response => {
              this.setState({
                cards: response
              });
            })
            .catch(error => {
              console.error(
                'ERROR!!! in JourneyPathwayCarousels.componentDidMount.getTrendingPathways()',
                error
              );
            });
          break;
        case 'discover/journeys':
          getTrendingJourneys({ filter_by_language: this.multilingualContent })
            .then(response => {
              this.setState({
                cards: response
              });
            })
            .catch(error => {
              console.error(
                'ERROR!!! in JourneyPathwayCarousels.componentDidMount.getTrendingJourneys()',
                error
              );
            });
          break;
        default:
          break;
      }
    }
  }

  cardComp = (card, id, title, imageUrl, slug, cardsCount, carouselKey) => {
    switch (carouselKey) {
      case 'discover/pathways':
        return (
          <Pathway
            card={card}
            id={id}
            title={title}
            imageUrl={imageUrl}
            slug={slug}
            cardsCount={cardsCount}
          />
        );
      case 'discover/journeys':
        return (
          <Journey
            card={card}
            id={id}
            title={title}
            imageUrl={imageUrl}
            slug={slug}
            cardsCount={cardsCount}
          />
        );
      default:
        return null;
    }
  };

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.cards && this.state.cards.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.cards,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  render() {
    let carousel = this.props.carousel;
    let cards = this.state.cards;
    let carouselUID = carousel.uid;

    if (this.props.isCustomCarousel && !(Array.isArray(cards) && cards.length)) {
      cards =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }

    let carouselKey = this.props.carouselKey;
    let link = (carouselKey === 'discover/pathways' && {
      text: tr('CREATE PATHWAY'),
      isPush: false,
      url: '/pathways/new'
    }) || { text: tr('CREATE JOURNEY'), isPush: false, url: '/journeys/new' };

    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div
        id={this.props.carouselKey === 'discover/pathways' ? 'pathwayCarousel' : ''}
        className={!!cards && cards.length == 0 ? 'hidden' : ''}
      >
        {(!cards || (Array.isArray(cards) && !!cards.length)) && (
          <div className="row discover-title" style={this.styles.customRow}>
            <div className="small-6 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(carousel.label || carousel.defaultLabel || (carouselKey === 'discover/pathways' ? 'PATHWAYS' : 'JOURNEY'))}
              </div>
            </div>
            {cards && cards.length >= 5 && (
              <div className="small-6 columns">
                <div className="text-right">
                  <small>
                    <a
                      href="#"
                      className="matte pathways"
                      onClick={e => this.viewMorePathways(e, '/' + carouselKey)}
                    >
                      {tr('View More')}
                    </a>
                  </small>
                </div>
              </div>
            )}
          </div>
        )}
        {Array.isArray(cards) && !!cards.length && (
          <div className="discovery-contents">
            <div className="small-12 columns discover-card-wrapper">
              <div className="discover-card-wrapper-inner discover-card-width">
                <Carousel
                  isDiscovery={true}
                  isDiscovery_card={true}
                  slidesToShow={3}
                  key="trending"
                >
                  {cards.map((card, index) => {
                    let imageUrl;
                    let isFileAttached = card.filestack && !!card.filestack.length;
                    let imageFileStack = !!(
                      isFileAttached &&
                      card.filestack[0].mimetype &&
                      ~card.filestack[0].mimetype.indexOf('image/')
                    );
                    if (card.imageUrl) imageUrl = card.imageUrl;
                    else {
                      if (imageFileStack) {
                        imageUrl = card.filestack[0].url;
                      } else if (card.fileResources) {
                        let findImage = _.find(card.fileResources, function(el) {
                          return el.fileType == 'image';
                        });
                        if (findImage) imageUrl = findImage.fileUrl;
                      }
                    }
                    return (
                      <div key={card.id}>
                        {this.cardComp(
                          card,
                          card.id,
                          card.title || card.message,
                          imageUrl || getDefaultImage(this.props.currentUser.get('id')).url,
                          card.slug,
                          carouselKey === 'discover/pathways'
                            ? card.packCardsCount
                            : card.journeyPacksCount,
                          carouselKey
                        )}
                      </div>
                    );
                  })}
                </Carousel>
              </div>
            </div>
          </div>
        )}
        {!cards && (
          <div>
            <div className="small-12 columns empty-state">
              <TrendingPathwaysAndJourneys loadingCards={4} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

JourneyPathwayCarousels.propTypes = {
  discovery: PropTypes.object,
  team: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any,
  carouselKey: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    team: state.team,
    currentUser: state.currentUser
  };
}

export default connect(mapStoreStateToProps)(JourneyPathwayCarousels);
