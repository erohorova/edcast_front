import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getRecommendedUsers } from 'edc-web-sdk/requests/users';
import Carousel from '../../common/Carousel';
import Influencer from '../Influencer.jsx';
import UserSquareItemV2 from '../../common/UserSquareItemV2';
import { UserLoader } from '../../common/EmptyStateLoaders';
import EmptyBlock from '../EmptyBlock.jsx';
import * as upshotActions from '../../../actions/upshotActions';
import findIndex from 'lodash/findIndex';

class UsersCarousels extends Component {
  constructor(props, context) {
    super(props, context);
    this.multilingualContent = window.ldclient.variation('multilingual-content', false);
    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let users = null;

    users =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      users: users || null,
      isPeopleCardV3: window.ldclient.variation('discover-people-card-v3', false)
    };

    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };

    (this.isNewProfileNavigation = window.ldclient.variation('is-me-new-navigation', false)),
      (this.onlySmeOnPeopleCarousel = !!(
        props.team &&
        props.team.get('config') &&
        props.team.get('config')['enable_only_sme_on_people_carousel']
      )),
      (this.isShowCustomLabels =
        this.props.team.get('config') && this.props.team.get('config').custom_labels);
  }

  componentDidMount() {
    let users = this.state.users;

    if (!this.props.isCustomCarousel && !(Array.isArray(users) && users.length)) {
      if (
        this.props.team.get('Discover') &&
        this.props.team.get('Discover')['discover/users'] &&
        this.props.team.get('Discover')['discover/users'].visible
      ) {
        let payload = { limit: 50 };
        let team_users_only =
          this.props.team.get('config') &&
          this.props.team.get('config')['enable_only_team_users_on_people_carousel'];
        payload['my_teams_users'] = team_users_only == undefined || team_users_only;
        payload['fields'] =
          'id,handle,avatarimages,roles,roles_default_names,name,is_following,following_count,followers_count,expert_skills,status,company,coverimages';
        getRecommendedUsers(payload)
          .then(response => {
            this.setState({
              users: response
            });
          })
          .catch(error => {
            console.error('ERROR!!! in CardCarousels.componentDidMount.getTrendingCards()', error);
          });
      }
    }
  }

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.users && this.state.users.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.users,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  userComp = (
    user,
    id,
    isFollowing,
    followPending,
    name,
    handle,
    imageUrl,
    expertSkills,
    roles,
    rolesDefaultNames,
    position
  ) => {
    return this.state.isPeopleCardV3 ? (
      <UserSquareItemV2 key={`team_user_${id}`} user={user} source={'discoveryCarousel'} />
    ) : (
      <Influencer
        id={id}
        name={name}
        handle={handle}
        imageUrl={imageUrl}
        expertSkills={expertSkills}
        roles={roles}
        rolesDefaultNames={rolesDefaultNames}
        position={position}
        following={isFollowing}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
        updateDiscoverStore={this.updateDiscoverStore}
      />
    );
  };

  openTeam = e => {
    e.preventDefault();
    if (this.onlySmeOnPeopleCarousel) {
      this.props.dispatch(push(this.isNewProfileNavigation ? '/team/sme' : '/me/team/sme'));
    } else {
      this.props.dispatch(push(this.isNewProfileNavigation ? '/team' : '/me/team'));
    }
  };

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  updateDiscoverStore = data => {
    let users = this.state.users;
    let index = -1;
    if (this.props.isCustomCarousel) {
      this.props.dispatch({
        type: 'update_discover_data',
        carosuel: this.props.carousel,
        data
      });
    } else {
      data['isFollowing'] = !data['isFollowing'];
      index = findIndex(users, ['id', data.id]);
      if (index > -1) {
        users[index] = data;
        this.setState({ users });
      }
    }
  };

  render() {
    let carousel = this.props.carousel;
    let users = this.state.users;
    let carouselUID = carousel.uid;

    if (this.props.isCustomCarousel && !(Array.isArray(users) && users.length)) {
      users =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }

    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div name="influencers" className={!!users && users.length == 0 ? 'hidden' : ''}>
        {(!users || (Array.isArray(users) && !!users.length)) && (
          <div className="row discover-title" style={this.styles.customRow}>
            <div className="small-6 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(carousel.label || carousel.defaultLabel || 'PEOPLE')}
              </div>
            </div>
            {users &&
              this.props.team.get('OrgConfig') &&
              this.props.team.get('OrgConfig').profile['web/profile/teams'].visible && (
                <div className="small-6 columns">
                  <div className="text-right">
                    <small>
                      <a href="#" className="matte" onClick={this.openTeam}>
                        {tr('View More')}
                      </a>
                    </small>
                  </div>
                </div>
              )}
          </div>
        )}

        <div className="discovery-contents">
          {Array.isArray(users) && !!users.length && (
            <div className="small-12 columns">
              <Carousel
                isDiscovery={true}
                isPeopleCarouselV3={this.state.isPeopleCardV3}
                slidesToShow={5}
                key="influencers"
              >
                {users
                  .map((user, index) => {
                    let position = 'top';
                    user['isComplete'] = true;
                    return (
                      <div
                        key={user.id}
                        style={this.state.isPeopleCardV3 ? this.styles.newPeopleCard : ''}
                      >
                        {this.userComp(
                          user,
                          user.id,
                          user.isFollowing,
                          user.followPending,
                          user.name,
                          user.handle,
                          (user.avatarimages && user.avatarimages.medium) || defaultUserImage,
                          user.expertSkills,
                          (user.roles !== undefined && user.roles) || ['influencer'],
                          (user.rolesDefaultNames !== undefined && user.rolesDefaultNames) || [
                            'influencer'
                          ],
                          position
                        )}
                      </div>
                    );
                  })
                  .concat(
                    !users && (
                      <div key="empty">
                        <EmptyBlock
                          className="influencers"
                          link={{
                            text: tr('VIEW TEAM'),
                            isPush: true,
                            url: '/me/team'
                          }}
                        />
                      </div>
                    )
                  )}
              </Carousel>
            </div>
          )}
          {!users && (
            <div className="small-12 columns empty-state">
              <UserLoader loadingCards={this.state.isPeopleCardV3 ? 5 : 7} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

UsersCarousels.propTypes = {
  discovery: PropTypes.object,
  users: PropTypes.object,
  team: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    users: state.users,
    team: state.team
  };
}

export default connect(mapStoreStateToProps)(UsersCarousels);
