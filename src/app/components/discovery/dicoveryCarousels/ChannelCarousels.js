import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getRecommendedChannels } from 'edc-web-sdk/requests/channels';
import Carousel from '../../common/Carousel';
import Channel from '../Channel';
import ChannelLoader from '../../common/ChannelLoader';
import EmptyBlock from '../EmptyBlock.jsx';
import * as filestack from '../../../utils/filestack';
import * as upshotActions from '../../../actions/upshotActions';
import findIndex from 'lodash/findIndex';

class ChannelCarousels extends Component {
  constructor(props, context) {
    super(props, context);

    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let channels = null;

    channels =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      channels: channels || null
    };

    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };

    this.isShowCustomLabels =
      this.props.team.get('config') && this.props.team.get('config').custom_labels;
  }

  componentDidMount() {
    let channels = this.state.channels;

    if (!this.props.isCustomCarousel && !(Array.isArray(channels) && channels.length)) {
      getRecommendedChannels({ limit: 50 })
        .then(data => {
          this.setState({
            channels: data.channels
          });
        })
        .catch(error => {
          console.error(
            'ERROR!!! in ChannelCarousels.componentDidMount.getRecommendedChannels()',
            error
          );
        });
    }
  }

  channelComp = (
    id,
    channel,
    following,
    followPending,
    title,
    imageUrl,
    slug,
    allowFollow,
    isChannelPrivate,
    channelShowLockIcon
  ) => {
    let smallImageUrl =
      imageUrl &&
      (~imageUrl.indexOf('default_banner_image')
        ? imageUrl
        : filestack.getResizedUrl(imageUrl, 'height:107'));
    return (
      <Channel
        id={id}
        isFollowing={following}
        title={title}
        channel={channel}
        titleMouseLeave={this.hideChanelTooltip}
        imageUrl={smallImageUrl}
        slug={slug}
        allowFollow={allowFollow}
        isPrivate={isChannelPrivate}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
        updateDiscoverStore={this.updateDiscoverStore}
      />
    );
  };

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.channels && this.state.channels.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.channels,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  updateDiscoverStore = data => {
    let channels = this.state.channels;
    let index = -1;
    if (this.props.isCustomCarousel) {
      this.props.dispatch({
        type: 'update_discover_data',
        carosuel: this.props.carousel,
        data
      });
    } else {
      let newData = Object.assign(data, { isFollowing: !data['isFollowing'] });
      index = findIndex(channels, ['id', newData.id]);
      if (index > -1) {
        channels[index] = newData;
        this.setState({ channels });
      }
    }
  };

  openChannelPage = e => {
    e.preventDefault();
    if (this.props.isCustomCarousel) {
      this.props.dispatch(push('/discover-channels/' + this.props.carousel.id));
    } else {
      this.props.dispatch(push('/channels/all'));
    }
  };

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  render() {
    let carousel = this.props.carousel;
    let channels = this.state.channels;
    let carouselUID = carousel.uid;
    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    if (this.props.isCustomCarousel) {
      channels =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }
    return (
      <div className={!!channels && channels.length == 0 ? 'hidden' : ''}>
        {(!channels || (Array.isArray(channels) && !!channels.length)) && (
          <div className="row discover-title" style={this.styles.customRow}>
            <div className="small-6 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(carousel.label || carousel.defaultLabel || 'CHANNELS')}
              </div>
            </div>

            <div className="small-6 columns">
              <div className="text-right">
                <small>
                  <a
                    href="#"
                    className="matte channel"
                    onClick={e => {
                      this.openChannelPage(e);
                    }}
                  >
                    {tr('View More')}
                  </a>
                </small>
              </div>
            </div>
          </div>
        )}
        {Array.isArray(channels) && !!channels.length && (
          <div className="discovery-contents">
            <div className="small-12 columns discover-card-wrapper">
              <div className="discover-card-wrapper-inner discover-card-width">
                <Carousel isDiscovery={true} slidesToShow={5} key="channels">
                  {channels.map(channel => {
                    let mediumProfileImage =
                      channel.profileImageUrls && channel.profileImageUrls.medium_url;
                    return (
                      <div key={channel.id}>
                        {this.channelComp(
                          channel.id,
                          channel,
                          channel.isFollowing,
                          channel.followPending,
                          channel.label,
                          mediumProfileImage || channel.bannerImageUrl,
                          channel.slug,
                          channel.allowFollow,
                          channel.isPrivate
                        )}
                      </div>
                    );
                  })}
                </Carousel>
              </div>
            </div>
          </div>
        )}
        {!channels && (
          <div>
            <div className="small-12 columns empty-state">
              <ChannelLoader loadingCards={5} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

ChannelCarousels.propTypes = {
  discovery: PropTypes.object,
  team: PropTypes.object,
  channels: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    team: state.team,
    channels: state.channels
  };
}

export default connect(mapStoreStateToProps)(ChannelCarousels);
