import { recommendedCourses } from 'edc-web-sdk/requests/edcastcloud';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Carousel from '../../common/Carousel';
import { CourseLoader } from '../../common/EmptyStateLoaders';
import EmptyBlock from '../EmptyBlock.jsx';
import Course from '../Course.jsx';
import * as upshotActions from '../../../actions/upshotActions';

class CourseCarousels extends Component {
  constructor(props, context) {
    super(props, context);

    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let courses = null;

    courses =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      courses: courses || null
    };

    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };

    this.isShowCustomLabels =
      this.props.team.get('config') && this.props.team.get('config').custom_labels;
  }

  componentDidMount() {
    let courses = this.state.courses;

    if (!this.props.isCustomCarousel && !(Array.isArray(courses) && courses.length)) {
      recommendedCourses(20, 0, true)
        .then(response => {
          this.setState({
            courses: response
          });
        })
        .catch(error => {
          console.error(
            'ERROR!!! in CourseCarousels.componentDidMount.recommendedCourses()',
            error
          );
        });
    }
  }

  courseComp = (name, logoUrl, startDate, endDate, status, url) => {
    return (
      <Course
        name={name}
        logoUrl={logoUrl}
        startDate={startDate}
        endDate={endDate}
        status={status}
        url={url}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
      />
    );
  };

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.courses && this.state.courses.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.courses,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  moreCourses = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };

  render() {
    let carousel = this.props.carousel;
    let courses = this.state.courses;
    let carouselUID = carousel.uid;

    if (this.props.isCustomCarousel && !(Array.isArray(courses) && courses.length)) {
      courses =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }

    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div name="course" className={!!courses && courses.length == 0 ? 'hidden' : ''}>
        {(!courses || (Array.isArray(courses) && !!courses.length)) && (
          <div className="row discover-title" style={this.styles.customRow}>
            <div className="small-6 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(carousel.label || carousel.defaultLabel || 'RECOMMENDED COURSES')}
              </div>
            </div>
            <div className="small-6 columns">
              <div className="text-right">
                {(Array.isArray(courses) && courses.length) > 5 && (
                  <small>
                    <a
                      href="#"
                      className="matte videostream"
                      onClick={e => this.moreCourses(e, '/discover/courses')}
                    >
                      {tr('View More')}
                    </a>
                  </small>
                )}
              </div>
            </div>
          </div>
        )}
        <div className="discovery-contents">
          {Array.isArray(courses) && !!courses.length && (
            <div className="small-12 columns">
              <Carousel isDiscovery={true} slidesToShow={5} key="courses">
                {courses.map(course => {
                  return (
                    <div key={course.id}>
                      {this.courseComp(
                        course.name,
                        course.logoUrl,
                        course.startDate,
                        course.endDate,
                        course.status,
                        course.url
                      )}
                    </div>
                  );
                })}
              </Carousel>
            </div>
          )}
          {!courses && (
            <div className="small-12 columns empty-state">
              <CourseLoader loadingCards={7} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

CourseCarousels.propTypes = {
  discovery: PropTypes.object,
  team: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    team: state.team
  };
}

export default connect(mapStoreStateToProps)(CourseCarousels);
