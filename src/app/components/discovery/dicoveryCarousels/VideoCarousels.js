import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTrendingVideoCards } from 'edc-web-sdk/requests/cards';
import Carousel from '../../common/Carousel';
import { VideoLoader } from '../../common/EmptyStateLoaders';
import EmptyBlock from '../EmptyBlock.jsx';
import Video from '../Video.jsx';
import * as upshotActions from '../../../actions/upshotActions';

class VideoCarousels extends Component {
  constructor(props, context) {
    super(props, context);

    this.multilingualContent = window.ldclient.variation('multilingual-content', false);
    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let videos = null;

    videos =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      videos: videos || null
    };

    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };

    this.isShowCustomLabels =
      this.props.team.get('config') && this.props.team.get('config').custom_labels;
  }

  componentDidMount() {
    let videos = this.state.videos;

    if (!this.props.isCustomCarousel && !(Array.isArray(videos) && videos.length)) {
      getTrendingVideoCards({ filter_by_language: this.multilingualContent })
        .then(response => {
          this.setState({
            videos: response
          });
        })
        .catch(error => {
          console.error(
            'ERROR!!! in VideoCarousels.componentDidMount.getTrendingVideoCards()',
            error
          );
        });
    }
  }

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.videos && this.state.videos.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.videos,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  viewMoreVideostreamCards = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  render() {
    let carousel = this.props.carousel;
    let videos = this.state.videos;
    let carouselUID = carousel.uid;

    if (this.props.isCustomCarousel && !(Array.isArray(videos) && videos.length)) {
      videos =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }

    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div className={!!videos && videos.length == 0 ? 'hidden' : ''}>
        <div name="video stream">
          {(!videos || (Array.isArray(videos) && !!videos.length)) && (
            <div className="row discover-title" style={this.styles.customRow}>
              <div className="small-6 columns">
                <div role="heading" aria-level="3">
                  {translatedLabel || tr(carousel.label || carousel.defaultLabel || 'VIDEOS')}
                </div>
              </div>
              {videos && videos.length >= 4 && (
                <div className="small-6 columns">
                  <div className="text-right">
                    <small>
                      <a
                        href="#"
                        className="matte videostreams"
                        onClick={e => this.viewMoreVideostreamCards(e, '/discover/videostreams')}
                      >
                        {tr('View More')}
                      </a>
                    </small>
                  </div>
                </div>
              )}
            </div>
          )}
          {Array.isArray(videos) && !!videos.length && (
            <div className="discovery-contents">
              <div className="small-12 columns">
                <Carousel isDiscovery={true} slidesToShow={4} key="videos">
                  {videos.map(video => {
                    return (
                      <div key={video.id}>
                        <Video
                          videoCard={video}
                          controlsList={'nodownload'}
                          discoverUpshotEventSend={this.discoverUpshotEventSend}
                        />
                      </div>
                    );
                  })}
                </Carousel>
              </div>
            </div>
          )}
          {!videos && (
            <div>
              <div className="small-12 columns empty-state">
                <VideoLoader loadingCards={4} />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

VideoCarousels.propTypes = {
  discovery: PropTypes.object,
  team: PropTypes.object,
  carousel: PropTypes.any,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    team: state.team
  };
}

export default connect(mapStoreStateToProps)(VideoCarousels);
