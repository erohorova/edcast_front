import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getTrendingCards, getCarouselCards } from 'edc-web-sdk/requests/cards';
import Carousel from '../../common/Carousel';
import Card from '../../cards/Card';
import CardLoader from '../../common/CardLoader';
import EmptyBlock from '../EmptyBlock.jsx';
import * as upshotActions from '../../../actions/upshotActions';

import { cardFields } from '../../../constants/cardFields';

class CardCarousels extends Component {
  constructor(props, context) {
    super(props, context);
    this.multilingualContent = window.ldclient.variation('multilingual-content', false);

    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let cards = null;

    cards =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.state = {
      cards: cards || null,
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
  }

  componentDidMount() {
    let cards = this.state.cards;

    if (!this.props.isCustomCarousel && !(Array.isArray(cards) && cards.length)) {
      if (!!this.props.isOldConfigCarousel) {
        let carouselArr = this.props.carousel.uid.split('/');
        getCarouselCards(carouselArr[carouselArr.length - 1], {
          filter_by_language: this.state.multilingualContent,
          last_access_at: localStorage.getItem('discoveryLastVisit'),
          fields: cardFields
        })
          .then(response => {
            this.setState({
              cards: response
            });
          })
          .catch(error => {
            console.error('ERROR!!! in CardCarousels.componentDidMount.getCarouselCards()', error);
          });
      } else {
        getTrendingCards(this.multilingualContent)
          .then(response => {
            this.setState({
              cards: response
            });
          })
          .catch(error => {
            console.error('ERROR!!! in CardCarousels.componentDidMount.getTrendingCards()', error);
          });
      }
    }
  }

  componentWillUnmount() {
    this.discoverPagePerfChngs =
      window.ldclient.variation('discover-page-perf-changes', 'OFF') === 'V2-WITH-CACHING';
    if (this.discoverPagePerfChngs && this.state.cards && this.state.cards.length) {
      this.props.dispatch({
        type: 'get_all_discover_carousel',
        data: this.state.cards,
        carosuel: this.props.carousel,
        isCustomCarousels: false
      });
    }
  }

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  render() {
    let cards = this.state.cards;
    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;

    if (this.props.isCustomCarousel && !(Array.isArray(cards) && cards.length)) {
      cards =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }
    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div className={!!cards && cards.length == 0 ? 'hidden' : ''}>
        {(!cards || (Array.isArray(cards) && !!cards.length)) && (
          <div className="discover-title">
            <div className="small-12 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(
                   carousel.label || carousel.defaultLabel || 'TRENDING CONTENT'
                )}
              </div>
            </div>
          </div>
        )}
        {Array.isArray(cards) && !!cards.length && (
          <div className="discovery-contents">
            <div className="small-12 columns discover-card-wrapper">
              <div className="discover-card-wrapper-inner discover-card-width">
                <Carousel
                  isDiscovery={true}
                  isDiscovery_card={true}
                  slidesToShow={3}
                  key="trending"
                >
                  {cards.map((card, index) => {
                    let updatedCard =
                      this.props.cards &&
                      this.props.cards.get(card.id) &&
                      this.props.cards.get(card.id).toJS();
                    let displayCard = updatedCard || card;
                    return (
                      <div key={displayCard.id}>
                        <Card
                          card={displayCard}
                          author={displayCard.author}
                          dueAt={
                            displayCard.dueAt ||
                            (displayCard.assignment && displayCard.assignment.dueAt)
                          }
                          startDate={
                            displayCard.startDate ||
                            (displayCard.assignment && displayCard.assignment.startDate)
                          }
                          removeCardFromList={this.removeCardFromList}
                          tooltipPosition="top-center"
                          moreCards={true}
                          openChannelModal={this.openChannelModal}
                          discoverUpshotEventSend={this.discoverUpshotEventSend}
                        />
                      </div>
                    );
                  })}
                </Carousel>
              </div>
            </div>
          </div>
        )}
        {!cards && (
          <div>
            <div className="small-12 columns empty-state">
              <CardLoader loadingCards={5} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

CardCarousels.propTypes = {
  discovery: PropTypes.object,
  cards: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any,
  isOldConfigCarousel: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    cards: state.cards
  };
}

export default connect(mapStoreStateToProps)(CardCarousels);
