import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Carousel from '../../common/Carousel';
import { ProviderLoader } from '../../common/EmptyStateLoaders';
import Provider from '../Provider.jsx';
import * as upshotActions from '../../../actions/upshotActions';

class FeatureProviderCarousles extends Component {
  constructor(props, context) {
    super(props, context);

    let carousel = this.props.carousel;
    let carouselUID = carousel.uid;
    let providers = null;

    providers =
      this.props.discovery &&
      this.props.discovery.get('carousels') &&
      this.props.discovery.get('carousels')[carouselUID] &&
      this.props.discovery.get('carousels')[carouselUID].get('data');

    this.upshotEnabled = window.ldclient.variation('upshot-ai-integration', false);

    this.state = {
      providers: providers || null
    };
  }

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  goToProvider = (e, providerLink, providerName) => {
    e.preventDefault();
    if (this.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['FEATURED_CONTENT_PROVIDER'], {
        providerName: providerName,
        event: 'Featured Provider'
      });
      this.discoverUpshotEventSend.bind(this, {
        screenName: 'Learn',
        category: 'Featured Provider',
        name: providerName,
        event: 'Accessed Featured Provider'
      });
    }
    this.props.dispatch(push(providerLink));
  };

  getFilteredProviders = (providers, category) => {
    if (category === 'default') {
      return providers.filter(function(fp) {
        return fp.categories && fp.categories.length == 0;
      });
    } else {
      return providers.filter(function(fp) {
        return fp.categories.indexOf(category) !== -1;
      });
    }
  };

  render() {
    let carousel = this.props.carousel;
    let providers = this.state.providers;
    let carouselUID = 'discover/featuredProviders';

    if (
      (this.props.isCustomCarousel && !(Array.isArray(providers) && providers.length)) ||
      !!this.props.isAPIDisabled
    ) {
      providers =
        this.props.discovery &&
        this.props.discovery.get('carousels') &&
        this.props.discovery.get('carousels')[carouselUID] &&
        this.props.discovery.get('carousels')[carouselUID].get('data');
    }
    if (!!providers) {
      let category = this.props.carousel.category || 'default';
      providers = this.getFilteredProviders(providers, category);
    }
    let isShowCustomLabels = this.props.isShowCustomLabels;
    let profileLanguage = this.props.profileLanguage;
    let translatedLabel =
      isShowCustomLabels &&
      carousel.languages &&
      carousel.languages[profileLanguage] &&
      carousel.languages[profileLanguage].trim();

    return (
      <div className={!!providers && providers.length == 0 ? 'hidden' : ''}>
        {(!providers || (Array.isArray(providers) && !!providers.length)) && (
          <div className="discover-title">
            <div className="small-12 columns">
              <div role="heading" aria-level="3">
                {translatedLabel || tr(carousel.label || carousel.defaultLabel || 'FEATURED PROVIDERS')}
              </div>
            </div>
          </div>
        )}
        {Array.isArray(providers) && !!providers.length && (
          <div className="discovery-contents">
            <div className="small-12 columns discover-card-wrapper">
              <div className="discover-card-wrapper-inner discover-card-width">
                <Carousel
                  isDiscovery={true}
                  isDiscovery_card={true}
                  slidesToShow={3}
                  key="trending"
                >
                  {providers.map((provider, index) => {
                    let providerLink = `/discover/${provider.display_name
                      .toLowerCase()
                      .replace(/\s/g, '-')}`;
                    if (
                      provider.source_type &&
                      (provider.source_type.name == 'mango_languages' ||
                        provider.source_type.category == 'deeplink')
                    ) {
                      if (provider.source_type.category == 'deeplink') {
                        providerLink = `${provider.source_config['deep_link_url']}`;
                      } else {
                        providerLink = `${
                          provider.source_config['api-key']
                        }?user_id=${this.props.currentUser.get(
                          'id'
                        )}&email=${this.props.currentUser.get(
                          'email'
                        )}&first_name=${this.props.currentUser.get(
                          'first_name'
                        )}&last_name=${this.props.currentUser.get('last_name')}`;
                      }
                      return (
                        <div key={provider.id}>
                          <a className="provider" target="_blank" href={providerLink}>
                            <Provider
                              providerName={provider.display_name}
                              logoUrl={provider.logo_url || provider.source_type.image_url}
                            />
                          </a>
                        </div>
                      );
                    } else {
                      return (
                        <div key={provider.id}>
                          <a
                            aria-label={tr(`Visit the catalog page for provider, %{provider_name}`, {provider_name: `${provider.display_name}`})}
                            href="#"
                            className="provider"
                            onClick={e => this.goToProvider(e, providerLink, provider.display_name)}
                          >
                            <Provider
                              providerName={provider.display_name}
                              logoUrl={provider.logo_url || provider.source_type.image_url}
                            />
                          </a>
                        </div>
                      );
                    }
                  })}
                </Carousel>
              </div>
            </div>
          </div>
        )}
        {!providers && (
          <div>
            <div className="small-12 columns empty-state">
              <ProviderLoader loadingCards={4} />
            </div>
          </div>
        )}
      </div>
    );
  }
}

FeatureProviderCarousles.propTypes = {
  discovery: PropTypes.object,
  currentUser: PropTypes.object,
  carousel: PropTypes.object,
  isCustomCarousel: PropTypes.bool,
  isShowCustomLabels: PropTypes.bool,
  profileLanguage: PropTypes.any,
  isAPIDisabled: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    currentUser: state.currentUser
  };
}

export default connect(mapStoreStateToProps)(FeatureProviderCarousles);
