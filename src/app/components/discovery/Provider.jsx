import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';

class Provider extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    let logoUrl = this.props.logoUrl ? this.props.logoUrl : ''; // getting the logo from passed component props
    return (
      <div className="provider">
        <Paper>
          <div className="provider-wrapper">
            <div className="provider-logo-container">
              <img
                alt={`Visit the catalog page for provider, ${this.props.providerName}`}
                src={logoUrl}
              />
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

Provider.propTypes = {
  logoUrl: PropTypes.string,
  providerName: PropTypes.string
};

export default connect()(Provider);
