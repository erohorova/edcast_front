import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PlayIcon from 'material-ui/svg-icons/av/play-circle-outline';
import Paper from 'edc-web-sdk/components/Paper';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import linkPrefix from '../../utils/linkPrefix';

class Video extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false)
    };

    this.styles = {
      paper: {
        minHeight: '247px'
      }
    };
  }

  cardLinkClick = e => {
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Video',
        name: this.props.videoCard.title,
        event: 'Video Clicked'
      });
    }
    e.preventDefault();
    let linkPrefixValue = linkPrefix(this.props.videoCard.cardType);
    window.history.pushState(null, null, `/${linkPrefixValue}/${this.props.videoCard.slug}`);
    this.props.dispatch(openStandaloneOverviewModal(this.props.videoCard, 'insights', {}));
  };

  cardTitle = () => {
    if (
      this.props.videoCard.cardType == 'video_stream' &&
      this.props.videoCard &&
      this.props.videoCard.title
    ) {
      return this.props.videoCard.title;
    } else if (this.props.videoCard.resource && this.props.videoCard.resource.title) {
      return this.props.videoCard.resource.title;
    } else if (this.props.videoCard.message) {
      return this.props.videoCard.message;
    } else {
      return '';
    }
  };

  cardImageUrl = () => {
    if (
      this.props.videoCard.cardType == 'video_stream' &&
      this.props.videoCard.videoStream &&
      this.props.videoCard.videoStream.imageUrl
    ) {
      return this.props.videoCard.videoStream.imageUrl;
    } else if (this.props.videoCard.resource && this.props.videoCard.resource.imageUrl) {
      return this.props.videoCard.resource.imageUrl;
    } else if (this.props.videoCard.filestack) {
      let fileStack = this.props.videoCard && this.props.videoCard.filestack;
      let imageUrl =
        fileStack.length > 1
          ? (fileStack[0] &&
              fileStack[0].mimetype &&
              fileStack[0].mimetype.includes('video/') &&
              (fileStack[1] && fileStack[1].url)) ||
            (fileStack[0] && fileStack[0].thumbnail)
          : fileStack[0] && fileStack[0].thumbnail;
      return (
        imageUrl ||
        'https://dp598loym07sk.cloudfront.net/channels/mobile_images/000/003/492/original/default_banner_image_min-9164e115397bff8e5b0fe8746a78941a.jpg'
      );
    } else {
      return '';
    }
  };

  cardStatus = () => {
    if (this.props.videoCard.cardType == 'video_stream') {
      return this.props.videoCard.videoStream.status;
    } else {
      return '';
    }
  };

  cardAuthorImage = () => {
    if (this.props.videoCard.author) {
      return this.props.videoCard.author.avatarimages.small;
    } else {
      return '';
    }
  };

  cardAuthorHandle = () => {
    if (this.props.videoCard.author) {
      return this.props.videoCard.author.handle;
    } else {
      return '\u00a0';
    }
  };

  render() {
    let title = this.cardTitle();
    let imageUrl = this.cardImageUrl();
    let status = this.cardStatus();
    let authorImage = this.cardAuthorImage();
    let authorHandle = this.cardAuthorHandle();

    return (
      <div className="videostream">
        <Paper style={this.styles.paper}>
          <a href="#" onClick={this.cardLinkClick}>
            <div className="stream-image">
              <div className="image" style={{ backgroundImage: 'url(' + imageUrl + ')' }}>
                <PlayIcon
                  className="play-icon"
                  style={{
                    fill: 'white',
                    top: '50%',
                    left: '50%',
                    width: '50px',
                    height: '50px',
                    position: 'absolute',
                    transform: 'translate(-50%, -50%)'
                  }}
                />
                {status.toLowerCase() === 'live' && (
                  <small className="live-label">{tr(status)}</small>
                )}
              </div>
            </div>
          </a>
          <div className={!authorImage ? 'stream-from-cms' : ''}>
            <ListItem
              disabled
              leftAvatar={
                authorImage ? (
                  <a href={`/${authorHandle}`}>
                    <Avatar size={40} src={authorImage} />
                  </a>
                ) : (
                  undefined
                )
              }
              primaryText={
                <a onClick={this.cardLinkClick} className="matte">
                  <div className="avatar-name">{title}</div>
                </a>
              }
            />
          </div>
        </Paper>
      </div>
    );
  }
}

Video.propTypes = {
  videoCard: PropTypes.object,
  discoverUpshotEventSend: PropTypes.func
};

export default connect()(Video);
