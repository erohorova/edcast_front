import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { openPathwayOverviewModal } from '../../actions/modalActions';
import * as logoType from '../../constants/logoTypes';
import * as filestack from '../../utils/filestack';
import linkPrefix from '../../utils/linkPrefix';
import SvgImageResized from '../common/ImageResized';
import unescape from 'lodash/unescape';
import { saveConsumptionPathwayHistoryURL } from '../../actions/pathwaysActions';

class Pathway extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {};

    this.state = {
      card: this.props.card,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      isStandalonePage: window.ldclient.variation('card-click-handle', 'modal'),
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false)
    };
  }
  standaloneLinkClickHandler = card => {
    let linkPrefixValue = linkPrefix(card.cardType);
    if (
      this.props.pathname === `/${linkPrefixValue}/${card.slug}` ||
      this.props.pathname === `/${linkPrefixValue}/${card.id}`
    ) {
      return;
    }
    let pathwayBackUrl = window.location.pathname;
    window.history.replaceState(null, null, `/discover#pathwayCarousel`);
    if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
      this.props.dispatch(saveConsumptionPathwayHistoryURL(pathwayBackUrl));
    }
    this.props.dispatch(push(`/${linkPrefixValue}/${card.slug}`));
  };
  showPathway = (e, slug) => {
    e.preventDefault();
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Pathway',
        name: slug,
        event: 'Pathway Clicked'
      });
    }
    const logoObj = logoType.LOGO;
    const defaultImage = `(/i/images/courses/course${Math.floor(Math.random() * 48) + 1}.jpg)`;
    if (this.state.isStandalonePage.toLowerCase().indexOf('standalone') >= 0) {
      this.standaloneLinkClickHandler(this.props.card);
    } else {
      this.props.dispatch(
        openPathwayOverviewModal(this.state.card, logoObj, defaultImage, null, null, null, null)
      );
    }
  };

  render() {
    return (
      <div className="pathway">
        <a
          aria-label={tr(`Visit the featured pathway, %{title}`, {title: `${this.props.title}`})}
          href="#"
          onClick={e => this.showPathway(e, this.props.slug)}
        >
          <Paper>
            <div className="stream-image">
              <div className="image">
                <svg width="100%" height="100%">
                  <SvgImageResized
                    cardId={`${this.props.card.id}`}
                    height="100%"
                    width="100%"
                    resizeOptions={'height:178'}
                    xlinkHref={this.props.imageUrl}
                  />
                </svg>
              </div>
            </div>
            <div className="info">
              <div className="title">{unescape(this.props.title)}</div>
              <div className="clearfix">
                <small className="card-count">
                  {this.props.cardsCount} {tr('Items')}
                </small>
              </div>
            </div>
          </Paper>
        </a>
      </div>
    );
  }
}

Pathway.propTypes = {
  card: PropTypes.object,
  id: PropTypes.number,
  title: PropTypes.string,
  imageUrl: PropTypes.string,
  pathname: PropTypes.string,
  slug: PropTypes.string,
  cardsCount: PropTypes.number,
  discoverUpshotEventSend: PropTypes.func
};

export default connect()(Pathway);
