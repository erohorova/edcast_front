import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import ListItem from 'material-ui/List/ListItem';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { follow, unfollow } from 'edc-web-sdk/requests/users.v2';
import TooltipLabel from '../common/TooltipLabel';
import UserBadgesContainer from '../../components/common/UserBadgesContainer';
import { tr } from 'edc-web-sdk/helpers/translations';
import { toggleTeamAdmin } from 'edc-web-sdk/requests/teams';
import IconButton from 'material-ui/IconButton/IconButton';
import MoreIcon from 'material-ui/svg-icons/navigation/more-horiz';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import colors from 'edc-web-sdk/components/colors/index';
import * as actionTypes from '../../constants/actionTypes';
import { updateFollowingUsersCount } from '../../actions/currentUserActions';
import { Permissions } from '../../utils/checkPermissions';
import { push } from 'react-router-redux';
import * as upshotActions from '../../actions/upshotActions';

class Influencer extends Component {
  constructor(props, context) {
    super(props, context);
    this.followClickHandler = this.followClickHandler.bind(this);
    this.getLink = this.getLink.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.state = {
      pending: false,
      following: this.props.following,
      role: this.props.roles && this.props.roles.length && this.props.roles[0],
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false)
    };
    this.styles = {
      badge: {
        position: 'absolute',
        left: 0,
        top: 0,
        padding: '1px'
      },
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '14px',
        fontWeight: 300,
        lineHeight: 1.86,
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '26px'
      }
    };
  }

  followClickHandler() {
    let action = this.state.following ? unfollow : follow;
    this.setState({ pending: true });
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        buttonName: this.state.following ? 'Follow' : 'Unfollow',
        category: 'User',
        name: this.props.title,
        event: this.state.following ? 'Follow Clicked' : 'Unfollow Clicked'
      });
    }
    action(this.props.id)
      .then(() => {
        if (this.props.updateDiscoverStore) {
          let updatedChannel = {
            expertSkills: this.props.expertSkills,
            handle: this.props.handle,
            id: this.props.id,
            isFollowing: !this.state.following,
            name: this.props.name,
            roles: this.props.roles,
            avatarimages: {
              medium: this.props.imageUrl
            },
            rolesDefaultNames: this.props.rolesDefaultNames
          };
          updatedChannel['isFollowing'] = !this.state.isFollowing;
          this.props.updateDiscoverStore(updatedChannel);
        }
        this.setState({ pending: false, following: !this.state.following }, () => {
          this.props.dispatch(updateFollowingUsersCount(this.state.following));
          if (this.state.upshotEnabled) {
            upshotActions.sendCustomEvent(
              window.UPSHOTEVENT[this.state.isFollowing ? 'FOLLOW' : 'UNFOLLOW'],
              {
                eventAction: this.state.following ? 'Follow' : 'Unfollow',
                event: 'User'
              }
            );
          }
        });
      })
      .catch(err => {
        console.error(`Error in Influencer.followClickHandler.func : ${err}`);
      });
  }

  getLink(e) {
    e.preventDefault();
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'User',
        name: this.props.title,
        event: 'User Open'
      });
    }
    if (this.props.disableLink) {
      return;
    } else {
      const currentUserHandle = this.props.currentUser.handle;
      this.props.dispatch(
        push(
          `/${this.props.handle.replace('@', '') === currentUserHandle ? 'me' : this.props.handle}`
        )
      );
    }
  }

  getStyle() {
    if (this.props.disableLink) {
      return { cursor: 'default' };
    } else {
      return { cursor: 'pointer' };
    }
  }

  handleToggleTeamAdmin = (teamID, UserID) => {
    let _this = this;
    toggleTeamAdmin(teamID, UserID)
      .then(data => {
        this.setState({ role: data.user.role });
        _this.props.dispatch({
          type: actionTypes.UPDATE_MEMBER,
          member: data.user
        });
        _this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: data.user.role == 'admin' ? 'Promoted to Group Leader' : 'Demoted to Member',
          autoClose: true
        });
      })
      .catch(err => {
        _this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating!',
          autoClose: true
        });
        console.error(err);
      });
  };

  render() {
    let extra_info =
      this.props.interests != undefined && this.props.interests.length > 0
        ? this.props.interests.join(', ')
        : this.props.handle;

    return (
      <div className="influencer">
        <Paper>
          <div className="influencer-wrapper">
            <ListItem disabled>
              {this.props.roles && (
                <div style={this.styles.badge}>
                  <UserBadgesContainer
                    rolesDefaultNames={this.props.rolesDefaultNames}
                    roles={this.props.roles}
                  />
                </div>
              )}
              {this.props.teamID &&
                !this.props.isCurrentUser &&
                this.props.isTeamAdmin &&
                !this.props.pending && (
                  <IconMenu
                    className="insight-dropdown"
                    style={{ position: 'absolute', top: 0, right: 0 }}
                    iconButtonElement={
                      <IconButton aria-label={tr('more')} style={this.props.style || {}}>
                        <MoreIcon color={colors.gray} />
                      </IconButton>
                    }
                    anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
                    targetOrigin={{ horizontal: 'right', vertical: 'top' }}
                    listStyle={this.styles.iconMenu}
                    menuStyle={this.styles.menuItem}
                  >
                    <MenuItem
                      style={this.styles.menuItem}
                      primaryText={tr(
                        this.state.role == 'admin' ? 'Demote to Member' : 'Promote to Group Leader'
                      )}
                      className=""
                      onTouchTap={this.handleToggleTeamAdmin.bind(
                        this,
                        this.props.teamID,
                        this.props.id
                      )}
                    />
                  </IconMenu>
                )}
              <a
                aria-label={tr(`Visit the profile of %{name}`, {name: `${this.props.name}`})}
                href="#"
                className="matte align-avtar"
                onClick={this.getLink}
                style={this.getStyle()}
              >
                <div
                  className="influencer-avatar"
                  style={{
                    backgroundImage: 'url(' + this.props.imageUrl + ')',
                    margin: '20px auto'
                  }}
                />
                <div className="influencer-info text-overflow">
                  <div className="influencer-name">{this.props.name}</div>
                  <div className="influencer-name">&nbsp;</div>
                </div>
              </a>
              {!this.props.pending &&
                this.props.handle &&
                this.props.currentUser.id != this.props.id &&
                !Permissions.has('DISABLE_USER_FOLLOW') && (
                  <FollowButton
                    onTouchTap={this.followClickHandler}
                    className="follow"
                    label={tr(this.state.following ? 'Following' : 'Follow')}
                    hoverLabel={tr(this.state.following ? 'Unfollow' : '')}
                    pendingLabel={tr(this.state.following ? 'Unfollowing...' : 'Following...')}
                    pending={this.state.pending}
                    following={this.state.following}
                  />
                )}
            </ListItem>
          </div>
        </Paper>
      </div>
    );
  }
}

Influencer.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  handle: PropTypes.string,
  imageUrl: PropTypes.string,
  teamID: PropTypes.string,
  disableLink: PropTypes.bool,
  isTeamAdmin: PropTypes.bool,
  following: PropTypes.bool,
  pending: PropTypes.bool,
  roles: PropTypes.array,
  rolesDefaultNames: PropTypes.array,
  expertSkills: PropTypes.array,
  currentUser: PropTypes.object,
  style: PropTypes.object,
  isCurrentUser: PropTypes.bool,
  interests: PropTypes.object,
  position: PropTypes.string,
  discoverUpshotEventSend: PropTypes.func,
  title: PropTypes.any,
  updateDiscoverStore: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(Influencer);
