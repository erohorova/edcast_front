import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { tr } from 'edc-web-sdk/helpers/translations';
import isEmpty from 'lodash/isEmpty';
import find from 'lodash/find';

import { getRecommendedChannels } from 'edc-web-sdk/requests/channels';
import { getRecommendedUsers, getUserById } from 'edc-web-sdk/requests/users';
import { getTrendingCards, getCarouselCards } from 'edc-web-sdk/requests/cards';
import * as carouselSdk from 'edc-web-sdk/requests/carousels';
import { getInitData } from '../../actions/discoveryActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';

import DiscoverCard from '../common/Card.jsx';
import Carousel from '../common/Carousel';
import Search from '../TopNav/Search';
import Card from '../cards/Card';
import getDefaultImage from '../../utils/getDefaultCardImage';
import { langs } from '../../constants/languages';
import { cardFields } from '../../constants/cardFields';
import * as filestack from '../../utils/filestack';
import updatePageLastVisit from '../../utils/updatePageLastVisit';

import EmptyBlock from './EmptyBlock.jsx';
import Channel from './Channel.jsx';
import Course from './Course.jsx';
import Video from './Video.jsx';
import Influencer from './Influencer.jsx';
import UserSquareItemV2 from '../common/UserSquareItemV2';
import Provider from './Provider.jsx';
import Pathway from './Pathway.jsx';
import Journey from './Journey.jsx';
import * as upshotActions from '../../actions/upshotActions';

class DiscoveryContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      openSearch: false,
      providerId: '',
      channels: [],
      users: [],
      cards: [],
      loadingTrendingContent: true,
      loadingChannels: true,
      carouselOutput: [],
      carouselCards: {},
      openTooltipChanel: false,
      privateRemovedIds: [],
      onlySmeOnPeopleCarousel: !!(
        props.team &&
        props.team.config &&
        props.team.config['enable_only_sme_on_people_carousel']
      ),
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      isCardV3: window.ldclient.variation('card-v3', false),
      isPeopleCardV3: window.ldclient.variation('discover-people-card-v3', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    this.styles = {
      channel: {
        borderRadius: '4px'
      },
      customRow: {
        width: '100%',
        maxWidth: 'none'
      }
    };
    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
    this.openSearch = this.openSearch.bind(this);
    this.toggleParentState = this.toggleParentState.bind(this);

    if (this.state.showNew) {
      updatePageLastVisit('discoveryLastVisit');
    }
  }

  componentDidMount() {
    let userInfoCallBack = getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    let _this = this;
    this.props
      .dispatch(userInfoCallBack)
      .then(async () => {
        let allPromises = [];
        let initData = [];
        let userData;
        userData = await getUserById(this.props.currentUser.id);
        let teams = userData.userTeams;
        let team_ids = teams.map(team => {
          return team.teamId;
        });
        if (team_ids.length == 0) {
          team_ids.push('0');
        }
        let payload = {
          is_featured: true,
          'team_ids[]': team_ids
        };

        initData = getInitData(payload, this.props.team.Discover);
        allPromises = [...initData, ...allPromises];

        if (
          this.props.team.Discover &&
          this.props.team.Discover['discover/channels'] &&
          this.props.team.Discover['discover/channels'].visible
        ) {
          allPromises.push(
            getRecommendedChannels({ limit: 50 })
              .then(data => {
                if (isEmpty(data)) {
                  return null;
                } else {
                  return () => {
                    this.setState({ channels: data.channels, loadingChannels: false });
                  };
                }
              })
              .catch(error => {
                return null;
              })
          );
        }
        if (
          this.props.team.Discover &&
          this.props.team.Discover['discover/users'] &&
          this.props.team.Discover['discover/users'].visible
        ) {
          let team_users_only =
            this.props.team.config &&
            this.props.team.config['enable_only_team_users_on_people_carousel'];

          if (team_users_only == undefined) {
            team_users_only = true;
          }

          allPromises.push(
            getRecommendedUsers({ limit: 50, my_teams_users: team_users_only })
              .then(users => {
                if (isEmpty(users)) {
                  return null;
                } else {
                  return () => {
                    this.setState({ users: users });
                  };
                }
              })
              .catch(error => {
                return null;
              })
          );
        }
        if (
          this.props.team.Discover &&
          this.props.team.Discover['discover/trending'] &&
          this.props.team.Discover['discover/trending'].visible
        ) {
          allPromises.push(
            getTrendingCards({
              filter_by_language: this.state.multilingualContent,
              last_access_at: localStorage.getItem('discoveryLastVisit'),
              fields: cardFields
            })
              .then(cards => {
                if (isEmpty(cards)) {
                  return null;
                } else {
                  return () => {
                    this.setState({ cards: cards, loadingTrendingContent: false });
                  };
                }
              })
              .catch(error => {
                return null;
              })
          );
        }

        // getting carousel content
        let DiscoverController = this.listDiscover(this.props.team.Discover).filter(
          item => item.visible
        );
        DiscoverController.map(item => {
          let customChannelCarouselIdx = item.key.indexOf(
            'discover/carousel/customCarousel/channel'
          );
          let customCardCarouselIdx = item.key.indexOf('discover/carousel/customCarousel/card');
          let customUserCarouselIdx = item.key.indexOf('discover/carousel/customCarousel/user');
          let carouselIdx = item.key.indexOf('discover/carousel/');
          let carouseItemsCall;
          if (
            customChannelCarouselIdx !== -1 ||
            customCardCarouselIdx !== -1 ||
            customUserCarouselIdx !== -1
          ) {
            let customCarouselArr = item.key.split('/');
            let customCarouselId = customCarouselArr[customCarouselArr.length - 1];
            let customCarouselCards = this.state.carouselCards;
            carouseItemsCall = carouselSdk
              .getCarouselItems(customCarouselId, {
                filter_by_language: this.state.multilingualContent
              })
              .then(data => {
                if (isEmpty(data)) {
                  return null;
                } else {
                  return () => {
                    customCarouselCards[customCarouselId] = data.structuredItems;
                    this.setState({
                      carouselCards: customCarouselCards
                    });
                  };
                }
              })
              .catch(error => {
                return null;
              });
            allPromises.push(carouseItemsCall);
          } else if (carouselIdx !== -1) {
            let carouselArr = item.key.split('/');
            carouseItemsCall = getCarouselCards(carouselArr[carouselArr.length - 1], {
              filter_by_language: this.state.multilingualContent,
              last_access_at: localStorage.getItem('discoveryLastVisit'),
              fields: cardFields
            })
              .then(cards => {
                if (isEmpty(cards)) {
                  return null;
                } else {
                  return () => {
                    let carouselCards = this.state.carouselCards;
                    carouselCards[carouselArr[carouselArr.length - 1]] = cards;
                    this.setState({
                      carouselCards: carouselCards
                    });
                  };
                }
              })
              .catch(error => {
                return () => {
                  let carouselCards = this.state.carouselCards;
                  carouselCards[carouselArr[carouselArr.length - 1]] = [];
                  this.setState({
                    carouselCards: carouselCards
                  });
                };
              });

            allPromises.push(carouseItemsCall);
          }
        });
        for (let i = 0; i < allPromises.length; i++) {
          (await allPromises[i]) && _this.props.dispatch(await allPromises[i]);
        }
      })
      .catch(err => {
        console.error(`Error in DiscoveryContainer.componentDidMount.func: ${err}`);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn === false) {
      this.props.dispatch(push('/log_in'));
    }
  }

  componentWillUnmount() {
    this.state.showNew &&
      localStorage.setItem('discoveryLastVisit', Math.round(new Date().getTime() / 1000));
  }

  discoverUpshotEventSend(payload) {
    upshotActions.sendCustomEvent(window.UPSHOTEVENT['CLICK_EXPLORE_LEARN'], payload);
  }

  openSearch(providerId) {
    this.setState({ openSearch: true, providerId: providerId });
  }

  toggleParentState() {
    this.setState({ openSearch: false, providerId: '' });
  }

  removeCardFromList = cardId => {
    let index = -1;
    let cards = this.state.cards;

    for (var i = 0; i < this.state.cards.length; i++) {
      if (this.state.cards[i].id == cardId) {
        index = i;
      }
    }

    if (index > -1) {
      cards.splice(index, 1);
      this.setState({ cards: cards });
    }
  };

  getFilteredProviders = (providers, category) => {
    if (category === 'default') {
      return providers;
    } else {
      return providers.filter(function(fp) {
        return fp.categories.includes(category);
      });
    }
  };

  createEnabledOption = option => {
    let title, viewAllLink, type, collection, viewAllText;
    switch (option) {
      case 'channels':
        title = 'CHANNELS';
        viewAllLink = '/discover/channels';
        viewAllText = 'Channels';
        break;
      case 'users':
        title = 'SUBJECT MATTER EXPERTS / INFLUENCERS';
        viewAllLink = '/me/team';
        viewAllText = 'Users';
        break;
      case 'pathways':
        title = 'PATHWAYS';
        viewAllLink = '/discover/pathways';
        viewAllText = 'Pathways';
        break;
      case 'journeys':
        title = 'JOURNEYS';
        viewAllLink = '/discover/journeys';
        viewAllText = 'Journeys';
        break;
      case 'courses':
        title = 'COURSES';
        viewAllLink = '/discover/courses';
        viewAllText = 'Courses';
        break;
      default:
        title = '';
        viewAllLink = '/';
    }
    return { title, viewAllLink, type: option, viewAllText };
  };

  pathwayComp = (pathway, id, title, imageUrl, slug, cardsCount) => {
    return (
      <Pathway
        card={pathway}
        id={id}
        title={title}
        imageUrl={imageUrl}
        slug={slug}
        cardsCount={cardsCount}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
      />
    );
  };

  journeyComp = (journey, id, title, imageUrl, slug, cardsCount) => {
    return (
      <Journey
        card={journey}
        id={id}
        title={title}
        imageUrl={imageUrl}
        slug={slug}
        cardsCount={cardsCount}
      />
    );
  };

  showChanelTooltip = (label, event) => {
    let coord = this.getAnchorPosition(event.currentTarget);
    this.setState(
      {
        openTooltipChanel: true,
        channelTooltip: label
      },
      () => {
        if (this._discoveryTooltip) {
          this._discoveryTooltip.style.left = `${Math.max(0, coord.left - 15)}px`;
        }
      }
    );
  };
  hideChanelTooltip = () => {
    this.setState({
      openTooltipChanel: false
    });
  };
  getAnchorPosition = el => {
    let rect = el.getBoundingClientRect();
    let a = {
      top: rect.top,
      left: rect.left,
      width: el.offsetWidth,
      height: el.offsetHeight
    };
    a.bottom = rect.bottom || a.top + a.height;
    a.middle = a.left + (a.right - a.left) / 2;
    a.center = a.top + (a.bottom - a.top) / 2;
    return a;
  };

  addPrivateIds = id => {
    let privateRemovedIds = this.state.privateRemovedIds;
    privateRemovedIds.push(id);
    this.setState({
      privateRemovedIds
    });
  };

  channelComp = (
    id,
    channel,
    following,
    followPending,
    title,
    imageUrl,
    slug,
    allowFollow,
    isChannelPrivate,
    channelShowLockIcon
  ) => {
    let smallImageUrl =
      imageUrl &&
      (~imageUrl.indexOf('default_banner_image')
        ? imageUrl
        : filestack.getResizedUrl(imageUrl, 'height:107'));
    return (
      <Channel
        id={id}
        isFollowing={following}
        title={title}
        channel={channel}
        titleMouseEnter={this.showChanelTooltip.bind(this, title)}
        titleMouseLeave={this.hideChanelTooltip}
        imageUrl={smallImageUrl}
        slug={slug}
        borderRadius={this.styles.channel}
        allowFollow={allowFollow}
        isPrivate={isChannelPrivate}
        addPrivateIds={this.addPrivateIds.bind(this, id)}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
      />
    );
  };

  courseComp = (name, logoUrl, startDate, endDate, status, url) => {
    return (
      <Course
        name={name}
        logoUrl={logoUrl}
        startDate={startDate}
        endDate={endDate}
        status={status}
        url={url}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
      />
    );
  };

  userComp = (
    user,
    id,
    isFollowing,
    followPending,
    name,
    handle,
    imageUrl,
    expertSkills,
    roles,
    rolesDefaultNames,
    position
  ) => {
    user['isComplete'] = true;
    return this.state.isPeopleCardV3 ? (
      <UserSquareItemV2 key={`team_user_${id}`} user={user} source={'discoveryCarousel'} />
    ) : (
      <Influencer
        id={id}
        name={name}
        handle={handle}
        imageUrl={imageUrl}
        expertSkills={expertSkills}
        roles={roles}
        rolesDefaultNames={rolesDefaultNames}
        position={position}
        following={isFollowing}
        discoverUpshotEventSend={this.discoverUpshotEventSend}
      />
    );
  };

  listDiscover(obj) {
    let DiscoverController = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        DiscoverController.push(listObj);
      });
      DiscoverController.sort((a, b) => a.index - b.index);
    }
    return DiscoverController;
  }

  openTeam = e => {
    e.preventDefault();
    if (this.state.onlySmeOnPeopleCarousel) {
      this.props.dispatch(push(this.state.isNewProfileNavigation ? '/team/sme' : '/me/team/sme'));
    } else {
      this.props.dispatch(push(this.state.isNewProfileNavigation ? '/team' : '/me/team'));
    }
  };

  openChannelPage = e => {
    e.preventDefault();
    this.props.dispatch(push('/channels/all'));
  };
  moreCourses = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };
  viewMoreVideostreamCards = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };
  viewMorePathways = (e, path) => {
    e.preventDefault();
    this.props.dispatch(push(path));
  };
  goToProvider = (e, providerLink, providerName) => {
    e.preventDefault();
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['FEATURED_CONTENT_PROVIDER'], {
        providerName: providerName,
        event: 'Featured Provider'
      });
      this.discoverUpshotEventSend.bind(this, {
        screenName: 'Learn',
        category: 'Featured Provider',
        name: providerName,
        event: 'Accessed Featured Provider'
      });
    }
    this.props.dispatch(push(providerLink));
  };

  render() {
    let _this = this;
    let wwwOrg = document.location.hostname.split('.')[0] === 'www';
    let videos = this.props.videos ? this.props.videos : [];
    let channels = this.state.channels;
    let influencers = this.state.users;
    let numberOfVideos = videos.length;
    let numberOfInfluencers = this.state.users.length;
    let numberOfChannels = this.state.channels.length;
    let courses = this.props.courses || [];
    let numberOfCourses = courses.length;
    let pathways = this.props.pathways.slice(0);
    let journeys = this.props.journeys.slice(0);
    let numberOfPathways = pathways.length;
    let numberOfJourneys = journeys.length;
    let showChannels, showChannelsEmptyState, showChannelsEmptyBlock;
    let showTrendingContent, showFeaturedProvider, showVideos;
    let showInfluencers,
      showInfluencersEmptyState,
      showInfluencersEmptyBlock,
      showTrendingContentEmptyBlock;
    let showPathways, showPathwaysEmptyState, showPathwaysEmptyBlock;
    let showJourneys, showJourneysEmptyState, showJourneysEmptyBlock;
    let showCourses, showCoursesEmptyState, showCoursesEmptyBlock;
    let cards = this.state.cards;
    let cardsCount = this.state.cards.length;
    let counts = {};
    let featuredProviders = this.props.featuredProviders;
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let customRow = this.styles.customRow;

    // if counts are recieved from API and not undefined declaring the value
    if (this.props.counts != undefined) {
      counts = this.props.counts;
    }
    let emptyChannelMsg;
    showTrendingContent = showChannels = showInfluencers = showPathways = showJourneys = showVideos = showCourses = true;

    if (this.props.isAdmin) {
      if (numberOfChannels !== 0) {
        showChannels = true;
      } else {
        showChannels = false;
      }
      if (numberOfInfluencers !== 0) {
        showInfluencers = true;
      } else {
        showInfluencers = false;
      }
      if (numberOfPathways !== 0) {
        showPathways = true;
      } else {
        showPathways = false;
      }
      if (numberOfJourneys !== 0) {
        showJourneys = true;
      } else {
        showJourneys = false;
      }
      if (numberOfCourses !== 0) {
        showCoursesEmptyBlock = numberOfCourses < 5;
      } else {
        showCoursesEmptyState = true;
      }
      if (cardsCount !== 0) {
        showTrendingContent = true;
      } else {
        showTrendingContent = false;
        showTrendingContentEmptyBlock = true;
      }
    } else {
      if (numberOfChannels !== 0) {
        showChannels = true;
      } else {
        showChannels = false;
      }
      if (numberOfPathways !== 0) {
        showPathways = true;
      } else {
        showPathways = false;
      }
      if (numberOfJourneys !== 0) {
        showJourneys = true;
      } else {
        showJourneys = false;
      }
      if (numberOfCourses === 0) {
        showCourses = false;
      }
      if (numberOfInfluencers !== 0) {
        showInfluencers = true;
      } else {
        showInfluencers = false;
      }

      if (cardsCount !== 0) {
        showTrendingContent = true;
      } else {
        showTrendingContent = false;
        showTrendingContentEmptyBlock = true;
      }
    }

    let enabled = [];
    let output = [];

    let DiscoverController = this.listDiscover(this.props.team.Discover);
    let isDownloadContentDisabled =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.content &&
      this.props.team.OrgConfig.content['web/content/disableDownload'].value;
    DiscoverController.filter(item => item.visible).map(item => {
      let carouselIdx = item.key.indexOf('discover/carousel/');
      let customChannelCarouselIdx = item.key.indexOf('discover/carousel/customCarousel/channel');
      let customCardCarouselIdx = item.key.indexOf('discover/carousel/customCarousel/card');
      let customUserCarouselIdx = item.key.indexOf('discover/carousel/customCarousel/user');
      let customSourceCarousel = item.key.indexOf('discover/featuredProviders');
      let virtualLabProvidersCarousel = item.key.indexOf('discover/virtualLabProviders');
      if (carouselIdx !== -1) {
        let carouselArr = item.key.split('/');
        item.uuid = carouselArr[carouselArr.length - 1];
        if (customChannelCarouselIdx !== -1) {
          item.key = 'discover/carousel/customCarousel/channel';
        } else if (customCardCarouselIdx !== -1) {
          item.key = 'discover/carousel/customCarousel/card';
        } else if (customUserCarouselIdx !== -1) {
          item.key = 'discover/carousel/customCarousel/user';
        } else {
          item.key = 'discover/carousel';
        }
      }

      if (customSourceCarousel !== -1 || virtualLabProvidersCarousel !== -1) {
        item.key = 'discover/featuredProviders';
      }
      let translatedLabel =
        this.isShowCustomLabels &&
        item.languages &&
        item.languages[this.profileLanguage] &&
        item.languages[this.profileLanguage].trim();
      switch (item.key) {
        case 'discover/carousel/customCarousel/channel':
          let customChannelCarouselContent = this.state.carouselCards[item.id] &&
            this.state.carouselCards[item.id].length > 0 && (
              <div key={item.id}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel ||
                        (item.defaultLabel && tr(item.defaultLabel.toUpperCase()))}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right" />
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {!this.state.carouselCards[item.id] && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock className="channels" title={tr('Content loading...')} />
                    </div>
                  )}
                  {this.state.carouselCards[item.id] !== undefined &&
                    this.state.carouselCards[item.id].length == 0 && (
                      <div className="small-12 columns empty-state">
                        <EmptyBlock
                          className="channels data-not-available-msg"
                          title={tr('No Content...')}
                        />
                      </div>
                    )}
                  {this.state.carouselCards[item.id] &&
                    this.state.carouselCards[item.id].length > 0 && (
                      <div className="small-12 columns">
                        <Carousel isDiscovery={true} slidesToShow={5} key="channels">
                          {this.state.carouselCards[item.id]
                            .filter(function(obj) {
                              return _this.state.privateRemovedIds.indexOf(obj.entity.id) == -1;
                            })
                            .map(carouselItem => {
                              let channel = carouselItem.entity;
                              let mediumProfileImage =
                                channel.profileImageUrls && channel.profileImageUrls.medium_url;
                              return (
                                <div key={channel.id}>
                                  {this.channelComp(
                                    channel.id,
                                    channel,
                                    channel.isFollowing,
                                    channel.followPending,
                                    channel.label,
                                    mediumProfileImage || channel.bannerImageUrl,
                                    channel.slug,
                                    channel.allowFollow,
                                    channel.isPrivate
                                  )}
                                </div>
                              );
                            })}
                        </Carousel>
                      </div>
                    )}
                </div>
              </div>
            );
          if (this.state.carouselCards[item.id] !== undefined) {
            output.push(customChannelCarouselContent);
          }
          break;
        case 'discover/carousel/customCarousel/card':
          let customCardCarouselContent = this.state.carouselCards[item.id] &&
            this.state.carouselCards[item.id].length > 0 && (
              <div key={item.id}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.defaultLabel && item.defaultLabel.toUpperCase())}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right" />
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {!this.state.carouselCards[item.id] && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock className="channels" title={tr('Content loading...')} />
                    </div>
                  )}
                  {this.state.carouselCards[item.id] !== undefined &&
                    this.state.carouselCards[item.id].length == 0 && (
                      <div className="small-12 columns empty-state">
                        <EmptyBlock
                          className="channels data-not-available-msg"
                          title={tr('No Content...')}
                        />
                      </div>
                    )}

                  {this.state.carouselCards[item.id] &&
                    this.state.carouselCards[item.id].length > 0 && (
                      <div className="small-12 columns">
                        <div className="row discovery-contents" style={customRow}>
                          <div className="small-12 columns discover-card-wrapper">
                            <div className="discover-card-wrapper-inner discover-card-width">
                              <Carousel
                                isDiscovery={true}
                                isDiscovery_card={true}
                                slidesToShow={3}
                                key="cards"
                              >
                                {this.state.carouselCards[item.id].map((card, index) => {
                                  return (
                                    <div key={card.entity.id}>
                                      <Card
                                        card={card.entity}
                                        author={card.entity.author}
                                        dueAt={
                                          card.entity.dueAt ||
                                          (card.entity.assignment && card.entity.assignment.dueAt)
                                        }
                                        startDate={
                                          card.entity.startDate ||
                                          (card.entity.assignment &&
                                            card.entity.assignment.startDate)
                                        }
                                        removeCardFromList={this.removeCardFromList}
                                        tooltipPosition="top-center"
                                        user={this.props.currentUser}
                                        moreCards={true}
                                        discoverUpshotEventSend={this.discoverUpshotEventSend}
                                      />
                                    </div>
                                  );
                                })}
                              </Carousel>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                </div>
              </div>
            );
          if (this.state.carouselCards[item.id] !== undefined) {
            output.push(customCardCarouselContent);
          }
          break;
        case 'discover/carousel/customCarousel/user':
          let userCarousel = this.state.carouselCards[item.id] &&
            this.state.carouselCards[item.id].length > 0 && (
              <div name="influencers" key={`${item.key}${item.id}`}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.defaultLabel && item.defaultLabel.toUpperCase())}
                    </div>
                  </div>
                  {this.props.team.OrgConfig.profile['web/profile/teams'].visible && (
                    <div className="small-6 columns">
                      <div className="text-right">
                        <small>
                          <a href="#" className="matte" onClick={this.openTeam}>
                            {tr('View More')}
                          </a>
                        </small>
                      </div>
                    </div>
                  )}
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {!this.state.carouselCards[item.id] && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock className="channels" title={tr('Content loading...')} />
                    </div>
                  )}
                  {this.state.carouselCards[item.id] &&
                    this.state.carouselCards[item.id] !== undefined &&
                    this.state.carouselCards[item.id].length == 0 && (
                      <div className="small-12 columns empty-state">
                        <EmptyBlock
                          className="channels data-not-available-msg"
                          title={tr('No Content...')}
                        />
                      </div>
                    )}
                  <div className="small-12 columns">
                    {this.state.carouselCards[item.id] &&
                      this.state.carouselCards[item.id].length > 0 && (
                        <Carousel
                          isDiscovery={true}
                          isPeopleCarouselV3={this.state.isPeopleCardV3}
                          slidesToShow={5}
                          key={item.id}
                        >
                          {this.state.carouselCards[item.id]
                            .filter(i => i.entity && i.entity.status === 'active')
                            .map((u, index) => {
                              let user = u.entity;
                              let position = 'top';
                              return (
                                <div
                                  key={`${user.id}${index}`}
                                  style={this.state.isPeopleCardV3 ? this.styles.newPeopleCard : ''}
                                >
                                  {this.userComp(
                                    user,
                                    user.id,
                                    user.isFollowing,
                                    user.followPending,
                                    user.name,
                                    user.handle,
                                    (user.avatarimages && user.avatarimages.medium) ||
                                      defaultUserImage,
                                    user.expertSkills,
                                    (user.roles !== undefined && user.roles) || ['influencer'],
                                    (user.rolesDefaultNames !== undefined &&
                                      user.rolesDefaultNames) || ['influencer'],
                                    position
                                  )}
                                </div>
                              );
                            })}
                        </Carousel>
                      )}
                  </div>
                </div>
              </div>
            );
          if (this.state.carouselCards[item.id] !== undefined) {
            output.push(userCarousel);
          }
          break;
        case 'discover/channels':
          enabled.push('channels');
          showChannels = (
            <div name="channels" key={item.key}>
              <div className="row discover-title" style={customRow}>
                <div className="small-6 columns">
                  <div role="heading" aria-level="3">
                    {translatedLabel || tr(item.label || 'CHANNELS')}
                  </div>
                </div>
                <div className="small-6 columns">
                  <div className="text-right">
                    <small>
                      <a href="#" className="matte channel" onClick={this.openChannelPage}>
                        {tr('View More')}
                      </a>
                    </small>
                  </div>
                </div>
              </div>
              <div className="row discovery-contents" style={customRow}>
                {this.state.loadingChannels && (
                  <div className="small-12 columns empty-state">
                    <EmptyBlock className="channels" title={tr('Content loading...')} />
                  </div>
                )}
                {!this.state.loadingChannels && (showChannelsEmptyState || showChannelsEmptyBlock) && (
                  <div className="small-12 columns empty-state">
                    <EmptyBlock
                      className="channels"
                      link={
                        this.props.isAdmin && counts.channels_count === 0 && numberOfChannels === 0
                          ? {
                              text: 'CREATE CHANNEL',
                              isPush: false,
                              url: '/admin/channel/channels'
                            }
                          : undefined
                      }
                      title={emptyChannelMsg}
                    />
                  </div>
                )}
                {!this.state.loadingChannels && (
                  <div className="small-12 columns">
                    {this.state.openTooltipChanel && this.state.channelTooltip.length > 20 ? (
                      <div
                        ref={_discoveryTooltip => {
                          this._discoveryTooltip = _discoveryTooltip;
                        }}
                        className="tooltip-text"
                      >
                        {this.state.channelTooltip}
                      </div>
                    ) : null}
                    <Carousel isDiscovery={true} slidesToShow={5} key="channels">
                      {channels.map(channel => {
                        let mediumProfileImage =
                          channel.profileImageUrls && channel.profileImageUrls.medium_url;
                        return (
                          <div key={channel.id}>
                            {this.channelComp(
                              channel.id,
                              channel,
                              channel.isFollowing,
                              channel.followPending,
                              channel.label,
                              mediumProfileImage || channel.bannerImageUrl,
                              channel.slug || channel.id,
                              channel.allowFollow,
                              channel.isPrivate
                            )}
                          </div>
                        );
                      })}
                    </Carousel>
                  </div>
                )}
              </div>
            </div>
          );
          output.push(showChannels);
          break;
        case 'discover/trending':
          enabled.push('trending');
          showTrendingContent = (
            <div name="trending-content" key={item.key}>
              <div className="row discover-title" style={customRow}>
                <div className="small-12 columns">
                  <div role="heading" aria-level="3">
                    {translatedLabel || tr(item.label || 'TRENDING CONTENT')}
                  </div>
                </div>
              </div>
              {!this.state.loadingTrendingContent && showTrendingContent && (
                <ReactCSSTransitionGroup
                  transitionName="content"
                  transitionAppear={true}
                  transitionAppearTimeout={300}
                  transitionEnter={false}
                  transitionLeave={true}
                  transitionLeaveTimeout={300}
                >
                  <div className="row discovery-contents" style={customRow}>
                    <div className="small-12 columns discover-card-wrapper">
                      <div className="discover-card-wrapper-inner discover-card-width">
                        <Carousel
                          isDiscovery={true}
                          isCardV3={this.state.isCardV3}
                          isDiscovery_card={true}
                          slidesToShow={3}
                          key="trending"
                        >
                          {cards.map((card, index) => {
                            return (
                              <div key={card.id}>
                                <Card
                                  isCardV3={this.state.isCardV3}
                                  card={card}
                                  author={card.author}
                                  dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                                  startDate={
                                    card.startDate || (card.assignment && card.assignment.startDate)
                                  }
                                  removeCardFromList={this.removeCardFromList}
                                  tooltipPosition="top-center"
                                  user={this.props.currentUser}
                                  moreCards={true}
                                  discoverUpshotEventSend={this.discoverUpshotEventSend}
                                />
                              </div>
                            );
                          })}
                        </Carousel>
                      </div>
                    </div>
                  </div>
                </ReactCSSTransitionGroup>
              )}
              {this.state.loadingTrendingContent && (
                <div className="row" style={customRow}>
                  <div className="small-12 columns empty-state">
                    <EmptyBlock className="content" title={tr('Content loading...')} />
                  </div>
                </div>
              )}

              {!this.state.loadingTrendingContent && showTrendingContentEmptyBlock && (
                <div className="row" style={customRow}>
                  <div className="small-12 columns empty-state">
                    <EmptyBlock
                      className="content"
                      title={tr('Low Team activity. Unable to show trending content.')}
                    />
                  </div>
                </div>
              )}
            </div>
          );
          output.push(showTrendingContent);
          break;
        case 'discover/featuredProviders':
          enabled.push('featuredProviders');
          if (featuredProviders.length > 0) {
            let category = item.category || 'default';
            let mappedProviders = this.getFilteredProviders(featuredProviders, category);
            if (mappedProviders.length > 0) {
              showFeaturedProvider = (
                <div name="featured-providers" key={item.key + item.index}>
                  <div className="row discover-title" style={customRow}>
                    <div className="small-6 columns">
                      <div role="heading" aria-level="3">
                        {translatedLabel ||
                          tr(item.label || item.defaultLabel || 'FEATURED PROVIDERS')}
                      </div>
                    </div>
                    <div className="small-6 columns">
                      <div className="text-right" />
                    </div>
                  </div>
                  <div className="row discovery-contents" style={customRow}>
                    <div className="small-12 columns">
                      <Carousel isDiscovery={true} slidesToShow={4} key="featured">
                        {mappedProviders.map(provider => {
                          let providerLink = `/discover/${provider.display_name
                            .toLowerCase()
                            .replace(/\s/g, '-')}`;
                          if (
                            provider.source_type &&
                            (provider.source_type.name == 'mango_languages' ||
                              provider.source_type.category == 'deeplink')
                          ) {
                            if (provider.source_type.category == 'deeplink') {
                              providerLink = `${provider.source_config['deep_link_url']}`;
                            } else {
                              providerLink = `${provider.source_config['api-key']}?user_id=${
                                this.props.currentUser.id
                              }&email=${this.props.currentUser.email}&first_name=${
                                this.props.currentUser.first_name
                              }&last_name=${this.props.currentUser.last_name}`;
                            }
                            return (
                              <div key={provider.id}>
                                <a className="provider" target="_blank" href={providerLink}>
                                  <Provider
                                    providerName={provider.display_name}
                                    logoUrl={provider.logo_url || provider.source_type.image_url}
                                  />
                                </a>
                              </div>
                            );
                          } else {
                            return (
                              <div key={provider.id}>
                                <a
                                  aria-label={tr(`Visit the catalog page for provider, %{provider_name}`, {provider_name: `${provider.display_name}`})}
                                  href="#"
                                  className="provider"
                                  onClick={e =>
                                    this.goToProvider(e, providerLink, provider.display_name)
                                  }
                                >
                                  <Provider
                                    providerName={provider.display_name}
                                    logoUrl={provider.logo_url || provider.source_type.image_url}
                                  />
                                </a>
                              </div>
                            );
                          }
                        })}
                      </Carousel>
                    </div>
                  </div>
                  {this.state.openSearch && (
                    <Search
                      origin={this.state.providerId}
                      toggleParentState={this.toggleParentState}
                    />
                  )}
                </div>
              );
              output.push(showFeaturedProvider);
            }
          }
          break;
        case 'discover/users':
          enabled.push('users');
          if (numberOfInfluencers > 0) {
            showInfluencers = (
              <div name="influencers" key={item.key}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.label || 'PEOPLE')}
                    </div>
                  </div>
                  {this.props.team.OrgConfig.profile['web/profile/teams'].visible && (
                    <div className="small-6 columns">
                      <div className="text-right">
                        <small>
                          <a href="#" className="matte" onClick={this.openTeam}>
                            {tr('View More')}
                          </a>
                        </small>
                      </div>
                    </div>
                  )}
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {showInfluencersEmptyState && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock
                        className="influencers"
                        link={{
                          text: tr('VIEW TEAM'),
                          isPush: true,
                          url: '/me/team/subject-matter-experts'
                        }}
                        title={
                          counts.total_suggested_users_count > 0
                            ? tr('You have discovered everyone on your team.')
                            : tr(
                                "Your team admin hasn't configured this section yet. Members assigned a SME or Influencer title from the Admin Console will appear here"
                              )
                        }
                      />
                    </div>
                  )}
                  <div className="small-12 columns">
                    <Carousel
                      isDiscovery={true}
                      isPeopleCarouselV3={this.state.isPeopleCardV3}
                      slidesToShow={5}
                      key="influencers"
                    >
                      {influencers
                        .map((user, index) => {
                          let position = 'top';
                          return (
                            <div
                              key={user.id}
                              style={this.state.isPeopleCardV3 ? this.styles.newPeopleCard : ''}
                            >
                              {this.userComp(
                                user,
                                user.id,
                                user.isFollowing,
                                user.followPending,
                                user.name,
                                user.handle,
                                (user.avatarimages && user.avatarimages.medium) || defaultUserImage,
                                user.expertSkills,
                                (user.roles !== undefined && user.roles) || ['influencer'],
                                (user.rolesDefaultNames !== undefined &&
                                  user.rolesDefaultNames) || ['influencer'],
                                position
                              )}
                            </div>
                          );
                        })
                        .concat(
                          showInfluencersEmptyBlock && (
                            <div key="empty">
                              <EmptyBlock
                                className="influencers"
                                link={{ text: tr('VIEW TEAM'), isPush: true, url: '/me/team' }}
                              />
                            </div>
                          )
                        )}
                    </Carousel>
                  </div>
                </div>
              </div>
            );
          }
          output.push(showInfluencers);
          break;
        case 'discover/courses':
          enabled.push('courses');
          if (this.props.isAdmin || numberOfCourses !== 0) {
            showCourses = (
              <div name="course" key={item.key}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.label || 'RECOMMENDED COURSES')}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right">
                      {numberOfCourses > 5 && (
                        <small>
                          <a
                            href="#"
                            className="matte videostream"
                            onClick={e => this.moreCourses(e, '/discover/courses')}
                          >
                            {tr('View More')}
                          </a>
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {showCoursesEmptyState && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock className="courses" title={tr('No Courses')} />
                    </div>
                  )}
                  <div className="small-12 columns">
                    <Carousel isDiscovery={true} slidesToShow={5} key="courses">
                      {courses
                        .map(course => {
                          return (
                            <div key={course.id}>
                              {this.courseComp(
                                course.name,
                                course.logoUrl,
                                course.startDate,
                                course.endDate,
                                course.status,
                                course.url
                              )}
                            </div>
                          );
                        })
                        .concat(
                          showCoursesEmptyBlock && (
                            <div key="empty">
                              <EmptyBlock className="courses" />
                            </div>
                          )
                        )}
                    </Carousel>
                  </div>
                </div>
              </div>
            );
            output.push(showCourses);
          }
          break;
        case 'discover/pathways':
          enabled.push('pathways');
          if (numberOfPathways > 0) {
            showPathways = (
              <div name="pathways" key={item.key}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.label || 'PATHWAYS')}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right">
                      {numberOfPathways >= 5 && (
                        <small>
                          <a
                            href="#"
                            className="matte pathways"
                            onClick={e => this.viewMorePathways(e, '/discover/pathways')}
                          >
                            {tr('View More')}
                          </a>
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {showPathwaysEmptyState && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock
                        className="pathways"
                        link={{ text: tr('CREATE PATHWAY'), isPush: false, url: '/pathways/new' }}
                        title={tr('No Pathway')}
                      />
                    </div>
                  )}
                  <div className="small-12 columns">
                    <Carousel isDiscovery={true} slidesToShow={4} key="pathways">
                      {pathways
                        .map((pathway, index) => {
                          let imageUrl;
                          let isFileAttached = pathway.filestack && !!pathway.filestack.length;
                          let imageFileStack = !!(
                            isFileAttached &&
                            pathway.filestack[0].mimetype &&
                            ~pathway.filestack[0].mimetype.indexOf('image/')
                          );
                          if (pathway.imageUrl) imageUrl = pathway.imageUrl;
                          else {
                            if (imageFileStack) {
                              imageUrl = pathway.filestack[0].url;
                            } else if (pathway.fileResources) {
                              let findImage = find(pathway.fileResources, function(el) {
                                return el.fileType == 'image';
                              });
                              if (findImage) imageUrl = findImage.fileUrl;
                            }
                          }
                          return (
                            <div key={pathway.id}>
                              {this.pathwayComp(
                                pathway,
                                pathway.id,
                                pathway.title || pathway.message,
                                imageUrl || getDefaultImage(this.props.currentUser.id).url,
                                pathway.slug,
                                pathway.packCardsCount
                              )}
                            </div>
                          );
                        })
                        .concat(
                          showPathwaysEmptyBlock && (
                            <div key="empty">
                              <EmptyBlock
                                className="pathways"
                                link={{
                                  text: tr('CREATE PATHWAY'),
                                  isPush: false,
                                  url: '/pathways/new'
                                }}
                              />
                            </div>
                          )
                        )}
                    </Carousel>
                  </div>
                </div>
              </div>
            );
          }
          output.push(showPathways);
          break;

        case 'discover/journeys':
          enabled.push('journeys');
          if (numberOfJourneys > 0) {
            showJourneys = (
              <div name="journeys" key={item.key}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.label || 'JOURNEYS')}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right">
                      {numberOfJourneys >= 5 && (
                        <small>
                          <a
                            className="matte journeys"
                            onTouchTap={() => {
                              this.props.dispatch(push('/discover/journeys'));
                            }}
                          >
                            {tr('View More')}
                          </a>
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  {showJourneysEmptyState && (
                    <div className="small-12 columns empty-state">
                      <EmptyBlock
                        className="journey"
                        link={{ text: tr('CREATE JOURNEY'), isPush: false, url: '/journeys/new' }}
                        title={tr('No Journey')}
                      />
                    </div>
                  )}
                  <div className="small-12 columns">
                    <Carousel isDiscovery={true} slidesToShow={4} key="journeys">
                      {journeys
                        .map((journey, index) => {
                          let imageUrl;
                          let isFileAttached = journey.filestack && !!journey.filestack.length;
                          let imageFileStack = !!(
                            isFileAttached &&
                            journey.filestack[0].mimetype &&
                            ~journey.filestack[0].mimetype.indexOf('image/')
                          );
                          if (journey.imageUrl) imageUrl = journey.imageUrl;
                          else {
                            if (imageFileStack) {
                              imageUrl = journey.filestack[0].url;
                            } else if (journey.fileResources) {
                              let findImage = find(journey.fileResources, function(el) {
                                return el.fileType == 'image';
                              });
                              if (findImage) imageUrl = findImage.fileUrl;
                            }
                          }
                          return (
                            <div key={journey.id}>
                              {this.journeyComp(
                                journey,
                                journey.id,
                                journey.title || journey.message,
                                imageUrl || getDefaultImage(this.props.currentUser.id).url,
                                journey.slug,
                                journey.journeyPacksCount
                              )}
                            </div>
                          );
                        })
                        .concat(
                          showJourneysEmptyBlock && (
                            <div key="empty">
                              <EmptyBlock
                                className="journeys"
                                link={{
                                  text: tr('CREATE JOURNEY'),
                                  isPush: false,
                                  url: '/journeys/new'
                                }}
                              />
                            </div>
                          )
                        )}
                    </Carousel>
                  </div>
                </div>
              </div>
            );
          }
          output.push(showJourneys);
          break;

        case 'discover/videos':
          enabled.push('videos');
          if (numberOfVideos > 0) {
            showVideos = (
              <div name="video stream" key={item.key}>
                <div className="row discover-title" style={customRow}>
                  <div className="small-6 columns">
                    <div role="heading" aria-level="3">
                      {translatedLabel || tr(item.label || 'VIDEOS')}
                    </div>
                  </div>
                  <div className="small-6 columns">
                    <div className="text-right">
                      {numberOfVideos >= 4 && (
                        <small>
                          <a
                            href="#"
                            className="matte videostreams"
                            onClick={e =>
                              this.viewMoreVideostreamCards(e, '/discover/videostreams')
                            }
                          >
                            {tr('View More')}
                          </a>
                        </small>
                      )}
                    </div>
                  </div>
                </div>
                <div className="row discovery-contents" style={customRow}>
                  <div className="small-12 columns">
                    <Carousel isDiscovery={true} slidesToShow={4} key="videos">
                      {videos.map(video => {
                        return (
                          <div key={video.id}>
                            <Video
                              videoCard={video}
                              controlsList={isDownloadContentDisabled ? 'nodownload' : ''}
                              discoverUpshotEventSend={this.discoverUpshotEventSend}
                            />
                          </div>
                        );
                      })}
                    </Carousel>
                  </div>
                </div>
              </div>
            );
          }
          output.push(showVideos);
          break;
        case 'discover/carousel':
          let carouselContent = (
            <div name="trending-content" key={item.uuid}>
              <div className="row discover-title" style={customRow}>
                <div className="small-12 columns">
                  <div role="heading" aria-level="3">
                    {translatedLabel || tr(item.defaultLabel && item.defaultLabel.toUpperCase())}
                  </div>
                </div>
              </div>
              <ReactCSSTransitionGroup
                transitionName="content"
                transitionAppear={true}
                transitionAppearTimeout={300}
                transitionEnter={false}
                transitionLeave={true}
                transitionLeaveTimeout={300}
              >
                <div className="row discovery-contents" style={customRow}>
                  <div className="small-12 columns">
                    {this.state.carouselCards[item.uuid] !== undefined && (
                      <Carousel isDiscovery={true} slidesToShow={4} key={item.uuid}>
                        {this.state.carouselCards[item.uuid].map((card, index) => {
                          return (
                            <div key={card.id}>
                              <DiscoverCard
                                card={card}
                                removeCardFromList={this.removeCardFromList}
                                carouselCard={true}
                              />
                            </div>
                          );
                        })}
                      </Carousel>
                    )}
                    {!this.state.carouselCards[item.uuid] &&
                      this.state.carouselCards[item.uuid] !== [] && (
                        <div className="row">
                          <div className="small-12 columns empty-state">
                            <EmptyBlock className="content" title={tr('Content loading...')} />
                          </div>
                        </div>
                      )}
                    {this.state.carouselCards[item.uuid] !== undefined &&
                      this.state.carouselCards[item.uuid].length == 0 && (
                        <div className="row">
                          <div className="small-12 columns empty-state">
                            <EmptyBlock className="content" title={tr('No Content')} />
                          </div>
                        </div>
                      )}
                  </div>
                </div>
              </ReactCSSTransitionGroup>
            </div>
          );
          output.push(carouselContent);
        default:
          // FIXME: implement default case
          break;
      }
    });

    let option1 = enabled.length <= 2 ? this.createEnabledOption(enabled[0]) : null;
    let option2 = enabled.length == 2 ? this.createEnabledOption(enabled[1]) : null;
    let limit = enabled.length == 2 ? 12 : 24;
    return (
      <div id="discovery">
        {enabled.length >= 0 && (
          <div>
            <div>{output}</div>
          </div>
        )}
      </div>
    );
  }
}

DiscoveryContainer.defaultProps = {
  featuredProviders: [],
  courses: []
};

DiscoveryContainer.propTypes = {
  videos: PropTypes.array,
  userIds: PropTypes.array,
  channelIds: PropTypes.array,
  courses: PropTypes.array,
  isAdmin: PropTypes.bool,
  isLoggedIn: PropTypes.bool,
  users: PropTypes.object,
  channels: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  pathways: PropTypes.array,
  journeys: PropTypes.array,
  cards: PropTypes.object,
  featuredProviders: PropTypes.array,
  counts: PropTypes.object
};

function mapStoreStateToProps(state) {
  return Object.assign({}, state.discovery.toJS(), state.currentUser.toJS(), {
    users: state.users.toJS(),
    channels: state.channels.toJS(),
    team: state.team.toJS(),
    cards: state.cards.toJS(),
    currentUser: state.currentUser.toJS()
  });
}

export default connect(mapStoreStateToProps)(DiscoveryContainer);
