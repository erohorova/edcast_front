import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import CardCarousels from './dicoveryCarousels/CardCarousels';
import ChannelCarousels from './dicoveryCarousels/ChannelCarousels';
import VideoCarousels from './dicoveryCarousels/VideoCarousels';
import CourseCarousels from './dicoveryCarousels/CourseCarousels';
import UsersCarousels from './dicoveryCarousels/UsersCarousels';
import FeatureProviderCarousles from './dicoveryCarousels/FeatureProviderCarousles';
import JourneyPathwayCarousels from './dicoveryCarousels/JourneyPathwayCarousels';
import { getCustomCarousels } from 'edc-web-sdk/requests/carousels';
import { langs } from '../../constants/languages';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { getUserById } from 'edc-web-sdk/requests/users';
import { getAllSources } from 'edc-web-sdk/requests/ecl';
import updatePageLastVisit from '../../utils/updatePageLastVisit';

class DiscoveryContainerV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showNew: window.ldclient.variation('show-new-for-card', false)
    };
    let discoverObj = props.team.get('Discover');
    this.soretedDiscoverObj = [];
    this.customCarousel = [];
    this.featuredProviderCarousel = [];
    this.customLabels = window.ldclient.variation('custom-labels', false);
    Object.keys(discoverObj).forEach(carousel => {
      if (!!discoverObj[carousel].visible) {
        if (discoverObj[carousel].index === undefined) {
          discoverObj[carousel]['index'] = -1;
        }
        discoverObj[carousel]['uid'] = carousel;
        if (carousel.indexOf('discover/virtualLabProviders') !== -1) {
          discoverObj[carousel]['key'] = 'discover/featuredProviders';
        } else {
          discoverObj[carousel]['key'] = !!~carousel.indexOf('discover/carousel')
            ? carousel
                .split('/')
                .slice(0, -1)
                .join('/')
            : carousel;
        }

        if (
          discoverObj[carousel].visible &&
          !!~carousel.indexOf('discover/carousel/customCarousel')
        ) {
          this.customCarousel.push(discoverObj[carousel]);
        }
        if (discoverObj[carousel].visible && !!~carousel.indexOf('discover/featuredProviders')) {
          this.featuredProviderCarousel.push(discoverObj[carousel]);
        }
        if (
          discoverObj[carousel]['key'] !== 'discover/carousel' ||
          (discoverObj[carousel]['key'] === 'discover/carousel' &&
            discoverObj[carousel].cards.length > 0)
        ) {
          this.soretedDiscoverObj.push(discoverObj[carousel]);
        }
      }
    });
    this.soretedDiscoverObj.sort((a, b) => a.index - b.index);
    this.isShowCustomLabels =
      this.props.team.get('config') && this.props.team.get('config').custom_labels;
    for (let prop in langs) {
      if (
        props.currentUser.get('profile') &&
        langs[prop] ===
          (this.props.currentUser.get('profile').language ||
            (this.props.currentUser.get('profile').get &&
              this.props.currentUser.get('profile').get('language')))
      )
        this.profileLanguage = prop.toLowerCase();
    }
    this.outputCarousel = this.carouselPageGenerator();

    if (this.state.showNew) {
      updatePageLastVisit('discoveryLastVisit');
    }
  }

  componentWillUnmount() {
    this.state.showNew &&
      localStorage.setItem('discoveryLastVisit', Math.round(new Date().getTime() / 1000));
  }

  componentDidMount() {
    let userInfoCallBack = getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser.toJS()
    );
    this.props
      .dispatch(userInfoCallBack)
      .then(async () => {
        let carosuels = this.props.discovery && this.props.discovery.get('carousels');
        if (this.customCarousel.length && !Object.keys(carosuels).length) {
          let payload = {
            filter_by_language: false,
            'structure_ids[]': this.customCarousel.map(carousel => carousel.id),
            user_fields:
              'id,handle,avatarimages,roles,roles_default_names,name,is_following,following_count,followers_count,expert_skills,status,company,coverimages'
          };
          getCustomCarousels(payload)
            .then(response => {
              this.props.dispatch({
                type: 'get_all_discover_carousel',
                data: response,
                customCarousel: this.customCarousel,
                isCustomCarousels: true
              });
            })
            .catch(error => {
              console.error(
                'ERROR!!! in DiscoveryContainerV2.componentDidMount.getCustomCarousels',
                error
              );
            });
        }
        if (this.featuredProviderCarousel && !Object.keys(carosuels).length) {
          let userData;
          userData = await getUserById(this.props.currentUser.get('id'));
          let teams = userData.userTeams;
          let team_ids = teams.map(team => {
            return team.teamId;
          });
          if (team_ids.length == 0) {
            team_ids.push('0');
          }
          let payload = {
            is_featured: true,
            'team_ids[]': team_ids
          };

          getAllSources(payload)
            .then(response => {
              let featuredTemp = Object.assign({}, this.featuredProviderCarousel);
              this.props.dispatch({
                type: 'get_all_discover_carousel',
                data: response,
                carosuel: featuredTemp[0],
                isCustomCarousels: false
              });
            })
            .catch(err => {
              console.error(
                `Error in DiscoveryContainer.componentDidMount.getAllSources.func: ${err}`
              );
            });
        }
      })
      .catch(err => {
        console.error(`Error in DiscoveryContainer.componentDidMount.func: ${err}`);
      });
  }

  carouselPageGenerator = () => {
    let dicoverCarousels = [];
    let carouselComponent = {};
    this.soretedDiscoverObj.forEach(carousel => {
      switch (carousel.key) {
        case 'discover/featuredProviders':
          carouselComponent = (
            <FeatureProviderCarousles
              key={carousel.uid}
              carousel={carousel}
              isAPIDisabled={true}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/journeys':
          carouselComponent = (
            <JourneyPathwayCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/channels':
          carouselComponent = (
            <ChannelCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/videos':
          carouselComponent = (
            <VideoCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/courses':
          carouselComponent = (
            <CourseCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/pathways':
          carouselComponent = (
            <JourneyPathwayCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/trending':
          carouselComponent = (
            <CardCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/users':
          carouselComponent = (
            <UsersCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/carousel/customCarousel/user':
          carouselComponent = (
            <UsersCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isCustomCarousel={true}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/carousel/customCarousel/channel':
          carouselComponent = (
            <ChannelCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isCustomCarousel={true}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/carousel/customCarousel/card':
          carouselComponent = (
            <CardCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isCustomCarousel={true}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        case 'discover/carousel':
          carouselComponent = (
            <CardCarousels
              key={carousel.uid}
              carousel={carousel}
              carouselKey={carousel.key}
              isCustomCarousel={false}
              isOldConfigCarousel={true}
              isShowCustomLabels={this.isShowCustomLabels}
              profileLanguage={this.profileLanguage}
            />
          );
          dicoverCarousels.push(carouselComponent);
          break;
        default:
          break;
      }
    });
    return dicoverCarousels;
  };

  render() {
    return <div id="discovery">{this.outputCarousel}</div>;
  }
}

DiscoveryContainerV2.propTypes = {
  discovery: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    discovery: state.discovery,
    team: state.team,
    currentUser: state.currentUser
  };
}

export default connect(mapStoreStateToProps)(DiscoveryContainerV2);
