import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';

class Course extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false)
    };
  }

  handleClick() {
    if (this.props.discoverUpshotEventSend && this.state.upshotEnabled) {
      this.props.discoverUpshotEventSend({
        screenName: 'Explore',
        category: 'Course',
        name: this.props.name,
        event: 'Course Clicked'
      });
    }
  }

  render() {
    let dateText;
    let startDate = new Date(this.props.startDate);
    let endDate = new Date(this.props.endDate);
    let currentDate = new Date();

    // If a course is future-dated.
    if (startDate > currentDate) {
      dateText = `Starts at ${startDate.toDateString()}`;
    } else if (endDate > currentDate) {
      // If a course is ongoing and has end_date already set.
      dateText = `End at ${endDate.toDateString()}`;
    } else {
      // If a course is an ongoing course and no end_date set i.e self-paced.
      dateText = this.props.status.replace('-', ' ');
    }

    return (
      <div className="channel-course">
        <a href={this.props.url} onClick={this.handleClick.bind(this)} target="_blank">
          <Paper>
            <div className="image" style={{ backgroundImage: 'url(' + this.props.logoUrl + ')' }} />
            <div className="info">
              <div className="title">{this.props.name}</div>
              <div className="clearfix" style={{ padding: '0 .2rem' }}>
                <small className="start-date" style={{ textTransform: 'capitalize' }}>
                  {dateText}
                </small>
              </div>
            </div>
          </Paper>
        </a>
      </div>
    );
  }
}

Course.propTypes = {
  name: PropTypes.string,
  logoUrl: PropTypes.string,
  startDate: PropTypes.string,
  url: PropTypes.string,
  endDate: PropTypes.string,
  status: PropTypes.string,
  discoverUpshotEventSend: PropTypes.func
};

export default Course;
