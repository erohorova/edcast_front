import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getPathways, searchPathways } from '../../actions/pathwaysActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import PathwayListItem from '../common/PathwayListItem';
import ListView from '../common/ListView';
import Breadcrumb from '../common/Breadcrumb';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

class PathwaysListContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pathways: [],
      filteredList: [],
      pending: false,
      offset: 0,
      limit: 20,
      filteredText: '',
      isLastPage: false,
      filterType: 'All Pathways',
      enableBtnSearch: true,
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false)
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount() {
    let userInfoCallBack = getSpecificUserInfo(
      [
        'followingChannels',
        'rolesDefaultNames',
        'roles',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props
      .dispatch(userInfoCallBack)
      .then(() => {
        this.loadPathways(this.state.limit, this.state.offset);
        window.addEventListener('scroll', this.handleScroll);
      })
      .catch(err => {
        console.error(`Error in PathwaysListContainer.componentWillMount.func: ${err}`);
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.loadPathways(this.state.limit, this.state.offset);
        }
      }
    },
    150,
    { leading: false }
  );

  loadPathways(limit, offset) {
    if (!this.state.pending) {
      this.setState({ pending: true });
      let payload = {
        limit: limit,
        offset: offset,
        fields: 'id,filestack,title,message,slug,pack_cards_count,views_count,created_at'
      };
      this.props
        .dispatch(getPathways(payload))
        .then(pathways => {
          this.setState({ offset: offset + limit, isLastPage: pathways.length < limit });
          this.afterLoad(pathways);
        })
        .catch(err => {
          console.error(`Error in PathwaysListContainer.getPathways.func : ${err}`);
        });
    }
  }

  afterLoad(pathways) {
    if (this.state.filterType == 'All Pathways') {
      this.setState({
        pathways: uniq(this.state.pathways.concat(pathways), 'id'),
        filteredList: uniq(this.state.pathways.concat(pathways), 'id'),
        pending: false
      });
    } else {
      this.setState({ pathways: uniq(this.state.pathways.concat(pathways), 'id'), pending: false });
      this.sortPathway(this.state.pathways);
    }
  }

  sortPathway(pathways) {
    let sorted;
    if (this.state.filterType === 'Sort by Alphabetical') {
      pathways.map(function(pathway, index) {
        if (!pathway.title) {
          pathway.title = pathway.message;
        }
      });

      sorted = pathways.sort((a, b) => a.title.localeCompare(b.title));
    } else if (this.state.filterType === 'Sort by Views') {
      sorted = pathways.sort((a, b) => {
        return parseInt(b.viewsCount) - parseInt(a.viewsCount);
      });
    } else if (this.state.filterType === 'Sort by Date') {
      sorted = pathways.sort((a, b) => {
        return new Date(b.createdAt) - new Date(a.createdAt);
      });
    } else if (this.state.filterType === 'All Pathways') {
      sorted = this.state.pathways;
    }
    this.setState({ filteredList: sorted });
  }

  handleFilterChange = e => {
    this.setState({ filterType: e });
    this.sortPathway(this.state.filteredList);
  };
  getCardsWrapper(queryParam) {
    this.setState({ pending: true, pathways: [], filteredList: [], filteredText: queryParam });
    let payload = {
      limit: null,
      offset: 0,
      fields: 'id,filestack,title,message,slug,pack_cards_count',
      q: queryParam
    };
    this.props
      .dispatch(searchPathways(payload))
      .then(pathways => {
        this.setState({ isLastPage: true });
        this.afterLoad(pathways);
      })
      .catch(err => {
        console.error(`Error in PathwaysListContainer.getCardsWrapper.func : ${err}`);
      });
  }
  handleFilterSearch = (e, inputValue = null) => {
    if (!!inputValue) {
      this.getCardsWrapper(inputValue);
    } else if (e.keyCode === 13 || !e.target.value) {
      this.setState({ pathways: [], filteredList: [], filteredText: e.target.value });
      if (!e.target.value) {
        this.loadPathways(20, 0);
      } else {
        this.getCardsWrapper(e.target.value);
      }
    }
  };

  render() {
    return (
      <div>
        <Breadcrumb
          levels={[
            { name: 'Discover', url: '/discover' },
            { name: 'Pathways', url: '/discover/pathways' }
          ]}
          backPress={this.state.isCardV3 && this.state.pathwayConsumptionV2}
        />
        <div className="container-padding row content" id="pathways">
          <div className="columns expand vertical-spacing-large">
            <div className="vertical-spacing-medium">
              <ListView
                isTable={true}
                filterOptions={[
                  'All Pathways',
                  'Sort by Alphabetical',
                  'Sort by Views',
                  'Sort by Date'
                ]}
                handleFilterChange={this.handleFilterChange}
                handleFilterSearch={this.handleFilterSearch}
                enableBtnSearch={this.state.enableBtnSearch}
                pending={this.state.pending}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr('There are no pathways available.')}
                  </div>
                }
              >
                {this.state.filteredList.map((pathway, index) => {
                  return <PathwayListItem key={index} pathway={pathway} />;
                })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PathwaysListContainer.propTypes = {
  currentUser: PropTypes.object
};

export default connect(state => ({
  pathways: state.pathways.toJS(),
  currentUser: state.currentUser.toJS()
}))(PathwaysListContainer);
