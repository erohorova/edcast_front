import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import LockedIcon from 'edc-web-sdk/components/icons/Lock';
import Card from '../cards/Card';

class JourneyDetailsSectionCard extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      lockedCard: {
        background: '#fff url(/i/images/empty_state_card.png) no-repeat',
        backgroundSize: '100% 100%'
      },
      bigLockIcon: {
        width: '70px',
        height: '70px',
        marginTop: '50px'
      }
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.card !== nextProps.card;
  }

  render() {
    return this.props.card.locked ? (
      <div
        className={`${
          !this.props.showIndex ? 'new-card-designed-locked' : ''
        } card-v2 more-cards card-v2__simple-size`}
      >
        <Paper className="paper-card" zDepth={0}>
          <div className="locked-card" style={this.styles.lockedCard}>
            <LockedIcon style={this.styles.bigLockIcon} color="#7c7d94" />
          </div>
          <div className="close-button pathway-card-btn number-card-btn">
            <span>{this.props.keyIndex + 1}</span>
          </div>
        </Paper>
      </div>
    ) : (
      <Card
        key={this.props.keyIndex}
        inStandalone={true}
        card={this.props.card}
        showIndex={this.props.showIndex}
        journeyEditor={false}
        journeyDetails={this.props.journey}
        index={this.props.index}
        currentSection={this.props.currentSection}
        author={this.props.card.author}
        dueAt={
          this.props.card.dueAt || (this.props.card.assignment && this.props.card.assignment.dueAt)
        }
        startDate={
          this.props.card.startDate ||
          (this.props.card.assignment && this.props.card.assignment.startDate)
        }
        pathwayChecking={this.props.pathwayChecking}
        hideComplete={this.props.findCheckedCardAtList}
        isPrivate={this.props.card.isPrivate}
        cardUpdated={this.props.cardUpdated}
        closeCardModal={this.props.findCheckedCardAtList}
        moreCards={true}
        previewMode={this.props.previewMode}
        isShowLockedCardContent={this.props.isShowLockedCardContent}
        isStandaloneModal={this.props.isStandaloneModal}
        cardSplat={this.props.cardSplat}
        isPartOfPathway={true}
      />
    );
  }
}

JourneyDetailsSectionCard.propTypes = {
  cardSplat: PropTypes.any,
  keyIndex: PropTypes.number,
  card: PropTypes.object,
  showIndex: PropTypes.bool,
  journey: PropTypes.object,
  index: PropTypes.number,
  currentSection: PropTypes.number,
  previewMode: PropTypes.any,
  isShowLockedCardContent: PropTypes.any,
  pathwayChecking: PropTypes.bool,
  isStandaloneModal: PropTypes.bool,
  findCheckedCardAtList: PropTypes.func,
  cardUpdated: PropTypes.func
};

export default JourneyDetailsSectionCard;
