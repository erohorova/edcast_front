import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { recordVisit } from 'edc-web-sdk/requests/analytics';

import uniq from 'lodash/uniq';
import difference from 'lodash/difference';
import times from 'lodash/times';
import findIndex from 'lodash/findIndex';
import unescape from 'lodash/unescape';
import { tr } from 'edc-web-sdk/helpers/translations';

import FlatButton from 'material-ui/FlatButton';
import LinearProgress from 'material-ui/LinearProgress';

import NavigationArrowDropDown from 'react-material-icons/icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'react-material-icons/icons/navigation/arrow-drop-up';

import { fetchCard, postv2 } from 'edc-web-sdk/requests/cards';
import colors from 'edc-web-sdk/components/colors/index';
import Paper from 'edc-web-sdk/components/Paper';
import LockedIcon from 'edc-web-sdk/components/icons/Lock';

import {
  markAsComplete,
  markAsUncomplete,
  createLeap,
  updateLeap,
  lockPathwayCard
} from 'edc-web-sdk/requests/cards.v2';
import {
  publishPathway,
  batchAddToPathway,
  batchAddToJourney,
  customReorderPathwayCards,
  addCardsToPathways
} from 'edc-web-sdk/requests/pathways.v2';

import {
  openJourneyCreationModal,
  openJourneyOverviewModal,
  openStatusModal,
  openCongratulationModal
} from '../../actions/modalActions';
import {
  deleteTempJourney,
  isPreviewMode,
  removeReorderCardIds
} from '../../actions/journeyActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { open as openSnackBar } from '../../actions/snackBarActions';

import Card from '../cards/Card';
import MainInfoSmartBite from '../common/MainInfoSmartBite';
import CommentsBlockSmartBite from '../common/CommentsBlockSmartBite';
import convertRichText from '../../utils/convertRichText';

import * as logoType from '../../constants/logoTypes';

import BreadcrumbV2 from '../common/BreadcrumpV2';

import Spinner from '../common/spinner';

const logoObj = logoType.LOGO;
const lightPurp = '#acadc1';

class JourneyDetailsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      slug: props.routeParams.splat || props.routeParams.slug,
      defaultImage: '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg',
      reviewStatusLabel: '',
      publishLabel: 'Publish',
      isCompleted: false,
      isUpvoted: false,
      comments: [],
      commentsCount: 0,
      completeStatus: 0,
      open: false,
      votesCount: 0,
      clicked: false,
      startRegime: true,
      editRegime: false,
      continueRegime: false,
      viewRegime:
        props.team.OrgConfig.cardView && props.team.OrgConfig.cardView['web/cardView/pathway']
          ? props.team.OrgConfig.cardView['web/cardView/pathway'].defaultValue
          : 'Tile',
      smartBites: [],
      journeySmartBites: [],
      currentUser: props.currentUser,
      publishError: '',
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      isLoaded: false,
      journey: {},
      isOpenBlock: false,
      arrIdsSections: [],
      viewPublishButton: false,
      isAllExpanded: false,
      openBlocks: [],
      journeyLoading: true,
      isCardV3: window.ldclient.variation('card-v3', false),
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false)
    };

    this.styles = {
      btnIcons: {
        verticalAlign: 'middle',
        width: '24px',
        height: '24px',
        padding: 0
      },
      tooltipActiveBts: {
        marginTop: -32
      },
      iconStyles: {
        width: '24px',
        height: '24px',
        padding: 0,
        border: 0
      },
      svgImage: {
        zIndex: 2,
        position: 'relative'
      },
      accordionTitleIcon: {
        fill: 'inherit',
        height: '34px',
        width: '34px'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      },
      completeBar: {
        backgroundColor: '#ffffff',
        borderRadius: '0.125rem',
        height: '0.25rem'
      },
      publishBtn: {
        color: colors.white,
        backgroundColor: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '12px',
        minWidth: '144px',
        height: '32px'
      },
      reviewBtn: {
        color: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '12px',
        minWidth: '144px',
        height: '32px'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      bigLockIcon: {
        width: '70px',
        height: '70px',
        marginTop: '50px'
      },
      lockedCard: {
        background: '#fff url(/i/images/empty_state_card.png) no-repeat',
        backgroundSize: '100% 100%'
      },
      actionIcon: {
        paddingLeft: 0,
        paddingRight: 0,
        width: 'auto'
      }
    };
    this.completeClickHandler = this.completeClickHandler.bind(this);
    this.isUncompleteEnabled = window.ldclient.variation('uncomplete-card', false);
  }

  setCheckedCard = (card, callback) => {
    let checkedCard = card;
    checkedCard.isCompleted =
      card.completionState && card.completionState.toUpperCase() === 'COMPLETED';
    checkedCard.isUpvoted = card.isUpvoted;
    checkedCard.commentsCount = card.commentsCount;
    checkedCard.votesCount = card.votesCount;
    this.setState({ checkedCard }, callback);
  };

  fetchCardById = id => {
    return fetchCard(id, true)
      .then(data => {
        this.setState({ journeyLoading: false });
        return data;
      })
      .catch(err => {
        console.error(`Error in JourneyDetailsContainer.fetchCardById.fetchCard.func : ${err}`);
      });
  };

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    let isOwner =
      this.props.journey &&
      this.props.journey.tempJourney &&
      this.props.journey.tempJourney.author &&
      this.props.journey.tempJourney.author.id == this.props.currentUser.id;
    if (this.props.journey && this.props.journey.isPreviewMode && isOwner) {
      this.previewJourney();
    } else {
      this.updateJourney(true, this.props.routeParams && this.props.routeParams.cardId);
    }
  }

  previewJourney = () => {
    let tempJourney = this.props.journey.tempJourney;
    let journeySmartBites = this.props.journey.tempJourney.journeySections;
    let viewPublishButton = true;
    journeySmartBites.forEach((section, index) => {
      let lockedIndex;
      viewPublishButton =
        section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      if (section.pack_cards) {
        for (let i = 0; i < section.pack_cards.length; i++) {
          let currentCard = section.pack_cards[i];
          if (
            tempJourney.cardsToLock &&
            tempJourney.cardsToLock[index] &&
            tempJourney.cardsToLock[index][currentCard.card_id || currentCard.id]
          ) {
            currentCard.isLocked =
              tempJourney.cardsToLock[index][currentCard.card_id || currentCard.id].locked ===
              'true';
          } else {
            currentCard.isLocked = currentCard.locked;
          }
          if (currentCard.isLocked) {
            lockedIndex = i;
            break;
          }
        }
      }
    });
    this.setState({
      previewMode: true,
      journey: this.props.journey.tempJourney,
      isLoaded: true,
      journeySmartBites: journeySmartBites,
      viewPublishButton
    });
  };

  validationJourney = () => {
    let valid = true;
    if (!this.state.journeySmartBites) {
      valid = false;
    }
    this.state.journeySmartBites &&
      this.state.journeySmartBites.map(section => {
        if (!section.cards || (section.cards && !section.cards.length)) {
          valid = false;
        }
      });
    if (!valid) {
      let publishError = 'Please add cards to the collection before publishing!';
      this.setState({ publishError });
    }
    return valid;
  };

  publishClickHandler = () => {
    let valid = this.validationJourney();
    if (valid) {
      this.setState({ publishLabel: 'Publishing...', clicked: true });
      this.startCreateJourney();
    }
  };

  async startCreateJourney() {
    let sharedtoGroups = [];
    this.state.journey.teams.map(team => {
      sharedtoGroups.push(team.id);
    });
    let sections = this.state.journeySmartBites;
    let isEditor = this.state.journey && this.state.journey.id;
    let batchAddToPathwaysData = [];
    if (!isEditor) {
      for (let i = 0; i < sections.length; i++) {
        let payload = {
          card: {
            message: sections[i].block_title,
            title: '',
            state: 'draft',
            card_type: 'pack',
            card_subtype: 'simple',
            hidden: 1,
            topics: [],
            channel_ids: [],
            team_ids: sharedtoGroups
          }
        };
        let data = await postv2(payload);
        if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(data.id);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await uniq(sections[i].pack_cards.map(card => card.id || card.cardId));

            batchAddToPathwaysData.push({ section_id: data.id, content_ids: ids });

            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payloadForLeap = {
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                if (leapObj.leapId) {
                  updateLeap(leapObj.cardId, leapObj.leapId, payloadForLeap);
                } else {
                  createLeap(leapObj.cardId, payloadForLeap);
                }
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      }

      if (batchAddToPathwaysData.length > 0) {
        addCardsToPathways({ data: batchAddToPathwaysData });
      }

      await this.createJourney(false);
      return;
    } else {
      for (let i = 0; i < sections.length; i++) {
        let cardId = sections[i].card_id;
        let current = this.state.journey.journeySection.find(item => item.card_id == cardId);
        let data = {};
        if (!cardId || sections[i].block_title !== current.block_title) {
          let payload = {
            card: {
              message: sections[i].block_title,
              title: '',
              state: 'draft',
              card_type: 'pack',
              card_subtype: 'simple',
              hidden: 1,
              topics: [],
              channel_ids: [],
              team_ids: sharedtoGroups
            }
          };
          data = await postv2(payload, cardId);
        } else {
          data.id = cardId;
        }
        if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(`${data.id}`);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await uniq(
              sections[i].pack_cards.map(card => `${card.id || card.cardId || card.card_id}`)
            );
            let currentIds =
              current &&
              current.pack_cards &&
              current.pack_cards.map(item => {
                return item.card_id + '';
              });
            let idsToAdd = difference(ids, currentIds);

            if (idsToAdd && idsToAdd.length) {
              batchAddToPathwaysData.push({ section_id: data.id, content_ids: idsToAdd });
            }

            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payload = {
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                if (leapObj.leapId) {
                  updateLeap(leapObj.cardId, leapObj.leapId, payload);
                } else {
                  createLeap(leapObj.cardId, payload);
                }
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with update journey!' });
        }
      }

      if (batchAddToPathwaysData.length > 0) {
        addCardsToPathways({ data: batchAddToPathwaysData });
      }

      await this.createJourney(true);
      return;
    }
  }

  createJourney = isEditor => {
    let sharedtoGroups = [];
    this.state.journey.teams.map(team => {
      sharedtoGroups.push(team.id);
    });
    let payload = {
      message: this.state.journey.message || this.state.journey.title,
      title: this.state.journey.title,
      state: 'draft',
      card_type: 'journey',
      card_subtype: this.state.journey.cardSubtype,
      team_ids: sharedtoGroups
    };
    if (this.state.journey.cardSubtype === 'weekly') {
      payload['journey_start_date'] = this.state.journey.journey_start_date;
    }
    payload.card_badging_attributes = this.state.journey.card_badging_attributes;

    if (
      this.state.journey.badging &&
      !!this.state.journey.badging.id &&
      !!this.state.journey.badging.title
    ) {
      payload['card_badging_attributes'] = {
        title: this.state.journey.badging.title,
        badge_id: this.state.journey.badging.id
      };
    }
    if (this.state.journey.tags || !!this.state.journey.tags.length) {
      payload.topics = this.state.journey.tags;
    }
    if (this.state.journey.channelIds) {
      payload.channel_ids = this.state.journey.channelIds;
    }
    payload.filestack = this.state.journey.filestack;
    payload.provider_image = this.state.journey.providerImage;
    payload.provider = this.state.journey.provider;
    if (this.state.journey.eclDurationMetadata) {
      payload.duration = this.state.journey.eclDurationMetadata.calculated_duration_display;
    }

    postv2({ card: payload }, this.state.journey && this.state.journey.id)
      .then(data => {
        if (data && data.id) {
          let idsToAdd;
          if (isEditor) {
            let currentIds = this.state.journey.journeySection.map(item => `${item.card_id}`);
            idsToAdd = difference(this.state.arrIdsSections, currentIds);
          } else {
            idsToAdd = this.state.arrIdsSections;
          }
          if (idsToAdd.length) {
            batchAddToJourney(data.id, idsToAdd)
              .then(() => {
                this.afterLastBatch(data);
              })
              .catch(err => {
                console.error(`Error in JourneyDetailsContainer.batchAddToJourney.func : ${err}`);
              });
          } else {
            this.afterLastBatch(data);
          }
          if (this.props.journey.reorderCardIds && !!this.props.journey.reorderCardIds.length) {
            this.props.journey.reorderCardIds.forEach(section => {
              customReorderPathwayCards(section.sectionId, section.cards);
            });
            this.props.dispatch(removeReorderCardIds());
          }
          if (this.state.previewMode) {
            this.props.dispatch(isPreviewMode(false));
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      })
      .catch(err => {
        console.error(`Error in JourneyDetailsContainer.postv2.func : ${err}`);
      });
  };

  afterLastBatch = data => {
    publishPathway(data.id)
      .then(() => {
        this.afterPublish(data.slug);
      })
      .catch(() => {
        if (this.state.newModalAndToast) {
          this.props.dispatch(openSnackBar('Something went wrong with create journey!'));
        } else {
          this.props.dispatch(openStatusModal('Something went wrong with create journey!'));
        }
      });
  };

  afterPublish = slug => {
    if (this.props.journey.tempJourney.cardsToLock.length) {
      this.props.journey.tempJourney.cardsToLock.forEach((sectionCardsToLock, index) => {
        if (Object.keys(sectionCardsToLock).length > 0) {
          for (let prop in sectionCardsToLock) {
            lockPathwayCard(prop, {
              pathway_id: this.state.arrIdsSections[index],
              locked: sectionCardsToLock[prop].locked
            });
          }
        }
      });
    }
    setTimeout(() => {
      this.props.dispatch(
        openStatusModal('Your Journey is ready to view!', this.openContentTab(slug))
      );
    }, 500);
  };

  openContentTab = slug => {
    this.props.dispatch(deleteTempJourney());
    this.setState({
      publishLabel: 'Publish',
      clicked: false,
      arrIdsSections: []
    });
    this.props.dispatch(push(`/me/content`));
  };

  allPathwaystoggle = () => {
    let isAllExpanded = !this.state.isAllExpanded;
    let journeySmartBites = this.state.journeySmartBites;
    let openBlocks = isAllExpanded ? times(journeySmartBites.length) : [];
    for (let i = 0; i < journeySmartBites.length; i++) {
      journeySmartBites[i].isOpenBlock = isAllExpanded;
    }
    this.setState({ journeySmartBites, isAllExpanded, openBlocks });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.journey && nextProps.journey.tempJourney && nextProps.journey.isPreviewMode) {
      let viewPublishButton = true;
      nextProps.journey.tempJourney.journeySections.map(section => {
        viewPublishButton =
          section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      });
      this.setState({
        previewMode: true,
        isLoaded: true,
        journey: nextProps.journey.tempJourney,
        journeySmartBites: nextProps.journey.tempJourney.journeySections,
        viewPublishButton
      });
    }
    if (
      !nextProps.journey.isPreviewMode &&
      nextProps.journey.isPreviewMode !== this.state.previewMode
    ) {
      this.setState({
        previewMode: false
      });
      if ((nextProps.routeParams.splat || nextProps.routeParams.slug) !== this.state.slug) {
        this.setState({
          slug: nextProps.routeParams.splat || nextProps.routeParams.slug
        });
      }
      this.updateJourney();
    }
  }

  isWeekly = i => {
    let cardDate = new Date(this.state.journey.journeySection[i].start_date);
    let nowDate = new Date();
    let nextSevenDay = new Date(cardDate);
    let dateValue = nextSevenDay.getDate() + 7;
    nextSevenDay.setDate(dateValue);
    return (
      this.state.journey.cardSubtype === 'weekly' &&
      !(nowDate >= cardDate && nowDate <= nextSevenDay)
    );
  };

  uploadChildCards = (i, j, journey, data, isOwner, isMentioned) => {
    return new Promise(resolve => {
      if (this.isWeekly(i) && !isOwner) {
        let locked = true;
        let card = { locked };
        data.cards.push(card);
        resolve();
      } else {
        this.fetchCardById(journey.journeySection[i].pack_cards[j].card_id)
          .then(card => {
            if (
              this.props.routeParams &&
              this.props.routeParams.cardId &&
              this.props.routeParams.cardId == card.id &&
              isMentioned
            ) {
              this.props.dispatch(
                openJourneyOverviewModal(
                  this.state.journey,
                  logoObj,
                  this.state.defaultImage,
                  this.cardUpdated.bind(this, this.state.journey.id),
                  null,
                  j + 1,
                  null,
                  null,
                  null,
                  i,
                  true
                )
              );
            }
            if (card && !card.id && card.message === 'You are not authorized to access this card') {
              card.isPrivate = true;
              card.id = journey.journeySection[i].pack_cards[j].card_id;
            }
            card.locked = false;
            data.cards[j] = card;
            resolve();
          })
          .catch(err => {
            console.error(
              `Error in JourneyDetailsContainer.uploadChildCards.fetchCardById.func : ${err}`
            );
          });
      }
    });
  };

  uploadSectionCards = (i, journeySmartBites, journey, isOwner, isMentioned) => {
    return new Promise(resolve => {
      let data = this.state.journey.journeySection[i];
      data.isOpenBlock = false;
      journeySmartBites[i] = data;
      let countChild = 0;
      data.cards = [];
      if (journey.journeySection[i].pack_cards.length === 0) {
        resolve();
      }
      for (let j = 0; j < journey.journeySection[i].pack_cards.length; j++) {
        this.uploadChildCards(i, j, journey, data, isOwner, isMentioned)
          .then(() => {
            countChild++;
            if (countChild === this.state.journey.journeySection[i].pack_cards.length) {
              resolve();
            }
          })
          .catch(err => {
            console.error(
              `Error in JourneyDetailsContainer.uploadSectionCards.uploadChildCards.func : ${err}`
            );
          });
      }
    });
  };

  updateJourney = (recordAnalytics, isMentioned) => {
    this.fetchCardById(this.state.slug)
      .then(journey => {
        if (journey) {
          // Record a visit to this pathway
          if (recordAnalytics) {
            try {
              recordVisit(journey.id);
            } catch (e) {}
          }

          let isOwner = journey.author && journey.author.id == this.state.currentUser.id;
          let isCompleted =
            journey.completionState && journey.completionState.toUpperCase() === 'COMPLETED';
          this.setCheckedCard(journey);
          this.setState({
            journey,
            isCompleted,
            isUpvoted: journey.isUpvoted,
            commentsCount: journey.commentsCount,
            completedPercentage: journey.completedPercentage,
            isOwner,
            reviewStatusLabel: isOwner ? 'Edit' : this.state.reviewStatusLabel,
            votesCount: journey.votesCount,
            previewMode:
              this.props.routeParams.mode && this.props.routeParams.mode === 'preview' && isOwner,
            isLoaded: true
          });
          if (journey.journeySection && journey.journeySection.length) {
            let journeySmartBites = [];
            let countCard = 0;
            for (let i = 0; i < journey.journeySection.length; i++) {
              this.uploadSectionCards(i, journeySmartBites, journey, isOwner, isMentioned)
                .then(() => {
                  countCard++;
                  journeySmartBites[i].cards.forEach((item, index) => {
                    item.isLocked = this.state.journey.journeySection[i].pack_cards[index].locked;
                  });
                  if (countCard === journey.journeySection.length) {
                    if (this.state.openBlocks && this.state.openBlocks.length) {
                      this.state.openBlocks.forEach(index => {
                        journeySmartBites[index].isOpenBlock = true;
                      });
                    }
                    this.setState({ journeySmartBites }, this.getCompleteCount);
                  }
                })
                .catch(err => {
                  console.error(
                    `Error in JourneyDetailsContainer.updateJourney.uploadSectionCards.func : ${err}`
                  );
                });
            }
          }
        }
      })
      .catch(err => {
        console.error(`Error in JourneyDetailsContainer.updateJourney.fetchCardById.func : ${err}`);
      });
  };

  completeCallback = data => {
    this.setState({ reviewStatusLabel: this.state.isOwner ? 'Edit' : '' });
    if (
      !this.state.isOwner &&
      !this.state.journeySmartBites.some(
        item =>
          item.isAssessment &&
          item.completionState === 'COMPLETED' &&
          item.attemptedOption &&
          !item.attemptedOption.is_correct
      ) &&
      data.userBadge
    ) {
      this.props.dispatch(openCongratulationModal(data.userBadge));
    }
  };

  completeClickHandler = () => {
    if (this.state.journey.state === 'published' && +this.state.completeStatus === 98) {
      markAsComplete(this.state.journey.id, { state: 'complete' })
        .then(data => {
          this.setState({ isCompleted: true, completeStatus: 100 });
          if (this.state.newModalAndToast) {
            this.props.dispatch(
              openSnackBar(
                'You have completed this Journey and can track it in completion history under Me tab',
                true,
                this.completeCallback(data)
              )
            );
          } else {
            this.props.dispatch(
              openStatusModal('You have completed this journey!', this.completeCallback(data))
            );
          }
        })
        .catch(err => {
          console.error(`Error in JourneyDetailsContainer.markAsComplete.func : ${err}`);
        });
    } else if (+this.state.completeStatus === 100) {
      markAsUncomplete(this.state.journey.id)
        .then(() => {
          this.setState({ isCompleted: false, completeStatus: 98 }, () => {
            if (this.state.newModalAndToast) {
              this.props.dispatch(openSnackBar('You have marked this Journey as incomplete', true));
            } else {
              this.props.dispatch(openStatusModal('Your have marked this journey as active'));
            }
          });
        })
        .catch(err => {
          console.error(`Error in JourneyDetailsContainer.markAsUncomplete.func : ${err}`);
        });
    } else {
      let msg =
        this.state.journey.state === 'published'
          ? 'Please complete all pending cards before marking the Journey as complete.'
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  openModal = (checkedCardId, checkedSectionId) => {
    this.props.dispatch(
      openJourneyOverviewModal(
        this.state.journey,
        logoObj,
        this.state.defaultImage,
        this.cardUpdated.bind(this, this.state.journey.id),
        null,
        checkedCardId + 1,
        null,
        null,
        null,
        checkedSectionId,
        true
      )
    );
  };

  cardUpdated = id => {
    if (id != this.state.journey.id) {
      this.findCheckedCardAtList(id);
    } else {
      this.updateJourney();
    }
  };

  getCompleteCount = () => {
    if (this.state.isCompleted) {
      this.setState({
        completeStatus: 100,
        reviewStatusLabel: this.state.isOwner ? 'Edit' : 'Completed'
      });
    } else {
      let completeArray = [];
      let countCards = 0;
      let lockedCard = 0;
      for (let i = 0; i < this.state.journeySmartBites.length; i++) {
        let sectionCardsComplete = this.state.journeySmartBites[i].cards.filter(card => {
          countCards++;
          if (card.locked) lockedCard++;
          return card.completionState && card.completionState.toUpperCase() === 'COMPLETED';
        });
        completeArray = completeArray.concat(sectionCardsComplete);
      }
      let completeCount = completeArray.length;
      let completeStatus = Math.round((completeCount * 100) / countCards);
      let fullComplete = completeStatus === 100;
      if (fullComplete) completeStatus = 98;
      let reviewStatusLabel = this.state.isOwner
        ? 'Edit'
        : fullComplete
        ? 'Mark as Complete'
        : !completeStatus && this.state.journey.completionState === null
        ? 'Start'
        : 'Continue';
      this.setState({ completeStatus, reviewStatusLabel });
      if (lockedCard !== 0 && countCards - completeCount === lockedCard) {
        this.setState({
          allCardsLocked: true
        });
      }
    }
  };

  findCheckedCardAtList = id => {
    this.fetchCardById(id)
      .then(data => {
        let journeySmartBites = this.state.journeySmartBites;
        let findCardIndex;
        let findSectionIndex;
        for (let i = 0; i < journeySmartBites.length; i++) {
          let childIndex = findIndex(journeySmartBites[i].cards, card => {
            return card.id == id;
          });
          if (childIndex !== -1) {
            findCardIndex = childIndex;
            findSectionIndex = i;
          }
        }
        if (journeySmartBites[findSectionIndex]) {
          journeySmartBites[findSectionIndex].cards[findCardIndex] = data;
          this.setState({ journeySmartBites }, this.getCompleteCount);
        } else {
          this.getCompleteCount();
        }
      })
      .catch(err => {
        console.error(
          `Error in JourneyDetailsContainer.findCheckedCardAtList.fetchCardById.func : ${err}`
        );
      });
  };

  openBlock = index => {
    let journeySmartBites = this.state.journeySmartBites;
    journeySmartBites[index].isOpenBlock = !this.state.journeySmartBites[index].isOpenBlock;
    let openBlocks = this.state.openBlocks;
    if (journeySmartBites[index].isOpenBlock) {
      openBlocks.push(index);
    } else {
      openBlocks = openBlocks.filter(el => +el !== +index);
    }
    this.setState({ journeySmartBites, openBlocks });
    let isAllExpanded = false;
    this.state.journeySmartBites.map(journeySmartBite => {
      journeySmartBite.isOpenBlock ? (isAllExpanded = true) : (isAllExpanded = false);
      this.setState({ isAllExpanded });
    });
  };

  startReview = () => {
    this.openModal(0, 0);
  };

  continueReview = () => {
    let journeySmartBites = this.state.journeySmartBites;
    let firstUnCompleteCard;
    let firstUnCompleteSection;
    for (let i = 0; i < journeySmartBites.length; i++) {
      firstUnCompleteCard = findIndex(journeySmartBites[i].cards, card => {
        return !card.completionState || card.completionState.toUpperCase() !== 'COMPLETED';
      });
      if (firstUnCompleteCard !== -1) {
        firstUnCompleteSection = i;
        break;
      }
    }
    if (firstUnCompleteCard !== undefined) {
      this.openModal(firstUnCompleteCard, firstUnCompleteSection);
    }
  };

  regimeStatusClick = () => {
    if (this.state.reviewStatusLabel === 'Start') {
      this.startReview();
    }
    if (this.state.reviewStatusLabel === 'Continue') {
      this.continueReview();
    }
    if (this.state.reviewStatusLabel === 'Edit') {
      this.editJourney();
    }
    if (this.state.reviewStatusLabel === 'Mark as Complete') {
      this.completeClickHandler();
    }
  };

  editJourney = () => {
    this.setState({ publishError: '' });
    this.props.dispatch(
      openJourneyCreationModal(this.state.journey, previewMode => {
        if (previewMode === 'preview') {
          this.props.routeParams.mode = 'preview';
          this.props.dispatch(push(`/journey/${this.state.journey.slug}/preview`));
        } else {
          this.props.routeParams.mode = undefined;
          this.props.dispatch(push(`/journey/${this.state.journey.slug}`));
        }
        this.updateJourney();
      })
    );
  };

  listTab = obj => {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => a.index - b.index);
    }
    return tabs.filter(tab => tab.visible !== false);
  };

  render() {
    let journey = this.state.journey;
    journey.completedPercentage = this.state.completeStatus;
    return (
      <div>
        {this.state.journeyLoading ? (
          <div>
            <div className="text-center">
              <Spinner />
            </div>
          </div>
        ) : (
          <div className="journey-details__container smartbite-details__container page-with-medium-width">
            {!this.state.previewMode && (
              <BreadcrumbV2
                card={journey}
                isStandaloneModal={this.props.isStandaloneModal}
                cardUpdated={this.props.cardUpdated}
              />
            )}
            {(journey.id || this.state.previewMode) && (
              <MainInfoSmartBite
                smartBite={journey}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                reviewStatusLabel={this.state.reviewStatusLabel}
                isCardV3={this.state.isCardV3}
                isOwner={this.state.isOwner}
                completeStatus={this.state.completedPercentage}
                regimeStatusClick={this.regimeStatusClick}
                type="journey"
                isPathwayPaid={journey.isPaid}
              />
            )}
            {this.state.journeySmartBites && this.state.isLoaded && (
              <div className="expandCollapse" onClick={this.allPathwaystoggle}>
                <span className="pointer">
                  {this.state.isAllExpanded ? tr('Collapse All') : tr('Expand All')}
                </span>
              </div>
            )}
            {this.state.journeySmartBites &&
              this.state.journeySmartBites.map((obj, indexSection) => {
                return (
                  <div className="journey-part__container" key={indexSection}>
                    <div
                      className={`${
                        obj.isOpenBlock
                          ? 'journey-section-header_open'
                          : 'journey-section-header_close'
                      } journey-section-header`}
                      onClick={() => {
                        this.openBlock(indexSection);
                      }}
                      style={{ cursor: 'pointer !important' }}
                    >
                      <input
                        type="reset"
                        style={{ textAlign: 'left' }}
                        className={`journey-section-header__input ${
                          !this.state.isOwner ? 'journey-section-header__input_notOwner' : null
                        }`}
                        value={unescape(obj.block_title)}
                      />

                      {!this.state.isOwner && (
                        <div className="progressContainer">
                          <div
                            className="progressText"
                            style={{
                              width:
                                (obj.completed_percentage >= 100 ? 100 : obj.completed_percentage) +
                                '%'
                            }}
                          >
                            {obj.completed_percentage}%
                          </div>
                          <LinearProgress
                            min={0}
                            style={this.styles.completeBar}
                            max={100}
                            mode="determinate"
                            value={obj.completed_percentage}
                          />
                        </div>
                      )}
                      <span>|</span>
                      <button className="my-icon-button journey__accordion-title">
                        {(obj.isOpenBlock && (
                          <NavigationArrowDropDown
                            style={{
                              ...this.styles.accordionTitleIcon,
                              ...this.styles.accordionTitleIcon_open
                            }}
                          />
                        )) ||
                          (!obj.isOpenBlock && (
                            <NavigationArrowDropUp
                              style={{
                                ...this.styles.accordionTitleIcon,
                                ...this.styles.accordionTitleIcon_close
                              }}
                            />
                          ))}
                      </button>
                    </div>
                    {obj.isOpenBlock && (
                      <div>
                        <div className="smartbites-block">
                          <div className="custom-card-container">
                            <div className="five-card-column vertical-spacing-medium">
                              {obj.cards &&
                                obj.cards.map((card, index) => {
                                  let smartBitesBeforeCurrent =
                                    obj.cards && obj.cards.slice(0, index);
                                  let isShowLockedCardContent =
                                    index === 0 ||
                                    (smartBitesBeforeCurrent &&
                                      smartBitesBeforeCurrent.length &&
                                      smartBitesBeforeCurrent.every(
                                        item =>
                                          item.completionState &&
                                          item.completionState.toUpperCase() === 'COMPLETED'
                                      ));
                                  return card.locked ? (
                                    <div className="card-v2 more-cards card-v2__simple-size">
                                      <Paper className="paper-card" zDepth={0}>
                                        <div className="locked-card" style={this.styles.lockedCard}>
                                          <LockedIcon
                                            style={this.styles.bigLockIcon}
                                            color="#7c7d94"
                                          />
                                        </div>
                                        <div className="close-button pathway-card-btn number-card-btn">
                                          <span>{index + 1}</span>
                                        </div>
                                      </Paper>
                                    </div>
                                  ) : (
                                    <Card
                                      key={index}
                                      inStandalone={true}
                                      card={card}
                                      showIndex={!this.state.isCardV3}
                                      journeyEditor={false}
                                      journeyDetails={journey}
                                      index={index + 1}
                                      currentSection={indexSection}
                                      author={card.author}
                                      dueAt={
                                        card.dueAt || (card.assignment && card.assignment.dueAt)
                                      }
                                      startDate={
                                        card.startDate ||
                                        (card.assignment && card.assignment.startDate)
                                      }
                                      user={this.state.currentUser}
                                      pathwayChecking={!this.state.isCompleted}
                                      hideComplete={this.findCheckedCardAtList}
                                      isPrivate={card.isPrivate}
                                      cardUpdated={this.cardUpdated.bind(this, journey.id)}
                                      closeCardModal={this.findCheckedCardAtList}
                                      moreCards={true}
                                      previewMode={this.state.previewMode}
                                      isShowLockedCardContent={isShowLockedCardContent}
                                      isStandaloneModal={this.props.isStandaloneModal}
                                      cardSplat={this.props.routeParams.splat}
                                      isPartOfPathway={true}
                                    />
                                  );
                                })}
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                );
              })}
            {this.state.previewMode && (
              <div className="action-buttons text-center">
                <div className="error-text">{tr(this.state.publishError)}</div>
                <FlatButton
                  label={tr('Back to Edit')}
                  className="back"
                  disabled={this.state.clicked}
                  style={this.state.clicked ? this.styles.disabled : this.styles.reviewBtn}
                  labelStyle={this.styles.previewLabel}
                  onTouchTap={this.editJourney}
                />

                {this.state.viewPublishButton && (
                  <FlatButton
                    label={tr(this.state.publishLabel)}
                    className="publish"
                    disabled={this.state.clicked}
                    onTouchTap={this.publishClickHandler}
                    style={this.state.clicked ? this.styles.disabled : this.styles.publishBtn}
                    labelStyle={this.styles.previewLabel}
                  />
                )}
              </div>
            )}
            {journey.id && (
              <CommentsBlockSmartBite
                smartBite={journey}
                checkedCard={this.state.checkedCard}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                isUpvoted={this.state.isUpvoted}
                votesCount={this.state.votesCount}
                isOwner={this.state.isOwner}
                cardUpdated={this.cardUpdated.bind(this, journey.id)}
                isCompleted={this.state.isCompleted}
                type="JourneyStandAlone"
                isCardV3={this.state.isCardV3}
                completeClickHandler={this.completeClickHandler.bind(this)}
                completeStatus={this.state.completeStatus}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

JourneyDetailsContainer.propTypes = {
  routeParams: PropTypes.object,
  journey: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  isStandaloneModal: PropTypes.bool,
  cardUpdated: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS(),
    journey: state.journey ? state.journey.toJS() : {}
  };
}

export default connect(mapStoreStateToProps)(JourneyDetailsContainer);
