import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { recordVisit } from 'edc-web-sdk/requests/analytics';

import uniq from 'lodash/uniq';
import difference from 'lodash/difference';
import times from 'lodash/times';
import findIndex from 'lodash/findIndex';
import { tr } from 'edc-web-sdk/helpers/translations';

import FlatButton from 'material-ui/FlatButton';
import { fetchCard, postv2 } from 'edc-web-sdk/requests/cards';
import { fetchJourney, fetchPathwayCardsInsideJourney } from 'edc-web-sdk/requests/journeys';
import colors from 'edc-web-sdk/components/colors/index';

import {
  markAsComplete,
  markAsUncomplete,
  createLeap,
  updateLeap,
  lockPathwayCard
} from 'edc-web-sdk/requests/cards.v2';
import {
  publishPathway,
  batchAddToJourney,
  customReorderPathwayCards,
  addCardsToPathways
} from 'edc-web-sdk/requests/pathways.v2';
import Paper from 'edc-web-sdk/components/Paper';

import {
  openJourneyCreationModal,
  openJourneyOverviewModalV2,
  openStatusModal,
  openCongratulationModal
} from '../../actions/modalActions';
import {
  deleteTempJourney,
  isPreviewMode,
  removeReorderCardIds,
  saveConsumptionJourneyOpenBlock,
  saveConsumptionJourneyPathwayPayload
} from '../../actions/journeyActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { open as openSnackBar } from '../../actions/snackBarActions';

import MainInfoSmartBite from '../common/MainInfoSmartBite';
import CommentsBlockSmartBite from '../common/CommentsBlockSmartBite';

import * as logoType from '../../constants/logoTypes';
import BreadcrumbV2 from '../common/BreadcrumpV2';

import Spinner from '../common/spinner';
import JourneyDetailsSection from './JourneyDetailsSection';
import { startAssignment } from '../../actions/cardsActions';

const logoObj = logoType.LOGO;
const lightPurp = '#acadc1';

class JourneyDetailsContainerV2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      slug: props.routeParams.splat || props.routeParams.slug,
      defaultImage: '/i/images/courses/course' + (Math.floor(Math.random() * 48) + 1) + '.jpg',
      reviewStatusLabel: '',
      publishLabel: 'Publish',
      isCompleted: false,
      isUpvoted: false,
      commentsCount: 0,
      completeStatus: 0,
      votesCount: 0,
      clicked: false,
      journeySmartBites: [],
      journeyPathwayPayload: [],
      publishError: '',
      isLoaded: false,
      journey: {},
      arrIdsSections: [],
      viewPublishButton: false,
      isAllExpanded: false,
      openBlocks: [],
      journeyLoading: true,
      isCardV3: window.ldclient.variation('card-v3', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false),
      limit: 10,
      modalLimit: 1000,
      unavailable: false,
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      fromConsumptionJourney: false,
      pathwaysCompletionBehaviour:
        props.team &&
        props.team.get('OrgConfig') &&
        props.team.get('OrgConfig').pathways &&
        props.team.get('OrgConfig').pathways['web/pathways/pathwaysCompletionBehaviour']
          ? props.team.get('OrgConfig').pathways['web/pathways/pathwaysCompletionBehaviour']
              .defaultValue
          : 'manuallyCompletion'
    };

    this.styles = {
      publishBtn: {
        color: colors.white,
        backgroundColor: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '12px',
        minWidth: '144px',
        height: '32px'
      },
      reviewBtn: {
        color: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '12px',
        minWidth: '144px',
        height: '32px'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      }
    };
    this.completeClickHandler = this.completeClickHandler.bind(this);
  }

  setCheckedCard = (card, callback) => {
    let checkedCard = card;
    checkedCard.isCompleted =
      card.completionState && card.completionState.toUpperCase() === 'COMPLETED';
    checkedCard.isUpvoted = card.isUpvoted;
    checkedCard.commentsCount = card.commentsCount;
    checkedCard.votesCount = card.votesCount;
    this.setState({ checkedCard }, callback);
  };

  fetchCardById = id => {
    return fetchCard(id, true)
      .then(data => {
        this.setState({ journeyLoading: false });
        return data;
      })
      .catch(err => {
        console.error(`Error in JourneyDetailsContainerV2.fetchCardById.fetchCard.func : ${err}`);
      });
  };

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser.toJS()
    );
    this.props.dispatch(userInfoCallBack);
    let isOwner =
      this.props.journey &&
      this.props.journey.get('tempJourney') &&
      this.props.journey.get('tempJourney').author &&
      this.props.journey.get('tempJourney').author.id == this.props.currentUser.get('id');
    if (this.props.journey && this.props.journey.get('isPreviewMode') && isOwner) {
      this.previewJourney();
    } else {
      let journeyConsumption = this.props.journey && this.props.journey.get('consumptionJourney');
      let slug = this.state.slug;
      if (
        this.state.isCardV3 &&
        this.state.journeyConsumptionV2 &&
        journeyConsumption &&
        journeyConsumption.author &&
        journeyConsumption.slug === slug
      ) {
        this.setState(
          {
            fromConsumptionJourney: true
          },
          () => {
            this.consumptionJourney(journeyConsumption);
          }
        );
      } else {
        this.updateJourney(true, this.props.routeParams && this.props.routeParams.cardId);
      }
    }
  }

  consumptionJourney = async journeyConsumption => {
    await this.checkToCompletedCard(journeyConsumption);
    await this.setStartedReview(journeyConsumption);
    this.updateJourney();
  };

  async setStartedReview(journeyConsumption) {
    if (journeyConsumption.completionState === null) {
      await startAssignment(journeyConsumption.id).catch(() => {
        console.error(`Error in JourneyDetailsContainerV2.setStartedReview.func`);
      });
    }
  }

  async checkToCompletedCard(journeyConsumption) {
    return new Promise((resolve, reject) => {
      let journeyDetails = journeyConsumption.journeySection;
      let card =
        this.props.journey &&
        this.props.journey.get('consumptionJourneyHistory') &&
        this.props.journey.get('consumptionJourneyHistory').card;
      if (card) {
        let currentCard = {};
        loopJourneySection: for (let i = 0; i < journeyDetails.length; i++) {
          for (let j = 0; j < journeyDetails[i].cards.length; j++) {
            if (journeyDetails[i].cards[j].id == card.id) {
              currentCard = journeyDetails[i].cards[j];
              break loopJourneySection;
            }
          }
        }
        let isCompleted =
          currentCard &&
          currentCard.completionState &&
          currentCard.completionState.toUpperCase() === 'COMPLETED';
        if (
          !isCompleted &&
          journeyConsumption &&
          journeyConsumption.autoComplete &&
          currentCard &&
          currentCard.id &&
          (currentCard.cardType === 'poll' ? currentCard.hasAttempted : true) &&
          !currentCard.locked &&
          !currentCard.isPrivate
        ) {
          markAsComplete(currentCard.id, { state: 'complete' })
            .then(completedCardStatus => {
              resolve(true);
            })
            .catch(error => {
              console.error(
                `Error in JourneyDetailsContainerV2.checkToCompletedCard.func : ${error}`
              );
              resolve(true);
            });
        } else {
          resolve(true);
        }
      } else {
        resolve(true);
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.currentUser !== nextProps.currentUser) {
      return true;
    } else if (nextState !== this.state) {
      return true;
    } else if (this.props.journey !== nextProps.journey) {
      return true;
    } else {
      return false;
    }
  }

  previewJourney = () => {
    let tempJourney = this.props.journey.get('tempJourney');
    let journeySmartBites = this.props.journey.get('tempJourney').journeySections;
    let viewPublishButton = true;
    journeySmartBites.forEach((section, index) => {
      let lockedIndex;
      viewPublishButton =
        section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      if (section.pack_cards) {
        for (let i = 0; i < section.pack_cards.length; i++) {
          let currentCard = section.pack_cards[i];
          if (
            tempJourney.cardsToLock &&
            tempJourney.cardsToLock[index] &&
            tempJourney.cardsToLock[index][currentCard.card_id || currentCard.id]
          ) {
            currentCard.isLocked =
              tempJourney.cardsToLock[index][currentCard.card_id || currentCard.id].locked ===
              'true';
          } else {
            currentCard.isLocked = currentCard.locked;
          }
          if (currentCard.isLocked) {
            lockedIndex = i;
            break;
          }
        }
      }
    });
    this.setState({
      previewMode: true,
      journey: this.props.journey.get('tempJourney'),
      isLoaded: true,
      journeySmartBites: journeySmartBites,
      viewPublishButton
    });
  };

  validationJourney = () => {
    let valid = true;
    if (!this.state.journeySmartBites) {
      valid = false;
    }
    this.state.journeySmartBites &&
      this.state.journeySmartBites.map(section => {
        if (!section.cards || (section.cards && !section.cards.length)) {
          valid = false;
        }
      });
    if (!valid) {
      let publishError = 'Please add cards to the collection before publishing!';
      this.setState({ publishError });
    }
    return valid;
  };

  publishClickHandler = () => {
    let valid = this.validationJourney();
    if (valid) {
      this.setState({ publishLabel: 'Publishing...', clicked: true });
      this.startCreateJourney();
    }
  };

  async startCreateJourney() {
    let sharedtoGroups = [];
    this.state.journey.teams.map(team => {
      sharedtoGroups.push(team.id);
    });
    let sections = this.state.journeySmartBites;
    let isEditor = this.state.journey && this.state.journey.id;
    let batchAddToPathwaysData = [];
    let leapJson = [];
    if (!isEditor) {
      for (let i = 0; i < sections.length; i++) {
        let payload = {
          card: {
            message: sections[i].block_title,
            title: '',
            state: 'draft',
            card_type: 'pack',
            card_subtype: 'simple',
            hidden: 1,
            topics: [],
            channel_ids: [],
            team_ids: sharedtoGroups
          }
        };
        let data = await postv2(payload);
        if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(data.id);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await uniq(sections[i].pack_cards.map(card => card.id || card.cardId));
            batchAddToPathwaysData.push({ section_id: data.id, content_ids: ids });
            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payloadForLeap = {
                  leap_id: leapObj.leapId,
                  card_id: leapObj.cardId,
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                leapJson.push(payloadForLeap);
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      }
      if (batchAddToPathwaysData.length > 0) {
        await addCardsToPathways({ data: batchAddToPathwaysData });
      }
      for (let iterator = 0; iterator <= leapJson.length - 1; iterator++) {
        let payloadForLeap = {
          correct_id: leapJson[iterator].correct_id,
          wrong_id: leapJson[iterator].wrong_id,
          pathway_id: leapJson[iterator].pathway_id
        };
        if (leapJson[iterator].leap_id) {
          updateLeap(leapJson[iterator].card_id, leapJson[iterator].leap_id, payloadForLeap);
        } else {
          createLeap(leapJson[iterator].card_id, payloadForLeap);
        }
      }
      await this.createJourney(false);
      return;
    } else {
      for (let i = 0; i < sections.length; i++) {
        let cardId = sections[i].card_id;
        let current = this.state.journey.journeySection.find(item => item.card_id == cardId);
        let data = {};
        if (!cardId || sections[i].block_title !== current.block_title) {
          let payload = {
            card: {
              message: sections[i].block_message || '',
              title: sections[i].block_title,
              state: 'draft',
              card_type: 'pack',
              card_subtype: 'simple',
              hidden: 1,
              topics: [],
              channel_ids: [],
              team_ids: sharedtoGroups
            }
          };
          data = await postv2(payload, cardId);
        } else {
          data.id = cardId;
        }
        if (data && data.id) {
          let arrIdsSections = this.state.arrIdsSections;
          arrIdsSections.push(`${data.id}`);
          this.setState({ arrIdsSections });
          if (sections[i].pack_cards && sections[i].pack_cards.length) {
            let ids = await uniq(
              sections[i].pack_cards.map(card => `${card.id || card.cardId || card.card_id}`)
            );
            let currentIds =
              current &&
              current.pack_cards &&
              current.pack_cards.map(item => {
                return item.card_id + '';
              });
            let idsToAdd = difference(ids, currentIds);
            if (idsToAdd && idsToAdd.length) {
              batchAddToPathwaysData.push({ section_id: data.id, content_ids: idsToAdd });
            }
            if (sections[i].arrayToLeap && sections[i].arrayToLeap.length) {
              sections[i].arrayToLeap.forEach(leapObj => {
                let payloadForLeap = {
                  leap_id: leapObj.leapId,
                  card_id: leapObj.cardId,
                  correct_id: leapObj.correctId,
                  wrong_id: leapObj.wrongId,
                  pathway_id: data.id
                };
                leapJson.push(payloadForLeap);
              });
            }
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with update journey!' });
        }
      }
      if (batchAddToPathwaysData.length > 0) {
        await addCardsToPathways({ data: batchAddToPathwaysData });
      }
      for (let iterator = 0; iterator <= leapJson.length - 1; iterator++) {
        let payloadForLeap = {
          correct_id: leapJson[iterator].correct_id,
          wrong_id: leapJson[iterator].wrong_id,
          pathway_id: leapJson[iterator].pathway_id
        };
        if (leapJson[iterator].leap_id) {
          updateLeap(leapJson[iterator].card_id, leapJson[iterator].leap_id, payloadForLeap);
        } else {
          createLeap(leapJson[iterator].card_id, payloadForLeap);
        }
      }
      await this.createJourney(true);
      return;
    }
  }

  createJourney = isEditor => {
    let sharedtoGroups = [];
    this.state.journey.teams.map(team => {
      sharedtoGroups.push(team.id);
    });
    let payload = {
      message: this.state.journey.message || this.state.journey.title,
      title: this.state.journey.title,
      state: 'draft',
      card_type: 'journey',
      card_subtype: this.state.journey.cardSubtype,
      team_ids: sharedtoGroups
    };
    if (this.state.journey.cardSubtype === 'weekly') {
      payload['journey_start_date'] = this.state.journey.journey_start_date;
    }
    payload.card_badging_attributes = this.state.journey.card_badging_attributes;

    if (
      this.state.journey.badging &&
      !!this.state.journey.badging.id &&
      !!this.state.journey.badging.title
    ) {
      payload['card_badging_attributes'] = {
        title: this.state.journey.badging.title,
        badge_id: this.state.journey.badging.id
      };
    }
    if (this.state.journey.tags || !!this.state.journey.tags.length) {
      payload.topics = this.state.journey.tags;
    }
    if (this.state.journey.channelIds) {
      payload.channel_ids = this.state.journey.channelIds;
    }
    payload.filestack = this.state.journey.filestack;
    payload.provider_image = this.state.journey.providerImage;
    payload.provider = this.state.journey.provider;
    if (this.state.journey.eclDurationMetadata) {
      payload.duration = this.state.journey.eclDurationMetadata.calculated_duration_display;
    }

    postv2({ card: payload }, this.state.journey && this.state.journey.id)
      .then(data => {
        if (data && data.id) {
          let idsToAdd;
          if (isEditor) {
            let currentIds = this.state.journey.journeySection.map(item => `${item.card_id}`);
            idsToAdd = difference(this.state.arrIdsSections, currentIds);
          } else {
            idsToAdd = this.state.arrIdsSections;
          }
          if (idsToAdd.length) {
            batchAddToJourney(data.id, idsToAdd)
              .then(() => {
                this.afterLastBatch(data);
              })
              .catch(err => {
                console.error(`Error in JourneyDetailsContainerV2.batchAddToJourney.func : ${err}`);
              });
          } else {
            this.afterLastBatch(data);
          }
          if (
            this.props.journey.get('reorderCardIds') &&
            !!this.props.journey.get('reorderCardIds').length
          ) {
            this.props.journey.get('reorderCardIds').forEach(section => {
              customReorderPathwayCards(section.sectionId, section.cards);
            });
            this.props.dispatch(removeReorderCardIds());
          }
          if (this.state.previewMode) {
            this.props.dispatch(isPreviewMode(false));
          }
        } else {
          this.setState({ createErrorText: 'Something went wrong with create journey!' });
        }
      })
      .catch(err => {
        console.error(`Error in JourneyDetailsContainerV2.postv2.func : ${err}`);
      });
  };

  afterLastBatch = data => {
    publishPathway(data.id)
      .then(() => {
        this.afterPublish(data.slug);
      })
      .catch(() => {
        if (this.state.newModalAndToast) {
          this.props.dispatch(openSnackBar('Something went wrong with create journey!'));
        } else {
          this.props.dispatch(openStatusModal('Something went wrong with create journey!'));
        }
      });
  };

  afterPublish = slug => {
    if (this.props.journey.get('tempJourney').cardsToLock.length) {
      this.props.journey.get('tempJourney').cardsToLock.forEach((sectionCardsToLock, index) => {
        if (Object.keys(sectionCardsToLock).length > 0) {
          for (let prop in sectionCardsToLock) {
            lockPathwayCard(prop, {
              pathway_id: this.state.arrIdsSections[index],
              locked: sectionCardsToLock[prop].locked
            });
          }
        }
      });
    }
    setTimeout(() => {
      if (this.state.newModalAndToast) {
        this.props.dispatch(
          openSnackBar('Your Journey is ready to view!', () => {
            this.openContentTab(slug);
          })
        );
      } else {
        this.props.dispatch(
          openStatusModal('Your Journey is ready to view!', this.openContentTab(slug))
        );
      }
    }, 500);
  };

  openContentTab = slug => {
    this.props.dispatch(deleteTempJourney());
    this.setState({
      publishLabel: 'Publish',
      clicked: false,
      arrIdsSections: []
    });
    this.props.dispatch(push(`/me/content`));
  };

  allPathwaysToggle = () => {
    let isAllExpanded = !this.state.isAllExpanded;
    let journeySmartBites = this.state.journeySmartBites;
    let openBlocks = isAllExpanded ? times(journeySmartBites.length) : [];
    for (let i = 0; i < journeySmartBites.length; i++) {
      journeySmartBites[i].isOpenBlock = isAllExpanded;
      if (
        isAllExpanded &&
        !journeySmartBites[i].cards.length &&
        journeySmartBites[i].totalCount !== 0
      ) {
        journeySmartBites[i].showLoader = true;
        this.setState({ journeySmartBites, openBlocks }, () => {
          if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
            this.props.dispatch(saveConsumptionJourneyOpenBlock(openBlocks));
          }
          let payload = {
            pathway_id: journeySmartBites[i].id,
            is_standalone_page: true,
            limit: this.state.limit,
            offset: journeySmartBites[i].offset
          };
          fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
            .then(data => {
              for (let j = 0; j < data.cards.length; j++) {
                if (!this.state.isOwner && this.isWeekly(i)) {
                  data.cards[j] = { locked: true, isLocked: false };
                } else {
                  if (
                    data.cards[j] &&
                    data.cards[j].message === 'You are not authorized to access this card'
                  ) {
                    data.cards[j].isPrivate = true;
                  }
                  data.cards[j].locked = false;
                }
              }
              let journeyPathwayPayload = this.state.journeyPathwayPayload;
              journeyPathwayPayload.push({ id: journeySmartBites[i].id, payload: payload });
              journeySmartBites[i].cards = data.cards;
              journeySmartBites[i].showLoader = false;
              journeySmartBites[i].offset = journeySmartBites[i].offset + this.state.limit;
              journeySmartBites[i].totalCount = data.totalCount;
              this.setState({ journeySmartBites, journeyPathwayPayload });
              if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
                this.props.dispatch(saveConsumptionJourneyPathwayPayload(journeyPathwayPayload));
              }
            })
            .catch(err => {
              journeySmartBites[i].showLoader = false;
              this.setState({ journeySmartBites });
              console.error(
                `Error in JourneyDetailsContainerV2.fetchPathwayCardsInsideJourney.allPathwaysToggle.func : ${err}`
              );
            });
        });
      } else if (isAllExpanded && !journeySmartBites[i].cards.length) {
        if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
          this.props.dispatch(saveConsumptionJourneyOpenBlock(openBlocks));
        }
        this.setState({ journeySmartBites, openBlocks });
      }
    }
    if (!isAllExpanded) {
      this.setState({ journeySmartBites, isAllExpanded, openBlocks });
    } else {
      this.setState({ isAllExpanded, openBlocks });
    }
    if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
      this.props.dispatch(saveConsumptionJourneyOpenBlock(openBlocks));
    }
  };

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.journey &&
      nextProps.journey.get('tempJourney') &&
      nextProps.journey.get('isPreviewMode')
    ) {
      let viewPublishButton = true;
      nextProps.journey.get('tempJourney').journeySections.map(section => {
        viewPublishButton =
          section.pack_cards && section.pack_cards.length ? viewPublishButton : false;
      });
      this.setState({
        previewMode: true,
        isLoaded: true,
        journey: nextProps.journey.get('tempJourney'),
        journeySmartBites: nextProps.journey.get('tempJourney').journeySections,
        viewPublishButton
      });
    }
    if (
      !nextProps.journey.get('isPreviewMode') &&
      nextProps.journey.get('isPreviewMode') !== this.state.previewMode
    ) {
      this.setState({
        previewMode: false
      });
      if ((nextProps.routeParams.splat || nextProps.routeParams.slug) !== this.state.slug) {
        this.setState({
          slug: nextProps.routeParams.splat || nextProps.routeParams.slug
        });
      }
      this.updateJourney();
    }
  }

  isWeekly = i => {
    let cardDate = new Date(this.state.journey.journeySection[i].start_date);
    let nowDate = new Date();
    let nextSevenDay = new Date(cardDate);
    let dateValue = nextSevenDay.getDate() + 7;
    nextSevenDay.setDate(dateValue);
    return (
      this.state.journey.cardSubtype === 'weekly' &&
      !(nowDate >= cardDate && nowDate <= nextSevenDay)
    );
  };

  uploadChildJourneyCards = (i, j, journey, data, isOwner, isMentioned, card) => {
    return new Promise(resolve => {
      if (!isOwner && this.isWeekly(i)) {
        data.cards.push({ locked: true, isLocked: false });
        resolve();
      } else {
        if (card && card.message === 'You are not authorized to access this card') {
          card.isPrivate = true;
        }
        data.cards[j] = card;
        resolve();
      }
    });
  };

  uploadJourneySectionCards = (i, journeySmartBites, journey, isOwner, isMentioned) => {
    return new Promise(resolve => {
      let data = this.state.journey.journeySection[i];
      data.isOpenBlock = false;
      journeySmartBites[i] = data;
      let countChild = 0;
      data.cards = [];
      data.offset = 0;
      let journeyPathwayPayload =
        this.state.journeyPathwayPayload.length > 0
          ? this.state.journeyPathwayPayload
          : this.props.journey && this.props.journey.get('consumptionJourneyPathwayPayload')
          ? this.props.journey.get('consumptionJourneyPathwayPayload')
          : [];
      if (journeyPathwayPayload.length === 0) {
        resolve();
      }
      let childIndex = findIndex(journeyPathwayPayload, journeySectionData => {
        return journeySectionData.id === journey.journeySection[i].id;
      });

      if (childIndex !== -1) {
        let payload = Object.assign({}, journeyPathwayPayload[childIndex].payload);
        let currentOffset = payload.offset;
        payload.limit = payload.limit + payload.offset;
        payload.offset = 0;
        fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
          .then(cardData => {
            data.offset = currentOffset + this.state.limit;
            data.totalCount = cardData.totalCount;
            if (cardData.cards.length === 0) {
              resolve();
            }
            for (let k = 0; k < cardData.cards.length; k++) {
              this.uploadChildJourneyCards(
                i,
                k,
                journey,
                data,
                isOwner,
                isMentioned,
                cardData.cards[k]
              )
                .then(() => {
                  countChild++;
                  if (countChild === cardData.cards.length) {
                    resolve();
                  }
                })
                .catch(err => {
                  console.error(
                    `Error in JourneyDetailsContainerV2.uploadJourneySectionCards.uploadChildJourneyCards.func : ${err}`
                  );
                });
            }
          })
          .catch(err => {
            console.error(
              `Error in JourneyDetailsContainerV2.uploadJourneySectionCards.fetchPathwayCardsInsideJourney.func : ${err}`
            );
          });
      } else {
        resolve();
      }
    });
  };

  updateJourney = (recordAnalytics, isMentioned) => {
    let payload = { is_standalone_page: true };
    fetchJourney(this.state.slug, payload)
      .then(journey => {
        if (journey) {
          this.setState({ journeyLoading: false });
          for (let i = 0; i < journey.journeySection.length; i++) {
            journey.journeySection[i].showLoader = false;
            journey.journeySection[i].offset = 0;
          }
          // Record a visit to this pathway
          if (recordAnalytics) {
            try {
              recordVisit(journey.id);
            } catch (e) {}
          }

          let isOwner = journey.author && journey.author.id == this.props.currentUser.get('id');
          let isCompleted =
            journey.completionState && journey.completionState.toUpperCase() === 'COMPLETED';
          this.setCheckedCard(journey);
          this.setState({
            journey,
            isCompleted,
            isUpvoted: journey.isUpvoted,
            commentsCount: journey.commentsCount,
            completedPercentage: journey.completedPercentage,
            isOwner,
            reviewStatusLabel: isOwner ? 'Edit' : this.state.reviewStatusLabel,
            votesCount: journey.votesCount,
            previewMode:
              this.props.routeParams.mode && this.props.routeParams.mode === 'preview' && isOwner,
            isLoaded: true
          });
          if (journey.journeySection && journey.journeySection.length) {
            let journeySmartBites = [];
            let countCard = 0;
            if (this.props.routeParams && this.props.routeParams.cardId && isMentioned) {
              for (let i = 0; i < journey.journeySection.length; i++) {
                for (let j = 0; j < journey.journeySection[i].card_pack_ids.length; j++) {
                  if (journey.journeySection[i].card_pack_ids[j] == this.props.routeParams.cardId) {
                    this.props.dispatch(
                      openJourneyOverviewModalV2(
                        this.state.journey,
                        logoObj,
                        this.state.defaultImage,
                        this.cardUpdated.bind(this, this.state.journey.id),
                        null,
                        j + 1,
                        null,
                        null,
                        null,
                        i,
                        true
                      )
                    );
                  }
                }
              }
            }
            for (let i = 0; i < journey.journeySection.length; i++) {
              this.uploadJourneySectionCards(i, journeySmartBites, journey, isOwner, isMentioned)
                .then(() => {
                  countCard++;
                  if (countCard === journey.journeySection.length) {
                    let openBlocks =
                      this.state.openBlocks.length > 0
                        ? this.state.openBlocks
                        : this.props.journey &&
                          this.props.journey.get('consumptionJourneyOpenBlock')
                        ? this.props.journey.get('consumptionJourneyOpenBlock')
                        : [];
                    if (openBlocks && openBlocks.length) {
                      openBlocks.forEach(index => {
                        if (journeySmartBites[index].cards.length) {
                          journeySmartBites[index].isOpenBlock = true;
                        }
                      });
                    }
                    let setStateObject = {};
                    if (
                      this.state.journeyPathwayPayload.length === 0 &&
                      this.props.journey &&
                      this.props.journey.get('consumptionJourneyPathwayPayload')
                    ) {
                      setStateObject['journeyPathwayPayload'] = this.props.journey.get(
                        'consumptionJourneyPathwayPayload'
                      );
                    }
                    if (this.state.openBlocks.length === 0 && openBlocks && openBlocks.length) {
                      setStateObject['openBlocks'] = openBlocks;
                    }
                    setStateObject['journeySmartBites'] = journeySmartBites;
                    this.setState(setStateObject, this.getCompleteCount);
                  }
                })
                .catch(err => {
                  console.error(
                    `Error in JourneyDetailsContainerV2.updateJourney.uploadJourneySectionCards.func : ${err}`
                  );
                });
            }
          }
        }
      })
      .catch(err => {
        this.setState({
          unavailable: true
        });
        console.error(
          `Error in JourneyDetailsContainerV2.updateJourney.fetchCardById.func : ${err}`
        );
      });
  };

  completeCallback = data => {
    this.setState({ reviewStatusLabel: this.state.isOwner ? 'Edit' : 'Completed' });
    if (
      !this.state.isOwner &&
      !this.state.journeySmartBites.some(
        item =>
          item.isAssessment &&
          item.completionState === 'COMPLETED' &&
          item.attemptedOption &&
          !item.attemptedOption.is_correct
      ) &&
      data.userBadge
    ) {
      this.props.dispatch(openCongratulationModal(data.userBadge));
    }
    this.cardUpdated(this.state.journey.id);
  };

  completeMainJourney = toastAutocomplete => {
    markAsComplete(this.state.journey.id, { state: 'complete' })
      .then(data => {
        this.setState({ isCompleted: true, completeStatus: 100 });
        if (this.state.newModalAndToast) {
          if (!toastAutocomplete) {
            this.props.dispatch(
              openSnackBar(
                'You have completed this Journey and can track it in completion history under Me tab',
                true,
                this.completeCallback(data)
              )
            );
          } else {
            this.props.dispatch(
              openSnackBar(
                'You have completed this Journey and can see it under your completed items.',
                true,
                this.completeCallback(data)
              )
            );
          }
        } else {
          this.props.dispatch(
            openStatusModal('You have completed this journey!', () => {
              setTimeout(this.completeCallback(data), 0);
            })
          );
        }
      })
      .catch(err => {
        console.error(
          `Error in JourneyDetailsContainerV2.completeMainJourney.markAsComplete.func : ${err}`
        );
      });
  };

  completeClickHandler = () => {
    if (this.state.journey.state === 'published' && +this.state.completeStatus === 98) {
      this.completeMainJourney();
    } else if (+this.state.completeStatus === 100) {
      markAsUncomplete(this.state.journey.id)
        .then(() => {
          this.setState({ isCompleted: false, completeStatus: 98 }, () => {
            if (this.state.newModalAndToast) {
              this.props.dispatch(openSnackBar('You have marked this Journey as incomplete', true));
            } else {
              this.props.dispatch(openStatusModal('Your have marked this journey as active'));
            }
          });
        })
        .catch(err => {
          console.error(`Error in JourneyDetailsContainerV2.markAsUncomplete.func : ${err}`);
        });
    } else {
      let msg =
        this.state.journey.state === 'published'
          ? 'Please complete all pending cards before marking the Journey as complete.'
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  openModal = (checkedCardId, checkedSectionId) => {
    if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
      if (
        checkedCardId === 0 &&
        checkedSectionId === 0 &&
        this.state.journey &&
        this.state.journey.journeySection &&
        this.state.journey.journeySection[checkedSectionId] &&
        this.state.journey.journeySection[checkedSectionId].cards &&
        this.state.journey.journeySection[checkedSectionId].cards.length === 0
      ) {
        let payload = {
          pathway_id: this.state.journey.journeySection[checkedSectionId].id,
          is_standalone_page: true,
          limit: this.state.limit,
          offset: 0
        };
        fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
          .then(data => {
            for (let i = 0; i < data.cards.length; i++) {
              if (!this.state.isOwner && this.isWeekly(0)) {
                data.cards[i] = { locked: true, isLocked: false };
              } else {
                if (
                  data.cards[i] &&
                  data.cards[i].message === 'You are not authorized to access this card'
                ) {
                  data.cards[i].isPrivate = true;
                }
                data.cards[i].locked = false;
              }
            }
            if (data.cards.length) {
              let cardId = data.cards[0].id ? data.cards[0].id : 'private-card';
              this.props.dispatch(push(`/journey/${this.state.journey.slug}/cards/${cardId}`));
            }
          })
          .catch(err => {
            console.error(
              `Error in JourneyDetailsContainerV2.openModal.fetchPathwayCardsInsideJourney.func : ${err}`
            );
          });
      } else {
        let cardId = this.state.journey.journeySection[checkedSectionId].cards[0].id
          ? this.state.journey.journeySection[checkedSectionId].cards[0].id
          : 'private-card';
        this.props.dispatch(push(`/journey/${this.state.journey.slug}/cards/${cardId}`));
      }
    } else {
      this.props.dispatch(
        openJourneyOverviewModalV2(
          this.state.journey,
          logoObj,
          this.state.defaultImage,
          this.cardUpdated.bind(this, this.state.journey.id),
          null,
          checkedCardId + 1,
          null,
          null,
          null,
          checkedSectionId,
          true
        )
      );
    }
  };

  cardUpdated = id => {
    if (id !== this.state.journey.id) {
      this.findCheckedCardAtList(id);
    } else {
      this.updateJourney();
    }
  };

  getCompleteCount = () => {
    if (this.state.isCompleted) {
      this.setState({
        completeStatus: 100,
        reviewStatusLabel: this.state.isOwner ? 'Edit' : 'Completed'
      });
    } else {
      let countPathway = 0;
      let pathwayCompletionState = 0;
      for (let i = 0; i < this.state.journeySmartBites.length; i++) {
        countPathway++;
        pathwayCompletionState += this.state.journeySmartBites[i].completed_percentage;
      }
      let completeStatus = Math.round(pathwayCompletionState / countPathway);
      let fullComplete = completeStatus === 100;
      if (fullComplete && this.state.pathwaysCompletionBehaviour === 'manuallyCompletion') {
        completeStatus = 98;
      }
      if (fullComplete && this.state.pathwaysCompletionBehaviour === 'autoCompletion') {
        if (
          this.state.journey.state === 'published' &&
          completeStatus === 100 &&
          !(
            this.state.journey.completionState &&
            this.state.journey.completionState.toUpperCase() === 'COMPLETED'
          )
        ) {
          this.completeMainJourney(true);
        }
      }
      let reviewStatusLabel = this.state.isOwner
        ? 'Edit'
        : fullComplete
        ? 'Mark as Complete'
        : !completeStatus && this.state.journey.completionState === null
        ? 'Start'
        : 'Continue';
      let setStateObject = {};
      setStateObject['completeStatus'] = completeStatus;
      setStateObject['reviewStatusLabel'] = reviewStatusLabel;
      this.setState(setStateObject);
    }
  };

  findCheckedCardAtList = id => {
    this.fetchCardById(id)
      .then(data => {
        let journeySmartBites = this.state.journeySmartBites;
        let findCardIndex;
        let findSectionIndex;
        for (let i = 0; i < journeySmartBites.length; i++) {
          let childIndex = findIndex(journeySmartBites[i].cards, card => {
            return card.id == id;
          });
          if (childIndex !== -1) {
            findCardIndex = childIndex;
            findSectionIndex = i;
          }
        }
        if (journeySmartBites[findSectionIndex]) {
          journeySmartBites[findSectionIndex].cards[findCardIndex] = data;
          this.setState({ journeySmartBites }, this.getCompleteCount);
        } else {
          this.getCompleteCount();
        }
      })
      .catch(err => {
        console.error(
          `Error in JourneyDetailsContainerV2.findCheckedCardAtList.fetchCardById.func : ${err}`
        );
      });
  };

  openBlock = (index, pathway) => {
    let journeySmartBites = this.state.journeySmartBites;
    journeySmartBites[index].isOpenBlock = !this.state.journeySmartBites[index].isOpenBlock;
    let openBlocks = this.state.openBlocks;
    if (journeySmartBites[index].isOpenBlock) {
      openBlocks.push(index);
    } else {
      openBlocks = openBlocks.filter(el => +el !== +index);
    }
    if (!journeySmartBites[index].cards.length) {
      journeySmartBites[index].showLoader = true;
      this.setState({ journeySmartBites, openBlocks }, () => {
        if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
          this.props.dispatch(saveConsumptionJourneyOpenBlock(openBlocks));
        }
        let payload = {
          pathway_id: pathway.id,
          is_standalone_page: true,
          limit: this.state.limit,
          offset: journeySmartBites[index].offset
        };
        fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
          .then(data => {
            for (let i = 0; i < data.cards.length; i++) {
              if (!this.state.isOwner && this.isWeekly(index)) {
                data.cards[i] = { locked: true, isLocked: false };
              } else {
                if (
                  data.cards[i] &&
                  data.cards[i].message === 'You are not authorized to access this card'
                ) {
                  data.cards[i].isPrivate = true;
                }
                data.cards[i].locked = false;
              }
            }
            let journeyPathwayPayload = this.state.journeyPathwayPayload;
            journeyPathwayPayload.push({ id: pathway.id, payload: payload });
            journeySmartBites[index].cards = data.cards;
            journeySmartBites[index].showLoader = false;
            journeySmartBites[index].offset = journeySmartBites[index].offset + this.state.limit;
            journeySmartBites[index].totalCount = data.totalCount;
            let setStateObject = {};
            setStateObject['journeySmartBites'] = journeySmartBites;
            setStateObject['journeyPathwayPayload'] = journeyPathwayPayload;
            let isAllExpanded = false;
            this.state.journeySmartBites.map(journeySmartBite => {
              journeySmartBite.isOpenBlock ? (isAllExpanded = true) : (isAllExpanded = false);
              setStateObject['isAllExpanded'] = isAllExpanded;
            });
            this.setState(setStateObject);
            if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
              this.props.dispatch(saveConsumptionJourneyPathwayPayload(journeyPathwayPayload));
            }
          })
          .catch(err => {
            journeySmartBites[index].showLoader = false;
            let setStateObject = {};
            setStateObject['journeySmartBites'] = journeySmartBites;
            let isAllExpanded = false;
            this.state.journeySmartBites.map(journeySmartBite => {
              journeySmartBite.isOpenBlock ? (isAllExpanded = true) : (isAllExpanded = false);
              setStateObject['isAllExpanded'] = isAllExpanded;
            });
            this.setState(setStateObject);
            console.error(
              `Error in JourneyDetailsContainerV2.fetchPathwayCardsInsideJourney.openBlock.func : ${err}`
            );
          });
      });
    } else {
      let setStateObject = {};
      setStateObject['journeySmartBites'] = journeySmartBites;
      setStateObject['openBlocks'] = openBlocks;
      let isAllExpanded = false;
      this.state.journeySmartBites.map(journeySmartBite => {
        journeySmartBite.isOpenBlock ? (isAllExpanded = true) : (isAllExpanded = false);
        setStateObject['isAllExpanded'] = isAllExpanded;
      });
      this.setState(setStateObject);
      if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
        this.props.dispatch(saveConsumptionJourneyOpenBlock(openBlocks));
      }
    }
  };

  startReview = () => {
    this.openModal(0, 0);
  };

  continueReview = async () => {
    let journeySmartBites = this.state.journeySmartBites;
    let firstUnCompleteCard;
    let firstUnCompleteSection;
    for (let i = 0; i < journeySmartBites.length; i++) {
      if (!journeySmartBites[i].cards.length) {
        let payload = {
          pathway_id: journeySmartBites[i].id,
          is_standalone_page: true,
          limit: this.state.modalLimit,
          offset: journeySmartBites[i].offset
        };
        let pathwayData = await fetchPathwayCardsInsideJourney(this.state.journey.id, payload);
        if (pathwayData && pathwayData.cards) {
          for (let j = 0; j < pathwayData.cards.length; j++) {
            if (!this.state.isOwner && this.isWeekly(i)) {
              pathwayData.cards[j] = { locked: true, isLocked: false };
            } else {
              if (
                pathwayData.cards[j] &&
                pathwayData.cards[j].message === 'You are not authorized to access this card'
              ) {
                pathwayData.cards[j].isPrivate = true;
              }
              pathwayData.cards[j].locked = false;
            }
          }
          let journeyPathwayPayload = this.state.journeyPathwayPayload;
          let pathwayPayload = {
            pathway_id: journeySmartBites[i].id,
            is_standalone_page: true,
            limit: 0,
            offset: pathwayData.totalCount
          };
          journeyPathwayPayload.push({ id: journeySmartBites[i].id, payload: pathwayPayload });
          journeySmartBites[i].cards = pathwayData.cards;
          journeySmartBites[i].showLoader = false;
          journeySmartBites[i].offset = pathwayData.totalCount;
          journeySmartBites[i].totalCount = pathwayData.totalCount;
          this.setState({ journeySmartBites, journeyPathwayPayload });
          if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
            this.props.dispatch(saveConsumptionJourneyPathwayPayload(journeyPathwayPayload));
          }
          firstUnCompleteCard = findIndex(journeySmartBites[i].cards, card => {
            return !card.completionState || card.completionState.toUpperCase() !== 'COMPLETED';
          });
          if (firstUnCompleteCard !== -1) {
            firstUnCompleteSection = i;
            break;
          }
        } else {
          this.setState({ journeySmartBites });
        }
      } else {
        firstUnCompleteCard = findIndex(journeySmartBites[i].cards, card => {
          return !card.completionState || card.completionState.toUpperCase() !== 'COMPLETED';
        });
        if (firstUnCompleteCard !== -1) {
          firstUnCompleteSection = i;
          break;
        }
      }
    }
    if (firstUnCompleteCard !== undefined) {
      this.openModal(firstUnCompleteCard, firstUnCompleteSection);
    }
  };

  regimeStatusClick = () => {
    if (this.state.reviewStatusLabel === 'Start') {
      this.startReview();
    }
    if (this.state.reviewStatusLabel === 'Continue') {
      this.continueReview();
    }
    if (this.state.reviewStatusLabel === 'Edit') {
      this.editJourney();
    }
    if (this.state.reviewStatusLabel === 'Mark as Complete') {
      this.completeClickHandler();
    }
  };

  editJourney = () => {
    this.setState({ publishError: '' });
    this.props.dispatch(
      openJourneyCreationModal(this.state.journey, previewMode => {
        if (previewMode === 'preview') {
          this.props.routeParams.mode = 'preview';
          this.props.dispatch(push(`/journey/${this.state.journey.slug}/preview`));
        } else {
          this.props.routeParams.mode = undefined;
          this.props.dispatch(push(`/journey/${this.state.journey.slug}`));
        }
        this.updateJourney();
      })
    );
  };

  loadMore = (index, pathway) => {
    let journeySmartBites = this.state.journeySmartBites;
    journeySmartBites[index].showLoader = true;
    this.setState({ journeySmartBites }, () => {
      let payload = {
        pathway_id: pathway.id,
        is_standalone_page: true,
        limit: this.state.limit,
        offset: journeySmartBites[index].offset
      };
      fetchPathwayCardsInsideJourney(this.state.journey.id, payload)
        .then(data => {
          for (let i = 0; i < data.cards.length; i++) {
            if (!this.state.isOwner && this.isWeekly(index)) {
              data.cards[i] = { locked: true, isLocked: false };
            } else {
              if (
                data.cards[i] &&
                data.cards[i].message === 'You are not authorized to access this card'
              ) {
                data.cards[i].isPrivate = true;
              }
              data.cards[i].locked = false;
            }
          }
          let journeyPathwayPayload = this.state.journeyPathwayPayload;
          for (let j = 0; j <= journeyPathwayPayload.length - 1; j++) {
            if (journeyPathwayPayload[j].id === pathway.id) {
              journeyPathwayPayload[j] = { id: pathway.id, payload: payload };
            }
          }
          journeySmartBites[index].cards = [...journeySmartBites[index].cards, ...data.cards];
          journeySmartBites[index].showLoader = false;
          journeySmartBites[index].offset = journeySmartBites[index].offset + this.state.limit;
          journeySmartBites[index].totalCount = data.totalCount;
          this.setState({ journeySmartBites, journeyPathwayPayload });
          if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
            this.props.dispatch(saveConsumptionJourneyPathwayPayload(journeyPathwayPayload));
          }
        })
        .catch(err => {
          journeySmartBites[index].showLoader = false;
          this.setState({ journeySmartBites });
          console.error(
            `Error in JourneyDetailsContainerV2.fetchPathwayCardsInsideJourney.loadMore.func : ${err}`
          );
        });
    });
  };

  render() {
    let journey = this.state.journey;
    journey.completedPercentage = this.state.completeStatus;

    if (this.state.unavailable) {
      return (
        <div className="row container-padding">
          <Paper className="small-12">
            <div className="container-padding vertical-spacing-large text-center">
              <p>{tr('This content does not exist or has been deleted.')}</p>
            </div>
          </Paper>
        </div>
      );
    }

    return (
      <div>
        {this.state.journeyLoading ? (
          <div>
            <div className="text-center">
              <Spinner />
            </div>
          </div>
        ) : (
          <div
            className={`journey-details__container smartbite-details__container page-with-medium-width ${
              this.state.isCardV3 ? 'smartbite-details__container_v3' : ''
            }`}
          >
            {!this.state.previewMode && (
              <BreadcrumbV2
                isStandaloneModal={this.props.isStandaloneModal}
                cardUpdated={this.props.cardUpdated}
                isConsumptionPage={
                  this.state.fromConsumptionJourney &&
                  this.props.journey &&
                  this.props.journey.get('consumptionJourney') &&
                  this.props.journey.get('consumptionJourneyHistoryUrl')
                    ? this.props.journey.get('consumptionJourneyHistoryUrl')
                    : this.state.fromConsumptionJourney &&
                      this.props.journey &&
                      this.props.journey.get('consumptionJourney')
                    ? '/'
                    : ''
                }
              />
            )}
            {(journey.id || this.state.previewMode) && (
              <MainInfoSmartBite
                smartBite={journey}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                reviewStatusLabel={this.state.reviewStatusLabel}
                isOwner={this.state.isOwner}
                completeStatus={this.state.completedPercentage}
                regimeStatusClick={this.regimeStatusClick}
                type="journey"
                isCardV3={this.state.isCardV3}
                isPathwayPaid={journey.isPaid}
              />
            )}
            {this.state.journeySmartBites && this.state.isLoaded && (
              <div className="expandCollapse" onClick={this.allPathwaysToggle}>
                <span className="pointer">
                  {this.state.isAllExpanded ? tr('Collapse All') : tr('Expand All')}
                </span>
              </div>
            )}
            {this.state.journeySmartBites &&
              this.state.journeySmartBites.map((obj, indexSection) => {
                return (
                  <JourneyDetailsSection
                    journeySection={obj}
                    routeParams={this.props.routeParams}
                    showIndex={!this.state.isCardV3}
                    indexSection={indexSection}
                    isOwner={this.state.isOwner}
                    journey={this.state.journey}
                    currentSection={indexSection}
                    isCompleted={this.state.isCompleted}
                    previewMode={this.state.previewMode}
                    findCheckedCardAtList={this.findCheckedCardAtList}
                    cardUpdated={this.cardUpdated.bind(this, journey.id)}
                    isStandaloneModal={this.props.isStandaloneModal}
                    openBlock={this.openBlock}
                    loadMore={this.loadMore}
                  />
                );
              })}
            {this.state.previewMode && (
              <div className="action-buttons text-center">
                <div className="error-text">{tr(this.state.publishError)}</div>
                <FlatButton
                  label={tr('Back to Edit')}
                  className="back"
                  disabled={this.state.clicked}
                  style={this.state.clicked ? this.styles.disabled : this.styles.reviewBtn}
                  labelStyle={this.styles.previewLabel}
                  onTouchTap={this.editJourney}
                />

                {this.state.viewPublishButton && (
                  <FlatButton
                    label={tr(this.state.publishLabel)}
                    className="publish"
                    disabled={this.state.clicked}
                    onTouchTap={this.publishClickHandler}
                    style={this.state.clicked ? this.styles.disabled : this.styles.publishBtn}
                    labelStyle={this.styles.previewLabel}
                  />
                )}
              </div>
            )}
            {journey.id && (
              <CommentsBlockSmartBite
                smartBite={journey}
                checkedCard={this.state.checkedCard}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                isUpvoted={this.state.isUpvoted}
                votesCount={this.state.votesCount}
                isOwner={this.state.isOwner}
                cardUpdated={this.cardUpdated.bind(this, journey.id)}
                isCompleted={this.state.isCompleted}
                type="JourneyStandAlone"
                isCardV3={this.state.isCardV3}
                completeClickHandler={this.completeClickHandler.bind(this)}
                completeStatus={this.state.completeStatus}
                reviewStatusLabel={this.state.reviewStatusLabel}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

JourneyDetailsContainerV2.propTypes = {
  routeParams: PropTypes.object,
  journey: PropTypes.object,
  currentUser: PropTypes.object,
  isStandaloneModal: PropTypes.bool,
  cardUpdated: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser,
    journey: state.journey ? state.journey : {},
    team: state.team
  };
}

export default connect(mapStoreStateToProps)(JourneyDetailsContainerV2);
