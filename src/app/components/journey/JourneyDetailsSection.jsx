import React, { Component } from 'react';
import PropTypes from 'prop-types';
import JourneyDetailsSectionCard from './JourneyDetailsSectionCard';
import LinearProgress from 'material-ui/LinearProgress';
import NavigationArrowDropDown from 'react-material-icons/icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'react-material-icons/icons/navigation/arrow-drop-up';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import isEqual from 'lodash/isEqual';
import unescape from 'lodash/unescape';

class JourneyDetailsSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      completeBar: {
        backgroundColor: '#ffffff',
        borderRadius: '0.125rem',
        height: '0.25rem'
      },
      accordionTitleIcon: {
        fill: 'inherit',
        height: '34px',
        width: '34px'
      },
      accordionTitleIcon_open: {
        fill: '#ffffff'
      },
      accordionTitleIcon_close: {
        fill: '#6E708A'
      },
      showSection: {
        display: 'block'
      },
      hideSection: {
        display: 'none'
      }
    };
    this.isUpdateJourneySection = true;
    this.journeySection = Object.assign({}, props.journeySection);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!isEqual(this.journeySection, nextProps.journeySection)) {
      this.journeySection = Object.assign({}, nextProps.journeySection);
      this.isUpdateJourneySection = true;
    } else {
      this.isUpdateJourneySection = false;
    }
    return this.isUpdateJourneySection;
  }

  openBlock = () => {
    this.journeySection = Object.assign({}, this.props.journeySection);
    this.props.openBlock(this.props.indexSection, this.props.journeySection);
  };

  render() {
    return (
      <div className="journey-part__container" key={this.props.indexSection}>
        {!this.props.journeySection.visible && <div className="small-2 medium-1" />}
        <div
          className={`${
            this.props.journeySection.isOpenBlock && this.props.journeySection.visible
              ? 'journey-section-header_open'
              : 'journey-section-header_close'
          } journey-section-header`}
          onClick={() => {
            this.openBlock();
          }}
          style={{ cursor: 'pointer !important' }}
        >
          <input
            type="reset"
            title={
              this.props.journeySection.visible
                ? ''
                : 'You do not have permission to view this section'
            }
            disabled={!this.props.journeySection.visible}
            style={{ textAlign: 'left' }}
            className={`journey-section-header__input ${
              !this.props.isOwner ? 'journey-section-header__input_notOwner' : null
            }`}
            value={unescape(
              this.props.journeySection.block_title || this.props.journeySection.block_message
            )}
          />

          {!this.props.isOwner && (
            <div className="progressContainer">
              {this.props.journeySection.visible && (
                <div
                  className="progressText"
                  style={{
                    width:
                      (this.props.journeySection.completed_percentage >= 100
                        ? 100
                        : this.props.journeySection.completed_percentage) + '%'
                  }}
                >
                  {this.props.journeySection.completed_percentage}%
                </div>
              )}
              {this.props.journeySection.visible && (
                <LinearProgress
                  min={0}
                  style={this.styles.completeBar}
                  max={100}
                  mode="determinate"
                  value={this.props.journeySection.completed_percentage}
                />
              )}
            </div>
          )}
          <span>|</span>
          <button
            title={
              this.props.journeySection.visible
                ? ''
                : 'You do not have permission view this section'
            }
            disabled={!this.props.journeySection.visible}
            className="my-icon-button journey__accordion-title"
          >
            {(this.props.journeySection.isOpenBlock && this.props.journeySection.visible && (
              <NavigationArrowDropUp
                style={{
                  ...this.styles.accordionTitleIcon,
                  ...this.styles.accordionTitleIcon_open
                }}
              />
            )) ||
              ((!this.props.journeySection.isOpenBlock || !this.props.journeySection.visible) && (
                <NavigationArrowDropDown
                  style={{
                    ...this.styles.accordionTitleIcon,
                    ...this.styles.accordionTitleIcon_close
                  }}
                />
              ))}
          </button>
        </div>
        <div
          style={
            this.props.journeySection.isOpenBlock && this.props.journeySection.visible
              ? this.styles.showSection
              : this.styles.hideSection
          }
        >
          <div className="smartbites-block">
            <div className="custom-card-container">
              <div className="five-card-column vertical-spacing-medium">
                {this.props.journeySection.cards &&
                  this.props.journeySection.cards.map((card, index) => {
                    let smartBitesBeforeCurrent =
                      this.props.journeySection.cards &&
                      this.props.journeySection.cards.slice(0, index);
                    let isShowLockedCardContent =
                      index === 0 ||
                      (smartBitesBeforeCurrent &&
                        smartBitesBeforeCurrent.length &&
                        smartBitesBeforeCurrent.every(
                          item =>
                            item.completionState &&
                            item.completionState.toUpperCase() === 'COMPLETED'
                        ));
                    return (
                      <JourneyDetailsSectionCard
                        keyIndex={index}
                        card={card}
                        showIndex={this.props.showIndex}
                        journey={this.props.journey}
                        index={index + 1}
                        currentSection={this.props.currentSection}
                        pathwayChecking={!this.props.isCompleted}
                        findCheckedCardAtList={this.props.findCheckedCardAtList}
                        cardUpdated={this.props.cardUpdated}
                        previewMode={this.props.previewMode}
                        isShowLockedCardContent={isShowLockedCardContent}
                        isStandaloneModal={this.props.isStandaloneModal}
                        cardSplat={this.props.routeParams.splat}
                      />
                    );
                  })}
              </div>
              <div>
                {!this.props.journeySection.showLoader &&
                  this.props.journeySection.totalCount > this.props.journeySection.offset && (
                    <div className="journey-load-more">
                      <small>
                        <a
                          onClick={() => {
                            this.props.loadMore(this.props.indexSection, this.props.journeySection);
                          }}
                        >
                          {tr('View More')}
                        </a>
                      </small>
                    </div>
                  )}
              </div>
              <div>
                {this.props.journeySection.showLoader && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

JourneyDetailsSection.propTypes = {
  routeParams: PropTypes.object,
  journeySection: PropTypes.object,
  indexSection: PropTypes.number,
  isOwner: PropTypes.any,
  journey: PropTypes.object,
  currentSection: PropTypes.number,
  isCompleted: PropTypes.bool,
  showIndex: PropTypes.bool,
  previewMode: PropTypes.any,
  isStandaloneModal: PropTypes.bool,
  openBlock: PropTypes.func,
  findCheckedCardAtList: PropTypes.func,
  cardUpdated: PropTypes.func,
  loadMore: PropTypes.func
};

export default JourneyDetailsSection;
