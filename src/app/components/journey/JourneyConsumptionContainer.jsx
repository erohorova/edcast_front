import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BreadcrumbV2 from '../common/BreadcrumpV2';
import PrevArrow from 'edc-web-sdk/components/icons/PrevArrow';
import NextArrow from 'edc-web-sdk/components/icons/NextArrow';
import CardConsumptionContainer from '../consumption/CardConsumptionContainer';
import { fetchJourney, fetchPathwayInsideJourney } from 'edc-web-sdk/requests/journeys';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import getCardType from '../../utils/getCardType';
import { startAssignment } from '../../actions/cardsActions';
import { markAsComplete } from 'edc-web-sdk/requests/cards.v2';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import { push } from 'react-router-redux';
import {
  saveConsumptionJourney,
  removeConsumptionJourneyHistory
} from '../../actions/journeyActions';
import { Permissions } from '../../utils/checkPermissions';
import findIndex from 'lodash/findIndex';
import unescape from 'lodash/unescape';

class JourneyConsumptionContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      unavailable: false,
      journey: props.journey && props.journey.get('consumptionJourney'),
      slug: props.routeParams.slug,
      cardId: props.routeParams.cardId,
      smartBites:
        (props.journey &&
          props.journey.get('consumptionJourney') &&
          props.journey.get('consumptionJourney').packCards) ||
        [],
      checkedCard: null,
      currentIndex: 0,
      loadChecked: true,
      fetchCard: true,
      autoComplete: true,
      journeyStarted: false,
      isInProgressToComplete: false,
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true)
    };

    this.styles = {
      leftArrow: {
        float: 'left',
        width: '1.5rem',
        height: '3rem',
        padding: 0,
        border: 0,
        color: '#000000'
      },
      leftButton: {
        float: 'left',
        cursor: 'pointer'
      },
      rightArrow: {
        float: 'right',
        width: '1.5rem',
        height: '3rem',
        padding: 0,
        border: 0,
        color: '#000000'
      },
      rightButton: {
        float: 'right',
        cursor: 'pointer'
      }
    };
    this.newComplete = window.ldclient.variation('method-of-complete-cards-in-pathways', false);
    let config = this.props.team.get('OrgConfig');
    this.completeMethodConf =
      config &&
      config.pathways &&
      config.pathways['web/pathways/pathwaysComplete'] &&
      config.pathways['web/pathways/pathwaysComplete'].defaultValue;
  }

  componentDidMount() {
    if (this.state.slug && this.state.cardId) {
      if (this.props.journey && this.props.journey.get('consumptionJourneyHistory')) {
        this.props.dispatch(removeConsumptionJourneyHistory());
      }
      this.fetchJourneyDetails();
    } else {
      this.setState({
        loadChecked: false,
        unavailable: true
      });
    }
  }

  fetchJourneyDetails = () => {
    let payload = { is_standalone_page: false };
    fetchJourney(this.state.slug, payload)
      .then(cardRes => {
        this.setState({ journey: cardRes }, () => {
          let autoComplete = true;
          if (this.newComplete) {
            switch (this.completeMethodConf) {
              case 'creatorChoose':
                autoComplete =
                  this.state.journey.autoComplete !== undefined
                    ? this.state.journey.autoComplete
                    : true;
                break;
              case 'creatorNotChoose':
                autoComplete = true;
                break;
              case 'disabledNext':
                autoComplete = false;
                break;
              default:
                // FIXME: implement default case
                break;
            }
          }
          this.setState({ autoComplete });
          this.loadPackCards()
            .then(() => {
              let checkedCurrentCardId =
                this.state.cardId === 'private-card' ? undefined : this.state.cardId;
              let cardNotAvailable = true;
              let journeyDetails = Object.assign({}, this.state.journey);
              journeyDetails.journeySection = this.state.journeySmartBites;
              this.props.dispatch(saveConsumptionJourney(journeyDetails));
              loopJourneySection: for (let i = 0; i < this.state.journeySmartBites.length; i++) {
                for (let j = 0; j < this.state.journeySmartBites[i].cards.length; j++) {
                  if (this.state.journeySmartBites[i].cards[j].id == checkedCurrentCardId) {
                    cardNotAvailable = false;
                    this.chooseCard(this.state.journeySmartBites[i].cards[j], j, i);
                    break loopJourneySection;
                  }
                }
              }
              if (cardNotAvailable) {
                this.setState({
                  loadChecked: false,
                  unavailable: true
                });
              }
            })
            .catch(err => {
              console.error(
                `Error in JourneyConsumptionContainer.fetchJourneyDetails.loadPackCards.func : ${err}`
              );
            });
        });
      })
      .catch(err => {
        this.setState({
          loadChecked: false,
          unavailable: true
        });
        console.error(`Error in JourneyConsumptionContainer.fetchJourneyDetails.func: ${err}`);
      });
  };

  chooseCard = (card, i, section, e) => {
    if (e && e.target && e.target.name === 'marked-link') {
      return;
    }
    this.setState({
      currentIndex: i,
      currentSection: section,
      loadChecked: false,
      checkedCard: card
    });
  };

  loadPackCards = () => {
    return new Promise(resolve => {
      if (this.state.journey.journeySection && this.state.journey.journeySection.length) {
        let isOwner =
          this.state.journey.author &&
          this.state.journey.author.id == this.props.currentUser.get('id');
        let journeySmartBites = [];
        let countCard = 0;
        for (let i = 0; i < this.state.journey.journeySection.length; i++) {
          this.uploadJourneySectionCards(i, journeySmartBites, isOwner)
            .then(() => {
              countCard++;
              let lockedIndex = findIndex(
                this.state.journey.journeySection[i].cards,
                item => !!item.locked
              );
              if (lockedIndex > -1) {
                journeySmartBites[i].cards[lockedIndex].isLocked = true;
                journeySmartBites[i].cards = this.checkToShowLocked(
                  journeySmartBites[i].cards,
                  lockedIndex
                );
              }
              if (countCard === this.state.journey.journeySection.length) {
                this.setState(
                  {
                    journeySmartBites
                  },
                  resolve
                );
              }
            })
            .catch(err => {
              console.error(
                `Error in JourneyConsumptionContainer.loadPackCards.uploadJourneySectionCards.func : ${err}`
              );
            });
        }
      }
    });
  };

  uploadJourneySectionCards = (i, journeySmartBites, isOwner) => {
    return new Promise(resolve => {
      let data = this.state.journey.journeySection[i];
      data.cards = [];
      journeySmartBites[i] = data;
      let countChild = 0;
      let payload = {
        fields:
          'id,title,message,slug,card_type,auto_complete,completion_state,author,pack_cards(id,title,message,card_type,card_subtype,slug,state,is_official,provider,provider_image,readable_card_type,share_url,average_rating,skill_level,filestack,votes_count,comments_count,published_at,prices,is_assigned,is_bookmarked,hidden,is_public,is_paid,mark_feature_disabled_for_source,completion_state,is_upvoted,all_ratings,is_reported,card_metadatum,ecl_duration_metadata,author,is_clone,locked,auto_complete,language,channel_ids,non_curated_channel_ids,channels,completed_percentage,duration,file_resources,paid_by_user,resource,show_restrict,tags,teams,teams_permitted,users_permitted,users_with_access,quiz,ecl_metadata,rank,payment_enabled,user_rating,voters,project_id,video_stream,assigner,shared_by,channels(id,label),teams(id,label)), pack_cards'
      };
      fetchPathwayInsideJourney(data.id, payload)
        .then(pathway => {
          pathway.packCards.map(smartBite => {
            if (smartBite.resource === null) {
              delete smartBite.resource;
            }
            if (smartBite.videoStream === null) {
              delete smartBite.videoStream;
            }
            return smartBite;
          });
          if (pathway && pathway.packCards && pathway.packCards.length) {
            data.totalCount = pathway.packCards.length;
            for (let k = 0; k < pathway.packCards.length; k++) {
              this.uploadChildJourneyCards(i, k, data, isOwner, pathway.packCards[k])
                .then(() => {
                  countChild++;
                  if (countChild === pathway.packCards.length) {
                    resolve();
                  }
                })
                .catch(err => {
                  console.error(
                    `Error in JourneyConsumptionContainer.uploadJourneySectionCards.uploadChildJourneyCards.func : ${err}`
                  );
                });
            }
          } else {
            data.totalCount = 0;
            resolve();
          }
        })
        .catch(err => {
          console.error(
            `Error in JourneyConsumptionContainer.uploadJourneySectionCards.fetchPathwayInsideJourney.func : ${err}`
          );
        });
    });
  };

  checkToShowLocked = (cards, lockedCardIndex) => {
    for (let ind = 0; ind < cards.length; ind++) {
      if (ind === lockedCardIndex) {
        continue;
      }
      if (cards[ind].isLocked) {
        break;
      }
      let showLocked = !!cards[lockedCardIndex].isLocked;
      cards[ind].showLocked = ind > lockedCardIndex ? showLocked : false;
    }
    return cards;
  };

  isWeekly = i => {
    let cardDate = new Date(this.state.journey.journeySection[i].start_date);
    let nowDate = new Date();
    let nextSevenDay = new Date(cardDate);
    let dateValue = nextSevenDay.getDate() + 7;
    nextSevenDay.setDate(dateValue);
    return (
      this.state.journey.cardSubtype === 'weekly' &&
      !(nowDate >= cardDate && nowDate <= nextSevenDay)
    );
  };

  uploadChildJourneyCards = (i, j, data, isOwner, card) => {
    return new Promise(resolve => {
      if (!isOwner && this.isWeekly(i)) {
        data.cards.push({ isPrivate: true });
        resolve();
      } else {
        if (card && card.message === 'You are not authorized to access this card') {
          card.isPrivate = true;
        }
        data.cards[j] = card;
        resolve();
      }
    });
  };

  arrowClick = async direction => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    let journeyDetails =
      this.props.journey &&
      this.props.journey.get('consumptionJourney') &&
      this.props.journey.get('consumptionJourney').journeySection;
    let section = this.state.currentSection;
    let index = this.state.currentIndex;
    let card = journeyDetails[section].cards[index];
    let cardType = getCardType(card);
    let oldIndex = this.state.currentIndex;
    if (
      cardType === 'QUIZ' &&
      card.attemptedOption &&
      card.leaps &&
      card.leaps.inPathways &&
      direction === 1
    ) {
      let correctAnswer = card.quizQuestionOptions.filter(function(obj) {
        return obj.isCorrect === true;
      })[0];
      let selected = card.attemptedOption && card.attemptedOption.id;
      let result = correctAnswer && selected === correctAnswer.id;
      let inPathwayLeap = card.leaps.inPathways.find(
        item => item.pathwayId == journeyDetails[this.state.currentSection].id
      );
      let nextCardId = result ? inPathwayLeap.correctId : inPathwayLeap.wrongId;
      if (nextCardId) {
        let newIndex = journeyDetails[section].cards.indexOf(
          journeyDetails[section].cards.find(item => item.id == nextCardId)
        );
        this.changeCompleteStatus(nextCardId, newIndex, section);
        this.setState(
          {
            fetchCard: false
          },
          () => {
            this.setState(
              {
                currentIndex: newIndex,
                checkedCard: journeyDetails[section].cards[newIndex],
                fetchCard: true
              },
              () => {
                if (journeyDetails[section].cards[newIndex].id === undefined) {
                  this.props.dispatch(push(`/journey/${this.state.slug}/cards/private-card`));
                } else {
                  this.props.dispatch(
                    push(
                      `/journey/${this.state.slug}/cards/${
                        journeyDetails[section].cards[newIndex].id
                      }`
                    )
                  );
                  try {
                    if (this.state.checkedCard && this.state.checkedCard.id) {
                      recordVisit(this.state.checkedCard.id);
                    }
                  } catch (e) {}
                }
              }
            );
          }
        );
      }
    } else {
      let newIndex = this.state.currentIndex + direction;
      if (newIndex >= 0 && newIndex <= journeyDetails[section].cards.length - 1) {
        let prevCard = journeyDetails[this.state.currentSection].cards[oldIndex];
        this.changeCompleteStatus(prevCard.id, oldIndex, section);
        this.setState(
          {
            fetchCard: false
          },
          () => {
            this.setState(
              {
                currentIndex: newIndex,
                checkedCard: journeyDetails[section].cards[newIndex],
                fetchCard: true
              },
              () => {
                if (journeyDetails[section].cards[newIndex].id === undefined) {
                  this.props.dispatch(push(`/journey/${this.state.slug}/cards/private-card`));
                } else {
                  this.props.dispatch(
                    push(
                      `/journey/${this.state.slug}/cards/${
                        journeyDetails[section].cards[newIndex].id
                      }`
                    )
                  );
                  try {
                    if (this.state.checkedCard && this.state.checkedCard.id) {
                      recordVisit(this.state.checkedCard.id);
                    }
                  } catch (e) {}
                }
              }
            );
          }
        );
      } else if (section !== (0 || journeyDetails.length)) {
        let nextSection = this.state.currentSection + direction;
        let nextIndex = direction === -1 ? journeyDetails[nextSection].cards.length - 1 : 0;
        let nextCard = journeyDetails[nextSection].cards[nextIndex];
        this.changeCompleteStatus(nextCard.id, nextIndex, nextSection);
        this.setState(
          {
            fetchCard: false
          },
          () => {
            this.setState(
              {
                currentIndex: nextIndex,
                currentSection: nextSection,
                checkedCard: journeyDetails[nextSection].cards[nextIndex],
                fetchCard: true
              },
              () => {
                if (journeyDetails[nextSection].cards[nextIndex].id === undefined) {
                  this.props.dispatch(push(`/journey/${this.state.slug}/cards/private-card`));
                } else {
                  this.props.dispatch(
                    push(
                      `/journey/${this.state.slug}/cards/${
                        journeyDetails[nextSection].cards[nextIndex].id
                      }`
                    )
                  );
                  try {
                    recordVisit(this.state.checkedCard.id);
                  } catch (e) {}
                }
              }
            );
          }
        );
      }
    }
  };

  setStartedReview = () => {
    if (this.state.journey.completionState === null && !this.state.journeyStarted) {
      startAssignment(this.state.journey.id)
        .then(userContentCompletion => {
          if (userContentCompletion && userContentCompletion.state === 'started') {
            this.setState({
              journeyStarted: true
            });
          }
        })
        .catch(err => {
          console.error(`Error in JourneyConsumptionContainer.setStartedReview.func : ${err}`);
        });
    }
  };

  async checkToCompletedCard() {
    return new Promise((resolve, reject) => {
      let journeyDetails =
        this.props.journey &&
        this.props.journey.get('consumptionJourney') &&
        this.props.journey.get('consumptionJourney').journeySection;
      let checkedCardId = this.state.checkedCard && this.state.checkedCard.id;

      let currentCard = {};
      loopJourneySection: for (let i = 0; i < journeyDetails.length; i++) {
        for (let j = 0; j < journeyDetails[i].cards.length; j++) {
          if (journeyDetails[i].cards[j].id == checkedCardId) {
            currentCard = journeyDetails[i].cards[j];
            break loopJourneySection;
          }
        }
      }

      let isCompleted =
        currentCard &&
        currentCard.completionState &&
        currentCard.completionState.toUpperCase() === 'COMPLETED';
      let cardType = getCardType(currentCard);
      let isLinkCard =
        !this.state.showMarkAsComplete &&
        currentCard &&
        currentCard.resource &&
        (currentCard.resource.url ||
          currentCard.resource.description ||
          currentCard.resource.fileUrl) &&
        currentCard.resource.type !== 'Video' &&
        ((currentCard.readableCardType &&
          currentCard.readableCardType.toUpperCase() === 'ARTICLE') ||
          cardType === 'ARTICLE');
      if (
        !isCompleted &&
        this.state.autoComplete &&
        currentCard &&
        (currentCard.hasAttempted || true) &&
        !currentCard.locked &&
        !currentCard.isPrivate &&
        !isLinkCard
      ) {
        try {
          this.setState({ isInProgressToComplete: true }, async () => {
            await markAsComplete(currentCard.id, { state: 'complete' })
              .then(completedCardStatus => {
                this.setState({
                  completedCardStatus: completedCardStatus,
                  isInProgressToComplete: false
                });
                resolve(true);
              })
              .catch(error => {
                this.setState({
                  completedCardStatus: null,
                  isInProgressToComplete: false
                });
                console.error(
                  `Error in JourneyConsumptionContainer.checkToCompletedCard.func : ${error}`
                );
                resolve(true);
              });
          });
        } catch (error) {
          this.setState({
            completedCardStatus: null,
            isInProgressToComplete: false
          });
          console.error(
            `Error in JourneyConsumptionContainer.checkToCompletedCard.func : ${error}`
          );
          resolve(true);
        }
      } else {
        resolve(true);
      }
    });
  }

  changeCompleteStatus = (id, i, section) => {
    let journey = this.props.journey && this.props.journey.get('consumptionJourney');
    if (
      this.state.completedCardStatus &&
      this.state.completedCardStatus.completableId == id &&
      !this.state.checkedCard.locked
    ) {
      journey.journeySection[section].cards[i].isCompleted = true;
      journey.journeySection[section].cards[i].completionState = 'COMPLETED';
    }
    this.props.dispatch(saveConsumptionJourney(journey));
  };

  render() {
    if (this.state.unavailable) {
      return (
        <div className="row container-padding">
          <Paper className="small-12">
            <div className="container-padding vertical-spacing-large text-center">
              <p>{tr('This content does not exist or has been deleted.')}</p>
            </div>
          </Paper>
        </div>
      );
    } else if (this.state.loadChecked || this.state.isInProgressToComplete) {
      return (
        <div className="text-center pathway-overview-spinner">
          <Spinner />
        </div>
      );
    }
    let journey = this.state.journey;
    let journeyDetails =
      this.props.journey &&
      this.props.journey.get('consumptionJourney') &&
      this.props.journey.get('consumptionJourney').journeySection;
    let journeySection = journeyDetails || this.state.journeySmartBites;
    let pathway = journeySection[this.state.currentSection];
    let smartBitesBeforeCurrent =
      pathway &&
      pathway.cards &&
      pathway.cards.length &&
      pathway.cards.slice(0, this.state.currentIndex);
    let isShowLockedCardContent =
      this.state.currentIndex === 0 ||
      (smartBitesBeforeCurrent &&
        smartBitesBeforeCurrent.length &&
        smartBitesBeforeCurrent.every(
          item => item.completionState && item.completionState.toUpperCase() === 'COMPLETED'
        ));

    return (
      <div className="stand-alone">
        <div className="row consumption-container-padding">
          <div className="journey-consumption-title">
            <span className="journey-consumption-title-heading pathway-consumption-ellipsis">
              {journey ? unescape(journey.title || journey.message) : ''}
            </span>
            <span className="journey-consumption-count pathway-consumption-ellipsis">
              {this.state.currentSection + 1}/{journeySection.length}
            </span>
          </div>
          <div className="journey-pathway-consumption-title">
            <span className="pathway-consumption-title-heading pathway-consumption-ellipsis">
              {pathway ? unescape(pathway.block_title || pathway.block_message) : ''}
            </span>
            <span className="pathway-consumption-count pathway-consumption-ellipsis">
              {this.state.currentIndex + 1}/{pathway && pathway.cards && pathway.cards.length}
            </span>
          </div>
          <div className="flex pathway-consumption-main-container">
            <div className="pathway-consumption-arrow left-consumption-arrow">
              {(this.state.currentIndex !== 0 || this.state.currentSection !== 0) && (
                <div style={this.styles.leftButton} onClick={this.arrowClick.bind(null, -1)}>
                  <PrevArrow style={this.styles.leftArrow} />
                </div>
              )}
            </div>
            <div className="insightV2 pathway-consumption-container">
              <BreadcrumbV2
                isJourneyConsumption={
                  this.props.journey && this.props.journey.get('consumptionJourneyHistory')
                }
              />
              {this.state.fetchCard && (
                <CardConsumptionContainer
                  card={this.state.checkedCard}
                  author={this.state.checkedCard && this.state.checkedCard.author}
                  journeyData={journey}
                  showComment={
                    Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')
                  }
                  currentSection={this.state.currentSection}
                  isShowLockedCardContent={isShowLockedCardContent}
                  isJourney={true}
                  autoComplete={this.state.autoComplete}
                />
              )}
            </div>
            <div className="pathway-consumption-arrow right-consumption-arrow">
              {(this.state.currentIndex + 1 !==
                (pathway && pathway.cards && pathway.cards.length) ||
                this.state.currentSection + 1 !== journeySection.length) && (
                <div style={this.styles.rightButton} onClick={this.arrowClick.bind(null, 1)}>
                  <NextArrow style={this.styles.rightArrow} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

JourneyConsumptionContainer.propTypes = {
  journey: PropTypes.object,
  routeParams: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    journey: state.journey ? state.journey : {},
    team: state.team,
    currentUser: state.currentUser
  };
}

export default connect(mapStoreStateToProps)(JourneyConsumptionContainer);
