import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { tr } from 'edc-web-sdk/helpers/translations';
import { renderSkillLabel, sortSkills } from './utils';
import { SKILLS_GRAPH_DATE_RANGE } from './config';
import './styles/FilterBar.scss';

export default class FilterBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filterGroupsSelectedOptions: null,
      filterSkillsSelectedOptions: null,
      filterDateSelectedOption: null,
      totalSkillsFilters: 0
    };
  }

  toggleSkillsDropdown() {
    this.refs.sfo.style.display === 'none'
      ? (this.refs.sfo.style.display = 'block')
      : (this.refs.sfo.style.display = 'none');
  }

  render() {
    const { filterDateSelectedOption, filterSkillsSelectedOptions, selectedOption } = this.state;
    const specifiedElement = this.refs.sfo;

    document.addEventListener('click', function(event) {
      if (event.target.id !== 'skills-filter-mask') {
        if (specifiedElement) {
          const isClickInside = specifiedElement.contains(event.target);

          if (!isClickInside) {
            specifiedElement.style.display = 'none';
          }
        }
      }
    });

    const groupsOptions = this.props.groupsOptions
      ? this.props.groupsOptions.map(group => ({ label: group, value: group }))
      : [];
    const skillsOptions = this.props.skillsOptions
      ? this.props.skillsOptions
          .sort(sortSkills)
          .reverse()
          .map(skill => ({ label: renderSkillLabel(skill), value: skill }))
      : [];

    return (
      <div id="sg-filters" className="container filters">
        <div className="row">
          <div className="small-4">
            {/* <Select
              value={selectedOption}
              onChange={this.handleChange}
              options={groupsOptions}
              placeholder="Groups: All"
            /> */}
          </div>
          <div className="small-4 skills-filter-container flex-end">
            <input
              type="text"
              id="skills-filter-mask"
              placeholder={`${
                this.state.totalSkillsFilters > 0
                  ? `${this.state.totalSkillsFilters} ${tr('skills')}`
                  : `${tr('Skills')}: ${tr('All')}`
              }`}
              onClick={() => this.toggleSkillsDropdown()}
            />

            <div id="skills-filter-options" ref="sfo" style={{ display: 'none' }}>
              {skillsOptions.map((skill, index) => (
                <div className="skills-filter-option">
                  <input
                    type="checkbox"
                    id={skill.value}
                    name={skill.value}
                    onClick={opt => {
                      const els = this.refs.sfo.parentNode.querySelectorAll('input');
                      this.setState(
                        {
                          filterSkillsSelectedOptions: opt.target.name,
                          totalSkillsFilters: Object.keys(els).filter(i => els[i].checked).length
                        },
                        this.props.handleSkillsChange(opt.target.name)
                      );
                    }}
                  />{' '}
                  <label htmlFor={skill.value}>{skill.label}</label>
                </div>
              ))}
            </div>
          </div>
          <div className="small-4">
            <Select
              value={filterDateSelectedOption}
              arrowRenderer={this.arrowRenderer}
              isClearable={false}
              onChange={opt => {
                this.setState(
                  {
                    filterDateSelectedOption: opt
                  },
                  this.props.handleDateChange(opt)
                );
              }}
              options={
                this.props.dateRanges.map(i => ({
                  value: i,
                  label: `${tr('Past')} ${tr(i)}`
                })) || []
              }
              placeholder={`${tr('Date')}: ${tr('Past')} ${localStorage.getItem(
                SKILLS_GRAPH_DATE_RANGE
              ) || tr('week')}`}
            />
          </div>
        </div>
      </div>
    );
  }
}

FilterBar.propTypes = {
  groupsOptions: PropTypes.array,
  skillsOptions: PropTypes.array,
  dateRanges: PropTypes.array,
  handleDateChange: PropTypes.func,
  handleSkillsChange: PropTypes.func
};
