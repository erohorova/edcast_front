import { USE_FULL_LABEL, DEFAULT_TOPIC_RADIUS } from './config';

export const renderSkillLabel = label =>
  USE_FULL_LABEL ? label : formatLabel(label.split('.').pop());

export const formatLabel = string => string.replace(/_/g, ' ');

export const sortSkills = (a, b) => {
  // ugly, but it sorts
  if (
    a
      .split('.')
      .pop()
      .toLowerCase() >
    b
      .split('.')
      .pop()
      .toLowerCase()
  )
    return -1;
  else if (
    a
      .split('.')
      .pop()
      .toLowerCase() <
    b
      .split('.')
      .pop()
      .toLowerCase()
  )
    return 1;
  return 0;
};
