import React from 'react';
import { renderSkillLabel } from './utils';
import { getMaps } from 'edc-web-sdk/requests/skillsGraph';
import { InteractiveForceGraph, ForceGraphNode, ForceGraphLink } from 'react-vis-force';
import Spinner from '../common/spinner';
import './styles/SkillsGraph.scss';
import * as config from './config';
import FilterBar from './FilterBar';

const dateRanges = ['week', 'month', 'quarter', 'year'];

export default class SkillsGraph extends React.Component {
  constructor(props) {
    super(props);
    this.ldEnabled = window.ldclient.variation('skills-graph', false);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleSkillsChange = this.handleSkillsChange.bind(this);
    this.state = {
      graphDataFetched: false,
      skillsFilter: [],
      dateRange: localStorage.getItem('skills-graph-date-range') || 'week',
      nodes: [],
      links: [],
      allUsers: {},
      allTopics: [],
      allGroups: []
    };

    this.width = 1000;
    this.height = 700;
  }
  componentWillUpdate(nextProps, nextState) {
    return this.state.nodes !== nextState.nodes;
  }
  componentDidMount() {
    this.drawGraph();
  }

  handleDateChange(dateRange) {
    localStorage.setItem('skills-graph-date-range', dateRange.value);
    this.setState({ dateRange: dateRange.value }, () => this.drawGraph());
  }
  handleSkillsChange(skillsFilter) {
    let newState = [...this.state.skillsFilter];
    if (newState.includes(skillsFilter)) {
      newState.splice(newState.indexOf(skillsFilter), 1);
    } else {
      newState.push(skillsFilter);
    }
    this.setState({ skillsFilter: newState }, () => this.drawGraph());
  }

  drawGraph() {
    // before drawing, clear nodes in state
    this.setState({ nodes: [], links: [] }, () =>
      getMaps(this.state.dateRange)
        .then(data => {
          let { users, topics, groups } = data;
          this.allUsers = Object.keys(users).map(user => users[user].name);
          this.state.allTopics = Object.keys(topics);
          this.state.allGroups = Object.keys(groups);
          this.setState({ allUsers: users });

          // FILTER TOPICS or return all
          let filteredTopics = Object.keys(topics)
            .filter((key, index) =>
              this.state.skillsFilter.length > 0 ? this.state.skillsFilter.includes(key) : true
            )
            .reduce((acc, current) => ({ ...acc, [current]: topics[current] }), {});
          // end FILTER TOPICS

          const links = Object.keys(filteredTopics).flatMap((topic, index) =>
            filteredTopics[topic]['users'].map((user, subIndex) => (
              <ForceGraphLink
                key={`${topic}-${index}-${user.id}-${subIndex}`}
                link={{
                  source: user.id,
                  target: topic,
                  strokeWidth: config.GRAPH_LINK_WIDTH,
                  value: 1
                }}
              />
            ))
          );

          const topicsNodes = Object.keys(topics).map((key, index) => (
            <ForceGraphNode
              key={`${key}-${index}`}
              node={{
                id: key,
                label: renderSkillLabel(key),
                radius: Math.max(
                  topics[key]['users'].length * config.NODE_SIZE_FACTOR,
                  config.DEFAULT_NODE_RADIUS
                ),
                group: ''
              }}
              fill={config.COLORS['topic']}
            />
          ));
          const filteredTopicsNodes = Object.keys(filteredTopics).map((key, index) => (
            <ForceGraphNode
              key={`${key}-${index}`}
              node={{
                id: key,
                label: renderSkillLabel(key),
                radius: Math.max(
                  topics[key]['users'].length * config.NODE_SIZE_FACTOR,
                  config.DEFAULT_NODE_RADIUS
                ),
                group: ''
              }}
              fill={config.COLORS['topic']}
            />
          ));
          const filteredUsersNodes = Object.keys(filteredTopics)
            .reduce((acc, cur) => [...acc, ...filteredTopics[cur]['users']], [])
            .map(user => user.id)
            .filter((v, i, a) => a.indexOf(v) === i)
            .map((userId, index) => (
              <ForceGraphNode
                key={`${userId}-${index}`}
                node={{
                  id: userId,
                  label: users[userId].name,
                  radius: config.DEFAULT_NODE_RADIUS,
                  group: ''
                }}
                fill={config.COLORS['user']}
              />
            ));

          const nodes = [...filteredTopicsNodes, ...filteredUsersNodes];

          this.setState({
            graphDataFetched: true,
            nodes,
            links
          });
        })
        .catch(err => console.error(`Error in SkillsGraph.componentDidMount(): ${err}`))
    );
  }

  render() {
    const rowOverride = { maxWidth: '96%' };
    return this.ldEnabled || process.env.NODE_ENV === 'development' ? (
      <div id="skills-graph-wrapper" className="container">
        <div className="skills-graph-header-wrapper">
          <div className="skills-graph-header row" style={rowOverride}>
            <div className="small-4">
              <h4>Skills Graph</h4>
            </div>
            <div className="small-8">
              {this.state.graphDataFetched && (
                <FilterBar
                  skillsOptions={this.state.allTopics}
                  usersOptions={this.allUsers}
                  groupsOptions={this.state.allGroups}
                  dateRanges={dateRanges}
                  handleDateChange={this.handleDateChange}
                  handleSkillsChange={this.handleSkillsChange}
                />
              )}
            </div>
          </div>
        </div>

        {this.state.graphDataFetched ? (
          <div>
            {this.state.allUsers ? (
              <div className="skills-graph-content mt-2 row" style={rowOverride}>
                <div className="small-2 form-container">
                  <div>
                    {this.allUsers && (
                      <div className="left-pane-container">
                        <span>Users</span>
                        <div className="left-pane-list">
                          {Object.keys(this.state.allUsers).map((user, index) => (
                            <div key={index}>
                              <a href={`/${this.state.allUsers[user]['handle']}`}>
                                {this.state.allUsers[user]['name']}
                              </a>
                            </div>
                          ))}
                        </div>
                      </div>
                    )}
                    {this.state.allTopics && (
                      <div className="left-pane-container mt-2">
                        <span>Skills</span>
                        <div className="left-pane-list">
                          {this.state.skillsFilter.length > 0
                            ? this.state.skillsFilter.map((skill, index) => (
                                <div key={index}>{renderSkillLabel(skill)}</div>
                              ))
                            : this.state.allTopics.map((skill, index) => (
                                <div key={index}>{renderSkillLabel(skill)}</div>
                              ))}
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="small-10">
                  <InteractiveForceGraph
                    showLabels={config.SHOW_LABELS}
                    onSelectNode={node => {}}
                    labelAttr="label"
                    highlightDependencies
                    zoom
                    simulationOptions={{
                      alpha: config.ALPHA,
                      height: this.height,
                      width: this.width,
                      animate: config.ANIMATE_GRAPH,
                      radiusMargin: config.NODE_RADIUS_MARGIN,
                      strength: {
                        collide: config.COLLIDE_VALUE,
                        charge: config.CHARGE_VALUE
                      }
                    }}
                  >
                    {this.state.nodes && this.state.nodes}
                    {this.state.links && this.state.links}
                  </InteractiveForceGraph>
                </div>
              </div>
            ) : (
              <div className="text-center" style={{ padding: '1em' }}>
                <h4>Oops!</h4>
                There was a problem retrieving graph data. Try again later.
                <br />
                <br />
                <br />
                <a href="/">Return to the home page</a>
              </div>
            )}
          </div>
        ) : (
          // TODO: get i18n string
          <div className="small-12 text-center">
            Loading graph data
            <br />
            <Spinner />
          </div>
        )}
      </div>
    ) : (
      <p className="text-center">This feature is currently disabled</p>
    );
  }
}
