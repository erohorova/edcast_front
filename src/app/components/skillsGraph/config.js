export const DEFAULT_NODE_RADIUS = 10;
export const USE_FULL_LABEL = false;
export const GRAPH_LINK_WIDTH = 3.5;
export const NODE_RADIUS_MARGIN = 20;
export const ANIMATE_GRAPH = true; // false for easier dev
export const SHOW_LABELS = false;
export const NODE_SIZE_FACTOR = 8;
export const ALPHA = 0.1;
export const CHARGE_VALUE = -100;
export const COLLIDE_VALUE = 0.7;
export const COLORS = {
  topic: '#454560',
  user: '#9476c9',
  gridLink: '#ACADC1'
};
export const SKILLS_GRAPH_DATE_RANGE = 'skills-graph-date-range';
