import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import LinearProgress from 'material-ui/LinearProgress';
import { PointerTop } from 'edc-web-sdk/components/icons';
import { getClcRecords } from 'edc-web-sdk/requests/clc';
import { getOrgDetails } from 'edc-web-sdk/requests/orgSettings.v2';
import { getUserClc } from 'edc-web-sdk/requests/users';
import checkLabelCustomLang from '../../utils/checkLabelCustomLang';

class LeftRailClcProgress extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      orgId: '',
      ClcTarget: '',
      ClcBadges: '',
      ClcHours: '',
      ClcExists: false,
      ClcPercentage: 0,
      badge25: false,
      badge50: false,
      badge75: false,
      badge100: false
    };
    this.styles = {
      clcProgressBar: {
        backgroundColor: '#f0f0f5',
        height: '6px',
        borderRadius: '5px'
      },
      badgeBar: {
        display: 'inline-block',
        textAlign: 'center',
        color: '#53536c',
        fontsize: '10px'
      },
      badgeAvatar: {
        margin: '10px 7px 0px'
      }
    };
  }

  componentDidMount() {
    getOrgDetails()
      .then(data => {
        this.setState({ orgId: data.id });
        let payload = {
          clc: {
            entity_id: this.props.team.orgId,
            entity_type: 'Organization'
          }
        };
        getClcRecords(payload)
          .then(recordsData => {
            this.setState({
              ClcTarget: data.targetScore,
              ClcBadges: recordsData.targetSteps,
              ClcExists: true
            });
            recordsData.targetSteps.forEach(item => {
              this.setState({ [`badge${item}`]: true });
            });
          })
          .catch(err => {
            console.error(`Error in LeftRailClcProgress.getClcRecords.func : ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in LeftRailClcProgress.getOrgDetails.func : ${err}`);
      });

    getUserClc()
      .then(data => {
        let ClcTargetMins = data.targetScore * 60;
        let clcProgressScore = (data.score / ClcTargetMins) * 100;
        this.setState({ ClcPercentage: clcProgressScore });
        this.setState({ ClcHours: data.score });
      })
      .catch(err => {
        console.error(`Error in LeftRailClcProgress.getUserClc.func : ${err}`);
      });
  }

  render() {
    let Badge25 = 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge25.png';
    let Badge50 = 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge50.png';
    let Badge75 = 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge75.png';
    let Badge100 = 'https://d1iwkfmdo6oqxx.cloudfront.net/badges/Badge100.png';
    let clcConfig =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.sections &&
      this.props.team.OrgConfig.sections['web/sections/continuousLearning'];
    let clcLabel = checkLabelCustomLang(this.props.team.config, this.props.currentUser, clcConfig);
    return (
      <div>
        {this.state.ClcExists && (
          <Paper>
            <div className="user-score container-padding vertical-spacing-medium">
              <hr />
              <div className="clcContainer">
                <p>{clcLabel}</p>

                <p>{tr(`Progress`)}</p>
                <LinearProgress
                  style={this.styles.clcProgressBar}
                  color={'#00a1e1'}
                  min={0}
                  max={100}
                  value={this.state.ClcPercentage}
                  mode="determinate"
                />
                <div id="calibrate_points">
                  <div className="tl">0</div>
                  <div className="tl">{this.state.ClcTarget * 0.25 + 'hr'}</div>
                  <div className="tc">{this.state.ClcTarget * 0.5 + 'hr'}</div>
                  <div className="tr">{this.state.ClcTarget * 0.75 + 'hr'}</div>
                  <div className="tr">{this.state.ClcTarget + 'hr'}</div>
                </div>
                <div
                  id="indicator"
                  style={{
                    marginLeft:
                      (this.state.ClcPercentage >= 100 ? 100 : this.state.ClcPercentage) + '%'
                  }}
                >
                  <PointerTop color="#acadc1" />
                  <div>
                    <span>
                      {this.state.ClcPercentage >= 100
                        ? 100
                        : parseInt(this.state.ClcPercentage, 10)}
                      %
                    </span>
                    <span>({this.state.ClcHours + 'hrs'})</span>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

LeftRailClcProgress.propTypes = {
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(LeftRailClcProgress);
