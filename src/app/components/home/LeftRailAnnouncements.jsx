import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Spinner from '../common/spinner';
import { getAnnouncements } from 'edc-web-sdk/requests/announcements';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Carousel from '../common/Carousel';
import convertTextToLink from '../../utils/convertTextToLink';

class LeftRailAnnouncements extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      items: [],
      pending: true,
      announcements: []
    };
    this.styles = {
      listItem: {
        padding: '8px 16px 8px 46px'
      }
    };
  }

  componentDidMount() {
    let payload = {
      pronouncement: {
        is_active: true,
        scope_type: 'Organization',
        scope_id: this.props.team.orgId
      }
    };
    getAnnouncements(payload)
      .then(data => {
        if (data.pronouncements.length > 0) {
          this.setState({
            announcements: data.pronouncements,
            pending: false
          });
        }
      })
      .catch(err => {
        console.error(`Error in LeftRailAnnouncements.getAnnouncements.func : ${err}`);
      });
  }

  activityClickHandler(link) {
    this.props.dispatch(push('/' + link));
  }

  render() {
    let label = this.props.label;
    let height = this.props.height;
    let allowVisible = this.props.autoHeightContent;

    if (this.state.announcements.length == 0) {
      return (
        <Paper>
          <div className="container-padding vertical-spacing-large left-rail-announcement">
            <div className="left-rail-title">
              <span title={label}>{label}</span>
            </div>
            <div>{tr('Nothing in your queue yet. Your Announcements will appear here.')}</div>
          </div>
        </Paper>
      );
    }
    return (
      <Paper>
        <div className="container-padding vertical-spacing-large left-rail-announcement">
          <div className="left-rail-title">
            <span title={label}>{label}</span>
          </div>
          {this.state.announcements.length == 1 ? (
            <div
              className="left-rail-item"
              style={{ height: height + 'px', overflowY: allowVisible ? 'visible' : 'auto' }}
            >
              <p
                className="snippet-text"
                dangerouslySetInnerHTML={{
                  __html: convertTextToLink(this.state.announcements[0].label)
                }}
              />
            </div>
          ) : (
            <Carousel
              slidesToShow={1}
              key="announcements"
              autoplay={this.state.announcements.length > 1 ? true : false}
              autoplaySpeed={6000}
              infinite={true}
            >
              {this.state.announcements.map(announcement => {
                return (
                  <div
                    className="left-rail-item"
                    key={announcement.id}
                    style={{ height: height + 'px', overflowY: 'auto' }}
                  >
                    <p
                      className="snippet-text"
                      dangerouslySetInnerHTML={{ __html: convertTextToLink(announcement.label) }}
                    />
                  </div>
                );
              })}
            </Carousel>
          )}
        </div>
        {this.state.pending && (
          <div className="text-center" style={{ paddingBottom: '2rem' }}>
            <Spinner />
          </div>
        )}
      </Paper>
    );
  }
}

LeftRailAnnouncements.propTypes = {
  team: PropTypes.object,
  label: PropTypes.string,
  height: PropTypes.string,
  autoHeightContent: PropTypes.bool
};

export default connect(state => ({
  team: state.team.toJS()
}))(LeftRailAnnouncements);
