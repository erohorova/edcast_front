import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Announcements from './LeftRailAnnouncements';
import LearningQueuev2 from './LeftRailLearningQueuev2';
import UserPanelv2 from './UserPanelv2';
import MyLearningPlan from './MyLearningPlan';
import LeftRailBookmarks from './LeftRailBookmarks';
import checkLabelCustomLang from '../../utils/checkLabelCustomLang';

class LeftRailv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showBookmarksBlock: window.ldclient.variation('show-bookmarks-on-left-pannel', false)
    };
  }

  render() {
    let announcements = this.props.leftRailControl['web/leftRail/announcements'];
    let myLearningQueue = this.props.leftRailControl['web/leftRail/myLearningQueue'];
    let announcementLabel = checkLabelCustomLang(
      this.props.team.config,
      this.props.currentUser,
      announcements
    );
    let learningLabel = checkLabelCustomLang(
      this.props.team.config,
      this.props.currentUser,
      myLearningQueue
    );
    return (
      <div id="left-rail" className="rail-container vertical-spacing-large">
        {announcements && announcements.visible && (
          <Announcements
            ref="announcements"
            height={announcements.height || 100}
            label={announcementLabel}
          />
        )}

        <UserPanelv2 ref="userPanel" leftRailControl={this.props.team.OrgConfig.leftRail} />

        <div id="learningqueue-bookmarks" className={this.props.sticky ? 'stick-to-top' : ''}>
          <MyLearningPlan />
          {!this.state.showBookmarksBlock && <LearningQueuev2 label={learningLabel} />}
          {this.state.showBookmarksBlock && <LeftRailBookmarks isGenpactUI label={learningLabel} />}
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

LeftRailv2.propTypes = {
  leftRailControl: PropTypes.any,
  team: PropTypes.any,
  sticky: PropTypes.any
};

export default connect(mapStoreStateToProps)(LeftRailv2);
