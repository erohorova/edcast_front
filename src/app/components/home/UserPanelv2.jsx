import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';
import Paper from 'edc-web-sdk/components/Paper';
import { Avatar } from 'material-ui';
import { getTopicScore } from 'edc-web-sdk/requests/profile.v2';

class UserPanelv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userScore: 0
    };
    this.styles = {
      userName: {
        fontSize: '16px',
        color: '#454560',
        padding: '5px 0px'
      },
      userScore: {
        textAlign: 'center',
        fontSize: '14px',
        color: '#26273b',
        padding: '5px 0px'
      },
      seperator: {
        border: '0.3px solid #d6d6e1'
      }
    };
  }

  componentDidMount() {
    getTopicScore()
      .then(data => {
        this.setState({
          userScore: data.smartbitesScore
        });
      })
      .catch(() => {
        console.error('Error in fetching userscore');
      });
  }

  render() {
    return (
      <div id="user-details-v2">
        <Paper
          id="user-details"
          style={{
            borderRadius: '0px',
            width: '300px',
            padding: '15px',
            marginBottom: '15px'
          }}
        >
          <div>
            <div className="user-details-container text-center">
              <div className="image-container">
                <Avatar
                  className="pointer"
                  src={this.props.currentUser.avatar}
                  size={100}
                  alt=" "
                  onClick={() => {
                    this.props.dispatch(push('/me'));
                  }}
                />
              </div>
              <div>
                <a
                  style={this.styles.userName}
                  onClick={() => {
                    this.props.dispatch(push('/me'));
                  }}
                >
                  {this.props.currentUser.name}
                </a>
              </div>
            </div>

            <div style={this.styles.seperator} />

            <div style={this.styles.userScore}>
              Score : {this.state.userScore.toLocaleString({ maximumFractionDigits: 0 })}
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

UserPanelv2.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(UserPanelv2);
