import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Spinner from '../common/spinner';
import Paper from 'edc-web-sdk/components/Paper';
import List from 'material-ui/List';
import ListItem from 'material-ui/List/ListItem';
import TooltipLabel from '../common/TooltipLabel';
import IconButton from 'material-ui/IconButton';
import {
  Comment,
  Stream,
  Like,
  SmartBite,
  Pathway,
  Completed
} from 'edc-web-sdk/components/icons/index';
import Journey from 'edc-web-sdk/components/icons/Journey';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import MarkdownRenderer from '../common/MarkdownRenderer';
import { getStream } from '../../actions/teamActivityAction';
import checkInsertedLinks from '../../utils/checkSnippetInsertedLinks';
import throttle from 'lodash/throttle';
import { saveConsumptionPathwayHistoryURL } from '../../actions/pathwaysActions';
import { saveConsumptionJourneyHistoryURL } from '../../actions/journeyActions';

import { ACTIVITYSMARTCARD } from '../../constants/regexConstants';

class RightRailActivityStream extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      items: [],
      limit: 25,
      userpanel: window.ldclient.variation('left-user-panel', false),
      offset: 0,
      pending: null,
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };
    this.styles = {
      listItem: {
        padding: '8px 16px 8px 46px'
      }
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.getStream = this.getStream.bind(this);
  }

  componentDidMount() {
    this.getStream();
    document.getElementById('right-rail-activity').addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document.getElementById('right-rail-activity').removeEventListener('scroll', this.handleScroll);
  }

  activityClickHandler(link) {
    let backUrl = window.location.pathname;
    if (
      link.indexOf('pathways/') !== -1 &&
      this.state.isCardV3 &&
      this.state.pathwayConsumptionV2
    ) {
      this.props.dispatch(saveConsumptionPathwayHistoryURL(backUrl));
    } else if (
      link.indexOf('journey/') !== -1 &&
      this.state.isCardV3 &&
      this.state.journeyConsumptionV2
    ) {
      this.props.dispatch(saveConsumptionJourneyHistoryURL(backUrl));
    }
    this.props.dispatch(push('/' + link));
  }

  getStream() {
    this.setState({ pending: true }, function() {
      let offset = this.state.offset;
      if (document.querySelector('#right-rail-activity .wide-item-list')) {
        offset = document.querySelector('#right-rail-activity .wide-item-list').childNodes.length;
      }
      this.props
        .dispatch(getStream(this.state.limit, false, '', offset))
        .then(() => {
          this.updateStreamUI(this.props.teamActivity.activityStreams);
        })
        .catch(err => {
          console.error(`Error in RightRailActivityStream.getStream.func : ${err}`);
        });
    });
  }
  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      let obj = this._rightRailActivity;
      if (obj.scrollTop === obj.scrollHeight - obj.offsetHeight) {
        this.getStream();
      }
    },
    150,
    { leading: false }
  );

  mentionClickHandler(value) {
    this.props.dispatch(push(`/${value}`));
  }

  updateStreamUI(data) {
    let items = this.state.items;
    let offset = this.state.offset;
    let limit = this.state.limit;
    let newOffset = offset + limit;
    if (document.querySelector('#right-rail-activity .wide-item-list')) {
      newOffset = document.querySelector('#right-rail-activity .wide-item-list').childNodes.length;
    }
    let updatedItems = [];
    if (data) {
      updatedItems = [...new Set([...items, ...data])];
    }
    this.setState(
      {
        items: updatedItems,
        pending: false,
        offset: newOffset
      },
      function() {
        document.getElementById('right-rail-activity').scrollTop -= 500;
      }
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.teamActivity.streamFlag) {
      this.updateStreamUI(nextProps.teamActivity.activityStreams);
    }
  }

  render() {
    return (
      <Paper
        id="right-rail-activity"
        ref={node => {
          this._rightRailActivity = node;
        }}
      >
        <div className="container-padding vertical-spacing-large">
          <div className="left-rail-title">
            <div
              style={
                this.state.userpanel
                  ? { marginBottom: '15px', fontSize: '16px', fontWeight: 600, color: '#454560' }
                  : {}
              }
              className={this.state.items.length === 0 ? 'inactive' : ''}
            >
              {this.props.label && this.props.label.length > 22 ? (
                <TooltipLabel text={tr(this.props.label)}>
                  {tr(this.props.label.slice(0, 22) + '...')}
                </TooltipLabel>
              ) : (
                tr(this.props.label)
              )}
            </div>
          </div>

          {!this.state.pending && this.state.items.length === 0 && (
            <div className="data-not-available-msg">
              {tr('Nothing in your activity queue yet. Your team activity will appear here.')}
            </div>
          )}

          {this.state.pending !== null && this.state.items.length > 0 && (
            <List className="wide-item-list">
              {this.state.items.map(item => {
                item.snippet.length > 100
                  ? (item.snippet = item.snippet.slice(0, 100) + '...')
                  : undefined;
                let icon;
                let linkUrl;
                let message = `**${item.user.name}**`;
                switch (item.action) {
                  case 'created_livestream':
                    icon = <Stream className="left-rail-activity-icon" />;
                    message += ` ${tr('created')} ${item.snippet}`;
                    break;
                  case 'upvote':
                    icon = <Like />;
                    message += ` ${tr('liked')} ${item.snippet}`;
                    break;
                  case 'comment':
                    icon = <Comment className="left-rail-activity-icon" />;
                    message += ` ${tr('commented on')} ${item.snippet}`;
                    break;
                  case 'created_card':
                    icon = <SmartBite className="left-rail-activity-icon" />;
                    message += ` ${tr('created SmartCard')}: ${item.snippet.replace(
                      ACTIVITYSMARTCARD,
                      ''
                    )}`;
                    break;
                  case 'created_pathway':
                    icon = <Pathway className="left-rail-activity-icon" />;
                    message += ` ${tr('created Pathway')}: ${item.snippet}`;
                    break;
                  case 'created_journey':
                    icon = <Journey className="left-rail-activity-icon" />;
                    message += ` ${tr('created Journey')}: ${item.snippet}`;
                    break;
                  case 'assignment_completed':
                    icon = <Completed className="left-rail-activity-icon" />;
                    message += ` ${tr('completed Assignment')}: ${item.snippet}`;
                    break;
                  case 'smartbite_completed':
                    icon = <Completed className="left-rail-activity-icon" />;
                    message += ` ${tr('completed SmartCard')}: ${item.snippet}`;
                    break;
                  default:
                    icon = null;
                    message += '';
                    break;
                }
                switch (item.linkable.type) {
                  case 'journey':
                    linkUrl = `journey/${item.linkable.id}`;
                    break;
                  case 'collection':
                    linkUrl = `pathways/${item.linkable.id}`;
                    break;
                  case 'card':
                    linkUrl = 'insights/' + item.linkable.id;
                    break;
                  case 'video_stream':
                    linkUrl = 'video_streams/' + item.streamableId;
                    break;
                  default:
                    linkUrl = '#';
                    break;
                }
                return (
                  <div className="left-rail-item" key={item.id}>
                    <ListItem
                      leftIcon={
                        <IconButton style={{ padding: 0, marginTop: '7px' }}>{icon}</IconButton>
                      }
                      innerDivStyle={this.styles.listItem}
                      primaryText={
                        <div className="snippet-text">
                          <MarkdownRenderer
                            onTouchTap={this.activityClickHandler.bind(this, linkUrl)}
                            onUserNameClick={this.mentionClickHandler.bind(
                              this,
                              item.user['handle']
                            )}
                            markdown={message}
                          />
                        </div>
                      }
                    />
                  </div>
                );
              })}
            </List>
          )}
        </div>
        {this.state.pending && (
          <div className="text-center" style={{ paddingBottom: '2rem' }}>
            <Spinner />
          </div>
        )}
      </Paper>
    );
  }
}

RightRailActivityStream.propTypes = {
  teamActivity: PropTypes.object,
  label: PropTypes.string
};

export default connect(state => ({
  teamActivity: state.teamActivity.toJS()
}))(RightRailActivityStream);
