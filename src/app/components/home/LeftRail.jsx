import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import UserScore from './LeftRailUserScore';
import LearningQueue from './LeftRailLearningQueue';
import ActivityStream from './LeftRailActivityStream';
import ActivityTeam from './ActivityTeam';
import Announcements from './LeftRailAnnouncements';
import { langs } from '../../constants/languages';
import { tr } from 'edc-web-sdk/helpers/translations';

class LeftRail extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      newFeedStyle: window.ldclient.variation('feed-style-v2', false)
    };
    this.styles = {
      listItem: {
        paddingLeft: '46px'
      }
    };
    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
    this.count = 0;
    let labels = {};
    let announcementLabel = '';
    let announcementHeight = '250';
    let isHideAnnouncements = false;
    Object.keys(this.props.leftRailControl).map(key => {
      let leftRail = this.props.leftRailControl[key];
      let translatedLabel =
        this.isShowCustomLabels &&
        leftRail.languages &&
        leftRail.languages[this.profileLanguage] &&
        leftRail.languages[this.profileLanguage].trim();
      labels[key] =
        translatedLabel ||
        tr(this.props.leftRailControl[key].label || this.props.leftRailControl[key].defaultLabel);

      if (key == 'web/leftRail/announcements') {
        announcementLabel = translatedLabel || tr(this.props.leftRailControl[key].label || '');
        announcementHeight = this.props.leftRailControl[key].height || '250';
        isHideAnnouncements =
          this.props.isUserPanelExist &&
          this.props.leftRailControl[key].align &&
          this.props.leftRailControl[key].visible;
      }
    });

    if (this.state.newFeedStyle) {
      this.leftRailStreams = {
        'web/leftRail/teamActivity': (
          <ActivityTeam
            label={labels['web/leftRail/teamActivity']}
            orgId={this.props.orgId}
            currentUser={this.props.currentUser}
          />
        )
      };
    } else {
      this.leftRailStreams = {
        'web/leftRail/myLearningQueue': (
          <LearningQueue label={labels['web/leftRail/myLearningQueue']} />
        ),
        'web/leftRail/teamActivity': this.props.newTeamActivity ? (
          <ActivityTeam
            label={labels['web/leftRail/teamActivity']}
            orgId={this.props.orgId}
            currentUser={this.props.currentUser}
          />
        ) : (
          <ActivityStream label={labels['web/leftRail/teamActivity']} />
        )
      };
      if (!isHideAnnouncements) {
        this.leftRailStreams['web/leftRail/announcements'] = (
          <Announcements height={announcementHeight} label={announcementLabel} />
        );
      }
    }
    this.leftRailController = this.listTab(this.props.leftRailControl);
  }

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        if (listObj.defaultLabel === 'Team Activity') {
          listObj.teamFeed = true;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => a.index - b.index);
    }
    return tabs.filter(tab => {
      if (this.leftRailStreams[tab.key] && tab.visible) {
        this.count++;
      }
      return tab.visible != false;
    });
  }

  render() {
    if (!!this.leftRailController.length) {
      let sectionCountClass = 'large-section';
      return (
        <div id="left-rail" className="rail-container vertical-spacing-large">
          {this.leftRailController.map((controller, i) => {
            if (this.leftRailStreams[controller.key]) {
              return (
                <section
                  key={i}
                  className={`${
                    controller.key !== 'web/leftRail/announcements'
                      ? 'team-feed'
                      : sectionCountClass
                  }  components-count_${this.count ? this.count : '3'}`}
                >
                  {this.leftRailStreams[controller.key]}
                </section>
              );
            }
          })}
        </div>
      );
    } else {
      return false;
    }
  }
}

LeftRail.propTypes = {
  leftRailControl: PropTypes.object,
  newTeamActivity: PropTypes.bool,
  isUserPanelExist: PropTypes.bool,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  orgId: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LeftRail);
