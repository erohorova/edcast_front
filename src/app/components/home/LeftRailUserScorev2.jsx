import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import colors from 'edc-web-sdk/components/colors/index';
import { getTopicScore } from 'edc-web-sdk/requests/profile.v2';
import Paper from 'edc-web-sdk/components/Paper';
import LinearProgress from 'material-ui/LinearProgress';
import { tr } from 'edc-web-sdk/helpers/translations';
import checkLabelCustomLang from '../../utils/checkLabelCustomLang';

class LeftRailUserScorev2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      totalScore: 0,
      topics: [],
      percentile: 0,
      showPopover: false,
      totalTopicScore: 0,
      totalTopicAverage: 0,
      myScoreTaxonomyTopics: window.ldclient.variation('my-score-taxonomy-topics', false)
    };
    this.topicColors = [
      {
        color: colors.primary600,
        highlight: colors.primary
      },
      {
        color: colors.primary400,
        highlight: colors.primary300
      },
      {
        color: colors.primary200,
        highlight: colors.primary100
      }
    ];

    this.showPopover = this.showPopover.bind(this);
    this.hidePopover = this.hidePopover.bind(this);
  }

  componentDidMount() {
    getTopicScore()
      .then(data => {
        let totalTopicScore = 0;
        let totalTopicAverage = 0;
        let topics = this.state.myScoreTaxonomyTopics ? data.taxonomyTopics : data.topics;
        topics.map(function(topic) {
          totalTopicScore += topic.score;
        });

        totalTopicAverage = totalTopicScore / topics.length;

        this.setState({
          pending: false,
          totalScore: data.smartbitesScore,
          topics: topics,
          percentile: data.percentile,
          infoEle: ReactDOM.findDOMNode(this.refs.infoIcon),
          totalTopicAverage: totalTopicAverage
        });
      })
      .catch(err => {
        console.error(`Error in LeftRailUserScorev2.getTopicScore.func : ${err}`);
      });
  }

  showPopover() {
    this.setState({ showPopover: true });
  }

  hidePopover() {
    this.setState({ showPopover: false });
  }

  render() {
    let percentile = Math.ceil(100 - this.state.percentile);
    if (percentile == 0) {
      percentile = 1;
    }
    if (percentile == 100) {
      percentile = 99;
    }
    let myScoreConfig =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.sections &&
      this.props.team.OrgConfig.sections['web/sections/myScore'];
    let myScoreLabel =
      checkLabelCustomLang(this.props.team.config, this.props.currentUser, myScoreConfig) ||
      tr('My Score');
    return (
      <Paper>
        <div className="user-score container-padding vertical-spacing-medium">
          <div>
            <div className="header-text">{myScoreLabel}</div>
            <div className="sub-text">
              {tr('Total Points')}{' '}
              {this.state.totalScore <= 0
                ? '-'
                : this.state.totalScore.toLocaleString({ maximumFractionDigits: 0 })}{' '}
              &nbsp;&nbsp;|&nbsp;&nbsp; {tr('TOP %{percentile}%', {percentile: percentile})}
            </div>
            {!this.props.isLearnersDashboard && <hr style={{ marginTop: '5px' }} />}
            {!this.props.isLearnersDashboard && this.state.topics && this.state.topics.length > 0 && (
              <div>
                <div className="score-points">
                  <table className="width-100">
                    <tbody>
                      {this.state.topics.map((topic, index) => {
                        return (
                          <tr className="margin-bottom-10" key={index}>
                            <td className="point-text">
                              {topic.name} <br /> {topic.score} Points
                            </td>
                            <td className="point-progress">
                              <LinearProgress
                                className="point-progress-bar"
                                mode="determinate"
                                min={0}
                                max={100}
                                color={this.topicColors[index].color}
                                value={(topic.score * 100) / this.state.totalTopicAverage}
                              />
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            )}
          </div>
        </div>
      </Paper>
    );
  }
}

LeftRailUserScorev2.propTypes = {
  isLearnersDashboard: PropTypes.bool,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LeftRailUserScorev2);
