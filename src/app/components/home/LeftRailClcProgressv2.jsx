import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import LinearProgress from 'material-ui/LinearProgress';
import { PointerTop } from 'edc-web-sdk/components/icons';
import { getUserClc } from 'edc-web-sdk/requests/users';
import IconButton from 'material-ui/IconButton';
import checkLabelCustomLang from '../../utils/checkLabelCustomLang';

class LeftRailClcProgressv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      orgId: '',
      ClcTarget: '',
      ClcExists: false,
      ClcPercentage: 0,
      ClcScore: 0,
      badge25: false,
      badge50: false,
      badge75: false,
      badge100: false
    };
    this.styles = {
      clcProgressBar: {
        height: '6px',
        borderRadius: '5px',
        backgroundColor: '#f0f0f5'
      },
      badgeBar: {
        display: 'inline-block',
        textAlign: 'center',
        color: '#53536c',
        fontsize: '10px',
        width: '100%'
      },
      pointerValue: {
        fontSize: '10px',
        color: '#6f708b',
        textAlign: 'center'
      },
      pointerTop: {
        height: '15px'
      },
      clcToolTip: {
        padding: '0px',
        width: 'auto',
        height: 'auto'
      }
    };
  }

  componentDidMount() {
    let payload = {
      clc: {
        entity_id: this.props.team.orgId,
        entity_type: 'Organization'
      }
    };
    getUserClc(payload)
      .then(data => {
        let score = data.score;
        let clcTarget = data.targetScore;
        let clcTargetMins = clcTarget * 60;
        let clcProgressScore = (score / clcTargetMins) * 100;
        this.setState({
          ClcTarget: clcTarget,
          ClcExists: true,
          ClcPercentage: clcProgressScore,
          ClcScore: Math.floor(score / 60) + ' hrs ' + (score % 60) + ' mns'
        });
      })
      .catch(err => {
        console.error(`Error in LeftRailClcProgressv2.getUserClc.func : ${err}`);
      });
  }

  showClcProgress = () => {
    let clcConfig =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.sections &&
      this.props.team.OrgConfig.sections['web/sections/continuousLearning'];
    let clcLabel = checkLabelCustomLang(this.props.team.config, this.props.currentUser, clcConfig);
    return (
      <div>
        {this.state.ClcExists && (
          <Paper>
            <div className="user-score container-padding vertical-spacing-medium">
              <div className="clcContainer">
                <p className="clcHeader">{clcLabel}</p>
                <p>{tr(`Progress`)}</p>
                <div style={{ padding: '0px 5px' }}>
                  <LinearProgress
                    style={this.styles.clcProgressBar}
                    color={'#00a1e1'}
                    min={0}
                    max={100}
                    value={this.state.ClcPercentage}
                    mode="determinate"
                  />
                  <div
                    id="indicator"
                    style={{
                      marginLeft:
                        (this.state.ClcPercentage >= 100 ? 100 : this.state.ClcPercentage) + '%',
                      position: 'relative',
                      left: '-17px'
                    }}
                  >
                    <div>
                      <IconButton
                        tooltip={this.state.ClcScore}
                        disableTouchRipple={true}
                        style={this.styles.clcToolTip}
                        tooltipPosition="bottom-center"
                        tooltipStyles={{ top: '15px', left: '-24px' }}
                      >
                        <div style={this.styles.pointerTop}>
                          <PointerTop color="#acadc1" />
                        </div>
                        <div>
                          <p style={this.styles.pointerValue}>
                            {this.state.ClcPercentage >= 100
                              ? 100
                              : parseInt(this.state.ClcPercentage, 10)}
                            %
                          </p>
                        </div>
                      </IconButton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        )}
      </div>
    );
  };

  render() {
    return (
      <div>
        {typeof this.props.team.OrgConfig.badgings == 'undefined'
          ? this.showClcProgress()
          : this.props.team.OrgConfig.badgings['web/badgings'] &&
            this.props.team.OrgConfig.badgings['web/badgings'].clcBadgings &&
            this.showClcProgress()}
      </div>
    );
  }
}

LeftRailClcProgressv2.propTypes = {
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(LeftRailClcProgressv2);
