import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Spinner from '../common/spinner';
import Paper from 'edc-web-sdk/components/Paper';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import BookmarkMoveToTop from 'edc-web-sdk/components/icons/BookmarkMoveToTop';

import { tr } from 'edc-web-sdk/helpers/translations';
import MarkdownRenderer from '../common/MarkdownRenderer';
import checkInsertedLinks from '../../utils/checkSnippetInsertedLinks';
import { unfeaturedClickHandler, featuredClickHandler } from '../../utils/bookmarkFunctions';
import calculateBookmarksHeight from '../../utils/calculateBookmarksHeight';

import { getLearningQueueItems } from 'edc-web-sdk/requests/feed';

class LeftRailBookmarks extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.2rem',
        left: '0.6rem',
        top: '0.4rem',
        verticalAlign: 'middle',
        width: '1.125rem',
        height: '1.125rem'
      },
      moveToTopIcon: {
        verticalAlign: 'middle',
        width: '0.875rem',
        height: '0.875rem'
      },
      viewAll: {
        fontSize: '0.75rem'
      }
    };
    this.state = {
      bookmarkStandalone: window.ldclient.variation('bookmark-standalone', false),
      bookmarkPending: true,
      bookmarked: []
    };
  }

  componentDidMount() {
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: this.state.bookmarked.length,
      'source[]': ['bookmarks'],
      'state[]': []
    };
    this.getBookmarks(payload);
  }

  getBookmarks = payload => {
    getLearningQueueItems(payload)
      .then(response => {
        let bookmarked = response.learningQueueItems;
        let isFeatured =
          bookmarked.length && response.pinnableCardId === bookmarked[0].deepLinkId
            ? bookmarked[0].deepLinkId
            : '';
        this.setState({ bookmarked, bookmarkPending: false, isFeatured }, calculateBookmarksHeight);
      })
      .catch(err => {
        console.error(`Error in LeftRailBookmarks.getBookmarks.func : ${err}`);
      });
  };

  searchEnterHandler = e => {
    if (e.keyCode === 13) {
      let query = e.target.value.trim();
      this.searchContent(query);
    }
  };

  searchContent = query => {
    this.setState({ query });
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: 0,
      'source[]': ['bookmarks'],
      'state[]': []
    };
    if (query !== '*') {
      payload['query'] = query;
    }
    this.getBookmarks(payload);
  };

  onClickFeature = bookmarked => {
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: 0,
      'source[]': ['bookmarks'],
      'state[]': []
    };
    if (this.state.query !== '*' && this.state.query !== '') {
      payload['query'] = this.state.query;
    }
    this.state.isFeatured === bookmarked.deepLinkId
      ? unfeaturedClickHandler(bookmarked, payload, this.getBookmarks)
      : featuredClickHandler(
          bookmarked,
          payload,
          this.state.isFeatured,
          this.state.bookmarked[0],
          this.getBookmarks
        );
  };

  oClickViewMore = () => {
    this.props.dispatch(
      push(this.state.bookmarkStandalone ? '/bookmarks' : '/me/content/bookmarked')
    );
  };

  render() {
    return (
      <div id="bookmarks-panel_v2">
        <Paper>
          <div className="bookmark-block">
            <div className="left-rail-title">
              <span className="bookmark-title">
                <span title={this.props.label}>{this.props.label}</span>
              </span>
            </div>
            <div className="bookmarks__container">
              <div className="my-team__search-input-block bookmarked-search">
                <SearchIcon style={this.styles.searchIcon} fill={'#acadc1'} viewBox="0 0 24 24" />
                <input
                  placeholder={tr('Search')}
                  className="search-channels-input"
                  type="text"
                  onKeyUp={this.searchEnterHandler.bind(this)}
                />
              </div>
              <div className="bookmark-items-view__container">
                {this.state.bookmarked && this.state.bookmarked.length > 0 && (
                  <div id="bookmark-items__container_v2" className="bookmark-items__container">
                    {this.state.bookmarked.slice(0, 10).map(item => {
                      let source;
                      let pushLink;
                      switch (item.deepLinkType) {
                        case 'video_stream':
                          source = 'video_streams';
                          pushLink = true;
                          break;
                        case 'collection':
                          source = 'pathways';
                          pushLink = false;
                          break;
                        default:
                          source = 'insights';
                          pushLink = true;
                          break;
                      }
                      let message = (
                        item.snippet ||
                        (item.queueable
                          ? item.queueable.message || item.queueable.title
                          : 'no title')
                      ).replace(/amp;/gi, '');
                      let displayMessage = checkInsertedLinks(message);
                      return (
                        <div className="bookmark__container">
                          <div className="bookmark__move-to-top">
                            <span
                              onClick={this.onClickFeature.bind(this, item)}
                              className="move-to-top tooltip"
                            >
                              <BookmarkMoveToTop style={this.styles.moveToTopIcon} />
                              <span className="tooltiptext">
                                {this.state.isFeatured === item.deepLinkId
                                  ? tr('Unfeature')
                                  : tr('Move to top')}
                              </span>
                            </span>
                          </div>
                          <a
                            className="bookmark__text"
                            key={item.id}
                            onTouchTap={() => {
                              pushLink
                                ? this.props.dispatch(push(`/${source}/${item.queueable.id}`))
                                : this.props.dispatch(push(`/${source}/${item.deepLinkId}`));
                            }}
                          >
                            <MarkdownRenderer
                              markdown={displayMessage}
                              markedClass="learning-queue-msg"
                            />
                          </a>
                        </div>
                      );
                    })}
                  </div>
                )}
                {!this.state.bookmarkPending && this.state.bookmarked.length > 0 && (
                  <div className="bookmark-view-all_v2">
                    <a
                      className="view-more-link"
                      style={this.styles.viewAll}
                      href={this.state.bookmarkStandalone ? '/bookmarks' : '/me/content/bookmarked'}
                      onTouchTap={this.oClickViewMore}
                    >
                      {tr('View more...')}
                    </a>
                  </div>
                )}
                {!this.state.bookmarked.length && !this.state.bookmarkPending && (
                  <div id="bookmark__no-content" className="bookmark__container">
                    <span className="bookmark__text">{tr('No bookmark found.')}</span>
                  </div>
                )}
                {this.state.bookmarkPending && (
                  <div id="bookmark__spinner" className="text-center bookmark__spinner-container">
                    <Spinner />
                  </div>
                )}
              </div>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

LeftRailBookmarks.propTypes = {
  label: PropTypes.string
};

export default connect()(LeftRailBookmarks);
