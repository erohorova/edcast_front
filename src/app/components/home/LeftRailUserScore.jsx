import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import { Doughnut } from 'react-chartjs';
import colors from 'edc-web-sdk/components/colors/index';
import Spinner from '../common/spinner';
import { getTopicScore } from 'edc-web-sdk/requests/profile.v2';
import Paper from 'edc-web-sdk/components/Paper';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';
import formatNumbers from '../../utils/formatNumbers';
import { tr } from 'edc-web-sdk/helpers/translations';
import checkLabelCustomLang from '../../utils/checkLabelCustomLang';

// icons
import InfoIcon from 'edc-web-sdk/components/icons/InfoIcon';

class LeftRailUserScore extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      pending: true,
      totalScore: 0,
      topics: [],
      percentile: 0,
      showPopover: false
    };
    this.topicColors = [
      {
        color: colors.primary600,
        highlight: colors.primary
      },
      {
        color: colors.primary400,
        highlight: colors.primary300
      },
      {
        color: colors.primary200,
        highlight: colors.primary100
      }
    ];

    this.showPopover = this.showPopover.bind(this);
    this.hidePopover = this.hidePopover.bind(this);
  }

  componentDidMount() {
    getTopicScore()
      .then(data => {
        this.setState({
          pending: false,
          totalScore: data.smartbitesScore,
          topics: data.topics,
          percentile: data.percentile,
          infoEle: ReactDOM.findDOMNode(this.refs.infoIcon)
        });
      })
      .catch(err => {
        console.error(`Error in LeftRailUserScore.getTopicScore.func : ${err}`);
      });
  }

  showPopover() {
    this.setState({ showPopover: true });
  }

  hidePopover() {
    this.setState({ showPopover: false });
  }

  render() {
    let data = this.state.topics
      .filter(topic => topic.score > 0)
      .map((topic, index) => {
        return {
          value: topic.score,
          label: topic.name,
          color: this.topicColors[index].color,
          highlight: this.topicColors[index].highlight
        };
      });
    if (!data.length) {
      data = [
        {
          value: this.state.totalScore,
          label: 'Overall',
          color: this.topicColors[0].color,
          highlight: this.topicColors[0].highlight
        }
      ];
    }
    let score = formatNumbers(this.state.totalScore) + ' pts';
    let fontSize = Math.min(26 - score.length, 20);
    let percentile = Math.ceil(100 - this.state.percentile);
    if (percentile == 0) {
      percentile = 1;
    }
    if (percentile == 100) {
      percentile = 99;
    }
    let myScoreConfig =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.sections &&
      this.props.team.OrgConfig.sections['web/sections/myScore'];
    let myScoreLabel =
      checkLabelCustomLang(this.props.team.config, this.props.currentUser, myScoreConfig) ||
      tr('My Score');
    return (
      <Paper>
        <div className="user-score container-padding vertical-spacing-medium">
          <div className={data.length === 0 ? 'inactive' : ''}>
            <strong>{myScoreLabel}</strong>
            <div className="info-icon-wrapper">
              <div
                className="false-div"
                ref="infoIcon"
                onMouseOver={this.showPopover}
                onMouseOut={this.hidePopover}
              />
              <div>
                <InfoIcon />
              </div>
            </div>
            <Popover
              open={this.state.showPopover}
              anchorEl={this.state.infoEle}
              anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
              targetOrigin={{ horizontal: 'left', vertical: 'top' }}
              useLayerForClickAway={false}
              canAutoPosition={false}
              animated={false}
              className="custom-popover"
            >
              <p className="popover-title">
                {tr(
                  'My Score is a personal tracker of your learning and influence. Every action you take on EdCast earns you points and impacts your score. You can see in real time how you are doing compared to your colleagues. Points are earned by:'
                )}
              </p>
              <ul className="score-steps">
                <li>{tr('Consuming: Every piece of content you consume earns you points.')}</li>
                <li>
                  {tr(
                    'Participating: Liking content, commenting on content, and taking a poll earns you points.'
                  )}
                </li>
                <li>
                  {tr(
                    'Publishing: Sharing your skills by publishing content which your colleagues interact with earns you points.'
                  )}
                </li>
              </ul>
            </Popover>
          </div>
          {!this.state.pending && this.state.totalScore > 0 && (
            <div className="row">
              <div className="small-6 columns score-chart-container">
                <div className="canvas-container">
                  <Doughnut
                    data={data}
                    options={{ percentageInnerCutout: 70, segmentShowStroke: false }}
                    width="300"
                    height="130"
                  />
                </div>
                <div className="score-chart-text text-center">
                  <div>
                    <strong style={{ fontSize }}>{score}</strong>
                  </div>
                  <small>
                    {tr('TOP')} {percentile}%
                  </small>
                </div>
              </div>
              <div className="small-6 columns vertical-spacing-medium">
                {data.length > 0 &&
                  data.map((item, index) => {
                    return (
                      <div key={index} className="score-chart-legend">
                        <div className="legend-icon" style={{ backgroundColor: item.color }} />
                        <div className="legend-text">
                          <div className="text-overflow">{tr(item.label)}</div>
                          <small style={{ color: colors.primary }} className="legend-score">
                            {formatNumbers(item.value)}
                          </small>
                          <small> pts</small>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          )}
          {!this.state.pending && this.state.totalScore === 0 && (
            <div className="user-score-empty-state vertical-spacing-large">
              <div>{tr('My Score helps you measure your progress')}</div>
              <div>
                <small>
                  <em>{tr('Read, like or comment on the feed card to start scoring')}</em>
                </small>
              </div>
            </div>
          )}
          {this.state.pending && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

LeftRailUserScore.propTypes = {
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(LeftRailUserScore);
