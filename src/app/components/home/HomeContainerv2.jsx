import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';

import Spinner from '../common/spinner';

import colors from 'edc-web-sdk/components/colors/index';
import {
  newContentCount,
  newTeamLearningCount,
  getNewAssignmentsCount
} from 'edc-web-sdk/requests/feed';
import { getAssignments } from 'edc-web-sdk/requests/assignments';
import { getCuratedChannels, getCurateCards } from 'edc-web-sdk/requests/curate';

import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';

import { markAssignmentCountHasBeenUpdated } from '../../actions/assignmentsActions';
import { changeFeedCardView } from '../../actions/teamActions';
import CardViewToggle from '../common/CardViewToggle';
import calculateTeemFeedPosition from '../../utils/calculateTeemFeedPosition';
import calculateLearningQBookmarksPosition from '../../utils/calculateLearningQBookmarks';
import calculateBookmarksHeight from '../../utils/calculateBookmarksHeight';
import UserPanel from './UserPanel';
import LeftRail from './LeftRail';
import LeftRailv2 from './LeftRailv2';
import RightRail from './RightRail';
import { langs } from '../../constants/languages';
import TooltipLabel from '../common/TooltipLabel';
import _ from 'lodash';
import ChatIcon from 'edc-web-sdk/components/icons/ChatIcon';

class HomeContainerv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      required: 0,
      assignmentCount: 0,
      curate: 0,
      featured: 0,
      curateTab: false,
      pending: true,
      teamLearning: 0,
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      homepageVersion: window.ldclient.variation('homepage-version', 'left-sidebar'),
      userpanel: window.ldclient.variation('left-user-panel', false),
      isPageAlignToCenter: window.ldclient.variation('is-feed-page-align-to-center', false),
      newTeamActivity: window.ldclient.variation('team-activity-v-2', false),
      bigCardView: window.ldclient.variation('big-card-view', false),
      totalCountCurate: 0,
      isMenuOpen: false,
      feedOffset: 0,
      userOffset: 0,
      footerOffset: 0,
      footerUserOffset: 0,
      showProjectCard: window.ldclient.variation('project-card', false),
      isShowRecommended: window.ldclient.variation('winterstorm', false),
      newFeedStyle: window.ldclient.variation('feed-style-v2', false),
      multilingualContent: window.ldclient.variation('multilingual-content', false),
      newUserPanelStyle: window.ldclient.variation('user-panel-v2', false),
      hideTeamFeed: localStorage.getItem('hideTeamFeed') === 'true',
      windowWidth: 1200,
      genpactUI: window.ldclient.variation('genpact-ui', false),
      makeLearningQBookmarksSticky: false,
      meetupIntegraion: window.ldclient.variation('meetup-integration', false),
      homePagev1: window.ldclient.variation('home-page-fix-v1', false),
      isCardV3: window.ldclient.variation('card-v3', false)
    };

    this.firstChild = null;
    this.style = {
      countBadge: {
        fontSize: '0.625rem',
        borderRadius: '0.1875rem',
        padding: 0,
        height: 18,
        width: 18,
        marginTop: 0,
        backgroundColor: colors.primary300,
        color: colors.white,
        top: '-0.75rem',
        right: '-1.25rem'
      },
      customBadge: {
        padding: 0
      }
    };

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
  }

  /**
   * Calculate & Update state of new dimensions
   */
  updatePosition = () => {
    let windowWidth =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    let position = calculateTeemFeedPosition();
    let learningQBookmarks = calculateLearningQBookmarksPosition();
    calculateBookmarksHeight();
    this.setState({
      makeLearningQBookmarksSticky: learningQBookmarks
    });
    position.windowWidth = windowWidth;
    this.setState(position); //position is object. I need fields of this object in state. Cause it I make setState without {}.
  };

  handleScroll = () => {
    this.updatePosition();
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.updatePosition);
    window.removeEventListener('scroll', this.handleScroll);
  }

  toggleTeamFeed = viewRegime => {
    let collapseWidth = 1100;
    if (viewRegime === 'BigCard') {
      collapseWidth = this.state.userpanel ? 1300 : 1000;
    } else {
      collapseWidth = this.state.userpanel ? 1100 : 900;
    }
    if (this.state.windowWidth <= collapseWidth) {
      this.toggleBlock(viewRegime);
    } else {
      localStorage.setItem('hideTeamFeed', `${!this.state.hideTeamFeed}`);
      this.setState(prevState => {
        return {
          hideTeamFeed: !prevState.hideTeamFeed
        };
      });
    }
  };

  componentDidMount() {
    if (this.state.newFeedStyle) {
      this.setState({
        homepageVersion: 'right-sidebar' //because in new version user can't move team feed to the left
      });
    }
    if (window.opener) {
      window.opener.postMessage(this.props.currentUser, '*');
    }

    if (localStorage.getItem('featuredLastVisit')) {
      newContentCount({
        content_types: ['featured'],
        last_access_at: localStorage.getItem('featuredLastVisit')
      })
        .then(data => {
          this.setState({ ['featured']: data['featured_count'] });
        })
        .catch(err => {
          console.error(`Error in HomeContainerv2.newContentCount.func for featured : ${err}`);
        });
    }

    if (!localStorage.getItem('teamLearning')) {
      localStorage.setItem('teamLearning', Math.round(new Date().getTime() / 1000));
    } else {
      newTeamLearningCount(localStorage.getItem('teamLearning'))
        .then(data => {
          localStorage.setItem('teamLearning', Math.round(new Date().getTime() / 1000));
          localStorage.setItem('teamLearningNewCardIds', data['items_ids']);
          this.setState({ teamLearning: data['items_count'] });
        })
        .catch(err => {
          console.error(`Error in HomeContainerv2.newTeamLearningCount.func : ${err}`);
        });
    }

    this.updateRequired();
    this.updateCurateCount();
    window.addEventListener('resize', this.updatePosition);
    window.addEventListener('scroll', this.handleScroll); // done
    // this.showGuideme();
    setTimeout(() => {
      this.updatePosition();
    }, 1);
  }

  // showGuideme() {
  //     if (readCookie("first_sign_in") === "true" && this.props.onboarding.onboarding_completed) {
  //         setCookie("first_sign_in", "");
  //         let guideMeConfig = this.props.team.OrgConfig.guideme["web/guideme/first_sign_in"];
  //         if (guideMeConfig.visible) {
  //             initiateGuideMe(guideMeConfig.userKey, guideMeConfig.tourId, guideMeConfig.tourType)
  //         }
  //     }
  // }

  updateRequired = () => {
    if (localStorage.getItem('assignmentsLastVisit')) {
      newContentCount({
        content_types: ['required'],
        last_access_at: localStorage.getItem('assignmentsLastVisit')
      })
        .then(data => {
          this.setState({ ['required']: data['required_count'] }, () => {
            this.props.dispatch(markAssignmentCountHasBeenUpdated());
          });
        })
        .catch(err => {
          console.error(`Error in HomeContainerv2.newContentCount.func for required : ${err}`);
        });
    }
  };

  updateCurateCount = () => {
    let newCurateItemsCount = 0;
    let isFeedCurateVisible =
      this.props.team &&
      this.props.team.Feed &&
      this.props.team.Feed['feed/curate'] &&
      this.props.team.Feed['feed/curate'].visible;
    if (isFeedCurateVisible) {
      getCuratedChannels()
        .then(data => {
          let curatedChannels = data;
          this.setState(
            {
              pending: false,
              curateTab: curatedChannels.length > 0
            },
            this.updatePosition
          );
        })
        .catch(err => {
          this.setState({ pending: false }, this.updatePosition);
          console.error('Error in HomeContainerv2.getCuratedChannels', err);
        });
    } else {
      this.setState(
        {
          pending: false,
          curateTab: false
        },
        this.updatePosition
      );
    }
  };

  tabActiveHandler = (e, path, tabKey) => {
    e.preventDefault();
    this.feedTabClick(tabKey);
    if (this.props.path === path) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  feedTabClick = feedType => {
    if (feedType === 'featured') {
      localStorage.setItem('featured', Math.round(new Date().getTime() / 1000));
      this.setState({ featured: 0 });
    } else if (feedType === 'team-learning') {
      localStorage.setItem('teamLearning', Math.round(new Date().getTime() / 1000));
      this.setState({ teamLearning: 0 });
    }
  };

  componentWillReceiveProps(nextProps) {
    const shouldUpdate =
      nextProps.assignments &&
      nextProps.assignments.assignmentCountShouldBeUpdated &&
      nextProps.assignments.assignmentCountShouldBeUpdated !=
        this.props.assignments.assignmentCountShouldBeUpdated;
    if (shouldUpdate) {
      this.updateRequired();
    }

    if (
      nextProps.curate &&
      nextProps.curate.totalCount &&
      nextProps.curate.totalCount !== this.state.totalCountCurate
    ) {
      this.setState({ totalCountCurate: nextProps.curate.totalCount });
      this.updateCurateCount();
    }
  }

  renderTab = (tab, index, path, translatedLabel) => {
    if (tab.only === 'mobile') {
      return;
    }
    let tabKey;
    if (
      tab.key
        .split('/')
        .pop()
        .indexOf('Z-') > -1
    ) {
      tabKey =
        '/feed/' +
        tab.label
          .split(' ')
          .join('-')
          .toLowerCase();
    } else {
      tabKey = tab.key
        .split('/')
        .pop()
        .replace(/([A-Z])/g, '-$1')
        .toLowerCase();
    }
    let notification = null;
    let isBadge =
      tabKey === 'curate' ||
      tabKey === 'my-assignments' ||
      tabKey === 'team-learning' ||
      tabKey === 'featured';
    if (isBadge) {
      const key =
        tabKey === 'my-assignments'
          ? 'assignmentCount'
          : tabKey === 'team-learning'
          ? 'teamLearning'
          : tabKey;
      notification = this.state[key] !== 0 && (
        <div>{this.state[key] < 100 ? this.state[key] : '99+'}</div>
      );
    }
    if (tabKey === 'curate' && !this.state.curateTab) {
      return;
    }
    let tabVal =
      (this.props.path === '/' || this.props.path === '/feed') && index === 0
        ? this.props.path === '/'
          ? '/'
          : '/feed'
        : tabKey;
    let tabSplit = tab.key.split('/');
    let tabClass = tabSplit[tabSplit.length - 1];
    let tabActive = path === tabVal;
    let tabLabel =
      translatedLabel || tr(tab.label || (tab.defaultLabel && tab.defaultLabel.toUpperCase()));
    let tooltipText = tab.tooltip;
    return (
      <a
        href="#"
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        className={`feed-tab-item ${tabClass} ${tabActive ? 'active-feed-tab-item' : ''} ${
          this.state.homePagev1 ? 'feed-tab-item-v1' : ''
        }`}
        key={index}
        onClick={e =>
          this.tabActiveHandler(e, tabKey.replace(/([A-Z])/g, '-$1').toLowerCase(), tabKey)
        }
      >
        {isBadge && notification ? (
          tooltipText ? (
            <TooltipLabel text={this.tooltipText(tooltipText)} html={true} position={'bottom'}>
              <Badge
                style={this.style.customBadge}
                badgeStyle={this.style.countBadge}
                badgeContent={notification}
                primary={true}
              >
                {' '}
                {translatedLabel ||
                  tr(tab.label || (tab.defaultLabel && tab.defaultLabel.toUpperCase()))}
              </Badge>
            </TooltipLabel>
          ) : (
            <Badge
              style={this.style.customBadge}
              badgeStyle={this.style.countBadge}
              badgeContent={notification}
              primary={true}
            >
              {' '}
              {translatedLabel ||
                tr(tab.label || (tab.defaultLabel && tab.defaultLabel.toUpperCase()))}
            </Badge>
          )
        ) : tooltipText ? (
          <TooltipLabel text={this.tooltipText(tooltipText)} html={true} position={'bottom'}>
            <span>{tabLabel}</span>
          </TooltipLabel>
        ) : (
          <span>{tabLabel}</span>
        )}
      </a>
    );
  };

  tooltipText = tooltipText => {
    return `<p className="tooltip-text">${tr(tooltipText)}</p>`;
  };

  changeViewMode = viewRegime => {
    if (
      this.props.children &&
      this.props.children.props &&
      this.props.children.props.route &&
      this.props.children.props.route.feedKey
    ) {
      this.props.dispatch(changeFeedCardView(viewRegime, this.props.children.props.route.feedKey));
    }
    setTimeout(() => {
      this.updatePosition();
    }, 1);
  };

  listTab = obj => {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => a.index - b.index);
    }
    return tabs.filter(tab => tab.visible != false);
  };

  toggleBlock = viewRegime => {
    let collapseWidth = 1100;
    if (viewRegime === 'BigCard') {
      collapseWidth = this.state.userpanel ? 1300 : 1000;
    } else {
      collapseWidth = this.state.userpanel ? 1100 : 900;
    }
    if (this.state.windowWidth <= collapseWidth || !this.state.newFeedStyle) {
      this.setState(prevState => ({
        isMenuOpen: !prevState.isMenuOpen
      }));
    }
    this.toggleTeamFeed(viewRegime);
  };

  render() {
    let leftRailObj =
      this.props &&
      this.props.team &&
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.leftRail;

    let isLeftRailVisible =
      !!(
        (leftRailObj['web/leftRail/announcements'] &&
          leftRailObj['web/leftRail/announcements'].visible &&
          !(this.state.userpanel && leftRailObj['web/leftRail/announcements'].align)) ||
        (leftRailObj['web/leftRail/myLearningQueue'] &&
          leftRailObj['web/leftRail/myLearningQueue'].visible) ||
        (leftRailObj['web/leftRail/teamActivity'] &&
          leftRailObj['web/leftRail/teamActivity'].visible)
      ) && !this.state.genpactUI;

    let isNewTeamActivityVisible =
      leftRailObj['web/leftRail/teamActivity'] &&
      leftRailObj['web/leftRail/teamActivity'].visible &&
      this.state.newFeedStyle &&
      !this.state.genpactUI;
    // Create array at key length
    let tabs = this.listTab(this.props.team.Feed);
    if (this.state.pending) {
      return null;
    }
    let viewRegime = 'Tile';
    let path = this.props.path;
    let feedKey = this.props.path;
    if (this.props.children && this.props.children.props && this.props.children.props.route) {
      if (this.props.children.props.route.path) {
        let currentPath = this.props.children.props.route.path.split('(feed/)');
        path = currentPath[currentPath.length - 1];
      }
      if (this.props.children.props.route.feedKey) {
        feedKey = this.props.children.props.route.feedKey;
        viewRegime =
          this.props.team.Feed &&
          this.props.team.Feed[feedKey] &&
          this.props.team.Feed[feedKey].hasOwnProperty('defaultCardView')
            ? this.props.team.Feed[feedKey].defaultCardView
            : 'Tile';
        if (viewRegime === 'BigCard' && !this.state.bigCardView) {
          viewRegime = 'Tile';
        }
      }
    }
    let isRail = !!this.listTab(this.props.team.OrgConfig.leftRail).length;
    let isCurate = feedKey === 'feed/curate';
    let isSubmission = feedKey === 'feed/submission';
    let cardLayoutClassName = 'feed-without-rail home-feed';
    let tabLayout = 'feed-without-rail home-feed';
    if (this.state.userpanel) {
      cardLayoutClassName = 'feed-with-userpanel home-feed-userpanel';
      tabLayout = 'feed-header-with-userpanel home-feed';
      if (isRail && isLeftRailVisible) {
        cardLayoutClassName += ' feed-with-rail';
        if (this.state.homepageVersion === 'left-sidebar') {
          tabLayout += ' feed-header-with-left-rail';
        }
        if (this.state.homepageVersion === 'right-sidebar') {
          tabLayout += ' feed-header-with-right-rail';
        }
      } else {
        tabLayout += ' feed-header-without-rail';
      }
    } else {
      if (isRail && isLeftRailVisible) {
        cardLayoutClassName = 'feed-with-rail home-feed';
        if (this.state.homepageVersion === 'left-sidebar') {
          tabLayout = 'feed-header-with-left-rail home-feed';
        }
        if (this.state.homepageVersion === 'right-sidebar') {
          tabLayout = 'feed-header-with-right-rail home-feed';
        }
      }
    }

    return (
      <div
        id="home"
        className={`home-new-ui ${this.state.homePagev1 ? 'home-fix-v1' : ''} ${viewRegime ===
          'BigCard' &&
          (this.state.isPageAlignToCenter
            ? 'feed-container_big-cards'
            : 'feed-container_big-cards_page-no-center')}
            ${isRail && isLeftRailVisible ? 'home-new-ui_' + this.state.homepageVersion : ''} ${
          this.state.userpanel ? 'home-new-ui_with-user-pane' : ''
        }
            ${
              isRail && isLeftRailVisible && this.state.newFeedStyle
                ? 'feed-page_new-style'
                : 'feed-page_old-style'
            } ${this.state.userpanel && this.state.newUserPanelStyle ? 'user-block_new-style' : ''}
            ${this.state.hideTeamFeed && this.state.newFeedStyle ? 'hide-team-feed' : ''}
            ${this.state.isCardV3 ? 'home-new-ui_with-small-space' : ''}
            `}
      >
        <div className={`feed-container-tabs ${tabLayout}`}>
          {!this.state.pending && (
            <div className="home-content">
              <div className="sub-header-tabs">
                <div
                  className={`home-tabs ${this.state.feedCardsStyle ? 'with-toggle-view' : ''}  ${
                    this.state.homePagev1 ? 'home-tabs-v1' : ''
                  }`}
                >
                  {tabs.map((tab, i) => {
                    if (
                      (tab.key.indexOf('submission') > 0 && !this.state.showProjectCard) ||
                      (tab.key.indexOf('recommended') > 0 && !this.state.isShowRecommended)
                    ) {
                      return;
                    } else {
                      let translatedLabel =
                        this.isShowCustomLabels &&
                        tab.languages &&
                        tab.languages[this.profileLanguage] &&
                        tab.languages[this.profileLanguage].trim();
                      return this.renderTab(tab, i, path, translatedLabel);
                    }
                  })}
                </div>
                {this.state.feedCardsStyle && (
                  <div className={`feed-toggle  ${this.state.homePagev1 ? 'fix-v1' : ''}`}>
                    <CardViewToggle
                      curateTab={isCurate || isSubmission}
                      bigCardView={this.state.bigCardView}
                      viewRegime={viewRegime}
                      changeViewMode={this.changeViewMode}
                    />
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
        <div
          className={`home-redesign ${
            isRail && isLeftRailVisible
              ? this.state.homepageVersion === 'right-sidebar'
                ? 'feed-with-right-rail'
                : 'feed-with-left-rail'
              : 'feed-without-rail-side'
          } ${this.state.userpanel ? 'home-redesign_with-user-pane' : ''}`}
        >
          <div>
            {this.state.userpanel && !this.state.genpactUI && (
              <div
                id="user-panel-container"
                className={`
                  ${
                    this.state.meetupIntegraion
                      ? 'user-panel-container left-user-panel left-rail-panel left-user-panel-meetup'
                      : 'user-panel-container left-user-panel left-rail-panel'
                  }
                `}
              >
                {' '}
                {/*///////DEMO FOR MEETUP//////////*/}
                <UserPanel
                  isCardV3={this.state.isCardV3}
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                />
              </div>
            )}
            {this.state.genpactUI && (
              <div
                id="user-panel-container"
                className="genpact-ui user-panel-container left-user-panel left-rail-panel"
              >
                <LeftRailv2
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                  sticky={this.state.makeLearningQBookmarksSticky}
                />
              </div>
            )}
            {this.state.homepageVersion === 'left-sidebar' && isLeftRailVisible && (
              <div
                id="left-rail-container"
                className={`left-rail ${this.state.isMenuOpen ? 'active-menu-container' : ''}`}
              >
                {this.state.hideTeamFeed && (
                  <div className={`left-rail__mobile-btn rail__mobile-btn`}>
                    <IconButton
                      tabIndex={0}
                      onClick={() => this.toggleBlock(viewRegime)}
                      className={`right-rail__mobile-btn rail__mobile-btn`}
                    >
                      <ChatIcon focusable="false" />
                    </IconButton>
                  </div>
                )}
                <LeftRail
                  isUserPanelExist={this.state.userpanel}
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                  newTeamActivity={this.state.newTeamActivity}
                  currentUser={this.props.currentUser}
                  orgId={this.props.team.orgId}
                />
              </div>
            )}
          </div>

          <div className={`feed-container ${cardLayoutClassName}`}>
            {!this.state.pending && <div className="home-content">{this.props.children}</div>}
          </div>
          {/* team activity stream - chat flyout toggle */}

          {this.state.hideTeamFeed &&
            this.state.homepageVersion === 'right-sidebar' &&
            (!this.state.newFeedStyle || isNewTeamActivityVisible) && (
              <button onClick={() => this.toggleBlock(viewRegime)} id="chat-button">
                <img src="/i/images/chat-icon.svg" alt="Chat" />
              </button>
            )}
          {this.state.homepageVersion === 'right-sidebar' &&
            ((!this.state.newFeedStyle && isLeftRailVisible) || isNewTeamActivityVisible) && (
              <div
                id="right-rail-container"
                className={`left-rail right-rail ${
                  this.state.isMenuOpen ? 'active-menu-container' : ''
                }`}
              >
                <RightRail
                  hideTeamFeed={
                    this.state.hideTeamFeed &&
                    (!this.state.newFeedStyle || isNewTeamActivityVisible)
                  }
                  newFeedStyle={this.state.newFeedStyle}
                  toggleTeamFeed={() => this.toggleTeamFeed(viewRegime)}
                  isUserPanelExist={this.state.userpanel}
                  leftRailControl={this.props.team.OrgConfig.leftRail}
                  newTeamActivity={this.state.newTeamActivity}
                  currentUser={this.props.currentUser}
                  orgId={this.props.team.orgId}
                />
              </div>
            )}
        </div>
      </div>
    );
  }
}

HomeContainerv2.propTypes = {
  currentUser: PropTypes.object,
  assignments: PropTypes.object,
  curate: PropTypes.object,
  team: PropTypes.object,
  children: PropTypes.any,
  path: PropTypes.string,
  onboarding: PropTypes.object
};

export default connect(state => ({
  path: state.routing.locationBeforeTransitions.pathname,
  currentUser: state.currentUser.toJS(),
  modal: state.modal.toJS(),
  team: state.team.toJS(),
  onboarding: state.onboarding.toJS(),
  assignments: state.assignments.toJS(),
  curate: state.curate.toJS()
}))(HomeContainerv2);
