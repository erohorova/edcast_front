import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import UserScore from './LeftRailUserScore';
import LearningQueue from './LeftRailLearningQueue';
import ActivityStream from './RightRailActivityStream';
import ActivityTeam from './ActivityTeam';
import Announcements from './LeftRailAnnouncements';
import { langs } from '../../constants/languages';
import { tr } from 'edc-web-sdk/helpers/translations';

class RightRail extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      hideTeamFeed: props.hideTeamFeed
    };
    this.styles = {
      listItem: {
        paddingLeft: '46px'
      }
    };
    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;

    this.count = 0;
    let labels = {};
    let announcementLabel = '';
    let announcementHeight = '250';
    let isHideAnnouncements = false;
    let onlyConversation =
      this.props.leftRailControl &&
      this.props.leftRailControl['web/leftRail/teamActivity'] &&
      this.props.leftRailControl['web/leftRail/teamActivity'].onlyConversation;
    Object.keys(this.props.leftRailControl).map(key => {
      let leftRail = this.props.leftRailControl[key];
      let translatedLabel =
        this.isShowCustomLabels &&
        leftRail.languages &&
        leftRail.languages[this.profileLanguage] &&
        leftRail.languages[this.profileLanguage].trim();
      labels[key] =
        translatedLabel ||
        tr(this.props.leftRailControl[key].label || this.props.leftRailControl[key].defaultLabel);

      if (key == 'web/leftRail/announcements') {
        announcementLabel = translatedLabel || tr(this.props.leftRailControl[key].label || '');
        announcementHeight = this.props.leftRailControl[key].height || announcementHeight;
        isHideAnnouncements =
          this.props.isUserPanelExist &&
          this.props.leftRailControl[key].align &&
          this.props.leftRailControl[key].visible;
      }
    });

    if (this.props.newFeedStyle) {
      this.leftRailStreams = {
        'web/leftRail/teamActivity': (
          <ActivityTeam
            label={labels['web/leftRail/teamActivity']}
            orgId={this.props.orgId}
            currentUser={this.currentUser}
            currentGroup={this.props.currentGroup}
            onlyConversation={!!onlyConversation}
            toggleTeamFeed={this.props.toggleTeamFeed}
            newFeedStyle={this.props.newFeedStyle}
          />
        )
      };
    } else {
      if (this.props.newTeamActivity) {
        if (this.props.justTeamFeed) {
          this.leftRailStreams = {
            'web/leftRail/teamActivity': (
              <ActivityTeam
                label={labels['web/leftRail/teamActivity']}
                orgId={this.props.orgId}
                currentUser={this.currentUser}
                currentGroup={this.props.currentGroup}
                onlyConversation={!!onlyConversation}
                toggleTeamFeed={this.props.toggleTeamFeed}
              />
            )
          };
        } else {
          this.leftRailStreams = {
            'web/leftRail/myLearningQueue': (
              <LearningQueue label={labels['web/leftRail/myLearningQueue']} />
            ),
            'web/leftRail/teamActivity': (
              <ActivityTeam
                label={labels['web/leftRail/teamActivity']}
                orgId={this.props.orgId}
                selectedGroup={this.props.selectedGroup}
                currentUser={this.props.currentUser}
                onlyConversation={!!onlyConversation}
                toggleTeamFeed={this.props.toggleTeamFeed}
              />
            )
          };
          if (!isHideAnnouncements) {
            this.leftRailStreams['web/leftRail/announcements'] = (
              <Announcements height={announcementHeight} label={announcementLabel} />
            );
          }
        }
      } else {
        this.leftRailStreams = {
          'web/leftRail/teamActivity': (
            <ActivityStream label={labels['web/leftRail/teamActivity']} />
          )
        };
        if (!isHideAnnouncements) {
          this.leftRailStreams['web/leftRail/announcements'] = (
            <Announcements height={announcementHeight} label={announcementLabel} />
          );
        }
      }
    }
    this.leftRailController = this.listTab(this.props.leftRailControl);
  }

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (listObj.index === undefined) {
          listObj['index'] = -1;
        }
        if (listObj.defaultLabel === 'Team Activity') {
          listObj.teamFeed = true;
          listObj.blockClass = 'team-feed';
        }
        if (listObj.defaultLabel === 'Announcements') {
          listObj.blockClass = 'announcements';
        }
        if (listObj.defaultLabel === 'My Learning Plan') {
          listObj.blockClass = 'my-learning-queue';
        }

        tabs.push(listObj);
      });
      tabs.sort((a, b) => a.index - b.index);
    }
    return tabs.filter(tab => {
      if (
        this.leftRailStreams[tab.key] &&
        tab.visible &&
        tab.key !== 'web/leftRail/announcements'
      ) {
        this.count++;
      }
      return tab.visible != false;
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.hideTeamFeed != this.props.hideTeamFeed) {
      this.setState({ hideTeamFeed: nextProps.hideTeamFeed });
    }
  }

  render() {
    if (!!this.leftRailController.length) {
      let fullSections = this.leftRailController.length === 3;
      return (
        <div
          id="right-rail"
          className={
            this.state.hideTeamFeed
              ? 'vertical-spacing-large hide-teamActivity'
              : 'rail-container vertical-spacing-large show-teamActivity'
          }
        >
          {this.leftRailController.map((controller, i) => {
            if (this.leftRailStreams[controller.key]) {
              let componentCountClass = `${controller.blockClass !== 'announcements' &&
                !this.props.newTeamActivity &&
                `components-count_${this.count ? this.count : '3'}`}`;
              let className =
                `${controller.blockClass ? controller.blockClass : ''} ${
                  fullSections ? 'fullSections ' : ''
                }` + componentCountClass;
              return (
                <section
                  key={i}
                  className={className}
                  style={this.props.newFeedStyle ? {} : { marginBottom: '0.625rem' }}
                >
                  {this.leftRailStreams[controller.key]}
                </section>
              );
            }
          })}
        </div>
      );
    } else {
      return false;
    }
  }
}

RightRail.propTypes = {
  leftRailControl: PropTypes.object,
  newTeamActivity: PropTypes.bool,
  justTeamFeed: PropTypes.bool,
  isUserPanelExist: PropTypes.bool,
  newFeedStyle: PropTypes.bool,
  selectedGroup: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  currentGroup: PropTypes.object,
  orgId: PropTypes.number,
  toggleTeamFeed: PropTypes.func,
  hideTeamFeed: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(RightRail);
