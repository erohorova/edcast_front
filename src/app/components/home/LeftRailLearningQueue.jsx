import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import colors from 'edc-web-sdk/components/colors/index';
import Spinner from '../common/spinner';
// import {getLearningQueue} from 'edc-web-sdk/requests/feed';
import { getLearningQueueContent } from '../../actions/learningQueueActions';
import Paper from 'edc-web-sdk/components/Paper';
import List from 'material-ui/List';
import ListItem from 'material-ui/List/ListItem';
import moment from 'moment';
import TooltipLabel from '../common/TooltipLabel';

//icons
import BookmarkAsset from 'edc-web-sdk/components/icons/Bookmark';
import LearningQueueAlert from 'edc-web-sdk/components/icons/LearningQueueAlert';
import IconButton from 'material-ui/IconButton';
import { Stream, SmartBite, Pathway } from 'edc-web-sdk/components/icons/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import MarkdownRenderer from '../common/MarkdownRenderer';
import checkInsertedLinks from '../../utils/checkSnippetInsertedLinks';

class LeftRailLearningQueue extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      items: [],
      userpanel: window.ldclient.variation('left-user-panel', false),
      pending: true
    };
    this.styles = {
      listItem: {
        padding: '7px 16px 7px 46px'
      },
      buttonStyle: {
        padding: 0,
        margin: '5px 10px'
      }
    };
  }

  componentDidMount() {
    this.props.dispatch(getLearningQueueContent());
  }

  render() {
    return (
      <Paper>
        <div className="container-padding vertical-spacing-large">
          <div className="left-rail-title">
            <div
              style={
                this.state.userpanel
                  ? { marginBottom: '15px', fontSize: '16px', fontWeight: 600, color: '#454560' }
                  : {}
              }
              className={this.props.content.items.length === 0 ? 'inactive' : ''}
            >
              <span title={this.props.label}>{this.props.label}</span>
            </div>
          </div>
          {!this.props.content.pending && this.props.content.items.length === 0 && (
            <div style={{ fontSize: '12px' }}>
              {tr('Nothing in your queue yet. Your Bookmarks and Learning will appear here.')}
            </div>
          )}
          <List className="wide-item-list">
            {this.props.content.items.map(item => {
              let source;
              let icon;
              let pushLink;
              switch (item.deepLinkType) {
                case 'video_stream':
                  source = 'video_streams';
                  icon = <Stream className="learning-queue-icon" />;
                  pushLink = true;
                  break;
                case 'collection':
                  source = 'pathways';
                  icon = <Pathway className="learning-queue-icon" />;
                  pushLink = false;
                  break;
                default:
                  source = 'insights';
                  icon = <SmartBite className="learning-queue-icon" />;
                  pushLink = true;
                  break;
              }
              switch (item.source) {
                case 'bookmarks':
                  icon = <BookmarkAsset className="learning-queue-icon" />;
                  break;
                case 'assignments':
                  let color;
                  if (
                    item.queueable &&
                    item.queueable.assignment &&
                    item.queueable.assignment.due_at
                  ) {
                    let today = new Date().getTime();
                    let dueAt = new Date(item.queueable.assignment.due_at);
                    let week = 604800000;
                    let dueWithinWeek = dueAt - today < week;
                    if (dueWithinWeek) {
                      color = colors.secondary;
                      dueAt = moment(dueAt);
                      item.assignmentTooltip = 'Due on ' + dueAt.format('MMM Do YYYY') + '.';
                    }
                    dueAt = moment(dueAt);
                  }
                  icon = <LearningQueueAlert color={color} className="learning-queue-icon" />;
                  break;
                default:
                  icon = null;
                  break;
              }
              let message = (
                item.snippet ||
                (item.queueable ? item.queueable.message || item.queueable.title : 'no title')
              ).replace(/amp;/gi, '');
              let displayMessage = checkInsertedLinks(message);
              return (
                <a
                  href={pushLink ? undefined : `/${source}/${item.deepLinkId}`}
                  key={item.id}
                  onTouchTap={() => {
                    pushLink && this.props.dispatch(push(`/${source}/${item.queueable.id}`));
                  }}
                >
                  <TooltipLabel text={message.trim()}>
                    <ListItem
                      innerDivStyle={this.styles.listItem}
                      leftIcon={<IconButton style={this.styles.buttonStyle}>{icon}</IconButton>}
                      primaryText={
                        <MarkdownRenderer
                          markdown={displayMessage}
                          markedClass="learning-queue-msg"
                        />
                      }
                    />
                  </TooltipLabel>
                </a>
              );
            })}
          </List>
        </div>
        {this.props.content.pending && (
          <div className="text-center" style={{ paddingBottom: '2rem' }}>
            <Spinner />
          </div>
        )}
      </Paper>
    );
  }
}

LeftRailLearningQueue.propTypes = {
  content: PropTypes.object,
  label: PropTypes.string
};

export default connect(state => ({
  content: state.learningQueue.toJS()
}))(LeftRailLearningQueue);
