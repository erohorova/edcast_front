import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';
import Paper from 'edc-web-sdk/components/Paper';
import { fetchQuarterlyPlan } from '../../actions/mylearningplanV5Actions';
import unescape from 'lodash/unescape';
import RichTextReadOnly from '../common/RichTextReadOnly';
import convertRichText from '../../utils/convertRichText';

class MyLearningPlan extends Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;

    let announcementsSetting =
      this.props.team &&
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.leftRail &&
      this.props.team.OrgConfig.leftRail['web/leftRail/announcements'];

    this.styles = {
      assignmentStyles: {
        marginBottom: '10px'
      }
    };
    this.state = {
      assignmentData: []
    };
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }
    return card_type;
  }

  componentDidMount() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let mlpPeriod = ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'untimed'];
    let card_type = this.getAssignmentContentType();

    mlpPeriod.map(period => {
      let payload = {
        limit: 3,
        offset: 0,
        mlp_period: period,
        'state[]': ['assigned', 'started'],
        sort: 'created_at',
        order: 'desc'
      };

      if (card_type.length > 0) {
        payload['card_type[]'] = card_type;
      }
      this.props.dispatch(fetchQuarterlyPlan(payload, 'current', false));
    });
  }

  navigateTo = (e, cardType, slug) => {
    e.preventDefault();
    let url =
      (cardType === 'pack' ? '/pathways/' : cardType === 'journey' ? '/journey/' : '/insights/') +
      slug;
    window.location = url;
  };

  viewMoreLearningPlan = e => {
    e.preventDefault();
    this.props.dispatch(push('/me/learning'));
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.mlpv5.current.quarter) {
      let q1Data =
        (nextProps.mlpv5.current.quarter['quarter-1'] &&
          nextProps.mlpv5.current.quarter['quarter-1'].assignments) ||
        [];
      let q2Data =
        (nextProps.mlpv5.current.quarter['quarter-2'] &&
          nextProps.mlpv5.current.quarter['quarter-2'].assignments) ||
        [];
      let q3Data =
        (nextProps.mlpv5.current.quarter['quarter-3'] &&
          nextProps.mlpv5.current.quarter['quarter-3'].assignments) ||
        [];
      let q4Data =
        (nextProps.mlpv5.current.quarter['quarter-4'] &&
          nextProps.mlpv5.current.quarter['quarter-4'].assignments) ||
        [];
      let untimedData =
        (nextProps.mlpv5.current.quarter['untimed'] &&
          nextProps.mlpv5.current.quarter['untimed'].assignments) ||
        [];
      let assignmentData = [...q1Data, ...q2Data, ...q3Data, ...q4Data, ...untimedData];
      this.setState({
        assignmentData
      });
    }
  }

  render() {
    return (
      <div id="user-panel" className="user-panel_v2" style={{ marginBottom: '15px' }}>
        <Paper
          style={{
            borderRadius: '0px',
            width: '300px',
            padding: '1px 15px 24px'
          }}
        >
          <div className="your-learning-plan">
            <div className="your-learning-plan-header" style={{ marginBottom: '15px' }}>
              {tr('My Assigned learning')}
            </div>

            {this.state.assignmentData.length > 0 ? (
              <div>
                {this.state.assignmentData.map((card, index) => {
                  return (
                    <div
                      className="your-learning-plan-skill-stats"
                      style={this.styles.assignmentStyles}
                      key={card.id}
                    >
                      <a
                        href="#"
                        className="your-learning-plan-skill-name"
                        onClick={e => {
                          this.navigateTo(e, card.assignable.cardType, card.assignable.slug);
                        }}
                      >
                        <RichTextReadOnly
                          text={convertRichText(card.assignable.title || card.assignable.message)}
                        />
                      </a>
                    </div>
                  );
                })}
                <a
                  href="#"
                  className="view-more-link pull-right"
                  onClick={e => {
                    this.viewMoreLearningPlan(e);
                  }}
                >
                  {tr('View more...')}
                </a>
              </div>
            ) : (
              <div
                className="data-not-available-msg"
                style={{ fontSize: '14px', color: 'rgb(69, 69, 96)' }}
              >
                {tr('Your learning plan is empty, assigned content will show here.')}
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

MyLearningPlan.propTypes = {
  team: PropTypes.object,
  mlpv5: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    assignments: state.assignments.toJS(),
    mlpv5: state.mlpv5.toJS()
  };
}

export default connect(mapStoreStateToProps)(MyLearningPlan);
