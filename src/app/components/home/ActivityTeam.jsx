import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import TimeAgo from 'react-timeago';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import uniqBy from 'lodash/uniqBy';
import ReactDOM from 'react-dom';
import Popover from 'react-simple-popover';
import Linkify from 'react-linkify';

import {
  getActivityStream,
  removeActivityComment,
  removeActivityStream
} from 'edc-web-sdk/requests/feed';
import { getList } from 'edc-web-sdk/requests/groups.v2';
import {
  addCommentToActivityStream,
  addVotes,
  dislikeVotes,
  deleteCommentFromOrg,
  deleteCommentFromTeam
} from 'edc-web-sdk/requests/activityStreams';
import { getMentionSuggest as getMentionSuggests } from 'edc-web-sdk/requests/users';
import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import DeleteV2 from 'edc-web-sdk/components/icons/DeleteV2';
import Like_outline from 'edc-web-sdk/components/icons/Like_outline';
import Comment_v2 from 'edc-web-sdk/components/icons/Comment_v2';
import LikeIconSelected from 'edc-web-sdk/components/icons/LikeSelected';

import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Divider from 'material-ui/Divider';
import List from 'material-ui/List';
import ListItem from 'material-ui/List/ListItem';
import CloseIcon from 'material-ui/svg-icons/content/clear';

import MarkdownRenderer from '../common/MarkdownRenderer';
import BlurImage from '../common/BlurImage';
import UserProfileHover from '../common/UserProfileHover';
import Spinner from '../common/spinner';

import translateTA from '../../utils/translateTimeAgo';
import extractMentions from '../../utils/extractMentions';
import abbreviateNumber from '../../utils/abbreviateNumbers';
import { Permissions } from '../../utils/checkPermissions';

import { getMentionSuggest } from '../../actions/usersActions';
import { recieveActivityFeed, activityGetTeams } from '../../actions/teamActivityAction';
import { openStandaloneOverviewModal } from '../../actions/modalStandAloneActions';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { confirmation } from '../../actions/modalActions';
import { saveConsumptionPathwayHistoryURL } from '../../actions/pathwaysActions';
import { saveConsumptionJourneyHistoryURL } from '../../actions/journeyActions';

import { ACTIVITYSMARTCARD } from '../../constants/regexConstants';

class ActivityTeam extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      items: [],
      offset: 0,
      pending: null,
      pendingGroup: null,
      pendingSmall: null,
      showComment: '',
      showSubComments: '',
      groups: (this.props.teamActivity && this.props.teamActivity.groups) || [],
      isLastGroup: false,
      open: false,
      openLast: false,
      mentionQuery: '',
      onlyConversation: props.onlyConversation,
      newFeedStyle: this.props.newFeedStyle, //should update only on feed pages
      showFilterBlock: false,
      userTagging: window.ldclient.variation('user-tagging', true),
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false),
      mentionUserIds: {}
    };
    this.styles = {
      selectContainer: {
        width: '100%',
        zIndex: '10',
        maxHeight: '112px',
        overflow: 'auto',
        boxShadow: '0 1px 5px 0 rgba(0, 0, 0, 0.3)',
        background: 'white'
      },
      selectContent: {
        width: '100%',
        marginTop: '-5px',
        padding: 0
      },
      container: {
        fontFamily: 'Open Sans',
        maxHeight: '678px',
        overflow: 'hidden'
      },
      listBlock: {
        maxHeight: '520px',
        overflowY: 'auto',
        overflowX: 'hidden',
        height: '100%',
        marginBottom: 0
      },
      mainStyle: {
        border: '1px solid #6f6f8b',
        padding: 0,
        fontSize: '10px',
        lineHeight: '1.8',
        color: '#6f708b',
        height: '21px',
        width: '100%',
        marginTop: '3px',
        maxWidth: '140px',
        verticalAlign: 'top',
        borderRadius: '2px'
      },
      mainStyleSend: {
        border: '1px solid #6f6f8b',
        padding: 0,
        fontSize: '0.625rem',
        lineHeight: '1.8',
        color: '#6f708b',
        height: '1.25rem',
        width: '100%',
        marginTop: '0.1875rem',
        maxWidth: '5.375rem',
        verticalAlign: 'top',
        borderRadius: '2px'
      },
      hint: {
        padding: '0 1.125rem 0 0.3125rem',
        fontSize: '0.625rem',
        color: '#acadc1',
        bottom: 0,
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        width: '100%'
      },
      iconStyleSendTo: {
        top: '-0.125rem',
        right: '6.5625rem',
        padding: 0,
        width: '1.5rem',
        height: '1.5rem',
        fill: '#6f708b'
      },
      iconStyle: {
        top: '-2px',
        right: '5px',
        padding: 0,
        width: '24px',
        height: '24px',
        fill: '#6f708b'
      },
      selectedSendTo: {
        lineHeight: '1.25rem',
        top: 0,
        overflow: 'hidden',
        padding: '0 1.125rem 0 0.3125rem',
        textOverflow: 'ellipsis',
        color: '#6f708b',
        width: '5.375rem',
        height: '100%'
      },
      selected: {
        lineHeight: '20px',
        top: 0,
        overflow: 'hidden',
        padding: '0 18px 0 5px',
        textOverflow: 'ellipsis',
        color: '#6f708b'
      },
      menuItems: {
        width: '175px'
      },
      menuStyle: {
        width: '12rem'
      },
      avatar: {
        width: '30px',
        height: '30px',
        margin: '12px 0',
        left: 0
      },
      avatarComment: {
        margin: '12px -3px',
        width: '30px',
        height: '30px'
      },
      commentText: {
        wordWrap: 'break-word'
      },
      listItem: {
        cursor: 'default',
        margin: '0 1rem',
        padding: '13px 10px 10px 35px'
      },
      listItemComment: {
        cursor: 'default',
        padding: '13px 10px 10px 37px'
      },
      dividerStyle: {
        marginTop: 0
      },
      avatarBox: {
        userSelect: 'none',
        position: 'absolute',
        width: '1.875rem',
        height: '1.875rem',
        margin: '0.75rem 0',
        left: 0,
        display: 'inline-flex',
        zIndex: 10,
        cursor: 'pointer'
      },
      avatarBoxComment: {
        userSelect: 'none',
        height: '2.5rem',
        width: '2.5rem',
        position: 'absolute',
        top: '0.5rem',
        left: '1rem'
      },
      userName: {
        cursor: 'pointer'
      },
      avatarBoxNew: {
        userSelect: 'none',
        position: 'absolute',
        width: '2.625rem',
        height: '2.625rem',
        margin: '0.75rem 0 0 1rem',
        left: 0,
        display: 'inline-flex',
        zIndex: 10,
        cursor: 'pointer'
      },
      avatarBoxNewSubComment: {
        userSelect: 'none',
        position: 'absolute',
        width: '1.875rem',
        height: '1.875rem',
        margin: '0.75rem 0',
        left: 0,
        display: 'inline-flex',
        zIndex: 10,
        cursor: 'pointer'
      },
      listItemNew: {
        cursor: 'default',
        margin: '0 1rem',
        padding: '0.6875rem 1rem 0.625rem 4.6875rem',
        borderBottom: 'solid 1px #d6d6e1'
      },
      checkboxOuter: {
        width: '0.9rem',
        height: '0.9rem',
        marginRight: '8px'
      },
      checkboxOuterLabel: {
        padding: '6px 0',
        lineHeight: '1.4'
      },
      uncheckedIcon: {
        width: '10px',
        height: '10px'
      },
      hideOutline: {
        outline: 'none',
        display: 'inline'
      },
      deleteIcon: {
        width: '1.6875rem',
        height: '1.6875rem'
      }
    };
    this.throttledScrollHandler = throttle(this.handleScroll.bind(this), 300, { leading: false });
    this.allowVote = Permissions.has('LIKE_CONTENT');

    this.getActivity = this.getActivity.bind(this);
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
    this.isAllActivityLoaded = false;
    this.activityStreamsLimit = 20;
  }

  componentDidMount() {
    if (this.props.currentGroup) {
      this.setState({
        defaultGroupLabel: this.props.currentGroup.name,
        selectedGroup: this.props.currentGroup.id
      });
      this.getActivity(this.props.currentGroup.id);
    } else {
      let selectedActivityGroupId = +localStorage.getItem('selectedActivityGroupId');
      selectedActivityGroupId &&
        this.setState({
          selectedGroup: selectedActivityGroupId
        });
      this.getActivity(selectedActivityGroupId);
    }
    !this.state.groups.length && this.props.dispatch(activityGetTeams(20, 0));

    let obj = ReactDOM.findDOMNode(this._listBlock);
    if (obj) obj.addEventListener('scroll', this.throttledScrollHandler);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.teamActivity.activityFlag) {
      let id = this.getCurrentId();
      this.setState({ offset: 0 });
      this.getActivity(id, true);
      this.props.dispatch(recieveActivityFeed(false));
    }
    if (nextProps.teamActivity.groups) {
      let groups = nextProps.teamActivity.groups;
      let pendingGroup = false;
      let isLastGroup = nextProps.teamActivity.isLastGroup;
      this.setState({ groups, pendingGroup, isLastGroup });
    }
  }

  componentWillUnmount() {
    let obj = ReactDOM.findDOMNode(this._listBlock);
    if (obj) obj.removeEventListener('scroll', this.throttledScrollHandler);
  }

  showMoreGroups = () => {
    let offset = this.state.groups.length;
    this.setState({ pendingGroup: true });
    getList(20, offset)
      .then(groups => {
        let isLastGroup = groups.length < offset + 20;
        this.setState({
          groups: uniq(this.state.groups.concat(groups), 'id'),
          pendingGroup: false,
          isLastGroup
        });
      })
      .catch(err => {
        console.error(`Error in ActivityTeam.showMoreGroups.getList.func : ${err}`);
      });
  };

  getActivity(team_id, isLoading) {
    this.setState({ [isLoading ? 'pendingSmall' : 'pending']: true }, () => {
      let offset = this.state.offset;
      let payload = {
        limit: this.activityStreamsLimit,
        offset
      };
      if (this.state.newFeedStyle) {
        payload.only_conversations = this.props.onlyConversation
          ? this.state.onlyConversation
          : false;
      }
      getActivityStream(payload, team_id)
        .then(data => {
          let els = data && data.activityStreams ? data.activityStreams : [];
          if (els && (team_id ? els.length < this.activityStreamsLimit : !els.length))
            this.isAllActivityLoaded = true;
          this.setState({
            items: offset === 0 ? els : uniqBy(this.state.items.concat(els), 'id'),
            [isLoading ? 'pendingSmall' : 'pending']: false,
            offset: this.state.offset + els.length
          });
        })
        .catch(err => {
          console.error(`Error in ActivityTeam.getActivity.getActivityStream.func : ${err}`);
        });
    });
  }

  getCurrentId = () => {
    let id;
    if (!this.state.selectedGroup && this.state.defaultGroupLabel) {
      id = this.props.currentGroup.id;
    } else if (!this.state.selectedGroup || this.state.selectedGroup === '0') {
      id = undefined;
    } else {
      id = this.state.selectedGroup;
    }
    return id;
  };

  handleScroll() {
    if (this.isAllActivityLoaded) return;
    if (!this.state.pending && !this.state.pendingSmall) {
      let obj = ReactDOM.findDOMNode(this._listBlock);
      if (obj.scrollTop >= obj.scrollHeight - obj.offsetHeight) {
        let id = this.getCurrentId();
        this.getActivity(id, true);
      }
    }
  }

  onChangeClick = (id, index, e) => {
    if (e.keyCode === 13) {
      this.sendSubComment(id, index);
    }
  };

  async sendSubComment(id, index) {
    let msg = this[`input-${id}`].value.trim();
    if (msg && msg.length) {
      let modifiedText = await this.commentPreprocessing(msg);

      this.setState({ sendingComment: true }, () => {
        let payload = { comment: { message: modifiedText } };
        addCommentToActivityStream(id, 'activity_streams', payload)
          .then(data => {
            if (data && data.embargoErr) {
              this.setState({
                inputValue: undefined,
                sendingComment: false,
                showComment: ''
              });
              this.props.dispatch(
                openSnackBar(
                  'Sorry, the content you are trying to post is from unapproved website or words.',
                  true
                )
              );
            } else {
              this[`input-${id}`].value = '';
              let items = this.state.items;
              if (!items[index]) {
                items[index] = { comments: [], commentsCount: 0 };
              }
              items[index].comments.push(data);
              items[index].commentsCount = items[index].commentsCount + 1;
              this.setState({
                inputValue: undefined,
                sendingComment: false,
                showComment: '',
                items
              });
            }
          })
          .catch(err => {
            console.error(`Error in ActivityTeam.addCommentToActivityStream.func : ${err}`);
          });
      });
    }
  }

  inputChangeHandler(event, id) {
    let hideComment = !id ? true : false;
    let inputValue = !id ? '' : this.state.inputValue;
    let showComment = !id ? false : this.state.showComment;
    let string = event.target.value;
    let parts = string.split(' ');
    if (parts.length && parts[parts.length - 1].charAt(0) === '@') {
      let mentionQuery = parts[parts.length - 1].slice(1).toLowerCase();

      let { userTagging } = this.state;

      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }

        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        open: !!id,
        openLast: !id,
        anchorEl: ReactDOM.findDOMNode(id ? this.refs[`input-${id}`] : this._lastCommentInput),
        mentionQuery,
        hideComment,
        inputValue,
        showComment
      });
    } else if (parts.length > 1 && parts[parts.length - 2].charAt(0) === '@') {
      let mentionQuery =
        parts[parts.length - 2].slice(1).toLowerCase() +
        ' ' +
        parts[parts.length - 1].toLowerCase();

      let { userTagging } = this.state;

      if (userTagging) {
        if (this.fetchUserTimeout) {
          clearTimeout(this.fetchUserTimeout);
        }

        this.fetchUserTimeout = setTimeout(() => {
          this.props.dispatch(getMentionSuggest(mentionQuery));
        }, 300);
      }

      this.setState({
        open: !!id,
        openLast: !id,
        anchorEl: ReactDOM.findDOMNode(id ? this.refs[`input-${id}`] : this._lastCommentInput),
        mentionQuery,
        hideComment,
        inputValue,
        showComment
      });
    } else {
      this.setState({
        open: false,
        openLast: false,
        mentionQuery: '',
        hideComment,
        inputValue,
        showComment
      });
    }
  }

  mentionClickHandler = (value, id, userId) => {
    let el = id ? this[`input-${id}`] : this._lastCommentInput;
    let inputValue = el.value.trim();
    let mentionQuery = '@' + this.state.mentionQuery;
    let n = inputValue.toLowerCase().lastIndexOf(mentionQuery.toLowerCase());
    mentionQuery = mentionQuery.replace(/[-[\]/{}()*+?.\\^$|]/g, '\\$&');
    inputValue =
      inputValue.slice(0, n) + inputValue.slice(n).replace(new RegExp(mentionQuery, 'i'), value);
    let mentionUser = this.state.mentionUserIds;
    mentionUser[value] = userId;
    this.setState({ mentionUserIds: mentionUser });
    el.value = inputValue + ' ';
    setTimeout(() => {
      el.focus();
    }, 20);
  };

  handleClose(e) {
    this.setState({ open: false, openLast: false });
  }

  onChangeClickLast = e => {
    if (e.keyCode === 13) {
      let msg = e.target.value.trim();
      this.sendComment(msg);
    }
  };

  async commentPreprocessing(msg) {
    let text = msg ? msg : this._lastCommentInput.value.trim();
    let dissectedText = extractMentions(text);
    let updatedDissectedText = dissectedText.map(async item => {
      if (item.type === 'text') {
        return item;
      } else {
        let currentUserId = this.state.mentionUserIds[item.handle];
        if (!currentUserId) {
          let results = await getMentionSuggests(item.handle);
          let currentUser = results.find(user => user.handle === item.handle);
          if (!currentUser) {
            return;
          }
          currentUserId = currentUser.id;
        }
        item.handle = `${item.handle}:${currentUserId}`;
        return item;
      }
    });
    updatedDissectedText = await Promise.all(updatedDissectedText);
    let modifiedText = '';
    updatedDissectedText.map(part => {
      modifiedText = `${modifiedText}${part.type === 'handle' && modifiedText ? ' ' : ''}${
        part[part.type]
      }`;
    });

    return modifiedText;
  }

  async sendComment(msg) {
    let modifiedText = await this.commentPreprocessing(msg);
    let payload = { comment: { message: modifiedText } };
    if (modifiedText && modifiedText.length) {
      this.setState({ sendingCommentLast: true }, () => {
        let id = this.props.orgId;
        let target = 'organizations';
        if (this.state.sendToGroup && this.state.sendToGroup !== '0') {
          id = this.state.sendToGroup;
          target = 'teams';
        } else if (!this.state.sendToGroup && this.state.defaultGroupLabel) {
          id = this.props.currentGroup.id;
          target = 'teams';
        }

        addCommentToActivityStream(id, target, payload)
          .then(data => {
            if (data && data.embargoErr) {
              this.setState({ sendingCommentLast: false });
              this.props.dispatch(
                openSnackBar(
                  'Sorry, the content you are trying to post is from unapproved website or words.',
                  true
                )
              );
            } else {
              this._lastCommentInput.value = '';
              let messageText = data.message;
              if (target === 'teams') {
                data['teamId'] = id;
              }
              data['action'] = 'comment';
              data['comments'] = [];
              data['voters'] = [];
              data['snippet'] = 'Edcast';
              data['linkable'] = {
                type: this.state.sendToGroup ? 'team' : 'organization'
              };
              data['message'] = {
                text: messageText,
                mentions: data.mentions
              };
              let items = [data].concat(this.state.items);
              let obj = ReactDOM.findDOMNode(this._listBlock);
              obj.scrollTop = 0;
              this.setState({
                sendingCommentLast: false,
                items
              });
            }
            this.setState({ offset: 0 });
          })
          .then(() => {
            this.getActivity(this.isAllActivityLoaded ? id : 0, true);
          })
          .catch(err => {
            console.error(
              `Error in ActivityTeam.sendComment.addCommentToActivityStream.func : ${err}`
            );
          });
      });
    }
  }

  handleSendToGroup = id => {
    this.setState(
      {
        sendToGroup: id,
        selectedGroup: id,
        offset: 0,
        hideComment: true,
        inputValue: '',
        showComment: false
      },
      () => {
        this.isAllActivityLoaded = false;
        this.getActivity(id);
      }
    );
  };

  showComment(id) {
    let val = this.state.showComment === id ? '' : id;
    this.setState({ showComment: val, hideComment: false, inputValue: undefined });
  }

  likeHandler(item, checkCurrentUserLike, index) {
    if (!this.allowVote) {
      return;
    }
    let payload = {
      vote: {
        content_id: item.id,
        content_type: 'ActivityStream'
      }
    };
    if (!checkCurrentUserLike) {
      addVotes(payload)
        .then(() => {
          let items = this.state.items;
          items[index].votesCount = ++items[index].votesCount;
          items[index].voters.push({ id: this.props.currentUser.id });
          this.setState({ items });
        })
        .catch(err => {
          console.error(`Error in ActivityTeam.addVotes.func : ${err}`);
        });
    } else {
      dislikeVotes(payload)
        .then(() => {
          let items = this.state.items;
          items[index].votesCount = --items[index].votesCount;
          items[index].voters = item.voters.filter(el => el.id != this.props.currentUser.id);
          this.setState({ items });
        })
        .catch(err => {
          console.error(`Error in ActivityTeam.dislikeVotes.func : ${err}`);
        });
    }
  }
  activityClickHandler(link, cardType, slug) {
    if (cardType) {
      let backUrl = window.location.pathname;
      window.history.pushState(null, null, `/${link}`);
      if (cardType === 'pathways' && this.state.isCardV3 && this.state.pathwayConsumptionV2) {
        this.props.dispatch(saveConsumptionPathwayHistoryURL(backUrl));
      } else if (cardType === 'journey' && this.state.isCardV3 && this.state.journeyConsumptionV2) {
        this.props.dispatch(saveConsumptionJourneyHistoryURL(backUrl));
      }
      this.props.dispatch(openStandaloneOverviewModal({ slug }, cardType));
    } else {
      const { currentUser } = this.props;
      this.props.dispatch(push(`/${link.replace('@', '') === currentUser.handle ? 'me' : link}`));
    }
  }

  activityImageClickHandler(link) {
    const { currentUser } = this.props;
    this.props.dispatch(push(`/${link.replace('@', '') === currentUser.handle ? 'me' : link}`));
  }

  findUser = (handle, mentions, userId) => {
    if (mentions.length === 1) {
      return mentions[0];
    }
    if (userId) {
      return mentions.find(user => +user.id === +userId);
    }
    return mentions.find(u => u.handle === handle);
  };

  actionByEnterClick = (e, func, arg1, arg2) => {
    e.preventDefault();
    let keyCode = e.which || e.keyCode;
    keyCode === 13 ? this[func](arg1, arg2) : null;
  };

  deleteActivityStream = activity => {
    this.props.dispatch(
      confirmation('Confirm', 'Do you want to delete the stream?', () => {
        if (activity.streamableId) {
          removeActivityStream(activity.id)
            .then(() => {
              this.removeActivityFromState(activity.id);
            })
            .catch(err => {
              console.error(
                `Error in ActivityTeam.deleteActivityStream.removeActivityStream.func : ${err}`
              );
            });
        } else if (activity.commentableType === 'Team') {
          deleteCommentFromTeam(activity.teamId, activity.id)
            .then(() => {
              this.removeActivityFromState(activity.id);
            })
            .catch(err => {
              console.error(
                `Error in ActivityTeam.deleteActivityStream.deleteCommentFromTeam.func : ${err}`
              );
            });
        } else {
          deleteCommentFromOrg(this.props.orgId, activity.id)
            .then(() => {
              this.removeActivityFromState(activity.id);
            })
            .catch(err => {
              console.error(
                `Error in ActivityTeam.deleteActivityStream.deleteCommentFromOrg.func : ${err}`
              );
            });
        }
      })
    );
  };

  removeActivityFromState = activityId => {
    this.setState(prevState => {
      let activityIndex = prevState.items.findIndex(item => item.id === activityId);
      prevState.items.splice(activityIndex, 1);
      return prevState;
    });
  };

  toggleConfirmModal = (activityId, commentId) => {
    this.props.dispatch(
      confirmation('Confirm', 'Do you want to delete the comment?', () => {
        removeActivityComment(activityId, commentId)
          .then(() => {
            this.setState(prevState => {
              let activityIndex = prevState.items.findIndex(item => item.id === activityId);
              let commentIndex = prevState.items[activityIndex].comments.findIndex(
                comment => comment.id === commentId
              );
              prevState.items[activityIndex].comments.splice(commentIndex, 1);
              prevState.items[activityIndex].commentsCount =
                prevState.items[activityIndex].commentsCount - 1;
              return prevState;
            });
          })
          .catch(err => {
            console.error(`Error in ActivityTeam.removeActivityComment.func : ${err}`);
          });
      })
    );
  };

  hideBlock = () => {
    this.props.toggleTeamFeed();
  };

  showComments = id => {
    this.setState({ showSubComments: id });
  };

  checkUser = id => {
    let isAdmin = this.props.currentUser.isAdmin;
    let isOwner = this.props.currentUser.id == id;
    return isOwner || isAdmin;
  };

  render() {
    let isMember = !!~window.location.pathname.indexOf('/teams/')
      ? this.props.currentGroup &&
        (this.props.currentGroup.isTeamAdmin ||
          this.props.currentGroup.isMember ||
          this.props.currentGroup.isTeamSubAdmin)
      : true;
    let usersMap = this.props.users.get('idMap');
    let usersArr = usersMap.filter(user => {
      let name = user.get('name') || '';
      return name.toLowerCase().indexOf(this.state.mentionQuery) !== -1;
    });
    if (!!usersArr && usersArr.size === 0) {
      usersArr = usersMap.filter(user => {
        return !!(user.get('fullName') || user.get('name')) || !!user.get('handle');
      });
    }

    let languageAbbreviation =
      this.props.currentUser &&
      this.props.currentUser.profile &&
      this.props.currentUser.profile.language !== null
        ? this.props.currentUser.profile.language
        : 'en';
    const formatter = translateTA(languageAbbreviation);

    const currentUserHandle = this.props.currentUser.handle;
    return (
      <Paper
        id="right-rail-activity"
        className={`${
          this.state.newFeedStyle ? 'team-feed-block_v2' : 'team-feed-block teem-feed-block-height'
        }`}
        style={this.styles.container}
      >
        <div className="vertical-spacing-large">
          <div className="header-line-at">
            <div className={`title-at ${!this.state.items.length ? 'inactive' : ''}`}>
              {this.props.label}
            </div>
            <div className="flex-block">
              <div className="icon-inline-block my-icon-button-block my-icon-button-block_close-btn">
                <button
                  tabIndex={0}
                  aria-label={tr('Close Team Activity')}
                  className="my-icon-button my-icon-button_small"
                  onClick={this.hideBlock}
                >
                  <span tabIndex={-1} className="hideOutline">
                    <CloseIcon focusable="false" color="#6f708b" />
                  </span>
                </button>
              </div>
            </div>
          </div>
          <Divider />
          <List
            className={`message-container`}
            style={this.styles.listBlock}
            ref={node => (this._listBlock = node)}
          >
            {this.state.pending === false && !this.state.items.length && (
              <div className="empty-msg-at data-not-available-msg">
                {tr('Nothing in your activity queue yet. Your team activity will appear here.')}
              </div>
            )}
            {this.state.pending === false &&
              this.state.items.length > 0 &&
              this.state.items.map((item, index) => {
                item.snippet && item.snippet.length > 100
                  ? (item.snippet = item.snippet.slice(0, 100) + '...')
                  : undefined;
                let linkUrl;
                let message = `<span class="user-name">**${item.user.name}**</span>`;
                let elParse = '';
                let isComment = false;
                switch (item.action) {
                  case 'created_livestream':
                    message += ` ${tr('created')} ${item.snippet}`;
                    break;
                  case 'upvote':
                    message += ` ${tr('liked')} ${item.snippet}`;
                    break;
                  case 'comment':
                    if (item.linkable.type === 'team' || item.linkable.type === 'organization') {
                      isComment = true;
                    } else {
                      message += ` ${tr('commented on')} ${item.snippet}`;
                    }
                    break;
                  case 'created_card':
                    message += ` ${tr('created')} <strong>${tr(
                      'SmartCard'
                    )}</strong>: ${item.snippet && item.snippet.replace(ACTIVITYSMARTCARD, '')}`;
                    break;
                  case 'created_pathway':
                    message += ` ${tr('created')} <strong>${tr('Pathway')}</strong>: ${
                      item.snippet
                    }`;
                    break;
                  case 'created_journey':
                    message += ` ${tr('created')} <strong>${tr('Journey')}</strong>: ${
                      item.snippet
                    }`;
                    break;
                  case 'assignment_completed':
                    message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${
                      item.snippet
                    }`;
                    break;
                  case 'smartbite_completed':
                    switch (item.linkable.type) {
                      case 'journey':
                        message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                      case 'collection':
                        message += ` ${tr('completed')} <strong>${tr('Pathway')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                      default:
                        message += ` ${tr('completed')} <strong>${tr('SmartCard')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                    }
                    break;
                  case 'smartbite_uncompleted':
                    switch (item.linkable.type) {
                      case 'journey':
                        message += ` ${tr('uncompleted')} <strong>${tr('Journey')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                      case 'collection':
                        message += ` ${tr('uncompleted')} <strong>${tr('Pathway')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                      default:
                        message += ` ${tr('uncompleted')} <strong>${tr('SmartCard')}</strong>: ${
                          item.snippet
                        }`;
                        break;
                    }
                    break;
                  default:
                    message += '';
                    break;
                }
                let linkPrefix, slug;
                switch (item.linkable.type) {
                  case 'journey':
                    linkUrl = `journey/${item.linkable.id}`;
                    slug = item.linkable.id;
                    linkPrefix = 'journey';
                    break;
                  case 'collection':
                    linkUrl = `pathways/${item.linkable.id}`;
                    slug = item.linkable.id;
                    linkPrefix = 'pathways';
                    break;
                  case 'card':
                    linkUrl = `insights/${item.linkable.id}`;
                    slug = item.linkable.id;
                    linkPrefix = 'insights';
                    break;
                  case 'video_stream':
                    linkUrl = `video_streams/${item.streamableId}`;
                    slug = item.streamableId;
                    linkPrefix = 'insights';
                    break;
                  default:
                    linkUrl = '#';
                    break;
                }
                if (isComment) {
                  elParse = extractMentions(item.message.text, item.message.mentions);
                }
                let checkCurrentUserLike = item.voters.find(
                  el => el.id == this.props.currentUser.id
                );
                let createdAtTime = new Date(item.createdAt);
                let currentTime = new Date();
                createdAtTime = currentTime < createdAtTime ? currentTime : createdAtTime;
                return (
                  <div key={`block-${index}`}>
                    <ListItem
                      disableKeyboardFocus={true}
                      leftIcon={
                        <BlurImage
                          tabIndex={0}
                          imageClickHandler={this.activityImageClickHandler.bind(
                            this,
                            item.user.handle
                          )}
                          style={
                            !this.state.newFeedStyle
                              ? this.styles.avatarBox
                              : this.styles.avatarBoxNew
                          }
                          id={item.user.id}
                          image={item.user.avatarimages && item.user.avatarimages.small}
                          titleText={`${item.user.name}'s profile`}
                        />
                      }
                      innerDivStyle={
                        !this.state.newFeedStyle ? this.styles.listItem : this.styles.listItemNew
                      }
                      hoverColor="rgba(214, 214, 225, 0.6)"
                      primaryText={
                        <div className="snippet-text">
                          {!isComment ? (
                            <div
                              className="text-comment-at"
                              tabIndex={0}
                              onClick={this.activityClickHandler.bind(
                                this,
                                linkUrl,
                                linkPrefix,
                                slug
                              )}
                            >
                              <MarkdownRenderer
                                style={this.styles.commentText}
                                markdown={message}
                                onUserNameClick={this.activityClickHandler.bind(
                                  this,
                                  item.user.handle,
                                  null
                                )}
                              />
                            </div>
                          ) : (
                            <span>
                              <div className="row">
                                <div className="small-11">
                                  <span className="text-comment-at" key={index}>
                                    <strong
                                      className="user-name"
                                      onClick={this.activityClickHandler.bind(
                                        this,
                                        item.user.handle,
                                        null
                                      )}
                                    >
                                      {item.user.name}
                                    </strong>
                                  </span>
                                </div>
                                <div className="small-1">
                                  {this.checkUser(item.userId) && (
                                    <a
                                      tabIndex={0}
                                      className="delete-link"
                                      style={{ position: 'absolute' }}
                                      onKeyPress={e =>
                                        this.actionByEnterClick(e, 'deleteActivityStream', item)
                                      }
                                      onClick={this.deleteActivityStream.bind(this, item)}
                                    >
                                      <DeleteV2
                                        focusable="false"
                                        customStyle={this.styles.deleteIcon}
                                      />
                                    </a>
                                  )}
                                </div>
                              </div>
                              {elParse.map((part, index2) => {
                                if (part.type === 'text') {
                                  return (
                                    <span className="text-comment-at" key={index2}>
                                      {' '}
                                      <Linkify properties={{ target: '_blank' }}>
                                        {part.text}
                                      </Linkify>{' '}
                                    </span>
                                  );
                                }
                                if (part.type === 'handle') {
                                  let user = this.findUser(
                                    part.handle,
                                    item.message.mentions,
                                    part.userId
                                  );
                                  if (user) {
                                    return (
                                      <UserProfileHover
                                        key={`user-id-${user.id}`}
                                        user={user}
                                        userProfileURL={`/${
                                          part.handle.replace('@', '') === currentUserHandle
                                            ? 'me'
                                            : part.handle
                                        }`}
                                      />
                                    );
                                  } else {
                                    return (
                                      <span className="text-comment-at" key={index2}>
                                        {part.handle}
                                      </span>
                                    );
                                  }
                                }
                              })}
                            </span>
                          )}
                          {this.state.newFeedStyle ? (
                            <div>
                              <span className="comment-date-at">
                                <TimeAgo date={createdAtTime} live={false} formatter={formatter} />
                              </span>
                              <div className="action-line-at">
                                {this.allowVote && (
                                  <div className="icon-inline-block my-icon-button-block">
                                    <button
                                      tabIndex={0}
                                      className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                                      disabled={!isMember || !this.allowVote}
                                      onClick={this.likeHandler.bind(
                                        this,
                                        item,
                                        checkCurrentUserLike,
                                        index
                                      )}
                                    >
                                      {!checkCurrentUserLike && (
                                        <span
                                          tabIndex={-1}
                                          className="hideOutline"
                                          aria-label="like"
                                        >
                                          <Like_outline color="#6f708b" />
                                        </span>
                                      )}
                                      {checkCurrentUserLike && (
                                        <span
                                          tabIndex={-1}
                                          className="hideOutline"
                                          aria-label="liked"
                                        >
                                          <LikeIconSelected color="#0076ff" />
                                        </span>
                                      )}
                                      <span className="tooltiptext">{tr('Like')}</span>
                                    </button>
                                    <small className="votes-count">
                                      {item.votesCount ? abbreviateNumber(item.votesCount) : ''}
                                    </small>
                                  </div>
                                )}
                                <div className="icon-inline-block my-icon-button-block">
                                  <button
                                    tabIndex={0}
                                    className="my-icon-button my-icon-button_small tooltip tooltip_bottom-center"
                                    disabled={!isMember}
                                    onClick={this.showComment.bind(this, item.id)}
                                  >
                                    <span tabIndex={-1} className="hideOutline">
                                      <Comment_v2
                                        focusable="false"
                                        color={
                                          this.state.showComment === item.id ? '#0076ff' : '#6f708b'
                                        }
                                      />
                                    </span>
                                    <span className="tooltiptext">{tr('Comment')}</span>
                                  </button>
                                  <small className="votes-count">
                                    {item.commentsCount ? abbreviateNumber(item.commentsCount) : ''}
                                  </small>
                                </div>
                              </div>
                            </div>
                          ) : (
                            <div className="action-line-at">
                              {isMember && (
                                <span>
                                  <span
                                    className="like-at"
                                    style={{ color: checkCurrentUserLike ? '#0076ff' : '#6f708b' }}
                                    onClick={this.likeHandler.bind(
                                      this,
                                      item,
                                      checkCurrentUserLike,
                                      index
                                    )}
                                  >
                                    {' '}
                                    {`${tr('Like')} ${item.votesCount}`}
                                  </span>
                                  <span>|</span>
                                  <span
                                    className="comment-at"
                                    style={{
                                      color:
                                        this.state.showComment === item.id ? '#0076ff' : '#6f708b'
                                    }}
                                    onClick={this.showComment.bind(this, item.id)}
                                  >
                                    {' '}
                                    {`${tr('Comment')} ${item.commentsCount}`}
                                  </span>
                                  <span>|</span>
                                </span>
                              )}
                              <span>
                                <TimeAgo date={createdAtTime} live={false} formatter={formatter} />
                              </span>
                            </div>
                          )}
                          {item.comments &&
                            !!item.comments.length &&
                            this.state.newFeedStyle &&
                            this.state.showSubComments !== item.id &&
                            item.comments.length > 2 && (
                              <div
                                tabIndex={0}
                                onKeyPress={e =>
                                  this.actionByEnterClick(e, 'showComments', item.id)
                                }
                                onClick={() => this.showComments(item.id)}
                                className="comments-view-more pointer"
                              >
                                {tr('View more...')}
                              </div>
                            )}
                          {this.state.newFeedStyle && (
                            <div
                              className={`flex-block sub-comment-input-container ${
                                this.state.showComment !== item.id || this.state.hideComment
                                  ? 'hide'
                                  : ''
                              }`}
                            >
                              <input
                                tabIndex={0}
                                placeholder={tr('Add a comment...')}
                                type="text"
                                className={`comment create input-comment-at`}
                                onChange={event => this.inputChangeHandler(event, item.id)}
                                value={this.state.inputValue}
                                onBlur={this.handleClose.bind(this)}
                                onKeyDown={this.onChangeClick.bind(this, item.id, index)}
                                disabled={this.state.sendingComment}
                                ref={node => (this[`input-${item.id}`] = node)}
                              />
                              <button
                                tabIndex={0}
                                style={{ color: this.state.sendingComment ? '#555555' : '#4a90e2' }}
                                disabled={this.state.sendingComment}
                                className="last-comment-btn-at"
                                onClick={this.sendSubComment.bind(this, item.id, index)}
                              >
                                {tr('Send')}
                              </button>
                            </div>
                          )}
                          {item.comments &&
                            !!item.comments.length &&
                            item.comments.map((comment, subCommentIndex) => {
                              let commentParse = extractMentions(comment.message, comment.mentions);
                              let isAbleDeleteComment =
                                comment.user.id == this.props.currentUser.id ||
                                this.props.currentUser.isAdmin;
                              if (
                                this.state.newFeedStyle &&
                                this.state.showSubComments !== item.id &&
                                subCommentIndex < item.comments.length - 2
                              ) {
                                return;
                              } else {
                                return (
                                  <ListItem
                                    key={`comment-id-${comment.id}`}
                                    disableKeyboardFocus={true}
                                    leftIcon={
                                      <BlurImage
                                        tabIndex={0}
                                        imageClickHandler={this.activityImageClickHandler.bind(
                                          this,
                                          comment.user.handle
                                        )}
                                        style={
                                          !this.state.newFeedStyle
                                            ? this.styles.avatarBox
                                            : this.styles.avatarBoxNewSubComment
                                        }
                                        image={comment.user.avatarimages.small}
                                      />
                                    }
                                    innerDivStyle={this.styles.listItemComment}
                                    primaryText={
                                      <div
                                        className={`${
                                          this.state.newFeedStyle ? 'snippet-text_sub-comment' : ''
                                        } snippet-text`}
                                      >
                                        <div>
                                          {!this.state.newFeedStyle && (
                                            <span className="text-comment-at" key={index}>
                                              <strong
                                                tabIndex={0}
                                                className="user-name"
                                                onClick={this.activityClickHandler.bind(
                                                  this,
                                                  comment.user.handle,
                                                  null
                                                )}
                                              >
                                                {comment.user.name}
                                              </strong>
                                            </span>
                                          )}
                                          {commentParse.map((part, index3) => {
                                            if (part.type === 'text') {
                                              return (
                                                <span
                                                  className="text-comment-at"
                                                  key={`text-comment-at${index3}`}
                                                >
                                                  <Linkify properties={{ target: '_blank' }}>
                                                    {part.text}
                                                  </Linkify>
                                                </span>
                                              );
                                            }
                                            if (part.type === 'handle') {
                                              let user = this.findUser(
                                                part.handle,
                                                comment.mentions
                                              );
                                              if (user) {
                                                return (
                                                  <UserProfileHover
                                                    key={`user-id-${user.id}`}
                                                    tabIndex={0}
                                                    user={user}
                                                    userProfileURL={`/${
                                                      part.handle.replace('@', '') ===
                                                      currentUserHandle
                                                        ? 'me'
                                                        : part.handle
                                                    }`}
                                                  />
                                                );
                                              } else {
                                                return (
                                                  <span
                                                    className="text-comment-at"
                                                    key={`text-comment-at${index3}`}
                                                  >
                                                    {part.handle}
                                                  </span>
                                                );
                                              }
                                            }
                                          })}
                                          {this.state.newFeedStyle && isAbleDeleteComment && (
                                            <a
                                              className="delete-link-new-feed"
                                              tabIndex={0}
                                              onKeyPress={e =>
                                                this.actionByEnterClick(
                                                  e,
                                                  'toggleConfirmModal',
                                                  item.id,
                                                  comment.id
                                                )
                                              }
                                              onClick={this.toggleConfirmModal.bind(
                                                this,
                                                item.id,
                                                comment.id
                                              )}
                                            >
                                              <DeleteV2
                                                focusable="false"
                                                customStyle={this.styles.deleteIcon}
                                              />
                                            </a>
                                          )}
                                        </div>
                                        {!this.state.newFeedStyle && (
                                          <div className="date-delete-container">
                                            <span className="comment-date-at">
                                              <TimeAgo
                                                date={comment.createdAt}
                                                live={false}
                                                formatter={formatter}
                                              />
                                            </span>
                                            {isAbleDeleteComment && (
                                              <a
                                                className="delete-link"
                                                tabIndex={0}
                                                onKeyPress={e =>
                                                  this.actionByEnterClick(
                                                    e,
                                                    'toggleConfirmModal',
                                                    item.id,
                                                    comment.id
                                                  )
                                                }
                                                onClick={this.toggleConfirmModal.bind(
                                                  this,
                                                  item.id,
                                                  comment.id
                                                )}
                                              >
                                                <DeleteV2
                                                  focusable="false"
                                                  customStyle={this.styles.deleteIcon}
                                                />
                                              </a>
                                            )}
                                          </div>
                                        )}
                                      </div>
                                    }
                                  />
                                );
                              }
                            })}

                          <div className="flex-block">
                            {!this.state.newFeedStyle && (
                              <input
                                placeholder={tr('Leave a comment / Tag peers...')}
                                type="text"
                                className={`comment create input-comment-at mt-5 ${
                                  this.state.showComment !== item.id || this.state.hideComment
                                    ? 'hide'
                                    : ''
                                }`}
                                onChange={event => this.inputChangeHandler(event, item.id)}
                                value={this.state.inputValue}
                                onBlur={this.handleClose.bind(this)}
                                onKeyDown={this.onChangeClick.bind(this, item.id, index)}
                                disabled={this.state.sendingComment}
                                ref={node => (this[`input-${item.id}`] = node)}
                              />
                            )}
                            <Popover
                              style={this.styles.selectContent}
                              containerStyle={this.styles.selectContainer}
                              placement="top"
                              container={document.getElementsByClassName('team-feed')[0]}
                              target={this[`input-${item.id}`]}
                              show={usersArr.size === 0 ? false : this.state.open}
                              onHide={this.handleClose.bind(this)}
                            >
                              <div>
                                {usersArr.map((user, index4) => {
                                  let userNameOrHandle =
                                    user.get('name') == null
                                      ? user.get('handle').replace('@', '')
                                      : user.get('name');
                                  return (
                                    <ListItem
                                      disableKeyboardFocus={true}
                                      key={`comment_user_live_${index4}${user.id}`}
                                      leftAvatar={
                                        <BlurImage
                                          imageClickHandler={this.activityClickHandler.bind(
                                            this,
                                            user.handle,
                                            null
                                          )}
                                          style={this.styles.avatarBoxComment}
                                          id={user.id}
                                          image={user.getIn(['avatarimages', 'small'])}
                                        />
                                      }
                                      primaryText={<span>{userNameOrHandle}</span>}
                                      onMouseDown={this.mentionClickHandler.bind(
                                        this,
                                        user.get('handle'),
                                        item.id,
                                        user.get('id')
                                      )}
                                    />
                                  );
                                })}
                              </div>
                            </Popover>
                          </div>
                        </div>
                      }
                    />
                  </div>
                );
              })}
            {(this.state.pendingSmall || this.state.pending) && (
              <div className="text-center pending-at">
                <Spinner />
              </div>
            )}
          </List>
          {this.state.pending !== null && (
            <div className="send-block-at">
              {!this.state.newFeedStyle && <Divider style={this.styles.dividerStyle} />}
              <div className="send-line-at">
                <span className="send-line-label-at">
                  {this.state.newFeedStyle ? tr('Interact with') : tr('Send to:')}
                </span>
                <SelectField
                  hintText={
                    this.state.defaultGroupLabel ? this.state.defaultGroupLabel : tr('All Groups')
                  }
                  value={this.state.sendToGroup}
                  hintStyle={this.styles.hint}
                  style={{
                    ...this.styles.mainStyleSend,
                    ...(this.state.newFeedStyle ? { background: 'white' } : {})
                  }}
                  fullWidth={this.props.fullWidth}
                  underlineShow={false}
                  iconStyle={this.styles.iconStyleSendTo}
                  labelStyle={this.styles.selectedSendTo}
                  maxHeight={200}
                  menuStyle={this.styles.menuStyle}
                  menuItemStyle={this.styles.menuItems}
                >
                  <MenuItem
                    value={0}
                    onTouchTap={this.handleSendToGroup.bind(this, 0)}
                    className="menu-item-custom postToChannel"
                    primaryText={tr('All Groups')}
                  />
                  {this.state.groups.length &&
                    this.state.groups.map((group, index) => (
                      <MenuItem
                        key={`group-key-send-${index}`}
                        value={group.id}
                        onTouchTap={this.handleSendToGroup.bind(this, group.id)}
                        className="menu-item-custom postToChannel"
                        primaryText={group.name}
                      />
                    ))}
                  {!this.state.isLastGroup && (
                    <div className="text-center">
                      <SecondaryButton
                        label={tr('View More')}
                        onTouchTap={this.showMoreGroups}
                        className="view-more-button viewMore"
                      />
                    </div>
                  )}
                </SelectField>
              </div>
              <div className="flex-block">
                <input
                  placeholder={
                    this.state.newFeedStyle
                      ? tr('Leave a comment here...')
                      : tr('Leave a comment / Tag peers...')
                  }
                  type="text"
                  className="comment create input-comment-at pr-65"
                  onChange={this.inputChangeHandler}
                  onBlur={this.handleClose.bind(this)}
                  onKeyDown={this.onChangeClickLast}
                  disabled={this.state.sendingCommentLast}
                  ref={node => (this._lastCommentInput = node)}
                />
                <span className="last-comment-span-at">&#64;</span>
                <button
                  style={{ color: this.state.sendingCommentLast ? '#555555' : '#4a90e2' }}
                  disabled={this.state.sendingCommentLast}
                  className="last-comment-btn-at"
                  onClick={this.sendComment.bind(this, false)}
                >
                  {tr('Send')}
                </button>
                <Popover
                  style={this.styles.selectContent}
                  containerStyle={this.styles.selectContainer}
                  placement="top"
                  container={document.getElementById('right-rail')}
                  target={this._lastCommentInput}
                  show={usersArr.size === 0 ? false : this.state.openLast}
                  onHide={this.handleClose.bind(this)}
                >
                  {usersArr.map((user, index) => {
                    let userNameOrHandle =
                      user.get('name') == null
                        ? user.get('handle').replace('@', '')
                        : user.get('name');
                    return (
                      <ListItem
                        disableKeyboardFocus={true}
                        key={`comment_user_live_${index}${user.id}`}
                        leftAvatar={
                          <BlurImage
                            imageClickHandler={this.activityClickHandler.bind(
                              this,
                              user.handle,
                              null
                            )}
                            style={this.styles.avatarBoxComment}
                            id={user.id}
                            image={user.getIn(['avatarimages', 'small'])}
                          />
                        }
                        primaryText={<span>{userNameOrHandle}</span>}
                        onMouseDown={this.mentionClickHandler.bind(
                          this,
                          user.get('handle'),
                          false,
                          user.get('id')
                        )}
                      />
                    );
                  })}
                </Popover>
              </div>
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

ActivityTeam.propTypes = {
  users: PropTypes.object,
  currentUser: PropTypes.object,
  currentGroup: PropTypes.object,
  teamActivity: PropTypes.object,
  fullWidth: PropTypes.bool,
  orgId: PropTypes.any,
  label: PropTypes.node,
  onlyConversation: PropTypes.bool,
  newFeedStyle: PropTypes.bool,
  toggleTeamFeed: PropTypes.func
};

export default connect(state => ({
  teamActivity: state.teamActivity.toJS(),
  users: state.users,
  currentUser: state.currentUser.toJS()
}))(ActivityTeam);
