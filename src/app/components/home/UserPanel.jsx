import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';

import LinearProgress from 'material-ui/LinearProgress';

import BlurImage from '../common/BlurImage';
import Announcements from './LeftRailAnnouncements';

import Paper from 'edc-web-sdk/components/Paper';
import { getAssignmentsV2UserPanel } from 'edc-web-sdk/requests/assignments.v2';
import { getUserClc } from 'edc-web-sdk/requests/users';
import { Menu, AsyncTypeahead } from 'react-bootstrap-typeahead';
import { getMeetUpData, getMeetUpevents } from 'edc-web-sdk/requests/ecl';
import unescape from 'lodash/unescape';
import Spinner from '../common/spinner';
import RichTextReadOnly from '../common/RichTextReadOnly';
import convertRichText from '../../utils/convertRichText';
import { langs } from '../../constants/languages';

class UserPanel extends Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;

    let announcementsSetting =
      this.props.team &&
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.leftRail &&
      this.props.team.OrgConfig.leftRail['web/leftRail/announcements'];
    this.assignmentData = {};
    this.allAssignmentData = {};
    this.styles = {
      clcProgressBar: {
        backgroundColor: '#f0f0f5',
        height: '6px',
        borderRadius: '5px'
      },
      learningPlan: {
        borderRadius: '0',
        width: this.props.isCardV3 ? '18.125rem' : '18.75rem',
        padding: '0.0625rem 0.9375rem 1.5rem',
        marginTop: '0.9375rem'
      },
      announcementStyle: {
        borderRadius: '0',
        width: this.props.isCardV3 ? '18.125rem' : '18.75rem',
        marginTop: '1rem',
        minHeight: '3.75rem'
      },
      blockStyle: {
        borderRadius: '0',
        width: this.props.isCardV3 ? '18.125rem' : '18.75rem',
        padding: '0.9375rem',
        marginBottom: '0.9375rem'
      }
    };

    this.state = {
      assignmentData: [],
      allAssignmentData: [],
      clcData: [],
      ClcTarget: '',
      ClcExists: false,
      ClcTargetHours: '',
      newUserPanelStyle: window.ldclient.variation('user-panel-v2', false),
      searchBoxValue: '',
      searchBoxSuggestions: [],
      meetUpEvents: null,
      loadingEvents: false,
      meetupIntegraion: window.ldclient.variation('meetup-integration', false),
      meetUpGroup: '',
      showLearningPlan: window.ldclient.variation('learning-plan-tab', true),
      homePagev1: window.ldclient.variation('home-page-fix-v1', false)
    };
    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;

    this.isShowAnnouncements =
      announcementsSetting && announcementsSetting.align && announcementsSetting.visible;
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }

    return card_type;
  }

  componentDidMount() {
    let payload = {
      clc: {
        entity_id: this.props.team.orgId,
        entity_type: 'Organization'
      }
    };

    getUserClc(payload)
      .then(data => {
        let score = data.score;
        let ClcTarget = data.targetScore;
        this.setState(
          {
            ClcTarget,
            ClcExists: true,
            ClcScore: score,
            ClcTargetHours: ClcTarget,
            awardedBadge: data.earnedBadge
          },
          this.updateScrollableHeight
        );
      })
      .catch(error => {
        this.updateScrollableHeight();
        console.error(`Error in LeftRailClcProgressv2.getUserClc.func : ${error}`);
        return error;
      });

    let card_types = this.getAssignmentContentType();
    let params = {
      limit: 5,
      'card_types[]': card_types
    };

    this.state.showLearningPlan &&
      getAssignmentsV2UserPanel(params)
        .then(data => {
          this.setState(
            {
              assignmentData: data && data.items
            },
            () =>
              setTimeout(() => {
                this.updateScrollableHeight();
              }, 1)
          );
        })
        .catch(err => {
          console.error(`Error in UserPanel.getAssignmentsV2UserPanel.func: ${err}`);
        });
  }

  updateScrollableHeight = () => {
    let userDetails = ReactDOM.findDOMNode(this._userDetails);
    let announcementBlock = ReactDOM.findDOMNode(this._announcementBlock);
    if (announcementBlock) {
      let userPanel = ReactDOM.findDOMNode(this._userPanel);
      let yourLearningPlan = ReactDOM.findDOMNode(this._yourLearningPlan);
      let minus = 0;
      if (userPanel) {
        minus =
          (userDetails ? userDetails.clientHeight : 0) +
          (yourLearningPlan ? yourLearningPlan.clientHeight : 0) +
          50;
      }
      announcementBlock.style.height = `calc(100% - ${minus}px)`;
    }
  };

  viewMoreLearningPlan = e => {
    e.preventDefault();
    this.props.dispatch(push('/me/learning'));
  };

  navigateTo = (e, cardType, slug) => {
    e.preventDefault();
    let url =
      (cardType === 'pack' ? '/pathways/' : cardType === 'journey' ? '/journey/' : '/insights/') +
      slug;
    window.location = url;
  };

  _renderMenu = (results, menuProps) => {
    const menuData = (results || []).map(data => {
      return [
        <li
          onClick={e => {
            this._handleResultsClick(data);
          }}
        >
          <a>
            <div>{data.name}</div>
          </a>
        </li>
      ];
    });
    return <Menu {...menuProps}>{menuData}</Menu>;
  };

  _handleResultsClick = data => {
    let payload = {
      urlname: data.urlname,
      key: '2e206b622b3653472b4427411959a',
      sign: true
    };
    this.setState({
      loadingEvents: true,
      meetUpGroup: data.name
    });
    getMeetUpevents(payload)
      .then(response => {
        let meetUpEvents = response.data;
        this.setState({
          meetUpEvents,
          loadingEvents: false
        });
      })
      .catch(e => {
        console.error(e);
      });
    this._searchBox.getInstance().clear();
  };

  _handleSearch = query => {
    let payload = {
      location: 'Mountain View',
      text: query,
      order: 'distance',
      key: '2e206b622b3653472b4427411959a',
      sign: true
    };

    if (query.length >= 3) {
      getMeetUpData(payload)
        .then(response => {
          let searchBoxSuggestions = response.data;
          this.setState({ searchBoxSuggestions });
        })
        .catch(e => {
          console.error(e);
        });
    }
  };

  render() {
    let enableClcBadgings =
      this.props.team.OrgConfig.badgings &&
      this.props.team.OrgConfig.badgings['web/badgings'] &&
      this.props.team.OrgConfig.badgings['web/badgings'].clcBadgings;
    let text = parseInt(this.state.ClcScore / 60, 10) == 1 ? tr('hr') : tr('hrs');
    let clcHours =
      parseInt(this.state.ClcScore / 60, 10) > 0
        ? `${parseInt(this.state.ClcScore / 60, 10)} ${text} ${tr('and')} `
        : '';
    let clcMins = `${this.state.ClcScore % 60} ${tr('mins')}`;

    let announcementLabel = '';

    let announcementsSetting = this.props.team.OrgConfig.leftRail['web/leftRail/announcements'];
    if (this.isShowAnnouncements) {
      let translatedLabel =
        this.isShowCustomLabels &&
        announcementsSetting.languages &&
        announcementsSetting.languages[this.profileLanguage] &&
        announcementsSetting.languages[this.profileLanguage].trim();
      announcementLabel = translatedLabel || tr(announcementsSetting.label || '');
    }

    const props = {};
    props.renderMenu = this._renderMenu;
    let dataSourse = this.state.searchBoxSuggestions;
    return (
      <div
        id="user-panel"
        ref={node => (this._userPanel = node)}
        className={this.state.newUserPanelStyle ? 'user-panel_v2' : 'user-panel'}
      >
        <Paper
          id="user-details"
          ref={node => (this._userDetails = node)}
          style={{ ...this.styles.blockStyle, ...{ marginBottom: '15px' } }}
        >
          <div>
            <div
              className={`user-details-container text-center ${
                this.state.homePagev1 ? 'fix-v1' : ''
              }`}
            >
              <div className="image-container">
                <BlurImage
                  image={this.props.currentUser.avatar}
                  id={this.props.currentUser.id}
                  userPanel={true}
                  imageClickHandler={() => {
                    this.props.dispatch(push('/me'));
                  }}
                />
              </div>
              <div className="user-name-container">
                <a
                  onClick={() => {
                    this.props.dispatch(push('/me'));
                  }}
                >
                  {this.props.currentUser.name}
                </a>
              </div>
            </div>
            {this.state.ClcExists && (
              <div className="your-learning-plan">
                <div
                  className={`your-learning-plan-count ${this.state.homePagev1 ? 'fix-v1' : ''}`}
                >
                  {clcHours}
                  {clcMins}
                </div>
                {this.state.newUserPanelStyle ? (
                  <div
                    className={`your-learning-plan-title ${this.state.homePagev1 ? 'fix-v1' : ''}`}
                  >
                    {new Date().getFullYear()} {tr('Continuous Learning Hours ')}
                  </div>
                ) : (
                  <div
                    className={`your-learning-plan-title ${this.state.homePagev1 ? 'fix-v1' : ''}`}
                  >
                    {tr('Continuous Learning Hours in')} {new Date().getFullYear()}
                  </div>
                )}

                {enableClcBadgings && (
                  <div className="clc-badgings-container">
                    <div
                      className={`your-learning-plan-count ${
                        this.state.homePagev1 ? 'fix-v1' : ''
                      }`}
                    >
                      {parseInt(this.state.ClcTargetHours, 10)}{' '}
                      {parseInt(this.state.ClcTargetHours, 10) == 1 ? ' hr' : ' hrs'}
                    </div>
                    <div
                      className={`your-learning-plan-title ${
                        this.state.homePagev1 ? 'fix-v1' : ''
                      }`}
                    >
                      {tr('Annual Learning Goal')}
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </Paper>

        {this.state.meetupIntegraion && (
          <Paper
            ref={node => (this._meetUp = node)}
            style={{ ...this.styles.blockStyle, ...{ marginBottom: '15px' } }}
          >
            <div>
              <div style={{ margin: '0 0 5px 0' }}>
                <img src="https://a248.e.akamai.net/secure.meetupstatic.com/img/84869143793177372874/birddog/everywhere_widget.png" />
              </div>
              <div className="user-search-input">
                <AsyncTypeahead
                  {...props}
                  labelKey="name"
                  options={dataSourse}
                  placeholder={tr('Search Groups')}
                  onSearch={q => {
                    this._handleSearch(q);
                  }}
                  submitFormOnEnter={false}
                  ref={ref => (this._searchBox = ref)}
                  value={this.state.searchBoxValue}
                  filterBy={(userData, property) => {
                    return true;
                  }}
                />
              </div>

              <div style={{ margin: '10px 0 0 0' }}>
                {!!this.state.meetUpEvents && !!this.state.meetUpEvents.length && (
                  <div
                    style={{
                      margin: '0 0 5px 0',
                      fontSize: '13px',
                      fontWeight: '600'
                    }}
                  >
                    Events for {this.state.meetUpGroup} group :{' '}
                  </div>
                )}
                {!!this.state.meetUpEvents &&
                  !!this.state.meetUpEvents.length &&
                  !this.state.loadingEvents && (
                    <div style={{ height: '170px', overflowY: 'auto' }}>
                      {this.state.meetUpEvents.map(meetupEvent => {
                        return (
                          <div
                            style={{
                              border: '1px solid #c3c3c3',
                              margin: '5px 0',
                              padding: '3px',
                              borderRadius: '6px'
                            }}
                          >
                            <div style={{ textAlign: 'center', fontSize: '13px' }}>
                              <a target="_blank" href={meetupEvent.link}>
                                {meetupEvent.name}
                              </a>
                            </div>
                            <div style={{ textAlign: 'center', fontSize: '12px' }}>
                              {meetupEvent.local_date}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )}
                {this.state.loadingEvents && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
                {!this.state.loadingEvents &&
                  !!this.state.meetUpEvents &&
                  !this.state.meetUpEvents.length && (
                    <div
                      style={{
                        textAlign: 'center',
                        fontSize: '12px',
                        color: '#c3c3c3',
                        margin: '15px 0'
                      }}
                    >
                      No Event found.
                    </div>
                  )}
              </div>
            </div>
          </Paper>
        )}

        {this.state.showLearningPlan && (
          <Paper ref={node => (this._yourLearningPlan = node)} style={this.styles.learningPlan}>
            <div className="your-learning-plan">
              {this.state.newUserPanelStyle ? (
                <div
                  className="your-learning-plan-header"
                  style={{ marginBottom: '15px' }}
                  role="heading"
                  aria-level="3"
                >
                  {tr('My Learning Plan')}
                  <a
                    href="#"
                    className="view-more-link pull-right"
                    onClick={e => {
                      this.viewMoreLearningPlan(e);
                    }}
                  >
                    {tr('View more...')}
                  </a>
                </div>
              ) : (
                <div className="your-learning-plan-header" style={{ marginBottom: '15px' }}>
                  <a
                    href="#"
                    role="heading"
                    aria-level="3"
                    onClick={e => {
                      this.viewMoreLearningPlan(e);
                    }}
                  >
                    {tr('My Learning Plan')}
                  </a>
                </div>
              )}
              {this.state.assignmentData.length > 0 ? (
                <div>
                  {this.state.assignmentData.map((card, index) => {
                    return (
                      <div className="your-learning-plan-skill-stats" key={card.id}>
                        <a
                          href="#"
                          className="your-learning-plan-skill-name"
                          onClick={e => {
                            this.navigateTo(e, card.cardType, card.slug);
                          }}
                        >
                          <RichTextReadOnly text={convertRichText(card.title || card.message)} />
                        </a>
                        <div className="your-learning-plan-progress">
                          <LinearProgress
                            style={{
                              backgroundColor: '#f0f0f5',
                              height: '1rem',
                              borderRadius: '0px'
                            }}
                            color={'#acadc1'}
                            min={0}
                            max={100}
                            value={card.progress}
                            mode="determinate"
                          />
                          <div
                            className="indicator dark"
                            style={{
                              width: (card.progress >= 100 ? 100 : card.progress) + '%'
                            }}
                          >
                            <div>
                              <span>
                                {card.progress >= 100 ? 100 : parseInt(card.progress, 10)}%
                              </span>
                            </div>
                          </div>
                          <div
                            className="indicator"
                            style={{
                              width: (card.progress >= 100 ? 100 : card.progress) + '%'
                            }}
                          >
                            <div className={card.progress <= 20 ? 'hide-right-indicator' : ''}>
                              <span style={{ color: '#45445e' }}>
                                {card.progress >= 100 ? 100 : parseInt(card.progress, 10)}%
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              ) : (
                <div
                  className="data-not-available-msg"
                  style={{ fontSize: '14px', color: 'rgb(69, 69, 96)' }}
                >
                  {tr('Your learning plan is empty, assigned content will show here.')}
                </div>
              )}
            </div>
          </Paper>
        )}

        {this.isShowAnnouncements && (
          <div
            className="announcement-block"
            ref={node => (this._announcementBlock = node)}
            style={this.styles.announcementStyle}
          >
            <Announcements label={announcementLabel} autoHeightContent={true} />
          </div>
        )}
      </div>
    );
  }
}

UserPanel.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  myLearningPlan: PropTypes.object
};

export default connect(state => ({
  team: state.team.toJS(),
  assignments: state.assignments.toJS(),
  currentUser: state.currentUser.toJS(),
  myLearningPlan: state.myLearningPlan.toJS()
}))(UserPanel);
