import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Card from '../cards/Card';
import Paper from 'edc-web-sdk/components/Paper';
import EdcFullLogo from 'edc-web-sdk/components/icons/EdcFullLogov2';
import LoginContainer from '../../components/login/LoginContainer.jsx';
import RaisedButton from 'material-ui/RaisedButton';
import { getCards } from 'edc-web-sdk/requests/cards';
import { getChannel } from 'edc-web-sdk/requests/channels';
import { getSearchTodayLearningItems } from 'edc-web-sdk/requests/feed';
import { tr } from 'edc-web-sdk/helpers/translations';
import linkPrefix from '../../utils/linkPrefix';

class WidgetContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      viewAll: false,
      loggedIn: false,
      cards: [],
      loading: true,
      showLogin: false,
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
  }

  componentDidMount() {
    if (this.props.location.query.showLogin) {
      this.setState({ showLogin: true });
    } else {
      this.setState(
        {
          loggedIn: this.props.currentUser.isLoggedIn,
          queries: this.props.location.query,
          label: this.props.location.query.label
        },
        () => {
          if (this.state.loggedIn) {
            this.getCards(this.state.queries);
          }
        }
      );
    }
  }

  componentDidUpdate() {
    window.parent.postMessage(
      { height: document.body.scrollHeight, source: window.location.href },
      '*'
    );
  }

  getCards = queries => {
    let limit = queries.limit || 4;
    switch (queries.type) {
      case 'channel':
        if (isNaN(queries.id)) {
          getChannel(queries.id)
            .then(channel => {
              getCards({ 'channel_id[]': channel.id, limit })
                .then(cards => {
                  this.setState({
                    cards,
                    viewAllLink: `/channel/${queries.id}`,
                    loading: false,
                    label: channel.label
                  });
                })
                .catch(err => {
                  console.error(`Error in WidgetContainer.getCards.func channel: ${err}`);
                });
            })
            .catch(err => {
              console.error(`Error in WidgetContainer.getCards.func channel by slug: ${err}`);
            });
        } else {
          getCards({ 'channel_id[]': queries.id, limit })
            .then(cards => {
              this.setState({ cards, viewAllLink: `/channel/${queries.id}`, loading: false });
            })
            .catch(err => {
              console.error(`Error in WidgetContainer.getCards.func channel: ${err}`);
            });
        }
        break;
      case 'search':
        getCards({ q: queries.q, limit })
          .then(cards => {
            this.setState({ cards, viewAllLink: `/search?q=${queries.q}`, loading: false });
          })
          .catch(err => {
            console.error(`Error in WidgetContainer.getCards.func search: ${err}`);
          });
        break;
      case 'dailylearning':
        getSearchTodayLearningItems({
          limit: 3,
          offset: 0,
          filter_by_language: this.state.multilingualContent
        })
          .then(resp => {
            this.setState({
              cards: resp.cards,
              viewAllLink: document.location.origin,
              loading: false
            });
          })
          .catch(err => {
            console.error(err);
          });
      default:
        // FIXME: implement default case
        break;
    }
  };

  cardClickHandler = (card, e) => {
    e.stopPropagation();
    window.open(`/${linkPrefix(card.cardType)}/${card.slug}`);
  };

  popupListener = message => {
    if (message.data) {
      if (message.data.id) {
        window.removeEventListener('message', this.popupListener);
        window.wtest.close();
        document.location.href = document.location.href;
      }
    }
  };

  openLogin = () => {
    window.addEventListener('message', this.popupListener, false);
    var w = 400;
    var h = 600;
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
      ? document.documentElement.clientWidth
      : screen.width;
    var height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
      ? document.documentElement.clientHeight
      : screen.height;

    var left = width / 2 - w / 2 + dualScreenLeft;
    var top = height / 2 - h / 2 + dualScreenTop;
    window.wtest = window.open(
      document.location.href + `&showLogin=true`,
      'loginWindow',
      `top=${top},left=${left},toolbar=no,scrollbars=yes,resizable=no,width=${w},height=${h}`
    );
  };

  removeCardFromList = id => {
    let newCardsList = this.state.cards.filter(card => {
      return card.id !== id;
    });
    this.setState({ cards: newCardsList });
  };

  render() {
    return (
      <div id="widget-container">
        {this.state.showLogin && <LoginContainer />}
        {!this.state.showLogin && !this.state.loggedIn && (
          <Paper>
            <div className="container-padding sign-in-modal">
              <div className="logo-wrapper container-padding">
                <div className="row">
                  <div className="small-12 columns">
                    <div className="logo-container">
                      <EdcFullLogo style={{ height: '100px', width: '200px' }} />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 columns">
                    <div className="tag-line">
                      <strong>
                        {tr(
                          'The leader in providing AI-powered Knowledge Cloud solutions for unified discovery, personalized learning, and sales enablement.'
                        )}
                      </strong>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="small-12 columns">
                    <div className="button-wrapper">
                      <RaisedButton
                        className="sign-up-button"
                        labelColor="white"
                        backgroundColor="#9476C9"
                        label={tr('Log In')}
                        onClick={this.openLogin}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Paper>
        )}
        {!this.state.showLogin && this.state.loggedIn && (
          <div className="container-padding">
            <div className="row">
              <div className="small-12 columns">
                <div className="widget-title">
                  <div className="widget-title-label">
                    {this.state.label || 'EdCast Knowledge Cloud'}
                  </div>
                  <a
                    style={{ display: 'inline', float: 'right' }}
                    href={this.state.viewAllLink}
                    target="_blank"
                  >
                    {tr('View All')}
                  </a>
                </div>
                <div className="row">
                  {this.state.loading && (
                    <div className="data-not-available-msg">{tr('Loading...')}</div>
                  )}
                  {!this.state.loading && (
                    <div className="small-12 columns channel-card-wrapper">
                      {this.state.cards.length > 0 && (
                        <div className="four-card-column container-padding widget-wrapper">
                          {this.state.cards.map(card => {
                            return (
                              <div
                                className="card-container"
                                onClickCapture={this.cardClickHandler.bind(this, card)}
                                key={card.id}
                              >
                                <Card
                                  key={card.id}
                                  toggleSearch={function() {}}
                                  author={card.author && card.author}
                                  card={card}
                                  user={this.props.currentUser}
                                  isMarkAsCompleteDisabled={true}
                                  moreCards={false}
                                  dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                                  startDate={
                                    card.startDate || (card.assignment && card.assignment.startDate)
                                  }
                                  withoutCardModal={true}
                                  hideActions={true}
                                  commentDisabled={true}
                                  removeCardFromList={this.removeCardFromList}
                                />
                              </div>
                            );
                          })}
                        </div>
                      )}
                      {this.state.cards.length === 0 && (
                        <div className="data-not-available-msg">{tr('No cards available.')}</div>
                      )}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

WidgetContainer.propTypes = {
  location: PropTypes.any,
  currentUser: PropTypes.object
};

export default connect(state => {
  return { currentUser: state.currentUser.toJS() };
})(WidgetContainer);
