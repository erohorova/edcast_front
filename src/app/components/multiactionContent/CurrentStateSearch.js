import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import { getCurrentlyAssigned } from 'edc-web-sdk/requests/assignments.v2';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';
import { Permissions } from '../../utils/checkPermissions';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import {
  _fetchSearchedAssigned,
  _fetchAllAssigned,
  _clearSearchQuery
} from '../../actions/multiactionActions';

class CurrentStateSearch extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      searchIcon: {
        position: 'absolute',
        right: '0.2rem',
        top: '0.2rem',
        height: '1.2rem',
        width: '1.2rem',
        zIndex: '10',
        color: '#6f708b'
      }
    };
    this.state = {
      users: [],
      totalUsers: 0,
      groups: [],
      totalGroups: 0,
      searchBoxValue: ''
    };
  }

  _handleInput = query => {
    if (query.length === 0) {
      this.props.dispatch(_clearSearchQuery(this.props.searchType));
    } else {
      this._handleSearch(query);
    }
  };

  _handleSearch = query => {
    if (this.props.currentAction === 'assign') {
      let payload = {
        limit: 15,
        offset: 0,
        q: query,
        search_type: this.props.searchType,
        content_id: this.props.card.id
      };

      if (!Permissions.has('ADMIN_ONLY')) {
        payload['assignor_id'] = this.props.currentUser.get('id');
      }

      getCurrentlyAssigned(payload)
        .then(response => {
          if (this.props.searchType === 'user') {
            this.props.dispatch(_fetchSearchedAssigned(response.body, 'User', query));
          }
          if (this.props.searchType === 'team') {
            this.props.dispatch(_fetchSearchedAssigned(response.body, 'Team', query));
          }
        })
        .catch(e => {
          console.error(`Error in _handleSearch.getCurrentlyAssigned.func : ${e}`);
        });
    }
  };

  render() {
    const props = {};
    const { contentMultiaction, searchType, currentAction } = this.props;

    props.renderMenu = this._renderMenu;

    let dataSourse = [];
    if (searchType === 'user') {
      dataSourse = contentMultiaction && contentMultiaction.get('allAssignedUsers');
    } else if (searchType === 'team') {
      dataSourse = contentMultiaction && contentMultiaction.get('allAssignedTeams');
    }

    let searchValue =
      currentAction === 'assign'
        ? contentMultiaction.get('searchAssignedQuery') || ''
        : currentAction === 'share'
        ? contentMultiaction.get('searchSharedQuery')
        : '';
    return (
      <div>
        <div className="user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={dataSourse}
            placeholder={tr('Search')}
            onInputChange={q => this._handleInput(q)}
            onSearch={q => q.length > 0 && this._handleSearch(q)}
            submitFormOnEnter={false}
            ref={ref => (this._currentStateSearch = ref)}
            value={searchValue}
          />
          <SearchIcon style={this.styles.searchIcon} />
        </div>
      </div>
    );
  }
}

CurrentStateSearch.propTypes = {
  currentUser: PropTypes.any,
  contentMultiaction: PropTypes.any,
  card: PropTypes.any,
  activeTab: PropTypes.any,
  assignments: PropTypes.any,
  currentAction: PropTypes.string,
  searchType: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser,
    contentMultiaction: state.contentMultiaction
  };
}

export default connect(mapStoreStateToProps)(CurrentStateSearch);
