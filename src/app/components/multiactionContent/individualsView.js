import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Scrollbars } from 'react-custom-scrollbars';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Checkbox from 'material-ui/Checkbox';

import ListItem from './listItem';
import Spinner from '../common/spinner';
import * as multiActions from '../../actions/multiactionActions';
import {
  getUserCustomFields,
  getAllCustomFields,
  searchForUsers
} from 'edc-web-sdk/requests/users.v2';
import { Permissions } from '../../utils/checkPermissions';

class IndividualsView extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      root: {
        display: 'inline-block',
        width: '1.25rem',
        height: '2.25rem',
        verticalAlign: 'middle',
        overflow: 'visible'
      },
      label: {
        margin: '0.3125rem 0 0.3125rem 0',
        fontSize: '0.75rem',
        letterSpacing: '0.025rem',
        fontWeight: 600
      },
      icon: {
        width: '1.125rem',
        height: '1.125rem',
        marginRight: '0.3125rem',
        marginTop: '0.25rem',
        marginLeft: '0'
      }
    };
    this.state = {
      loadMore: false,
      isCheckedAll: false,
      userCustomFields: [],
      customFieldsList: [],
      customFieldsLoading: false,
      usersToShow: [],
      currentGroup: {},
      users: props.users || []
    };
    this.currentCard =
      (this.props.contentMultiaction &&
        this.props.contentMultiaction.currentCard &&
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard]) ||
      {};
  }

  componentWillReceiveProps(nextProps) {
    let oldUsers =
      this.props.users ||
      (this.props.contentMultiaction && this.props.contentMultiaction[this.props.currentAction]) ||
      [];
    let newUsers =
      nextProps.users ||
      (nextProps.contentMultiaction && nextProps.contentMultiaction[this.props.currentAction]) ||
      [];
    if (
      oldUsers.length !== newUsers.length ||
      (newUsers.length && !(this.state.usersToShow && this.state.usersToShow.length)) ||
      _.difference(_.map(oldUsers, 'id').sort(), _.map(newUsers, 'id').sort()).length
    ) {
      this.setUsersToShow(nextProps);
    }
    if (!this.state.customFieldsLoading) {
      this.fetchUserCustomFields(newUsers);
    }
    let oldGroups = this.props.groupsV2;
    let newGroups = nextProps.groupsV2;
    if (
      oldGroups &&
      oldGroups.currentGroupID &&
      oldGroups[oldGroups.currentGroupID] &&
      oldGroups[oldGroups.currentGroupID].members &&
      oldGroups[oldGroups.currentGroupID].members.length !== newGroups &&
      newGroups.currentGroupID &&
      newGroups[newGroups.currentGroupID] &&
      newGroups[newGroups.currentGroupID].members &&
      oldGroups[oldGroups.currentGroupID].members.length
    ) {
      this.setState({ currentGroup: newGroups[newGroups.currentGroupID] });
    }
    this.checkAllState();
  }

  fetchUserCustomFields(users) {
    let user_ids = [];
    if (users.length) {
      user_ids = users.map(function(data) {
        return data.id;
      });
    } else {
      return;
    }
    if (user_ids.length !== 0) {
      this.setState({ customFieldsLoading: true }, () => {
        getUserCustomFields({ 'user_ids[]': user_ids })
          .then(data => {
            getAllCustomFields()
              .then(res => {
                this.setState({
                  userCustomFields: data,
                  customFieldsList: res.customFields,
                  customFieldsLoading: false
                });
              })
              .catch(err => {
                this.setState({ customFieldsLoading: false });
                console.error(
                  'An error had been occurred while calling getUserCustomFields.getAllCustomFields function\n',
                  err
                );
              });
          })
          .catch(err => {
            this.setState({ customFieldsLoading: false });
            console.error(
              'An error had been occurred while calling getUserCustomFields function\n',
              err
            );
          });
      });
    }
  }

  _toggleAllCheckboxes = () => {
    let assignedUsers =
      (this.props.assignments &&
        this.props.assignments.assignedUsers &&
        this.props.assignments.assignedUsers.users) ||
      [];
    let sharedUsers = this.currentCard && this.currentCard.usersWithAccess;
    let invitedUsers = [
      ...((this.state.currentGroup && this.state.currentGroup.members) || []),
      ...((this.state.currentGroup && this.state.currentGroup.pending) || [])
    ];
    let users =
      this.props.users ||
      (this.props.contentMultiaction && this.props.contentMultiaction.users) ||
      [];
    let currentUsers =
      this.props.currentAction === 'assign'
        ? assignedUsers
        : this.props.currentAction === 'invite'
        ? invitedUsers
        : sharedUsers;
    users =
      this.props.currentAction === 'invite' ? _.differenceBy(users, invitedUsers, 'email') : users;
    this.props.dispatch(
      multiActions.toggleIndividuals(
        this.state.isCheckedAll,
        currentUsers,
        users,
        this.props.currentAction
      )
    );
  };

  handleScroll = _.throttle(
    e => {
      let users =
        (this.props.contentMultiaction &&
          this.props.contentMultiaction[this.props.currentAction] &&
          this.props.contentMultiaction[this.props.currentAction]) ||
        [];
      let userTotal =
        (this.props.contentMultiaction && this.props.contentMultiaction.totalUsers) || 0;

      if (this.props.modal.filter && this.props.modal.filter.length > 1) {
        let usersPayload = { limit: 200, offset: this.state.users.length, q: '' };
        if (
          !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
          this.props.currentAction === 'assign'
        ) {
          usersPayload['only_from_my_teams'] = true;
        }

        if (
          userTotal > this.state.users.length &&
          e.clientHeight + e.scrollTop === e.scrollHeight
        ) {
          this.setState({ loadMore: true });
          searchForUsers(usersPayload)
            .then(user => {
              let newUsers = [...this.state.users, ...user.users];
              this.setState({
                users: newUsers,
                loadMore: false,
                usersToShow: newUsers
              });
              this.getShownUsers(newUsers.length);
            })
            .catch(() => {
              this.setState({ loadMore: false });
            });
        }
      } else {
        if (userTotal > users.length && e.clientHeight + e.scrollTop === e.scrollHeight) {
          this.setState({ loadMore: true });

          let payload = {
            limit: 15,
            offset: users.length,
            full_response: true,
            sort: 'first_name',
            order: 'asc'
          };
          if (
            !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
            this.props.currentAction === 'assign'
          ) {
            payload['only_from_my_teams'] = true;
          }

          this.props
            .dispatch(multiActions._fetchUsers(payload, this.props.currentAction))
            .then(() => {
              this.setState({ loadMore: false });
            })
            .catch(() => {
              this.setState({ loadMore: false });
            });
        }
      }
    },
    150,
    { leading: false }
  );

  _removeSearchedUser = (user, card, isSearch) => {
    this.props.dispatch(
      multiActions._removeUser(
        user,
        card,
        this.props.assignments,
        this.props.currentAction,
        isSearch
      )
    );
  };

  checkAllState() {
    let users =
      this.props.users ||
      (this.props.contentMultiaction && this.props.contentMultiaction[this.props.currentAction]) ||
      [];
    let userEmails = _.map(users, 'email') || [];
    let userIds = _.map(users, 'id') || [];
    let addedEmails = _.map(
      (this.props.contentMultiaction && this.props.contentMultiaction.actionWithUsers) || [],
      'email'
    );
    let addedIds = _.map(
      (this.props.contentMultiaction && this.props.contentMultiaction.actionWithUsers) || [],
      'id'
    );
    let pendingInviteEmails = this.state.currentGroup.pending
      ? _.map(this.state.currentGroup.pending, 'email')
      : [];
    let removedAssignEmails = _.map(
      (this.props.assignments &&
        this.props.assignments.assignedUsers &&
        this.props.assignments.assignedUsers.users) ||
        [],
      'email'
    );
    let removedShareIds = this.currentCard.usersWithAccess
      ? _.map(this.currentCard.usersWithAccess, 'id')
      : [];
    let hiddenInviteEmails = _.map(
      (this.state.currentGroup && this.state.currentGroup.members) || [],
      'email'
    );
    let allAssigned = [...addedEmails, ...removedAssignEmails];
    let allAdded = [...addedIds, ...removedShareIds];
    let allInvited = [...addedEmails, ...pendingInviteEmails, ...hiddenInviteEmails];
    let xorAssigned = _.xor(userEmails, allAssigned);
    let xorShared = _.xor(userIds, allAdded);
    let xorInvited = _.xor(userEmails, allInvited);
    if (this.props.currentAction === 'assign' && users.length) {
      this.setState({
        isCheckedAll:
          !(
            this.props.contentMultiaction &&
            this.props.contentMultiaction.removeUsersFromList &&
            this.props.contentMultiaction.removeUsersFromList.length
          ) && !(xorAssigned.length && _.intersection(userEmails, xorAssigned).length)
      });
    } else if (this.props.currentAction === 'share' && users.length) {
      this.setState({
        isCheckedAll:
          !(
            this.props.contentMultiaction &&
            this.props.contentMultiaction.removeUsersFromList &&
            this.props.contentMultiaction.removeUsersFromList.length
          ) && !(xorShared.length && _.intersection(userIds, xorShared).length)
      });
    } else {
      this.setState({
        isCheckedAll:
          userEmails.length && !(xorInvited.length && _.intersection(userEmails, xorInvited).length)
      });
    }
  }

  getShownUsers(usersNumber) {
    this.props.getShownUsers(usersNumber);
  }

  setUsersToShow(props) {
    let users =
      props.users ||
      (props.contentMultiaction && props.contentMultiaction[this.props.currentAction]) ||
      [];
    let myEmail = this.props.currentUser && this.props.currentUser.email;

    let usersNumber = !~_.findIndex(users, { email: myEmail }) ? users.length : users.length - 1;
    this.setState({ usersToShow: users, users: props.users || [] });
    this.getShownUsers(usersNumber);
  }

  render() {
    let customHeaders =
      this.state.customFieldsList && _.orderBy(this.state.customFieldsList, 'id', 'asc');
    customHeaders = _.filter(customHeaders, 'enableInFilters');
    let searchedUsers =
      (this.props.contentMultiaction && this.props.contentMultiaction.searchedUsers) || [];
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let contentReload =
      !!this.props.contentMultiaction.contentLoaded ||
      !!this.props.contentMultiaction.usersLoading ||
      (this.props.currentAction === 'invite' && !this.props.contentMultiaction.usersLoading);
    let containerWidth = (customHeaders.length && (customHeaders.length + 1) * 212 + 30) || 'auto';
    return (
      <div>
        <Scrollbars style={{ height: 340 }} className="ma-list-wrapper__outer">
          {!!this.state.usersToShow.length && (
            <div
              className="ma-row__header"
              onClick={e => {
                this._toggleAllCheckboxes(e);
              }}
            >
              <Checkbox
                label=""
                checked={this.state.isCheckedAll}
                style={this.styles.root}
                iconStyle={this.styles.icon}
                uncheckedIcon={<div className="unchecked" />}
              />
              <div className="ma-row-item ma-row-item__header">{tr('Name')}</div>
              {!!customHeaders.length &&
                customHeaders.map((header, index) => {
                  return (
                    <div className="ma-row-item ma-row-item__header" key={`custom-header-${index}`}>
                      {header.displayName}
                    </div>
                  );
                })}
            </div>
          )}
          <Scrollbars
            style={{ width: containerWidth, height: 280 }}
            id="ma-individual-list-content"
            className="ma-individual"
            onScrollFrame={this.handleScroll}
          >
            <div className="list-item-wrapper">
              {((this.props.currentAction === 'invite' && contentReload) ||
                (contentReload && this.currentCard)) &&
                this.state.usersToShow.map((user, index) => {
                  if (this.props.currentUser.id != user.id) {
                    let key = user.id || user.email;
                    return (
                      <ListItem
                        user={user}
                        key={`${key}-${index}`}
                        {...this.props}
                        currentAction={this.props.currentAction}
                        customFieldsContent={
                          this.state.userCustomFields && this.state.userCustomFields[user.id]
                        }
                        customFieldsHeader={customHeaders}
                        currentTab={this.props.currentTab}
                      />
                    );
                  }
                })}
            </div>

            {(this.props.contentMultiaction.usersLoading === undefined ||
              (!contentReload ||
                (this.props.currentAction !== 'invite' && !this.currentCard) ||
                this.props.contentMultiaction.usersLoading) ||
              this.state.loadMore) && (
              <div className="text-center">
                <Spinner />
              </div>
            )}
          </Scrollbars>
        </Scrollbars>

        {!!searchedUsers.length && (
          <div id="searched-users" className="search-clip-list">
            {searchedUsers.map(user => {
              let key = user.id ? user.id : user.email;
              return (
                <div className="clip-container" key={key}>
                  {user.avatarimages && user.avatarimages.tiny && (
                    <img
                      className="user-avatar"
                      src={(user.avatarimages && user.avatarimages.tiny) || defaultUserImage}
                    />
                  )}
                  <span className="clip-main-text">{user.name}</span>
                  {user.handle && <span className="clip-handle-text">({user.handle})</span>}
                  <IconButton
                    className="clip-remove-btn"
                    aria-label="remove user"
                    onClick={() => {
                      this._removeSearchedUser(
                        user,
                        this.props.contentMultiaction[this.props.card.id],
                        true
                      );
                    }}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

IndividualsView.propTypes = {
  currentUser: PropTypes.any,
  assignments: PropTypes.any,
  users: PropTypes.any,
  contentMultiaction: PropTypes.any,
  currentAction: PropTypes.string,
  card: PropTypes.any,
  groupsV2: PropTypes.any,
  currentTab: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    contentMultiaction: state.contentMultiaction.toJS(),
    groupsV2: state.groupsV2.toJS()
  };
}

export default connect(mapStoreStateToProps)(IndividualsView);
