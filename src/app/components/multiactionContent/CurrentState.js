import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import filter from 'lodash/filter';
import sortBy from 'lodash/sortBy';

import Checkbox from 'material-ui/Checkbox';

import HardwareKeyboardArrowDown from 'react-material-icons/icons/hardware/keyboard-arrow-down';
import HardwareKeyboardArrowRight from 'react-material-icons/icons/hardware/keyboard-arrow-right';

import * as multiActions from '../../actions/multiactionActions';
import CurrentStateSearch from '../../components/multiactionContent/CurrentStateSearch';

import { getCurrentlyAssigned } from 'edc-web-sdk/requests/assignments.v2';

import Spinner from '../common/spinner';
import ListItem from './listItem';
import { Permissions } from '../../utils/checkPermissions';

class CurrentState extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      icon: {
        width: '1rem',
        height: '1rem',
        marginRight: '0.625rem',
        marginTop: '0'
      },
      checkboxPosition: {
        display: 'inline-block',
        width: 'auto',
        verticalAlign: 'middle'
      }
    };
    this.state = {
      groupsCollapsed: false,
      usersCollapsed: false,
      dynamicCollapsed: false
    };
  }

  componentDidMount() {
    if (this.props.currentAction === 'assign' && !Permissions.has('ADMIN_ONLY')) {
      this.fetchCurrentlyAssigned();
    }
  }

  fetchCurrentlyAssigned = async () => {
    let payload = {
      content_id: this.props.card.id
    };
    if (!Permissions.has('ADMIN_ONLY')) {
      payload['assignor_id'] = this.props.currentUser.id;
    }
    await this.props.dispatch(multiActions._fetchAssigned(payload)).catch(err => {
      console.error(`Error in componentDidMount.getCurrentlyAssigned.func: ${err}`);
    });
  };

  collapseItems = target => {
    this.setState(prevState => {
      return { [`${target}Collapsed`]: !prevState[`${target}Collapsed`] };
    });
  };

  toggleAll = target => {
    let usersToDelete =
      (this.props.contentMultiaction && this.props.contentMultiaction.removeUsersFromList) || [];
    let groupsToDelete =
      (this.props.contentMultiaction && this.props.contentMultiaction.removeGroupsFromList) || [];
    let sharedWithUsers =
      (this.props.contentMultiaction &&
        this.props.contentMultiaction.currentCard &&
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard] &&
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard].usersWithAccess) ||
      [];
    let assignedToUsers =
      (this.props.contentMultiaction && this.props.contentMultiaction.allAssignedUsers) || [];
    let invitedUsers =
      (this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID] &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID].members) ||
      [];
    let users =
      this.props.currentAction === 'assign'
        ? assignedToUsers
        : this.props.currentAction === 'invite'
        ? invitedUsers
        : sharedWithUsers;
    let assignedToGroups =
      (this.props.contentMultiaction && this.props.contentMultiaction.allAssignedTeams) || [];
    let sharedWithGroups =
      (this.props.contentMultiaction &&
        this.props.contentMultiaction.currentCard &&
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard] &&
        this.props.contentMultiaction[this.props.contentMultiaction.currentCard].teams) ||
      [];
    let groups = this.props.currentAction === 'assign' ? assignedToGroups : sharedWithGroups;

    if (
      this.props.contentMultiaction.searchAssignedUserQuery &&
      this.props.contentMultiaction.searchAssignedUsers
    ) {
      assignedToUsers = this.props.contentMultiaction.searchAssignedUsers || [];
    }
    if (
      this.props.contentMultiaction.searchAssignedTeamQuery &&
      this.props.contentMultiaction.searchAssignedTeams
    ) {
      assignedToGroups = this.props.contentMultiaction.searchAssignedTeams || [];
    }

    if (target === 'users') {
      usersToDelete.length
        ? this.props.dispatch(multiActions._clearRemoveUsersList(this.props.currentAction))
        : this.props.dispatch(multiActions._fillRemoveUsersList(users));
    } else {
      groupsToDelete.length
        ? this.props.dispatch(multiActions._clearRemoveGroupsList(this.props.currentAction))
        : this.props.dispatch(multiActions._fillRemoveGroupsList(groups));
    }
  };

  render() {
    let contents = this.props.contentMultiaction || {};
    let card = this.props.card || {};

    let sharedWithUsers =
      (contents[contents.currentCard] && contents[contents.currentCard].usersWithAccess) || [];

    let sharedWithGroups =
      (contents[contents.currentCard] && contents[contents.currentCard].teams) || [];

    let assignedToGroups =
      (contents[card.id] && contents[card.id].assignment && contents[card.id].assignment.teams) ||
      [];

    let assignedToUsers =
      (this.props.assignments &&
        this.props.assignments.assignedUsers &&
        this.props.assignments.assignedUsers.users) ||
      [];

    let invitedUsers =
      (this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID] &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID].members) ||
      [];

    let userList =
      this.props.currentAction === 'share'
        ? sharedWithUsers
        : this.props.currentAction === 'assign'
        ? assignedToUsers.filter(user => this.props.currentUser.id != user.id)
        : invitedUsers;

    let groupList = this.props.currentAction === 'share' ? sharedWithGroups : assignedToGroups;
    groupList = filter(groupList, group => {
      return !!group.id;
    });

    if (this.props.currentAction === 'assign' && Permissions.has('ADMIN_ONLY')) {
      userList = contents.allAssignedUsers || [];
      groupList = contents.allAssignedTeams || [];
    }

    // handle result based on search
    if (
      this.props.currentAction === 'assign' &&
      (contents.searchAssignedUserQuery || contents.searchAssignedTeamQuery)
    ) {
      if (contents.searchAssignedUsers && contents.searchAssignedUsers.length > 0) {
        userList = contents.searchAssignedUsers || [];
      }
      if (contents.searchAssignedTeams && contents.searchAssignedTeams.length > 0) {
        groupList = contents.searchAssignedTeams || [];
      }
    }

    if (this.props.currentAction === 'share' && contents.searchSharedQuery) {
      if (contents.searchSharedUsers && contents.searchSharedUsers.length > 0) {
        userList = contents.searchSharedUsers || [];
      }
      if (contents.searchSharedUsers && contents.searchSharedTeams.length > 0) {
        groupList = contents.searchSharedTeams || [];
      }
    }

    if (userList.length > 0) {
      if (this.props.currentAction === 'share') {
        userList = sortBy(userList, [user => user.fullName && user.fullName.toLowerCase()]);
      } else if (this.props.currentAction === 'assign') {
        userList = sortBy(userList, [user => user.firstName && user.firstName.toLowerCase()]);
      }
    }

    let filterContent = (this.props.modal && this.props.modal.filter) || [];
    filterContent = filter(filterContent, item => {
      return item.id !== -1;
    });

    let message =
      this.props.currentAction === 'share'
        ? 'This content is not shared with anyone.'
        : this.props.currentAction === 'assign'
        ? 'This content is not assigned to anyone.'
        : 'No one had been invited yet.';

    let isCheckedAllUsers = !(
      contents &&
      contents.removeUsersFromList &&
      contents.removeUsersFromList.length
    );
    let isCheckedAllGroups = !(
      contents &&
      contents.removeGroupsFromList &&
      contents.removeGroupsFromList.length
    );

    let dataExists =
      (contents && contents.currentCard && contents[contents.currentCard]) ||
      (this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID]);

    return (
      <div>
        {dataExists && (
          <div className="ma-multiaction-list-container">
            {!dataExists && !sharedWithGroups.length && !sharedWithUsers.length && (
              <div className="text-center">
                <Spinner />
              </div>
            )}
            {!!userList.length && (
              <div className="list-container">
                <div className="title">
                  <div className="collapse-arrow" onClick={this.collapseItems.bind(this, 'users')}>
                    {!this.state.usersCollapsed ? (
                      <HardwareKeyboardArrowDown />
                    ) : (
                      <HardwareKeyboardArrowRight />
                    )}
                  </div>
                  <Checkbox
                    label=""
                    onCheck={this.toggleAll.bind(this, 'users')}
                    className="checkbox"
                    checked={isCheckedAllUsers}
                    style={this.styles.checkboxPosition}
                    iconStyle={this.styles.icon}
                    disabled={this.props.currentAction === 'invite'}
                  />
                  <span onClick={this.collapseItems.bind(this, 'users')}>{tr('Individuals')}</span>
                </div>
                {this.props.currentAction === 'assign' && (
                  <CurrentStateSearch
                    {...this.props}
                    activeTab={this.state.activeTab}
                    currentAction={this.props.currentAction}
                    searchType={'user'}
                  />
                )}
                <div className="list" style={{ height: this.state.usersCollapsed ? '0' : 'auto' }}>
                  {userList.map(user => {
                    let key = user.id ? user.id : user.assignmentId;
                    return (
                      <ListItem
                        user={user}
                        key={`user-${key}`}
                        currentAction={this.props.currentAction}
                        {...this.props}
                      />
                    );
                  })}
                </div>
              </div>
            )}
            {!!groupList.length && this.props.currentAction !== 'invite' && (
              <div className="list-container">
                <div className="title">
                  <div className="collapse-arrow" onClick={this.collapseItems.bind(this, 'groups')}>
                    {!this.state.groupsCollapsed ? (
                      <HardwareKeyboardArrowDown />
                    ) : (
                      <HardwareKeyboardArrowRight />
                    )}
                  </div>
                  <Checkbox
                    label=""
                    onCheck={this.toggleAll.bind(this, 'groups')}
                    className="checkbox"
                    checked={isCheckedAllGroups}
                    style={this.styles.checkboxPosition}
                    iconStyle={this.styles.icon}
                  />
                  <span onClick={this.collapseItems.bind(this, 'groups')}>{tr('Groups')}</span>
                </div>
                {this.props.currentAction === 'assign' && (
                  <CurrentStateSearch
                    {...this.props}
                    activeTab={this.state.activeTab}
                    currentAction={this.props.currentAction}
                    searchType={'team'}
                  />
                )}
                <div className="list" style={{ height: this.state.groupsCollapsed ? '0' : 'auto' }}>
                  {groupList.map(group => {
                    return (
                      <ListItem
                        group={group}
                        key={`group-${group.id}`}
                        currentAction={this.props.currentAction}
                        {...this.props}
                      />
                    );
                  })}
                </div>
              </div>
            )}
            {!filterContent.length && (!!groupList.length || !!userList.length) && (
              <div className="ma-remark ma-remark__groups">
                <div>
                  *
                  {tr(
                    `Removing users ${
                      this.props.currentAction !== 'invite' ? 'or groups ' : ''
                    }will ${
                      this.props.currentAction === 'assign'
                        ? 'unassign content'
                        : this.props.currentAction === 'share'
                        ? 'unshare content'
                        : 'remove them from the group'
                    }`
                  )}
                  .
                </div>
              </div>
            )}
            {!!filterContent.length && (
              <div className="list-container">
                <div
                  className="preview-link"
                  onClick={() => {
                    this.props.preview();
                  }}
                >
                  {tr('Preview Data')}
                </div>
                <div className="title">
                  <div
                    className="collapse-arrow"
                    onClick={this.collapseItems.bind(this, 'dynamic')}
                  >
                    {!this.state.dynamicCollapsed ? (
                      <HardwareKeyboardArrowDown />
                    ) : (
                      <HardwareKeyboardArrowRight />
                    )}
                  </div>
                  <Checkbox
                    label=""
                    onCheck={this.toggleAll.bind(this, 'dynamic')}
                    className="checkbox"
                    checked={true}
                    disabled={true}
                    style={this.styles.checkboxPosition}
                    iconStyle={this.styles.icon}
                  />
                  <span onClick={this.collapseItems.bind(this, 'dynamic')}>
                    {tr('Dynamic Selection')}
                  </span>
                </div>
                <div
                  className="list"
                  style={{ height: this.state.dynamicCollapsed ? '0' : 'auto' }}
                >
                  {filterContent.map(item => {
                    return (
                      <ListItem
                        dynamic={item}
                        key={`dynamic-${item.id}`}
                        currentAction={this.props.currentAction}
                        {...this.props}
                      />
                    );
                  })}
                </div>
                <div className="ma-remark">
                  <div>*{tr('Users data will update every 24 hours')}.</div>
                  <div>
                    {tr('To update it now, press')}{' '}
                    <span className="remark-action" onClick={() => {}}>
                      {tr('refresh')}
                    </span>
                  </div>
                </div>
              </div>
            )}
            {!userList.length && !groupList.length && (
              <div className="text-center empty-msg">{tr(message)}</div>
            )}
          </div>
        )}
      </div>
    );
  }
}

CurrentState.propTypes = {
  currentUser: PropTypes.any,
  contentMultiaction: PropTypes.any,
  currentAction: PropTypes.any,
  card: PropTypes.any,
  modal: PropTypes.any,
  dispatch: PropTypes.any,
  assignments: PropTypes.any,
  preview: PropTypes.any,
  groupsV2: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    contentMultiaction: state.contentMultiaction.toJS(),
    assignments: state.assignments.toJS(),
    modal: state.modal.toJS()
  };
}

export default connect(mapStoreStateToProps)(CurrentState);
