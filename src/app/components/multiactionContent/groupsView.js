import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import uniqBy from 'lodash/uniqBy';
import differenceBy from 'lodash/differenceBy';
import throttle from 'lodash/throttle';
import isEmpty from 'lodash/isEmpty';

import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import Checkbox from 'material-ui/Checkbox';

import Spinner from '../common/spinner';
import ListItem from './listItem';
import * as multiActions from '../../actions/multiactionActions';
import { Permissions } from '../../utils/checkPermissions';

class GroupsView extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      root: {
        display: 'inline-block',
        width: '1.25rem',
        height: '2.25rem',
        verticalAlign: 'middle',
        overflow: 'visible'
      },
      icon: {
        width: '1.125rem',
        height: '1.125rem',
        marginRight: '0.3125rem',
        marginTop: '0.25rem',
        marginLeft: '0'
      }
    };
    this.state = {
      loadMore: false,
      isCheckedAll: false,
      currentTab: props.currentTab
    };
  }

  componentDidMount() {
    document.getElementById('ma-group-list-content') &&
      document
        .getElementById('ma-group-list-content')
        .addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document
      .getElementById('ma-group-list-content')
      .removeEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let oldGroupList =
      (this.props.contentMultiaction && this.props.contentMultiaction.groups) || [];
    let newGroupList = (nextProps.contentMultiaction && nextProps.contentMultiaction.groups) || [];

    let currentlyAssigned =
      (nextProps.contentMultiaction &&
        nextProps.card &&
        nextProps.card.id &&
        nextProps.contentMultiaction[nextProps.card.id] &&
        nextProps.contentMultiaction[nextProps.card.id].assignment &&
        nextProps.contentMultiaction[nextProps.card.id].assignment.teams) ||
      [];
    let currentlyShared =
      (nextProps.contentMultiaction &&
        nextProps.contentMultiaction.currentCard &&
        nextProps.contentMultiaction[nextProps.contentMultiaction.currentCard] &&
        nextProps.contentMultiaction[nextProps.contentMultiaction.currentCard].teams) ||
      [];
    let newlyAdded =
      (nextProps.contentMultiaction && nextProps.contentMultiaction.actionWithGroups) || [];
    let addedGroups =
      this.props.currentAction === 'assign'
        ? uniqBy([...currentlyAssigned, ...newlyAdded], 'id')
        : uniqBy([...currentlyShared, ...newlyAdded], 'id');
    if (oldGroupList.length !== newGroupList.length) {
      this.getShownGroups(nextProps);
    }
    if (addedGroups.length !== newGroupList.length) {
      this.checkAllGroupsState(nextProps);
    }
  }

  handleScroll = throttle(
    e => {
      let groups = (this.props.contentMultiaction && this.props.contentMultiaction.groups) || [];
      let groupTotal =
        (this.props.contentMultiaction && this.props.contentMultiaction.totalGroups) || 0;
      let payload = { limit: 15, offset: groups.length };

      if (!(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) {
        if (this.props.currentAction === 'assign') {
          payload['role'] = 'admin';
        }
        if (this.props.currentAction === 'share') {
          payload['writables'] = true;
        }
      }

      if (
        groupTotal > groups.length &&
        e.target.clientHeight + e.target.scrollTop === e.target.scrollHeight
      ) {
        this.setState({ loadMore: true });
        if (this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) {
          this.props
            .dispatch(multiActions._fetchOrgGroups(payload, this.props.currentAction))
            .then(() => {
              this.setState({ loadMore: false });
            })
            .catch(() => {
              this.setState({ loadMore: false });
            });
        } else {
          this.props
            .dispatch(multiActions._fetchGroups(payload, this.props.currentAction))
            .then(() => {
              this.setState({ loadMore: false });
            })
            .catch(() => {
              this.setState({ loadMore: false });
            });
        }
      }
    },
    150,
    { leading: false }
  );

  _removeSearchedGroup = (user, card, isSearch) => {
    this.props.dispatch(multiActions._removeGroup(user, card, this.props.currentAction, isSearch));
  };

  _toggleAllGroupCheckboxes = () => {
    let card = this.props.card;
    let contentMultiaction = this.props.contentMultiaction;
    let groups = (contentMultiaction && contentMultiaction.groups) || [];
    let assignedGroups =
      (contentMultiaction &&
        card &&
        card.id &&
        contentMultiaction[card.id] &&
        contentMultiaction[card.id].assignment &&
        contentMultiaction[card.id].assignment.teams) ||
      [];
    let sharedWithGroups =
      (contentMultiaction &&
        contentMultiaction.currentCard &&
        contentMultiaction[contentMultiaction.currentCard] &&
        contentMultiaction[contentMultiaction.currentCard].teams) ||
      [];
    if (this.props.currentAction === 'assign') {
      groups = differenceBy(groups, assignedGroups, 'id');
    } else {
      groups = differenceBy(groups, sharedWithGroups, 'id');
    }
    this.props.dispatch(
      multiActions.toggleGroups(this.state.isCheckedAll, groups, this.props.currentAction)
    );
    this.checkAllGroupsState(this.props);
  };

  checkAllGroupsState(props) {
    let card = props.card;
    let contentMultiaction = props.contentMultiaction;
    let groups = (contentMultiaction && contentMultiaction.groups) || [];
    let assignedGroups =
      (contentMultiaction &&
        card &&
        card.id &&
        contentMultiaction[card.id] &&
        contentMultiaction[card.id].assignment &&
        contentMultiaction[card.id].assignment.teams) ||
      [];
    let sharedWithGroups =
      (contentMultiaction &&
        contentMultiaction.currentCard &&
        contentMultiaction[contentMultiaction.currentCard] &&
        contentMultiaction[contentMultiaction.currentCard].teams) ||
      [];
    if (this.props.currentAction === 'assign') {
      groups = differenceBy(groups, assignedGroups, 'id');
    } else {
      groups = differenceBy(groups, sharedWithGroups, 'id');
    }
    let checkedGroups = (contentMultiaction && contentMultiaction.actionWithGroups) || [];
    this.setState({ isCheckedAll: !differenceBy(groups, checkedGroups, 'id').length });
  }

  getShownGroups(props) {
    let groups = (props.contentMultiaction && props.contentMultiaction.groups) || [];
    if (this.props.currentAction === 'assign') {
      let currentlyAssigned =
        (props.contentMultiaction &&
          this.props.card &&
          props.card.id &&
          props.contentMultiaction[props.card.id] &&
          props.contentMultiaction[props.card.id].assignment &&
          props.contentMultiaction[props.card.id].assignment.teams) ||
        [];
      groups = differenceBy(groups, currentlyAssigned, 'id') || [];
    }
    this.props.getShownGroups(groups.length);
  }

  render() {
    let groupKey = this.props.currentAction + 'Group';
    let groups =
      (this.props.contentMultiaction &&
        this.props.contentMultiaction[groupKey] &&
        this.props.contentMultiaction[groupKey]) ||
      [];
    let content = this.props.contentMultiaction[this.props.contentMultiaction.currentCard];
    let searchedGroups =
      (this.props.contentMultiaction && this.props.contentMultiaction.searchedGroups) || [];
    let contentReload = !!this.props.contentMultiaction.contentLoaded;

    return (
      <div>
        {!!groups.length && (
          <div
            className="ma-row__header"
            onClick={e => {
              this._toggleAllGroupCheckboxes(e);
            }}
          >
            <Checkbox
              label=""
              checked={this.state.isCheckedAll}
              style={this.styles.root}
              iconStyle={this.styles.icon}
              uncheckedIcon={<div className="unchecked" />}
            />
            <div className="ma-row-item ma-row-item__header">{tr('Name')}</div>
          </div>
        )}
        <div id="ma-group-list-content" className="ma-individual">
          {contentReload &&
            content &&
            groups.map(group => {
              return (
                <ListItem
                  group={group}
                  key={group.id}
                  {...this.props}
                  currentTab={this.state.currentTab}
                />
              );
            })}
          {((this.props.contentMultiaction.groupsLoading === undefined
            ? !contentReload || !content || true
            : !contentReload || !content || this.props.contentMultiaction.groupsLoading) ||
            this.state.loadMore) && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
        </div>
        {!!searchedGroups.length && (
          <div id="searched-groups">
            {searchedGroups.map(group => {
              return (
                <div className="clip-container" key={group.id}>
                  <span className="clip-main-text clip-group-name">{group.name}</span>
                  <IconButton
                    className="clip-remove-btn"
                    aria-label="remove group"
                    onClick={() => {
                      this._removeSearchedGroup(
                        group,
                        this.props.contentMultiaction[this.props.card.id],
                        true
                      );
                    }}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

GroupsView.propTypes = {
  currentUser: PropTypes.any,
  currentAction: PropTypes.string,
  contentMultiaction: PropTypes.any,
  card: PropTypes.any,
  getShownGroups: PropTypes.func,
  currentTab: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    contentMultiaction: state.contentMultiaction.toJS()
  };
}

export default connect(mapStoreStateToProps)(GroupsView);
