import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import { Menu, AsyncTypeahead } from 'react-bootstrap-typeahead';

import { fetchGroups, fetchOrgTeams } from 'edc-web-sdk/requests/groups.v2';
import { searchUsers } from 'edc-web-sdk/requests/users.v2';
import { getCurrentlyAssigned } from 'edc-web-sdk/requests/assignments.v2';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';

import { _addGroup, _addUser } from '../../actions/multiactionActions';
import { Permissions } from '../../utils/checkPermissions';

class MultiActionSearchBox extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      searchIcon: {
        position: 'absolute',
        right: '0.2rem',
        top: '0.2rem',
        height: '1.2rem',
        width: '1.2rem',
        zIndex: '10',
        color: '#6f708b'
      }
    };
    this.state = {
      activeTab: 'Individuals',
      users: [],
      totalUsers: 0,
      groups: [],
      totalGroups: 0,
      searchBoxValue: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    let activeTab = nextProps.activeTab;
    this._searchBox.getInstance().clear();
    this.setState({
      activeTab
    });
  }

  _handleResultsClick = result => {
    this.state.activeTab === 'Individuals'
      ? this.props.dispatch(
          _addUser(
            result,
            this.props.card,
            this.props.assignments,
            this.props.currentAction,
            true,
            this.props.groupsV2
          )
        )
      : this.state.activeTab === 'Groups' &&
        this.props.dispatch(_addGroup(result, this.props.card, this.props.currentAction, true));

    this._searchBox.getInstance().clear();
  };

  _renderMenu = (results, menuProps) => {
    const menuData = (results || []).map(data => {
      return [
        <li
          onClick={e => {
            this._handleResultsClick(data);
          }}
        >
          <a>
            <div className="user-search-name" style={{ fontSize: '0.75rem' }}>
              {data.name}
            </div>
            {data.handle && (
              <span style={{ fontSize: '0.6875rem', margin: '0 0 0 0.3125rem' }}>
                {' '}
                ({data.handle})
              </span>
            )}
          </a>
        </li>
      ];
    });
    return <Menu {...menuProps}>{menuData}</Menu>;
  };

  _handleSearch = query => {
    let payload = { limit: 15, offset: 0, q: query };
    if (this.state.activeTab === 'Individuals') {
      if (
        !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
        this.props.currentAction === 'assign'
      ) {
        payload['only_from_my_teams'] = true;
      }
      searchUsers(payload)
        .then(response => {
          let totalUsers = response.total;

          let { shareWithUsers, searchedUsers } = this.props.contentMultiaction;

          let selectedUsers;

          switch (this.props.currentAction) {
            case 'assign':
              let assignedToUsers =
                (this.props.assignments &&
                  this.props.assignments.assignedUsers &&
                  this.props.assignments.assignedUsers.users) ||
                [];
              selectedUsers = [
                ...(shareWithUsers || []),
                ...(searchedUsers || []),
                ...assignedToUsers
              ];
              break;
            case 'share':
              let alreadyShared =
                (this.props.contentMultiaction[this.props.card.id] &&
                  this.props.contentMultiaction[this.props.card.id].usersWithAccess) ||
                [];
              selectedUsers = [
                ...(shareWithUsers || []),
                ...(searchedUsers || []),
                ...alreadyShared
              ];
              break;
            case 'invite':
              let alreadyInvited =
                this.props.groupsV2 &&
                this.props.groupsV2[this.props.groupsV2.currentGroupID].pending.map(user => {
                  return { id: user.userId };
                });
              let alreadyInGroup =
                this.props.groupsV2 &&
                this.props.groupsV2[this.props.groupsV2.currentGroupID].members;
              selectedUsers = [
                ...(shareWithUsers || []),
                ...(searchedUsers || []),
                ...alreadyInvited,
                ...alreadyInGroup
              ];
              break;
            default:
              break;
          }

          let users = this._searchFilterForExistingEntries(selectedUsers, response.users);
          this.setState({ users, totalUsers });
        })
        .catch(e => {});
    } else if (this.state.activeTab === 'Groups') {
      if (!(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY'))) {
        delete payload.q;
        payload['search_term'] = query;
        if (this.props.currentAction === 'assign') {
          payload['role'] = 'admin';
        } else {
          payload['writables'] = true;
        }
        fetchGroups(payload)
          .then(response => {
            let totalGroups = response.total;

            let { shareWithGroups, searchedGroups } = this.props.contentMultiaction;
            let selectedGroups;

            switch (this.props.currentAction) {
              case 'assign':
                let assignedToGroups =
                  (this.props.contentMultiaction &&
                    this.props.contentMultiaction.allAssignedTeams) ||
                  [];
                selectedGroups = [
                  ...(shareWithGroups || []),
                  ...(searchedGroups || []),
                  ...assignedToGroups
                ];
                break;
              case 'share':
                let alreadyShared =
                  (this.props.contentMultiaction[this.props.card.id] &&
                    this.props.contentMultiaction[this.props.card.id].teams) ||
                  [];
                selectedGroups = [
                  ...(shareWithGroups || []),
                  ...(searchedGroups || []),
                  ...alreadyShared
                ];
                break;
              default:
                break;
            }

            let groups = this._searchFilterForExistingEntries(selectedGroups, response.teams);

            this.setState({ groups, totalGroups });
          })
          .catch(e => {
            console.error(`Error in _handleSearch.fetchGroups.func : ${e}`);
          });
      } else {
        fetchOrgTeams(payload)
          .then(response => {
            let totalGroups = response.total;

            let { shareWithGroups, searchedGroups } = this.props.contentMultiaction;
            let selectedGroups;

            switch (this.props.currentAction) {
              case 'assign':
                let assignedToGroups =
                  (this.props.contentMultiaction &&
                    this.props.contentMultiaction.allAssignedTeams) ||
                  [];
                selectedGroups = [
                  ...(shareWithGroups || []),
                  ...(searchedGroups || []),
                  ...assignedToGroups
                ];
                break;
              case 'share':
                let alreadyShared =
                  (this.props.contentMultiaction[this.props.card.id] &&
                    this.props.contentMultiaction[this.props.card.id].teams) ||
                  [];
                selectedGroups = [
                  ...(shareWithGroups || []),
                  ...(searchedGroups || []),
                  ...alreadyShared
                ];
                break;
              default:
                break;
            }

            let groups = this._searchFilterForExistingEntries(selectedGroups, response.teams);

            this.setState({ groups, totalGroups });
          })
          .catch(e => {
            console.error(`Error in _handleSearch.fetchOrgTeams.func : ${e}`);
          });
      }
    }
    if (this.props.searchType) {
      payload = {
        limit: 15,
        offset: 0,
        q: query,
        search_type: this.props.searchType,
        content_id: this.props.card.id
      };
      if (!Permissions.has('ADMIN_ONLY')) {
        payload['assignor_id'] = this.props.currentUser.id;
      }
      getCurrentlyAssigned(payload)
        .then(response => {
          if (this.props.searchType === 'user') {
            this.setState({ users: response.body.users, totalUsers: response.body.totalUsers });
          } else if (this.props.searchType === 'team') {
            this.setState({ groups: response.body.teams, totalGroups: response.body.totalTeams });
          }
        })
        .catch(e => {
          console.error(`Error in _handleSearch.getCurrentlyAssigned.func : ${e}`);
        });
    }
  };

  _searchFilterForExistingEntries = (existingEntries, searchResults) => {
    existingEntries.map(existingEntry => {
      let index = findIndex(searchResults, searchResult => {
        return searchResult.id == existingEntry.id;
      });
      if (index >= 0) {
        searchResults.splice(index, 1);
      }
      if (this.state.activeTab === 'Individuals') {
        index = findIndex(searchResults, ['id', parseInt(this.props.currentUser.id)]);
        index >= 0 && searchResults.splice(index, 1);
      }
    });

    return searchResults;
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    let dataSourse =
      this.state.activeTab === 'Individuals'
        ? this.state.users
        : this.state.activeTab === 'Groups'
        ? this.state.groups
        : [];

    return (
      <div>
        <div className="user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={dataSourse}
            placeholder={tr('Search')}
            onSearch={q => {
              this._handleSearch(q);
            }}
            submitFormOnEnter={false}
            ref={ref => (this._searchBox = ref)}
            value={this.state.searchBoxValue}
          />
          <SearchIcon style={this.styles.searchIcon} />
        </div>
      </div>
    );
  }
}

MultiActionSearchBox.propTypes = {
  currentUser: PropTypes.any,
  contentMultiaction: PropTypes.any,
  card: PropTypes.any,
  activeTab: PropTypes.any,
  assignments: PropTypes.any,
  currentAction: PropTypes.string,
  searchType: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    contentMultiaction: state.contentMultiaction.toJS(),
    assignments: state.assignments.toJS()
  };
}

export default connect(mapStoreStateToProps)(MultiActionSearchBox);
