import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as multiactionAction from '../../actions/multiactionActions';
import { Permissions } from '../../utils/checkPermissions';

class ListItem extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isChecked: false,
      isDisabled: false,
      disableItem: true
    };
    this.styles = {
      root: {
        display: 'inline-block',
        height: '2.25rem',
        minWidth: '100%',
        width: 'auto'
      },
      label: {
        margin: '0.3125rem 0 0.3125rem 0',
        fontSize: '0.75rem',
        letterSpacing: '0.025rem',
        fontWeight: 300
      },
      icon: {
        width: '1.125rem',
        height: '1.125rem',
        marginRight: '0.1875rem',
        marginTop: '0.25rem',
        marginLeft: '0'
      }
    };
  }

  componentDidMount() {
    this._checkboxSetup(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this._checkboxSetup(nextProps);
  }

  _checkboxSetup = compProps => {
    let card = compProps.card || {};
    let groups = compProps.groupsV2 || {};

    let contents = compProps.contentMultiaction || {};

    let sharedWithUsers = (contents[card.id] && contents[card.id].usersWithAccess) || [];

    let sharedWithGroups = (contents[card.id] && contents[card.id].teams) || [];

    let assignedToTeams = contents.allAssignedTeams || [];

    let assignedToUsers = contents.allAssignedUsers || [];

    // assignedToUsers = contents.allAssignedUsers;
    // assignedToTeams = contents.allAssignedTeams;

    let invitedUsers =
      (groups.currentGroupID &&
        groups[groups.currentGroupID] &&
        groups[groups.currentGroupID].members) ||
      [];

    let addToUsers = contents.actionWithUsers || [];
    let addToGroups = contents.actionWithGroups || [];

    let removeUsers = contents.removeUsersFromList || [];
    let removeGroups = contents.removeGroupsFromList || [];
    let disableItem = false;

    if (compProps.user && compProps.user.id) {
      let checkedUser =
        compProps.currentAction === 'assign'
          ? _.some([...assignedToUsers, ...addToUsers], ['id', compProps.user.id]) &&
            !_.some(removeUsers, ['id', compProps.user.id])
          : compProps.currentAction === 'share'
          ? (_.some([...sharedWithUsers, ...addToUsers], ['id', compProps.user.id]) &&
              !_.some(removeUsers, ['id', compProps.user.id])) ||
            compProps.isCurrentlyShared
          : _.some([...invitedUsers, ...addToUsers], ['id', compProps.user.id]) &&
            !_.some(removeUsers, ['id', compProps.user.id]);

      if (
        compProps.currentAction === 'share' &&
        sharedWithUsers.some(o => o.id === compProps.user.id)
      ) {
        disableItem = true;
      }

      if (
        compProps.currentAction === 'assign' &&
        assignedToUsers.some(o => o.id === compProps.user.id)
      ) {
        disableItem = true;
      }
      this.setState({
        isChecked: checkedUser,
        isDisabled: disableItem
      });
    } else if (compProps.group && compProps.group.id) {
      let checkedGroup =
        compProps.currentAction === 'assign'
          ? _.some([...assignedToTeams, ...(addToGroups || [])], ['id', compProps.group.id]) &&
            !_.some(removeGroups, ['id', compProps.group.id])
          : (_.some([...sharedWithGroups, ...(addToGroups || [])], ['id', compProps.group.id]) &&
              !_.some(removeGroups, ['id', compProps.group.id])) ||
            compProps.isCurrentlyShared;

      if (
        compProps.currentAction === 'share' &&
        sharedWithGroups.some(o => o.id === compProps.group.id)
      ) {
        disableItem = true;
      }

      if (
        compProps.currentAction === 'assign' &&
        assignedToTeams.some(o => o.id === compProps.group.id)
      ) {
        disableItem = true;
      }
      this.setState({
        isChecked: checkedGroup,
        isDisabled: disableItem
      });
    } else if (compProps.dynamic && compProps.dynamic.id.toString()) {
      this.setState({
        isChecked: true
      });
    }
  };

  _toggleCheckbox = e => {
    let user = this.props.user || {};
    let group = this.props.group || {};
    let isChecked = e.target.checked;
    if (user.id) {
      isChecked
        ? this.props.dispatch(
            multiactionAction._addUser(
              user,
              this.props.card,
              this.props.assignments,
              this.props.currentAction,
              false,
              this.props.groupsV2
            )
          )
        : this.props.dispatch(
            multiactionAction._removeUser(
              user,
              this.props.card,
              this.props.assignments,
              this.props.currentAction,
              false,
              this.props.groupsV2
            )
          );
    } else if (group.id) {
      isChecked
        ? this.props.dispatch(
            multiactionAction._addGroup(group, this.props.card, this.props.currentAction)
          )
        : this.props.dispatch(
            multiactionAction._removeGroup(group, this.props.card, this.props.currentAction)
          );
    }
    this.setState({ isChecked, disableItem: false });
  };

  itemView = props => {
    let user = props.user || {};
    let group = props.group || {};
    let dynamic = props.dynamic || {};
    let customHeaders = this.props.customFieldsHeader || [];
    let customFieldsArray = [];
    if (this.props.customFieldsContent) {
      customFieldsArray = Object.keys(this.props.customFieldsContent).map(i => {
        return { cfId: +i, cfContent: this.props.customFieldsContent[i] };
      });
    }
    if (customHeaders.length) {
      _.forEach(customHeaders, header => {
        let index = _.findIndex(customFieldsArray, ['cfId', header.id]);
        if (!~index) {
          customFieldsArray.push({ cfId: header.id, cfContent: '-', allowed: true });
        } else {
          customFieldsArray[index].allowed = true;
        }
      });
    }
    customFieldsArray = _.sortBy(_.filter(customFieldsArray, 'allowed'), 'cfId');

    if (user.id || user.assignmentId) {
      return (
        <a style={{ color: '#454560' }} className="checkbox-label">
          <div className="item-name item-name_user item-name_user__custom-fields">
            {user.name || user.fullName || user.firstName + ' ' + user.lastName}
            {user.handle && <span className="user-handle"> ({user.handle})</span>}
          </div>
          {!!customFieldsArray.length &&
            customFieldsArray.map(item => {
              return (
                <div
                  key={item.id}
                  className="item-name item-name_user item-name_user__custom-fields"
                >
                  {item.cfContent}
                </div>
              );
            })}
        </a>
      );
    } else if (group.id) {
      return (
        <a style={{ color: '#454560' }} className="checkbox-label">
          <div className="item-name">{group.name || group.label || ''}</div>
        </a>
      );
    } else if (dynamic && dynamic.options && dynamic.id.toString()) {
      let optionList = _.filter(dynamic.options, item => {
        return item.checked;
      });
      optionList = _.map(optionList, 'option');
      return (
        <a style={{ color: '#454560' }} className="checkbox-label">
          <div className="item-name">
            {dynamic.type}: {optionList.join(', ')}
          </div>
        </a>
      );
    }
  };

  render() {
    let checkboxLabel = this.itemView(this.props);
    let groupPending =
      (this.props.groupsV2 &&
        this.props.groupsV2.currentGroupID &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID] &&
        this.props.groupsV2[this.props.groupsV2.currentGroupID].pending) ||
      [];
    // AS we do not want to allow un-inviting of users from this modal under any circumstances
    let disabled =
      (this.state.isDisabled && this.state.isChecked
        ? this.props.currentTab === 'Individuals' || this.props.currentTab === 'Groups'
          ? true
          : false
        : false) ||
      (this.props.currentAction === 'invite' && this.props.currentTab !== 'Individuals');
    return (
      <div className="overflow-wrapper__outer">
        <Checkbox
          label={checkboxLabel}
          checked={this.state.isChecked}
          onCheck={e => {
            this._toggleCheckbox(e);
          }}
          style={this.styles.root}
          labelStyle={this.styles.label}
          iconStyle={this.styles.icon}
          disabled={disabled}
          uncheckedIcon={<div className="unchecked" />}
        />
      </div>
    );
  }
}

ListItem.propTypes = {
  user: PropTypes.any,
  group: PropTypes.any,
  dynamic: PropTypes.any,
  contentMultiaction: PropTypes.any,
  isCurrentlyShared: PropTypes.bool,
  card: PropTypes.object,
  assignments: PropTypes.object,
  currentAction: PropTypes.string,
  groupsV2: PropTypes.object,
  customFieldsContent: PropTypes.any,
  customFieldsHeader: PropTypes.any,
  currentTab: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    contentMultiaction: state.contentMultiaction.toJS(),
    assignments: state.assignments.toJS()
  };
}

export default connect(mapStoreStateToProps)(ListItem);
