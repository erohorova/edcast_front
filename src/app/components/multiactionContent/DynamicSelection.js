import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import findIndex from 'lodash/findIndex';
import throttle from 'lodash/throttle';
import includes from 'lodash/includes';
import { tr } from 'edc-web-sdk/helpers/translations';
import { Scrollbars } from 'react-custom-scrollbars';
import Spinner from '../common/spinner';

import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import ContentClear from 'react-material-icons/icons/content/clear';

import Paper from 'edc-web-sdk/components/Paper';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import { Permissions } from '../../utils/checkPermissions';

import { getUsersFields, getFieldValues, searchUsers } from 'edc-web-sdk/requests/users.v2';

class MultiActionDynamicSelections extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      filterArr: [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }],
      anchorEl: {},
      filterAdd: false,
      currentFilter: {},
      filterList: [],
      searchText: '',
      filteredOptions: [],
      valuesPending: false,
      selectedFieldsPending: false,
      scrollHeightField: 0
    };
    this.styles = {
      removeSpan: {
        border: '0.0625rem solid #fff',
        paddingLeft: '0.1875rem',
        paddingRight: '0.1875rem',
        display: 'inline-block',
        marginLeft: '0.3125rem'
      },
      searchContainer: {
        border: '1px solid #d6d6e1'
      },
      searchIcon: {
        position: 'absolute',
        bottom: '0',
        width: '1rem',
        height: '1rem',
        fill: colors.silverSand
      },
      hr: {
        margin: '0.5rem 0',
        border: 'solid 0.0625rem #f0f0f5'
      },
      addButton: {
        width: 'auto',
        minHeight: '1.5rem',
        backgroundColor: '#acadc1',
        fontSize: '0.75rem',
        color: '#fff',
        borderRadius: '0.125rem',
        padding: '0.3rem 0.5rem',
        marginRight: '0.5rem',
        marginBottom: '0.3125rem'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif !important',
        fontSize: '0.75rem',
        color: '#6f708b',
        fontWeight: '300',
        paddingLeft: '0 !important',
        lineHeight: '1.3125rem',
        minHeight: '0 !important'
      },
      menuStyle: {
        fontFamily: 'Open Sans, sans-serif !important',
        fontSize: '0.75rem',
        color: '#6f708b',
        fontWeight: '300',
        lineHeight: '1.3125rem',
        minHeight: '0 !important'
      },
      checkbox: {
        width: '0.75rem',
        borderRadius: '0.125rem',
        height: '0.75rem',
        marginRight: '0.625rem',
        marginLeft: '0.0625rem',
        marginTop: '0.125rem'
      },
      label: {
        fontSize: '0.75rem',
        lineHeight: '1.3125rem',
        fontWeight: '300',
        color: '#6f708b'
      },
      label__checked: {
        fontWeight: '600',
        fontSize: '0.75rem',
        lineHeight: '1.3125rem',
        color: '#6f708b'
      },
      span: {
        fontSize: '0.75rem',
        lineHeight: '1.75',
        fontWeight: '600',
        color: '#6f708b'
      },
      cancel: {
        width: '3.6875rem',
        height: '1.75rem',
        color: '#acadc1',
        textAlign: 'center',
        fontSize: '0.75rem',
        marginRight: '0.75rem',
        border: 'none'
      },
      apply: {
        width: '3.6875rem',
        height: '1.75rem',
        color: '#fff',
        textAlign: 'center',
        borderRadius: '0.125rem',
        backgroundColor: '#6f708b',
        fontSize: '0.75rem'
      },
      options: {
        lineHeight: '1.25rem',
        verticalAlign: 'middle',
        cursor: 'pointer',
        fontSize: '0.625rem',
        fontWeight: '300',
        width: 'auto',
        borderRadius: '0.125rem',
        backgroundColor: '#d6d6e1',
        display: 'inline-block',
        margin: '0.6875rem 0.5rem 0.3125rem 0',
        padding: '0.1875rem 0.5rem',
        color: '#6f708b'
      },
      fieldList: {
        maxHeight: '7.8125rem'
      },
      clearIcon: {
        lineHeight: '1.25rem',
        width: '0.875rem',
        height: '0.875rem'
      }
    };
    this.valuesLimit = 10;
    this.valuesOffset = 0;
    this.isAllValuesLoaded = false;
  }

  componentDidMount() {
    getUsersFields()
      .then(data => {
        let filterList = [];
        let excludeList = ['last_name', 'handle', 'status', 'email'];
        if (data.fields) {
          filterList = data.fields
            .filter(el => !~excludeList.indexOf(el))
            .map((item, index) => {
              return {
                type: item === 'first_name' ? 'name' : item,
                options: [],
                id: index
              };
            });
        }
        let filterArr = (this.props.filterContent &&
          this.props.filterContent.length &&
          this.props.filterContent) ||
          (this.props.modal && this.props.modal.filter) || [
            { id: -1, isMain: true, type: 'Add Filter +', options: [] }
          ];
        this.setState({ filterList, filterArr });
      })
      .catch(err => {
        console.error(`Error in DynamicSelections.getUsersFields.func : ${err}`);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeTab !== this.props.activeTab) {
      if (nextProps.modal && nextProps.modal.filter) {
        this.setState({
          filterArr: nextProps.modal.filter
        });
        this.getPreviewList(
          nextProps.modal.filter,
          this._inputFilter && this._inputFilter.value.trim()
        );
      } else {
        let empty = [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }];
        this.setState({
          filterArr: empty
        });
        this.getPreviewList(empty, this._inputFilter && this._inputFilter.value.trim());
      }
    }
  }

  getPreviewList = (filter, search) => {
    this.props.getPreviewList(
      filter || this.state.filterArr,
      search || (this._inputFilter && this._inputFilter.value.trim())
    );
    this.calculateHeightScroll();
  };

  handleTouchTap = (id, filter, event) => {
    event.preventDefault();
    this.setState(
      {
        filterAdd: true,
        currentFilter: filter,
        filteredOptions: filter.options || [],
        anchorEl: event.currentTarget
      },
      () => {
        this.calculateHeightScroll();
      }
    );
  };

  handleRequestClose = () => {
    this.setState(
      {
        filterAdd: false,
        currentFilter: {},
        filteredOptions: []
      },
      () => {
        this.getPreviewList();
      }
    );
  };

  cancelFilter = () => {
    this.setState({ searchText: '' });
    //resetting the offset on cancel
    this.valuesOffset = 0;
    let filterArr = this.state.filterArr;
    let getIndexCurrentFilter = findIndex(
      filterArr,
      filter => +filter.id === +this.state.currentFilter.id
    );
    if (getIndexCurrentFilter > -1) {
      filterArr.splice(getIndexCurrentFilter, 1);
      if (filterArr.length === 1 && filterArr[0].isMain) {
        filterArr[0].type = 'Add Filter +';
      }
      this.setState({
        filterAdd: false,
        filterArr,
        currentFilter: {},
        filteredOptions: []
      });
    }
    this.getPreviewList();
  };

  applyFilter = () => {
    let filterArr = this.state.filterArr.filter(
      filter =>
        filter.type === '+' ||
        filter.type === 'Add Filter +' ||
        (filter.options && filter.options.some(option => !!option.checked))
    );

    this.setState(
      {
        searchText: '',
        filterAdd: false,
        currentFilter: {},
        filteredOptions: [],
        filterArr
      },
      () => {
        this.getPreviewList();
      }
    );
  };

  toggleItems = id => {
    let currentFilter = this.state.currentFilter;
    let index = findIndex(this.state.currentFilter.options, el => +el.id === +id);
    currentFilter.options[index].checked = !currentFilter.options[index].checked;
    currentFilter.isCheckedAll =
      this.isAllValuesLoaded && currentFilter.options.every(item => item.checked);
    this.setState({ currentFilter }, () => {
      this.calculateHeightScroll();
    });
  };

  calculateHeightScroll = () => {
    let scrollHeightField =
      this._selectedFieldsContainer && this._selectedFieldsContainer.clientHeight < 125
        ? this._selectedFieldsContainer.clientHeight
        : 125;
    this.setState({ scrollHeightField });
  };

  clearOptions = () => {
    let currentFilter = this.state.currentFilter;
    currentFilter.options.forEach(el => {
      el.checked = false;
    });
    this.setState({ currentFilter }, () => {
      this.calculateHeightScroll();
    });
  };

  addFilter = filter => {
    let filterArr = this.state.filterArr;
    if (filterArr.length === 1) {
      filterArr[0].type = '+';
    }
    if (!filter.options.length) {
      let type = filter.type;
      let payload = { limit: this.valuesLimit, offset: 0, fields: 'first_name, last_name, email' };
      if (
        !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
        this.props.currentAction === 'assign'
      ) {
        payload['only_from_my_teams'] = true;
      }
      (type !== 'name'
        ? getFieldValues(type, { limit: this.valuesLimit, offset: 0 })
        : searchUsers(payload)
      )
        .then(data => {
          if (data && (data.users || data.values)) {
            let options = [];
            if (type !== 'name') {
              options = data.values.map((item, index) => {
                return { option: item, checked: false, id: index };
              });
            } else {
              options = data.users.map((item, index) => {
                return {
                  option: `${item.firstName} ${item.lastName}`,
                  checked: false,
                  id: index,
                  email: item.email
                };
              });
            }
            filter.options = options;
            filterArr.splice(filterArr.length - 1, 0, filter);
            this.isAllValuesLoaded = filter.options.length === data.total;
            this.setState(
              {
                filterArr,
                currentFilter: filter,
                filteredOptions: filter.options || []
              },
              () => {
                this.calculateHeightScroll();
              }
            );
          }
        })
        .catch(err => {
          console.error(`Error in DynamicSelections.getFieldValues.func : ${err}`);
        });
    } else {
      filterArr.splice(filterArr.length - 1, 0, filter);
      this.setState(
        {
          filterArr,
          currentFilter: filter,
          filteredOptions: filter.options || []
        },
        () => {
          this.calculateHeightScroll();
        }
      );
    }
  };

  handleValuesScroll = throttle(
    (type, values) => {
      const { scrollTop, clientHeight, scrollHeight } = values;
      if (this.state[`${type}Pending`] || this.isAllValuesLoaded) {
        return;
      }
      if (scrollTop === scrollHeight - clientHeight) {
        this.showMoreValues(type, scrollTop);
      }
    },
    300,
    { leading: false }
  );

  showMoreValues = (value, scrollTop) => {
    this.setState({ [`${value}Pending`]: true }, () => {
      if (this[`_${value}Container`]) {
        this[`_${value}Container`].scrollTop(scrollTop + this._pendingContainer.clientHeight);
      }
      this.valuesOffset = this.valuesOffset + this.valuesLimit;
      let currentFilter = this.state.currentFilter;
      let payload = {
        limit: this.valuesLimit,
        offset: this.valuesOffset,
        fields: 'first_name, last_name, email'
      };
      if (
        !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
        this.props.currentAction === 'assign'
      ) {
        payload['only_from_my_teams'] = true;
      }
      (currentFilter.type !== 'name'
        ? getFieldValues(currentFilter.type, {
            limit: this.valuesLimit,
            offset: this.valuesOffset
          })
        : searchUsers(payload)
      )
        .then(data => {
          let newOptions = [];
          if (currentFilter.type !== 'name') {
            newOptions =
              (data &&
                data.values &&
                data.values.map((item, index) => {
                  return {
                    option: item,
                    checked: currentFilter.isCheckedAll,
                    id: currentFilter.options.length + index
                  };
                })) ||
              [];
          } else {
            newOptions =
              (data &&
                data.users &&
                data.users.map((item, index) => {
                  return {
                    option: `${item.firstName} ${item.lastName}`,
                    checked: currentFilter.isCheckedAll,
                    id: currentFilter.options.length + index
                  };
                })) ||
              [];
          }
          currentFilter.options = currentFilter.options.concat(newOptions);
          this.isAllValuesLoaded = currentFilter.options.length === data.total;
          this[`_${value}Container`].scrollTop(scrollTop);
          this.setState(
            {
              currentFilter,
              filteredOptions: currentFilter.options || [],
              [`${value}Pending`]: false
            },
            () => {
              this.calculateHeightScroll();
            }
          );
        })
        .catch(err => {
          console.error(`Error in DynamicSelections.showMoreValues.getFieldValues.func : ${err}`);
        });
    });
  };

  filterSearch = () => {
    let val = this._inputFilter.value.trim();
    let currentFilter = this.state.currentFilter;
    let filteredOptions = [];
    if (currentFilter.type === 'name') {
      let payload = {
        q: val,
        limit: this.valuesLimit,
        offset: 0,
        fields: 'first_name, last_name, email'
      };
      if (
        !(this.props.currentUser.isAdmin && Permissions.has('ADMIN_ONLY')) &&
        this.props.currentAction === 'assign'
      ) {
        payload['only_from_my_teams'] = true;
      }
      searchUsers(payload)
        .then(data => {
          filteredOptions =
            data &&
            data.users &&
            data.users.map((item, index) => {
              return {
                option: `${item.firstName} ${item.lastName}`,
                checked: currentFilter.isCheckedAll,
                id: currentFilter.options.length + index
              };
            });
          currentFilter.options = filteredOptions;
          this.setState({ searchText: val, filteredOptions, currentFilter });
        })
        .catch(err => {
          console.error(`Error in DynamicSelections.filterSearch.searchUsers.func : ${err}`);
        });
    } else {
      filteredOptions =
        currentFilter.options &&
        currentFilter.options.filter(
          option => ~option.option.toLowerCase().indexOf(val.toLowerCase())
        );
      this.setState({ searchText: val, filteredOptions });
    }
  };

  removeClick = (currentFilter, e) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ currentFilter }, () => {
      this.calculateHeightScroll();
      this.cancelFilter();
    });
  };

  toggleAll = () => {
    this.setState(
      prevState => {
        let currentFilter = prevState.currentFilter;
        currentFilter.isCheckedAll = !currentFilter.isCheckedAll;
        let filteredOptions = prevState.filteredOptions;
        let isCheckedAll = filteredOptions.every(elem => elem.checked);
        filteredOptions.forEach((item, index) => {
          filteredOptions[index].checked = !isCheckedAll;
        });
        return { filteredOptions, currentFilter };
      },
      () => {
        this.calculateHeightScroll();
      }
    );
  };

  render() {
    let filterMenuForGroupModal =
      this.props.isGroupModal && this.state.currentFilter && !this.state.currentFilter.isMain;
    let initialFilter = this.state.currentFilter && this.state.currentFilter.isMain;
    return (
      <div
        className={`dynamic-container ${
          this.props.forIndividuals ? 'dynamic-container_only-filter' : ''
        }`}
      >
        <div>
          {this.state.filterArr.map((filter, index) => {
            let checkedFilters =
              (filter.options && filter.options.filter(el => el.checked).slice(0, 5)) || [];
            return (
              <button
                key={index}
                className={filter.type === '+' ? 'dynamic-button__plus' : ''}
                onClick={this.handleTouchTap.bind(this, index, filter)}
                style={this.styles.addButton}
              >
                {filter.type}
                {filter.type !== '+' && filter.type !== 'Add Filter +' && (
                  <span>
                    {checkedFilters.length ? ':' : ''}
                    {checkedFilters.map((el, idx) => (
                      <span key={el.id}>
                        {' '}
                        {idx ? ', ' : ''}
                        {el.option}{' '}
                      </span>
                    ))}
                    {filter.options && filter.options.length > 5 && <span>...</span>}
                    {this.state.filterAdd ? (
                      ''
                    ) : (
                      <span
                        style={this.styles.removeSpan}
                        onClick={this.removeClick.bind(this, filter)}
                      >
                        x
                      </span>
                    )}
                  </span>
                )}
              </button>
            );
          })}
        </div>
        <Popover
          open={this.state.filterAdd}
          style={{ marginTop: '0.625rem', overflowY: 'auto' }}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{
            horizontal: 'left',
            vertical: filterMenuForGroupModal ? 'top' : 'bottom'
          }}
          targetOrigin={{
            horizontal: 'left',
            vertical: filterMenuForGroupModal ? 'bottom' : 'top'
          }}
          onRequestClose={this.handleRequestClose}
          animation={PopoverAnimationVertical}
        >
          {initialFilter && (
            <Menu>
              {this.state.filterList
                .filter(filter => {
                  return (
                    !~findIndex(this.state.filterArr, elem => +elem.id === +filter.id) &&
                    !(this.props.activeTab === 'Dynamic Selections' && filter.type === 'name')
                  );
                })
                .map(el => (
                  <MenuItem
                    key={`filter-options-${el.id}`}
                    style={this.styles.menuStyle}
                    onClick={() => {
                      this.addFilter(el);
                    }}
                    menuItemStyle={this.styles.menuItem}
                    disabled={includes(this.state.filterArr.map(elem => elem.id), el.id)}
                    primaryText={el.type}
                    innerDivStyle={{ padding: '0 0.5rem' }}
                  />
                ))}
            </Menu>
          )}
          {this.state.currentFilter && !this.state.currentFilter.isMain && (
            <div className="current_filter">
              <div
                className="my-team__search-input-block ma-search-input-block__filter"
                style={this.styles.searchContainer}
              >
                <input
                  placeholder={tr('Search')}
                  onChange={this.filterSearch}
                  type="text"
                  ref={node => (this._inputFilter = node)}
                  style={{ paddingLeft: '0.5rem' }}
                />
                <SearchIcon style={this.styles.searchIcon} onClick={this.filterSearch} />
              </div>
              <Scrollbars
                style={{ height: 125 }}
                onScrollFrame={this.handleValuesScroll.bind(this, 'values')}
                ref={node => (this._valuesContainer = node)}
              >
                <Paper style={this.styles.fieldList} className="list-item-wrapper">
                  {!!this.state.filteredOptions.length && (
                    <Checkbox
                      label={tr(`All`)}
                      onCheck={this.toggleAll}
                      className="checkbox"
                      style={{ marginBottom: '0.25rem' }}
                      iconStyle={this.styles.checkbox}
                      uncheckedIcon={<div className="unchecked" />}
                      checked={this.state.currentFilter.isCheckedAll}
                      labelStyle={
                        this.state.currentFilter.isCheckedAll
                          ? this.styles.label__checked
                          : this.styles.label
                      }
                    />
                  )}
                  {this.state.filteredOptions.map((el, index) => (
                    <div
                      key={`all-filter-options-${el.id + index}`}
                      className="overflow-wrapper__outer"
                    >
                      <Checkbox
                        label={tr(`${el.option}`)}
                        onCheck={() => {
                          this.toggleItems(el.id);
                        }}
                        className="checkbox"
                        iconStyle={this.styles.checkbox}
                        checked={el.checked}
                        uncheckedIcon={<div className="unchecked" />}
                        labelStyle={el.checked ? this.styles.label__checked : this.styles.label}
                      />
                    </div>
                  ))}
                  {this.state.valuesPending && (
                    <div
                      ref={node => (this._pendingContainer = node)}
                      className="values-spinner-container"
                    >
                      <Spinner />
                    </div>
                  )}
                </Paper>
              </Scrollbars>
              <hr style={this.styles.hr} />
              <div>
                <span style={this.styles.span}>
                  {' '}
                  {tr('Selected')} {this.state.currentFilter.type}:{' '}
                </span>
                <span className="clear_button" onClick={this.clearOptions.bind(this)}>
                  {tr('Clear')}
                </span>
                <Scrollbars
                  style={{ height: this.state.scrollHeightField }}
                  onScrollFrame={this.handleValuesScroll.bind(this, 'selectedFields')}
                >
                  <div
                    ref={node => (this._selectedFieldsContainer = node)}
                    style={this.styles.fieldList}
                    className="list-item-wrapper"
                  >
                    {this.state.currentFilter.options &&
                      this.state.currentFilter.options
                        .filter(elem => elem.checked)
                        .map((el, index) => {
                          return (
                            <div
                              key={`selected-options-${el.id}`}
                              onClick={() => {
                                this.toggleItems(el.id);
                              }}
                              style={this.styles.options}
                            >
                              <div className="filter-option-name">
                                <span className="filter-option-name">{el.option}</span>
                                <ContentClear style={this.styles.clearIcon} />
                              </div>
                            </div>
                          );
                        })}
                    {this.state.selectedFieldsPending && (
                      <div
                        ref={node => (this._pendingContainer = node)}
                        className="values-spinner-container"
                      >
                        <Spinner />
                      </div>
                    )}
                  </div>
                </Scrollbars>
              </div>
              <hr style={this.styles.hr} />
              <div className="footer_button">
                <button onClick={this.cancelFilter.bind(this)} style={this.styles.cancel}>
                  {' '}
                  {tr('Cancel')}{' '}
                </button>
                <button onClick={this.applyFilter.bind(this)} style={this.styles.apply}>
                  {' '}
                  {tr('Apply')}{' '}
                </button>
              </div>
            </div>
          )}
        </Popover>
      </div>
    );
  }
}

MultiActionDynamicSelections.propTypes = {
  modal: PropTypes.any,
  getPreviewList: PropTypes.func,
  filterContent: PropTypes.array,
  forIndividuals: PropTypes.bool,
  isGroupModal: PropTypes.bool,
  activeTab: PropTypes.any,
  currentUser: PropTypes.any,
  contentMultiaction: PropTypes.any,
  currentAction: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS()
  };
}

export default connect(mapStoreStateToProps)(MultiActionDynamicSelections);
