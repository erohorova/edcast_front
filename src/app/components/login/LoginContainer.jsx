import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Paper from 'edc-web-sdk/components/Paper';
import TextField from 'material-ui/TextField';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import ssos from 'edc-web-sdk/components/ssos/index';
import FlashAlert from '../common/FlashAlert';
import {
  requestLogin,
  changePasswordInput,
  changeEmailInput
} from '../../actions/currentUserActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import includes from 'lodash/includes';
import find from 'lodash/find';
import Spinner from '../common/spinner';
import * as upshotActions from '../../actions/upshotActions';
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min';

class LoginContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      password: '',
      email: '',
      emailError: '',
      passwordError: '',
      signUpV2: window.ldclient.variation('signup-ui-v2', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      encryption_payload:
        this.props.team && this.props.team.config && this.props.team.config.encryption_payload
    };
    this.loginButtonClickHandler = this.loginButtonClickHandler.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.emailKeydownHandler = this.emailKeydownHandler.bind(this);
    this.passwordKeydownHandler = this.passwordKeydownHandler.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
    this.getSsoLabelName = this.getSsoLabelName.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentUser.hasEmailLogin) {
      setTimeout(() => {
        if (this._emailLabel.input.value) {
          this._passwordLabel.setState({ hasValue: true });
        }
      }, 500);
    }
  }

  getSsoLabelName(ssoLabelName, ssoName, customSsoName, noSsoOptionName) {
    if (customSsoName && ssoLabelName === ssoName) {
      return customSsoName;
    } else if (ssoLabelName && ssoName && ssoLabelName !== ssoName) {
      return ssoLabelName;
    } else if (noSsoOptionName) {
      return noSsoOptionName;
    }
  }

  loginButtonClickHandler() {
    if (
      this._emailLabel.input.value === '' ||
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this._emailLabel.input.value
      )
    ) {
      this.setState({ emailError: 'Enter a valid email address' });
    } else {
      let encrypted_password = this.state.password;
      if (this.state.encryption_payload && JSEncrypt) {
        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(window.process.env.JsPublic);
        encrypted_password = encrypt.encrypt(this.state.password);
      }
      this.props
        .dispatch(requestLogin(this.state.email, encrypted_password))
        .then(user => {
          let webSessionTimeout =
            this.props.team && this.props.team.config && this.props.team.config.web_session_timeout;
          webSessionTimeout = webSessionTimeout ? webSessionTimeout * 60 * 1000 + Date.now() : 0;
          localStorage.setItem('webSessionTimeout', webSessionTimeout);

          // This is required for EdCast -> Okta -> EdCast to create a remote session
          let afterLoginURL = localStorage.getItem('afterLoginContentURL') || '';
          afterLoginURL = afterLoginURL.replace(/^\/|\/$/g, '');

          if (user.consumer_redirect_uri) {
            window.location.href = user.consumer_redirect_uri + afterLoginURL;
          }
          if (window.opener) {
            window.opener.postMessage(user, '*');
          }
          if (this.state.upshotEnabled) {
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['LOGIN'], {
              status: 'Success',
              email: this.state.email,
              event: 'Login'
            });
          }
        })
        .catch(err => {
          console.error(`Error in LoginContainer.requestLogin.func: ${err}`);
          if (this.state.upshotEnabled) {
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['LOGIN'], {
              status: 'Failure',
              email: this.state.email,
              event: 'Login'
            });
          }
        });
    }
  }

  handlePasswordChange(e) {
    if (this.props.currentUser.invalidPasswordInput === true) {
      this.setState({ password: e.target.value });
      this.props.dispatch(changePasswordInput());
      this.props.dispatch(changeEmailInput());
    } else {
      this.setState({ password: e.target.value });
    }
  }

  handleEmailChange(e) {
    this.setState({ email: e.target.value, emailError: '' });
    if (this.props.currentUser.invalidEmailInput === true) {
      this.props.dispatch(changeEmailInput());
    }
  }

  emailKeydownHandler(e) {
    if (e.keyCode === 13) {
      this._passwordLabel.focus();
    }
  }

  passwordKeydownHandler(e) {
    if (e.keyCode === 13) {
      this.loginButtonClickHandler();
    }
    if (e.keyCode === 8 && this.props.currentUser.invalidEmailInput === true) {
      this.setState({ password: '' });
      this.props.dispatch(changeEmailInput());
    }
  }

  validateEmail() {
    if (
      this._emailLabel.input.value === '' ||
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        this._emailLabel.input.value
      )
    ) {
      this.setState({ emailError: 'Enter a valid email address' });
    }
  }

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  showResetPasswordNotification() {
    return window.location.search.replace('?', '') == 'sent_reset_password_mail=true';
  }

  getAutoSsoUrl() {
    // Automatic redirection to SSO
    if (
      (includes(window.location.search, 'auto_sso=true') || window.location.pathname == '/') &&
      this.props.team.config &&
      this.props.team.config['feature/auto_sso_login']
    ) {
      if (this.props.team.ssos) {
        var sso = find(this.props.team.ssos, el => {
          return el.name == this.props.team.config['feature/auto_sso_login'];
        });

        if (sso) {
          let urlStr = localStorage.getItem('afterLoginContentURL')
            ? '&origin=' +
              'https://' +
              window.location.hostname +
              localStorage.getItem('afterLoginContentURL') +
              '?clearURL=true'
            : '';
          return sso.webAuthenticationUrl + urlStr;
        }
      }
    }

    return null;
  }

  getErrorText = () => {
    let lockCondition = this.props.currentUser.locked;
    let invalidEmail = this.props.currentUser.invalidEmailInput
      ? tr('Invalid email or password')
      : '';
    return lockCondition
      ? tr('You account has been locked, please contact support@edcast.com to get it reactivated')
      : invalidEmail;
  };

  render() {
    // Automatic redirection to SSO
    var autoSsoUrl = this.getAutoSsoUrl();
    if (autoSsoUrl !== null) {
      window.location = autoSsoUrl;
      return <div />;
    }

    let customSplashScreen = false;
    if (this.props.team.config && this.props.team.config['custom_splash_screen']) {
      customSplashScreen = this.props.team.config['custom_splash_screen'];
    }

    let SsoArray = this.props.team.ssos;

    return (
      <div
        id="login"
        style={{ backgroundImage: `url(${customSplashScreen || '/i/images/gg_bridge.jpg'})` }}
      >
        {this.showResetPasswordNotification() && (
          <FlashAlert
            message="You will receive an email with instructions about how to reset your password in a few minutes."
            toShow={true}
            removeAlert={false}
          />
        )}
        <div className="row align-center top-login-row">
          <div className="small-12 medium-7 align-self-middle columns login-table">
            <Paper zDepth={2}>
              {this.props.team.invalidTeamName && (
                <div className="text-center" style={{ margin: '1rem' }}>
                  Team <strong>{window.location.host.split('.')[0]}</strong>{' '}
                  {tr('not found. Please enter valid team name in')} URL.
                </div>
              )}
              {!this.props.team.invalidTeamName && this.props.team.pending && (
                <div className="text-center">
                  <Spinner />
                </div>
              )}
              {!this.props.team.invalidTeamName && !this.props.team.pending && (
                <div className="login-container">
                  <div className="text-center">
                    <div className="row">
                      <div className="small-10 small-offset-1 columns">
                        <img className="cobrand-logo" src={this.props.team.coBrandingLogo} />
                      </div>
                    </div>
                  </div>
                  <h5 className="login-text text-center">{tr('Log In')}</h5>
                  <div>
                    {this.props.team.config &&
                      this.props.team.config.hasOwnProperty('AllowedEmailDomains') &&
                      this.props.team.config['AllowedEmailDomains'] !== 'none' && (
                        <div>
                          {tr('Need an EdCast account?')}{' '}
                          <a
                            onTouchTap={() => {
                              this.props.dispatch(
                                push(this.state.signUpV2 ? '/create_account' : '/sign_up')
                              );
                            }}
                          >
                            {tr('Create account')}
                          </a>
                        </div>
                      )}
                    <div className="vertical-spacing-large">
                      {this.props.team.hasEmailLogin && (
                        <div className="vertical-spacing-large">
                          <div>
                            <div className="email-text-label">
                              <TextField
                                hintText="name@domain.com"
                                floatingLabelText={tr('Email')}
                                fullWidth={true}
                                onChange={this.handleEmailChange}
                                onBlur={this.validateEmail}
                                onKeyDown={this.emailKeydownHandler}
                                value={this.state.email}
                                errorText={
                                  this.state.emailError ? tr('Please enter a valid email') : ''
                                }
                                ref={node => (this._emailLabel = node)}
                                aria-label="enter a valid email of format name@domain.com"
                              />
                            </div>
                            <div className="password-text-label">
                              <TextField
                                type="password"
                                floatingLabelText={tr('Password')}
                                fullWidth={true}
                                onChange={this.handlePasswordChange}
                                onKeyDown={this.passwordKeydownHandler}
                                value={this.state.password}
                                errorText={this.getErrorText()}
                                ref={node => (this._passwordLabel = node)}
                                aria-label="enter password"
                              />
                            </div>
                          </div>
                          <div className="login-button">
                            <PrimaryButton
                              label={tr('LOG IN')}
                              className="loginEmail"
                              onTouchTap={this.loginButtonClickHandler}
                              style={{ width: '100%', margin: 0, marginBottom: '8px' }}
                            />
                            <a
                              onTouchTap={() => {
                                this.props.dispatch(push('/forgot_password'));
                              }}
                            >
                              <div className="small-font text-center">{tr('Forgot Password?')}</div>
                            </a>
                          </div>
                        </div>
                      )}
                      {this.props.team.hasNonEmailLogin && SsoArray.length > 0 && (
                        <div className="non-email-sso vertical-spacing-large">
                          {this.props.team.hasEmailLogin && (
                            <div className="text-center">
                              <div className="text-line">
                                <div className="line-center">{tr('or')}</div>
                              </div>
                            </div>
                          )}
                          <div>{tr('Log In via:')}</div>
                          {SsoArray.length > 0 && (
                            <div className="sso-options vertical-spacing-medium">
                              {SsoArray.map((sso, index) => {
                                let nonSSOoption = {};
                                if (sso.name !== 'email') {
                                  if (!ssos[sso.name]) {
                                    nonSSOoption.name = sso.name
                                      .charAt(0)
                                      .toUpperCase()
                                      .concat(sso.name.slice(1));
                                    nonSSOoption.color = '#003C4D';
                                  }
                                  let buttonName = this.getSsoLabelName(
                                    sso.labelName,
                                    sso.name,
                                    ssos[sso.name] ? ssos[sso.name].label : null,
                                    nonSSOoption.name
                                  );

                                  let buttonLabel =
                                    sso.labelName == '' || sso.labelName == sso.name
                                      ? ssos[sso.name].label
                                      : sso.labelName;
                                  let urlStr = localStorage.getItem('afterLoginContentURL')
                                    ? '&origin=' +
                                      'https://' +
                                      window.location.hostname +
                                      localStorage.getItem('afterLoginContentURL')
                                    : '';

                                  let link = sso.webAuthenticationUrl + urlStr;
                                  if (location.pathname !== '/log_in') {
                                    //check to see if it is a widget link
                                    if (~window.location.href.indexOf('/widgets/v1')) {
                                      //if it is a widget link, we need to encodeURI for query params (also remove showLogin query param)
                                      let splitUrl = window.location.href
                                        .replace(/\b&showLogin=true/, '')
                                        .split('/widgets/v1?');
                                      link +=
                                        '&consumer_redirect_uri=' +
                                        splitUrl[0] +
                                        '/widgets/v1?' +
                                        encodeURIComponent(splitUrl[1]);
                                    } else {
                                      link += '&consumer_redirect_uri=' + window.location.href;
                                    }
                                  }
                                  return (
                                    <div key={index}>
                                      <a href={link}>
                                        <PrimaryButton
                                          label={buttonLabel}
                                          className={this.toCamelCase('login ' + buttonLabel)}
                                          style={{
                                            backgroundColor:
                                              ssos[sso.name] && ssos[sso.name].color
                                                ? ssos[sso.name].color
                                                : nonSSOoption.color,
                                            borderColor:
                                              ssos[sso.name] && ssos[sso.name].color
                                                ? ssos[sso.name].color
                                                : nonSSOoption.color,
                                            width: '100%',
                                            margin: 0
                                          }}
                                        />
                                      </a>
                                    </div>
                                  );
                                }
                              })}
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                    <div className="copyright text-center">
                      <div>{tr(`Copyright © %{year} - EdCast Inc.`, {year: new Date().getFullYear()})}</div>
                      <div>
                        <a href="/corp/privacy-policy">{tr('Privacy')}</a>
                        {tr(' and ')}
                        <a href="/corp/terms-of-service">{tr('Terms')}</a>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

LoginContainer.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object
};

export default connect(state => {
  return { currentUser: state.currentUser.toJS(), team: state.team.toJS() };
})(LoginContainer);
