import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import ssos from 'edc-web-sdk/components/ssos/index';
import { tr } from 'edc-web-sdk/helpers/translations';
import { requestSignup } from 'edc-web-sdk/requests/users';
import findIndex from 'lodash/findIndex';
import Spinner from '../common/spinner';
import * as upshotActions from '../../actions/upshotActions';

class SignupContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      password: '',
      email: '',
      emailError: '',
      showErr: false,
      passwordError: '',
      showEmailSignup: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      lxpOAuth: window.ldclient.variation('enable-lxp-oauth-okta', false)
    };
    this.styles = {
      emailErrorStyle: {
        display: 'none',
        color: '#ff0000',
        fontSize: '12px',
        paddingLeft: '100px',
        textAlign: 'left'
      }
    };
    this.selectEmailOption = this.selectEmailOption.bind(this);
    this.hideEmailOption = this.hideEmailOption.bind(this);
    this.getSsoLabelName = this.getSsoLabelName.bind(this);
    this.toCamelCase = this.toCamelCase.bind(this);
  }

  selectEmailOption() {
    this.setState({
      showEmailSignup: true,
      showErr: false
    });
  }

  hideEmailOption() {
    this.setState({
      showEmailSignup: false,
      showErr: false
    });
  }

  getSsoLabelName(ssoLabelName, ssoName, customSsoName, noSsoOptionName) {
    if (customSsoName && ssoLabelName === ssoName) {
      return customSsoName;
    } else if (ssoLabelName && ssoName && ssoLabelName !== ssoName) {
      return ssoLabelName;
    } else if (noSsoOptionName) {
      return noSsoOptionName;
    }
  }

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  handleSubmit(event) {
    event.preventDefault();
    let dataObj = {
      user: { email: this.refs.emailInput.value },
      utf8: this.refs.utf8.value,
      commit: this.refs.commit.value,
      from_edc_web: 'true'
    };
    this.setState({ showErr: false });
    requestSignup(dataObj)
      .then(res => {
        let showErr = typeof res == 'string';
        if (showErr) {
          this.setState({ emailError: res, showErr: showErr });
        } else {
          window.location.href = '/confirmation';
          if (this.state.upshotEnabled) {
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['REGISTER'], {
              status: 'Success',
              email: this.refs.emailInput.value,
              event: 'Register'
            });
          }
          if (window.upshot && this.state.upshotEnabled) {
            window.upshot.updateUserProfile({ email: this.refs.emailInput.value });
          }
        }
      })
      .catch(err => {
        console.error(`Error in SignupContainer.requestSignup.func: ${err}`);
        if (this.state.upshotEnabled) {
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['REGISTER'], {
            status: 'Failure',
            email: this.refs.emailInput.value,
            event: 'Register'
          });
        }
        if (window.upshot && this.state.upshotEnabled) {
          window.upshot.updateUserProfile({ email: this.refs.emailInput.value });
        }
      });
  }

  render() {
    let customSplashScreen = false;
    if (this.props.team.config && this.props.team.config['custom_splash_screen']) {
      customSplashScreen = this.props.team.config['custom_splash_screen'];
    }

    let teamSsos = this.props.team.ssos;
    // code to move the email option to the last
    let emailOptionIndex = findIndex(teamSsos, function(item, index) {
      return item.name == 'email';
    });

    if (emailOptionIndex > -1) {
      let emailOption = teamSsos[emailOptionIndex];
      teamSsos.splice(emailOptionIndex, 1);
      teamSsos.push(emailOption);
    }

    return (
      <div
        id="login"
        className="signup-container"
        style={{
          backgroundImage: `url(${customSplashScreen ||
            'https://d2rdbjk9w0dffy.cloudfront.net/assets/sign_up_bg.jpg'})`
        }}
      >
        <div className="row align-center top-login-row">
          <div className="small-12 medium-7 align-self-middle columns login-table">
            <Paper className="transparent-signup" zDepth={2}>
              {this.props.team.invalidTeamName && (
                <div className="text-center" style={{ margin: '1rem' }}>
                  Team <strong>{window.location.host.split('.')[0]}</strong>{' '}
                  {tr('not found. Please enter valid team name in')} URL.
                </div>
              )}
              {!this.props.team.invalidTeamName && this.props.team.pending && (
                <div className="text-center">
                  <Spinner />
                </div>
              )}
              {!this.props.team.invalidTeamName && !this.props.team.pending && (
                <div className="login-container">
                  <div className="text-center">
                    <div className="row">
                      <div className="small-10 small-offset-1 columns">
                        <img className="cobrand-logo" src={this.props.team.coBrandingLogo} />
                      </div>
                    </div>
                  </div>

                  <div>
                    <div className="vertical-spacing-large">
                      <div className="non-email-sso vertical-spacing-large">
                        {this.props.team.hasEmailLogin && (
                          <div className="text-center">
                            <div className="border-div" />
                          </div>
                        )}

                        {!this.state.showEmailSignup && (
                          <div>
                            <div className="text-center signup-with">{tr('Sign up with')}</div>
                            {this.props.team.ssos && (
                              <div className="sso-options vertical-spacing-medium">
                                {this.props.team.ssos.map((sso, index) => {
                                  let nonSSOoption = {};
                                  if (!ssos[sso.name]) {
                                    nonSSOoption.name = sso.name
                                      .charAt(0)
                                      .toUpperCase()
                                      .concat(sso.name.slice(1));
                                    nonSSOoption.color = '#003C4D';
                                  }
                                  let buttonName = this.getSsoLabelName(
                                    sso.labelName,
                                    sso.name,
                                    ssos[sso.name] ? ssos[sso.name].label : null,
                                    nonSSOoption.name
                                  );
                                  let buttonLabel =
                                    sso.labelName == '' || sso.labelName == sso.name
                                      ? ssos[sso.name]
                                        ? ssos[sso.name].label
                                        : sso.labelName
                                      : sso.labelName;
                                  let link = sso.webAuthenticationUrl;
                                  if (location.pathname !== '/log_in') {
                                    link += '&consumer_redirect_uri=' + window.location.href;
                                  }

                                  if (sso.name !== 'email') {
                                    return (
                                      <div className="sso-option" key={index}>
                                        <a href={link}>
                                          <PrimaryButton
                                            label={this.state.lxpOAuth ? buttonLabel : buttonName}
                                            className={this.toCamelCase(
                                              'login ' + this.state.lxpOAuth
                                                ? buttonLabel
                                                : buttonName
                                            )}
                                            style={{
                                              backgroundColor:
                                                ssos[sso.name] && ssos[sso.name].color
                                                  ? ssos[sso.name].color
                                                  : nonSSOoption.color,
                                              borderColor:
                                                ssos[sso.name] && ssos[sso.name].color
                                                  ? ssos[sso.name].color
                                                  : nonSSOoption.color,
                                              width: '100%',
                                              margin: 0
                                            }}
                                          />
                                        </a>
                                      </div>
                                    );
                                  } else if (sso.name === 'email') {
                                    return (
                                      <div className="sso-option" key={index}>
                                        <PrimaryButton
                                          label={buttonName}
                                          className={this.toCamelCase('login ' + buttonName)}
                                          style={{
                                            backgroundColor:
                                              ssos[sso.name] && ssos[sso.name].color
                                                ? ssos[sso.name].color
                                                : nonSSOoption.color,
                                            borderColor:
                                              ssos[sso.name] && ssos[sso.name].color
                                                ? ssos[sso.name].color
                                                : nonSSOoption.color,
                                            width: '100%',
                                            margin: 0
                                          }}
                                          onClick={this.selectEmailOption.bind(this)}
                                        />
                                      </div>
                                    );
                                  }
                                })}
                              </div>
                            )}
                          </div>
                        )}

                        {this.state.showEmailSignup && (
                          <div>
                            <div className="text-center signup-with">
                              {tr('Sign up with Email')}
                            </div>
                            <form
                              onSubmit={this.handleSubmit.bind(this)}
                              method="post"
                              className="signup-form"
                              action="/auth/users"
                            >
                              <div className="input-block">
                                <input
                                  required
                                  ref="emailInput"
                                  name="user[email]"
                                  type="email"
                                  placeholder={tr('Your work email here...')}
                                />
                                <input name="utf8" ref="utf8" type="hidden" value="&#x2713;" />
                                <span
                                  style={{
                                    ...this.styles.emailErrorStyle,
                                    ...{ display: this.state.showErr ? 'block' : 'none' }
                                  }}
                                >
                                  {this.state.emailError}
                                </span>
                              </div>
                              <div className="input-block">
                                <button
                                  className="cancel-btn"
                                  onClick={this.hideEmailOption.bind(this)}
                                  type="submit"
                                >
                                  {tr('CANCEL')}
                                </button>
                                <button
                                  className="start-btn"
                                  ref="commit"
                                  name="commit"
                                  value="Get Started"
                                  type="submit"
                                >
                                  {tr('GET STARTED')}
                                </button>
                              </div>
                            </form>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </Paper>
            <div className="privacy-text text-center">
              {tr(
                "We'll never share anything or send emails to your contacts without your permission. By signing up, you agree to the  Terms of Use and Privacy Policy."
              )}
            </div>
          </div>
        </div>
        <div className="signup-footer">
          <div className="left-part">
            <ul className="terms-list">
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/privacy-policy/">
                  {tr('Privacy Policy')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/terms-of-service/">
                  {tr('Terms of Service')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/ferpa-compliance/">
                  {tr('FERPA Compliance')}
                </a>
              </li>
            </ul>
          </div>
          <div className="right-part">
            <div className="powered-by">
              {tr('Powered by')}{' '}
              <img
                className="ed-logo"
                src="https://d2rdbjk9w0dffy.cloudfront.net/assets/edcast-white.png"
              />
            </div>
            <ul className="terms-list">
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/about/">
                  {tr('About')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/careers/">
                  {tr('Careers')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/contact/">
                  {tr('Contact')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/press/">
                  {tr('Press')} &nbsp; &nbsp; |
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.edcast.com/corp/media-kit/">
                  {tr('Media Kit')}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

SignupContainer.propTypes = {
  team: PropTypes.object
};

export default connect(state => {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
})(SignupContainer);
