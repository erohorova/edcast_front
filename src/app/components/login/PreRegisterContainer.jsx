import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import trim from 'lodash/trim';
import SignUpConfirmation from 'edc-web-sdk/components/icons/SignUpConfirmation';
import { tr } from 'edc-web-sdk/helpers/translations';

class PreRegisterContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      confirmation: false,
      showInvalidEmailMessage: false,
      emailErrorMessage: tr('Please enter a valid email'),
      registerText: 'Register',
      firstname: '',
      lastname: '',
      email: '',
      companyName: '',
      designation: '',
      firstnameError: false,
      lastnameError: false,
      config:
        (this.props.team.config.onboardingMicroserviceConfig &&
          this.props.team.config.onboardingMicroserviceConfig.configs &&
          this.props.team.config.onboardingMicroserviceConfig.configs.loginScreen) ||
        {}
    };
    this.styles = {
      signupColor: {
        color: '#f0f0f5',
        marginBottom: '28px',
        fontSize: '20px',
        fontWeight: 'bold'
      },
      loginButtonColor: {
        backgroundColor: '#56e7ce'
      },
      signupButtonColor: {
        backgroundColor: '#6f708b',
        boxShadow: '0 0 4px 0 rgba(38, 39, 59, 0.6)',
        margin: '0px 0px 15px 0px',
        color: '#56e7ce'
      },
      emailMarginBottom: {
        marginBottom: '18px'
      },
      errorMessage: {
        float: 'left',
        color: '#F1736A',
        fontSize: '10px',
        margin: '1px 0 5px 0'
      },
      download: {
        width: '100%',
        height: '100%',
        padding: 0
      },
      tooltipStyles: {
        color: '#606063',
        backgroundColor: '#ffffff',
        left: '10%'
      }
    };
  }

  preRegister(e) {
    window.Intercom('boot', {
      app_id: 'nd7di7ge',
      email: this.state.email,
      first_name: this.state.firstname,
      last_name: this.state.lastname,
      company_name: this.state.companyName,
      designation: this.state.designation,
      org_id: this.props.team.orgId,
      org_url: location.hostname,
      source: 'preregistration'
    });
    e.preventDefault();
    this.setState({ confirmation: true });
  }

  fnamelnameHandle = e => {
    let fieldName = e.target.name;
    let fieldValue = e.target.value;
    let fieldNameReg = /^[a-zA-Z0-9-_]+$/;
    let validInput = trim(fieldValue).search(fieldNameReg) > -1;
    this.setState({
      [fieldName]: validInput ? fieldValue : '',
      [fieldName + 'Hint']: e.type == 'change' && !validInput,
      [fieldName + 'Error']: e.type == 'blur' && !validInput
    });
  };

  companyDesiganationHandler = e => {
    let fieldName = e.target.name;
    let fieldValue = e.target.value;
    this.setState({
      [fieldName]: fieldValue
    });
  };
  checkInvalidDomain = email => {
    return !!(
      email.indexOf('@gmail.com', email.length - '@gmail.com'.length) !== -1 ||
      email.indexOf('@yahoo.com', email.length - '@yahoo.com'.length) !== -1 ||
      email.indexOf('@rocketmail.com', email.length - '@rocketmail.com'.length) !== -1 ||
      email.indexOf('@outlook.com', email.length - '@outlook.com'.length) !== -1 ||
      email.indexOf('@msn.com', email.length - '@msn.com'.length) !== -1 ||
      email.indexOf('@hotmail.com', email.length - '@hotmail.com'.length) !== -1 ||
      email.indexOf('@rediffmail.com', email.length - '@rediffmail.com'.length) !== -1 ||
      email.indexOf('@indiatimes.com', email.length - '@indiatimes.com'.length) !== -1 ||
      email.indexOf('@yahoo.co.in', email.length - '@yahoo.co.in'.length) !== -1 ||
      email.indexOf('@yahoo.co.uk', email.length - '@yahoo.co.uk'.length) !== -1 ||
      email.indexOf('@live.com', email.length - '@live.com'.length) !== -1
    );
  };

  emailValidation = e => {
    let email = e.target.value;
    if (
      email === '' ||
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      this.setState({
        showInvalidEmailMessage: true,
        disableCreateButton: true,
        email: '',
        emailErrorMessage: tr('Please enter a valid email')
      });
      this.styles.emailMarginBottom.marginBottom = '0px';
    } else if (this.checkInvalidDomain(email)) {
      this.setState({
        showInvalidEmailMessage: true,
        disableCreateButton: true,
        email: '',
        emailErrorMessage: tr('Please enter only company email.')
      });
      this.styles.emailMarginBottom.marginBottom = '0px';
    } else {
      this.setState({
        showInvalidEmailMessage: false,
        email
      });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
    if (e.type == 'change') {
      this.setState({ showInvalidEmailMessage: false });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
  };

  render() {
    let customStyle = this.state.config;
    let isValidForm = !(
      this.state.firstname &&
      this.state.lastname &&
      this.state.email &&
      this.state.companyName &&
      this.state.designation
    );
    return (
      <div className="login-form">
        {!this.state.confirmation ? (
          <div>
            <div
              style={Object.assign(
                this.styles.signupColor,
                (customStyle.formHeadingColor && customStyle.formHeadingColor.styles) || {}
              )}
            >
              Register
            </div>
            <form onSubmit={this.preRegister.bind(this)}>
              <div className="text-fields-container">
                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder={tr('First Name')}
                    name="firstname"
                    onChange={this.fnamelnameHandle}
                    onBlur={this.fnamelnameHandle}
                    required
                  />
                  <div ref="firstname" />
                  {this.state.firstnameError && (
                    <p style={this.styles.errorMessage}>{tr('First Name is invalid.')}</p>
                  )}
                </div>

                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder={tr('Last Name')}
                    name="lastname"
                    onChange={this.fnamelnameHandle}
                    onBlur={this.fnamelnameHandle}
                    required
                  />
                  <div ref="lastname" />
                  {this.state.lastnameError && (
                    <p style={this.styles.errorMessage}>Last Name is invalid.</p>
                  )}
                </div>

                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder="Company Name"
                    name="companyName"
                    onChange={this.companyDesiganationHandler}
                    onBlur={this.companyDesiganationHandler}
                    required
                  />
                </div>

                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder="Designation"
                    name="designation"
                    onChange={this.companyDesiganationHandler}
                    onBlur={this.companyDesiganationHandler}
                    required
                  />
                </div>

                <div>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { ...this.styles.emailMarginBottom },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="email"
                    placeholder="Email"
                    onChange={this.emailValidation}
                    onBlur={this.emailValidation}
                    required
                  />
                  {this.state.showInvalidEmailMessage && (
                    <p style={this.styles.errorMessage}>{this.state.emailErrorMessage}</p>
                  )}
                </div>
              </div>
              <button
                className="login-page-button"
                style={Object.assign(
                  this.styles.signupButtonColor,
                  (customStyle.sigupButtonStyle && customStyle.sigupButtonStyle.styles) || {}
                )}
                disabled={isValidForm}
              >
                <span
                  className="button-loader"
                  dangerouslySetInnerHTML={{ __html: tr(this.state.registerText) }}
                />
              </button>
            </form>
          </div>
        ) : (
          <div className="confirmation">
            <div
              style={Object.assign(
                { margin: '118px auto 25px', fontSize: '20px', color: '#fff' },
                (customStyle.formHeadingColor && customStyle.formHeadingColor.styles) || {}
              )}
            >
              <p style={{ textTransform: 'capitalize' }}>
                {tr('Hi')} {this.state.firstname}&nbsp;{this.state.lastname},
              </p>
              <br />
              <p>{tr("Thanks for Registering! We'll get back to you soon.")}</p>
            </div>
            <div style={{ margin: '0px 0px 20px 0px', textAlign: 'center' }}>
              <SignUpConfirmation />
            </div>
          </div>
        )}
      </div>
    );
  }
}

PreRegisterContainer.propTypes = {
  team: PropTypes.object
};
export default connect(state => {
  return { currentUser: state.currentUser.toJS(), team: state.team.toJS() };
})(PreRegisterContainer);
