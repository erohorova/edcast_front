import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import trim from 'lodash/trim';
import find from 'lodash/find';
import includes from 'lodash/includes';
import { push } from 'react-router-redux';
// import EdcastLogoPlus from 'edc-web-sdk/components/icons/EdcastLogoPlus';
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min';
import ssos from 'edc-web-sdk/components/ssos/index';
import { newRequestLogin } from '../../actions/currentUserActions';
import * as upshotActions from '../../actions/upshotActions';
import SsoIcons from 'edc-web-sdk/components/icons/SsoIcons';
import { tr } from 'edc-web-sdk/helpers/translations';

class LoginForm extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showInvalidEmailMessage: false,
      showErrorMessage: false,
      email: '',
      password: '',
      errorMessage: 'Invalid email or password.',
      loginText: 'Log In',
      lockedError: '',
      submitData: false,
      showConfirmationError: false,
      config:
        (this.props.team.config.onboardingMicroserviceConfig &&
          this.props.team.config.onboardingMicroserviceConfig.configs &&
          this.props.team.config.onboardingMicroserviceConfig.configs.loginScreen) ||
        {},
      promptChecked:
        this.props.team.config.acceptancePromptEnabledForLogin == 'true'
          ? window.ldclient.variation('auto-check-tos', true)
          : true,
      acceptancePromptEnabledForLogin: this.props.team.config.acceptancePromptEnabledForLogin
        ? this.props.team.config.acceptancePromptEnabledForLogin
        : null,
      acceptancePromptMessage: this.props.team.config.acceptancePromptMessage
        ? this.props.team.config.acceptancePromptMessage
        : null,
      lxpOAuth: window.ldclient.variation('enable-lxp-oauth-okta', false),
      preRegistration: window.ldclient.variation('pre-registration', false),
      newLoginPageSettings: window.ldclient.variation('new-login-page-settings', false),
      isFormLoading: true,
      tocRed: false,
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      encryption_payload:
        this.props.team && this.props.team.config && this.props.team.config.encryption_payload,
      showNewLoginPage: window.ldclient.variation('new-login-page-settings', false)
    };

    // footer options to get TOS & Privacy Policy links
    this.footerOptions =
      this.props.team && this.props.team.config && this.props.team.config.footer_options;

    this.pageConfiguration =
      this.props.team.config &&
      this.props.team.config.OrgCustomizationConfig &&
      this.props.team.config.OrgCustomizationConfig.web &&
      this.props.team.config.OrgCustomizationConfig.web.login;

    this.styles = {
      loginColor: {
        color: '#f0f0f5',
        marginBottom: '28px',
        fontSize: '20px',
        fontWeight: 'bold'
      },
      loginButtonColor: {
        backgroundColor: '#454560',
        boxShadow: '0 0 4px 0 rgba(0, 0, 0, 0.5)',
        color: '#56e7ce'
      },
      signupButtonColor: {
        backgroundColor: '#6f708b',
        boxShadow: '0 0 4px 0 rgba(38, 39, 59, 0.6)',
        margin: '0px 0px 15px 0px',
        color: '#56e7ce'
      },
      socialShareButton: {
        backgroundColor: '#6f708b',
        boxShadow: '0 0 4px 0 rgba(38, 39, 59, 0.6)',
        margin: '0px 0px 15px 0px',
        color: '#56e7ce'
      },
      or: {
        margin: '0px 0px 15px 0px',
        fontSize: '12px',
        fontWeight: '500',
        textAlign: 'center',
        color: '#d6d6e1'
      },
      errorMessage: {
        float: 'left',
        color: '#F1736A',
        fontSize: '10px',
        margin: '1px 0 5px 0'
      },
      emailMarginBottom: {
        marginBottom: '1.125rem'
      },
      note: {
        margin: '0px 0px 15px 0px',
        fontSize: '12px',
        fontWeight: '500',
        color: '#ffffff'
      }
    };
  }

  preRegister() {
    this.props.dispatch(push('/pre_register'));
  }

  goToSignUp = () => {
    this.props.dispatch(push('/create_account'));
  };

  submitLoginForm = e => {
    e.preventDefault();

    if ((this.state.password && this.state.email) || this.state.submitData) {
      this.setState({
        submitData: true,
        loginText: 'Please wait <span>.</span><span>.</span><span>.</span>'
      });

      // Start Encryption of password in payload
      let encrypted_password = this.state.password;
      if (this.state.encryption_payload && JSEncrypt) {
        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(window.process.env.JsPublic);
        encrypted_password = encrypt.encrypt(this.state.password);
      }
      // End encryption of password in payload
      this.props
        .dispatch(newRequestLogin(this.state.email, encrypted_password, this.state.promptChecked))
        .then(response => {
          if (response.error && response.error === 'Invalid email or password.') {
            this.setState({
              showErrorMessage: true,
              loginText: 'Log In',
              submitData: false
            });
          } else if (
            response.error &&
            response.error === 'You have to confirm your account before continuing.'
          ) {
            this.setState({
              showConfirmationError: true,
              loginText: 'Log In',
              submitData: false
            });
          } else if (response.error && response.error === 'Your account is locked.') {
            this.setState({
              lockedError: `${response.error} Please, contact your Administrator to unlock it.`,
              loginText: 'Log In',
              submitData: false
            });
          } else {
            // This is required for EdCast -> Okta -> EdCast to create a remote session
            let urlStr1 = localStorage.getItem('afterLoginContentURL') || '';

            urlStr1 = urlStr1.replace(/^\/|\/$/g, '');

            if (this.state.upshotEnabled) {
              upshotActions.sendCustomEvent(window.UPSHOTEVENT['LOGIN'], {
                status: 'Success',
                email: this.state.email,
                event: 'Login'
              });
            }
            if (response.consumer_redirect_uri) {
              window.location.href = response.consumer_redirect_uri + urlStr1;
            }

            let webSessionTimeout =
              this.props.team &&
              this.props.team.config &&
              this.props.team.config.web_session_timeout;
            webSessionTimeout = webSessionTimeout ? webSessionTimeout * 60 * 1000 + Date.now() : 0;
            localStorage.setItem('webSessionTimeout', webSessionTimeout);
            if (window.opener) {
              /*eslint no-undef: "off"*/
              window.opener.postMessage(user, '*');
            }
          }
        })
        .catch(err => {
          console.error(`Error in LoginForm.newRequestLogin.func: ${err}`);
        });
    }
  };

  emailValidation = e => {
    let email = e.target.value;
    if (
      email === '' ||
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      this.setState({
        showInvalidEmailMessage: true,
        disableCreateButton: true,
        showErrorMessage: false,
        showConfirmationError: false,
        lockedError: '',
        email: ''
      });
      this.styles.emailMarginBottom.marginBottom = '0px';
    } else {
      this.setState({
        showConfirmationError: false,
        lockedError: '',
        showInvalidEmailMessage: false,
        showErrorMessage: false,
        email
      });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
    if (e.type == 'change') {
      this.setState({ showInvalidEmailMessage: false });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
  };

  passwordHandle = e => {
    let password = e.target.value;
    if (!!trim(password)) {
      this.setState({
        showErrorMessage: false,
        lockedError: '',
        showConfirmationError: false,
        password
      });
    } else {
      this.setState({
        showConfirmationError: false,
        lockedError: '',
        showErrorMessage: false,
        password: ''
      });
    }
  };

  getAutoSsoUrl() {
    // Automatic redirection to SSO
    if (
      (includes(window.location.search, 'auto_sso=true') || window.location.pathname == '/') &&
      this.props.team.config &&
      this.props.team.config['feature/auto_sso_login']
    ) {
      if (this.props.team.ssos) {
        var sso = find(this.props.team.ssos, el => {
          return el.name == this.props.team.config['feature/auto_sso_login'];
        });
        if (sso) {
          let urlStr = localStorage.getItem('afterLoginContentURL')
            ? '&origin=' +
              'https://' +
              window.location.hostname +
              localStorage.getItem('afterLoginContentURL') +
              '?clearURL=true'
            : '';
          return sso.webAuthenticationUrl + urlStr;
        }
      }
    }
    return null;
  }

  handlePromptChange = e => {
    let newValue = e.currentTarget.checked;
    this.setState({
      promptChecked: newValue
    });
  };

  getSSOarray = mainSSO => {
    let sso = [];
    for (let i = 0; i < mainSSO.length; i++) {
      if (mainSSO[i].ssoName != 'email') {
        sso.push(mainSSO[i]);
      }
    }
    return sso;
  };

  firstFormInit = e => {
    if (!this.state.isFormLoading) {
      return;
    }
    if (this.emailInput.value.length > 1) {
      this.passwordHandle(e);
    }
    this.setState({ isFormLoading: false });
  };

  redirectToSSO = link => {
    window.location = link;
  };

  promptTOC = () => {
    this.setState({ tocRed: true }, () => {
      setTimeout(() => {
        this.setState({ tocRed: false });
      }, 1000);
    });
  };

  render() {
    let { field } = this.props;

    // Automatic redirection to SSO
    var autoSsoUrl = this.getAutoSsoUrl();
    if (autoSsoUrl !== null) {
      window.location = autoSsoUrl;
      return <div />;
    }
    let customStyle = this.state.config;
    let SsoArray = this.getSSOarray(this.props.team.ssos);
    if (this.state.showNewLoginPage) {
      SsoArray = _.sortBy(SsoArray, 'position');
    }

    let loginButtonColor =
      (this.state.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/loginButtonColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/loginButtonColor'][field] ||
              this.pageConfiguration['web/login/loginButtonColor'].defaultValue
          }
        : customStyle.loginButtonStyle && customStyle.loginButtonStyle.styles) || {};

    let signUpButtonColor =
      (this.state.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/otherButtonsColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/otherButtonsColor'][field] ||
              this.pageConfiguration['web/login/otherButtonsColor'].defaultValue
          }
        : customStyle.sigupButtonStyle && customStyle.sigupButtonStyle.styles) || {};

    let ssoButtonColor =
      (this.state.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/otherButtonsColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/otherButtonsColor'][field] ||
              this.pageConfiguration['web/login/otherButtonsColor'].defaultValue
          }
        : customStyle.ssoButtonStyle && customStyle.ssoButtonStyle.styles) || {};

    let messageUnderLogIn = this.state.newLoginPageSettings
      ? this.pageConfiguration &&
        this.pageConfiguration['web/login/messageBelowLogIn'] &&
        (this.pageConfiguration['web/login/messageBelowLogIn'].value ||
          this.pageConfiguration['web/login/messageBelowLogIn'].defaultValue)
      : 'When you Sign up or Log in, you agree to the EdCast User Agreement, Privacy Policy and Cookie Policy.';

    return (
      <div className="login-form">
        <div
          style={Object.assign(
            this.styles.loginColor,
            (customStyle.formHeadingColor && customStyle.formHeadingColor.styles) || {}
          )}
        >
          {this.state.preRegistration ? tr('Existing Members Login:') : tr('Login')}
        </div>
        <form onSubmit={this.submitLoginForm} onChange={this.firstFormInit}>
          <div className="text-fields-container">
            <div>
              <input
                className="text-field"
                ref={input => {
                  this.emailInput = input;
                }}
                style={Object.assign(
                  { ...this.styles.emailMarginBottom },
                  (customStyle.textBox && customStyle.textBox.styles) || {}
                )}
                type="text"
                placeholder={tr('Email')}
                onChange={this.emailValidation}
                onBlur={this.emailValidation}
                required
              />
              {this.state.showInvalidEmailMessage && (
                <p style={this.styles.errorMessage}>{tr('Please enter a valid email')}</p>
              )}
            </div>
            <div>
              <input
                className="text-field"
                style={Object.assign(
                  { marginBottom: '5px' },
                  (customStyle.textBox && customStyle.textBox.styles) || {}
                )}
                type="password"
                placeholder={tr('Password')}
                onChange={this.passwordHandle}
                onBlur={this.passwordHandle}
                required
              />
            </div>
            <div className="forget-your-password" style={{ marginBottom: '15px' }}>
              {this.state.showErrorMessage && (
                <p style={this.styles.errorMessage}>{tr(this.state.errorMessage)}</p>
              )}
              <a
                onClick={() => {
                  this.props.dispatch(push('/forgot_password'));
                }}
                style={(customStyle.forgotPassLink && customStyle.forgotPassLink.styles) || {}}
              >
                {tr('forgot your password?')}
              </a>
            </div>
          </div>
          <button
            className="login-page-button"
            style={Object.assign(this.styles.loginButtonColor, loginButtonColor)}
            disabled={this.state.submitData}
          >
            <span
              className="button-loader"
              dangerouslySetInnerHTML={{ __html: tr(this.state.loginText) }}
            />
          </button>
          {this.state.showConfirmationError && (
            <p style={this.styles.errorMessage}>
              {tr('You have to confirm your account before continuing.')}
            </p>
          )}
          {this.state.lockedError && (
            <p style={this.styles.errorMessage}>{tr(this.state.lockedError)}</p>
          )}
        </form>

        <div
          className="signup-page-message"
          style={(customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}}
        >
          {this.state.acceptancePromptEnabledForLogin !== null &&
          this.state.acceptancePromptEnabledForLogin == 'true' ? (
            <div
              className="signup-page-message"
              style={(customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}}
            >
              <input
                type="checkbox"
                checked={this.state.promptChecked}
                onChange={this.handlePromptChange}
              />
              &nbsp;&nbsp;
              <span
                dangerouslySetInnerHTML={{ __html: tr(this.state.acceptancePromptMessage) }}
                style={this.state.tocRed ? { color: '#F1736A' } : {}}
              />
            </div>
          ) : (
            <div
              className="signup-page-message"
              style={(customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}}
            >
              {tr(messageUnderLogIn)}{' '}
            </div>
          )}
          {this.state.preRegistration && (
            <p
              style={{
                lineHeight: '1.8',
                color: 'rgb(255, 255, 255)',
                marginBottom: '4px',
                fontSize: '14px',
                fontWeight: 'bold',
                textAlign: 'left',
                marginTop: '20px'
              }}
            >
              {tr('New Members Interested?')}
            </p>
          )}
        </div>

        {!this.state.preRegistration &&
          this.props.team.config &&
          this.props.team.config.hasOwnProperty('AllowedEmailDomains') &&
          this.props.team.config['AllowedEmailDomains'] !== 'none' && (
            <button
              className="login-page-button"
              style={Object.assign(this.styles.signupButtonColor, signUpButtonColor)}
              onClick={this.goToSignUp}
            >
              <span>{tr('sign up')}</span>
            </button>
          )}

        {this.state.preRegistration && (
          <div>
            <button
              className="login-page-button"
              style={Object.assign(
                this.styles.signupButtonColor,
                (customStyle.sigupButtonStyle && customStyle.sigupButtonStyle.styles) || {}
              )}
              onClick={this.preRegister.bind(this)}
            >
              <span>{tr('write to us')}</span>
            </button>
          </div>
        )}
        {this.state.config.note && (
          <div style={this.styles.note}>
            <strong>
              <u>{tr('Note:')}</u>
            </strong>{' '}
            {this.state.config.note}
          </div>
        )}

        {SsoArray.length > 0 && (
          <div>
            <div
              style={Object.assign(
                this.styles.or,
                (customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}
              )}
            >
              {tr('or login with')}
            </div>
            {SsoArray.map((sso, index) => {
              let urlStr = localStorage.getItem('afterLoginContentURL')
                ? '&origin=' +
                  'https://' +
                  window.location.hostname +
                  localStorage.getItem('afterLoginContentURL')
                : '';
              let link =
                sso.webAuthenticationUrl + urlStr + ('&terms_accepted=' + this.state.promptChecked);

              let buttonLabel =
                sso.labelName == '' || sso.labelName == sso.name
                  ? 'Login with ' + ssos[sso.name].label
                  : sso.labelName;
              return (
                <button
                  onClick={e => {
                    this.redirectToSSO(link);
                  }}
                  className="login-page-button"
                  style={Object.assign(this.styles.socialShareButton, ssoButtonColor || {})}
                >
                  <div
                    className="text-social-share"
                    style={{
                      color:
                        (customStyle.ssoButtonStyle && customStyle.ssoButtonStyle.styles.color) ||
                        '#56e7ce'
                    }}
                  >
                    {tr(buttonLabel)}
                  </div>
                  <SsoIcons
                    ssoOption={sso.name}
                    color={
                      (customStyle.ssoButtonStyle && customStyle.ssoButtonStyle.styles.color) ||
                      '#56e7ce'
                    }
                    style={{
                      height: '16px',
                      width: '23px',
                      fill:
                        (customStyle.ssoButtonStyle && customStyle.ssoButtonStyle.styles.color) ||
                        '#56e7ce'
                    }}
                  />
                </button>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

LoginForm.propTypes = {
  coBrandingLogo: PropTypes.string,
  field: PropTypes.string,
  team: PropTypes.object
};

export default connect(state => {
  return { currentUser: state.currentUser.toJS(), team: state.team.toJS() };
})(LoginForm);
