import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ReactPlayer from 'react-player';
import Loadable from 'react-loadable';
import hexToRGB from '../../utils/hexToRGB';

import SignUpForm from './SignUpForm';
import PreRegisterContainer from './PreRegisterContainer';

import Logo from '../TopNav/Logo';

import { tr } from 'edc-web-sdk/helpers/translations';

const LoginForm = Loadable({
  loader: () => import('./LoginForm'),
  loading: () => null
});

class LoginContainerV2 extends Component {
  constructor(props, context) {
    super(props, context);
    let config =
      (this.props.team.config.onboardingMicroserviceConfig &&
        this.props.team.config.onboardingMicroserviceConfig.configs &&
        this.props.team.config.onboardingMicroserviceConfig.configs.loginScreen) ||
      {};
    this.state = {
      config,
      enableEdcastLogo:
        this.props.team.config && this.props.team.config.enable_edcast_logo !== undefined
          ? this.props.team.config.enable_edcast_logo
          : true,
      coBrandingLogo: this.props.team && this.props.team.coBrandingLogo,
      videoUrl: Array.isArray(config.videoUrl) ? config.videoUrl[0] : config.videoUrl,
      updateColours: window.ldclient.variation('update-header-colours', false)
    };

    this.styles = {
      edLogoStyle: {
        margin: 'auto auto',
        height: '1.6875rem',
        width: '50px'
      },
      orgLogoStyle: {
        height: '2.8125rem',
        width: '8.25rem'
      },
      listIconStyle: {
        height: '48.8px',
        width: '50px'
      },
      leftArrow: {
        borderStyle: 'solid',
        borderColor:
          (this.state.config.formHeadingColor && this.state.config.formHeadingColor.styles.color) ||
          '#fff',
        borderImage: 'initial',
        borderWidth: '0 2px 2px 0',
        display: 'inline-block',
        padding: '4px',
        transform: 'rotate(135deg)',
        WebkitTransform: 'rotate(135deg)',
        position: 'absolute',
        top: '53px',
        left: '30px',
        cursor: 'pointer'
      },
      mainContentContainer: {
        margin: '0px',
        background:
          'linear-gradient(rgba(69, 68, 96, 0.8), rgba(69, 68, 96, 0.74)) center center / 100% no-repeat, url("https://s3.amazonaws.com/edc-dev-web/assets/signin-signup-background-image.jpg")',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundSize: '100%',
        height: '540px',
        color: '#ffffff'
      }
    };

    this.defaultConfigText = {
      heading: 'SkillSET: Sustainable Education and Training for the Digital Economy.',
      bodyText1:
        'An IT Industry Initiative in collaboration with the Future of Education, Gender and Work System of the World Economic Forum.',
      bodyText2: 'Let the Journey Begin.',
      bodyText3:
        'Micro learning content to stay engaged daily/ weekly and personalized learning paths for your chosen skills goal with certified courses.',
      bodyText4:
        'No cost for setting up the  account and free access to our library of content contributed by our partners and a unique digital-fitness assessment that can recommend skills based on your aptitude.'
    };

    // footer options to get TOS & Privacy Policy links
    this.footerOptions =
      this.props.team && this.props.team.config && this.props.team.config.footer_options;

    this.customStyle = this.state.config;

    this.newLoginPageSettings = window.ldclient.variation('new-login-page-settings', false);

    this.pageConfiguration =
      this.props.team.config &&
      this.props.team.config.OrgCustomizationConfig &&
      this.props.team.config.OrgCustomizationConfig.web &&
      this.props.team.config.OrgCustomizationConfig.web.login;
    this.splashImage = this.props.team.config && this.props.team.config.custom_splash_screen;

    this.field = this.state.updateColours ? 'value' : 'defaultValue'; //use for colors
    this.backgroundColor =
      (this.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/backgroundColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/backgroundColor'][this.field] ||
              this.pageConfiguration['web/login/backgroundColor'].defaultValue
          }
        : this.customStyle.bodyBackgroundColor && this.customStyle.bodyBackgroundColor.styles) ||
      {};

    this.overlayColor =
      this.pageConfiguration &&
      (this.pageConfiguration['web/login/imageOverlayColor'][this.field] ||
        this.pageConfiguration['web/login/imageOverlayColor'].defaultValue) &&
      hexToRGB(
        this.pageConfiguration['web/login/imageOverlayColor'][this.field] ||
          this.pageConfiguration['web/login/imageOverlayColor'].defaultValue
      );
    this.mainContentContainerStyles =
      (this.newLoginPageSettings
        ? this.overlayColor && {
            background: `linear-gradient(rgba(${this.overlayColor[0]}, ${this.overlayColor[1]}, ${
              this.overlayColor[2]
            }, 0.8), rgba(${this.overlayColor[0]}, ${this.overlayColor[1]}, ${
              this.overlayColor[2]
            }, 0.74)) center center / 100% no-repeat, url("${this.splashImage ||
              'https://s3.amazonaws.com/edc-dev-web/assets/signin-signup-background-image.jpg'}")`
          }
        : this.customStyle.mainContentContainer && this.customStyle.mainContentContainer.styles) ||
      {};

    this.formContainerBackground =
      (this.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/rightPanelColor'] && {
            background:
              this.pageConfiguration['web/login/rightPanelColor'][this.field] ||
              this.pageConfiguration['web/login/rightPanelColor'].defaultValue
          }
        : this.customStyle.formBackground && this.customStyle.formBackground.styles) || {};

    this.leftSideText =
      (this.newLoginPageSettings
        ? {
            heading:
              this.pageConfiguration &&
              this.pageConfiguration['web/login/mainTitle'] &&
              this.pageConfiguration['web/login/mainTitle'].value,
            subTitle:
              this.pageConfiguration &&
              this.pageConfiguration['web/login/subTitle'] &&
              this.pageConfiguration['web/login/subTitle'].value
          }
        : this.customStyle.loginPageText) || this.defaultConfigText;
  }

  goToLogin = () => {
    this.props.dispatch(push('/log_in'));
  };

  changeVideoUrl = () => {
    if (!Array.isArray(this.state.config.videoUrl)) {
      return;
    }
    let videoUrlArr = (this.state.config && this.state.config.videoUrl) || [];
    let videoUrl = this.state.videoUrl;
    let index = videoUrlArr.indexOf(videoUrl);
    if (index < videoUrlArr.length - 1) {
      index++;
    } else {
      index = 0;
    }
    videoUrl = videoUrlArr[index];
    this.setState({ videoUrl });
  };

  render() {
    let customStyle = this.state.config;
    let pageLeftSection = (customStyle && customStyle.pageLeftSection) || 'text';
    return (
      <div id="login" style={this.backgroundColor}>
        <div className="row custom-row">
          <div className="small-8 columns small-header" style={{ padding: '0px' }}>
            <div className="logo-container">
              <Logo
                coBrandingLogo={this.state.coBrandingLogo}
                withCoBrandingDivStyle={{}}
                enableEdcastLogo={this.state.enableEdcastLogo}
              />
            </div>

            {pageLeftSection == 'text' || this.newLoginPageSettings ? (
              <div
                className="custom-row header-list-container"
                style={Object.assign(
                  this.styles.mainContentContainer,
                  this.mainContentContainerStyles
                )}
              >
                <div className="small-12 columns main-header-container">
                  <div className="header-content">{this.leftSideText.heading}</div>
                </div>

                <div className="small-12 columns main-list-container">
                  {Object.keys(this.leftSideText).map((item, index) => {
                    if (item != 'heading') {
                      return (
                        <div className="list-content">
                          <span
                            className="list-content-description"
                            dangerouslySetInnerHTML={{ __html: tr(this.leftSideText[item]) }}
                          />
                        </div>
                      );
                    }
                  })}
                </div>
              </div>
            ) : (
              <div className="custom-row video-container" style={{ height: '540px' }}>
                <ReactPlayer
                  url={this.state.videoUrl}
                  playing={true}
                  muted={true}
                  loop={true}
                  controls={true}
                  onEnded={this.changeVideoUrl}
                  className="login-page-video-player"
                />
              </div>
            )}
          </div>

          <div className="small-4 columns login-small-4" style={{ padding: '0px' }}>
            <div className="login-container">
              <div className="form-container" style={this.formContainerBackground}>
                {window.location.pathname === '/create_account' && (
                  <i style={this.styles.leftArrow} onClick={this.goToLogin} title="Login" />
                )}
                {window.location.pathname !== '/create_account' &&
                  window.location.pathname !== '/pre_register' && <LoginForm field={this.field} />}
                {window.location.pathname === '/create_account' && (
                  <SignUpForm field={this.field} />
                )}
                {window.location.pathname === '/pre_register' && (
                  <i style={this.styles.leftArrow} onClick={this.goToLogin} title="Login" />
                )}
                {window.location.pathname === '/pre_register' && <PreRegisterContainer />}
              </div>
            </div>
          </div>

          <div className="small-12 columns copy-right">
            <span>
              {tr(`Copyright &copy; 2014 - %{year} EdCast Inc. All rights reserved.`, {year: new Date().getFullYear()})}
              |
              <a
                href={
                  (this.footerOptions && this.footerOptions.terms_of_service) ||
                  'https://www.edcast.com/corp/terms-of-service/'
                }
                style={{ color: '#56e7ce' }}
                target="_blank"
              >
                {tr('Terms of Use')}{' '}
              </a>{' '}
              |{' '}
              <a
                href={
                  (this.footerOptions && this.footerOptions.privacy_policy) ||
                  'https://www.edcast.com/corp/privacy-policy/'
                }
                target="_blank"
                style={{ color: '#56e7ce' }}
              >
                {tr('Privacy Policy')}
              </a>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

LoginContainerV2.propTypes = {
  coBrandingLogo: PropTypes.string,
  team: PropTypes.object
};

export default connect(state => {
  return { currentUser: state.currentUser.toJS(), team: state.team.toJS() };
})(LoginContainerV2);
