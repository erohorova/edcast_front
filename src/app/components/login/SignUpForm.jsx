import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import trim from 'lodash/trim';
import { Popover, PopoverAnimationVertical } from 'material-ui/Popover';
// import EdcastLogoPlus from 'edc-web-sdk/components/icons/EdcastLogoPlus';
import ssos from 'edc-web-sdk/components/ssos/index';
import { requestSignup } from 'edc-web-sdk/requests/users.v2';
import SsoIcons from 'edc-web-sdk/components/icons/SsoIcons';
import SignUpConfirmation from 'edc-web-sdk/components/icons/SignUpConfirmation';
import * as upshotActions from '../../actions/upshotActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class SignUpForm extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showInvalidEmailMessage: false,
      showInvalidPasswordMessage: false,
      showPopover: false,
      showToolTip: false,
      firstnameHint: false,
      lastnameHint: false,
      firstnameError: false,
      lastnameError: false,
      firstname: '',
      loginButtonEle: '',
      lastname: '',
      email: '',
      password: '',
      signUpText: 'create account',
      submitData: false,
      emailErrorMessage: tr('Please enter a valid email'),
      promptChecked:
        this.props.team.config.acceptancePromptEnabled == 'true'
          ? window.ldclient.variation('auto-check-tos', true)
          : true,
      acceptancePromptEnabled: this.props.team.config.acceptancePromptEnabled
        ? this.props.team.config.acceptancePromptEnabled
        : null,
      acceptancePromptMessage: this.props.team.config.acceptancePromptMessage
        ? this.props.team.config.acceptancePromptMessage
        : null,
      confirmation: false,
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      day: new Date().getDate(),
      dob: new Date().getFullYear() + '/' + new Date().getMonth() + 1 + '/' + new Date().getDate(),
      showInvalidAge: false,
      config:
        (this.props.team.config.onboardingMicroserviceConfig &&
          this.props.team.config.onboardingMicroserviceConfig.configs &&
          this.props.team.config.onboardingMicroserviceConfig.configs.loginScreen) ||
        {},
      lxpOAuth: window.ldclient.variation('enable-lxp-oauth-okta', false),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      newLoginPageSettings: window.ldclient.variation('new-login-page-settings', false),
      tocRed: false
    };

    this.styles = {
      signupColor: {
        color: '#f0f0f5',
        marginBottom: '28px',
        fontSize: '20px',
        fontWeight: 'bold'
      },
      loginButtonColor: {
        backgroundColor: '#56e7ce'
      },
      signupButtonColor: {
        backgroundColor: '#6f708b',
        boxShadow: '0 0 4px 0 rgba(38, 39, 59, 0.6)',
        margin: '0px 0px 15px 0px',
        color: '#56e7ce'
      },
      socialShareButton: {
        backgroundColor: '#6f708b',
        boxShadow: '0 0 4px 0 rgba(38, 39, 59, 0.6)',
        margin: '0px 0px 15px 0px',
        color: '#56e7ce'
      },
      or: {
        margin: '0px 0px 15px 0px',
        height: '16px',
        fontSize: '12px',
        fontWeight: '500',
        textAlign: 'center',
        color: '#d6d6e1'
      },
      emailMarginBottom: {
        marginBottom: '18px'
      },
      errorMessage: {
        float: 'left',
        color: '#F1736A',
        fontSize: '10px',
        margin: '1px 0 5px 0'
      },
      download: {
        width: '100%',
        height: '100%',
        padding: 0
      },
      tooltipStyles: {
        color: '#606063',
        backgroundColor: '#ffffff',
        left: '10%'
      },
      datePicker: {
        color: '#acadc1',
        width: '55px',
        height: '30px',
        borderRadius: '5px',
        backgroundColor: '#454560',
        boxShadow: '0 0 4px 0 rgba(0, 0, 0, 0.5)',
        border: '0px',
        fontSize: '12px',
        marginRight: '10px'
      },
      dateErrorMessage: {
        fontSize: '12px',
        letterSpacing: '1px',
        margin: '0px 0px 20px 0px',
        float: 'left',
        color: '#F1736A'
      }
    };

    this.pageConfiguration =
      this.props.team.config &&
      this.props.team.config.OrgCustomizationConfig &&
      this.props.team.config.OrgCustomizationConfig.web &&
      this.props.team.config.OrgCustomizationConfig.web.login;
  }

  componentDidMount() {
    this.setState({
      infoEle: ReactDOM.findDOMNode(this.refs.password),
      fnameTip: ReactDOM.findDOMNode(this.refs.firstname),
      lnameTip: ReactDOM.findDOMNode(this.refs.lastname)
    });
  }

  showPopover = () => {
    this.setState({
      showPopover: true
    });
  };

  showToolTip = refElement => {
    this.setState({
      loginButtonEle: ReactDOM.findDOMNode(this.refs[refElement]),
      showToolTip: !this.state.promptChecked
    });
  };

  emailValidation = e => {
    let email = e.target.value;
    if (
      email === '' ||
      !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        email
      )
    ) {
      this.setState({
        showInvalidEmailMessage: true,
        disableCreateButton: true,
        email: '',
        emailErrorMessage: tr('Please enter a valid email')
      });
      this.styles.emailMarginBottom.marginBottom = '0px';
    } else {
      this.setState({
        showInvalidEmailMessage: false,
        email
      });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
    if (e.type == 'change') {
      this.setState({ showInvalidEmailMessage: false });
      this.styles.emailMarginBottom = { marginBottom: '18px' };
    }
  };

  passwordHandle = e => {
    let password = e.target.value;
    let state = {};
    if (e.type == 'change') {
      this.setState({
        showPopover: !this.passwordValidation(password),
        showInvalidPasswordMessage: false
      });
    } else if (e.type == 'blur') {
      this.setState({
        showPopover: false,
        showInvalidPasswordMessage: !this.passwordValidation(password)
      });
    }
    if (this.passwordValidation(password)) {
      this.setState({ password });
    } else this.setState({ password: '' });
  };

  passwordValidation = password => {
    let passwordReg = new RegExp(
      '(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])(?=.{8,})'
    );
    if (!trim(password) || !passwordReg.test(password)) {
      return false;
    } else {
      return true;
    }
  };

  fnamelnameHandle = e => {
    let fieldName = e.target.name;
    let fieldValue = e.target.value;
    let fieldNameReg = /^[a-zA-Z0-9-_' ]+$/;
    let validInput = trim(fieldValue).search(fieldNameReg) > -1;
    this.setState({
      [fieldName]: validInput ? fieldValue : '',
      [fieldName + 'Hint']: e.type == 'change' && !validInput,
      [fieldName + 'Error']: e.type == 'blur' && !validInput
    });
  };

  handlePromptChange = e => {
    let newValue = e.currentTarget.checked;
    if (newValue) {
      if (this.state.upshotEnabled) {
        upshotActions.sendCustomEvent(window.UPSHOTEVENT['TERMS_CONDITIONS'], {
          status: 'Success',
          event: 'T & C Accepted'
        });
      }
    }
    this.setState({
      promptChecked: newValue
    });
  };

  submitSignupForm = e => {
    e.preventDefault();

    let isValidForm =
      !!(this.state.firstname && this.state.lastname && this.state.password && this.state.email) ||
      this.state.submitData ||
      !this.state.promptChecked;

    if (!this.state.promptChecked) {
      this.promptTOC();
      return;
    }
    if (isValidForm) {
      let dataObj = {
        user: {
          first_name: this.state.firstname,
          last_name: this.state.lastname,
          email: this.state.email,
          password: this.state.password,
          profile_attributes: {
            tac_accepted: this.state.promptChecked
          }
        }
      };
      this.setState({
        submitData: true,
        signUpText: 'Please wait <span>.</span><span>.</span><span>.</span>'
      });
      requestSignup(dataObj)
        .then(res => {
          this.setState({
            signUpText: 'create account',
            submitData: false
          });
          this.setState({
            confirmation: true
          });
          if (window.upshot && this.state.upshotEnabled) {
            window.upshot.updateUserProfile({
              firstName: this.state.firstname,
              lastName: this.state.lastname,
              email: this.state.email
            });
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['REGISTER'], {
              status: 'Success',
              email: this.state.email,
              event: 'Register'
            });
          }
        })
        .catch(res => {
          this.styles.emailMarginBottom.marginBottom = '0px';
          this.setState({
            emailErrorMessage: res.message,
            showInvalidEmailMessage: true,
            signUpText: 'create account',
            submitData: false
          });
          if (window.upshot && this.state.upshotEnabled) {
            window.upshot.updateUserProfile({
              firstName: this.state.firstname,
              lastName: this.state.lastname,
              email: this.state.email
            });
            upshotActions.sendCustomEvent(window.UPSHOTEVENT['REGISTER'], {
              status: 'Failure',
              email: this.state.email,
              event: 'Register'
            });
          }
        });
    }
  };

  iconFetch = icon => {
    return `<icon.charAt(0).toUpperCase() + icon.slice(1) style={{height: '16.7px', fill:"#9470C9"}}/>`;
  };

  dateChangeHandle = (dropDownSelect, e) => {
    if (dropDownSelect == 'month') {
      this.setState({
        month: e.target.value
      });
    }
    if (dropDownSelect == 'year') {
      this.setState({
        year: e.target.value
      });
    }
    if (dropDownSelect == 'day') {
      this.setState({
        day: e.target.value
      });
    }
    this.setState({
      dob: this.state.year + '/' + this.state.month + '/' + this.state.day
    });
  };

  BindDays = (month, year) => {
    if (month == 2) {
      return parseInt(year, 10) % 4 == 0 ? 29 : 28;
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
      return 30;
    } else {
      return 31;
    }
  };

  getAge = dateString => {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  };

  getSSOarray = mainSSO => {
    let sso = [];
    for (let i = 0; i < mainSSO.length; i++) {
      if (mainSSO[i].ssoName != 'email') {
        sso.push(mainSSO[i]);
      }
    }
    return sso;
  };

  redirectToSSO = link => {
    if (this.state.promptChecked) {
      window.location = link;
    } else {
      this.promptTOC();
    }
  };

  promptTOC = () => {
    this.setState({ tocRed: true }, () => {
      setTimeout(() => {
        this.setState({ tocRed: false });
      }, 1000);
    });
  };

  render() {
    let { field } = this.props;
    let isValidForm =
      !(this.state.firstname && this.state.lastname && this.state.password && this.state.email) ||
      this.state.submitData ||
      !this.state.promptChecked;
    let dropDownDays = [],
      dropDownYears = [];
    for (let i = 1; i <= this.BindDays(this.state.month, this.state.year); i++) {
      dropDownDays.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }
    for (let i = new Date().getFullYear() - 100; i <= new Date().getFullYear(); i++) {
      dropDownYears.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }
    let customStyle = this.state.config;
    let SsoArray = this.getSSOarray(this.props.team.ssos);

    let signUpButtonColor =
      (this.state.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/otherButtonsColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/otherButtonsColor'][field] ||
              this.pageConfiguration['web/login/otherButtonsColor'].defaultValue
          }
        : customStyle.sigupButtonStyle && customStyle.sigupButtonStyle.styles) || {};

    let ssoButtonColor =
      (this.state.newLoginPageSettings
        ? this.pageConfiguration &&
          this.pageConfiguration['web/login/otherButtonsColor'] && {
            backgroundColor:
              this.pageConfiguration['web/login/otherButtonsColor'][field] ||
              this.pageConfiguration['web/login/otherButtonsColor'].defaultValue
          }
        : customStyle.ssoButtonStyle && customStyle.ssoButtonStyle.styles) || {};

    return (
      <div className="login-form">
        {!this.state.confirmation ? (
          <div>
            <div
              style={Object.assign(
                this.styles.signupColor,
                (customStyle.formHeadingColor && customStyle.formHeadingColor.styles) || {}
              )}
            >
              {tr('Sign Up')}
            </div>
            <form onSubmit={this.submitSignupForm.bind(this)}>
              <div className="text-fields-container">
                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder="First Name"
                    name="firstname"
                    onChange={this.fnamelnameHandle}
                    onBlur={this.fnamelnameHandle}
                    required
                  />
                  <div ref="firstname" />
                  {this.state.firstnameError && (
                    <p style={this.styles.errorMessage}>{tr('First Name is invalid.')}</p>
                  )}
                </div>

                <div style={{ marginBottom: '18px' }}>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { marginBottom: '1px' },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="text"
                    placeholder="Last Name"
                    name="lastname"
                    onChange={this.fnamelnameHandle}
                    onBlur={this.fnamelnameHandle}
                    required
                  />
                  <div ref="lastname" />
                  {this.state.lastnameError && (
                    <p style={this.styles.errorMessage}>{tr('Last Name is invalid.')}</p>
                  )}
                </div>

                <div>
                  <input
                    className="text-field"
                    style={Object.assign(
                      { ...this.styles.emailMarginBottom },
                      (customStyle.textBox && customStyle.textBox.styles) || {}
                    )}
                    type="email"
                    placeholder="Email"
                    onChange={this.emailValidation}
                    onBlur={this.emailValidation}
                    required
                  />
                  {this.state.showInvalidEmailMessage && (
                    <p style={this.styles.errorMessage}>{tr(this.state.emailErrorMessage)}</p>
                  )}
                </div>

                <div>
                  <input
                    className="text-field"
                    style={(customStyle.textBox && customStyle.textBox.styles) || {}}
                    type="password"
                    placeholder="Password"
                    onFocus={this.showPopover}
                    onChange={this.passwordHandle}
                    onBlur={this.passwordHandle}
                    required
                  />
                  {this.state.showInvalidPasswordMessage && (
                    <p style={this.styles.errorMessage}>{tr('Password is invalid')}</p>
                  )}
                </div>
                <div ref="password" style={{ marginTop: '5px' }} />

                <Popover
                  open={this.state.showPopover}
                  anchorEl={this.state.infoEle}
                  anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  useLayerForClickAway={false}
                  canAutoPosition={false}
                  animated={false}
                  className="custom-popover"
                >
                  <div className="popover-title" style={{ width: '280px', fontSize: '12px' }}>
                    <ul>
                      <li>{tr('Use at least 8 characters.')}</li>
                      <li>{tr('At least one letter should be capital.')}</li>
                      <li>{tr('Include at least a number and symbol (#?!@$%^&*-).')}</li>
                      <li>{tr('Password is case sensitive.')}</li>
                    </ul>
                  </div>
                </Popover>
                <Popover
                  open={this.state.firstnameHint}
                  anchorEl={this.state.fnameTip}
                  anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  useLayerForClickAway={false}
                  canAutoPosition={false}
                  animated={false}
                  className="custom-popover"
                >
                  <div className="popover-title" style={{ width: '310px', fontSize: '12px' }}>
                    {tr('First Name cannot contain the following characters:')} <br />+ . ( ) [ ] ;
                    ! @ \ / : * ? &quot; # % &lt; &gt; | ~ &amp;
                  </div>
                </Popover>
                <Popover
                  open={this.state.lastnameHint}
                  anchorEl={this.state.lnameTip}
                  anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                  targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                  useLayerForClickAway={false}
                  canAutoPosition={false}
                  animated={false}
                  className="custom-popover"
                >
                  <div className="popover-title" style={{ width: '310px', fontSize: '12px' }}>
                    {tr('Last Name cannot contain the following characters:')} <br />+ . ( ) [ ] ; !
                    @ \ / : * ? &quot; # % &lt; &gt; | ~ &amp;
                  </div>
                </Popover>
              </div>

              {this.state.acceptancePromptEnabled !== null &&
              this.state.acceptancePromptEnabled == 'true' ? (
                <div
                  className="signup-page-message"
                  style={(customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}}
                >
                  <input
                    type="checkbox"
                    checked={this.state.promptChecked}
                    onChange={this.handlePromptChange}
                  />
                  &nbsp;&nbsp;
                  <span
                    dangerouslySetInnerHTML={{ __html: this.state.acceptancePromptMessage }}
                    style={this.state.tocRed ? { color: '#F1736A' } : {}}
                  />
                </div>
              ) : (
                <div
                  className="signup-page-message"
                  style={(customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}}
                >
                  {tr(
                    'By clicking on create account, you agree to the EdCast User Agreement, Privacy Policy and Cookie Policy.'
                  )}
                </div>
              )}
              {this.state.showInvalidAge && (
                <div style={this.styles.dateErrorMessage}>
                  {tr(
                    'Sorry, we are not able to process your registration! You need to be above 13 years of age to sign up'
                  )}
                </div>
              )}
              <button
                className="login-page-button"
                style={Object.assign(this.styles.signupButtonColor, signUpButtonColor)}
              >
                <span
                  className="button-loader"
                  dangerouslySetInnerHTML={{ __html: tr(this.state.signUpText) }}
                />
              </button>
              {SsoArray.length > 0 && (
                <div>
                  <div
                    style={Object.assign(
                      this.styles.or,
                      (customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}
                    )}
                  >
                    {tr('or login with')}
                  </div>
                  <Popover
                    open={this.state.showToolTip}
                    anchorEl={this.state.loginButtonEle}
                    anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    targetOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    useLayerForClickAway={false}
                    canAutoPosition={false}
                    animated={false}
                    className="custom-popover"
                    style={{
                      marginLeft: -20,
                      marginTop: -40,
                      padding: 10
                    }}
                  >
                    <div style={{ fontSize: '12px' }}>
                      {tr('Please accept terms and conditions before signing in.')}
                    </div>
                  </Popover>
                  {SsoArray.map((sso, index) => {
                    let link =
                      sso.webAuthenticationUrl + ('&terms_accepted=' + this.state.promptChecked);
                    let refName = 'ssoName_' + index;
                    let buttonLabel =
                      sso.labelName == '' || sso.labelName == sso.name
                        ? 'Sign Up with ' + ssos[sso.name].label
                        : sso.labelName;
                    return (
                      <button
                        type="button"
                        className="login-page-button"
                        style={Object.assign(this.styles.socialShareButton, ssoButtonColor)}
                      >
                        <div
                          className="text-social-share"
                          style={{
                            color:
                              (customStyle.ssoButtonStyle &&
                                customStyle.ssoButtonStyle.styles.color) ||
                              '#56e7ce'
                          }}
                          onClick={e => {
                            this.redirectToSSO(link);
                          }}
                        >
                          {buttonLabel}
                        </div>
                        <SsoIcons
                          ssoOption={sso.name}
                          color={
                            (customStyle.ssoButtonStyle &&
                              customStyle.ssoButtonStyle.styles.color) ||
                            '#56e7ce'
                          }
                          style={{
                            height: '16px',
                            width: '23px',
                            fill:
                              (customStyle.ssoButtonStyle &&
                                customStyle.ssoButtonStyle.styles.color) ||
                              '#56e7ce'
                          }}
                        />
                      </button>
                    );
                  })}
                </div>
              )}
            </form>
          </div>
        ) : (
          <div className="confirmation">
            <div
              style={Object.assign(
                { margin: '118px auto 25px', fontSize: '20px', color: '#fff' },
                (customStyle.formHeadingColor && customStyle.formHeadingColor.styles) || {}
              )}
            >
              <p style={{ textTransform: 'capitalize' }}>
                Hi {this.state.firstname}&nbsp;{this.state.lastname},
              </p>
              <br />
              <p>{tr('Thanks for joining! Please check for our verification email.')}</p>
            </div>
            <div style={{ margin: '0px 0px 20px 0px', textAlign: 'center' }}>
              <SignUpConfirmation />
            </div>
            <div
              style={Object.assign(
                { color: '#d6d6e1', fontSize: '12px', margin: '0px 0px 175px 0px' },
                (customStyle.otherTextColor && customStyle.otherTextColor.styles) || {}
              )}
            >
              <img
                src="https://s3.amazonaws.com/edc-dev-web/assets/check-icon-sk.png"
                width="20"
                style={{ float: 'left', margin: '0px 5px 0px 0px' }}
              />
              <p>{tr('We have sent a verification email to your registered email address.')}</p>
              <br />
              <img
                src="https://s3.amazonaws.com/edc-dev-web/assets/check-icon-sk.png"
                width="20"
                style={{ float: 'left', margin: '0px 5px 0px 0px' }}
              />
              <p>{tr('Please click on the link inside to verify your account.')}</p>
            </div>
          </div>
        )}
      </div>
    );
  }
}

SignUpForm.propTypes = {
  coBrandingLogo: PropTypes.string,
  field: PropTypes.string,
  team: PropTypes.object
};

export default connect(state => {
  return { currentUser: state.currentUser.toJS(), team: state.team.toJS() };
})(SignUpForm);
