import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import radium from 'radium';
import colors from 'edc-web-sdk/components/colors';
import listMenu from '../../utils/orderMenu';
import { langs } from '../../constants/languages';

import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

import { tr } from 'edc-web-sdk/helpers/translations';

class MenuLinks extends Component {
  constructor(props, context) {
    super(props, context);

    this.linkClickHandler = this.linkClickHandler.bind(this);

    this.state = {
      isMenuLinksPopover: false,
      homePagev1: window.ldclient.variation('home-page-fix-v1', false)
    };

    this.styles = {
      navigationMenuIcon: {
        padding: '8px',
        marginLeft: '-12px'
      },
      menuLink: {
        paddingBottom: 0
      }
    };

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
  }

  linkClickHandler(e, link) {
    e.preventDefault();
    let path = link.url || link.defaultUrl;
    if (path.substr(0, 1) == '/') {
      if (this.props.pathname == path) {
        window.location.href = path;
      } else {
        this.props.dispatch(push(path));
      }
    } else {
      window.location = path;
    }
    this.handleRequestClose();
  }

  enterMenuLinkHandler = link => {
    if (link.options) {
      if (this._clearNavTimeout) {
        clearTimeout(this._clearNavTimeout);
      }
      this.setState({ hoveredNavBar: link.text, subNavOptions: link.options });
    }
  };

  leaveMenuLinkHandler = link => {
    this._clearNavTimeout = setTimeout(() => {
      this.setState({ hoveredNavBar: undefined, subNav: undefined });
    }, 333);
  };

  stillHoveringCallback = (bool, menuType) => {
    if (!bool) {
      this._clearNavTimeout = setTimeout(() => {
        this.setState({ hoveredNavBar: undefined, subNav: undefined });
      }, 333);
    } else if (bool) {
      this.setState({ subNav: menuType });
      if (this._clearNavTimeout) {
        clearTimeout(this._clearNavTimeout);
      }
    }
  };

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  toggleMenuLinksPopover = event => {
    this.setState({
      isMenuLinksPopover: !this.state.isMenuLinksPopover,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState({
      isMenuLinksPopover: false
    });
  };

  render() {
    let TopMenu = listMenu(this.props.team.OrgConfig.topMenu);
    let feedLinks = Object.keys(this.props.team.Feed).map(key => {
      let tab = this.props.team.Feed[key];
      if (tab.visible) {
        return key
          .split('/')
          .pop()
          .replace(/([A-Z])/g, '-$1')
          .toLowerCase();
      }
    });

    let customMenuColor =
      (this.props.customMenuStyle &&
        this.props.customMenuStyle.styles &&
        this.props.customMenuStyle.styles.color) ||
      colors.fontColorRedesign;

    return (
      <div className="menu-links-container">
        {!this.props.topNav && (
          <ul className={`menu ${this.state.homePagev1 ? 'fix-v1' : ''}`}>
            {TopMenu.map((link, i) => {
              let translatedLabel =
                this.isShowCustomLabels &&
                link.languages &&
                link.languages[this.profileLanguage] &&
                link.languages[this.profileLanguage].trim();
              // Get active path
              let isActive = false;
              if (
                this.props.pathname === '/' ||
                (link.defaultLabel === 'Home' && ~feedLinks.indexOf(this.props.pathname))
              ) {
                isActive =
                  (link.url || link.defaultUrl) === '/' ||
                  (link.defaultLabel === 'Home' && ~feedLinks.indexOf(this.props.pathname));
              } else {
                isActive =
                  (link.url || link.defaultUrl) !== '/' &&
                  ~this.props.pathname.indexOf(link.url || link.defaultUrl);
              }
              return (
                <li key={i}>
                  <a
                    href="#"
                    onClick={e => this.linkClickHandler(e, link)}
                    className={this.toCamelCase('topMenu ' + (link.label || link.defaultLabel))}
                    key={`topMenu-${i}`}
                    style={{
                      color: isActive ? customMenuColor || colors.secondary : customMenuColor,
                      fontWeight: isActive ? 'bold' : 'normal',
                      padding: 0,
                      cursor: 'default'
                    }}
                  >
                    <span
                      aria-label={translatedLabel || tr(link.label || link.defaultLabel)}
                      className="top-menu-link"
                    >
                      {translatedLabel ||
                        tr(
                          (link.label || link.defaultLabel) &&
                            (link.label || link.defaultLabel).toUpperCase()
                        )}
                    </span>
                  </a>
                </li>
              );
            })}
          </ul>
        )}
        {!this.props.topNav && (
          <div className="action-icon-wrapper" onClick={this.toggleMenuLinksPopover}>
            <IconButton style={this.styles.navigationMenuIcon}>
              <NavigationMenu size={26} />
            </IconButton>
            <Popover
              open={this.state.isMenuLinksPopover}
              anchorEl={this.state.anchorEl}
              anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
              targetOrigin={{ horizontal: 'left', vertical: 'top' }}
              onRequestClose={this.handleRequestClose}
              className={`menu-popover ${
                this.props.isFixed ? 'menu-popover_fixed-header' : ''
              } menu-popover_header-${this.props.headerVersion}`}
            >
              <Menu>
                {TopMenu.map((link, i) => {
                  let translatedLabel =
                    this.isShowCustomLabels &&
                    link.languages &&
                    link.languages[this.profileLanguage] &&
                    link.languages[this.profileLanguage].trim();
                  // Get active path
                  let isActive = false;
                  if (
                    this.props.pathname === '/' ||
                    (link.defaultLabel === 'Home' && ~feedLinks.indexOf(this.props.pathname))
                  ) {
                    isActive =
                      (link.url || link.defaultUrl) === '/' ||
                      (link.defaultLabel === 'Home' && ~feedLinks.indexOf(this.props.pathname));
                  } else {
                    isActive =
                      (link.url || link.defaultUrl) !== '/' &&
                      ~this.props.pathname.indexOf(link.url || link.defaultUrl);
                  }
                  return (
                    <MenuItem
                      className={`topMenu ${this.toCamelCase(
                        'topMenu ' + (link.label || link.defaultLabel)
                      )} ${isActive && 'topMenu__active'}`}
                      key={`topMenu-${i}`}
                      primaryText={translatedLabel || tr(link.label || link.defaultLabel)}
                      style={{
                        color: isActive ? colors.secondary : colors.fontColorRedesign
                      }}
                      onTouchTap={e => this.linkClickHandler(e, link)}
                    />
                  );
                })}
              </Menu>
            </Popover>
          </div>
        )}
      </div>
    );
  }
}

MenuLinks.propTypes = {
  handle: PropTypes.string,
  pathname: PropTypes.string,
  headerVersion: PropTypes.string,
  team: PropTypes.object,
  customMenuStyle: PropTypes.object,
  isFixed: PropTypes.bool,
  topNav: PropTypes.bool,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    handle: state.currentUser.toJS().handle,
    pathname: state.routing.locationBeforeTransitions.pathname,
    groups: state.groups.toJS(),
    users: state.users,
    userIds: state.discovery.toJS().userIds,
    suggestedChannelIds: state.discovery.toJS().channelIds,
    suggestedPathways: state.discovery.toJS().pathways,
    suggestedStreams: state.discovery.toJS().streams,
    suggestedCourses: state.discovery.toJS().courses,
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(radium()(MenuLinks));
