import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Logo from './Logo';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors';

class TopNavPublic extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.styles = {
      helpIcon: {
        width: '26px',
        height: '26px',
        color: colors.fontColorRedesign,
        fontWeight: 100
      }
    };
  }

  componentDidMount() {
    let bannerImage = `background-image: url(${
      this.props.team.bannerImage
    }); background-position: center`;
    this.setInlineCSS('#topNavPublic', bannerImage);
  }

  setInlineCSS(selector, cssString) {
    document.querySelectorAll(selector).forEach(function(item, index) {
      item.style.cssText += cssString;
    });
  }

  render() {
    return (
      <div
        className={`topnav-main-container-v4 ${this.props.isFixed ? '' : 'clearfix'} ${
          this.state.isSearchInputOpen ? 'search-input-block__active' : ''
        }`}
        id="topNavPublic"
      >
        <div className="top-nav-container-v4 top-nav-container-v4-public">
          <div className="topnav-logo-v4">
            <Logo
              coBrandingLogo={this.props.coBrandingLogo}
              enableEdcastLogo={this.props.enableEdcastLogo}
            />
          </div>
          <div className="public-log-link">
            <a href="/log_in">Login</a>
          </div>
        </div>
      </div>
    );
  }
}

TopNavPublic.propTypes = {
  coBrandingLogo: PropTypes.string,
  team: PropTypes.object,
  menuLinks: PropTypes.array,
  fixNav: PropTypes.bool,
  enableEdcastLogo: PropTypes.bool,
  isFixed: PropTypes.bool,
  topNav: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(TopNavPublic);
