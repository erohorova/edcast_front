import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import AddIcon from 'material-ui/svg-icons/content/add';
import { openInlineCreationModal } from '../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import CreateList from './CreateList';
import { langs } from '../../constants/languages';

class CreateInsightModal extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.modalOpenClick = this.modalOpenClick.bind(this);

    this.state = {
      isOpen: false,
      anchor: null,
      keypressed: true
    };

    this.styles = {
      list: {
        maxWidth: '350px',
        paddingBottom: '0',
        paddingTop: '0'
      },
      create: {
        marginTop: '-0.2rem',
        backgroundColor: colors.secondary,
        borderColor: colors.secondary,
        minWidth: '110px',
        borderRadius: '3px'
      }
    };

    this.labelCreate = Object.keys(this.props.team.OrgConfig.labels)
      .map(key => {
        return Object.assign({}, this.props.team.OrgConfig.labels[key], { key: key });
      })
      .filter(label => label.key == 'web/labels/create')[0];

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
    this.translatedLabel =
      this.isShowCustomLabels &&
      this.labelCreate.languages &&
      this.labelCreate.languages[this.profileLanguage] &&
      this.labelCreate.languages[this.profileLanguage].trim();
  }

  modalOpenClick() {
    this.props.dispatch(openInlineCreationModal());
  }

  handleClick(e) {
    e.preventDefault();
    this.setState({
      isOpen: !this.state.isOpen,
      anchor: e.currentTarget,
      keypressed: false
    });
  }

  handleClose(escPressed) {
    if (this.state.keypressed && escPressed === true) {
      this.refs.createButton.focus();
    }
    this.setState({
      isOpen: false,
      keypressed: true
    });
  }

  onKeyPressed = e => {
    if (e.keyCode == 27) {
      this.handleClose(e);
    } else if (e.keyCode == 13) {
      e.preventDefault();
      this.setState({
        isOpen: !this.state.isOpen,
        anchor: e.currentTarget,
        keypressed: true
      });
    }
  };
  render() {
    let isVisibleCreateGroup =
      this.props.team &&
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.labels &&
      this.props.team.OrgConfig.labels['web/labels/create_group'] &&
      this.props.team.OrgConfig.labels['web/labels/create_group'].visible;
    return (
      <a
        tabIndex={0}
        href="#"
        ref="createButton"
        className="action-icon-wrapper createButton"
        onClick={this.handleClick}
        onKeyDown={this.onKeyPressed}
      >
        <PrimaryButton
          label={
            this.translatedLabel || tr(this.labelCreate.label || this.labelCreate.defaultLabel)
          }
          style={this.styles.create}
          icon={<AddIcon focusable="false" />}
          className="createOpen"
          tabIndex={-1}
        />
        <CreateList
          open={this.state.isOpen}
          anchorEl={this.state.anchor}
          requestCloseHandler={this.handleClose}
          createGroup={isVisibleCreateGroup}
          handleClose={this.handleClose}
        />
      </a>
    );
  }
}

CreateInsightModal.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CreateInsightModal);
