{
  /* Notes:
   * Treat this component as a way to assemble the Navigation Bar.
   * Each object within the Naviagtion Bar should be treated as a separate components.
   * Keep the behavior of the object within the object of the component.
   * Preferrably use store to get all the data for the components.
   */
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import Logo from './Logo';
import MenuLinks from './MenuLinks';
import SearchV1 from './SearchV1';
import { Notifications } from 'blackbox-notifications';
import ProfileDropdown from './ProfileDropdown';
import { connect } from 'react-redux';
import CreateInsightModal from './CreateInsightModal';
import { Permissions } from '../../utils/checkPermissions';
import colors from 'edc-web-sdk/components/colors';
import MoreOptions from './MoreOptions';
import Help from 'edc-web-sdk/components/icons/Help.jsx';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';
import { tr } from 'edc-web-sdk/helpers/translations';
import MyGuide from 'edc-web-sdk/components/icons/MyGuide';
import handleGuideMeHelpClick from '../../utils/handleGuideMeHelpClick';
import detectHeaderWidth from '../../utils/detectHeaderWidth';

class TopNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDisplayMore: false,
      isSearchInputOpen: false,
      enableGuideMe: this.props.team && this.props.team.config.enable_guide_me,
      homePagev1: window.ldclient.variation('home-page-fix-v1', false),
      allowAlignSearchInHeader:
        window.ldclient.variation('allow-search-align-in-header', false) &&
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.align_search_in_header,
      shortSelectField: false,
      reduceHeaderSize: window.ldclient.variation('reduce-header-size', false)
    };
    this.styles = {
      helpIcon: {
        width: '26px',
        height: '26px',
        color: colors.fontColorRedesign,
        fontWeight: 100
      }
    };
  }

  componentWillMount() {
    let enableGuideMe = typeof this.state.enableGuideMe == 'boolean' && !this.state.enableGuideMe;
    if (enableGuideMe) {
      let css =
          '.gmClientstart-button, .gss-client-button, #gmClientlaunch-button{display: none !important;} #gmClient_launch-button{display: none !important;}',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
      style.type = 'text/css';
      if (style.styleSheet) {
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }
      head.appendChild(style);
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.setSelectView);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setSelectView);
  }

  setSelectView = debounce(() => {
    let value = detectHeaderWidth();
    if (value === this.state.shortSelectField) {
      return;
    }
    this.setState({ shortSelectField: value });
  }, 100);

  handleHelpClick = e => {
    e.preventDefault();
    handleGuideMeHelpClick(
      this.props.team &&
        this.props.team.config.guide_me_config &&
        this.props.team.config.guide_me_config.edition
    );
  };

  toggleSearchInput = () => {
    this.setState({ isSearchInputOpen: !this.state.isSearchInputOpen });
  };

  render() {
    let enableGuideMe =
      (typeof this.state.enableGuideMe == 'undefined' && !this.state.enableGuideMe) ||
      !!this.state.enableGuideMe;
    let imageUrl =
      this.props.user.avatar ||
      this.props.user.picture ||
      (this.props.user.avatarimages && this.props.user.avatarimages.medium) ||
      '';
    let isImageMissing = /anonymous-user/gi.test(imageUrl) || !imageUrl;
    return (
      <div
        className={`topnav-main-container-v2  ${
          this.state.isSearchInputOpen ? 'search-input-block__active' : ''
        } ${this.state.allowAlignSearchInHeader ? 'nav-search-align-container' : ''} ${
          this.state.reduceHeaderSize ? 'reducing-header' : ''
        }`}
      >
        <div className="top-nav-container-v2">
          <div className="topnav-logo-v2">
            <Logo
              coBrandingLogo={this.props.coBrandingLogo}
              enableEdcastLogo={this.props.enableEdcastLogo}
            />
          </div>
        </div>

        {!this.state.reduceHeaderSize && <div className="hidden-top-nav-container" />}

        <div
          className={`secondary-nav-container secondary-nav-container-v2  ${
            this.state.homePagev1 ? 'fix-v1' : ''
          } ${this.props.isFixed ? (this.state.reduceHeaderSize ? '' : 'fixed') : ''}`}
          id="secondary-nav"
        >
          {/*/!* Left side of the Navigation - start *!/*/}
          <div className={`left-menu-content`}>
            <MenuLinks
              menuLinks={this.props.menuLinks}
              topNav={this.props.topNav}
              isFixed={this.props.isFixed && !this.state.reduceHeaderSize}
              headerVersion="v2"
              queryTerm={this.props.queryTerm}
            />
          </div>

          <div
            className={`search-redesign-header search-input-block action-icon-list horizontal-spacing-large ${
              !this.state.allowAlignSearchInHeader ? 'ml-auto' : ''
            } icon ${this.state.homePagev1 ? 'fix-v1' : ''} search-v2`}
          >
            <SearchV1
              queryType={this.props.queryType}
              queryTerm={this.props.queryTerm}
              short={this.state.shortSelectField}
            />
          </div>
          {/* Right Side of the Navigation - start */}
          <div
            className={'search-redesign-header action-icon-list horizontal-spacing-large search-v1'}
          >
            <div className="action-icon-wrapper-redesign search-bnt-block">
              <div className="action-icon-wrapper">
                <IconButton tabIndex={0} onClick={this.toggleSearchInput}>
                  <SearchIcon focusable="false" className="action-icon" color={'#6f708b'} />
                </IconButton>
                <span className="username-redesign">{tr('Search')}</span>
              </div>
            </div>
            <div
              className={`action-icon-wrapper-redesign ${
                !isImageMissing ? 'user-name-redesign-ie' : ''
              }`}
            >
              <ProfileDropdown
                profile={this.props.profileDropdown}
                teams={this.props.teams}
                isAdmin={this.props.isAdmin}
              />
            </div>
            <div className="action-icon-wrapper-redesign">
              <Notifications homePagev1={this.state.homePagev1} />
            </div>
            {enableGuideMe && (
              <div className="action-icon-wrapper-redesign">
                <a
                  tabIndex={0}
                  className="action-icon-wrapper"
                  aria-label="Guide Me"
                  role="button"
                  href="#"
                  onClick={this.handleHelpClick}
                >
                  <IconButton tabIndex={-1} style={{ padding: '0' }}>
                    {this.state.homePagev1 ? (
                      <MyGuide
                        focusable="false"
                        viewBox={'10 0 31 33'}
                        className="action-icon"
                        fill="none"
                        color={'#6f708b'}
                      />
                    ) : (
                      <Help focusable="false" className="action-icon" color={'#6f708b'} />
                    )}
                  </IconButton>
                  <span className="username-redesign">GuideMe</span>
                </a>
              </div>
            )}
            <div className="action-icon-wrapper-redesign">
              <MoreOptions />
            </div>
            {Permissions.has('MANAGE_CARD') && (
              <div className="create-btn-container">
                <CreateInsightModal />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

TopNav.propTypes = {
  coBrandingLogo: PropTypes.string,
  menuLinks: PropTypes.array,
  fixNav: PropTypes.bool,
  queryTerm: PropTypes.string,
  queryType: PropTypes.string,
  profileDropdown: PropTypes.object,
  team: PropTypes.object,
  teams: PropTypes.array,
  user: PropTypes.object,
  enableEdcastLogo: PropTypes.bool,
  isAdmin: PropTypes.bool,
  isFixed: PropTypes.bool,
  topNav: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(TopNav);
