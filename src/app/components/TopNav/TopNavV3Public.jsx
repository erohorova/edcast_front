{
  /* Notes:
   * Treat this component as a way to assemble the Navigation Bar.
   * Each object within the Naviagtion Bar should be treated as a separate components.
   * Keep the behavior of the object within the object of the component.
   * Preferrably use store to get all the data for the components.
   */
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Logo from './Logo';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors';

class TopNavPublic extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.styles = {
      helpIcon: {
        width: '26px',
        height: '26px',
        color: colors.fontColorRedesign,
        fontWeight: 100
      }
    };
  }

  setInlineCSS(selector, cssString) {
    document.querySelectorAll(selector).forEach(function(item, index) {
      item.style.cssText += cssString;
    });
  }

  render() {
    return (
      <div
        className={`topnav-main-container-v3 ${this.props.isFixed ? '' : 'clearfix'} ${
          this.state.isSearchInputOpen ? 'search-input-block__active' : ''
        }`}
      >
        <div className="top-nav-container-v3 top-nav-container-v3-public">
          <div className="topnav-logo-v3">
            <Logo
              coBrandingLogo={this.props.coBrandingLogo}
              enableEdcastLogo={this.props.enableEdcastLogo}
            />
          </div>
          <div className="public-log-link">
            <a href="/log_in">Login</a>
          </div>
        </div>
      </div>
    );
  }
}

TopNavPublic.propTypes = {
  coBrandingLogo: PropTypes.string,
  menuLinks: PropTypes.array,
  fixNav: PropTypes.bool,
  enableEdcastLogo: PropTypes.bool,
  isFixed: PropTypes.bool,
  topNav: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(TopNavPublic);
