import { groupBy, map, debounce } from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';
import { search } from 'edc-web-sdk/requests/index';

import Select from 'react-select';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import { Menu, MenuItem, AsyncTypeahead } from 'react-bootstrap-typeahead';

const MenuDivider = props => <li className="divider" role="separator" />;
const MenuHeader = props => <li {...props} className="dropdown-header" />;
import { showLoadingWithReset, getSearchResults } from '../../actions/searchActions';

import { searchForUsers } from 'edc-web-sdk/requests/users.v2';
import { getSearchChannels } from 'edc-web-sdk/requests/channels.v2';
import { searchGroups } from 'edc-web-sdk/requests/groups.v2';
import { searchV2, orgSearch, publicSearch } from 'edc-web-sdk/requests/search';
import * as upshotActions from '../../actions/upshotActions';
import { saveConsumptionPathwayHistoryURL } from '../../actions/pathwaysActions';
import { saveConsumptionJourneyHistoryURL } from '../../actions/journeyActions';

class SearchV1 extends Component {
  constructor(props) {
    super(props);

    let isNewPeople =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.profile &&
      this.props.team.OrgConfig.profile['web/profile/teams'].visible &&
      window.ldclient.variation('new-people-search-filter', false);
    this.filterOptions = [
      { value: 'all', label: tr('All'), visible: true },
      {
        value: 'groups',
        label: tr('Groups'),
        visible: window.ldclient.variation('group-page-v2', false)
      },
      { value: 'content', label: tr('Content'), visible: true },
      { value: 'channels', label: tr('Channels'), visible: true },
      { value: 'skills', label: tr('People (By Skill)'), visible: isNewPeople },
      { value: 'name', label: tr('People (By Name)'), visible: isNewPeople },
      { value: 'user', label: tr('People'), visible: !isNewPeople }
    ];

    let currentSelect = props.queryType
      ? this.filterOptions.find(obj => obj.value === props.queryType)
      : {};

    this.state = {
      dataSource: [],
      sociativeSearch: window.ldclient.variation('search-with-sociative', false),
      homePagev1: window.ldclient.variation('home-page-fix-v1', false),
      searchWithDropdowns: window.ldclient.variation('header-search-with-dropdowns', false),
      detailedSearch: window.ldclient.variation('detailed-global-search', true),
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      currentOption:
        currentSelect && currentSelect.visible
          ? currentSelect
          : {
              value: 'all',
              label: tr('All')
            },
      shortSelect: false,
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };

    this.handleRedirect = this.handleRedirect.bind(this);
    // Debounce binds a function, you can't pass it into a function or it will call each time
    // If LD flag to disable-search-typeahead, just kill the request
    this._handleSearch = window.ldclient.variation('disable-search-typeahaed', false)
      ? () => {
          return [];
        }
      : debounce(this.props.isSmartSearchOn ? this._doSmartSearchQuery : this._doSearch, 700);
  }

  componentDidMount() {
    if (
      this.props.queryTerm &&
      (window.location.pathname == '/search' || window.location.pathname === '/smartsearch')
    ) {
      this.refs.searchTypeahead.getInstance().state.text = decodeURIComponent(this.props.queryTerm);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.queryTerm &&
      (window.location.pathname == '/search' || window.location.pathname === '/smartsearch')
    ) {
      this.refs.searchTypeahead.getInstance().state.text = decodeURIComponent(nextProps.queryTerm);
    } else {
      this.refs.searchTypeahead.getInstance().state.text = '';
    }
    if (this.props.short !== nextProps.short) {
      this.setState({ shortSelect: nextProps.short });
    }
  }

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;

    // If typeahead disabled, don't attemp to show loading
    // This is rarely used, so not creating multiple components to handle different options
    let hiddenAsyncDropdown = null;
    if (window.ldclient.variation('disable-search-typeahaed', false)) {
      hiddenAsyncDropdown = (
        <style>{`.search-input-container>.bootstrap-typeahead .bootstrap-typeahead-menu { display: none!important;}`}</style>
      );
    }
    let isSmartSearchOn = this.props.isSmartSearchOn;
    let inputWidth =
      this.state.searchWithDropdowns && !isSmartSearchOn ? 'calc(100% - 150px)' : '100%';
    return (
      <form
        onSubmit={this.handleRedirect.bind(this)}
        style={{ display: 'inline-block', position: 'relative' }}
      >
        <div
          className={`search-input-container ${
            this.state.searchWithDropdowns
              ? 'input-container_special-radius input-container_special-width'
              : ''
          } ${this.props.short ? 'search-input-container__wide' : ''}`}
          style={{ display: 'inline-block', position: 'relative', width: inputWidth }}
        >
          {hiddenAsyncDropdown}
          <AsyncTypeahead
            {...props}
            labelKey="name"
            style={{ color: '#fff', fontWeight: 'bold' }}
            options={this.state.dataSource}
            placeholder={
              this.state.homePagev1 || this.state.searchWithDropdowns
                ? tr('Search')
                : tr('Type the topic you want to learn about')
            }
            onSearch={this._handleSearch}
            onFocus={this._handleSearchClick}
            submitFormOnEnter={true}
            ref="searchTypeahead"
            filterBy={(option, text) => {
              return true;
            }}
          />
          <button
            tabIndex={-1}
            className="search-button"
            type="submit"
            onClick={this.handleRedirect.bind(this)}
          >
            <SearchIcon focusable="false" height={18} />
          </button>
        </div>
        {/* TODO: Not sure if we will add this back in -- !isSmartSearchOn && */ this.state
          .searchWithDropdowns && (
          <div
            className={`header__seacrh-filter ${
              this.props.short ? 'header__search-filter_small' : ''
            }`}
          >
            <Select
              cache={false}
              style={{ width: `${8.5 * this.state.currentOption.label.length + 27}px` }}
              onChange={this.filterChange.bind(this)}
              value={this.state.currentOption}
              options={this.filterOptions.filter(obj => obj.visible)}
              clearable={false}
              searchable={false}
            />
          </div>
        )}
      </form>
    );
  }

  filterChange = value => {
    this.setState(prevState => {
      return {
        currentOption: value,
        currentOptionPrev: prevState.currentOption.value,
        dataSource: []
      };
    });
  };

  _handleSearchClick = () => {
    let text = this.refs.searchTypeahead.getInstance().state.text;
    if (
      text &&
      text.length &&
      this.state.currentOptionPrev &&
      this.state.currentOptionPrev.value !== this.state.currentOption.value
    ) {
      let t = text.split('&');
      this._handleSearch(t[0]);
    }
  };

  handleRedirect = event => {
    let qvarInput = this.refs.searchTypeahead.getInstance().state.text.trim();
    event.preventDefault();

    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['SEARCH'], {
        keyword: qvarInput,
        event: 'Search'
      });
    }
    if (qvarInput && qvarInput !== '') {
      let type = this.state.currentOption.value;
      if (
        this.props.queryTerm === qvarInput &&
        ((this.state.searchWithDropdowns && this.props.queryType === type) ||
          !this.state.searchWithDropdowns)
      ) {
        let params = {
          q: encodeURIComponent(qvarInput),
          limit: 12,
          cards_limit: 39,
          sociative: this.state.sociativeSearch
        };
        let isResearch = true;
        if (this.state.searchWithDropdowns) {
          switch (type) {
            case 'content':
              params[`content_type[]`] = [
                'article',
                'video',
                'course',
                'book',
                'poll',
                'insight',
                'video_stream'
              ];
              break;
            case 'user':
              params[`content_type[]`] = ['user'];
              break;
            case 'channels':
              params[`content_type[]`] = ['channel'];
              isResearch = false;
              break;
            case 'groups':
            case 'name':
            case 'skills':
              isResearch = false;
              break;
            case 'all':
              return;
            default:
              break;
          }
        }
        this.setState({ q: params.q, loading: true });
        !this.props.isSmartSearchOn && this.props.dispatch(showLoadingWithReset(true));
        !this.props.isSmartSearchOn &&
          isResearch &&
          this.props.dispatch(getSearchResults(params, true, this.state.detailedSearch));
        return;
      }

      switch (type) {
        case 'all':
        case 'content':
          this.props.dispatch(
            push(
              `/${this.props.isSmartSearchOn ? 'smartsearch' : 'search'}?q=${encodeURIComponent(
                qvarInput
              )}`
            )
          );
          break;
        case 'name':
        case 'skills':
        case 'user':
          this.props.dispatch(push(`/me/team?q=${encodeURIComponent(qvarInput)}&type=${type}`));
          break;
        case 'groups':
          this.props.dispatch(
            push(`/org-groups/search?q=${encodeURIComponent(qvarInput)}&type=${type}`)
          );
          break;
        case 'channels':
          this.props.dispatch(push(`/channels/search?q=${encodeURIComponent(qvarInput)}`));
          break;
        default:
          this.props.dispatch(
            push(
              `/${this.props.isSmartSearchOn ? 'smartsearch' : 'search'}?q=${encodeURIComponent(
                qvarInput
              )}`
            )
          );
      }
      this.refs.searchTypeahead.getInstance().blur();
    } else {
      return;
    }
  };

  itemClick = item => {
    let path = this.props.isSmartSearchOn ? 'smartsearch' : 'search';
    let value = `/${path}?q=${item.name}`;
    switch (item.type) {
      case 'Card':
        let backUrl = window.location.pathname;
        if (item.cardType === 'pack') {
          value = `/pathways/${item.id}`;
          if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
            this.props.dispatch(saveConsumptionPathwayHistoryURL(backUrl));
          }
        } else if (item.cardType === 'journey') {
          value = `/journey/${item.id}`;
          if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
            this.props.dispatch(saveConsumptionJourneyHistoryURL(backUrl));
          }
        } else {
          value = item.shareUrl;
        }
        break;
      case 'User':
        value = `/${item.slug}`;
        break;
      case 'Channel':
        value = `/channel/${item.slug}`;
        break;
      default:
        // FIXME: implement default case
        break;
    }
    this.props.dispatch(push(value));
    this.refs.searchTypeahead.getInstance().blur();
  };

  _renderMenu = (results, menuProps) => {
    let idx = 0;
    const grouped = groupBy(this.state.dataSource, r => r.type);
    const items = Object.keys(grouped)
      .sort()
      .map(type => {
        return [
          !!idx && <MenuDivider key={`${type}-divider`} />,
          <MenuHeader key={`${type}-header`}>{tr(type === 'Card' ? 'Article' : type)}</MenuHeader>,
          map(grouped[type], eachItem => {
            const item = (
              <li key={idx}>
                <a onClick={this.itemClick.bind(this, eachItem)}>{tr(eachItem.name)}</a>
              </li>
            );
            idx++;
            return item;
          })
        ];
      });

    return <Menu {...menuProps}>{items}</Menu>;
  };

  stylizeCardTitle = card => {
    return card
      .replace(/(\*\*)(.*?)(\*\*)/g, '<strong>$2</strong>')
      .replace(/(\+\+)(.*?)(\+\+)/g, '<u>$2</u>')
      .replace(/(\_)(.*?)(\_)/g, '<em>$2</em>')
      .replace(/(\~\~)(.*?)(\~\~)/g, '<del>$2</del>')
      .replace(/(\[)(.*?)(\])(\()(.*?)(\))/g, '<a href=$5>$2</a>');
  };

  _doSmartSearchQuery = async query => {
    // localStorage set inin bootstrap.jsx window.booostrapOnboarding()
    let isDefaultSearchEnabled =
      localStorage.getItem('isDefaultSourceEnabled') === 'true' ? true : false;
    // Smartsearch specific
    let payload = {
      candidates: localStorage.getItem('candidates'),
      limit: 2,
      search_term: query
    };

    let option = this.state.currentOption.value || 'all';

    // Default empty arrays
    let orgCards = { cards: [] };
    let publicCards = { cards: [] };
    let orgResults = {
      users: [],
      channels: [],
      tags: []
    };
    let users = [];
    let channels = [];
    let groupResults = null;

    let allPromises = [];

    if (option === 'all' || option === 'content') {
      allPromises.push(orgSearch(payload));

      isDefaultSearchEnabled
        ? allPromises.push(publicSearch(payload))
        : allPromises.push(publicCards);
    } else {
      // Set default responses
      allPromises.push(orgCards);
      allPromises.push(publicCards);
    }

    if (option !== 'content') {
      allPromises.push(search.getSearchSuggestV2({ q: query, limit: 2 }));
    } else {
      allPromises.push(orgResults);
    }

    // Get groups separate API
    if (
      window.ldclient.variation('group-page-v2', false) &&
      (option === 'all' || option === 'groups')
    ) {
      allPromises.push(searchGroups(payload));
    } else {
      allPromises.push(groupResults);
    }

    // Call all API requests at same time, speed up rendering
    [orgCards, publicCards, orgResults, groupResults] = await Promise.all(allPromises);

    // NOTE: Suggestions API doesn't support `content_type` param. So we query everything

    // Group any data we have together
    let orgCardsMap = orgCards.cards.map(card => {
      return {
        id: card.id,
        name: this.stylizeCardTitle(card.title),
        slug: card.id,
        shareUrl: `${document.location.origin}/insights/${card.id}`,
        type: 'Card',
        cardType: card.content_type
      };
    });

    let publicCardsMap = publicCards.cards.map(card => {
      return {
        id: card.id,
        name: this.stylizeCardTitle(card.title),
        slug: card.id,
        shareUrl: `${document.location.origin}/insights/${card.id}`,
        type: 'Card',
        cardType: card.content_type
      };
    });

    if (option === 'all' || option === 'name' || option === 'user' || option === 'skills') {
      users = orgResults.users.map(user => {
        return { id: user.id, name: user.name, slug: user.handle, type: 'User' };
      });
    }

    if (option === 'all' || option === 'channels') {
      channels = orgResults.channels.map(channel => {
        return { id: channel.id, name: channel.label, slug: channel.slug, type: 'Channel' };
      });
    }

    let tags = orgResults.tags.map(tag => {
      return { name: tag.name, slug: tag.name, type: ' ' };
    });

    let groups =
      groupResults &&
      groupResults.teams.map(item => {
        return { id: item.id, name: item.name, slug: item.slug, type: 'Group' };
      });

    let finalData = tags
      .concat([...orgCardsMap, ...publicCardsMap])
      .concat(channels)
      .concat(groups)
      .concat(users);
    this.setState({ dataSource: finalData });
  };

  _doSearch = query => {
    let that = this;
    if (!query) {
      return;
    }
    let type = this.state.currentOption.value;
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(window.UPSHOTEVENT['SEARCH'], {
        keyword: query,
        event: 'Search'
      });
    }

    if (this.state.searchWithDropdowns && type !== 'all') {
      let payload = {};
      switch (type) {
        case 'content':
          payload.q = query;
          payload.limit = 10;
          payload[`content_type[]`] = [
            'article',
            'video',
            'course',
            'book',
            'poll',
            'insight',
            'video_stream'
          ];
          search
            .getSearchSuggestV2(payload)
            .then(data => {
              let result = data.cards.map(item => {
                return {
                  id: item.id,
                  name: this.stylizeCardTitle(item.title || item.message),
                  slug: item.slug,
                  type: 'Content'
                };
              });
              that.setState({ dataSource: result });
            })
            .catch(err => {
              console.error(`Error in SearchV1.searchV2.func: ${err}`);
            });
          break;
        case 'name':
        case 'user':
          payload.q = query === '*' ? '' : query;
          payload.search_by = type;
          payload[`content_type[]`] = ['user'];
          search
            .getSearchSuggestV2(payload)
            .then(data => {
              let result = data.users.map(user => {
                return { id: user.id, name: user.name, slug: user.handle, type: 'User' };
              });
              that.setState({ dataSource: result });
            })
            .catch(err => {
              console.error(`Error in byName SearchV1.searchForUsers.func: ${err}`);
            });
          break;
        case 'skills':
          payload.q = query === '*' ? '' : query;
          payload.search_by = type;
          payload[`content_type[]`] = ['user'];
          search
            .getSearchSuggestV2(payload)
            .then(data => {
              let result = data.users.map(user => {
                return { id: user.id, name: user.name, slug: user.handle, type: 'User' };
              });
              that.setState({ dataSource: result });
            })
            .catch(err => {
              console.error(`Error in bySkill SearchV1.searchForUsers.func: ${err}`);
            });
          break;
        case 'channels':
          payload.q = query;
          payload.limit = 10;
          payload[`content_type[]`] = ['channel'];
          search
            .getSearchSuggestV2(payload)
            .then(data => {
              let result = data.channels.map(item => {
                return { id: item.id, name: item.label, slug: item.slug, type: 'Channel' };
              });
              that.setState({ dataSource: result });
            })
            .catch(err => {
              console.error(`Error in SearchV1.getSearchChannels.func: ${err}`);
            });
          break;
        case 'groups':
          payload.q = query;
          payload.limit = 10;
          payload[`content_type[]`] = ['team'];
          search
            .getSearchSuggestV2(payload)
            .then(data => {
              let result = data.teams.map(item => {
                return { id: item.id, name: item.name, slug: item.slug, type: 'Group' };
              });
              that.setState({ dataSource: result });
            })
            .catch(err => {
              console.error(`Error in SearchV1.searchGroups.func: ${err}`);
            });
          break;
        default:
          break;
      }
    } else {
      search
        .getSearchSuggestV2({ q: query, limit: 2 })
        .then(data => {
          let users = data.users.map(user => {
            return { id: user.id, name: user.name, slug: user.handle, type: 'User' };
          });
          let cards = data.cards.map(card => {
            return {
              id: card.id,
              name: this.stylizeCardTitle(card.title || card.message),
              slug: card.slug,
              shareUrl: card.shareUrl,
              type: 'Card',
              cardType: card.cardType
            };
          });
          let channels = data.channels.map(channel => {
            return { id: channel.id, name: channel.label, slug: channel.slug, type: 'Channel' };
          });
          let tags = data.tags.map(tag => {
            return { name: tag.name, slug: tag.name, type: ' ' };
          });
          let finalData = tags
            .concat(cards)
            .concat(channels)
            .concat(users);
          that.setState({ dataSource: finalData });
        })
        .catch(err => {
          console.error(`Error in SearchV1.getSearchSuggestV2.func: ${err}`);
        });
    }
  };
}

SearchV1.propTypes = {
  queryTerm: PropTypes.string,
  queryType: PropTypes.string,
  isSmartSearchOn: PropTypes.bool,
  team: PropTypes.object,
  short: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(SearchV1);
