import React, { Component } from 'react';
import PropTypes from 'prop-types';
import EdcFullLogo from 'edc-web-sdk/components/icons/EdcFullLogo';
import EdcastLogoPlus from 'edc-web-sdk/components/icons/EdcastLogoPlus';
import { connect } from 'react-redux';
import listMenu from '../../utils/orderMenu';
class Logo extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      imgContainer: {
        display: 'flex'
      },
      edLogo: {
        height: '28px',
        width: 'auto'
      },
      edLogoPlus: {
        height: '50px',
        width: '62px',
        margin: 'auto auto'
      },
      edLogoImage: {
        paddingLeft: '0px',
        margin: '0px 0px 0px 2px'
      }
    };
  }

  render() {
    let edLogo = null;
    let isOrgANZ =
      this.props.team.hostName == 'anzswitch' && this.props.team.name.indexOf('ANZ') >= 0;
    if (!this.props.coBrandingLogo && this.props.showEdcastLogo) {
      edLogo = <EdcastLogoPlus style={this.styles.edLogoPlus} />;
    } else if (!this.props.coBrandingLogo) {
      edLogo = <EdcFullLogo style={this.styles.edLogo} />;
    }
    return (
      <div className="logo">
        <a
          // TODO do away with hardcoded strings -- create field for orgs to set their slogan/aria-labels?
          aria-label={isOrgANZ ? 'Our way of learning' : 'go to home page'}
          href={listMenu(this.props.team.OrgConfig.topMenu)[0].defaultUrl}
        >
          {this.props.coBrandingLogo && (
            <div
              style={
                this.props.withCoBrandingDivStyle
                  ? this.props.withCoBrandingDivStyle
                  : this.styles.imgContainer
              }
            >
              {this.props.enableEdcastLogo && <EdcastLogoPlus style={this.styles.edLogoPlus} />}
              <img style={this.styles.edLogoImage} src={this.props.coBrandingLogo} alt=" " />
            </div>
          )}
          {edLogo}
        </a>
      </div>
    );
  }
}

Logo.propTypes = {
  withCoBrandingDivStyle: PropTypes.object,
  team: PropTypes.object,
  enableEdcastLogo: PropTypes.bool,
  isV3: PropTypes.string,
  coBrandingLogo: PropTypes.string,
  showEdcastLogo: PropTypes.string
};

function mapStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}
export default connect(mapStateToProps)(Logo);
