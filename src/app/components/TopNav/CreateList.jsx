import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { setTimeout } from 'timers';

import { List, ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';

import colors from 'edc-web-sdk/components/colors';
import PathwayIcon from 'edc-web-sdk/components/icons/Pathway';
import SmartbiteIcon from 'edc-web-sdk/components/icons/SmartBite';
import GroupIcon from 'edc-web-sdk/components/icons/GroupIcon';
import SmartbiteActiveIcon from 'edc-web-sdk/components/icons/SmartBiteActiveIcon';
import PathwayActiveIcon from 'edc-web-sdk/components/icons/PathwayActiveIcon';
import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import WalkThrough from 'edc-web-sdk/components/icons/WalkThrough';
import Journey from 'edc-web-sdk/components/icons/Journey';
import JourneyActive from 'edc-web-sdk/components/icons/JourneyActive';

import {
  openGroupCreationModal,
  openSmartBiteCreationModal,
  openPathwayCreationModal,
  openJourneyCreationModal
} from '../../actions/modalActions';
import { Permissions } from '../../utils/checkPermissions';

class CreateList extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      list: {
        maxWidth: '350px',
        padding: 0
      },
      listItem: {
        fontSize: '14px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        width: '130px'
      },
      createIcon: {
        color: colors.fontColorRedesign,
        height: '41px',
        width: '42px'
      },
      createIconWalkthrough: {
        color: colors.fontColorRedesign,
        height: '36px',
        width: '38px'
      },
      listItemInner: {
        padding: '10px 15px'
      },
      hr: {
        margin: '0 15px'
      },
      channelPopover: {
        background: 'transparent',
        boxShadow: 'none'
      },
      walkThroughToolTip: {
        padding: '10px',
        textAlign: 'center',
        borderRadius: '4px',
        fontSize: '12px',
        position: 'absolute',
        top: '84px',
        right: '-30px',
        zIndex: '1000000000',
        backgroundColor: '#171717',
        color: '#fff',
        width: '300px'
      }
    };
    this.prevSelection = 0;
    this.state = {
      newSmartBiteModal: window.ldclient.variation('create-smartbite-modal', false),
      walkThrough: window.ldclient.variation('guideme-walkThrough', false),
      showJourney: window.ldclient.variation('journey', false) && Permissions.has('CREATE_JOURNEY'),
      createGroup: Permissions.has('CREATE_GROUP'),
      hoveredSmartbite: false,
      hoveredPathway: false,
      hoveredGroup: false,
      hoveredJourney: false,
      isShowTooltip: false,
      popoverMsg: tr(
        'You need the EdCast GuideMe Chrome extension for the creation of a Walkthrough'
      )
    };
    this.modalSmartBiteOpen = this.modalSmartBiteOpen.bind(this);
    this.modalPathwayOpen = this.modalPathwayOpen.bind(this);
    this.modalGroupOpen = this.modalGroupOpen.bind(this);
    this.modalJourneyOpen = this.modalJourneyOpen.bind(this);
    this.openWalkThrough = this.openWalkThrough.bind(this);
  }

  modalSmartBiteOpen() {
    this.props.requestCloseHandler();
    this.props.dispatch(openSmartBiteCreationModal());
  }

  modalPathwayOpen() {
    this.props.requestCloseHandler();
    this.props.dispatch(openPathwayCreationModal());
  }
  modalGroupOpen() {
    this.props.requestCloseHandler();
    this.props.dispatch(openGroupCreationModal());
  }

  modalJourneyOpen() {
    this.props.requestCloseHandler();
    this.props.dispatch(openJourneyCreationModal());
  }

  openWalkThrough() {
    let guidemeBtn = document.querySelector('.gss-start-button img');
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    if (guidemeBtn && isChrome) {
      guidemeBtn.click();
    } else if (!guidemeBtn && isChrome) {
      this.setState({
        isShowTooltip: true,
        popoverMsg: tr(
          'You need the EdCast GuideMe Chrome extension for the creation of a Walkthrough'
        )
      });
    } else {
      this.setState({
        isShowTooltip: true,
        popoverMsg: tr(
          'You need the EdCast GuideMe Chrome extension and a Chrome browser for the creation of a Walkthrough'
        )
      });
    }
    setTimeout(
      function() {
        this.setState({
          isShowTooltip: false
        });
      }.bind(this),
      4000
    );
  }

  closeTooltip = () => {
    this.setState({
      isShowTooltip: false
    });
  };

  onkeyPressed = e => {
    if (e.key == 'Enter') {
      this.modalSmartBiteOpen();
    }
  };
  closeOnEsc = e => {
    if (e.keyCode == 27) {
      this.props.handleClose(true);
    }
  };
  componentWillReceiveProps(nextProps) {
    if (this.props.open && !nextProps.open) {
      this.prevSelection = 0;
    }
  }
  render() {
    let labels = this.props.team.OrgConfig.labels;
    let shouldShowGroup = labels && labels['web/labels/create_group'];
    if (this.props.open) {
      setTimeout(() => {
        if (this.prevSelection <= 0) {
          this.refs.focusOnPopover.focus();
          this.prevSelection++;
        }
      }, 200);
    }
    return (
      <Popover
        open={this.props.open}
        style={{ marginTop: '6px' }}
        anchorEl={this.props.anchorEl}
        onRequestClose={this.props.requestCloseHandler}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
        className="create-popover"
      >
        <style type="text/css">{`.menu-item-create:hover {color: ${colors.primary}}`}</style>
        <div style={this.styles.list}>
          <div ref="focusOnPopover" className="blankContainer" tabIndex={0} />
          <div className="create-list-redesign" onKeyDown={this.closeOnEsc}>
            <div className="create-item-redesign">
              <IconButton
                aria-label={tr('Smart card')}
                style={{ width: '65px' }}
                iconStyle={this.styles.createIcon}
                onTouchTap={this.modalSmartBiteOpen}
                ref="create"
                onKeyDown={this.onkeyPressed}
                onMouseEnter={() => {
                  this.setState({ hoveredSmartbite: true });
                }}
                onMouseLeave={() => {
                  this.setState({ hoveredSmartbite: false });
                }}
              >
                {this.state.hoveredSmartbite ? <SmartbiteActiveIcon /> : <SmartbiteIcon />}
              </IconButton>
              <span className="create-label-redesign">{tr('SmartCard')}</span>
            </div>
            {Permissions.has('ADD_TO_PATHWAY') && (
              <div className="create-item-redesign">
                <IconButton
                  aria-label={tr('Pathway')}
                  style={{ width: '65px' }}
                  iconStyle={this.styles.createIcon}
                  onTouchTap={this.modalPathwayOpen}
                  onMouseEnter={() => {
                    this.setState({ hoveredPathway: true });
                  }}
                  onMouseLeave={() => {
                    this.setState({ hoveredPathway: false });
                  }}
                >
                  {this.state.hoveredPathway ? <PathwayActiveIcon /> : <PathwayIcon />}
                </IconButton>
                <span className="create-label-redesign">{tr('Pathway')}</span>
              </div>
            )}
            {this.state.showJourney && (
              <div className="create-item-redesign">
                <IconButton
                  aria-label={tr('journey')}
                  style={{ width: '65px' }}
                  iconStyle={this.styles.createIcon}
                  onTouchTap={this.modalJourneyOpen}
                  onMouseEnter={() => {
                    this.setState({ hoveredJourney: true });
                  }}
                  onMouseLeave={() => {
                    this.setState({ hoveredJourney: false });
                  }}
                >
                  {this.state.hoveredJourney ? <JourneyActive /> : <Journey />}
                </IconButton>
                <span className="create-label-redesign">{tr('Journey')}</span>
              </div>
            )}
            {!this.state.walkThrough &&
              this.props.createGroup &&
              this.state.createGroup &&
              (shouldShowGroup && shouldShowGroup.visible) && (
                <div className="create-item-redesign">
                  <IconButton
                    style={{ width: '65px' }}
                    iconStyle={this.styles.createIcon}
                    onTouchTap={this.modalGroupOpen}
                    onMouseEnter={() => {
                      this.setState({ hoveredGroup: true });
                    }}
                    onMouseLeave={() => {
                      this.setState({ hoveredGroup: false });
                    }}
                  >
                    {this.state.hoveredGroup ? <GroupActiveIcon /> : <GroupIcon />}
                  </IconButton>
                  <span className="create-label-redesign">{tr('Group')}</span>
                </div>
              )}

            {this.state.walkThrough && (
              <div className="create-item-redesign">
                <IconButton
                  style={{ padding: '0.625rem' }}
                  iconStyle={this.styles.createIconWalkthrough}
                  onTouchTap={this.openWalkThrough}
                  onMouseEnter={() => {
                    this.setState({ hoveredGroup: true });
                  }}
                  onMouseLeave={() => {
                    this.setState({ hoveredGroup: false });
                  }}
                >
                  <WalkThrough color="#acadc1" />
                </IconButton>
                <span className="create-label-redesign">{tr('Walk-through')}</span>
              </div>
            )}
            {this.state.isShowTooltip && (
              <div
                className="walkThroughToolTip"
                style={this.styles.walkThroughToolTip}
                onClick={this.closeTooltip}
              >
                {this.state.popoverMsg}
              </div>
            )}
          </div>
        </div>
      </Popover>
    );
  }
}

CreateList.propTypes = {
  open: PropTypes.bool,
  team: PropTypes.object,
  anchorEl: PropTypes.any,
  createGroup: PropTypes.bool,
  requestCloseHandler: PropTypes.func,
  handleClose: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(CreateList);
