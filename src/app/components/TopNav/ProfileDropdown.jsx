import React, { Component } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import Avatar from 'edc-web-sdk/components/Avatar';
import ProfileDropdownList from './ProfileDropdownList';
import { tr } from 'edc-web-sdk/helpers/translations';

class ProfileDropdown extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isOpen: false,
      anchor: null,
      keypressed: true
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  handleClick(e) {
    e.preventDefault();
    this.setState({
      isOpen: true,
      anchor: e.currentTarget,
      keypressed: false
    });
  }

  handleClose(escPressed) {
    if (this.state.keypressed || escPressed === true) {
      this.refs.profileButton.focus();
    }
    this.setState({
      isOpen: false,
      keypressed: true
    });
  }

  onKeyPressed = e => {
    if (e.keyCode == 13) {
      this.setState({
        keypressed: true
      });
    }
    if (e.keyCode == 27) {
      this.handleClose();
    }
  };

  render() {
    let firstName =
      (this.props.profile &&
        this.props.profile.user &&
        this.props.profile.user.name.split(' ')[0]) ||
      '';
    let displayName = firstName.length > 10 ? `${firstName.slice(0, 10)}…` : firstName;
    return (
      <a
        href="#"
        tabIndex={0}
        ref="profileButton"
        role="button"
        aria-label={tr('Profile')}
        className="action-icon-wrapper"
        onClick={this.handleClick}
        onKeyDown={this.onKeyPressed}
      >
        <IconButton tabIndex={-1} style={{ padding: '8px', margin: '0 auto' }}>
          <Avatar focusable="false" size={26} user={this.props.profile.user} />
        </IconButton>
        <ProfileDropdownList
          open={this.state.isOpen}
          anchorEl={this.state.anchor}
          requestCloseHandler={this.handleClose}
          currentOrgName={this.props.profile.currentOrgName}
          user={this.props.profile.user}
          links={this.props.profile.links}
          teams={this.props.teams}
          isAdmin={this.props.isAdmin}
          handleClose={this.handleClose}
        />
        <span className="username-redesign">
          <span>{displayName}</span> &#9662;
        </span>
      </a>
    );
  }
}

ProfileDropdown.propTypes = {
  profile: PropTypes.object,
  teams: PropTypes.array,
  isAdmin: PropTypes.bool,
  links: PropTypes.object
};

export default ProfileDropdown;
