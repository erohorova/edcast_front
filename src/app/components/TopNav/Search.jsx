{
  /* Notes:
   * This is a placeholder component for the new Search
   * TODO: Hook this up with action and behavior.
   * cc: @ramin
   */
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Typeahead } from 'react-bootstrap-typeahead';
import Insight from '../feed/Insight.jsx';
import * as actionTypes from '../../constants/actionTypes';
import { search, ecl } from 'edc-web-sdk/requests/index';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { follow, unfollow } from 'edc-web-sdk/requests/users.v2';
import * as channels from '../../actions/channelsActions';
import { Permissions } from '../../utils/checkPermissions';
import Paper from 'edc-web-sdk/components/Paper';

//material-ui
import IconButton from 'material-ui/IconButton';

import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import Spinner from '../common/spinner';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../cards/Card';

class Search extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      searchModal: false,
      suggests: [],
      cardResultIds: [],
      userResultIds: [],
      channelResultIds: [],
      videoResultIds: [],
      pending: false,
      facets: null,
      filter: '',
      query: '',
      providerLogos: {},
      limit: 10,
      offset: 0,
      totalCount: 0,
      providerId: '',
      allFacets: null,
      contentFacets: {},
      providerFacets: {},
      users: []
    };
    this.styles = {
      userListItem: {
        marginRight: '92px'
      },
      userFollowButton: {
        top: '6px'
      }
    };
    this._handleKeydown = null;
    this._window = window;
    this._esc = this.listenForEscapeKey.bind(this);
    this.viewMoreClickHandler = this.viewMoreClickHandler.bind(this);
    this.checkIfProvider = this.checkIfProvider.bind(this);

    this.providerKeys = [];
  }

  componentDidMount() {
    ecl
      .getProviderLogos()
      .then(providerLogos => {
        this.setState({
          providerLogos: providerLogos
        });
      })
      .catch(err => {
        console.error(`Error in Search.getProviderLogos.func: ${err}`);
      });
    if (this.props.origin != '') {
      this.setState({ searchModal: true });
      this.toggleSearch();
    }

    ecl
      .getAllSources({ is_featured: true })
      .then(data => {
        data.forEach(d => {
          this.providerKeys.push(d.display_name);
        });
      })
      .catch(err => {
        console.error(`Error in Search.getAllSources.func: ${err}`);
      });
  }

  listenForEscapeKey(e) {
    if (e.keyCode == 27) {
      this.toggleSearch();
    }
  }

  fetchEmptyStateData() {
    //fetch the popular searches
    if (!this.state.suggests.length) {
      search
        .getSearchInit()
        .then(items => {
          this.setState({ suggests: items });
        })
        .catch(err => {
          console.error(`Error in Search.fetchEmptyStateData.getSearchInit.func: ${err}`);
        });
    }
  }

  async followClickHandler(user) {
    if (!user.isFollowing) {
      await follow(user.id);
    } else {
      await unfollow(user.id);
    }

    let users = this.state.users;
    try {
      users.filter(u => u.id == user.id)[0].isFollowing = !user.isFollowing;
      this.setState({
        users
      });
    } catch (e) {}
  }

  toggleFollowChannelClickHandler(channel, following) {
    this.props.dispatch(channels.toggleFollow(channel, !following));
  }

  toggleSearch() {
    //toggle background scrollable
    document.getElementsByTagName('body')[0].style.overflow = !this.state.searchModal
      ? 'hidden'
      : '';
    // remove event listener
    this.setState(
      {
        searchModal: !this.state.searchModal,
        offset: 0
      },
      function() {
        // Focus on input field
        if (this.state.searchModal) {
          this._window.addEventListener('keydown', this._esc, false);
          this.refs.typeahead.getInstance().focus();
          this._handleKeydown = this.refs.typeahead.getInstance()._handleKeydown;
          this.refs.typeahead.getInstance()._handleKeydown = this.handleEnterPress.bind(this);
          this.fetchEmptyStateData();
        } else {
          this._window.removeEventListener('keydown', this._esc, false);
          this.setState(
            {
              suggests: [],
              cardResultIds: [],
              userResultIds: [],
              channelResultIds: [],
              videoResultIds: [],
              facets: null,
              query: '',
              pending: false,
              filter: '',
              providerLogos: {},
              limit: 10,
              offset: 0,
              totalCount: 0
            },
            function() {
              this.props.toggleParentState();
            }
          );
        }
      }
    );

    if (this.props.origin !== '' && this.state.searchModal == true) {
      this.props.toggleParentState();
    }
  }

  handleEnterPress(options, evt) {
    let input = this.refs.typeahead.getInstance().state;
    // If there is no active index and the user presses enter
    this._handleKeydown(options, evt);
    if (evt.keyCode == 13) {
      let val = input.activeIndex == -1 ? input.text : options[input.activeIndex];
      if (val === '') {
        search
          .getSearchInit()
          .then(items => {
            this.setState({
              suggests: items,
              cardResultIds: [],
              userResultIds: [],
              channelResultIds: [],
              videoResultIds: [],
              users: [],
              query: '',
              offset: 0,
              totalCount: 0
            });
          })
          .catch(err => {
            console.error(`Error in Search.getSearchInit.func: ${err}`);
          });
      }
      if (val) {
        this.setState({ pending: true, cardResultIds: [] }, function() {
          this.handleChange(val);
        });
      }
    }
  }

  checkInputBlink() {
    let inp = document.querySelector('.bootstrap-typeahead-input-main');
    if (inp.value) {
      inp.classList.remove('blink');
    } else {
      inp.classList.add('blink');
    }
  }

  checkIfProvider(o) {
    return o.type == 'source_id';
  }

  handleChange(selected, params = null) {
    let val = Array.isArray(selected) ? selected[0] : selected;
    let limit = this.state.limit;
    let offset = this.state.offset;
    let filter = this.state.filter;
    let payLoad = { q: val, limit: limit, offset: offset };
    if (params !== null) {
      payLoad = params;
    }

    if (this.props.origin != '') {
      payLoad['source_id[]'] = this.props.origin;
      this.setState({ provider: this.props.origin });
    }

    if (typeof val === 'object') {
      val = val.label;
    }
    if (!val) return;
    val = val.replace('#', ''); // remove hashtags for search, do not encode!
    this.setState(
      {
        pending: true,
        query: val
      },
      () => {
        let eclSearch = ecl.eclSearch(payLoad);
        Promise.all([eclSearch])
          .then(values => {
            let eclData = values[0];
            let totalCount = 0;
            let allFacets = eclData.aggregations;
            this.setState({ allFacets: allFacets });

            if (eclData.users != undefined && eclData.users.length > 0) {
              this.props.dispatch({
                type: actionTypes.RECEIVE_USERS,
                users: eclData.users
              });
            }

            if (eclData.channels != undefined && eclData.channels.length > 0) {
              this.props.dispatch({
                type: actionTypes.RECEIVE_CHANNELS,
                channels: eclData.channels
              });
            }

            if (eclData.cards != undefined && eclData.cards.length > 0) {
              totalCount = values[0].cards.length;
              this.props.dispatch({
                type: actionTypes.RECEIVE_CARDS,
                cards: eclData.cards
              });

              // setting hide flag for view more
              this.setState({
                hideViewMore: false
              });
            } else {
              this.setState({
                hideViewMore: true
              });
            }

            let menu = document.querySelector('ul.dropdown-menu');
            if (menu) {
              menu.style.display = 'none';
            }

            let facets;
            if (this.state.facets === null) {
              if (this.props.origin == '') {
                facets = eclData.aggregations;
              } else {
                facets = eclData.aggregations.filter(this.checkIfProvider);
              }
            } else {
              facets = this.state.facets;
            }

            let providerFacets = {};
            let contentFacets = {};
            facets.forEach(provider => {
              if (provider.type === 'content_type') {
                contentFacets[provider.display_name] = {
                  count: provider.count,
                  id: provider.id,
                  type: 'content_type'
                };
              } else if (provider.type === 'source_type_name') {
                if (providerFacets['Open Web']) {
                  providerFacets['Open Web'].count += provider.count;
                  providerFacets['Open Web'].ids.push(provider.id);
                } else {
                  providerFacets['Open Web'] = {
                    count: provider.count,
                    type: 'source_type_name',
                    ids: [provider.id],
                    id: 'openweb'
                  };
                }
              } else {
                providerFacets[provider.display_name] = {
                  count: provider.count,
                  id: provider.id,
                  type: 'source_id'
                };
              }
            });

            this.setState({
              userResultIds: eclData.users.map(user => user.id + ''),
              users: eclData.users,
              channelResultIds: eclData.channels.map(channel => channel.id + ''),
              facets,
              providerFacets,
              contentFacets,
              cardResultIds: this.state.cardResultIds.concat(eclData.cards.map(card => card.id)),
              searchQuery: val,
              totalCount,
              pending: false
            });
          })
          .catch(err => {
            console.error(`Error in Search.Promise.all.func: ${err}`);
          });
      }
    );
  }

  // to handle view more clickhandler
  viewMoreClickHandler() {
    let searchQuery = this.state.searchQuery;
    let limit = this.state.limit;
    let newOffset = this.state.offset + this.state.limit;
    this.setState(
      {
        offset: newOffset
      },
      function() {
        let params = {
          q: this.state.query,
          limit: this.state.limit,
          offset: this.state.offset
        };

        if (this.state.filter) {
          params[this.state.filterType + '[]'] = this.state.filter;
        }

        if (this.state.filterIds) {
          params[this.state.filterType + '[]'] = this.state.filterIds;
        }

        this.handleChange(this.state.query, params);
      }
    );
  }

  handleFacetFilter(facet) {
    let filter = facet.id;
    let type = facet.type;

    if (this.state.filter && this.state.filter == filter) {
      filter = '';
      type = '';
    }

    this.setState(
      {
        filter: filter,
        filterIds: facet.ids ? facet.ids : null,
        pending: true,
        filterType: type,
        cardResultIds: [],
        userResultIds: [],
        users: [],
        channelResultIds: [],
        offset: 0
      },
      function() {
        let params = {
          q: this.state.query,
          limit: this.state.limit,
          offset: this.state.offset
        };

        if (filter) {
          params[this.state.filterType + '[]'] = this.state.filter;
        }

        if (facet.ids) {
          params[this.state.filterType + '[]'] = facet.ids;
        }

        this.handleChange(this.state.query, params);
      }
    );
  }

  scrollToTop() {
    window.scrollTo(0, 0);
    this.refs.typeahead.getInstance().focus();
  }

  handleInputChange(query) {
    // Show the typeahead options again and reset state.filter
    let menu = document.querySelector('ul.dropdown-menu');
    let hint = document.querySelector('input.bootstrap-typeahead-input-hint');
    if (menu) {
      menu.style.display = 'block';
    }
    this.setState({
      filter: '',
      facets: null,
      allFacets: null
    });
    if (this.suggestTimeout) {
      window.clearTimeout(this.suggestTimeout);
    }
    this.suggestTimeout = window.setTimeout(() => {
      search
        .getSearchSuggest({ q: query, limit: 10 })
        .then(suggests => {
          this.setState({ suggests });
          if (!suggests.length) {
            hint.style.display = 'none';
          } else {
            hint.style.display = 'block';
          }
        })
        .catch(err => {
          console.error(`Error in Search.getSearchSuggest.func: ${err}`);
        });
    }, 500);
  }

  removeCardFromList = id => {
    let newCardIdsList = this.state.cardResultIds.filter(cardId => {
      return cardId !== id;
    });
    this.setState({ cardResultIds: newCardIdsList });
  };

  searchModal() {
    const toggleSearch = this.toggleSearch.bind(this);
    return (
      <div id="search" onClick={this.checkInputBlink}>
        <div className="row" style={{ minHeight: '100%' }}>
          <div className="small-4 medium-4 columns container-padding">
            <div className="search-filters">
              <h1>{tr('Search')}</h1>
              {/*<input type="text" className="search-input" autoFocus={true}/>*/}
              <Typeahead
                ref="typeahead"
                onChange={selected => this.handleChange(selected)}
                onInputChange={v => this.handleInputChange(v)}
                options={this.state.suggests}
                emptyLabel=""
                newSelectionPrefix=""
                allowNew={true}
                maxHeight={500}
              />
              {this.state.facets && this.state.facets != undefined && this.state.facets.length > 0 && (
                <div id="facets">
                  <ul>
                    {Object.keys(this.state.providerFacets).map(f => {
                      let facet = this.state.providerFacets[f];
                      return (
                        <li
                          key={facet.id}
                          className={
                            this.state.filter == facet.id ? 'selected facetFilter' : 'facetFilter'
                          }
                          onClick={this.handleFacetFilter.bind(this, facet)}
                        >
                          {f.substring(0, 25)} <span>{facet.count}</span>
                        </li>
                      );
                    })}
                  </ul>
                  <ul>
                    {Object.keys(this.state.contentFacets).map(f => {
                      let facet = this.state.contentFacets[f];
                      return (
                        <li
                          key={facet.id}
                          className={
                            this.state.filter == facet.id ? 'selected facetFilter' : 'facetFilter'
                          }
                          onClick={this.handleFacetFilter.bind(this, facet)}
                        >
                          {f.substring(0, 25)} <span>{facet.count}</span>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              )}
            </div>
          </div>
          <div className="small-8 medium-8 columns search-results-container container-padding vertical-spacing-large">
            <h1>
              &nbsp;
              <span className="search-close" onClick={this.toggleSearch.bind(this)}>
                &times;
              </span>
            </h1>

            <div className="search-results">
              <div className="vertical-spacing-large">
                {(this.state.filter == '' || this.state.filter == 'user') &&
                  this.props.origin == '' &&
                  this.state.users.map(user => {
                    let defaultUserImage =
                      'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
                    return (
                      <Paper key={user.id} className="result-paper">
                        <ListItem
                          disabled
                          style={this.styles.userListItem}
                          primaryText={
                            <div>
                              <a href={'/' + user.handle} className="matte">
                                {user.firstName} {user.lastName}
                              </a>
                            </div>
                          }
                          secondaryText={
                            <div>
                              <a href={'/' + user.handle} className="matte">
                                {user.handle}
                              </a>
                            </div>
                          }
                          leftAvatar={
                            <a href={'/' + user.handle}>
                              <Avatar src={user.avatarimages.small || defaultUserImage} />
                            </a>
                          }
                          rightIcon={
                            <div style={this.styles.userFollowButton}>
                              {this.props.currentUser.id != user.id &&
                                !Permissions.has('DISABLE_USER_FOLLOW') && (
                                  <FollowButton
                                    following={user.isFollowing}
                                    className="follow"
                                    label={tr(user.isFollowing ? 'Following' : 'Follow')}
                                    hoverLabel={tr(user.isFollowing ? 'Unfollow' : '')}
                                    pendingLabel={tr(
                                      user.isFollowing ? 'Unfollowing...' : 'Following...'
                                    )}
                                    onTouchTap={this.followClickHandler.bind(this, user)}
                                    pending={user.followPending}
                                  />
                                )}
                            </div>
                          }
                        />
                      </Paper>
                    );
                  })}
                {(this.state.filter == '' || this.state.filter == 'channel') &&
                  this.props.origin == '' &&
                  this.state.channelResultIds.map(id => {
                    let channel = this.props.channels[id];
                    return (
                      <Paper key={id}>
                        <ListItem
                          disabled
                          primaryText={
                            <div className="text-overflow">
                              <a className="matte" href={`/channel/${channel.slug}`}>
                                {channel.label}
                              </a>
                            </div>
                          }
                          secondaryText={
                            <div className="text-overflow">
                              <small>{channel.description}</small>
                            </div>
                          }
                          innerDivStyle={{ paddingLeft: '8.125rem' }}
                          leftIcon={
                            <a href={`/channel/${channel.slug}`}>
                              <div
                                className="banner"
                                style={{
                                  width: '6.25rem',
                                  height: '4.0625rem',
                                  backgroundImage: `url(\'${channel.bannerImageUrl}\')`
                                }}
                              />
                            </a>
                          }
                          style={{ height: '6.5rem', paddingRight: '10rem' }}
                          rightIconButton={
                            <div
                              className="following-button"
                              style={{ top: '1.75rem', right: '1rem' }}
                            >
                              <FollowButton
                                following={channel.isFollowing}
                                className="follow"
                                label={tr(channel.isFollowing ? 'Following' : 'Follow')}
                                hoverLabel={tr(channel.isFollowing ? 'Unfollow' : '')}
                                pendingLabel={tr(
                                  channel.isFollowing ? 'Unfollowing...' : 'Following...'
                                )}
                                onTouchTap={this.toggleFollowChannelClickHandler.bind(
                                  this,
                                  channel.id,
                                  channel.isFollowing
                                )}
                                pending={channel.followPending}
                              />
                            </div>
                          }
                        />
                      </Paper>
                    );
                  })}
                <div className="custom-card-container">
                  <div className="three-card-column vertical-spacing-medium">
                    {this.state.cardResultIds.map(id => {
                      let card = this.props.cards[id];

                      if (card.cardType === 'pack') {
                        card.packCount = card.packCards.length;
                        card.title = card.message;
                      } else if (
                        card.cardType === 'pack' &&
                        card.fileResources != undefined &&
                        card.fileResources.length > 0
                      ) {
                        card.imageUrl = card.fileResources[0].fileUrl;
                        card.packCount = card.packCards.length;
                        card.title = card.message;
                      }
                      // ECL cards have author_id
                      let user = card.authorId
                        ? this.props.users.idMap[card.authorId]
                        : this.props.users.idMap[card.author_id];
                      // Filter on this.state.filter
                      // if(this.state.filter && card.ecl && card.ecl.origin != this.state.filter) {
                      //   return;
                      // }
                      return (
                        <Card
                          key={id}
                          toggleSearch={toggleSearch}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          author={user}
                          card={card}
                          user={user}
                          isMarkAsCompleteDisabled={true}
                          providerLogos={this.state.providerLogos}
                          moreCards={false}
                          removeCardFromList={this.removeCardFromList}
                        />
                      );
                    })}
                  </div>
                </div>

                {this.state.cardResultIds.length > 0 && !this.state.hideViewMore && (
                  <div className="text-center">
                    <SecondaryButton
                      label={tr('View More')}
                      onTouchTap={this.viewMoreClickHandler}
                      className="view-more-button viewMore"
                    />
                  </div>
                )}
                {((!this.state.pending &&
                  this.state.videoResultIds.length === 0 &&
                  this.state.userResultIds.length === 0 &&
                  this.state.cardResultIds.length === 0 &&
                  this.state.channelResultIds.length === 0 &&
                  this.state.query != '') ||
                  (this.props.origin != '' &&
                    this.state.allFacets &&
                    this.state.allFacets.length != 0 &&
                    this.state.facets &&
                    this.state.facets.length == 0)) && (
                  <div className="text-center no-results data-not-available-msg">
                    {tr('NO RESULTS FOUND...')}
                  </div>
                )}
                {this.state.pending && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="action-icon-wrapper">
        <IconButton className="search" onTouchTap={this.toggleSearch.bind(this)}>
          <img src="/i/images/search-icon.svg" className="action-icon" />
        </IconButton>
        {this.state.searchModal && this.searchModal()}
      </div>
    );
  }
}

Search.defaultProps = {
  origin: '',
  toggleParentState: function() {}
};

Search.propTypes = {
  origin: PropTypes.string,
  currentUser: PropTypes.object,
  channels: PropTypes.object,
  cards: PropTypes.object,
  users: PropTypes.object,
  toggleParentState: PropTypes.func
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  cards: state.cards.toJS(),
  users: state.users.toJS(),
  channels: state.channels.toJS()
}))(Search);
