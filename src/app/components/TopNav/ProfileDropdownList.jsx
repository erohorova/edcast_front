import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover';
import { List, ListItem } from 'material-ui/List';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import Avatar from 'edc-web-sdk/components/Avatar';
import colors from 'edc-web-sdk/components/colors/index';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { logout } from 'edc-web-sdk/requests/users.v2';
import * as upshotActions from '../../actions/upshotActions';
import { Permissions } from '../../utils/checkPermissions';

class ProfileDropdownList extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      profileName: {
        fontSize: '14px',
        fontWeight: '600',
        width: '168px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        marginLeft: '0.2rem'
      },
      profileEmail: {
        fontSize: '12px',
        color: colors.silverSand,
        width: '168px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        marginLeft: '0.2rem'
      },
      profileSettings: {
        fontSize: '12px',
        fontWeight: '600',
        marginLeft: '0.2rem',
        paddingBottom: '0.2rem'
      },
      listItem: {
        fontSize: '14px',
        width: '172px',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
      },
      list: {
        width: '260px',
        paddingBottom: 0
      },
      subheader: {
        paddingTop: '8px',
        lineHeight: '16px',
        color: colors.darkGray,
        fontWeight: '600'
      },
      profileListItem: {
        paddingTop: '8px'
      },
      moreProfileOptions: {
        cursor: 'pointer',
        fontSize: '12px',
        color: '#454560',
        marginBottom: '10px',
        display: 'block'
      },
      moreProfileOptionsContainer: {
        padding: '10px 10px 0px 16px'
      }
    };
    this.bookmarkStandalone = window.ldclient.variation('bookmark-standalone', false);
    this.domoUrl =
      (this.props.team && this.props.team.config && this.props.team.config.domo_url) ||
      'https://edcast.domo.com';
    this.settingsClickHandler = this.settingsClickHandler.bind(this);
    this.state = {
      open: props.open,
      genpactUI: window.ldclient.variation('genpact-ui', false)
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.open !== nextProps.open) {
      this.setState({ open: nextProps.open });
    }
  }

  settingsClickHandler(e, path) {
    e.preventDefault();
    this.props.requestCloseHandler();
    if (this.props.pathname === path) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  }
  signOut = () => {
    logout(this.props.links.signOutLink.url)
      .then(() => {
        localStorage.setItem('afterLoginContentURL', '/');
        localStorage.setItem('afterOnboardingURL', '/');
        localStorage.setItem('userLoggedOut', 'true');
        localStorage.setItem('isLoggedIn', false);
        upshotActions.updateUpshotUserProfile({ appuid: '' });
        window.location = '/log_in';
      })
      .catch(err => {
        console.error(`Error in ProfileDropdownList.logout.func: ${err}`);
      });
  };

  pushTo = (e, link) => {
    e.preventDefault();
    this.props.dispatch(push(link));
  };

  closeOnEsc = e => {
    if (e.keyCode == 27) {
      this.props.handleClose(true);
    }
  };

  render() {
    let showEdgrapf = Permissions['enabled'] !== undefined && Permissions.has('ENABLE_EDGRAPH');
    let isCurator = Permissions['enabled'] !== undefined && Permissions.has('CURATE_CONTENT');
    if (this.state.open) {
      setTimeout(() => {
        this.refs['focusOnPopover'].focus();
      }, 800);
    }

    let moreProfileOptions = [
      { key: 'About Me', link: '/me' },
      {
        key: 'My Bookmarks',
        link: this.bookmarkStandalone ? '/bookmarks' : '/me/content/bookmarked'
      },
      { key: 'My Groups', link: '/me/groups' },
      { key: 'People', link: '/team' },
      { key: 'My Learning Plan', link: '/me/learning' }
    ];
    if (this.props.team && this.props.team.config && this.props.team.config.leaderboard) {
      moreProfileOptions.push({ key: 'Leaderboard', link: '/me/leaderboard' });
    }
    return (
      <Popover
        open={this.state.open}
        anchorEl={this.props.anchorEl}
        onRequestClose={this.props.requestCloseHandler}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      >
        <List style={this.styles.list} onKeyDown={this.closeOnEsc}>
          {this.props.currentOrgName && !this.state.genpactUI && (
            <Subheader style={this.styles.subheader}>{this.props.currentOrgName}</Subheader>
          )}
          <ListItem
            innerDivStyle={this.styles.profileListItem}
            disabled={true}
            primaryText={<div style={this.styles.profileName}>{this.props.user.name}</div>}
            secondaryText={
              <div style={{ overflow: 'hidden', height: 'auto' }}>
                <div ref="focusOnPopover" className="blankContainer" tabIndex={0} />
                <div style={this.styles.profileEmail}>{this.props.user.email}</div>
                <div style={this.styles.profileSettings}>
                  <a
                    id="profile"
                    href="#"
                    className="settingsAccountDetails"
                    onClick={e => this.settingsClickHandler(e, this.props.links.settingsLink.url)}
                  >
                    {tr(this.props.links.settingsLink.label)}
                  </a>
                </div>
                {this.props.isAdmin && (
                  <div style={this.styles.profileSettings}>
                    <a href="/admin">{tr('Admin Console')}</a>
                  </div>
                )}
                {(this.props.isAdmin || isCurator) &&
                  window.ldclient.variation('ed-graph', '0') !== '0' && (
                    <div style={this.styles.profileSettings}>
                      {showEdgrapf && (
                        <a target="_blank" href={this.domoUrl}>
                          EdGraph
                        </a>
                      )}
                    </div>
                  )}
              </div>
            }
            leftAvatar={<Avatar user={this.props.user} />}
            secondaryTextLines={2}
          />
          {this.props.teams.length > 0 && (
            <div>
              <Divider />
            </div>
          )}
          {this.props.teams.length > 0 &&
            this.props.teams.map((team, idx) => {
              return (
                <ListItem
                  primaryText={<div style={this.styles.listItem}>{team.label}</div>}
                  href={team.url}
                  key={idx}
                  style={{ backgroundColor: colors.concrete }}
                />
              );
            })}
          <Divider style={{ marginTop: 0 }} />

          {this.state.genpactUI && (
            <div style={this.styles.moreProfileOptionsContainer}>
              {moreProfileOptions.map(option => {
                return (
                  <a
                    href="#"
                    key={option.key}
                    style={this.styles.moreProfileOptions}
                    onClick={e => this.pushTo(e, option.link)}
                  >
                    {tr(option.key)}
                  </a>
                );
              })}
            </div>
          )}
          {this.props.links.newOrganizationLink && (
            <ListItem
              style={{ backgroundColor: colors.concrete }}
              primaryText={
                <div style={this.styles.listItem}>{this.props.links.newOrganizationLink.label}</div>
              }
              href={this.props.links.newOrganizationLink.url}
            />
          )}
          <ListItem
            style={{ backgroundColor: colors.concrete, lineHeight: '20px' }}
            onClick={() => {
              this.signOut();
            }}
            primaryText={
              <div style={this.styles.listItem}>{tr(this.props.links.signOutLink.label)}</div>
            }
          />
        </List>
      </Popover>
    );
  }
}

ProfileDropdownList.propTypes = {
  open: PropTypes.bool,
  requestCloseHandler: PropTypes.func,
  currentOrgName: PropTypes.string,
  pathname: PropTypes.string,
  user: PropTypes.object,
  team: PropTypes.object,
  anchorEl: PropTypes.object,
  links: PropTypes.object,
  teams: PropTypes.array,
  isAdmin: PropTypes.bool,
  handleClose: PropTypes.func
};

export default connect(state => ({
  pathname: state.routing.locationBeforeTransitions.pathname,
  team: state.team.toJS()
}))(ProfileDropdownList);
