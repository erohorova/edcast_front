{
  /* Notes:
   * Treat this component as a way to assemble the Navigation Bar.
   * Each object within the Naviagtion Bar should be treated as a separate components.
   * Keep the behavior of the object within the object of the component.
   * Preferrably use store to get all the data for the components.
   */
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Logo from './Logo';
import MenuLinks from './MenuLinks';
import SearchV1 from './SearchV1';
import { Notifications } from 'blackbox-notifications';
import MoreOptions from './MoreOptions';
import ProfileDropdown from './ProfileDropdown';
import CreateInsightModal from './CreateInsightModal';
import { Permissions } from '../../utils/checkPermissions';
import IconButton from 'material-ui/IconButton';
import SearchIcon from 'edc-web-sdk/components/icons/Searchv2';

class TopNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSearchInputOpen: false
    };
  }
  toggleSearchInput = () => {
    this.setState({ isSearchInputOpen: !this.state.isSearchInputOpen });
  };

  render() {
    return (
      <div
        className={`${this.props.isFixed ? '' : 'clearfix'} ${
          this.state.isSearchInputOpen ? 'search-input-block__active' : ''
        }`}
      >
        <div className="top-nav-container">
          <div className="top-nav">
            <div className="clearfix border-bottom-line">
              {/* Left side of the Navigation - start */}
              <div className="float-left clearfix">
                <div className="float-left">
                  <Logo
                    coBrandingLogo={this.props.coBrandingLogo}
                    enableEdcastLogo={this.props.enableEdcastLogo}
                  />
                </div>
              </div>
              {/* Left Side of the Navigation - end*/}
            </div>
          </div>
        </div>

        <div className="hidden-top-nav-container" />

        <div
          className={`secondary-nav-container ${this.props.isFixed ? 'fixed' : ''}`}
          id="secondary-nav"
        >
          <div className="secondary-nav">
            {/* Left side of the Navigation - start */}
            <div className="float-left left-menu-content">
              <MenuLinks
                menuLinks={this.props.menuLinks}
                topNav={this.props.topNav}
                isFixed={this.props.isFixed}
              />
            </div>
            {/* Left side of the Navigation - end */}

            {/* Right Side of the Navigation - start */}
            <div className={'action-icon-list float-right horizontal-spacing-large search-v1'}>
              <div className="search-input-block">
                <SearchV1 queryTerm={this.props.queryTerm} />
              </div>
              <div className="action-icon-wrapper-redesign search-bnt-block">
                <div className="action-icon-wrapper">
                  <IconButton tabIndex={0} onClick={this.toggleSearchInput}>
                    <SearchIcon focusable="false" className="action-icon" color={'#6f708b'} />
                  </IconButton>
                </div>
              </div>
              <Notifications />
              <MoreOptions />
              <ProfileDropdown
                profile={this.props.profileDropdown}
                teams={this.props.teams}
                isAdmin={this.props.isAdmin}
              />
              {Permissions.has('MANAGE_CARD') && <CreateInsightModal />}
            </div>
            {/* Right Side of the Navigation - end */}
          </div>
        </div>
      </div>
    );
  }
}

TopNav.propTypes = {
  coBrandingLogo: PropTypes.string,
  menuLinks: PropTypes.array,
  fixNav: PropTypes.bool,
  queryTerm: PropTypes.string,
  profileDropdown: PropTypes.object,
  team: PropTypes.object,
  teams: PropTypes.array,
  user: PropTypes.object,
  enableEdcastLogo: PropTypes.bool,
  isAdmin: PropTypes.bool,
  isFixed: PropTypes.bool,
  topNav: PropTypes.bool
};

export default TopNav;
