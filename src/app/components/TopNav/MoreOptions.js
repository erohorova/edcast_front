import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import MoreOptionsList from './MoreOptionsList';
import Tile from 'edc-web-sdk/components/icons/Tile.jsx';
import { tr } from 'edc-web-sdk/helpers/translations';

class MoreOptions extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isOpen: false,
      anchor: null,
      keypressed: true
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.setState({
      isOpen: true,
      anchor: e.currentTarget,
      keypressed: false
    });
  }

  handleClose(escPressed) {
    if (this.state.keypressed || escPressed === true) {
      this.refs.moreOptions.focus();
    }
    this.setState({
      isOpen: false,
      keypressed: true
    });
  }

  onKeyPressed = e => {
    if (e.keyCode == 13) {
      this.setState({
        keypressed: true
      });
    }
    if (e.keyCode == 27) {
      this.handleClose(e);
    }
  };

  render() {
    return (
      <a
        tabIndex={0}
        role="button"
        ref="moreOptions"
        aria-label={tr('More options')}
        className="action-icon-wrapper"
        href="#"
        onClick={this.handleClick}
        onKeyDown={this.onKeyPressed}
      >
        <IconButton tabIndex={-1}>
          <Tile focusable="false" className="action-icon" color={'#6f708b'} />
        </IconButton>
        <MoreOptionsList
          open={this.state.isOpen}
          anchorEl={this.state.anchor}
          requestCloseHandler={this.handleClose}
          teams={this.props.teams}
          isAdmin={this.props.isAdmin}
          handleClose={this.handleClose}
        />
        <span className="username-redesign">{tr('More')} &#9662;</span>
      </a>
    );
  }
}

MoreOptions.propTypes = {
  teams: PropTypes.object,
  isAdmin: PropTypes.bool
};

export default connect()(MoreOptions);
