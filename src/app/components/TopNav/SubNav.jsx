import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import throttle from 'lodash/throttle';

class SubNav extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      fixNav: false
    };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      let scrollTop = document.body.scrollTop;
      if (scrollTop != undefined && scrollTop >= 68) {
        this.setState({
          fixNav: true
        });
      } else {
        this.setState({
          fixNav: false
        });
      }
    },
    150,
    { leading: false }
  );

  truncateText = title => {
    if (typeof title === 'string') {
      if (title.length > 20) {
        return title.slice(0, 20) + '...';
      } else {
        return title;
      }
    }
  };

  linkClickHandler = link => {
    if (link.type === 'push') {
      this.props.dispatch(push(link.path));
    } else {
      window.location = link.path;
    }
  };

  stillHoveringCallback = bool => {
    this.props.stillHoveringCallback(bool, this.props.menuType);
  };

  renderSubNavChildren = option => {
    switch (option.dataType) {
      case 'other':
        return option.children.map((child, index) => {
          return (
            <div
              className="subnav-option"
              key={index}
              onClick={() => {
                this.stillHoveringCallback(false);
                this.props.dispatch(push(child.path));
              }}
            >
              <a>
                <small>{tr(this.truncateText(child.text))}</small>
              </a>
            </div>
          );
        });
      default:
        if (option.children) {
          return option.children.map((child, index) => {
            if (index < 5) {
              return (
                <div
                  className="subnav-option"
                  key={index}
                  onClick={
                    child.type === 'push'
                      ? () => {
                          this.stillHoveringCallback(false);
                          this.props.dispatch(push(child.path));
                        }
                      : undefined
                  }
                >
                  <a href={child.type === 'link' ? child.path : undefined}>
                    <small>{tr(this.truncateText(child.name))}</small>
                  </a>
                </div>
              );
            } else if (index === 5) {
              return (
                <div
                  className="subnav-option"
                  key={index}
                  onClick={
                    option.viewAllType === 'push'
                      ? () => {
                          this.props.dispatch(push(option.viewAll));
                        }
                      : undefined
                  }
                >
                  <a href={option.viewAllType === 'link' ? option.viewAll : undefined}>
                    <small>{tr(option.viewAllText)}</small>
                  </a>
                </div>
              );
            }
          });
        }
        break;
    }
  };

  render() {
    return (
      <div
        id="sub-nav"
        className={`top-nav-sub-nav ${this.props.isFixed ? 'fixed' : ''}`}
        onMouseEnter={this.stillHoveringCallback.bind(this, true)}
        onMouseLeave={this.stillHoveringCallback.bind(this, false)}
      >
        <div className="sub-nav-container">
          <div className="container-padding">
            <div className="row" style={{ maxWidth: '63.625rem' }}>
              {this.props.subNavOptions.map((option, index) => {
                return (
                  <div className="sub-nav-clmn column" key={index}>
                    <div>
                      <a
                        className="nav-option"
                        href={option.type === 'link' ? option.path : undefined}
                        onClick={() => {
                          if (option.type === 'push') {
                            this.stillHoveringCallback(false);
                            this.props.dispatch(push(option.path));
                          }
                        }}
                      >
                        <strong className="option">{tr(option.text)}</strong>
                      </a>
                      <div className="sub-nav-second">{this.renderSubNavChildren(option)}</div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {};
}

SubNav.propTypes = {
  menuType: PropTypes.string,
  stillHoveringCallback: PropTypes.func,
  subNavOptions: PropTypes.object,
  isFixed: PropTypes.bool
};

export default connect(mapStoreStateToProps)(SubNav);
