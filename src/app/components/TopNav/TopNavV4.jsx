import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import Logo from './Logo';
import MenuLinks from './MenuLinks';
import SearchV1 from './SearchV1';
import { Notifications } from 'blackbox-notifications';
import ProfileDropdown from './ProfileDropdown';
import { connect } from 'react-redux';
import CreateInsightModal from './CreateInsightModal';
import { Permissions } from '../../utils/checkPermissions';
import colors from 'edc-web-sdk/components/colors';
import MoreOptions from './MoreOptions';
import Help from 'edc-web-sdk/components/icons/Help.jsx';
import IconButton from 'material-ui/IconButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import SvgIcon from 'material-ui/SvgIcon';
import Toggle from 'material-ui/Toggle';
import { getWalletBalance } from '../../actions/currentUserActions';
import { push } from 'react-router-redux';
import SkillCoins from 'edc-web-sdk/components/icons/SkillCoins';
import MyGuide from 'edc-web-sdk/components/icons/MyGuide';
import handleGuideMeHelpClick from '../../utils/handleGuideMeHelpClick';
import detectHeaderWidth from '../../utils/detectHeaderWidth';

class TopNav extends Component {
  constructor(props) {
    super(props);
    let smartsearch = window.ldclient.variation('smartsearch', 'disabled');
    let reduceHeaderSize = window.ldclient.variation('reduce-header-size', false);
    this.state = {
      isDisplayMore: false,
      isSearchInputOpen: false,
      enableGuideMe: this.props.team && this.props.team.config.enable_guide_me,
      config:
        (this.props.team.config.lxpCustomCSS &&
          this.props.team.config.lxpCustomCSS.configs &&
          this.props.team.config.lxpCustomCSS.configs.header) ||
        {},
      configurableHeader: window.ldclient.variation('new-configurable-header', false),
      edcastWallet: this.props.team.config.wallet || false,
      homePagev1: window.ldclient.variation('home-page-fix-v1', false),
      isSmartSearch: smartsearch !== 'disabled',
      isSmartSearchOn:
        smartsearch === 'enabled-on' || localStorage.getItem('isSmartSearchOn') === 'true' || false,
      allowAlignSearchInHeader:
        window.ldclient.variation('allow-search-align-in-header', false) &&
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.align_search_in_header,
      shortSelectField: false,
      reduceHeaderSize: reduceHeaderSize
    };
    this.styles = {
      helpIcon: {
        width: '26px',
        height: '26px',
        color: colors.fontColorRedesign,
        fontWeight: 100
      },
      walletBalance: {
        background: '#626180',
        padding: '1px 6px',
        fontSize: '12px',
        fontWeight: 600,
        marginBottom: '6px',
        marginTop: reduceHeaderSize ? '0' : '20px'
      }
    };
  }

  componentDidMount() {
    this.props.team &&
      this.props.team.config &&
      this.props.team.config.wallet &&
      this.props.dispatch(getWalletBalance());
    window.addEventListener('resize', this.setSelectView);
  }

  componentWillMount() {
    let enableGuideMe = typeof this.state.enableGuideMe == 'boolean' && !this.state.enableGuideMe;
    if (enableGuideMe) {
      let css =
          '.gmClientstart-button, .gss-client-button, #gmClientlaunch-button{display: none !important;} #gmClient_launch-button{display: none !important;}',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
      style.type = 'text/css';

      if (style.styleSheet) {
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }

      head.appendChild(style);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setSelectView);
  }

  setConfigStyle() {
    let customStyle = this.state.configurableHeader ? this.state.config : {};
    if (this.state.configurableHeader) {
      let searchBoxStyle = (customStyle.searchBox && customStyle.searchBox.styles) || {};
      let menuListColor = (customStyle.menuListColor && customStyle.menuListColor.styles) || {};
      let createButton = (customStyle.createButton && customStyle.createButton.styles) || {};
      let searchBoxStyleString = `background-color:${searchBoxStyle.backgroundColor ||
        '#6f708b'}!important; font-weight:${this.state.homePagev1 ? 'bold' : 'normal'}; color:${
        this.state.homePagev1 ? 'white' : searchBoxStyle.color || '#acadc1'
      };`;
      let searchIconColorString = `fill:${searchBoxStyle.color || '#acadc1'};`;
      let menuListColorString = `color:${menuListColor.color || '#d6d6e1'};`;
      let menuListIconColorString = `fill:${
        this.state.homePagev1 ? 'white' : menuListColor.color || '#d6d6e1'
      };`;
      let menuListIconColorStringGuide = `fill: ${
        this.state.homePagev1 ? 'none !important' : menuListColor.color || '#d6d6e1'
      };`;
      let menuListIconColorStringIcons = `width: 2.9375rem; height: 2.0625rem; margin-left: 7px;`;
      let createButtonStyleString = `background-color:${createButton.backgroundColor ||
        '#d6d6e1'}!important; color:${createButton.color || '#454560'}!important;`;
      let createBtnIconColorString = `fill:${createButton.color || '#454560'};`;
      let bannerImage = `background-image: url(${
        this.props.team.bannerImage
      }); background-position: center`;
      let noBannerImage = `background-image: none`;
      let uA = window.navigator.userAgent,
        isIE =
          /msie\s|trident\/|edge\//i.test(uA) &&
          !!(
            document.uniqueID ||
            document.documentMode ||
            window.ActiveXObject ||
            window.MSInputMethodContext
          );
      let menuListIconSizeString = `${
        isIE ? 'height: 33px; width: 2rem' : 'height: 29px; width: 2rem'
      }`;

      this.setInlineCSS(
        '.search-redesign-header.search-v4 .bootstrap-typeahead-input-main',
        searchBoxStyleString
      );
      this.setInlineCSS(
        '.search-redesign-header.search-v4 .search-button svg path',
        searchIconColorString
      );
      this.setInlineCSS('.header__seacrh-filter .Select-control', searchBoxStyleString);
      this.setInlineCSS('.header__seacrh-filter .Select-value-label>span', searchBoxStyleString);
      this.setInlineCSS(
        '.header__seacrh-filter .Select-arrow-zone svg path',
        searchIconColorString
      );
      this.setInlineCSS('.action-icon-wrapper-redesign', menuListColorString);
      this.setInlineCSS('.action-icon-wrapper-redesign button svg path', menuListIconColorString);
      this.setInlineCSS('.action-icon-wrapper-redesign #guide', menuListIconColorStringGuide);
      this.setInlineCSS(
        '.action-icon-wrapper-redesign .icon-tabs-v1',
        menuListIconColorStringIcons
      );
      this.state.homePagev1 &&
        this.setInlineCSS('.action-icon-wrapper-redesign button svg', menuListIconSizeString);
      this.setInlineCSS('.createOpen', createButtonStyleString);
      this.setInlineCSS('.createOpen svg path', createBtnIconColorString);
      this.setInlineCSS('.topnav-main-container-v4.clearfix', bannerImage);
      this.setInlineCSS(
        '.secondary-nav-container.secondary-nav-container-v4',
        this.props.isFixed ? bannerImage : noBannerImage
      );
    }
  }

  setInlineCSS(selector, cssString) {
    document.querySelectorAll(selector).forEach(function(item, index) {
      item.style.cssText += cssString;
    });
  }

  setSelectView = debounce(() => {
    let value = detectHeaderWidth();
    if (value === this.state.shortSelectField) {
      return;
    }
    this.setState({ shortSelectField: value });
  }, 100);

  handleHelpClick = e => {
    e.preventDefault();
    handleGuideMeHelpClick(
      this.props.team &&
        this.props.team.config.guide_me_config &&
        this.props.team.config.guide_me_config.edition
    );
  };

  sendToSkillCoins(e) {
    e.preventDefault();
    this.props.dispatch(push('/me/skill-coins'));
  }

  toggleSearchInput = () => {
    this.setState({ isSearchInputOpen: !this.state.isSearchInputOpen });
  };

  smartSearchToggleHandler = () => {
    let isSmartSearchOn = !this.state.isSmartSearchOn;
    localStorage.setItem('isSmartSearchOn', isSmartSearchOn);
    this.setState({ isSmartSearchOn });
    if (this.props.location.query.q) {
      if (isSmartSearchOn) {
        this.props.dispatch(push(`/smartsearch?q=${this.props.location.query.q}`));
      } else {
        this.props.dispatch(push(`/search?q=${this.props.location.query.q}&type=all`));
      }
    }
  };

  render() {
    let enableGuideMe =
      (typeof this.state.enableGuideMe == 'undefined' && !this.state.enableGuideMe) ||
      !!this.state.enableGuideMe;
    let imageUrl =
      this.props.user.avatar ||
      this.props.user.picture ||
      (this.props.user.avatarimages && this.props.user.avatarimages.medium) ||
      '';
    let isImageMissing = /anonymous-user/gi.test(imageUrl) || !imageUrl;
    let customMenuStyle = this.state.config.menuListColor || { styles: { color: '#d6d6e1' } };

    let walletBalance = (this.props.currentUser && this.props.currentUser.walletBalance) || 0;

    return (
      <div
        className={`topnav-main-container-v4 ${this.props.isFixed ? '' : 'clearfix'} ${
          this.state.isSearchInputOpen ? 'search-input-block__active' : ''
        } ${this.state.allowAlignSearchInHeader ? 'nav-search-align-container' : ''} ${
          this.state.reduceHeaderSize ? 'reducing-header' : ''
        }`}
      >
        <div className="top-nav-container-v4">
          <div className="topnav-logo-v4">
            <Logo
              coBrandingLogo={this.props.coBrandingLogo}
              enableEdcastLogo={this.props.enableEdcastLogo}
              showEdcastLogo="true"
            />
          </div>
        </div>

        {!this.state.reduceHeaderSize && <div className="hidden-top-nav-container" />}

        <div
          className={`secondary-nav-container ${
            this.state.homePagev1 ? 'fix-v1' : ''
          } secondary-nav-container-v4 ${
            this.props.isFixed ? (this.state.reduceHeaderSize ? '' : 'fixed') : ''
          }`}
          id="secondary-nav"
        >
          {/*/!* Left side of the Navigation - start *!/*/}
          <div className={`left-menu-content`}>
            <MenuLinks
              menuLinks={this.props.menuLinks}
              topNav={this.props.topNav}
              isFixed={this.props.isFixed && !this.state.reduceHeaderSize}
              queryTerm={this.props.queryTerm}
              customMenuStyle={customMenuStyle}
            />
          </div>

          {this.state.isSmartSearch && (
            <div
              className="smartSearchToggle"
              style={!this.state.homePagev1 ? { marginTop: 0 } : null}
            >
              <Toggle
                label="SmartSearch"
                onToggle={this.smartSearchToggleHandler}
                defaultToggled={this.state.isSmartSearchOn}
              />
            </div>
          )}

          <div
            className={`search-redesign-header search-input-block action-icon-list horizontal-spacing-large search-v1 ${
              this.state.homePagev1 ? 'fix-v1' : ''
            } search-v4`}
          >
            <SearchV1
              queryType={this.props.queryType}
              queryTerm={this.props.queryTerm}
              isSmartSearchOn={this.state.isSmartSearchOn && this.state.isSmartSearch}
              short={this.state.shortSelectField}
            />
          </div>
          {/* Right Side of the Navigation - start */}
          <div
            className={`search-redesign-header action-icon-list horizontal-spacing-large search-v1 profile-menu align-header-icons profile-menu__include-guide-me ${
              this.state.homePagev1 ? 'action-icon-list-v1' : ''
            }`}
          >
            <div
              className={`action-icon-wrapper-redesign ${
                !isImageMissing ? 'user-name-redesign-ie' : ''
              }`}
            >
              <ProfileDropdown
                profile={this.props.profileDropdown}
                teams={this.props.teams}
                isAdmin={this.props.isAdmin}
              />
            </div>
            {this.state.edcastWallet && (
              <div className="action-icon-wrapper-redesign">
                <a
                  tabIndex={0}
                  className={`action-icon-wrapper ${
                    this.state.homePagev1 ? 'action-icon-wrapper-v1' : ''
                  }`}
                  href="#"
                  onClick={this.sendToSkillCoins.bind(this)}
                >
                  <IconButton tabIndex={-1} aria-label="Skill coins" style={{ padding: '0' }}>
                    <SkillCoins focusable="false" walletBalance={walletBalance} color={'#6f708b'} />
                  </IconButton>
                  <span className="username-redesign">Skillcoins</span>
                </a>
              </div>
            )}
            <div className="action-icon-wrapper-redesign">
              <Notifications homePagev1={this.state.homePagev1} />
            </div>
            {enableGuideMe && (
              <div className="action-icon-wrapper-redesign">
                <a
                  tabIndex={0}
                  className="action-icon-wrapper"
                  aria-label="Guide Me"
                  role="button"
                  href="#"
                  onClick={this.handleHelpClick}
                >
                  <IconButton tabIndex={-1} style={{ padding: '0' }}>
                    {this.state.homePagev1 ? (
                      <MyGuide
                        focusable="false"
                        viewBox={'10 0 31 33'}
                        className="action-icon"
                        fill="none"
                        color={'#6f708b'}
                      />
                    ) : (
                      <Help focusable="false" className="action-icon" color={'#6f708b'} />
                    )}
                  </IconButton>
                  <span className="username-redesign">GuideMe</span>
                </a>
              </div>
            )}

            <div className="action-icon-wrapper-redesign">
              <MoreOptions />
            </div>
            {Permissions.has('MANAGE_CARD') && (
              <div className="create-btn-container">
                <CreateInsightModal />
              </div>
            )}
          </div>
        </div>
        {this.setConfigStyle()}
      </div>
    );
  }
}

TopNav.propTypes = {
  coBrandingLogo: PropTypes.string,
  queryTerm: PropTypes.string,
  queryType: PropTypes.string,
  profileDropdown: PropTypes.object,
  team: PropTypes.object,
  teams: PropTypes.array,
  user: PropTypes.object,
  enableEdcastLogo: PropTypes.bool,
  isAdmin: PropTypes.bool,
  isFixed: PropTypes.bool,
  menuLinks: PropTypes.array,
  fixNav: PropTypes.bool,
  topNav: PropTypes.bool,
  currentUser: PropTypes.object,
  location: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS()
  };
}
export default connect(mapStoreStateToProps)(TopNav);
