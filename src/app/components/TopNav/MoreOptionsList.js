import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import keys from 'lodash/keys';
import findKey from 'lodash/findKey';

import Popover from 'material-ui/Popover';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import colors from 'edc-web-sdk/components/colors/index';
import GroupIcon from 'edc-web-sdk/components/icons/GroupIcon.jsx';
import TeamIcon from 'edc-web-sdk/components/icons/TeamIcon.jsx';
import GroupActiveIcon from 'edc-web-sdk/components/icons/GroupActiveIcon';
import TeamActiveIcon from 'edc-web-sdk/components/icons/TeamActiveIcon';
import LeaderboardIcon from 'edc-web-sdk/components/icons/LeaderboardIcon';
import LeaderboardActiveIcon from 'edc-web-sdk/components/icons/LeaderboardActiveIcon';
import MarketplaceIcon from 'edc-web-sdk/components/icons/MarketplaceIcon';
import MarketplaceActiveIcon from 'edc-web-sdk/components/icons/MarketplaceActiveIcon';
import ChannelIcon from 'edc-web-sdk/components/icons/ChannelIcon';
import ChannelIconActive from 'edc-web-sdk/components/icons/ChannelIconActive';
import BookmarkHover from 'edc-web-sdk/components/icons/BookmarkHover';
import Bookmark_v2 from 'edc-web-sdk/components/icons/Bookmark_v2';
import SkillsDirectoryIcon from 'edc-web-sdk/components/icons/SkillsDirectoryIcon';
import SkillsDirectoryHoverIcon from 'edc-web-sdk/components/icons/SkillsDirectoryHoverIcon';
import SkillsGraphIcon from 'edc-web-sdk/components/icons/SkillsGraph';
import SkillsGraphHoverIcon from 'edc-web-sdk/components/icons/SkillsGraphHover';
import { setCookie, readCookie } from 'edc-web-sdk/helpers/cookies';

import * as languages from '../../constants/languages';
import * as actionTypes from '../../constants/actionTypes';

import { updateUserInfo } from '../../actions/currentUserActions';

const languageAbbreviations = languages.langs;

class MoreOptionsList extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      defaultLanguage: this.setDefaultLanguage(),
      availableLanguages: keys(languages.langs),
      hoveredGroup: false,
      hoveredTeam: false,
      hoveredLeaderBoard: false,
      hoveredMarketplace: false,
      groupsOrgSetting: this.props.team.OrgConfig.profile['web/profile/groups'].visible,
      teamsOrgSetting: this.props.team.OrgConfig.profile['web/profile/teams'].visible,
      channelsOrgSetting: this.props.team.OrgConfig.profile['web/profile/channels'].visible,
      profileConfig: this.props.team.OrgConfig.profile,
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      isShowTooltip: false,
      hoveredChannel: false,
      isGroupPageV2: window.ldclient.variation('group-page-v2', false),
      bookmarkStandalone: window.ldclient.variation('bookmark-standalone', false)
    };
    this.styles = {
      list: {
        width: '206px',
        padding: 0,
        borderRadius: '4px',
        boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.5)'
      },
      profileListItem: {
        paddingTop: '8px'
      },
      langDropDown: {
        textAlign: 'center',
        width: '85px',
        height: '30px',
        verticalAlign: 'top',
        border: '1px solid #454560',
        float: 'right'
      },
      iconStyles: {
        height: '41px',
        width: '46px'
      },
      groupIconStyles: {
        height: '41px',
        width: '42px'
      },
      selectLabel: {
        marginTop: '-5px'
      },
      iconSelect: {
        height: '30px',
        width: '30px',
        padding: 0
      },
      menuItemStyle: {
        fontSize: '12px',
        width: '150'
      },
      labelStyleSelect: {
        fontSize: '12px',
        lineHeight: '38px',
        paddingRight: '18px'
      },
      groupIconDisable: {},
      teamIconDisable: {},
      channelIconDisable: {}
    };
    this.prevSelection = 0;
    this.tabs = [
      {
        path:
          this.state.isNewProfileNavigation || this.state.isGroupPageV2
            ? '/org-groups'
            : '/me/groups',
        label: 'Groups'
      },
      {
        path: this.state.isNewProfileNavigation ? '/team' : '/me/team',
        label: 'Team'
      }
    ];
    for (let prop in languages.langs) {
      if (
        props.currentUser &&
        props.currentUser.profile &&
        languages.langs[prop] ===
          (props.currentUser && props.currentUser.profile && props.currentUser.profile.language)
      )
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;

    if (!this.state.groupsOrgSetting) {
      this.styles.groupIconDisable = {
        display: 'none'
      };
    }
    if (!this.state.teamsOrgSetting) {
      this.styles.teamIconDisable = {
        display: 'none'
      };
    }
    if (!this.state.channelsOrgSetting) {
      this.styles.channelIconDisable = {
        display: 'none'
      };
    }
  }
  componentWillReceiveProps(nextProps) {
    if (this.props.open && !nextProps.open) {
      this.prevSelection = 0;
    }
  }

  setDefaultLanguage() {
    let languageAbbreviation =
      (this.props.currentUser.profile && this.props.currentUser.profile.language) ||
      (this.props.team.config && this.props.team.config.DefaultOrgLanguage) ||
      'en';

    let language = Object.keys(languageAbbreviations).find(
      key => languageAbbreviations[key] === languageAbbreviation
    );
    setCookie('selectedLanguage', languageAbbreviation);
    return language;
  }

  handleTabChange = path => {
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  goToTeamsTab = (e, path) => {
    e.preventDefault();
    if (this.state.teamsOrgSetting) {
      this.handleTabChange(path);
    }
    this.props.handleClose();
  };

  goToChannelsTab = e => {
    e.preventDefault();
    this.props.dispatch(push('/channels/all'));
    this.props.handleClose();
  };

  goToBookmarksTab = () => {
    this.props.dispatch(push('/bookmarks'));
    this.props.handleClose();
  };

  goToMarketplaceTab = e => {
    e.preventDefault();
    document.location.href = '/marketplace_demo.html';
  };

  goToGroupsTab = (e, path) => {
    e.preventDefault();
    if (this.state.groupsOrgSetting) {
      this.handleTabChange(path);
    }
    this.props.handleClose();
  };
  goToLeadership = (e, isNewProfileNavigation) => {
    e.preventDefault();
    this.props.dispatch(push(isNewProfileNavigation ? '/leaderboard' : '/me/leaderboard'));
    this.props.handleClose();
  };

  languageSelectHandler(language) {
    this.setState({ defaultLanguage: language });

    let userObj = {
      profile_attributes: { language: languageAbbreviations[language] }
    };

    this.props
      .dispatch(updateUserInfo(this.props.currentUser, userObj))
      .then(data => {
        this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Language updated!',
          autoClose: true
        });
        window.location.reload();
      })
      .catch(() => {
        this.props.dispatch({
          type: actionTypes.OPEN_SNACKBAR,
          message: 'Error Updating Language!',
          autoClose: true
        });
      });
  }

  closeOnEsc = e => {
    if (e.keyCode == 27) {
      this.props.handleClose(true);
    }
  };

  setIconLabel(defaultName) {
    let itemKey = findKey(this.state.profileConfig, item => {
      return defaultName === 'People'
        ? defaultName.toLowerCase() === item.defaultLabel.toLowerCase() ||
            'team' === item.defaultLabel.toLowerCase()
        : defaultName.toLowerCase() === item.defaultLabel.toLowerCase();
    });
    let config = this.state.profileConfig[itemKey];
    let translatedLabel =
      this.isShowCustomLabels &&
      config &&
      config.languages &&
      config.languages[this.profileLanguage] &&
      config.languages[this.profileLanguage].trim();
    return (itemKey && (translatedLabel || tr(config.label))) || tr(defaultName);
  }

  skillsDirectoryClickHandler = () => {
    this.props.dispatch(push('/skills-directory'));
  };

  render() {
    let disableSection =
      this.state.groupsOrgSetting === false && this.state.teamsOrgSetting === false;
    if (this.props.open) {
      setTimeout(() => {
        if (this.prevSelection <= 0) {
          this.refs['focusOnPopover'].focus();
          this.prevSelection++;
        }
      }, 100);
    }
    return (
      <Popover
        open={this.props.open}
        anchorEl={this.props.anchorEl}
        onRequestClose={this.props.requestCloseHandler}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      >
        <List style={this.styles.list} onKeyDown={this.closeOnEsc}>
          {!disableSection && (
            <ListItem
              style={{ padding: '0px 0px 10px' }}
              innerDivStyle={this.styles.profileListItem}
              disabled={true}
              secondaryText={
                <div style={{ overflow: 'auto', height: 'auto' }}>
                  <div ref="focusOnPopover" className="blankContainer" tabIndex={0} />
                  <div className="more-icons">
                    <a
                      href="#"
                      style={{ ...this.styles.groupIconDisable }}
                      onClick={e => this.goToGroupsTab(e, this.tabs[0].path)}
                      onMouseEnter={() => {
                        this.state.groupsOrgSetting && this.setState({ hoveredGroup: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ hoveredGroup: false });
                      }}
                    >
                      {this.state.hoveredGroup ? (
                        <GroupActiveIcon style={this.styles.groupIconStyles} />
                      ) : (
                        <GroupIcon style={this.styles.groupIconStyles} />
                      )}
                      <div>{this.setIconLabel('Groups')}</div>
                    </a>
                    {this.props.team &&
                      this.props.team.config &&
                      this.props.team.config.leaderboard && (
                        <a
                          href="#"
                          onClick={e => this.goToLeadership(e, this.state.isNewProfileNavigation)}
                          onMouseEnter={() => this.setState({ hoveredLeaderBoard: true })}
                          onMouseLeave={() => this.setState({ hoveredLeaderBoard: false })}
                        >
                          {this.state.hoveredLeaderBoard ? (
                            <LeaderboardActiveIcon style={this.styles.groupIconStyles} />
                          ) : (
                            <LeaderboardIcon style={this.styles.groupIconStyles} />
                          )}
                          <div>{this.setIconLabel('Leaderboard')}</div>
                        </a>
                      )}
                    <a
                      href="#"
                      style={{ ...this.styles.teamIconDisable }}
                      onClick={e => this.goToTeamsTab(e, this.tabs[1].path)}
                      onMouseEnter={() => {
                        this.state.teamsOrgSetting && this.setState({ hoveredTeam: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ hoveredTeam: false });
                      }}
                    >
                      {this.state.hoveredTeam ? (
                        <TeamActiveIcon style={this.styles.iconStyles} />
                      ) : (
                        <TeamIcon style={this.styles.iconStyles} />
                      )}
                      <div>{this.setIconLabel('People')}</div>
                    </a>
                    <a
                      href="#"
                      style={{ ...this.styles.channelIconDisable }}
                      onClick={e => this.goToChannelsTab(e)}
                      onMouseEnter={() => {
                        this.setState({ hoveredChannel: true });
                      }}
                      onMouseLeave={() => {
                        this.setState({ hoveredChannel: false });
                      }}
                    >
                      {this.state.hoveredChannel ? (
                        <ChannelIconActive fill="#454560" style={this.styles.groupIconStyles} />
                      ) : (
                        <ChannelIcon style={this.styles.groupIconStyles} />
                      )}
                      <div>{this.setIconLabel('Channels')}</div>
                    </a>
                    {this.state.bookmarkStandalone && (
                      <a
                        href="JavaScript:void(0);"
                        onClick={this.goToBookmarksTab}
                        onMouseEnter={() => {
                          this.setState({ hoveredBookmark: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ hoveredBookmark: false });
                        }}
                      >
                        {this.state.hoveredBookmark ? (
                          <BookmarkHover fill="#454560" style={this.styles.groupIconStyles} />
                        ) : (
                          <Bookmark_v2 style={this.styles.groupIconStyles} />
                        )}
                        <div>{this.setIconLabel('Bookmarks')}</div>
                      </a>
                    )}
                    {window.ldclient.variation('instructor-marketplace', false) && (
                      <a
                        ref="marketplace"
                        href="#"
                        onClick={e => this.goToMarketplaceTab(e)}
                        onMouseEnter={() => {
                          this.setState({ hoveredMarketplace: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ hoveredMarketplace: false });
                        }}
                      >
                        {this.state.hoveredMarketplace ? (
                          <MarketplaceActiveIcon
                            fill="#454560"
                            style={this.styles.groupIconStyles}
                          />
                        ) : (
                          <MarketplaceIcon style={this.styles.groupIconStyles} />
                        )}
                        <div>{this.setIconLabel('Instructor Marketplace')}</div>
                      </a>
                    )}
                    {(window.ldclient.variation('skills-graph', false) ||
                      process.env.NODE_ENV === 'development') && (
                      <a
                        href="/skills-graph"
                        onMouseEnter={() => {
                          this.setState({ skillsGraphHovered: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ skillsGraphHovered: false });
                        }}
                      >
                        {this.state.skillsGraphHovered ? (
                          <SkillsGraphHoverIcon style={this.styles.iconStyles} />
                        ) : (
                          <SkillsGraphIcon style={this.styles.iconStyles} />
                        )}
                        <div style={{ marginTop: 0 }}>Skills Graph</div>
                      </a>
                    )}
                    {(window.ldclient.variation('skills-directory', false) ||
                      process.env.NODE_ENV === 'development') && (
                      <a
                        onClick={this.skillsDirectoryClickHandler}
                        onMouseEnter={() => {
                          this.setState({ skillsDirectoryHover: true });
                        }}
                        onMouseLeave={() => {
                          this.setState({ skillsDirectoryHover: false });
                        }}
                      >
                        {this.state.skillsDirectoryHover ? (
                          <SkillsDirectoryHoverIcon style={this.styles.iconStyles} />
                        ) : (
                          <SkillsDirectoryIcon style={this.styles.iconStyles} />
                        )}
                        <div style={{ marginTop: 0 }}>Skills Directory</div>
                      </a>
                    )}
                  </div>
                </div>
              }
              secondaryTextLines={2}
            />
          )}

          {!disableSection && (
            <div>
              <Divider />
            </div>
          )}

          <ListItem
            style={{ backgroundColor: colors.concrete, padding: '10px' }}
            disabled={true}
            primaryText={
              <div className="row">
                <div className="small-6 translation-labels">
                  <span className="main-label">{tr('Translations')}</span>
                  <span>{tr('Pick a language')}</span>
                </div>
                <div className="small-6">
                  <SelectField
                    value={this.state.defaultLanguage}
                    style={this.styles.langDropDown}
                    maxHeight={250}
                    underlineStyle={{ display: 'none' }}
                    labelStyle={this.styles.labelStyleSelect}
                    iconStyle={this.styles.iconSelect}
                    menuItemStyle={this.styles.menuItemStyle}
                  >
                    {this.state.availableLanguages.sort().map((language, index) => {
                      return (
                        <MenuItem
                          key={index}
                          value={language}
                          innerDivStyle={{ padding: '0 8px' }}
                          primaryText={language}
                          onTouchTap={this.languageSelectHandler.bind(this, language)}
                        />
                      );
                    })}
                  </SelectField>
                </div>
              </div>
            }
          />
        </List>
      </Popover>
    );
  }
}

MoreOptionsList.propTypes = {
  open: PropTypes.bool,
  team: PropTypes.object,
  pathname: PropTypes.string,
  anchorEl: PropTypes.object,
  currentUser: PropTypes.object,
  requestCloseHandler: PropTypes.func,
  handleClose: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(MoreOptionsList);
