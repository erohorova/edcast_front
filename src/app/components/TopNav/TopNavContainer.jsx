import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TopNav from './TopNav';
import TopNavV2 from './TopNavV2';
import TopNavV3 from './TopNavV3';
import TopNavV4 from './TopNavV4';
import TopNavV5 from './TopNavV5';
import TopNavPublicV3 from './TopNavV3Public';
import TopNavPublicV4 from './TopNavV4Public';
import { openPaymentModal, close } from '../../actions/modalActions';
import throttle from 'lodash/throttle';
import findIndex from 'lodash/findIndex';
import { addEvent, removeEvent } from '../../utils/eventManager';
import { push } from 'react-router-redux';
import calculateTeemFeedPosition from '../../utils/calculateTeemFeedPosition';

class TopNavContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.filterTeams = this.filterTeams.bind(this);
    this.state = {
      fixNav: false,
      queryTerm: null,
      topNavVersion: window.ldclient.variation('topnav-version', 'V2'),
      edcastPaywall: window.ldclient.variation('edcast-paywall', false)
    };

    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
    this._bindShowPaywallEvents = this._bindShowPaywallEvents.bind(this);
  }

  componentDidMount() {
    let onboarding = this.props.onboarding;
    let onboardingUser = onboarding.get('user');

    if (
      this.props.onboarding &&
      Object.keys(onboardingUser).length &&
      (onboardingUser.get('onboarding_steps_names').size &&
        onboarding.get('onboarding_completed') === false) &&
      this.onboardingVersion === 'v1' &&
      this.props.path !== '/onboarding'
    ) {
      this.props.dispatch(push('/onboarding'));
      return;
    }

    if (
      this.props.params &&
      this.props.params.handle &&
      !this.props.currentUser.get('isLoggedIn')
    ) {
      return;
    }
    this._standaloneRoot = document.getElementsByClassName('standalone-root')[0];
    if (!this.props.isModal) {
      let inactivityTimeout = this.props.team.get('config').inactive_web_session_timeout;
      inactivityTimeout = inactivityTimeout ? inactivityTimeout * 60 * 1000 : 0;
    }
    let el = this.props.isModal ? this._standaloneRoot : window;
    el.addEventListener('scroll', this.handleScroll);
    let search;
    let q = '';
    let type = '';
    if (this.props.location && this.props.location.search) {
      search = this.props.location.search.split('&');
      q = search[0] ? search[0].replace('?q=', '').replace(/%20/g, ' ') : '';
      type = search[1] ? search[1].replace('type=', '').replace(/%20/g, ' ') : '';
    }
    this.setState({
      queryTerm: q,
      queryType: type
    });

    this.props.team.get('config') &&
      this.props.team.get('config').enable_guide_me &&
      setTimeout(this.guideMe(), 3000);
    if (
      this.state.edcastPaywall &&
      this.props.team.get('memberPay') &&
      !this.props.currentUser.get('isSuperAdmin') &&
      !this.props.currentUser.get('orgAnnualSubscriptionPaid')
    ) {
      this._bindShowPaywallEvents();
      addEvent(document, 'click', this._bindShowPaywallEvents);
    }

    // We have many places where the user can be redirected to
    // Implementing this as the TopNavContainer is loaded on all pages
    if (window.opener) {
      window.opener.postMessage(this.props.currentUser.toJS(), '*');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      !(
        this.state.edcastPaywall &&
        this.props.team.get('memberPay') &&
        !this.props.currentUser.get('orgAnnualSubscriptionPaid')
      )
    ) {
      removeEvent(document, 'click', this._bindShowPaywallEvents);
    }
    let search;
    let q = '';
    let type = '';
    if (nextProps.location && nextProps.location.search) {
      search = nextProps.location.search.split('&');
      q = search[0] ? search[0].replace('?q=', '').replace(/%20/g, ' ') : '';
      type = search[1] ? search[1].replace('type=', '').replace(/%20/g, ' ') : '';
    }
    if (q !== this.state.queryTerm) {
      this.setState({
        queryTerm: q,
        queryType: type
      });
    }
  }

  componentWillUnmount() {
    let el = this.props.isModal ? this._standaloneRoot : window;
    el.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextProps.currentUser !== this.props.currentUser ||
      nextProps.team !== this.props.team ||
      (this.props.location && this.props.location.pathname !== nextProps.location.pathname)
    ) {
      return true;
    } else if (
      nextState.fixNav !== this.state.fixNav ||
      nextState.queryTerm !== this.state.queryTerm ||
      nextState.queryType !== this.state.queryType
    ) {
      return true;
    } else return false;
  }

  guideMe() {
    let clientKey = '';
    if (
      this.props.team.get('OrgConfig').guideme &&
      this.props.team.get('OrgConfig').guideme['web/guideme/client_key']
    ) {
      let key = this.props.team.get('OrgConfig').guideme['web/guideme/client_key'];
      clientKey = key.userKey && key.visible ? key.userKey : clientKey;
    }

    let a = document.createElement('script');
    if (
      this.props.team.get('config').enable_guide_me &&
      this.props.team.get('config').guide_me_config &&
      this.props.team.get('config').guide_me_config.edition == 'ent'
    ) {
      a.src = 'https://cdn.guideme.io/guideme-player/v3/guideme.js';
      window.myGuideOrgKey = this.props.team.get('config').guide_me_config.encrypted_org_key || '';
    } else {
      a.src = 'https://cdn.guideme.io/guideme-player/guideme.js';
    }

    document.body.appendChild(a);
    window.guideMe = { userKey: clientKey, trackingId: '' };
  }

  handleScroll = throttle(
    event => {
      let hiddenTopNavContainer = document.getElementsByClassName('hidden-top-nav-container')[0];
      let scrollTop = this.props.isModal
        ? this._standaloneRoot.scrollTop
        : document.documentElement.scrollTop || document.body.scrollTop;
      if (this.scrollTop > scrollTop && this.state.fixNav && this.updateScroll) {
        hiddenTopNavContainer &&
          ~hiddenTopNavContainer.className.indexOf(' visible') &&
          hiddenTopNavContainer.classList.remove('visible');
        this.updateScroll = false;
      }
      if (scrollTop != undefined && scrollTop >= 30) {
        if (this.state.fixNav !== true) {
          hiddenTopNavContainer &&
            !~hiddenTopNavContainer.className.indexOf(' visible') &&
            hiddenTopNavContainer.classList.add('visible');
          window.scrollBy(0, hiddenTopNavContainer ? hiddenTopNavContainer.offsetHeight : 0);
          this.setState(
            {
              fixNav: true
            },
            () => {
              this.updateScroll = true;
            }
          );
        }
      } else {
        if (this.state.fixNav !== false) {
          this.setState(
            {
              fixNav: false
            },
            () => {
              calculateTeemFeedPosition();
            }
          );
        }
      }
      this.scrollTop = this.props.isModal
        ? this._standaloneRoot.scrollTop
        : document.documentElement.scrollTop || document.body.scrollTop;
    },
    10,
    { leading: false }
  );

  filterTeams() {
    let teams = this.props.currentUser.get('teams').toJS();
    let currentOrgIndex = findIndex(teams, { id: this.props.team.get('orgId') }) || {};
    !!~currentOrgIndex && teams.splice(currentOrgIndex, 1);
    let filterTeams = teams.map(team => {
      return {
        label: team.name || '',
        url: window.location.hostname.replace(/([^.]*)/, '//' + (team.hostName || ''))
      };
    });
    return filterTeams;
  }

  _bindShowPaywallEvents() {
    if (!document.querySelector('.paywall-modal-root')) {
      this.props.dispatch(push(`/me`));
      this.props.dispatch(close());
      this.props.dispatch(openPaymentModal(this.state.card));
    }
  }

  render() {
    let menuLinks = [
      { text: 'HOME', url: '/' },
      { text: 'DISCOVER', url: '/discover' },
      { text: 'TEAM', url: '/home/members' },
      { text: 'ME', url: `/@${this.props.handle}` }
    ];

    let teams = this.filterTeams();
    let teamConfig = this.props.team.get('config');
    let isDefaultTeam = window.location.host.split('.')[0] === 'www';
    if (isDefaultTeam) {
      menuLinks.splice(2, 1); // Remove "TEAM" from menu
    }
    let { name, avatar, email, first_name, last_name } = this.props.currentUser.toJS();
    let handle = this.props.handle;
    let enableEdcastLogo =
      teamConfig.enable_edcast_logo !== undefined ? teamConfig.enable_edcast_logo : true;
    let staticLinks = {
      links: {
        myProfileLink: {
          label: 'Profile',
          url: this.props.handle
        },
        settingsLink: {
          label: 'Settings',
          url: '/settings'
        },
        signOutLink: {
          label: 'Sign Out',
          url: '/sign_out'
        },
        impersonationLink: {
          enabled: false,
          url: '#',
          label: 'Impersonate',
          method: 'post'
        }
      }
    };
    if (isDefaultTeam) {
      staticLinks.links['newOrganizationLink'] = {
        label: 'Create a Team',
        url: 'https://' + window.location.hostname + '/organizations/new'
      };
    }
    let user = { user: { name, avatar, email, handle, first_name, last_name } };
    let props = {
      isFixed: this.state.fixNav,
      coBrandingLogo: this.props.team.get('coBrandingLogo'),
      user,
      profileDropdown: { ...this.props.profileDropdown, ...user, ...staticLinks },
      teams,
      isAdmin: this.props.currentUser.get('isAdmin'),
      topNav: false,
      queryTerm: this.state.queryTerm,
      queryType: this.state.queryType,
      menuLinks,
      enableEdcastLogo,
      location: this.props.location
    };

    let TopNavVersion;
    let TopNavPublicVersion;
    switch (this.state.topNavVersion) {
      case 'V3':
        TopNavVersion = <TopNavV3 {...props} />;
        TopNavPublicVersion = <TopNavPublicV3 {...props} />;
        break;
      case 'V4':
        TopNavVersion = this.props.team.get('bannerImage') ? (
          <TopNavV4 {...props} />
        ) : (
          <TopNavV3 {...props} />
        );
        TopNavPublicVersion = this.props.team.get('bannerImage') ? (
          <TopNavPublicV4 {...props} />
        ) : (
          <TopNavPublicV3 {...props} />
        );
        break;
      case 'V5':
        TopNavVersion = this.props.team.get('bannerImage') ? (
          <TopNavV5 {...props} />
        ) : (
          <TopNavV3 {...props} />
        );
        TopNavPublicVersion = this.props.team.get('bannerImage') ? (
          <TopNavPublicV4 {...props} />
        ) : (
          <TopNavPublicV3 {...props} />
        );
        break;
      default:
        TopNavVersion = <TopNavV2 {...props} />;
        TopNavPublicVersion = <TopNavPublicV3 {...props} />;
        break;
    }

    let publicProfile =
      this.props.params && this.props.params.handle && !this.props.currentUser.get('isLoggedIn');
    return (
      <div id="top-nav" className={this.state.fixNav ? 'fixed' : ''}>
        {this.props.currentUser.get('isLoaded') && TopNavVersion}

        {publicProfile && TopNavPublicVersion}

        <div
          className={`navigation-wrapped-content ${
            !this.props.currentUser.get('isLoaded') ? 'navigation-wrapped-content_full-height' : ''
          }`}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}

TopNavContainer.propTypes = {
  currentUser: PropTypes.any,
  config: PropTypes.any,
  team: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser,
    team: state.team,
    onboarding: state.onboarding,
    path: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(TopNavContainer);
