import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import UserScore from '../../home/LeftRailUserScorev2';
import LeftRailClcProgress from '../../home/LeftRailClcProgressv2';
import { Tabs, Tab } from 'material-ui/Tabs';
import ReactDOM from 'react-dom';
import colors from 'edc-web-sdk/components/colors/index';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getClcRecords } from 'edc-web-sdk/requests/clc';
import { getUserPwcRecords } from 'edc-web-sdk/requests/users.v2';
import EmptyBlock from '../../discovery/EmptyBlock';
import { getCoursesV2 } from '../../../actions/coursesActions';
import CourseCarousel from './CourseCarousel';
import { getSpecificUserInfo, getCustomTopics } from '../../../actions/currentUserActions';
import DashboardProfileContainer from '../DashboardProfileContainer';
import Spinner from '../../common/spinner';
import { getIntegration, integrationConnectStatus } from '../../../actions/profileActions';
import { openIntegrationModal, openSkillCreationModal } from '../../../actions/modalActions';
import ConfirmationModal from '../../modals/ConfirmationCommentModal';
import { removeSkill } from 'edc-web-sdk/requests/users';
import { getUserSkills } from '../../../actions/usersActions';

class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      popover: {
        width: ''
      }
    };

    this.state = {
      openConfirm: false,
      removeId: '',
      activeTab: {},
      open: false,
      pathwayBadges: window.ldclient.variation('pathway-badge', false),
      enableWallet: this.props.config.wallet || false,
      clc: window.ldclient.variation('clc', false),
      skillsMap: window.ldclient.variation('skillsmap', false),
      externalIntegration: window.ldclient.variation('enable-external-integrations', false),
      onboardingVersion: window.ldclient.variation('onboarding-version', 'v1'),
      showPWCAssesment: window.ldclient.variation('show-pwc', false),
      courseState: 'Content Loading...',
      courses: [],
      enableBIA: this.props.team.get('config').enabled_bia,
      ClcExists: false,
      upgradeSkillsPassport:
        this.props.team &&
        this.props.team.get('OrgConfig') &&
        !this.props.team.get('OrgConfig').profileShowUserContent
    };

    this.enableExternalIntegrations = window.ldclient.variation(
      'enable-external-integrations',
      false
    );

    this.tabs = [
      { path: '/me', label: 'Profile', pathsList: ['/me'] },
      { path: '/me/groups', label: 'Groups' },
      {
        path: '/me/content',
        label: 'Content',
        options: [
          { path: '/me/dashboard', label: 'Performance' },
          { path: '/me/content', label: 'SmartCards' },
          { path: '/me/channels', label: 'Channels' }
        ],
        pathsList: [
          '/me/dashboard',
          '/me/content',
          '/me/channels',
          '/me/content/Pathways',
          '/me/content/VideoStreams',
          '/me/content/SmartBites',
          '/me/content/all'
        ]
      },
      {
        path: '/me/learning',
        label: 'My Learning Plan',
        pathsList: [
          '/me/learning',
          '/me/learning/all',
          '/me/learning/active',
          '/me/learning/completed',
          '/me/learning/bookmarks'
        ]
      },
      {
        path: '/me/team',
        label: 'Team',
        pathsList: ['/me/team/following', '/me/team/followers'],
        options: [{ path: '/me/leaderboard', label: 'Leaderboard' }]
      },
      { path: '/me/channels', label: 'Channels' }
    ];

    this.removeSkill = this.removeSkill.bind(this);
  }

  removeSkill() {
    this.setState({ openConfirm: false });
    let payload = {
      skills_user_id: this.state.removeId,
      id: this.props.currentUser.id
    };
    removeSkill(this.props.currentUser.id, payload)
      .then(data => {
        this.props.dispatch(getUserSkills(this.props.currentUser.id));
      })
      .catch(err => {
        console.error(`Error in MyProfile.getUserSkills.func : ${err}`);
      });
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'handle',
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    if (this.state.onboardingVersion === 'v4') {
      let payload = {
        'state[]': ['pending', 'approved'],
        requestor_id: this.props.currentUser.id
      };
      this.props.dispatch(getCustomTopics(payload));
    }
    this.props
      .dispatch(userInfoCallBack)
      .then(() => {
        if (this.state.showPWCAssesment) {
          getUserPwcRecords(this.props.currentUser.id)
            .then(data => {
              let records = new Array();
              data.pwcRecords &&
                data.pwcRecords.forEach(record => {
                  let temp = new Object();
                  temp[this.props.currentUser.id] = record.level;
                  temp['fullScore'] = 3;
                  temp['topic'] = record.skill.topic_label;
                  records.push(temp);
                });
              this.setState({ pwcRecords: records });
            })
            .catch(err => {
              console.error(`Error in Dashboard.getUserPwcRecords.func : ${err}`);
            });
        }
        this.props.dispatch(getUserSkills(this.props.currentUser.id));
        this.props.dispatch(getIntegration(this.props.currentUser.id));
        this.props
          .dispatch(getCoursesV2(this.props.currentUser.id))
          .then(response => {
            this.setState({
              courses: response && response.externalCourses,
              courseState:
                response && response.externalCourses && response.externalCourses.length > 0
            });
          })
          .catch(err => {
            console.error(`Error in Dashboard.getCoursesV2.func : ${err}`);
          });

        if (this.state.clc) {
          let clcPayload = {
            clc: {
              entity_id: this.props.team.get('orgId'),
              entity_type: 'Organization'
            }
          };
          getClcRecords(clcPayload)
            .then(data => {
              this.setState({ ClcExists: data.clcs.length > 0 });
            })
            .catch(err => {
              console.error(`Error in Dashboard.getClcRecords.func : ${err}`);
            });
        }
      })
      .catch(err => {
        console.error(`Error in Dashboard.componentDidMount.func: ${err}`);
      });
  }

  showMyScore(val) {
    let show = val;
    if (show === undefined) {
      return true;
    } else {
      return show.visible;
    }
  }

  handleConnectKeyPress = (e, integration) => {
    if (e.keyCode === 13) {
      this.handleConnectClick(integration);
    }
  };

  handleConnectClick = integration => {
    this.props.dispatch(openIntegrationModal(integration, this.importClickHandler));
  };

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  renderTab = (tab, index, label) => {
    let tabHoverColor = this.state.hoveredTab === tab.label ? { color: colors.primary200 } : {};
    return (
      <Tab
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        label={tr(label)}
        style={tabHoverColor}
        value={tab.path}
        className={this.toCamelCase('me ' + tab.label)}
        key={index}
        onClick={this.handleTabChange.bind(this, tab.path)}
        onMouseOver={this.handleTabHover.bind(this, tab)}
        onMouseLeave={this.handleTabLeave.bind(this, tab)}
      />
    );
  };

  handleTabChange = path => {
    this.setState({
      open: false
    });
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(
      function(tab) {
        if (currentPath === tab.path || (tab.pathsList && tab.pathsList.includes(currentPath))) {
          value = tab.path;
        }
      }.bind(this)
    );
    return value;
  };

  handleTabLeave = tab => {
    this.setState({ hoveredTab: false });
  };

  handleTabHover = tab => {
    if (tab.options) {
      this.setState({
        activeTab: tab,
        open: true,
        hoveredTab: tab.label
      });
    } else {
      this.setState({ hoveredTab: tab.label });
    }
  };

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.filter(tab => tab.visible);
  }
  toggleModal = id => {
    this.setState({ openConfirm: !this.state.openConfirm, removeId: id });
  };

  modalSkillOpenToAddSkills = (e, skill) => {
    e.preventDefault();
    this.modalSkillOpen(skill);
  };

  modalSkillOpen = (skill = null) => {
    this.props.dispatch(openSkillCreationModal(skill));
  };

  removeSkill = () => {
    this.setState({ openConfirm: false });
    let payload = {
      skills_user_id: this.state.removeId,
      id: this.props.currentUser.id
    };
    removeSkill(this.props.currentUser.id, payload)
      .then(data => {
        this.props.dispatch(getUserSkills(this.props.currentUser.id));
      })
      .catch(err => {
        console.error(`Error in MyProfile.getUserSkills.func : ${err}`);
      });
  };

  render() {
    let user =
      (this.props.currentUser &&
        this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.profile) ||
      this.props.currentUser;
    let showContinuousLearning = this.props.team.get('OrgConfig').sections[
      'web/sections/continuousLearning'
    ].visible;
    let showMyScoreVal = this.props.team.get('OrgConfig').sections['web/sections/myScore'];
    let showMyScore = this.showMyScore(showMyScoreVal);
    let tabValue = this.selectTabValue(this.props.pathname);
    let output = [];

    let ProfileController = this.listTab(this.props.team.get('OrgConfig').profile);
    let isChannelsOn = Boolean(
      ProfileController.find(item => item.key === 'web/profile/channels' && item.visible)
    );

    ProfileController.sort((a, b) => a.index - b.index);

    ProfileController.filter(item => item.visible).map(item => {
      let label = item.label || item.defaultLabel;
      switch (item.key) {
        case 'web/profile/profile':
          output.push(this.renderTab(this.tabs[0], item.index, label));
          break;
        case 'web/profile/groups':
          output.push(this.renderTab(this.tabs[1], item.index, label));
          break;
        case 'web/profile/content':
          let contentTabs = this.tabs[2];
          if (isChannelsOn && contentTabs.options) {
            contentTabs.options = contentTabs.options.filter(
              itemValue => itemValue.label !== 'Channels'
            );
          }

          contentTabs.options = null;
          output.push(this.renderTab(contentTabs, item.index, label));
          break;
        case 'web/profile/learningQueue':
          output.push(this.renderTab(this.tabs[3], item.index, label));
          break;
        case 'web/profile/teams':
          output.push(this.renderTab(this.tabs[4], item.index, label));
          break;
        case 'web/profile/channels':
          output.push(this.renderTab(this.tabs[5], item.index, label));
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });

    let isfullLayout =
      !(
        this.state.skillsMap ||
        (showMyScore && this.state.ClcExists) ||
        (this.state.clc && this.state.ClcExists)
      ) || false;

    let showExpertise =
      this.props.config &&
      this.props.config.onboarding_options &&
      this.props.config.onboarding_options.add_expertise;
    let { profileCourses } = this.props;
    let { integrationList } = this.props;
    let publicProfile = this.props.params && this.props.params.handle;

    return (
      <div id="dashboard">
        <DashboardProfileContainer
          profileCourses={profileCourses}
          handleConnectKeyPress={this.handleConnectKeyPress}
          handleConnectClick={this.handleConnectClick}
          integrationList={integrationList}
          showAccountLinking={this.enableExternalIntegrations}
          upgradeSkillsPassport={this.state.upgradeSkillsPassport}
          user={user}
          publicProfile={publicProfile}
          toggleModal={this.toggleModal}
          modalSkillOpen={this.modalSkillOpen.bind(this)}
          enableBIA={this.state.enableBIA}
          modalSkillOpenToAddSkills={this.modalSkillOpenToAddSkills}
          componentName="Dashboard"
        />
        {this.state.openConfirm && (
          <ConfirmationModal
            title={'Confirm'}
            message={'Do you want to delete the skill?'}
            closeModal={this.toggleModal}
            callback={this.removeSkill}
          />
        )}
      </div>
    );
  }
}

Dashboard.propTypes = {
  currentUser: PropTypes.object,
  config: PropTypes.object,
  team: PropTypes.object,
  profileCourses: PropTypes.any,
  pathname: PropTypes.string,
  params: PropTypes.object,
  integrationList: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    team: state.team,
    config: state.team.get('config'),
    profileCourses: state.profile.get('externalCourses'),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    integrationList: state.profile.get('integrationList')
  };
}

export default connect(mapStoreStateToProps)(Dashboard);
