import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import Carousel from '../../common/Carousel';
import Course from '../../discovery/Course.jsx';

class CourseCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.newProfileLD = window.ldclient.variation('new-profile', false);
    this.state = {
      viewMore: false
    };
  }

  clickViewMoreHandler = () => {
    this.setState(prevState => {
      const viewMore = !prevState.viewMore;
      return { viewMore };
    });
  };

  render() {
    return (
      <div id="dashboard-course-carousel">
        <div style={{ margin: '0px 0px 15px 0px' }}>
          <span className="profile-title">
            {tr('Courses ')} {!this.newProfileLD && <span>({this.props.courses.length})</span>}
          </span>
        </div>
        <div className="channel-card-wrapper">
          {!this.newProfileLD ? (
            <div className="channel-card-wrapper-inner">
              <Carousel slidesToShow={this.props.numberOfCourses}>
                {this.props.courses.map(course => {
                  return (
                    <div key={course.id}>
                      <Course
                        name={course.name}
                        logoUrl={course.imageUrl}
                        startDate={course.assignedDate}
                        endDate={course.dueDate}
                        status={course.status}
                        url={course.courseUrl}
                      />
                    </div>
                  );
                })}
              </Carousel>
            </div>
          ) : (
            <div className="linked-courses">
              <div className="linked-courses-container">
                {(!this.state.viewMore ? this.props.courses.slice(0, 6) : this.props.courses).map(
                  course => {
                    return (
                      <div className="course" key={course.id}>
                        <Course
                          name={course.name}
                          logoUrl={course.imageUrl}
                          startDate={course.assignedDate}
                          endDate={course.dueDate}
                          status={course.status}
                          url={course.courseUrl}
                        />
                      </div>
                    );
                  }
                )}
              </div>
              {(this.props.courses || []).length > 3 && (
                <div className="view-more-btn">
                  <span
                    className="cursor-pointer"
                    onClick={() => {
                      this.clickViewMoreHandler();
                    }}
                  >
                    {this.state.viewMore ? tr('View Less') : tr('View More')}
                  </span>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

CourseCarousel.propTypes = {
  numberOfCourses: PropTypes.number,
  courses: PropTypes.array
};
export default connect()(CourseCarousel);
