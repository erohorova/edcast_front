import React, { Component } from 'react';
import { tr } from 'edc-web-sdk/helpers/translations';
import fileDownload from 'js-file-download';
import transcript from 'edc-web-sdk/requests/transcript';

class DownloadTranscript extends Component {
  constructor(props) {
    super(props);
    this.isTranscriptEnabled = window.ldclient.variation('user-transcript', false);
  }

  getTranscript = e => {
    e.preventDefault();
    transcript()
      .then(response => {
        fileDownload(response.xhr.response, 'Transcript.pdf');
      })
      .catch(err => {
        console.error(`Error in DownloadTranscript.getTranscript.func : ${err}`);
      });
  };

  render() {
    return (
      <div className="download-transcript-container">
        {this.isTranscriptEnabled && (
          <div>
            <a
              onClick={this.getTranscript}
              href="JavaScript:void(0);"
              style={{ borderBottom: '1px solid #2199e8' }}
            >
              {tr('Download Transcript')}
            </a>
          </div>
        )}
      </div>
    );
  }
}

DownloadTranscript.propTypes = {};

export default DownloadTranscript;
