import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { _getUserActivity, publishToPublicProfile } from '../../../../actions/profileActions';
import Paper from 'edc-web-sdk/components/Paper';
import ListItem from 'material-ui/List/ListItem';
import BlurImage from '../../../common/BlurImage';
import Spinner from '../../../common/spinner';
import MarkdownRenderer from '../../../common/MarkdownRenderer';
import TimeAgo from 'react-timeago';
import translateTA from '../../../../utils/translateTimeAgo';
import { tr } from 'edc-web-sdk/helpers/translations';
import { openStandaloneOverviewModal } from '../../../../actions/modalStandAloneActions';

class UserActivity extends Component {
  constructor(props) {
    super(props);
    this.userID = props.userID;
  }

  getChildContext() {
    const languageAbbreviation =
      this.props.userProfile.language ||
      (this.props.userProfile.get && this.props.userProfile.get('language')) ||
      'en';
    const formatter = translateTA(languageAbbreviation);
    return {
      loggedInUserID: this.props.loggedInUserID,
      loggedInUserHandle: this.props.loggedInUserHandle,
      userID: this.userID,
      dispatch: this.props.dispatch,
      formatter
    };
  }

  init = userID => {
    const userPostActivity = ['created_card', 'created_pathway', 'created_livestream'];
    const otherUserActivity = [
      'upvote',
      'smartbite_uncompleted',
      'comment',
      'smartbite_completed',
      'created_journey',
      'changed_author',
      'assignment_completed'
    ];

    const payload = {
      user_id: userID,
      only_conversations: false,
      limit: 3,
      offset: 0,
      'activity_actions[]': [...userPostActivity, ...otherUserActivity]
    };

    this.props.dispatch(_getUserActivity(payload));
  };

  componentDidMount() {
    this.init(this.userID);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userID !== this.props.userID) {
      this.init(this.props.loggedInUserID);
    }
  }

  render() {
    const { userActivities, isDashboard, publicProfile, dashboardInfo, userID } = this.props;
    const enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'Activity Stream';
    });
    const handle = this.props.handle ? `/@${this.props.handle}` : location.pathname;

    if (!enabledForPublicProfile[0].visible && !isDashboard) {
      return null;
    }

    return (
      <div id="user-activity-list">
        {userActivities && !!userActivities.length && (
          <div className="user-activity-list-title-container">
            <div className="user-activity-list-title">{tr('Activity')}</div>
            <div className="user-activity-list-view-all">
              <a
                href="#"
                onClick={e => {
                  e.preventDefault();
                  this.props.dispatch(push(`/activity${handle}`));
                }}
              >
                {tr('View All')}
              </a>
            </div>
          </div>
        )}
        {userActivities ? (
          <Paper className="user-activity-list-container">
            {isDashboard && !publicProfile && (
              <div className="public-profile-overlay">
                <div className="public-profile-post-handler">
                  {!enabledForPublicProfile[0].visible ? (
                    <button
                      className="public-profile-post-btn"
                      onClick={() => {
                        this.props.dispatch(
                          publishToPublicProfile('Activity Stream', dashboardInfo, userID)
                        );
                      }}
                    >
                      {tr('Post to Profile')}
                    </button>
                  ) : (
                    <button
                      className="public-profile-post-btn remove"
                      onClick={() => {
                        this.props.dispatch(
                          publishToPublicProfile('Activity Stream', dashboardInfo, userID)
                        );
                      }}
                    >
                      {tr('Remove from Profile')}
                    </button>
                  )}
                </div>
              </div>
            )}

            {userActivities.map((userActivity, index) => {
              return (
                <div key={index}>
                  <ActivityContainer userActivity={userActivity} />
                </div>
              );
            })}
          </Paper>
        ) : (
          <Paper className="user-activity-loader">
            <div className="text-center">
              <Spinner />
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    loggedInUserID: state.currentUser.get('id'),
    loggedInUserHandle: state.currentUser.get('handle'),
    userProfile: state.currentUser.get('profile'),
    userActivities: state.profile.get('userActivities'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS()
  };
}

UserActivity.propTypes = {
  userID: PropTypes.string,
  handle: PropTypes.string,
  userProfile: PropTypes.object,
  userActivities: PropTypes.object,
  isDashboard: PropTypes.any,
  publicProfile: PropTypes.any,
  componentName: PropTypes.any
};

UserActivity.childContextTypes = {
  loggedInUserHandle: PropTypes.string,
  loggedInUserID: PropTypes.string,
  userID: PropTypes.string,
  dispatch: PropTypes.func,
  formatter: PropTypes.func
};

export default connect(mapStoreStateToProps)(UserActivity);

/*********************
 * ACTIVITY CONTAINER*
 *********************/

function ActivityContainer(props, context) {
  const { userActivity } = props;
  const { formatter, loggedInUserHandle } = context;
  const { createdAt, user, snippet } = userActivity;

  const style = {
    listItem: {
      padding: '10px 20px',
      alignItems: 'center',
      display: 'flex',
      cursor: 'default'
    },
    avatarBox: {
      userSelect: 'none',
      position: 'relative',
      width: '1.875rem',
      height: '1.875rem',
      margin: '0',
      left: 0,
      display: 'inline-flex',
      zIndex: 10,
      cursor: 'pointer'
    }
  };

  const leftIcon = (
    <BlurImage
      tabIndex={0}
      imageClickHandler={() => {
        activityClickHandler(user.handle, null, null, context.dispatch, loggedInUserHandle);
      }}
      image={user && user.avatarimages && user.avatarimages.tiny}
      titleText={`${snippet}'s profile`}
      style={style.avatarBox}
    />
  );

  return (
    <ListItem
      className="activity-list-item"
      disableKeyboardFocus={true}
      leftIcon={leftIcon}
      innerDivStyle={style.listItem}
      hoverColor="rgba(214, 214, 225, 0.6)"
      primaryText={
        <div className="activity-text-container">
          <ActivityMessage userActivity={userActivity} />
          <TimeAgo date={createdAt} live={false} formatter={formatter} />
        </div>
      }
    />
  );
}

ActivityContainer.propTypes = {
  userActivity: PropTypes.object
};

ActivityContainer.contextTypes = {
  dispatch: PropTypes.func,
  formatter: PropTypes.func,
  loggedInUserHandle: PropTypes.string
};

/*-----------------
 * ACTIVITY MESSAGE*
 ------------------*/

function ActivityMessage(props, context) {
  const { userActivity } = props;
  const { loggedInUserID, currentUserID, loggedInUserHandle } = context;

  let { user, action, snippet, linkable, streamableId } = userActivity;
  let linkUrl, linkPrefix, slug;
  let message = `<span tabIndex={-1} class="user-name">**${
    user.id == loggedInUserID ? 'You' : user.name
  }**</span>`;
  if (snippet.length > 100) {
    snippet = snippet.slice(0, 100) + '...';
  }

  switch (action) {
    case 'created_livestream':
      message += ` ${tr('created')} ${snippet}`;
      break;
    case 'upvote':
      message += ` ${tr('liked')} ${snippet}`;
      break;
    case 'comment':
      message += ` ${tr('commented on')} ${snippet}`;
      break;
    case 'created_card':
      message += ` ${tr('created')} <strong>${tr('SmartCard')}</strong>: ${snippet
        .replace(/[\[\]\*_\\]*/g, '')
        .replace(/\(.*/g, '')}`;
      break;
    case 'created_pathway':
      message += ` ${tr('created')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
      break;
    case 'created_journey':
      message += ` ${tr('created')} <strong>${tr('Journey')}</strong>: ${snippet}`;
      break;
    case 'assignment_completed':
      message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${snippet}`;
      break;
    case 'smartbite_completed':
      switch (linkable.type) {
        case 'journey':
          message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${snippet}`;
          break;
        case 'collection':
          message += ` ${tr('completed')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
          break;
        default:
          message += ` ${tr('completed')} <strong>${tr('SmartCard')}</strong>: ${snippet}`;
          break;
      }
      break;
    case 'smartbite_uncompleted':
      switch (linkable.type) {
        case 'journey':
          message += ` ${tr('uncompleted')} <strong>${tr('Journey')}</strong>: ${snippet}`;
          break;
        case 'collection':
          message += ` ${tr('uncompleted')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
          break;
        default:
          message += ` ${tr('uncompleted')} <strong>${tr('SmartCard')}</strong>: ${snippet}`;
          break;
      }
      break;
    default:
      message += '';
      break;
  }

  switch (linkable.type) {
    case 'journey':
      linkUrl = `journey/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'journey';
      break;
    case 'collection':
      linkUrl = `pathways/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'pathways';
      break;
    case 'card':
      linkUrl = `insights/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'insights';
      break;
    case 'video_stream':
      linkUrl = `video_streams/${streamableId}`;
      slug = streamableId;
      linkPrefix = 'insights';
      break;
    default:
      linkUrl = '#';
      break;
  }

  return (
    <div
      style={{ cursor: 'pointer' }}
      tabIndex={0}
      onClick={() => {
        activityClickHandler(linkUrl, linkPrefix, slug, context.dispatch, loggedInUserHandle);
      }}
    >
      <MarkdownRenderer
        markdown={message}
        onUserNameClick={() => {
          activityClickHandler(user.handle, null, null, context.dispatch, loggedInUserHandle);
        }}
      />
    </div>
  );
}

ActivityMessage.propTypes = {
  userActivity: PropTypes.object
};

ActivityMessage.contextTypes = {
  loggedInUserHandle: PropTypes.string,
  loggedInUserID: PropTypes.string,
  userID: PropTypes.string,
  dispatch: PropTypes.func
};

function activityClickHandler(link, cardType, slug, dispatch, loggedInUserHandle) {
  const modalStandalone = window.ldclient.variation('modal-standalone-page', false);
  if (modalStandalone && cardType) {
    window.history.pushState(null, null, `/${link}`);
    dispatch(openStandaloneOverviewModal({ slug }, cardType));
  } else {
    dispatch(push(`/${link.replace('@', '') === loggedInUserHandle ? 'me' : link}`));
  }
}
