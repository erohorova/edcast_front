import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BreadcrumbV2 from '../../../common/BreadcrumpV2';
import { getUserCustomFields, getAllCustomFields } from 'edc-web-sdk/requests/users.v2';
import { push } from 'react-router-redux';
import Paper from 'edc-web-sdk/components/Paper';
import BlurImage from '../../../common/BlurImage';
import Spinner from '../../../common/spinner';
import MarkdownRenderer from '../../../common/MarkdownRenderer';
import { tr } from 'edc-web-sdk/helpers/translations';
import { openStandaloneOverviewModal } from '../../../../actions/modalStandAloneActions';
import { getUserActivity } from 'edc-web-sdk/requests/profile.v2';
import Card from '../../../cards/Card';
import FollowButtonv3 from 'edc-web-sdk/components/FollowButtonv3';
import { getRecommendedUsers } from 'edc-web-sdk/requests/users';
import { throttle, findIndex, filter } from 'lodash';
import { getUserFromHandle, toggleFollow } from '../../../../actions/usersActions';

class UserActivityStandalone extends Component {
  constructor(props) {
    super(props);
    this.styles = {
      avatarBox: {
        userSelect: 'none',
        height: '6.25rem',
        width: '6.25rem',
        position: 'relative',
        margin: '-50px auto 0 auto',
        border: '4px solid #ffffff'
      },
      userAvatar: {
        height: '3.625rem',
        width: '3.625rem',
        position: 'relative'
      },
      dividerStyle: {
        margin: '0.625rem 0',
        height: '0.0625rem',
        border: 'none',
        backgroundColor: '#d6d6e1'
      }
    };

    this.handle = '@' + this.props.params.handle;

    this.state = {
      userCustomFields: [],
      activityStreams: [],
      recommendedUsers: [],
      activeTab: 'all',
      user: null
    };
  }

  async componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);

    const user = await getUserFromHandle(this.handle);
    const userID = user.profile.id;

    this.setState({
      user
    });

    this.fetchUserActivity(3, 0, this.state.activeTab, false, userID);

    getUserCustomFields({ 'user_ids[]': userID, send_array: true })
      .then(response => {
        const userCustomInfo = response[0].customFields;
        getAllCustomFields()
          .then(allFieldsResult => {
            const userCustomFields = filter(userCustomInfo, item => {
              let indexInAllFields = findIndex(allFieldsResult.customFields, field => {
                return field.abbreviation === item.abbreviation;
              });
              if (
                ~indexInAllFields &&
                allFieldsResult.customFields[indexInAllFields].showInProfile
              ) {
                return item;
              }
            });
            this.setState({ userCustomFields });
          })
          .catch(error =>
            console.error(
              'Error @ UserActivityStandalone.componentDidMount.getAllCustomFields.fnc => ',
              error
            )
          );
      })
      .catch(error =>
        console.error(
          'Error @ UserActivityStandalone.componentDidMount.getUserCustomFields.fnc => ',
          error
        )
      );

    getRecommendedUsers({ limit: 5 })
      .then(response => {
        const recommendedUsers = response;
        this.setState({ recommendedUsers });
      })
      .catch(error =>
        console.error(
          'Error @  UserActivityStandalone.componentDidMount.getRecommendedUsers.fnc => ',
          error
        )
      );
  }

  tabChangeHandler = (activeTab, e) => {
    e.preventDefault();
    this.setState({ activeTab });
    const userID = this.state.user.profile.id;
    this.fetchUserActivity(3, 0, activeTab, false, userID);
  };

  fetchUserActivity = (limit, offset, activeTab, loadMore, userID) => {
    const userPostActivity = ['created_card', 'created_pathway', 'created_livestream'];
    const otherUserActivity = [
      'upvote',
      'smartbite_uncompleted',
      'comment',
      'smartbite_completed',
      'created_journey',
      'changed_author',
      'assignment_completed'
    ];

    const payload = {
      user_id: userID,
      linkable_card_required: true,
      limit,
      offset,
      'activity_actions[]':
        activeTab === 'all' ? [...userPostActivity, ...otherUserActivity] : userPostActivity
    };

    getUserActivity(payload)
      .then(response => {
        const activityStreams = loadMore
          ? [...this.state.activityStreams, ...response.activityStreams]
          : response.activityStreams;
        this.setState({ activityStreams });
      })
      .catch(error =>
        console.error(
          'Error @ UserActivityStandalone.componentDidMount.getUserActivity.fnc => ',
          error
        )
      );
  };

  handleScroll = throttle(
    event => {
      const userID = this.state.user && this.state.user.profile.id;

      if (
        userID &&
        window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight
      ) {
        this.fetchUserActivity(
          3,
          this.state.activityStreams.length,
          this.state.activeTab,
          true,
          userID
        );
      }
    },
    150,
    { leading: false }
  );

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  followClickHandler = user => {
    let userToFollow = user;
    this.props
      .dispatch(toggleFollow(user['id'], !user.isFollowing))
      .then(() => {
        let recommendedUser = this.state.recommendedUsers;
        let index = findIndex(recommendedUser, userToFollow);
        userToFollow.isFollowing = !user.isFollowing;
        recommendedUser[index] = userToFollow;
        this.setState({ userToFollow });
      })
      .catch(err => {
        console.error(`Error in UserSquareItem.toggleFollow.func : ${err}`);
      });
  };

  render() {
    const { userCustomFields, activityStreams, recommendedUsers, activeTab } = this.state;
    const { currentUser } = this.props;
    const userData = this.state.user && this.state.user.profile;

    return (
      <div id="user-activity-standalone">
        {!userData ? (
          <div className="text-center">
            <Spinner />
          </div>
        ) : (
          <div className="full-width-row row">
            <div className="user-name-title">
              <BreadcrumbV2 />
              <div className="user-name">{`${userData.firstName}'s ${tr('Activity')}`}</div>
            </div>
            <div className="user-panel">
              <Paper className="user-panel-container">
                <div className="user-avatar">
                  <BlurImage style={this.styles.avatarBox} image={userData.avatarimages.medium} />
                </div>
                <div className="user-name-designation text-center">
                  <div>{userData.firstName}</div>
                  <div>{userData.jobTitle}</div>
                </div>
                {!!userCustomFields.length && <hr style={this.styles.dividerStyle} />}
                <div className="user-custom-fiels">
                  {userCustomFields.map((field, index) => {
                    return (
                      <div key={index} className="custom-field">
                        {field.displayName}: <b>{field.value}</b>
                      </div>
                    );
                  })}
                </div>
                {(currentUser && currentUser.id) != userData.id && (
                  <div className="follow-button-container">
                    <FollowButtonv3
                      following={userData.isFollowing}
                      label={tr(userData.isFollowing ? 'Following' : 'Follow')}
                      hoverLabel={tr(userData.isFollowing ? 'Unfollow' : '')}
                      className="follow"
                      onTouchTap={this.followClickHandler.bind(this, userData)}
                    />
                  </div>
                )}
              </Paper>
            </div>
            <div className="card-panel">
              <div className="activity-tab-container">
                <div className={activeTab === 'all' ? 'activity-tabs active' : 'activity-tabs'}>
                  <a href="#" onClick={this.tabChangeHandler.bind(this, 'all')}>
                    {tr('All Activity')}
                  </a>
                </div>
                <div className={activeTab === 'post' ? 'activity-tabs active' : 'activity-tabs'}>
                  <a href="#" onClick={this.tabChangeHandler.bind(this, 'post')}>
                    {tr('Posts')}
                  </a>
                </div>
              </div>
              <div className="card-list-container">
                {activityStreams.map(activity => {
                  const card = activity.linkable.cards;
                  return (
                    <div className="card-user-activity-container" key={activity.id}>
                      <div className="user-activity-container">
                        <ActivityMessage
                          currentUserHandle={currentUser.handle}
                          userActivity={activity}
                          currentUserID={currentUser.id}
                          dispatch={this.props.dispatch}
                        />
                      </div>
                      {card && (
                        <Card
                          key={card.id}
                          card={card}
                          author={card.author}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          removeCardFromList={this.removeCardFromList}
                          tooltipPosition="top-center"
                          user={this.props.currentUser}
                          moreCards={true}
                          type={'BigCard'}
                        />
                      )}
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="recommended-user-panel">
              <div className="recommended-user-title">
                <div className="title">{tr('Recommended People')}</div>
              </div>
              <Paper className="recommended-user-container">
                {recommendedUsers.map(recommendedUser => {
                  return (
                    <div className="user-info-container" key={recommendedUser.id}>
                      <div className="user-image-container">
                        <BlurImage
                          style={this.styles.userAvatar}
                          image={recommendedUser.avatarimages.small}
                          imageClickHandler={() => {
                            this.props.dispatch(push(`/${recommendedUser.handle}`));
                          }}
                        />
                      </div>
                      <div className="user-name-follow-btn">
                        <div
                          className="user-name"
                          onClick={() => {
                            this.props.dispatch(push(`/${recommendedUser.handle}`));
                          }}
                        >
                          {recommendedUser.name}
                        </div>
                        <FollowButtonv3
                          following={recommendedUser.isFollowing}
                          label={tr(recommendedUser.isFollowing ? 'Following' : 'Follow')}
                          hoverLabel={tr(recommendedUser.isFollowing ? 'Unfollow' : '')}
                          className="follow"
                          onTouchTap={this.followClickHandler.bind(this, recommendedUser)}
                        />
                      </div>
                    </div>
                  );
                })}
              </Paper>
            </div>
          </div>
        )}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    userProfile: state.currentUser.get('profile'),
    currentUser: state.currentUser.toJS()
  };
}

UserActivityStandalone.propTypes = {
  user: PropTypes.object,
  userProfile: PropTypes.object,
  currentUser: PropTypes.object,
  params: PropTypes.object
};

export default connect(mapStoreStateToProps)(UserActivityStandalone);

function ActivityMessage(props) {
  const { userActivity, dispatch, currentUserID, currentUserHandle } = props;

  let { user, action, snippet, linkable, streamableId } = userActivity;
  let linkUrl, linkPrefix, slug;
  let message = `<span tabIndex={-1} class="user-name">**${
    user.id == currentUserID ? 'You' : user.name
  }**</span>`;

  if (snippet.length > 100) {
    snippet = snippet.slice(0, 100) + '...';
  }

  switch (action) {
    case 'created_livestream':
      message += ` ${tr('created')} ${snippet}`;
      break;
    case 'upvote':
      message += ` ${tr('liked')} ${snippet}`;
      break;
    case 'comment':
      message += ` ${tr('commented on')} ${snippet}`;
      break;
    case 'created_card':
      message += ` ${tr('created')} <strong>${tr('SmartCard')}</strong>: ${snippet
        .replace(/[\[\]\*_\\]*/g, '')
        .replace(/\(.*/g, '')}`;
      break;
    case 'created_pathway':
      message += ` ${tr('created')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
      break;
    case 'created_journey':
      message += ` ${tr('created')} <strong>${tr('Journey')}</strong>: ${snippet}`;
      break;
    case 'assignment_completed':
      message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${snippet}`;
      break;
    case 'smartbite_completed':
      switch (linkable.type) {
        case 'journey':
          message += ` ${tr('completed')} <strong>${tr('Journey')}</strong>: ${snippet}`;
          break;
        case 'collection':
          message += ` ${tr('completed')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
          break;
        default:
          message += ` ${tr('completed')} <strong>${tr('SmartCard')}</strong>: ${snippet}`;
          break;
      }
      break;
    case 'smartbite_uncompleted':
      switch (linkable.type) {
        case 'journey':
          message += ` ${tr('uncompleted')} <strong>${tr('Journey')}</strong>: ${snippet}`;
          break;
        case 'collection':
          message += ` ${tr('uncompleted')} <strong>${tr('Pathway')}</strong>: ${snippet}`;
          break;
        default:
          message += ` ${tr('uncompleted')} <strong>${tr('SmartCard')}</strong>: ${snippet}`;
          break;
      }
      break;
    default:
      message += '';
      break;
  }

  switch (linkable.type) {
    case 'journey':
      linkUrl = `journey/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'journey';
      break;
    case 'collection':
      linkUrl = `pathways/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'pathways';
      break;
    case 'card':
      linkUrl = `insights/${linkable.id}`;
      slug = linkable.id;
      linkPrefix = 'insights';
      break;
    case 'video_stream':
      linkUrl = `video_streams/${streamableId}`;
      slug = streamableId;
      linkPrefix = 'insights';
      break;
    default:
      linkUrl = '#';
      break;
  }

  return (
    <div
      style={{ cursor: 'pointer' }}
      tabIndex={0}
      onClick={() => {
        activityClickHandler(linkUrl, linkPrefix, slug, dispatch, currentUserHandle);
      }}
    >
      <MarkdownRenderer
        markdown={message}
        onUserNameClick={() => {
          activityClickHandler(user.handle, null, null, dispatch, currentUserHandle);
        }}
      />
    </div>
  );
}

ActivityMessage.propTypes = {
  userActivity: PropTypes.object,
  dispatch: PropTypes.func,
  currentUserID: PropTypes.string,
  currentUserHandle: PropTypes.string
};

function activityClickHandler(link, cardType, slug, dispatch, currentUserHandle) {
  const modalStandalone = window.ldclient.variation('modal-standalone-page', false);
  if (modalStandalone && cardType) {
    window.history.pushState(null, null, `/${link}`);
    dispatch(openStandaloneOverviewModal({ slug }, cardType));
  } else {
    dispatch(push(`/${link.replace('@', '') === currentUserHandle ? 'me' : link}`));
  }
}
