import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import { Tabs, Tab } from 'material-ui/Tabs';
import Spinner from '../../common/spinner';

import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import { removeSkill, getRecommendedUsers } from 'edc-web-sdk/requests/users';
import { cards as cardsSDK } from 'edc-web-sdk/requests/index';
import transcript from 'edc-web-sdk/requests/transcript';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import { getIntegrationList, getUserSkills, getUserByHandle } from '../../../actions/usersActions';
import { openIntegrationModal, openSkillCreationModal } from '../../../actions/modalActions';
import { getCoursesV2 } from '../../../actions/coursesActions';
import { getIntegration, integrationConnectStatus } from '../../../actions/profileActions';
import ConfirmationModal from '../../modals/ConfirmationCommentModal';
import Bia from '../../common/Bia';
import * as actionTypes from '../../../constants/actionTypes';
import Influencer from '../../discovery/Influencer.jsx';
import fileDownload from 'js-file-download';
import DashboardProfileContainer from '../DashboardProfileContainer';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';

class MyProfile extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      popover: {
        width: ''
      },
      editIcon: {
        width: 14,
        height: 14,
        color: '#4990e2',
        verticalAlign: 'middle'
      },
      download: {
        marginTop: '10px',
        textAlign: 'right'
      },
      summaryEditIcon: {
        width: 14,
        height: 14,
        color: window.ldclient.variation('additional-profile-info', false) ? '#6f708b' : '#4990e2',
        verticalAlign: 'middle'
      }
    };

    this.state = {
      activeTab: {},
      open: false,
      pathwayBadges: window.ldclient.variation('pathway-badge', false),
      enableWallet: this.props.team.get('config').wallet || false,
      clc: window.ldclient.variation('clc', false),
      skillsMap: window.ldclient.variation('skillsmap', false),
      courseState: 'Content Loading...',
      courses: [],
      cardState: ['published'],
      openConfirm: false,
      removeId: '',
      userSumaryExpand: false,
      userBadgings:
        this.props.team &&
        this.props.team.get('OrgConfig') &&
        this.props.team.get('OrgConfig').badgings['web/badgings'],
      showRecommendedContent: window.ldclient.variation('winterstorm', false),
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      upgradeSkillsPassport:
        this.props.team &&
        this.props.team.get('OrgConfig') &&
        !this.props.team.get('OrgConfig').profileShowUserContent,
      isTranscriptEnabled: window.ldclient.variation('user-transcript', false),
      additionalProfileInfoEnabled: window.ldclient.variation('additional-profile-info', false),
      enableBIA: this.props.team.get('config').enabled_bia,
      publicProfileViewV4: window.ldclient.variation('public-profile-view-v4', false),
      showProfileRecommendSme: window.ldclient.variation('profile-recommend-sme', false)
    };

    this.tabs = [
      { path: '/me', label: 'Profile', pathsList: ['/me'] },
      { path: '/me/groups', label: 'Groups' },
      {
        path: '/me/content',
        label: 'Content',
        options: [
          { path: '/me/dashboard', label: 'Performance' },
          { path: '/me/content', label: 'SmartCards' },
          { path: '/me/channels', label: 'Channels' }
        ],
        pathsList: [
          '/me/dashboard',
          '/me/content',
          '/me/channels',
          '/me/content/Pathways',
          '/me/content/VideoStreams',
          '/me/content/SmartBites',
          '/me/content/all'
        ]
      },
      {
        path: '/me/learning',
        label: 'My Learning Plan',
        pathsList: [
          '/me/learning',
          '/me/learning/all',
          '/me/learning/active',
          '/me/learning/completed',
          '/me/learning/bookmarks'
        ]
      },
      {
        path: '/me/team',
        label: 'Team',
        pathsList: ['/me/team/following', '/me/team/followers'],
        options: [{ path: '/me/leaderboard', label: 'Leaderboard' }]
      },
      { path: '/me/channels', label: 'Channels' }
    ];

    this.modalSkillOpen = this.modalSkillOpen.bind(this);
    this.importClickHandler = this.importClickHandler.bind(this);
    this.removeSkill = this.removeSkill.bind(this);
    this.userHandle = '';
    this.enableExternalIntegrations = window.ldclient.variation(
      'enable-external-integrations',
      false
    );
  }

  getUserDetails(props) {
    let _this = this;
    this.userHandle =
      (props.params && props.params.handle) ||
      (props.currentUser && props.currentUser.handle && this.props.currentUser.handle);
    if (this.userHandle) {
      this.props
        .dispatch(getUserByHandle('@' + this.userHandle))
        .then(({ id, coverImage, user }) => {
          this.props.dispatch({
            type: actionTypes.LOADING_PUBLIC_PROFILE_CARDS,
            loading: true
          });
          cardsSDK
            .getUserCards(
              id,
              10,
              undefined,
              undefined,
              undefined,
              this.state.cardState,
              undefined,
              true
            )
            .then(data => {
              let cards = data[0];
              let totalCardsNumber = data[1];
              _this.props.dispatch({
                type: actionTypes.LOADING_PUBLIC_PROFILE_CARDS,
                loading: false
              });
            })
            .catch(err => {
              console.error(`Error in MyProfile.getUserCards.func : ${err}`);
            });
        })
        .catch(err => {
          console.error(`Error in MyProfile.getUserByHandle.func : ${err}`);
        });
    }
  }

  componentDidMount() {
    if (this.props.currentUser && this.props.currentUser.isLoggedIn) {
      this.getUserDetails(this.props);
    }

    if (this.state.showProfileRecommendSme && this.state.showRecommendedContent) {
      getRecommendedUsers({ limit: 10 })
        .then(users => {
          this.setState({ recommendedItems: users });
        })
        .catch(err => {
          console.error(`Error in MyProfile.getRecommendedUsers.func : ${err}`);
        });
    }
    if (this.props.pathname.indexOf('/@') > -1) {
      return;
    }

    this.props.dispatch(getIntegration(this.props.currentUser.id));
    this.props.dispatch(getUserSkills(this.props.currentUser.id));

    this.props
      .dispatch(getCoursesV2(this.props.currentUser.id))
      .then(response => {
        this.setState({
          courseState:
            response && response.externalCourses && response.externalCourses.length > 0
              ? ''
              : 'No Courses undertaken'
        });
      })
      .catch(err => {
        console.error(`Error in MyProfile.getCoursesV2.func : ${err}`);
      });
  }

  async componentWillReceiveProps(nextProps) {
    let loadUserDetails =
      (nextProps.params && nextProps.params.handle) !==
        (this.props.params && this.props.params.handle) || false;

    if (
      (this.props.currentUser.publicProfile && !nextProps.currentUser.publicProfile) ||
      loadUserDetails
    ) {
      if (typeof nextProps.params.handle === 'undefined' || loadUserDetails) {
        this.getUserDetails(nextProps);
      }
      let userInfoCallBack = await getSpecificUserInfo(['dashboardInfo'], this.props.currentUser);
      this.props.dispatch(userInfoCallBack);
      this.props.dispatch(getIntegration(this.props.currentUser.id));
      this.props.dispatch(getUserSkills(this.props.currentUser.id));

      this.props
        .dispatch(getCoursesV2(this.props.currentUser.id))
        .then(response => {
          this.setState({
            courseState:
              response && response.externalCourses && response.externalCourses.length > 0
                ? ''
                : 'No Courses undertaken'
          });
        })
        .catch(err => {
          console.error(`Error in MyProfile.componentWillReceiveProps.getCoursesV2.func : ${err}`);
        });
    }
  }

  showMyScore(val) {
    let show = val;
    if (show === undefined) {
      return true;
    } else {
      return show.visible;
    }
  }

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  renderTab = (tab, index, label) => {
    let tabHoverColor = this.state.hoveredTab === tab.label ? { color: colors.primary200 } : {};
    return (
      <Tab
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        label={tr(label)}
        style={tabHoverColor}
        value={tab.path}
        className={this.toCamelCase('me ' + tab.label)}
        key={index}
        onClick={this.handleTabChange.bind(this, tab.path)}
        onMouseOver={this.handleTabHover.bind(this, tab)}
        onMouseLeave={this.handleTabLeave.bind(this, tab)}
      />
    );
  };

  handleTabChange = path => {
    this.setState({
      open: false
    });
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(
      function(tab) {
        if (currentPath === tab.path || (tab.pathsList && tab.pathsList.includes(currentPath))) {
          value = tab.path;
        }
      }.bind(this)
    );
    return value;
  };

  handleTabLeave = tab => {
    this.setState({ hoveredTab: false });
  };

  handleTabHover = tab => {
    if (tab.options) {
      this.setState({
        activeTab: tab,
        open: true,
        hoveredTab: tab.label
      });
    } else {
      this.setState({ hoveredTab: tab.label });
    }
  };

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.filter(tab => tab.visible);
  }

  toSettingsPage = e => {
    e.preventDefault();
    this.props.dispatch(push('/settings'));
  };

  handleConnectClick = integration => {
    this.props.dispatch(openIntegrationModal(integration, this.importClickHandler));
  };

  handleConnectKeyPress = (e, integration) => {
    if (e.keyCode === 13) {
      this.handleConnectClick(integration);
    }
  };

  importClickHandler(integration) {
    this.props.dispatch(integrationConnectStatus(integration));
  }

  modalSkillOpenToAddSkills = (e, skill) => {
    e.preventDefault();
    this.modalSkillOpen(skill);
  };

  modalSkillOpen(skill = null) {
    this.props.dispatch(openSkillCreationModal(skill));
  }

  toggleModal = id => {
    this.setState({ openConfirm: !this.state.openConfirm, removeId: id });
  };

  removeSkill() {
    this.setState({ openConfirm: false });
    let payload = {
      skills_user_id: this.state.removeId,
      id: this.props.currentUser.id
    };
    removeSkill(this.props.currentUser.id, payload)
      .then(data => {
        this.props.dispatch(getUserSkills(this.props.currentUser.id));
      })
      .catch(err => {
        console.error(`Error in MyProfile.getUserSkills.func : ${err}`);
      });
  }

  expandBioSection = () => {
    this.setState({
      userSumaryExpand: !this.state.userSumaryExpand
    });
  };

  removeCardFromList(cardId) {
    this.setState(prevState => {
      let cards = prevState.cards.filter(item => item.id != cardId);
      return { cards };
    });
  }

  openTeam = e => {
    e.preventDefault();
    this.props.dispatch(push(this.state.isNewProfileNavigation ? '/team' : '/me/team'));
  };

  getTranscript = e => {
    e.preventDefault();
    transcript()
      .then(response => {
        fileDownload(response.xhr.response, 'Transcript.pdf');
      })
      .catch(err => {
        console.error(`Error in MyProfile.getTranscript.func : ${err}`);
      });
  };

  openUrl = (skill, skillCred, flag) => {
    if (flag && skill.credentialUrl) {
      window.open(skill.credentialUrl, '_blank');
    } else {
      if (skillCred) {
        refreshFilestackUrl(skillCred.url)
          .then(resp => {
            window.open(resp.signed_url, '_blank');
          })
          .catch(err => {
            console.error(`Error in MyProfile.openUrl.refreshFilestackUrl.func : ${err}`);
          });
      }
    }
  };

  render() {
    let showContinuousLearning = this.props.team.get('OrgConfig').sections[
      'web/sections/continuousLearning'
    ].visible;
    let showMyScoreVal = this.props.team.get('OrgConfig').sections['web/sections/myScore'];
    let showMyScore = this.showMyScore(showMyScoreVal);
    let tabValue = this.selectTabValue(this.props.pathname);
    let output = [];
    let user =
      (this.props.currentUser &&
        this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.profile) ||
      this.props.currentUser;

    let ProfileController = this.listTab(this.props.team.get('OrgConfig').profile);
    let isChannelsOn = Boolean(
      ProfileController.find(item => item.key === 'web/profile/channels' && item.visible)
    );

    ProfileController.sort((a, b) => a.index - b.index);

    ProfileController.filter(item => item.visible).map(item => {
      let label = item.label || item.defaultLabel;
      switch (item.key) {
        case 'web/profile/profile':
          output.push(this.renderTab(this.tabs[0], item.index, label));
          break;
        case 'web/profile/groups':
          output.push(this.renderTab(this.tabs[1], item.index, label));
          break;
        case 'web/profile/content':
          let contentTabs = this.tabs[2];
          if (isChannelsOn && contentTabs.options) {
            contentTabs.options = contentTabs.options.filter(
              itemValue => itemValue.label !== 'Channels'
            );
          }

          contentTabs.options = null;
          output.push(this.renderTab(contentTabs, item.index, label));
          break;
        case 'web/profile/learningQueue':
          output.push(this.renderTab(this.tabs[3], item.index, label));
          break;
        case 'web/profile/teams':
          output.push(this.renderTab(this.tabs[4], item.index, label));
          break;
        case 'web/profile/channels':
          output.push(this.renderTab(this.tabs[5], item.index, label));
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });

    let publicProfile = this.props.currentUser.publicProfile;

    let { profileCourses } = this.props;
    let { integrationList } = this.props;
    let courseState = profileCourses && profileCourses.length > 0 ? '' : 'No Courses undertaken';
    user.userSkills = !publicProfile
      ? this.props.currentUser && this.props.currentUser.userSkills
      : {
          skills:
            (this.props.currentUser &&
              this.props.currentUser.publicProfile &&
              this.props.currentUser.publicProfile.skills) ||
            []
        };

    let _this = this;

    let showSkillsPassport =
      (user && user.bio) ||
      (profileCourses && profileCourses.length) ||
      (user && user.userSkills && user.userSkills.skills && user.userSkills.skills.length) ||
      (this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userCardBadges &&
        this.props.currentUser.publicProfile.userCardBadges.length) ||
      (this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userClcBadges &&
        this.props.currentUser.publicProfile.userClcBadges.length);

    let SPLabel = tr('Skills Passport');

    return (
      <div id="profile-wrapper">
        <div id="profile" className="horizontal-spacing-xlarge">
          <div className="profile-container small-12 profile-container-full">
            <DashboardProfileContainer
              profileCourses={profileCourses}
              integrationList={integrationList}
              showAccountLinking={!publicProfile && this.enableExternalIntegrations}
              upgradeSkillsPassport={this.state.upgradeSkillsPassport}
              user={user}
              publicProfile={publicProfile}
              toggleModal={this.toggleModal}
              modalSkillOpen={this.modalSkillOpen}
              enableBIA={this.state.enableBIA}
              modalSkillOpenToAddSkills={this.modalSkillOpenToAddSkills}
              componentName="Profile"
            />

            {this.state.openConfirm && (
              <ConfirmationModal
                title={'Confirm'}
                message={'Do you want to delete the skill?'}
                closeModal={this.toggleModal}
                callback={this.removeSkill}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: state.team,
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    profileCourses: state.profile.get('externalCourses'),
    integrationList: state.profile.get('integrationList')
  };
}

MyProfile.propTypes = {
  team: PropTypes.object,
  params: PropTypes.object,
  pathname: PropTypes.string,
  currentUser: PropTypes.object,
  profileCourses: PropTypes.array,
  integrationList: PropTypes.array
};

export default connect(mapStoreStateToProps)(MyProfile);
