import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { getOnBoardingSkillsLevels } from '../../../actions/onboardingActions';
import {
  openUpdateExpertiseModal,
  openUpdateMultilevelExpertiseModal
} from '../../../actions/modalActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import { langs } from '../../../constants/languages';

class Expertise extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px',
        border: '1px solid',
        borderColor: colors.primary
      },
      smallIconButton: {
        width: '36px',
        height: '20px',
        padding: '0px',
        verticalAlign: 'middle'
      },
      smallIcon: {
        width: '14px',
        height: '14px'
      }
    };

    this.state = {
      expertise: [],
      loadingComplete: false,
      expertiseLabel: ''
    };

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    let { team } = props;
    let config = team.get('config');
    this.isShowCustomLabels = config && config.custom_labels;

    this.triggerUpdateExpertise = this.triggerUpdateExpertise.bind(this);
    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
  }
  componentDidMount() {
    let { team } = this.props;
    let OrgConfig = team.get('OrgConfig');
    let labels = team && OrgConfig && OrgConfig.labels;
    let expertiseLabel = tr('Skills');
    if (labels) {
      let expertiseLabelFlag =
        labels['web/labels/expertise'] && labels['web/labels/expertise'].label.length > 0;
      expertiseLabel = expertiseLabelFlag ? tr(labels['web/labels/expertise'].label) : tr('Skills');
    }
    let curExpertiseLabel = labels['web/labels/expertise'];
    this.translatedLabel =
      this.isShowCustomLabels &&
      curExpertiseLabel &&
      curExpertiseLabel.languages &&
      curExpertiseLabel.languages[this.profileLanguage] &&
      curExpertiseLabel.languages[this.profileLanguage].trim();

    let stateObj = { expertiseLabel };

    if (this.props.currentUser.id) {
      stateObj.expertise =
        (this.props.currentUser &&
          this.props.currentUser.profile &&
          this.props.currentUser.profile.expertTopics) ||
        [];
      stateObj.loadingComplete = true;
    }
    this.setState(stateObj);
  }

  triggerUpdateExpertise = () => {
    if (this.onboardingVersion === 'v3') {
      this.loadMultilevel();
    } else {
      this.props.dispatch(openUpdateExpertiseModal(this.props.currentUser.id));
    }
  };

  loadMultilevel = () => {
    let { team, onboardingv3 } = this.props;
    let config = team.get('config');
    let taxonomyDomain = team && config && config.taxonomy_domain;

    if (onboardingv3 && onboardingv3.get('multiLevelTaxonomy')) {
      this.props.dispatch(openUpdateMultilevelExpertiseModal());
    } else {
      this.props
        .dispatch(getOnBoardingSkillsLevels(taxonomyDomain))
        .then(() => {
          this.props.dispatch(openUpdateMultilevelExpertiseModal());
        })
        .catch(err => {
          console.error(`Error in profilev2 Expertise.getOnBoardingSkillsLevels.func : ${err}`);
        });
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      expertise:
        (nextProps.currentUser &&
          nextProps.currentUser.profile &&
          nextProps.currentUser.profile.expertTopics) ||
        [],
      loadingComplete: true
    });
  }

  render() {
    let expertise = this.state.expertise != undefined ? this.state.expertise : [];
    let showViewAll = false;
    let showingExpertise = expertise;
    if (expertise.length > 3) {
      showingExpertise = expertise.slice(0, 3);
      showViewAll = true;
    }
    return (
      <div className="expertise-section">
        <div className="profile-title column small-6" role="heading" aria-level="3">
          {this.translatedLabel || this.state.expertiseLabel} (
          {this.state.expertise && this.state.expertise.length})
          <IconButton
            className="edit"
            aria-label="update expertise"
            onTouchTap={this.triggerUpdateExpertise}
            iconStyle={this.styles.smallIcon}
            style={this.styles.smallIconButton}
          >
            <EditIcon color="#6f708b" />
          </IconButton>
        </div>

        <div className="column small-6">
          <div className="float-right">
            {showViewAll && (
              <div className="view-more-expertise">
                <a
                  className="view-more-expertise underline"
                  onTouchTap={this.triggerUpdateExpertise}
                >
                  {tr('View All')}
                </a>
              </div>
            )}
          </div>
        </div>

        {!this.state.loadingComplete && (
          <p className="data-not-available-msg">
            <small>{tr('Loading your list of skills ...')}</small>
          </p>
        )}

        <div>
          <div style={this.styles.chipsWrapper}>
            {showingExpertise.map((exp, idx) => {
              return (
                <div title={exp.topic_label} className="chip-container" key={idx}>
                  {exp.topic_label.substr(0, 30)}
                  {exp.topic_label.length > 30 ? '...' : ''}
                </div>
              );
            })}
          </div>
        </div>

        {this.state.loadingComplete && expertise.length == 0 && (
          <div className="text-center">
            <p className="text-left">
              <small className="data-not-available-msg">
                {tr(
                  "You haven't added any skills yet. Including skills in your profile will help you gain more followers and visibility."
                )}
              </small>
              <br />
              <br />
            </p>
          </div>
        )}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal,
    team: state.team,
    onboardingv3: state.onboardingv3
  };
}

Expertise.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  team: PropTypes.object,
  onboardingv3: PropTypes.object
};

export default connect(mapStoreStateToProps)(Expertise);
