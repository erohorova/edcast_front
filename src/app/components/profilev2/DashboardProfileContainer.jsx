import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Goals from './MyProfile/Goals';
import CourseCarousel from './Dashboard/CourseCarousel';
import Paper from 'edc-web-sdk/components/Paper';
import getFleIcon from '../../utils/getFileIcon';
import LearningAnalytics from './LearningAnalytics/index';
import { connect } from 'react-redux';
import UserActivity from './UserActivity/List/UserActivity';
import Skills from './Skills';
import AccountLinking from './AccountLinking';
import Expertise from './MyProfile/Expertise';

class DashboardProfileContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      popover: {
        width: ''
      },
      editIconNew: {
        width: 16,
        height: 16,
        color: '#6f708b',
        verticalAlign: 'middle'
      }
    };
    this.additionalProfileInfoEnabled = window.ldclient.variation('additional-profile-info', false);
    this.enableExternalIntegrations = window.ldclient.variation(
      'enable-external-integrations',
      false
    );
    this.isHideUploadCredential = window.ldclient.variation('hide-upload-credential', false);
    const { orgConfig } = props;
    this.enableSkillPassport = orgConfig.hasOwnProperty('enable_skill_passport')
      ? orgConfig['enable_skill_passport']
      : true;
  }

  render() {
    let {
      componentName,
      profileCourses,
      showAccountLinking,
      integrationList,
      upgradeSkillsPassport,
      handleConnectClick,
      handleConnectKeyPress,
      user,
      publicProfile,
      toggleModal,
      modalSkillOpen,
      enableBIA,
      modalSkillOpenToAddSkills
    } = this.props;
    let isDashboard = componentName == 'Dashboard';
    let iconFileSrc = getFleIcon('pdf');

    return (
      <div className="profile-dashboard full-width-row">
        {/* Goals */}
        {isDashboard && (
          <div>
            <Goals />
          </div>
        )}
        {/* Goals */}

        {/* Expertise */}
        {isDashboard && (
          <div>
            <Expertise />
          </div>
        )}
        {/* Expertise */}

        {/* Skill names */}
        <UserActivity
          userID={user.id}
          handle={user.handle}
          isDashboard={isDashboard}
          componentName={componentName}
          publicProfile={publicProfile}
        />

        {/* Learning Analytics */}
        <LearningAnalytics isDashboard={isDashboard} componentName={componentName} />
        {/* Learning Analytics */}

        {this.enableSkillPassport && user.userSkills && (
          <Skills
            userSkills={user.userSkills.skills}
            toggleModal={toggleModal}
            modalSkillOpen={modalSkillOpen}
            publicProfile={publicProfile}
          />
        )}

        {this.enableSkillPassport && (
          <div className="skills-section">
            {!publicProfile && (
              <a
                href="#"
                onClick={e => modalSkillOpenToAddSkills(e, null)}
                className="small-12 profile-box"
              >
                <Paper className="add-more-skill__flat">
                  <span className="add-more-skill__label">
                    <span className="add-more-skill__plus">+ </span>
                    <span className="add-more-skill__text">{tr('Add Skill')}</span>
                  </span>
                </Paper>
              </a>
            )}
          </div>
        )}

        {/* Account linking */}
        {showAccountLinking && <AccountLinking />}
        {/* Account linking */}

        {/* Course Carousel */}
        {this.enableExternalIntegrations &&
          !upgradeSkillsPassport &&
          profileCourses &&
          profileCourses.length > 0 && (
            <CourseCarousel courses={profileCourses} numberOfCourses={6} />
          )}
        {/* Course Carousel */}
      </div>
    );
  }
}

DashboardProfileContainer.propTypes = {
  profileCourses: PropTypes.any,
  integrationList: PropTypes.any,
  componentName: PropTypes.string,
  upgradeSkillsPassport: PropTypes.bool,
  showAccountLinking: PropTypes.bool,
  handleConnectClick: PropTypes.func,
  handleConnectKeyPress: PropTypes.func,
  user: PropTypes.object,
  publicProfile: PropTypes.object,
  toggleModal: PropTypes.function,
  modalSkillOpen: PropTypes.function,
  enableBIA: PropTypes.any,
  modalSkillOpenToAddSkills: PropTypes.func,
  orgConfig: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    orgConfig: state.team.get('config')
  };
}

export default connect(mapStoreStateToProps)(DashboardProfileContainer);
