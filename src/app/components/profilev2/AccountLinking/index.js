import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import getFleIcon from '../../../utils/getFileIcon';
import { openIntegrationModal } from '../../../actions/modalActions';

class AccountLinking extends Component {
  constructor(props) {
    super(props);
    const { integrationList } = this.props;
    this.state = {
      viewMore: false,
      integrationList
    };
  }

  clickViewMoreHandler = () => {
    this.setState(prevState => {
      const viewMore = !prevState.viewMore;
      return { viewMore };
    });
  };

  handleConnectClick = integration => {
    this.props.dispatch(openIntegrationModal(integration, this.importClickHandler));
  };

  importClickHandler = integration => {
    let { integrationList } = this.props;
    let index = integrationList.findIndex(i => i.id === integration.id);
    integrationList[index].connected = !integrationList[index].connected;

    this.setState({
      integrationList
    });
  };

  handleConnectKeyPress = (e, integration) => {
    if (e.keyCode === 13) {
      this.handleConnectClick(integration);
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      integrationList: nextProps.integrationList
    });
  }

  render() {
    const { integrationList } = this.props;
    const { viewMore } = this.state;

    return (
      <div>
        <div className="account-linking-container">
          <div className="account-linking">
            {(integrationList || []).length && (
              <div className="account-linking-title" role="heading" aria-level="4">
                {tr('Account Linking')}
              </div>
            )}
            <div className="integration-section">
              {(!viewMore ? integrationList.slice(0, 6) : integrationList).map(
                (integration, index) => {
                  return (
                    <div className="integration-container" key={index}>
                      <Paper
                        key={integration.id}
                        className="integration-logo integration-logo__new-skill-passport"
                      >
                        <div className="integration-logo__image-container">
                          <img
                            abIndex={0}
                            alt={integration.name}
                            className={`integration-logo__image ${
                              !integration.connected ? 'integration-logo__image-fade' : ''
                            }`}
                            src={integration.logoUrl}
                          />
                        </div>
                        <span
                          tabIndex={0}
                          onTouchTap={() => {
                            this.handleConnectClick(integration);
                          }}
                          onKeyDown={e => {
                            this.handleConnectKeyPress(e, integration);
                          }}
                        >
                          <span
                            tabIndex="-1"
                            className={`integration-logo__action  hideOutline ${
                              integration.connected ? 'integration-logo__connected ' : ''
                            }`}
                          >
                            {integration.connected ? tr('Connected') : tr('Connect')}
                          </span>
                        </span>
                      </Paper>
                    </div>
                  );
                }
              )}
            </div>
            {(integrationList || []).length > 3 && (
              <div className="view-more-btn">
                <span
                  className="cursor-pointer"
                  onClick={() => {
                    this.clickViewMoreHandler();
                  }}
                >
                  {viewMore ? tr('View Less') : tr('View More')}
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    integrationList: state.profile.get('integrationList')
  };
}

AccountLinking.propTypes = {
  integrationList: PropTypes.any
};

export default connect(mapStoreStateToProps)(AccountLinking);
