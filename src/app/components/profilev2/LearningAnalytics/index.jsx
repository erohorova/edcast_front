import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import LearningHours from './LearningHours';
import MyBadges from './MyBadges';
import InProgress from './InProgress';
import ActiveTopics from './ActiveTopics';
import TopContent from './TopContent';
import Sortable from 'sortablejs';
import { connect } from 'react-redux';
import { reorderingLearningAnalytics } from '../../../actions/profileActions';

class LearningAnalytics extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    if (this.props.isDashboard) {
      const list = document.getElementById('learningAnalytics');
      Sortable.create(list, {
        handle: '.learning-analytic-box',
        animation: 150,
        forceFallback: true,
        group: {
          name: 'learningAnalytics',
          pull: 'clone',
          revertClone: true
        },
        onUpdate: this.changeOrder
      });
    }
  }

  changeOrder = e => {
    let { dashboardInfo } = this.props;
    dashboardInfo = this.getSortables(dashboardInfo);
    let removedElement = dashboardInfo.splice(e.oldIndex, 1);
    dashboardInfo.splice(e.newIndex, 0, removedElement[0]);
    const { userID } = this.props;
    this.props.dispatch(reorderingLearningAnalytics(dashboardInfo, userID));
  };

  getSortables = dashboardInfo => {
    const unSortableWidgets = ['Activity Stream', 'Active Topics'];
    let sortableInfo = dashboardInfo;
    let unSortableInfo = [];
    sortableInfo.forEach((info, index) => {
      if (!!~unSortableWidgets.indexOf(info.name)) {
        unSortableInfo.push(info);
        sortableInfo.splice(index, 1);
      }
    });
    return [...sortableInfo, ...unSortableInfo];
  };

  makeComponentArray = (dashboardInfo, isDashboard) => {
    let { OrgConfig } = this.props;
    let componentArray = [];
    let componentElement = {};
    let pathwayBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].pathwayBadgings;
    let clcBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].clcBadgings;

    dashboardInfo.forEach((info, index) => {
      if (info.visible || isDashboard) {
        switch (info.name) {
          case 'Learning Hours':
            componentElement = (
              <LearningHours key={index} {...this.props} sequeceNumber={componentArray.length} />
            );
            componentArray.push(componentElement);
            break;

          case 'My Badges':
            if (pathwayBadgings || clcBadgings) {
              componentElement = (
                <MyBadges key={index} {...this.props} sequeceNumber={componentArray.length} />
              );
              componentArray.push(componentElement);
            }
            break;

          case 'In Progress':
            componentElement = (
              <InProgress key={index} {...this.props} sequeceNumber={componentArray.length} />
            );
            componentArray.push(componentElement);
            break;

          // case "Active Topics":
          // 	componentElement = <ActiveTopics key={index} {...this.props} sequeceNumber={componentArray.length}/>;
          // 	componentArray.push(componentElement);
          // 	break;

          case 'Top Content':
            componentElement = <TopContent {...this.props} sequeceNumber={componentArray.length} />;
            componentArray.push(componentElement);
            break;

          default:
            break;
        }
      }
    });
    return componentArray;
  };

  render() {
    const { isDashboard, dashboardInfo } = this.props;
    const componentArray = this.makeComponentArray(dashboardInfo, isDashboard);
    if (!componentArray.length) {
      return null;
    }
    return (
      <div id="learning-analytics">
        <div className="profile-title">{tr('My Learning Analytics')}</div>
        {isDashboard ? (
          <div id="learningAnalytics" className="learning-analytic-box-container">
            {componentArray}
          </div>
        ) : (
          <div className="learning-analytic-box-container">{componentArray}</div>
        )}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    userID: state.currentUser.get('id'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    OrgConfig: state.team.get('OrgConfig')
  };
}

LearningAnalytics.propTypes = {
  dashboardInfo: PropTypes.any,
  userID: PropTypes.string,
  OrgConfig: PropTypes.object
};

export default connect(mapStoreStateToProps)(LearningAnalytics);
