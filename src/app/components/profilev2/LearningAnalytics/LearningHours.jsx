import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import { connect } from 'react-redux';
import { getUserTopicScore, publishToPublicProfile } from '../../../actions/profileActions';
import { getUserClc } from 'edc-web-sdk/requests/users';

class LearningHours extends Component {
  constructor(props, context) {
    super(props, context);
    this.myScoreTaxonomyTopics = window.ldclient.variation('my-score-taxonomy-topics', false);
    this.state = {
      ClcPercentage: 0,
      ClcTargetHours: 0
    };
  }

  componentDidMount = () => {
    let payload = this.props.pathname.indexOf('/@') > -1 ? { user_id: this.props.profile.id } : {};
    this.props.dispatch(getUserTopicScore(this.myScoreTaxonomyTopics, payload));
    getUserClc(payload)
      .then(data => {
        let ClcTargetMins = data.targetScore * 60;
        let clcProgressScore = (data.score / ClcTargetMins) * 100;
        this.setState({
          ClcPercentage: clcProgressScore,
          ClcScore: data.score,
          ClcTargetHours: data.targetScore
        });
      })
      .catch(err => {
        console.error(`Error in LeftRailClcProgress.getUserClc.func : ${err}`);
      });
  };

  render() {
    let { OrgConfig, learningHours, publicProfile, isDashboard, userID } = this.props;
    let enableClcBadgings =
      OrgConfig.badgings &&
      OrgConfig.badgings['web/badgings'] &&
      OrgConfig.badgings['web/badgings'].clcBadgings;

    let percentile = learningHours && Math.ceil(100 - learningHours.percentile);
    if (percentile == 0) {
      percentile = 1;
    }
    if (percentile == 100) {
      percentile = 99;
    }
    let myScoreConfig =
      OrgConfig && OrgConfig.sections && OrgConfig.sections['web/sections/myScore'];
    let myScoreLabel = myScoreConfig
      ? myScoreConfig.label || myScoreConfig.defaultLabel
      : 'My Score';
    let dashboardInfo = this.props.dashboardInfo;
    let enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'Learning Hours' && info.visible;
    });

    let text = parseInt(this.state.ClcScore / 60, 10) == 1 ? tr('hr') : tr('hrs');
    let clcHours =
      parseInt(this.state.ClcScore / 60, 10) > 0
        ? `${parseInt(this.state.ClcScore / 60, 10)} ${text} ${tr('and')} `
        : '';
    let clcMinUnit = this.state.ClcScore % 60;
    let clcMins = `${clcMinUnit ? clcMinUnit : 0} ${tr('mins')}`;

    return (
      <div className="learning-analytic-container">
        <div className="learning-analytic-box">
          {isDashboard && !publicProfile && (
            <div className="public-profile-overlay">
              <div className="public-profile-post-handler">
                {!enabledForPublicProfile.length ? (
                  <button
                    className="public-profile-post-btn"
                    onClick={() => {
                      this.props.dispatch(
                        publishToPublicProfile('Learning Hours', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Post to Profile')}
                  </button>
                ) : (
                  <button
                    className="public-profile-post-btn remove"
                    onClick={() => {
                      this.props.dispatch(
                        publishToPublicProfile('Learning Hours', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Remove from Profile')}
                  </button>
                )}
              </div>
            </div>
          )}
          <div className="learning-analytic-box-margin">
            <div className="learning-analytic-title">{tr('Learning Hours')}</div>
            <div className="learning-hours-time">
              <div className="sub-title">
                {clcHours}
                {clcMins}
              </div>
              {tr('Continuous Learning Hours in')} {new Date().getFullYear()}
            </div>
            {enableClcBadgings && (
              <div className="learning-hours-time">
                <div className="sub-title">
                  {parseInt(this.state.ClcTargetHours, 10)}{' '}
                  {parseInt(this.state.ClcTargetHours, 10) == 1 ? ' hr' : ' hrs'}
                </div>
                {tr('Annual Learning Goal')}
              </div>
            )}
            <div className="points-percent-box row">
              {learningHours && (
                <div className="points-percent small-6">
                  <div className="sub-title">
                    {learningHours.totalScore.toLocaleString({ maximumFractionDigits: 0 })}
                  </div>
                  {tr('Points')}
                </div>
              )}
              <div className="points-percent small-6">
                <div className="sub-title">{percentile}%</div>
                {tr('Percentile')}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LearningHours.propTypes = {
  OrgConfig: PropTypes.any,
  learningHours: PropTypes.any,
  userID: PropTypes.any,
  dashboardInfo: PropTypes.array,
  pathname: PropTypes.string,
  profile: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    OrgConfig: state.team.get('OrgConfig'),
    learningHours: state.profile.get('learningHours'),
    userID: state.currentUser.get('id'),
    pathname: state.routing.locationBeforeTransitions.pathname,
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    profile: state.profile.get('profile')
  };
}

export default connect(mapStoreStateToProps)(LearningHours);
