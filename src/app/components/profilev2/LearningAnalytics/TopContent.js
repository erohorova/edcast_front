import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import { connect } from 'react-redux';
import { publishToPublicProfile, _getUserContent } from '../../../actions/profileActions';
import LAExpandedViewWrapper from './LAExpandedViewWrapper';
import { push } from 'react-router-redux';
import RichTextReadOnly from './../../common/RichTextReadOnly';
import convertRichText from './../../../utils/convertRichText';
import { saveConsumptionPathwayHistoryURL } from '../../../actions/pathwaysActions';
import { saveConsumptionJourneyHistoryURL } from '../../../actions/journeyActions';

class TopContent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };
  }

  componentDidMount = () => {
    this.getTopContentData();
  };

  getTopContentData = () => {
    let { profile, userID } = this.props;

    let payload = {
      author_id: userID,
      limit: 5,
      offset: 0,
      fields: 'views_count,title,message,id,slug,card_type',
      'state[]': ['published'],
      sort: 'views_count',
      order: 'desc'
    };

    if (this.props.pathname.indexOf('/@') > -1) {
      payload.author_id = profile.id;
    }

    this.props.dispatch(_getUserContent(payload));
  };

  expandedViewHandler = () => {
    this.refs.inProgressExpanded.openSection();
  };

  redirectUrl = card => {
    let backUrl = window.location.pathname;
    if (card.cardType === 'pack') {
      if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
        this.props.dispatch(saveConsumptionPathwayHistoryURL(backUrl));
      }
      this.props.dispatch(push(`/pathways/${card.slug}`));
    } else if (card.cardType === 'journey') {
      if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
        this.props.dispatch(saveConsumptionJourneyHistoryURL(backUrl));
      }
      this.props.dispatch(push(`/journey/${card.slug}`));
    } else {
      this.props.dispatch(push(`/insights/${card.slug}`));
    }
  };

  getCardTitle = card => {
    if (card.cardType === 'pack' || card.cardType === 'journey') {
      return card.title || card.message;
    } else {
      return card.message;
    }
  };

  render() {
    let { userTopContent, publicProfile, isDashboard, userID, sequeceNumber, team } = this.props;
    let dashboardInfo = this.props.dashboardInfo;
    let enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'Top Content' && info.visible;
    });

    return (
      <div className="learning-analytic-container">
        <div className="learning-analytic-box" onClick={this.expandedViewHandler.bind(this)}>
          {isDashboard && !publicProfile && (
            <div className="public-profile-overlay">
              <div className="public-profile-post-handler">
                {!enabledForPublicProfile.length ? (
                  <button
                    className="public-profile-post-btn"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('Top Content', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Post to Profile')}
                  </button>
                ) : (
                  <button
                    className="public-profile-post-btn remove"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('Top Content', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Remove from Profile')}
                  </button>
                )}
              </div>
            </div>
          )}
          <div className="learning-analytic-box-margin">
            <div className="learning-analytic-title">{tr('Top Content')}</div>
            <div className="top-contents-container">
              {!!userTopContent &&
                userTopContent.map(content => {
                  const { id, viewsCount } = content;
                  let message = this.getCardTitle(content);

                  message = message.length >= 100 ? message.substr(0, 100) + '...' : message;
                  return (
                    <div
                      className="top-content-box"
                      key={id}
                      onClick={() => {
                        this.redirectUrl(content);
                      }}
                    >
                      <div className="top-content-msg">
                        <span>
                          <RichTextReadOnly text={convertRichText(message)} />
                        </span>
                      </div>
                      <div className="top-content-view-count">{viewsCount} views</div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>

        <LAExpandedViewWrapper ref="inProgressExpanded" sequeceNumber={sequeceNumber}>
          <Paper className="top-content-expanded-view">
            <div className="top-contents-container">
              {!!userTopContent &&
                userTopContent.map(content => {
                  const { id, viewsCount } = content;
                  let message = this.getCardTitle(content);
                  message = message.length >= 250 ? message.substr(0, 250) + '...' : message;
                  return (
                    <div
                      className="top-content-box"
                      key={id}
                      onClick={() => {
                        this.redirectUrl(content);
                      }}
                    >
                      <div className="top-content-msg">
                        <span>
                          <RichTextReadOnly text={convertRichText(message)} />
                        </span>
                      </div>
                      <div className="top-content-view-count">{viewsCount} views</div>
                    </div>
                  );
                })}
            </div>
          </Paper>
        </LAExpandedViewWrapper>
      </div>
    );
  }
}

TopContent.propTypes = {
  publicProfile: PropTypes.object,
  userTopContent: PropTypes.array,
  userID: PropTypes.string,
  pathname: PropTypes.string,
  dashboardInfo: PropTypes.array,
  isDashboard: PropTypes.bool,
  sequeceNumber: PropTypes.number,
  team: PropTypes.object,
  profile: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    userTopContent: state.profile.get('userTopContent'),
    userID: state.currentUser.get('id'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    profile: state.profile.get('profile')
  };
}

export default connect(mapStoreStateToProps)(TopContent);
