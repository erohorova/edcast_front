import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import { connect } from 'react-redux';
import { publishToPublicProfile, getActiveTopics } from '../../../actions/profileActions';
import LAExpandedViewWrapper from './LAExpandedViewWrapper';
import { renderSkillLabel } from './../../skillsGraph/utils';
import WordCloud from 'wordcloud';

class ActiveTopics extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount = () => {
    const dateRange = localStorage.getItem('skills-graph-date-range') || 'week';
    this.props.dispatch(getActiveTopics(dateRange));
  };

  expandedViewHandler = () => {
    this.refs.activeTopicExpanded.openSection();
    setTimeout(() => {
      this.geenerateWordCloud(this.props.activeTopics, 'wordcloud-activetopic-expanded', 75, [
        6,
        5,
        4.5,
        4,
        4
      ]);
    }, 500);
  };

  geenerateWordCloud = (activeTopics, eleID, limit, fontSize) => {
    let activeTopicsData = [];
    const ele = document.getElementById(eleID);

    if (activeTopics && ele) {
      (activeTopics || []).slice(0, limit).forEach((activeTopic, index) => {
        let size = 0;
        if (index < 4) {
          size = fontSize[0];
        } else if (index < 15) {
          size = fontSize[1];
        } else if (index < 35) {
          size = fontSize[2];
        } else {
          size = fontSize[3];
        }
        activeTopicsData.push([renderSkillLabel(activeTopic), size]);
      });
      WordCloud(ele, {
        list: activeTopicsData,
        gridSize: 1,
        weightFactor: size => {
          return (Math.pow(size, 2.4) * ele.offsetHeight) / 1024;
        },
        color: '#000000',
        rotateRatio: 0,
        rotationSteps: 0,
        backgroundColor: '#ffffff',
        shape: 'diamond'
      });
    }
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (
      nextProps.dashboardInfo !== this.props.dashboardInfo ||
      nextProps.activeTopics !== this.props.activeTopics
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    let { publicProfile, isDashboard, userID, sequeceNumber, activeTopics } = this.props;
    let dashboardInfo = this.props.dashboardInfo;
    let enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'Active Topics' && info.visible;
    });

    this.geenerateWordCloud(activeTopics, 'wordcloud-activetopic', 40, [8, 7, 6, 5, 4]);

    return (
      <div className="learning-analytic-container">
        <div className="learning-analytic-box" onClick={this.expandedViewHandler.bind(this)}>
          {isDashboard && !publicProfile && (
            <div className="public-profile-overlay">
              <div className="public-profile-post-handler">
                {!enabledForPublicProfile.length ? (
                  <button
                    className="public-profile-post-btn"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('Active Topics', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Post to Profile')}
                  </button>
                ) : (
                  <button
                    className="public-profile-post-btn remove"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('Active Topics', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Remove from Profile')}
                  </button>
                )}
              </div>
            </div>
          )}
          <div className="learning-analytic-box-margin">
            <div className="learning-analytic-title">{tr('Active Topics')}</div>
            <div className="active-topic-section">
              <canvas id="wordcloud-activetopic" />
            </div>
          </div>
        </div>

        <LAExpandedViewWrapper ref="activeTopicExpanded" sequeceNumber={sequeceNumber}>
          <Paper className="active-topic-expanded-view">
            <canvas id="wordcloud-activetopic-expanded" />
          </Paper>
        </LAExpandedViewWrapper>
      </div>
    );
  }
}

ActiveTopics.propTypes = {
  publicProfile: PropTypes.object,
  inProgressArray: PropTypes.array,
  inProgressCount: PropTypes.number,
  userID: PropTypes.string,
  pathname: PropTypes.string,
  dashboardInfo: PropTypes.array,
  isDashboard: PropTypes.bool,
  sequeceNumber: PropTypes.number,
  team: PropTypes.object,
  profile: PropTypes.object,
  activeTopics: PropTypes.array
};

function mapStoreStateToProps(state) {
  return {
    userID: state.currentUser.get('id'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    profile: state.profile.get('profile'),
    activeTopics: state.profile.get('activeTopics')
  };
}

export default connect(mapStoreStateToProps)(ActiveTopics);
