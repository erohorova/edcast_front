import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import { getInProgress } from '../../../actions/mylearningplanV5Actions';
import { connect } from 'react-redux';
import { publishToPublicProfile } from '../../../actions/profileActions';
import MyLearningPlanHeaderRow from '../../profile/v5/LearningQueue/MyLearningPlanHeaderRow';
import MyLearningPlanContentRow from '../../profile/v5/LearningQueue/MyLearningPlanContentRow';
import LAExpandedViewWrapper from './LAExpandedViewWrapper';
import { push } from 'react-router-redux';
import { saveConsumptionJourneyHistoryURL } from '../../../actions/journeyActions';
import { saveConsumptionPathwayHistoryURL } from '../../../actions/pathwaysActions';
import unescape from 'lodash/unescape';
class InProgress extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      journeyConsumptionV2: window.ldclient.variation('journey-consumption-v2', false)
    };
  }

  componentDidMount = () => {
    this.getInProgressData();
  };

  getInProgressData = () => {
    let { profile } = this.props;

    let payload = {
      limit: 15,
      offset: 0,
      'state[]': ['started'],
      'card_type[]': ['pack', 'journey']
    };

    if (this.props.pathname.indexOf('/@') > -1) {
      payload.user_id = profile.id;
    }

    this.props.dispatch(getInProgress(payload));
  };

  expandedViewHandler = () => {
    this.refs.inProgressExpanded.openSection();
  };

  redirectUrl = card => {
    let BackUrl = window.location.pathname;
    if (card.cardType === 'pack') {
      if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
        this.props.dispatch(saveConsumptionPathwayHistoryURL(BackUrl));
      }
      this.props.dispatch(push(`/pathways/${card.slug}`));
    } else if (card.cardType === 'journey') {
      if (this.state.isCardV3 && this.state.journeyConsumptionV2) {
        this.props.dispatch(saveConsumptionJourneyHistoryURL(BackUrl));
      }
      this.props.dispatch(push(`/journey/${card.slug}`));
    }
  };

  render() {
    let {
      inProgressArray,
      inProgressCount,
      publicProfile,
      isDashboard,
      userID,
      sequeceNumber,
      team
    } = this.props;
    let dashboardInfo = this.props.dashboardInfo;
    let enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'In Progress' && info.visible;
    });

    return (
      <div className="learning-analytic-container">
        <div
          className="learning-analytic-box"
          onClick={inProgressCount ? this.expandedViewHandler.bind(this) : () => {}}
        >
          {isDashboard && !publicProfile && (
            <div className="public-profile-overlay">
              <div className="public-profile-post-handler">
                {!enabledForPublicProfile.length ? (
                  <button
                    className="public-profile-post-btn"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('In Progress', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Post to Profile')}
                  </button>
                ) : (
                  <button
                    className="public-profile-post-btn remove"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('In Progress', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Remove from Profile')}
                  </button>
                )}
              </div>
            </div>
          )}
          <div className="learning-analytic-box-margin">
            <div className="learning-analytic-title">{tr('In Progress')}</div>
            <div className="in-progress-title">
              {!!inProgressCount &&
                inProgressArray.slice(0, 4).map((assignment, index) => {
                  let card = assignment.assignable;
                  let msg = unescape(
                    (card.title || card.message).length >= 63
                      ? (card.title || card.message).substr(0, 63) + '...'
                      : card.title || card.message
                  );
                  let per = card.completedPercentage;

                  return (
                    <div
                      className="in-progress-assignment"
                      key={index}
                      onClick={() => {
                        this.redirectUrl(card);
                      }}
                    >
                      <div className="in-progress-msg">{msg}</div>
                      <div className="completedPerContainer">
                        <div className="completedBar" />
                        <div className="completedPer" style={{ width: per + '%' }} />
                        <div
                          className="perCount"
                          style={{ marginLeft: (per >= 95 ? 95 : per) + '%' }}
                        >{`${per}%`}</div>
                      </div>
                    </div>
                  );
                })}
              {!inProgressCount && (
                <div className="empty-state">You don't have anything in progress.</div>
              )}
            </div>
          </div>
        </div>

        <LAExpandedViewWrapper ref="inProgressExpanded" sequeceNumber={sequeceNumber}>
          <Paper className="in-progress-expanded-view">
            <MyLearningPlanHeaderRow activeContentTab="current" team={team} />
            {inProgressArray.map((item, index) => {
              return (
                <MyLearningPlanContentRow
                  key={index}
                  assignment={item}
                  assignmentCard={item.assignable}
                  activeContentTab="current"
                  team={team}
                />
              );
            })}
          </Paper>
        </LAExpandedViewWrapper>
      </div>
    );
  }
}

InProgress.propTypes = {
  publicProfile: PropTypes.object,
  inProgressArray: PropTypes.any,
  inProgressCount: PropTypes.number,
  userID: PropTypes.string,
  pathname: PropTypes.string,
  dashboardInfo: PropTypes.array,
  isDashboard: PropTypes.bool,
  sequeceNumber: PropTypes.number,
  team: PropTypes.object,
  profile: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    inProgressArray: state.profile.get('inProgressArray'),
    inProgressCount: state.profile.get('inProgressCount'),
    userID: state.currentUser.get('id'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    profile: state.profile.get('profile')
  };
}

export default connect(mapStoreStateToProps)(InProgress);
