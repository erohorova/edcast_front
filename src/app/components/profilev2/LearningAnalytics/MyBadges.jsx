import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import { connect } from 'react-redux';
import { publishToPublicProfile, getCombinedUserBadges } from '../../../actions/profileActions';
import LAExpandedViewWrapper from './LAExpandedViewWrapper';

class MyBadges extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount = () => {
    if (this.props.pathname.indexOf('/@') > -1) {
      return;
    }
    let { OrgConfig, userID } = this.props;
    let pathwayBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].pathwayBadgings;
    let clcBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].clcBadgings;
    this.props.dispatch(
      getCombinedUserBadges(userID, { limit: 100 }, pathwayBadgings, clcBadgings)
    );
  };

  expandedViewHandler = () => {
    this.refs.myBadgesExpanded.openSection();
  };

  shareOnLinkedIn = (identifier, badge_title) => {
    let LinkedInUrl = 'https://www.linkedin.com/shareArticle';
    let mini = 'mini=true';
    let title = `title=Posted from EdCast: ${
      this.props.currentUserName
    } earned a badge for ${badge_title}`;
    let shared_url = 'url=' + window.location.origin + '/verify_badge/' + identifier;
    let source = 'edCast';
    let url = `${LinkedInUrl}?${mini}&${title}&${shared_url}&${source}`;
    window.open(encodeURI(url), 'Share on Linkedin', 'height=520,width=570');
  };

  render() {
    let {
      userCardBadges,
      userClcBadges,
      OrgConfig,
      publicProfile,
      isDashboard,
      sequeceNumber,
      userID
    } = this.props;
    let showShareonLinkedIn =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].sharePathwayBadgesonLinkedIn;
    let pathwayBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].pathwayBadgings;
    let clcBadgings =
      OrgConfig &&
      !!OrgConfig.badgings['web/badgings'] &&
      !!OrgConfig.badgings['web/badgings'].clcBadgings;
    let dashboardInfo = this.props.dashboardInfo;
    let enabledForPublicProfile = dashboardInfo.filter(info => {
      return info.name === 'My Badges' && info.visible;
    });
    let userBadges = ((pathwayBadgings ? userCardBadges : []) || [])
      .concat((clcBadgings ? userClcBadges : []) || [])
      .filter(item => item.imageUrl);
    return (
      <div className="learning-analytic-container">
        <div
          className="learning-analytic-box"
          onClick={!!userBadges && userBadges.length && this.expandedViewHandler.bind(this)}
        >
          {isDashboard && !publicProfile && (
            <div className="public-profile-overlay">
              <div className="public-profile-post-handler">
                {!enabledForPublicProfile.length ? (
                  <button
                    className="public-profile-post-btn"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('My Badges', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Post to Profile')}
                  </button>
                ) : (
                  <button
                    className="public-profile-post-btn remove"
                    onClick={e => {
                      e.stopPropagation();
                      this.props.dispatch(
                        publishToPublicProfile('My Badges', dashboardInfo, userID)
                      );
                    }}
                  >
                    {tr('Remove from Profile')}
                  </button>
                )}
              </div>
            </div>
          )}
          <div className="learning-analytic-box-margin">
            <div className="learning-analytic-title">{tr('My Badges')}</div>
            <div className="all-badge">
              {userBadges &&
                userBadges.slice(0, 6).map(badge => {
                  return (
                    <div key={badge.id} className="badge">
                      <img src={badge.imageUrl} />
                      <div>{badge.title}</div>
                    </div>
                  );
                })}
              {userBadges && !userBadges.length && (
                <div className="empty-state">No badges found.</div>
              )}
            </div>
          </div>
        </div>

        <LAExpandedViewWrapper ref="myBadgesExpanded" sequeceNumber={sequeceNumber}>
          <Paper className="badges-expanded-view">
            <div className="all-badge">
              {userBadges &&
                userBadges.map(badge => {
                  return (
                    <div key={badge.id} className="badge">
                      <img src={badge.imageUrl} />
                      <div>{badge.title}</div>
                      {showShareonLinkedIn && !publicProfile && (
                        <button
                          className="share-with-linkedIn-btn"
                          onClick={this.shareOnLinkedIn.bind(this, badge.identifier, badge.title)}
                        >
                          <span className="share-with-linkedIn-btn-icon">
                            <img src="https://d1iwkfmdo6oqxx.cloudfront.net/organizations/co_branding_logos/000/001/454/original/LinkedInLogo.png" />
                          </span>
                          <span className="share-with-linkedIn-btn-title">
                            {tr('Share with Linkedin')}
                          </span>
                        </button>
                      )}
                    </div>
                  );
                })}
              {userBadges && !userBadges.length && (
                <div className="empty-state">No badges found.</div>
              )}
            </div>
          </Paper>
        </LAExpandedViewWrapper>
      </div>
    );
  }
}

MyBadges.propTypes = {
  userCardBadges: PropTypes.array,
  userClcBadges: PropTypes.array,
  OrgConfig: PropTypes.object,
  currentUserName: PropTypes.string,
  pathname: PropTypes.string,
  userID: PropTypes.string,
  dashboardInfo: PropTypes.array,
  publicProfile: PropTypes.any,
  isDashboard: PropTypes.bool,
  sequeceNumber: PropTypes.number
};

function mapStoreStateToProps(state) {
  return {
    userCardBadges: state.profile.get('userCardBadges'),
    userClcBadges: state.profile.get('userClcBadges'),
    OrgConfig: state.team.get('OrgConfig'),
    currentUserName: state.currentUser.get('name'),
    userID: state.currentUser.get('id'),
    dashboardInfo: state.profile.get('dashboardInfo').toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    publicProfile: state.profile.get('publicProfile')
  };
}

export default connect(mapStoreStateToProps)(MyBadges);
