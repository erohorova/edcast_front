import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from 'material-ui/svg-icons/content/clear';

class LAExpandedViewWrapper extends Component {
  constructor(props, context) {
    super(props, context);

    this.style = {
      closeIcon: {
        position: 'absolute',
        top: '15px',
        right: '15px',
        cursor: 'pointer'
      }
    };

    this.state = {
      isExpanded: false
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside = e => {
    if (
      this.refs &&
      this.refs.currentComponent &&
      (this.refs.currentComponent || '').contains(e.target)
    ) {
      return;
    } else {
      this.setState({
        isExpanded: false
      });
    }
  };

  closeSection = () => {
    this.setState({
      isExpanded: false
    });
  };

  openSection = () => {
    this.setState({
      isExpanded: true
    });
  };

  render() {
    const { sequeceNumber, children } = this.props;
    const { isExpanded } = this.state;
    const leftPossition =
      'calc(' +
      (sequeceNumber % 3) * Math.pow(2, sequeceNumber % 3) * 0.625 +
      'rem - ' +
      (sequeceNumber % 3) * 2.5 +
      'rem - ' +
      (sequeceNumber % 3) * 33.3333 +
      'vw)';

    if (!isExpanded) {
      return null;
    }

    return (
      <div
        className="LA-expanded-view"
        style={{ padding: '0 2.5rem', left: sequeceNumber % 3 ? leftPossition : '-2.5rem' }}
      >
        <span
          className="arrow_box"
          style={{ left: (2 * (sequeceNumber % 3) + 1) * (100 / 6) + '%' }}
        />
        <div className="LA-expanded-view-constainer" ref="currentComponent">
          <CloseIcon
            style={this.style.closeIcon}
            color="#6f708b"
            onClick={this.closeSection.bind(this)}
          />
          {children}
        </div>
      </div>
    );
  }
}

LAExpandedViewWrapper.propTypes = {
  sequeceNumber: PropTypes.number,
  children: PropTypes.object
};

export default LAExpandedViewWrapper;
