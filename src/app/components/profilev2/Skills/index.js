import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import getFleIcon from '../../../utils/getFileIcon';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';
import SkillPassport from 'edc-web-sdk/components/icons/SkillPassport';
import DownloadTranscript from '../DownloadTranscript';
import DateConverter from '../../common/DateConverter';

class Skills extends Component {
  constructor(props) {
    super(props);

    this.state = {
      viewMore: false
    };

    this.styles = {
      editIconNew: {
        width: 16,
        height: 16,
        color: '#6f708b',
        verticalAlign: 'middle'
      }
    };
  }

  openUrl = (skill, skillCred, flag) => {
    if (flag && skill.credentialUrl) {
      window.open(skill.credentialUrl, '_blank');
    } else {
      if (skillCred) {
        refreshFilestackUrl(skillCred.url)
          .then(resp => {
            window.open(resp.signed_url, '_blank');
          })
          .catch(err => {
            console.error(`Error in MyProfile.openUrl.refreshFilestackUrl.func : ${err}`);
          });
      }
    }
  };

  clickViewMoreHandler = () => {
    this.setState(prevState => {
      const viewMore = !prevState.viewMore;
      return { viewMore };
    });
  };

  render() {
    const { userSkills, toggleModal, modalSkillOpen, publicProfile } = this.props;
    const { viewMore } = this.state;

    return (
      <div className="skill-passport">
        {(!publicProfile || !!(userSkills && userSkills.length)) && (
          <div className="skill-passport-title">
            <div className="skill-passport-title-holder">
              <span>
                {tr('Skills Passport')}
                <sup>TM</sup>
              </span>
            </div>
            {!publicProfile && <DownloadTranscript />}
          </div>
        )}
        <div className="skill-passport-container">
          {(!viewMore ? userSkills.slice(0, 3) : userSkills).map(skill => {
            return (
              <div className="skill">
                <Paper className="skill-block">
                  {!publicProfile && (
                    <div className="float-right icon-block">
                      <button
                        aria-label={tr('Delete skill')}
                        onClick={toggleModal.bind(this, skill.id)}
                      >
                        <DeleteIcon style={this.styles.editIconNew} />
                      </button>
                      <button
                        aria-label={tr('Edit skill')}
                        onClick={modalSkillOpen.bind(this, skill)}
                      >
                        <span tabIndex={-1} className="hideOutline">
                          <EditIcon style={this.styles.editIconNew} />
                        </span>
                      </button>
                    </div>
                  )}
                  <div className="skill-name">{skill.name}</div>
                  <div className="skill-description">{skill.description}</div>
                  <div className="skill-other-details">
                    <strong>{tr('Level')}</strong>:{' '}
                    <span style={{ textTransform: 'capitalize' }}>{skill.skillLevel}</span>
                    <br /> <strong>{tr('Experience')}</strong>: {skill.experience}
                    <br /> <strong>{tr('Credentials')}</strong>:{' '}
                    <Credentials skill={skill} openUrl={this.openUrl} />
                    <br /> <strong>{tr('Expiry')}</strong>:{' '}
                    {skill.expiryDate ? (
                      <DateConverter date={skill.expiryDate} dateFormat={'MM/DD/YYYY'} />
                    ) : (
                      '-'
                    )}
                  </div>
                  <div className="skill-passport-img">
                    <SkillPassport className="skill-passport-img" />
                  </div>
                </Paper>
              </div>
            );
          })}

          {(userSkills || []).length > 3 && (
            <div className="view-more-btn">
              <span
                className="cursor-pointer"
                onClick={() => {
                  this.clickViewMoreHandler();
                }}
              >
                {viewMore ? tr('View Less') : tr('View More')}
              </span>
            </div>
          )}
        </div>
      </div>
    );
  }
}

Skills.propTypes = {
  userSkills: PropTypes.any,
  toggleModal: PropTypes.func,
  modalSkillOpen: PropTypes.func,
  publicProfile: PropTypes.bool
};

export default Skills;

function Credentials(props) {
  const { skill, openUrl } = props;
  const skillCred = skill.credential && skill.credential[0];
  const isCredIcon =
    skillCred && skillCred.mimetype && !~skillCred.mimetype.indexOf(`image/`) && iconFileSrc;
  const iconFileSrc = getFleIcon('pdf');

  return (
    <span>
      {!((skillCred && skillCred.url) || skill.credentialUrl || skill.credentialName) && (
        <span>-</span>
      )}
      <a target="_blank" onClick={() => openUrl(skill, skillCred, false)}>
        <span>{skill.credentialName || ''}</span> &nbsp;
        {isCredIcon ? (
          <img className="iconFileSrc" src={iconFileSrc} title={tr('Source of the content')} />
        ) : (
          <img className="cred-img-margin" src={skillCred && skillCred.url} />
        )}
      </a>
    </span>
  );
}

Credentials.propTypes = {
  skill: PropTypes.any,
  openUrl: PropTypes.func
};
