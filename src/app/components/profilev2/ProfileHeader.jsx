import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import _ from 'lodash';

import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import TooltipLabel from '../common/TooltipLabel';

import colors from 'edc-web-sdk/components/colors/index';
import {
  GroupLeaderBadge,
  InfluencerBadge,
  AdminBadge,
  MemberBadge,
  SMEBadge,
  Twitter,
  Email,
  LinkedIn
} from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { getUserCustomFields, getAllCustomFields } from 'edc-web-sdk/requests/users.v2';
import { getClcRecords } from 'edc-web-sdk/requests/clc';

import { toggleFollow } from '../../actions/usersActions';
import { openFollowingUserListModal } from '../../actions/modalActions';
import BlurImage from '../common/BlurImage';
import { Permissions } from '../../utils/checkPermissions';
import moment from 'moment';

class ProfileHeader extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      iconColor: window.ldclient.variation('additional-profile-info', false)
        ? 'rgba(0, 0, 0, 0.6)'
        : '#454560',
      showEmail:
        !!(this.props.team && this.props.team.config && this.props.team.config.enable_show_email) &&
        this.props.profileShowUserContent,
      user: {},
      additionalInfo: [],
      clc: window.ldclient.variation('clc', false),
      followPending: false,
      publicPrivateProfileBtn: this.props.publicPrivateProfileBtn,
      additionalProfileInfoEnabled: window.ldclient.variation('additional-profile-info', false),
      upgradeSkillsPassport: !this.props.profileShowUserContent,
      viewMoreFlag: false,
      isShowUserJob: window.ldclient.variation('is-show-user-job', false)
    };

    this.skypeIntegration = window.ldclient.variation('skype-integration', false);

    this.styles = {
      badgeStyles: {
        padding: '0 8px',
        width: 27,
        height: 27
      },
      downArrow: {
        transform: 'rotate(45deg)',
        width: 7,
        height: 7,
        margin: '3px 10px'
      },
      upArrow: {
        transform: 'rotate(225deg)',
        width: 7,
        height: 7,
        margin: '-1px 10px'
      },
      twitterStyles: {
        padding: '0 8px',
        width: 20,
        height: 16
      },
      followCount: {
        color: '#ffffff'
      },
      iconStyle: {
        background: 'rgba(0, 0, 0, 0.6)',
        borderRadius: '50%',
        color: '#ffffff',
        width: 17.7,
        height: 17.7
      },
      twitterIconStyle: {
        background: 'rgba(0, 0, 0, 0.6)',
        borderRadius: '50%',
        color: '#ffffff',
        width: 20,
        height: 16
      },
      editIcon: {
        width: 14,
        height: 14,
        color: '#ffffff',
        verticalAlign: 'middle'
      },
      avatarBox: {
        userSelect: 'none',
        height: '10.2rem',
        width: '10.2rem',
        border: 'solid 0.1rem #acadc1',
        position: 'absolute'
      },
      tooltipStyles: {
        top: '-2.5rem',
        pointerEvents: 'none'
      }
    };
  }

  getRoleBadges = (roles, rolesDefaultNames = []) => {
    let allRoles = _.uniqWith(
      _.concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    let badgeIcons = [];
    allRoles.map((role, index) => {
      let data;
      let roleParam = role ? role.toLowerCase() : '';
      switch (roleParam) {
        case 'member':
          data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'admin':
          data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'groupleader':
          data = [<GroupLeaderBadge />, this.state.iconColor];
          break;
        case 'influencer':
          data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'sme':
          data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        default:
          break;
      }
      if (data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;
        badgeIcons.push(
          <IconButton
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={this.styles.tooltipStyles}
            disableTouchRipple
            style={this.styles.badgeStyles}
            iconStyle={Object.assign({}, this.styles.iconStyle, { backgroundColor: data[1] })}
          >
            {data[0]}
          </IconButton>
        );
      }
    });
    return badgeIcons;
  };

  toSettingsPage = () => {
    this.props.dispatch(push('/settings'));
  };
  openFollow = (e, type, value) => {
    e.preventDefault();
    if (value) {
      this.props.dispatch(openFollowingUserListModal(type));
    }
  };

  followClickHandler = user => {
    this.setState({
      followPending: true
    });
    this.props
      .dispatch(toggleFollow(user.id, !user.isFollowing))
      .then(() => {
        let newUser = Object.assign({}, user);
        if (newUser.isFollowing) {
          newUser.followersCount--;
          newUser.isFollowing = !user.isFollowing;
        } else {
          newUser.followersCount++;
          newUser.isFollowing = !user.isFollowing;
        }
        this.setState({
          followPending: false,
          user: newUser
        });
      })
      .catch(err => {
        console.error(`Error in ProfileHeader.toggleFollow.func : ${err}`);
      });
  };

  componentWillReceiveProps(nextProps) {
    let user =
      (nextProps.publicProfile &&
        nextProps.currentUser &&
        nextProps.currentUser.publicProfileBasicInfo) ||
      nextProps.currentUser;
    if (this.state.user && this.state.user.email === user.email) {
      this.setState({ user });
      return;
    }
    this.setState(
      prevState => {
        return { user, oldUser: prevState.user };
      },
      () => {
        if (this.state.oldUser && this.state.oldUser.id !== user.id) {
          let userCustomInfo = [];
          getUserCustomFields({ 'user_ids[]': user.id, send_array: true })
            .then(userFieldsResult => {
              let userCustomFields = userFieldsResult[0].customFields;
              getAllCustomFields()
                .then(allFieldsResult => {
                  userCustomInfo = _.filter(userCustomFields, item => {
                    let indexInAllFields = _.findIndex(allFieldsResult.customFields, field => {
                      return field.abbreviation === item.abbreviation;
                    });
                    if (
                      ~indexInAllFields &&
                      allFieldsResult.customFields[indexInAllFields].showInProfile
                    ) {
                      return item;
                    }
                  });
                  this.setState({ additionalInfo: userCustomInfo });
                })
                .catch(err =>
                  console.error('An error had occurred while getAllCustomFields.fnc running ', err)
                );
            })
            .catch(err =>
              console.error('An error had occurred while getUserCustomFields.fnc running ', err)
            );
        }
      }
    );
  }

  showMyScore(val) {
    let show = val;
    if (show === undefined) {
      return true;
    } else {
      return show.visible;
    }
  }

  toViewMore = () => {
    this.setState({ viewMoreFlag: !this.state.viewMoreFlag });
  };

  onTwitterHandleClick = () => {};

  onLinkedInHandleClick = () => {};

  getSlicedColumn = (splitedColumns, bio, showJD) => {
    let totalColumn = 3;
    let slicedColumn;
    this.showViewMore = false;
    if (bio) {
      totalColumn -= 1;
    }

    if (showJD) {
      totalColumn -= 1;
    }
    if (splitedColumns.length > 2 && totalColumn == 2) {
      totalColumn = 1;
      this.showViewMore = true;
    }
    if (bio && showJD && splitedColumns.length > 1) {
      totalColumn = 0;
      this.showViewMore = true;
    }
    if (totalColumn === 3 && splitedColumns.length > 3) {
      totalColumn = 2;
      this.showViewMore = true;
    }
    return totalColumn;
  };

  render() {
    let user =
      Object.keys(this.state.user).length == 0
        ? (this.props.publicProfile &&
            this.props.currentUser &&
            this.props.currentUser.publicProfileBasicInfo &&
            this.props.currentUser.publicProfileBasicInfo) ||
          this.props.currentUser
        : this.state.user;
    let bgImageURL =
      user.coverImage ||
      (user.coverimages && user.coverimages.banner_url) ||
      (user.coverimages && user.coverimages.banner) ||
      '//dp598loym07sk.cloudfront.net/assets/default_banner_user_image-6ea329e6241a87af91827cecafd26fc1.png';
    let bannerStyle = {
      backgroundImage: `linear-gradient(to bottom, rgba(255,0,0,0), rgba(69,69,96,1)), url('${bgImageURL}')`
    };
    let imageUrl =
      user.avatar || user.picture || (user.avatarimages && user.avatarimages.medium) || '';

    let publicProfileUserID =
      this.props.currentUser &&
      this.props.currentUser.publicProfileBasicInfo &&
      this.props.currentUser.publicProfileBasicInfo.id;

    let currentUserID = this.props.currentUser && this.props.currentUser.id;

    let userHandle = this.props && this.props.currentUser && this.props.currentUser.handle;
    let columnHeight = 5;
    let splitedColumns = [];
    if (this.state.additionalInfo && this.state.additionalInfo.length > columnHeight) {
      for (let i = 0; i < this.state.additionalInfo.length; i++) {
        let columnIndex = Math.floor(i / columnHeight);
        if (!Array.isArray(splitedColumns[columnIndex])) {
          splitedColumns[columnIndex] = [];
        }
        splitedColumns[columnIndex].push(this.state.additionalInfo[i]);
      }
    } else {
      splitedColumns[0] = this.state.additionalInfo;
    }

    if (
      this.props.pathname == '/@' + userHandle ||
      this.props.pathname == '/me' ||
      this.props.pathname == '/'
    ) {
      publicProfileUserID = currentUserID;
    }
    let joinDate = moment(user.onboardingCompletedDate).format('MMM YYYY');
    let showEmailField =
      user.email && this.state.showEmail && !this.state.additionalProfileInfoEnabled;
    let jobTitle = (user.profile && user.profile.jobTitle) || '';
    let showJD = jobTitle && this.state.isShowUserJob;
    return (
      <div className="profile-header-v4">
        {currentUserID &&
          ((publicProfileUserID && currentUserID == publicProfileUserID) ||
            this.props.pathname.indexOf('/me') !== -1) && (
            <div className="edit-tab">
              <button
                aria-label={tr('Edit Profile')}
                onClick={this.toSettingsPage}
                className="edit-container pointer"
              >
                <div
                  className="edit-tab-button"
                  onClick={() => {
                    this.props.dispatch(push('/settings'));
                  }}
                >
                  <EditIcon style={this.styles.editIcon} /> {tr('Edit Profile')}
                </div>
              </button>
            </div>
          )}

        <div className="profile-header-banner" style={bannerStyle} />
        <div className="profile-header-user-info">
          <div className="profile-header-user-block">
            <div
              className={`user-avatar ${
                this.state.additionalProfileInfoEnabled ? 'user-avatar__v2' : ''
              }`}
            >
              <BlurImage style={this.styles.avatarBox} id={user.id} image={imageUrl} />
            </div>

            <div className="user-main-info">
              {user.roles && (
                <div className="user-roles">
                  {this.getRoleBadges(user.roles, user.rolesDefaultNames)}
                </div>
              )}
              <div className="user-name">
                <span>{user.name || user.firstName + ' ' + user.lastName || ''}</span>
              </div>
              {showJD && (
                <div className="user-name user-name__thin">
                  <span>{jobTitle}</span>
                </div>
              )}
              <div className="user-info-summary">
                <div className="user-info__information">
                  <div className="user-info__column">
                    {user.bio && (
                      <div className="user-info__row">
                        <span className="user-info__label">{tr('Bio')}: </span>
                        <span className="user-info__content">{user.bio}</span>
                      </div>
                    )}
                    {showEmailField && (
                      <div className="user-info__row">
                        <span className="user-info__label">{tr('Email')}: </span>
                        <span className="user-info__content">{user.email}</span>
                      </div>
                    )}
                  </div>
                </div>
              </div>
              {splitedColumns[0] &&
                !!splitedColumns[0].length &&
                this.state.additionalProfileInfoEnabled && (
                  <div className="user-info-summary">
                    <div className="user-info__information">
                      {splitedColumns.map((column, index) => {
                        let slicedColumn = this.getSlicedColumn(
                          splitedColumns[0],
                          user.bio,
                          showJD
                        );
                        return (
                          <div className="user-info__column" key={index}>
                            {!this.state.viewMoreFlag &&
                              column.slice(0, slicedColumn).map((item, number) => {
                                return (
                                  <div className="user-info__row" key={index + number}>
                                    <span className="user-info__label">{item.displayName}: </span>
                                    <span className="user-info__content">{item.value}</span>
                                  </div>
                                );
                              })}
                            {this.state.viewMoreFlag &&
                              column.map((item, number) => {
                                return (
                                  <div className="user-info__row" key={index + number}>
                                    <span className="user-info__label">{item.displayName}: </span>
                                    <span className="user-info__content">{item.value}</span>
                                  </div>
                                );
                              })}
                          </div>
                        );
                      })}
                      {this.showViewMore && (
                        <div
                          aria-label={tr('View More')}
                          onClick={this.toViewMore}
                          className="view-more-button pointer"
                        >
                          View {this.state.viewMoreFlag ? 'Less' : 'More'}
                          <i
                            style={
                              this.state.viewMoreFlag ? this.styles.upArrow : this.styles.downArrow
                            }
                          />
                        </div>
                      )}
                    </div>
                  </div>
                )}
            </div>

            <div className="right-info-block">
              <div>
                {false && (
                  <div>
                    <IconButton
                      tooltip={tr('Twitter')}
                      tooltipStyles={this.styles.tooltipStyles}
                      disableTouchRipple
                      style={this.styles.twitterStyles}
                      iconStyle={this.styles.twitterIconStyle}
                      onClick={this.onTwitterHandleClick}
                    >
                      {<Twitter />}
                    </IconButton>
                    <IconButton
                      tooltip={tr('LinkedIn')}
                      tooltipStyles={this.styles.tooltipStyles}
                      disableTouchRipple
                      style={this.styles.twitterStyles}
                      iconStyle={this.styles.twitterIconStyle}
                      onClick={this.onLinkedInHandleClick}
                    >
                      {<LinkedIn />}
                    </IconButton>
                  </div>
                )}
                <div className="joined-date">
                  Joined - <span>{joinDate}</span>
                </div>
                {currentUserID &&
                  ((publicProfileUserID && currentUserID == publicProfileUserID) ||
                    this.props.pathname.indexOf('/me') !== -1) && (
                    <div className="user-follow-data">
                      <a
                        tabIndex={0}
                        href="#"
                        className="followers-following following-block pointer"
                        onClick={e => this.openFollow(e, 'following', user.followingCount)}
                      >
                        {user.followingCount} <span>{tr('Following')}</span>
                      </a>
                      <div className="dot-seperator">•</div>
                      <a
                        tabIndex={0}
                        href="#"
                        className="followers-following pointer"
                        onClick={e => this.openFollow(e, 'follower', user.followersCount)}
                      >
                        {user.followersCount} <span>{tr('Followers')}</span>
                      </a>
                    </div>
                  )}
                {currentUserID &&
                  currentUserID != publicProfileUserID &&
                  this.props.pathname.indexOf('/me') === -1 &&
                  !Permissions.has('DISABLE_USER_FOLLOW') && (
                    <div
                      style={{
                        marginTop: 20,
                        marginRight: -4
                      }}
                    >
                      <TooltipLabel
                        text={user.isSuspended ? 'This action is currently unavailable' : ''}
                        html={true}
                      >
                        <FollowButton
                          following={user.isFollowing}
                          className="follow"
                          label={tr(user.isFollowing ? 'Following' : 'Follow')}
                          hoverLabel={tr(user.isFollowing ? 'Unfollow' : '')}
                          pendingLabel={tr(user.isFollowing ? 'Unfollowing...' : 'Following...')}
                          pending={this.state.followPending}
                          onTouchTap={this.followClickHandler.bind(this, user)}
                          disabled={user.isSuspended}
                        />
                      </TooltipLabel>

                      {currentUserID &&
                        currentUserID != publicProfileUserID &&
                        this.props.pathname.indexOf('/me') === -1 &&
                        this.skypeIntegration &&
                        user.email && (
                          <div className="text-center" style={{ margin: '10px 0 0 0' }}>
                            <button
                              className="skype-call-btn"
                              onClick={() => {
                                window.open(`sip:${user.email}`);
                              }}
                              target="_blank"
                            >
                              Skype Call
                            </button>
                          </div>
                        )}
                    </div>
                  )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProfileHeader.propTypes = {
  publicProfile: PropTypes.bool,
  pathname: PropTypes.any,
  currentUser: PropTypes.object,
  team: PropTypes.object,
  profileShowUserContent: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(ProfileHeader);
