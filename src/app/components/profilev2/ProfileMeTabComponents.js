import React, { Component } from 'react';
import MyChannelsContainerv2 from './../profile/MyChannels/v2/MyChannelsContainer_v2';
import GroupsContainerv3 from './../team/groups/GroupsContainer_v3';
import MyTeamContainerv2 from './../profile/MyTeam/MyTeamV2';

const componentDefaultRoutes = ['/me/team', '/me/groups', '/me/channels'];

class ProfileMeTabComponents extends Component {
  constructor(props, context) {
    super(props, context);
    this.componentRoute = '';
    this.ProfileMeTabComponents = {};
  }

  render() {
    componentDefaultRoutes.forEach(route => {
      if (!!~window.location.pathname.indexOf(route)) {
        this.componentRoute = route;
      }
    });
    switch (this.componentRoute) {
      case '/me/team':
        this.ProfileMeTabComponents = <MyTeamContainerv2 {...this.props} isMeTab />;
        break;
      case '/me/groups':
        this.ProfileMeTabComponents = <GroupsContainerv3 {...this.props} isMeTab />;
        break;
      case '/me/channels':
        this.ProfileMeTabComponents = <MyChannelsContainerv2 {...this.props} isMeTab />;
        break;
    }
    return <div>{this.ProfileMeTabComponents}</div>;
  }
}

export default ProfileMeTabComponents;
