import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { getConsumeHistory } from 'edc-web-sdk/requests/profile';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';
import Avatar from 'material-ui/Avatar';
import ListItem from 'material-ui/List/ListItem';
import colors from 'edc-web-sdk/components/colors/index';
import Pathway from 'edc-web-sdk/components/icons/Pathway';
import SmartBite from 'edc-web-sdk/components/icons/SmartBite';
import Stream from 'edc-web-sdk/components/icons/Stream';
import Spinner from '../../common/spinner';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import formatNumbers from '../../../utils/formatNumbers';
import { tr } from 'edc-web-sdk/helpers/translations';

class HistoryContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      history: [],
      offset: 0,
      isLastPage: true,
      pending: true
    };
    this.recordsPerPage = 10;
    this.viewMoreClickHandler = this.viewMoreClickHandler.bind(this);
  }

  componentDidMount() {
    getConsumeHistory(this.recordsPerPage, 0, undefined, 'all_time')
      .then(data => {
        this.setState({
          history: data,
          offset: this.recordsPerPage,
          isLastPage: data.length < this.recordsPerPage,
          pending: false
        });
      })
      .catch(err => {
        console.error(`Error in HistoryContainer.getConsumeHistory.func : ${err}`);
      });
  }

  viewMoreClickHandler() {
    getConsumeHistory(this.recordsPerPage, this.state.offset, undefined, 'all_time')
      .then(data => {
        this.setState({
          history: this.state.history.concat(data),
          offset: this.state.offset + this.recordsPerPage,
          isLastPage: data.length < this.recordsPerPage
        });
      })
      .catch(err => {
        console.error(
          `Error in HistoryContainer.viewMoreClickHandler.getConsumeHistory.func : ${err}`
        );
      });
  }

  render() {
    return (
      <div className="content" id="content-leaderboard">
        <div className="row">
          <div className="small-12 columns">
            <Table
              selectable={false}
              style={{
                backgroundColor: 'transparent',
                borderCollapse: 'separate',
                borderSpacing: '0 0.5rem',
                padding: 0
              }}
            >
              <TableBody displayRowCheckbox={false} style={{ backgroundColor: colors.white }}>
                {this.state.history.map((record, index) => {
                  let icon;
                  switch (record.item.type) {
                    case 'collection':
                      icon = <Pathway />;
                      break;
                    case 'card':
                      icon = <SmartBite />;
                      break;
                    case 'video_stream':
                      icon = <Stream />;
                      break;
                    default:
                      icon = <SmartBite />;
                      break;
                  }
                  return (
                    <TableRow key={index} className="table-row" displayBorder={false}>
                      <TableRowColumn>
                        <ListItem
                          disabled
                          primaryText={
                            record.item.isDeleted ? (
                              <div className="text-overflow data-not-available-msg">
                                {tr('Deleted Content')}
                              </div>
                            ) : (
                              <a className="matte" href={record.item.url}>
                                <div className="text-overflow">{record.item.title || '\u00a0'}</div>
                              </a>
                            )
                          }
                          leftIcon={icon}
                        />
                      </TableRowColumn>
                      <TableRowColumn style={{ width: '9rem' }}>
                        <ListItem
                          disabled
                          primaryText={
                            <div className="text-right">
                              {tr('Score')} <small>{formatNumbers(record.score)}</small>
                            </div>
                          }
                        />
                      </TableRowColumn>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
        </div>
        {this.state.pending && (
          <div className="text-center">
            <Spinner />
          </div>
        )}
        {!this.state.isLastPage && !this.state.pending && (
          <div className="text-center">
            <SecondaryButton
              label={tr('View More')}
              className="viewMore"
              onTouchTap={this.viewMoreClickHandler}
            />
          </div>
        )}
        {/*Empty state*/
        this.state.history.length === 0 && !this.state.pending && (
          <TableRow className="table-row" displayBorder={false}>
            <TableRowColumn>
              <div className="text-center data-not-available-msg">
                {tr('You do not have any content history, visit the')}
                <a
                  onTouchTap={() => {
                    this.props.dispatch(push('/discover'));
                  }}
                >
                  {tr('discover page')}
                </a>{' '}
                {tr('to find new content.')}
              </div>
            </TableRowColumn>
          </TableRow>
        )}
      </div>
    );
  }
}

HistoryContainer.propTypes = {};

function mapStoreStateToProps(state) {
  return state;
}

export default connect(mapStoreStateToProps)(HistoryContainer);
