import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';

import Checkbox from 'material-ui/Checkbox';

import { channelsv2 } from 'edc-web-sdk/requests';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import CheckOn1 from 'edc-web-sdk/components/icons/CheckOn1'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox
import CheckOff from 'edc-web-sdk/components/icons/CheckOff'; // this is loaded from the edc-web-sdk repo & used to create a checked checkbox

import Spinner from '../../../common/spinner';
import Channelv2 from '../../../common/ChannelBlock';
import { updateChannels } from '../../../../actions/channelsActions';
import { openChannelEditModal } from '../../../../actions/modalActions';

import { Permissions } from '../../../../utils/checkPermissions';
import { getCustomCarousels, getCarousel } from 'edc-web-sdk/requests/carousels';

class MyChannelsContainerv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channelIds: [],
      pending: true,
      offset: 0,
      filterType: props.isMeTab ? 'my' : 'all',
      filterVersion: 'v1',
      searchQuery: '',
      aggs: {},
      selectedCurators: [],
      selectedFollowers: [],
      selectedCollaborators: [],
      isShowSuggestions: false,
      suggestions: [],
      carouselDisplayName: []
    };
    this.limit = 20;

    this.styles = {
      searchByName: {
        border: '1px solid #454560',
        marginLeft: '1.5625rem',
        width: '14rem',
        height: '2rem',
        marginTop: '1rem',
        color: '#454560'
      },
      searchIconByName: {
        position: 'absolute',
        bottom: '0.3125rem',
        verticalAlign: 'middle',
        width: '1.3rem',
        height: '1.3rem',
        fill: '#454560'
      },
      searchIcon: {
        position: 'absolute',
        bottom: '0.125rem',
        verticalAlign: 'middle',
        width: '1.2rem',
        height: '1.5rem',
        fill: colors.silverSand
      },
      checkboxInner: {
        width: '0.8125rem',
        height: '0.8125rem',
        marginRight: '0.25rem'
      },
      checkboxInnerLabel: {
        lineHeight: '2.17',
        fontSize: '0.75rem',
        fontWeight: '300',
        color: '#6f708b',
        marginTop: '1px'
      },
      checkboxOuter: {
        width: '1.1875rem',
        height: '1.1875rem',
        marginRight: '0.5rem',
        marginBottom: '0.25rem'
      },
      checkboxOuterLabel: {
        padding: '0.375rem 0',
        lineHeight: '1.4'
      },
      checkboxIconStyle: {
        width: '1.1875rem',
        height: '1.1875rem'
      },
      uncheckedIcon: {
        width: '1.3125rem',
        height: '1.3125rem'
      },
      uncheckedIcon_inner: {
        width: '0.8125rem',
        height: '0.8125rem'
      }
    };
    this.filterOptions = [
      { value: 'all', text: 'All Channels' },
      { value: 'my', text: 'My Channels' }
    ];

    if (props.isMeTab) {
      this.filterOptions = [{ value: 'my', text: 'my channels' }];
    }
    this.gettingChannelsCounter = 0;
  }

  componentDidMount() {
    let searchQuery =
      (this.props.location && this.props.location.query && this.props.location.query.q) || '';
    if (searchQuery) {
      this._inputFilter.value = searchQuery === '*' ? '' : searchQuery;
    }
    let filterType = this.props.isMeTab ? 'my' : 'all';
    if (this.props.location.pathname.includes('discover-channels')) {
      filterType = 'discover';
    } else {
      filterType =
        (this.props.routeParams && this.props.routeParams.filter) ||
        (this.props.isMeTab ? 'my' : 'all');
    }
    this.setState(
      {
        filterType: filterType,
        searchQuery,
        filterVersion: searchQuery ? 'v2' : 'v1'
      },
      () => {
        this.getData();
      }
    );
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    let searchQuery =
      (nextProps.location && nextProps.location.query && nextProps.location.query.q) || '';
    if (this._inputFilter) {
      this._inputFilter.value = searchQuery === '*' ? '' : searchQuery;
    }

    if (nextProps.routeParams.filter !== this.props.routeParams.filter) {
      this.setState(
        {
          filterType: nextProps.routeParams.filter,
          channelIds: [],
          offset: 0,
          filterVersion: searchQuery ? 'v2' : 'v1',
          searchQuery
        },
        () => {
          this.getData();
        }
      );
    } else if (this.state.searchQuery !== searchQuery && searchQuery) {
      this.setState(
        { searchQuery, channelIds: [], offset: 0, filterVersion: searchQuery ? 'v2' : 'v1' },
        () => {
          this.getData();
        }
      );
    } else if (
      nextProps.channels &&
      this.props.channels &&
      _.keys(nextProps.channels).length !== _.keys(this.props.channels).length
    ) {
      let nextChannelsIds = _.keys(nextProps.channels);
      if (
        nextChannelsIds.length &&
        nextProps.channels[nextChannelsIds[nextChannelsIds.length - 1]].isNewlyCreated
      ) {
        this.setState({ channelIds: [], offset: 0 }, () => {
          this.getData();
        });
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  getData = () => {
    let offset = this.state.offset;
    this.setState({ pending: true });
    let payload = {
      limit: this.limit,
      offset
    };
    if (this.state.filterType === 'my') {
      payload.is_following = true;
    } else if (this.state.filterType === 'search') {
      payload.q = this.state.searchQuery === '*' ? '' : this.state.searchQuery;
      payload.skip_aggs = false;
      if (this.state.selectedCurators.length) {
        payload['curators_names[]'] = this.state.selectedCurators;
      }
      if (this.state.selectedCollaborators.length) {
        payload['collaborators_names[]'] = this.state.selectedCollaborators;
      }

      if (this.state.selectedFollowers.length) {
        payload['followers_names[]'] = this.state.selectedFollowers;
      }
      if (this.state.isPrivate && this.state.isPublic) {
      } else {
        if (this.state.isPrivate) {
          payload.is_private = true;
        }

        if (this.state.isPublic) {
          payload.is_private = false;
        }
      }
    }

    if (this.state.filterType === 'my' || this.state.filterType === 'all') {
      payload.sort = 'created_at';
      payload.order_label = 'desc';
      payload.fields =
        'id,slug,label,description,followers_count,allow_follow,is_following,banner_image_urls,profile_image_url,is_private';

      channelsv2
        .fetchChannels(payload)
        .then(data => {
          let channelIds = [];
          let isLastPage = true;
          if (data && data.length) {
            offset = offset + data.length;
            channelIds = data.map(channel => channel.id + '');
            isLastPage = data.length < this.limit;

            this.props.dispatch(updateChannels(data));
          }
          this.setState({
            channelIds: _.uniq(this.state.channelIds.concat(channelIds)),
            pending: false,
            isLastPage: isLastPage,
            offset
          });
        })
        .catch(err => {
          console.error(`Error in MyChannelsContainer.getChannels.func : ${err}`);
        });
    } else if (this.state.filterType === 'discover') {
      let carousel_id = this.props.location.pathname.split('/').pop();

      getCarousel(carousel_id)
        .then(carousel => {
          if (carousel.enabled && carousel.slug == 'discover-channels') {
            let entity_payload = {
              filter_by_language: false,
              'structure_ids[]': carousel_id,
              channel_fields:
                'id,label,description,is_private,allow_follow,banner_image_urls,profile_image_url,profile_image_urls,is_following,updated_at,slug,followers_count'
            };
            this.setState({ carouselDisplayName: carousel.displayName });

            getCustomCarousels(entity_payload)
              .then(response => {
                let data = [];
                for (let key in response[carousel_id]) {
                  data.push(response[carousel_id][key].entity);
                }
                this.props.dispatch(updateChannels(data));
                this.setState({
                  channelIds: data.map(channel => channel.id + ''),
                  pending: false,
                  isLastPage: true
                });
              })
              .catch(error => {
                console.error(
                  'ERROR!!! in MyChannelsContainerv2.getData.getCustomCarousels',
                  error
                );
                this.setState({ channelIds: [], pending: false, isLastPage: true });
              });
          } else {
            this.setState({ channelIds: [], pending: false, isLastPage: true });
          }
        })
        .catch(error => {
          console.error('ERROR!!! in MyChannelsContainerv2.getData.getCarousel', error);
          this.setState({ channelIds: [], pending: false, isLastPage: true });
        });
    } else {
      channelsv2
        .getSearchChannels(payload)
        .then(data => {
          let channelIds = [];
          let isLastPage = true;
          let aggs = this.state.aggs;
          if (data && data.channels && data.channels.length) {
            offset = offset + data.channels.length;
            channelIds = data.channels.map(channel => channel.id + '');
            isLastPage = data.channels.length < this.limit;
            this.props.dispatch(updateChannels(data.channels));
          }
          aggs.collaborators =
            (data.aggs &&
              data.aggs.collaborators &&
              data.aggs.collaborators.filtered &&
              data.aggs.collaborators.filtered.values &&
              data.aggs.collaborators.filtered.values.buckets) ||
            [];
          aggs.curators =
            (data.aggs &&
              data.aggs.curators &&
              data.aggs.curators.filtered &&
              data.aggs.curators.filtered.values &&
              data.aggs.curators.filtered.values.buckets) ||
            [];
          aggs.followers =
            (data.aggs &&
              data.aggs.followers &&
              data.aggs.followers.filtered &&
              data.aggs.followers.filtered.values &&
              data.aggs.followers.filtered.values.buckets) ||
            [];
          this.setState({
            channelIds: _.uniq(this.state.channelIds.concat(channelIds)),
            pending: false,
            isLastPage: isLastPage,
            offset,
            aggs
          });
        })
        .catch(err => {
          console.error(`Error in MyChannelsContainer.getChannels.func : ${err}`);
        });
    }
  };

  handleScroll = _.throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.getData();
        }
      }
    },
    150,
    { leading: false }
  );

  handleOpenChannelCreation = () => {
    this.props.dispatch(openChannelEditModal());
  };

  handleFilterChange = value => {
    if (this.state.filterType !== value) {
      this.setState({ filterType: value, channelIds: [] }, () => {
        this.getData();
      });
    }
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      this.filterSearch();
    }
  };

  filterResults = type => {
    let value = this[`_${type}Input`].value.trim();
    this.setState({ [`${type}-filtered`]: !!value.length });
  };

  filterSearch = () => {
    let val = this._inputFilter.value;
    val = !val ? '*' : val;
    this.props.dispatch(push(`/channels/search?q=${val}`));
  };

  filterChangeHandler = filter => {
    this.props.dispatch(
      push(this.props.isMeTab ? `/me/channels/${filter}` : `/channels/${filter}`)
    );
  };

  openEditModal = channel => {
    this.props.dispatch(openChannelEditModal(channel));
  };

  handleChangeType = (event, isCheck, field) => {
    this.setState({ [field]: isCheck, offset: 0, channelIds: [] }, () => {
      this.getData();
    });
  };

  handleChange = (event, isCheck, field, value) => {
    let arr = this.state[field];
    if (isCheck) {
      arr.push(value);
    } else {
      arr = arr.filter(el => el !== value);
    }
    this.setState({ [field]: arr, offset: 0, channelIds: [] }, () => {
      this.getData();
    });
  };

  showAll = block => {
    this.setState({ [`show-all-${block}`]: true });
  };

  toggleShowingSuggestions = (e, clearSuggestions) => {
    this.setState(
      prevState => {
        let newState = { isShowSuggestions: !prevState.isShowSuggestions };
        if (clearSuggestions) newState = { ...newState, ...{ suggestions: [] } };
        return newState;
      },
      () => {
        if (this.state.isShowSuggestions) {
          document.body.addEventListener('click', this.hideSuggsContainer);
        } else {
          document.body.removeEventListener('click', this.hideSuggsContainer);
        }
      }
    );
  };

  hideSuggsContainer = e => {
    if (
      e &&
      e.target &&
      e.target.className &&
      (e.target.className === 'suggestion-item' || e.target.className === 'search-channels-input')
    )
      return;
    this.toggleShowingSuggestions();
  };

  getSuggestions = _.debounce(
    () => {
      let requestNumber = ++this.gettingChannelsCounter;
      let currentSearchText = this._inputFilter.value && this._inputFilter.value.trim();
      if (!currentSearchText) {
        return this.setState({
          isShowSuggestions: false,
          suggestions: []
        });
      }
      channelsv2
        .getChannelSuggest(currentSearchText)
        .then(data => {
          if (requestNumber !== this.gettingChannelsCounter) return;
          if (data) {
            this.setState({
              suggestions: data,
              isShowSuggestions: !!data.length
            });
          }
        })
        .catch(err => {
          console.error(`Error in MyChannelsContainer.getSuggestions.func : ${err}`);
        });
    },
    100,
    { leading: false, trailing: true }
  );

  chooseSuggestion = channel => {
    this._inputFilter.value = channel && channel.text;
    this.toggleShowingSuggestions(null, true);
    this.filterSearch();
  };

  render() {
    let curatorsExtraBlock = false;
    let curators = (this.state.aggs && this.state.aggs.curators) || [];
    let collaborators = (this.state.aggs && this.state.aggs.collaborators) || [];
    let followers = (this.state.aggs && this.state.aggs.followers) || [];
    let collaboratorsExtraBlock = false;
    let followersExtraBlock = false;

    let curatorsKeys = curators.map(el => el.key) || [];
    let filteredCurators = this.state.selectedCurators.filter(el => !~curatorsKeys.indexOf(el));

    let collaboratorsKeys = collaborators.map(el => el.key) || [];
    let filteredCollaborators = this.state.selectedCollaborators.filter(
      el => !~collaboratorsKeys.indexOf(el)
    );

    let followersKeys = followers.map(el => el.key) || [];
    let filteredFollowers = this.state.selectedFollowers.filter(el => !~followersKeys.indexOf(el));

    if (
      curators &&
      this.state['curators-filtered'] &&
      this._curatorsInput &&
      this._curatorsInput.value
    ) {
      curators = curators.filter(
        el => !!~el.key.toLowerCase().indexOf(this._curatorsInput.value.toLowerCase().trim())
      );
    }
    if (curators.length > 4 || this.state['curators-filtered']) {
      curatorsExtraBlock = true;
      if (!this.state['show-all-curators']) {
        curators = curators.slice(0, 4);
      }
    }

    if (
      this.state['collaborators-filtered'] &&
      this._collaboratorsInput &&
      this._collaboratorsInput.value
    ) {
      collaborators = collaborators.filter(
        el => !!~el.key.toLowerCase().indexOf(this._collaboratorsInput.value.toLowerCase().trim())
      );
    }
    if (collaborators.length > 4 || this.state['collaborators-filtered']) {
      collaboratorsExtraBlock = true;
      if (!this.state['show-all-collaborators']) {
        collaborators = collaborators.slice(0, 4);
      }
    }

    if (this.state['followers-filtered'] && this._followersInput && this._followersInput.value) {
      followers = followers.filter(
        el => !!~el.key.toLowerCase().indexOf(this._followersInput.value.toLowerCase().trim())
      );
    }

    if (followers.length > 4 || this.state['followers-filtered']) {
      followersExtraBlock = true;
      if (!this.state['show-all-followers']) {
        followers = followers.slice(0, 4);
      }
    }

    const isMeTab = this.props.isMeTab || false;
    return (
      <div id="channels" className="channelsv2 home-new-ui lbv2_new">
        <div className="lbv2__title channelv2__title">
          <div>
            {!isMeTab && (
              <button
                className="breadcrumbBack"
                onClick={() => {
                  window.history.back();
                }}
              >
                <span aria-label="back" tabIndex={-1} className="hideOutline">
                  <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
                </span>
              </button>
            )}
            {!isMeTab && (
              <span>
                {this.state.filterType == 'discover'
                  ? `${tr(this.state.carouselDisplayName)}`
                  : `${tr('Channels')}`}
              </span>
            )}
          </div>
          {!isMeTab && (
            <div className="my-team__search-bar">
              {this.state.filterType != 'discover' && (
                <div className="my-team__search-input-block">
                  <input
                    placeholder={tr('Search')}
                    onFocus={this.toggleShowingSuggestions}
                    className="search-channels-input"
                    onChange={this.getSuggestions}
                    onKeyDown={this.handleFilterSearch}
                    type="text"
                    ref={node => (this._inputFilter = node)}
                  />
                  <SearchIcon style={this.styles.searchIcon} onClick={this.filterSearch} />
                  {this.state.isShowSuggestions &&
                    this.state.suggestions &&
                    !!this.state.suggestions.length && (
                      <div className="channels-suggestions-container">
                        <ul>
                          {this.state.suggestions.map(suggestion => (
                            <li
                              key={`suggestion-${suggestion.id}`}
                              className="suggestion-item"
                              onClick={this.chooseSuggestion.bind(this, suggestion)}
                            >
                              {suggestion.text}
                            </li>
                          ))}
                        </ul>
                      </div>
                    )}
                </div>
              )}

              {Permissions.has('CREATE_CHANNEL') && (
                <PrimaryButton
                  onClick={this.handleOpenChannelCreation}
                  label={tr('Create Channel')}
                />
              )}
            </div>
          )}
        </div>

        <div className="my-team__container">
          {this.state.filterType !== 'discover' && (
            <div
              className={`my-team__filter-block ${
                this.state.filterVersion === 'v2' ? 'my-team__filter-block_wide' : ''
              }`}
            >
              {this.state.filterVersion === 'v1' ? (
                this.filterOptions.map((opt, index) => {
                  return (
                    <div
                      className={`my-team__filter-item ${
                        this.state.filterType === opt.value ? 'my-team__filter-item_active' : ''
                      }`}
                      key={`filter_${index}`}
                      tabIndex={0}
                      onClick={() => this.filterChangeHandler(opt.value)}
                    >
                      {tr(opt.text)}
                    </div>
                  );
                })
              ) : (
                <div>
                  <div className="filter-block">
                    <div className="filter-title">{tr('Type')}</div>
                    <div className="filter-content">
                      <div className="outer-checkbox">
                        <Checkbox
                          label={`${tr('Private')}`}
                          checkedIcon={<CheckOn1 color="#6f708b" />}
                          uncheckedIcon={
                            <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                          }
                          iconStyle={this.styles.checkboxOuter}
                          labelStyle={this.styles.checkboxOuterLabel}
                          onCheck={(event, isInputChecked) =>
                            this.handleChangeType(event, isInputChecked, 'isPrivate')
                          }
                          value={'open'}
                          checked={this.state.isPrivate}
                        />
                      </div>
                      <div className="outer-checkbox">
                        <Checkbox
                          label={`${tr('Public')}`}
                          checkedIcon={<CheckOn1 color="#6f708b" />}
                          uncheckedIcon={
                            <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                          }
                          iconStyle={this.styles.checkboxOuter}
                          labelStyle={this.styles.checkboxOuterLabel}
                          onCheck={(event, isInputChecked) =>
                            this.handleChangeType(event, isInputChecked, 'isPublic')
                          }
                          value={'private'}
                          checked={this.state.isPublic}
                        />
                      </div>
                    </div>
                  </div>
                  {(!!curators.length ||
                    !!this.state.selectedCurators.length ||
                    (this._curatorsInput && this._curatorsInput.value)) && (
                    <div className="filter-block">
                      <div className="filter-title">{tr('Curator')}</div>
                      <div className="filter-content">
                        {curators.map((curator, index) => {
                          return (
                            <div className="outer-checkbox" key={`curator-${index}`}>
                              <Checkbox
                                label={tr(`${curator.key}(${curator.doc_count})`)}
                                checkedIcon={<CheckOn1 color="#6f708b" />}
                                uncheckedIcon={
                                  <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                }
                                iconStyle={this.styles.checkboxOuter}
                                labelStyle={this.styles.checkboxOuterLabel}
                                onCheck={(event, isInputChecked) =>
                                  this.handleChange(
                                    event,
                                    isInputChecked,
                                    'selectedCurators',
                                    curator.key
                                  )
                                }
                                value={this.state.selectedCurators[curator.key]}
                                checked={!!~this.state.selectedCurators.indexOf(curator.key)}
                              />
                            </div>
                          );
                        })}
                        {!!filteredCurators &&
                          filteredCurators.map((curator, index) => {
                            return (
                              <div className="outer-checkbox" key={`curator-null-${index}`}>
                                <Checkbox
                                  label={tr(`${curator}(0)`)}
                                  checkedIcon={<CheckOn1 color="#6f708b" />}
                                  uncheckedIcon={
                                    <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                  }
                                  iconStyle={this.styles.checkboxOuter}
                                  labelStyle={this.styles.checkboxOuterLabel}
                                  onCheck={(event, isInputChecked) =>
                                    this.handleChange(
                                      event,
                                      isInputChecked,
                                      'selectedCurators',
                                      curator
                                    )
                                  }
                                  value={this.state.selectedCurators[curator]}
                                  checked={!!~this.state.selectedCurators.indexOf(curator)}
                                />
                              </div>
                            );
                          })}
                        {curatorsExtraBlock && (
                          <div>
                            <div
                              className="my-team__search-input-block"
                              style={this.styles.searchByName}
                            >
                              <SearchIcon
                                style={this.styles.searchIconByName}
                                onClick={this.filterResults.bind(this, 'curators')}
                              />
                              <input
                                placeholder={tr('By name')}
                                onKeyDown={this.filterResults.bind(this, 'curators')}
                                type="text"
                                ref={node => (this._curatorsInput = node)}
                                style={{ paddingLeft: '1.5rem' }}
                              />
                            </div>
                            <div
                              style={{ marginTop: '1rem' }}
                              className="view-more"
                              onClick={this.showAll.bind(this, 'curators')}
                            >
                              {tr('View all')}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {(!!collaborators.length ||
                    !!this.state.selectedCollaborators.length ||
                    (this._collaboratorsInput && this._collaboratorsInput.value)) && (
                    <div className="filter-block">
                      <div className="filter-title">{tr('Collaborators')}</div>
                      <div className="filter-content">
                        {collaborators.map((collaborator, index) => {
                          return (
                            <div className="outer-checkbox" key={`collaborator-${index}`}>
                              <Checkbox
                                label={tr(`${collaborator.key}(${collaborator.doc_count})`)}
                                checkedIcon={<CheckOn1 color="#6f708b" />}
                                uncheckedIcon={
                                  <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                }
                                iconStyle={this.styles.checkboxOuter}
                                labelStyle={this.styles.checkboxOuterLabel}
                                onCheck={(event, isInputChecked) =>
                                  this.handleChange(
                                    event,
                                    isInputChecked,
                                    'selectedCollaborators',
                                    collaborator.key
                                  )
                                }
                                value={this.state.selectedCollaborators[collaborator.key]}
                                checked={
                                  !!~this.state.selectedCollaborators.indexOf(collaborator.key)
                                }
                              />
                            </div>
                          );
                        })}
                        {!!filteredCollaborators &&
                          filteredCollaborators.map((collaborator, index) => {
                            return (
                              <div className="outer-checkbox" key={`collaborator-null-${index}`}>
                                <Checkbox
                                  label={tr(`${collaborator}(0)`)}
                                  checkedIcon={<CheckOn1 color="#6f708b" />}
                                  uncheckedIcon={
                                    <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                  }
                                  iconStyle={this.styles.checkboxOuter}
                                  labelStyle={this.styles.checkboxOuterLabel}
                                  onCheck={(event, isInputChecked) =>
                                    this.handleChange(
                                      event,
                                      isInputChecked,
                                      'selectedCollaborators',
                                      collaborator
                                    )
                                  }
                                  value={this.state.selectedCollaborators[collaborator]}
                                  checked={
                                    !!~this.state.selectedCollaborators.indexOf(collaborator)
                                  }
                                />
                              </div>
                            );
                          })}
                        {collaboratorsExtraBlock && (
                          <div>
                            <div
                              className="my-team__search-input-block"
                              style={this.styles.searchByName}
                            >
                              <SearchIcon
                                style={this.styles.searchIconByName}
                                onClick={this.filterResults.bind(this, 'collaborators')}
                              />
                              <input
                                placeholder={tr('By name')}
                                onKeyDown={this.filterResults.bind(this, 'collaborators')}
                                type="text"
                                ref={node => (this._collaboratorsInput = node)}
                                style={{ paddingLeft: '1.5rem' }}
                              />
                            </div>
                            <div
                              style={{ marginTop: '1rem' }}
                              className="view-more"
                              onClick={this.showAll.bind(this, 'collaborators')}
                            >
                              {tr('View all')}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {(!!followers.length ||
                    !!this.state.selectedFollowers.length ||
                    (this._followersInput && this._followersInput.value)) && (
                    <div className="filter-block">
                      <div className="filter-title">{tr('Channel Followers')}</div>
                      <div className="filter-content">
                        {followers.map((follower, index) => {
                          return (
                            <div className="outer-checkbox" key={`follower-${index}`}>
                              <Checkbox
                                label={tr(`${follower.key}(${follower.doc_count})`)}
                                checkedIcon={<CheckOn1 color="#6f708b" />}
                                uncheckedIcon={
                                  <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                }
                                iconStyle={this.styles.checkboxOuter}
                                labelStyle={this.styles.checkboxOuterLabel}
                                onCheck={(event, isInputChecked) =>
                                  this.handleChange(
                                    event,
                                    isInputChecked,
                                    'selectedFollowers',
                                    follower.key
                                  )
                                }
                                value={this.state.selectedFollowers[follower.key]}
                                checked={!!~this.state.selectedFollowers.indexOf(follower.key)}
                              />
                            </div>
                          );
                        })}

                        {!!filteredFollowers &&
                          filteredFollowers.map((follower, index) => {
                            return (
                              <div className="outer-checkbox" key={`follower-${index}`}>
                                <Checkbox
                                  label={tr(`${follower}(0)`)}
                                  checkedIcon={<CheckOn1 color="#6f708b" />}
                                  uncheckedIcon={
                                    <CheckOff style={this.styles.uncheckedIcon} color="#6f708b" />
                                  }
                                  iconStyle={this.styles.checkboxOuter}
                                  labelStyle={this.styles.checkboxOuterLabel}
                                  onCheck={(event, isInputChecked) =>
                                    this.handleChange(
                                      event,
                                      isInputChecked,
                                      'selectedFollowers',
                                      follower
                                    )
                                  }
                                  value={this.state.selectedFollowers[follower]}
                                  checked={!!~this.state.selectedFollowers.indexOf(follower)}
                                />
                              </div>
                            );
                          })}
                        {followersExtraBlock && (
                          <div>
                            <div
                              className="my-team__search-input-block"
                              style={this.styles.searchByName}
                            >
                              <SearchIcon
                                style={this.styles.searchIconByName}
                                onClick={this.filterResults.bind(this, 'followers')}
                              />
                              <input
                                placeholder={tr('By name')}
                                onKeyDown={this.filterResults.bind(this, 'followers')}
                                type="text"
                                ref={node => (this._followersInput = node)}
                                style={{ paddingLeft: '1.5rem' }}
                              />
                            </div>
                            <div
                              style={{ marginTop: '1rem' }}
                              className="view-more"
                              onClick={this.showAll.bind(this, 'followers')}
                            >
                              {tr('View all')}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                </div>
              )}
            </div>
          )}
          <div
            className={
              this.state.filterVersion === 'v2' ? 'my-content-wrapper' : 'my-content-wrapper-v1'
            }
          >
            <div className="channel-container">
              {!!this.state.channelIds.length &&
                this.state.channelIds.map(channelId => {
                  let channel = this.props.channels[channelId];
                  return (
                    <Channelv2
                      key={`channel_${channelId}`}
                      openForEdit={() => this.openEditModal(channel)}
                      channel={channel}
                    />
                  );
                })}
              {!this.state.channelIds.length && !this.state.pending && (
                <div className="channels-not-found">{tr('No results found')}.</div>
              )}
              {this.state.pending && (
                <div className="channel-loading">
                  <Spinner />
                </div>
              )}
            </div>
            {isMeTab && this.state.isLastPage && (
              <p className="explore-msg-channel-group-user text-center">
                <a
                  onClick={() => {
                    this.props.dispatch(push('/channels'));
                  }}
                >
                  <strong>Search</strong>
                </a>{' '}
                other channel to explore and follow
              </p>
            )}
          </div>
        </div>
      </div>
    );
  }
}

MyChannelsContainerv2.propTypes = {
  channels: PropTypes.object,
  team: PropTypes.object,
  location: PropTypes.any,
  routeParams: PropTypes.any,
  isMeTab: PropTypes.bool
};

export default connect(state => ({ channels: state.channels.toJS(), team: state.team.toJS() }))(
  MyChannelsContainerv2
);
