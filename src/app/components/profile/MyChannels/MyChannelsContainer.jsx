import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ListView from '../../common/ListView';
import Breadcrumb from '../../common/Breadcrumb';
import ChannelListItem from '../../common/ChannelListItem';
import { updateChannels } from '../../../actions/channelsActions';
import { channelsv2 } from 'edc-web-sdk/requests';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';

class MyChannelsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      channelIds: [],
      pending: true,
      limit: 20
    };

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreChannels = this.showMoreChannels.bind(this);
    this.fields =
      'id,slug,label,description,followers_count,allow_follow,is_following,banner_image_urls,profile_image_url,is_private';
  }

  componentDidMount() {
    let payload = {
      limit: this.state.limit,
      offset: 0,
      fields: this.fields
    };
    channelsv2
      .fetchChannels(payload)
      .then(data => {
        this.props.dispatch(updateChannels(data));
        this.setState({ channelIds: data.map(channel => channel.id + ''), pending: false });
      })
      .catch(err => {
        console.error(`Error in MyChannelsContainer.getFollowingChannels.func : ${err}`);
      });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreChannels();
        }
      }
    },
    150,
    { leading: false }
  );

  showMoreChannels = () => {
    let offset = this.state.channelIds.length;
    let payload = {
      limit: this.state.limit,
      offset,
      fields: this.fields
    };
    this.setState({ pending: true });
    channelsv2
      .fetchChannels(payload)
      .then(data => {
        let channelIds = data.map(channel => channel.id + '');
        let isLastPage = data.length < this.state.limit;
        this.props.dispatch(updateChannels(data));
        this.setState({
          channelIds: uniq(this.state.channelIds.concat(channelIds), 'id'),
          pending: false,
          isLastPage: isLastPage
        });
      })
      .catch(err => {
        console.error(`Error in MyChannelsContainer.getFollowingChannels.func : ${err}`);
      });
  };

  render() {
    return (
      <div>
        <div className="row content" id="channels">
          <div className="columns expand vertical-spacing-large">
            <div className="vertical-spacing-medium">
              <ListView
                isTable={true}
                pending={this.state.pending}
                emptyMessage={
                  <div className="data-not-available-msg">
                    {tr('You are not following any channels.')}
                  </div>
                }
              >
                {this.state.channelIds.map((channelId, index) => {
                  let channel = this.props.channels[channelId];
                  let orgPrimaryColor =
                    this.props.team.config && this.props.team.config.orgPrimaryColor;
                  return (
                    <ChannelListItem
                      key={index}
                      channel={channel}
                      orgPrimaryColor={orgPrimaryColor}
                      isPrivate={channel.isPrivate}
                    />
                  );
                })}
              </ListView>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MyChannelsContainer.propTypes = {
  channels: PropTypes.object,
  team: PropTypes.object
};

export default connect(state => ({ channels: state.channels.toJS(), team: state.team.toJS() }))(
  MyChannelsContainer
);
