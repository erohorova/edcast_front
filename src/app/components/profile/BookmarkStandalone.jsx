import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import { push } from 'react-router-redux';
import { getLearningQueueItems } from 'edc-web-sdk/requests/feed';
import throttle from 'lodash/throttle';
import { tr } from 'edc-web-sdk/helpers/translations';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import More from 'edc-web-sdk/components/icons/More';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import { unBookmark } from 'edc-web-sdk/requests/cards';
import { pinCard, unpinCard } from 'edc-web-sdk/requests/channels.v2';
import SvgImageResized from '../common/ImageResized';
import { openUnbookmarkModal } from '../../actions/modalActions';
import Featured from 'edc-web-sdk/components/icons/Featured';
import Spinner from '../common/spinner';
import linkPrefix from '../../utils/linkPrefix';
import convertRichText from '../../utils/convertRichText';
const RichTextReadOnly = Loadable({
  loader: () => import('../common/RichTextReadOnly'),
  loading: () => null
});
class BookmarkStandalone extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      bookmarked: [],
      pending: false,
      isLastPage: false,
      isOpen: false,
      query: '',
      isFeatured: ''
    };
    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.2rem',
        left: '0.6rem',
        top: '0.4rem',
        verticalAlign: 'middle',
        width: '1.125rem',
        height: '1.125rem',
        fill: '#454560'
      },
      closeIcon: {
        paddingRight: 0,
        width: 'auto'
      },
      svgStyle: {
        width: '5.375rem',
        marginTop: '0.375rem',
        height: '3.8125rem',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
      },
      insightDropDownActions: {
        paddingRight: '0.3125rem',
        paddingLeft: '0',
        width: 'auto'
      },
      moreIconStyle: {
        height: '0.875rem',
        top: '0.25rem',
        position: 'relative'
      },
      iconMenu: {
        backgroundColor: '#000'
      },
      menuItem: {
        fontFamily: 'Open Sans, sans-serif',
        fontSize: '0.875rem',
        fontWeight: '300',
        lineHeight: '1.67',
        letterSpacing: '0.5px',
        textAlign: 'left',
        color: '#ffffff',
        minHeight: '1.625rem',
        backgroundColor: '#000',
        width: '6.9375rem'
      }
    };
    this.limit = 10;
    //this.fields = 'id,deepLinkId,display_type,queueable,bookmarkId'
    //TODO uncomment this.fields after EP-19905 merge
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: this.state.bookmarked.length,
      'source[]': ['bookmarks'],
      'state[]': []
      //'fields': this.fields
    };
    this.getBookmarks(payload);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  getBookmarks = payload => {
    getLearningQueueItems(payload)
      .then(response => {
        let bookmarked = response.learningQueueItems;
        let isFeatured =
          bookmarked.length && response.pinnableCardId === bookmarked[0].deepLinkId
            ? bookmarked[0].deepLinkId
            : '';
        this.setState({ bookmarked, isFeatured });
      })
      .catch(err => {
        console.error(`Error in BookmarkStandalone.fetchCards.func : ${err}`);
      });
  };

  showMoreCards() {
    this.setState({ pending: true }, () => {
      getLearningQueueItems({
        limit: this.limit,
        offset: this.state.bookmarked.length,
        'source[]': ['bookmarks'],
        'state[]': []
        //'fields': this.fields
      })
        .then(data => {
          let bookmarked = data.learningQueueItems;
          let isLastPage = bookmarked.length < this.limit;
          this.setState({
            pending: false,
            isLastPage: isLastPage,
            bookmarked: [...this.state.bookmarked, ...bookmarked]
          });
        })
        .catch(err => {
          console.error(
            `Error in BookmarkStandalone.showMoreCards.getLearningQueueItems.func : ${err}`
          );
        });
    });
  }

  confirmHandler = bookmarked => {
    this.setState({ pending: true });
    unBookmark(bookmarked.queueable.id)
      .then(() => {
        let filterBookmark = this.state.bookmarked.filter(
          item => item.queueable.id !== bookmarked.queueable.id
        );
        this.setState({ bookmarked: filterBookmark, pending: false });
      })
      .catch(err => {
        this.setState({ pending: false });
        console.error(`Error in BookmarkStandalone.bookmarkClickHandler.func : ${err}`);
      });
  };

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreCards();
        }
      }
    },
    150,
    { leading: false }
  );

  unbookmarkClickHandler = bookmarked => {
    this.props.dispatch(openUnbookmarkModal(bookmarked, this.confirmHandler));
  };

  async featuredClickHandler(bookmarked, payload) {
    if (this.state.isFeatured) {
      await unpinCard({
        object_id: this.state.bookmarked[0].deepLinkId,
        object_type: 'Card',
        pinnable_id: this.state.bookmarked[0].bookmarkId,
        pinnable_type: 'Bookmark'
      }).catch(err => {
        console.error(`Error in BookmarkStandalone.unpinCard.func : ${err}`);
      });
    }
    await pinCard({
      object_id: bookmarked.deepLinkId,
      object_type: 'Card',
      pinnable_id: bookmarked.bookmarkId,
      pinnable_type: 'Bookmark'
    })
      .then(() => {
        this.getBookmarks(payload);
      })
      .catch(err => {
        console.error(`Error in BookmarkStandalone.pinCard.func : ${err}`);
      });
  }

  onClickFeature = bookmarked => {
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: 0,
      'source[]': ['bookmarks'],
      'state[]': []
    };
    if (this.state.query !== '*' && this.state.query !== '') {
      payload['query'] = this.state.query;
    }
    this.state.isFeatured === bookmarked.deepLinkId
      ? this.unfeaturedClickHandler(bookmarked, payload)
      : this.featuredClickHandler(bookmarked, payload);
  };

  unfeaturedClickHandler = (bookmarked, payload) => {
    unpinCard({
      object_id: bookmarked.deepLinkId,
      object_type: 'Card',
      pinnable_id: bookmarked.bookmarkId,
      pinnable_type: 'Bookmark'
    })
      .then(() => {
        this.getBookmarks(payload);
      })
      .catch(err => {
        console.error(`Error in BookmarkStandalone.unfeaturedClickHandler.func : ${err}`);
      });
  };

  searchEnterHandler = e => {
    if (e.keyCode === 13) {
      let query = e.target.value.trim();
      this.searchContent(query);
    }
  };

  searchContent = query => {
    this.setState({ query });
    let payload = {
      include_queueable_details: true,
      limit: this.limit,
      offset: 0,
      'source[]': ['bookmarks'],
      'state[]': []
      //'fields': this.fields
    };
    if (query !== '*') {
      payload['query'] = query;
    }
    this.getBookmarks(payload);
  };

  handleClickBookmark = (e, bookmarked) => {
    e && e.stopPropagation();
    this.props.dispatch(
      push(`/${linkPrefix(bookmarked.queueable.cardType)}/${bookmarked.queueable.slug}`)
    );
  };

  render() {
    return (
      <div className="channelsv2 home-new-ui lbv2_new">
        <div className="bookmarks__header channelv2__title" style={{ fontSize: '1rem' }}>
          <div>
            <button
              className="breadcrumbBack"
              onClick={() => {
                window.history.back();
              }}
            >
              <span aria-label="back" tabIndex={-1} className="hideOutline">
                <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
              </span>
            </button>
            {tr('Bookmarks')}
          </div>
          <div>
            <div className="my-team__search-input-block bookmarked-search">
              <SearchIcon style={this.styles.searchIcon} color={'#454560'} viewBox="0 0 24 24" />
              <input
                placeholder={tr('Search')}
                className="search-channels-input"
                type="text"
                onKeyUp={this.searchEnterHandler.bind(this)}
                ref={node => (this._inputFilter = node)}
              />
            </div>
          </div>
        </div>
        <div>
          {this.state.bookmarked.length ? (
            <table className="table-bookmarked">
              <thead className="table-bookmarked__header">
                <tr>
                  <td className="table-bookmarked__cell table-bookmarked__cell_header">
                    {tr('Title')}
                  </td>
                  <td className="table-bookmarked__cell table-bookmarked__cell_header">
                    {tr('Duration')}
                  </td>
                  <td className="table-bookmarked__cell table-bookmarked__cell_header">
                    {tr('Type')}
                  </td>
                  <td className="table-bookmarked__cell table-bookmarked__cell_header">
                    {tr('Price')}
                  </td>
                  <td className="table-bookmarked__cell table-bookmarked__cell_header" />
                </tr>
              </thead>
              <tbody>
                {this.state.bookmarked.map(bookmarked => {
                  let type =
                    bookmarked.queueable.cardType === 'media'
                      ? bookmarked.displayType[0] &&
                        bookmarked.displayType[0].toUpperCase() + bookmarked.displayType.slice(1)
                      : bookmarked.queueable.cardType[0] &&
                        bookmarked.queueable.cardType[0].toUpperCase() +
                          bookmarked.queueable.cardType.slice(1);
                  if (type === 'Pack') {
                    type = 'Pathway';
                  }
                  return (
                    <tr key={bookmarked.queueable.id} className="row-bookmarked">
                      <td
                        className="table-bookmarked__cell"
                        onClick={e => this.handleClickBookmark(e, bookmarked)}
                      >
                        <div className="bookmark__info">
                          <div className="card-img button-icon card-blurred-background">
                            <svg style={this.styles.svgStyle}>
                              <SvgImageResized
                                cardId={`${bookmarked.queueable.id}`}
                                resizeOptions={'height:180'}
                                xlinkHref={
                                  (bookmarked.queueable.filestack.length &&
                                    bookmarked.queueable.filestack[0].url) ||
                                  (bookmarked.queueable.resource &&
                                    bookmarked.queueable.resource.imageUrl) ||
                                  '/i/images/bookmark.png'
                                }
                                width="100%"
                                height="100%"
                              />
                              <filter id={`${bookmarked.queueable.id}-blur-effect-bookmark`}>
                                <feGaussianBlur stdDeviation="10" />
                              </filter>
                            </svg>
                          </div>
                          <div className="bookmarked__title">
                            <RichTextReadOnly
                              text={tr(
                                convertRichText(
                                  `${bookmarked.queueable.title || bookmarked.queueable.message}`
                                )
                              )}
                            />
                            <br />
                            {(bookmarked.queueable.eclSourceLogoUrl ||
                              bookmarked.queueable.providerImage) && (
                              <img
                                src={
                                  bookmarked.queueable.eclSourceLogoUrl ||
                                  bookmarked.queueable.providerImage
                                }
                                style={{ height: '1.125rem' }}
                              />
                            )}
                            <div className="bookmark__featured">
                              {this.state.isFeatured === bookmarked.deepLinkId && (
                                <div>
                                  <Featured color="#454560" /> {tr('Featured')}{' '}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>
                      </td>
                      <td className="table-bookmarked__cell">
                        {tr(
                          `${(bookmarked.queueable.eclDurationMetadata &&
                            bookmarked.queueable.eclDurationMetadata.calculated_duration &&
                            bookmarked.queueable.eclDurationMetadata.calculated_duration_display) ||
                            ''}`
                        )}
                      </td>
                      <td className="table-bookmarked__cell">
                        {tr(
                          `${(bookmarked.queueable.resource &&
                            bookmarked.queueable.resource.type) ||
                            bookmarked.queueable.readableCardType ||
                            (bookmarked.queueable.cardType && type)}`
                        )}
                      </td>
                      <td className="table-bookmarked__cell">
                        {tr(
                          `${
                            bookmarked.queueable.prices.length
                              ? `${bookmarked.queueable.prices[0].symbol}${
                                  bookmarked.queueable.prices[0].amount
                                }`
                              : 'Free'
                          }`
                        )}
                      </td>
                      <td className="table-bookmarked__cell">
                        <IconMenu
                          className="vertical-insight-dropdown"
                          iconButtonElement={
                            <IconButton aria-label="view options">
                              <More color={colors.gray} />
                            </IconButton>
                          }
                          anchorOrigin={{ horizontal: 'middle', vertical: 'bottom' }}
                          targetOrigin={{ horizontal: 'middle', vertical: 'top' }}
                          listStyle={this.styles.iconMenu}
                          menuStyle={this.styles.menuItem}
                          animated={false}
                        >
                          <MenuItem
                            style={this.styles.menuItem}
                            primaryText={
                              this.state.isFeatured === bookmarked.deepLinkId
                                ? tr('Unfeature')
                                : tr('Make Featured')
                            }
                            className="bookmark bookmark-standalone iconmenu-style-ontab show-outline-iconmenu"
                            onTouchTap={this.onClickFeature.bind(this, bookmarked)}
                          />
                          <MenuItem
                            style={this.styles.menuItem}
                            primaryText={tr('Unbookmark')}
                            className="bookmark bookmark-standalone iconmenu-style-ontab show-outline-iconmenu"
                            onTouchTap={this.unbookmarkClickHandler.bind(this, bookmarked)}
                          />
                        </IconMenu>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          ) : (
            <div style={{ display: 'flex', justifyContent: 'center' }}>{tr('No content')}</div>
          )}
          {this.state.pending && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(BookmarkStandalone);
