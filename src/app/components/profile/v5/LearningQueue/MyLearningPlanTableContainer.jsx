import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import SearchIcon from 'edc-web-sdk/components/icons/Search';

import AddIcon from 'material-ui/svg-icons/content/add';
import { SecondaryButton } from 'edc-web-sdk/components';
import MyLearningPlanContentContainer from './MyLearningPlanContentContainer';
import {
  contentTypeTabChange,
  searchPlan,
  changeCurrentYearObj,
  changeCurrentYear
} from '../../../../actions/mylearningplanV5Actions';
import Spinner from '../../../common/spinner';
import Select from 'react-select';
import { getSpecificUserInfo } from '../../../../actions/currentUserActions';
import moment from 'moment';

class MyLearningPlanTableContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    let currentYear = new Date().getFullYear();
    this.filterOptions = [];

    this.state = {
      activeContentTab: 'quarter',
      contentTypeTab: [
        { value: 'quarter', displayName: 'Quarter' },
        { value: 'competency', displayName: 'Skills' }
      ],
      currentOption: { value: currentYear + '', label: currentYear + '' },
      loadingData: true,
      mlpSections: {},
      isShowSearch: window.ldclient.variation('search-in-my-learning-plan', 'false'),
      showCompetency: window.ldclient.variation('enable-competency'),
      enableCustomQuarters:
        this.props.team && this.props.team.config && this.props.team.config.enable_custom_quarters,
      isSearching: false
    };

    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.25rem',
        verticalAlign: 'middle',
        width: '1.25rem',
        height: '1.25rem',
        fill: '#6f708b'
      }
    };
  }

  componentDidMount = async () => {
    let currentUser = await this.props.currentUser;
    this.props
      .dispatch(getSpecificUserInfo(['onboardingCompletedDate'], currentUser))
      .then(user => {
        let currentYear = new Date().getFullYear();
        let joiningDate = moment(this.props.currentUser.onboardingCompletedDate).format('YYYY');
        let diff = currentYear - parseInt(joiningDate) + 1;
        for (let a = currentYear; a > currentYear - diff; a--) {
          this.filterOptions.push({ value: a + '', label: a + '', visible: true });
        }
      })
      .catch(err => {
        console.error(`MyLearningPlanTableContainer.componentDidMount.getSpecificUserInfo`);
      });
  };

  componentWillReceiveProps(nextProps) {
    let mlpData = nextProps.mlpv5;
    let activeContentTab = mlpData.activeContentTab;
    let activeContentTypeTab = mlpData.activeContentTypeTab;
    let competencyDataLength = Object.keys(mlpData[activeContentTab].competency).length;
    let quarterDataLength = Object.keys(mlpData[activeContentTab].quarter).length;
    let userInterestLength = mlpData.userInterest.length;
    this.setState({
      activeContentTab: mlpData && mlpData.activeContentTypeTab,
      mlpSections: mlpData[activeContentTab][activeContentTypeTab],
      loadingData: !(userInterestLength + 1 === competencyDataLength && quarterDataLength === 5),
      isSearching: !(userInterestLength + 1 === competencyDataLength && quarterDataLength === 5)
    });
  }

  tabChange = activeTab => {
    this.props.dispatch(contentTypeTabChange(activeTab));
  };

  filterSearch = () => {
    this.props.searchData(this._inputFilter.value);
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      this.setState({ isSearching: true, mlpSections: [] }, () => {
        this.filterSearch();
      });
    }
  };

  filterChange = value => {
    this.props.dispatch(changeCurrentYearObj(value));
    this.props.dispatch(changeCurrentYear(value.value));
    this.props.setYear(value.value);
  };

  render() {
    let activeContentTab = this.props.mlpv5.activeContentTab;
    let activeContentTypeTab = this.props.mlpv5.activeContentTypeTab;
    let isDataPresent = !!Object.values(
      this.props.mlpv5[activeContentTab][activeContentTypeTab]
    ).filter(element => {
      return element.assignments.length > 0;
    }).length;
    let mlpNoOfPeriods = 0;
    return (
      <div>
        {!this.state.loadingData ? (
          <Paper className="mlp-table-container">
            <div className="quarter-competency-tab-holder">
              {this.state.showCompetency && (
                <div style={{ fontSize: '12px', fontWeight: '300', color: '#454560' }}>
                  {tr('View by')}
                </div>
              )}
              <div>
                {this.state.showCompetency && (
                  <div className="quarter-competency-tab">
                    {this.state.contentTypeTab.map((tab, index) => {
                      let tabClassName =
                        this.state.activeContentTab === tab.value
                          ? 'mpl-top-tabs activeTab'
                          : 'mpl-top-tabs';
                      return (
                        <button
                          key={index}
                          className={tabClassName}
                          onClick={this.tabChange.bind(this, tab.value)}
                        >
                          <span tabIndex={-1} className="hideOutline">
                            {tr(tab.displayName)}
                          </span>
                        </button>
                      );
                    })}
                  </div>
                )}
                {false && (
                  <div className="create-mlp-button">
                    <SecondaryButton
                      style={{ margin: '0px' }}
                      label={tr('Create Plan')}
                      icon={<AddIcon />}
                    />
                  </div>
                )}

                {this.state.isShowSearch && (
                  <div className="mlp__search-input-block">
                    <input
                      placeholder={tr('Search')}
                      onKeyDown={this.handleFilterSearch}
                      type="text"
                      ref={node => (this._inputFilter = node)}
                    />
                    <button
                      tabIndex={0}
                      className="search-button"
                      type="submit"
                      onClick={this.filterSearch.bind(this)}
                    >
                      <SearchIcon focusable="false" height={18} />
                    </button>
                  </div>
                )}
                {!this.state.enableCustomQuarters && (
                  <div
                    className="mlp-filter-dropdown"
                    style={{ float: 'right', marginRight: '10px' }}
                  >
                    <div className="mlp-filter-title">{tr('Select Year')}</div>
                    <Select
                      cache={false}
                      style={{
                        width: `${8.5 * this.props.mlpv5.currentYearObj.label.length + 27}px`
                      }}
                      onChange={this.filterChange.bind(this)}
                      value={this.props.mlpv5.currentYearObj}
                      options={this.filterOptions.filter(obj => obj.visible)}
                      clearable={false}
                      searchable={false}
                    />
                  </div>
                )}
              </div>
            </div>
            {this.state.isSearching ? (
              <Paper className="mlp-table-container">
                <div className="text-center" style={{ margin: '40px 0px 0px 0px' }}>
                  <Spinner />
                </div>
              </Paper>
            ) : isDataPresent ? (
              Object.keys(this.state.mlpSections)
                .sort()
                .map((item, index) => {
                  if (
                    this.state.mlpSections[item].assignments.length > 0 &&
                    this.state.mlpSections[item].assignmentCount > 0
                  ) {
                    return (
                      <MyLearningPlanContentContainer
                        key={index}
                        mlpPeriodData={this.state.mlpSections[item]}
                        mlpOrder={index}
                        mlpNoOfPeriods={mlpNoOfPeriods++}
                      />
                    );
                  }
                })
            ) : (
              <p className="empty-mlp-state">
                {tr(
                  `You don’t have anything in your ${activeContentTypeTab} learning plan yet. ${
                    activeContentTab == 'current' ? 'Assigned' : 'Completed'
                  } content will appear here`
                )}
              </p>
            )}
          </Paper>
        ) : (
          <Paper className="mlp-table-container">
            <div className="text-center" style={{ margin: '40px 0px 0px 0px' }}>
              <Spinner />
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

MyLearningPlanTableContainer.propTypes = {
  mlpv5: PropTypes.object,
  currentUser: PropTypes.object,
  searchData: PropTypes.func,
  team: PropTypes.object,
  setYear: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    mlpv5: state.mlpv5.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(MyLearningPlanTableContainer);
