import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import MyLearningPlanTableContainer from './MyLearningPlanTableContainer';
import {
  contentTabChange,
  getUserInterests,
  fetchQuarterlyPlan,
  fetchCompetencyPlan,
  clearAssignments,
  changeCurrentYearObj,
  changeCurrentYear
} from '../../../../actions/mylearningplanV5Actions';

class MyLearningContainerV5 extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;
    let yearFilter = new Date().getFullYear();

    this.state = {
      activeContentTab: 'current',
      contentTab: [
        { value: 'current', displayName: 'CURRENT' },
        { value: 'completed', displayName: 'COMPLETED' }
      ],
      enableCustomQuarters:
        this.props.team && this.props.team.config && this.props.team.config.enable_custom_quarters,
      userInterest: [],
      yearFilter
    };
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      activeContentTab: nextProps.mlpv5 && nextProps.mlpv5.activeContentTab,
      userInterest: nextProps.mlpv5 && nextProps.mlpv5.userInterest
    });
  }

  componentDidMount() {
    this.getMLPData(false, this.state.activeContentTab);
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }

    return card_type;
  }

  searchData = (query, fromFilter = false, yearFilter) => {
    this.props.dispatch(clearAssignments(this.state.activeContentTab));
    this.getMLPData(false, this.state.activeContentTab, query, fromFilter, yearFilter);
  };

  setYear = value => {
    this.searchData('', true, value);
  };

  getMLPData = (loadMore, activeTab, query = '', fromFilter, yearFilter) => {
    let mlpPeriod = ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'untimed'];
    let activeContentTab = activeTab;
    let card_type = this.getAssignmentContentType();
    mlpPeriod.map(period => {
      let payload = {
        limit: 15,
        offset: 0,
        mlp_period: period,
        'state[]': activeContentTab === 'current' ? ['assigned', 'started'] : ['completed'],
        query
      };

      if (!this.state.enableCustomQuarters && fromFilter) {
        payload.year_filter = yearFilter;
      }

      if (card_type.length > 0) {
        payload['card_type[]'] = card_type;
      }
      this.props.dispatch(fetchQuarterlyPlan(payload, activeContentTab, loadMore, query));
    });
    this.props
      .dispatch(getUserInterests(this.props.currentUser.id))
      .then(() => {
        this.state.userInterest.map(interest => {
          let payload = {
            limit: 15,
            offset: 0,
            mlp_period: 'skill-wise',
            learning_topics_name: interest.topic_name,
            'state[]': activeContentTab == 'current' ? ['assigned', 'started'] : ['completed'],
            query
          };

          if (card_type.length > 0) {
            payload['card_type[]'] = card_type;
          }

          if (!this.state.enableCustomQuarters && fromFilter) {
            payload.year_filter = yearFilter;
          }

          this.props.dispatch(fetchCompetencyPlan(payload, activeContentTab, loadMore, query));
        });
      })
      .catch(err => {
        console.error(`Error in MyLearningPlanContainer.getUserInterests.func : ${err}`);
      });
    let payload = {
      limit: 15,
      offset: 0,
      mlp_period: 'other',
      'state[]': activeContentTab == 'current' ? ['assigned', 'started'] : ['completed'],
      query
    };

    if (card_type.length > 0) {
      payload['card_type[]'] = card_type;
    }

    if (!this.state.enableCustomQuarters && fromFilter) {
      payload.year_filter = yearFilter;
    }
    this.props.dispatch(fetchCompetencyPlan(payload, activeContentTab, loadMore, query));
  };

  tabChange = (e, activeTab) => {
    e.preventDefault();
    let currentYear = new Date().getFullYear() + '';
    this.props.dispatch(changeCurrentYearObj({ value: currentYear, label: currentYear }));
    this.props.dispatch(changeCurrentYear(''));

    this.props.dispatch(contentTabChange(activeTab));
    this.getMLPData(false, activeTab);
  };

  render() {
    return (
      <div>
        <div id="mlp-v5">
          <div className="mlp-container">
            <div className="left-nav-filters">
              {this.state.contentTab.map((tab, index) => {
                let tabClassName =
                  this.state.activeContentTab === tab.value
                    ? 'mpl-left-tabs activeTab'
                    : 'mpl-left-tabs';
                return (
                  <a
                    href="#"
                    key={index}
                    className={tabClassName}
                    onClick={e => this.tabChange(e, tab.value)}
                  >
                    {tr(tab.displayName)}
                  </a>
                );
              })}
            </div>
            <div className="my-mlp-v5">
              <MyLearningPlanTableContainer searchData={this.searchData} setYear={this.setYear} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    mlpv5: state.mlpv5.toJS(),
    team: state.team.toJS()
  };
}

MyLearningContainerV5.propTypes = {
  mlpv5: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

export default connect(mapStoreStateToProps)(MyLearningContainerV5);
