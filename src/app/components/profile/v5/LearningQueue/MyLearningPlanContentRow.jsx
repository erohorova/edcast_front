import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors';
import LinearProgress from 'material-ui/LinearProgress';
import DateConverter from '../../../common/DateConverter';
import _ from 'lodash';
import getCardType from '../../../../utils/getCardType';
import RichTextReadOnly from '../../../common/RichTextReadOnly';
import convertRichText from '../../../../utils/convertRichText';

function navigateTo(cardType, slug, e) {
  e.preventDefault();
  let url =
    (cardType === 'pack' ? '/pathways/' : cardType === 'journey' ? '/journey/' : '/insights/') +
    slug;
  window.location = url;
}

function MyLearningPlanContentRow(props) {
  let assignment = props.assignment;
  let card = props.assignmentCard;
  let activeContentTab = props.activeContentTab;
  let enableBIA = props.team.config.enabled_bia;
  let message = _.unescape(
    (card.title || card.message).length >= 100
      ? (card.title || card.message).substr(0, 100) + '...'
      : card.title || card.message
  );

  return (
    <a
      href="#"
      className="mlp-content-row"
      aria-label={`visit the detailed page of ${message}`}
      onClick={navigateTo.bind(this, card.cardType, card.slug)}
    >
      <div className="mlp-content-name-row" style={enableBIA ? { width: '30%' } : { width: '40%' }}>
        <RichTextReadOnly text={convertRichText(message)} />
        <div className="content-assigner-type">
          {assignment.assignor && assignment.assignor.name && (
            <span>
              <small>{tr('Assigner')}:</small> {assignment.assignor.name} |{' '}
            </span>
          )}
          <small>{tr('Type')}:</small>{' '}
          {tr(card.readableCardType || _.startCase(_.toLower(getCardType(card))))}
        </div>
      </div>
      {enableBIA && (
        <div style={{ width: '10%', textAlign: 'center', margin: 'auto' }}>
          <div className="card-bia" style={{ margin: 'auto' }}>
            <div className={`${card.skillLevel} level-dots`} style={{ textAlign: 'center' }}>
              <span className="first">
                <div className="level-tip">{tr('Beginner')}</div>
              </span>
              <span className="second">
                <div className="level-tip">{tr('Intermediate')}</div>
              </span>
              <span className="third">
                <div className="level-tip">{tr('Advanced')}</div>
              </span>
            </div>
            <div className="level">{card.skillLevel}</div>
          </div>
        </div>
      )}
      <div
        className="mlp-content-efforts-row"
        style={{ width: '10%', textTransform: 'capitalize' }}
      >
        {(card.eclDurationMetadata &&
          card.eclDurationMetadata.calculated_duration_display.replace('about ', '')) ||
          tr('NA')}
      </div>
      {activeContentTab === 'current' &&
        (!(card.completedPercentage === 0 && assignment.state === 'started') ? (
          <div className="mlp-content-complete-row" style={{ width: '25%' }}>
            <div>
              <LinearProgress
                style={{
                  backgroundColor: '#D6D5E0',
                  height: '12px',
                  borderRadius: '0px'
                }}
                color={'#45445E'}
                min={0}
                max={100}
                value={card.completedPercentage}
                mode="determinate"
              />
              <div
                className="indicator dark"
                style={{
                  width: (card.completedPercentage >= 100 ? 100 : card.completedPercentage) + '%'
                }}
              >
                <div className={card.completedPercentage <= 20 ? 'hide-right-indicator' : ''}>
                  <span>
                    {card.completedPercentage >= 100 ? 100 : parseInt(card.completedPercentage, 10)}
                    %
                  </span>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="mlp-content-complete-row" style={{ width: '25%', textAlign: 'center' }}>
            {tr('In Progress')}
          </div>
        ))}
      {activeContentTab === 'completed' && (
        <div className="mlp-content-complete-date-row" style={{ width: '10%' }}>
          {assignment.completedAt ? <DateConverter date={assignment.completedAt} /> : tr('NA')}
        </div>
      )}

      <div className="mlp-content-assigned-date-row" style={{ width: '10%', textAlign: 'center' }}>
        {assignment.createdAt ? <DateConverter date={assignment.createdAt} /> : tr('NA')}
      </div>
      {activeContentTab === 'current' && (
        <div className="mlp-content-due-date-row" style={{ width: '10%', textAlign: 'center' }}>
          {assignment.dueAt ? <DateConverter date={assignment.dueAt} /> : tr('NA')}
        </div>
      )}
      {activeContentTab === 'completed' && (
        <div className="mlp-badge-row" style={{ width: '25%', textAlign: 'center' }}>
          {card.badging ? (
            <div>
              <img className="badge-img" src={card.badging.imageUrl} />
              <span className="badge-name">{card.badging.title}</span>
            </div>
          ) : (
            <span className="badge-name" style={{ textAlign: 'center' }}>
              {tr('NA')}
            </span>
          )}
        </div>
      )}
      <div style={{ width: '5%', textAlign: 'center' }} />
    </a>
  );
}

MyLearningPlanContentRow.propTypes = {
  activeContentTab: PropTypes.string,
  assignmentCard: PropTypes.object,
  assignment: PropTypes.object,
  team: PropTypes.object
};

export default MyLearningPlanContentRow;
