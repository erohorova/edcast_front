import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

function MyLearningPlanHeaderRow(props) {
  let activeContentTab = props.activeContentTab;
  let enableBIA = props.team.config.enabled_bia;

  return (
    <div className="mlp-content-type-headings">
      <div style={enableBIA ? { width: '30%' } : { width: '40%' }}>{tr('Name')}</div>
      {enableBIA && <div style={{ width: '10%' }}>{tr('Level')}</div>}
      <div style={{ width: '10%' }}>{tr('Duration')}</div>
      {activeContentTab === 'current' && (
        <div style={{ width: '25%' }}>{tr('Completion Status')}</div>
      )}
      {activeContentTab === 'completed' && (
        <div style={{ width: '10%' }}>{tr('Completed date')}</div>
      )}
      <div style={{ width: '10%' }}>{tr('Assigned Date')}</div>
      {activeContentTab === 'current' && <div style={{ width: '10%' }}>{tr('Due Date')}</div>}
      {activeContentTab === 'completed' && <div style={{ width: '25%' }}>{tr('Credentials')}</div>}
    </div>
  );
}

MyLearningPlanHeaderRow.propTypes = {
  activeContentTab: PropTypes.string,
  team: PropTypes.object
};

export default MyLearningPlanHeaderRow;
