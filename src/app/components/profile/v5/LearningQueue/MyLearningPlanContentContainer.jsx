import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import MyLearningPlanHeaderRow from './MyLearningPlanHeaderRow';
import MyLearningPlanContentRow from './MyLearningPlanContentRow';
import {
  fetchQuarterlyPlan,
  fetchCompetencyPlan
} from '../../../../actions/mylearningplanV5Actions';
class MyLearningPlanContentContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;

    this.state = {
      activeContentTab: 'current',
      userInterest: [],
      mlpv5ActiveData: [],
      loadMore: false,
      enableCustomQuarters:
        this.props.team && this.props.team.config && this.props.team.config.enable_custom_quarters
    };
  }

  componentWillMount() {
    let reducerProps = this.props.mlpv5;
    reducerProps = reducerProps[reducerProps.activeContentTab][reducerProps.activeContentTypeTab];
    this.setState({
      mlpv5ActiveData: reducerProps
    });
  }

  componentWillReceiveProps(nextProps) {
    let mlpv5Data = nextProps.mlpv5 && nextProps.mlpv5;
    this.setState({
      activeContentTab: mlpv5Data.activeContentTab,
      userInterest: mlpv5Data.userInterest,
      mlpv5ActiveData: mlpv5Data[mlpv5Data.activeContentTab][mlpv5Data.activeContentTypeTab]
    });
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }

    return card_type;
  }

  loadMore(activeContentTab, mlp_period, offset, mlpPeriodData) {
    this.setState({
      loadMore: true
    });

    let card_type = this.getAssignmentContentType();

    let payload = {
      limit: 15,
      offset,
      mlp_period,
      'state[]': activeContentTab === 'current' ? ['assigned', 'started'] : ['completed']
    };

    if (card_type.length > 0) {
      payload['card_type[]'] = card_type;
    }

    if (!this.state.enableCustomQuarters && this.props.mlpv5.currentYear) {
      payload.year_filter = this.props.mlpv5.currentYear;
    }

    if (mlpPeriodData.learningTopicsName) {
      payload['learning_topics_name'] = mlpPeriodData.learningTopicsName;
      this.getCompetencyData(activeContentTab, payload, true);
    } else {
      this.getQuarterlyData(activeContentTab, payload, true);
    }
  }

  getQuarterlyData(activeContentTab, payload, loadMore) {
    this.props
      .dispatch(fetchQuarterlyPlan(payload, activeContentTab, loadMore))
      .then(data => {
        this.setState({
          loadMore: false
        });
      })
      .catch(err => {
        console.error(`Error in MyLearningPlanContentContainer.getQuarterlyData.func : ${err}`);
      });
  }

  getCompetencyData(activeContentTab, payload, loadMore) {
    this.props
      .dispatch(fetchCompetencyPlan(payload, activeContentTab, loadMore))
      .then(data => {
        this.setState({
          loadMore: false
        });
      })
      .catch(err => {
        console.error(`Error in MyLearningPlanContentContainer.fetchCompetencyPlan.func : ${err}`);
      });
  }

  render() {
    let mlpPeriodData = this.state.mlpv5ActiveData[
      this.props.mlpPeriodData.learningTopicsName || this.props.mlpPeriodData.mlpPeriod
    ];
    let skillName = this.state.userInterest.map((item, index) => {
      if (mlpPeriodData.learningTopicsName === item.topic_name) {
        return item.topic_label;
      }
    });
    let quearterName = mlpPeriodData.mlpPeriod.replace(/-/i, ' ').replace(/-/i, ' ');
    quearterName = quearterName[0].toUpperCase() + quearterName.slice(1);
    if (!this.state.enableCustomQuarters && quearterName.indexOf('Quarter') > -1) {
      quearterName = quearterName + ' - ' + this.props.mlpv5.currentYearObj.value;
    }
    let contentHeaderName = mlpPeriodData.learningTopicsName ? skillName : quearterName;
    let assignments = mlpPeriodData.assignments;

    return (
      <div>
        <div className="mlp-table-holder">
          <div role="heading" aria-level="2" className="mlp-content-type-header">
            {tr(contentHeaderName == 'Other' ? 'Others' : contentHeaderName)}
          </div>
          {this.props.mlpNoOfPeriods === 0 && (
            <MyLearningPlanHeaderRow
              activeContentTab={this.props.mlpv5.activeContentTab}
              team={this.props.team}
            />
          )}
          {assignments.map((item, index) => {
            return (
              <MyLearningPlanContentRow
                key={index}
                assignment={item}
                assignmentCard={item.assignable}
                activeContentTab={this.props.mlpv5.activeContentTab}
                team={this.props.team}
              />
            );
          })}
          <div className="load-more-section">
            {!this.props.mlpv5.searchQuery && (
              <div className="total-count-container">
                <small>
                  {assignments.length} of{' '}
                  {this.props.mlpPeriodData.assignmentCount.toLocaleString('en-IN', {
                    maximumFractionDigits: 0
                  })}
                </small>
              </div>
            )}
            {mlpPeriodData.assignmentCount > assignments.length && (
              <div
                style={!this.props.mlpv5.searchQuery ? {} : { width: '100%' }}
                className="load-more-container"
                onClick={this.loadMore.bind(
                  this,
                  this.props.mlpv5.activeContentTab,
                  mlpPeriodData.mlpPeriod,
                  mlpPeriodData.assignments.length,
                  mlpPeriodData
                )}
              >
                <span>{tr(this.state.loadMore ? 'Loading...' : 'See more')}</span>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    mlpv5: state.mlpv5.toJS(),
    team: state.team.toJS()
  };
}

MyLearningPlanContentContainer.propTypes = {
  mlpv5: PropTypes.object,
  currentUser: PropTypes.object,
  mlpPeriodData: PropTypes.object,
  mlpNoOfPeriods: PropTypes.number,
  team: PropTypes.object
};

export default connect(mapStoreStateToProps)(MyLearningPlanContentContainer);
