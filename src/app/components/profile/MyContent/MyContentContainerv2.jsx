import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  getUserCards,
  getCards,
  getContentCards,
  getBookmarkedCards,
  getPrivateCards,
  getCardsFromCardSubtype,
  getCardsWithBaseTypes,
  getManagedCards,
  getSharedCards,
  getPurchasedCards,
  _getAssignments
} from '../../../actions/cardsActions';
import Card from '../../cards/Card';
import { push } from 'react-router-redux';
import uniq from 'lodash/uniq';
import throttle from 'lodash/throttle';
import { tr } from 'edc-web-sdk/helpers/translations';
import * as insightType from '../../../constants/insightTypes';
import { Permissions } from '../../../utils/checkPermissions';
import updatePageLastVisit from '../../../utils/updatePageLastVisit';
import SelectField from 'material-ui/SelectField';
import FilterIcon from 'edc-web-sdk/components/icons/Filter';
import colors from 'edc-web-sdk/components/colors/index';
import MenuItem from 'material-ui/MenuItem';
import Spinner from '../../common/spinner';
import { getSpecificUserInfo } from '../../../actions/currentUserActions';
import { cardFields } from '../../../constants/cardFields';

const SHARED_BY_FIELDS =
  'all_ratings,article,assignment,author,auto_complete,average_rating,badging,card_metadatum,card_subtype,card_type,channel_ids,channels,comments_count,completed_percentage,completion_state,created_at,duration,ecl_duration_metadata,ecl_id,ecl_metadata,file_resources,filestack,hidden,id,is_assigned,is_bookmarked,is_clone,is_official,is_paid,is_public,is_reported,is_upvoted,journey_packs_count,language,leaps,locked,mark_feature_disabled_for_source,message,non_curated_channel_ids,pack_cards,pack_cards_count,paid_by_user,payment_enabled,prices,project_id,provider,provider_image,published_at,quiz,rank,readable_card_type,resource,share_url,show_restrict,skill_level,shared_by,slug,state,tags,teams,teams_permitted,title,user_rating,users_permitted,users_with_access,video_stream,views_count,voters,votes_count';

class MyContentContainerv2 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: 10,
      cardIds: [],
      pending: true,
      filterType: 'all',
      cardState: ['draft', 'published', 'processing', 'error'],
      showJourney: window.ldclient.variation('journey', false),
      readableCardTypeFilter: 'All',
      shareOnSmartCardCreation: window.ldclient.variation('share-on-smart-card-creation', false),
      bookmarkStandalone: window.ldclient.variation('bookmark-standalone', false),
      showNew: window.ldclient.variation('show-new-for-card', false)
    };

    this.baseTypes = ['article', 'image', 'music', 'text', 'video'];
    this.offsetWithType = 0;
    this.offsetWithoutType = 0;
    if (Permissions.has('MANAGE_CARD')) {
      this.filterOptions = [
        { value: 'all', text: 'All' },
        { value: 'SmartBites', text: 'SmartCards' },
        { value: 'Pathways', text: 'Pathways' },
        { value: 'VideoStreams', text: 'VideoStreams' },
        { value: 'completed', text: 'Completed' },
        { value: 'bookmarked', text: 'bookmarked' },
        { value: 'sharedWithMe', text: 'Shared With Me' },
        { value: 'sharedWithTeam', text: 'Shared With Team' }
      ];

      if (this.state.showJourney) {
        this.filterOptions.push({ value: 'Journeys', text: 'Journeys' });
      }
      if (this.state.shareOnSmartCardCreation) {
        this.filterOptions.push({ value: 'privateContent', text: 'Private Content' });
      }

      if (
        !!(this.props.team && this.props.team.config && this.props.team.config.enable_managed_cards)
      ) {
        this.filterOptions.push({ value: 'managed', text: 'Managed' });
      }

      if (
        !!(
          this.props.team &&
          this.props.team.config &&
          this.props.team.config.enable_smart_card_price_field
        )
      ) {
        this.filterOptions.push({ value: 'purchased', text: 'Purchased' });
      }

      if (Permissions.has('DISMISS_ASSIGNMENT')) {
        this.filterOptions.push({ value: 'dismissed', text: 'Dismissed' });
      }
    } else {
      this.filterOptions = [
        { value: 'completed', text: 'Completed' },
        { value: 'sharedWithMe', text: 'Shared With User' },
        { value: 'sharedWithTeam', text: 'Shared With Team' }
      ];
    }
    // if (!this.state.bookmarkStandalone) {
    //     this.filterOptions.push({value: 'bookmarked', text: 'Bookmarked'});
    // }
    let keepHistoryConfig =
      props.team && props.team.config && props.team.config['keep_deleted_history'];
    if (window.ldclient.variation('keep-deleted-history', false) && keepHistoryConfig) {
      this.filterOptions.push({ value: 'deleted', text: 'Deleted' });
    }
    this.availableCardTypes = this.props.team.readableCardTypes;
    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreCards = this.showMoreCards.bind(this);
  }

  componentDidMount() {
    let userInfoCallBack = getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props
      .dispatch(userInfoCallBack)
      .then(async () => {
        this.handleRouteParams(this.props.routeParams.filter);
        window.addEventListener('scroll', this.handleScroll);
      })
      .catch(err => {
        console.error(`Error in MyContentContainerv2.componentDidMount.func: ${err}`);
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    this.state.showNew &&
      localStorage.setItem('contentLastVisit', Math.round(new Date().getTime() / 1000));
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreCards();
        }
      }
    },
    150,
    { leading: false }
  );

  componentWillReceiveProps(nextProps) {
    if (nextProps.routeParams.filter !== this.props.routeParams.filter) {
      if (nextProps.routeParams.filter == 'SmartBites') {
        this.setState({
          readableCardTypeFilter: 'All'
        });
      }
      this.handleRouteParams(nextProps.routeParams.filter);
    }
  }

  fetchCards = (param, strOffset, cOffset) => {
    let func, arg;
    let streamsOffset = strOffset ? strOffset : 0;
    let cardsOffset = cOffset ? cOffset : 0;
    switch (param) {
      case 'VideoStreams':
        func = getCards;
        arg = [
          this.props.currentUser.id,
          this.state.limit,
          streamsOffset,
          insightType.VIDEO_STREAM,
          null
        ];
        break;
      case 'privateContent':
        func = getPrivateCards;
        arg = [this.props.currentUser.id, this.state.limit, cardsOffset];
        break;
      case 'sharedWithMe':
        func = getSharedCards;
        arg = [this.state.limit, cardsOffset, 'user_share', null, SHARED_BY_FIELDS];
        break;
      case 'sharedWithTeam':
        func = getSharedCards;
        arg = [this.state.limit, cardsOffset, 'team_share', null, SHARED_BY_FIELDS];
        break;
      case 'completed':
        func = getContentCards;
        arg = [this.state.limit, cardsOffset, null, 'completed'];
        break;
      case 'bookmarked':
        func = getBookmarkedCards;
        arg = [this.state.limit, cardsOffset];
        break;
      case 'SmartBites':
        if (!cardsOffset && !streamsOffset) {
          func = getUserCards;
          arg = [
            this.props.currentUser.id,
            this.state.limit,
            undefined,
            undefined,
            this.state.filterType,
            this.state.cardState,
            'created'
          ];
          break;
        } else {
          if (this.state.readableCardTypeFilter === 'All') {
            func = getUserCards;
            arg = [
              this.props.currentUser.id,
              this.state.limit,
              { cards: cardsOffset, streams: streamsOffset },
              undefined,
              this.state.filterType,
              this.state.cardState,
              'created'
            ];
            break;
          } else {
            this.getCardsIds(this.state.readableCardTypeFilter, cardsOffset);
            break;
          }
        }
      case 'managed':
        func = getManagedCards;
        arg = [this.state.limit, cardsOffset + streamsOffset];
        break;
      case 'purchased':
        func = getPurchasedCards;
        arg = [this.state.limit, cardsOffset + streamsOffset];
        break;
      case 'deleted':
        func = getCards;
        arg = [this.props.currentUser.id, this.state.limit, cardsOffset, null, true];
        break;
      case 'dismissed':
        func = _getAssignments;
        arg = [this.state.limit, cardsOffset, ['dismissed'], null, null];
        break;
      default:
        func = getUserCards;
        arg = [
          this.props.currentUser.id,
          this.state.limit,
          cardsOffset > 0 || streamsOffset > 0
            ? { cards: cardsOffset, streams: streamsOffset }
            : undefined,
          undefined,
          this.state.filterType,
          this.state.cardState,
          'created'
        ];
    }
    if (this.state.showNew && param !== 'completed') {
      arg.push(localStorage.getItem('contentLastVisit'));
      arg.push(cardFields);
      updatePageLastVisit('contentLastVisit');
    }

    if (func) {
      this.props
        .dispatch(func(...arg))
        .then(cardIds => {
          this.setState({
            cardIds:
              !cardsOffset && !streamsOffset ? cardIds : uniq(this.state.cardIds.concat(cardIds)),
            pending: false,
            isLastPage: cardIds.length < this.state.limit
          });
        })
        .catch(err => {
          console.error(`Error in MyContentContainerv2.fetchCards.func : ${err}`);
        });
    }
  };

  handleRouteParams = param => {
    let isFilterValid = this.filterOptions.some(option => option.value === param);
    let filter = isFilterValid ? param : 'all';
    this.setState({ pending: true, filterType: filter, cardIds: [] }, () => {
      this.fetchCards(param);
    });
  };

  showMoreCards = () => {
    let cards = this.state.cardIds
      .filter(id => this.props.cards[id])
      .map(id => this.props.cards[id]);
    let cardsOffset = 0,
      streamsOffset = 0;
    cards.forEach(card => {
      if (card.cardType === 'VideoStream' || card.cardType === 'video_stream') {
        streamsOffset++;
      } else {
        cardsOffset++;
      }
    });
    this.setState({ pending: true }, () => {
      this.fetchCards(this.state.filterType, streamsOffset, cardsOffset);
    });
  };

  filterChangeHandler = (e, filter) => {
    e.preventDefault();
    this.props.dispatch(push(`/me/content/${filter}`));
  };

  handleFilterChange = (e, index, value) => {
    this.setState({ pending: true });
    if (value === 'All') {
      this.props
        .dispatch(
          getUserCards(
            this.props.currentUser.id,
            this.state.limit,
            undefined,
            undefined,
            'SmartBites',
            this.state.cardState,
            'created'
          )
        )
        .then(cardIds => {
          this.setState({
            cardIds: uniq(this.state.cardIds.concat(cardIds)),
            pending: false,
            filterType: 'SmartBites',
            isLastPage: cardIds.length < this.state.limit,
            readableCardTypeFilter: value
          });
        })
        .catch(err => {
          console.error(`Error in MyContentContainerv2.getUserCards.func : ${err}`);
        });
    } else {
      this.getCardsIds(value);
    }
  };

  getCardsIds = (value, offset, last_access_at) => {
    let val = value.toLowerCase();
    let payload = {
      author_id: this.props.currentUser.id,
      limit: 15,
      sort: 'created',
      'card_type[]': ['media', 'poll'],
      'state[]': ['draft', 'published'],
      'readable_card_type[]': [val],
      last_access_at
    };
    if (val == 'quiz') {
      payload['card_type[]'] = 'poll';
      payload['readable_card_type[]'] = null;
    }

    if (val == 'text') {
      payload['card_type[]'] = ['media'];
      payload['card_subtype[]'] = [value];
    }

    if (offset) {
      payload.offset = offset;
    }

    if (~this.baseTypes.indexOf(val)) {
      let subtype = val === 'music' ? 'audio' : val === 'article' ? 'link' : val;
      payload.limit = 10;
      let payloadSub = {
        author_id: this.props.currentUser.id,
        limit: 10,
        sort: 'created',
        'card_type[]': ['media', 'poll'],
        'state[]': ['draft', 'published'],
        'readable_card_type[]': null,
        card_subtype: [subtype]
      };
      if (offset) {
        payload.offset = this.offsetWithType;
        payloadSub.offset = this.offsetWithoutType;
      }
      let card_ids = this.state.cardIds;

      if (value == 'text') {
        payloadSub['card_type[]'] = ['media'];
      }

      this.props
        .dispatch(getCardsWithBaseTypes(payload, payloadSub))
        .then(data => {
          // Comparison of the last 10 obtained ID cards with the results of a new card request
          let isTheSameIds =
            card_ids
              .slice(-10)
              .sort()
              .toString() === data.cards.sort().toString();
          let new_card_ids = isTheSameIds ? card_ids : uniq(card_ids.concat(data.cards));
          this.setState({
            cardIds: offset ? new_card_ids : data.cards,
            pending: false,
            filterType: 'SmartBites',
            isLastPage:
              (data.cards_with_type_length < 10 && data.cards_without_type_length < 10) ||
              isTheSameIds,
            readableCardTypeFilter: value
          });
          this.offsetWithType = data.cards_with_type_length;
          this.offsetWithoutType = data.cards_without_type_length;
        })
        .catch(err => {
          console.error(`Error in MyContentContainerv2.getCardsWithBaseTypes.func : ${err}`);
        });
    } else {
      this.props
        .dispatch(getCardsFromCardSubtype(payload))
        .then(cardIds => {
          this.setState({
            cardIds: offset ? uniq(this.state.cardIds.concat(cardIds)) : cardIds,
            pending: false,
            filterType: 'SmartBites',
            isLastPage: cardIds.length < this.state.limit,
            readableCardTypeFilter: value
          });
        })
        .catch(err => {
          console.error(`Error in MyContentContainerv2.getCardsFromCardSubtype.func : ${err}`);
        });
    }
  };

  removeCardFromList = id => {
    let newCardIdsList = this.state.cardIds.filter(card => {
      return card !== id;
    });
    this.setState({ cardIds: newCardIdsList });
  };

  render() {
    return (
      <div id="myContent">
        <div className="my-content-wrapper">
          <div className="left-filter">
            {this.filterOptions.map((filter, index) => {
              return (
                <div key={index} className="left-filter-item">
                  <a
                    href="#"
                    className={`left-filter-item-text ${filter.value === this.state.filterType &&
                      'active-filter'}`}
                    onClick={e => this.filterChangeHandler(e, filter.value)}
                  >
                    {tr(filter.text)}
                  </a>
                </div>
              );
            })}
          </div>
          <div className="my-content-right-block">
            {this.props.routeParams.filter === 'SmartBites' && (
              <div style={{ margin: '0px 0px 8px 0px' }}>
                <div style={{ display: 'inline-block', margin: '11px 15px 0px 0px' }}>
                  Card type :{' '}
                </div>
                <SelectField
                  hintText={
                    <div>
                      <FilterIcon
                        style={{
                          bottom: 0,
                          verticalAlign: 'middle',
                          width: '1.5rem',
                          height: '1.5rem',
                          fill: colors.silverSand
                        }}
                        viewBox="0 0 16 16"
                      />{' '}
                      {tr('Filter/Sort')}
                    </div>
                  }
                  onChange={this.handleFilterChange}
                  style={{ verticalAlign: 'top' }}
                  value={this.state.readableCardTypeFilter}
                  iconStyle={{
                    width: '1.87rem',
                    height: '1.87rem',
                    top: '0.437rem',
                    fill: colors.silverSand
                  }}
                >
                  <MenuItem key={-1} value="All" primaryText={tr('All')} />
                  {this.availableCardTypes.map((option, index) => {
                    return (
                      <MenuItem
                        key={index}
                        value={option}
                        primaryText={tr(option.replace('_', ' '))}
                      />
                    );
                  })}
                </SelectField>
              </div>
            )}

            <div>
              {!!this.state.cardIds.length && (
                <div className="custom-card-container">
                  <div className="five-card-column">
                    {this.state.cardIds
                      .filter(id => this.props.cards[id])
                      .map((id, index) => {
                        let card = this.props.cards[id];
                        let author =
                          card &&
                          this.props.users.idMap[card.authorId || (card.author && card.author.id)];
                        return (
                          <Card
                            state="published"
                            card={card}
                            key={card.id}
                            dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                            startDate={
                              card.startDate || (card.assignment && card.assignment.startDate)
                            }
                            user={author}
                            author={author}
                            removeCardFromList={this.removeCardFromList}
                            moreCards={true}
                          />
                        );
                      })}
                  </div>
                </div>
              )}
              {!this.state.pending && !this.state.cardIds.length && (
                <div className="container-padding vertical-spacing-medium custom-result">
                  <div className="text-center vertical-spacing-large">
                    <div className="data-not-available-msg">
                      {tr('There are no available cards.')}
                    </div>
                  </div>
                </div>
              )}
              {this.state.pending && (
                <div className="text-center pending-cards">
                  <Spinner />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MyContentContainerv2.propTypes = {
  currentUser: PropTypes.object,
  cards: PropTypes.object,
  routeParams: PropTypes.object,
  users: PropTypes.object,
  team: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  cards: state.cards.toJS(),
  users: state.users.toJS(),
  team: state.team.toJS()
}))(MyContentContainerv2);
