import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import Carousel from '../../common/Carousel';
import Course from '../../discovery/Course.jsx';

class CourseCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <Paper style={{ padding: '1rem' }}>
        <div id="learners-dashboard-course-carousel">
          <div style={{ margin: '0px 0px 15px 0px' }}>
            <span className="header-text">
              {tr('Courses ')}({this.props.courses.length})
            </span>
          </div>
          <div className="channel-card-wrapper">
            <div className="channel-card-wrapper-inner">
              <Carousel slidesToShow={this.props.numberOfCourses}>
                {this.props.courses.map(course => {
                  return (
                    <div key={course.id}>
                      <Course
                        name={course.name}
                        logoUrl={course.imageUrl}
                        startDate={course.assignedDate}
                        endDate={course.dueDate}
                        status={course.status}
                        url={course.courseUrl}
                      />
                    </div>
                  );
                })}
              </Carousel>
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

CourseCarousel.propTypes = {
  numberOfCourses: PropTypes.number,
  courses: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {};
}

export default connect(mapStoreStateToProps)(CourseCarousel);
