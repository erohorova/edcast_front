import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import Paper from 'edc-web-sdk/components/Paper';

import Carousel from '../../common/Carousel';
import Card from '../../cards/Card';

class CardCarousel extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  viewAll = e => {
    e.preventDefault();
    this.props.dispatch(push('/me/content'));
  };
  render() {
    let currentUserID = this.props.currentUser && this.props.currentUser.id;
    let publicProfileUserID =
      this.props.currentUser &&
      this.props.currentUser.publicProfileBasicInfo &&
      this.props.currentUser.publicProfileBasicInfo.id;
    return (
      <Paper style={{ padding: '1rem' }}>
        <div id="learners-dashboard-card-carousel">
          <div style={{ margin: '0 0 0.625rem 0' }}>
            <span className="header-text">
              {tr('Published content')} ({this.props.cardsCount})
            </span>
            {this.props.cardsCount > 4 &&
              !(
                this.props.publicProfile &&
                publicProfileUserID &&
                currentUserID !== publicProfileUserID
              ) && (
                <div className="float-right">
                  <a href="#" className="view-more-badges" onClick={this.viewAll}>
                    {tr('View All')}
                  </a>
                </div>
              )}
          </div>
          <div className="channel-card-wrapper">
            <div className="channel-card-wrapper-inner">
              <Carousel isProfileCardCarousel="true">
                {this.props.cards &&
                  Object.keys(this.props.cards).map(cardId => {
                    let card = this.props.cards[cardId];
                    return (
                      <div key={card.id}>
                        <Card
                          toggleSearch={function() {}}
                          author={card.author && card.author}
                          card={card}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.props.currentUser}
                          tooltipPosition="top"
                          moreCards={false}
                          withoutCardModal={true}
                          removeCardFromList={this.props.removeCardFromList}
                        />
                      </div>
                    );
                  })}
              </Carousel>
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

CardCarousel.propTypes = {
  numberOfCards: PropTypes.number,
  cardsCount: PropTypes.number,
  currentUser: PropTypes.object,
  publicProfile: PropTypes.bool,
  cards: PropTypes.array,
  removeCardFromList: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardCarousel);
