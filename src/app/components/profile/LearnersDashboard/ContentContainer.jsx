import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import CardContainer from './CardContainer';
import {
  getUserContentCards,
  getBookmarkedCompletedCards,
  getPrivateCards
} from '../../../actions/cardsActions';
class ContentContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      createdCards: [],
      bookmarkedCards: [],
      completedCards: [],
      createdCardsEmpty: false,
      bookmarkedCardsEmpty: false,
      completedCardsEmpty: false,
      privateCards: false,
      myContentEnabled: window.ldclient.variation('show-my-content-section', true)
    };
  }

  componentDidMount() {
    let payload = {
      author_id: this.props.currentUser.id,
      limit: this.props.numberOfCards,
      offset: 0
    };
    if (this.state.myContentEnabled) {
      this.props
        .dispatch(
          getUserContentCards({
            ...payload,
            ...{ sort: 'created', 'state[]': ['draft', 'published'] }
          })
        )
        .then(response => {
          if (response) {
            this.setState({
              createdCards: response.cards,
              createdCardsEmpty: response.cards && response.cards.length === 0
            });
          }
        })
        .catch(err => {
          console.error(`Error in ContentContainer.getUserContentCards.func : ${err}`);
        });
      this.props
        .dispatch(getBookmarkedCompletedCards({ ...payload, ...{ 'source[]': 'bookmarks' } }))
        .then(response => {
          if (response) {
            this.setState({
              bookmarkedCards: response,
              bookmarkedCardsEmpty: response.length === 0
            });
          }
        })
        .catch(err => {
          console.error(
            `Error in ContentContainer.getBookmarkedCompletedCards.func bookmarks: ${err}`
          );
        });
      this.props
        .dispatch(getBookmarkedCompletedCards({ ...payload, ...{ 'state[]': 'completed' } }))
        .then(response => {
          if (response) {
            this.setState({
              completedCards: response,
              completedCardsEmpty: response.length === 0
            });
          }
        })
        .catch(err => {
          console.error(
            `Error in ContentContainer.getBookmarkedCompletedCards.func completed: ${err}`
          );
        });
      this.props
        .dispatch(getPrivateCards(this.props.currentUser.id, this.props.numberOfCards, 0))
        .then(response => {
          if (response) {
            let cards = response.map(item => {
              return this.props.cards[item];
            });
            this.setState({
              privateCards: cards,
              privateCardsEmpty: cards && cards.length === 0
            });
          }
        })
        .catch(err => {
          console.error(`Error in ContentContainer.getPrivateCards.func : ${err}`);
        });
    }
  }

  removeCardFromCardContainer = cardSectionName => {
    let payload = {
      author_id: this.props.currentUser.id,
      limit: this.props.numberOfCards,
      offset: 0
    };

    switch (cardSectionName) {
      case 'Created Cards':
        this.props
          .dispatch(
            getUserContentCards({
              ...payload,
              ...{ sort: 'created', 'state[]': ['draft', 'published'] }
            })
          )
          .then(response => {
            this.setState({
              createdCards: response.cards,
              createdCardsEmpty: response.cards && response.cards.length === 0
            });
          })
          .catch(err => {
            console.error(`Error in ContentContainer.getUserContentCards.func : ${err}`);
          });
        break;
      case 'Bookmarked Cards':
        this.props
          .dispatch(getBookmarkedCompletedCards({ ...payload, ...{ 'source[]': 'bookmarks' } }))
          .then(response => {
            this.setState({
              bookmarkedCards: response,
              bookmarkedCardsEmpty: response.length === 0
            });
          })
          .catch(err => {
            console.error(
              `Error in ContentContainer.getBookmarkedCompletedCards.func bookmarks: ${err}`
            );
          });
        break;
      case 'Completed Cards':
        this.props
          .dispatch(getBookmarkedCompletedCards({ ...payload, ...{ 'state[]': 'completed' } }))
          .then(response => {
            this.setState({
              completedCards: response,
              completedCardsEmpty: response.length === 0
            });
          })
          .catch(err => {
            console.error(
              `Error in ContentContainer.getBookmarkedCompletedCards.func completed: ${err}`
            );
          });
        break;
      case 'Private Cards':
        this.props
          .dispatch(getPrivateCards(this.props.currentUser.id, this.props.numberOfCards, 0))
          .then(response => {
            let cards = response.map(item => {
              return this.props.cards[item];
            });
            this.setState({
              privateCards: cards,
              privateCardsEmpty: cards && cards.length === 0
            });
          })
          .catch(err => {
            console.error(`Error in ContentContainer.getPrivateCards.func : ${err}`);
          });
        break;
      default:
        break;
    }
  };

  render() {
    return (
      !!this.state.myContentEnabled &&
      (!this.state.createdCardsEmpty ||
        !this.state.bookmarkedCardsEmpty ||
        !this.state.completedCardsEmpty ||
        !this.state.privateCardsEmpty) && (
        <Paper style={{ padding: '1rem' }}>
          <div style={{ margin: '0px 0px 15px 0px' }}>
            <span className="header-text" role="heading" aria-level="3">
              {tr('My Content')}
            </span>
          </div>
          {!this.state.createdCardsEmpty && (
            <CardContainer
              numberOfCards={this.props.numberOfCards}
              cardSectionName="Created Cards"
              cards={this.state.createdCards}
              removeCardFromCardContainer={this.removeCardFromCardContainer}
            />
          )}
          {!this.state.bookmarkedCardsEmpty && (
            <CardContainer
              numberOfCards={this.props.numberOfCards}
              cardSectionName="Bookmarked Cards"
              cards={this.state.bookmarkedCards}
              removeCardFromCardContainer={this.removeCardFromCardContainer}
            />
          )}
          {!this.state.completedCardsEmpty && (
            <CardContainer
              numberOfCards={this.props.numberOfCards}
              cardSectionName="Completed Cards"
              cards={this.state.completedCards}
              removeCardFromCardContainer={this.removeCardFromCardContainer}
            />
          )}
          {!this.state.privateCardsEmpty && (
            <CardContainer
              numberOfCards={this.props.numberOfCards}
              cardSectionName="Private Cards"
              cards={this.state.privateCards}
              removeCardFromCardContainer={this.removeCardFromCardContainer}
            />
          )}
        </Paper>
      )
    );
  }
}

ContentContainer.propTypes = {
  numberOfCards: PropTypes.number,
  currentUser: PropTypes.object,
  cards: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    cards: state.cards.toJS()
  };
}

export default connect(mapStoreStateToProps)(ContentContainer);
