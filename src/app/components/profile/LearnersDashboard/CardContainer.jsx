import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Card from '../../cards/Card';
import { push } from 'react-router-redux';
import SearchLoaderCard from '../../search/SearchLoaderCard';

class CardContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.isNewTileCard = window.ldclient.variation('new-ui-tile-card', false);
    this.bookmarkStandalone = window.ldclient.variation('bookmark-standalone', false);
    this.isCardV3 = window.ldclient.variation('card-v3', false);
  }

  contentTabNav = (e, cardSection) => {
    e.preventDefault();
    switch (cardSection) {
      case 'Created Cards':
        this.props.cards.length > 0 && this.props.dispatch(push('/me/content'));
        break;
      case 'Bookmarked Cards':
        this.props.cards.length > 0 &&
          (this.bookmarkStandalone
            ? this.props.dispatch(push('/bookmarks'))
            : this.props.dispatch(push('/me/content/bookmarked')));
        break;
      case 'Completed Cards':
        this.props.cards.length > 0 && this.props.dispatch(push('/me/content/completed'));
        break;
      case 'Private Cards':
        this.props.cards.length > 0 && this.props.dispatch(push('/me/content/privateContent'));
        break;
      default:
        break;
    }
  };

  render() {
    let emptyStateCards = [];
    for (let i = 0; i < this.props.numberOfCards; i++) {
      emptyStateCards.push(
        <div
          key={i}
          className={`column content-cards ${
            this.props.numberOfCards === 5 ? 'custom-5-cards' : 'custom-4-cards'
          } ${this.isNewTileCard ? 'custom-cards__wide' : 'custom-cards__simple-size'}`}
        >
          <SearchLoaderCard />
        </div>
      );
    }
    return (
      <div style={{ padding: '0px 30px', margin: '0px 0px 15px 0px' }}>
        <div style={{ margin: '0px 0px 10px 0px' }}>
          <span className="header-text" role="heading" aria-level="4">
            {tr(this.props.cardSectionName)}
          </span>
          <a
            href="#"
            style={{ color: '#4a90e2', cursor: 'pointer', float: 'right' }}
            onClick={e => this.contentTabNav(e, this.props.cardSectionName)}
          >
            {tr('View All')}
          </a>
        </div>
        <div>
          {this.props.cards.length > 0 ? (
            <div className="row channel-card-wrapper">
              {this.props.cards.slice(0, this.props.numberOfCards).map(card => {
                return (
                  <div
                    key={card.id}
                    className={`channel-card-wrapper-inner column content-cards ${
                      this.props.numberOfCards === 5 ? 'custom-5-cards' : 'custom-4-cards'
                    }
                    ${
                      this.isCardV3
                        ? 'custom-cards__v3'
                        : this.isNewTileCard
                        ? 'custom-cards__wide'
                        : 'custom-cards__simple-size'
                    }`}
                  >
                    <Card
                      isCardV3={this.isCardV3}
                      toggleSearch={function() {}}
                      author={card.author && card.author}
                      card={card}
                      dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                      startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                      user={this.props.currentUser}
                      tooltipPosition="top-center"
                      moreCards={false}
                      withoutCardModal={true}
                      cardSectionName={this.props.cardSectionName}
                      removeCardFromCardContainer={this.props.removeCardFromCardContainer}
                    />
                  </div>
                );
              })}
            </div>
          ) : (
            <div className="row">{emptyStateCards}</div>
          )}
        </div>
      </div>
    );
  }
}

CardContainer.propTypes = {
  numberOfCards: PropTypes.number,
  currentUser: PropTypes.object,
  cardSectionName: PropTypes.string,
  removeCardFromCardContainer: PropTypes.func,
  cards: PropTypes.array
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(CardContainer);
