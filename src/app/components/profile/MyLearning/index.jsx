import React from 'react';
import LearningQueueContainer from '../../feed/assignments/LearningQueueContainer';

export default function(props, context) {
  return (
    <div className="row">
      <div className="vertical-spacing-large column large-8 large-offset-2 small-12">
        <LearningQueueContainer />
      </div>
    </div>
  );
}
