import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';
import { push } from 'react-router-redux';
import _ from 'lodash';
import { langs } from '../../constants/languages';

class ProfileTabs extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      tabValue: props.pathname,
      isChannelsOn: true,
      ProfileController: [],
      output: [],
      edcastWallet: this.props.team.config.wallet || false,
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      upgradeSkillsPassport:
        this.props.team &&
        this.props.team.OrgConfig &&
        !this.props.team.OrgConfig.profileShowUserContent,
      isGroupPageV2: window.ldclient.variation('group-page-v2', false),
      isCareerAdvisorOn: window.ldclient.variation('career-advisor', false)
    };

    this.groupChannelUserNewNav = window.ldclient.variation('group-channel-users-new-nav', false);

    this.tabs = [
      { path: '/me', label: 'Profile', pathsList: ['/me'] },
      { path: '/me/learners-dashboard', label: 'My Dashboard' },
      {
        path:
          (this.state.isNewProfileNavigation || this.state.isGroupPageV2) &&
          !this.groupChannelUserNewNav
            ? '/org-groups'
            : '/me/groups',
        label: 'Groups'
      },
      {
        path: '/me/content',
        label: 'Content',
        options: [
          { path: '/me/dashboard', label: 'Performance' },
          { path: '/me/content', label: 'SmartCards' }
        ],
        pathsList: [
          '/me/dashboard',
          '/me/content',
          '/me/content/Pathways',
          '/me/content/VideoStreams',
          '/me/content/SmartBites',
          '/me/content/Journeys',
          '/me/content/all',
          '/me/content/completed',
          '/me/content/privateContent',
          '/me/content/purchased',
          '/me/content/managed',
          '/me/content/bookmarked',
          '/me/content/deleted',
          '/me/content/sharedWithTeam',
          '/me/content/sharedWithMe'
        ]
      },
      {
        path: '/me/learning',
        label: 'My Learning Plan',
        pathsList: [
          '/me/learning',
          '/me/learning/all',
          '/me/learning/active',
          '/me/learning/completed',
          '/me/learning/bookmarks'
        ]
      },
      {
        path: '/me/team',
        label: 'Team',
        pathsList: ['/me/team/following', '/me/team/followers'],
        options: [{ path: '/me/leaderboard', label: 'Leaderboard' }]
      },
      { path: '/me/channels', label: 'Channels' }
    ];

    if (this.props.team.config.leaderboard && !this.state.isNewProfileNavigation) {
      this.tabs.push({ path: '/me/leaderboard', label: 'Leaderboard' });
    }

    if (this.state.isCareerAdvisorOn) {
      this.tabs.push({ path: '/me/career-advisor', label: 'Career Advisor' });
    }

    if (this.props.team.config.wallet || false) {
      this.tabs.push({ path: '/me/skill-coins', label: 'Skill Coins' });
    }
    for (let prop in langs) {
      if (
        props.currentUser &&
        props.currentUser.profile &&
        langs[prop] === props.currentUser.profile.language
      )
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
  }

  handleTabChange = (e, path) => {
    e.preventDefault();
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(tab => {
      if (currentPath === tab.path || (tab.pathsList && !!~tab.pathsList.indexOf(currentPath))) {
        value = tab.path;
      }
    });
    return value;
  };

  listTab = obj => {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.filter(tab => tab.visible);
  };

  renderTab = (tab, index, label, translatedLabel) => {
    let tabValue = this.selectTabValue(this.props.pathname);
    let tabActive = tabValue === tab.path || (tab.pathsList && !!~tab.pathsList.indexOf(tabValue));
    return (
      <a
        href="#"
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        className={`profile-tabs-item ${this.toCamelCase('me ' + tab.label)}${
          tabActive ? ' active-profile-tabs-item' : ''
        }`}
        key={index + tab.label}
        onClick={e => this.handleTabChange(e, tab.path)}
      >
        {translatedLabel || tr(label)}
      </a>
    );
  };

  toCamelCase = str => {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  };

  getProfileTabClassName = arr => {
    if (arr && arr.length && arr.length > 6) {
      if (arr.length > 8) {
        if (arr.length > 10) {
          return 'profile-tabs profile-tabs-more-than-10';
        } else {
          return 'profile-tabs profile-tabs-more-than-8';
        }
      } else {
        return 'profile-tabs profile-tabs-more-than-6';
      }
    } else {
      return 'profile-tabs';
    }
  };

  render() {
    let output = [];
    const { profile } = this.props.team.OrgConfig;
    let hasDasboardSetting = profile && profile['web/profile/myDashboard'];
    let ProfileController = this.listTab(profile);
    let isChannelsOn = Boolean(
      ProfileController.find(item => item.key === 'web/profile/channels' && item.visible)
    );
    const maxIndex = Math.max(..._.map(profile, 'index'));
    if (
      this.props.team.OrgConfig.profile['web/profile/teams'].visible &&
      !this.state.isNewProfileNavigation
    ) {
      ProfileController.push({
        defaultLabel: 'Leaderboard',
        label: '',
        visible: true,
        index: maxIndex + 1,
        key: 'web/profile/leaderboard'
      });
    }

    ProfileController.map(item => {
      let translatedLabel =
        this.isShowCustomLabels &&
        item.languages &&
        item.languages[this.profileLanguage] &&
        item.languages[this.profileLanguage].trim();
      let label =
        item.label ||
        (item.defaultLabel === 'Learning Queue' ? 'My Learning Plan' : item.defaultLabel);
      switch (item.key) {
        case 'web/profile/profile':
          output.push(this.renderTab(this.tabs[0], item.index, label, translatedLabel));
          !hasDasboardSetting &&
            output.push(this.renderTab(this.tabs[1], item.index + 1, this.tabs[1].label));
          break;
        case 'web/profile/myDashboard':
          output.push(this.renderTab(this.tabs[1], item.index, label, translatedLabel));
          break;
        case 'web/profile/groups':
          if (!this.state.isNewProfileNavigation || this.groupChannelUserNewNav) {
            output.push(this.renderTab(this.tabs[2], item.index, label, translatedLabel));
          }
          break;
        case 'web/profile/content':
          output.push(this.renderTab(this.tabs[3], item.index, label, translatedLabel));
          break;
        case 'web/profile/learningQueue':
          output.push(this.renderTab(this.tabs[4], item.index, label, translatedLabel));
          break;
        case 'web/profile/teams':
          if (!this.state.isNewProfileNavigation || this.groupChannelUserNewNav) {
            output.push(this.renderTab(this.tabs[5], item.index, label, translatedLabel));
          }
          break;
        case 'web/profile/channels':
          if (isChannelsOn && this.groupChannelUserNewNav) {
            output.push(this.renderTab(this.tabs[6], item.index, label, translatedLabel));
          }
          break;
        case 'web/profile/leaderboard':
          if (this.props.team.config.leaderboard && !this.state.isNewProfileNavigation) {
            output.push(this.renderTab(this.tabs[7], item.index, label, translatedLabel));
          }
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });

    let skillCoinIndex;
    if (this.props.team.config.leaderboard && !this.state.isNewProfileNavigation) {
      if (this.state.isCareerAdvisorOn) {
        output.push(this.renderTab(this.tabs[8], 8, this.tabs[8].label));
        skillCoinIndex = 9;
      } else {
        skillCoinIndex = 8;
      }
    } else {
      if (this.state.isCareerAdvisorOn) {
        output.push(this.renderTab(this.tabs[7], 7, this.tabs[7].label));
        skillCoinIndex = 8;
      } else {
        skillCoinIndex = 7;
      }
    }

    if (this.state.edcastWallet) {
      output.push(
        this.renderTab(this.tabs[skillCoinIndex], skillCoinIndex, this.tabs[skillCoinIndex].label)
      );
      skillCoinIndex = 10;
    }
    if (this.state.upgradeSkillsPassport) {
      output.push(this.renderTab(this.tabs[3], skillCoinIndex, this.tabs[3].label));
    }

    return <div className={this.getProfileTabClassName(output)}>{output}</div>;
  }
}

ProfileTabs.propTypes = {
  pathname: PropTypes.string,
  team: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    topNav: state.config.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(ProfileTabs);
