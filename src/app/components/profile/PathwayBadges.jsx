import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Paper from 'edc-web-sdk/components/Paper';
import { fetchUserBadges } from 'edc-web-sdk/requests/badges';
import BadgesListModal from '../modals/BadgesListModal';
import { setUserBadges } from '../../actions/currentUserActions';
import { users } from 'edc-web-sdk/requests';

class PathwayBadges extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      badgesList: [],
      openModal: false,
      totalCount: 0,
      badgesExpand: false,
      fetchingAllBadges: false,
      userBadgings:
        this.props.team &&
        this.props.team.OrgConfig &&
        this.props.team.OrgConfig.badgings['web/badgings']
    };

    this.styles = {
      shareToLinkedIn: {
        display: 'inline-block',
        fontSize: '12px',
        cursor: 'pointer',
        zIndex: '2',
        bottom: '24px',
        top: '80px',
        right: '0%',
        fontWeight: '600',
        color: '#3b5998'
      },
      linkedInLogo: {
        height: '15px',
        width: '20px',
        marginLeft: '5px',
        marginBottom: '2px'
      },
      badgeTitle: {
        fontSize: '14px',
        width: '100%',
        margin: '0 auto',
        paddingTop: '8px'
      }
    };
    this.clcBadgesLimit = 6;
    this.cardBadgesLimit = 6;
  }

  getUserBadges = (badgeType, limit, offset) => {
    return new Promise(resolve => {
      fetchUserBadges(this.props.currentUser.id, { limit, offset, type: badgeType })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          console.error(`Error in PathwayBadges.fetchUserBadges.func : ${err}`);
        });
    });
  };

  async fetchAllUserBadges(loadMore) {
    let [cardBadges, clcBadges] = await Promise.all([
      (this.state.userBadgings &&
        this.state.userBadgings.pathwayBadgings &&
        this.getUserBadges('CardBadge', this.cardBadgesLimit, 0)) || { userBadges: [] },
      (this.state.userBadgings &&
        this.state.userBadgings.clcBadgings &&
        this.getUserBadges('ClcBadge', this.clcBadgesLimit, 0)) || { userBadges: [] }
    ]);
    let badgesList = cardBadges.userBadges.concat(clcBadges.userBadges);
    badgesList = loadMore ? badgesList : badgesList.slice(0, 6);
    this.clcBadgesLimit = clcBadges.totalCount;
    this.cardBadgesLimit = cardBadges.totalCount;
    this.setState({
      badgesList,
      totalCount: (cardBadges.totalCount || 0) + (clcBadges.totalCount || 0),
      badgesExpand: loadMore ? !this.state.badgesExpand : false,
      fetchingAllBadges: false
    });
    this.props.dispatch(setUserBadges(this.state.badgesList, 'CardBadge'));
  }

  componentDidMount() {
    if (!this.props.publicProfile) {
      this.fetchAllUserBadges(false);
    } else {
      let cardBadges =
        this.state.userBadgings &&
        this.state.userBadgings.pathwayBadgings &&
        this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userCardBadges;

      let clcBadges =
        this.state.userBadgings &&
        this.state.userBadgings.clcBadgings &&
        this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userClcBadges;
      this.setState({
        badgesList: (cardBadges || []).concat(clcBadges || []),
        totalCount:
          ((cardBadges && cardBadges.length) || 0) + ((clcBadges && clcBadges.length) || 0)
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.props.currentUser.publicProfile &&
      !nextProps.currentUser.publicProfile &&
      this.fetchAllUserBadges(false);
  }

  toggleModal = () => {
    let openModal = !this.state.openModal;
    this.setState({ openModal });
    document.body.style.overflow = openModal ? 'hidden' : '';
  };

  shareOnLinkedIn = (identifier, badge_title) => {
    let LinkedInUrl = 'https://www.linkedin.com/shareArticle';
    let mini = 'mini=true';
    let title =
      'title=' +
      'Posted from EdCast: ' +
      this.props.currentUser.name +
      ' earned a badge for ' +
      badge_title;
    let shared_url = 'url=' + window.location.origin + '/verify_badge/' + identifier;
    let source = 'edCast';
    let url = LinkedInUrl + '?' + mini + '&' + title + '&' + shared_url + '&' + source;
    window.open(encodeURI(url), 'Share on Linkedin', 'height=520,width=570');
  };

  truncateMessageText = message => {
    if (message.length > 25) return message.substr(0, 25) + '...';
    else return message;
  };

  expandBioSection = () => {
    this.setState({
      fetchingAllBadges: true
    });
    !this.state.badgesExpand && this.state.badgesList.length < this.state.totalCount
      ? this.fetchAllUserBadges(true)
      : this.setState({ badgesExpand: !this.state.badgesExpand, fetchingAllBadges: false });
  };

  render() {
    let showShareonLinkedIn =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.badgings &&
      !!this.props.team.OrgConfig.badgings['web/badgings'] &&
      !!this.props.team.OrgConfig.badgings['web/badgings'].sharePathwayBadgesonLinkedIn;

    if (this.state.badgesList && this.state.badgesList.length == 0) {
      return <div />;
    }
    let userDetails = this.props.currentUser.publicProfile;
    let userClcBadgesCount =
      userDetails && userDetails.userClcBadges && userDetails.userClcBadges.length;
    let userCardBadgesCount =
      userDetails && userDetails.userCardBadges && userDetails.userCardBadges.length;
    let badgesList = this.state.badgesExpand
      ? this.state.badgesList
      : this.state.badgesList.slice(0, 6);

    return (
      <div className="profile-box">
        {((!!this.state.totalCount &&
          !!this.props.currentUser.userBadges &&
          !!this.props.currentUser.userBadges[this.props.type]) ||
          this.props.publicProfile) && (
          <Paper className="container-padding badges-block">
            <div id="pathwayBadgesProfile">
              <div className="row">
                <div className="column small-6">
                  <div className="header-text">
                    {tr('Badges')} ({this.state.totalCount})
                  </div>
                </div>
              </div>
              <div className="row">
                {badgesList.map(badge => {
                  return (
                    <div
                      key={badge.id}
                      className="badge-block-item column large-2 medium-4 small-6"
                      style={{ position: 'relative' }}
                    >
                      <img src={badge.imageUrl} />
                      <div>
                        <p title={badge.title} style={this.styles.badgeTitle}>
                          {this.truncateMessageText(badge.title)}
                        </p>
                        {showShareonLinkedIn && !this.props.publicProfile && (
                          <span
                            style={this.styles.shareToLinkedIn}
                            onClick={this.shareOnLinkedIn.bind(this, badge.identifier, badge.title)}
                          >
                            {tr('Share it on')}
                            <span>
                              <img
                                style={this.styles.linkedInLogo}
                                src="https://d1iwkfmdo6oqxx.cloudfront.net/organizations/co_branding_logos/000/001/454/original/LinkedInLogo.png"
                              />
                            </span>
                          </span>
                        )}
                      </div>
                    </div>
                  );
                })}
              </div>
              {this.state.totalCount > 6 && (
                <div className="show-more">
                  {this.state.fetchingAllBadges ? (
                    <span>Loading...</span>
                  ) : (
                    <span onClick={this.expandBioSection.bind(this)}>
                      {this.state.badgesExpand ? 'Show less...' : 'Show more...'}{' '}
                      <i
                        className="arrow-icon"
                        style={{
                          borderWidth: this.state.badgesExpand
                            ? '0px 2px 2px 0px'
                            : '2px 0px 0px 2px'
                        }}
                      />
                    </span>
                  )}
                </div>
              )}
              {this.state.openModal && (
                <BadgesListModal closeModal={this.toggleModal} type={this.props.type} />
              )}
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

PathwayBadges.propTypes = {
  type: PropTypes.string,
  pathname: PropTypes.string,
  publicProfile: PropTypes.bool,
  redirectUrl: PropTypes.func,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    team: state.team.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(PathwayBadges);
