/**
Edge Data routes
**/

var request = require('superagent');
const edgeUser = 'edcast-dem0-user';
const edgePass = 'Edcast-Dem0-Edge!';

module.exports = function(app) {
  app.post('/wapi/edge/data', edgeData);

  function edgeData(req, res) {
    request
      .post('https://edcast-demo.hirealchemy.com/api/v1/profile/skillgap')
      .auth(edgeUser, edgePass)
      .set('Content-Type', 'application/json')
      .send(req.body)
      /*eslint handle-callback-err: "off"*/
      .end(function(err, resp) {
        res.json(resp.text);
      });
  }
};
