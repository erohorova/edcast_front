import agent from 'superagent';

export function getLinkedInData({ token = null }) {
  if (!token) {
    return {};
  }
  return new Promise((resolve, reject) => {
    agent
      .get(`/wapi/linkedin/profile`)
      .query({ token })
      .then(res => {
        try {
          let role = res.body.positions.values[0].title;
          resolve({ role });
        } catch (e) {
          resolve({ role: null });
        }
      })
      .catch(err => {
        console.error(`Error in linkedIn.getLinkedInData.func : ${err}`);
      });
  });
}
