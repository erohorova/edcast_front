import agent from 'superagent';
import { EDGE } from '../edge_data';

const authUser = 'edcast';
const authPass = '8e944092-9d77-42af-9621-960fba17499f';
const sovrenAccountId = '72920302';
const sovrenServiceKey = 'UURCzVaiFuqu2vy4tbqjzodsgSo/t1l1cuL92Lz6';
const URI = 'https://emsiservices.com/jpa';

let date = new Date();
// There will be no data for the current month
date.setMonth(-1);
// getMonth() is 0 offset in JS
// We need to be in format 0N for month
const endDate = `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}`;
// subtract 6 months
date.setMonth(date.getMonth() - 5);
const startDate = `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}`;

export function getStatus(query) {
  agent
    .get(`${URI}/status`)
    .auth(authUser, authPass)
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.getStatus.func : ${err}`);
    });
}

export function meta() {
  agent
    .get(`${URI}/meta`)
    .auth(authUser, authPass)
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.meta.func : ${err}`);
    });
}

export function searchTitles({ title, autocomplete = true }) {
  return new Promise((resolve, reject) => {
    agent
      .get(`${URI}/taxonomies/title`)
      .auth(authUser, authPass)
      .query({ q: title, autocomplete, limit: 5 })
      .then(res => {
        resolve(res.body.data || []);
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.searchTitles.func : ${err}`);
      });
  });
}

export function searchSkills({ title, autocomplete = true }) {
  return new Promise((resolve, reject) => {
    agent
      .get(`${URI}/taxonomies/skills`)
      .auth(authUser, authPass)
      .query({ q: title, autocomplete, limit: 1 })
      .then(res => {
        resolve(res.body.data || []);
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.searchSkills.func : ${err}`);
      });
  });
}

export function getHardSkills({ title }) {
  agent
    .post(`${URI}/rankings/hard_skills_name`)
    .auth(authUser, authPass)
    .send({
      filter: {
        when: {
          start: startDate,
          end: endDate
        },
        title: [...title],
        keywords: {
          query: 'team learning'
        }
      },
      rank: {
        by: 'unique_postings',
        limit: 20,
        extra_metrics: ['duplicate_postings']
      }
    })
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.getHardSkills.func : ${err}`);
    });
}

export function getSoftSkills({ title }) {
  agent
    .post(`${URI}/rankings/soft_skills_name`)
    .auth(authUser, authPass)
    .send({
      filter: {
        when: {
          start: startDate,
          end: endDate
        },
        title: [...title],
        keywords: {
          query: 'team learning'
        }
      },
      rank: {
        by: 'unique_postings',
        limit: 20,
        extra_metrics: ['duplicate_postings']
      }
    })
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.getSoftSkills.func : ${err}`);
    });
}

export function getSkills({ title }) {
  return new Promise((resolve, reject) => {
    agent
      .post(`${URI}/rankings/skills_name`)
      .auth(authUser, authPass)
      .send({
        filter: {
          when: {
            start: startDate,
            end: endDate
          },
          title_name: [...title],
          keywords: {
            query: 'team learning'
          }
        },
        rank: {
          by: 'unique_postings',
          limit: 5
        }
      })
      .then(res => {
        try {
          resolve(res.body.data.ranking.buckets);
        } catch (e) {
          resolve([]);
        }
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getSkills.func : ${err}`);
      });
  });
}

export function getCertifications({ title }) {
  agent
    .post(`${URI}/rankings/certifications_name`)
    .auth(authUser, authPass)
    .send({
      filter: {
        when: {
          start: startDate,
          end: endDate
        },
        title: [...title],
        keywords: {
          query: 'team learning'
        }
      },
      rank: {
        by: 'unique_postings',
        limit: 20,
        extra_metrics: ['duplicate_postings']
      }
    })
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.getCertifications.func : ${err}`);
    });
}

export function getEducation({ title }) {
  agent
    .post(`${URI}/rankings/edulevels_name`)
    .auth(authUser, authPass)
    .send({
      filter: {
        when: {
          start: startDate,
          end: endDate
        },
        title: [...title],
        keywords: {
          query: 'team learning'
        }
      },
      rank: {
        by: 'unique_postings', // || significance || total_postings
        limit: 20,
        extra_metrics: ['duplicate_postings']
      }
    })
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      console.error(`Error in CareerAdvisor.api.getEducation.func : ${err}`);
    });
}

export function getSkillsTimeseries({ names }) {
  return new Promise((resolve, reject) => {
    agent
      .post(`${URI}/timeseries`)
      .auth(authUser, authPass)
      .send({
        filter: {
          when: {
            start: startDate,
            end: endDate
          },
          skills_name: [...names],
          keywords: {
            query: 'team learning'
          }
        },
        metrics: ['unique_postings']
      })
      .then(res => {
        try {
          resolve(res.body.data.timeseries);
        } catch (e) {
          resolve({});
        }
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getSkillsTimeseries.func : ${err}`);
      });
  });
}

export function getJobsTimeseries({ names }) {
  return new Promise((resolve, reject) => {
    agent
      .post(`${URI}/timeseries`)
      .auth(authUser, authPass)
      .send({
        filter: {
          when: {
            start: startDate,
            end: endDate
          },
          title_name: [...names],
          keywords: {
            query: 'team learning'
          }
        },
        metrics: ['unique_postings']
      })
      .then(res => {
        try {
          resolve(res.body.data.timeseries);
        } catch (e) {
          resolve({});
        }
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getJobsTimeseries.func : ${err}`);
      });
  });
}

export function getIndustryIds({ title }) {
  return new Promise((resolve, reject) => {
    agent
      .post(`${URI}/rankings/naics2`)
      .auth(authUser, authPass)
      .send({
        filter: {
          when: {
            start: startDate,
            end: endDate
          },
          title_name: [...title]
        },
        rank: {
          by: 'unique_postings'
        }
      })
      .then(res => {
        try {
          resolve(res.body.data.ranking.buckets);
        } catch (e) {
          resolve([]);
        }
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getIndustryIds.func : ${err}`);
      });
  });
}

export function getIndustries({ ids }) {
  return new Promise((resolve, reject) => {
    agent
      .post(`${URI}/taxonomies/naics2/lookup`)
      .auth(authUser, authPass)
      .send({
        ids
      })
      .then(res => {
        resolve(res.body.data || []);
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getIndustries.func : ${err}`);
      });
  });
}

export function getResumeData(data) {
  return new Promise((resolve, reject) => {
    agent
      .post(`https://rest.resumeparsing.com/v8/parser/resume`)
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Sovren-AccountId', sovrenAccountId)
      .set('Sovren-ServiceKey', sovrenServiceKey)
      .send(data)
      .then(res => {
        resolve(res.body || {});
      })
      .catch(err => {
        console.error(`Error in CareerAdvisor.api.getResumeData.func : ${err}`);
      });
  });
}

export function getEdgeData(data) {
  return EDGE[data.aspirational_role];
}
