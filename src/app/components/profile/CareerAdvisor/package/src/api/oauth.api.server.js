/**
Box OAuth routes
**/

var request = require('superagent');

module.exports = function(app) {
  app.use('/wapi/linkedin/auth', liAuth);
  app.use('/wapi/linkedin/token', liToken);
  app.use('/wapi/linkedin/profile', liProfile);

  // Send client to LinkedIn OAuth login
  function liAuth(req, res) {
    let org = req.hostname.split('.')[0];
    let redirectUri = `https://${app.config['LINKEDIN_REDIRECT']}/wapi/linkedin/token`;
    let authUri = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${
      app.config['!linkedin.client_id']
    }&state=${org}&security_token%3DKnhMJatFipTAnM0nHlZA&redirect_uri=${redirectUri}&scope=r_basicprofile`;
    res.redirect(authUri);
  }

  function liToken(req, res) {
    let orgUrl = [req.query.state, ...req.hostname.split('.').slice(1)].join('.');
    let returnUrl = `https://${orgUrl}/me/career-advisor`;
    if (!req.query.code) {
      return res.redirect(returnUrl);
    }
    let params = {
      grant_type: 'authorization_code',
      code: req.query.code,
      client_id: app.config['!linkedin.client_id'],
      client_secret: app.config['!linkedin.client_secret'],
      redirect_uri: `https://${app.config['LINKEDIN_REDIRECT']}/wapi/linkedin/token`
    };
    request
      .post('https://www.linkedin.com/oauth/v2/accessToken')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send(params)
      .end(function(err, resp) {
        if (err) {
          return res.redirect(returnUrl);
        }
        res.redirect(`${returnUrl}?scope=linkedin&access_token=${resp.body.access_token}`);
      });
  }

  function liProfile(req, res) {
    request
      .get(`https://api.linkedin.com/v1/people/~:(positions,specialties,summary)`)
      .query({ format: 'json', oauth2_access_token: req.query.token })
      /*eslint handle-callback-err: "off"*/
      .end(function(err, resp) {
        res.json(resp.body);
      });
  }
};
