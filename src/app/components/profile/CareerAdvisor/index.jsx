import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Select, { Async } from 'react-select';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import find from 'lodash/find';
import colors from 'edc-web-sdk/components/colors/index';

require('./package/datalist.polyfill.js');
import { base64ArrayBuffer } from './package/utils.js';
import './package/src/styles/styles.scss';
import { getLinkedInData } from './package/src/api/linkedin';
// import edxLogo from './package/src/components/logos/edx_logo.png';
// import courseraLogo from './package/src/components/logos/coursera_logo.png';
// import pluralsightLogo from './package/src/components/logos/pluralsight_logo.png';
// import udemyLogo from './package/src/components/logos/udemy_logo.png';
// import edxBtn from './package/src/components/assets/edx.png';
// import mettlBtn from './package/src/components/assets/mettl.png';
import { getContentFromProviders } from 'edc-web-sdk/requests/ecl';
import { EDGE } from './package/src/edge_data';
import * as EMSI from './package/src/api';
let LocaleCurrency = require('locale-currency');

function debounce(fn, delay) {
  var timer = null;
  return function() {
    var context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function() {
      fn.apply(context, args);
    }, delay);
  };
}

const colorSet = [
  'rgb(69,68,94)',
  'rgb(143,119,196)',
  'rgb(91,180,160)',
  'rgb(125,229,206)',
  'rgb(86,95,196)'
];

class CareerAdvisorContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      titles: [],
      industries: [],
      skills: [],
      resumeSkills: localStorage.getItem('ca_resumeSkills')
        ? localStorage.getItem('ca_resumeSkills').split(',')
        : [],
      selectedJobTitle: localStorage.getItem('ca_selectedJobTitle'),
      aspiringJobTitle: localStorage.getItem('ca_aspiringJobTitle'),
      uploadingResume: false,
      uploadingLinkedIn: false,
      edgeData: localStorage.getItem('ca_edgeData')
        ? JSON.parse(localStorage.getItem('ca_edgeData'))
        : null,
      fitament: localStorage.getItem('ca_fitament'),
      skillGap: localStorage.getItem('ca_skillGap')
        ? localStorage.getItem('ca_skillGap').split(',')
        : null,
      currentRoleCourses: [],
      currentCoursesLimit: 3,
      aspiringRoleCourses: [],
      aspiringRoleChannels: [],
      aspiringRolePathways: [],
      aspiringCoursesLimit: 3,
      aspiringRoleChannelsLimit: 3,
      aspiringRolePathwaysLimit: 3,
      assessmentTaken: localStorage.getItem('ca_assessmentTaken'),
      edcastPricing: !!(
        this.props.team &&
        this.props.team.config &&
        this.props.team.config.enable_smart_card_price_field
      ),
      edcastPlansForPricing: window.ldclient.variation('edcast-pricing-plans', false),
      step:
        localStorage.getItem('ca_selectedJobTitle') && localStorage.getItem('ca_aspiringJobTitle')
          ? 3
          : 1,
      gettingEdgeData: false,
      selectedSkill: null,
      loadingSkills: false,
      enableBIA: this.props.team.config.enabled_bia
    };
  }

  componentWillMount() {
    this.loadChartJS();
  }

  componentDidMount() {
    if (this.props.location.query.access_token) {
      this.setState(
        {
          uploadingLinkedIn: true
        },
        () => {
          this.getLinkedInData();
        }
      );
    }

    if (localStorage.getItem('ca_aspiringJobTitle')) {
      // this.getAspiringRoles();
      this.getSkillsForAspiringRole();
    }
  }

  getLinkedInData = async () => {
    let titleToSet = this.state.selectedJobTitle;
    let role = await getLinkedInData({ token: this.props.location.query.access_token });
    let newTitles = await EMSI.searchTitles({ title: role.role });
    if (newTitles.length > 0) {
      titleToSet = newTitles[0].name;
    }
    this.setState(
      {
        selectedJobTitle: titleToSet,
        titles: [],
        uploadingLinkedIn: false
      },
      () => {
        if (this.refs.current_role) {
          window.ttt = this.refs.current_role;
        }
        this.getSkillsForTitle();
      }
    );
  };

  loadChartJS() {
    if (!window.Chart) {
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js';
      document.getElementsByTagName('head')[0].appendChild(script);
    }
  }

  getTitles = debounce(async (title, callback) => {
    if (!title && !this.state.selectedJobTitle) {
      return callback(null, {});
    } else if (!title && this.state.selectedJobTitle && this.state.titles.length === 0) {
      // this.getSkillsForTitle();
      return callback(null, {
        options: [{ value: this.state.selectedJobTitle, label: this.state.selectedJobTitle }]
      });
    } else {
      let titles = await EMSI.searchTitles({ title });
      let options = titles.map(t => {
        return { value: t.name, label: t.name };
      });
      return callback(null, { options });
    }
  }, 300);

  getAspiringRoles = (title, callback) => {
    let options = Object.keys(EDGE).map(k => {
      return { value: k, label: k };
    });
    callback(null, { options });
  };

  handleAspiringRoleChange = input => {
    this.setState(
      {
        aspiringJobTitle: input.value,
        edgeData: EDGE[input.value]
      },
      () => {
        // Save item
        localStorage.setItem('ca_aspiringJobTitle', input.value);
        localStorage.setItem('ca_edgeData', JSON.stringify(EDGE[input.value]));
        // this.getAspiringRoleCourses();
      }
    );
  };

  // Capture the input change events, if it matches a selected item, stop searching
  handleTitleChange = (input, setting = 'selectedJobTitle') => {
    let state = {
      titles: []
    };
    state[setting] = input.value;
    this.setState(state, () => {
      if (setting === 'selectedJobTitle') {
        // this.getSkillsForTitle(setting);
        localStorage.setItem('ca_selectedJobTitle', input.value);
        // this.getCurrentRoleCourses();
      }
    });
  };

  async getSkillsForAspiringRole() {
    let skills = [];
    let skillsDatasets = [];
    let skillsLabels = [];
    let colorIndex = -1;
    for (let skill of this.state.skillGap) {
      let skillName = await EMSI.searchSkills({ title: skill });
      if (skillName.length > 0) {
        let skillTS = await EMSI.getSkillsTimeseries({ names: [skillName[0]['name']] });
        let struct = {
          label: `${skill.toUpperCase()}`,
          lineTension: 0.1,
          fill: false,
          borderColor: colorSet[++colorIndex],
          data: []
        };
        let n = 0;
        for (let ts of skillTS.month) {
          if (!~skillsLabels.indexOf(skillTS.month[n])) {
            skillsLabels.push(skillTS.month[n]);
          }
          struct.data.push(skillTS.unique_postings[n]);
          n++;
        }
        skillsDatasets.push(struct);
      }
    }
    this.setState({ skills }, () => {
      let ctx = document.querySelector('#trendingSkillsGraph');
      var skillGraphs = new window.Chart(ctx, {
        type: 'line',
        data: {
          labels: skillsLabels,
          datasets: skillsDatasets
        },
        options: {}
      });
    });
  }

  async getSkillsForTitle(title = 'selectedJobTitle') {
    let reqSkills = await EMSI.getSkills({ title: [this.state[title]] });
    let skillsDatasets = [];
    let skillsLabels = [];
    let colorIndex = -1;
    let skills = [...reqSkills];
    // if(title === 'aspiringJobTitle') {
    //   skills = [...this.state.skills, ...reqSkills];
    // }
    for (let skill of skills) {
      let newSkill =
        this.state.aspiringJobTitle && reqSkills.filter(s => s.name === skill.name).length > 0
          ? true
          : false;
      let skillTS = await EMSI.getSkillsTimeseries({ names: [skill.name] });
      let struct = {
        label: `${newSkill ? 'NEW: ' : ''}${skill.name}`,
        lineTension: 0.1,
        fill: false,
        borderColor: colorSet[++colorIndex],
        data: []
      };
      let n = 0;
      for (let ts of skillTS.month) {
        if (!~skillsLabels.indexOf(skillTS.month[n])) {
          skillsLabels.push(skillTS.month[n]);
        }
        struct.data.push(skillTS.unique_postings[n]);
        n++;
      }
      skillsDatasets.push(struct);
    }
    this.setState({ skills }, () => {
      let ctx = document.querySelector('#trendingSkillsGraph');
      var skillGraphs = new window.Chart(ctx, {
        type: 'line',
        data: {
          labels: skillsLabels,
          datasets: skillsDatasets
        },
        options: {}
      });
      // this.getJobsTimeseries();
    });
  }

  // async getJobsTimeseries() {
  //   let jobsTS = await EMSI.getJobsTimeseries({names: [this.state.selectedJobTitle]});
  //   let jobsDatasets = [];
  //   let jobsLabels = [];
  //   let struct = {
  //     label: this.state.selectedJobTitle,
  //     lineTension: 0.1,
  //     fill: true,
  //     borderColor: colorSet[2],
  //     backgroundColor: colorSet[3],
  //     data: []
  //   };
  //   let n = 0;
  //   for(let ts of jobsTS.month) {
  //     if(!~jobsLabels.indexOf(jobsTS.month[n])) {
  //       jobsLabels.push(jobsTS.month[n]);
  //     }
  //     struct.data.push(jobsTS.unique_postings[n]);
  //     n++;
  //   }
  //   jobsDatasets.push(struct);
  //   let ctx = document.querySelector('#trendingJobsGraph');
  //   var jobsGraph = new Chart(ctx, {
  //     "type": "line",
  //     "data": {
  //       "labels": jobsLabels,
  //       "datasets": jobsDatasets
  //     },
  //     "options": {}
  //   });
  // }

  clickResumeUpload = () => {
    document.querySelector('#resumeInput').click();
  };

  linkedInAuthStart = () => {
    document.location.href = '/wapi/linkedin/auth';
  };

  handleResumeUpload = e => {
    if (e.target.files.length) {
      let file = e.target.files[0];
      let reader = new FileReader();
      this.setState(
        {
          uploadingResume: true
        },
        () => {
          reader.onload = async () => {
            let b64 = base64ArrayBuffer(reader.result);
            let data = {
              DocumentAsBase64String: b64
            };

            // Store resume
            localStorage.setItem('current_resume', b64);

            let results = await EMSI.getResumeData(data);
            let parsed = results.Value.ParsedDocument;
            try {
              parsed = JSON.parse(parsed);
            } catch (e) {
              console.log('JSON parse error in index');
            }
            let currentTitle = null;
            let resumeSkills = [];
            let titleToSet = this.state.selectedJobTitle;

            try {
              let arr =
                parsed.Resume.UserArea['sov:ResumeUserArea']['sov:ExperienceSummary'][
                  'sov:SkillsTaxonomyOutput'
                ]['sov:TaxonomyRoot'][0]['sov:Taxonomy'];
              let n1 = [];
              let n2 = [];
              let n3 = [];
              arr.map(a => {
                n1.push(a['@name'].toLowerCase());
                a['sov:Subtaxonomy'].map(b => {
                  n2.push(b['@name'].toLowerCase());
                  b['sov:Skill'].map(c => {
                    n3.push(c['@name'].toLowerCase());
                  });
                });
              });
              resumeSkills = [...new Set([...n1, ...n2, ...n3])];
              /*eslint no-shadow: "off"*/
            } catch (e) {}
            try {
              currentTitle =
                parsed.Resume.StructuredXMLResume.EmploymentHistory.EmployerOrg[0]
                  .PositionHistory[0].Title;
              /*eslint no-shadow: "off"*/
            } catch (e) {}

            if (currentTitle) {
              let newTitles = await EMSI.searchTitles({ title: currentTitle });
              if (newTitles.length > 0) {
                titleToSet = newTitles[0].name;
              }
            }

            // Store data
            localStorage.setItem('ca_selectedJobTitle', titleToSet);
            localStorage.setItem('ca_resumeSkills', resumeSkills);

            this.setState(
              {
                uploadingResume: false,
                selectedJobTitle: titleToSet,
                resumeSkills,
                titles: []
              },
              () => {
                // this.getSkillsForTitle();
                // this.getCurrentRoleCourses();
              }
            );
          };
          reader.readAsArrayBuffer(file);
        }
      );
    }
  };

  getEdgeData = async () => {
    let cv = localStorage.getItem('current_resume');
    let fitament = 50; //default value
    let gap = this.state.edgeData.hard_skills.slice(0, 5); //default value
    if (cv) {
      let payload = {
        current_role: this.state.selectedJobTitle,
        aspirational_role: this.state.aspiringJobTitle,
        binary: cv
      };

      let resp = await EMSI.getEdgeData(payload);
      let data = resp;
      if (data.hard_skills) {
        gap = data.hard_skills.slice(0, 5);
      }
    }

    localStorage.setItem('ca_fitament', fitament);
    localStorage.setItem('ca_skillGap', gap);
    this.setState(
      {
        fitament: fitament,
        skillGap: gap,
        gettingEdgeData: false
      },
      () => this.getSkillsForAspiringRole()
    );
  };

  // getCurrentRoleCourses = async () => {
  //   let payload = {
  //     "content_type[]": 'course',
  //     q: this.state.selectedJobTitle,
  //     limit: 3,
  //     offset: 0,
  //     cards_limit: 39,
  //     cards_offset: 0,
  //     segregate_pathways: true,
  //     load_tags: false,
  //     load_mentions: false
  //   };
  //
  //   let resp = await getContentFromProviders(payload);
  //   this.setState({
  //     currentRoleCourses: resp.cards
  //   });
  // }

  // loadMoreCurrentCourses = () => {
  //   this.setState({
  //     currentCoursesLimit: this.state.currentCoursesLimit + 3
  //   });
  // }

  getAspiringRoleCourses = async () => {
    let payload = {
      // "content_type[]": 'course',
      // q: this.state.aspiringJobTitle,
      q: this.state.selectedSkill,
      limit: 3,
      offset: 0,
      cards_limit: 39,
      cards_offset: 0,
      segregate_pathways: true,
      load_tags: false,
      load_mentions: false
    };

    this.setState({ loadingSkills: true });
    let resp = await getContentFromProviders(payload);
    this.setState({
      aspiringRoleCourses: resp.cards.filter(c => c.cardType === 'course'),
      aspiringRoleChannels: resp.channels,
      aspiringRolePathways: resp.pathways,
      loadingSkills: false
    });
  };

  loadMoreAspiringCourses = () => {
    this.setState({
      aspiringCoursesLimit: this.state.aspiringCoursesLimit + 3
    });
  };

  loadMoreAspiringRoleChannels = () => {
    this.setState({
      aspiringRoleChannelsLimit: this.state.aspiringRoleChannelsLimit + 3
    });
  };

  loadMoreAspiringRolePathways = () => {
    this.setState({
      aspiringRolePathwaysLimit: this.state.aspiringRolePathwaysLimit + 3
    });
  };

  mockAssessment = () => {
    localStorage.setItem('ca_assessmentTaken', new Date().toLocaleString());
    this.setState({
      assessmentTaken: new Date().toLocaleString()
    });
  };

  selectSkillStep = skill => {
    this.setState(
      {
        selectedSkill: skill,
        loadingSkills: true
      },
      () => {
        this.getAspiringRoleCourses();
      }
    );
  };

  nextStep = ({ step = null }) => {
    this.setState({
      step: 3,
      gettingEdgeData: true
    });
    this.getEdgeData();
  };

  stepSection = () => {
    return (
      <div className="flex f100 columns">
        <div className="col ca-block p0">
          <div className="step-selection">
            <div className={`step ${this.state.step === 1 ? 'active' : 'completed'}`}>
              <span onClick={() => this.nextStep({ step: 1 })}>1. {tr('Current role')}</span>
            </div>
            <div
              className={`step ${
                this.state.step === 2 ? 'active' : this.state.step > 2 ? 'completed' : ''
              }`}
            >
              <span onClick={() => this.nextStep({ step: 2 })}>2. {tr('Aspiring role')}</span>
            </div>
            <div className={`step ${this.state.step === 3 ? 'active' : ''}`}>
              3. {tr('Career path')}
            </div>
          </div>
          {this.state.step === 1 && (
            <div className="select-column">
              {this.state.uploadingResume ? (
                <input
                  className="loading-resume-placeholder"
                  type="text"
                  placeholder="Please wait..."
                />
              ) : (
                <Async
                  ref="current_role"
                  multi={false}
                  value={this.state.selectedJobTitle}
                  autoload={true}
                  onChange={this.handleTitleChange}
                  loadOptions={this.getTitles}
                  placeholder={tr('Enter Current Role or Upload Resume')}
                  backspaceRemoves={true}
                />
              )}
              <input
                type="file"
                id="resumeInput"
                style={{ display: 'none' }}
                onChange={this.handleResumeUpload}
              />
              <button
                className="step-upload-resume"
                onClick={this.clickResumeUpload}
                disabled={this.state.uploadingResume}
              >
                ⬆
              </button>
              <button
                className="step-next"
                onClick={this.nextStep}
                disabled={!this.state.selectedJobTitle}
              >
                {tr('Next')}
              </button>
            </div>
          )}
          {this.state.step === 2 && (
            <div className="select-column">
              <Async
                multi={false}
                value={this.state.aspiringJobTitle}
                autoload={true}
                onChange={this.handleAspiringRoleChange}
                loadOptions={this.getAspiringRoles}
                placeholder={tr('Enter Role...')}
                backspaceRemoves={true}
              />
              <button
                className="step-next"
                onClick={this.nextStep}
                disabled={!this.state.aspiringJobTitle}
              >
                {tr('Show Career Path')}
              </button>
            </div>
          )}
          {this.state.step === 3 && this.state.gettingEdgeData && (
            <div className="text-center">{tr('Loading Career Path...')}</div>
          )}
        </div>
      </div>
    );
  };

  startCareerPath = () => {
    if (this.state.step !== 3 || !this.state.skillGap) {
      return null;
    }
    return (
      <div className="flex f100 columns">
        <div className="col ca-block">
          <div className="flex-header">
            <h4
              dangerouslySetInnerHTML={{
                __html: `${tr('Your career path from')} <strong>${
                  this.state.selectedJobTitle
                }</strong> ${tr('to')} <strong>${this.state.aspiringJobTitle}</strong>`
              }}
            />
          </div>
          <div className="career-path-skills-steps">
            {this.state.skillGap.map((skill, i) => {
              return (
                <button
                  key={`skill-step-${i}`}
                  className={`skill-step ${this.state.selectedSkill == skill ? 'selected' : ''}`}
                  onClick={() => this.selectSkillStep(skill)}
                >
                  {skill}
                </button>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  preparingLearning = () => {
    if (!this.state.loadingSkills) {
      return null;
    }
    return (
      <div className="flex f100 columns">
        <div className="col ca-block text-center">{tr('Preparing learning...')}</div>
      </div>
    );
  };

  getPricingPlans(card) {
    return this.state.edcastPlansForPricing && card.cardMetadatum && card.cardMetadatum.plan
      ? tr(startCase(toLower(card.cardMetadatum.plan)))
      : tr('Free');
  }

  render() {
    return (
      <section id="career-advisor" className="row full-width-row">
        <div className="flex f100 columns">
          <div className="col ca-block">
            <div className="flex-header">
              <h4>Current Role</h4>
            </div>
            <div className="select-column">
              {this.state.uploadingResume ? (
                <input
                  className="loading-resume-placeholder"
                  type="text"
                  placeholder="Please wait..."
                />
              ) : (
                <Async
                  ref="current_role"
                  aria-label="Enter current role"
                  multi={false}
                  value={this.state.selectedJobTitle}
                  autoload={true}
                  onChange={this.handleTitleChange}
                  loadOptions={this.getTitles}
                  placeholder={tr('Enter Current Role')}
                  backspaceRemoves={true}
                />
              )}
            </div>
            <div>
              <div style={{ margin: '15px 0px 15px 100px' }}>Or</div>
              <input
                type="file"
                id="resumeInput"
                style={{ display: 'none' }}
                onChange={this.handleResumeUpload}
              />
              <button className="btn-upload" onClick={this.clickResumeUpload}>
                <svg
                  version="1.1"
                  id="Capa_1"
                  x="0px"
                  y="0px"
                  viewBox="0 0 32 33"
                  style={{
                    stroke: '#fff',
                    fill: '#fff',
                    height: '33px',
                    strokeWidth: '1',
                    width: '25px'
                  }}
                >
                  <g>
                    <g>
                      <path d="M16.646,24.786L16.646,24.786c-0.276,0-0.5-0.224-0.5-0.5l0.001-11.279c0-0.276,0.224-0.5,0.5-0.5l0,0    c0.276,0,0.5,0.224,0.5,0.5l-0.001,11.279C17.146,24.562,16.922,24.786,16.646,24.786z" />
                      <path d="M20.959,17.819c-0.128,0-0.256-0.049-0.354-0.146l-3.959-3.959l-3.959,3.959c-0.195,0.195-0.512,0.195-0.707,0    s-0.195-0.512,0-0.707l4.313-4.313c0.195-0.195,0.512-0.195,0.707,0l4.313,4.313c0.195,0.195,0.195,0.512,0,0.707    C21.215,17.77,21.087,17.819,20.959,17.819z" />
                    </g>
                    <g>
                      <path d="M27.578,33.293H5.715c-1.378,0-2.5-1.121-2.5-2.5V2.5c0-1.379,1.122-2.5,2.5-2.5h15.877c0.276,0,0.5,0.224,0.5,0.5    S21.868,1,21.592,1H5.715c-0.827,0-1.5,0.673-1.5,1.5v28.293c0,0.827,0.673,1.5,1.5,1.5h21.863c0.827,0,1.5-0.673,1.5-1.5V8.704    c0-0.276,0.224-0.5,0.5-0.5s0.5,0.224,0.5,0.5v22.09C30.078,32.172,28.957,33.293,27.578,33.293z" />
                      <path d="M29.578,9.204h-7.986c-0.276,0-0.5-0.224-0.5-0.5V0.717c0-0.276,0.224-0.5,0.5-0.5s0.5,0.224,0.5,0.5v7.486h7.486    c0.276,0,0.5,0.224,0.5,0.5S29.855,9.204,29.578,9.204z" />
                      <path d="M29.578,9.204c-0.13,0-0.26-0.051-0.358-0.151l-7.986-8.203c-0.192-0.197-0.188-0.515,0.01-0.707    c0.197-0.193,0.514-0.188,0.707,0.01l7.986,8.203c0.192,0.197,0.188,0.515-0.01,0.707C29.83,9.157,29.704,9.204,29.578,9.204z" />
                    </g>
                  </g>
                </svg>
                Upload Resume
              </button>
            </div>
          </div>

          <div className="col ca-block">
            <div className="flex-header">
              <h4>Aspiring Role</h4>
            </div>
            <div className="select-column">
              <Async
                multi={false}
                value={this.state.aspiringJobTitle}
                autoload={true}
                onChange={this.handleAspiringRoleChange}
                loadOptions={this.getAspiringRoles}
                aria-label="Enter Aspiring role"
                placeholder={tr('Enter Aspiring Role...')}
                backspaceRemoves={true}
              />
            </div>
            <div style={{ textAlign: 'center' }}>
              <button
                className="btn-upload"
                onClick={this.nextStep}
                disabled={!this.state.aspiringJobTitle}
                style={{ background: '#8F77C4', margin: '55px 0px 0px 0px' }}
              >
                <span style={{ margin: '8px 0px' }}>{tr('Show Career Path')}</span>
              </button>
            </div>
          </div>
        </div>

        {this.startCareerPath()}

        {this.state.step === 3 && (
          <div className="flex f100 columns">
            <div className="col ca-block">
              <div className="flex-header" role="heading" aria-level="3">
                <h4>
                  {tr('Requirements')}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: this.state.aspiringJobTitle
                        ? ` ${tr('for')} <strong>'${this.state.aspiringJobTitle}'</strong>`
                        : null
                    }}
                  />
                </h4>
              </div>
              {this.state.edgeData && (
                <div className="aspiring-map">
                  <div>
                    <h4>{tr('Soft Skills')}</h4>
                    <ul>
                      {this.state.edgeData.soft_skills.map((s, i) => {
                        return <li key={`soft-skill-${i}`}>{s}</li>;
                      })}
                    </ul>
                  </div>
                  <div>
                    <h4>{tr('Intermediate Skills')}</h4>
                    <ul>
                      {this.state.edgeData.req.skills.map((s, i) => {
                        return <li key={`intermediate-skill-${i}`}>{s}</li>;
                      })}
                    </ul>
                  </div>
                  <div>
                    <h4>{tr('Education')}</h4>
                    <ul>
                      <li>{this.state.edgeData.req.education}</li>
                    </ul>
                  </div>
                  <div>
                    <h4>{tr('Experience')}</h4>
                    <ul>
                      <li>{this.state.edgeData.req.experience}</li>
                    </ul>
                  </div>
                </div>
              )}
            </div>

            <div className="col ca-block">
              <div className="flex-header">
                <h4>{tr('Skill Trends')}</h4>
              </div>
              <canvas id="trendingSkillsGraph" width="250" height="100" />
            </div>
          </div>
        )}

        {this.state.step === 3 &&
          this.state.aspiringJobTitle === 'Data Scientist' &&
          !this.state.assessmentTaken && (
            <div className="flex f100 columns">
              <div className="col ca-block flex-middle">
                <div className="mettl-container">
                  <div>
                    <svg
                      width="81"
                      height="101"
                      viewBox="0 0 81 101"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                    >
                      <defs>
                        <polygon
                          id="path-1"
                          points="0 0.201464727 77.7685822 0.201464727 77.7685822 67.8608188 0 67.8608188"
                        />
                        <polygon id="path-3" points="0 101 81 101 81 0 0 0" />
                      </defs>
                      <g
                        id="Me---Career-Advisor2---V8"
                        fill="none"
                        fillRule="evenodd"
                        transform="translate(-1126 -846)"
                      >
                        <g id="Group-29" transform="translate(318 760)">
                          <g id="Group-34" transform="translate(808 43)">
                            <g id="Page-1" transform="translate(0 43)">
                              <path
                                d="M15.9673373,61.5773289 C8.87753888,53.6460146 6.00421135,42.6210252 8.53468559,32.2658073 C10.6467013,23.6214259 16.3802076,16.1047007 24.1504531,11.7442983 C32.2339673,7.20776097 42.0919034,6.38842252 50.8328543,9.46700968 C59.2089278,12.4166937 66.1314086,18.8930099 69.6904427,27.004723 C74.4801985,37.922457 72.6653442,50.8514735 65.2487997,60.1525066 C64.5969511,60.969877 65.7523963,62.1382051 66.4108193,61.3119787 C73.3563103,52.6019977 75.8108506,40.9757884 72.6203098,30.2578049 C69.9077256,21.1447146 63.2870083,13.3882231 54.6828042,9.29546684 C45.9112824,5.12300709 35.5599401,4.89668862 26.6214293,8.70769482 C17.9133502,12.4199737 11.0401772,19.7694199 7.88974009,28.6752156 C3.75051801,40.3762084 6.5679634,53.5226874 14.8049891,62.7368011 C15.5104187,63.5259637 16.6698085,62.3632116 15.9673373,61.5773289"
                                id="Fill-1"
                                fill="#EFF0F4"
                              />
                              <g id="Group-5">
                                <mask id="mask-2" fill="#fff">
                                  <use xlinkHref="#path-1" />
                                </mask>
                                <path
                                  d="M14.1671597,66.4925799 C4.91301681,58.2183789 0.209747899,45.8397761 1.9794958,33.5506469 C3.49852941,23.0028383 9.70477311,13.4206947 18.7360588,7.66412057 C27.9505462,1.79171866 39.5283025,0.240082297 49.9865294,3.46731675 C60.1832269,6.61418756 68.6786723,14.1833359 73.0011176,23.8679675 C78.7006891,36.6396038 76.4232857,51.8315847 67.4634454,62.5498813 C66.7945462,63.3499388 67.9478319,64.5065895 68.6219748,63.7000249 C77.0007479,53.6776708 79.9552437,39.9878718 76.0965378,27.4862833 C72.8267647,16.8948766 64.8979664,7.98524976 54.6897983,3.49757512 C44.130958,-1.14464498 31.7889328,-0.875247847 21.4562269,4.25468517 C11.4728824,9.21087656 3.92490756,18.4852306 1.17852101,29.2298813 C-2.38063866,43.1542641 2.2810084,58.0521206 13.0086303,67.6430488 C13.7902689,68.342245 14.9527311,67.1950297 14.1671597,66.4925799"
                                  id="Fill-3"
                                  fill="#EFF0F4"
                                  mask="url(#mask-2)"
                                />
                              </g>
                              <path
                                d="M40.492478,13 C24.7811959,13.0009744 12.0298255,25.0515356 12.0000563,39.9434841 C11.9980935,40.8854163 12.0474908,41.81663 12.1449768,42.7342019 C13.5905836,56.3373272 25.6991349,66.9710924 40.4450435,67 L40.5058905,67 C56.2168455,67 68.970833,54.9478148 68.9999479,40.0571655 C69.0297171,25.1450792 56.2943763,13.0311812 40.5536521,13 L40.492478,13 Z M40.5006564,13 L40.5006564,13.9744126 L40.5520165,13.9744126 C47.9112311,13.9890288 54.8222717,16.7151106 60.0119313,21.6518099 C65.1888326,26.5761666 68.0326116,33.111877 68.0185448,40.0552167 C67.9904113,54.375185 55.6479588,66.0255874 40.5058905,66.0255874 L40.4470063,66.0255874 C33.5843816,66.0122704 27.0158507,63.595727 21.9514836,59.2222383 C16.9332425,54.8883756 13.7970053,48.9964272 13.1208186,42.6322134 C13.0266039,41.743549 12.9794966,40.8396189 12.9814594,39.9454329 C12.995199,33.0124869 15.8579518,26.4949655 21.0423772,21.5933451 C26.2385795,16.6806814 33.1460216,13.9747374 40.4928051,13.9744126 L40.5006564,13 Z"
                                id="Fill-6"
                                fill="#464660"
                              />
                              <path
                                d="M29.16916,49.1725038 C29.091002,49.247612 29.0302489,49.3550493 29.022039,49.4935096 C29.0141575,49.6241325 29.0558637,49.732223 29.1182588,49.8145154 L29.16916,49.1725038 Z M29.3770342,49.9999999 C27.349522,50.0003265 16.6559877,48.8831741 12,42.5799657 L12.7940597,42 C17.0838867,47.8074944 27.170875,49.002694 29.1790119,49.0451465 L29.1839378,48.9860396 C29.5872072,49.0173891 29.9359629,49.0448199 30,49.4448526 L29.7005035,49.9771409 C29.666022,49.9915094 29.5546961,49.9999999 29.3770342,49.9999999 Z"
                                id="Fill-8"
                                fill="#464660"
                              />
                              <path
                                d="M31.4985398,47 C30.1205637,47.0003246 29.0026014,48.1165206 29.0000045,49.4956183 C28.9974076,50.8756897 30.1130976,51.9974034 31.493346,52 L31.4982152,52 C32.8771651,52 33.997075,50.8841285 33.9999965,49.50568 C34.0022688,48.1243103 32.8852804,47.0025966 31.5037336,47 L31.4985398,47 Z M31.4985398,47 L31.5021105,47.9737098 C32.3438283,47.9753327 33.0274603,48.6617981 33.0261618,49.5037326 C33.0245388,50.343395 32.3392837,51.0262902 31.4956183,51.0262902 C30.6548743,51.0246673 29.9722162,50.338851 29.9738392,49.4972412 C29.9754623,48.6572541 30.6594189,47.9740344 31.4985398,47.9737098 L31.4985398,47 Z"
                                id="Fill-10"
                                fill="#464660"
                              />
                              <path
                                d="M29.2204827,30 C23.0384795,30.0003329 18.0209037,35.1077572 18.0085058,41.4190434 L18.000023,45.4904006 C17.987625,51.8096771 22.9983494,56.9430696 29.1914454,56.9553879 L50.7563527,57 L50.7788648,57 C56.9611942,57 61.9800751,51.891577 61.9921468,45.5799578 L61.999977,41.5092665 C62.012375,35.1896571 57.0016506,30.0562646 50.8085546,30.0436134 L29.244626,30 L29.2204827,30 Z M29.2204827,30.9987793 L29.2426685,30.9987793 L50.806597,31.0423926 C56.4502687,31.0537121 61.0322854,35.7486406 61.0211925,41.5072689 L61.0133622,45.5779603 C61.0022693,51.3252691 56.4111173,56.0012207 50.7788648,56.0012207 L50.7583103,56.0012207 L29.193403,55.9566086 C26.4593315,55.9509488 23.8913271,54.859616 21.9621428,52.8830318 C20.0329585,50.9067806 18.9735873,48.2819887 18.9788075,45.4927311 L18.9872903,41.4210409 C18.9983832,35.674398 23.5888827,30.9987793 29.2204827,30.9987793 Z"
                                id="Fill-12"
                                fill="#464660"
                              />
                              <polygon
                                id="Fill-14"
                                fill="#464660"
                                points="31.9696186 31 31 30.9979692 31.0303814 15 32 15.0016924"
                              />
                              <polygon
                                id="Fill-16"
                                fill="#464660"
                                points="48.9696186 31 48 30.9979692 48.0303814 15 49 15.0016923"
                              />
                              <path
                                d="M32.999997,41.0025344 C32.9977792,42.1078704 32.1014639,43.0022135 30.9960402,42.9999959 C29.8906164,42.9977782 28.9977863,42.0999502 29.0000041,40.9949311 C29.0019051,39.8918128 29.8982203,38.9977865 31.0036441,39.0000041 C32.1093847,39.0022218 33.001898,39.9000498 32.999997,41.0025344"
                                id="Fill-18"
                                fill="#26273B"
                              />
                              <path
                                d="M50.9999932,41.0026937 C50.9977756,42.1081168 50.1015322,43.0022137 48.9958802,42.9999959 C47.8911786,42.9977781 46.9977865,42.100196 47.0000041,40.9947729 C47.0028554,39.8915676 47.8994156,38.9974707 49.0038003,39.0000054 C50.1091355,39.0022232 51.0028444,39.8998052 50.9999932,41.0026937"
                                id="Fill-20"
                                fill="#26273B"
                              />
                              <path
                                d="M35,47.0003174 L45,47.0196783 C44.9969512,48.669168 43.5667344,50.002852 41.8069106,49.9999954 L38.3119919,49.993965 C36.552168,49.9901562 35.12771,48.6507592 35.1314363,47"
                                id="Fill-22"
                                fill="#ABACC0"
                              />
                              <polygon
                                id="Fill-24"
                                fill="#464660"
                                points="39.9917355 73 39 72.9979709 39.0082645 68 40 68.0020291"
                              />
                              <path
                                d="M33.0168831,73 L33,80.9745436 L45.9831169,81 L46,73.02482 L33.0168831,73 Z M34.0080214,73.9565252 L45.0048892,73.9775267 L44.9923097,80.0434748 L33.9951108,80.0215186 L34.0080214,73.9565252 Z"
                                id="Fill-26"
                                fill="#464660"
                              />
                              <path
                                d="M25.0643411,95 C22.8250014,95 21.0060557,97.6676707 21,100.96419 L38,101 C38.0057234,97.7001339 36.1937896,95.0227577 33.9525376,95.018407 L25.0703968,95 L25.0643411,95 Z M25.0643379,95 L25.0684813,96.0040161 L33.9506149,96.022423 C35.4180092,96.0254351 36.6839714,97.7613788 36.9791076,99.9939759 L22.02469,99.9625167 C22.3277942,97.7342704 23.5985372,96.0040161 25.0643379,96.0040161 L25.0643379,95 Z"
                                id="Fill-28"
                                fill="#464660"
                              />
                              <polygon
                                id="Fill-30"
                                fill="#464660"
                                points="24.9405857 96 23.6674211 95.9980612 23.7251379 73.5694744 22 72.8498492 22.6119677 72 25 72.9959069"
                              />
                              <path
                                d="M23.9980789,79 L16,78.9851681 L16.0243336,67.9815425 C16.3521972,64.8816744 19.0417034,59.2386289 19.1556872,59 L20.0172897,59.4370468 C19.9900744,59.4937376 17.3047306,65.1288728 16.9819899,68.0629532 L16.9621388,77.998352 L24,78.0112063 L23.9980789,79 Z"
                                id="Fill-32"
                                fill="#464660"
                              />
                              <path
                                d="M23.8793035,85 C22.5704699,84.8177832 21.5669074,84.2092333 20.8958486,83.1917596 C19.7346075,81.4303308 20.0297927,79.0982655 20.0429046,79 L21.0400792,79.1744793 C21.0373896,79.1942098 20.7996949,81.1324392 21.7034054,82.4965504 C22.2033375,83.2505642 22.9755931,83.705139 24,83.8475079 L23.8793035,85 Z"
                                id="Fill-34"
                                fill="#464660"
                              />
                              <path
                                d="M38.9872729,101 L38.0082672,100.998067 L38.0203416,95.126212 C37.3712607,93.4630351 36.4177091,91.9673678 36,91.9664014 L36.0026107,91 C37.3826825,91.0035435 38.6214511,93.9604098 38.9680191,94.8665722 L39,94.9496827 L38.9872729,101 Z"
                                id="Fill-36"
                                fill="#464660"
                              />
                              <path
                                d="M45.0636131,95 C42.8251038,95 41.0060561,97.6680054 41,100.96419 L58,101 C58.0060409,97.7004685 56.1943241,95.0227577 53.9532649,95.018407 L45.0696692,95 L45.0636131,95 Z M45.0636095,95 L45.0677532,96.0040161 L53.951341,96.022423 C55.4184988,96.0251004 56.6838945,97.7613788 56.9790473,99.9939759 L42.0247474,99.9625167 C42.3278686,97.7346051 43.5977267,96.0040161 45.0636095,96.0040161 L45.0636095,95 Z"
                                id="Fill-38"
                                fill="#464660"
                              />
                              <path
                                d="M54.9493494,96 L54,95.9980442 L54.0446194,72.5942981 L54.6094823,72.5995137 L67.2105136,74.4989472 L69.726606,67.8761927 L62.6473074,65.9738254 L62.6128144,65.6370973 C62.3289589,62.8536302 58.807189,59.7800488 58.7717466,59.7494075 L59.3818618,59 C59.5331248,59.1310404 62.9738836,62.1316042 63.5099496,65.1947545 L71,67.207626 L67.8199959,75.5792154 L54.9920701,73.6455549 L54.9493494,96 Z"
                                id="Fill-40"
                                fill="#464660"
                              />
                              <path
                                d="M40.9703504,101 L40,100.998065 L40.012938,94.9415046 L40.0446361,94.8586308 C40.3916981,93.9528232 41.6301887,91 42.9977358,91 L43,91 L42.9977358,91.9673987 L42.9974124,91.9673987 C42.5824259,91.9673987 41.6321294,93.4600948 40.9826415,95.1217632 L40.9703504,101 Z"
                                id="Fill-42"
                                fill="#464660"
                              />
                              <path
                                d="M34.0681679,7.93519869 C38.0080005,7.34796798 41.9985665,7.36535611 45.9322702,7.98644792 C47.003458,8.15575339 47.4634626,6.68599881 46.3847841,6.51577817 C42.1323798,5.8443523 37.8748682,5.82970966 33.6156541,6.46452894 C32.537316,6.62529288 32.9949372,8.09504747 34.0681679,7.93519869"
                                id="Fill-44"
                                fill="#77CABE"
                              />
                              <path
                                d="M27.0165265,4.10081762 C35.5385697,0.919556234 44.6455014,1.21683476 53.000576,4.931673 C53.9970771,5.37492291 54.4289385,3.53332053 53.4417319,3.09388189 C44.8383619,-0.731101856 35.3467051,-1.0112297 26.5753706,2.26340764 C25.5639319,2.64072269 25.9967891,4.48156282 27.0165265,4.10081762"
                                id="Fill-46"
                                fill="#77CABE"
                              />
                              <mask id="mask-4" fill="#fff">
                                <use xlinkHref="#path-3" />
                              </mask>
                              <polygon
                                id="Fill-48"
                                fill="#464660"
                                mask="url(#mask-4)"
                                points="54 73 55 73 55 71 54 71"
                              />
                              <path
                                d="M78,77 L74,76 M79,75 L74,74 M80,72 L75,71 M80.2930873,72.6619362 C80.4011675,72.5288009 80.5161619,72.3812168 80.5347212,72.2047351 C80.6191474,71.3993868 80.2552411,70.7113489 78.7996156,70.36733 L74.4327393,69.3352732 L77.3254309,68.6561799 C78.1067379,68.5343972 78.6384051,67.8432632 78.5110379,67.1163512 C78.3836706,66.3890952 77.6449407,65.8964601 76.8643616,66.0185868 L73.648521,66.5211985 L71.9217854,66.767172 C69.6200776,66.1775236 67.2514111,67.4331926 66.6404123,69.5650779 L66.1451358,71.293773 C65.534137,73.4263463 66.9086113,75.640108 69.2095912,76.2294124 L72.3162599,77.0251282 L75.9356725,77.9519152 C76.699512,78.1476619 77.4870054,77.7279588 77.6907929,77.0165277 C77.7340978,76.8651594 77.7421037,76.712759 77.7290031,76.5648309 L77.7934145,76.5809998 C78.557254,76.7767465 79.3447473,76.3570434 79.5488988,75.6456123 C79.5976623,75.4746349 79.6067599,75.3022814 79.5841977,75.1357763 C80.2104806,75.1323361 80.7825414,74.748411 80.9517578,74.1584185 C81.1209743,73.567738 80.8316687,72.9636408 80.2930873,72.6619362"
                                id="Fill-50"
                                fill="#FFF"
                                mask="url(#mask-4)"
                              />
                              <path
                                d="M70.9527651,67.5223425 C70.3519863,67.5223425 69.7596882,67.6648827 69.2253987,67.9445783 C68.4149749,68.3687147 67.8362461,69.0570257 67.597088,69.8821753 L67.135394,71.4741913 C66.6390984,73.1843574 67.7602583,74.961359 69.6341726,75.435226 L75.9041868,77.0212237 C76.1182419,77.076656 76.3424738,77.0500485 76.534818,76.9490033 C76.7037553,76.8599949 76.8299494,76.7218892 76.8950818,76.5562258 L73.5577231,75.7063692 C73.286338,75.6373164 73.12656,75.3759927 73.2005125,75.1229046 C73.2744649,74.8694997 73.5553485,74.7209411 73.8253767,74.7893604 L77.5569223,75.7396286 C77.5572615,75.7396286 77.55794,75.7399454 77.5582792,75.7399454 C77.5637069,75.7412124 77.5694739,75.7424794 77.5749016,75.7440632 L77.6349455,75.7592675 C78.0766248,75.8698154 78.5308557,75.6341489 78.6478905,75.2321854 L78.6516221,75.2191984 L74.2392391,74.1232224 C73.9675147,74.0557533 73.8060405,73.7956966 73.8782968,73.541975 C73.9502139,73.2882534 74.2297406,73.136844 74.5007865,73.2049465 L79.3124449,74.4000672 C79.620467,74.3487527 79.8748905,74.1415942 79.9556276,73.8628489 C80.0394178,73.5736506 79.921026,73.2638631 79.6649063,73.0763435 L74.9183803,71.9062466 C74.6469952,71.8394111 74.4845034,71.5793543 74.5564204,71.3256327 C74.6283375,71.0719111 74.9082034,70.9208184 75.1782316,70.9873372 L79.5577092,72.0671587 C79.5841692,71.6379542 79.4203205,71.2147681 78.3171399,70.9575621 L74.2463629,70.0072939 C74.0197563,69.9543956 73.8606568,69.7640252 73.860996,69.5460971 C73.8613353,69.3275354 74.0204348,69.1377985 74.2473806,69.085217 L76.9439311,68.4599405 C76.9568219,68.4567729 76.9700519,68.4542389 76.9836212,68.4526551 C77.2031039,68.4184454 77.3954481,68.3078976 77.5250345,68.1416006 C77.653264,67.9772042 77.704488,67.7779646 77.6698864,67.5809424 C77.6346063,67.3832866 77.5175714,67.210021 77.339475,67.0928212 C77.1590038,66.9740377 76.9391819,66.9277913 76.7196992,66.9613675 L73.7219112,67.4238313 L72.104795,67.651579 C72.0345741,67.6607649 71.964014,67.6572806 71.8958284,67.6404925 C71.5837355,67.5613035 71.2672326,67.5223425 70.9527651,67.5223425 M76.1274011,78 C75.9638916,78 75.7997036,77.9797276 75.6378902,77.9385493 L69.3678759,76.3528684 C66.9525483,75.7415292 65.5101363,73.4415633 66.1529798,71.225221 L66.6146738,69.6335217 C66.9264275,68.5597186 67.6768073,67.6655162 68.7277463,67.1156277 C69.7437444,66.584111 70.9212166,66.4361859 72.0559457,66.6975097 L73.5628116,66.4855998 L76.5534758,66.0240862 C77.0385767,65.9483815 77.5253738,66.0525943 77.9260061,66.3161353 C78.3283345,66.5809434 78.5939527,66.974988 78.6736721,67.4254151 C78.7533915,67.8758423 78.6377136,68.3288035 78.3483492,68.7006751 C78.0644125,69.0649446 77.6468186,69.3078965 77.1708769,69.386452 L76.4747741,69.5479976 L78.5641004,70.035802 C80.0105831,70.3734639 80.6839575,71.1134061 80.5642088,72.2353562 C80.5543711,72.3319668 80.5285895,72.4203417 80.4946663,72.500481 C80.925151,72.9255676 81.1052829,73.5350063 80.938381,74.1111856 C80.7694438,74.6914828 80.2792544,75.1349413 79.6716909,75.2964869 C79.6618532,75.3582543 79.6479447,75.4200218 79.6299655,75.4808389 C79.4142143,76.2252157 78.694026,76.7193552 77.8998853,76.7370935 C77.899546,76.7389941 77.8988676,76.7412114 77.8985283,76.7427951 C77.7702989,77.1837196 77.4636337,77.5505231 77.0345059,77.7763702 C76.7536223,77.9246121 76.4425471,78 76.1274011,78"
                                id="Fill-51"
                                fill="#464660"
                                mask="url(#mask-4)"
                              />
                              <path
                                d="M75,70 C75,70 74.3333333,73.7037037 72,72.8806584"
                                id="Fill-52"
                                fill="#FFF"
                                mask="url(#mask-4)"
                              />
                              <path
                                d="M72.1547238,73 C71.899733,73 71.6354092,72.9545355 71.3627524,72.8636065 C71.0970953,72.7753976 70.9434342,72.4528717 71.0194315,72.1431691 C71.0950954,71.8330779 71.372752,71.6551058 71.6374091,71.7429261 C72.1010591,71.8971945 72.4973781,71.8544501 72.8476987,71.6111956 C73.6923349,71.0252088 74.0086568,69.4724213 74.01199,69.4568779 C74.0719878,69.1425123 74.3373116,68.9435565 74.6083018,69.0142791 C74.8779587,69.0842245 75.0479526,69.3954814 74.9879547,69.7094585 C74.9722886,69.7930043 74.584636,71.7623554 73.3593469,72.6129689 C72.9876937,72.870601 72.5847082,73 72.1547238,73"
                                id="Fill-53"
                                fill="#464660"
                                mask="url(#mask-4)"
                              />
                            </g>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </div>
                  <div>
                    <div className="flex-header">
                      {!this.state.assessmentTaken && <h5>+ {tr('Take the Pre-assessment')}</h5>}
                    </div>
                    <a
                      target="_blank"
                      href="https://tests.mettl.com/authenticateKey/a55a0dc"
                      onClick={this.mockAssessment}
                      className="btn-assessment"
                    >
                      <img src="/i/images/ca/mettl.png" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          )}

        {this.state.assessmentTaken && (
          <div className="flex f100 ca-block flex-center">
            <div className="flex-header">
              <h4>Assessement Reports</h4>
            </div>
            <div className="flex-table">
              <div className="flex-table-header flex-table-row">
                <div className="item__provider">Provider</div>
                <div className="item__title">Title</div>
                <div className="item__duration">Date</div>
                <div className="item__credential">Score</div>
                <div className="item__action" />
              </div>
              <div className="flex-table-row">
                <div className="item__provider">
                  <img src="/i/images/ca/mettl.png" />
                </div>
                <div className="item__title">Data Scientists</div>
                <div className="item__duration">{this.state.assessmentTaken}</div>
                <div className="item__credential">1%</div>
                <div className="item__action">
                  <a
                    target="_blank"
                    href="https://tests.mettl.com/authenticateKey/a55a0dc"
                    className="btn__action"
                  >
                    Retake
                  </a>
                </div>
              </div>
            </div>
          </div>
        )}

        {this.preparingLearning()}

        {!this.state.loadingSkills && this.state.currentRoleCourses.length > 0 && (
          <div className="flex f100 ca-block flex-center CA_courses">
            <div className="flex-header">
              <h4>
                {tr('Current Role')} - <strong>{tr('Recommended Learning')}</strong>
              </h4>
            </div>
            <div className="flex-table">
              <div className="flex-table-header flex-table-row">
                <div className="item__provider">Provider</div>
                <div className="item__title" style={!this.state.enableBIA ? { width: '45%' } : {}}>
                  Title
                </div>
                <div className="item__duration">Duration</div>
                {this.state.enableBIA && <div className="item__credential">Credential</div>}
                {this.state.edcastPricing && <div className="item__price">Price</div>}
                <div className="item__action" />
              </div>
              {this.state.currentRoleCourses.map((c, i) => {
                if (i >= this.state.currentCoursesLimit) {
                  return null;
                }
                let countryCode = this.props.currentUser.countryCode || 'us';
                let currency = LocaleCurrency.getCurrency(countryCode);
                let priceData = null;

                if (c.prices && c.prices.length > 0) {
                  priceData =
                    find(c.prices, { currency: currency }) ||
                    find(c.prices, { currency: 'USD' }) ||
                    find(c.prices, function(price) {
                      return price.currency != 'SKILLCOIN';
                    });
                }
                let courseTitle = c.title || c.message;
                return (
                  <div className="flex-table-row">
                    <div className="item__provider">
                      <img src={c.eclSourceLogoUrl} />
                    </div>
                    <div
                      className="item__title"
                      style={!this.state.enableBIA ? { width: '45%' } : {}}
                    >
                      {courseTitle.slice(0, 250) + (courseTitle.length > 250 && '...')}
                    </div>
                    <div className="item__duration">
                      {c.eclDurationMetadata
                        ? c.eclDurationMetadata.calculated_duration_display
                        : 'Self-paced'}
                    </div>
                    {this.state.enableBIA && (
                      <div className="item__credential">{c.skillLevel || 'Intermediate'}</div>
                    )}
                    {this.state.edcastPricing && (
                      <div className="item__price">
                        {priceData && c.isPaid
                          ? priceData.symbol + priceData.amount
                          : this.getPricingPlans(c)}
                      </div>
                    )}
                    <div className="item__action">
                      <a target="_blank" href={c.resource.url} className="btn__action">
                        Start Learning
                      </a>
                    </div>
                  </div>
                );
              })}
              {this.state.currentCoursesLimit < this.state.currentRoleCourses.length && (
                <div className="flex-table-row">
                  <div className="item__action">
                    <button className="btn__action" onClick={this.loadMoreCurrentCourses}>
                      {tr('Load more')}
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}

        {!this.state.loadingSkills && this.state.aspiringRolePathways.length > 0 && (
          <div className="flex f100 ca-block flex-center CA_pathways">
            <div className="flex-header">
              <h4>
                {this.state.selectedSkill.toUpperCase()} -{' '}
                <strong>{tr('Recommended Pathways')}</strong>
              </h4>
            </div>
            <div className="flex-table">
              <div className="flex-table-header flex-table-row">
                <div className="item__title">Title</div>
                <div className="item__cards_count">Cards</div>
                <div className="item__action" />
              </div>
              {this.state.aspiringRolePathways
                .slice(0, this.state.aspiringRolePathwaysLimit)
                .map((c, i) => {
                  let pathwayTitle = c.title || c.message;
                  let cardConsumsionState = c.completedPercentage
                    ? c.completedPercentage < 100
                      ? 'In Progress'
                      : 'Completed'
                    : 'Start Learning';
                  let btnClassName = c.completedPercentage
                    ? c.completedPercentage < 100
                      ? 'btn__action started'
                      : 'btn__action completed'
                    : 'btn__action';
                  return (
                    <div className="flex-table-row">
                      <div className="item__title">
                        {pathwayTitle.slice(0, 250) + (pathwayTitle.length > 250 ? '...' : '')}
                      </div>
                      <div className="item__cards_count">
                        {c && c.packCards && c.packCards.length ? (
                          <span>{c && c.packCards && c.packCards.length} cards</span>
                        ) : (
                          '-'
                        )}
                      </div>
                      <div className="item__action" style={{}}>
                        <a
                          target="_blank"
                          href={c.shareUrl}
                          className={btnClassName}
                          style={
                            cardConsumsionState === 'Start Learning'
                              ? { backgroundColor: colors.primary }
                              : {}
                          }
                        >
                          {cardConsumsionState}
                        </a>
                      </div>
                    </div>
                  );
                })}
            </div>
            {this.state.aspiringRolePathwaysLimit < this.state.aspiringRolePathways.length && (
              <div className="loadmore-container">
                <div>
                  <span className="loadmore-button" onClick={this.loadMoreAspiringRolePathways}>
                    {tr('See more')}
                  </span>
                </div>
              </div>
            )}
          </div>
        )}

        {!this.state.loadingSkills && this.state.aspiringRoleChannels.length > 0 && (
          <div className="flex f100 ca-block flex-center CA_channels">
            <div className="flex-header">
              <h4>
                {this.state.selectedSkill.toUpperCase()} -{' '}
                <strong>{tr('Recommended Channels')}</strong>
              </h4>
            </div>
            <div className="flex-table">
              <div className="flex-table-header flex-table-row">
                <div className="item__title">Channel</div>
                <div className="item__action" />
              </div>
              {this.state.aspiringRoleChannels
                .slice(0, this.state.aspiringRoleChannelsLimit)
                .map((c, i) => {
                  return (
                    <div className="flex-table-row">
                      <div className="item__title">{c.label}</div>
                      <div className="item__action">
                        <a
                          target="_blank"
                          href={`/channel/${c.slug}`}
                          className="btn__action"
                          style={{ backgroundColor: colors.primary }}
                        >
                          Visit Channel
                        </a>
                      </div>
                    </div>
                  );
                })}
            </div>
            {this.state.aspiringRoleChannelsLimit < this.state.aspiringRoleChannels.length && (
              <div className="loadmore-container">
                <div>
                  <span className="loadmore-button" onClick={this.loadMoreAspiringRoleChannels}>
                    {tr('See more')}
                  </span>
                </div>
              </div>
            )}
          </div>
        )}

        {!this.state.loadingSkills && this.state.aspiringRoleCourses.length > 0 && (
          <div className="flex f100 ca-block flex-center CA_courses">
            <div className="flex-header">
              <h4>
                {this.state.selectedSkill.toUpperCase()} -{' '}
                <strong>{tr('Recommended Learning')}</strong>
              </h4>
            </div>
            <div className="flex-table">
              <div className="flex-table-header flex-table-row">
                <div className="item__provider">Provider</div>
                <div className="item__title" style={!this.state.enableBIA ? { width: '45%' } : {}}>
                  Title
                </div>
                <div className="item__duration">Duration</div>
                {this.state.enableBIA && <div className="item__credential">Credential</div>}
                {this.state.edcastPricing && <div className="item__price">Price</div>}
                <div className="item__action" />
              </div>
              {this.state.aspiringRoleCourses
                .slice(0, this.state.aspiringCoursesLimit)
                .filter(c => c.cardType === 'course')
                .map((c, i) => {
                  let countryCode = this.props.currentUser.countryCode || 'us';
                  let currency = LocaleCurrency.getCurrency(countryCode);
                  let priceData = null;

                  if (c.prices && c.prices.length > 0) {
                    priceData =
                      find(c.prices, { currency: currency }) ||
                      find(c.prices, { currency: 'USD' }) ||
                      find(c.prices, function(price) {
                        return price.currency != 'SKILLCOIN';
                      });
                  }
                  let courseTitle = c.title || c.message;
                  let cardConsumsionState = c.completedPercentage
                    ? c.completedPercentage < 100
                      ? 'In Progress'
                      : 'Completed'
                    : 'Start Learning';
                  let btnClassName = c.completedPercentage
                    ? c.completedPercentage < 100
                      ? 'btn__action started'
                      : 'btn__action completed'
                    : 'btn__action';

                  return (
                    <div className="flex-table-row">
                      <div className="item__provider">
                        <img src={c.eclSourceLogoUrl} />
                      </div>
                      <div
                        className="item__title"
                        style={!this.state.enableBIA ? { width: '45%' } : {}}
                      >
                        {courseTitle.slice(0, 250) + (courseTitle.length > 250 && '...')}
                      </div>
                      <div className="item__duration">
                        {c.eclDurationMetadata
                          ? c.eclDurationMetadata.calculated_duration_display
                          : 'Self-paced'}
                      </div>
                      {this.state.enableBIA && (
                        <div className="item__credential">{c.skillLevel || 'Intermediate'}</div>
                      )}
                      {this.state.edcastPricing && (
                        <div className="item__price">
                          {priceData && c.isPaid
                            ? priceData.symbol + priceData.amount
                            : this.getPricingPlans(c)}
                        </div>
                      )}
                      <div className="item__action">
                        <a
                          target="_blank"
                          href={c.shareUrl}
                          className={btnClassName}
                          style={
                            cardConsumsionState === 'Start Learning'
                              ? { backgroundColor: colors.primary }
                              : {}
                          }
                        >
                          {cardConsumsionState}
                        </a>
                      </div>
                    </div>
                  );
                })}
            </div>
            {this.state.aspiringCoursesLimit < this.state.aspiringRoleCourses.length && (
              <div className="loadmore-container">
                <div>
                  <span className="loadmore-button" onClick={this.loadMoreAspiringCourses}>
                    {tr('See more')}
                  </span>
                </div>
              </div>
            )}
          </div>
        )}
      </section>
    );
  }
}

CareerAdvisorContainer.propTypes = {
  currentUser: PropTypes.object,
  publicProfile: PropTypes.bool,
  location: PropTypes.object,
  team: PropTypes.object
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS()
}))(CareerAdvisorContainer);
