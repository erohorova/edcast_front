import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import LearningAsgPerSMEEach from './LearningAsgPerSMEEach';

class LearningAsgPerSME extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        {this.props.assignorsIdList && (
          <div>
            {this.props.assignorsIdList.map((assignorId, index) => {
              let emailName = this.props.assignorsNameMappings[`${assignorId}`];
              let ind = emailName.indexOf('<NAME>');
              let name = emailName.substring(ind + 6, emailName.length);
              if (name.trim().length === 0) {
                name = emailName.substring(0, ind);
              }
              return (
                <LearningAsgPerSMEEach
                  assignorId={assignorId}
                  sectionTitle={name}
                  key={index}
                  redirectUrl={this.props.redirectUrl}
                />
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

LearningAsgPerSME.propTypes = {};

LearningAsgPerSME.defaultProps = {
  sectionsList: {}
};

export default LearningAsgPerSME;
