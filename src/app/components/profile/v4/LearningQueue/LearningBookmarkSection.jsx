import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import DateConverter from '../../../common/DateConverter';
import LinearProgress from 'material-ui/LinearProgress';
import colors from 'edc-web-sdk/components/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LearningHeaderRow from './LearningHeaderRow';
import LearningContentRow from './LearningContentRow';

class LearningBookmarkSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      paperStyle: {
        padding: '20px',
        width: '100%',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
      }
    };
  }

  render() {
    let contentList = this.props.contentList;
    return (
      <div className="learning-card-section">
        {this.props.contentList.length > 0 && (
          <div>
            <h5 className="learning-plan-section-title">{tr('BOOKMARKS')}</h5>
            <LearningHeaderRow rowType="bookmark" />
            <div className="learning-content-row-wrapper">
              {this.props.contentList.map(card => {
                let duration = this.props.fetchDuration(card);
                return (
                  <LearningContentRow
                    key={card.id}
                    duration={duration}
                    redirectUrl={this.props.redirectUrl}
                    card={card}
                    rowType="bookmark"
                  />
                );
              })}
            </div>
            {this.props.showLoadMore && (
              <div className="learning-load-more">
                <span onClick={this.props.loadMore.bind(this, 'bookmarks')}>
                  {tr(this.props.cardsLoadMoreButtonText)}
                </span>
              </div>
            )}
            {!this.props.showLoadMore && <br />}
          </div>
        )}
      </div>
    );
  }
}

LearningBookmarkSection.propTypes = {};

LearningBookmarkSection.defaultProps = {
  contentList: []
};

export default LearningBookmarkSection;
