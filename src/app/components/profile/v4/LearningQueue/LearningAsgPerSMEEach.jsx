import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import DateConverter from '../../../common/DateConverter';
import LinearProgress from 'material-ui/LinearProgress';
import colors from 'edc-web-sdk/components/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LearningHeaderRow from './LearningHeaderRow';
import LearningContentRow from './LearningContentRow';
import { assignmentsv2 } from 'edc-web-sdk/requests';

class LearningAsgPerSMEEach extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: 5,
      cardsCount: 0,
      offset: 0,
      cards: [],
      showLoadMore: false,
      loading: false
    };

    this.styles = {
      paperStyle: {
        padding: '20px',
        width: '100%',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
      }
    };
  }

  componentDidMount = () => {
    if (this.props.assignorId) {
      assignmentsv2
        .getAssignmentsV2PerAssignor(this.state.limit, this.state.offset, this.props.assignorId)
        .then(data => {
          this.setState({ cards: data.assignments, cardsCount: data.assignmentCount }, function() {
            let showLoadMore = this.state.cards.length < this.state.cardsCount;
            this.setState({ showLoadMore: showLoadMore });
          });
        });
    }
  };

  loadMore = () => {
    if (!this.state.loading) {
      let offset = this.state.offset + 5;
      this.setState({ loading: true });
      assignmentsv2
        .getAssignmentsV2PerAssignor(this.state.limit, offset, this.props.assignorId)
        .then(data => {
          let cards = this.state.cards;
          cards = cards.concat(data.assignments);
          this.setState({ cards: cards }, function() {
            let showLoadMore = this.state.cards.length < this.state.cardsCount;
            this.setState({ showLoadMore: showLoadMore, offset: offset, loading: false });
          });
        });
    }
  };

  fetchDuration = card => {
    if (card.assignable && card.assignable.eclDurationMetadata) {
      return card.assignable.eclDurationMetadata.calculated_duration_display;
    } else if (card.eclDurationMetadata) {
      return card.eclDurationMetadata.calculated_duration_display;
    } else {
      return 'NA';
    }
  };

  render() {
    let _this = this;

    return (
      <div className="learning-card-section">
        <h5 className="learning-plan-section-title">
          {tr('Assigned By')} {this.props.sectionTitle}
        </h5>
        <LearningHeaderRow />
        <div className="learning-content-row-wrapper">
          {this.state.cards.length > 0 &&
            this.state.cards.map(card => {
              let duration = _this.fetchDuration(card);
              return (
                <LearningContentRow
                  key={card.id}
                  duration={duration}
                  redirectUrl={this.props.redirectUrl}
                  card={card}
                />
              );
            })}
        </div>
        {this.state.showLoadMore && (
          <div className="learning-load-more">
            <span onClick={this.loadMore.bind(this)}>
              {tr(this.state.loading ? 'Loading...' : 'Load More')}
            </span>
          </div>
        )}
        {!this.state.showLoadMore && <br />}
        <br />
      </div>
    );
  }
}

LearningAsgPerSMEEach.propTypes = {};

LearningAsgPerSMEEach.defaultProps = {};

export default LearningAsgPerSMEEach;
