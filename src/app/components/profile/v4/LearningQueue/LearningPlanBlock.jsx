import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import DateConverter from '../../../common/DateConverter';
import LinearProgress from 'material-ui/LinearProgress';
import colors from 'edc-web-sdk/components/colors';
import IconButton from 'material-ui/IconButton/IconButton';
import ArrowDown from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LearningHeaderRow from './LearningHeaderRow';
import LearningContentRow from './LearningContentRow';
import * as requests from 'edc-web-sdk/requests/myLearningPlan';

class LearningPlanBlock extends Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;

    this.styles = {
      paperStyle: {
        padding: '20px',
        width: '100%',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
      }
    };
    this.state = {
      showLoadMore: true,
      multilingualContent: window.ldclient.variation('multilingual-content', false)
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.setState({
      contentList: this.props.contentList
    });
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }

    return card_type;
  }

  loadMore() {
    let mlpPeriod = '';
    let learningTopicsName = '';
    let card_type = this.getAssignmentContentType();

    if (this.props.type == 'other' && this.props.learningTopic == undefined) {
      mlpPeriod = this.props.type;
      learningTopicsName = '';
    } else if (
      this.props.learningTopic !== undefined &&
      this.props.type !== 'other' &&
      this.props.type === this.props.type.split('.')[this.props.type.split('.').length - 1]
    ) {
      mlpPeriod = 'skill-wise';
      learningTopicsName = this.props.learningTopic;
    } else {
      mlpPeriod = this.props.type;
    }
    let payload = {
      limit: 5,
      offset: this.state.contentList.length,
      learning_topics_name: learningTopicsName,
      mlp_period: mlpPeriod,
      filter_by_language: this.state.multilingualContent
    };

    if (card_type.length > 0) {
      payload['card_type[]'] = card_type;
    }

    requests
      .getQuarterlyPlan(payload)
      .then(data => {
        let contentList = this.state.contentList;
        contentList = contentList.concat(data.assignments);
        this.setState({
          contentList: contentList,
          showLoadMore: data.assignmentsCount < 4
        });
      })
      .catch(err => {
        console.error(`Error in LearningPlanBlock.getQuarterlyPlan.func : ${err}`);
      });
  }

  render() {
    let contentList = this.props.contentList;
    let label = '';
    switch (this.props.type) {
      case 'quarter-1':
        label = 'QUARTER-1';
        break;
      case 'quarter-2':
        label = 'QUARTER-2';
        break;
      case 'quarter-3':
        label = 'QUARTER-3';
        break;
      case 'quarter-4':
        label = 'QUARTER-4';
        break;
      case 'untimed':
        label = 'UNTIMED';
        break;
      case 'other':
        label = 'OTHERS';
        break;
      default:
        label = this.props.type;
        break;
    }
    return (
      <div className="learning-card-section">
        {this.props.contentList.length > 0 && (
          <div>
            <h5 className="learning-plan-section-title">{tr(label)}</h5>
            <LearningHeaderRow colType="myLearningPlan" />
            <div className="learning-content-row-wrapper">
              {this.state.contentList &&
                this.state.contentList.map(card => {
                  let duration = this.props.fetchDuration(card);
                  return (
                    <LearningContentRow
                      key={card.id}
                      duration={duration}
                      redirectUrl={this.props.redirectUrl}
                      card={card}
                      userId={this.props.currentUser.id}
                    />
                  );
                })}
            </div>
            {this.state.showLoadMore &&
              (this.state.contentList !== undefined && this.state.contentList.length > 4) && (
                <div className="learning-load-more" onClick={this.loadMore.bind(this)}>
                  <span className="arrow-symbol">
                    <IconButton aria-label={tr('more')}>
                      <ArrowDown color={'#fff'} />
                    </IconButton>
                  </span>
                  {tr('Load More...')}
                </div>
              )}

            <br />
          </div>
        )}
      </div>
    );
  }
}

LearningPlanBlock.propTypes = {
  contentList: PropTypes.object,
  currentUser: PropTypes.object,
  learningTopic: PropTypes.string,
  type: PropTypes.string,
  redirectUrl: PropTypes.func,
  fetchDuration: PropTypes.func,
  team: PropTypes.object
};

LearningPlanBlock.defaultProps = {
  contentList: []
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  team: state.team.toJS()
}))(LearningPlanBlock);
