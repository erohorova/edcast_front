import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';
import LearningPlanBlock from '../../v4/LearningQueue/LearningPlanBlock';
import {
  fetchQuarterlyPlan,
  fetchCompetencyPlan
} from '../../../../actions/myLearningPlansActions';
import * as requests from 'edc-web-sdk/requests/myLearningPlan';
import { fetchUserInterests } from 'edc-web-sdk/requests/interests';
import { tr } from 'edc-web-sdk/helpers/translations';

class MyLearningContainer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.mlpOptions = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.mlp;

    this.state = {
      quarterlyPlan: ''
    };
    this.styles = {
      container: {
        margin: '20px 40px'
      },
      paperStyle: {
        padding: '25px'
      },
      heading: {
        fontSize: '20px',
        padding: '10px 0px',
        color: '#6f708b'
      },
      emptyBlock: {
        fontSize: '100%',
        padding: '90px 0px',
        textAlign: 'center'
        /*color:'#acadc1'*/
      }
    };
  }

  getAssignmentContentType() {
    let contentType = this.mlpOptions && this.mlpOptions['web/mlp/myAssignments']['defaultValue'];
    let card_type = ['course', 'pack', 'journey'];

    if (contentType == 'allContentType') {
      card_type = [];
    }

    return card_type;
  }

  componentDidMount() {
    let card_type = this.getAssignmentContentType();

    fetchUserInterests(this.props.currentUser.id)
      .then(interests => {
        interests.map(interest => {
          let payload = {
            limit: 5,
            offset: 0,
            mlp_period: 'skill-wise',
            learning_topics_name: interest.topic_name
          };

          if (card_type.length > 0) {
            payload['card_type[]'] = card_type;
          }

          this.props.dispatch(fetchCompetencyPlan(payload));
        });
      })
      .catch(err => {
        console.error(`Error in MyLearningContainer.fetchUserInterests.func : ${err}`);
      });

    let payload = {
      limit: 5,
      offset: 0,
      mlp_period: 'other'
    };

    if (card_type.length > 0) {
      payload['card_type[]'] = card_type;
    }
    this.props.dispatch(fetchCompetencyPlan(payload));

    let mlp_period = ['quarter-1', 'quarter-2', 'quarter-3', 'quarter-4', 'untimed'];

    mlp_period.map(period => {
      let payloadForQuarterlyPlan = {
        limit: 5,
        offset: 0,
        mlp_period: period
      };

      if (card_type.length > 0) {
        payloadForQuarterlyPlan['card_type[]'] = card_type;
      }
      this.props.dispatch(fetchQuarterlyPlan(payloadForQuarterlyPlan));
    });
  }

  fetchDuration = card => {
    if (card.assignable && card.assignable.eclDurationMetadata) {
      return card.assignable.eclDurationMetadata.calculated_duration_display;
    } else if (card.eclDurationMetadata) {
      return card.eclDurationMetadata.calculated_duration_display;
    } else {
      return 'NA';
    }
  };

  redirectUrl = card => {
    if (card.assignable.cardType === 'pack') {
      this.props.dispatch(push(`/pathways/${card.assignable.slug}`));
    } else if (card.assignable.cardType === 'journey') {
      this.props.dispatch(push(`/journey/${card.assignable.slug}`));
    } else {
      this.props.dispatch(push(card.assignUrl));
    }
  };

  render() {
    let _this = this;

    let quarterlyExists =
      (this.props.myLearningPlan['quarter-1'] &&
        this.props.myLearningPlan['quarter-1'].assignmentCount > 0) ||
      (this.props.myLearningPlan['quarter-2'] &&
        this.props.myLearningPlan['quarter-2'].assignmentCount > 0) ||
      (this.props.myLearningPlan['quarter-3'] &&
        this.props.myLearningPlan['quarter-3'].assignmentCount > 0) ||
      (this.props.myLearningPlan['quarter-4'] &&
        this.props.myLearningPlan['quarter-4'].assignmentCount > 0) ||
      (this.props.myLearningPlan['untimed'] &&
        this.props.myLearningPlan['untimed'].assignmentCount > 0) ||
      (this.props.myLearningPlan['undefined'] &&
        this.props.myLearningPlan['undefined'].assignmentCount > 0);

    let skillExists = false;
    if (this.props.myLearningPlan.skills) {
      Object.keys(this.props.myLearningPlan.skills).map((key, index) => {
        if (
          _this.props.myLearningPlan &&
          _this.props.myLearningPlan.skills[key] &&
          _this.props.myLearningPlan.skills[key].length > 0
        ) {
          skillExists = true;
        }
      });
    }

    let showEmptyState = !quarterlyExists && !skillExists;

    if (showEmptyState) {
      return (
        <div className="data-not-available-msg" style={this.styles.emptyBlock}>
          <p>
            {tr(
              'You don’t have anything in your Learning Plan yet. Assigned content will appear here'
            )}
          </p>
        </div>
      );
    }

    return (
      <div className="v3-container" style={this.styles.container}>
        {quarterlyExists && <p style={this.styles.heading}>{tr('Quarterly Plan')}</p>}

        {this.props.myLearningPlan['quarter-1'] &&
          this.props.myLearningPlan['quarter-1'].assignmentCount > 0 && (
            <div>
              <Paper style={this.styles.paperStyle}>
                <LearningPlanBlock
                  type={`quarter-1`}
                  contentList={this.props.myLearningPlan['quarter-1'].assignments}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={true}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              </Paper>
              <br />
            </div>
          )}

        {this.props.myLearningPlan['quarter-2'] &&
          this.props.myLearningPlan['quarter-2'].assignmentCount > 0 && (
            <div>
              <Paper style={this.styles.paperStyle}>
                <LearningPlanBlock
                  type={`quarter-2`}
                  contentList={this.props.myLearningPlan['quarter-2'].assignments}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCoursesLoadMore}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              </Paper>
              <br />
            </div>
          )}

        {this.props.myLearningPlan['quarter-3'] &&
          this.props.myLearningPlan['quarter-3'].assignmentCount > 0 && (
            <div>
              <Paper style={this.styles.paperStyle}>
                <LearningPlanBlock
                  type={`quarter-3`}
                  contentList={this.props.myLearningPlan['quarter-3'].assignments}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCoursesLoadMore}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              </Paper>
              <br />
            </div>
          )}

        {this.props.myLearningPlan['quarter-4'] &&
          this.props.myLearningPlan['quarter-4'].assignmentCount > 0 && (
            <div>
              <Paper style={this.styles.paperStyle}>
                <LearningPlanBlock
                  type={`quarter-4`}
                  contentList={this.props.myLearningPlan['quarter-4'].assignments}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCoursesLoadMore}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              </Paper>
              <br />
            </div>
          )}

        {this.props.myLearningPlan['untimed'] &&
          this.props.myLearningPlan['untimed'].assignmentCount > 0 && (
            <div>
              <Paper style={this.styles.paperStyle}>
                <LearningPlanBlock
                  type={`untimed`}
                  contentList={this.props.myLearningPlan['untimed'].assignments}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCoursesLoadMore}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              </Paper>
              <br />
            </div>
          )}

        {skillExists && <p style={this.styles.heading}>{tr('Competency Plan')}</p>}

        {this.props.myLearningPlan.skills &&
          Object.keys(this.props.myLearningPlan.skills).map((key, index) => {
            let item = _this.props.myLearningPlan.skills[key];
            let type = key !== 'undefined' ? key.split('.')[key.split('.').length - 1] : 'other';
            if (item && item.length > 0 && key !== 'undefined') {
              return (
                <div>
                  <Paper style={_this.styles.paperStyle}>
                    <LearningPlanBlock
                      type={type}
                      learningTopic={key}
                      contentList={item}
                      fetchDuration={_this.fetchDuration}
                      redirectUrl={_this.redirectUrl}
                      limit={_this.state.macroDataShowLimit}
                      loadMore={_this.loadMore}
                      showLoadMore={_this.state.showCoursesLoadMore}
                      cardsLoadMoreButtonText={_this.state.coursesLoadMoreButtonText}
                    />
                  </Paper>
                  <br />
                </div>
              );
            }
          })}

        {this.props.myLearningPlan.skills &&
          Object.keys(this.props.myLearningPlan.skills).map((key, index) => {
            let item = _this.props.myLearningPlan.skills[key];
            let type = key !== 'undefined' ? key.split('.')[key.split('.').length - 1] : 'other';
            if (item && item.length > 0 && key == 'undefined') {
              return (
                <div>
                  <Paper style={_this.styles.paperStyle}>
                    <LearningPlanBlock
                      type={type}
                      learningTopic={key}
                      contentList={item}
                      fetchDuration={_this.fetchDuration}
                      redirectUrl={_this.redirectUrl}
                      limit={_this.state.macroDataShowLimit}
                      loadMore={_this.loadMore}
                      showLoadMore={_this.state.showCoursesLoadMore}
                      cardsLoadMoreButtonText={_this.state.coursesLoadMoreButtonText}
                    />
                  </Paper>
                  <br />
                </div>
              );
            }
          })}
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    myLearningPlan: state.myLearningPlan.toJS(),
    team: state.team.toJS()
  };
}

MyLearningContainer.propTypes = {
  myLearningPlan: PropTypes.object,
  currentUser: PropTypes.object,
  team: PropTypes.object
};

export default connect(mapStoreStateToProps)(MyLearningContainer);
