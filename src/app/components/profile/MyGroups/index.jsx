import React from 'react';
import { tr } from 'edc-web-sdk/helpers/translations';

export default class MyGroups extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return <div>{tr('My Groups page')}</div>;
  }
}
