import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Tabs, Tab } from 'material-ui/Tabs';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import ReactDOM from 'react-dom';
import colors from 'edc-web-sdk/components/colors/index';
import {
  getPublicProfile,
  removePublicProfile,
  loadingPublicProfileCards
} from '../../actions/usersActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import ProfileTabs from './ProfileTabs';
import PublicProfileViewV4 from './PublicProfileViewV4';
import ProfileHeader from './ProfileHeader';
import Spinner from '../common/spinner';
import MyProfileContainerv3 from './../../components/profile/v3/MyProfile/index';
import { getSpecificUserInfo } from '../../actions/currentUserActions';

/*
 Tab container components
 */

class ProfileContainerv3 extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      activeTab: {},
      open: false,
      isLoadingPublicProfile: true,
      publicProfileViewV4: window.ldclient.variation('public-profile-view-v4', false)
    };
    this.styles = {
      popover: {
        width: ''
      },
      contentTabs: {
        background: 'transparent'
      }
    };
    this.tabs = [
      { path: '/me', label: 'Profile', pathsList: ['/me'] },
      { path: '/me/career-advisor', label: 'Career Advisor' },
      { path: '/me/groups', label: 'Groups' },
      {
        path: '/me/content',
        label: 'Content',
        options: [
          { path: '/me/dashboard', label: 'Performance' },
          { path: '/me/content', label: 'SmartCards' },
          { path: '/me/channels', label: 'Channels' }
        ],
        pathsList: [
          '/me/dashboard',
          '/me/content',
          '/me/channels',
          '/me/content/Pathways',
          '/me/content/VideoStreams',
          '/me/content/SmartBites',
          '/me/content/all'
        ]
      },
      {
        path: '/me/learning',
        label: 'My Learning Plan',
        pathsList: [
          '/me/learning',
          '/me/learning/all',
          '/me/learning/active',
          '/me/learning/completed',
          '/me/learning/bookmarks'
        ]
      },
      {
        path: '/me/team',
        label: 'Team',
        pathsList: ['/me/team/following', '/me/team/followers'],
        options: [{ path: '/me/leaderboard', label: 'Leaderboard' }]
      },
      { path: '/me/channels', label: 'Channels' }
    ];
  }

  async componentDidMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'handle',
        'bio',
        'roles',
        'rolesDefaultNames',
        'followersCount',
        'followingCount',
        'coverImage',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);

    if (this.props.params && this.props.params.handle) {
      this.props.dispatch(loadingPublicProfileCards(true));
      this.props
        .dispatch(getPublicProfile('@' + this.props.params.handle, 'componentDidMount'))
        .then(() => {
          this.setState({ isLoadingPublicProfile: false });
        })
        .catch(err => {
          console.error(`Error in ProfileContainerv3.getPublicProfile.func : ${err}`);
        });
    } else {
      this.setState({ isLoadingPublicProfile: false });
      this.props.dispatch(removePublicProfile());
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params && this.props.params.handle && !nextProps.params.handle) {
      this.props.dispatch(removePublicProfile());
    } else if (
      nextProps.params.handle !== (this.props.params && this.props.params.handle) ||
      (!(this.props.params && this.props.params.handle) && nextProps.params.handle)
    ) {
      this.setState({ isLoadingPublicProfile: true });
      this.props.dispatch(loadingPublicProfileCards(true));
      this.props
        .dispatch(getPublicProfile(`@${nextProps.params.handle}`))
        .then(() => {
          this.setState({ isLoadingPublicProfile: false });
        })
        .catch(err => {
          console.error(
            `Error in ProfileContainerv3.componentWillReceiveProps.getPublicProfile.func : ${err}`
          );
        });
    }
  }

  /*
   MUI handler to push to a new route
   */
  handleTabChange = path => {
    this.setState({
      open: false
    });
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  handleTabHover = tab => {
    if (tab.options) {
      this.setState({
        activeTab: tab,
        open: true,
        hoveredTab: tab.label
      });
    } else {
      this.setState({ hoveredTab: tab.label });
    }
  };

  handleTabLeave = tab => {
    this.setState({ hoveredTab: false });
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(
      function(tab) {
        if (currentPath === tab.path || (tab.pathsList && tab.pathsList.includes(currentPath))) {
          value = tab.path;
        }
      }.bind(this)
    );
    return value;
  };

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  renderTab = (tab, index, label) => {
    let tabHoverColor = this.state.hoveredTab === tab.label ? { color: colors.primary200 } : {};
    return (
      <Tab
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        label={tr(label)}
        style={tabHoverColor}
        value={tab.path}
        className={this.toCamelCase('me ' + tab.label)}
        key={index}
        onClick={this.handleTabChange.bind(this, tab.path)}
        onMouseOver={this.handleTabHover.bind(this, tab)}
        onMouseLeave={this.handleTabLeave.bind(this, tab)}
      />
    );
  };

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.filter(tab => tab.visible);
  }

  render() {
    let tabValue = this.selectTabValue(this.props.pathname);
    let output = [];
    let ProfileController = this.listTab(this.props.team.OrgConfig.profile);
    let isChannelsOn = Boolean(
      ProfileController.find(item => item.key === 'web/profile/channels' && item.visible)
    );

    ProfileController.sort((a, b) => a.index - b.index);

    ProfileController.filter(item => item.visible).map(item => {
      let label = item.label || item.defaultLabel;
      switch (item.key) {
        case 'web/profile/profile':
          output.push(this.renderTab(this.tabs[0], item.index, label));
          break;
        case 'web/profile/groups':
          output.push(this.renderTab(this.tabs[1], item.index, label));
          break;
        case 'web/profile/content':
          let contentTabs = this.tabs[2];
          if (isChannelsOn && contentTabs.options) {
            contentTabs.options = contentTabs.options.filter(
              itemValue => itemValue.label !== 'Channels'
            );
          }

          contentTabs.options = null;
          output.push(this.renderTab(contentTabs, item.index, label));
          break;
        case 'web/profile/learningQueue':
          output.push(this.renderTab(this.tabs[3], item.index, label));
          break;
        case 'web/profile/teams':
          output.push(this.renderTab(this.tabs[4], item.index, label));
          break;
        case 'web/profile/channels':
          output.push(this.renderTab(this.tabs[5], item.index, label));
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });
    let showTopNav = tabValue !== '/me';

    if (this.state.isLoadingPublicProfile) {
      return (
        <div className="container-padding text-center">
          <Spinner />
        </div>
      );
    }

    return (
      <section id="profile-v3">
        <Popover
          open={this.state.open}
          anchorEl={this.state.activeTab.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
          useLayerForClickAway={false}
        >
          <Menu onMouseLeave={this.handleRequestClose}>
            {this.state.activeTab.options &&
              this.state.activeTab.options.map((option, index) => {
                return (
                  <MenuItem
                    key={index}
                    primaryText={tr(option.label)}
                    className={this.toCamelCase('me ' + option.label)}
                    onTouchTap={this.handleTabChange.bind(this, option.path)}
                  />
                );
              })}
          </Menu>
        </Popover>
        {showTopNav && <div className="profile" id="user-main-info" />}
        <div>
          <ProfileHeader
            publicProfile={this.props.params && this.props.params.handle}
            profileShowUserContent={
              this.props.team &&
              this.props.team.OrgConfig &&
              this.props.team.OrgConfig.profileShowUserContent
            }
            publicPrivateProfileBtn={
              !!(
                this.props.team &&
                this.props.team.config &&
                this.props.team.config.enable_profile_view_buttons
              )
            }
          />
          {!(this.props.params && this.props.params.handle) && <ProfileTabs />}
          {this.props.params && this.props.params.handle && this.state.publicProfileViewV4 && (
            <PublicProfileViewV4 handle={this.props.params.handle} />
          )}
          <div>{this.props.children}</div>
        </div>
      </section>
    );
  }
}

ProfileContainerv3.propTypes = {
  user: PropTypes.object,
  team: PropTypes.object,
  currentUser: PropTypes.object,
  params: PropTypes.object,
  children: PropTypes.any,
  pathname: PropTypes.string,
  topNav: PropTypes.object
};

/*
 @pathname: render updates based on the path changing
 */
function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    topNav: state.config.toJS(),
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(ProfileContainerv3);
