import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import { Tabs, Tab } from 'material-ui/Tabs';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import Spinner from '../../../common/spinner';

import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';
import { removeSkill, getRecommendedUsers } from 'edc-web-sdk/requests/users';
import { cards as cardsSDK } from 'edc-web-sdk/requests/index';
import transcript from 'edc-web-sdk/requests/transcript';
import { refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';

import {
  getIntegrationList,
  getUserSkills,
  getUserByHandle
} from '../../../../actions/usersActions';
import { openIntegrationModal, openSkillCreationModal } from '../../../../actions/modalActions';
import { getCoursesV2 } from '../../../../actions/coursesActions';
import ConfirmationModal from '../../../modals/ConfirmationCommentModal';
import Carousel from '../../../common/Carousel';
import Bia from '../../../common/Bia';
import CourseCarousel from '../../LearnersDashboard/CourseCarousel';
import CardCarousel from '../../LearnersDashboard/CardCarousel';
import PathwayBadges from '../../PathwayBadges';
import * as actionTypes from '../../../../constants/actionTypes';
import Influencer from '../../../discovery/Influencer.jsx';
import getFleIcon from '../../../../utils/getFileIcon';
import fileDownload from 'js-file-download';

class MyProfile extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      popover: {
        width: ''
      },
      editIcon: {
        width: 14,
        height: 14,
        color: '#4990e2',
        verticalAlign: 'middle'
      },
      editIconNew: {
        width: 16,
        height: 16,
        color: '#6f708b',
        verticalAlign: 'middle'
      },
      download: {
        marginTop: '10px',
        textAlign: 'right'
      },
      summaryEditIcon: {
        width: 14,
        height: 14,
        color: window.ldclient.variation('additional-profile-info', false) ? '#6f708b' : '#4990e2',
        verticalAlign: 'middle'
      }
    };

    this.state = {
      activeTab: {},
      open: false,
      enableWallet: this.props.team.config.wallet || false,
      clc: window.ldclient.variation('clc', false),
      skillsMap: window.ldclient.variation('skillsmap', false),
      integrationList: [],
      courseState: 'Content Loading...',
      courses: [],
      enableExternalIntegrations: window.ldclient.variation('enable-external-integrations', false),
      cardsCount: 0,
      totalCardsNumber: 0,
      cardState: ['published'],
      openConfirm: false,
      removeId: '',
      userSumaryExpand: false,
      userBadgings:
        this.props.team &&
        this.props.team.OrgConfig &&
        this.props.team.OrgConfig.badgings['web/badgings'],
      showRecommendedContent: window.ldclient.variation('winterstorm', false),
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      upgradeSkillsPassport:
        this.props.team &&
        this.props.team.OrgConfig &&
        !this.props.team.OrgConfig.profileShowUserContent,
      isTranscriptEnabled: window.ldclient.variation('user-transcript', false),
      isHideUploadCredential: window.ldclient.variation('hide-upload-credential', false),
      additionalProfileInfoEnabled: window.ldclient.variation('additional-profile-info', false),
      enableBIA: this.props.team.config.enabled_bia,
      publicProfileViewV4: window.ldclient.variation('public-profile-view-v4', false),
      showProfileRecommendSme: window.ldclient.variation('profile-recommend-sme', false)
    };

    this.tabs = [
      { path: '/me', label: 'Profile', pathsList: ['/me'] },
      { path: '/me/groups', label: 'Groups' },
      {
        path: '/me/content',
        label: 'Content',
        options: [
          { path: '/me/dashboard', label: 'Performance' },
          { path: '/me/content', label: 'SmartCards' },
          { path: '/me/channels', label: 'Channels' }
        ],
        pathsList: [
          '/me/dashboard',
          '/me/content',
          '/me/channels',
          '/me/content/Pathways',
          '/me/content/VideoStreams',
          '/me/content/SmartBites',
          '/me/content/all'
        ]
      },
      {
        path: '/me/learning',
        label: 'My Learning Plan',
        pathsList: [
          '/me/learning',
          '/me/learning/all',
          '/me/learning/active',
          '/me/learning/completed',
          '/me/learning/bookmarks'
        ]
      },
      {
        path: '/me/team',
        label: 'Team',
        pathsList: ['/me/team/following', '/me/team/followers'],
        options: [{ path: '/me/leaderboard', label: 'Leaderboard' }]
      },
      { path: '/me/channels', label: 'Channels' }
    ];

    this.modalSkillOpen = this.modalSkillOpen.bind(this);
    this.importClickHandler = this.importClickHandler.bind(this);
    this.removeSkill = this.removeSkill.bind(this);
    this.userHandle = '';
  }

  getUserDetails(props) {
    let _this = this;
    this.userHandle =
      (props.params && props.params.handle) ||
      (props.currentUser && props.currentUser.handle && this.props.currentUser.handle);
    if (this.userHandle) {
      this.props
        .dispatch(getUserByHandle('@' + this.userHandle))
        .then(({ id, coverImage, user }) => {
          this.props.dispatch({
            type: actionTypes.LOADING_PUBLIC_PROFILE_CARDS,
            loading: true
          });
          cardsSDK
            .getUserCards(
              id,
              10,
              undefined,
              undefined,
              undefined,
              this.state.cardState,
              undefined,
              true
            )
            .then(data => {
              let cards = data[0];
              let totalCardsNumber = data[1];
              _this.setState({ cards, totalCardsNumber });
              _this.props.dispatch({
                type: actionTypes.LOADING_PUBLIC_PROFILE_CARDS,
                loading: false
              });
            })
            .catch(err => {
              console.error(`Error in MyProfile.getUserCards.func : ${err}`);
            });
        })
        .catch(err => {
          console.error(`Error in MyProfile.getUserByHandle.func : ${err}`);
        });
    }
  }

  componentDidMount() {
    if (this.props.currentUser && this.props.currentUser.isLoggedIn) {
      this.getUserDetails(this.props);
    }

    if (this.state.showProfileRecommendSme && this.state.showRecommendedContent) {
      getRecommendedUsers({ limit: 10 })
        .then(users => {
          this.setState({ recommendedItems: users });
        })
        .catch(err => {
          console.error(`Error in MyProfile.getRecommendedUsers.func : ${err}`);
        });
    }

    if (this.props.pathname.indexOf('/@') > -1) {
      return;
    }

    this.fetchIntegrations();
    this.props.dispatch(getUserSkills(this.props.currentUser.id));

    this.props
      .dispatch(getCoursesV2(this.props.currentUser.id))
      .then(response => {
        this.setState({
          courses: response && response.externalCourses,
          courseState:
            response && response.externalCourses && response.externalCourses.length > 0
              ? ''
              : 'No Courses undertaken'
        });
      })
      .catch(err => {
        console.error(`Error in MyProfile.getCoursesV2.func : ${err}`);
      });
  }

  componentWillReceiveProps(nextProps) {
    let loadUserDetails =
      (nextProps.params && nextProps.params.handle) !==
        (this.props.params && this.props.params.handle) || false;

    if (
      (this.props.currentUser.publicProfile && !nextProps.currentUser.publicProfile) ||
      loadUserDetails
    ) {
      if (typeof nextProps.params.handle === 'undefined' || loadUserDetails) {
        this.getUserDetails(nextProps);
      }
      this.fetchIntegrations();
      this.props.dispatch(getUserSkills(this.props.currentUser.id));

      this.props
        .dispatch(getCoursesV2(this.props.currentUser.id))
        .then(response => {
          this.setState({
            courses: response && response.externalCourses,
            courseState:
              response && response.externalCourses && response.externalCourses.length > 0
                ? ''
                : 'No Courses undertaken'
          });
        })
        .catch(err => {
          console.error(`Error in MyProfile.componentWillReceiveProps.getCoursesV2.func : ${err}`);
        });
    }
  }

  showMyScore(val) {
    let show = val;
    if (show === undefined) {
      return true;
    } else {
      return show.visible;
    }
  }

  toCamelCase(str) {
    return str
      .replace(/\s(.)/g, function($1) {
        return $1.toUpperCase();
      })
      .replace(/\s/g, '')
      .replace(/^(.)/, function($1) {
        return $1.toLowerCase();
      });
  }

  renderTab = (tab, index, label) => {
    let tabHoverColor = this.state.hoveredTab === tab.label ? { color: colors.primary200 } : {};
    return (
      <Tab
        ref={node => {
          tab.anchorEl = ReactDOM.findDOMNode(node);
        }}
        label={tr(label)}
        style={tabHoverColor}
        value={tab.path}
        className={this.toCamelCase('me ' + tab.label)}
        key={index}
        onClick={this.handleTabChange.bind(this, tab.path)}
        onMouseOver={this.handleTabHover.bind(this, tab)}
        onMouseLeave={this.handleTabLeave.bind(this, tab)}
      />
    );
  };

  handleTabChange = path => {
    this.setState({
      open: false
    });
    if (this.props.pathname === path || (this.props.pathname === '/me' && path === '')) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
  };

  selectTabValue = currentPath => {
    let value = currentPath;
    this.tabs.forEach(
      function(tab) {
        if (currentPath === tab.path || (tab.pathsList && tab.pathsList.includes(currentPath))) {
          value = tab.path;
        }
      }.bind(this)
    );
    return value;
  };

  handleTabLeave = tab => {
    this.setState({ hoveredTab: false });
  };

  handleTabHover = tab => {
    if (tab.options) {
      this.setState({
        activeTab: tab,
        open: true,
        hoveredTab: tab.label
      });
    } else {
      this.setState({ hoveredTab: tab.label });
    }
  };

  listTab(obj) {
    let tabs = [];
    if (obj) {
      Object.keys(obj).forEach(key => {
        let listObj = obj[key];
        listObj['key'] = key;
        if (!listObj.index) {
          listObj['index'] = -1;
        }
        tabs.push(listObj);
      });
      tabs.sort((a, b) => {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }
        return 0;
      });
    }
    return tabs.filter(tab => tab.visible);
  }

  toSettingsPage = e => {
    e.preventDefault();
    this.props.dispatch(push('/settings'));
  };

  fetchIntegrations() {
    this.props
      .dispatch(getIntegrationList(this.props.currentUser.id))
      .then(result => {
        this.setState({ integrationList: result, pending: false });
      })
      .catch(err => {
        console.error(`Error in MyProfile.getIntegrationList.func : ${err}`);
      });
  }

  handleConnectClick = integration => {
    this.props.dispatch(openIntegrationModal(integration, this.importClickHandler));
  };

  handleConnectKeyPress = (e, integration) => {
    if (e.keyCode === 13) {
      this.handleConnectClick(integration);
    }
  };

  importClickHandler(integration) {
    let index = this.state.integrationList.findIndex(i => i.id === integration.id);
    this.state.integrationList[index].connected = !this.state.integrationList[index].connected;

    this.props.dispatch({
      type: actionTypes.OPEN_SNACKBAR,
      message: 'Your courses will be imported shortly',
      autoClose: true
    });

    this.setState({
      integrationList: this.state.integrationList
    });
  }
  modalSkillOpenToAddSkills = (e, skill) => {
    e.preventDefault();
    this.modalSkillOpen(skill);
  };
  modalSkillOpen = (skill = null) => {
    this.props.dispatch(openSkillCreationModal(skill));
  };

  toggleModal = id => {
    this.setState({ openConfirm: !this.state.openConfirm, removeId: id });
  };

  removeSkill() {
    this.setState({ openConfirm: false });
    let payload = {
      skills_user_id: this.state.removeId,
      id: this.props.currentUser.id
    };
    removeSkill(this.props.currentUser.id, payload)
      .then(data => {
        this.props.dispatch(getUserSkills(this.props.currentUser.id));
      })
      .catch(err => {
        console.error(`Error in MyProfile.getUserSkills.func : ${err}`);
      });
  }

  expandBioSection = () => {
    this.setState({
      userSumaryExpand: !this.state.userSumaryExpand
    });
  };

  removeCardFromList(cardId) {
    this.setState(prevState => {
      let cards = prevState.cards.filter(item => item.id != cardId);
      return { cards };
    });
  }

  openTeam = e => {
    e.preventDefault();
    this.props.dispatch(push(this.state.isNewProfileNavigation ? '/team' : '/me/team'));
  };

  getTranscript = e => {
    e.preventDefault();
    transcript()
      .then(response => {
        fileDownload(response.xhr.response, 'Transcript.pdf');
      })
      .catch(err => {
        console.error(`Error in MyProfile.getTranscript.func : ${err}`);
      });
  };

  openUrl = (skill, skillCred, flag) => {
    if (flag && skill.credentialUrl) {
      window.open(skill.credentialUrl, '_blank');
    } else {
      if (skillCred) {
        refreshFilestackUrl(skillCred.url)
          .then(resp => {
            window.open(resp.signed_url, '_blank');
          })
          .catch(err => {
            console.error(`Error in MyProfile.openUrl.refreshFilestackUrl.func : ${err}`);
          });
      }
    }
  };

  render() {
    let showContinuousLearning = this.props.team.OrgConfig.sections[
      'web/sections/continuousLearning'
    ].visible;
    let showMyScoreVal = this.props.team.OrgConfig.sections['web/sections/myScore'];
    let showMyScore = this.showMyScore(showMyScoreVal);
    let tabValue = this.selectTabValue(this.props.pathname);
    let output = [];
    let user =
      (this.props.currentUser &&
        this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.profile) ||
      this.props.currentUser;

    let ProfileController = this.listTab(this.props.team.OrgConfig.profile);
    let isChannelsOn = Boolean(
      ProfileController.find(item => item.key === 'web/profile/channels' && item.visible)
    );
    let loadingPublicProfileCards =
      this.props.currentUser && this.props.currentUser.loadingPublicProfileCards;

    ProfileController.sort((a, b) => a.index - b.index);

    ProfileController.filter(item => item.visible).map(item => {
      let label = item.label || item.defaultLabel;
      switch (item.key) {
        case 'web/profile/profile':
          output.push(this.renderTab(this.tabs[0], item.index, label));
          break;
        case 'web/profile/groups':
          output.push(this.renderTab(this.tabs[1], item.index, label));
          break;
        case 'web/profile/content':
          let contentTabs = this.tabs[2];
          if (isChannelsOn && contentTabs.options) {
            contentTabs.options = contentTabs.options.filter(
              itemValue => itemValue.label !== 'Channels'
            );
          }

          contentTabs.options = null;
          output.push(this.renderTab(contentTabs, item.index, label));
          break;
        case 'web/profile/learningQueue':
          output.push(this.renderTab(this.tabs[3], item.index, label));
          break;
        case 'web/profile/teams':
          output.push(this.renderTab(this.tabs[4], item.index, label));
          break;
        case 'web/profile/channels':
          output.push(this.renderTab(this.tabs[5], item.index, label));
          break;
        default:
          // FIXME: implement default case
          break;
      }
    });

    let publicProfile = this.props.params && this.props.params.handle;

    let courses = !publicProfile
      ? this.state.courses
      : (this.props.currentUser &&
          this.props.currentUser.publicProfile &&
          this.props.currentUser.publicProfile.externalCourses) ||
        [];
    let courseState = courses.length > 0 ? '' : 'No Courses undertaken';
    user.userSkills = !publicProfile
      ? this.props.currentUser && this.props.currentUser.userSkills
      : {
          skills:
            (this.props.currentUser &&
              this.props.currentUser.publicProfile &&
              this.props.currentUser.publicProfile.skills) ||
            []
        };

    let _this = this;

    let cards = (this.state.cards && Object.values(this.state.cards)) || [];
    let userBio = this.state.userSumaryExpand
      ? user.bio
      : user.bio && user.bio.length > 300
      ? user.bio && user.bio.substr(0, 300) + '...'
      : user.bio;
    let showSkillsPassport =
      (user && user.bio) ||
      (courses && courses.length) ||
      (user && user.userSkills && user.userSkills.skills && user.userSkills.skills.length) ||
      (this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userCardBadges &&
        this.props.currentUser.publicProfile.userCardBadges.length) ||
      (this.props.currentUser.publicProfile &&
        this.props.currentUser.publicProfile.userClcBadges &&
        this.props.currentUser.publicProfile.userClcBadges.length);

    let SPLabel = tr('Skills Passport');
    let iconFileSrc = getFleIcon('pdf');

    return (
      <div id="profile-wrapper" className="row full-width-row">
        <div id="profile" className="horizontal-spacing-xlarge">
          <div className="profile-container small-12 profile-container-full">
            <div className="width-100">
              <div className="small-12 profile-box">
                {(user.bio || !publicProfile) && (
                  <Paper
                    className={`container-padding ${this.state.additionalProfileInfoEnabled &&
                      'container-padding__summary'}`}
                  >
                    {!publicProfile && (
                      <div className="float-right">
                        <button aria-label={tr('Edit Profile')} onClick={this.toSettingsPage}>
                          <span tabIndex={-1} className="hideOutline">
                            <EditIcon style={this.styles.summaryEditIcon} />
                          </span>
                        </button>
                      </div>
                    )}
                    <div className="user-summary">
                      <div dangerouslySetInnerHTML={{ __html: userBio }} />
                      {user && user.bio && user.bio.length > 300 && (
                        <div className="show-more">
                          <span onClick={this.expandBioSection.bind(this)}>
                            {tr(this.state.userSumaryExpand ? 'Show less...' : 'Show more...')}{' '}
                            <i
                              className="arrow-icon"
                              style={{
                                borderWidth: this.state.userSumaryExpand
                                  ? '0px 2px 2px 0px'
                                  : '2px 0px 0px 2px'
                              }}
                            />
                          </span>
                        </div>
                      )}
                      {!user.bio && !publicProfile && (
                        <div>
                          <a href="#" onClick={this.toSettingsPage}>
                            {tr('Click here to add summary')}
                          </a>
                        </div>
                      )}
                    </div>
                  </Paper>
                )}
                <div className="row" style={{ padding: '0 1.25rem 0 0.25rem' }}>
                  {publicProfile ? (
                    !!showSkillsPassport && (
                      <div className="passport-heading small-11" role="heading" aria-level="3">
                        {SPLabel} <sup>TM</sup>
                      </div>
                    )
                  ) : (
                    <div className="passport-heading small-10" role="heading" aria-level="3">
                      {SPLabel} <sup>TM</sup>
                    </div>
                  )}
                  {!publicProfile && this.state.isTranscriptEnabled && (
                    <div style={this.styles.download} className="small-2">
                      <a
                        onClick={this.getTranscript}
                        href="JavaScript:void(0);"
                        style={{ borderBottom: '1px solid #2199e8' }}
                      >
                        {tr('Download Transcript')}
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>

            <div className="width-100">
              {!this.state.userBadgings ? (
                <PathwayBadges type={'CardBadge'} />
              ) : (
                (this.state.userBadgings.pathwayBadgings ||
                  this.state.userBadgings.clcBadgings) && (
                  <PathwayBadges
                    type={'CardBadge'}
                    params={this.props.params}
                    publicProfile={publicProfile}
                  />
                )
              )}
            </div>

            {!publicProfile && this.state.enableExternalIntegrations && (
              <div className="width-100">
                <div className="small-12 profile-box">
                  <Paper className="container-padding">
                    <div className="header-text" role="heading" aria-level="4">
                      {tr('Account Linking')}
                      {(this.state.upgradeSkillsPassport ||
                        this.state.additionalProfileInfoEnabled) && (
                        <span> ({this.state.integrationList.length})</span>
                      )}
                    </div>
                    <div className="integration-section">
                      <Carousel slidesToShow={6} key="integrations">
                        {this.state.integrationList.map(integration => {
                          return (
                            <div key={integration.id}>
                              {this.state.upgradeSkillsPassport ||
                              this.state.additionalProfileInfoEnabled ? (
                                <div className="integration-logo integration-logo__new-skill-passport">
                                  <div className="integration-logo__image-container">
                                    <img
                                      tabIndex={0}
                                      alt={integration.name}
                                      className={`integration-logo__image ${
                                        !integration.connected ? 'integration-logo__image-fade' : ''
                                      }`}
                                      src={integration.logoUrl}
                                    />
                                  </div>
                                  <span
                                    tabIndex={0}
                                    onTouchTap={() => this.handleConnectClick(integration)}
                                    onKeyDown={e => this.handleConnectKeyPress(e, integration)}
                                  >
                                    <span
                                      tabIndex="-1"
                                      className={`integration-logo__action  hideOutline ${
                                        integration.connected ? 'integration-logo__connected ' : ''
                                      }`}
                                    >
                                      {integration.connected ? tr('Connected') : tr('Connect')}
                                    </span>
                                  </span>
                                </div>
                              ) : (
                                <div className="integration-logo">
                                  <span className="helper" />
                                  <span
                                    tabIndex={0}
                                    onKeyDown={e => this.handleConnectKeyPress(e, integration)}
                                    onTouchTap={() => this.handleConnectClick(integration)}
                                  >
                                    <img
                                      tabIndex="-1"
                                      className="hideOutline"
                                      alt={integration.name}
                                      src={integration.logoUrl}
                                    />
                                  </span>
                                </div>
                              )}
                            </div>
                          );
                        })}
                      </Carousel>
                    </div>
                  </Paper>
                </div>
              </div>
            )}

            {this.state.enableExternalIntegrations && !this.state.upgradeSkillsPassport && (
              <div className="width-100">
                <div className="small-12 profile-box">
                  {courses.length > 0 && <CourseCarousel courses={courses} numberOfCourses={6} />}
                </div>
              </div>
            )}

            <div className="width-100 skills-section">
              {user.userSkills &&
                user.userSkills.skills.map(skill => {
                  let skillCred = skill.credential && skill.credential[0];
                  let isCredIcon =
                    skillCred &&
                    skillCred.mimetype &&
                    !~skillCred.mimetype.indexOf(`image/`) &&
                    iconFileSrc;
                  return (
                    <div className="small-6 profile-box" key={skill.id}>
                      {this.state.upgradeSkillsPassport ||
                      this.state.additionalProfileInfoEnabled ? (
                        <Paper className="container-padding skill-block_new">
                          {!publicProfile && (
                            <div className="float-right icon-block">
                              <button
                                aria-label={tr('Delete skill')}
                                onClick={this.toggleModal.bind(this, skill.id)}
                              >
                                <DeleteIcon style={this.styles.editIconNew} />
                              </button>
                              <button
                                aria-label={tr('Edit skill')}
                                onClick={this.modalSkillOpen.bind(this, skill)}
                              >
                                <span tabIndex={-1} className="hideOutline">
                                  <EditIcon style={this.styles.editIconNew} />
                                </span>
                              </button>
                            </div>
                          )}
                          <div className="header-text " role="heading" aria-level="4">
                            {tr('Skill Name')}: {skill.name}
                          </div>
                          <div
                            className={`skill-information ${
                              this.state.upgradeSkillsPassport ||
                              this.state.additionalProfileInfoEnabled
                                ? 'skill-information__smaller-font'
                                : ''
                            }`}
                          >
                            {this.state.enableBIA && (
                              <div
                                className={`row ${
                                  skill.skillLevel ? 'margin-bottom-10' : 'margin-bottom-20'
                                } vertical-align-center`}
                              >
                                <div className="column small-2 text-lighter">{tr('Level')}:</div>
                                <div className="column small-9">
                                  <Bia
                                    card={skill}
                                    skillValue={true}
                                    isNotAbsolutePosition={true}
                                    allowTooltip={false}
                                    isClickable={false}
                                  />
                                </div>
                              </div>
                            )}
                            <div className="row margin-bottom-20">
                              <div className="column small-2 text-lighter">{tr('Experience')}:</div>
                              <div className="column small-9">{tr(skill.experience) || '-'}</div>
                            </div>
                            {!this.state.isHideUploadCredential && (
                              <div className="row margin-bottom-20 vertical-align-center">
                                <div className="column small-2 text-lighter">
                                  {tr('Credentials')}:
                                </div>
                                <div className="column small-9 skill-cred">
                                  {!(
                                    (skillCred && skillCred.url) ||
                                    skill.credentialUrl ||
                                    skill.credentialName
                                  ) && <span>-</span>}
                                  <a
                                    target="_blank"
                                    onClick={() => this.openUrl(skill, skillCred, true)}
                                  >
                                    <span
                                      className={
                                        this.state.upgradeSkillsPassport ||
                                        this.state.additionalProfileInfoEnabled
                                          ? 'credName'
                                          : ''
                                      }
                                    >
                                      {skill.credentialName || ''}
                                    </span>
                                    {isCredIcon ? (
                                      <img
                                        className="iconFileSrc"
                                        src={iconFileSrc}
                                        title={tr('Source of the content')}
                                      />
                                    ) : (
                                      <img
                                        className={
                                          this.state.upgradeSkillsPassport ||
                                          this.state.additionalProfileInfoEnabled
                                            ? 'cred-img-margin'
                                            : ''
                                        }
                                        src={skillCred && skillCred.url}
                                      />
                                    )}
                                  </a>
                                </div>
                              </div>
                            )}
                          </div>
                        </Paper>
                      ) : (
                        <Paper className="container-padding">
                          {!publicProfile && (
                            <div className="float-right">
                              <button
                                aria-label={tr('Delete skill')}
                                onClick={this.toggleModal.bind(this, skill.id)}
                              >
                                <span tabIndex={-1} className="hideOutline">
                                  <DeleteIcon style={this.styles.editIcon} />
                                </span>
                              </button>
                              <button
                                aria-label={tr('Edit skill')}
                                onClick={this.modalSkillOpen.bind(this, skill)}
                              >
                                <span tabIndex={-1} className="hideOutline">
                                  <EditIcon style={this.styles.editIcon} />
                                </span>
                              </button>
                            </div>
                          )}
                          {this.state.enableBIA && (
                            <Bia card={skill} skillValue={true} isClickable={false} />
                          )}
                          <div className="header-text" role="heading" aria-level="4">
                            {skill.name}
                          </div>
                          <div className="row margin-bottom-20">
                            <div className="column small-2 strong">{tr('Experience')}</div>
                            <div className="column small-9">{tr(skill.experience) || '-'}</div>
                          </div>
                          <div className="row margin-bottom-20">
                            <div className="column small-2 strong">{tr('Description')}</div>
                            <div className="column small-9">{skill.description || '-'}</div>
                          </div>
                          {!this.state.isHideUploadCredential && (
                            <div className="row margin-bottom-20">
                              <div className="column small-2 strong">{tr('Credentials')}</div>
                              <div className="column small-9 skill-cred">
                                {!(
                                  (skillCred && skillCred.url) ||
                                  skill.credentialUrl ||
                                  skill.credentialName
                                ) && <span>-</span>}
                                <a
                                  target="_blank"
                                  onClick={() => this.openUrl(skill, skillCred, true)}
                                >
                                  <span
                                    className={
                                      this.state.upgradeSkillsPassport ||
                                      this.state.additionalProfileInfoEnabled
                                        ? 'credName'
                                        : ''
                                    }
                                  >
                                    {skill.credentialName || ''}
                                  </span>
                                  {isCredIcon ? (
                                    <img
                                      className="iconFileSrc"
                                      src={iconFileSrc}
                                      title={tr('Source of the content')}
                                    />
                                  ) : (
                                    <img
                                      className={
                                        this.state.upgradeSkillsPassport ||
                                        this.state.additionalProfileInfoEnabled
                                          ? 'cred-img-margin'
                                          : ''
                                      }
                                      src={skillCred && skillCred.url}
                                    />
                                  )}
                                </a>
                              </div>
                            </div>
                          )}
                        </Paper>
                      )}
                    </div>
                  );
                })}

              {!publicProfile &&
                (!this.state.upgradeSkillsPassport && !this.state.additionalProfileInfoEnabled ? (
                  <a
                    href="#"
                    onClick={e => this.modalSkillOpenToAddSkills(e, null)}
                    className="small-6 profile-box"
                  >
                    <Paper className="container-padding add-more-skill">
                      <div className="">
                        +<div>{tr('Add Skill')}</div>
                      </div>
                    </Paper>
                  </a>
                ) : (
                  <a
                    href="#"
                    onClick={e => this.modalSkillOpenToAddSkills(e, null)}
                    className="small-12 profile-box"
                  >
                    <Paper className="add-more-skill__flat">
                      <span className="add-more-skill__label">
                        <span className="add-more-skill__plus">+ </span>
                        <span className="add-more-skill__text">{tr('Add Skill')}</span>
                      </span>
                    </Paper>
                  </a>
                ))}
            </div>

            {!this.state.publicProfileViewV4 &&
              !(this.state.upgradeSkillsPassport || this.state.additionalProfileInfoEnabled) && (
                <div className="width-100">
                  <div className="small-12 profile-box">
                    {cards && cards.length > 0 && !loadingPublicProfileCards && (
                      <CardCarousel
                        cardsCount={this.state.totalCardsNumber}
                        cards={cards}
                        numberOfCards={5}
                        publicProfile={publicProfile}
                        removeCardFromList={this.removeCardFromList.bind(this)}
                      />
                    )}
                    {loadingPublicProfileCards && (
                      <div className="container-padding text-center">
                        <Spinner />
                      </div>
                    )}
                  </div>
                </div>
              )}

            {this.state.showProfileRecommendSme &&
              this.state.showRecommendedContent &&
              this.state.recommendedItems && (
                <div className="width-100">
                  <div className="small-12 profile-box">
                    <Paper className="container-padding">
                      <div style={{ margin: '0 0 0.625rem 0' }}>
                        <span className="header-text" role="heading" aria-level="4">
                          {tr('SME FOR')}: {this.userHandle}
                        </span>
                        {this.state.recommendedItems.length > 4 && (
                          <div className="float-right">
                            <a href="#" className="view-more-badges" onClick={this.openTeam}>
                              {tr('View More')}
                            </a>
                          </div>
                        )}
                      </div>
                      <div className="integration-section">
                        <Carousel isProfileCardCarousel="true">
                          {this.state.recommendedItems.map(recommendedUser => {
                            return (
                              <div
                                className="card-v2"
                                key={`recommended-user_${recommendedUser.id}`}
                              >
                                <div className="paper paper-card">
                                  <Influencer
                                    id={recommendedUser.id}
                                    name={recommendedUser.name}
                                    handle={recommendedUser.handle}
                                    imageUrl={
                                      (recommendedUser.avatarimages &&
                                        recommendedUser.avatarimages.medium) ||
                                      this.defaultUserImage
                                    }
                                    expertSkills={recommendedUser.expertSkills}
                                    roles={recommendedUser.roles}
                                    rolesDefaultNames={recommendedUser.rolesDefaultNames}
                                    position={'top'}
                                    following={recommendedUser.isFollowing}
                                  />
                                </div>
                              </div>
                            );
                          })}
                        </Carousel>
                      </div>
                    </Paper>
                  </div>
                </div>
              )}

            {this.state.openConfirm && (
              <ConfirmationModal
                title={'Confirm'}
                message={'Do you want to delete the skill?'}
                closeModal={this.toggleModal}
                callback={this.removeSkill}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    topNav: state.config.toJS(),
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    cards: state.cards.toJS()
  };
}

MyProfile.propTypes = {
  team: PropTypes.object,
  params: PropTypes.object,
  pathname: PropTypes.string,
  currentUser: PropTypes.object
};

export default connect(mapStoreStateToProps)(MyProfile);
