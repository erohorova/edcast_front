import React, { Component } from 'react';
import PropTypes from 'prop-types';
import request from 'superagent';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import Done from 'material-ui/svg-icons/action/done';
import { openUpdateInterestsModal } from '../../../../actions/modalActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';

class Interests extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px',
        border: '1px solid',
        borderColor: colors.primary
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px',
        verticalAlign: 'middle'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };

    this.state = {
      interests: [],
      loadingComplete: false
    };

    this.triggerUpdateInterests = this.triggerUpdateInterests.bind(this);
  }
  componentDidMount() {
    if (this.props.currentUser.id) {
      this.setState({
        interests:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.learningTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modal && nextProps.modal.reloadInterests) {
      this.setState({
        interests:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.learningTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  triggerUpdateInterests() {
    this.props.dispatch(openUpdateInterestsModal(this.props.currentUser.id));
  }

  render() {
    let interests = this.state.interests != undefined ? this.state.interests : [];
    return (
      <div>
        <div className="block-name">
          <div>
            <strong>
              {tr('LEARNING GOALS')} ({interests && interests.length})
            </strong>
            <IconButton
              aria-label="update interests"
              className="edit"
              onTouchTap={this.triggerUpdateInterests}
              iconStyle={this.styles.smallIcon}
              style={this.styles.smallIconButton}
            >
              <EditIcon color={colors.silverSand} />
            </IconButton>
            <br />
          </div>
        </div>
        <Paper>
          <div className="container-padding vertical-spacing-medium">
            {!this.state.loadingComplete && (
              <p className="data-not-available-msg">
                <small>{tr('Loading your list of learning goals ...')}</small>
              </p>
            )}

            <div>
              <div style={this.styles.chipsWrapper}>
                {interests.map((int, idx) => {
                  return (
                    <Chip key={idx} style={this.styles.chip} backgroundColor={colors.white}>
                      <small style={{ color: colors.primary }}>
                        {int.topic_label}
                        <Done
                          style={{
                            color: colors.primary,
                            verticalAlign: 'middle',
                            marginLeft: '5px',
                            border: '1px solid',
                            borderRadius: '20px',
                            height: '15px',
                            width: '15px'
                          }}
                        />
                      </small>
                    </Chip>
                  );
                })}
              </div>
            </div>

            {this.state.loadingComplete && interests.length == 0 && (
              <div className="text-center">
                <p className="text-left data-not-available-msg">
                  <small>
                    {tr(
                      'It’s easy to start learning on EdCast. Tell us three things you are interested in learning and we will personalize your feed around those topics.'
                    )}
                  </small>
                  <br />
                  <br />
                </p>
                <PrimaryButton
                  label={tr('Add Topics')}
                  className="edit"
                  onTouchTap={this.triggerUpdateInterests}
                />
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal.toJS()
  };
}

Interests.propTypes = {
  currentUser: PropTypes.object
};

export default connect(mapStoreStateToProps)(Interests);
