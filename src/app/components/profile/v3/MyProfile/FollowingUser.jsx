import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { follow, unfollow } from 'edc-web-sdk/requests/users.v2';
import { tr } from 'edc-web-sdk/helpers/translations';
import { followingUserAction, updateFollowingList } from '../../../../actions/currentUserActions';
import { sendCustomEvent } from '../../../../actions/upshotActions';

class Influencer extends Component {
  constructor(props, context) {
    super(props, context);
    this.followClickHandler = this.followClickHandler.bind(this);
    this.getLink = this.getLink.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.state = {
      pending: false,
      following: props.following
    };
  }

  componentWillReceiveProps(nextProp) {
    this.setState({ following: nextProp.following });
  }

  followClickHandler() {
    let action = this.state.following ? unfollow : follow;
    if (this.state.upshotEnabled) {
      upshotActions.sendCustomEvent(
        window.UPSHOTEVENT[!this.state.isFollowing ? 'FOLLOW' : 'UNFOLLOW'],
        {
          eventAction: !this.state.following ? 'Follow' : 'Unfollow',
          event: 'User'
        }
      );
    }
    this.setState({ pending: true });
    action(this.props.id).then(() => {
      if (this.props.type === 'followingUsers') {
        this.props.dispatch(followingUserAction(this.props.id, !this.state.following));
      }
      if (this.props.type === 'followerUsers') {
        this.props.dispatch(updateFollowingList(this.props.id, !this.state.following));
      }
      this.setState({ pending: false });
    });
  }

  getLink() {
    if (this.props.disableLink) {
      return 'javascript:;';
    } else {
      return '/' + this.props.handle;
    }
  }

  getStyle() {
    if (this.props.disableLink) {
      return { cursor: 'default' };
    } else {
      return {};
    }
  }

  formatSkills(skills) {
    let skillsString = '';
    for (var i = 0; i < skills.length; i++) {
      skillsString = skillsString + skills[i].topic_label;
      if (i != skills.length - 1) {
        skillsString = skillsString + ', ';
      }
    }
    return skillsString;
  }

  render() {
    return (
      <div className="influencer">
        <div className="influencer-wrapper">
          <Paper>
            <div className="influencer-spacing">
              <a className="matte" href={this.getLink()} style={this.getStyle()}>
                <div
                  className="influencer-avatar"
                  style={{ backgroundImage: 'url(' + this.props.imageUrl + ')', marginTop: '20px' }}
                />
                <div className="influencer-info text-overflow">
                  <div className="influencer-name">{this.props.name}</div>
                </div>
              </a>
              <FollowButton
                onTouchTap={this.followClickHandler}
                className="follow"
                label={tr(this.state.following ? 'Following' : 'Follow')}
                hoverLabel={tr(this.state.following ? 'Unfollow' : '')}
                pendingLabel={tr(this.state.following ? 'Unfollowing...' : 'Following...')}
                pending={this.state.pending}
                following={this.state.following}
              />
            </div>
          </Paper>
        </div>
      </div>
    );
  }
}

Influencer.propTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  type: PropTypes.string,
  handle: PropTypes.string,
  imageUrl: PropTypes.string,
  following: PropTypes.bool,
  disableLink: PropTypes.bool
};

export default connect()(Influencer);
