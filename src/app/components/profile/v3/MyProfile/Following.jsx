import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Carousel from '../../../common/Carousel';
import FollowingUser from './FollowingUser';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getFollowingUsers } from '../../../../actions/currentUserActions';
import { usersv2 } from 'edc-web-sdk/requests';

class Following extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      followingList: [],
      limit: 10,
      offset: 0
    };

    this.handleConnectClick = this.handleConnectClick.bind(this);
  }

  componentDidMount() {
    this.getFollowingList(this.state.offset, this.state.limit);
  }

  getFollowingList = (offset = 0, limit = 10) => {
    let followingList = this.state.followingList;
    return usersv2.getfollowingUsers({ offset, limit }).then(users => {
      followingList = followingList.concat(users.users);
      if (users && users.users && users.users.length < 10) {
        this.props.dispatch(getFollowingUsers(followingList));
      } else {
        this.setState(
          { followingList, offset: followingList.length },
          this.getFollowingList.bind(this, followingList.length)
        );
      }
    });
  };

  handleConnectClick() {
    window.location.assign('/settings/integrations');
  }

  followingUser = user => {
    return (
      <FollowingUser
        id={user.id}
        type={'followingUsers'}
        name={user.name}
        handle={user.handle}
        imageUrl={user.pictureUrl}
        following={user.isFollowing}
        style={{ marginTop: '5px' }}
      />
    );
  };

  render() {
    let followingUsers = this.props.currentUser.followingUsers || [];
    let followingUserCount = followingUsers.filter(user => user.isFollowing).length;

    return (
      <div className="small-12 columns">
        {followingUsers && followingUsers.length > 0 && (
          <div>
            <div className="clearfix">
              <div className="float-left block-name">
                <div>
                  {tr('Following')} ({followingUserCount})
                </div>
              </div>
            </div>
            <Carousel slidesToShow={this.props.slidesToShow}>
              {followingUsers.map((user, index) => {
                let position = 'top';
                return <div key={user.id}>{this.followingUser(user)}</div>;
              })}
            </Carousel>
          </div>
        )}
      </div>
    );
  }
}

Following.propTypes = {
  currentUser: PropTypes.object,
  slidesToShow: PropTypes.number
};

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

export default connect(mapStoreStateToProps)(Following);
