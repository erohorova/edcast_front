import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import Paper from 'edc-web-sdk/components/Paper';
import * as usersSdk from 'edc-web-sdk/requests/users.v2';
import UsersChannelsListModal from '../../../modals/UsersChannelsListModal';
import { channelsv2 } from 'edc-web-sdk/requests';
import Channel from '../../../discovery/Channel.jsx';
import Carousel from '../../../common/Carousel';
import EmptyBlock from '../../../discovery/EmptyBlock';

class UserChannelsBlock extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      wrapChannel: {
        marginRight: '8px',
        marginTop: '10px'
      },
      channelsContainer: {
        justifyContent: 'start'
      }
    };

    this.state = {
      userChannels: [],
      channelsCount: 0,
      openModal: false,
      channelCarouselState: 'Content Loading...'
    };
  }

  componentDidMount() {
    let payload = {
      limit: 16,
      offset: 0,
      is_following: true,
      order_label: 'desc',
      sort: 'created_at'
    };
    channelsv2
      .fetchChannels(payload)
      .then(data => {
        this.setState({ userChannels: data, channelCarouselState: data.length > 0 });
      })
      .catch(err => {
        console.error(`Error in UserChannelsBlock.getChannels.func : ${err}`);
      });
  }

  toggleModal = e => {
    e && e.preventDefault();
    this.props.dispatch(push(`/channels/my`));
  };

  render() {
    return (
      this.state.channelCarouselState && (
        <div className="column small-12 profile-box">
          {this.state.userChannels.length > 0 ? (
            <Paper className="badges-block container-padding">
              <div id="pathwayBadgesProfile">
                {!!this.state.userChannels.length && (
                  <div>
                    <div className="row">
                      <div className="column small-6">
                        <div className="header-text" role="heading" aria-level="3">
                          {tr('Channels you follow')}{' '}
                          {this.state.channelsCount ? `(${this.state.channelsCount})` : ''}
                        </div>
                      </div>
                      {this.state.userChannels.length > 5 && (
                        <div className="column small-6">
                          <div className="float-right">
                            <a href="#" className="view-more-badges" onClick={this.toggleModal}>
                              {tr('View All')}
                            </a>
                          </div>
                        </div>
                      )}
                    </div>

                    <div className="channel-card-wrapper">
                      <div className="channel-card-wrapper-inner">
                        <Carousel slidesToShow={this.props.numberOfChannels}>
                          {this.state.userChannels.map(channel => {
                            return (
                              <div key={channel.id}>
                                <Channel
                                  channel={channel}
                                  id={channel.id}
                                  isFollowing={channel.isFollowing}
                                  title={channel.label}
                                  imageUrl={channel.profileImageUrl || channel.bannerImageUrl}
                                  slug={channel.slug}
                                  style={{ border: '1px #d3d3d3 solid' }}
                                  allowFollow={channel.allowFollow}
                                  isPrivate={channel.isPrivate}
                                />
                              </div>
                            );
                          })}
                        </Carousel>
                      </div>
                    </div>
                  </div>
                )}
                {this.state.openModal && (
                  <UsersChannelsListModal
                    closeModal={this.toggleModal}
                    userChannels={this.state.userChannels}
                    channelsCount={this.state.channelsCount}
                  />
                )}
              </div>
            </Paper>
          ) : (
            <EmptyBlock
              className="empty-content-channel column small-12"
              title={tr(this.state.channelCarouselState)}
            />
          )}
        </div>
      )
    );
  }
}

UserChannelsBlock.propTypes = {
  numberOfChannels: PropTypes.number
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(UserChannelsBlock);
