import React, { Component } from 'react';
import PropTypes from 'prop-types';
import request from 'superagent';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import Done from 'material-ui/svg-icons/action/done';
import { getOnBoardingSkillsLevels } from '../../../../actions/onboardingActions';
import {
  openUpdateExpertiseModal,
  openUpdateMultilevelExpertiseModal
} from '../../../../actions/modalActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import { langs } from '../../../../constants/languages';

class Expertisev2 extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px',
        border: '1px solid',
        borderColor: colors.primary
      },
      smallIconButton: {
        width: '36px',
        height: '20px',
        padding: '0px',
        verticalAlign: 'middle'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };

    this.state = {
      expertise: [],
      loadingComplete: false,
      expertiseLabel: ''
    };

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;

    this.triggerUpdateExpertise = this.triggerUpdateExpertise.bind(this);
    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
  }
  componentDidMount() {
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let expertiseLabel = tr('Skills');
    if (labels) {
      let expertiseLabelFlag =
        labels['web/labels/expertise'] && labels['web/labels/expertise'].label.length > 0;
      expertiseLabel = expertiseLabelFlag ? tr(labels['web/labels/expertise'].label) : tr('Skills');
    }
    let curExpertiseLabel = labels['web/labels/expertise'];
    this.translatedLabel =
      this.isShowCustomLabels &&
      curExpertiseLabel &&
      curExpertiseLabel.languages &&
      curExpertiseLabel.languages[this.profileLanguage] &&
      curExpertiseLabel.languages[this.profileLanguage].trim();

    this.setState({ expertiseLabel });
    if (this.props.currentUser.id) {
      this.setState({
        expertise:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.expertTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  triggerUpdateExpertise() {
    if (this.onboardingVersion === 'v3') {
      this.loadMultilevel();
    } else {
      this.props.dispatch(openUpdateExpertiseModal(this.props.currentUser.id));
    }
  }

  loadMultilevel = () => {
    let _this = this;
    let taxonomyDomain =
      this.props.team && this.props.team.config && this.props.team.config.taxonomy_domain;

    if (this.props.onboardingv3 && this.props.onboardingv3.multiLevelTaxonomy) {
      this.props.dispatch(openUpdateMultilevelExpertiseModal());
    } else {
      this.props
        .dispatch(getOnBoardingSkillsLevels(taxonomyDomain))
        .then(() => {
          _this.props.dispatch(openUpdateMultilevelExpertiseModal());
        })
        .catch(err => {
          console.error(`Error in Expertisev2.getOnBoardingSkillsLevels.func : ${err}`);
        });
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      expertise:
        (nextProps.currentUser &&
          nextProps.currentUser.profile &&
          nextProps.currentUser.profile.expertTopics) ||
        [],
      loadingComplete: true
    });
  }

  render() {
    let expertise = this.state.expertise != undefined ? this.state.expertise : [];
    let showViewAll = false;
    let showingExpertise = expertise;
    if (expertise.length > 3) {
      showingExpertise = expertise.slice(0, 3);
      showViewAll = true;
    }
    return (
      <div className="interests-section">
        <Paper>
          <div className="container-padding" style={{ height: '130px' }}>
            <div className="row">
              <div className="header-text column small-6" role="heading" aria-level="3">
                {this.translatedLabel || this.state.expertiseLabel} (
                {this.state.expertise && this.state.expertise.length})
                <IconButton
                  className="edit"
                  aria-label="update expertise"
                  onTouchTap={this.triggerUpdateExpertise}
                  iconStyle={this.styles.smallIcon}
                  style={this.styles.smallIconButton}
                >
                  <EditIcon color={colors.silverSand} />
                </IconButton>
              </div>

              <div className="column small-6">
                <div className="float-right">
                  {showViewAll && (
                    <div className="view-more-expertise">
                      <a
                        className="view-more-expertise underline"
                        onTouchTap={this.triggerUpdateExpertise}
                      >
                        {tr('View All')}
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>

            {!this.state.loadingComplete && (
              <p className="data-not-available-msg">
                <small>{tr('Loading your list of skills ...')}</small>
              </p>
            )}

            <div>
              <div style={this.styles.chipsWrapper}>
                {showingExpertise.map((exp, idx) => {
                  return (
                    <div title={exp.topic_label} className="sub-text" key={idx}>
                      {exp.topic_label.substr(0, 30)}
                      {exp.topic_label.length > 30 ? '...' : ''}
                    </div>
                  );
                })}
              </div>
            </div>

            {this.state.loadingComplete && expertise.length == 0 && (
              <div className="text-center">
                <p className="text-left">
                  <small className="data-not-available-msg">
                    {tr(
                      "You haven't added any skills yet. Including skills in your profile will help you gain more followers and visibility."
                    )}
                  </small>
                  <br />
                  <br />
                </p>
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal.toJS(),
    team: state.team.toJS(),
    onboardingv3: state.onboardingv3.toJS()
  };
}

Expertisev2.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  team: PropTypes.object,
  onboardingv3: PropTypes.object
};

export default connect(mapStoreStateToProps)(Expertisev2);
