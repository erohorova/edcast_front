import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Popover from 'material-ui/Popover';
import { List } from 'material-ui/List';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import CloseIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';

class ViewMoreInterests extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {
      list: {
        width: 'auto',
        padding: '15px'
      },
      iconStyle: {
        padding: 0,
        width: 'auto',
        height: 'auto'
      },
      textBox: {
        overflow: 'auto',
        height: 'auto',
        fontSize: '14px'
      },
      headerBox: {
        marginBottom: '10px',
        fontWeight: '700'
      },
      closeBox: {
        float: 'right'
      }
    };
  }

  render() {
    return (
      <Popover
        open={this.props.open}
        anchorEl={this.props.anchorEl}
        onRequestClose={this.props.requestCloseHandler}
        anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
      >
        <List style={this.styles.list}>
          <div style={this.styles.closeBox}>
            <IconButton
              aria-label={tr('close')}
              style={this.styles.iconStyle}
              onTouchTap={this.props.requestCloseHandler}
            >
              <CloseIcon color="#d6d6e1" />
            </IconButton>
          </div>
          <div style={this.styles.textBox}>
            <div style={this.styles.headerBox}>{tr('Goals')}</div>
            {this.props.interests.map((item, index) => {
              return <div key={index}>{item.topic_label}</div>;
            })}
          </div>
        </List>
      </Popover>
    );
  }
}

ViewMoreInterests.propTypes = {
  open: PropTypes.bool,
  anchorEl: PropTypes.object,
  interests: PropTypes.array,
  requestCloseHandler: PropTypes.func
};

function mapStoreStateToProps(state) {
  return {};
}

export default connect(mapStoreStateToProps)(ViewMoreInterests);
