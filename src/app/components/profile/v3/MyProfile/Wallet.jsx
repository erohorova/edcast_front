import React, { Component } from 'react';
import PropTypes from 'prop-types';
import request from 'superagent';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';
import { initiateWalletSdk } from 'edc-web-sdk/helpers/wallet';
import { getWalletApiToken } from 'edc-web-sdk/requests/users.v2';
import { fetchAccountInfo } from 'edc-web-sdk/requests/wallet';
import config from 'edc-web-sdk/requests/config';

class Wallet extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      walletApiKey: null,
      balance: 0
    };
  }

  componentDidMount() {
    this.invokeWalletSdk();
    this.setWalletApiToken();
  }

  fetchBalance() {
    fetchAccountInfo(this.state.walletApiKey).then(data => {
      this.setState({ balance: data.edu_balance });
    });
  }

  invokeWalletSdk() {
    initiateWalletSdk();
  }

  setWalletApiToken() {
    getWalletApiToken().then(data => {
      this.setState({ walletApiKey: data.walletApiToken });

      this.fetchBalance();
    });
  }

  depositCallback(response) {
    if (response.payment_state == 'successful') {
      this.fetchBalance();
    } else {
      // TODO: Display status and message for the payment response.
    }
  }

  triggerCredit() {
    /*eslint no-undef: "off"*/
    let digital_wallet = new DigitalWallet({
      api_key: this.state.walletApiKey,
      wallet_domain: config.WalletHost
    });
    digital_wallet.deposit(this.depositCallback.bind(this));
  }

  render() {
    return (
      <div id="wallet">
        <div className="block-name">
          <div>
            <strong>{tr('Digital Wallet')}</strong>
            <br />
          </div>
        </div>
        <Paper>
          <div className="container-padding vertical-spacing-medium">
            <div className="clearfix">
              <span>
                {tr('Digital Credit Balance:')}&nbsp;
                <strong>{this.state.balance} EDU$</strong>
              </span>
              <div>
                <PrimaryButton
                  label={tr('Load Credit')}
                  className="edit"
                  onTouchTap={this.triggerCredit.bind(this)}
                />
              </div>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

Wallet.propTypes = {
  currentUser: PropTypes.object
};

export default connect(mapStoreStateToProps)(Wallet);
