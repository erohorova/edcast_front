import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';

import Paper from 'edc-web-sdk/components/Paper';
import colors from 'edc-web-sdk/components/colors/index';

import {
  openUpdateInterestsModal,
  openUpdateInterestsOnboardV2Modal,
  openUpdateMultilevelInterestsModal
} from '../../../../actions/modalActions';

import ViewMoreInterests from './ViewMoreInterests';
import { getOnBoardingSkillsLevels } from '../../../../actions/onboardingActions';
import { langs } from '../../../../constants/languages';

class Interestsv2 extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px',
        border: '1px solid',
        borderColor: colors.primary
      },
      smallIconButton: {
        width: '36px',
        height: '20px',
        padding: '0px',
        verticalAlign: 'middle'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      },
      customTopics: {
        color: '#bcbcbc'
      }
    };

    this.state = {
      interests: [],
      customTopics: [],
      loadingComplete: false,
      interestLabel: '',
      isOpen: false
    };

    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;

    this.onboardingVersion = window.ldclient.variation('onboarding-version', 'v1');
    this.triggerUpdateInterests = this.triggerUpdateInterests.bind(this);
  }
  componentDidMount() {
    let labels = this.props.team && this.props.team.OrgConfig && this.props.team.OrgConfig.labels;
    let interestLabel = 'Learning Goals';
    if (labels) {
      let interestsLabelFlag =
        labels['web/labels/interests'] && labels['web/labels/interests'].label.length > 0;
      interestLabel = interestsLabelFlag ? labels['web/labels/interests'].label : 'Learning Goals';
    }
    let interestsLabel = labels['web/labels/interests'];
    this.translatedLabel =
      this.isShowCustomLabels &&
      interestsLabel &&
      interestsLabel.languages &&
      interestsLabel.languages[this.profileLanguage] &&
      interestsLabel.languages[this.profileLanguage].trim();

    this.setState({ interestLabel });
    if (this.props.currentUser.id) {
      this.setState({
        interests:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.learningTopics) ||
          [],
        loadingComplete: true,
        customTopics:
          (this.props.currentUser.customTopics && this.props.currentUser.customTopics) != undefined
            ? this.props.currentUser.customTopics
            : []
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      interests:
        (nextProps.currentUser &&
          nextProps.currentUser.profile &&
          nextProps.currentUser.profile.learningTopics) ||
        [],
      loadingComplete: true,
      customTopics:
        (this.props.currentUser.customTopics && this.props.currentUser.customTopics) != undefined
          ? this.props.currentUser.customTopics
          : []
    });
  }

  triggerUpdateInterests() {
    if (this.onboardingVersion === 'v2') {
      this.props.dispatch(openUpdateInterestsOnboardV2Modal(this.props.currentUser.id));
    } else if (this.onboardingVersion === 'v3') {
      this.loadMultilevel();
    } else {
      this.props.dispatch(openUpdateInterestsModal(this.props.currentUser.id));
    }
  }

  loadMultilevel = () => {
    let _this = this;
    let taxonomyDomain =
      this.props.team && this.props.team.config && this.props.team.config.taxonomy_domain;

    if (this.props.onboardingv3 && this.props.onboardingv3.multiLevelTaxonomy) {
      _this.props.dispatch(openUpdateMultilevelInterestsModal());
    } else {
      this.props
        .dispatch(getOnBoardingSkillsLevels(taxonomyDomain))
        .then(() => {
          _this.props.dispatch(openUpdateMultilevelInterestsModal());
        })
        .catch(err => {
          console.error(`Error in Interestsv2.getOnBoardingSkillsLevels.func : ${err}`);
        });
    }
  };

  handleOpen = e => {
    this.setState({
      isOpen: true,
      anchor: e.currentTarget
    });
  };

  handleClose = () => {
    this.setState({
      isOpen: false
    });
  };

  render() {
    let interests = this.state.interests != undefined ? this.state.interests : [];
    let customTopics = this.state.customTopics;
    let showLongVariant = false;
    let showMiddleVariant = false;
    let showingInterests = interests;
    let showingcustomTopics = customTopics;
    if (interests.length > 12) {
      showingInterests = interests.slice(0, 12);
    }
    if (interests.length <= 3) {
      showLongVariant = true;
    }
    if (interests.length > 3 && interests.length <= 6) {
      showMiddleVariant = true;
    }

    return (
      <div className="interests-section">
        <Paper>
          <div className="container-padding" style={{ height: 'auto' }}>
            <div className="row">
              <div className="header-text column small-6" role="heading" aria-level="3">
                {this.translatedLabel || tr(this.state.interestLabel)} (
                {(this.state.interests && this.state.interests.length) +
                  (this.state.customTopics && this.state.customTopics.length)}
                )
                <IconButton
                  disableTouchRipple={false}
                  className="edit"
                  onTouchTap={this.triggerUpdateInterests}
                  iconStyle={this.styles.smallIcon}
                  style={this.styles.smallIconButton}
                  tooltip="Want to learn something new? Just add it to your 'Interests'."
                  aria-label="Want to learn something new? Just add it to your 'Interests'."
                  tooltipPosition="bottom-center"
                >
                  <EditIcon color={colors.silverSand} />
                </IconButton>
              </div>
              <div className="column small-6">
                <div className="float-right">
                  {this.state.interests && this.state.interests.length > 12 && (
                    <div className="view-more-expertise">
                      <a className="view-more-expertise" onClick={this.handleOpen}>
                        {tr('View All')}
                      </a>
                    </div>
                  )}
                </div>
              </div>
              <ViewMoreInterests
                open={this.state.isOpen}
                anchorEl={this.state.anchor}
                requestCloseHandler={this.handleClose}
                interests={this.state.interests}
              />
            </div>

            {!this.state.loadingComplete && (
              <p className="data-not-available-msg">
                <small>{tr('Loading your list of learning goals ...')}</small>
              </p>
            )}

            <div>
              <div style={this.styles.chipsWrapper}>
                {showingInterests.map((int, idx) => {
                  return (
                    <div
                      title={int.topic_label}
                      className={`sub-text ${
                        showLongVariant
                          ? 'sub-text_long'
                          : showMiddleVariant
                          ? 'sub-text_middle'
                          : ''
                      }`}
                      key={idx}
                    >
                      {int.topic_label}
                    </div>
                  );
                })}
                {showingcustomTopics.map((int, idx) => {
                  return (
                    <div
                      title={tr('Pending for Admin Approval')}
                      style={this.styles.customTopics}
                      className={`sub-text ${
                        showLongVariant
                          ? 'sub-text_long'
                          : showMiddleVariant
                          ? 'sub-text_middle'
                          : ''
                      }`}
                      key={idx}
                    >
                      {int.label}
                    </div>
                  );
                })}
              </div>
            </div>

            {this.state.loadingComplete && !interests.length && !showingcustomTopics.length && (
              <div className="text-center">
                <p className="text-left">
                  <small className="data-not-available-msg">
                    {tr(
                      'It’s easy to start learning on EdCast. Tell us three things you are interested in learning and we will personalize your feed around those topics.'
                    )}
                  </small>
                  <br />
                  <br />
                </p>
              </div>
            )}
          </div>
        </Paper>
      </div>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal.toJS(),
    onboardingv3: state.onboardingv3.toJS(),
    team: state.team.toJS()
  };
}

Interestsv2.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object,
  team: PropTypes.object,
  onboardingv3: PropTypes.object
};

export default connect(mapStoreStateToProps)(Interestsv2);
