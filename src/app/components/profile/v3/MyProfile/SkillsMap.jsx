import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar, Tooltip } from 'recharts';
import { tr } from 'edc-web-sdk/helpers/translations';

class SkillsMap extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showPWCAssesment: window.ldclient.variation('show-pwc', false)
    };

    this.styles = {
      pwcButton: {
        fontSize: '10px',
        border: '1px solid',
        padding: '5px',
        color: '#454460',
        textAlign: 'center',
        float: 'right',
        width: '200px',
        verticalAlign: 'middle'
      },
      pwcContainer: {
        marginTop: '-5px'
      }
    };
  }

  componentDidMount() {
    let points = document.querySelectorAll('.recharts-polar-angle-axis-tick');
    points.forEach(point => {
      let text =
        point.textContent.length > 12 ? point.textContent.substr(0, 10) + '...' : point.textContent;
      point.innerHTML = point.innerHTML + '<title>' + text + '</title>';
    });
  }

  //Due to this Update Skill is not working on IE 11 so commented it

  // componentWillReceiveProps(){
  //   let charts = document.querySelectorAll(".recharts-polar-angle-axis-tick-value>tspan");
  //   for(let i = 0; i < charts.length; i++) {
  //     charts[i].innerHTML = charts[i].innerHTML.length > 12 ? charts[i].innerHTML.substr(0, 10) + "..." : charts[i].innerHTML;
  //   }
  // }

  render() {
    let emptyPwcObj = [
      { abc: 0, fullScore: 3, topic: '(Customer Relationship Management)' },
      { abc: 0, fullScore: 3, topic: 'IoT' },
      { abc: 0, fullScore: 3, topic: 'Cybersecurity' },
      { abc: 0, fullScore: 3, topic: 'Application Development' },
      { abc: 0, fullScore: 3, topic: 'Big Data' },
      { abc: 0, fullScore: 3, topic: 'Computer Networking' },
      { abc: 0, fullScore: 3, topic: 'Business Strategy' },
      { abc: 0, fullScore: 3, topic: 'Project Management' }
    ];
    return (
      <Paper>
        <div className="user-score container-padding vertical-spacing-medium">
          <div className="clcContainer">
            <div className="row">
              <div className="columns">
                <p>{tr('Skills Map')}</p>
              </div>
              {this.state.showPWCAssesment && (
                <div className="columns" style={this.styles.pwcContainer}>
                  <a
                    href={`https://talentsnap.pwc.com/?id=${this.props.currentUser &&
                      this.props.currentUser.jwtToken}&url=${
                      window.location.origin
                    }/api/profile_pwc_callback`}
                    style={this.styles.pwcButton}
                  >
                    {tr('Take the Skills Assessment ')}
                    <img
                      src="https://s3.amazonaws.com/edc-dev-web/assets/pwc-trans.png"
                      width="20"
                    />
                  </a>
                </div>
              )}
            </div>
            {this.props.showGraph && (
              <div>
                <RadarChart
                  outerRadius={65}
                  width={290}
                  height={168}
                  data={this.props.skills}
                  className="recharts"
                >
                  <PolarGrid />
                  <PolarAngleAxis dataKey="topic" />

                  <PolarRadiusAxis angle={30} domain={[0, 3]} ticks={[0, 1, 2, 3]} />
                  <Radar
                    name={this.props.userName}
                    dataKey={this.props.userId}
                    stroke="#454460"
                    fill="#454460"
                    fillOpacity={0.6}
                  />

                  <Tooltip cursor={true} />
                </RadarChart>
              </div>
            )}

            {!this.props.showGraph && (
              <div>
                <RadarChart
                  outerRadius={65}
                  width={290}
                  height={168}
                  data={emptyPwcObj}
                  className="recharts"
                >
                  <PolarGrid />
                  <PolarAngleAxis dataKey="topic" />

                  <PolarRadiusAxis angle={30} domain={[0, 3]} ticks={[0, 1, 2, 3]} />
                  <Radar
                    name={this.props.userName}
                    dataKey="abc"
                    stroke="#454460"
                    fill="#454460"
                    fillOpacity={0.6}
                  />

                  <Tooltip cursor={true} />
                </RadarChart>
              </div>
            )}
          </div>
        </div>
      </Paper>
    );
  }
}

SkillsMap.propTypes = {
  userName: PropTypes.string,
  showGraph: PropTypes.bool,
  userId: PropTypes.string,
  skills: PropTypes.object,
  currentUser: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

export default connect(mapStoreStateToProps)(SkillsMap);
