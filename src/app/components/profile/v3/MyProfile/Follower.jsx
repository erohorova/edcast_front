import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import request from 'superagent';
import { connect } from 'react-redux';
import colors from 'edc-web-sdk/components/colors/index';
import Carousel from '../../../common/Carousel';
import FollowingUser from './FollowingUser';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getFollowerUsers } from '../../../../actions/currentUserActions';
import { usersv2 } from 'edc-web-sdk/requests';

class Follower extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      followerList: [],
      limit: 10,
      offset: 0
    };

    this.handleConnectClick = this.handleConnectClick.bind(this);
  }

  componentDidMount() {
    this.getFollowerList(this.state.offset, this.state.limit);
  }

  getFollowerList = (offset = 0, limit = 10) => {
    let followerList = this.state.followerList;

    return usersv2.getfollowerUsers({ offset, limit }).then(users => {
      followerList = followerList.concat(users.users);
      if (users && users.users && users.users.length < 10) {
        this.props.dispatch(getFollowerUsers(followerList));
      } else {
        this.setState(
          { followerList, offset: followerList.length },
          this.getFollowerList.bind(this, followerList.length)
        );
      }
    });
  };

  handleConnectClick() {
    window.location.assign('/settings/integrations');
  }

  followerUser = user => {
    return (
      <FollowingUser
        id={user.id}
        type={'followerUsers'}
        name={user.name}
        handle={user.handle}
        imageUrl={user.pictureUrl}
        following={user.isFollowing}
        style={{ marginTop: '5px' }}
      />
    );
  };

  render() {
    let showInfluencersEmptyBlock;
    let followerUsers = this.props.currentUser.followerUsers;

    return (
      <div className="small-12 columns follower">
        {followerUsers && (
          <div>
            <div className="clearfix">
              <div className="float-left block-name">
                <div>
                  {tr('Follower')} ({followerUsers.length})
                </div>
              </div>
            </div>
            <Carousel slidesToShow={this.props.slidesToShow}>
              {followerUsers.map((user, index) => {
                let position = 'top';
                return <div key={user.id}>{this.followerUser(user)}</div>;
              })}
            </Carousel>
          </div>
        )}
      </div>
    );
  }
}

Follower.propTypes = {
  currentUser: PropTypes.object,
  slidesToShow: PropTypes.number
};

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

export default connect(mapStoreStateToProps)(Follower);
