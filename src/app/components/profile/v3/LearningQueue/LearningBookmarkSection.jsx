import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import LearningHeaderRow from './LearningHeaderRow';
import LearningContentRow from './LearningContentRow';

class LearningBookmarkSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      paperStyle: {
        padding: '20px',
        width: '100%',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
      }
    };
  }

  render() {
    let contentList = this.props.contentList;
    return (
      <div className="learning-card-section">
        {this.props.contentList.length > 0 && (
          <div>
            <h5 className="learning-plan-section-title">{tr('BOOKMARKS')}</h5>
            <LearningHeaderRow rowType="bookmark" />
            <div className="learning-content-row-wrapper">
              {this.props.contentList.map(card => {
                let duration = this.props.fetchDuration(card);
                return (
                  <LearningContentRow
                    key={card.id}
                    duration={duration}
                    redirectUrl={this.props.redirectUrl}
                    card={card}
                    rowType="bookmark"
                  />
                );
              })}
            </div>
            {this.props.showLoadMore && (
              <div className="learning-load-more">
                <span onClick={this.props.loadMore.bind(this, 'bookmarks')}>
                  {tr(this.props.cardsLoadMoreButtonText)}
                </span>
              </div>
            )}
            {!this.props.showLoadMore && <br />}
          </div>
        )}
      </div>
    );
  }
}

LearningBookmarkSection.propTypes = {
  contentList: PropTypes.object,
  cardsLoadMoreButtonText: PropTypes.string,
  fetchDuration: PropTypes.func,
  redirectUrl: PropTypes.func,
  loadMore: PropTypes.func,
  showLoadMore: PropTypes.bool
};

LearningBookmarkSection.defaultProps = {
  contentList: []
};

export default LearningBookmarkSection;
