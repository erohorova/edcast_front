import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import LearningHeaderRow from './LearningHeaderRow';
import LearningContentRow from './LearningContentRow';

class LearningCardsSection extends Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      paperStyle: {
        padding: '20px',
        width: '100%',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
      }
    };
  }

  render() {
    let contentList = this.props.contentList;
    return (
      <div className="learning-card-section">
        {this.props.contentList.length > 0 && (
          <div>
            <h5 className="learning-plan-section-title">{tr('INFORMAL LEARNING')}</h5>
            <LearningHeaderRow />
            <div className="learning-content-row-wrapper">
              {this.props.contentList.map(card => {
                let duration = this.props.fetchDuration(card);
                return (
                  <LearningContentRow
                    key={card.id}
                    duration={duration}
                    redirectUrl={this.props.redirectUrl}
                    card={card}
                  />
                );
              })}
            </div>
            {this.props.showLoadMore && (
              <div className="learning-load-more">
                <span onClick={this.props.loadMore.bind(this, 'cards')}>
                  {tr(this.props.cardsLoadMoreButtonText)}
                </span>
              </div>
            )}
            {!this.props.showLoadMore && <br />}
          </div>
        )}
      </div>
    );
  }
}

LearningCardsSection.propTypes = {
  contentList: PropTypes.object,
  cardsLoadMoreButtonText: PropTypes.string,
  redirectUrl: PropTypes.func,
  fetchDuration: PropTypes.func,
  loadMore: PropTypes.func,
  showLoadMore: PropTypes.bool
};

LearningCardsSection.defaultProps = {
  contentList: []
};

export default LearningCardsSection;
