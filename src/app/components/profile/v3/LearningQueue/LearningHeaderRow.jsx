import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';

function LearningHeaderRow(props) {
  return (
    <div className="row learning-header-row">
      <div className="small-12 medium-5 columns learning-header">
        <div>{tr('Content')}</div>
      </div>
      <div className="small-12 medium-1 columns learning-header">
        <div>{tr('Duration')}</div>
      </div>
      {props.rowType !== 'bookmark' && (
        <div className="small-12 medium-3 columns learning-header">
          <div>{tr('% Complete')}</div>
        </div>
      )}
      {props.rowType !== 'bookmark' ? (
        <div className="small-12 medium-1 columns learning-header">
          <div>{tr('Due Date')}</div>
        </div>
      ) : (
        <div
          className="small-12 medium-2 columns learning-header"
          style={{ maxWidth: '13.66667%' }}
        >
          <div>{tr('Bookmarked Date')}</div>
        </div>
      )}
      <div className="small-12 medium-1 columns learning-header">
        <div>{tr('Status')}</div>
      </div>
      {props.rowType !== 'bookmark' && (
        <div className="small-12 medium-1 columns learning-header">
          <div>{tr('Assigned Date')}</div>
        </div>
      )}
    </div>
  );
}

LearningHeaderRow.propTypes = {
  rowType: PropTypes.string
};

export default LearningHeaderRow;
