import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { tr } from 'edc-web-sdk/helpers/translations';
import colors from 'edc-web-sdk/components/colors';
import LinearProgress from 'material-ui/LinearProgress';
import DateConverter from '../../../common/DateConverter';

function LearningContentRow(props) {
  var title = '';
  var completedPercentage = 0;
  if (props.rowType === 'bookmark') {
    title = props.card.title || props.card.message;
  } else {
    completedPercentage = props.card.assignable.completedPercentage;
    title = props.card.title || props.card.assignable.message;
  }
  return (
    <div className="row card-list-row">
      <div className="small-12 medium-5 columns title-wrapper">
        <div
          className="path-title"
          onClick={props.redirectUrl.bind(this, props.card)}
          style={{ color: colors.primary }}
        >
          {title}
        </div>
      </div>
      <div className="small-12 medium-1 columns">
        <div>{props.duration}</div>
      </div>
      {props.rowType !== 'bookmark' && (
        <div className="small-12 medium-3 columns learning-progress-wrapper">
          <div style={{ marginTop: '3px' }}>
            <LinearProgress
              mode="determinate"
              className="learning-progress"
              value={completedPercentage}
              color={colors.primary}
            />
          </div>
        </div>
      )}
      {props.rowType !== 'bookmark' ? (
        <div className="small-12 medium-1 columns">
          <div>
            {props.card.dueAt && <DateConverter date={props.card.dueAt} />}
            {!props.card.dueAt && <div>NA</div>}
          </div>
        </div>
      ) : (
        <div className="small-12 medium-2 columns" style={{ maxWidth: '13.66667%' }}>
          <div>
            {props.card.updatedAt && <DateConverter date={props.card.bookmarkedAt} />}
            {!props.card.updatedAt && <div>NA</div>}
          </div>
        </div>
      )}
      <div className="small-12 medium-1 columns">
        <div className="card-state">{props.card.state.toLowerCase()}</div>
      </div>
      {props.rowType !== 'bookmark' && (
        <div className="small-12 medium-1 columns">
          <div className="card-state">
            {props.card.completedAt && <DateConverter date={props.card.completedAt} />}
            {!props.card.completedAt && <div>NA</div>}
          </div>
        </div>
      )}
    </div>
  );
}

LearningContentRow.propTypes = {
  card: PropTypes.object,
  rowType: PropTypes.string,
  duration: PropTypes.string,
  redirectUrl: PropTypes.func
};

export default LearningContentRow;
