import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../../../common/spinner';
import LearningCardsSection from './LearningCardsSection';
import LearningCoursesSection from './LearningCoursesSection';
import LearningBookmarkSection from './LearningBookmarkSection';
import LearningAsgPerSME from './LearningAsgPerSME';
import colors from 'edc-web-sdk/components/colors';
import { fetchAssignments, getLearningQueue } from '../../../../actions/assignmentsActions';

class LearningPathContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      microDataShowLimit: 5,
      macroDataShowLimit: 5,
      bookmarkShowLimit: 5,

      showCardsLoadMore: true,
      showCoursesLoadMore: true,
      showBookmarkLoadMore: true,

      areCardsLoading: true,
      areCoursesLoading: true,
      areBookmarkLoading: true,

      cardsOffset: 0,
      coursesOffset: 0,
      bookmarkOffset: 0,

      cardsLoadMoreButtonText: 'Load More',
      coursesLoadMoreButtonText: 'Load More',
      bookmarkLoadMoreButtonText: 'Load More',

      bookmarkedCards: []
    };
    this.styles = {
      paperStyle: {
        padding: '25px'
      },
      pageTitle: {
        fontSize: '20px',
        color: '#454560',
        marginBottom: '30px'
      },
      container: {
        margin: '20px 40px'
      }
    };
  }

  componentDidMount() {
    let that = this;
    this.props
      .dispatch(
        fetchAssignments(
          50,
          0,
          ['media', 'poll', 'pack', 'video_stream'],
          'informal',
          this.props.currentUser.id
        )
      )
      .then(() => {
        that.setState({
          areCardsLoading: false,
          showCardsLoadMore: that.props.microData.length <= 5 ? false : true
        });
      })
      .catch(err => {
        console.error(`Error in LearningPathContainer.fetchAssignments.func : ${err}`);
      });

    this.props
      .dispatch(fetchAssignments(50, 0, ['course'], 'formal', this.props.currentUser.id))
      .then(() => {
        that.setState({
          areCoursesLoading: false,
          showCoursesLoadMore: that.props.macroData.length <= 5 ? false : true
        });
      })
      .catch(err => {
        console.error(`Error in LearningPathContainer.fetchAssignments.func course: ${err}`);
      });

    this.props
      .dispatch(getLearningQueue(50, 0, 'bookmarks'))
      .then(bookmarkedCards => {
        that.setState({
          areBookmarkLoading: false,
          showBookmarkLoadMore: bookmarkedCards.length > 5,
          bookmarkedCards
        });
      })
      .catch(err => {
        console.error(`Error in LearningPathContainer.getLearningQueue.func : ${err}`);
      });
  }

  fetchDuration = card => {
    if (card.assignable && card.assignable.eclDurationMetadata) {
      return card.assignable.eclDurationMetadata.calculated_duration_display;
    } else if (card.eclDurationMetadata) {
      return card.eclDurationMetadata.calculated_duration_display;
    } else {
      return 'NA';
    }
  };

  redirectUrl = card => {
    let cardType = card.assignable ? card.assignable.cardType : card.cardType;
    let cardSlug = card.assignable ? card.assignable.slug : card.slug;
    let url =
      (cardType === 'pack' ? '/pathways/' : cardType === 'journey' ? '/journey/' : '/insights/') +
      cardSlug;
    this.props.dispatch(push(url));
  };

  fetchCards = () => {
    let that = this;
    this.props
      .dispatch(
        fetchAssignments(
          50,
          this.state.cardsOffset + 50,
          ['media', 'poll', 'pack', 'video_stream'],
          'informal',
          this.props.currentUser.id
        )
      )
      .then(() => {
        that.setState({
          areCardsLoading: false,
          showCardsLoadMore: that.props.microData.length <= 5 ? false : true,
          cardsOffset: this.state.cardsOffset + 50,
          microDataShowLimit: this.state.microDataShowLimit + 5,
          cardsLoadMoreButtonText: 'Load More...'
        });
      })
      .catch(err => {
        console.error(`Error in LearningPathContainer.fetchCards.fetchAssignmentsfunc : ${err}`);
      });
  };

  fetchCourses = () => {
    let that = this;
    this.props
      .dispatch(
        fetchAssignments(
          50,
          this.state.cardsOffset + 50,
          ['course'],
          'formal',
          this.props.currentUser.id
        )
      )
      .then(() => {
        that.setState({
          areCoursesLoading: false,
          showCoursesLoadMore: that.props.macroData.length <= 5 ? false : true,
          coursesOffset: this.state.coursesOffset + 50,
          macroDataShowLimit: this.state.macroDataShowLimit + 5,
          coursesLoadMoreButtonText: 'Load More...'
        });
      })
      .catch(err => {
        console.error(`Error in LearningPathContainer.fetchCourses.fetchAssignments.func : ${err}`);
      });
  };

  fetchBookmark = () => {
    let that = this;
    this.props
      .dispatch(getLearningQueue(50, this.state.cardsOffset + 50, 'bookmark'))
      .then(bookmarkedCards => {
        that.setState({
          areBookmarkLoading: false,
          showBookmarkLoadMore: bookmarkedCards.length > 5,
          bookmarkOffset: this.state.bookmarkOffset + 50,
          bookmarkShowLimit: this.state.bookmarkShowLimit + 5,
          bookmarkLoadMoreButtonText: 'Load More...',
          bookmarkedCards: this.state.bookmarkedCards.concat(bookmarkedCards)
        });
      })
      .catch(err => {
        console.error(
          `Error in LearningPathContainer.fetchBookmark.getLearningQueue.func : ${err}`
        );
      });
  };

  loadMore = type => {
    if (type === 'cards' && this.state.cardsLoadMoreButtonText !== 'Loading...') {
      let limit = this.state.microDataShowLimit + 5;
      let offset = this.state.cardsOffset + 50;
      this.setState({
        microDataShowLimit: limit,
        showCardsLoadMore: limit >= this.props.microData.length ? false : true
      });
      if (this.props.cardsCount > this.state.cardsOffset && limit === offset) {
        this.setState({ cardsLoadMoreButtonText: 'Loading...', showCardsLoadMore: true });
        this.fetchCards();
      }
    } else if (type === 'courses' && this.state.cardsLoadMoreButtonText !== 'Loading...') {
      let limit = this.state.macroDataShowLimit + 5;
      let offset = this.state.cardsOffset + 50;
      this.setState({
        macroDataShowLimit: limit,
        showCoursesLoadMore: limit >= this.props.macroData.length ? false : true
      });
      if (this.props.coursesCount > this.state.coursesOffset && limit === offset) {
        this.setState({ coursesLoadMoreButtonText: 'Loading...', showCoursesLoadMore: true });
        this.fetchCourses();
      }
    } else if (type === 'bookmarks' && this.state.cardsLoadMoreButtonText !== 'Loading...') {
      let limit = this.state.bookmarkShowLimit + 5;
      let offset = this.state.cardsOffset + 50;
      this.setState({
        bookmarkShowLimit: limit,
        showBookmarkLoadMore: this.state.bookmarkedCards.length > limit
      });
      if (this.state.bookmarkOffset && limit === offset) {
        this.setState({ bookmarkLoadMoreButtonText: 'Loading...', showBookmarkLoadMore: true });
        this.fetchBookmark();
      }
    }
  };

  render() {
    return (
      <div className="v3-container" style={this.styles.container}>
        <Paper style={this.styles.paperStyle}>
          {!(this.state.areCardsLoading || this.state.areCoursesLoading) && (
            <div>
              {this.props.microData.length > 0 && (
                <LearningCardsSection
                  contentList={this.props.microData.slice(0, this.state.microDataShowLimit)}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.microDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCardsLoadMore}
                  cardsLoadMoreButtonText={this.state.cardsLoadMoreButtonText}
                />
              )}
              <br />
              {this.props.macroData.length > 0 && (
                <LearningCoursesSection
                  contentList={this.props.macroData.slice(0, this.state.macroDataShowLimit)}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.macroDataShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showCoursesLoadMore}
                  cardsLoadMoreButtonText={this.state.coursesLoadMoreButtonText}
                />
              )}
              {this.state.bookmarkedCards.length > 0 && (
                <LearningBookmarkSection
                  contentList={this.state.bookmarkedCards.slice(0, this.state.bookmarkShowLimit)}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                  limit={this.state.bookmarkShowLimit}
                  loadMore={this.loadMore}
                  showLoadMore={this.state.showBookmarkLoadMore}
                  cardsLoadMoreButtonText={this.state.bookmarkLoadMoreButtonText}
                />
              )}
              <br />
              {this.props.assignorsIdList.length > 0 && (
                <LearningAsgPerSME
                  assignorsIdList={this.props.assignorsIdList}
                  assignorsNameMappings={this.props.assignorsNameMappings}
                  fetchDuration={this.fetchDuration}
                  redirectUrl={this.redirectUrl}
                />
              )}
            </div>
          )}

          {this.state.areCardsLoading &&
            this.state.areCoursesLoading &&
            this.state.areBookmarkLoading && (
              <div className="text-center">
                <Spinner />
              </div>
            )}

          {!(
            this.state.areCardsLoading ||
            this.state.areCoursesLoading ||
            this.state.areBookmarkLoading
          ) &&
            (this.props.microData.length === 0 &&
              this.props.macroData.length === 0 &&
              this.state.bookmarkedCards.length === 0) && (
              <div className="text-center data-not-available-msg">
                {tr('There are currently no learning items at this time.')}
              </div>
            )}
        </Paper>
      </div>
    );
  }
}

LearningPathContainer.propTypes = {
  currentUser: PropTypes.object,
  microData: PropTypes.object,
  macroData: PropTypes.object,
  cards: PropTypes.object,
  assignorsNameMappings: PropTypes.object,
  assignorsIdList: PropTypes.object,
  cardsCount: PropTypes.number,
  coursesCount: PropTypes.number
};

LearningPathContainer.defaultProps = {
  microData: [],
  macroData: []
};

export default connect(state => ({
  currentUser: state.currentUser.toJS(),
  microData: state.assignments.toJS().informalAssignments,
  macroData: state.assignments.toJS().formalAssignments,
  assignorsIdList: state.assignments.toJS().assignorsIdList,
  assignorsNameMappings: state.assignments.toJS().assignorsNameMappings,
  cardsCount: state.assignments.toJS().formalAssignmentsCount,
  coursesCount: state.assignments.toJS().informalAssignmentsCount,
  cards: state.cards.toJS()
}))(LearningPathContainer);
