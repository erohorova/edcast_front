import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import UserBadgesContainer from '../../../components/common/UserBadgesContainer';
import { toggleFollow } from '../../../actions/usersActions';
import UserAvatar from 'edc-web-sdk/components/Avatar';
import Paper from 'edc-web-sdk/components/Paper';
import { Popover } from 'material-ui/Popover';

class UserInfo extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      followPending: false,
      user: props.user,
      showPopover: false
    };

    this.styles = {
      button: {
        margin: 12
      },
      label: {
        color: 'white',
        textTransform: 'uppercase'
      },
      editIcon: {
        width: '1rem',
        height: '1rem'
      },
      avatar: {
        maxHeight: '80px',

        maxWidth: '80px',

        width: '80px'
      },
      topics: {
        height: 'auto'
      }
    };

    this.showPopover = this.showPopover.bind(this);
    this.hidePopover = this.hidePopover.bind(this);
  }

  componentDidMount() {
    this.setState({
      infoEle: ReactDOM.findDOMNode(this.refs.infoIcon)
    });
  }

  followClickHandler = user => {
    this.setState({
      followPending: true
    });
    this.props.dispatch(toggleFollow(user.id, !user.isFollowing)).then(() => {
      let newUser = Object.assign({}, user);
      if (newUser.isFollowing) {
        newUser.followersCount--;
        newUser.isFollowing = !user.isFollowing;
      } else {
        newUser.followersCount++;
        newUser.isFollowing = !user.isFollowing;
      }
      this.setState({
        followPending: false,
        user: newUser
      });
    });
  };

  showPopover() {
    this.setState({ showPopover: true });
  }

  hidePopover() {
    this.setState({ showPopover: false });
  }

  render() {
    let user = this.state.user;
    return (
      <Paper>
        <div
          className="user-info"
          style={{
            background: 'url(' + user.coverImage + ')',
            backgroundSize: 'cover',
            height: '225px',
            overflow: 'hidden'
          }}
        >
          <div style={{ minHeight: '125px' }} className="row" />

          <Popover
            open={this.state.showPopover}
            anchorEl={this.state.infoEle}
            anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
            targetOrigin={{ horizontal: 'right', vertical: 'top' }}
            useLayerForClickAway={false}
            canAutoPosition={false}
            animated={false}
            className="custom-popover"
          >
            <p className="popover-title">{user.bio}</p>
          </Popover>

          <div
            style={{ backgroundColor: '#26273b', minHeight: '100px', border: '1px solid #646464' }}
            className="row"
          >
            <div
              className="small-3"
              style={{ paddingLeft: '32px', marginTop: '-44px', maxWidth: '140px' }}
            >
              <div
                style={{
                  width: '90px',
                  height: '90px',
                  border: '5px solid #ffffff',
                  borderRadius: '50px'
                }}
              >
                <UserAvatar size={80} style={this.styles.avatar} user={user} />
              </div>
            </div>
            <div className="small-6" style={{ padding: '6px 0 0 0' }}>
              <div style={{ color: 'rgb(255, 255, 255)', fontSize: '24px', fontWeight: 'bold' }}>
                {user.first_name} {user.last_name}
              </div>

              <div style={{ fontSize: '15px', color: 'rgb(203, 203, 203)' }}>
                <div>{user.email}</div>

                <div
                  className="v2-bio"
                  onMouseOver={this.showPopover}
                  onMouseOut={this.hidePopover}
                  ref="infoIcon"
                >
                  {user.bio}
                </div>
              </div>
            </div>
            <div className="small-3" style={{ marginTop: '-15px' }}>
              <UserBadgesContainer rolesDefaultNames={user.rolesDefaultNames} roles={user.roles} />
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

UserInfo.propTypes = {
  user: PropTypes.object
};

export default connect()(UserInfo);
