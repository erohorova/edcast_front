import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import ReactDOM from 'react-dom';
import { push } from 'react-router-redux';
import { langs } from '../../constants/languages';

class PublicProfileViewV4 extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      activeTab: 'Profile'
    };
  }

  handleTabChange = (e, path, label) => {
    e.preventDefault();
    if (this.props.pathname === path) {
      window.location.href = path;
    } else {
      this.props.dispatch(push(path));
    }
    this.setState({ activeTab: label });
  };

  renderTab = (activeTab, label, path) => {
    let selectedTab = activeTab === label;
    return (
      <a
        href="#"
        className={`profile-tabs-item ${selectedTab ? ' active-profile-tabs-item' : ''}`}
        onClick={e => this.handleTabChange(e, path, label)}
      >
        {tr(label)}
      </a>
    );
  };

  render() {
    let activeTab = '';
    switch (this.props.pathname) {
      case '/@' + this.props.handle + '/content':
        activeTab = 'Content';
        break;
      default:
        activeTab = 'Profile';
    }

    let output = [];
    output.push(this.renderTab(activeTab, 'Profile', '/@' + this.props.handle));
    output.push(this.renderTab(activeTab, 'Content', '/@' + this.props.handle + '/content'));

    return <div className="profile-tabs">{output}</div>;
  }
}

PublicProfileViewV4.propTypes = {
  pathname: PropTypes.string,
  handle: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

export default connect(mapStoreStateToProps)(PublicProfileViewV4);
