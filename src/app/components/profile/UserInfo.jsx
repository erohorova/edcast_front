import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import UserBadgesContainer from '../../components/common/UserBadgesContainer';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { toggleFollow } from '../../actions/usersActions';
import ListItem from 'material-ui/List/ListItem';
import UserAvatar from 'edc-web-sdk/components/Avatar';
import Paper from 'edc-web-sdk/components/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import { Permissions } from '../../utils/checkPermissions';
import { tr } from 'edc-web-sdk/helpers/translations';
import BlurImage from '../common/BlurImage';

class UserInfo extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      followPending: false,
      user: {}
    };

    this.styles = {
      button: {
        margin: 12
      },
      label: {
        color: 'white',
        textTransform: 'uppercase'
      },
      editIcon: {
        width: '1rem',
        height: '1rem'
      },
      topics: {
        height: 'auto'
      },
      avatarBox: {
        height: '5rem',
        width: '5rem',
        position: 'relative'
      }
    };
  }

  componentDidMount() {
    this.setState({
      user: this.props.user
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user) {
      this.setState({
        user: nextProps.user
      });
    }
  }

  followClickHandler = user => {
    this.setState({
      followPending: true
    });
    this.props
      .dispatch(toggleFollow(user.id, !user.isFollowing))
      .then(() => {
        let newUser = Object.assign({}, user);
        if (newUser.isFollowing) {
          newUser.followersCount--;
          newUser.isFollowing = !user.isFollowing;
        } else {
          newUser.followersCount++;
          newUser.isFollowing = !user.isFollowing;
        }
        this.setState({
          followPending: false,
          user: newUser
        });
      })
      .catch(err => {
        console.error(`Error in UserInfo.v1.toggleFollow.func : ${err}`);
      });
  };

  render() {
    let user = this.state.user;
    let getExpertTopics = !!user.profile && !!user.profile.expertTopics;
    let topics = getExpertTopics
      ? user.profile.expertTopics.map(topic => topic.topic_label).join(', ')
      : [];
    let defaultUserImage = '/i/images/default_user_blue.png';
    return (
      <Paper>
        <div className="container-padding user-info">
          <div className="row">
            <div className="small-9 user-info-content">
              <BlurImage
                style={this.styles.avatarBox}
                id={user.id}
                image={(user.avatarimages && user.avatarimages.medium) || defaultUserImage}
              />
              <div>
                <ListItem
                  className="user-info-list"
                  disabled
                  primaryText={
                    <div>
                      <b>{user.name}</b>
                      <UserBadgesContainer
                        roles={user.roles}
                        rolesDefaultNames={user.rolesDefaultNames}
                      />
                    </div>
                  }
                  secondaryText={
                    getExpertTopics && (
                      <div style={this.styles.topics} className="expertise-topics">
                        <span>{topics}</span>
                      </div>
                    )
                  }
                />
                <div className="user-data">
                  <small>
                    {tr('Following')} <strong>{user.followingCount}</strong>
                  </small>
                  <small>
                    {tr('Followers')} <strong>{user.followersCount}</strong>
                  </small>
                </div>
              </div>
            </div>
            <div className="small-3 right">
              {!this.props.isCurrentUser && !Permissions.has('DISABLE_USER_FOLLOW') && (
                <FollowButton
                  following={user.isFollowing}
                  className="follow"
                  label={tr(user.isFollowing ? 'Following' : 'Follow')}
                  hoverLabel={tr(user.isFollowing ? 'Unfollow' : '')}
                  pendingLabel={tr(user.isFollowing ? 'Unfollowing...' : 'Following...')}
                  pending={this.state.followPending}
                  onTouchTap={this.followClickHandler.bind(this, user)}
                />
              )}
              {this.props.isCurrentUser && (
                <RaisedButton
                  onTouchTap={() => {
                    this.props.dispatch(push('/settings'));
                  }}
                  label={tr('Edit')}
                  labelStyle={this.styles.label}
                  backgroundColor="#bcbcbc"
                  style={this.styles.button}
                  icon={<EditIcon style={this.styles.editIcon} />}
                />
              )}
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

UserInfo.propTypes = {
  isCurrentUser: PropTypes.bool,
  user: PropTypes.object
};

export default connect()(UserInfo);
