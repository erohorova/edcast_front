import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push, replace } from 'react-router-redux';
import Loadable from 'react-loadable';
import Select from 'react-select';

import { searchForUsers, getItems } from 'edc-web-sdk/requests/users.v2';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import BackIcon from 'edc-web-sdk/components/icons/BackIcon';
import Paper from 'edc-web-sdk/components/Paper';

import { updateUser } from '../../../actions/usersActions';

const UserSquareItem = Loadable({
  loader: () =>
    window.ldclient.variation('people-card-v2', false)
      ? import('../../common/UserSquareItemV2')
      : import('../../common/UserSquareItem'),
  loading: () => null
});

import SkillsFilterContainer from '../../peopleFilters/Skills';
import CategoriesFilterContainer from '../../peopleFilters/Categories';
import DateJoinedFilterContainer from '../../peopleFilters/DateJoined';
import CustomBlockFilterComponent from '../../peopleFilters/CustomBlock';
import Spinner from '../../common/spinner';

class MyTeamContainerv2 extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userIds: [],
      pending: true,
      isLastPage: false,
      filterType: props.isMeTab ? 'followers' : 'all',
      searchText: props.location.query ? props.location.query.q : '',
      itemMargin: 0,
      itemsCount: 0,
      isNewProfileNavigation: window.ldclient.variation('is-me-new-navigation', false),
      isNewPeopleFilter: window.ldclient.variation('new-people-search-filter', false),
      skills: [],
      customFields: [],
      limit: 20,
      searchBy:
        props.location.query && props.location.query.type === 'name'
          ? { value: 'name', label: tr('By Name') }
          : { value: 'skills', label: tr('By Skill') },
      users: [],
      isPeopleCardV2: window.ldclient.variation('people-card-v2', false)
    };
    this.styles = {
      searchIcon: {
        position: 'absolute',
        bottom: '0.375rem',
        verticalAlign: 'middle',
        width: '1.5rem',
        height: '1.5rem',
        fill: colors.silverSand
      },
      teamGroup: {
        display: 'flex',
        justifyContent: 'flex-end'
      }
    };
    this.query = {
      limit: 20,
      q: props.location.query ? props.location.query.q : '',
      search_by: props.location.query && props.location.query.type,
      fields:
        'id,avatarimages,following_count,handle,name,is_following,roles,followers_count,is_complete,is_suspended'
    };
    if (this.state.isPeopleCardV2) {
      this.query.fields += ',profile,coverimages';
    }
    let filterParam = this.props.routeParams.filter ? `/${this.props.routeParams.filter}` : '';

    this.props.dispatch(
      replace(
        this.state.isNewProfileNavigation && !props.isMeTab
          ? `/team${filterParam}`
          : `/me/team${filterParam}`
      )
    );

    /***
        This condition is regarding the ticket:
        http://jira.edcastcloud.com/browse/EP-17897 Specifically for the client "Dell"
    ***/
    let minimizeFilters = window.ldclient.variation('minimise-team-filters', false);
    if (minimizeFilters) {
      this.filterOptions = [
        { value: 'all', text: 'All users' },
        { value: 'followers', text: 'People following me' },
        { value: 'following', text: 'People I am following' }
      ];
    } else {
      this.filterOptions = [
        { value: 'all', text: 'All users' },
        { value: 'followers', text: 'People following me' },
        { value: 'following', text: 'People I am following' },
        { value: 'sme', text: this.props.discover['discover/users'].label || "SME's" },
        { value: 'influencer', text: 'Influencers' },
        { value: 'admin', text: 'Admins' }
      ];
    }

    if (props.isMeTab) {
      this.filterOptions = [
        { value: 'followers', text: 'People following me' },
        { value: 'following', text: 'People I am following' }
      ];
    }

    this.skillTopics = [];
    this.gettingUsersCounter = 0;

    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreTeams = this.showMoreTeams.bind(this);
  }

  componentDidMount() {
    if (this.state.isNewPeopleFilter && this.state.searchText && this.state.searchText.length) {
      this.usersUpdate(this.query, true);
      this._inputFilter.value = this.state.searchText;
    } else {
      this.handleRouteParams(this.props.routeParams.filter, true);
    }
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.routeParams.filter !== this.props.routeParams.filter &&
      (!this.state.isNewPeopleFilter || !(this.state.searchText && this.state.searchText.length))
    ) {
      if (
        this.state.users &&
        this.state.users.length &&
        ['following', 'sme', 'influencer', 'admin'].indexOf(nextProps.routeParams.filter) > -1
      ) {
        this.setState(
          prevState => ({
            users: prevState.users.filter(item => {
              if (nextProps.routeParams.filter === 'following') {
                return item.isFollowing;
              } else {
                return (
                  item.roles &&
                  item.roles.some(role => role.toLowerCase() === nextProps.routeParams.filter)
                );
              }
            }),
            filterType: nextProps.routeParams.filter || (nextProps.isMeTab ? 'followers' : 'all')
          }),
          () => {
            if (this.state.users.length < 20) {
              this.handleRouteParams(nextProps.routeParams.filter);
            }
          }
        );
      } else {
        this.handleRouteParams(nextProps.routeParams.filter);
      }
    }
    if (
      nextProps.location.query &&
      (nextProps.location.query.q || nextProps.location.query.type) &&
      (nextProps.location.query.q !== this.query.q ||
        nextProps.location.query.type !== this.query.search_by)
    ) {
      this.setState({
        searchText: nextProps.location.query.q,
        searchBy:
          nextProps.location.query.type === 'name'
            ? { value: 'name', label: tr('By Name') }
            : { value: 'skills', label: tr('By Skill') }
      });
      this.query.q = nextProps.location.query.q || '';
      this.query.search_by = nextProps.location.query.type;
      this.query.fields =
        'id,avatarimages,following_count,handle,name,is_following,roles,followers_count,is_complete,is_suspended';
      if (this.state.isPeopleCardV2) {
        this.query.fields += ',profile,coverimages';
      }
      this.props.dispatch(replace(`/team`));
      this.usersUpdate(this.query, true);
      this._inputFilter.value = nextProps.location.query.q || '';
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleRouteParams = (param, isFirst) => {
    this.query = { limit: 20 };
    let isFilterValid = this.filterOptions.some(option => option.value === param);
    let filter = isFilterValid ? param : this.props.isMeTab ? 'followers' : 'all';
    this.setState({ pending: true, filterType: filter, cardIds: [] });
    this.query = this.state.isNewPeopleFilter
      ? this.formPayload('category', param, true, '')
      : this.formPayload(filter, null, null, this.state.searchText);
    this.usersUpdate(this.query, true, isFirst);
  };

  handleScroll = _.throttle(
    () => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreTeams();
        }
      }
    },
    150,
    { leading: false }
  );

  handleQueryArray = (filterSubtype, name, checked) => {
    if (checked) {
      if (this.query[filterSubtype] && this.query[filterSubtype].length) {
        this.query[filterSubtype].push(name);
      } else {
        this.query[filterSubtype] = [name];
      }
    } else {
      this.query[filterSubtype].splice(this.query[filterSubtype].indexOf(name), 1);
      if (!this.query[filterSubtype].length) {
        delete this.query[filterSubtype];
      }
    }
  };

  formPayload = (filterType, name, value, searchText, customFieldName) => {
    if (this.state.isNewPeopleFilter) {
      let payload = this.query;
      switch (filterType) {
        case 'category':
          if (~['followers', 'following'].indexOf(name)) {
            if (value) {
              this.query[name] = true;
            } else {
              delete this.query[name];
            }
          } else if (~['sme', 'admin', 'influencer'].indexOf(name)) {
            this.handleQueryArray('role[]', name, value);
          }
          break;
        case 'skill':
          this.handleQueryArray('expertises_names[]', name, value);
          break;
        case 'dates':
          this.query[name] = value;
          break;
        case 'customField':
          if (this.query['custom_fields']) {
            try {
              this.query['custom_fields'] = JSON.parse(this.query['custom_fields']);
            } catch (e) {
              console.error('JSON parse error in MyTeamV2');
            }
          }
          let customFieldIndex =
            this.query['custom_fields'] &&
            _.findIndex(this.query['custom_fields'], item => item.name === customFieldName);
          if (value) {
            if (this.query['custom_fields']) {
              if (~customFieldIndex) {
                this.query['custom_fields'][customFieldIndex].values.push(name);
              } else {
                this.query['custom_fields'].push({ name: customFieldName, values: [name] });
              }
            } else {
              this.query['custom_fields'] = [{ name: customFieldName, values: [name] }];
            }
          } else {
            this.query['custom_fields'][customFieldIndex].values.splice(
              _.findIndex(
                this.query['custom_fields'][customFieldIndex].values,
                item => item === name
              ),
              1
            );
            if (!this.query['custom_fields'][customFieldIndex].values.length) {
              this.query['custom_fields'].splice(customFieldIndex, 1);
            }
            if (!this.query['custom_fields'].length) {
              delete this.query['custom_fields'];
            }
          }
          if (this.query['custom_fields'])
            this.query['custom_fields'] = JSON.stringify(this.query['custom_fields']);
          break;
        default:
          break;
      }
      payload.q = searchText || this.state.searchText || '';
      payload.fields =
        'id,avatarimages,following_count,handle,name,is_following,roles,followers_count,is_complete,is_suspended';
      if (this.state.isPeopleCardV2) {
        payload.fields += ',profile,coverimages';
      }
      if (
        (!(this.props.routeParams && this.props.routeParams.filter) ||
          window.location.pathname === '/me/team') &&
        this.props.isMeTab
      ) {
        payload['followers'] = true;
      }
      return payload;
    } else {
      let payload = {};
      switch (filterType) {
        case 'followers':
          payload = { followers: true };
          break;
        case 'following':
          payload = { following: true };
          break;
        case 'sme':
          payload = { role: 'sme' };
          break;
        case 'influencer':
          payload = { role: 'influencer' };
          break;
        case 'admin':
          payload = { role: 'admin' };
          break;
        default:
          break;
      }
      if (searchText !== undefined) {
        payload.q = searchText;
      }
      payload.limit = this.state.limit;
      this.query = payload;
      return payload;
    }
  };

  showMoreTeams = () => {
    let query = { ...this.query, ...{ offset: this.state.userIds.length } };
    this.setState({ pending: true });
    this.usersUpdate(query);
  };

  usersUpdate = (query, isFilter, isFirst) => {
    if (this.state.isNewPeopleFilter) {
      this.setState({ pending: true }, () => {
        searchForUsers(query)
          .then(data => {
            let users = data.users;
            let userIds = users.map(user => user.id);
            let isLastPage = userIds.length < this.query.limit;
            let customFields =
              (data.aggregations &&
                Object.values(data.aggregations).map(
                  item =>
                    item.filtered &&
                    item.filtered.keywords &&
                    item.filtered.keywords.values &&
                    item.filtered.keywords.values.buckets
                )) ||
              [];
            customFields = _.compact(_.flatten(customFields));
            let skills =
              (data.aggregations &&
                data.aggregations.expert_topics &&
                data.aggregations.expert_topics.filtered &&
                data.aggregations.expert_topics.filtered.values &&
                data.aggregations.expert_topics.filtered.values.buckets) ||
              [];
            if (isFirst) {
              this.skillTopics = skills;
            }
            this.setState({
              userIds: isFilter ? userIds : _.uniq(this.state.userIds.concat(userIds)),
              pending: false,
              isLastPage,
              skills,
              customFields,
              users: isFilter ? users : _.uniqBy(this.state.users.concat(users), 'id')
            });
          })
          .catch(err => {
            console.error(`Error in MyTeamContainerv2.usersUpdate.searchForUsers.func : ${err}`);
          });
      });
    } else {
      getItems(query)
        .then(users => {
          let userIds = users.items.map(user => user.id);
          let isLastPage = userIds.length < this.state.limit;
          this.setState({
            userIds: isFilter ? userIds : _.uniq(this.state.userIds.concat(userIds)),
            pending: false,
            isLastPage,
            users: isFilter ? users.items : _.uniqBy(this.state.users.concat(users.items), 'id')
          });
        })
        .catch(err => {
          console.error(`Error in MyTeamContainerv2.usersUpdate.getItems.func : ${err}`);
        });
    }
  };

  handleFilterChange = (checked, name, type, customFieldName) => {
    if (this.state.isNewPeopleFilter && (this.state.searchText && this.state.searchText.length)) {
      this.query = this.formPayload(type, name, checked, this.state.searchText, customFieldName);
      this.usersUpdate(this.query, true);
    } else {
      this.filterOptions.map(option => {
        if (checked === option.value && checked !== this.state.filterType) {
          this.setState({ userIds: [], filterType: option.value });
          if (this.props.isMeTab) {
            this.props.dispatch(push(`/me/team/${option.value}`));
          } else {
            this.props.dispatch(
              push(
                this.state.isNewProfileNavigation
                  ? `/team/${option.value}`
                  : `/me/team/${option.value}`
              )
            );
          }
        }
      });
    }
  };

  handleFilterSearch = e => {
    if (e.keyCode === 13) {
      this.filterSearch();
    }
  };

  filterSearch = () => {
    let val = this._inputFilter.value;
    this.setState({ searchText: val }, () => {
      if (this.state.searchText && !val && this.props.routeParams.filter) {
        this.handleRouteParams(this.props.routeParams.filter);
      } else {
        this.query.q = val === '*' ? '' : val;
        this.query.search_by = this.state.searchBy.value;
        this.query.skip_aggs = false;
        this.usersUpdate(this.query, true);
      }
    });
  };

  filterChange = value => {
    this.setState({ searchBy: value, isShowSuggestions: false, suggestions: [] }, () => {
      this.getSuggestions(true);
      if (this.state.searchText && this.state.searchText.length) {
        this.filterSearch();
      }
    });
  };

  toggleShowingSuggestions = (e, clearSuggestions) => {
    this.setState(
      prevState => {
        let newState = { isShowSuggestions: !prevState.isShowSuggestions };
        if (clearSuggestions) newState = { ...newState, ...{ suggestions: [] } };
        return newState;
      },
      () => {
        if (this.state.isShowSuggestions) {
          document.body.addEventListener('click', this.hideSuggsContainer);
        } else {
          document.body.removeEventListener('click', this.hideSuggsContainer);
        }
      }
    );
  };

  hideSuggsContainer = e => {
    if (
      e &&
      e.target &&
      e.target.className &&
      (e.target.className === 'suggestion-item' || e.target.className === 'search-channels-input')
    )
      return;
    this.toggleShowingSuggestions();
  };

  getSuggestionsOnKeystroke = _.debounce(
    () => {
      this.getSuggestions();
    },
    150,
    { leading: false, trailing: true }
  );

  getSuggestions = param => {
    let requestNumber = ++this.gettingUsersCounter;
    let currentSearchText = this._inputFilter.value && this._inputFilter.value.trim();
    if (!currentSearchText) {
      return this.setState({
        isShowSuggestions: false,
        suggestions: []
      });
    }
    if (this.state.searchBy.value === 'name') {
      getItems({ q: currentSearchText })
        .then(data => {
          if (requestNumber !== this.gettingUsersCounter) return;
          if (data && data.items) {
            this.setState({
              suggestions: data.items.map(item => ({
                id: item.id,
                label: item.fullName || item.name || `${item.firstName} ${item.lastName}`
              })),
              isShowSuggestions: param ? false : !!data.items.length
            });
          }
        })
        .catch(err => {
          console.error(`Error in MyTeamV2.getItems.func : ${err}`);
        });
    } else {
      if (this.skillTopics.length) {
        this.setState({
          suggestions: this.skillTopics
            .filter(
              item =>
                item.key && item.key.toLowerCase().indexOf(currentSearchText.toLowerCase()) > -1
            )
            .slice(0, 10),
          isShowSuggestions: true
        });
      }
    }
  };

  chooseSuggestion = item => {
    this._inputFilter.value = item && item[this.state.searchBy.value === 'name' ? 'label' : 'key'];
    this.toggleShowingSuggestions(null, true);
    this.filterSearch();
  };

  render() {
    const isMeTab = this.props.isMeTab || false;
    let currentUserId = parseInt(this.props.currentUser.get('id'));
    return (
      <div className={`my-team_v2 ${this.state.isNewProfileNavigation ? 'lbv2_new' : ''}`}>
        {!isMeTab && (
          <div
            className="team-group"
            style={this.state.isNewProfileNavigation ? {} : this.styles.teamGroup}
          >
            {this.state.isNewProfileNavigation && (
              <div className="lbv2__title">
                <div
                  aria-label="back"
                  role="heading"
                  aria-level="3"
                  className="breadcrumbBack"
                  onClick={() => {
                    window.history.back();
                  }}
                >
                  <BackIcon style={{ width: '1.1875rem' }} color={'#454560'} />
                </div>
                <span>{tr('People')}</span>
              </div>
            )}
            {this.handleFilterSearch && (
              <div className="my-team__search-bar">
                <div className="my-team__search-input-block">
                  <input
                    placeholder={tr('Search people')}
                    onFocus={this.toggleShowingSuggestions}
                    className="search-channels-input"
                    onChange={this.getSuggestionsOnKeystroke}
                    onKeyDown={this.handleFilterSearch}
                    type="text"
                    ref={node => (this._inputFilter = node)}
                  />
                  <SearchIcon style={this.styles.searchIcon} onClick={this.filterSearch} />
                  {this.state.isShowSuggestions &&
                    this.state.suggestions &&
                    !!this.state.suggestions.length && (
                      <div className="channels-suggestions-container">
                        <ul>
                          {this.state.suggestions.map((suggestion, index) => (
                            <li
                              key={`suggestion-${
                                this.state.searchBy.value === 'name' ? suggestion.id : index
                              }`}
                              className="suggestion-item"
                              onClick={this.chooseSuggestion.bind(this, suggestion)}
                            >
                              {this.state.searchBy.value === 'name'
                                ? suggestion.label
                                : suggestion.key}
                            </li>
                          ))}
                        </ul>
                      </div>
                    )}
                </div>
                {this.state.isNewPeopleFilter && (
                  <div className="my-team__seacrh-by-filter">
                    <Select
                      cache={false}
                      onChange={this.filterChange.bind(this)}
                      value={this.state.searchBy}
                      options={[
                        { value: 'name', label: tr('By Name') },
                        { value: 'skills', label: tr('By Skill') }
                      ]}
                      clearable={false}
                      searchable={false}
                    />
                  </div>
                )}
              </div>
            )}
          </div>
        )}

        <div className="my-team__container">
          <div
            className={`my-team__filter-block ${
              this.state.isNewPeopleFilter ? 'my-team__filter-block_new' : ''
            }`}
          >
            {this.state.isNewPeopleFilter &&
            this.state.searchText &&
            this.state.searchText.length !== 0 ? (
              <div>
                {this.state.skills && !!this.state.skills.length && (
                  <SkillsFilterContainer
                    hideFilters={false}
                    handleFilterChange={this.handleFilterChange}
                    type={'skills'}
                    skills={this.state.skills}
                    query={this.query}
                  />
                )}
                <DateJoinedFilterContainer
                  hideFilters={false}
                  handleFilterChange={this.handleFilterChange}
                  type={'date_joined'}
                />
                <CategoriesFilterContainer
                  options={this.filterOptions.slice(1, this.filterOptions.length)}
                  hideFilters={false}
                  handleFilterChange={this.handleFilterChange}
                  type={'categories'}
                  query={this.query}
                />
                {!!this.state.customFields.length &&
                  this.state.customFields.map((item, index) => {
                    return (
                      <CustomBlockFilterComponent
                        key={index}
                        handleFilterChange={this.handleFilterChange}
                        type={item.key}
                        options={(item.count && item.count.buckets) || []}
                        hideFilters={false}
                        query={this.query}
                      />
                    );
                  })}
              </div>
            ) : (
              this.filterOptions.map((opt, index) => {
                return (
                  <div
                    className={`my-team__filter-item ${
                      this.state.filterType === opt.value ? 'my-team__filter-item_active' : ''
                    }`}
                    key={`filter_${index}`}
                    onClick={() => this.handleFilterChange(opt.value)}
                  >
                    {tr(opt.text)}
                  </div>
                );
              })
            )}
          </div>
          <div className="custom-card-container">
            <div className="five-card-column">
              {this.state.users.length
                ? this.state.users.map((user, index) => {
                    return user['handle'] ? (
                      <UserSquareItem
                        key={`team_user_${index}`}
                        user={user}
                        isCurrentUser={user.id == currentUserId}
                        currentUser={this.props.currentUser}
                      />
                    ) : null;
                  })
                : !this.state.pending && (
                    <div className="row">
                      <Paper className="small-12">
                        <div className="container-padding vertical-spacing-large text-center">
                          <p>
                            {tr(
                              `Looks like the person you are looking for has not joined Edcast yet.`
                            )}
                          </p>
                          <p>{tr(`Invite your colleague today!`)}</p>
                        </div>
                      </Paper>
                    </div>
                  )}
              {this.state.pending && (
                <div className="channel-loading">
                  <Spinner />
                </div>
              )}

              {isMeTab && this.state.isLastPage && (
                <p className="explore-msg-channel-group-user text-center">
                  <a
                    onClick={() => {
                      this.props.dispatch(push('/team'));
                    }}
                  >
                    <strong>Search</strong>
                  </a>{' '}
                  other users to explore and follow
                </p>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MyTeamContainerv2.propTypes = {
  users: PropTypes.object,
  routeParams: PropTypes.object,
  currentUser: PropTypes.object,
  location: PropTypes.any,
  isMeTab: PropTypes.bool
};

export default connect(state => ({
  currentUser: state.currentUser,
  users: state.users,
  discover: state.team.get('Discover')
}))(MyTeamContainerv2);
