import React from 'react';
import { tr } from 'edc-web-sdk/helpers/translations';

export default class MyContributions extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return <div>{tr('My Contributions page')}</div>;
  }
}
