import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserCards } from '../../actions/cardsActions';
import Card from '../cards/Card';
import throttle from 'lodash/throttle';
import uniq from 'lodash/uniq';
import { tr } from 'edc-web-sdk/helpers/translations';
import Spinner from '../common/spinner';
import { getUserByHandle } from '../../actions/usersActions';

class PublicProfileContent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      limit: 10,
      cardIds: [],
      pending: true,
      filterType: 'all',
      cardState: ['published'],
      user: null
    };
    this.handleScroll = this.handleScroll.bind(this);
    this.showMoreCards = this.showMoreCards.bind(this);
  }

  componentDidMount() {
    this.props
      .dispatch(getUserByHandle('@' + this.props.params.handle))
      .then(data => {
        this.setState({
          user: data.user
        });
        this.fetchCards('All');
        window.addEventListener('scroll', this.handleScroll);
      })
      .catch(err => {
        console.error(`Error in PublicProfileContent.getUserByHandle.func : ${err}`);
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    event => {
      if (this.state.pending) {
        return;
      }
      if (window.innerHeight + Math.ceil(window.pageYOffset) >= document.body.offsetHeight) {
        if (!this.state.isLastPage) {
          this.showMoreCards();
        }
      }
    },
    150,
    { leading: false }
  );

  fetchCards = (param, strOffset, cOffset) => {
    let func, arg;
    let streamsOffset = strOffset ? strOffset : 0;
    let cardsOffset = cOffset ? cOffset : 0;

    func = getUserCards;
    arg = [
      this.state.user.id,
      this.state.limit,
      { cards: cardsOffset, streams: streamsOffset },
      undefined,
      this.state.filterType,
      this.state.cardState,
      'created'
    ];

    if (func) {
      this.props
        .dispatch(func(...arg))
        .then(cardIds => {
          this.setState({
            cardIds:
              !cardsOffset && !streamsOffset ? cardIds : uniq(this.state.cardIds.concat(cardIds)),
            pending: false,
            isLastPage: cardIds.length < this.state.limit
          });
        })
        .catch(err => {
          console.error(`Error in PublicProfileContent.fetchCards.func : ${err}`);
        });
    }
  };

  showMoreCards = () => {
    let cards = this.state.cardIds
      .filter(id => this.props.cards[id])
      .map(id => this.props.cards[id]);
    let cardsOffset = 0,
      streamsOffset = 0;
    cards.forEach(card => {
      if (card.cardType === 'VideoStream' || card.cardType === 'video_stream') {
        streamsOffset++;
      } else {
        cardsOffset++;
      }
    });
    this.setState({ pending: true }, () => {
      this.fetchCards(this.state.filterType, streamsOffset, cardsOffset);
    });
  };

  render() {
    return (
      <div id="publicContent">
        <div className="public-content-wrapper">
          <div className="public-content-right-block">
            <div>
              {!!this.state.cardIds.length && (
                <div className="custom-card-container">
                  <div className="five-card-column">
                    {this.state.cardIds
                      .filter(id => this.props.cards[id])
                      .map((id, index) => {
                        let card = this.props.cards[id];
                        let usersMap = this.props.users.get('idMap');
                        let author =
                          card && usersMap[card.authorId || (card.author && card.author.id)];
                        return (
                          <Card
                            state="published"
                            card={card}
                            key={card.id}
                            dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                            startDate={
                              card.startDate || (card.assignment && card.assignment.startDate)
                            }
                            user={author}
                            author={author}
                            moreCards={true}
                          />
                        );
                      })}
                  </div>
                </div>
              )}
              {!this.state.pending && !this.state.cardIds.length && (
                <div className="container-padding vertical-spacing-medium custom-result">
                  <div className="text-center vertical-spacing-large">
                    <div className="data-not-available-msg">
                      {this.state.user.fullName}
                      {tr(" hasn't posted anything yet.")}
                    </div>
                  </div>
                </div>
              )}
              {this.state.pending && (
                <div className="text-center pending-cards">
                  <Spinner />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PublicProfileContent.propTypes = {
  cards: PropTypes.object,
  users: PropTypes.object,
  params: PropTypes.object
};

export default connect(state => ({
  cards: state.cards.toJS(),
  users: state.users
}))(PublicProfileContent);
