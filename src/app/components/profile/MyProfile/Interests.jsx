import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { openUpdateInterestsModal } from '../../../actions/modalActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';

class Interests extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };

    this.state = {
      interests: [],
      loadingComplete: false
    };

    // this.fetchUserInterests = this.fetchUserInterests.bind(this);
    this.triggerUpdateInterests = this.triggerUpdateInterests.bind(this);
  }
  componentDidMount() {
    if (this.props.currentUser.id) {
      this.setState({
        interests:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.learningTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modal && nextProps.modal.reloadInterests) {
      this.setState({
        interests:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.learningTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  triggerUpdateInterests() {
    this.props.dispatch(openUpdateInterestsModal(this.props.currentUser.id));
  }

  render() {
    let interests = this.state.interests != undefined ? this.state.interests : [];
    return (
      <Paper>
        <div className="container-padding vertical-spacing-medium">
          <div className="clearfix">
            <div className="float-left" style={{ maxWidth: '85%' }}>
              <strong>{tr('LEARNING GOALS')}</strong>
              <br />
              {this.state.loadingComplete && interests.length > 0 && (
                <small className="data-not-available-msg">
                  {tr(
                    'Here are the three topics that you are currently interested in. Want to learn something new? Simply change your topics and we will personalize your feed around those topics.'
                  )}
                </small>
              )}
            </div>
          </div>
          <div className="float-right">
            <IconButton
              className="edit"
              aria-label="update interests"
              onTouchTap={this.triggerUpdateInterests}
              iconStyle={this.styles.smallIcon}
              style={this.styles.smallIconButton}
            >
              <EditIcon color={colors.silverSand} />
            </IconButton>
          </div>
          {!this.state.loadingComplete && (
            <p className="data-not-available-msg">
              <small>{tr('Loading your list of learning goals ...')}</small>
            </p>
          )}

          <div>
            <div style={this.styles.chipsWrapper}>
              {interests.map((int, idx) => {
                return (
                  <Chip key={idx} style={this.styles.chip} backgroundColor={colors.primary}>
                    <small style={{ color: colors.white }}>{int.topic_label}</small>
                  </Chip>
                );
              })}
            </div>
          </div>

          {this.state.loadingComplete && interests.length == 0 && (
            <div className="text-center">
              <p className="text-left">
                <small className="data-not-available-msg">
                  {tr(
                    'It’s easy to start learning on EdCast. Tell us three things you are interested in learning and we will personalize your feed around those topics.'
                  )}
                </small>
                <br />
                <br />
              </p>
              <PrimaryButton
                label={tr('Add Topics')}
                className="edit"
                onTouchTap={this.triggerUpdateInterests}
              />
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal.toJS(),
    onboarding: state.onboarding.toJS()
  };
}

Interests.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object
};

export default connect(mapStoreStateToProps)(Interests);
