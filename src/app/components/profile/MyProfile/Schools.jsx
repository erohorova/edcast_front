import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import ListItem from 'material-ui/List/ListItem';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import { getShcools, addShcool, editShcool, removeSchool } from 'edc-web-sdk/requests/profile';
import { confirmation } from '../../../actions/modalActions';
import { open as openSnack } from '../../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class Schools extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      schools: [],
      editing: []
    };
  }

  componentDidMount() {
    if (this.props.currentUser.id) {
      getShcools(this.props.currentUser.id).then(schools => {
        this.setState({ schools });
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentUser.id !== this.props.currentUser.id) {
      getShcools(this.props.currentUser.id).then(schools => {
        this.setState({ schools });
      });
    }
  }

  addClickHandler = () => {
    let schools = this.state.schools.concat({ isEditing: true });
    let editing = this.state.editing;
    editing[schools.length - 1] = {};
    this.setState({ schools, editing });
  };

  cancelEditClickHandler = index => {
    let schools = this.state.schools;
    this.state.schools[index].isEditing = false;
    let editing = this.state.editing;
    editing[index] = {};
    this.setState({ schools, editing });
  };

  saveClickHandler = index => {
    let data = this.state.editing[index];
    if (data.id) {
      editShcool(
        this.props.currentUser.id,
        data.id,
        data.specialization,
        data.name,
        data.fromYear,
        data.toYear
      )
        .then(() => {
          let schools = this.state.schools;
          schools[index] = Object.assign({}, data, { isEditing: false });
          this.setState({ schools });
        })
        .catch(err => {
          this.props.dispatch(openSnack(err, true));
        });
    } else {
      addShcool(
        this.props.currentUser.id,
        data.specialization,
        data.name,
        data.fromYear,
        data.toYear
      )
        .then(id => {
          let schools = this.state.schools;
          schools[index] = Object.assign({}, data, { id, isEditing: false });
          this.setState({ schools });
        })
        .catch(err => {
          this.props.dispatch(openSnack(err, true));
        });
    }
  };

  inputChangeHandler = (field, index, event, value) => {
    let editing = this.state.editing;
    editing[index][field] = value;
    this.setState({ editing });
  };

  editClickHandler = index => {
    let schools = this.state.schools;
    schools[index].isEditing = true;
    let editing = this.state.editing;
    editing[index] = Object.assign({}, schools[index]);
    this.setState({ schools, editing });
  };

  removeClickHandler = index => {
    this.props.dispatch(
      confirmation('Confirm', 'Do you want to remove this education source?', () => {
        let schools = this.state.schools;
        removeSchool(this.props.currentUser.id, schools[index].id).then(() => {
          schools[index].id = undefined;
          this.setState({ schools });
        });
      })
    );
  };

  render() {
    return (
      <Paper className="container-padding vertical-spacing-medium">
        <div>
          <strong>{tr('EDUCATION')}</strong>
        </div>
        <div className="data-not-available-msg">
          <small>{tr('Track all your learning in one place.')}</small>
        </div>
        {this.state.schools.map((school, index) => {
          if (!school.isEditing && school.id) {
            return (
              <ListItem
                primaryText={<strong>{school.specialization}</strong>}
                secondaryText={
                  <div>
                    <small>{school.name}</small>
                    <br />
                    <small>
                      {school.fromYear} - {school.toYear}
                    </small>
                  </div>
                }
                secondaryTextLines={2}
                rightIconButton={
                  <div>
                    <IconButton
                      aria-label="edit"
                      className="edit"
                      onTouchTap={this.editClickHandler.bind(this, index)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      aria-label="remove"
                      className="delete"
                      onTouchTap={this.removeClickHandler.bind(this, index)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                }
                className="edit"
                onTouchTap={this.editClickHandler.bind(this, index)}
              />
            );
          } else if (school.isEditing) {
            let school = this.state.editing[index];
            return (
              <div>
                <div>
                  <TextField
                    value={school.specialization}
                    onChange={this.inputChangeHandler.bind(this, 'specialization', index)}
                    fullWidth={true}
                    aria-label={tr('Degree or Specialization')}
                    hintText={tr('Degree or Specialization')}
                  />
                </div>
                <div>
                  <TextField
                    value={school.name}
                    onChange={this.inputChangeHandler.bind(this, 'name', index)}
                    fullWidth={true}
                    aria-label={tr('College or University')}
                    hintText={tr('College or University')}
                  />
                </div>
                <TextField
                  value={school.fromYear}
                  onChange={this.inputChangeHandler.bind(this, 'fromYear', index)}
                  aria-label={tr('From Year')}
                  hintText={tr('From Year')}
                />{' '}
                -{' '}
                <TextField
                  value={school.toYear}
                  onChange={this.inputChangeHandler.bind(this, 'toYear', index)}
                  aria-label={tr('To Year')}
                  hintText={tr('To Year')}
                />
                <div className="text-right">
                  <SecondaryButton
                    className="close"
                    onTouchTap={this.cancelEditClickHandler.bind(this, index)}
                    label={tr('Cancel')}
                  />
                  <PrimaryButton
                    className="create"
                    onTouchTap={this.saveClickHandler.bind(this, index)}
                    label={tr('Save')}
                  />
                </div>
              </div>
            );
          }
        })}
        <div className="text-center">
          <SecondaryButton
            className="edit"
            onTouchTap={this.addClickHandler}
            label={tr('Add your Education details')}
          />
        </div>
      </Paper>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS())
  };
}

Schools.propTypes = {
  currentUser: PropTypes.object
};

export default connect(mapStoreStateToProps)(Schools);
