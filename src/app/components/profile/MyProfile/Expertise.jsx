import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import Chip from 'material-ui/Chip';
import colors from 'edc-web-sdk/components/colors/index';
import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import { openUpdateExpertiseModal } from '../../../actions/modalActions';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import { tr } from 'edc-web-sdk/helpers/translations';

class Expertise extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      chipsWrapper: {
        display: 'flex',
        flexWrap: 'wrap'
      },
      chip: {
        margin: '4px'
      },
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };

    this.state = {
      expertise: [],
      loadingComplete: false
    };

    // this.fetchUserExpertise = this.fetchUserExpertise.bind(this);
    this.triggerUpdateExpertise = this.triggerUpdateExpertise.bind(this);
  }
  componentDidMount() {
    if (this.props.currentUser.id) {
      this.setState({
        expertise:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.expertTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  triggerUpdateExpertise() {
    this.props.dispatch(openUpdateExpertiseModal(this.props.currentUser.id));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modal && nextProps.modal.reloadInterests) {
      this.setState({
        expertise:
          (this.props.currentUser &&
            this.props.currentUser.profile &&
            this.props.currentUser.profile.expertTopics) ||
          [],
        loadingComplete: true
      });
    }
  }

  render() {
    let expertise = this.state.expertise != undefined ? this.state.expertise : [];
    return (
      <Paper>
        <div className="container-padding vertical-spacing-medium">
          <div className="clearfix">
            <div className="float-left" style={{ maxWidth: '85%' }}>
              <strong>{tr('SKILLS')}</strong>
              <br />
              {this.state.loadingComplete && expertise.length > 0 && (
                <small className="data-not-available-msg">
                  {tr(
                    'Add your current skills to increase your visibility in the team and help others discover you.'
                  )}
                </small>
              )}
            </div>
            <div className="float-right">
              <IconButton
                className="edit"
                aria-label="update expertise"
                onTouchTap={this.triggerUpdateExpertise}
                iconStyle={this.styles.smallIcon}
                style={this.styles.smallIconButton}
              >
                <EditIcon color={colors.silverSand} />
              </IconButton>
            </div>
          </div>
          {!this.state.loadingComplete && (
            <p className="data-not-available-msg">
              <small>{tr('Loading your list of skills ...')}</small>
            </p>
          )}

          <div>
            <div style={this.styles.chipsWrapper}>
              {expertise.map((exp, idx) => {
                return (
                  <Chip key={idx} style={this.styles.chip} backgroundColor={colors.primary}>
                    <small style={{ color: colors.white }}>{exp.topic_label}</small>
                  </Chip>
                );
              })}
            </div>
          </div>

          {this.state.loadingComplete && expertise.length == 0 && (
            <div className="text-center">
              <p className="text-left data-not-available-msg">
                <small>
                  {tr(
                    "You haven't added any skills yet. Including skills in your profile will help you gain more followers and visibility in your organization."
                  )}
                </small>
                <br />
                <br />
              </p>
              <PrimaryButton
                label={tr('Add Now')}
                className="edit"
                onTouchTap={this.triggerUpdateExpertise}
              />
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    modal: state.modal.toJS()
  };
}

Expertise.propTypes = {
  currentUser: PropTypes.object,
  modal: PropTypes.object
};

export default connect(mapStoreStateToProps)(Expertise);
