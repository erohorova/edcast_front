import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { edcastcloud } from 'edc-web-sdk/requests/index';
import { langs } from '../../../constants/languages';

class ContinuousLearning extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      courses: [],
      collapsedMode: true
    };

    this.styles = {
      smallIconButton: {
        width: '36px',
        height: '36px',
        padding: '8px'
      },
      smallIcon: {
        width: '18px',
        height: '18px'
      }
    };
    for (let prop in langs) {
      if (props.currentUser.profile && langs[prop] === props.currentUser.profile.language)
        this.profileLanguage = prop.toLowerCase();
    }
    this.isShowCustomLabels = props.team.config && props.team.config.custom_labels;
    let learningConfig =
      this.props.team.OrgConfig &&
      this.props.team.OrgConfig.sections &&
      this.props.team.OrgConfig.sections['web/sections/continuousLearning'];
    this.translatedLabel =
      this.isShowCustomLabels &&
      learningConfig &&
      learningConfig.languages &&
      learningConfig.languages[this.profileLanguage] &&
      learningConfig.languages[this.profileLanguage].trim();
    this.handleConnectClick = this.handleConnectClick.bind(this);
    this.fetchCourses = this.fetchCourses.bind(this);
    this.toggleCollapsedMode = this.toggleCollapsedMode.bind(this);
    this.getCoursesList = this.getCoursesList.bind(this);
  }

  componentDidMount() {
    this.fetchCourses();
  }

  handleConnectClick() {
    window.location.assign('/settings/integrations');
  }

  toggleCollapsedMode() {
    this.setState({ collapsedMode: !this.state.collapsedMode });
  }

  getCoursesList() {
    if (this.state.collapsedMode) {
      return this.state.courses.slice(0, 2);
    } else {
      return this.state.courses;
    }
  }

  fetchCourses() {
    edcastcloud
      .enrolledCourses()
      .then(data => {
        this.setState({ courses: data.currentCourses });
      })
      .catch(err => {
        console.error(`Error in ContinuousLearning.enrolledCourses.func : ${err}`);
      });
  }

  render() {
    let continuousLearningLabel =
      (this.props.team.OrgConfig &&
        this.props.team.OrgConfig.sections &&
        this.props.team.OrgConfig.sections['web/sections/continuousLearning'].label) ||
      'CONTINUOUS LEARNING';
    return (
      <Paper>
        <div className="container-padding vertical-spacing-medium">
          <div className="clearfix">
            <div className="float-left" style={{ maxWidth: '85%' }}>
              <strong>{this.translatedLabel || tr(continuousLearningLabel)}</strong>
              <br />
              <small className="data-not-available-msg">
                {tr(
                  "Let's help you track all your learning in one place. EdCast can connect to many different providers and help you track all the courses you have completed in one place."
                )}
              </small>
              <br />
              <br />
            </div>
          </div>
          {this.state.courses.length == 0 && (
            <div style={{ textAlign: 'center' }}>
              <div className="row">
                <div className="small-3 columns">
                  <div className="text-center">
                    <img src="/i/images/coursera-ed.png" />
                  </div>
                  <div className="text-center">Coursera</div>
                  <div className="text-center">
                    <SecondaryButton
                      label={tr('Connect')}
                      className="settingsIntegrations"
                      onTouchTap={this.handleConnectClick}
                    />
                  </div>
                </div>
                <div className="small-3 columns">
                  <div className="text-center">
                    <img src="/i/images/skillshare-ed.png" />
                  </div>
                  <div className="text-center">Skillshare</div>
                  <div className="text-center">
                    <SecondaryButton
                      label={tr('Connect')}
                      className="settingsIntegrations"
                      onTouchTap={this.handleConnectClick}
                    />
                  </div>
                </div>
                <div className="small-3 columns">
                  <div className="text-center">
                    <img src="/i/images/udemy-ed.png" />
                  </div>
                  <div className="text-center">Udemy</div>
                  <div className="text-center">
                    <SecondaryButton
                      label={tr('Connect')}
                      className="settingsIntegrations"
                      onTouchTap={this.handleConnectClick}
                    />
                  </div>
                </div>
                <div className="small-3 columns">
                  <div className="text-center">
                    <img src="/i/images/pluralsight-ed.png" />
                  </div>
                  <div className="text-center">Pluralsight</div>
                  <div className="text-center">
                    <SecondaryButton
                      label={tr('Connect')}
                      className="settingsIntegrations"
                      onTouchTap={this.handleConnectClick}
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="small-12 columns">
                  <div className="text-center">
                    <br />
                    <small>
                      <a href="/settings/integrations">{tr('Connect Additional Sources')}</a>
                    </small>
                  </div>
                </div>
              </div>
            </div>
          )}
          {this.state.courses.length > 0 &&
            this.getCoursesList().map((course, idx) => {
              return (
                <div key={idx}>
                  <div className="clearfix" style={{ padding: '8px 0px' }}>
                    <div className="float-left" style={{ width: '120px', marginRight: '16px' }}>
                      <img src={course.logoUrl} />
                    </div>
                    <div className="float-left" style={{ width: '78%' }}>
                      <div>
                        <strong>{course.name}</strong>
                      </div>
                      <div>
                        <small dangerouslySetInnerHTML={{ __html: course.description }} />
                      </div>
                      <div className="clearfix" style={{ padding: '8px 0' }}>
                        <div className="float-left">
                          <small>
                            <a href={course.url} target="_blank">
                              {tr('View Details')}
                            </a>
                          </small>
                          <br />
                          <div>
                            <span style={{ textTransform: 'capitalize' }}>
                              <small>{course.completionStatus}</small>
                            </span>
                            {course.duration && (
                              <span>
                                {' '}
                                | {tr('Duration')}: <small>{course.duration}</small>
                              </span>
                            )}
                          </div>
                        </div>
                        <div className="float-right">
                          {course.integrationLogoUrl && (
                            <img
                              src={course.integrationLogoUrl}
                              alt={course.integrationName}
                              style={{ width: '90px' }}
                            />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          {this.state.courses.length > 0 && this.state.collapsedMode && (
            <div style={{ textAlign: 'center' }}>
              <SecondaryButton
                label={tr('Show More')}
                className="viewMore"
                onTouchTap={this.toggleCollapsedMode}
              />
            </div>
          )}
          {this.state.courses.length > 0 && !this.state.collapsedMode && (
            <div style={{ textAlign: 'center' }}>
              <SecondaryButton label={tr('Show Less')} onTouchTap={this.toggleCollapsedMode} />
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

ContinuousLearning.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: Object.assign({}, state.currentUser.toJS()),
    team: Object.assign({}, state.team.toJS())
  };
}

export default connect(mapStoreStateToProps)(ContinuousLearning);
