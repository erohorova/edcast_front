import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import UserScore from '../../home/LeftRailUserScorev2';
import LeftRailClcProgress from '../../home/LeftRailClcProgressv2';
import Interests from '../v3/MyProfile/Interestsv2';
import Expertise from '../v3/MyProfile/Expertisev2';
import { Tabs, Tab } from 'material-ui/Tabs';
import ReactDOM from 'react-dom';
import colors from 'edc-web-sdk/components/colors/index';
import { push } from 'react-router-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { getClcRecords } from 'edc-web-sdk/requests/clc';
import EmptyBlock from '../../discovery/EmptyBlock';
import { getCoursesV2 } from '../../../actions/coursesActions';
import * as walletActions from '../../../actions/walletActions';
import { openWalletPaymentModal } from '../../../actions/modalActions';
import Paper from 'edc-web-sdk/components/Paper';
import FlatButton from 'material-ui/FlatButton';
import Spinner from '../../common/spinner';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import moment from 'moment';
let LocaleCurrency = require('locale-currency');
import { open as openSnackBar } from '../../../actions/snackBarActions';
import {
  initiateCardPurchase,
  completedCardPurchase,
  cancelCardPurchase
} from 'edc-web-sdk/requests/cards.v2';
import 'url-search-params-polyfill';
import * as upshotActions from '../../../actions/upshotActions';

const lightPurp = '#acadc1';

class SkillCoinsContainer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.styles = {
      create: {
        backgroundColor: colors.primary,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      btnLabel: {
        textTransform: 'none',
        lineHeight: '1.875rem'
      },
      disabled: {
        backgroundColor: lightPurp,
        color: 'white',
        boxSizing: 'content-box',
        borderColor: lightPurp,
        borderStyle: 'solid',
        borderWidth: '1px',
        textTransform: 'none',
        margin: '4px',
        lineHeight: '1rem',
        height: '1.875rem'
      },
      spinnerContainer: {
        width: '75px',
        margin: 'auto'
      }
    };

    this.state = {
      selectedValue: null,
      amounts: [100, 500, 1000, 2000],
      showViewMore: false,
      limit: 10,
      offset: 0,
      rechargeData: {},
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false)
    };

    this.handleAmountClick = this.handleAmountClick.bind(this);
    this.openWalletModal = this.openWalletModal.bind(this);
    this.viewMoreTransactions = this.viewMoreTransactions.bind(this);
  }

  componentDidMount() {
    let urlParamsExtractor = new URLSearchParams(window.location.search);

    if (urlParamsExtractor.getAll('paymentId').length > 0) {
      let payPalPayLoad = {
        payment_id: urlParamsExtractor.getAll('paymentId')[0],
        payer_id: urlParamsExtractor.getAll('PayerID')[0],
        token: urlParamsExtractor.getAll('token')[0]
      };
      let payload = {
        paypal_data: payPalPayLoad,
        token: urlParamsExtractor.getAll('app_token')[0],
        gateway: 'paypal'
      };
      completedCardPurchase(payload)
        .then(data => {
          this.props.dispatch(openSnackBar('Recharge successful', true));
        })
        .catch(error => {
          console.error('Error in completed paypal API');
        });
    } else if (
      urlParamsExtractor.getAll('paymentId').length <= 0 &&
      urlParamsExtractor.getAll('token').length > 0 &&
      urlParamsExtractor.getAll('cancelled') == 'true'
    ) {
      let payload = {
        token: urlParamsExtractor.getAll('app_token')[0]
      };
      cancelCardPurchase(payload)
        .then(data => {})
        .catch(err1 => {
          console.log('Error in canceling payment');
        });
    }

    let countryCode = this.props.currentUser.countryCode || 'us';
    let localCurrency = LocaleCurrency.getCurrency(countryCode);
    this.props.dispatch(walletActions.getWalletRecharges(localCurrency));
    let payload = { limit: this.state.limit, offset: this.state.offset };
    this.props.dispatch(walletActions.getWalletTranscations(payload));
  }

  handleAmountClick(id, amount) {
    this.setState({
      selectedValue: this.state.selectedValue == id ? null : id,
      rechargeData: amount
    });
  }

  openWalletModal() {
    let paypalEnabled = this.props.team.paymentGateways && this.props.team.paymentGateways.paypal;
    if (paypalEnabled) {
      let payload = {
        orderable_type: 'recharge',
        price_id: this.state.rechargeData.prices[0].id,
        orderable_id: this.state.rechargeData.id,
        gateway: 'paypal'
      };
      let walletRecharges = (this.props.team && this.props.team.walletRecharges) || [];
      if (this.state.upshotEnabled) {
        upshotActions.sendCustomEvent(window.UPSHOTEVENT['SKILL_COINS_PURCHASE'], {
          totalPrice: parseInt(walletRecharges[this.state.selectedValue].prices[0].amount),
          coinsAdded: this.state.rechargeData.skillcoin
        });
      }
      initiateCardPurchase(payload)
        .then(data => {
          window.open(data.transactionDetails.url, '_self');
        })
        .catch(error => {
          this.props.dispatch(openSnackBar('Something went wrong! Please try again', true));
          console.error('Error in paypal initiate wallet recharge');
        });
    } else {
      // return if nothing is selected
      if (this.state.selectedValue == null) {
        return;
      }
      if (this.state.upshotEnabled) {
        upshotActions.sendCustomEvent(window.UPSHOTEVENT['SKILL_COINS_PURCHASE'], {
          totalPrice: parseInt(this.state.rechargeData.prices[0].amount),
          coinsAdded: this.state.rechargeData.skillcoin
        });
      }

      this.props.dispatch(openWalletPaymentModal(this.state.rechargeData));
      this.setState({
        selectedValue: null,
        rechargeData: {}
      });
    }
  }

  viewMoreTransactions() {
    let offset = this.props.currentUser.walletTransactions.length;
    let payload = { limit: this.state.limit, offset: offset };
    this.props.dispatch(walletActions.getWalletTranscations(payload));
  }

  render() {
    let _this = this;
    let walletTransactions =
      (this.props.currentUser && this.props.currentUser.walletTransactions) || [];
    let walletBalance = (this.props.currentUser && this.props.currentUser.walletBalance) || 0;
    let walletRecharges = (this.props.team && this.props.team.walletRecharges) || [];
    let showWalletLoading =
      (this.props.currentUser && this.props.currentUser.showWalletLoading) || false;
    let walletTransactionsCount =
      (this.props.currentUser && this.props.currentUser.walletTransactionsCount) || 0;
    let skillcoin_image = '/i/images/skillcoin-new.png';

    return (
      <div id="profile-wrapper" className="row full-width-row skill-coin-container">
        <div id="profile" className="">
          <div className="profile-container column small-12 profile-container-full horizontal-spacing-xlarge">
            <div className="horizontal-spacing-xlarge">
              <Paper className="container-padding ">
                <div className="row width-100">
                  <div className="column small-4 flex-center">
                    <span style={{ paddingLeft: '10px' }}>
                      <span>{tr('Your wallet balance is')}: </span>
                      <img
                        width="20"
                        style={{ verticalAlign: 'middle', margin: '0px 5px' }}
                        src={skillcoin_image}
                      />
                      <span
                        style={{ verticalAlign: 'middle', fontSize: '20px', fontWeight: '600' }}
                      >
                        {parseInt(walletBalance)}
                        <br />
                      </span>
                    </span>
                  </div>

                  <div className="column small-6 flex-center">
                    {walletRecharges.map((amount, index) => {
                      return (
                        <button
                          key={index}
                          className={
                            this.state.selectedValue == index ? 'coin-amount active' : 'coin-amount'
                          }
                          onClick={this.handleAmountClick.bind(_this, index, amount)}
                        >
                          {Math.round(Number(amount.skillcoin))}
                        </button>
                      );
                    })}

                    <div className="dollar-amount">
                      {this.state.selectedValue !== null ? (
                        `${tr('Amount to pay')}: ${walletRecharges[this.state.selectedValue]
                          .prices[0] &&
                          walletRecharges[this.state.selectedValue].prices[0]
                            .symbol} ${walletRecharges[this.state.selectedValue].prices[0] &&
                          parseInt(walletRecharges[this.state.selectedValue].prices[0].amount)}`
                      ) : (
                        <span />
                      )}
                    </div>
                  </div>

                  <div className="column small-2 flex-center align-right">
                    <FlatButton
                      label={tr('Add Skillcoins to Wallet')}
                      className="create"
                      labelStyle={this.styles.btnLabel}
                      style={
                        this.state.selectedValue == null ? this.styles.disabled : this.styles.create
                      }
                      disabled={this.state.selectedValue == null}
                      onClick={this.openWalletModal.bind(this)}
                    />
                  </div>
                </div>
              </Paper>
            </div>
            {walletTransactionsCount > 0 && (
              <div>
                <div className="passport-heading" role="heading" aria-level="3">
                  {tr('Transaction History')}
                </div>
                <div className="horizontal-spacing-xlarge">
                  <Paper className="container-padding ">
                    <div className="row width-100">
                      <div className="column small-12">
                        <Table
                          fixedHeader={true}
                          fixedFooter={false}
                          selectable={false}
                          multiSelectable={false}
                        >
                          <TableHeader
                            displaySelectAll={false}
                            adjustForCheckbox={false}
                            enableSelectAll={false}
                          >
                            <TableRow>
                              <TableHeaderColumn colSpan={2}>
                                <strong>{tr('Order')}</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>{tr('Mode')}</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>Skillcoins</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>{tr('Amount')}</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>{tr('Status')}</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>{tr('Skillcoin Balance')}</strong>
                              </TableHeaderColumn>
                              <TableHeaderColumn>
                                <strong>{tr('Date')}</strong>
                              </TableHeaderColumn>
                            </TableRow>
                          </TableHeader>
                          <TableBody
                            displayRowCheckbox={false}
                            deselectOnClickaway={false}
                            showRowHover={false}
                            stripedRows={false}
                          >
                            {walletTransactions.map(row => {
                              let tDate = new Date(row.date);
                              let tDateString = moment(tDate).format('LLL');

                              return (
                                <TableRow>
                                  <TableRowColumn colSpan={2}>
                                    <strong>{tr(row.order)}</strong>
                                    <br /> {tr('Transaction ID')}: {row.id}
                                  </TableRowColumn>
                                  <TableRowColumn>{tr(row.mode)}</TableRowColumn>
                                  <TableRowColumn>
                                    {row.skillcoins
                                      ? (parseInt(row.skillcoins) != 0 &&
                                          (row.type == 'credit' ? '+' : '-')) +
                                        parseInt(row.skillcoins)
                                      : '--'}
                                  </TableRowColumn>
                                  <TableRowColumn>
                                    {row.priceDetails && row.priceDetails.amount
                                      ? parseInt(row.priceDetails.amount)
                                      : '--'}{' '}
                                    {row.priceDetails && row.priceDetails.currency}
                                  </TableRowColumn>
                                  <TableRowColumn>
                                    <span
                                      className={row.status == 'successful' ? 'success' : 'failure'}
                                    >
                                      {tr(row.status)}
                                    </span>
                                  </TableRowColumn>
                                  <TableRowColumn>
                                    {row.balance ? parseInt(row.balance) : '--'}
                                  </TableRowColumn>
                                  <TableRowColumn>{tDateString}</TableRowColumn>
                                </TableRow>
                              );
                            })}
                          </TableBody>
                        </Table>
                        {
                          <div>
                            <div className="text-center">
                              {!showWalletLoading &&
                                walletTransactionsCount > walletTransactions.length && (
                                  <button
                                    className="view-more-v2"
                                    onClick={() => {
                                      this.viewMoreTransactions();
                                    }}
                                  >
                                    {tr('View More')}
                                  </button>
                                )}
                              {showWalletLoading && (
                                <div className="text-center">
                                  <div style={this.styles.spinnerContainer}>
                                    <Spinner />
                                  </div>
                                </div>
                              )}
                            </div>
                          </div>
                        }
                      </div>
                    </div>
                  </Paper>
                </div>
              </div>
            )}
            {walletTransactionsCount <= 0 && (
              <div>
                <p className="passport-heading">{tr('Transaction History')}</p>
                <Paper className="container-padding text-center">
                  <p>{tr('No transactions done yet')}</p>
                </Paper>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

SkillCoinsContainer.propTypes = {
  currentUser: PropTypes.object,
  team: PropTypes.object,
  pathname: PropTypes.string
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    topNav: state.config.toJS(),
    cards: state.cards.toJS(),
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(SkillCoinsContainer);
