import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class Banner extends Component {
  constructor(props, context) {
    super(props, context);

    this.styles = {
      bgkWithOverlay: {
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        padding: '0',
        top: '0',
        bottom: 'calc(50% - 2.5rem)',
        position: 'absolute',
        width: '100%',
        opacity: '0.6'
      }
    };
  }

  render() {
    let coverImage = this.props.coverImage;
    this.styles.bgkWithOverlay.backgroundImage = "url('" + coverImage + "')";
    return <div style={this.styles.bgkWithOverlay} />;
  }
}

Banner.defaultProps = {
  followLinkEnabled: false
};

Banner.propTypes = {
  coverImage: PropTypes.string
};

export default connect()(Banner);
