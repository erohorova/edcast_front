import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import { push } from 'react-router-redux';
import _ from 'lodash';

import IconButton from 'material-ui/IconButton';
import EditIcon from 'material-ui/svg-icons/editor/mode-edit';
import TooltipLabel from '../common/TooltipLabel';

import colors from 'edc-web-sdk/components/colors/index';
import {
  GroupLeaderBadge,
  InfluencerBadge,
  AdminBadge,
  MemberBadge,
  SMEBadge,
  Twitter,
  Email,
  LinkedIn
} from 'edc-web-sdk/components/icons/index';
import AdminBadgev2 from 'edc-web-sdk/components/icons/AdminBadgev2';
import InfluencerBadgev2 from 'edc-web-sdk/components/icons/InfluencerBadgev2';
import MemberBadgev2 from 'edc-web-sdk/components/icons/MemberBadgev2';
import SMEBadgev2 from 'edc-web-sdk/components/icons/SMEBadgev2';
import FollowButton from 'edc-web-sdk/components/FollowButton';
import { getUserCustomFields, getAllCustomFields } from 'edc-web-sdk/requests/users.v2';

import { toggleFollow } from '../../actions/usersActions';
import { openFollowingUserListModal } from '../../actions/modalActions';
import BlurImage from '../common/BlurImage';
import { Permissions } from '../../utils/checkPermissions';

class ProfileHeader extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      iconColor: window.ldclient.variation('additional-profile-info', false)
        ? 'rgba(0, 0, 0, 0.6)'
        : '#454560',
      showEmail:
        !!(this.props.team && this.props.team.config && this.props.team.config.enable_show_email) &&
        this.props.profileShowUserContent,
      user: {},
      additionalInfo: [],
      followPending: false,
      publicPrivateProfileBtn: this.props.publicPrivateProfileBtn,
      additionalProfileInfoEnabled: window.ldclient.variation('additional-profile-info', false),
      upgradeSkillsPassport: !this.props.profileShowUserContent,
      isShowUserJob: window.ldclient.variation('is-show-user-job', false)
    };

    this.skypeIntegration = window.ldclient.variation('skype-integration', false);

    this.styles = {
      badgeStyles: {
        padding: '0 8px',
        width: 40,
        height: 27
      },
      followCount: {
        color: colors.primary
      },
      iconStyle: {
        background: 'rgba(0, 0, 0, 0.6)',
        borderRadius: '50%',
        border: 'solid 0.5px #f0f0f5',
        color: '#ffffff',
        width: 27,
        height: 27
      },
      editIcon: {
        width: 14,
        height: 14,
        color: '#4990e2',
        verticalAlign: 'middle'
      },
      avatarBox: {
        userSelect: 'none',
        height: '10.94rem',
        width: '10.94rem',
        top: '-5.438rem',
        border: 'solid 0.25rem #ffffff',
        position: 'absolute'
      }
    };
  }

  getRoleBadges = (roles, rolesDefaultNames = []) => {
    let allRoles = _.uniqWith(
      _.concat(roles, rolesDefaultNames),
      (a, b) => (a && a.toLowerCase()) === (b && b.toLowerCase())
    );
    let badgeIcons = [];
    allRoles.map((role, index) => {
      let data;
      let roleParam = role ? role.toLowerCase() : '';
      switch (roleParam) {
        case 'member':
          data = [<MemberBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'admin':
          data = [<AdminBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'groupleader':
          data = [<GroupLeaderBadge />, this.state.iconColor];
          break;
        case 'influencer':
          data = [<InfluencerBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        case 'sme':
          data = [<SMEBadgev2 color="#ffffff" />, this.state.iconColor];
          break;
        default:
          break;
      }
      if (data) {
        let indexPos = rolesDefaultNames.indexOf(role);
        let tooltip = roles[indexPos] ? roles[indexPos].toLowerCase() : roleParam;
        badgeIcons.push(
          <IconButton
            key={index}
            tooltip={tr(tooltip)}
            tooltipStyles={this.styles.tooltipStyles}
            disableTouchRipple
            style={this.styles.badgeStyles}
            iconStyle={Object.assign({}, this.styles.iconStyle, { backgroundColor: data[1] })}
          >
            {data[0]}
          </IconButton>
        );
      }
    });
    return badgeIcons;
  };

  toSettingsPage = () => {
    this.props.dispatch(push('/settings'));
  };
  openFollow = (e, type, value) => {
    e.preventDefault();
    if (value) {
      this.props.dispatch(openFollowingUserListModal(type));
    }
  };

  followClickHandler = user => {
    this.setState({
      followPending: true
    });
    this.props
      .dispatch(toggleFollow(user.id, !user.isFollowing))
      .then(() => {
        let newUser = Object.assign({}, user);
        if (newUser.isFollowing) {
          newUser.followersCount--;
          newUser.isFollowing = !user.isFollowing;
        } else {
          newUser.followersCount++;
          newUser.isFollowing = !user.isFollowing;
        }
        this.setState({
          followPending: false,
          user: newUser
        });
      })
      .catch(err => {
        console.error(`Error in ProfileHeader.toggleFollow.func : ${err}`);
      });
  };

  componentWillReceiveProps(nextProps) {
    let user =
      (nextProps.publicProfile &&
        nextProps.currentUser &&
        nextProps.currentUser.publicProfileBasicInfo) ||
      nextProps.currentUser;
    if (this.state.user && this.state.user.email === user.email) {
      this.setState({ user });
      return;
    }
    this.setState(
      prevState => {
        return { user, oldUser: prevState.user };
      },
      () => {
        if (this.state.oldUser && this.state.oldUser.id !== user.id) {
          let userCustomInfo = [];
          getUserCustomFields({ 'user_ids[]': user.id, send_array: true })
            .then(userFieldsResult => {
              let userCustomFields = userFieldsResult[0].customFields;
              getAllCustomFields()
                .then(allFieldsResult => {
                  userCustomInfo = _.filter(userCustomFields, item => {
                    let indexInAllFields = _.findIndex(allFieldsResult.customFields, field => {
                      return field.abbreviation === item.abbreviation;
                    });
                    if (
                      ~indexInAllFields &&
                      allFieldsResult.customFields[indexInAllFields].showInProfile
                    ) {
                      return item;
                    }
                  });
                  this.setState({ additionalInfo: userCustomInfo });
                })
                .catch(err =>
                  console.error('An error had occurred while getAllCustomFields.fnc running ', err)
                );
            })
            .catch(err =>
              console.error('An error had occurred while getUserCustomFields.fnc running ', err)
            );
        }
      }
    );
  }

  render() {
    let user =
      Object.keys(this.state.user).length == 0
        ? (this.props.publicProfile &&
            this.props.currentUser &&
            this.props.currentUser.publicProfileBasicInfo &&
            this.props.currentUser.publicProfileBasicInfo) ||
          this.props.currentUser
        : this.state.user;
    let bgImageURL =
      user.coverImage ||
      (user.coverimages && user.coverimages.banner_url) ||
      (user.coverimages && user.coverimages.banner) ||
      '//dp598loym07sk.cloudfront.net/assets/default_banner_user_image-6ea329e6241a87af91827cecafd26fc1.png';
    let bannerStyle = {
      backgroundImage: `url('${bgImageURL}')`
    };
    let imageUrl =
      user.avatar || user.picture || (user.avatarimages && user.avatarimages.medium) || '';

    let publicProfileUserID =
      this.props.currentUser &&
      this.props.currentUser.publicProfileBasicInfo &&
      this.props.currentUser.publicProfileBasicInfo.id;

    let currentUserID = this.props.currentUser && this.props.currentUser.id;

    let userHandle = this.props && this.props.currentUser && this.props.currentUser.handle;
    let columnHeight = 5;
    let splitedColumns = [];
    if (this.state.additionalInfo && this.state.additionalInfo.length > columnHeight) {
      for (let i = 0; i < this.state.additionalInfo.length; i++) {
        let columnIndex = Math.floor(i / columnHeight);
        if (!Array.isArray(splitedColumns[columnIndex])) {
          splitedColumns[columnIndex] = [];
        }
        splitedColumns[columnIndex].push(this.state.additionalInfo[i]);
      }
    } else {
      splitedColumns[0] = this.state.additionalInfo;
    }

    if (
      this.props.pathname == '/@' + userHandle ||
      this.props.pathname == '/me' ||
      this.props.pathname == '/'
    ) {
      publicProfileUserID = currentUserID;
    }
    return (
      <div className="profile-header">
        {this.state.publicPrivateProfileBtn &&
          currentUserID &&
          currentUserID == publicProfileUserID &&
          (this.props.pathname == '/@' + userHandle ||
            this.props.pathname == '/me' ||
            this.props.pathname == '/') && (
            <div className="public-private-view-tabs">
              <div className="view-tabs-continer">
                <div
                  className={
                    !~this.props.pathname.indexOf('/@')
                      ? 'private-tab active-view-tab'
                      : 'private-tab'
                  }
                  onClick={() => {
                    this.props.dispatch(push('/me'));
                  }}
                >
                  {tr('Private View')}
                </div>
                <div
                  className={
                    ~this.props.pathname.indexOf('/@') ? 'public-tab active-view-tab' : 'public-tab'
                  }
                  onClick={() => {
                    this.props.dispatch(push('/@' + userHandle));
                  }}
                >
                  {tr('Public View')}
                </div>
              </div>
            </div>
          )}
        <div className="profile-header-banner" style={bannerStyle} />
        <div className="profile-header-user-info">
          <div className="profile-header-user-block">
            <div
              className={`user-avatar ${
                this.state.additionalProfileInfoEnabled ? 'user-avatar__v2' : ''
              }`}
            >
              <BlurImage style={this.styles.avatarBox} id={user.id} image={imageUrl} />
            </div>
            <div className="user-main-info">
              <div className="user-name">
                <span>{user.name || user.firstName + ' ' + user.lastName || ''}</span>
              </div>
              {this.state.isShowUserJob && user.profile && user.profile.jobTitle && (
                <div className="user-job-title">
                  <span>{user.profile.jobTitle}</span>
                </div>
              )}
              {user.email && this.state.showEmail && !this.state.additionalProfileInfoEnabled && (
                <div className="user-email">
                  <span>
                    <b>{tr('Email')}</b>: {user.email}
                  </span>
                </div>
              )}
              {splitedColumns[0] &&
                !!splitedColumns[0].length &&
                this.state.additionalProfileInfoEnabled && (
                  <div className="user-info-summary">
                    <div className="user-info__information">
                      {splitedColumns.map((column, index) => {
                        return (
                          <div className="user-info__column" key={index}>
                            {column.map((item, number) => {
                              return (
                                <div className="user-info__row" key={index + number}>
                                  <span className="user-info__label">{item.displayName}: </span>
                                  <span className="user-info__content">{item.value}</span>
                                </div>
                              );
                            })}
                            {index < splitedColumns.length - 1 && <div className="vertical-line" />}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
            </div>
            {user.roles && (
              <div className="user-roles">
                {this.getRoleBadges(user.roles, user.rolesDefaultNames)}
              </div>
            )}

            <div
              className={`right-info-block ${
                this.state.additionalProfileInfoEnabled ? 'right-info-block__updated' : ''
              }`}
            >
              {currentUserID &&
                ((publicProfileUserID && currentUserID == publicProfileUserID) ||
                  this.props.pathname.indexOf('/me') !== -1) && (
                  <div>
                    <div
                      className={`edit-user-data  ${
                        this.state.upgradeSkillsPassport || this.state.additionalProfileInfoEnabled
                          ? 'edit-user-data__updated'
                          : ''
                      }`}
                    >
                      <button
                        aria-label={tr('Edit Profile')}
                        onClick={this.toSettingsPage}
                        className="pointer"
                      >
                        <span tabIndex={-1} className="hideOutline">
                          <EditIcon style={this.styles.editIcon} />
                        </span>
                        {(this.state.upgradeSkillsPassport ||
                          this.state.additionalProfileInfoEnabled) && (
                          <span> {tr('Edit Profile')}</span>
                        )}
                      </button>
                    </div>
                    <div className="user-follow-data">
                      <a
                        tabIndex={0}
                        href="#"
                        className="following-block pointer"
                        onClick={e => this.openFollow(e, 'following', user.followingCount)}
                      >
                        <strong
                          style={this.styles.followCount}
                          className="user-follow-data-item follow-color"
                        >
                          {user.followingCount}
                        </strong>{' '}
                        {tr('Following')}
                      </a>
                      <span className="vertical-line" />
                      <a
                        tabIndex={0}
                        href="#"
                        className="pointer"
                        onClick={e => this.openFollow(e, 'follower', user.followersCount)}
                      >
                        <strong
                          style={this.styles.followCount}
                          className="user-follow-data-item follow-color"
                        >
                          {user.followersCount}
                        </strong>{' '}
                        {tr('Followers')}
                      </a>
                    </div>
                  </div>
                )}
              {currentUserID &&
                currentUserID != publicProfileUserID &&
                this.props.pathname.indexOf('/me') === -1 &&
                !Permissions.has('DISABLE_USER_FOLLOW') && (
                  <TooltipLabel
                    text={user.isSuspended ? 'This action is currently unavailable' : ''}
                    html={true}
                  >
                    <FollowButton
                      following={user.isFollowing}
                      className="follow"
                      label={tr(user.isFollowing ? 'Following' : 'Follow')}
                      hoverLabel={tr(user.isFollowing ? 'Unfollow' : '')}
                      pendingLabel={tr(user.isFollowing ? 'Unfollowing...' : 'Following...')}
                      pending={this.state.followPending}
                      onTouchTap={this.followClickHandler.bind(this, user)}
                      disabled={user.isSuspended}
                    />
                  </TooltipLabel>
                )}
              {currentUserID &&
                currentUserID != publicProfileUserID &&
                this.props.pathname.indexOf('/me') === -1 &&
                this.skypeIntegration &&
                user.email && (
                  <div className="text-center">
                    <button
                      className="skype-call-btn"
                      onClick={() => {
                        window.open(`sip:${user.email}`);
                      }}
                      target="_blank"
                    >
                      Skype Call
                    </button>
                  </div>
                )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProfileHeader.propTypes = {
  publicProfile: PropTypes.bool,
  pathname: PropTypes.any,
  currentUser: PropTypes.object,
  profileShowUserContent: PropTypes.bool,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    pathname: state.routing.locationBeforeTransitions.pathname,
    team: state.team.toJS()
  };
}

export default connect(mapStoreStateToProps)(ProfileHeader);
