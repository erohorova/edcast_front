/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SnackBar from 'edc-web-sdk/components/Snackbar';
//actions
import { close } from '../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class SnackbarContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.newModalAndToast = window.ldclient.variation('new-modal-and-toast', false);
  }
  requestCloseHandler = reason => {
    if (reason !== 'clickaway') {
      this.props.dispatch(close());
      this.props.closeHandler && this.props.closeHandler();
    }
  };

  render() {
    return (
      <SnackBar
        open={this.props.open}
        onRequestClose={this.requestCloseHandler}
        message={<span dangerouslySetInnerHTML={{ __html: tr(this.props.message) }} />}
        className={this.newModalAndToast ? 'snackbar-white' : ''}
      />
    );
  }
}

SnackbarContainer.propTypes = {
  open: PropTypes.bool,
  message: PropTypes.string,
  autoClose: PropTypes.bool,
  closeHandler: PropTypes.func
};

function mapStoreStateToProps(state) {
  return state.snackbar.toJS();
}

export default connect(mapStoreStateToProps)(SnackbarContainer);
