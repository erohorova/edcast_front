import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { recordVisit } from 'edc-web-sdk/requests/analytics';

import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';

import colors from 'edc-web-sdk/components/colors/index';
import Paper from 'edc-web-sdk/components/Paper';
import {
  markAsComplete,
  lockPathwayCard,
  createLeap,
  updateLeap
} from 'edc-web-sdk/requests/cards.v2';
import {
  batchRemoveFromPathway,
  batchAddToPathway,
  customReorderPathwayCards,
  publishPathway
} from 'edc-web-sdk/requests/pathways.v2';

import FlatButton from 'material-ui/FlatButton';

import Card from '../cards/Card';

import {
  openPathwayCreationModal,
  openStatusModal,
  openPathwayOverviewModal,
  openCongratulationModal
} from '../../actions/modalActions';
import {
  deleteTempPathway,
  removeReorderCardIds,
  isPreviewMode,
  saveConsumptionPathway
} from '../../actions/pathwaysActions';
import { getSpecificUserInfo } from '../../actions/currentUserActions';
import { startAssignment } from '../../actions/cardsActions';
import { open as openSnackBar } from '../../actions/snackBarActions';

import MainInfoSmartBite from '../common/MainInfoSmartBite';
import CardViewToggle from '../common/CardViewToggle';
import BreadcrumbV2 from '../common/BreadcrumpV2';
import { postv2, rateCard } from 'edc-web-sdk/requests/cards';
import { fetchPathway } from 'edc-web-sdk/requests/pathways';
import CommentsBlockSmartBite from '../common/CommentsBlockSmartBite';

import getDefaultImage from '../../utils/getDefaultCardImage';

import * as logoType from '../../constants/logoTypes';
import Spinner from '../common/spinner';

const logoObj = logoType.LOGO;

class PathwayDetailsContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      slug: props.routeParams.splat || props.routeParams.slug,
      pathway: {},
      defaultImage: getDefaultImage(this.props.currentUser.id).url,
      reviewStatusLabel: '',
      publishLabel: 'Publish',
      isCompleted: false,
      isUpvoted: false,
      comments: [],
      commentsCount: 0,
      completeStatus: 0,
      open: false,
      votesCount: 0,
      clicked: false,
      startRegime: true,
      editRegime: false,
      continueRegime: false,
      viewRegime:
        props.team.OrgConfig.cardView &&
        props.team.OrgConfig.cardView['web/cardView/pathway'] &&
        !window.ldclient.variation('card-v3', false)
          ? props.team.OrgConfig.cardView['web/cardView/pathway'].defaultValue
          : 'Tile',
      pathwaysCompletionBehaviour:
        props.team.OrgConfig.pathways &&
        props.team.OrgConfig.pathways['web/pathways/pathwaysCompletionBehaviour']
          ? props.team.OrgConfig.pathways['web/pathways/pathwaysCompletionBehaviour'].defaultValue
          : 'manuallyCompletion',
      smartBites: [],
      currentUser: props.currentUser,
      publishError: '',
      feedCardsStyle: window.ldclient.variation('feed-style-card-layout', false),
      isLoaded: false,
      isPathwayCardProject: false,
      modalStandAlone: this.props.modalStandAlone.open,
      pathwayLoading: true,
      unavailable: false,
      isCardV3: window.ldclient.variation('card-v3', false),
      pathwayConsumptionV2: window.ldclient.variation('pathway-consumption-v2', false),
      newModalAndToast: window.ldclient.variation('new-modal-and-toast', false),
      fromConsumptionPathway: false
    };

    this.styles = {
      reviewBtn: {
        color: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '0.0625rem',
        textTransform: 'none',
        margin: '0.25rem',
        lineHeight: '0.75rem',
        minWidth: '9rem',
        height: '2rem'
      },
      tileView: {
        border: 0,
        padding: 0,
        width: 'auto',
        height: 'auto'
      },
      publishBtn: {
        color: colors.white,
        backgroundColor: colors.primary,
        boxSizing: 'content-box',
        borderColor: colors.primary,
        borderStyle: 'solid',
        borderWidth: '0.0625rem',
        textTransform: 'none',
        margin: '0.25rem',
        lineHeight: '0.75rem',
        minWidth: '9rem',
        height: '2rem'
      },
      previewLabel: {
        textTransform: 'none',
        lineHeight: '0.75rem',
        fontSize: '0.75rem'
      },
      dropDownBtn: {
        paddingRight: '0.3rem',
        width: 'auto'
      },
      btnIcons: {
        verticalAlign: 'middle',
        width: '1.5rem',
        height: '1.5rem',
        padding: 0
      },
      tooltipActiveBts: {
        marginTop: -32
      },
      iconStyles: {
        width: '1.5rem',
        height: '1.5rem',
        padding: 0,
        border: 0
      },
      completeBar: {
        backgroundColor: '#f0f0f5',
        borderRadius: '0.125rem',
        height: '0.25rem'
      },
      svgImage: {
        zIndex: 2,
        position: 'relative'
      },
      pathwayActions: {
        paddingLeft: '5rem'
      },
      pricing: {
        fontSize: '0.75rem',
        color: '#454560',
        margin: '10px 0px'
      }
    };
    this.completeClickHandler = this.completeClickHandler.bind(this);
  }

  setCheckedCard = (card, callback) => {
    let checkedCard = card;
    checkedCard.isCompleted =
      card.completionState && card.completionState.toUpperCase() === 'COMPLETED';
    checkedCard.isUpvoted = card.isUpvoted;
    checkedCard.commentsCount = card.commentsCount;
    checkedCard.votesCount = card.votesCount;
    this.setState({ checkedCard }, callback);
  };

  async componentWillMount() {
    let userInfoCallBack = await getSpecificUserInfo(
      [
        'followingChannels',
        'roles',
        'rolesDefaultNames',
        'writableChannels',
        'first_name',
        'last_name'
      ],
      this.props.currentUser
    );
    this.props.dispatch(userInfoCallBack);
    let isOwner =
      this.props.pathway &&
      this.props.pathway.tempPathway &&
      this.props.pathway.tempPathway.author &&
      this.props.pathway.tempPathway.author.id == this.props.currentUser.id;
    if (this.props.pathway && this.props.pathway.isPreviewMode && isOwner) {
      this.previewPathway();
    } else {
      let pathwayConsumption = this.props.pathway && this.props.pathway.consumptionPathway;
      let slug = this.state.slug;
      if (
        this.state.isCardV3 &&
        this.state.pathwayConsumptionV2 &&
        pathwayConsumption &&
        pathwayConsumption.author &&
        pathwayConsumption.slug === slug
      ) {
        this.consumptionPathway();
      } else {
        this.updatePathway(true, this.props.routeParams && this.props.routeParams.cardId);
      }
    }
  }

  componentWillUnmount() {
    if (!this.state.modalStandAlone) {
      if (this.state.previewMode) {
        this.props.dispatch(isPreviewMode(false));
      }
      this.props.dispatch(deleteTempPathway());
    }
  }

  previewPathway = () => {
    this.setState({
      previewMode: true,
      pathway: this.props.pathway.tempPathway,
      isLoaded: true,
      smartBites: this.props.pathway.tempPathway.packCards
    });
  };

  consumptionPathway = () => {
    let isOwner =
      this.props.pathway.consumptionPathway &&
      this.props.pathway.consumptionPathway.author &&
      this.props.pathway.consumptionPathway.author.id == this.state.currentUser.id;
    let isCompleted =
      this.props.pathway.consumptionPathway &&
      this.props.pathway.consumptionPathway.completionState &&
      this.props.pathway.consumptionPathway.completionState.toUpperCase() === 'COMPLETED';
    this.setState(
      {
        fromConsumptionPathway: true,
        pathway: this.props.pathway.consumptionPathway,
        isCompleted: isCompleted,
        isUpvoted:
          this.props.pathway.consumptionPathway && this.props.pathway.consumptionPathway.isUpvoted,
        commentsCount:
          this.props.pathway.consumptionPathway &&
          this.props.pathway.consumptionPathway.commentsCount,
        isOwner: isOwner,
        reviewStatusLabel: this.state.reviewStatusLabel,
        votesCount: this.props.pathway.votesCount,
        previewMode:
          this.props.routeParams.mode && this.props.routeParams.mode === 'preview' && isOwner,
        isLoaded: true,
        pathwayLoading: false,
        averageRating:
          this.props.pathway.consumptionPathway &&
          this.props.pathway.consumptionPathway.averageRating,
        smartBites:
          this.props.pathway.consumptionPathway && this.props.pathway.consumptionPathway.packCards
      },
      () => {
        this.state.smartBites.forEach((item, index) => {
          item.isLocked = this.state.smartBites[index].locked;
          if (item && item.cardType === 'project') {
            this.setState({ isPathwayCardProject: true });
            this.getCompleteCount(item.cardType);
          }
          if (!this.state.isPathwayCardProject) {
            this.getCompleteCount(item.cardType);
          }
        });
        this.checkToCompletedCardDetails();
      }
    );
  };

  checkToCompletedCardDetails = async () => {
    await this.checkToCompletedCard();
    await this.setStartedReview();
    this.updatePathway();
  };

  async setStartedReview() {
    if (this.state.pathway.completionState === null) {
      await startAssignment(this.state.pathway.id).catch(() => {
        console.error(`Error in PathwayDetailsContainer.setStartedReview.func`);
      });
    }
  }

  async checkToCompletedCard() {
    return new Promise((resolve, reject) => {
      let card =
        this.props.pathway &&
        this.props.pathway.consumptionPathwayHistory &&
        this.props.pathway.consumptionPathwayHistory.card;
      if (card) {
        let consumptionPathway = this.props.pathway && this.props.pathway.consumptionPathway;
        let currentCard = consumptionPathway.packCards.find(item => item.id == card.id);
        let isCompleted =
          currentCard &&
          currentCard.completionState &&
          currentCard.completionState.toUpperCase() === 'COMPLETED';
        if (
          !isCompleted &&
          consumptionPathway &&
          consumptionPathway.autoComplete &&
          currentCard &&
          currentCard.id &&
          (currentCard.cardType === 'poll' ? currentCard.hasAttempted : true) &&
          !currentCard.isLocked &&
          !currentCard.isPrivate
        ) {
          markAsComplete(currentCard.id, { state: 'complete' })
            .then(completedCardStatus => {
              resolve(true);
            })
            .catch(error => {
              console.error(
                `Error in PathwayDetailsContainer.checkToCompletedCard.func : ${error}`
              );
              resolve(true);
            });
        } else {
          resolve(true);
        }
      } else {
        resolve(true);
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pathway && nextProps.pathway.tempPathway) {
      this.setState({
        previewMode: true,
        isLoaded: true,
        pathway: nextProps.pathway.tempPathway,
        smartBites: nextProps.pathway.tempPathway.packCards
      });
    } else if (nextProps.pathway && nextProps.pathway.tempPathway === null) {
      this.setState({ previewMode: false });
    }
  }

  updatePathway = (recordAnalytics, isMentioned) => {
    let payload = { is_standalone_page: true };
    fetchPathway(this.state.slug, payload)
      .then(pathway => {
        if (pathway) {
          // Record a visit to this pathway
          if (recordAnalytics) {
            try {
              recordVisit(pathway.id);
            } catch (e) {}
          }
          let isOwner = pathway.author && pathway.author.id == this.state.currentUser.id;
          let isCompleted =
            pathway.completionState && pathway.completionState.toUpperCase() === 'COMPLETED';
          let smartBites = [];
          this.setCheckedCard(pathway);
          this.setState({
            pathway,
            isCompleted: isCompleted,
            isUpvoted: pathway.isUpvoted,
            commentsCount: pathway.commentsCount,
            isOwner: isOwner,
            reviewStatusLabel: this.state.reviewStatusLabel,
            votesCount: pathway.votesCount,
            previewMode:
              this.props.routeParams.mode && this.props.routeParams.mode === 'preview' && isOwner,
            isLoaded: true,
            averageRating: pathway.averageRating
          });

          if (pathway.packCards && pathway.packCards.length) {
            for (let i = 0; i < pathway.packCards.length; i++) {
              if (
                this.props.routeParams &&
                this.props.routeParams.cardId &&
                this.props.routeParams.cardId === pathway.packCards[i].id &&
                isMentioned
              ) {
                this.props.dispatch(
                  openPathwayOverviewModal(
                    pathway,
                    logoObj,
                    this.state.defaultImage,
                    this.cardUpdated.bind(this, this.state.pathway.id),
                    null,
                    pathway.packCards[i].id
                  )
                );
              }
              if (
                pathway.packCards[i] &&
                pathway.packCards[i].message === 'You are not authorized to access this card'
              ) {
                pathway.packCards[i].isPrivate = true;
              }
              smartBites[i] = pathway.packCards[i];
            }
            this.setState({ smartBites }, () => {
              smartBites.forEach((item, index) => {
                item.isLocked = pathway.packCards[index].locked;
                if (item && item.cardType === 'project') {
                  this.setState({ isPathwayCardProject: true });
                  this.getCompleteCount(item.cardType);
                }
                if (!this.state.isPathwayCardProject) {
                  this.getCompleteCount(item.cardType);
                }
              });
            });
          } else {
            this.setState({ smartBites: [] });
          }
          if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
            let pathwayConsumptionArray = Object.assign({}, pathway);
            pathwayConsumptionArray.packCards = smartBites;
            this.props.dispatch(saveConsumptionPathway(pathwayConsumptionArray));
          }
          this.setState({ pathwayLoading: false });
        }
      })
      .catch(err => {
        console.error(`Error in PathwayDetailsContainer.updatePathway.fetchPathway.func : ${err}`);
        this.setState({
          unavailable: true
        });
      });
  };

  startReview = () => {
    this.openModal(this.state.smartBites[0].id ? this.state.smartBites[0].id : 0);
    startAssignment(this.state.pathway.id);
  };

  continueReview = () => {
    let firstUnCompleteCardIndex = 0;
    let firstUnCompleteCard = _.find(this.state.smartBites, (item, index) => {
      firstUnCompleteCardIndex = index;
      return !item.completionState || item.completionState.toUpperCase() !== 'COMPLETED';
    });
    if (firstUnCompleteCard) {
      this.openModal(firstUnCompleteCard.id ? firstUnCompleteCard.id : firstUnCompleteCardIndex);
    }
  };

  regimeStatusClick = () => {
    if (this.state.reviewStatusLabel === 'Start') {
      this.startReview();
    }
    if (this.state.reviewStatusLabel === 'Continue') {
      this.continueReview();
    }
    if (this.state.reviewStatusLabel === 'Edit') {
      this.editPathway();
    }
    if (
      this.state.reviewStatusLabel === 'Mark as Complete' &&
      !this.setState.isPathwayCardProject
    ) {
      this.completeClickHandler();
    }
  };

  completeCallback = (pathway, badge) => {
    this.setState({ reviewStatusLabel: 'Completed', pathway });
    if (
      !this.state.isOwner &&
      !this.state.smartBites.some(
        item =>
          item.isAssessment &&
          item.completionState === 'COMPLETED' &&
          item.attemptedOption &&
          !item.attemptedOption.is_correct
      ) &&
      badge
    ) {
      this.props.dispatch(openCongratulationModal(badge));
    }
  };

  completeMainPathway = toastAutocomplete => {
    markAsComplete(this.state.pathway.id, { state: 'complete' })
      .then(data => {
        let pathway = this.state.pathway;
        pathway.completedPercentage = 100;
        this.setState({ isCompleted: true, completeStatus: 100 });
        let badge = data.userBadge || this.state.pathway.badging;
        if (this.state.newModalAndToast) {
          if (!toastAutocomplete) {
            this.props.dispatch(
              openSnackBar(
                'You have completed this Pathway and can track it in completion history under Me tab',
                true,
                this.completeCallback(pathway, badge)
              )
            );
          } else {
            this.props.dispatch(
              openSnackBar(
                'You have completed this Pathway and can see it under your completed items.',
                true,
                this.completeCallback(pathway, badge)
              )
            );
          }
        } else {
          this.props.dispatch(
            openStatusModal('You have completed this pathway!', () => {
              setTimeout(this.completeCallback(pathway, badge), 0);
            })
          );
        }
      })
      .catch(err => {
        console.error(`Error in PathwayDetailsContainer.markAsComplete.func : ${err}`);
      });
  };

  completeClickHandler = () => {
    if (this.state.pathway.state === 'published' && +this.state.completeStatus === 98) {
      this.completeMainPathway();
    } else if (+this.state.completeStatus === 100) {
      return; // todo uncompleted
    } else {
      let msg =
        this.state.pathway.state === 'published'
          ? 'Please complete all pending cards before marking the Pathway as complete.'
          : 'You should publish the pathway before complete';
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(msg));
      } else {
        this.props.dispatch(openStatusModal(msg));
      }
    }
  };

  openModal = checkedCardId => {
    if (this.state.isCardV3 && this.state.pathwayConsumptionV2) {
      if (checkedCardId === 0) {
        this.props.dispatch(push(`/pathways/${this.state.pathway.slug}/cards/private-card`));
      } else {
        this.props.dispatch(push(`/pathways/${this.state.pathway.slug}/cards/${checkedCardId}`));
      }
    } else {
      this.props.dispatch(
        openPathwayOverviewModal(
          this.state.pathway,
          logoObj,
          this.state.defaultImage,
          this.cardUpdated.bind(this, this.state.pathway.id),
          null,
          checkedCardId
        )
      );
    }
  };

  cardUpdated = id => {
    if (id !== this.state.pathway.id) {
      this.findCheckedCardAtList(id);
    } else {
      this.updatePathway();
    }
  };

  changeViewMode = viewRegime => {
    this.setState({ viewRegime });
  };

  getCompleteCount = cardType => {
    if (this.state.isCompleted) {
      this.setState({
        completeStatus: 100,
        reviewStatusLabel: cardType && cardType == 'project' ? '' : 'Completed'
      });
    } else {
      let completeCount = this.state.smartBites.filter(item => {
        return item.completionState && item.completionState.toUpperCase() === 'COMPLETED';
      }).length;
      let completeStatus = Math.round((completeCount * 100) / this.state.pathway.packCards.length);
      let fullComplete = completeStatus === 100;
      if (fullComplete && this.state.pathwaysCompletionBehaviour === 'manuallyCompletion') {
        completeStatus = 98;
      }
      if (fullComplete && this.state.pathwaysCompletionBehaviour === 'autoCompletion') {
        if (
          this.state.pathway.state === 'published' &&
          completeStatus === 100 &&
          !(
            this.state.pathway.completionState &&
            this.state.pathway.completionState.toUpperCase() === 'COMPLETED'
          )
        ) {
          this.completeMainPathway(true);
        }
      }
      let reviewStatusLabel = fullComplete
        ? cardType && cardType === 'project' && fullComplete
          ? 'Completed'
          : 'Mark as Complete'
        : !completeStatus && this.state.pathway.completionState === null
        ? 'Start'
        : 'Continue';
      this.setState({ completeStatus, reviewStatusLabel: reviewStatusLabel });
    }
  };

  findCheckedCardAtList = () => {
    if (this.state.reviewStatusLabel === 'Start') {
      startAssignment(this.state.pathway.id);
    }
    let payload = { is_standalone_page: true };
    let smartBites = [];
    fetchPathway(this.state.slug, payload)
      .then(pathway => {
        if (pathway && pathway.packCards && pathway.packCards.length) {
          for (let i = 0; i < pathway.packCards.length; i++) {
            if (
              pathway.packCards[i] &&
              pathway.packCards[i].message === 'You are not authorized to access this card'
            ) {
              pathway.packCards[i].isPrivate = true;
            }
            smartBites[i] = pathway.packCards[i];
          }
          this.setState({ smartBites }, () => {
            smartBites.forEach((item, index) => {
              item.isLocked = pathway.packCards[index].locked;
              if (item && item.cardType === 'project') {
                this.setState({ isPathwayCardProject: true });
                this.getCompleteCount(item.cardType);
              }
              if (!this.state.isPathwayCardProject) {
                this.getCompleteCount(item.cardType);
              }
            });
          });
        } else {
          this.setState({ smartBites: [] });
        }
      })
      .catch(err => {
        console.error(
          `Error in PathwayDetailsContainer.findCheckedCardAtList.fetchPathway.func : ${err}`
        );
      });
  };

  publishClickHandler = () => {
    if (this.state.pathway.packCards && !this.state.pathway.packCards.length) {
      return this.setState({
        publishError: 'Please add cards to the collection before publishing!'
      });
    }
    if (this.state.smartBites && !this.state.smartBites.length) {
      return this.setState({
        publishError: 'Please add cards to the collection before publishing!'
      });
    } else {
      this.setState({ publishLabel: 'Publishing ...', clicked: true });
      this.savePathway(data => {
        if (data) {
          if (this.state.pathway.arrToLeap.length) {
            this.state.pathway.arrToLeap.forEach(leapObj => {
              let payload = {
                correct_id: leapObj.correctId,
                wrong_id: leapObj.wrongId,
                pathway_id: data.id
              };
              if (leapObj.leapId) {
                updateLeap(leapObj.cardId, leapObj.leapId, payload);
              } else {
                createLeap(leapObj.cardId, payload);
              }
            });
          }
          if (this.state.pathway && this.state.pathway.publishedAt) {
            this.afterPublish();
          } else {
            publishPathway(data.id)
              .then(() => {
                if (this.state.previewMode) {
                  this.props.dispatch(isPreviewMode(false));
                }
                if (
                  this.props.pathway.reorderCardIds &&
                  !!this.props.pathway.reorderCardIds.length
                ) {
                  customReorderPathwayCards(
                    this.state.pathway.id,
                    this.props.pathway.reorderCardIds
                  )
                    .then(() => {
                      this.props.dispatch(removeReorderCardIds());
                    })
                    .catch(err => {
                      console.error(
                        `Error in PathwayDetailsContainer.customReorderPathwayCards.func : ${err}`
                      );
                    });
                }
                if (this.props.cardUpdated) {
                  this.props.cardUpdated(data);
                }
                setTimeout(() => {
                  if (this.state.newModalAndToast) {
                    this.props.dispatch(
                      openSnackBar('Your Pathway is saved! Go to Me/Content to finish', () => {
                        this.openContentTab();
                      })
                    );
                  } else {
                    this.props.dispatch(
                      openStatusModal('Your Pathway is saved! Go to Me/Content to finish', () => {
                        this.openContentTab();
                      })
                    );
                  }
                }, 500);
              })
              .catch(err => {
                console.error(`Error in PathwayDetailsContainer.publishPathway.func : ${err}`);
              });
          }
        }
      });
    }
  };

  openContentTab = () => {
    this.props.dispatch(deleteTempPathway());
    this.props.dispatch(push('/me/content'));
  };

  savePathway = async callback => {
    let sharedtoGroups = [];
    this.state.pathway.teams.map(team => {
      sharedtoGroups.push(team.id);
    });
    let payload = {
      message:
        this.state.pathway.message || (this.state.mainPathway && this.state.mainPathway.title),
      title: this.state.pathway.title,
      state: this.state.pathway ? this.state.pathway.state : 'draft',
      card_type: 'pack',
      card_subtype: 'simple',
      auto_complete: this.state.completeMethod === 'automatically',
      team_ids: sharedtoGroups
    };
    if (
      this.state.pathway.badging &&
      !!this.state.pathway.badging.id &&
      !!this.state.pathway.badging.title
    ) {
      payload['card_badging_attributes'] = {
        title: this.state.pathway.badging.title,
        badge_id: this.state.pathway.badging.id
      };
    }
    if (this.state.pathway.tags || !!this.state.pathway.tags.length) {
      payload.topics = this.state.pathway.tags;
    }
    if (this.state.pathway.channelIds) {
      payload.channel_ids = this.state.pathway.channelIds;
    }
    payload.filestack = this.state.pathway.filestack;
    payload.provider_image = this.state.pathway.providerImage;
    payload.provider = this.state.pathway.provider;
    if (this.state.pathway.eclDurationMetadata) {
      payload.duration = this.state.pathway.eclDurationMetadata.calculated_duration_display;
    }
    if (this.state.pathway.cardsToRemove && this.state.pathway.cardsToRemove.length) {
      await batchRemoveFromPathway(this.state.pathway.id, this.state.pathway.cardsToRemove);
    }
    this.setState({ clicked: true });
    postv2({ card: payload }, this.state.pathway && this.state.pathway.id)
      .then(data => {
        if (data && data.embargoErr) {
          this.setState({
            publishLabel: 'Publish',
            clicked: false,
            embargoErr: true
          });
          this.props.dispatch(
            openSnackBar(
              'Sorry, the content you are trying to post is from unapproved website or words.',
              true
            )
          );
          return callback();
        } else {
          if (this.state.selectedBia) {
            rateCard(data.id, { level: this.state.selectedBia });
          }
          if (!!this.state.smartBites.length) {
            let ids = _.uniq(this.state.smartBites.map(card => card.id || card.cardId));
            let uniqCards = _.uniqBy(this.state.smartBites, 'id');
            if (
              this.props.pathway &&
              this.props.pathway.tempPathway &&
              this.props.pathway.tempPathway.packCards
            ) {
              let currentIds = this.props.pathway.tempPathway.pathwayPackCards.map(
                item => item.card_id + ''
              );
              let idsToAdd = _.difference(ids, currentIds);
              if (!idsToAdd.length) {
                ids = [];
                if (Object.keys(this.props.pathway.tempPathway.cardIdsToLock).length) {
                  this.lockCards(callback, data);
                } else {
                  return callback(data);
                }
              } else {
                ids = idsToAdd;
              }
            }
            uniqCards.map(card => {
              if (card.newSkillLevel && card.newSkillLevel !== card.skillLevel) {
                rateCard(card.id, { level: card.newSkillLevel });
              }
            });
            if (ids.length) {
              return batchAddToPathway(data.id, ids, 'Card').then(() => {
                if (Object.keys(this.props.pathway.tempPathway.cardIdsToLock).length) {
                  this.lockCards(callback, data);
                }
                return callback(data);
              });
            }
          } else {
            return callback(data);
          }
        }
      })
      .catch(err => {
        console.error(`Error in PathwayDetailsContainer.postv2.func : ${err}`);
        return callback();
      });
  };

  lockCards = (callback, data) => {
    let cardIdsToLock = this.props.pathway.tempPathway.cardIdsToLock;
    let arrLockPromises = [];
    for (let prop in cardIdsToLock) {
      if (!~this.state.smartBites.findIndex(item => item.id === prop)) {
        continue;
      }
      let payloadForLockPathwayCard = {
        pathway_id: data.id,
        locked: `${cardIdsToLock[prop]}`
      };
      arrLockPromises.push(lockPathwayCard(prop, payloadForLockPathwayCard));
    }
    Promise.all(arrLockPromises)
      .then(() => {
        return callback(data);
      })
      .catch(err => {
        console.error(`Error in PathwayDetailsContainer.arrLockPromises.func : ${err}`);
      });
  };

  afterPublish = () => {
    if (this.state.previewMode) {
      this.props.dispatch(isPreviewMode(false));
    }
    this.props.routeParams.mode = undefined;
    this.props.dispatch(deleteTempPathway());
    this.props.dispatch(push(`/pathways/${this.state.pathway.slug}`));
    if (this.state.previewMode) {
      this.props.dispatch(isPreviewMode(false));
    }
    if (this.props.pathway.reorderCardIds && !!this.props.pathway.reorderCardIds.length) {
      customReorderPathwayCards(this.state.pathway.id, this.props.pathway.reorderCardIds)
        .then(() => {
          this.props.dispatch(removeReorderCardIds());
        })
        .catch(err => {
          console.error(
            `Error in PathwayDetailsContainer.afterPublish.customReorderPathwayCards.func : ${err}`
          );
        });
    }
    this.updatePathway();
    this.setState({ publishLabel: 'Publish', clicked: false });
    setTimeout(() => {
      if (this.state.newModalAndToast) {
        this.props.dispatch(openSnackBar(`Your Pathway is saved successfully!`));
      } else {
        this.props.dispatch(openStatusModal(`Your Pathway is saved successfully!`));
      }
    }, 500);
  };

  editPathway = () => {
    this.setState({ publishError: '', embargoErr: false });
    this.props.dispatch(
      openPathwayCreationModal(this.state.pathway, previewMode => {
        if (previewMode === 'preview') {
          this.props.routeParams.mode = 'preview';
          this.props.dispatch(push(`/pathways/${this.state.pathway.slug}/preview`));
        } else {
          this.props.routeParams.mode = undefined;
          this.props.dispatch(push(`/pathways/${this.state.pathway.slug}`));
        }
        this.updatePathway();
      })
    );
  };

  removeCardFromList = id => {
    let smartBites = this.state.smartBites;
    smartBites.forEach((item, index) => {
      if (item.id === id) {
        smartBites.splice(index, 1);
      }
    });
    this.setState({ smartBites });
  };

  render() {
    if (this.state.unavailable) {
      return (
        <div className="row container-padding">
          <Paper className="small-12">
            <div className="container-padding vertical-spacing-large text-center">
              <p>{tr('This content does not exist or has been deleted.')}</p>
            </div>
          </Paper>
        </div>
      );
    }

    return (
      <div>
        {this.state.pathwayLoading ? (
          <div>
            <div className="text-center">
              <Spinner />
            </div>
          </div>
        ) : (
          <div
            id="pathwayDetails"
            className={`smartbite-details__container page-with-medium-width ${
              this.state.isCardV3 ? 'smartbite-details__container_v3' : ''
            }`}
          >
            {!this.state.previewMode && (
              <BreadcrumbV2
                isStandaloneModal={this.props.isStandaloneModal}
                cardUpdated={this.props.cardUpdated}
                isConsumptionPage={
                  this.state.fromConsumptionPathway &&
                  this.props.pathway &&
                  this.props.pathway.consumptionPathwayHistoryUrl
                    ? this.props.pathway.consumptionPathwayHistoryUrl
                    : this.state.fromConsumptionPathway
                    ? '/'
                    : ''
                }
              />
            )}
            {(this.state.pathway.id || this.state.previewMode) && (
              <MainInfoSmartBite
                smartBite={this.state.pathway}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                reviewStatusLabel={this.state.reviewStatusLabel}
                isOwner={this.state.isOwner}
                completeStatus={this.state.completeStatus}
                regimeStatusClick={this.regimeStatusClick}
                type="pathway"
                isPathwayPaid={this.state.pathway.isPaid}
                dueAt={this.state.pathway.dueAt}
                isCardV3={this.state.isCardV3}
              />
            )}
            {this.state.feedCardsStyle && (
              <CardViewToggle
                viewRegime={this.state.viewRegime}
                isPathway={true}
                changeViewMode={this.changeViewMode}
              />
            )}
            <div className="smartbites-block">
              <div className="custom-card-container">
                {!this.state.feedCardsStyle ||
                this.state.viewRegime === 'Tile' ||
                this.state.isCardV3 ? (
                  <div className="five-card-column vertical-spacing-medium">
                    {this.state.smartBites.map((card, index) => {
                      let smartBitesBeforeCurrent =
                        this.state.smartBites && this.state.smartBites.slice(0, index);
                      let isShowLockedCardContent =
                        index === 0 ||
                        (smartBitesBeforeCurrent &&
                          smartBitesBeforeCurrent.length &&
                          smartBitesBeforeCurrent.every(
                            item =>
                              item.completionState &&
                              item.completionState.toUpperCase() === 'COMPLETED'
                          ));
                      card['isPathwayCompleted'] = this.state.isCompleted;
                      return (
                        <Card
                          isCardV3={this.state.isCardV3}
                          card={card}
                          showIndex={true}
                          key={index}
                          pathwayEditor={false}
                          pathwayDetails={this.state.pathway}
                          index={index + 1}
                          author={card.author}
                          dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                          startDate={
                            card.startDate || (card.assignment && card.assignment.startDate)
                          }
                          user={this.state.currentUser}
                          pathwayChecking={
                            card.completionState &&
                            card.completionState.toUpperCase() === 'COMPLETED'
                          }
                          hideComplete={this.findCheckedCardAtList}
                          closeCardModal={this.findCheckedCardAtList.bind(this)}
                          isPathwayCard={true}
                          moreCards={true}
                          removeCardFromList={this.removeCardFromList}
                          isShowLockedCardContent={isShowLockedCardContent}
                          isPrivate={card.isPrivate}
                          cardSplat={this.props.routeParams.splat}
                          isStandaloneModal={this.props.isStandaloneModal}
                          isPartOfPathway={true}
                          type={this.state.viewRegime}
                        />
                      );
                    })}
                  </div>
                ) : (
                  this.state.smartBites.map((card, index) => {
                    let smartBitesBeforeCurrent =
                      this.state.smartBites && this.state.smartBites.slice(0, index);
                    let isShowLockedCardContent =
                      index === 0 ||
                      (smartBitesBeforeCurrent &&
                        smartBitesBeforeCurrent.length &&
                        smartBitesBeforeCurrent.every(
                          item =>
                            item.completionState &&
                            item.completionState.toUpperCase() === 'COMPLETED'
                        ));
                    card['isPathwayCompleted'] = this.state.isCompleted;
                    return (
                      <Card
                        isCardV3={this.state.isCardV3}
                        card={card}
                        index={index + 1}
                        dueAt={card.dueAt || (card.assignment && card.assignment.dueAt)}
                        startDate={card.startDate || (card.assignment && card.assignment.startDate)}
                        author={card.author}
                        pathwayDetails={this.state.pathway}
                        user={this.state.currentUser}
                        showControls={true}
                        pathwayView={true}
                        pathwayChecking={
                          card.completionState && card.completionState.toUpperCase() === 'COMPLETED'
                        }
                        hideComplete={this.findCheckedCardAtList.bind(this)}
                        closeCardModal={this.findCheckedCardAtList.bind(this)}
                        isPathwayCard={true}
                        key={index}
                        removeCardFromList={this.removeCardFromList}
                        isShowLockedCardContent={isShowLockedCardContent}
                        isPrivate={card.isPrivate}
                        cardSplat={this.props.routeParams.splat}
                        type="List"
                        isPartOfPathway={true}
                      />
                    );
                  })
                )}
              </div>
            </div>
            {this.state.previewMode && (
              <div className="action-buttons text-center">
                <div className="error-text">{tr(this.state.publishError)}</div>
                <FlatButton
                  label={tr('Back to Edit')}
                  className="back"
                  disabled={this.state.clicked}
                  style={this.styles.reviewBtn}
                  labelStyle={this.styles.previewLabel}
                  onTouchTap={this.editPathway}
                />

                {!!this.state.smartBites.length && (
                  <FlatButton
                    label={tr(this.state.publishLabel)}
                    className="publish"
                    disabled={this.state.clicked || this.state.embargoErr}
                    onTouchTap={this.publishClickHandler}
                    style={this.styles.publishBtn}
                    labelStyle={this.styles.previewLabel}
                  />
                )}
              </div>
            )}
            {this.state.pathway.id && (
              <CommentsBlockSmartBite
                smartBite={this.state.pathway}
                checkedCard={this.state.checkedCard}
                isLoaded={this.state.isLoaded}
                previewMode={this.state.previewMode}
                isUpvoted={this.state.isUpvoted}
                votesCount={this.state.votesCount}
                isOwner={this.state.isOwner}
                cardUpdated={this.cardUpdated.bind(this, this.state.pathway.id)}
                isCompleted={this.state.isCompleted}
                isPathwayCardProject={this.state.isPathwayCardProject}
                type={'PathwayStandAlone'}
                isCardV3={this.state.isCardV3}
                completeClickHandler={this.completeClickHandler.bind(this)}
                completeStatus={this.state.completeStatus}
                reviewStatusLabel={this.state.reviewStatusLabel}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

PathwayDetailsContainer.propTypes = {
  currentUser: PropTypes.object,
  routeParams: PropTypes.object,
  pathway: PropTypes.object,
  modalStandAlone: PropTypes.object,
  isStandaloneModal: PropTypes.bool,
  cardUpdated: PropTypes.func,
  team: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    team: state.team.toJS(),
    currentUser: state.currentUser.toJS(),
    pathway: state.pathways ? state.pathways.toJS() : {},
    modalStandAlone: state.modalStandAlone.toJS()
  };
}

export default connect(mapStoreStateToProps)(PathwayDetailsContainer);
