import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import BreadcrumbV2 from '../common/BreadcrumpV2';
import PrevArrow from 'edc-web-sdk/components/icons/PrevArrow';
import NextArrow from 'edc-web-sdk/components/icons/NextArrow';
import CardConsumptionContainer from '../consumption/CardConsumptionContainer';
import { fetchPathwayInsideJourney } from 'edc-web-sdk/requests/journeys';
import Paper from 'edc-web-sdk/components/Paper';
import { tr } from 'edc-web-sdk/helpers/translations';
import find from 'lodash/find';
import unescape from 'lodash/unescape';
import Spinner from '../common/spinner';
import getCardType from '../../utils/getCardType';
import { startAssignment } from '../../actions/cardsActions';
import { markAsComplete } from 'edc-web-sdk/requests/cards.v2';
import { recordVisit } from 'edc-web-sdk/requests/analytics';
import { push } from 'react-router-redux';
import {
  saveConsumptionPathway,
  removeConsumptionPathwayHistory
} from '../../actions/pathwaysActions';
import { Permissions } from '../../utils/checkPermissions';

class PathwayConsumptionContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      unavailable: false,
      pathway: props.pathway && props.pathway.get('consumptionPathway'),
      slug: props.routeParams.slug,
      cardId: props.routeParams.cardId,
      smartBites:
        (props.pathway &&
          props.pathway.get('consumptionPathway') &&
          props.pathway.get('consumptionPathway').packCards) ||
        [],
      checkedCard: null,
      currentIndex: 0,
      loadChecked: true,
      fetchCard: true,
      autoComplete: true,
      pathwayStarted: false,
      isInProgressToComplete: false,
      showMarkAsComplete: window.ldclient.variation('show-markAsComplete-on-visit', true)
    };

    this.styles = {
      leftArrow: {
        float: 'left',
        width: '24px',
        height: '48px',
        padding: 0,
        border: 0,
        color: '#000000'
      },
      leftButton: {
        float: 'left',
        cursor: 'pointer'
      },
      rightArrow: {
        float: 'right',
        width: '24px',
        height: '48px',
        padding: 0,
        border: 0,
        color: '#000000'
      },
      rightButton: {
        float: 'right',
        cursor: 'pointer'
      }
    };
  }

  componentDidMount() {
    if (this.state.slug && this.state.cardId) {
      if (this.props.pathway && this.props.pathway.get('consumptionPathwayHistory')) {
        this.props.dispatch(removeConsumptionPathwayHistory());
      }
      this.fetchPathwayDetails();
    } else {
      this.setState({
        loadChecked: false,
        unavailable: true
      });
    }
  }

  fetchPathwayDetails = () => {
    let payload = {
      fields:
        'id,title,message,slug,card_type,auto_complete,completion_state,author,pack_cards(id,title,message,card_type,card_subtype,slug,state,is_official,provider,provider_image,readable_card_type,share_url,average_rating,skill_level,filestack,votes_count,comments_count,published_at,prices,is_assigned,is_bookmarked,hidden,is_public,is_paid,mark_feature_disabled_for_source,completion_state,is_upvoted,all_ratings,is_reported,card_metadatum,ecl_duration_metadata,author,is_clone,locked,auto_complete,language,channel_ids,non_curated_channel_ids,channels,completed_percentage,duration,file_resources,paid_by_user,resource,show_restrict,tags,teams,teams_permitted,users_permitted,users_with_access,quiz,ecl_metadata,rank,payment_enabled,user_rating,voters,project_id,video_stream,assigner,shared_by,channels(id,label),teams(id,label)), pack_cards'
    };
    let pathwayData = this.props.pathway && this.props.pathway.get('consumptionPathway');
    fetchPathwayInsideJourney(this.state.slug, payload)
      .then(pathway => {
        if (pathway && pathway.packCards && pathway.packCards.length) {
          pathway.packCards.map(smartBite => {
            if (smartBite.resource === null) {
              delete smartBite.resource;
            }
            if (smartBite.videoStream === null) {
              delete smartBite.videoStream;
            }
            return smartBite;
          });
        }
        this.setState(
          {
            pathway: pathway,
            autoComplete: pathway.autoComplete !== undefined ? pathway.autoComplete : true
          },
          () => {
            if (pathwayData) {
              pathwayData.packCards = pathway.packCards;
            } else {
              pathwayData = pathway;
            }
            this.loadPackCards();
          }
        );
      })
      .catch(err => {
        console.error(`Error in PathwayConsumptionContainer.fetchPathway.func : ${err}`);
        this.setState({
          loadChecked: false,
          unavailable: true
        });
      });
  };

  loadPackCards = () => {
    if (this.state.pathway.packCards && this.state.pathway.packCards.length) {
      let smartBites = [];
      let countCard = 0;
      for (let i = 0; i < this.state.pathway.packCards.length; i++) {
        countCard++;
        if (
          this.state.pathway.packCards[i].message === 'You are not authorized to access this card'
        ) {
          this.state.pathway.packCards[i].isPrivate = true;
        }
        smartBites[i] = this.state.pathway.packCards[i];
        if (countCard === this.state.pathway.packCards.length) {
          smartBites.forEach((item, index) => {
            item.isLocked = this.state.pathway.packCards[index].locked;
          });
        }
        this.setState({ smartBites });
        if (countCard === this.state.pathway.packCards.length) {
          let pathwayDetails = this.props.pathway && this.props.pathway.get('consumptionPathway');
          if (!pathwayDetails) {
            pathwayDetails = Object.assign({}, this.state.pathway);
          }
          pathwayDetails.packCards = smartBites;
          this.props.dispatch(saveConsumptionPathway(pathwayDetails));
          let checkedCardId = this.state.cardId;
          if (checkedCardId) {
            let checkedCurrentCardId =
              this.state.cardId === 'private-card' ? undefined : this.state.cardId;
            let findCard = find(smartBites, el => el.id == checkedCurrentCardId);
            let index;
            //To check public card
            if (findCard) {
              index = smartBites.indexOf(findCard);
              this.setState({ checkedCardId: null }, this.chooseCard.bind(this, findCard, index));
            } else {
              this.setState({
                loadChecked: false,
                unavailable: true
              });
            }
          }
        }
        if (i === this.state.pathway.packCards.length - 1) {
          this.setState({ loadingPackCards: false });
        }
      }
    }
  };

  chooseCard = (card, i, e) => {
    if (card.cardType === 'journey') {
      window.open('/journey' + `/${card.slug}`, '_blank');
      return;
    }
    if (e && e.target && e.target.name === 'marked-link') {
      return;
    }
    this.setState({
      currentIndex: i,
      loadChecked: false,
      checkedCard: card
    });
  };

  arrowClick = async direction => {
    this.setStartedReview();
    await this.checkToCompletedCard();
    let oldIndex = this.state.currentIndex;
    let pathwayDetails =
      this.props.pathway &&
      this.props.pathway.get('consumptionPathway') &&
      this.props.pathway.get('consumptionPathway').packCards;
    let smartBites = pathwayDetails || this.state.smartBites;
    let card = smartBites[oldIndex];
    let cardType = getCardType(card);
    if (
      cardType === 'QUIZ' &&
      card.attemptedOption &&
      card.leaps &&
      card.leaps.inPathways &&
      direction === 1
    ) {
      let correctAnswer = card.quizQuestionOptions.find(obj => {
        return obj.isCorrect === true;
      });
      let selected = card.attemptedOption && card.attemptedOption.id;
      let result = correctAnswer && selected === correctAnswer.id;
      let inPathwayLeap = card.leaps.inPathways.find(
        item => item.pathwayId == this.state.pathway.id
      );
      let nextCardId = result ? inPathwayLeap.correctId : inPathwayLeap.wrongId;
      if (nextCardId) {
        let newIndex = smartBites.indexOf(smartBites.find(item => item.id == nextCardId));
        this.changeCompleteStatus(smartBites[oldIndex].id, oldIndex);
        this.setState(
          {
            fetchCard: false
          },
          () => {
            this.setState(
              {
                currentIndex: newIndex,
                checkedCard: smartBites[newIndex],
                fetchCard: true
              },
              () => {
                if (smartBites[newIndex].id === undefined) {
                  this.props.dispatch(push(`/pathways/${this.state.slug}/cards/private-card`));
                } else {
                  this.props.dispatch(
                    push(`/pathways/${this.state.slug}/cards/${smartBites[newIndex].id}`)
                  );
                  try {
                    recordVisit(this.state.checkedCard.id);
                  } catch (e) {}
                }
              }
            );
          }
        );
      }
    } else {
      let newIndex = this.state.currentIndex + direction;
      if (newIndex >= 0 && newIndex <= smartBites.length - 1) {
        this.changeCompleteStatus(smartBites[oldIndex].id, oldIndex);
        this.setState(
          {
            fetchCard: false
          },
          () => {
            this.setState(
              {
                currentIndex: newIndex,
                checkedCard: smartBites[newIndex],
                fetchCard: true
              },
              () => {
                if (this.state.smartBites[newIndex].id === undefined) {
                  this.props.dispatch(push(`/pathways/${this.state.slug}/cards/private-card`));
                } else {
                  this.props.dispatch(
                    push(`/pathways/${this.state.slug}/cards/${this.state.smartBites[newIndex].id}`)
                  );
                  try {
                    recordVisit(this.state.checkedCard.id);
                  } catch (e) {}
                }
              }
            );
          }
        );
      }
    }
  };

  updatePathwayConsumptionDetails = data => {
    let pathway = this.props.pathway && this.props.pathway.get('consumptionPathway');
    if (pathway && pathway.packCards && pathway.packCards.length) {
      pathway.packCards.forEach((cards, index) => {
        if (cards.id === data.id) {
          pathway.packCards[index] = data;
        }
      });
      this.props.dispatch(saveConsumptionPathway(pathway));
    }
  };

  setStartedReview = () => {
    if (this.state.pathway.completionState === null && !this.state.pathwayStarted) {
      startAssignment(this.state.pathway.id)
        .then(userContentCompletion => {
          if (userContentCompletion && userContentCompletion.state === 'started') {
            this.setState({
              pathwayStarted: true
            });
          }
        })
        .catch(err => {
          console.error(`Error in PathwayConsumptionContainer.setStartedReview.func : ${err}`);
        });
    }
  };

  async checkToCompletedCard() {
    return new Promise((resolve, reject) => {
      let pathway = this.props.pathway && this.props.pathway.get('consumptionPathway');
      let checkedCardId = this.state.checkedCard && this.state.checkedCard.id;
      let currentCard = pathway.packCards.find(item => item.id == checkedCardId);
      let isCompleted =
        currentCard &&
        currentCard.completionState &&
        currentCard.completionState.toUpperCase() === 'COMPLETED';
      let cardType = getCardType(currentCard);
      let isLinkCard =
        !this.state.showMarkAsComplete &&
        currentCard &&
        currentCard.resource &&
        (currentCard.resource.url ||
          currentCard.resource.description ||
          currentCard.resource.fileUrl) &&
        currentCard.resource.type !== 'Video' &&
        ((currentCard.readableCardType &&
          currentCard.readableCardType.toUpperCase() === 'ARTICLE') ||
          cardType === 'ARTICLE');
      if (
        !isCompleted &&
        this.state.autoComplete &&
        currentCard &&
        (currentCard.cardType === 'poll' ? currentCard.hasAttempted : true) &&
        !currentCard.isLocked &&
        !currentCard.isPrivate &&
        !isLinkCard
      ) {
        try {
          this.setState({ isInProgressToComplete: true }, async () => {
            await markAsComplete(currentCard.id, { state: 'complete' })
              .then(completedCardStatus => {
                this.setState({
                  completedCardStatus: completedCardStatus,
                  isInProgressToComplete: false
                });
                resolve(true);
              })
              .catch(error => {
                this.setState({
                  completedCardStatus: null,
                  isInProgressToComplete: false
                });
                console.error(
                  `Error in PathwayConsumptionContainer.checkToCompletedCard.func : ${error}`
                );
                resolve(true);
              });
          });
        } catch (error) {
          this.setState({
            completedCardStatus: null,
            isInProgressToComplete: false
          });
          console.error(
            `Error in PathwayConsumptionContainer.checkToCompletedCard.func : ${error}`
          );
          resolve(true);
        }
      } else {
        resolve(true);
      }
    });
  }

  changeCompleteStatus = (id, i) => {
    let pathwayDetails = this.props.pathway && this.props.pathway.get('consumptionPathway');
    let smartBites = pathwayDetails ? pathwayDetails.packCards : this.state.smartBites;
    if (
      this.state.completedCardStatus &&
      this.state.completedCardStatus.completableId == id &&
      !this.state.checkedCard.locked
    ) {
      smartBites[i].isCompleted = true;
      smartBites[i].completionState = 'COMPLETED';
    }
    this.setState({ smartBites }, this.updatePathwayConsumptionDetails(smartBites[i]));
  };

  render() {
    if (this.state.unavailable) {
      return (
        <div className="row container-padding">
          <Paper className="small-12">
            <div className="container-padding vertical-spacing-large text-center">
              <p>{tr('This content does not exist or has been deleted.')}</p>
            </div>
          </Paper>
        </div>
      );
    } else if (this.state.loadChecked || this.state.isInProgressToComplete) {
      return (
        <div className="text-center pathway-overview-spinner">
          <Spinner />
        </div>
      );
    }
    let pathway = this.state.pathway;
    let pathwayDetails =
      this.props.pathway &&
      this.props.pathway.get('consumptionPathway') &&
      this.props.pathway.get('consumptionPathway').packCards;
    let smartBites = pathwayDetails || this.state.smartBites;
    let smartBitesBeforeCurrent = smartBites && smartBites.slice(0, this.state.currentIndex);
    let isShowLockedCardContent =
      this.state.currentIndex === 0 ||
      (smartBitesBeforeCurrent &&
        smartBitesBeforeCurrent.length &&
        smartBitesBeforeCurrent.every(
          item => item.completionState && item.completionState.toUpperCase() === 'COMPLETED'
        ));
    return (
      <div className="stand-alone">
        <div className="row consumption-container-padding">
          <div className="pathway-consumption-title">
            <span className="pathway-consumption-title-heading pathway-consumption-ellipsis">
              {pathway ? unescape(pathway.title || pathway.message) : ''}
            </span>
            <span className="pathway-consumption-count pathway-consumption-ellipsis">
              {this.state.currentIndex + 1}/{this.state.smartBites.length}
            </span>
          </div>
          <div className="flex pathway-consumption-main-container">
            <div className="pathway-consumption-arrow left-consumption-arrow">
              {this.state.currentIndex !== 0 && (
                <div style={this.styles.leftButton} onClick={this.arrowClick.bind(null, -1)}>
                  <PrevArrow style={this.styles.leftArrow} />
                </div>
              )}
            </div>
            <div className="insightV2 pathway-consumption-container">
              <BreadcrumbV2
                isPathwayConsumption={
                  this.props.pathway && this.props.pathway.get('consumptionPathwayHistory')
                }
              />
              {this.state.fetchCard && (
                <CardConsumptionContainer
                  card={this.state.checkedCard}
                  author={this.state.checkedCard && this.state.checkedCard.author}
                  pathwayData={pathway}
                  showComment={
                    Permissions['enabled'] !== undefined && Permissions.has('CREATE_COMMENT')
                  }
                  isShowLockedCardContent={isShowLockedCardContent}
                  autoComplete={this.state.autoComplete}
                />
              )}
            </div>
            <div className="pathway-consumption-arrow right-consumption-arrow">
              {this.state.currentIndex + 1 !== this.state.smartBites.length && (
                <div style={this.styles.rightButton} onClick={this.arrowClick.bind(null, 1)}>
                  <NextArrow style={this.styles.rightArrow} />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PathwayConsumptionContainer.propTypes = {
  pathway: PropTypes.object,
  routeParams: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    pathway: state.pathways ? state.pathways : {}
  };
}

export default connect(mapStoreStateToProps)(PathwayConsumptionContainer);
