import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class ForgotPasswordContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.footerOptions =
      this.props.team && this.props.team.config && this.props.team.config.footer_options;
    this.showPasswordPolicy = this.showPasswordPolicy.bind(this);
  }

  showPasswordPolicy(newValue) {
    this.setState({
      showPasswordPolicy: newValue
    });
  }

  privacyPolicy() {
    let link = this.footerOptions && this.footerOptions.privacy_policy;
    let showPrivacyPolicy = link != undefined && link != '';
    link = showPrivacyPolicy ? link : 'https://www.edcast.com/corp/privacy-policy/';

    return link;
  }

  termsOfService() {
    let link = this.footerOptions && this.footerOptions.terms_of_service;
    let showTermsOfService = link != undefined && link != '';
    link = showTermsOfService ? link : 'https://www.edcast.com/corp/terms-of-service/';

    return link;
  }

  render() {
    let config = this.props.team.config;
    let enableEdcastLogo =
      config &&
      ((typeof config.enable_edcast_logo == 'undefined' && !config.enable_edcast_logo) ||
        !!config.enable_edcast_logo);
    return (
      <div id="forgot-password">
        <div id="header">
          <div className="logo">
            <img src={this.props.team.coBrandingLogo} />
          </div>
        </div>
        <div id="forgot-body">{this.props.children}</div>
        <div id="footerv2" className="row">
          <div className="footer-left small-6 columns">
            <ul>
              <li className="">
                <a target="_blank" href={this.privacyPolicy()}>
                  Privacy Policy &nbsp; &nbsp;|
                </a>
              </li>
              <li className="">
                <a target="_blank" href={this.termsOfService()}>
                  &nbsp; &nbsp;Terms of Service
                </a>
              </li>
            </ul>
          </div>
          <div className="footer-right small-6 columns">
            {enableEdcastLogo && <img src="/i/images/powered_by_edcast_image.png" />}
          </div>
        </div>
      </div>
    );
  }
}

ForgotPasswordContainer.propTypes = {
  team: PropTypes.object,
  children: PropTypes.any
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(ForgotPasswordContainer);
