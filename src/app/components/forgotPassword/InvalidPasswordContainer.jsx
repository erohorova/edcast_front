import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';

class InvalidPasswordContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {};
  }

  render() {
    return (
      <div>
        <Paper className="content-paper invalid-token">
          <div className="forgot-heading">Password Reset</div>
          <div className="page-text margin-top-10 color-error">
            Your password reset email has expired. Please tap below to request a new one.
          </div>
          <div className="margin-top-20 small-text">
            <a
              onTouchTap={() => {
                this.props.dispatch(push('/forgot_password'));
              }}
            >
              <button className="button-green">RESET PASSWORD</button>
            </a>
          </div>
        </Paper>
      </div>
    );
  }
}

InvalidPasswordContainer.propTypes = {
  team: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(InvalidPasswordContainer);
