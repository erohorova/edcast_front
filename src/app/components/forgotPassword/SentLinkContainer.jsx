import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';

class SentLinkContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {};
  }

  render() {
    let email = this.props.team.fromEmailAddress;
    return (
      <div>
        <Paper className="content-paper">
          <div className="forgot-heading">Email Sent!</div>
          <div className="page-text margin-top-10">
            If a valid email address has been provided, a reset link shall be sent to you. If you do
            not see the email in a few minutes, please check your spam filter for a message from{' '}
            <b>{email}</b>
          </div>
          <div className="margin-top-10 small-text">
            Unsure if that email address was correct?{' '}
            <a
              className="back-link"
              onTouchTap={() => {
                this.props.dispatch(push('/log_in'));
              }}
            >
              Back to Login page?
            </a>
          </div>
        </Paper>
      </div>
    );
  }
}

SentLinkContainer.propTypes = {
  location: PropTypes.object,
  team: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(SentLinkContainer);
