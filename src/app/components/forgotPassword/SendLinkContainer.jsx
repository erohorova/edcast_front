import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import { push } from 'react-router-redux';
import { sendResetLink } from 'edc-web-sdk/requests/users.v2';
import * as upshotActions from '../../actions/upshotActions';
import { tr } from 'edc-web-sdk/helpers/translations';

class SendLinkContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      upshotEnabled: window.ldclient.variation('upshot-ai-integration', false),
      buttonMsg: tr('GET RESET LINK')
    };

    this.sendResetLink = this.sendResetLink.bind(this);
  }

  sendResetLink(e) {
    e.preventDefault();

    this.setState({
      buttonMsg: tr('Please wait...')
    });

    let payload = {
      user: {
        email: this.refs.email.value,
        organization_id: this.props.team.orgId
      }
    };

    sendResetLink(payload)
      .then(data => {
        if (this.state.upshotEnabled) {
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['FORGOT_PASSWORD'], {
            status: 'Success',
            email: this.refs.email.value,
            event: 'Forgot Password'
          });
        }
        this.props.dispatch(push(`/forgot_password/sent?email=${this.refs.email.value}`));
        /*eslint handle-callback-err: "off"*/
      })
      .catch(err => {
        if (this.state.upshotEnabled) {
          upshotActions.sendCustomEvent(window.UPSHOTEVENT['FORGOT_PASSWORD'], {
            status: 'Failure',
            email: this.refs.email.value,
            event: 'Forgot Password'
          });
        }
        this.props.dispatch(push(`/forgot_password/sent?email=${this.refs.email.value}`));
      });
  }

  render() {
    return (
      <div>
        <Paper className="content-paper">
          <div className="forgot-heading">{tr('Password Reset')}</div>
          <div className="page-text margin-top-10">
            {tr('To reset your password, enter your email address.')}
          </div>
          <div className="form-container margin-top-20">
            <form onSubmit={this.sendResetLink.bind(this)}>
              <div className="form-group-container">
                <div className="input-label">{tr('Email')}</div>
                <div className="input-element">
                  <input
                    required
                    type="email"
                    className="input-text"
                    ref="email"
                    placeholder="abc@example.com"
                  />
                </div>
              </div>
              <button type="submit" className="button-green">
                {this.state.buttonMsg}
              </button>
            </form>
            <div className="margin-top-10">
              <a
                className="back-link"
                onTouchTap={() => {
                  this.props.dispatch(push('/log_in'));
                }}
              >
                {tr('Back to Login page?')}
              </a>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

SendLinkContainer.propTypes = {
  team: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(SendLinkContainer);
