import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';

class ChangedPasswordContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {};
    this.styles = {};
  }

  render() {
    return (
      <div>
        <Paper className="content-paper margin-bottom-20">
          <div className="page-text margin-top-10 color-green">
            Your password has been updated Successfully
          </div>
        </Paper>
        <Paper className="content-paper">
          <div className="forgot-heading margin-bottom-20">
            Do you have the EdCast app on this Device?
          </div>

          <div className="page-text margin-bottom-20">
            If you are using a device with EdCast app installed, we can save you the step of typing
            out your password. Use the button below to automatically sign yourself in.
          </div>

          <a
            href={`/api/v2/users/magic_link_login?token=${
              this.props.params.token
            }&sent_from_forgot_password=true`}
          >
            {' '}
            <button className="button-green">LAUNCH EDCAST APP</button>
          </a>
        </Paper>
      </div>
    );
  }
}

ChangedPasswordContainer.propTypes = {
  team: PropTypes.object,
  params: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(ChangedPasswordContainer);
