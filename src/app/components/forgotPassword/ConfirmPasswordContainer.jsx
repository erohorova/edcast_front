import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'edc-web-sdk/components/Paper';
import {
  requestLogin,
  changePasswordInput,
  changeEmailInput
} from '../../actions/currentUserActions';
import { push } from 'react-router-redux';
import { saveNewPassword } from 'edc-web-sdk/requests/users.v2';
import JSEncrypt from 'jsencrypt/bin/jsencrypt.min';

class ConfirmPasswordContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showPasswordPolicy: false,
      buttonMsg: 'RESET',
      errorMsg: null,
      encryption_payload:
        this.props.team && this.props.team.config && this.props.team.config.encryption_payload
    };

    this.saveNewPassword = this.saveNewPassword.bind(this);
  }

  saveNewPassword(e) {
    e.preventDefault();

    this.setState({
      buttonMsg: 'RESETTING...'
    });

    let password = this.refs.password;
    let confirm_password = this.refs.confirm_password;

    if (password.value !== confirm_password.value) {
      this.setState({
        errorMsg: 'Hmm, the passwords you entered didn’t match. Please try again!',
        buttonMsg: 'RESET'
      });
      return;
    }

    let payload = {
      user: {
        reset_password_token: this.props.params && this.props.params.token,
        password: password.value,
        password_confirmation: confirm_password.value
      }
    };
    // Start Encryption of password in payload
    if (this.state.encryption_payload && JSEncrypt) {
      let encrypt = new JSEncrypt();
      encrypt.setPublicKey(window.process.env.JsPublic);
      payload.user = Object.entries(payload.user).reduce(
        (a, [k, v]) => ((a[k] = encrypt.encrypt(v)), a),
        {}
      );
    }
    // End encryption of password in payload
    saveNewPassword(payload)
      .then(data => {
        if (data.message && data.message.indexOf('token is invalid') > -1) {
          this.props.dispatch(push('/forgot_password/invalid'));
        }

        if (data.message) {
          this.setState({
            showPasswordPolicy: true,
            errorMsg: false,
            buttonMsg: 'RESET'
          });
        }

        if (data.email) {
          if (
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
              navigator.userAgent
            )
          ) {
            this.props.dispatch(push(`/forgot_password/changed/${data.jwtToken}`));
          } else {
            this.props
              .dispatch(requestLogin(data.email, payload.user.password))
              .then(user => {
                if (user['consumer_redirect_uri']) {
                  window.location.href = user['consumer_redirect_uri'];
                } else {
                  let webSessionTimeout =
                    this.props.team &&
                    this.props.team.config &&
                    this.props.team.config.web_session_timeout;
                  webSessionTimeout = webSessionTimeout
                    ? webSessionTimeout * 60 * 1000 + Date.now()
                    : 0;
                  localStorage.setItem('webSessionTimeout', webSessionTimeout);
                  if (window.opener) {
                    window.opener.postMessage(user, '*');
                  }

                  this.props.dispatch(push('/'));
                }
              })
              .catch(err => {
                console.error(`Error in ConfirmPasswordContainer.requestLogin.func : ${err}`);
              });
          }
        }
      })
      .catch(err => {
        console.error(`Error in ConfirmPasswordContainer.saveNewPassword.func : ${err}`);
      });
  }

  render() {
    return (
      <div>
        {(this.state.showPasswordPolicy || this.state.errorMsg) && (
          <Paper className="content-paper margin-bottom-20">
            <div className="password-policy">
              {!this.state.errorMsg && (
                <ul>
                  <li>Use at least 8 characters.</li>
                  <li>At least one letter should be capital.</li>
                  <li>Include at least a number and symbol (#?!@$%^&amp;*-=).</li>
                  <li>Password is case sensitive.</li>
                </ul>
              )}
              {this.state.errorMsg && (
                <ul>
                  <li>{this.state.errorMsg}</li>
                </ul>
              )}
            </div>
          </Paper>
        )}
        <Paper className="content-paper">
          <div className="forgot-heading">Password Reset</div>
          <div className="page-text margin-top-10">Enter new password for your account</div>
          <div className="form-container margin-top-20">
            <form onSubmit={this.saveNewPassword.bind(this)}>
              <div className="form-group-container">
                <div className="input-label">New Password</div>
                <div className="input-element">
                  <input
                    required
                    type="password"
                    className="input-text"
                    ref="password"
                    placeholder="*********"
                  />
                </div>
              </div>

              <div className="form-group-container">
                <div className="input-label">Confirm New Password</div>
                <div className="input-element">
                  <input
                    required
                    type="password"
                    className="input-text"
                    ref="confirm_password"
                    placeholder="*********"
                  />
                </div>
              </div>

              <button className="button-green">{this.state.buttonMsg}</button>
              <div className="margin-top-10">
                <a
                  className="back-link"
                  onTouchTap={() => {
                    this.props.dispatch(push('/log_in'));
                  }}
                >
                  Back to Login page?
                </a>
              </div>
            </form>
          </div>
        </Paper>
      </div>
    );
  }
}

ConfirmPasswordContainer.propTypes = {
  team: PropTypes.object,
  params: PropTypes.object
};

export default connect(state => {
  return {
    team: state.team.toJS()
  };
})(ConfirmPasswordContainer);
