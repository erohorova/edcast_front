import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SharedWith from '../../components/shareContent/SharedWith';
import Spinner from '../common/spinner';
import * as shareContentAction from '../../actions/shareContentActions';
import _ from 'lodash';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

class Individuals extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loadMore: false
    };
    this.pending = false;
  }

  componentDidMount() {
    document.getElementById('sc-individual-list-content') &&
      document
        .getElementById('sc-individual-list-content')
        .addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document
      .getElementById('sc-individual-list-content')
      .removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = _.throttle(
    e => {
      if (this.pending) return;
      if (
        this.props.modal &&
        this.props.modal.filter &&
        this.props.modal.filter.length &&
        this.props.modal.filter.some(item => item.id > -1)
      ) {
        let users =
          this.props.users || (this.props.shareContent && this.props.shareContent.users) || [];
        if (
          !this.props.isAllUsers &&
          e.target.clientHeight + Math.ceil(e.target.scrollTop) >= e.target.scrollHeight
        ) {
          this.pending = true;
          this.setState({ loadMore: true }, () => {
            this.pending = false;
          });
          let cb = function() {
            this.setState({ loadMore: false });
            this.pending = false;
          }.bind(this);
          this.props.applyFilterForIndividual(users.length, cb);
        }
      } else {
        let users = (this.props.shareContent && this.props.shareContent.users) || [];
        let userTotal = (this.props.shareContent && this.props.shareContent.totalUsers) || 0;
        let payload = { limit: 15, offset: users.length };
        if (
          userTotal > users.length &&
          e.target.clientHeight + Math.ceil(e.target.scrollTop) >= e.target.scrollHeight
        ) {
          this.pending = true;
          this.setState({ loadMore: true });
          this.props
            .dispatch(shareContentAction._fetchUsers(payload))
            .then(() => {
              this.setState({ loadMore: false });
              this.pending = false;
            })
            .catch(() => {
              this.setState({ loadMore: false });
              this.pending = false;
            });
        }
      }
    },
    500,
    { leading: false }
  );

  _removeSearchedUser = (user, card, isSearch) => {
    this.props.dispatch(shareContentAction._removeUser(user, card, isSearch));
  };

  render() {
    let users =
      (this.props.modal &&
      this.props.modal.filter &&
      this.props.modal.filter.length &&
      this.props.modal.filter.some(item => item.id > -1)
        ? this.props.users
        : this.props.shareContent && this.props.shareContent.users) || [];
    let content = this.props.shareContent[this.props.shareContent.currentCard];
    let searchedUsers = (this.props.shareContent && this.props.shareContent.searchedUsers) || [];
    let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
    let contentReload = !!this.props.shareContent.contentLoaded;

    return (
      <div>
        <div id="sc-individual-list-content">
          {contentReload &&
            content &&
            users.map((user, index) => {
              if (this.props.currentUser.id != user.id) {
                return <SharedWith user={user} key={user.id + '-' + index} {...this.props} />;
              }
            })}
          {(this.state.loadMore ||
            (this.props.shareContent.usersLoading === undefined
              ? !contentReload || !content || true
              : !contentReload || !content || this.props.shareContent.usersLoading)) && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
        </div>
        {!!searchedUsers.length && (
          <div id="searched-users">
            {searchedUsers.map(user => {
              return (
                <div className="clip-container" key={user.id}>
                  {user.avatarimages && user.avatarimages.tiny && (
                    <img
                      className="user-avatar"
                      src={(user.avatarimages && user.avatarimages.tiny) || defaultUserImage}
                    />
                  )}
                  <span className="clip-main-text">{user.name}</span>
                  {user.handle && <span className="clip-handle-text">({user.handle})</span>}
                  <IconButton
                    aria-label="close"
                    className="clip-remove-btn"
                    onClick={() => {
                      this._removeSearchedUser(
                        user,
                        this.props.shareContent[this.props.card.id],
                        true
                      );
                    }}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

Individuals.propTypes = {
  currentUser: PropTypes.any,
  shareContent: PropTypes.any,
  card: PropTypes.any,
  isAllUsers: PropTypes.bool,
  modal: PropTypes.object,
  applyFilterForIndividual: PropTypes.func,
  users: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(Individuals);
