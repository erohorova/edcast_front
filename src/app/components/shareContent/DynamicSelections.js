import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'edc-web-sdk/components/Paper';
import SearchIcon from 'edc-web-sdk/components/icons/Search';
import colors from 'edc-web-sdk/components/colors/index';
import Popover, { PopoverAnimationVertical } from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import { connect } from 'react-redux';
import { getUsersFields, getFieldValues } from 'edc-web-sdk/requests/users.v2';
import { updateDynamicSelectionFilters } from '../../actions/modalActions';
import _ from 'lodash';
import { tr } from 'edc-web-sdk/helpers/translations';

class DynamicSelections extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      filterArr: [{ id: -1, isMain: true, type: 'Add Filter +', options: [] }],
      anchorEl: {},
      filterAdd: false,
      currentFilter: {},
      filterList: [],
      searchText: '',
      filteredOptions: []
    };
    this.styles = {
      removeSpan: {
        border: '1px solid #fff',
        paddingLeft: '0.1875rem',
        paddingRight: '0.1875rem',
        display: 'inline-block',
        marginLeft: '0.3125rem'
      },
      searchContainer: {
        border: '1px solid #d6d6e1'
      },
      searchIcon: {
        position: 'absolute',
        bottom: '0.3125rem',
        verticalAlign: 'middle',
        width: '1.5rem',
        height: '1.5rem',
        fill: colors.silverSand
      },
      hr: {
        margin: '0.625rem 0',
        border: 'solid 1px #f0f0f5'
      },
      addButton: {
        width: 'auto',
        minHeight: '1.5rem',
        backgroundColor: '#acadc1',
        fontSize: '0.75rem',
        color: '#fff',
        borderRadius: '0.125rem',
        padding: '0 0.5rem',
        marginRight: '0.5rem',
        marginBottom: '0.3125rem'
      },
      menuItem: {
        textSize: '0.75rem',
        color: '#6f708b',
        fontWeight: '300',
        paddingLeft: '0 !important',
        lineHeight: '1.75',
        minHeight: '0 !important'
      },
      menuStyle: {
        textSize: '0.75rem',
        color: '#6f708b',
        fontWeight: '300',
        lineHeight: '1.75',
        minHeight: '0 !important'
      },
      checkbox: {
        width: '0.625rem',
        borderRadius: '0.125rem',
        height: '0.625rem',
        marginRight: '0.625rem',
        marginTop: '0.25rem'
      },
      label: {
        fontSize: '0.75rem',
        fontWeight: '300',
        color: '#6f708b'
      },
      label__checked: {
        fontWeight: '600',
        fontSize: '0.75rem',
        color: '#6f708b'
      },
      span: {
        fontWeight: '600',
        color: '#6f708b',
        marginTop: '0.625rem'
      },
      cancel: {
        width: '3.6875rem',
        height: '1.75rem',
        color: '#acadc1',
        textAlign: 'center',
        borderRadius: '0.125rem',
        fontSize: '0.75rem',
        marginRight: '0.75rem',
        border: '1px solid #acadc1'
      },
      apply: {
        width: '3.6875rem',
        height: '1.75rem',
        color: '#fff',
        textAlign: 'center',
        borderRadius: '0.125rem',
        backgroundColor: '#6f708b',
        fontSize: '0.75rem'
      },
      options: {
        cursor: 'pointer',
        fontSize: '0.625rem',
        fontWeight: '300',
        height: '1.25rem',
        width: 'auto',
        borderRadius: '0.125rem',
        backgroundColor: '#d6d6e1',
        display: 'inline-block',
        margin: '0.6875rem 0.5rem 0.3125rem 0',
        padding: '0.1875rem 0.5rem'
      },
      fieldList: {
        maxHeight: '125px',
        overflow: 'auto'
      }
    };
    this.target = props.target;
  }

  componentDidMount() {
    getUsersFields()
      .then(data => {
        let filterList = [];
        let excludeList = ['last_name', 'handle', 'status'];
        if (data.fields) {
          filterList = data.fields
            .filter(el => !~excludeList.indexOf(el))
            .map((item, index) => {
              return { type: item === 'first_name' ? 'name' : item, options: [], id: index };
            });
        }
        let filterArr = (this.props.filterContent &&
          !!this.props.filterContent.length &&
          this.props.filterContent) ||
          (this.props.modal && this.props.modal.filter) || [
            { id: -1, isMain: true, type: 'Add Filter +', options: [] }
          ];
        this.setState({ filterList, filterArr });
      })
      .catch(err => {
        console.error(`Error in DynamicSelections.getUsersFields.func : ${err}`);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modal && nextProps.modal.filter) {
      this.setState({ filterArr: nextProps.modal.filter });
    }
  }

  getPreviewList = () => {
    this.props.getPreviewList(this.state.filterArr);
  };

  handleTouchTap = (id, filter, event) => {
    event.preventDefault();
    this.setState({
      filterAdd: true,
      currentFilter: filter,
      filteredOptions: filter.options || [],
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState(
      {
        filterAdd: false,
        currentFilter: {},
        filteredOptions: []
      },
      () => {
        !this.target
          ? this.props.dispatch(updateDynamicSelectionFilters(this.state.filterArr))
          : this.getPreviewList();
      }
    );
  };

  cancelFilter = () => {
    this.setState({ searchText: '' });
    let filterArr = this.state.filterArr;
    let getIndexCurrentFilter = _.findIndex(
      filterArr,
      filter => +filter.id === +this.state.currentFilter.id
    );
    if (getIndexCurrentFilter > -1) {
      filterArr.splice(getIndexCurrentFilter, 1);
      if (filterArr.length === 1 && filterArr[0].isMain) {
        filterArr[0].type = 'Add Filter +';
      }
      this.setState({
        filterAdd: false,
        filterArr,
        currentFilter: {},
        filteredOptions: []
      });
    }
    !this.target
      ? this.props.dispatch(updateDynamicSelectionFilters(this.state.filterArr))
      : this.getPreviewList();
  };

  applyFilter = () => {
    let filterArr = this.state.filterArr.filter(
      filter =>
        filter.type === '+' ||
        filter.type === 'Add Filter +' ||
        (filter.options && filter.options.some(option => !!option.checked))
    );

    this.setState(
      {
        searchText: '',
        filterAdd: false,
        currentFilter: {},
        filteredOptions: [],
        filterArr
      },
      () => {
        !this.target
          ? this.props.dispatch(updateDynamicSelectionFilters(this.state.filterArr))
          : this.getPreviewList();
      }
    );
  };

  toggleItems = id => {
    let currentFilter = this.state.currentFilter;
    let index = _.findIndex(this.state.currentFilter.options, el => +el.id === +id);
    currentFilter.options[index].checked = !currentFilter.options[index].checked;
    this.setState({ currentFilter });
  };

  clearOptions = () => {
    let currentFilter = this.state.currentFilter;
    currentFilter.options.forEach(el => {
      el.checked = false;
    });
    this.setState({ currentFilter });
  };

  addFilter = filter => {
    let filterArr = this.state.filterArr;
    if (filterArr.length === 1) {
      filterArr[0].type = '+';
    }
    if (!filter.options.length) {
      getFieldValues(filter.type)
        .then(data => {
          let options = [];
          if (data && data.values) {
            options = data.values.map((item, index) => {
              return { option: item, checked: false, id: index };
            });
          }
          filter.options = options;
          filterArr.splice(filterArr.length - 1, 0, filter);
          this.setState({
            filterArr,
            currentFilter: filter,
            filteredOptions: filter.options || []
          });
        })
        .catch(err => {
          console.error(`Error in DynamicSelections.getFieldValues.func : ${err}`);
        });
    } else {
      filterArr.splice(filterArr.length - 1, 0, filter);
      this.setState({
        filterArr,
        currentFilter: filter,
        filteredOptions: filter.options || []
      });
    }
  };

  filterSearch = () => {
    let val = this._inputFilter.value.trim();
    let currentFilter = this.state.currentFilter;
    let filteredOptions =
      (currentFilter.options &&
        currentFilter.options.filter(
          option => ~option.option.toLowerCase().indexOf(val.toLowerCase())
        )) ||
      [];
    this.setState({ searchText: val, filteredOptions });
  };

  removeClick = (currentFilter, e) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({ currentFilter }, () => {
      this.cancelFilter();
    });
  };

  toggleAll = () => {
    this.setState(prevState => {
      let filteredOptions = prevState.filteredOptions;
      let isCheckedAll = filteredOptions.every(elem => elem.checked);
      filteredOptions.forEach((item, index) => {
        filteredOptions[index].checked = !isCheckedAll;
      });
      return { filteredOptions };
    });
  };

  render() {
    let initialFilter = this.state.currentFilter && this.state.currentFilter.isMain;
    let isCheckedAll = this.state.filteredOptions.every(item => item.checked);
    return (
      <div
        className={`dynamic-container ${
          this.props.forIndividuals ? 'dynamic-container_only-filter' : ''
        }`}
      >
        <div>
          {this.state.filterArr.map((filter, index) => {
            let checkedFilters = (filter.options && filter.options.filter(el => el.checked)) || [];
            return (
              <button
                onClick={this.handleTouchTap.bind(this, index, filter)}
                style={this.styles.addButton}
              >
                {filter.type}
                {filter.type !== '+' && filter.type !== 'Add Filter +' && (
                  <span>
                    {checkedFilters.length ? ':' : ''}
                    {checkedFilters.map((el, idx) => (
                      <span key={el.id}>
                        {' '}
                        {idx ? ', ' : ''}
                        {el.option}{' '}
                      </span>
                    ))}
                    {this.state.filterAdd ? (
                      ''
                    ) : (
                      <span
                        style={this.styles.removeSpan}
                        onClick={this.removeClick.bind(this, filter)}
                      >
                        x
                      </span>
                    )}
                  </span>
                )}
              </button>
            );
          })}
        </div>
        <Popover
          open={this.state.filterAdd}
          style={{ marginTop: '0.625rem', overflowY: 'auto' }}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
          animation={PopoverAnimationVertical}
        >
          {initialFilter && (
            <Menu style={{ padding: '0.5rem' }}>
              {this.state.filterList
                .filter(
                  filter => !~_.findIndex(this.state.filterArr, elem => +elem.id === +filter.id)
                )
                .map(el => (
                  <MenuItem
                    key={`filter-options-${el.id}`}
                    style={this.styles.menuStyle}
                    onClick={() => {
                      this.addFilter(el);
                    }}
                    menuItemStyle={this.styles.menuItem}
                    disabled={_.includes(this.state.filterArr.map(elem => elem.id), el.id)}
                    primaryText={el.type}
                  />
                ))}
            </Menu>
          )}
          {this.state.currentFilter && !this.state.currentFilter.isMain && (
            <div className="current_filter">
              <div className="my-team__search-input-block" style={this.styles.searchContainer}>
                <input
                  placeholder={tr('Search')}
                  onChange={this.filterSearch}
                  type="text"
                  ref={node => (this._inputFilter = node)}
                />
                <SearchIcon style={this.styles.searchIcon} onClick={this.filterSearch} />
              </div>
              <Paper style={this.styles.fieldList}>
                {!!this.state.filteredOptions.length && (
                  <Checkbox
                    label={tr(`All`)}
                    aria-label={tr(`All`)}
                    onCheck={this.toggleAll}
                    className="checkbox"
                    iconStyle={this.styles.checkbox}
                    checked={isCheckedAll}
                    labelStyle={isCheckedAll ? this.styles.label__checked : this.styles.label}
                  />
                )}
                {this.state.filteredOptions.map((el, index) => (
                  <div key={`all-filter-options-${el.id + index}`}>
                    <Checkbox
                      label={tr(`${el.option}`)}
                      aria-label={tr(`${el.option}`)}
                      onCheck={() => {
                        this.toggleItems(el.id);
                      }}
                      className="checkbox"
                      iconStyle={this.styles.checkbox}
                      checked={el.checked}
                      labelStyle={el.checked ? this.styles.label__checked : this.styles.label}
                    />
                  </div>
                ))}
              </Paper>
              <hr style={this.styles.hr} />
              <div>
                <span style={this.styles.span}>
                  {' '}
                  {tr('Selected')} {this.state.currentFilter.type}:{' '}
                </span>
                <span className="clear_button" onClick={this.clearOptions.bind(this)}>
                  {tr('Clear')}
                </span>
                <div style={this.styles.fieldList}>
                  {this.state.currentFilter.options &&
                    this.state.currentFilter.options
                      .filter(elem => elem.checked)
                      .map((el, index) => {
                        return (
                          <div
                            key={`selected-options-${el.id}`}
                            onClick={() => {
                              this.toggleItems(el.id);
                            }}
                            style={this.styles.options}
                          >
                            {el.option} x
                          </div>
                        );
                      })}
                </div>
              </div>
              <hr style={this.styles.hr} />
              <div className="footer_button">
                <button onClick={this.cancelFilter.bind(this)} style={this.styles.cancel}>
                  {' '}
                  {tr('Cancel')}{' '}
                </button>
                <button onClick={this.applyFilter.bind(this)} style={this.styles.apply}>
                  {' '}
                  {tr('Apply')}{' '}
                </button>
              </div>
            </div>
          )}
        </Popover>
      </div>
    );
  }
}

DynamicSelections.propTypes = {
  modal: PropTypes.any,
  getPreviewList: PropTypes.func,
  filterContent: PropTypes.array,
  target: PropTypes.string,
  forIndividuals: PropTypes.bool
};

function mapStoreStateToProps(state) {
  return {
    modal: state.modal.toJS()
  };
}

export default connect(mapStoreStateToProps)(DynamicSelections);
