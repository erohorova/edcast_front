import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../common/spinner';
import SharedWith from '../../components/shareContent/SharedWith';
import { tr } from 'edc-web-sdk/helpers/translations';

class CurrentlShared extends Component {
  render() {
    let contents = this.props.shareContent && this.props.shareContent;
    let card = this.props.card || {};
    let sharedWithUsers =
      (this.props.shareContent &&
        this.props.shareContent[this.props.shareContent.currentCard] &&
        contents[this.props.shareContent.currentCard].usersWithAccess) ||
      [];
    let sharedWithGroups =
      (this.props.shareContent &&
        this.props.shareContent[this.props.shareContent.currentCard] &&
        contents[this.props.shareContent.currentCard].teams) ||
      [];

    return (
      <div>
        {this.props.shareContent &&
          this.props.shareContent.currentCard &&
          contents[this.props.shareContent.currentCard] && (
            <div id="sc-shared-list-container">
              {!(
                this.props.shareContent &&
                this.props.shareContent.currentCard &&
                contents[this.props.shareContent.currentCard]
              ) &&
                !sharedWithGroups.length &&
                !sharedWithUsers.length && (
                  <div className="text-center">
                    <Spinner />
                  </div>
                )}
              {!!sharedWithUsers.length && (
                <div className="individual-list">
                  <div className="title">{tr('Individuals')}</div>
                  <div className="list">
                    {sharedWithUsers.map(user => {
                      if (this.props.currentUser.id != user.id) {
                        return (
                          <SharedWith
                            user={user}
                            key={user.id}
                            isCurrentlyShared={true}
                            {...this.props}
                          />
                        );
                      }
                    })}
                  </div>
                </div>
              )}
              {!!sharedWithGroups.length && (
                <div className="group-list">
                  <div className="title">{tr('Groups')}</div>
                  <div className="list">
                    {sharedWithGroups.map(group => {
                      return (
                        <SharedWith
                          group={group}
                          key={group.id}
                          isCurrentlyShared={true}
                          {...this.props}
                        />
                      );
                    })}
                  </div>
                </div>
              )}
              {!sharedWithUsers.length && !sharedWithGroups.length && (
                <div className="text-center empty-msg">
                  {tr('This content is not shared with anyone.')}
                </div>
              )}
            </div>
          )}
      </div>
    );
  }
}

CurrentlShared.propTypes = {
  currentUser: PropTypes.any,
  shareContent: PropTypes.any,
  card: PropTypes.any,
  dispatch: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(CurrentlShared);
