import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';
import findIndex from 'lodash/findIndex';
import { Menu, AsyncTypeahead } from 'react-bootstrap-typeahead';
import { fetchGroups } from 'edc-web-sdk/requests/groups.v2';
import { searchUsers } from 'edc-web-sdk/requests/users.v2';
import { _addGroup, _addUser } from '../../actions/shareContentActions';

class SearchBox extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      activeTab: 'Individuals',
      users: [],
      totalUsers: 0,
      groups: [],
      totalGroups: 0,
      searchBoxValue: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    let activeTab = nextProps.activeTab;
    this._searchBox.getInstance().clear();
    this.setState({
      activeTab
    });
  }

  _handleResultsClick = result => {
    this.state.activeTab === 'Individuals'
      ? this.props.dispatch(_addUser(result, this.props.card, true))
      : this.state.activeTab === 'Groups' &&
        this.props.dispatch(_addGroup(result, this.props.card, true));

    this._searchBox.getInstance().clear();
  };

  _renderMenu = (results, menuProps) => {
    const menuData = (results || []).map(data => {
      return [
        <li
          onClick={e => {
            this._handleResultsClick(data);
          }}
        >
          <a>
            <div className="user-search-name" style={{ fontSize: '12px' }}>
              {data.name}
            </div>
            {data.handle && (
              <span style={{ fontSize: '11px', margin: '0 0 0 5px' }}> ({data.handle})</span>
            )}
          </a>
        </li>
      ];
    });
    return <Menu {...menuProps}>{menuData}</Menu>;
  };

  _handleSearch = query => {
    let payload = { limit: 15, offset: 0, q: query };
    if (this.state.activeTab === 'Individuals') {
      searchUsers(payload)
        .then(response => {
          let totalUsers = response.total;

          let { shareWithUsers, searchedUsers } = this.props.shareContent;
          let alreadyShared =
            (this.props.shareContent[this.props.card.id] &&
              this.props.shareContent[this.props.card.id].usersWithAccess) ||
            [];
          let selectedUsers = [
            ...(shareWithUsers || []),
            ...(searchedUsers || []),
            ...alreadyShared
          ];

          let users = this._searchFilterForExistingEntries(selectedUsers, response.users);

          this.setState({ users, totalUsers });
        })
        .catch(e => {});
    } else if (this.state.activeTab === 'Groups') {
      delete payload.q;
      payload['search_term'] = query;
      payload['writables'] = true;
      fetchGroups(payload)
        .then(response => {
          let totalGroups = response.total;

          let { shareWithGroups, searchedGroups } = this.props.shareContent;
          let alreadyShared =
            (this.props.shareContent[this.props.card.id] &&
              this.props.shareContent[this.props.card.id].teams) ||
            [];
          let selectedGroups = [
            ...(shareWithGroups || []),
            ...(searchedGroups || []),
            ...alreadyShared
          ];

          let groups = this._searchFilterForExistingEntries(selectedGroups, response.teams);

          this.setState({ groups, totalGroups });
        })
        .catch(e => {});
    }
  };

  _searchFilterForExistingEntries = (existingEntries, searchResults) => {
    existingEntries.map(existingEntry => {
      let index = findIndex(searchResults, searchResult => {
        return searchResult.id == existingEntry.id;
      });
      if (index >= 0) {
        searchResults.splice(index, 1);
      }
      if (this.state.activeTab === 'Individuals') {
        index = findIndex(searchResults, ['id', parseInt(this.props.currentUser.id)]);
        index >= 0 && searchResults.splice(index, 1);
      }
    });

    return searchResults;
  };

  render() {
    const props = {};
    props.renderMenu = this._renderMenu;
    let dataSourse =
      this.state.activeTab === 'Individuals'
        ? this.state.users
        : this.state.activeTab === 'Groups'
        ? this.state.groups
        : [];

    return (
      <div>
        <div className="user-search-input">
          <AsyncTypeahead
            {...props}
            labelKey="name"
            options={dataSourse}
            placeholder={tr('Search')}
            onSearch={q => {
              this._handleSearch(q);
            }}
            submitFormOnEnter={false}
            ref={ref => (this._searchBox = ref)}
            value={this.state.searchBoxValue}
          />
        </div>
      </div>
    );
  }
}

SearchBox.propTypes = {
  currentUser: PropTypes.any,
  shareContent: PropTypes.any,
  card: PropTypes.any,
  activeTab: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(SearchBox);
