import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SharedWith from '../../components/shareContent/SharedWith';
import Spinner from '../common/spinner';
import * as shareContentAction from '../../actions/shareContentActions';
import throttle from 'lodash/throttle';
import IconButton from 'material-ui/IconButton/IconButton';
import CloseIcon from 'material-ui/svg-icons/content/clear';

class Groups extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loadMore: false
    };
  }

  componentDidMount() {
    document.getElementById('sc-group-list-content') &&
      document
        .getElementById('sc-group-list-content')
        .addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document
      .getElementById('sc-group-list-content')
      .removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = throttle(
    e => {
      let groups =
        (this.props.shareContent &&
          this.props.shareContent.groups &&
          this.props.shareContent.groups) ||
        [];
      let groupTotal = (this.props.shareContent && this.props.shareContent.totalGroups) || 0;
      let payload = { limit: 15, offset: groups.length };
      payload['writables'] = true;
      if (
        groupTotal > groups.length &&
        e.target.clientHeight + e.target.scrollTop === e.target.scrollHeight
      ) {
        this.setState({ loadMore: true });
        this.props
          .dispatch(shareContentAction._fetchGroups(payload))
          .then(() => {
            this.setState({ loadMore: false });
          })
          .catch(() => {
            this.setState({ loadMore: false });
          });
      }
    },
    150,
    { leading: false }
  );

  _removeSearchedGroup = (user, card, isSearch) => {
    this.props.dispatch(shareContentAction._removeGroup(user, card, isSearch));
  };

  render() {
    let groups =
      (this.props.shareContent &&
        this.props.shareContent.groups &&
        this.props.shareContent.groups) ||
      [];
    let content = this.props.shareContent[this.props.shareContent.currentCard];
    let searchedGroups = (this.props.shareContent && this.props.shareContent.searchedGroups) || [];
    let contentReload = !!this.props.shareContent.contentLoaded;

    return (
      <div>
        <div id="sc-group-list-content">
          {contentReload &&
            content &&
            groups.map(group => {
              return <SharedWith group={group} key={group.id} {...this.props} />;
            })}
          {((this.props.shareContent.groupsLoading === undefined
            ? !contentReload || !content || true
            : !contentReload || !content || this.props.shareContent.groupsLoading) ||
            this.state.loadMore) && (
            <div className="text-center">
              <Spinner />
            </div>
          )}
        </div>
        {!!searchedGroups.length && (
          <div id="searched-groups">
            {searchedGroups.map(group => {
              return (
                <div className="clip-container" key={group.id}>
                  <span className="clip-main-text clip-group-name">{group.name}</span>
                  <IconButton
                    aria-label="close"
                    className="clip-remove-btn"
                    onClick={() => {
                      this._removeSearchedGroup(
                        group,
                        this.props.shareContent[this.props.card.id],
                        true
                      );
                    }}
                  >
                    <CloseIcon color="#acadc1" />
                  </IconButton>
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  }
}

Groups.propTypes = {
  currentUser: PropTypes.any,
  shareContent: PropTypes.any,
  card: PropTypes.any
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(Groups);
