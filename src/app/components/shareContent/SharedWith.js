import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'material-ui/Checkbox';
import { connect } from 'react-redux';
import some from 'lodash/some';
import * as shareContentAction from '../../actions/shareContentActions';

class SharedWith extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      isChecked: false
    };
  }

  componentDidMount() {
    this._checkboxSetup(this.props);
  }

  _checkboxSetup = compProps => {
    let card = compProps.card || {};

    let contents = (compProps.shareContent && compProps.shareContent) || {};

    let sharedWithUsers =
      (compProps.shareContent && contents[card.id] && contents[card.id].usersWithAccess) || [];

    let sharedWithGroups =
      (compProps.shareContent && contents[card.id] && contents[card.id].teams) || [];

    let shareWithUsers = compProps.shareContent.shareWithUsers;
    let shareWithGroups = compProps.shareContent.shareWithGroups;

    if (compProps.user && compProps.user.id) {
      this.setState({
        isChecked:
          some([...sharedWithUsers, ...(shareWithUsers || [])], ['id', compProps.user.id]) ||
          compProps.isCurrentlyShared
      });
    } else if (compProps.group && compProps.group.id) {
      this.setState({
        isChecked:
          some([...sharedWithGroups, ...(shareWithGroups || [])], ['id', compProps.group.id]) ||
          compProps.isCurrentlyShared
      });
    }
  };

  _toggleCheckbox = e => {
    let user = this.props.user || {};
    let group = this.props.group || {};
    let isChecked = e.target.checked;
    if (user.id) {
      isChecked
        ? this.props.dispatch(shareContentAction._addUser(user, this.props.card))
        : this.props.dispatch(shareContentAction._removeUser(user, this.props.card));
    } else if (group.id) {
      isChecked
        ? this.props.dispatch(shareContentAction._addGroup(group, this.props.card))
        : this.props.dispatch(shareContentAction._removeGroup(group, this.props.card));
    }
    this.setState({ isChecked });
  };

  render() {
    let checkboxLabel = groupORuser(this.props);
    return (
      <div>
        <Checkbox
          label={checkboxLabel}
          checked={this.state.isChecked}
          onCheck={e => {
            this._toggleCheckbox(e);
          }}
          labelStyle={{ margin: '5px 0 5px 0', fontSize: '13px', fontWeight: 300 }}
          iconStyle={{ width: '16px', height: '16px', marginRight: '10px', marginTop: '6px' }}
        />
      </div>
    );
  }
}

let groupORuser = props => {
  let user = props.user || {};
  let group = props.group || {};
  let defaultUserImage = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';

  if (user.id) {
    return (
      <a style={{ color: '#454560' }} onClick={() => {}} className="checkbox-label">
        {user.avatarimages && user.avatarimages.tiny && (
          <img
            src={(user.avatarimages && user.avatarimages.tiny) || defaultUserImage}
            className="user-image"
          />
        )}
        <div className="user-name">{user.name || user.fullName || ''}</div>
        {user.handle && <span className="user-handle">({user.handle})</span>}
      </a>
    );
  } else if (group.id) {
    return (
      <a style={{ color: '#454560' }} onClick={() => {}} className="checkbox-label">
        <div className="group-name" style={{ fontSize: '13px', fontWeight: 500 }}>
          {group.name || group.label || ''}
        </div>
      </a>
    );
  }
};

SharedWith.propTypes = {
  user: PropTypes.any,
  group: PropTypes.any,
  isCurrentlShared: PropTypes.bool,
  card: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS(),
    shareContent: state.shareContent.toJS()
  };
}

export default connect(mapStoreStateToProps)(SharedWith);
