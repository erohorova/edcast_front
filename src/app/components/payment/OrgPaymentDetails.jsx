/**
 * Created by ypling on 7/5/16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Dialog from 'material-ui/Dialog';
import PrimaryButton from 'edc-web-sdk/components/PrimaryButton';
import SecondaryButton from 'edc-web-sdk/components/SecondaryButton';
//actions
import CloseIcon from 'material-ui/svg-icons/content/clear';
import { close } from '../../actions/modalActions';
import TextField from 'material-ui/TextField';
import { open as openSnackBar } from '../../actions/snackBarActions';
import { tr } from 'edc-web-sdk/helpers/translations';
import IconButton from 'material-ui/IconButton';
import TextFieldCustom from '../../components/common/SmartBiteInput';
import * as usersSDK from 'edc-web-sdk/requests/users.v2';
import { initiateCardPurchase, completedCardPurchase } from 'edc-web-sdk/requests/cards.v2';
import InputMask from 'react-text-mask';
import {
  Icon_AmericanExpress,
  Icon_CreditCardOutline,
  Icon_DinersClub,
  Icon_Discover,
  Icon_JCB,
  Icon_Visa,
  Icon_MasterCard
} from 'material-ui-credit-card-icons';
import Spinner from '../common/spinner';
let LocaleCurrency = require('locale-currency');
var braintree = require('braintree-web');
var creditCardType = require('credit-card-type');
var valid = require('card-validator');
import FontIcon from 'material-ui/FontIcon';
import { orgSubscriptionPaid } from '../../actions/usersActions';

class OrgPaymentDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      titleLabel: '',
      sentDetails: false,
      disabled: true,
      cardType: null,
      cardNumberError: null,
      showCVV: true,
      showForm: false,
      paymentError: null,
      paymentInitializationError: ''
    };
    this.styles = {
      closeBtn: {
        paddingRight: 0,
        width: 'auto'
      },
      cardNumberStyle: {},
      hint: {},
      inputs: {}
    };

    this.onSubmitCardDetails = this.onSubmitCardDetails.bind(this);
    this.cardChangeHandle = this.cardChangeHandle.bind(this);
    this.dateChangeHandle = this.dateChangeHandle.bind(this);
    this.cvvChangeHandle = this.cvvChangeHandle.bind(this);
    this.disableButton = this.disableButton.bind(this);
    this.showCVV = this.showCVV.bind(this);
    this.finalCardPurchase = this.finalCardPurchase.bind(this);
  }

  componentDidMount() {
    let payload = {
      orderable_type: 'org_subscription',
      price_id: this.props.priceData.id,
      orderable_id: this.props.orgSubscriptions[0].id
    };

    initiateCardPurchase(payload)
      .then(data => {
        let clientToken = data.transactionDetails.clientToken;
        this.setState(
          {
            token: data.transactionDetails.token,
            clientToken,
            client: new braintree.api.Client({ clientToken: clientToken }),
            showForm: true,
            paymentInitializationError: ''
          },
          function() {
            this.setUpBrainTree(data.transactionDetails.token, clientToken);
          }
        );
      })
      .catch(err1 => {
        this.setState({
          paymentInitializationError: err1.message,
          showForm: true
        });
      });
  }

  setUpBrainTree = (token, clientToken) => {
    braintree.setup(clientToken, 'custom', {
      container: 'payment-form'
    });
  };

  onChangeCardNumber = () => {};

  checkCardType = (type, event) => {};

  finalCardPurchase = () => {
    let _this = this;
    _this.state.client.tokenizeCard(
      {
        number: _this._cardNumber.input.value,
        expirationMonth: _this._month.input.value,
        expirationYear: _this._year.input.value,
        cvv: _this._cvv.input.value
      },
      function(err, nonce) {
        let payload = {
          payment_method_nonce: nonce,
          token: _this.state.token,
          gateway: 'braintree'
        };

        completedCardPurchase(payload)
          .then(data => {
            _this.props.dispatch(close());
            _this.props.dispatch(openSnackBar(data.message, true));
            _this.props.dispatch(orgSubscriptionPaid());
            _this.props.dispatch(push(`/me`));
          })
          .catch(err1 => {
            _this.setState({
              paymentError: err1.message,
              disabled: true
            });
          });
      }
    );
  };

  onSubmitCardDetails = () => {
    let _this = this;

    _this.setState({ disabled: true, paymentError: null });
    /*eslint handle-callback-err: "off"*/
    if (_this.state.paymentError != null) {
      let payload = {
        orderable_type: 'org_subscription',
        price_id: _this.props.priceData.id,
        orderable_id: _this.props.orgSubscriptions[0].id
      };

      initiateCardPurchase(payload)
        .then(data => {
          let clientToken = data.transactionDetails.clientToken;
          _this.setState(
            {
              token: data.transactionDetails.token,
              clientToken,
              client: new braintree.api.Client({ clientToken: clientToken }),
              showForm: true,
              paymentInitializationError: ''
            },
            function() {
              _this.setUpBrainTree(data.transactionDetails.token, clientToken);
              _this.finalCardPurchase();
            }
          );
        })
        .catch(err1 => {
          _this.setState({
            paymentInitializationError: err1.message,
            showForm: true
          });
        });
    } else {
      _this.finalCardPurchase();
    }
  };

  disableButton() {
    let numberValidation = valid.number(this._cardNumber.input.value);
    let dateValidation = valid.expirationDate(
      `${this._month.input.value}/${this._year.input.value}`
    );

    let cvvLength =
      (numberValidation.card && numberValidation.card.code && numberValidation.card.code.size) || 3;

    let cvvValidation = valid.cvv(this._cvv.input.value, cvvLength);
    let monthValidation = valid.expirationMonth(this._month.input.value);
    let yearValidation = valid.expirationYear(this._year.input.value);

    let disabled = !numberValidation.isValid || !dateValidation.isValid || !cvvValidation.isValid;

    this.setState({
      disabled: disabled,
      cardNumberError: this._cardNumber.input.value && !numberValidation.isValid ? ' ' : null,
      monthError:
        (this._month.input.value && !monthValidation.isValid) ||
        (this._month.input.value && this._year.input.value && !dateValidation.isValid)
          ? ' '
          : null,
      yearError: this._year.input.value && !yearValidation.isValid ? ' ' : null,
      cvvError: this._cvv.input.value && !cvvValidation.isValid ? ' ' : null
    });
  }

  cardChangeHandle() {
    let numberValidation = valid.number(this._cardNumber.input.value);
    this.disableButton();

    if (numberValidation.isPotentiallyValid) {
      this.setState({
        cardType: (numberValidation.card && numberValidation.card.type) || null
      });
    }
  }

  dateChangeHandle() {
    this.disableButton();
  }

  cvvChangeHandle() {
    this.disableButton();
  }

  showCVV() {
    this.setState({
      showCVV: !this.state.showCVV
    });
  }

  render() {
    if (!this.state.showForm) {
      return (
        <div className="text-center">
          <Spinner />
        </div>
      );
    }
    return (
      <div>
        {!this.state.paymentInitializationError && (
          <form id="payment-form" method="post" className="payment-details-container">
            <div className="row">
              <div className=" column small-5 position-relative">
                <TextField
                  hintText={'Enter Card Number'}
                  type="text"
                  aria-label={tr('Card Number')}
                  hintStyle={this.styles.hint}
                  style={this.styles.cardNumberStyle}
                  inputStyle={this.styles.inputs}
                  fullWidth={true}
                  guide={false}
                  onChange={this.cardChangeHandle.bind(this)}
                  ref={node => (this._cardNumber = node)}
                  errorText={this.state.cardNumberError}
                  icon={
                    <IconButton>
                      <Icon_CreditCardOutline />
                    </IconButton>
                  }
                />
                <div className="position-absolute">
                  {this.state.cardType == null && (
                    <IconButton>
                      <Icon_CreditCardOutline />
                    </IconButton>
                  )}
                  {this.state.cardType == 'visa' && (
                    <IconButton>
                      <Icon_Visa />
                    </IconButton>
                  )}
                  {this.state.cardType == 'master-card' && (
                    <IconButton>
                      <Icon_MasterCard />
                    </IconButton>
                  )}
                </div>
              </div>
            </div>
            <div className=" row">
              <div className=" column small-2">
                <TextField
                  hintText={'MM'}
                  aria-label={'enter card expiry month'}
                  type="text"
                  onChange={this.dateChangeHandle.bind(this)}
                  style={this.styles.cardNumberStyle}
                  hintStyle={this.styles.hint}
                  fullWidth={true}
                  inputStyle={this.styles.inputs}
                  ref={node => (this._month = node)}
                  errorText={this.state.monthError}
                  maxLength="2"
                />
              </div>
              <input type="hidden" name="payment_method_nonce" />
              <div className="column small-3 position-relative">
                <TextField
                  hintText={'YYYY'}
                  aria-label={'enter card expiry year'}
                  type="text"
                  onChange={this.dateChangeHandle.bind(this)}
                  style={this.styles.cardNumberStyle}
                  hintStyle={this.styles.hint}
                  fullWidth={true}
                  inputStyle={this.styles.inputs}
                  ref={node => (this._year = node)}
                  errorText={this.state.yearError}
                  maxLength="4"
                />
              </div>
            </div>
            <div className="row ">
              <div className="column small-2">
                <TextField
                  hintText={'CVV'}
                  aria-label={'enter card CVV'}
                  type="password"
                  onChange={this.cvvChangeHandle.bind(this)}
                  style={this.styles.cardNumberStyle}
                  hintStyle={this.styles.hint}
                  fullWidth={true}
                  ref={node => (this._cvv = node)}
                  inputStyle={this.styles.inputs}
                  errorText={this.state.cvvError}
                  maxLength="4"
                />
              </div>
              <div className="column small-2" style={{ paddingTop: '14px' }}>
                <a
                  onTouchTap={this.showCVV.bind(this)}
                  style={{
                    background: '#454460',
                    color: '#ffffff',
                    padding: '1px 8px',
                    fontSize: '12px',
                    border: '1px solid #bdbdbd',
                    borderRadius: '50%'
                  }}
                >
                  ?
                </a>
              </div>
            </div>
            {this.state.showCVV && (
              <div style={{ position: 'absolute', top: '65px', right: '100px' }}>
                <div className="cvv-heading">What is CVV?</div>
                <img src="/i/images/cvv.svg" />
                <div className="cvv-fade" />
              </div>
            )}

            <div className="row ">
              <div className="column small-5" style={{ marginTop: '10px' }}>
                <PrimaryButton
                  label={'Pay ' + this.props.priceData.symbol + this.props.priceData.amount}
                  className="confirm"
                  pendingLabel={tr('Confirming...')}
                  pending={false}
                  disabled={this.state.disabled}
                  onTouchTap={this.onSubmitCardDetails.bind(this)}
                  fullWidth={true}
                />
                {this.state.paymentError && (
                  <div className="error" style={{ fontSize: '12px', color: 'red' }}>
                    {this.state.paymentError}
                  </div>
                )}
              </div>
            </div>
          </form>
        )}
        {this.state.paymentInitializationError && (
          <div className="row">
            <div className="small-12 columns payment-buttons">
              <div className="actions-container">
                <div className="text-center">
                  <div
                    className="error"
                    style={{ fontSize: '18px', color: 'red', marginBottom: '25px' }}
                  >
                    {this.state.paymentInitializationError}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

OrgPaymentDetails.propTypes = {
  priceData: PropTypes.object,
  orgSubscriptions: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    currentUser: state.currentUser.toJS()
  };
}

export default connect(mapStoreStateToProps)(OrgPaymentDetails);
