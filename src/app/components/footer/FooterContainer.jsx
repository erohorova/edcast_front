import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Footer from './Footer.jsx';
import SearchV1 from '../TopNav/SearchV1';

class FooterContainer extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div id="footer-wrapper">
        <div className="footer-wrapped-content">
          <div
            className={`search-redesign-header search-input-block action-icon-list horizontal-spacing-large ml-auto search-v1 search-v2`}
          >
            <SearchV1 queryTerm={this.props.queryTerm} />
          </div>
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
}

FooterContainer.propTypes = {
  queryTerm: PropTypes.string,
  children: PropTypes.element
};

export default FooterContainer;
