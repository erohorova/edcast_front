import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getSettingsVersion } from 'edc-web-sdk/requests/orgSettings';
import { connect } from 'react-redux';
import { tr } from 'edc-web-sdk/helpers/translations';

class Footer extends Component {
  constructor(props, context) {
    super(props, context);

    this.footerOptions =
      this.props.team && this.props.team.config && this.props.team.config.footer_options;

    this.state = {
      version: null
    };
  }

  componentDidMount() {
    if (this.props.user && this.props.user.isAdmin) {
      getSettingsVersion()
        .then(data => {
          if (data && data.build) {
            this.setState({
              version: data.build.split('release-')[data.build.split('release-').length - 1]
            });
          }
        })
        .catch(err => {
          console.error(`Error in Footer.getSettingsVersion.func : ${err}`);
        });
    }
  }

  isPromotionsEnabled() {
    let promotions;
    let parsePromotions;
    if (this.footerOptions == undefined) {
      return true;
    } else {
      promotions = this.footerOptions.promotions;
      try {
        parsePromotions = JSON.parse(promotions) == true;
      } catch (e) {
        console.log('JSON parse error in Footer');
      }
      return (
        promotions == undefined ||
        (typeof promotions == 'string' && promotions.trim().length == 0) ||
        parsePromotions
      );
    }
  }

  checkUrl(url) {
    let isUrlValid = new RegExp('^(http|https)://');
    return isUrlValid.test(url) ? url : 'http://' + url;
  }

  privacyPolicy() {
    let link = this.footerOptions && this.footerOptions.privacy_policy;
    let showPrivacyPolicy = link != undefined && link != '';
    link = showPrivacyPolicy ? this.checkUrl(link) : 'https://www.edcast.com/corp/privacy-policy/';

    return link;
  }

  termsOfService() {
    let link = this.footerOptions && this.footerOptions.terms_of_service;
    let showTermsOfService = link != undefined && link != '';
    link = showTermsOfService
      ? this.checkUrl(link)
      : 'https://www.edcast.com/corp/terms-of-service/';

    return link;
  }

  appStore() {
    let link = this.footerOptions && this.footerOptions.appStore;
    let showAppStore = link != undefined && link != '';
    link = showAppStore
      ? this.checkUrl(link)
      : 'https://itunes.apple.com/app/apple-store/id974833832?pt=100220803&ct=KKWebsiteLink&mt=8';

    return link;
  }

  playStore() {
    let link = this.footerOptions && this.footerOptions.playStore;
    let showPlayStore = link != undefined && link != '';
    link = showPlayStore
      ? this.checkUrl(link)
      : 'https://play.google.com/store/apps/details?id=com.edcast';

    return link;
  }

  chromeExtension() {
    let link = this.footerOptions && this.footerOptions.chromeExtension;
    let showChromeExtension = link != undefined && link != '';
    link = showChromeExtension
      ? this.checkUrl(link)
      : 'https://chrome.google.com/webstore/detail/edcast-guideme/acildjkogadjdnolbaiepkefilhglcdm';

    return link;
  }

  render() {
    let showExtension = /(.*)Chrome(.*)/.test(navigator.userAgent);
    let promotionEnabled = this.isPromotionsEnabled();
    let config = this.props.team.config;
    let enableEdcastLogo =
      config &&
      ((typeof config.enable_edcast_logo == 'undefined' && !config.enable_edcast_logo) ||
        !!config.enable_edcast_logo);
    return (
      <div id="footer">
        <div className="row container-padding vertical-spacing-large">
          <div className="small-12 large-4 horizontal-spacing-large flex">
            {enableEdcastLogo && (
              <a href="https://www.edcast.com" target="_blank">
                <img className="powered-logo" src="/i/images/powered-by-edcast.png" />
              </a>
            )}
          </div>
          <div className="small-12 large-4 horizontal-spacing-large flex justify-center">
            <a href={this.privacyPolicy()} className="matte text-center" target="_blank">
              {tr('Privacy Policy')}
            </a>
            &bull;
            <a href={this.termsOfService()} className="matte text-center" target="_blank">
              {tr('Terms of Service')}
            </a>
            {this.state.version && (
              <span>
                &bull;
                <a href="#" className="matte">
                  {this.state.version}
                </a>
              </span>
            )}
          </div>
          <div className="small-12 large-4 horizontal-spacing-large text-right flex flex-end">
            {promotionEnabled && (
              <div>
                <a href={this.appStore()} target="_blank">
                  <img className="app-links-img" src="/i/images/app_store.png" />
                </a>
                <a href={this.playStore()} target="_blank">
                  <img className="app-links-img" src="/i/images/play_store.png" />
                </a>
                {showExtension && (
                  <a href={this.chromeExtension()} target="_blank">
                    <img className="app-links-img" src="/i/images/chrome_extension.png" />
                  </a>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

Footer.propTypes = {
  team: PropTypes.object,
  user: PropTypes.object
};

function mapStoreStateToProps(state) {
  return {
    user: state.currentUser.toJS(),
    team: state.team.toJS()
  };
}
export default connect(mapStoreStateToProps)(Footer);
