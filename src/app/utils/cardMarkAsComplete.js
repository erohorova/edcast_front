/**
 * update complete card status
 *
 */
import { markAsComplete, markAsUncomplete } from 'edc-web-sdk/requests/cards.v2';
import { openStatusModal, confirmation } from '../actions/modalActions';
import { open as openSnackBar } from '../actions/snackBarActions';
import { markAssignmentCountToBeUpdated } from '../actions/assignmentsActions';

export default function completeClickHandler(
  state_card,
  isCompleted,
  cardUpdated,
  isPartOfPathway
) {
  let cardType;
  switch (state_card.cardType) {
    case 'VideoStream':
      cardType = 'video_stream';
      break;
    default:
      cardType = 'card';
      break;
  }

  let newModalAndToast = window.ldclient.variation('new-modal-and-toast', false);

  let callCompleteClickHandlerFunction = true;
  let markFeatureDisabledForSource = state_card.markFeatureDisabledForSource;
  let isPack = state_card.cardType === 'pack' || state_card.cardType === 'journey';
  if (
    (!isPack && !isCompleted) ||
    (isPack &&
      state_card.state === 'published' &&
      (+state_card.completedPercentage === 98 || +this.props.completeStatus === 98))
  ) {
    markAsComplete(state_card.id, { state: 'complete' })
      .then(() => {
        if (cardUpdated) cardUpdated();
        if (this.props.markAsComplete) this.props.markAsComplete();
        this.setState({ isCompleted: true }, () => {
          if (this.props.isOverviewModal) {
            if (newModalAndToast) {
              let message = isPartOfPathway
                ? `You have completed this SmartCard, please complete all SmartCards to complete this ${
                    !!~window.location.pathname.indexOf('journey/') ? 'Journey' : 'Pathway'
                  }`
                : `You have completed this ${
                    this.props.card.cardType === 'pack'
                      ? 'Pathway'
                      : this.props.card.cardType === 'journey'
                      ? 'Journey'
                      : 'SmartCard'
                  } and can track it in completion history under Me tab`;

              this.props.dispatch(openSnackBar(message, true));
            } else {
              this.props.dispatch(openSnackBar('Marked as complete', true));
            }
          } else {
            if (newModalAndToast) {
              let message = isPartOfPathway
                ? `You have completed this SmartCard, please complete all SmartCards to complete this ${
                    !!~window.location.pathname.indexOf('journey/') ? 'Journey' : 'Pathway'
                  }`
                : `You have completed this ${
                    this.props.card.cardType === 'pack'
                      ? 'Pathway'
                      : this.props.card.cardType === 'journey'
                      ? 'Journey'
                      : 'SmartCard'
                  } and can track it in completion history under Me tab`;
              this.props.dispatch(openSnackBar(message, true));
            } else {
              let simpleMessage =
                this.props.card.cardType === 'pack'
                  ? 'You have completed this pathway!'
                  : this.props.card.cardType === 'journey'
                  ? 'You have completed this journey!'
                  : 'Marked as complete';
              let compoundMessage = [
                'You have completed this task. You can view it under',
                '/me/content/completed',
                'Me',
                'Tab'
              ];
              let messageArray =
                this.props.card.hidden ||
                this.props.card.cardType === 'pack' ||
                this.props.card.cardType === 'journey'
                  ? [simpleMessage]
                  : compoundMessage;
              this.props.dispatch(openStatusModal('', null, messageArray));
            }
          }
        });
        if (
          this.props.hideComplete !== undefined &&
          ['my-assignments', '/my-assignments', '/'].indexOf(this.props.pathname) > -1
        ) {
          this.props.hideComplete(this.props.card.id);
          this.props.dispatch(markAssignmentCountToBeUpdated());
        }
      })
      .catch(err => {
        console.error(`Error in markAsComplete.func : ${err}`);
      });
  } else if ((!isPack && isCompleted) || (isPack && +state_card.completedPercentage === 100)) {
    callCompleteClickHandlerFunction = false;
    if (!markFeatureDisabledForSource) {
      this.props.dispatch(
        confirmation(
          'Please note',
          'Marking the card as incomplete will not change the Continuous Learning hour or the score associated with it.',
          () => {
            markAsUncomplete(state_card.id)
              .then(() => {
                if (cardUpdated) cardUpdated();
                this.setState({ isCompleted: false }, () => {
                  if (this.props.isOverviewModal) {
                    if (newModalAndToast) {
                      this.props.dispatch(
                        openSnackBar('You have marked this SmartCard as incomplete', true)
                      );
                    } else {
                      this.props.dispatch(openSnackBar('Marked as incomplete', true));
                    }
                  } else {
                    setTimeout(() => {
                      if (newModalAndToast) {
                        let message = `You have marked this ${
                          this.props.card.cardType === 'pack'
                            ? 'Pathway'
                            : this.props.card.cardType === 'journey'
                            ? 'Journey'
                            : 'SmartCard'
                        } as incomplete`;
                        this.props.dispatch(openSnackBar(message, true));
                      } else {
                        this.props.dispatch(
                          openStatusModal('You have marked this task as incomplete')
                        );
                      }
                    }, 1500);
                  }
                });
              })
              .catch(err => {
                console.error(`Error in markAsUncomplete.func : ${err}`);
              });
          }
        )
      );
    }
  } else {
    let msg =
      this.state.card.state === 'published'
        ? `Please complete all pending cards before marking the ${
            this.state.card.cardType === 'pack' ? 'Pathway' : 'Journey'
          } as complete.`
        : 'You should publish the pathway before complete';
    if (this.state.newModalAndToast) {
      this.props.dispatch(openSnackBar(msg));
    } else {
      this.props.dispatch(openStatusModal(msg));
    }
  }
  if (callCompleteClickHandlerFunction && this.props.callCompleteClickHandler) {
    this.props.callCompleteClickHandler();
  }
}
