/**
 * Get icon url for attached files
 *
 * @param {String} [type] - type od attached file
 * @return {String}
 */

export default function getFleIcon(type) {
  let iconFileSrc = '';

  switch (type) {
    case 'pdf':
      iconFileSrc = '/i/images/pdf-icon-min.png';
      break;
    case 'ppt':
      iconFileSrc = '/i/images/powerpoint-icon-min.png';
      break;
    default:
      // FIXME: implement default case
      break;
  }

  return iconFileSrc;
}
