import addSecurity from './filestackSecurity';
/**
 * Get default card image
 *
 *
 * @return FileStack object
 */

export default function getDefaultImage(userId) {
  let imagesFromS3 = JSON.parse(localStorage.getItem('defaultImagesS3'));
  const filestack = localStorage.getItem('filestackDefaultImages')
    ? JSON.parse(localStorage.getItem('filestackDefaultImages')).value
    : [];
  let arr = imagesFromS3 && imagesFromS3.length ? imagesFromS3 : filestack;
  let image = arr[Math.floor(Math.random() * arr.length)];
  if (imagesFromS3 && imagesFromS3.length) {
    let resultObj = {
      url: image,
      mimetype: 'image/jpeg'
    };
    return resultObj;
  }
  image['url'] = addSecurity(image.url, 3600, userId);
  return image;
}
