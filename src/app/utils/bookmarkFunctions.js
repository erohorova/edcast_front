/**
 * featured and unfeatured bookmark
 *
 */
import { pinCard, unpinCard } from 'edc-web-sdk/requests/channels.v2';

export function unfeaturedClickHandler(bookmarked, payload, callback) {
  unpinCard({
    object_id: bookmarked.deepLinkId,
    object_type: 'Card',
    pinnable_id: bookmarked.bookmarkId,
    pinnable_type: 'Bookmark'
  })
    .then(() => {
      callback(payload);
    })
    .catch(err => {
      console.error(`Error in LeftRailBookmarks.unfeaturedClickHandler.func : ${err}`);
    });
}

export async function featuredClickHandler(
  bookmarked,
  payload,
  isFeatured,
  featuredBookmark,
  callback
) {
  if (isFeatured) {
    await unpinCard({
      object_id: featuredBookmark.deepLinkId,
      object_type: 'Card',
      pinnable_id: featuredBookmark.bookmarkId,
      pinnable_type: 'Bookmark'
    }).catch(err => {
      console.error(`Error in LeftRailBookmarks.unpinCard.func : ${err}`);
    });
  }
  await pinCard({
    object_id: bookmarked.deepLinkId,
    object_type: 'Card',
    pinnable_id: bookmarked.bookmarkId,
    pinnable_type: 'Bookmark'
  })
    .then(() => {
      callback(payload);
    })
    .catch(err => {
      console.error(`Error in LeftRailBookmarks.pinCard.func : ${err}`);
    });
}
