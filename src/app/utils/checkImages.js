/**
 * Created by dunice on 11.05.2018.
 */

export default function checkImage(url) {
  return new Promise(function(resolve, reject) {
    let img = new Image();
    img.onerror = img.onabort = function() {
      resolve('error');
    };
    img.onload = function() {
      resolve('success');
    };
    img.src = url;
  });
}
