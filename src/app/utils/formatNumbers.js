export default function formatNumbers(number) {
  var formattedNumber = number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return formattedNumber;
}
