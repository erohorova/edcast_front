/*eslint no-param-reassign: "off"*/
export function CamelCase(str) {
  if (!str) {
    return '';
  }
  str = str.split(' ');

  for (var i = 0, x = str.length; i < x; i++) {
    str[i] = str[i][0].toUpperCase() + str[i].substr(1);
  }

  return str.join(' ');
}
