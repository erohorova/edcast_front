export default function checkInsertedLinks(message) {
  let urlRegexp = /\((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?\)/gi;
  let linksArr = message.match(urlRegexp);
  if (!linksArr) {
    return message;
  }
  let wordsArr = message.split(' ');
  for (let i = 0; i < linksArr.length; i++) {
    for (let j = wordsArr.length; j > 0; j--) {
      if (wordsArr[j] === linksArr[i]) {
        let link = linksArr[i].slice(1, -1);
        wordsArr[j - 1] = `<a href=${link} target="_blank">${wordsArr[j - 1]}</a>`;
        wordsArr[j] = '';
      }
    }
  }
  return wordsArr.join(' ');
}
