/**
 * Get card type UI
 *
 * @param {string} [containerId] - cards container id
 * @param {number} [elementWidth] - width of card
 * @return {Object}
 */

export default function resizeCards(containerId, elementWidth) {
  let container = document.getElementById(containerId);
  let containerStyle = container ? window.getComputedStyle(container) : {};
  let containerWidth = containerStyle.width ? containerStyle.width.replace('px', '') : 0;
  if (containerWidth) {
    let itemsCount = Math.floor(containerWidth / (elementWidth + 8)); //236 - width of one block + 8 - min margin
    let itemMargin = 8;
    if (itemsCount !== 1) {
      itemMargin = (containerWidth - elementWidth * itemsCount) / (itemsCount - 1);
    } else {
      itemMargin = containerWidth - elementWidth;
    }
    return { itemMargin, itemsCount };
  }
  return undefined;
}
