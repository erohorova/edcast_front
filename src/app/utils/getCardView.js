/**
 * Get card type UI
 *
 * @param {Object} [feedConfig] - feed config object
 * @param {String} [feedKey] - key of current feed page
 * @return {String}
 */

export default function getCardView(feedConfig, feedKey) {
  let viewRegime = 'Tile';
  if (feedConfig && feedConfig[feedKey]) {
    viewRegime = feedConfig[feedKey].defaultCardView;
  }
  return viewRegime;
}
