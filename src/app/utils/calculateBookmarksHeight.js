export default function calculateBookmarksHeight() {
  let bookmarksNode = document.getElementById('bookmarks-panel_v2');
  let bookmarkItems__container_v2 = document.getElementById('bookmark-items__container_v2');
  let bookmark__noContent = document.getElementById('bookmark__no-content');
  let bookmark__spinner = document.getElementById('bookmark__spinner');
  if (bookmarksNode) {
    let learningqueueBookmarksNode = document.getElementById('learningqueue-bookmarks');
    let userPanelNode = document.getElementById('user-panel');
    let itemsCommonHeight = 0;
    if (bookmarkItems__container_v2) {
      itemsCommonHeight = (bookmarkItems__container_v2.childElementCount * 60 || 15) + 116;
    } else if (bookmark__noContent) {
      itemsCommonHeight = bookmark__noContent.offsetHeight + 116;
    } else if (bookmark__spinner) {
      itemsCommonHeight = bookmark__spinner.offsetHeight + 116;
    }
    if (
      learningqueueBookmarksNode.className &&
      learningqueueBookmarksNode.className.indexOf('stick-to-top') > -1 &&
      bookmarksNode.offsetHeight + userPanelNode.offsetHeight + 150 >
        learningqueueBookmarksNode.offsetHeight
    ) {
      let height = userPanelNode.offsetHeight + 150;
      bookmarksNode.style.height = `calc(100vh - ${height}px)`;
      bookmarksNode.style.maxHeight = `${itemsCommonHeight}px`;
    }
  }
}
