/**
 * Check card data resource and set params to resource object
 *
 * @param {string} [url] - card data
 * @return {boolean}
 */

export default function checkResourceURL(url) {
  let request;
  try {
    if (window.XMLHttpRequest) {
      request = new XMLHttpRequest();
    } else {
      /*global ActiveXObject*/
      /*eslint no-undef: "error"*/
      request = new ActiveXObject('Microsoft.XMLHTTP');
    }
    request.open('GET', url, false);
    request.send(); // there will be a 'pause' here until the response to come.
    return request.status === 200;
  } catch (err) {
    return false;
  }
}
