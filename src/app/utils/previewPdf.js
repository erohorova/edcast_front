import { parseUrl } from './urlParser';
import addSecurity from './filestackSecurity';

export default function pdfPreviewUrl(url, expiry_time, userId) {
  let securedUrl = addSecurity(url, expiry_time, userId);
  if (!!window.process.env.SECURE_AUTHENTICATED_IMAGES) {
    return `${securedUrl}?action=preview`;
  } else {
    let path = parseUrl(securedUrl).pathname;
    return `https://cdn.filestackcontent.com/preview/${path}`;
  }
}
