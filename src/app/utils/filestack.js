import filestack from 'filestack-js';
import { parseUrl } from './urlParser';

export function filestackClient(policy, signature) {
  let api_key = window.process.env.FILESTACK_APIKEY || 'AWW0DswE5R3iPt9CyK9OTz';
  return filestack.init(api_key, { policy: policy, signature: signature });
}
/*
 * Refer to resize options here https://www.filestack.com/docs/image-transformations/resize
 * */
export const getResizedUrl = (src, options, mimetype) => {
  let modifiedSrc = src;

  if (modifiedSrc && !!~modifiedSrc.indexOf('?size=medium')) {
    return modifiedSrc;
  }

  let parser = parseUrl(modifiedSrc);
  if (parser.hostname == 'cdn.filestackcontent.com') {
    let pathname = parser.pathname;

    if (convertToJpg(modifiedSrc, mimetype)) {
      return `https://process.filestackapi.com/resize=${options}/rotate=deg:exif/output=format:jpg/${pathname}`;
    } else {
      return `https://process.filestackapi.com/resize=${options}/rotate=deg:exif/${pathname}`;
    }
  }

  if (modifiedSrc.indexOf('/uploads/') > 0) {
    if (convertToJpg(modifiedSrc, mimetype)) {
      return modifiedSrc + `?action=resize=${options}/rotate=deg:exif/output=format:jpg`;
    } else {
      return modifiedSrc + `?action=resize=${options}/rotate=deg:exif`;
    }
  }
  return modifiedSrc;
};

function convertToJpg(url, mimetype) {
  return (
    mimetype &&
    (mimetype.indexOf('image/tiff') === -1 ||
      (mimetype.indexOf('image/tiff') !== -1 && url.indexOf('output=format:jpg/') !== -1))
  );
}
