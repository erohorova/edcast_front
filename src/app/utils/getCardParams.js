/**
 * Get current card params
 *
 * @param {Object} [card] - card data
 * @param {Object} [props] - props data
 * @param {String} [place] - file name where func was call
 * @return {Object}
 */
import getFileIcon from './getFileIcon';
import getCardType from './getCardType';
import getVideoStatus from './getVideoStatus';
import getCardImage from './getCardImage';
import { Permissions } from './checkPermissions';

export default function getCardParams(card, props, place) {
  let params = {};

  let isInsightV2 = place === 'insightV2';
  let isCardFeedView = place === 'cardFeedView';
  let isCardListViewPreview = place === 'cardListViewPreview';
  let isCard = place === 'card';

  let message =
    card.message || card.title || (isInsightV2 ? card.url : card.name) || card.description;

  let isFileAttached = card.filestack && !!card.filestack.length;
  let videoFileStack = !!(
    isFileAttached &&
    card.filestack[0].mimetype &&
    ~card.filestack[0].mimetype.indexOf('video/')
  );
  let audioFileStack = !!(
    isFileAttached &&
    card.filestack[0].mimetype &&
    ~card.filestack[0].mimetype.indexOf('audio/')
  );
  let pdfFileStack = !!(
    isFileAttached &&
    card.filestack[0].mimetype &&
    !!~card.filestack[0].mimetype.indexOf('/pdf')
  );
  let imageFileStack = !!(
    isFileAttached &&
    card.filestack[0].mimetype &&
    ~card.filestack[0].mimetype.indexOf('image/')
  );
  let appFileStack = !!(
    isFileAttached &&
    card.filestack[0].mimetype &&
    ~card.filestack[0].mimetype.indexOf('application/')
  );

  let imageFileResources =
    !!(
      card.fileResources &&
      card.fileResources.length &&
      card.fileResources[0].fileType === 'image'
    ) || card.image;

  let fileFileStack = isFileAttached && !videoFileStack && !imageFileStack;
  let cardType = audioFileStack
    ? 'AUDIO'
    : pdfFileStack
    ? 'FILE'
    : getCardType(card, videoFileStack, fileFileStack);
  let imageExists =
    (card.resource &&
      card.resource.imageUrl &&
      !~card.resource.imageUrl.indexOf('default_card_image')) ||
    card.image;

  // This is specifically for cards returned in smartsearch
  if (!imageExists && card.content_type === 'article') {
    imageExists = true;
  }
  if (isCard || isCardFeedView || isCardListViewPreview) {
    imageExists = imageExists || imageFileStack || imageFileResources;
  }

  let cardImage = getCardImage(card, imageFileStack);

  let showBlurryBackground = true;
  if (imageExists && (cardImage && cardImage.slice(-3) === 'png')) {
    showBlurryBackground = false;
  }
  let fileType = '';
  if (isFileAttached) {
    fileType = card.filestack[0].mimetype && card.filestack[0].mimetype.slice(-3);
    showBlurryBackground = !(fileType === 'png');
  }
  if (message) {
    message = isCard ? message.replace(/\\\*/g, '*') : message;
    message =
      isCard && ~['PATHWAY', 'JOURNEY'].indexOf(cardType)
        ? card.title || card.message || card.name
        : message;
    message =
      !isCardFeedView && isFileAttached && card.filestack[0].handle === message ? '' : message;
    message =
      isCardFeedView || isCardListViewPreview || isCard
        ? message.replace(/amp;/gi, '')
        : isInsightV2
        ? message
        : message;
  }
  if (isCard && (cardType === 'POLL' || cardType === 'QUIZ')) {
    message = card.resource && card.resource.description ? card.resource.description : card.message;
  }

  let videoData = {};
  if (cardType === 'VIDEO' && card.videoStream && !isInsightV2) {
    videoData = isCardFeedView || isCard ? getVideoStatus(card) : {};
    imageExists =
      (card.videoStream &&
        card.videoStream.imageUrl &&
        !~card.videoStream.imageUrl.indexOf('default_card_image')) ||
      ((isCardFeedView || isCard) && imageExists);
  } else if (cardType === 'AUDIO' && !isInsightV2) {
    imageExists =
      !!(
        card.filestack &&
        card.filestack[1] &&
        card.filestack[1].mimetype &&
        !!~card.filestack[1].mimetype.indexOf('image/')
      ) ||
      (isCard &&
        !!(
          card.resource &&
          card.resource.imageUrl &&
          !~card.resource.imageUrl.indexOf('default_card_image')
        ));
  }

  let isShowTopImage =
    (cardType === 'TEXT' && imageExists) ||
    (cardType !== 'TEXT' &&
      cardType !== 'POLL' &&
      cardType !== 'QUIZ' &&
      cardType !== 'FILE' &&
      cardType !== 'AUDIO') ||
    (cardType === 'QUIZ' && imageExists) ||
    (cardType === 'POLL' && imageExists) ||
    (cardType === 'AUDIO' && imageExists);
  let poster =
    (card.filestack &&
      ((card.filestack[1] &&
        card.filestack[1].mimetype &&
        !!~card.filestack[1].mimetype.indexOf('image/') &&
        card.filestack[1].url) ||
        (card.filestack[0] && card.filestack[0].thumbnail))) ||
    cardImage;

  isShowTopImage =
    isCardFeedView || isCard
      ? isShowTopImage || (cardType === 'FILE' && imageExists) || (cardType === 'FILE' && poster)
      : isShowTopImage;

  const isYoutubeVideo =
    card.resource &&
    card.resource.siteName &&
    (card.resource.siteName.toLowerCase() === 'youtube' ||
      card.resource.url.indexOf('https://youtube.com') > -1 ||
      card.resource.url.indexOf('https://www.youtube.com') > -1 ||
      card.resource.url.indexOf('https://www.youtu.be') > -1 ||
      card.resource.url.indexOf('https://youtu.be') > -1);

  const isVimeoVideo =
    card.resource &&
    card.resource.siteName &&
    (card.resource.siteName.toLowerCase() === 'vimeo' ||
      card.resource.url.indexOf('https://vimeo.com') > -1);

  let isShowLeap =
    cardType === 'QUIZ' &&
    Permissions['enabled'] !== undefined &&
    ((!card.leaps && Permissions.has('CREATE_LEAP')) ||
      (card.leaps && Permissions.has('UPDATE_LEAP'))) &&
    !(props.pathwayDetails || props.journeyDetails);

  let isHideSpecialInfo =
    props.currentUser &&
    !props.currentUser.isAdmin &&
    card.message === 'You are not authorized to access this card'
      ? true
      : false;

  let config = props.team && props.team.config;

  params['fileFileStack'] = isCardFeedView ? fileFileStack && !audioFileStack : fileFileStack;
  params['videoFileStack'] = videoFileStack;
  params['audioFileStack'] = audioFileStack;
  params['imageFileStack'] = imageFileStack;
  params['isFileAttached'] = isFileAttached;
  params['imageFileResources'] = imageFileResources;
  params['iconFileSrc'] = isInsightV2 || !isFileAttached ? '' : getFileIcon(fileType);
  params['cardType'] = cardType;
  params['message'] = message;
  params['showBlurryBackground'] = showBlurryBackground;
  params['videoData'] = videoData;
  params['imageExists'] = imageExists;
  params['isShowTopImage'] = isShowTopImage;
  params['defaultUserImage'] = 'https://d2rdbjk9w0dffy.cloudfront.net/assets/anonymous-user.jpeg';
  params['cardSvgBackground'] = cardImage;
  params['isOwner'] = props.author && props.author.id == props.currentUser.id;
  params['isPathwayOwner'] =
    props.pathwayDetails &&
    props.pathwayDetails.author &&
    props.pathwayDetails.author.id == props.currentUser.id;
  params['isJourneyOwner'] =
    props.journeyDetails &&
    props.journeyDetails.author &&
    props.journeyDetails.author.id == props.currentUser.id;
  params['poster'] = poster;
  params['showCreator'] =
    config &&
    ((typeof config.show_creator_in_card === 'undefined' && !config.show_creator_in_card) ||
      !!config.show_creator_in_card);
  params['hideProvider'] = !(config && !config.hide_provider);
  params['isDownloadContentDisabled'] =
    props.team &&
    props.team.OrgConfig &&
    props.team.OrgConfig.content &&
    props.team.OrgConfig.content['web/content/disableDownload'].value;
  params['cardImage'] = imageExists ? cardImage : null;
  params['isYoutubeVideo'] = isYoutubeVideo;
  params['isVimeoVideo'] = isVimeoVideo;
  params['isShowLeap'] = isShowLeap;
  params['appFileStack'] = appFileStack;
  params['isHideSpecialInfo'] = isHideSpecialInfo;
  return params;
}
