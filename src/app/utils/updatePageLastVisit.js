export default function updatePageLastVisit(value) {
  if (!localStorage.getItem(value)) {
    localStorage.setItem(value, Math.round(new Date().getTime() / 1000));
  }
  // when we reload page we set new data
  if (window.performance && window.performance.navigation.type === 1) {
    localStorage.setItem(value, Math.round(new Date().getTime() / 1000));
  }
}
