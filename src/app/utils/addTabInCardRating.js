/**
 * add tabindex in card's raiting
 *
 */

export default function addTabInCardRating(ratingChanged) {
  let starList = document.querySelectorAll('.relevancyRatingStars_tab span[data-forhalf]');
  let starLength = starList ? starList.length : 0;
  for (let i = 0; i < starLength; i++) {
    starList[i].setAttribute('tabIndex', '0');
    starList[i].setAttribute('aria-label', `rating, ${Number(i) + 1}`);
    starList[i].addEventListener('keydown', e => {
      starList[i].classList.remove('removeBlueOutline');
      if (e.keyCode == 13) {
        ratingChanged(Number(e.currentTarget.dataset.index) + 1);
      }
    });
    starList[i].addEventListener('mouseover', e => {
      starList[i].classList.add('removeBlueOutline');
    });
  }
}
