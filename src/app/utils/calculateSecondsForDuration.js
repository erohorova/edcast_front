export default function calculateSeconds(string) {
  let timeArr = string.split(':');
  let months = parseInt(timeArr[0], 10);
  let days = parseInt(timeArr[1], 10);
  let hrs = parseInt(timeArr[2], 10);
  let mins = parseInt(timeArr[3], 10);
  let monthSeconds = 0;
  let daySeconds = 0;
  let hrSeconds = 0;
  let minSeconds = 0;
  if (!isNaN(months)) {
    monthSeconds = months * 2592000;
  }
  if (!isNaN(days)) {
    daySeconds = days * 86400;
  }
  if (!isNaN(hrs)) {
    hrSeconds = hrs * 3600;
  }
  if (!isNaN(mins)) {
    minSeconds = mins * 60;
  }
  return monthSeconds + daySeconds + hrSeconds + minSeconds;
}
