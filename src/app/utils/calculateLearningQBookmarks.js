export default function calculateLearningQBookmarksPosition() {
  let announcementsNode = document.getElementsByClassName('left-rail-announcement')[0];
  let userDetailsNode = document.getElementById('user-details-v2');
  let announcementsNodeHeight = (announcementsNode && announcementsNode.offsetHeight) || 0;
  let userDetailsNodeHeight = (userDetailsNode && userDetailsNode.offsetHeight) || 0;

  if (window.pageYOffset > announcementsNodeHeight + userDetailsNodeHeight + 45) {
    return true;
  }
  return false;
}
