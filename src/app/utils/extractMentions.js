import find from 'lodash/find';

export default function extractMentions(message, messageMentions) {
  let messageParam = message;
  if (!messageParam) {
    return [];
  }
  let result = [];
  let mentions =
    messageParam.match(/(?:^|\W)@(([a-zA-Z0-9\-_]+)(:[0-9]*)|([a-zA-Z0-9\-_]+))(?!\w)/g) || [];
  mentions.forEach(mention => {
    let index,
      length = mention.length;
    index = messageParam.indexOf(mention);
    let text = messageParam.substring(0, index);
    let userId = mention.match(/(:[0-9]*)/g);
    messageParam = messageParam.substring(index + length);
    if (text) {
      result.push({ type: 'text', text });
    }
    if (userId) {
      let mentionText = mention.replace(userId[0], '');
      let handle = mentionText.trim();
      if (messageMentions) {
        let mentionUser = find(messageMentions, { id: +userId[0].substring(1) });
        handle = mentionUser ? mentionUser.handle : handle;
      }
      result.push({ type: 'handle', handle: handle, userId: userId[0].substring(1) });
    } else {
      result.push({ type: 'handle', handle: mention.trim() });
    }
  });
  if (messageParam) {
    result.push({ type: 'text', text: messageParam });
  }
  return result;
}
