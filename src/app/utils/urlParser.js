/*
USAGE:
url = http://example.com:3000/pathname/?search=test#hash
parser.protocol; // => "http:"
parser.host;     // => "example.com:3000"
parser.hostname; // => "example.com"
parser.port;     // => "3000"
parser.pathname; // => "/pathname/"
parser.hash;     // => "#hash"
parser.search;   // => "?search=test"
parser.origin;   // => "http://example.com:3000"
*/

export function parseUrl(url) {
  let parser = document.createElement('a');
  parser.href = url;
  return parser;
}
