export const isEdgeBrowser = () => {
  const ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones*/
  var is_ie = ua.indexOf('Edge/') > -1;
  return is_ie;
};
