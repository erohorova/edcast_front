/**
 * Created by dunice on 08.08.2018.
 */

export default function convertRichText(str, link) {
  let newStr = str;
  newStr = newStr && newStr.replace(/&amp;/g, '&');
  newStr = newStr && newStr.replace(/&lt;/g, '<');
  newStr = newStr && newStr.replace(/&gt;/g, '>');
  newStr = newStr && newStr.replace(/\n|↵/gi, '\n');
  return newStr || '';
}
