export function addEvent(el, type, handler) {
  if (el.attachEvent) el.attachEvent('on' + type, handler);
  else el.addEventListener(type, handler);
}
export function removeEvent(el, type, handler) {
  if (el.detachEvent) el.detachEvent('on' + type, handler);
  else el.removeEventListener(type, handler);
}
