import * as crypto from 'crypto';
import { parseUrl } from './urlParser';
import SimpleCrypto from 'simple-crypto-js';
import * as Aes from './aes';

export default function addSecurity(url, seconds, user_id) {
  if (!!window.process.env.SECURE_AUTHENTICATED_IMAGES) {
    return filestackUrlWithProxy(url, seconds, user_id);
  } else {
    return filestackUrlWithoutProxy(url, seconds);
  }
}

function filestackUrlWithoutProxy(url, seconds) {
  if (url && isFilestackUrl(url)) {
    let cipherText =
      window.process.env.FILESTACK_CIPHER ||
      '9451781ec3129cdca9dc6d12371c3b7a65b7ccd4a5bc66935b0966835ad408bdvefzMak1/EjB61JKx7MptnK9e/m8WDnXGg+1U1jd7+Y=';
    let json = { expiry: expiryTime(seconds), call: ['convert'] };
    let policy = new Buffer(JSON.stringify(json)).toString('base64');
    let signature = crypto
      .createHmac('sha256', decryptSecretKey(cipherText))
      .update(policy)
      .digest('hex');
    let handle = getHandle(url);
    let secured_url = `https://cdn.filestackcontent.com/security=p:${policy},s:${signature}/${handle}`;
    return secured_url;
  } else {
    return url;
  }
}

function filestackUrlWithProxy(url, seconds, user_id) {
  let AES_SECRET =
    window.process.env.SECURE_AUTHENTICATED_IMAGES_KEY ||
    'c0ec98a5cd76da3152f7eb0ea2a43805c98d3a7490468bf84c5c38de3d3db686DNOszgsmqp80kuipa0vfQPD7oZhFaS7sHWqlxOhKb8ECRsD/5To3vl4obaTYYGgV';
  let unsecuredUrl = url;
  if (unsecuredUrl && isEncryptedFilestackUrl(unsecuredUrl)) {
    let encryptedText = getHandle(unsecuredUrl);
    let decrypted = Aes.aesDecrypt(encryptedText, decryptSecretKey(AES_SECRET));
    unsecuredUrl = decrypted.filestack_url;
  }

  if (unsecuredUrl && isFilestackUrl(unsecuredUrl)) {
    let filestackUrl = addSecurityParameters(unsecuredUrl, seconds, user_id);
    let encrypted = Aes.aesEncrypt(filestackUrl, decryptSecretKey(AES_SECRET));
    return `https://${window.location.hostname}/uploads/${encrypted}`;
  } else {
    return unsecuredUrl;
  }
}

function addSecurityParameters(url, seconds, user_id) {
  let secured_url = filestackUrlWithoutProxy(url, seconds);
  let obj = { filestack_url: secured_url, user_id: user_id };
  return JSON.stringify(obj);
}

function isEncryptedFilestackUrl(url) {
  return url.indexOf(window.location.hostname + '/uploads/') > -1;
}

function decryptSecretKey(cipherText) {
  let FILESTACK_SECRET =
    window.process.env.FILESTACK_SECRET ||
    'bd4687de495c1b7292b40189c9b68c0338ee2ea2bda7981c5cfc5058995cbb73aGMuDa0R1BQSP/cbscg3XluIikewiJ8lTjTD22+IxwY=';
  let simpleCrypto = new SimpleCrypto(FILESTACK_SECRET);
  return simpleCrypto.decrypt(cipherText);
}

function expiryTime(seconds) {
  let date = parseInt(new Date().getTime() / 1000) + seconds;
  return date;
}

function getHandle(url) {
  let parser = parseUrl(url);
  let pathname = parser.pathname;
  return pathname.split('/').pop();
}

function isFilestackUrl(src) {
  let parser = parseUrl(src);
  return parser.hostname == 'cdn.filestackcontent.com';
}
