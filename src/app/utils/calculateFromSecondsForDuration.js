export default function calculateFromSeconds(seconds, returnNumber = false) {
  let day = 86400;
  let hour = 3600;
  let month = 2592000;

  let months = Math.floor(seconds / month);

  let total_months_converted_to_days = months * 30;

  let days = Math.floor(seconds / day - total_months_converted_to_days);

  let total_days_converted_to_hours = days * 24;
  let total_months_converted_to_hours = months * 24 * 30;

  let hours = Math.floor(
    seconds / hour - total_months_converted_to_hours - total_days_converted_to_hours
  );

  let total_months_converted_to_minutes = months * 60 * 24 * 30;
  let total_days_converted_to_minutes = days * 24 * 60;
  let total_hours_converted_to_minutes = hours * 60;

  let mins = Math.floor(
    seconds / 60 -
      total_months_converted_to_minutes -
      total_days_converted_to_minutes -
      total_hours_converted_to_minutes
  );

  if (returnNumber) {
    months = ('0' + months).slice(-2);
    days = ('0' + days).slice(-2);
    hours = ('0' + hours).slice(-2);
    mins = ('0' + mins).slice(-2);
    return `${months} ${days} ${hours} ${mins}`;
  } else {
    return `${months}m ${days}d ${hours}h ${mins}m`;
  }
}
