import PubNubReact from 'pubnub-react';
import { PUBNUB_PUBLISH_KEY, PUBNUB_SUBSCRIBE_KEY } from '../constants/pubnubKeys';

const pubnubAdapter = new PubNubReact({
  ssl: true,
  publishKey: PUBNUB_PUBLISH_KEY,
  subscribeKey: PUBNUB_SUBSCRIBE_KEY
});

export default pubnubAdapter;
