import * as crypto from 'crypto';

export function aesEncrypt(string, key) {
  let iv = crypto.randomBytes(16);
  let pass = new Buffer(
    crypto
      .createHash('md5')
      .update(key)
      .digest('hex'),
    'hex'
  );
  let cipher = crypto.createCipheriv('aes-128-cbc', pass, iv);
  let encrypted = cipher.update(string);
  let finalBuffer = Buffer.concat([encrypted, cipher.final()]);
  let encryptedHex = iv.toString('hex') + ':' + finalBuffer.toString('hex');
  return encryptedHex;
}

export function aesDecrypt(encryptedText, key) {
  let encryptedArray = decodeURIComponent(encryptedText).split(':');
  let decipher = crypto.createDecipheriv(
    'aes-128-cbc',
    new Buffer(
      crypto
        .createHash('md5')
        .update(key)
        .digest('hex'),
      'hex'
    ),
    new Buffer(encryptedArray[0], 'hex')
  );
  let decryptedText = Buffer.concat([
    decipher.update(new Buffer(encryptedArray[1], 'hex')),
    decipher.final()
  ]).toString();
  return JSON.parse(decryptedText);
}
