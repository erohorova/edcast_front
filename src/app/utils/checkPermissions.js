let _rolePermissions = false;

export class Permissions {
  set enabled(permissions) {}
  get enabled() {
    return _rolePermissions;
  }
}

Permissions.has = function(permission) {
  if (this.enabled) {
    for (var i = 0; i < this.enabled.length; i++) {
      if (this.enabled[i] === permission) {
        return true;
      }
    }
  }
  return false;
};
