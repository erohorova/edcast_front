export default function handleGuideMeHelpClick(version) {
  if (version === 'pro') {
    // FIXME: GuideMeClientXt is not defined
    /*eslint no-undef: "off"*/
    if (typeof GuideMeClientXt != 'undefined') {
      GuideMeClientXt.openAppPanel();
    }
  } else if (version === 'ent') {
    /*eslint no-undef: "off"*/
    if (typeof GmCXt != 'undefined') {
      GmCXt.openAppPanel();
    }
  }
}
