export default function linkify(inputText) {
  let replacedText, replacePatternIgnoreA1, replacePatternIgnoreA2, replacePatternIgnoreA3;

  //URLs starting with http://, https://, or ftp://
  replacePatternIgnoreA1 = /((\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])(?![^<>]*>|[^"]*?<\/a))/gim;
  replacedText = inputText.replace(replacePatternIgnoreA1, '<a href="$1" target="_blank">$1</a>');

  //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  replacePatternIgnoreA2 = /((\b(www\.)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])(?![^<>]*>|[^"]*?<\/a))/gim;
  replacedText = replacedText.replace(
    replacePatternIgnoreA2,
    '<a href="http://$1" target="_blank">$1</a>'
  );

  //Change email addresses to mailto:: links.
  replacePatternIgnoreA3 = /((([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)(?![^<>]*>|[^"]*?<\/a))/gim;
  replacedText = replacedText.replace(replacePatternIgnoreA3, '<a href="mailto:$1">$1</a>');

  return replacedText;
}
