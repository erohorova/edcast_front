export default function listMenu(obj) {
  let TopMenu = [];
  if (obj) {
    Object.keys(obj).forEach(key => {
      let listObj = obj[key];
      listObj['key'] = key;
      if (!listObj.index) {
        listObj['index'] = -1;
      }

      // Change home link if not first item
      if (listObj.defaultLabel == 'Home' && listObj.index != 0) {
        listObj.defaultUrl = '/feed';
      }
      if (listObj.visible !== false) {
        TopMenu.push(listObj);
      }
    });

    TopMenu.sort((a, b) => {
      if (a.index < b.index) {
        return -1;
      }
      if (a.index > b.index) {
        return 1;
      }
      return 0;
    });

    // Set first menu item to '/'
    TopMenu[0].defaultUrl = '/';
  }
  return TopMenu;
}

// // Order
// export function orderMenu(obj) {
//   let topMenu = [];
//   Object.keys(this.props.team.OrgConfig.topMenu).map((key) => {
//     let menu = this.props.team.OrgConfig.topMenu[key];
//     topMenu.splice(menu.index, 0, menu);
//   });
//   return topMenu;
// }
