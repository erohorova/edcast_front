/**
 * update video card thumbnail & video
 *
 */
import { videoTranscode, refreshFilestackUrl } from 'edc-web-sdk/requests/filestack';
import { postv2, fetchCard, fetchCardForStandaloneLayout } from 'edc-web-sdk/requests/cards';
import { parseUrl } from './urlParser';
import addSecurity from './filestackSecurity';

function update_card(state_card, card, isUpdated, filestack_thumb) {
  let default_thumb =
    state_card.filestack[0].thumbnail === undefined || state_card.filestack[0].thumbnail === ''
      ? ''
      : state_card.filestack[0].thumbnail;
  let old_custom_thumb =
    state_card.filestack[1] &&
    (state_card.filestack[1].url !== undefined || state_card.filestack[1].url !== '')
      ? state_card.filestack[1].url
      : '';
  let new_custom_thumb =
    card.filestack[1] && (card.filestack[1].url !== undefined || card.filestack[1].url !== '')
      ? card.filestack[1].url
      : '';
  let is_thumb_updated =
    (new_custom_thumb !== old_custom_thumb || new_custom_thumb !== default_thumb) &&
    new_custom_thumb !== '' &&
    default_thumb !== '';
  let is_video_updated = false;

  if (isUpdated) {
    is_video_updated = state_card.filestack[0].url !== card.filestack[0].url;
  }

  if (default_thumb === '' || is_video_updated || is_thumb_updated) {
    return {
      default_thumb: default_thumb,
      is_thumb_updated: is_thumb_updated,
      new_custom_thumb: new_custom_thumb,
      filestack_thumb: filestack_thumb
    };
  } else {
    return false;
  }
}

function getCardAPIResponse(card, payload) {
  if (payload['is_thumb_updated']) {
    card.filestack[0].thumbnail = payload['new_custom_thumb'];
  } else if (payload['default_thumb'] === '') {
    card.filestack[0].thumbnail = payload['filestack_thumb'] || '';
  }

  return {
    card: {
      all_ratings: card.allRatings,
      auto_complete: card.autoComplete,
      average_rating: card.averageRating,
      card_subtype: card.cardSubtype,
      card_type: card.cardType,
      channel_ids: card.channelIds,
      channels: card.channels,
      duration: card.duration,
      message: card.message,
      hidden: card.hidden,
      comments_count: card.commentsCount,
      completed_percentage: card.completedPercentage,
      completion_state: card.completionState,
      ecl_duration_metadata: card.eclDurationMetadata,
      file_resources: card.fileResources,
      filestack: card.filestack,
      is_assigned: card.isAssigned + '',
      is_bookmarked: card.isBookmarked + '',
      is_official: card.isOfficial + '',
      is_paid: card.isPaid + '',
      is_public: card.isPublic + '',
      is_upvoted: card.isUpvoted + '',
      mark_feature_disabled_for_source: card.markFeatureDisabledForSource + '',
      non_curated_channel_ids: card.nonCuratedChannelIds,
      paid_by_user: card.paidByUser + '',
      readable_card_type: card.readableCardType,
      share_url: card.shareUrl,
      skill_level: card.skillLevel,
      users_with_access_ids:
        (card.usersWithAccess && card.usersWithAccess.map(user => user.id)) || [],
      team_ids: (card.teams && card.teams.map(team => team.id)) || []
    }
  };
}

function cardFetching(state_card, card, isUpdated, data, isStandalone) {
  let res = update_card(state_card, card, isUpdated, data.data.thumb);
  if (res) {
    fetchCard(card.id)
      .then(newCard => {
        let payloadResponse = getCardAPIResponse(newCard, res);
        postv2(payloadResponse, encodeURIComponent(card.id))
          .then(cdata => {
            if (cdata.card) {
              this.setState({
                card: cdata.card
              });
            }
          })
          .catch(err => {
            console.error(`Error in getTranscodedVideo.postv2.func status === 'completed: ${err}`);
          });
      })
      .catch(err => {
        console.error(`Error in getTranscodedVideo.fetchCard.func status === 'completed': ${err}`);
      });
  } else {
    if (isUpdated) {
      if (isStandalone) {
        fetchCardForStandaloneLayout(card.id)
          .then(newCard => {
            if (newCard && newCard.card) {
              this.setState({
                card: newCard.card
              });
            }
          })
          .catch(err => {
            console.error(
              `Error in getTranscodedVideo.fetchCardForStandaloneLayout.func status === 'completed': ${err}`
            );
          });
      } else {
        fetchCard(card.id)
          .then(newCard => {
            if (newCard && newCard.card) {
              this.setState({
                card: newCard.card
              });
            }
          })
          .catch(err => {
            console.error(
              `Error in getTranscodedVideo.fetchCard.func status === 'completed': ${err}`
            );
          });
      }
    }
  }
}

function updateStateWithTranscodedData(comp, transcodedData) {
  if (transcodedData.status == 'completed') {
    refreshFilestackUrl(transcodedData.data.url)
      .then(resp => {
        let transcodedSecuredVideoUrl = resp.signed_url;
        comp.setState({
          transcodedVideoUrl: transcodedSecuredVideoUrl,
          transcodedVideoStatus: transcodedData.status
        });
      })
      .catch(err => {
        console.error(
          `Error in getTranscodedVideo.updateStateWithTranscodedData.refreshFilestackUrl.func status === 'completed': ${err}`
        );
      });
  } else {
    comp.setState({
      transcodedVideoUrl: null,
      transcodedVideoStatus: transcodedData.status
    });
  }
}

export default function getTranscodedVideo(
  state_card,
  card,
  userId,
  isUpdated,
  isStandalone = false
) {
  if (
    card &&
    card.filestack &&
    card.filestack.length &&
    card.filestack[0].mimetype &&
    card.filestack[0].mimetype.includes('video')
  ) {
    let securedUrl = addSecurity(card.filestack[0].url, 3600, userId);
    let securedVideoPath = parseUrl(securedUrl).pathname;
    videoTranscode(securedVideoPath)
      .then(data => {
        updateStateWithTranscodedData(this, data);
        if (data.status === 'completed') {
          cardFetching(state_card, card, isUpdated, data, isStandalone);
        } else {
          this.videoTranscodeEditInterval = setInterval(() => {
            refreshFilestackUrl(card.filestack[0].url)
              .then(resp => {
                let securedVideoUrl = resp.signed_url;
                securedVideoPath = parseUrl(securedVideoUrl).pathname;

                videoTranscode(securedVideoPath)
                  .then(dataTranscode => {
                    updateStateWithTranscodedData(this, dataTranscode);
                    if (dataTranscode.status === 'completed') {
                      clearInterval(this.videoTranscodeEditInterval);
                      cardFetching(state_card, card, isUpdated, dataTranscode, isStandalone);
                    }
                  })
                  .catch(err => {
                    console.error(
                      `Error in getTranscodedVideo.videoTranscode.func status !== 'completed': ${err}`
                    );
                  });
              })
              .catch(err => {
                console.error(
                  `Error in getTranscodedVideo.videoTranscode.refreshFilestackUrl.func status !== 'completed': ${err}`
                );
              });
          }, 10000);
        }
      })
      .catch(err => {
        console.error(`Error in getTranscodedVideo.videoTranscode.func: ${err}`);
      });
  }
}
