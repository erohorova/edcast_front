/**
 * calculate teem feed position
 *
 * @return {Object}
 */

export default function calculateTeemFeedPosition() {
  let feedOffset = 0;
  let homePagev1 = window.ldclient.variation('home-page-fix-v1', false);
  let userOffset = homePagev1 ? 15 : 0;
  let doc = /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
    ? document.body
    : document.documentElement;
  let fixedHeader = document.querySelector('#secondary-nav.fixed');
  let reducingHeader = document.querySelector('.reducing-header');
  let footer = document.getElementById('footer');
  let topNav, defaultSize, secNav;
  let userPanelContainer = document.getElementById('user-panel-container');
  let userpanel = !!userPanelContainer;
  let isFeedV2 = window.ldclient.variation('feed-style-v2', false);
  let topNavs = document.querySelectorAll('[class*=topnav-main-container]');
  topNav = topNavs ? topNavs[0] : undefined;

  let docViewTop = doc.scrollTop,
    docViewBottom = docViewTop + doc.clientHeight,
    elemTop = footer && footer.offsetTop,
    elemBottom = elemTop;

  let footerOffset =
    footer && elemBottom <= docViewBottom && elemTop >= docViewTop ? docViewBottom - elemBottom : 0;
  let footerUserOffset = footerOffset;

  if (isFeedV2) {
    if (topNav) {
      if (reducingHeader) {
        feedOffset = reducingHeader.offsetHeight;
      } else {
        if (doc.scrollTop > topNav.offsetHeight) {
          if (fixedHeader) {
            feedOffset = fixedHeader.offsetHeight;
          } else {
            feedOffset = topNav.offsetHeight;
          }
        } else {
          feedOffset =
            topNav.offsetHeight - doc.scrollTop > 86 ? topNav.offsetHeight - doc.scrollTop : 86;
        }
      }
    } else {
      feedOffset = defaultSize;
    }
    userOffset = `${userOffset + feedOffset + 48}px`;
    footerUserOffset = `${footerUserOffset}px`;
  } else {
    if (fixedHeader) {
      feedOffset = fixedHeader.offsetHeight;
      feedOffset += 10;
      userOffset = `${feedOffset + 36}px`;
      footerUserOffset = `${footerOffset + 36}px`;
    } else {
      if (reducingHeader && doc.scrollTop > 40) {
        feedOffset = reducingHeader.offsetHeight + 10;
        userOffset = `${feedOffset + 36}px`;
        footerUserOffset = `${footerOffset + 36}px`;
      } else {
        if (userpanel) {
          feedOffset =
            document.getElementsByClassName('home-redesign')[0] && topNav
              ? document.getElementsByClassName('home-redesign')[0].offsetTop - doc.scrollTop
              : 172;
        } else {
          feedOffset = topNav ? topNav.offsetHeight - doc.scrollTop : defaultSize;
          feedOffset += 10;
        }
        userOffset = `${feedOffset}px`;
        footerUserOffset = `${footerOffset}px`;
      }
    }
  }

  feedOffset = `${feedOffset}px`;
  footerOffset = `${footerOffset}px`;

  let leftRailContainer = document.getElementById('left-rail-container');
  let rightRailContainer = document.getElementById('right-rail-container');

  if (!!userPanelContainer) {
    userPanelContainer.style.height = `calc(100% - ${userOffset} - ${footerUserOffset})`;
    userPanelContainer.style.top = userOffset;
  }
  if (!!leftRailContainer) {
    let bottomSpace = '0px';
    if (!isFeedV2) {
      bottomSpace = '0.625rem';
    }
    leftRailContainer.style.height = `calc(100% - ${feedOffset} - ${footerOffset} - ${bottomSpace})`;
    leftRailContainer.style.top = feedOffset;
  }

  if (!!rightRailContainer) {
    if (isFeedV2) {
      footerOffset = '0px';
    }
    rightRailContainer.style.height = `calc(100% - ${feedOffset} - ${footerOffset})`;
    rightRailContainer.style.top = feedOffset;
  }

  return { feedOffset, footerOffset, userOffset, footerUserOffset };
}
