/**
 * Checking user inactivity Initialization
 *
 * @param {Number} [timeout] - Inactive Web session time out
 * @param {String} [signOutLink] - link to sign out
 */

import throttle from 'lodash/throttle';

export default function checkingUserInactivity(timeout, signOutLink) {
  let signOutLinkParam = signOutLink;
  let interval;
  if (timeout > 0) {
    localStorage['inactivityTimeout'] = Date.now() + timeout;
    document.onclick = function() {
      localStorage['inactivityTimeout'] = Date.now() + timeout;
    };
    document.onmousemove = throttle(() => {
      localStorage['inactivityTimeout'] = Date.now() + timeout;
    }, 1000);
    document.onkeypress = function() {
      localStorage['inactivityTimeout'] = Date.now() + timeout;
    };
    window.addEventListener(
      'scroll',
      throttle(() => {
        localStorage['inactivityTimeout'] = Date.now() + timeout;
      }, 1000)
    );
    interval = setInterval(() => {
      let _idleSecondsCounter = localStorage['inactivityTimeout'];
      if (
        (_idleSecondsCounter > 0 && Date.now() >= _idleSecondsCounter) ||
        _idleSecondsCounter === undefined
      ) {
        delete localStorage['inactivityTimeout'];
        clearInterval(interval);
        localStorage['isLoggedIn'] = false;
        signOutLinkParam = signOutLinkParam ? signOutLinkParam : '/sign_out';
        window.location.href = signOutLinkParam;
      }
    }, 5000);
  } else {
    localStorage['inactivityTimeout'] = 0;
  }
}
