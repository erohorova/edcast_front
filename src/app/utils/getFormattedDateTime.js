export default function getFormattedDateTime(dateTime, format) {
  let monthNamesShort = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec'
  ];

  let date = new Date(dateTime) || new Date();
  let day = ('0' + date.getDate()).substr(-2);
  let monthIndex = date.getMonth();
  let year = date.getFullYear();

  // Default format 'DD MMM YYYY'
  let formattedDateTime = day + ' ' + monthNamesShort[monthIndex] + ' ' + year;

  switch (format) {
    case 'DD MMM YYYY':
      break;
    case 'DD/MM/YYYY':
      formattedDateTime = day + '/' + ('0' + (monthIndex + 1)).substr(-2) + '/' + year;
      break;
    default:
    // noop
  }

  return formattedDateTime;
}
