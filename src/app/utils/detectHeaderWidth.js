export default function detectHeaderWidth() {
  let doc = /^((?!chrome|android).)*safari/i.test(navigator.userAgent)
    ? document.body
    : document.documentElement;
  return doc.clientWidth < 1094;
}
