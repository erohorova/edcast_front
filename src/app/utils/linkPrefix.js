export default function linkPrefix(cardType) {
  let prefix;
  if (cardType === 'VideoStream' || cardType === 'video_stream') {
    prefix = 'video_streams';
  } else if (cardType === 'pack' || cardType === 'pack_draft') {
    prefix = 'pathways';
  } else if (cardType === 'journey') {
    prefix = 'journey';
  } else {
    prefix = 'insights';
  }
  return prefix;
}
