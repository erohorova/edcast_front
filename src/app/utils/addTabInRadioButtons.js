export default function addTabInRadioButtons(groupName, callback) {
  let radioList = document.querySelectorAll(`.${groupName}`);
  let radioListLength = radioList ? radioList.length : 0;
  let groups = [];
  for (let i = 0; i < radioListLength; i++) {
    let el = radioList[i];
    el.setAttribute('tabIndex', '0');
    let thisGroup1 = (groups[el.name] = groups[el.name] || []);
    thisGroup1.push(el);
    el.addEventListener('keydown', e => {
      setTimeout(function() {
        let eventTarget = e.target;
        let thisGroup = (groups[eventTarget.name] = groups[eventTarget.name] || []);
        let indexOfTarget = thisGroup.indexOf(e.target);
        if (e.keyCode === 9) {
          if (indexOfTarget < thisGroup.length - 1 && !e.shiftKey) {
            thisGroup[indexOfTarget + 1].focus();
          } else if (indexOfTarget > 0 && e.shiftKey) {
            thisGroup[indexOfTarget - 1].focus();
          }
        }
        if (e.keyCode === 13) {
          let t = thisGroup[indexOfTarget].children[0];
          callback(e, t.value);
          return 0;
        }
      });
    });
  }
}
