export default function addParamStart(embedHtml, videoUrl) {
  let startIndex = videoUrl.indexOf('&start=');
  if (!~startIndex) return embedHtml;
  let nextParamIndex = videoUrl.indexOf('&', startIndex + 1);
  let startParam = videoUrl.slice(startIndex, ~nextParamIndex ? nextParamIndex : videoUrl.length);
  let embedHtmlArr = embedHtml.__html.split('"');
  let srcValueIndex = embedHtmlArr.findIndex(item => item === ' src=') + 1;
  embedHtmlArr[srcValueIndex] = `${embedHtmlArr[srcValueIndex]}${startParam}`;
  return { __html: embedHtmlArr.join('"') };
}
