import { getTypeCards, getSingleChannel } from '../actions/channelsActionsV2';

export default function updateChannel(dispatch, currentChannel, limit, isUpdatingCurrent) {
  dispatch(getSingleChannel(currentChannel.slug, isUpdatingCurrent))
    .then(() => {
      dispatch(getTypeCards(currentChannel.id, ['video_stream'], limit));
      dispatch(getTypeCards(currentChannel.id, ['pack'], limit));
      dispatch(getTypeCards(currentChannel.id, ['media', 'poll'], limit));
      dispatch(getTypeCards(currentChannel.id, ['course'], limit));
      dispatch(getTypeCards(currentChannel.id, ['journey'], limit));
    })
    .catch(err => {
      console.error(`Error in updateChannel.updateChannel.func: ${err}`);
    });
}
