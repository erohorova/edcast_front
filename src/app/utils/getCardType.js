/**
 * Get current card type
 *
 * @param {Object} [card] - card data
 * @param {Boolean} [videoFileStack] - flag of attached video file by filestack
 * @param {Boolean} [fileFileStack] - flag of attached file (not image or video) by filestack
 * @return {String}
 */

export default function getCardType(card, videoFileStack, fileFileStack) {
  let cardType = '';
  let switchType = card.cardType || card.content_type; // Smartsearch uses content_type
  switch (switchType) {
    case 'pack':
    case 'pathway':
      cardType = 'PATHWAY';
      break;
    case 'journey':
      cardType = 'JOURNEY';
      break;
    case 'video_stream':
    case 'video':
    case 'VideoStream':
    case 'video.other':
      cardType = 'VIDEO';
      break;
    case 'Article':
    case 'article':
    case 'insight':
      cardType = 'ARTICLE';
      break;
    case 'media':
      switch (card.cardSubtype) {
        case 'link':
          cardType = 'ARTICLE';
          break;
        case 'file':
          cardType = 'FILE';
          break;
        case 'video':
          cardType = 'VIDEO';
          break;
        case 'audio':
          cardType = 'AUDIO';
          break;
        case 'image':
          cardType = 'IMAGE';
          break;
        default:
          cardType = card.cardSubtype || 'FILE';
          break;
      }
      break;
    case 'poll':
      cardType = card.isAssessment ? 'QUIZ' : 'POLL';
      break;
    case 'course':
      cardType = 'COURSE';
      break;
    case 'project':
      cardType = 'PROJECT';
      break;
    default:
      cardType = 'ARTICLE';
      break;
  }
  if (
    card.cardSubtype === 'text' &&
    card.cardType !== 'poll' &&
    card.cardType !== 'project' &&
    card.cardType !== 'pack' &&
    card.cardType !== 'course'
  ) {
    cardType = videoFileStack ? 'VIDEO' : fileFileStack ? 'FILE' : 'TEXT';
  }
  if (
    card.filestack &&
    card.filestack.length > 1 &&
    card.filestack[0].mimetype &&
    ~card.filestack[0].mimetype.indexOf('video/') &&
    ~card.filestack[1].mimetype.indexOf('image/')
  ) {
    cardType = 'VIDEO';
  }
  return cardType;
}
