import getDefaultImage from './getDefaultCardImage';

/**
 * Set thumbnail image
 *
 * @param {number} [cardId] - card id
 * @return {string}
 */

export default function thumbnailImage(cardId, imageUrl, userId) {
  return `<img style="float: left; margin-right: 1rem" src="${imageUrl}" onerror="this.onerror=null;this.src=&quot;${setDefaultImage(
    cardId,
    userId
  )}&quot;;" />`;
}

function setDefaultImage(cardId, userId) {
  let cardImages = localStorage.getItem('cardImages');
  cardImages = cardImages ? JSON.parse(cardImages) : {};
  if (cardImages[cardId]) {
    return cardImages[cardId];
  } else {
    let img = getDefaultImage(userId).url;
    cardImages[cardId] = img;
    localStorage.setItem('cardImages', JSON.stringify(cardImages));
    return img;
  }
}
