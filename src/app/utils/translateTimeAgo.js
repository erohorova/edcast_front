import frStrings from 'react-timeago/lib/language-strings/fr';
import enStrings from 'react-timeago/lib/language-strings/en';
import ruStrings from 'react-timeago/lib/language-strings/ru';
import svStrings from 'react-timeago/lib/language-strings/sv';
import nlStrings from 'react-timeago/lib/language-strings/nl';
import itStrings from 'react-timeago/lib/language-strings/it';
import plStrings from 'react-timeago/lib/language-strings/pl';
import esStrings from 'react-timeago/lib/language-strings/es';
import deStrings from 'react-timeago/lib/language-strings/de';
import ptStrings from 'react-timeago/lib/language-strings/pt';
import elStrings from 'react-timeago/lib/language-strings/el';
import csStrings from 'react-timeago/lib/language-strings/cs';
import srStrings from 'react-timeago/lib/language-strings/sr';
import ukStrings from 'react-timeago/lib/language-strings/uk';
import trStrings from 'react-timeago/lib/language-strings/tr';
import ltStrings from 'react-timeago/lib/language-strings/lt';
import huStrings from 'react-timeago/lib/language-strings/hu';
import jaStrings from 'react-timeago/lib/language-strings/ja';
import zhCNStrings from 'react-timeago/lib/language-strings/zh-CN';

import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';

/**
 * Get translate formatter
 *
 * @param {string} [currentLanguage] - current language key
 * @return {Object} [formatter] - formatter for timeAgo
 */

export default function translateTA(currentLanguage) {
  let lgKey;
  switch (currentLanguage) {
    case 'nl-BE':
      lgKey = nlStrings;
      break;
    case 'fr-CA':
      lgKey = frStrings;
      break;
    case 'cs-CZ':
      lgKey = csStrings;
      break;
    case 'sr-Latn-RS':
      lgKey = srStrings;
      break;
    case 'uk-UA':
      lgKey = ukStrings;
      break;
    case 'tr-TR':
      lgKey = trStrings;
      break;
    case 'lt-LT':
      lgKey = ltStrings;
      break;
    case 'hu-HU':
      lgKey = huStrings;
      break;
    case 'ja-JP':
      lgKey = jaStrings;
      break;
    case 'ru':
      lgKey = ruStrings;
      break;
    case 'sv':
      lgKey = svStrings;
      break;
    case 'it':
      lgKey = itStrings;
      break;
    case 'pl':
      lgKey = plStrings;
      break;
    case 'es':
      lgKey = esStrings;
      break;
    case 'de':
      lgKey = deStrings;
      break;
    case 'pt':
      lgKey = ptStrings;
      break;
    case 'el':
      lgKey = elStrings;
      break;
    case 'zh-CN':
      lgKey = zhCNStrings;
      break;
    case 'en':
    default:
      lgKey = enStrings;
      break;
  }
  return buildFormatter(lgKey);
}
