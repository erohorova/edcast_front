/**
 * Get video status data
 *
 * @param {Object} [card] - card data
 * @return {Object}
 */

export default function getVideoStatus(card) {
  let videoLabelText = '',
    videoLabelClass = '',
    channelCardLable = '';

  let videoStatus = card.videoStream ? card.videoStream.status : card.status;

  switch (videoStatus) {
    case 'upcoming':
      videoLabelText = 'scheduled stream';
      videoLabelClass = 'video-label-type scheduled-stream';
      channelCardLable = 'SCHEDULED STREAM';
      break;
    case 'live':
      videoLabelText = 'live stream';
      videoLabelClass = 'video-label-type live-stream';
      channelCardLable = 'LIVE STREAM';
      break;
    case 'past':
      videoLabelText = 'past stream';
      videoLabelClass = 'video-label-type past-stream';
      channelCardLable = 'PAST STREAM';
      break;
    default:
      // FIXME: implement default case
      break;
  }

  return { videoStatus, videoLabelText, videoLabelClass, channelCardLable };
}
