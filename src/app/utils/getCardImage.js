/**
 * Get card image url
 *
 * @param {Object} [card] - card data
 * @param {Boolean} [imageFileStack] - flag of attached image by filestack
 * @return {String}
 */

export default function getCardImage(card, imageFileStack) {
  let cardImage = '';

  if (
    card.resource &&
    card.resource.type &&
    card.resource.type.toLowerCase() === 'video' &&
    card.resource.imageUrl
  ) {
    cardImage = card.resource.imageUrl;
  } else if (imageFileStack) {
    cardImage = card.filestack[0].url;
  } else if (card.fileResources && card.fileResources.length && card.fileResources[0].fileUrl) {
    cardImage = card.fileResources[0].fileUrl;
  } else if (card.resource && card.resource.imageUrl) {
    cardImage = card.resource.imageUrl;
  } else if (card.videoStream && card.videoStream.imageUrl) {
    cardImage = card.videoStream.imageUrl;
  } else if (card.image) {
    //Smartsearch required
    cardImage = card.image;
  }

  return cardImage;
}
