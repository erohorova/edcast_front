/**
 * Check card data resource and set params to resource object
 *
 * @param {Object} [card] - card data
 * @return {Object}
 */

export default function checkResources(card) {
  if (
    card.fileResources &&
    card.fileResources.length &&
    card.fileResources[0].fileUrls &&
    !(card.fileResources[0].filestack && card.fileResources[0].filestack.scorm_course) &&
    !(card.resource && card.resource.type && card.resource.type.toLowerCase() === 'video')
  ) {
    card.resource = {
      imageUrl: card.fileResources[0].fileUrls.large_url,
      filestack: !!card.fileResources[0].filestack,
      url: (card.resource && card.resource.url) || ''
    };
  }

  if (card.resource && card.resource !== undefined) {
    card.resource.hrefLink = card.resource.slug;
    card.resource.url = (card.resource && card.resource.url) || '';
    if (typeof card.id === 'string' && card.id.indexOf('ECL') > -1) {
      card.resource.hrefLink = card.resource.url;
    }
  } else {
    card.resource = {};
  }

  return card;
}
