/**
 * Created by dunice on 19.02.2019.
 */

import { langs } from '../constants/languages';
import { tr } from 'edc-web-sdk/helpers/translations';

export default function checkLabelCustomLang(teamConfig, currentUser, langConfig) {
  let profileLanguage;
  if (!langConfig) return;
  for (let prop in langs) {
    if (currentUser.profile && langs[prop] === currentUser.profile.language)
      profileLanguage = prop.toLowerCase();
  }
  let isShowCustomLabels = teamConfig && teamConfig.custom_labels;
  let translatedLabelConfig =
    isShowCustomLabels &&
    langConfig.languages &&
    langConfig.languages[profileLanguage] &&
    langConfig.languages[profileLanguage].trim();
  if (langConfig.defaultLabel === 'Continuous Learning') {
    return translatedLabelConfig || tr(langConfig.label || 'Continuous Learning Credits');
  }
  return translatedLabelConfig || tr(langConfig.label || langConfig.defaultLabel);
}
