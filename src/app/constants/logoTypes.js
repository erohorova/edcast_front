export const LOGO = {
  lynda: 'https://s3.amazonaws.com/ed-general/ecl-logos/lynda.png',
  get_abstract: 'https://s3.amazonaws.com/ed-general/ecl-logos/get_abstract.png',
  blinkist: 'https://s3.amazonaws.com/ed-general/ecl-logos/blinkist.png',
  crossknowledge: 'https://s3.amazonaws.com/ed-general/ecl-logos/crossknowledge.png',
  edx: 'https://s3.amazonaws.com/ed-general/ecl-logos/edx.png',
  box: 'https://s3.amazonaws.com/ed-general/ecl-logos/box.png',
  brainshark: 'https://s3.amazonaws.com/ed-general/ecl-logos/brainshark.png',
  treehouse: 'https://s3.amazonaws.com/ed-general/ecl-logos/treehouse.png',
  intuition: 'https://s3.amazonaws.com/ed-general/ecl-logos/intuition.png',
  khanacademy: 'https://s3.amazonaws.com/ed-general/ecl-logos/khanacademy.png',
  udacity: 'https://s3.amazonaws.com/ed-general/ecl-logos/udacity.png',
  udemy: 'https://s3.amazonaws.com/ed-general/ecl-logos/udemy.png',
  youtube: 'https://s3.amazonaws.com/ed-general/ecl-logos/youtube.png',
  goskills: 'https://s3.amazonaws.com/ed-general/ecl-logos/goskills.png',
  datacamp: 'https://s3.amazonaws.com/ed-general/ecl-logos/datacamp2.png',
  canvas: 'https://s3.amazonaws.com/ed-general/ecl-logos/canvas.png'
};
