export const cardFields =
  'id,completed_percentage,card_subtype,all_ratings,author,average_rating,card_metadatum,comments_count,completion_state,filestack,hidden,is_assigned,' +
  'is_bookmarked,is_clone,is_official,is_paid,is_public,is_upvoted,mark_feature_disabled_for_source,prices,provider,provider_image,share_url,' +
  'resource,state,votes_count,card_type,slug,badging,skill_level,title,message,ecl_duration_metadata,readable_card_type,is_new,quiz,video_stream,published_at,ecl_metadata,is_reported,teams_permitted,users_permitted';
