export const langs = {
  English: 'en',
  Russian: 'ru',
  French: 'fr',
  Swedish: 'sv',
  'Dutch (Belgium)': 'nl-BE',
  Italian: 'it',
  Polish: 'pl',
  'French (Canada)': 'fr-CA',
  Spanish: 'es',
  German: 'de',
  'Tamil (India)': 'ta-IN',
  Hindi: 'hi',
  Telugu: 'te',
  Gujarati: 'gu',
  'Marathi (India)': 'mr-IN',
  Portuguese: 'pt',
  Greek: 'el',
  Czech: 'cs-CZ',
  'Serbian (Serbia)': 'sr-Latn-RS',
  Ukrainian: 'uk-UA',
  Turkish: 'tr-TR',
  'Malay (Malaysia)': 'ms-Latn-MY',
  Lithuanian: 'lt-LT',
  Hungarian: 'hu-HU',
  Japanese: 'ja-JP',
  Chinese: 'zh-CN',
  'Chinese (Traditional)': 'zh-Hans-CN',
  'Portuguese (Brazil)': 'pt-BR',
  'Romanian (Romania)': 'ro-RO',
  Arabic: 'ar',
  Hebrew: 'he',
  'Slovak (Slovakia)': 'sk-SK',
  Urdu: 'ur',
  'Indonesian (Indonesia)': 'id-ID'
};

export let languageKeyVal = [];
for (const key in langs) {
  if (langs.hasOwnProperty(key)) {
    const temp = {};
    temp.name = key;
    temp.id = langs[key];
    languageKeyVal.push(temp);
  }
}
