//current user
export const RECEIVE_INIT_USER_INFO = 'receive_init_user_info';
export const RECEIVE_GROUPLEADER_USER_INFO = 'receive_groupleader_user_info';
export const ERROR_INIT_USER = 'error_init_user';
export const ERROR_INIT_USER_PUBLIC = 'error_init_user_public';
export const ERROR_USER_LOGIN = 'error_user_login';
export const USER_LOG_OUT = 'user_log_out';
export const RECEIVE_USER_AUTHENTICATED = 'receive_user_authenticated';
export const CHANGE_EMAIL_INPUT = 'change_email_input';
export const CHANGE_PASSWORD_INPUT = 'change_password_input';
export const RECEIVE_ORG_INFO = 'receive_org_info';
export const RECEIVE_FEED_TYPE_VIEW = 'receive_feed_type_view';
export const ERROR_INIT_ORG = 'error_init_org';
export const ERROR_UPDATED_USER_INFO = 'error_updated_user_info';
export const UPDATE_USER_DETAILS = 'update_user_details';
export const RECEIVE_WALLET_BALANCE = 'recieve_wallet_balance';
export const RECEIVE_WALLET_RECHARGES = 'recieve_wallet_recharges';
export const RECEIVE_WALLET_TRANSACTIONS = 'recive_wallet_transcations';
export const SHOW_WALLET_LOADING = 'show_wallet_loading';
export const HIDE_WALLET_LOADING = 'hide_wallet_loading';
export const FOLLOWING_CHANNEL_STATE = 'following_channel_state';
export const FETCH_NOTIFICATION_CONFIG = 'fetch_notification_config';
export const GET_CUSTOM_TOPICS = 'get_custom_topics';

//users
export const RECEIVE_USERS = 'receive_users';
export const RECEIVE_MENTION_USERS = 'receive_mention_users';
export const RECEIVE_LEADERBOARD_USERS = 'receive_leaderboard_users';
export const REQUEST_TOGGLE_USER_FOLLOW = 'request_toggle_user_follow';
export const RECEIVE_TOGGLE_USER_FOLLOW = 'receive_toggle_user_follow';
export const RECEIVE_INTEGRATION = 'receive_integrations';
export const GET_FOLLOWING_USERS = 'get_following_users';
export const SET_USER_BADGES = 'set_user_badges';
export const FOLLOWING_USER_STATE = 'following_user_state';
export const FOLLOWER_USER_STATE = 'follower_user_state';
export const GET_FOLLOWER_USERS = 'get_follower_users';
export const UPDATE_FOLLOWING_USERS_COUNT = 'update_following_users_count';
export const RECEIVE_SKILLS = 'receive_skills';
export const RECEIVE_PUBLIC_PROFILE = 'receive_public_profile';
export const LOADING_PUBLIC_PROFILE_CARDS = 'loading_public_profile_cards';
export const REMOVE_PUBLIC_PROFILE = 'remove_public_profile';
export const RECEIVE_PUBLIC_PROFILE_BASIC_INFO = 'receive_public_profile_basic_info';
export const UPDATE_USER_EXPERTISE_AND_INTEREST = 'update_user_expertise_and_interest';
export const ORG_SUBSCRIPTION_PAID = 'org_subscription_paid';
export const SET_CURRENT_USER_INFO = 'set_current_user_info';
export const UPDATE_REPORTED_CARDS = 'update_reported_cards';
export const UPDATE_REPORTED_COMMENTS = 'update_reported_comments';

//profile
export const RECEIVE_INIT_PROFILE = 'receive_init_profile';
export const RECEIVE_PROFILE_BADGES = 'receive_profile_badges';
export const RECEIVE_PROFILE_SKILLS = 'receive_profile_skills';
export const RECEIVE_PROFILE_COURSES = 'receive_profile_courses';
export const INTEGRATION_CONNECT_STATUS = 'integration_connect_status';
export const RECEIVE_USER_ACTIVITIES = 'receive_user_activities';
export const GET_IN_PROGRESS = 'get_in_progress';
export const SET_TOPIC_SCORE = 'set_topic_score';
export const UPDATE_PUBLIC_PROFILE_INFO = 'update_public_profile_info';
export const SET_ACTIVE_TOPICS = 'set_active_topics';
export const RECEIVE_USER_CONTENT = 'receive_user_content';
export const CHANGE_YEAR = 'change_year';
export const CHANGE_YEAR_OBJ = 'change_year_obj';

//channels
export const REQUEST_TOGGLE_CHANNEL_FOLLOW = 'request_toggle_channel_follow';
export const RECEIVE_TOGGLE_CHANNEL_FOLLOW = 'receive_toggle_channel_follow';
export const RECEIVE_CHANNELS = 'receive_channels';

// channels V2
export const REQUEST_TOGGLE_CHANNEL_FOLLOW_V2 = 'request_toggle_channel_follow_v2';
export const RECEIVE_TOGGLE_CHANNEL_FOLLOW_V2 = 'receive_toggle_channel_follow_v2';
export const RECEIVE_CHANNELS_V2 = 'receive_channels_v2';
export const RECEIVE_CHANNEL_CARDS = 'receive_channel_cards';
export const RECEIVE_CHANNEL_CARDS_AFTER_REORDERING = 'receive_channel_cards_after_reordering';
export const CHANGE_OFFSET = 'change_offset';
export const UPDATE_CURATORS = 'update_curators';
export const UPDATE_CHANNEL_DETAILS = 'update_channel_details';
export const REMOVE_CURATOR = 'remove_curator';
export const UPDATE_CHANNEL_CARD_PINNED_STATUS = 'update_channel_card_pinned_status';
export const FETCH_PINNED_CARDS = 'fetch_pinned_cards';
export const UPDATE_CHANNEL_CARD_PINNED_STATUS_IN_ALL = 'update_channel_card_pinned_status_in_all';
export const OPEN_CHANNEL_CARD_REMOVE_MODAL = 'open_channel_card_remove_modal';
export const REMOVE_CHANNEL_CARD = 'remove_channel_card';
export const UPDATE_CHANNEL_SLUG = 'update_channel_slug';
export const CREATE_CHANNEL = 'create_channel';
export const UPDATE_CHANNEL = 'update_channel';

//courses
export const RECEIVE_COURSES = 'receive_courses';

//carousels
export const RECEIVE_CAROUSELS = 'receive_carousels';
export const RECEIVE_CAROUSEL_ITEMS = 'receive_carousel_items';

//groups
export const RECEIVE_GROUPS = 'receive_groups';
export const OPEN_INVITE_USER_MODAL = 'open_invite__modal';
export const OPEN_INVITE_V2_MODAL = 'open_invite_v2_modal';
export const UPDATE_PENDING_MEMBER = 'update_pending_member';
//groups v2
export const UPDATE_GROUP_DETAILS = 'update_group_details';
export const UPDATE_GROUP_DETAILS_V2 = 'update_group_details_v2';
export const OPEN_GROUP_MEMBER_MODAL = 'open_group_member_modal';
export const OPEN_TEAM_CARD_MODAL = 'open_team_card_modal';
export const RECEIVE_GROUP_PENDING = 'receive_group_pending';
export const RECEIVE_GROUP_MEMBERS = 'receive_group_members';
export const GET_TEAM_CHANNELS = 'get_team_channels';
export const UPDATE_CARD_MODAL = 'update_card_modal';
export const UPDATE_MEMBERS = 'update_members';
export const UPDATE_GROUP_CAROUSEL_ORDER = 'update_group_carousel_order';
export const RECEIVE_GROUP_CARDS = 'receive_group_cards';
export const RECEIVE_FEATURED_GROUP_CARDS = 'receive_featured_group_cards';
export const REMOVE_SHARED_CARD = 'remove_shared_card';
export const DELETE_TEAM_CARD = 'delete_team_card';
export const UPDATE_MEMBER = 'update_member';
export const ADD_PENDING_MEMBER = 'add_pending_member';
export const OPEN_UPDATE_GROUP_DETAILS_MODAL = 'open_update_group_details_modal';
export const UPDATE_GROUP_USERS = 'update_group_users';
export const LEAVE_GROUP = 'leave_group';
export const JOIN_GROUP = 'join_group';
export const ACCEPT_INVITE = 'accept_invite';
export const DECLINE_INVITE = 'decline_invite';
export const UPDATE_GROUP_INFORMATION = 'update_group_information';
//groups v3
export const REMOVE_GROUP_MEMBER = 'remove_group_member';
export const REMOVE_PENDING_GROUP_MEMBER = 'remove_pending_group_member';

//group analytics
export const GET_GRAPH_DATA = 'get_graph_data';
export const SET_GROUP_DETAILS = 'set_group_details';
export const GET_TOP_CONTENT = 'get_top_content';
export const GET_TOP_CONTRIBUTORS = 'get_top_contributors';
export const GET_TEAM_ASSIGNMENT_METRICS = 'get_team_assignment_metrics';
export const OPEN_TEAM_ANALYTICS_USER_LIST_MODAL = 'open_team_analytics_user_list_modal';

//navigation
export const RECEIVE_NAVIGATION_DATA = 'receive_navigation_data';
export const RECEIVE_AVAILABLE_TEAMS = 'receive_available_teams';
export const RECEIVE_ASSIGNMENT_NOTIFICATIONS = 'receive_assignment_notifications';
export const DISMISS_ASSIGNMENT_NOTIFICATIONS = 'dismiss_assignment_notifications';

//discovery
export const RECEIVE_DISCOVERY_COURSES = 'receive_discovery_courses';
export const RECEIVE_DISCOVERY_VIDEOS = 'receive_discovery_videos';
export const RECEIVE_DISCOVERY_PATHWAYS = 'receive_discovery_pathways';
export const RECEIVE_DISCOVERY_JOURNEYS = 'receive_discovery_journeys';
export const RECEIVE_ECL_CONFIGS = 'receive_ecl_configs';
export const RECEIVE_FEATURED_PROVIDERS = 'receive_featured_providers';
export const GET_ALL_DISCOVER_CAROUSEL = 'get_all_discover_carousel';
export const UPDATE_DISCOVER_DATA = 'update_discover_data';

// onboarding
export const RECEIVE_INIT_ONBOARDING_STATE = 'receive_init_onboarding_state';
export const RECEIVE_TOPIC_SUGGESTIONS_USER = 'receive_topic_suggestions_user';
export const RECEIVE_SOURCE_SUGGESTIONS_USER = 'receive_source_suggestions_user';
export const RECEIVE_UPDATED_USER_INFO = 'receive_updated_user_info';
export const RECEIVE_UPDATED_USER_PREFS = 'receive_updated_user_prefs';
export const UPDATE_ONBOARDING_USER = 'update_onboarding_user';
export const RECEIVE_RECOMMENDED_INFLUENCERS = 'receive_recommended_influencers';
export const RECEIVE_RECOMMENDED_CHANNELS = 'receive_recommended_channels';
export const UPDATE_ONBOARDING_STEP = 'update_onboarding_step';
export const RECEIVE_TOPIC_SUGGESTIONS_USER_ONBOARDING =
  'receive_topic_suggestions_user_onboarding';
export const RECEIVE_TOPIC_SKILLS = 'receive_topic_skills';
export const ERROR_FETCHING_SKILLS = 'error_fetching_skills';
export const RECEIVE_USER_PROFILE = 'receive_user_profile';
export const SET_MULTILEVEL_TAXONOMY = 'set_multilevel_taxonomy';

//feed
export const RECEIVE_PROMOTED_FEED = 'receive_promoted_feed';
export const FIRST_FEED_TAB = 'first_feed_tab';

//assignments
export const REQUEST_USER_ASSIGNMENTS = 'request_user_assignments';
export const RECEIVE_USER_ASSIGNMENTS = 'receive_user_assignments';
export const UPDATE_ASSIGNMENT_COUNT = 'update_assignment_count';
export const UPDATE_ASSIGNMENT_COUNT_DONE = 'update_assignment_count_done';
export const RECEIVE_ASSIGNEMENTS = 'receive_assignments';
export const GET_ASSIGNED_USERS_BY_METRICS = 'get_assigned_users_by_metrics';

// activity team
export const GET_ACTIVITY_STREAM = 'get_activity_stream';
export const RECEIVE_NEW_ACTIVITY = 'receive_new_activity';
export const GET_ACTIVITY_TEAMS = 'get_activity_teams';

//curate
export const REQUEST_CURATE_CARDS = 'request_curate_cards';
export const RECEIVE_CURATE_CARDS = 'receive_curate_cards';
export const RECEIVE_CURATE_CHANGE_STATE = 'receive_curate_change_state';

//cards
export const RECEIVE_CARDS = 'receive_cards';
export const RECEIVE_CARD = 'receive_card';
export const RECEIVE_CARD_STATS = 'receive_card_stats';
export const RECEIVE_CARD_STATS_USERS = 'receive_card_stats_users';
export const RECEIVE_ECL_CARDS = 'receive_ecl_cards';
export const REQUEST_LIKE_COMMENT = 'request_like_comment';
export const RECEIVE_LIKE_COMMENT = 'receive_like_comment';

export const REQUEST_LIKE_CARD = 'request_like_card';
export const RECEIVE_LIKE_CARD = 'receive_like_card';
export const REQUEST_RATING_CARD = 'request_rating_card';
export const RECEIVE_RATING_CARD = 'receive_rating_card';

export const DELETE_CARD_COMMENT = 'delete_card_comment';
export const REQUEST_BOOKMARK_CARD = 'request_bookmark_card';
export const RECEIVE_BOOKMARK_CARD = 'receive_bookmark_card';
export const RECEIVE_DISMISS_CARD = 'receive_dismiss_card';
export const RECEIVE_CLEAR_CARD = 'receive_clear_card';
export const RECEIVE_DELETE_CARD = 'receive_delete_card';
export const CHANGE_CARD_STATE = 'change_card_state';

//config
export const TOGGLE_TOPNAV = 'toggle_topnav';

//modal
export const OPEN_CONFIRMATION_MODAL = 'open_confirmation_modal';
export const OPEN_PRIVATE_CARD_CONFIRMATION_MODAL = 'open_private_card_confirmation_modal';
export const CLOSE_MODAL = 'close_modal';
export const CLOSE_CARD_MODAL = 'close_card_modal';
export const CLOSE_INTEREST_MODAL = 'close_interest_modal';
export const OPEN_ASSIGN_MODAL = 'open_assign_modal';
export const OPEN_CARD_STATS_MODAL = 'open_card_stats_modal';
export const OPEN_STANDALONE_MODAL = 'open_standalone_modal';
export const CLOSE_STANDALONE_CARD_MODAL = 'close_standalone_card_modal';
export const OPEN_INTEGRATION_MODAL = 'open_integration_modal';
export const OPEN_ADD_TO_PATHWAY_MODAL = 'open_add_to_pathway_modal';
export const OPEN_ADD_TO_JOURNEY_MODAL = 'open_add_to_journey_modal';
export const OPEN_INLINE_CREATION_MODAL = 'open_inline_creation_modal';
export const OPEN_FOLLOW_USER_LIST_MODAL = 'open_follow_user_list_modal';
export const OPEN_STATUS_MODAL = 'open_status_modal';
export const OPEN_PATHWAY_PREVIEW_MODAL = 'open_pathway_preview_modal';
export const OPEN_SMARTBITE_PREVIEW_MODAL = 'open_smartbite_preview_modal';
export const OPEN_JOURNEY_PREVIEW_MODAL = 'open_journey_preview_modal';
export const OPEN_SMARTBITE_CREATION_MODAL = 'open_smatrbite_creation_modal';
export const OPEN_PATHWAY_CREATION_MODAL = 'open_pathway_creation_modal';
export const OPEN_UPDATE_INTERESTS_MODAL = 'open_update_interests_modal';
export const OPEN_UPDATE_INTERESTS_ONBOARD_V2_MODAL = 'open_update_interests_onboard_v2_modal';
export const OPEN_UPDATE_MULTILEVEL_INTERESTS_MODAL = 'open_update_multilevel_interests_modal';
export const OPEN_UPDATE_MULTILEVEL_EXPERTISE_MODAL = 'open_update_multilevel_expertise_modal';
export const OPEN_UPDATE_EXPERTISE_MODAL = 'open_update_expertise_modal';
export const OPEN_INSIGHT_EDIT_MODAL = 'open_insight_edit_modal';
export const OPEN_UPLOAD_IMAGE_MODAL = 'open_upload_image_modal';
export const OPEN_GROUP_CREATION_MODAL = 'open_group_creation_modal';
export const OPEN_JOURNEY_CREATION_MODAL = 'open_journey_creation_modal';
export const OPEN_REASON_MODAL = 'open_reason_modal';
export const OPEN_MDP_MODAL = 'open_mdp_modal';
export const OPEN_COMMENT_REASON_MODAL = 'open_comment_reason_modal';
export const OPEN_UPDATE_CHANNEL_DETAILS_MODAL = 'open_update_channel_details_modal';
export const OPEN_CHANNEL_CARDS_MODAL = 'open_channel_cards_modal';
export const OPEN_ADD_CURATORS_MODAL = 'open_add_curators_modal';
export const OPEN_CURATE_CARD_MODAL = 'open_curate_card_modal';
export const OPEN_CARD_ACTION_MODAL = 'open_card_action_modal';
export const UPDATE_DATA_IN_MODAL = 'update_data_in_modal';
export const OPEN_RELEVANCY_RATING_MODAL = 'open_relevancy_rating_modal';
export const OPEN_SHARE_MODAL = 'open_share_modal';
export const OPEN_MULTIACTIONS_MODAL = 'open_multiactions_modal';
export const OPEN_POST_TO_CHANNEL_MODAL = 'open_post_to_channel_modal';
export const OPEN_SHOW_CHANNELS_MODAL = 'open_show_channnels_modal';
export const OPEN_GROUP_INVITE_MODAL = 'open_group_invite_modal';
export const OPEN_SHOW_SKILL_MODAL = 'open_show_skill_modal';
export const OPEN_COURSES_MODAL = 'open_courses_modal';
export const OPEN_SHARE_CONTENT_MODAL = 'open_share_content_modal';
export const OPEN_CHANNEL_EDIT_MODAL = 'open_channel_edit_modal';
export const OPEN_SLIDE_OUT_CARD_MODAL = 'open_slide_out_card_modal';
export const UPDATE_DYNAMIC_SELECTION_FILTERS = 'update_dynamic_selection_filters';
export const UPDATE_DYNAMIC_SELECTION_FILTERS_V2 = 'update_dynamic_selection_filters_v2';
export const OPEN_CHANGE_AUTHOR_MODAL = 'open_change_author_modal';
export const OPEN_UNBOOKMARK_MODAL = 'open_unbookmark_modal';
export const OPEN_SKILLS_DIRECTORY_MODAL = 'open_skills_directory_modal';

//snackBar
export const OPEN_SNACKBAR = 'open_snackbar';
export const CLOSE_SNACKBAR = 'close_snackbar';

//ecl
export const ECL_RESULTS_FOUND = 'ecl_results_found';
export const CARD_TYPE_CLICKED = 'card_type_clicked';
export const ORIGIN_TYPE_CLICKED = 'origin_type_clicked';

//pathways
export const RECEIVE_PATHWAYS = 'receive_pathways';
export const RECEIVE_CARD_INTO_PATHWAYS = 'receive_card_into_pathways';
export const PATHWAYS_FOUND = 'pathways_found';
export const ADDED_CARD_TO_PATHWAY = 'added_card_to_pathway';
export const ERROR_WHILE_ADDING_TO_PATHWAY = 'error_while_adding_to_pathway';
export const OPEN_PATHWAY_CONGRATULATION_MODAL = 'open_pathway_congratulation_modal';
export const SAVE_TEMP_PATHWAY = 'save_temp_pathway';
export const DELETE_TEMP_PATHWAY = 'delete_temp_pathway';
export const SAVE_CONSUMPTION_PATHWAY = 'save_consumption_pathway';
export const SAVE_CONSUMPTION_PATHWAY_HISTORY_URL = 'save_consumption_pathway_history_url';
export const CONSUMPTION_PATHWAY_HISTORY = 'consumption_pathway_history';
export const REMOVE_CONSUMPTION_PATHWAY = 'remove_consumption_pathway';
export const REMOVE_CONSUMPTION_PATHWAY_HISTORY = 'remove_consumption_pathway_history';
export const SAVE_REORDER_CARD_IDS_PATHWAY = 'save_reorder_card_ids_pathway';
export const REMOVE_REORDER_CARD_IDS_PATHWAY = 'delete_reorder_card_ids_pathway';
export const IS_PREVIEW_MODE_PATHWAY = 'is_preview_mode_pathway';

//journey
export const RECEIVE_JOURNEYS = 'receive_journeys';
export const SAVE_TEMP_JOURNEY = 'save_temp_journey';
export const DELETE_TEMP_JOURNEY = 'delete_temp_journey';
export const SAVE_REORDER_CARD_IDS_JOURNEY = 'save_reorder_card_ids_journey';
export const REMOVE_REORDER_CARD_IDS_JOURNEY = 'delete_reorder_card_ids_journey';
export const IS_PREVIEW_MODE_JOURNEY = 'is_preview_mode_journey';
export const SAVE_CONSUMPTION_JOURNEY = 'save_consumption_journey';
export const SAVE_CONSUMPTION_JOURNEY_HISTORY_URL = 'save_consumption_journey_history_url';
export const SAVE_CONSUMPTION_JOURNEY_OPEN_BLOCK = 'save_consumption_journey_open_block';
export const SAVE_CONSUMPTION_JOURNEY_PATHWAY_PAYLOAD = 'save_consumption_journey_pathway_payload';
export const CONSUMPTION_JOURNEY_HISTORY = 'consumption_journey_history';
export const REMOVE_CONSUMPTION_JOURNEY = 'remove_consumption_journey';
export const REMOVE_CONSUMPTION_JOURNEY_HISTORY = 'remove_consumption_journey_history';

// providers
export const RECEIVE_ECL_ITEMS = 'receive_ecl_items';

//today learning
export const REQUEST_USER_INTEREST = 'request_user_interest';
export const RECEIVE_USER_INTEREST = 'receive_user_interest';

// search
export const RECEIVE_SEARCH_ITEMS = 'receive_search_items';
export const RECEIVE_FILTERS = 'recieve_filters';
export const UPDATE_COUNTS = 'update_counts';
export const UPDATE_MINIMAL_COUNTS = 'update_minimal_counts';
export const MODIFY_TYPE_FILTERS = 'modify_type_filters';
export const MODIFY_PLAN_FILTERS = 'modify_plan_filters';
export const MODIFY_SOURCE_FILTERS = 'modify_source_filters';
export const MODIFY_PROVIDER_FILTERS = 'modify_provider_filters';
export const MODIFY_SUBJECTS_FILTERS = 'modify_subjects_filters';
export const MODIFY_LANGUAGE_FILTERS = 'modify_languages_filters';
export const LOAD_MORE_SEARCH_ITEMS = 'load_more_search_items';
export const MODIFY_ACTION_BY_FILTERS = 'modify_action_by_filters';
export const RESET_STATE = 'reset_state';
export const SHOW_LOADER_WITH_RESET = 'show_loader_with_reset';
export const TURN_OFF_RESET_INLINE = 'turn_off_reset_inline';
export const SHOW_FILTERS = 'show_filters';
export const HIDE_FILTERS = 'hide_filters';
export const ERROR_FETCHING_SEARCH_RESULTS = 'error_fetching_search_results';
export const UPDATE_CARD_IN_RESULT = 'update_card_in_result';

// learning queue
export const RECEIVE_LEARNING_QUEUE_ITEMS = 'receive_learning_queue_items';
export const REMOVE_BOOKMARK_FROM_LEARNING_QUEUE = 'remove_bookmark_from_learning_queue';

// relevancy rating
export const UPDATE_RELEVANCY_RATING_QUEUE = 'update_relevancy_rating_queue';
export const FLUSH_RELEVANCY_RATING_QUEUE = 'flush_relevancy_rating_queue';
export const UPDATE_RATED_QUEUE_AFTER_RATING = 'update_rated_queue_after_rating';

//sociative
export const RECEIVE_SOCIATIVE_REGISTRATION_STATUS = 'receive_sociative_registration_status';

// my learning plan
export const QUARTERLY_ASSIGNMENTS = 'quarterly_assignments';
export const COMPETENCY_ASSIGNMENTS = 'competency_assignments';
export const RECEIVE_BOOKMARK_LEARNING_QUEUE = 'receive_bookmark_learning_queue';

// MLP v5

export const CONTENT_TAB_CHANGE = 'content_tab_change';
export const CONTENT_TYPE_TAB_CHANGE = 'content_type_tab_change';
export const GET_USER_INTEREST_FOR_MLP = 'get_user_interest_for_mlp';
export const STORE_QUARTERLY_ASSIGNMENTS = 'store_quarterly_assignments';
export const STORE_COMPETENCY_ASSIGNMENTS = 'store_competency_assignments';
export const LOAD_MORE_ASSIGNMENTS = 'load_more_assignments';
export const SEARCH_RESULT = 'search_result';
export const CLEAN_ASSIGNMENTS = 'clean_assignments';

// Monetization
export const OPEN_PAYMENT_MODAL = 'open_payment_modal';
export const OPEN_WALLET_PAYMENT_MODAL = 'open_wallet_payment_modal';
export const OPEN_CARD_WALLET_PAYMENT_MODAL = 'open_card_wallet_payment_modal';
export const OPEN_PAYPAL_SUCCESS_MODAL = 'open_paypal_success_modal';

// Share Content
export const ADD_SEARCHED_USER_FOR_SHARE = 'add_searched_user_for_share';
export const FETCH_CONTENT_TO_SHARE = 'fetch_content_to_share';
export const FETCH_MY_TEAM = 'fetch_my_team';
export const FETCH_USERS_FOR_SHARE = 'fetch_users_for_share';
export const ADD_USER_FOR_SHARE = 'add_user_for_share';
export const ADD_GROUP_FOR_SHARE = 'add_group_for_share';
export const REMOVE_USER_FROM_SHARE = 'remove_user_from_share';
export const REMOVE_GROUP_FROM_SHARE = 'remove_group_from_share';
export const CLEAR_SHARE_CONTENT_STATE = 'clear_share_content_state';
export const ADD_SEARCHED_GROUP_FOR_SHARE = 'add_searched_group_for_share';
export const REMOVE_SEARCHED_USER_FROM_SHARE = 'remove_searched_user_from_share';
export const REMOVE_SEARCHED_GROUP_FROM_SHARE = 'remove_searched_group_from_share';

// MultiAction Modal
export const ADD_SEARCHED_USER_FOR_ACTION = 'add_searched_user_for_action';
export const FETCH_CONTENT_TO_ACTION = 'fetch_content_to_action';
export const FETCH_MY_TEAM_FOR_ACTIONS = 'fetch_my_team_for_actions';
export const FETCH_USERS_FOR_ACTION = 'fetch_users_for_action';
export const FETCH_ALL_ASSIGNED = 'fetch_all_assigned';
export const FETCH_ASSIGNED_USERS_AND_GROUPS = 'fetch_assigned_users_and_groups';
export const FETCH_SEARCHED_ASSIGNED = 'fetch_searched_assigned';
export const ADD_USER_FOR_ACTION = 'add_user_for_action';
export const ADD_GROUP_FOR_ACTION = 'add_group_for_action';
export const REMOVE_USER_FROM_ACTION = 'remove_user_from_action';
export const REMOVE_GROUP_FROM_ACTION = 'remove_group_from_action';
export const CLEAR_ACTION_CONTENT_STATE = 'clear_action_content_state';
export const ADD_SEARCHED_GROUP_FOR_ACTION = 'add_searched_group_for_action';
export const REMOVE_SEARCHED_USER_FROM_ACTION = 'remove_searched_user_from_action';
export const REMOVE_SEARCHED_GROUP_FROM_ACTION = 'remove_searched_group_from_action';
export const CLEAR_REMOVE_USERS_LIST = 'clear_remove_users_list';
export const CLEAR_REMOVE_GROUPS_LIST = 'clear_remove_groups_list';
export const CLEAR_CURRENT_SEARCH = 'clear_current_search';
export const FILL_REMOVE_USERS_LIST = 'fill_remove_users_list';
export const FILL_REMOVE_GROUPS_LIST = 'fill_remove_groups_list';
export const FILL_ADD_USERS_LIST = 'fill_add_users_list';
export const FILL_ADD_GROUPS_LIST = 'fill_add_groups_list';

// close popovers
//export const CLOSE_POPOVER = 'close_popover';

// New Channel Reducer actions
export const RECIEVE_PINNED_CARDS = 'recieve_pinned_cards';
export const ADD_TO_PINNED_CARD_CAROUSEL = 'add_to_pinned_card_carousel';
export const REMOVE_FROM_PINNED_CARD_CAROUSEL = 'remove_from_pinned_card_carousel';

// New Group Reducer actions
export const RECEIVE_PINNED_CARDS_GROUP = 'receive_pinned_cards_group';
export const ADD_TO_PINNED_CARD_CAROUSEL_GROUP = 'add_to_pinned_card_carousel_group';
export const REMOVE_FROM_PINNED_CARD_CAROUSEL_GROUP = 'remove_from_pinned_card_carousel_group';

// remove assessment card from my learning plan in user panel
export const REMOVE_DISMISS_CARD = 'remove_dismiss_card';

// clear channle reducer data
export const REMOVE_ALL_PINNED_CARD = 'remove_all_pinned_card';

// channel v3 actions
export const SAVE_CHANNEL_DETAILS = 'save_channel_details';
export const SAVE_CHANNEL_FEATURED_CARDS = 'save_channel_fatured_cards';
export const REMOVE_CHANNEL_FEATURED_CARDS = 'remove_channel_fatured_cards';
export const OPEN_CHANNEL_CAROUSEL_CARDS_MODAL = 'open_channel_carousel_cards_modal';
export const ADD_FEATURED_CARD_IN_CHANNEL = 'add_featured_card_in_channel';
export const REMOVE_FEATURED_CARD_IN_CHANNEL = 'remove_featured_card_in_channel';
export const REMOVE_CARD_FROM_CHANNEL_CAROUSEL = 'remove_card_from_channel_carousel';
export const REMOVE_CHANNEL_CARD_FROM_CAROUSELS = 'remove_cards_from_carousels';
export const OPEN_CHANNEL_CURATE_CARDS_MODAL = 'open_channel_curate_cards_modal';
