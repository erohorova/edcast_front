const source = [
  'local_file_system',
  'imagesearch',
  'googledrive',
  'onedrive',
  'dropbox',
  'box',
  'github',
  'url',
  'onedriveforbusiness',
  'clouddrive',
  'evernote',
  'flickr',
  'gmail',
  'picasa',
  'facebook',
  'instagram',
  'customsource'
];

if (location.protocol === 'https:') {
  source.push('video', 'audio', 'webcam');
}

export const fileStackSources = source;
