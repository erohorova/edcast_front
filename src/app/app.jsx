import './styles.scss';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Routing from './Routing.jsx';
import Loadable from 'react-loadable';
import ThemeProvider from 'edc-web-sdk/components/ThemeProvider';
import { requestUserIsGroupLeader } from './actions/currentUserActions';
import { open as openSnackBar } from './actions/snackBarActions';
const ModalContainer = Loadable({
  loader: () => import('./components/modals/ModalContainer.jsx'),
  loading(props) {
    if (props.error) {
      return props.retry;
    } else {
      return <div style={{ display: 'none' }} />;
    }
  }
});
import ModalContainerStandalone from './components/modals/ModalContainerStandalone.jsx';
import SnackBarContainer from './components/snackbar/SnackBarContainer';
import colors from 'edc-web-sdk/components/colors';
import { toggleTopNav } from './actions/configActions';
import { setCookie, readCookie } from 'edc-web-sdk/helpers/cookies';
import { initiateGuideMe } from 'edc-web-sdk/helpers/guideme';
import * as upshotActions from './actions/upshotActions';
import has from 'lodash/has';
import { changeLanguage } from 'edc-web-sdk/helpers/translations';

let faviconLoaded = false;

class App extends Component {
  constructor(props, context) {
    super(props, context);
  }

  changeTranslationLanguage = lang => {
    if (!!lang) {
      let language = window.translationsMeta && window.translationsMeta.language;
      if (!(language && language == lang)) {
        changeLanguage(lang)
          .then(data => {
            // DO NOTHING
          })
          .catch(err => {
            console.error(`Error in app.changeTranslationLanguage.func: ${err}`);
          });
      }
    }
  };

  componentWillMount = async () => {
    try {
      const {
        dispatches,
        isLoggedIn,
        willRedirectToOnboarding,
        isOnboardingCompleted
      } = await window.bootstrapOnboarding();
      if (isLoggedIn && isOnboardingCompleted) {
        this.props.dispatch(requestUserIsGroupLeader());
        if (window.ldclient.variation('upshot-ai-integration', false)) {
          upshotActions.updateUpshotUserProfile({
            email: this.props.currentUser.get('email'),
            appuid: this.props.currentUser.get('id')
          });
        }
      }
      dispatches &&
        dispatches.forEach(d => {
          this.props.dispatch(dispatch => dispatch(d));
        });
      document.getElementById('splashscreen').className += ' loaded';
    } catch (err) {
      console.error(`Error in app.window.bootstrapOnboarding.func: ${err}`);
    }
  };

  componentDidMount() {
    if (this.props.query.newTopNav === 'true') {
      this.props.dispatch(toggleTopNav(true));
    } else {
      this.props.dispatch(toggleTopNav(false));
    }
  }

  showGuideme(guideMeConfig) {
    if (readCookie('first_sign_in') == 'true' && !!localStorage.getItem('isOnboardingCompleted')) {
      setCookie('first_sign_in', '');
      if (guideMeConfig.visible) {
        // initiateGuideMe('BAB01157-01B3-4848-8C8F-450965E54E10', '1981', 'liveTour')
        initiateGuideMe(guideMeConfig.userKey, guideMeConfig.tourId, guideMeConfig.tourType);
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.team.get('OrgConfig') !== nextProps.team.get('OrgConfig') ||
      this.props.currentUser.get('profile') !== nextProps.currentUser.get('profile')
    )
      return true;
    else return false;
  }

  isNotLoggedIn = () => {
    let location = window.location.href;

    return (
      location.indexOf('/onboarding') > 0 ||
      location.indexOf('/create_account') > 0 ||
      location.indexOf('/log_in') > 0 ||
      location.indexOf('/forgot_password') > 0
    );
  };

  updateLanguage = (userProps, teamProps) => {
    let user, team, lang;

    if (userProps) {
      user = userProps.toJS();
    }
    if (teamProps) {
      team = teamProps.toJS();
    }

    let userLang = user && user.profile && user.profile.language;
    let orgLang = team && team.config && team.config.DefaultOrgLanguage;

    if (this.isNotLoggedIn()) {
      lang = orgLang;
    } else {
      lang = userLang;
    }
    if (!~document.location.search.indexOf('?locale=')) {
      this.changeTranslationLanguage(lang);
    }
  };

  enableDisableRightClick() {
    let orgConfig = this.props.team && this.props.team.get('OrgConfig');
    let rightClickDisabled =
      orgConfig &&
      orgConfig.content &&
      orgConfig.content['web/content/disableRightClick'] &&
      orgConfig.content['web/content/disableRightClick'].value;

    let bodyTag = document.getElementsByTagName('body')[0];
    if (rightClickDisabled) {
      bodyTag.setAttribute('oncontextmenu', 'return false;');
    } else {
      bodyTag.removeAttribute('oncontextmenu');
    }
  }

  componentWillReceiveProps(nextProps) {
    this.enableDisableRightClick();
    if (
      nextProps.team.toJS().OrgConfig &&
      nextProps.team.toJS().OrgConfig.guideme['web/guideme/first_sign_in']
    ) {
      let guideMeConfig = nextProps.team.toJS().OrgConfig.guideme['web/guideme/first_sign_in'];
      this.showGuideme(guideMeConfig);
    }
    this.updateLanguage(nextProps.currentUser, nextProps.team);
    if (nextProps.team) {
      this.appendFavicon(nextProps.team.toJS());
    }
  }

  appendFavicon = team => {
    if (faviconLoaded) {
      return;
    }
    let sizes = {
      tiny: '16x16',
      small: '96x96',
      medium: '192x192',
      large: '32x32'
    };

    let favicons = team.favicons || {};
    if (Object.keys(favicons).length > 0) {
      faviconLoaded = true;
      Object.keys(sizes).forEach(function(key) {
        let link = document.querySelector("link[rel*='icon']") || document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'icon';
        link.sizes = sizes[key];
        link.href = favicons[key];
        document.getElementsByTagName('head')[0].appendChild(link);
      });
    }
  };

  render() {
    if (this.props.team.get('pending')) {
      return <div />;
    } else {
      let primaryColor = this.props.team.get('config').orgPrimaryColor;

      let secondaryColor = this.props.team.get('config').orgSecondaryColor;

      let followButtonColor = primaryColor;

      let markAsCompleteBackgroundColor = this.props.team.get('config')
        .mark_as_complete_button_background_color;

      let markAsCompleteTextColor = this.props.team.get('config').mark_as_complete_button_color;

      let completedBackgroundColor = this.props.team.get('config')
        .completed_button_background_color;

      let completedTextColor = this.props.team.get('config').completed_button_text_color;

      colors.updateColors({
        primaryColor,
        secondaryColor,
        followButtonColor,
        markAsCompleteBackgroundColor,
        markAsCompleteTextColor,
        completedBackgroundColor,
        completedTextColor
      });
      let font = this.props.team.getIn(['config', 'orgFont']);
      if (font) {
        let styleSheet = document.styleSheets[document.styleSheets.length - 1];
        if (
          styleSheet.href &&
          styleSheet.href.indexOf('upshot.css') !== -1 &&
          document.styleSheets.length >= 2
        ) {
          styleSheet = document.styleSheets[document.styleSheets.length - 2];
        }
        if (styleSheet.insertRule) {
          styleSheet.insertRule(`div * {font-family: ${font} !important}`, 0);
        } else {
          //IE
          styleSheet.addRule('div *', `{font-family: ${font} !important}`, -1);
        }
      }
      return (
        <ThemeProvider>
          <div>
            <ModalContainer />
            <ModalContainerStandalone />
            <SnackBarContainer />
            <Routing history={this.props.history} />
          </div>
        </ThemeProvider>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    team: state.team,
    query: state.routing.locationBeforeTransitions.query,
    currentUser: state.currentUser,
    pathname: state.routing.locationBeforeTransitions.pathname
  };
}

App.propTypes = {
  team: PropTypes.object,
  currentUser: PropTypes.object,
  user: PropTypes.object,
  history: PropTypes.object,
  query: PropTypes.object
};

export default connect(mapStateToProps)(App);
