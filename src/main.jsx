import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/app.jsx';
import { browserHistory } from 'react-router';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import {
  syncHistoryWithStore,
  routerReducer as routing,
  routerMiddleware
} from 'react-router-redux';
import currentUser from './app/reducers/currentUserReducer';
import users from './app/reducers/usersReducer';
import discovery from './app/reducers/discoveryReducer';
import cards from './app/reducers/cardsReducer';
import pathways from './app/reducers/pathwaysReducer';
import ecl from './app/reducers/eclReducer';
import results from './app/reducers/resultsReducer';
import feed from './app/reducers/feedReducer';
import todayLearning from './app/reducers/todayLearningReducer';
import modal from './app/reducers/modalReducer';
import modalStandAlone from './app/reducers/modalStandAloneReducer';
import snackbar from './app/reducers/snackBarReducer';
import curate from './app/reducers/curateReducer';
import onboarding from './app/reducers/onboardingReducer';
import onboardingv3 from './app/reducers/onboardingV3Reducer';
import channels from './app/reducers/channelsReducer';
import channelsV2 from './app/reducers/channelsReducerV2';
import courses from './app/reducers/coursesReducer';
import groups from './app/reducers/groupsReducer';
import groupsV2 from './app/reducers/groupsReducerV2';
import groupAnalytic from './app/reducers/groupAnalyticReducer';
import assignments from './app/reducers/assignmentsReducer';
import origin from './app/reducers/originReducer';
import team from './app/reducers/teamReducer';
import config from './app/reducers/configReducer';
import search from './app/reducers/searchReducer';
import learningQueue from './app/reducers/learningQueueReducer';
import teamActivity from './app/reducers/teamActivityReducer';
import relevancyRating from './app/reducers/relevancyRatingReducer';
import sociative from './app/reducers/sociativeReducer';
import myLearningPlan from './app/reducers/myLearningPlansReducer';
import journey from './app/reducers/journeyReducer';
import mlpv5 from './app/reducers/mylearningplanV5Reducer';
import routerLocations from './app/reducers/routerLocationsReduser';
import carousel from './app/reducers/carouselsReducer';
import shareContent from './app/reducers/shareContentReducer';
import contentMultiaction from './app/reducers/multiactionReducer';
import channelNewReducer from './app/reducers/channelNewReducer';
import profile from './app/reducers/profileReducer';
import teamNewReducer from './app/reducers/teamNewReducer';
import channelReducerV3 from './app/reducers/channelReducerV3';

const store = createStore(
  combineReducers({
    routing,
    currentUser,
    discovery,
    users,
    feed,
    cards,
    pathways,
    modal,
    modalStandAlone,
    snackbar,
    curate,
    onboarding,
    onboardingv3,
    groups,
    groupsV2,
    groupAnalytic,
    channels,
    channelsV2,
    courses,
    ecl,
    results,
    assignments,
    origin,
    routerLocations,
    team,
    todayLearning,
    config,
    search,
    learningQueue,
    teamActivity,
    relevancyRating,
    sociative,
    myLearningPlan,
    journey,
    mlpv5,
    carousel,
    shareContent,
    contentMultiaction,
    channelNewReducer,
    profile,
    teamNewReducer,
    channelReducerV3
  }),
  compose(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(browserHistory)),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

browserHistory.listen(() => {
  if (document) {
    const officeKey = 'redirectTo_App';
    if (~window.location.href.indexOf(officeKey)) {
      localStorage.setItem('redirectTo_App', window.location.search.slice(`?${officeKey}=`.length));
    }
    document.getElementsByTagName('body')[0].style.overflow = '';
  }
});

const history = syncHistoryWithStore(browserHistory, store);

// Once LaunchDarkly is ready, Render the app to remove any "flashing"
window.ldclient.waitUntilReady
  .then(() => {
    ReactDOM.render(
      <Provider store={store}>
        <App history={history} />
      </Provider>,
      document.getElementById('edc-web-body')
    );
  })
  .catch(err => {
    console.error(`Error in main.ldclient.waitUntilReady.func: ${err}`);
  });
