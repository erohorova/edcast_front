import * as actionTypes from '../constants/actionTypes';

export function syncAction() {
  return {
    type: actionTypes.SOME_ACTION,
    someParam: {}
  };
}

export function asyncAction() {
  return dispatch => {
    dispatch({
      type: actionTypes.SOME_ACTION,
      someParam: {}
    });
  };
}
