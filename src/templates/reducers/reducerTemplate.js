/**
 * Created by ypling on 7/5/16.
 */
import * as actionTypes from '../constants/actionTypes';
import Immutable from 'immutable';
export default function reducerTemplate(state = Immutable.Map({}), action) {
  let newState;
  switch (action.type) {
    case actionTypes.SOME_ACTION:
      newState = state;
      return newState;
    default:
      return state;
  }
}
