import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ComponentTemplate extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <h4>ComponentTemplate</h4>
      </div>
    );
  }
}

ComponentTemplate.propTypes = {};

export default ComponentTemplate;
