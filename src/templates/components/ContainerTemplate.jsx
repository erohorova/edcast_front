import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class ContainerTemplate extends Component {
  constructor(props, context) {
    super(props, context);
    this.buttonClickHandler = this.buttonClickHandler.bind(this);
  }

  render() {
    return (
      <div>
        <h4>Container</h4>
      </div>
    );
  }
}

ContainerTemplate.propTypes = {};

function mapStoreStateToProps(state) {
  return state;
}

export default connect(mapStoreStateToProps)(ContainerTemplate);
