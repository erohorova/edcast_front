const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanUpStatsPlugin = require('./config/CleanUpStatsPlugin');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    bootstrap: [
      'babel-polyfill',
      path.resolve(__dirname, 'src/app/polyfill.js'),
      path.resolve(__dirname, 'src/bootstrap.jsx')
    ],
    main: [path.resolve(__dirname, 'src/main.jsx')]
  },
  output: {
    filename: '[name].js',
    publicPath: 'http://localhost:8001/'
  },
  devtool: '#inline-source-map',
  resolve: {
    modules: ['src', 'sass', 'public', 'node_modules'],
    // When requiring, you don't need to add these extensions
    extensions: ['.js', '.jsx'],
    enforceExtension: false
  },
  devServer: {
    proxy: {
      '/api/*': 'http://localhost:4000',
      '/cms/*': 'http://localhost:4000',
      '/assets/*': 'http://localhost:4000',
      '/follows': 'http://localhost:4000',
      '/system/*': 'http://localhost:4000',
      '/auth/*': 'http://localhost:4000',
      '/posts/*': 'http://localhost:4000',
      '/comments/*': 'http://localhost:4000'
    },
    publicPath: 'http://localhost:8001/',
    hot: true,
    progress: true,
    quiet: false,
    noInfo: false,
    historyApiFallback: true,
    stats: {
      assets: true,
      colors: true,
      version: false,
      hash: true,
      timings: true,
      chunks: false,
      chunkModules: false
    },
    headers: { 'Access-Control-Allow-Origin': '*' },
    host: 'localhost',
    port: 8001
  },
  resolveLoader: {
    moduleExtensions: ['-loader']
  },
  module: {
    rules: [
      // First, run the linter.
      // It's important to do this before Babel processes the JS.
      {
        test: /\.(js|jsx)$/,
        enforce: 'pre',
        use: [
          {
            options: {
              eslintPath: require.resolve('eslint')
            },
            loader: require.resolve('eslint-loader')
          }
        ],
        include: path.resolve(__dirname, 'src')
      },
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules.(?!edc-web-sdk|blackbox-notifications)/, /vendor/],
        use: [
          {
            loader: 'react-hot-loader'
          },
          {
            loader: 'babel-loader'
          }
        ]
      },
      {
        test: /\.(ico|gif|png|jpg|jpeg|svg|webp)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          context: 'public',
          name: './images/[name].[ext]'
        }
      },
      {
        test: /\.(html|css)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.(scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [
                  path.resolve(__dirname, './node_modules'),
                  path.resolve(__dirname, './node_modules/edc-web-sdk/node_modules')
                  //     path.resolve(__dirname, "./node_modules/blackbox-notifications/node_modules"),
                ]
              }
            }
          ]
          // publicPath: "/dist",
        })
      }
    ]
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
      'process.env.ECL_HOST': JSON.stringify(process.env.ECL_HOST),
      'process.env.TAXONOMY_HOST': JSON.stringify(process.env.TAXONOMY_HOST),
      'process.env.WALLET_HOST': JSON.stringify(process.env.WALLET_HOST),
      'process.env.JS_PUBLIC_KEY': JSON.stringify(process.env.JS_PUBLIC_KEY),
      'process.env.UpshotApplicationID': JSON.stringify(process.env.UpshotApplicationID),
      'process.env.UpshotApplicationOwnerID': JSON.stringify(process.env.UpshotApplicationOwnerID),
      'process.env.TRANSLATION_DOMAIN': JSON.stringify(process.env.TRANSLATION_DOMAIN),
      'process.env.FILESTACK_APIKEY': JSON.stringify(process.env.FILESTACK_APIKEY),
      'process.env.FILESTACK_DEFAULT_IMAGES_URL': JSON.stringify(
        process.env.FILESTACK_DEFAULT_IMAGES_URL
      ),
      'process.env.FILESTACK_CIPHER': JSON.stringify(process.env.FILESTACK_CIPHER),
      'process.env.FILESTACK_SECRET': JSON.stringify(process.env.FILESTACK_SECRET),
      'process.env.SECURE_AUTHENTICATED_IMAGES': JSON.stringify(
        process.env.SECURE_AUTHENTICATED_IMAGES
      ),
      'process.env.SECURE_AUTHENTICATED_IMAGES_KEY': JSON.stringify(
        process.env.SECURE_AUTHENTICATED_IMAGES_KEY
      )
    }),
    new ExtractTextPlugin({
      filename: '[name].css'
    }),
    // to see eslint logs instead of unnecessary ExtractTextPlugin logs
    new CleanUpStatsPlugin()
  ],
  node: {
    fs: 'empty'
  }
};
