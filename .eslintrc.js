module.exports = {
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: [
    // "eslint:all",
    // "eslint:recommended",
    // "plugin:react/all",
  ],
  plugins: ['react', 'promise'],
  parserOptions: {
    allowImportExportEverywhere: false,
    codeFrame: false,
    sourceType: 'module'
  },
  rules: {
    'react/no-this-in-sfc': 'error',
    'promise/catch-or-return': 'error',
    'no-duplicate-imports': 'error',
    'no-param-reassign': 'error',
    'require-yield': 'error',
    'no-unreachable': 'error',
    'block-scoped-var': 'error',
    'no-shadow': 'error',
    'no-undef': 'error',
    'no-dupe-args': 'error',
    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-redeclare': 'error',
    'constructor-super': 'error',
    'no-this-before-super': 'error',
    'no-const-assign': 'error',

    'react/prop-types': ['warn', { ignore: ['dispatch'] }],
    'handle-callback-err': 'warn',
    'callback-return': 'warn',
    'no-constant-condition': 'warn',
    'default-case': 'warn'

    // "no-unused-vars": ["warn", { "varsIgnorePattern": "(trl?|require|React)" }],
    // "max-statements-per-line": "warn",
  }
};
