

# Setup
EdCast WWW Interface

#### Overview
`edc-web` is the client interface for EdCast.

#### Local

References:<br/>
[NodeJS](https://nodejs.org/)<br/>
[NPM](https://docs.npmjs.com/)

1. `npm install`
1. `npm run dev`

Note: If you need to run a specific host (e.g. `org1.lvh.me`), you must pass in the param on start
`HOST=org1 npm run dev`

Accessing the site is through `http://{ORG}.lvh.me:4001/`

***

# How Web is built and deployed

References:<br/>
[Jenkins](https://jenkins.edcasting.co)<br/>
[AWS](https://cm-prod.signin.aws.amazon.com/console)

## QA

#### Details
Under the `Node` Application on AWS, the main QA EBS is `edc-web-qa`. This can be deployed to by running the Slack command `/deploywebqa`. This will run the Jenkins build on [EdCastWeb-QA](https://jenkins.edcasting.co/job/EdCastWeb-QA/) project.

#### Building
Under the Jenkins configuration, you can view the build configuration. This build is performed in these steps.

1. Pull the latest commit from `edc-web` on GitHub (`dev` branch)
1. Get the local Jenkins `ssh-agent` and `ssh-add` the RSA private key to the agent
1. Modify `package.json` to allow a connection to `edc-web-sdk` for the build dependencies.
1. `npm install`
1. `npm run build` which generates all the assets and versioning
1. The build will package up all files except `node_modules` and publish those to AWS

#### Deploy
In Slack, run `/deploywebqa` to submit a request to Jenkins to start the build process. Note: There will be no response in the Slack channel.

## Production

#### Overview
Production is deployed manually currently from the AWS web console.

#### Deploy
To deploy to production:

1. Find `edc-web-prod` environment
1. Click "Upload and Deploy"
1. In the modal, select "go to the Applications Versions page"
1. Find the latest version (or select another version). The latest on QA will be denoted by "Deployed to" column saying "edc-web-qa"
1. Check the Version you want, and click Deploy
1. Select "edc-web-prod" under the "Environment" dropdown.
1. Click "Deploy"

After a few minutes, that version will be deployed to `edc-web-prod`

## Testing from local on QA

Run this script in your console on http://qa.cmnetwork.co:

```
_babelPolyfill = null;
var script = document.createElement("script")
script.type = "text/javascript";
script.src = "http://localhost:4001/assets.js";
document.getElementsByTagName("head")[0].appendChild(script);
```

CSS Injection

```
var link = document.createElement("link");
link.href = "http://localhost:8001/main.css";
link.type = "text/css";
link.rel = "stylesheet";
document.getElementsByTagName("head")[0].appendChild(link);
```
