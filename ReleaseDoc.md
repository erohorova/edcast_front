# Upcoming Release v2.55.0

### LD FLag:
  `show-bookmarks-on-left-pannel` : for new bookmarks left block (EP-22291)
  `reduce-header-size` : for reduce header size (EP-22109)
  `configurable-colors-for-complete-button` : for complete button colors changing (EP-21843)
  `enable-custom-badges-for-clc` : for Custom Badges S3 bucket for CLC badges enabling (EP-22970)
  `new-login-page-settings` : for show new login settings on admin panel (EP-21965)

# Upcoming Release v2.54.0

### LD FLag:
  `new-modal-and-toast` : for card details in toast messages  (EP-22440)
  `admin-search-option` : for search option on Admin Console (EP-22497)

# Upcoming Release v2.52.0

### LD FLag:
  `public-profile-view-v4` : for new public profile view (EP-17111)
  `is-show-user-job` : for show user job title (EP-20629)
  `people-card-v2` : for new people card design (EP-20628)
  `search-in-my-learning-plan` : for showing search on My learning plan (EP-18296)
### Route addded:
  `/@(:handle)/content` : for new public profile view (EP-17111)

# Upcoming Release v2.51.0

### LD FLag:
  `card-v3` : for show new card version (EP-13392)
  `users-fields-edit-enabled` : for ability to edit user fields (EP-18007)


# Upcoming Release v2.50.0

### LD FLag:
  `channel-tuning-tab` : for show/hide Tuning tab in ChannelEditorModal (EP-20140);

### Environment Variables
	* EP-18087<br>
	  Description: Add public and private key to environment variables to encrypt and decrypt payload <br>

	  **JS_PUBLIC_KEY=value**



# Upcoming Release v2.49.0

### LD FLag:
  `hide-upload-credential` : TRUE (only for `anz` client);

  `header-search-with-dropdowns` : add select in header search (EP-19048);

  `group-creation-modal-v3`: updated Group Creation Flow (EP-19678);


# Upcoming release v2.46.0

### Deployment Dependencies
- EP-17894<br>
  Description: Public instances like FutureSkillsNasscom and NASSCOM should not show provider SSOs - Will be deprecated soon<br>
---------------------------------------------------------------------------------------------------------------------------------------------
# Upcoming release v2.44.0
---------------------------------------------------------------------------------------------------------------------------------------------
# Upcoming release v2.43.0

### Deployment Dependencies
- EP-13783-Sample<br>
  Remove following environmental variables - <br>
   **FEATURE_FILESTACK_SHIRE**, **FEATURE_FILESTACK_SPARK**, **FEATURE_FILESTACK_UPSTREAM**, **FEATURE_FILESTACK_PMI**, **FEATURE_FILESTACK_CHEMONICS**, **FEATURE_FILESTACK_HPE**, **FEATURE_FILESTACK_HPA** from Elastic Bean stalk.<br>
  There is no code change dependency for this ticket.

- EP-14189<br>
  Disable the flag **allow-consumer-modify-level** for all the organizations. We are only allowing author and admin to edit the BIA.
