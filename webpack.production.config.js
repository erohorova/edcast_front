const webpack = require('webpack');
const path = require('path');
const version = require('./version.json');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('compression-webpack-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const glob = require('glob');

const generalDynamicUrlToDependencies = [
  'version.json'
  // ...glob.sync(`public/dist-*`),
];

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    bootstrap: [
      'babel-polyfill',
      path.resolve(__dirname, 'src/app/polyfill.js'),
      path.resolve(__dirname, 'src/bootstrap.jsx')
    ],
    main: [path.resolve(__dirname, 'src/app/polyfill.js'), path.resolve(__dirname, 'src/main.jsx')]
  },
  output: {
    filename: `dist-${version.v}-[name].js`,
    chunkFilename: `dist-${version.v}-[name].chunk.js`,
    path: path.resolve(__dirname, 'public'),
    publicPath: '/'
  },
  resolve: {
    // When requiring, you don't need to add these extensions
    extensions: ['.js', '.jsx'],
    enforceExtension: false
  },
  stats: {
    warnings: false
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: [/node_modules.(?!(edc-web-sdk|blackbox-notifications))/, /vendor/],
        loader: 'babel-loader'
      },
      {
        test: /\.(html|css)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]'
        }
      },
      {
        test: /\.(s?css)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [
                  path.resolve(__dirname, './node_modules'),
                  path.resolve(__dirname, './node_modules/edc-web-sdk/node_modules'),
                  path.resolve(__dirname, './node_modules/blackbox-notifications/node_modules')
                ]
              }
            }
          ]
          // publicPath: "/dist",
        })
      },
      {
        test: /\.(ico|gif|png|jpg|jpeg|svg|webp)$/,
        exclude: /node_modules/,
        loader: 'file-loader',
        options: {
          context: 'public',
          name: './images/[name].[ext]'
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
      'process.env.ECL_HOST': JSON.stringify(process.env.ECL_HOST),
      'process.env.TAXONOMY_HOST': JSON.stringify(process.env.TAXONOMY_HOST),
      'process.env.WALLET_HOST': JSON.stringify(process.env.WALLET_HOST),
      'process.env.JS_PUBLIC_KEY': JSON.stringify(process.env.JS_PUBLIC_KEY),
      'process.env.UpshotApplicationID': JSON.stringify(process.env.UpshotApplicationID),
      'process.env.UpshotApplicationOwnerID': JSON.stringify(process.env.UpshotApplicationOwnerID),
      'process.env.TRANSLATION_DOMAIN': JSON.stringify(process.env.TRANSLATION_DOMAIN),
      'process.env.FILESTACK_DEFAULT_IMAGES_URL': JSON.stringify(
        process.env.FILESTACK_DEFAULT_IMAGES_URL
      ),
      'process.env.FILESTACK_APIKEY': JSON.stringify(process.env.FILESTACK_APIKEY),
      'process.env.FILESTACK_CIPHER': JSON.stringify(process.env.FILESTACK_CIPHER),
      'process.env.FILESTACK_SECRET': JSON.stringify(process.env.FILESTACK_SECRET),
      'process.env.SECURE_AUTHENTICATED_IMAGES': JSON.stringify(
        process.env.SECURE_AUTHENTICATED_IMAGES
      ),
      'process.env.SECURE_AUTHENTICATED_IMAGES_KEY': JSON.stringify(
        process.env.SECURE_AUTHENTICATED_IMAGES_KEY
      )
    }),
    new ExtractTextPlugin(`dist-${version.v}-[name].css`),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    }),
    new webpack.optimize.UglifyJsPlugin(), //minify everything
    new webpack.optimize.AggressiveMergingPlugin(), //Merge chunks
    // we don't need moment locale, just keep english moment locale
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      minRatio: 1
    })
    // TODO: remove it before merging in mater
    // new BundleAnalyzerPlugin({
    //   analyzerMode: 'server',
    //   defaultSizes: 'gzip',
    //   openAnalyzer: true,
    // })

    // Generate a service worker script that will precache, and keep up to date,
    // the HTML & assets that are part of the Webpack build.
    // new SWPrecacheWebpackPlugin({
    //   // By default, a cache-busting query parameter is appended to requests
    //   // used to populate the caches, to ensure the responses are fresh.
    //   // If a URL is already hashed by Webpack, then there is no concern
    //   // about it being stale, and the cache-busting can be skipped.
    //   dontCacheBustUrlsMatching: /\.\w{8}\./,
    //   filename: 'service-worker.js',
    //   mergeStaticsConfig: true,
    //   stripPrefix: 'public/',
    //   maximumFileSizeToCacheInBytes: 10 * 1024 * 1024,
    //   staticFileGlobs: [
    //     // e.g. images for the splashscreen and inital html for critical rendering path
    //     'public/i/images/apple-touch-icon*',
    //     'public/i/imagesmstile-144x144.png',
    //     'public/i/magesbrowserconfig.xml',
    //     'public/i/images/manifest.json',
    //     'public/i/images/loading.gif',
    //   ],
    //   dynamicUrlToDependencies: {
    //     '/': [...generalDynamicUrlToDependencies],
    //     '/assets.js': [
    //       ...generalDynamicUrlToDependencies,
    //       // `server/config/views/assets.js`,
    //     ],
    //   },
    //   ignoreUrlParametersMatching: [
    //     /auto_sso/,
    //     /from_/,
    //   ],
    //   directoryIndex: '/',
    //   runtimeCaching: [{
    //     urlPattern: /^\/?$/,
    //     handler: 'cacheFirst'
    //   }],
    //   logger(message) {
    //     if (message.indexOf('Total precache size is') === 0) {
    //       // This message occurs for every build and is a bit too noisy.
    //       return;
    //     }
    //     if (message.indexOf('Skipping static resource') === 0) {
    //       // This message obscures real errors so we ignore it.
    //       // https://github.com/facebookincubator/create-react-app/issues/2612
    //       return;
    //     }
    //     console.log(message);
    //   },
    //   // minify: true,
    //   // For unknown URLs, fallback to the index page
    //   navigateFallback: '/',
    //   // IMPORTANT: keep this list in sync with appropriate CloudFront configs
    //   // these routes will fall back to / html page
    //   navigateFallbackWhitelist: [
    //     /^\/log_in/,
    //     /^\/\@/,
    //     /^\/me/,
    //     /^\/insights((?!visit).)*$/, // do not end by "visit", as this handles by rails backend
    //     /^\/discover/,
    //     /^\/onboarding/,
    //     /^\/team-learning/,
    //     /^\/teamLearning/,
    //     /^\/required/,
    //     /^\/curate/,
    //     /^\/featured/,
    //     /^\/video_streams/,
    //     /^\/channel/,
    //     /^\/settings/,
    //     /^\/todays-learning/,
    //     /^\/teams/,
    //     /^\/feed/,
    //     /^\/search/,
    //     /^\/pathways/,
    //     /^\/widgets/,
    //     /^\/my-assignments/,
    //     /^\/create_account/,
    //     /^\/todays-learning-search/,
    //     /^\/leaderboard/,
    //     /^\/sign_up/,
    //     /^\/notifications/,
    //     /^\/forgot_password/,
    //     /^\/journey/,
    //   ],
    //   // Don't precache sourcemaps (they're large) and build asset manifest:
    //   staticFileGlobsIgnorePatterns: [
    //     /\.map$/,
    //     /asset-manifest\.json$/,
    //     /[aA]xis/,
    //   ],
    // }),
  ],
  node: {
    fs: 'empty'
  }
};
