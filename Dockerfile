FROM node:6

ARG SSH_PRIVATE_KEY

RUN mkdir -p ~/.ssh && umask 0077 && echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa \
  && git config --global url."git@github.com:".insteadOf https://github.com/ \
  && ssh-keyscan github.com >> ~/.ssh/known_hosts

WORKDIR /var/app

RUN chmod 700 /root/.ssh/id_rsa

RUN echo "Host github.com\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
RUN ssh-keyscan github.com >> ~/.ssh/known_hosts

COPY . /var/app/

RUN npm install node-sass \
  && npm install -g bower \
  && npm install \
  && npm run build

RUN rm /root/.ssh/id_rsa

CMD ["NODE_ENV=production node app.js"]
